<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests;

use Exception;
use Ox\Core\Auth\User;
use Ox\Core\CApp;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\Admin\CUser;
use Ox\Tests\JsonApi\Collection;
use Ox\Tests\JsonApi\Error;
use Ox\Tests\JsonApi\Item;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OxWebTestCase extends WebTestCase
{
    use OxTestTrait {
        setConfig as setConfigTrait;
    }
    use OxAssertionsTrait;

    protected Translator $translator;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->translator = new Translator();
    }

    public function setConfig($configs)
    {
        $configs['standard'] += [
            'anti_csrf_enable' => '0',
        ];

        $this->setConfigTrait($configs);
    }

    /**
     * Get response content
     *
     * @param KernelBrowser $client
     * @param bool          $decoded
     *
     * @return array
     * @throws TestsException
     */
    protected function getResponseContent(KernelBrowser $client, bool $decoded = true): array
    {
        $content = $client->getResponse()->getContent();
        if ($decoded) {
            $content = json_decode($content, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new TestsException(json_last_error_msg());
            }
        }

        return mb_convert_encoding($content, 'Windows-1252', 'UTF-8');
    }

    /***
     * Get response content and return JSON:API item
     *
     * @param KernelBrowser $client
     *
     * @return Item
     * @throws TestsException
     */
    protected function getJsonApiItem(KernelBrowser $client): Item
    {
        return Item::createFromArray($this->getResponseContent($client));
    }

    /**
     * Get response content and return JSON:API collection
     *
     * @param KernelBrowser $client
     *
     * @return Collection
     * @throws TestsException
     */
    protected function getJsonApiCollection(KernelBrowser $client): Collection
    {
        return Collection::createFromArray($this->getResponseContent($client));
    }

    /**
     * Get response content and return JSON:API error
     *
     * @param KernelBrowser $client
     *
     * @return Error
     * @throws TestsException
     */
    protected function getJsonApiError(KernelBrowser $client): Error
    {
        return Error::createFromArray($this->getResponseContent($client));
    }

    /**
     * @param array      $options
     * @param array      $server
     * @param CUser|null $user
     * @param bool       $force_auth_current_user
     *
     * @return KernelBrowser
     * @throws Exception
     */
    protected static function createClient(
        array  $options = [],
        array  $server = [],
        ?CUser $user = null,
        bool   $force_auth_current_user = true
    ): KernelBrowser {
        // Force APP_ENV to 'test'
        $server['APP_ENV'] = 'test';

        $client = parent::createClient($options, $server);

        // Current user
        $current_user = CUser::get();

        // Auth phpunit user (previously auth by TestBootstrap::boot)
        if ($user === null && $force_auth_current_user) {
            if ($current_user->user_username !== CUser::USER_PHPUNIT) {
                $user = TestBootstrap::authUser();
            } else {
                $user = $current_user;
            }

            $client->loginUser(new User($user));

            return $client;
        }

        // No auth forced
        if ($user === null) {
            return $client;
        }

        // Auth another user
        if ($user !== $current_user) {
            // Legacy compat
            TestBootstrap::authUser($user);
        }

        // SF untracked_token_storage_auth
        $client->loginUser(new User($user));

        return $client;
    }

    protected function setJsonApiContentTypeHeader(KernelBrowser $client): KernelBrowser
    {
        $client->setServerParameter('CONTENT_TYPE', 'application/vnd.api+json');

        return $client;
    }

    protected function resetClient(
        array  $options = [],
        array  $server = [],
        ?CUser $user = null,
        bool   $force_auth_current_user = true
    ): KernelBrowser {
        self::ensureKernelShutdown();

        return static::createClient($options, $server, $user, $force_auth_current_user);
    }
}
