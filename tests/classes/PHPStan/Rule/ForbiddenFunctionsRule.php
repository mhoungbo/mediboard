<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests\PHPStan\Rule;

use LogicException;
use PhpParser\Node;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\CallLike;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\New_;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\ReflectionProvider;
use PHPStan\Rules\Rule;
use PHPStan\Type\Constant\ConstantStringType;
use PHPStan\Type\Generic\GenericClassStringType;
use PHPStan\Type\TypeUtils;

use function count;
use function explode;
use function is_string;
use function sprintf;

/**
 * Inspired from https://github.com/voku/phpstan-rules-1#forbidcustomfunctionsrule
 */
class ForbiddenFunctionsRule implements Rule
{
    private const ANY = '*';
    private const FUNCTION = '';

    private array $data = [];

    private ReflectionProvider $reflectionProvider;

    /**
     * @param array                $data
     * @param ReflectionProvider   $reflectionProvider
     */
    public function __construct(array $data, ReflectionProvider $reflectionProvider)
    {
        $this->reflectionProvider = $reflectionProvider;

        foreach ($data as $item) {
            if (!array_key_exists('rules', $item) || empty($item['rules'])) {
                throw new LogicException('Rules expected');
            }

            $rules = $item['rules'];

            if (!array_key_exists('scopes', $item) || empty($item['scopes'])) {
                $this->buildRules($rules);
            } else {
                $scopes = $item['scopes'];
                foreach ($scopes as $scope) {
                    $this->buildRules($rules, $scope);
                }
            }
        }
    }

    public function buildRules(array $rules, string $scope = self::ANY): void
    {
        foreach ($rules as $rule => $description) {
            if (!is_string($description)) {
                throw new LogicException('Unexpected forbidden function description, string expected');
            }

            $parts = explode('::', $rule);

            if (count($parts) === 1) {
                $className  = self::FUNCTION;
                $methodName = $parts[0];
            } elseif (count($parts) === 2) {
                $className  = $parts[0];
                $methodName = $parts[1];
            } else {
                throw new LogicException(
                    "Unexpected format of forbidden function {$rule}, expected Namespace\Class::methodName"
                );
            }

            $this->data[$scope][$className][$methodName] = $description;
        }
    }

    public function getNodeType(): string
    {
        return CallLike::class;
    }

    /**
     * @param CallLike $node
     * @param Scope    $scope
     * @return string[]
     */
    public function processNode(Node $node, Scope $scope): array
    {
        if ($node instanceof MethodCall) {
            $methodName = $this->getMethodName($node->name, $scope);

            if ($methodName === null) {
                return [];
            }

            return $this->validateCallOverExpr($methodName, $node->var, $scope);
        }

        if ($node instanceof StaticCall) {
            $methodName = $this->getMethodName($node->name, $scope);

            if ($methodName === null) {
                return [];
            }

            $classNode = $node->class;

            if ($classNode instanceof Name) {
                return $this->validateMethod($methodName, $scope->resolveName($classNode), $scope);
            }

            return $this->validateCallOverExpr($methodName, $classNode, $scope);
        }

        if ($node instanceof FuncCall) {
            $methodName = $this->getFunctionName($node->name, $scope);

            if ($methodName === null) {
                return [];
            }

            return $this->validateFunction($methodName, $scope);
        }

        if ($node instanceof New_) {
            $classNode = $node->class;

            if ($classNode instanceof Name) {
                return $this->validateMethod('__construct', $scope->resolveName($classNode), $scope);
            }

            if ($classNode instanceof Expr) {
                return $this->validateConstructorWithDynamicString($classNode, $scope);
            }

            return [];
        }

        return [];
    }

    /**
     * @param Expr  $name
     * @param Scope $scope
     *
     * @return string|null
     */
    private function getFunctionName(Node $name, Scope $scope): ?string
    {
        if ($name instanceof Name) {
            return $this->reflectionProvider->resolveFunctionName($name, $scope);
        }

        $nameType = $scope->getType($name);

        if ($nameType instanceof ConstantStringType) {
            return $nameType->getValue();
        }

        return null;
    }

    private function validateCallOverExpr(string $methodName, Expr $expr, Scope $scope): array
    {
        $classType = $scope->getType($expr);

        if ($classType instanceof GenericClassStringType) {
            $classType = $classType->getGenericType();
        }

        $classNames = TypeUtils::getDirectClassNames($classType);
        $errors = [];

        foreach ($classNames as $className) {
            $errors = [
                ...$errors,
                ...$this->validateMethod($methodName, $className, $scope),
            ];
        }

        return $errors;
    }

    private function validateConstructorWithDynamicString(Expr $expr, Scope $scope): array
    {
        $type = $scope->getType($expr);

        if ($type instanceof ConstantStringType) {
            return $this->validateMethod('__construct', $type->getValue(), $scope);
        }

        return [];
    }

    private function validateFunction(string $functionName, Scope $scope): array
    {
        $scopeClasses = $this->getParentClassesNames($scope);

        if (isset($this->data[self::ANY][self::FUNCTION][$functionName])) {
            return [sprintf('Function %s() is forbidden. %s', $functionName, $this->data[self::ANY][self::FUNCTION][$functionName])];
        }

        foreach ($scopeClasses as $scopeClass) {
            if (isset($this->data[$scopeClass][self::FUNCTION][$functionName])) {
                return [sprintf('Function %s() is forbidden. %s', $functionName, $this->data[$scopeClass][self::FUNCTION][$functionName])];
            }
        }

        return [];
    }

    /**
     * @return list<string>
     */
    private function validateMethod(string $methodName, string $className, Scope $scope): array
    {
        $scopeClasses = $this->getParentClassesNames($scope);

        foreach ($this->reflectionProvider->getClass($className)->getAncestors() as $ancestor) {
            $ancestorClassName = $ancestor->getName();

            if (in_array($ancestorClassName, $scopeClasses)) {
                return [];
            }

            if (isset($this->data[self::ANY][$ancestorClassName][$methodName])) {
                return [sprintf('Method %s::%s() is forbidden. %s', $ancestorClassName, $methodName, $this->data[self::ANY][$ancestorClassName][$methodName])];
            }

            if (isset($this->data[self::ANY][$ancestorClassName][self::ANY])) {
                return [sprintf('Class %s is forbidden. %s', $ancestorClassName, $this->data[self::ANY][$ancestorClassName][self::ANY])];
            }

            foreach ($scopeClasses as $scopeClass) {
                if (isset($this->data[$scopeClass][$ancestorClassName][$methodName])) {
                    return [sprintf('Method %s::%s() is forbidden. %s', $ancestorClassName, $methodName, $this->data[$scopeClass][$ancestorClassName][$methodName])];
                }

                if (isset($this->data[$scopeClass][$ancestorClassName][self::ANY])) {
                    return [sprintf('Class %s is forbidden. %s', $ancestorClassName, $this->data[$scopeClass][$ancestorClassName][self::ANY])];
                }
            }
        }

        return [];
    }

    /**
     * @param Name|Expr|Identifier $name
     */
    private function getMethodName(Node $name, Scope $scope): ?string
    {
        if ($name instanceof Name) {
            return $name->toString();
        }

        if ($name instanceof Identifier) {
            return $name->toString();
        }

        $nameType = $scope->getType($name);

        if ($nameType instanceof ConstantStringType) {
            return $nameType->getValue();
        }

        return null;
    }

    /**
     * @param Scope $scope
     *
     * @return array
     */
    private function getParentClassesNames(Scope $scope): array
    {
        $class = $scope->getClassReflection();
        $classesNames = [];

        while ($class !== null) {
            $classesNames[] = $class->getName();
            $class = $class->getParentClass();
        }

        return $classesNames;
    }
}
