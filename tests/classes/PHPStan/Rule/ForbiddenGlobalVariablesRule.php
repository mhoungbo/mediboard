<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests\PHPStan\Rule;

use PhpParser\Node;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PHPStan\Analyser\Scope;
use PHPStan\Rules\Rule;
use PHPStan\Rules\RuleErrorBuilder;
use PHPStan\ShouldNotHappenException;
use PHPStan\Type\Constant\ConstantStringType;

class ForbiddenGlobalVariablesRule implements Rule
{
    protected array $superglobals = [
        'GLOBALS',
        '_SERVER',
        'HTTP_SERVER_VARS',
        '_GET',
        'HTTP_GET_VARS',
        '_POST',
        'HTTP_POST_VARS',
        '_FILES',
        'HTTP_POST_FILES',
        '_COOKIE',
        'HTTP_COOKIE_VARS',
        '_SESSION',
        'HTTP_SESSION_VARS',
        '_REQUEST',
        '_ENV',
        'HTTP_ENV_VARS'
    ];

    /**
     * @inheritDoc
     */
    public function getNodeType(): string
    {
        return Variable::class;
    }

    /**
     * @throws ShouldNotHappenException
     */
    public function processNode(Node $node, Scope $scope): array
    {
        if (!property_exists($node, 'name') || !is_string($node->name)) {
            return [];
        }

        if (in_array($node->name, $this->superglobals)) {
            return [
                RuleErrorBuilder::message(
                    'Using global variable $' . $node->name . ' is forbidden. Use Symfony\Component\HttpFoundation\Request instead.'
                )->build(),
            ];
        }

        return [];
    }

    private function isGlobalVariable(ArrayDimFetch $node): bool
    {
        if ($node->var instanceof Variable) {
            return $node->var->name === 'GLOBALS';
        } elseif ($node->var instanceof ArrayDimFetch) {
            return $this->isGlobalVariable($node->var);
        }

        return false;
    }

    /**
     * @param Name|Expr|Identifier $name
     */
    private function getMethodName(Node $name, Scope $scope): ?string
    {
        if ($name instanceof Name) {
            return $name->toString();
        }

        if ($name instanceof Identifier) {
            return $name->toString();
        }

        $nameType = $scope->getType($name);

        if ($nameType instanceof ConstantStringType) {
            return $nameType->getValue();
        }

        return null;
    }
}
