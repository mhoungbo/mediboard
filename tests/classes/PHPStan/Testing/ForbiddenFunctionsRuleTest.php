<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests\PHPStan\Testing;

use Ox\Tests\PHPStan\Rule\ForbiddenFunctionsRule;
use PHPStan\Reflection\ReflectionProvider;
use PHPStan\Testing\RuleTestCase;
use PHPStan\Rules\Rule;

/**
 * @group schedules
 */
class ForbiddenFunctionsRuleTest extends RuleTestCase
{
    protected const FORBIDDEN_CLASS = 'Ox\Core\CView';
    protected const FORBIDDEN_CLASS_METHOD = 'get';
    protected const FORBIDDEN_METHOD = 'dd';

    protected const FORBIDDEN_CLASS_MESSAGE = 'Use an other class';
    protected const FORBIDDEN_CLASS_METHOD_MESSAGE = 'Use an other method';
    protected const FORBIDDEN_METHOD_MESSAGE = 'This method can never be used';

    protected function getRule(): Rule
    {
        return new ForbiddenFunctionsRule(
            [
                [
                    'rules' => [
                        self::FORBIDDEN_CLASS . '::*'                               => self::FORBIDDEN_CLASS_MESSAGE,
                        self::FORBIDDEN_CLASS . '::' . self::FORBIDDEN_CLASS_METHOD => self::FORBIDDEN_CLASS_METHOD_MESSAGE,
                    ],
                    'scopes' => [
                        'Ox\Core\Controller',
                    ]
                ],
                [
                    'rules' => [
                        self::FORBIDDEN_METHOD => self::FORBIDDEN_METHOD_MESSAGE,
                    ],
                ]
            ],
            self::getContainer()->getByType(ReflectionProvider::class),
        );
    }

    public function testRule(): void
    {
        $this->analyse(
            [
                __DIR__ . '/data/code.php'
            ],
            [
                [
                    'Class ' . self::FORBIDDEN_CLASS . ' is forbidden. ' . self::FORBIDDEN_CLASS_MESSAGE,
                    22
                ],
                [
                    'Method ' . self::FORBIDDEN_CLASS . '::' . self::FORBIDDEN_CLASS_METHOD . '() is forbidden. ' . self::FORBIDDEN_CLASS_METHOD_MESSAGE,
                    23
                ],
                [
                    'Function ' . self::FORBIDDEN_METHOD . '() is forbidden. ' . self::FORBIDDEN_METHOD_MESSAGE,
                    28
                ],
            ],
        );
    }
}
