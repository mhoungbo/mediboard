<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests\PHPStan\Testing;

use Ox\Tests\PHPStan\Rule\ForbiddenGlobalVariablesRule;
use PHPStan\Rules\Rule;
use PHPStan\Testing\RuleTestCase;

/**
 * @group schedules
 */
class ForbiddenGlobalVariablesRuleTest extends RuleTestCase
{
    protected function getRule(): Rule
    {
        return new ForbiddenGlobalVariablesRule();
    }

    public function testRule(): void
    {
        $this->analyse(
            [
                __DIR__ . '/data/code.php'
            ],
            [
                [
                    'Using global variable $_GET is forbidden. Use Symfony\Component\HttpFoundation\Request instead.',
                    13
                ],
                [
                    'Using global variable $_POST is forbidden. Use Symfony\Component\HttpFoundation\Request instead.',
                    14
                ],
            ],
        );
    }
}
