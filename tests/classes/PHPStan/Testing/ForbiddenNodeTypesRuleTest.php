<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests\PHPStan\Testing;

use Ox\Tests\PHPStan\Rule\ForbiddenNodeTypesRule;
use PHPStan\Testing\RuleTestCase;
use PHPStan\Rules\Rule;

/**
 * @group schedules
 */
class ForbiddenNodeTypesRuleTest extends RuleTestCase
{
    protected const FORBIDDEN_TYPE = 'Stmt_Echo';

    protected function getRule(): Rule
    {
        return new ForbiddenNodeTypesRule(
            [
                self::FORBIDDEN_TYPE
            ],
        );
    }

    public function testRule(): void
    {
        $this->analyse(
            [
                __DIR__ . '/data/code.php'
            ],
            [
                [
                    'Should not use node with type "' . self::FORBIDDEN_TYPE . '", please change the code.',
                    36
                ],
            ],
        );
    }
}
