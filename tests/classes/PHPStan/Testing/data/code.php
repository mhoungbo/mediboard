<?php

namespace Ox\Tests\PHPStan\Testing;

use Exception;
use Ox\Core\Controller;
use Ox\Core\CView;

class PHPStanRulesTestDataController extends Controller
{
    private function usingGlobalVariablesFunction(): void
    {
        $get  = $_GET;
        $post = $_POST;
    }

    /**
     * @throws Exception
     */
    private function usingForbiddenClassesMethod(): void
    {
        CView::reset();
        CView::get('test', 'str');
    }

    private function usingForbiddenGenericMethod(): int
    {
        return dd();
    }

    /**
     * @throws Exception
     */
    private function usingForbiddenNodeType(): void
    {
        echo 'this is wrong';
    }
}
