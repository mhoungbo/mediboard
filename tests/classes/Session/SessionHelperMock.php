<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests\Session;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\Sessions\Handler\SessionHandlerInterface;
use Ox\Core\Sessions\SessionHelper;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

/**
 * Mock the session helper to have an array session for unit tests.
 */
class SessionHelperMock extends SessionHelper
{
    public function __construct()
    {
        $this->session = new Session(new MockArraySessionStorage());
    }

    public function sessionExists(string $session_id): bool
    {
        return true;
    }

    public function isDistributed(): bool
    {
        return false;
    }

    public function getLifetime(): int
    {
        return 0;
    }

    public function destroy(string $id): bool
    {
        return true;
    }
}
