<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests\Session;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\Sessions\SessionHelper;
use Symfony\Component\HttpFoundation\Session\Storage\MetadataBag;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorageFactory;

/**
 * SF's MockFileSessionStorageFactory with our custom computed session's name
 */
class OxMockFileSessionStorageFactory extends MockFileSessionStorageFactory
{
    public function __construct()
    {
        parent::__construct(name: SessionHelper::getSessionName());
    }
}
