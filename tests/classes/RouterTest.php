<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Tests;

use Google\Service\Compute\RouterInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

/**
 * RouterTest is a standalone SF router
 * used for tests application who need generate url
 */
class RouterTest extends Router
{
    /** @var string */
    public const RESOURCE = 'includes/all_routes.yml';


    private static ?RouterTest $instance = null;

    public RouterInterface $router;


    public function __construct()
    {
        $root_dir = dirname(__DIR__, 2);

        $fileLocator = new FileLocator($root_dir);

        $loader = new YamlFileLoader($fileLocator);

        parent::__construct($loader, static::RESOURCE);

        self::$instance = $this;
    }


    /**
     * @return RouterTest
     */
    public static function getInstance(): RouterTest
    {
        if (is_null(self::$instance)) {
            self::$instance = new RouterTest();
        }

        return self::$instance;
    }
}
