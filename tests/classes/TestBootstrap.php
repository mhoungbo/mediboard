<?php

namespace Ox\Tests;

use Ox\Core\Autoload\CAutoloadAlias;
use Ox\Core\Cache;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbConfig;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\System\CConfiguration;
use Ox\Tests\Session\SessionHelperMock;
use ReflectionProperty;

class TestBootstrap
{
    public static function boot()
    {
        $app = CApp::getInstance();

        $config = CApp::includeConfigs(dirname(__DIR__, 2));

        $property = new ReflectionProperty($app, 'config');
        $property->setAccessible(true);
        $property->setValue($app, $config);

        $app->setRouter(RouterTest::getInstance());

        $app->setLogger(new LoggerTest());
        $app->startChrono();

        // Timezone
        date_default_timezone_set($config["timezone"]);

        // Init
        Cache::init(CApp::getAppIdentifier());
        CAppUI::init();
        CClassMap::init();
        CAutoloadAlias::register();

        CModule::loadModules();

        CApp::getInstance()->setSessionHelper(new SessionHelperMock());

        TestBootstrap::authUser();

        // Include config in DB
        CMbConfig::loadValuesFromDB();

        // Init shared memory, must be after DB init and Modules loading
        Cache::initDistributed();

        // Register configuration
        CConfiguration::registerAllConfiguration();

        // Started
        $property = new ReflectionProperty($app, 'is_started');
        $property->setAccessible(true);
        $property->setValue($app, true);
    }

    /**
     * Authenticate the PHPUnit user for tests purpose.
     *
     * @throws TestsException
     */
    public static function authUser(CUser $user = null): CUser
    {
        if ($user === null) {
            $user                = new CUser();
            $user->user_username = CUser::USER_PHPUNIT;
            $user->loadMatchingObjectEsc();
        }

        if (!$user->user_id) {
            throw new TestsException(sprintf('Missing user_username %s', $user->user_username));
        }

        $mediuser = $user->loadRefMediuser();

        // CAppUI
        CAppUI::initInstance();
        CAppUI::$instance->_ref_user = $mediuser;
        CAppUI::$instance->user_id   = $user->_id;

        if ($function = $mediuser?->loadRefFunction()) {
            // Avoid setting group config on a bad group
            // If $g is not initialized the first group (alphabetical order) will be returned and will change later
            global $g;
            $g                            = $function->group_id;
            CAppUI::$instance->user_group = $function->group_id;
        }

        // Reset modules perms (webtestcase switch user)
        foreach (CModule::$active as $mod) {
            $mod->_can = null;
        }

        CPermModule::loadUserPerms();
        CPermObject::loadUserPerms();

        // Must build preferences
        CAppUI::buildPrefs();

        return $user;
    }

}
