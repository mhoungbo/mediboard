# OX Mediboard

## About

Mediboard is an open source health facility management application.

## Requirements

- Apache
- Php
- Mysql
- Pecl
- Composer

## Installation
```
composer install --no-dev --optimize-autoloader
nvm exec npm install --no-save
nvm exec npm run build
composer ox-install-config
composer ox-install-database
```

### Front End

#### Project setup
```
nvm exec npm install
```

#### Compiles for development
```
nvm exec npm run build:dev
```

#### Compiles and minifies for production
```
nvm exec npm run build
```

#### Run your unit tests
```
nvm exec npm run test:unit
```

#### Run your unit tests with coverage
```
nvm exec npm run test:coverage
```

## Links
- Api state: `/api/status`
- Api documentation: `/api/doc`

## Documentation
The documentation is available on GitLab wiki.

## Version
0.5.0

## Licenses
- [GNU General Public License](https://www.gnu.org/licenses/gpl.html)
- [OpenXtrem Open License](https://www.openxtrem.com/licenses/oxol.html)
