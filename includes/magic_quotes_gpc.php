<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CMbArray;
use Ox\Core\CMbString;

if (PHP_SAPI === "cli") {
    return;
}

// UTF decode inputs from ajax requests
if ((isset($_REQUEST["ajax"]) && $_REQUEST["ajax"]) && empty($_REQUEST["accept_utf8"]) || isset($_REQUEST["is_utf8"])) {
    $_GET     = CMbArray::mapRecursive([CMbString::class, 'fixISOEncoding'], $_GET);
    $_POST    = CMbArray::mapRecursive([CMbString::class, 'fixISOEncoding'], $_POST);
    $_COOKIE  = CMbArray::mapRecursive([CMbString::class, 'fixISOEncoding'], $_COOKIE);
    $_REQUEST = CMbArray::mapRecursive([CMbString::class, 'fixISOEncoding'], $_REQUEST);
}

// Emulates magic quotes
$_GET     = CMbArray::mapRecursive("addslashes", $_GET);
$_POST    = CMbArray::mapRecursive("addslashes", $_POST);
$_COOKIE  = CMbArray::mapRecursive("addslashes", $_COOKIE);
$_REQUEST = CMbArray::mapRecursive("addslashes", $_REQUEST);
