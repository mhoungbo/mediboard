<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Configuration loader
 */

// Distribution configuration
require __DIR__."/config_dist.php";

// Local configuration, if it exists (does not exist when installing)
if (file_exists(__DIR__."/config.php")) {
  include __DIR__."/config.php";
}

// Overload configuration (for master/slave)
if (is_file(__DIR__."/config_overload.php")) {
  include __DIR__."/config_overload.php";
}
