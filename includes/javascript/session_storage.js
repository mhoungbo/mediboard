/**
 * @package Mediboard\Includes
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

var OxSessionStorage = Class.create({
  timeout: null,

  /**
   * @param {?int} timeout TTL of cache in seconds
   */
  initialize: function (timeout) {
    this.timeout = timeout;
  },

  setItem: function (key, value) {
    const store = {
      time: this.timeout ? (Date.now() + (this.timeout * 1000)) : null,
      value: value
    };

    sessionStorage.setItem(key, Object.toJSON(store));
  },

  getItem: function (key) {
    const store = sessionStorage.getItem(key);

    if (undefined === store || null === store) {
      return store;
    }

    const object = store.evalJSON();

    if (null !== object.time && (Date.now() > object.time)) {
      return null;
    }

    return object.value;
  }
});
