// Override the path where the plugin will look for the missing grammars
Prism.plugins.autoloader.languages_path = "lib/prismjs/components/";

// Adding new "ER7" language
Prism.languages.er7 = {
  'esc': /\\/,
  'fs': /\|/,
  'cs': /\^/,
  'rs': /~/,
  'scs': /&/
};
