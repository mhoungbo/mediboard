{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=soins script=soins ajax=true}}

<script type="text/javascript">
    Calendar.regField(getForm("changeDatePermissions").date, null, {noView: true});
</script>

{{mb_include module=admissions template=inc_refresh_page_message}}

<table class="tbl me-no-align" id="admissions">
    <tbody>
        {{foreach from=$affectations item=affectation}}
            {{assign var=_sejour value=$affectation->_ref_sejour}}
            <tr
              class="sejour-type-default sejour-type-{{$_sejour->type}} {{if !$_sejour->facturable}} non-facturable {{/if}}"
              id="permission{{$affectation->_id}}">
                {{mb_include module=admissions template=permissions_line sejour=$affectation->_ref_sejour nodebug=true}}
            </tr>
            {{foreachelse}}
            <tr>
                <td colspan="10" class="empty">{{tr}}None{{/tr}}</td>
            </tr>
        {{/foreach}}
    </tbody>
    <thead>
        <tr>
            <th class="title" colspan="10">
                <div style="margin-bottom: 5px; text-align: center;">
                    <a href="?m=dPadmissions&tab=viewSejourPermissions&date={{$hier}}" style="display: inline"><<<</a>
                    {{$date|date_format:$conf.longdate}}
                    <form name="changeDatePermissions" action="?" method="get">
                        <input type="hidden" name="m" value="{{$m}}"/>
                        <input type="hidden" name="tab" value="viewSejourPermissions"/>
                        <input type="hidden" name="date" class="date" value="{{$date}}" onchange="this.form.submit()"/>
                    </form>
                    <a href="?m=dPadmissions&tab=viewSejourPermissions&date={{$demain}}" style="display: inline">>>></a>
                </div>

                <em style="float: left; font-weight: normal;">
                    {{$total}}
                    {{if $type_externe == "depart"}}
                        {{tr}}permissions-title-Leaves{{/tr}}
                    {{else}}
                        {{tr}}permissions-title-Returns{{/tr}}
                    {{/if}}
                </em>

                <select style="float: right" name="filterFunction" style="width: 16em;"
                        onchange="SejourPermission.changeFunction(this.value);">
                    <option value=""> &mdash; {{tr}}CFunctions.all{{/tr}}</option>
                    {{foreach from=$functions item=_function}}
                        <option value="{{$_function->_id}}"
                                {{if $_function->_id == $filterFunction}}selected="selected"{{/if}} class="mediuser"
                                style="border-color: #{{$_function->color}};">{{$_function}}</option>
                    {{/foreach}}
                </select>
            </th>
        </tr>

        <tr>
            <td colspan="10">
                {{mb_include module=system template=inc_pagination total=$total current=$page
                change_page='SejourPermission.changePage' step=$step}}
            </td>
        </tr>

        {{assign var=url value="?m=$m&tab=viewSejourPermissions&type_externe=$type_externe"}}
        <tr>
            <th class="narrow">{{tr}}Validate{{/tr}}</th>
            <th>
                {{tr}}CPatient{{/tr}}
            </th>
            <th class="narrow">
                <input type="text" size="3" onkeyup="Admissions.filter(this, 'admissions')" id="filter-patient-name"/>
            </th>
            <th>{{tr}}common-Practitioner{{/tr}}</th>
            <th>{{tr}}Hour{{/tr}}</th>
            {{if $type_externe == "depart"}}
                <th>{{tr}}CChambre{{/tr}}</th>
                <th>{{tr}}CSejour-destination{{/tr}}</th>
            {{else}}
                <th>{{tr}}CAffectation-provenance{{/tr}}</th>
                <th>{{tr}}CChambre{{/tr}}</th>
            {{/if}}
            <th>{{tr}}common-Duration{{/tr}}</th>
        </tr>
    </thead>
</table>
