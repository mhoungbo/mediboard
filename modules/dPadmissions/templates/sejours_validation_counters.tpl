{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl" style="text-align: center;">
  <tr>
    <th class="title" colspan="4">
      <a style="display: inline;" href="?m={{$module_view}}&tab=viewSejourValidation&date={{$last_month}}">&lt;&lt;&lt;</a>
      {{$date|date_format:"%b %Y"}}
      <a style="display: inline;" href="?m={{$module_view}}&tab=viewSejourValidation&date={{$next_month}}">&gt;&gt;&gt;</a>
    </th>
  </tr>

  <tr>
    <th rowspan="2">{{tr}}Date{{/tr}}</th>
  </tr>

  <tr>
    <th class="text">
      <a class="{{if $recuse=='-1'}}selected{{else}}selectable{{/if}}" title="{{tr}}CSejour-title-waiting-desc{{/tr}}" href="?m={{$module_view}}&tab=viewSejourValidation&recuse=-1">
        {{tr}}CSejour-title-waiting{{/tr}}
      </a>
    </th>
    <th class="text">
      <a class="{{if $recuse=='0'}}selected{{else}}selectable{{/if}}" title="{{tr}}CSejour-title-validated-desc{{/tr}}" href="?m={{$module_view}}&tab=viewSejourValidation&recuse=0">
        {{tr}}CSejour-title-validated{{/tr}}
      </a>
    </th>
    <th class="text">
      <a class="{{if $recuse=='1'}}selected{{else}}selectable{{/if}}" title="{{tr}}CSejour-title-contested-desc{{/tr}}" href="?m={{$module_view}}&tab=viewSejourValidation&recuse=1">
        {{tr}}CSejour-title-contested{{/tr}}
      </a>
    </th>
  </tr>

  {{foreach from=$days key=day item=counts}}
  <tr {{if $day == $date}}class="selected"{{/if}}>
    {{assign var=day_number value=$day|date_format:"%w"}}
    <td style="text-align: right;
      {{if array_key_exists($day, $bank_holidays)}}
        background-color: #fc0;
      {{elseif $day_number == '0' || $day_number == '6'}}
        background-color: #ccc;
      {{/if}}">
      <a href="?m={{$module_view}}&tab=viewSejourValidation&date={{$day|iso_date}}" title="{{$day|date_format:$conf.longdate}}">
        <strong>
          {{$day|date_format:"%a"|upper|substr:0:1}}
          {{$day|date_format:"%d"}}
        </strong>
      </a>
    </td>
    <td {{if $recuse == "-1" && $day == $date}}style="font-weight: bold;"{{/if}}>
      {{if $counts.waiting}}{{$counts.waiting}}{{else}}-{{/if}}
    </td>
    <td {{if $recuse == "0" && $day == $date}}style="font-weight: bold;"{{/if}}>
      {{if $counts.validated}}{{$counts.validated}}{{else}}-{{/if}}
    </td>
    <td {{if $recuse == "1" && $day == $date}}style="font-weight: bold;"{{/if}}>
      {{if $counts.contested}}{{$counts.contested}}{{else}}-{{/if}}
    </td>
  </tr>
  {{foreachelse}}
  <tr>
    <td colspan="10" class="empty">{{tr}}admissions-msg-no_admissions_this_month{{/tr}}</td>
  </tr>
  {{/foreach}}
</table>
