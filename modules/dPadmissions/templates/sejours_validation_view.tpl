{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=hprim21 script=pat_hprim_selector}}
{{mb_script module=hprim21 script=sejour_hprim_selector}}
{{mb_script module=admissions script=admissions}}
{{mb_script module=planningOp  script=prestations}}
{{mb_script module=admissions  script=SejourValidation}}

<script>
    Main.add(function() {
        SejourValidation.initializeView('{{$date}}', '{{$module_view}}', '{{$recuse}}');
    });
</script>

<table class="main">
    <tr>
        <td>
            <a href="#legend" onclick="Admissions.showLegend();" class="button search">{{tr}}Legend{{/tr}}</a>
        </td>
        <td>
            {{if $can->edit && $module_view === 'ssr'}}
                <a class="button new" style="float: left;" href="?m={{$module_view}}&tab=vw_aed_sejour_ssr&sejour_id=0">
                    {{tr}}ssr-create_pec{{/tr}}
                </a>
            {{/if}}
            <a href="#" onclick="SejourValidation.printPlanning()" class="button print" style="float: right">{{tr}}Print{{/tr}}</a>
            <form action="?" name="selType" method="get" style="float: right">
                <select name="service_id" onchange="SejourValidation.reloadView();">
                    <option value="">&mdash; {{tr}}CService.all{{/tr}}</option>
                    {{foreach from=$services item=_service}}
                        <option value="{{$_service->_id}}"
                                {{if $_service->_id == $sejour->service_id}}selected{{/if}}>{{$_service}}</option>
                    {{/foreach}}
                </select>
                <select name="prat_id" onchange="SejourValidation.reloadView();">
                    <option value="">&mdash; {{tr}}common-Practitioner.all{{/tr}}</option>
                    {{foreach from=$prats item=_prat}}
                        <option value="{{$_prat->_id}}"
                                {{if $_prat->_id == $sejour->praticien_id}}selected{{/if}}>{{$_prat}}</option>
                    {{/foreach}}
                </select>
            </form>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            {{if $nb_sejours_attente}}
                <div class="small-warning">
                    {{tr var1=$nb_sejours_attente}}SejourValidation-patients_count{{/tr}}
                </div>
            {{else}}
                <div class="small-info">
                    {{tr}}SejourValidation-patients.none{{/tr}}
                </div>
            {{/if}}
        </td>
    </tr>
    <tr>
        <td id="allAdmissions" style="width: 250px">
        </td>
        <td id="listAdmissions" style="width: 100%">
        </td>
    </tr>
</table>
