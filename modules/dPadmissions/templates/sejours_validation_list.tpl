{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script type="text/javascript">
    Calendar.regField(getForm("changeDateAdmissions").date, null, {noView: true});
</script>

{{mb_include module=admissions template=inc_refresh_page_message}}

<table class="tbl me-no-align" id="admissions">
    <tr>
        <th class="title me-text-align-center" colspan="10">
            <a href="?m={{$module_view}}&tab=viewSejourValidation&date={{$yesterday}}" style="display: inline"><<<</a>
            {{$date|date_format:$conf.longdate}}
            <form name="changeDateAdmissions" action="?" method="get">
                <input type="hidden" name="m" value="admissions"/>
                <input type="hidden" name="module_view" value="{{$module_view}}"/>
                <input type="hidden" name="tab" value="viewSejourValidation"/>
                <input type="hidden" name="date" class="date" value="{{$date}}" onchange="this.form.submit()"/>
            </form>
            <a href="?m=admissions&tab=viewSejourValidation&module_view={{$module_view}}&date={{$tomorrow}}"
               style="display: inline">>>></a>
            <br/>

            <em style="float: left; font-weight: normal;">
                {{if is_array($sejours)}}
                    {{assign var=sejours_count value=$sejours|@count}}
                {{else}}
                    {{assign var=sejours_count value=0}}
                {{/if}}
                {{if $recuse == -1}}
                    {{tr var1=$sejours_count}}SejourValidation-waiting-count{{/tr}}
                {{elseif $recuse == 0}}
                    {{tr var1=$sejours_count}}SejourValidation-validated-count{{/tr}}
                {{else}}
                    {{tr var1=$sejours_count}}SejourValidation-contested-count{{/tr}}
                {{/if}}
            </em>

            <select style="float: right" id="filterFunction" name="filterFunction" style="width: 16em;"
                    onchange="SejourValidation.reloadListSejours(this.value);">
                <option value=""> &mdash; {{tr}}CFunctions.all{{/tr}}</option>
                {{foreach from=$functions item=_function}}
                    <option value="{{$_function->_id}}"
                            {{if $_function->_id == $filterFunction}}selected="selected"{{/if}} class="mediuser"
                            style="border-color: #{{$_function->color}};">{{$_function}}</option>
                {{/foreach}}
            </select>
        </th>
    </tr>

    {{assign var=url value="?m=$module_view&tab=viewSejourValidation&recuse=$recuse"}}
    <tr>
        <th class="narrow">{{tr}}CSejour-validation{{/tr}}</th>
        <th>{{mb_colonne class="CSejour" field="patient_id" order_col=$order_column order_way=$order_way function='SejourValidation.orderSejours'}}</th>
        <th class="narrow">
            <input type="text" size="3" onkeyup="Admissions.filter(this, 'admissions')" id="filter-patient-name"/>
        </th>
        <th>{{mb_colonne class="CSejour" field="praticien_id" order_col=$order_column order_way=$order_way function='SejourValidation.orderSejours'}}</th>
        <th>{{mb_colonne class="CSejour" field="entree_prevue" order_col=$order_column order_way=$order_way function='SejourValidation.orderSejours'}}</th>
        <th class="narrow">{{tr}}CChambre{{/tr}}</th>
        <th>{{tr}}CPatient-couverture-court{{/tr}}</th>
    </tr>

    {{foreach from=$sejours item=_sejour}}
        <tr
          class="sejour-type-default sejour-type-{{$_sejour->type}} {{if !$_sejour->facturable}} non-facturable {{/if}}"
          id="admission{{$_sejour->sejour_id}}">
            {{mb_include module=dPadmissions template=sejours_validation_line nodebug=true}}
        </tr>
        {{foreachelse}}
        <tr>
            <td colspan="10" class="empty">{{tr}}None{{/tr}}</td>
        </tr>
    {{/foreach}}
</table>
