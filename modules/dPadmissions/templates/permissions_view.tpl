{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=admissions script=admissions}}
{{mb_script module=admissions script=SejourPermission}}

{{mb_default var=page value=0}}
{{mb_default var=filterFunction value=''}}

<script>
  {{assign var=auto_refresh_frequency value="dPadmissions automatic_reload auto_refresh_frequency_permissions"|gconf}}

  Main.add(function () {
      SejourPermission.initialize('{{$date}}', '{{$auto_refresh_frequency}}');
  });

</script>

<form name="permissionFilters">
  <input type="hidden" name="filterFunction" value="{{$filterFunction}}"/>
  <input type="hidden" name="date" value="{{$date}}"/>
  <input type="hidden" name="page" value="{{$page}}"/>
</form>

<table class="main">
  <tr>
    <td>
      <a href="#legend" onclick="Admissions.showLegend()" class="button search me-tertiary me-dark">{{tr}}Legend{{/tr}}</a>
    </td>
    <td style="float: right;">
      <a href="#" onclick="Admissions.printPermissions('{{$date}}', '{{$type_externe}}');" class="button print me-tertiary">{{tr}}Print{{/tr}}</a>
    </td>
  </tr>
  <tr>
    <td style="width: 250px">
      <div id="allPermissions" class="me-align-auto"></div>
    </td>
    <td style="width: 100%">
      <div id="listPermissions" class="me-align-auto"></div>
    </td>
  </tr>
</table>
