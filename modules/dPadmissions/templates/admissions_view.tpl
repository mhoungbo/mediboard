{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=hprim21     script=pat_hprim_selector}}
{{mb_script module=hprim21     script=sejour_hprim_selector}}
{{mb_script module=admissions  script=admissions}}
{{mb_script module=patients    script=antecedent}}
{{mb_script module=compteRendu script=document}}
{{mb_script module=compteRendu script=modele_selector}}
{{mb_script module=files       script=file}}
{{mb_script module=planningOp  script=sejour}}
{{mb_script module=planningOp  script=prestations}}
{{mb_script module=planningOp  script=appel}}
{{mb_script module=patients    script=identity_validator}}

{{if 'appFineClient'|module_active && 'appFineClient Sync allow_appfine_sync'|gconf}}
    {{mb_script module=appFineClient  script=appFineClient}}
{{/if}}

{{if 'dPsante400'|module_active}}
    {{mb_script module=dPsante400  script=Idex}}
{{/if}}

{{if 'web100T'|module_active}}
    {{mb_script module=web100T script=web100T}}
{{/if}}

<script>
    function confirmation(form) {
        if (confirm('La date enregistr�e d\'admission est diff�rente de la date pr�vue, souhaitez vous confimer l\'admission du patient ?')) {
            Admissions.submitAdmission(form);
        }
    }

    function submitCote(form, sejour_id) {
        return onSubmitFormAjax(form, Admissions.reloadAdmissionLine.bind(Admissions, sejour_id));
    }

    function submitMultiple(form) {
        return onSubmitFormAjax(form, Admissions.reloadAdmissionsCounters.bind(Admissions));
    }

    function changeEtablissementId(oForm) {
        $V(oForm._modifier_entree, '0');
        Admissions.submitAdmission(oForm);
        $V(oForm._modifier_entree, '1');
    }

    function afterEditCorrespondant() {
        Admissions.reloadListAdmissions()
    }

    Main.add(function () {
        Admissions.initializeAdmissionsView('{{'dPadmissions automatic_reload auto_refresh_frequency_admissions'|gconf}}');

        {{if 'dPpatients CPatient manage_identity_vide'|gconf}}
            IdentityValidator.active = true;
        {{/if}}

        {{if 'dPsante400'|module_active && 'dPsante400 CIdSante400 admit_ipp_nda_obligatory'|gconf}}
            Admissions.ipp_nda_mandatory_for_admission = true;
        {{/if}}
    });
</script>

{{mb_include module=admissions template=inc_prompt_modele type=admissions}}

<table class="main">
    <tr>
        <td id="idx_admission_legend" colspan="2">
            <a href="#legend" id="idx_admission_legend_button" onclick="Admissions.showLegend()"
               class="button search me-tertiary me-dark">L�gende</a>
        </td>
        <td id="filter_admission">
            {{mb_include module=admissions template=inc_admission_filter reload_full_function='Admissions.reloadAdmissionsCounters' reload_lite_function='Admissions.reloadListAdmissions' view='admissions' table_id='admissions'}}
        </td>
    </tr>
    <tr>
        <td>
            <div id="allAdmissions" class="admissionScrollbar me-align-auto"></div>
        </td>
        <td class="separator expand"
            onclick="MbObject.toggleColumn(this, $(this).previous()); Admissions.updateAdmissionIdxLayout();"
            style="padding: 0;"></td>
        <td style="width: 100%">
            <div id="listAdmissions" class="me-align-auto"></div>
        </td>
    </tr>
</table>
