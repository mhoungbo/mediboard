{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=admissions  script=admissions}}
{{mb_script module=admissions  script=Sorties}}
{{mb_script module=compteRendu script=document}}
{{mb_script module=compteRendu script=modele_selector}}
{{mb_script module=files       script=file}}
{{mb_script module=planningOp  script=sejour}}
{{mb_script module=planningOp  script=prestations}}
{{mb_script module=planningOp  script=appel}}

{{if "web100T"|module_active}}
    {{mb_script module=web100T script=web100T}}
{{/if}}

{{if "appFineClient"|module_active && "appFineClient Sync allow_appfine_sync"|gconf}}
    {{mb_script module=appFineClient  script=appFineClient}}
{{/if}}

<script>
    function changeEtablissementId(form) {
        $V(form._modifier_sortie, '0');
        var type = $V(form.type);
        Sorties.submitSortie(form, type);
    };

    Main.add(function () {
        Admissions.table_id = "listSorties";

        Sorties.initializeSortiesView('{{'dPadmissions automatic_reload auto_refresh_frequency_sorties'|gconf}}')
    });
</script>

{{mb_include module=admissions template=inc_prompt_modele type=sortie}}

<form name="edit_sejour_sortie_preparee" method="post" onsubmit="return onSubmitFormAjax(this, Sorties.reloadSortiesCounter.bind(Sorties))">
    <input type="hidden" name="m" value="planningOp"/>
    <input type="hidden" name="dosql" value="do_sejour_aed"/>
    <input type="hidden" name="sejour_id" value=""/>
    <input type="hidden" name="sortie_preparee" value=""/>
    <input type="hidden" name="_sortie_preparee_trigger" value=""/>
</form>

<table class="main">
    <tr>
        <td id="idx_admission_legend" colspan="2">
            <a id="idx_admission_legend_button" href="#legend" onclick="Admissions.showLegend()"
               class="button search me-tertiary me-dark">{{tr}}Legend{{/tr}}</a>
        </td>
        <td id="filter_sortie">
            {{mb_include module=admissions template=inc_admission_filter reload_full_function='Sorties.reloadSortiesCounter' reload_lite_function='Sorties.reloadListSorties' view='sorties' table_id='sortie'}}
        </td>
    </tr>
    <tr>
        <td style="width: 250px">
            <div id="allSorties" class="me-align-auto"></div>
        </td>
        <td class="separator expand"
            onclick="MbObject.toggleColumn(this, $(this).previous()); Admissions.updateAdmissionIdxLayout();"
            style="padding: 0;"></td>
        <td style="width: 100%">
            <div id="listSorties" class="me-align-auto"></div>
        </td>
    </tr>
</table>
