{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=patient value=$sejour->_ref_patient}}

<td>
    {{if $type_externe == "depart"}}
        {{if $affectation->_ref_prev->_id}}
            {{assign var=_affectation value=$affectation->_ref_prev}}
        {{else}}
            {{assign var=_affectation value=$affectation}}
        {{/if}}
        <button type="button" class="tick me-primary"
                onclick="Soins.askDepartEtablissement('{{$affectation->_id}}');">
            {{tr}}CAffectation-action-Validate the departure{{/tr}}
        </button>
    {{else}}
        <button type="button" class="tick me-primary" onclick="Soins.askRetourEtablissement(
          '{{$affectation->_ref_prev->_id}}',
          '{{$affectation->_id}}',
          {{if $affectation->_in_permission_sup_48h}}1{{else}}0{{/if}},
          '0'
          );">{{tr}}CAffectation-action-Validate the return{{/tr}}</button>
    {{/if}}
</td>

<td colspan="2" class="text">
    <span class="CPatient-view" onmouseover="ObjectTooltip.createEx(this, '{{$patient->_guid}}');">
        {{$patient}}
    </span>
</td>

<td class="text">
    {{mb_include module=mediusers template=inc_vw_mediuser mediuser=$sejour->_ref_praticien}}
</td>

<td>
    <div style="float: right;"></div>
    <span onmouseover="ObjectTooltip.createEx(this, '{{$sejour->_guid}}');">
    {{$affectation->entree|date_format:$conf.time}}
  </span>
</td>

{{if $type_externe == "depart"}}
    <td class="text">
        {{if $affectation->_ref_prev && $affectation->_ref_prev->_id}}
            {{$affectation->_ref_prev->_ref_lit->_view}}
        {{/if}}
    </td>
    <td class="text">
        {{$affectation->_ref_lit->_view}}
    </td>
{{else}}
    <td class="text">
        {{$affectation->_ref_lit->_view}}
    </td>
    <td class="text">
        {{if $affectation->_ref_next && $affectation->_ref_next->_id}}
            {{$affectation->_ref_next->_ref_lit->_view}}
        {{/if}}
    </td>
{{/if}}

<td class="text">
    {{tr var1=$affectation->_duree}}common-%d day(s){{/tr}}
</td>
