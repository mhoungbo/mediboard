{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=admissions script=MassSorties}}

<script>
    Main.add(function () {
        MassSorties.initialize(getForm('formDateMasse'));
        MassSorties.listMassSorties();
    });
</script>

<div>
    <form name="formDateMasse" method="get" action="?">
        <table class="form">
            <tr>
                <th class="title" colspan="2">{{tr}}mod-dPadmissions-tab-viewMassSorties{{/tr}}</th>
            </tr>
            <tr>
                {{me_form_field nb_cells=2 mb_class=CSejour mb_field=entree_reelle}}
                {{mb_field object=$filter field="_date_entree" form="formDateMasse" register=true canNull="false"}}
                {{/me_form_field}}
            </tr>
            <tr>
                <td colspan="2" class="button">
                    <button type="button" onclick="MassSorties.listMassSorties();"
                            class="search me-primary">{{tr}}Filter{{/tr}}</button>
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="see_sejour_masse"></div>
