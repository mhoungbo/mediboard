{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=patient value=$consultation->_ref_patient}}
{{assign var=dossiers_anesth value=$consultation->_refs_dossiers_anesth}}

{{assign var=sejour value=""}}
{{assign var=first_dossier_anesth value=$dossiers_anesth|smarty:nodefaults|@reset}}
{{if $first_dossier_anesth && $first_dossier_anesth->_ref_sejour->_id}}
    {{assign var=sejour value=$first_dossier_anesth->_ref_sejour}}
{{elseif $preadmission.CSejour->_id}}
    {{assign var=sejour value=$preadmission.CSejour}}
{{/if}}

<tr id="consultation{{$consultation->_id}}" data-id="{{$consultation->_id}}"
    class="sejour sejour-type-default {{if $sejour}}sejour-type-{{$sejour->type}} {{if !$sejour->facturable}} non-facturable {{/if}}{{/if}}">

    {{assign var=type_event value='CConsultation'}}
    {{if $preadmission.COperation->_id}}
        {{assign var=type_event value='COperation'}}
    {{else}}
        {{assign var=type_event value='CSejour'}}
    {{/if}}

    {{if "appFineClient"|module_active && "appFineClient Sync allow_appfine_sync"|gconf}}
        <td class="button" rowspan="{{$dossiers_anesth|@count}}">
            {{if $sejour && $sejour->_id}}
                <input type="checkbox" name="order_checkbox" style="float: left;"
                       title="{{tr}}CAppFineClient-msg-select sejour to send form{{/tr}}"/>
            {{/if}}

            {{mb_include module=appFineClient template=inc_create_account_appFine loadJS=0 idex=$consultation->_ref_patient->_ref_appFine_idex patient=$consultation->_ref_patient}}

            {{if $sejour && $sejour->_id}}
                {{mb_include module=appFineClient template=inc_buttons_action_preadmission loadJS=0 _sejour_appFine=$sejour _patient_appFine=$patient}}
            {{/if}}
        </td>
    {{/if}}

    <td class="text patient_td" rowspan="{{$dossiers_anesth|@count}}" colspan="2">
        <span onmouseover="ObjectTooltip.createEx(this, '{{$patient->_guid}}');">
          {{$patient}}
        </span>

        {{mb_include module=patients template=inc_status_icon}}
        {{mb_include module=patients template=inc_icon_bmr_bhre}}
    </td>

    <td class="text" rowspan="{{$dossiers_anesth|@count}}">
        {{if $consultation->_id}}
            <div class="{{if $consultation->chrono == 64}}small-success{{else}}small-info{{/if}}" style="margin: 0;">
                <span
                  onmouseover="ObjectTooltip.createEx(this, '{{$consultation->_guid}}')">{{$consultation->heure|date_format:$conf.time}}</span>
                <br/>
                {{mb_include module=mediusers template=inc_vw_mediuser mediuser=$consultation->_ref_plageconsult->_ref_chir}}
            </div>
        {{else}}
            <div class="small-warning" style="margin: 0;">
                {{tr}}admissions-msg-Pre-anesthetic consultation not created{{/tr}}
            </div>
        {{/if}}
    </td>

    {{foreach from=$dossiers_anesth item=_dossier name=dossiers_anesth}}
        {{if !$smarty.foreach.dossiers_anesth.first}}
            <tr class="more" data-consult_id="{{$consultation->_id}}">
        {{/if}}

        {{if $_dossier->_etat_dhe_anesth != "non_associe"}}
            {{assign var=_sejour value=""}}
            {{if $_dossier->_ref_sejour->_id}}
                {{assign var=_sejour value=$_dossier->_ref_sejour}}
            {{elseif $preadmission.CSejour->_id}}
                {{assign var=_sejour value=$preadmission.CSejour}}
            {{/if}}

            {{assign var=cell_style value="background: #ccc;"}}

            {{if     $sejour->type == 'ambu'}}
                {{assign var=cell_style value="background: #faa;"}}
            {{elseif $sejour->type == 'comp'}}
                {{assign var=cell_style value="background: #fff;"}}
            {{elseif $sejour->type == 'exte'}}
                {{assign var=cell_style value="background: #afa;"}}
            {{elseif in_array($sejour->type, 'Ox\Mediboard\PlanningOp\CSejour::getTypesSejoursUrgence'|static_call:$sejour->praticien_id)}}
                {{assign var=cell_style value="background: #ff6;"}}
            {{/if}}

            {{if !$sejour->facturable}}
                {{assign var=cell_style value="$cell_style background-image:url(images/icons/ray_vertical.gif); background-repeat:repeat;"}}
            {{/if}}
            <td class="text" style="{{$cell_style}}">
                {{foreach from=$sejour->_ref_operations item=_op name=op_sejour}}
                    {{if $smarty.foreach.op_sejour.first}}
                        <button class="print notext" style="float: right;"
                                title="{{tr}}admissions-action-Print the DHE from the intervention{{/tr}}"
                                onclick="Admissions.printDHE('operation_id', {{$_op->_id}}); return false;">
                        </button>
                        <a href="#showDocs" title="{{tr}}admissions-action-Display document and file|pl{{/tr}}"
                           class="button" onclick="Admissions.showDocs('{{$sejour->_id}}');" style="float: right;">
                            <i class="far fa-file" style="font-size: 1.4em; padding-left: 2px;" aria-hidden="true"></i>
                        </a>
                    {{/if}}
                {{foreachelse}}
                    <button class="print notext" style="float: right;"
                            title="{{tr}}admissions-action-Print the DHE of the stay{{/tr}}"
                            onclick="Admissions.printDHE('sejour_id', {{$sejour->_id}}); return false;">
                    </button>
                {{/foreach}}
                {{mb_include module=mediusers template=inc_vw_mediuser mediuser=$sejour->_ref_praticien}}
            </td>
            <td class="text" style="{{$cell_style}}">
                <div>
                    {{if "dmp"|module_active && $sejour}}
                        <span style="float: right;">
                            {{mb_include module=dmp template=inc_button_dmp patient=$patient compact=true}}
                        </span>
                    {{/if}}
                    {{mb_include module=system template=inc_object_notes object=$sejour float=right}}
                    {{mb_include module=planningOp template=inc_vw_numdos nda_obj=$sejour _show_numdoss_modal=1}}
                </div>
                <span onmouseover="ObjectTooltip.createEx(this, '{{$sejour->_guid}}');">
                    {{if $sejour->presence_confidentielle}}
                        {{mb_include module=planningOp template=inc_badge_sejour_conf}}
                    {{/if}}
                    {{$sejour->entree|date_format:$conf.date}}
                </span>
            </td>
            {{if !$sejour->annule && $_dossier && $_dossier->_ref_sejour->_id}}
                <td class="text" style="{{$cell_style}}">
                    {{mb_include template=inc_form_prestations sejour=$sejour edit=$canAdmissions->edit}}
                    {{mb_include module=hospi template=inc_placement_sejour sejour=$sejour}}
                </td>
                <td style="{{$cell_style}}">
                    {{if $canAdmissions->edit}}
                        <form name="editSaisFrm{{$sejour->_id}}" action="?" method="post">
                            <input type="hidden" name="m" value="dPplanningOp"/>
                            <input type="hidden" name="dosql" value="do_sejour_aed"/>
                            <input type="hidden" name="sejour_id" value="{{$sejour->_id}}"/>
                            <input type="hidden" name="patient_id" value="{{$sejour->patient_id}}"/>
                            {{mb_field object=$sejour field=type hidden=true}}

                            {{mb_include module=forms template=inc_widget_ex_class_register_multiple object=$sejour cssStyle="display: inline-block;"}}

                            {{if !$sejour->entree_preparee}}
                                <input type="hidden" name="entree_preparee" value="1"/>
                                <input type="hidden" name="_entree_preparee_trigger" value="1"/>
                                <button class="tick" type="button" onclick="Admissions.submitPreAdmission(this.form);">
                                    {{tr}}CSejour-entree_preparee{{/tr}}
                                </button>
                            {{else}}
                                <input type="hidden" name="entree_preparee" value="0"/>
                                <button class="cancel" type="button" onclick="Admissions.submitPreAdmission(this.form);">
                                    {{tr}}Cancel{{/tr}}
                                </button>
                            {{/if}}
                            {{if ($sejour->entree_modifiee == 1) && ($conf.dPplanningOp.CSejour.entree_modifiee == 1)}}
                                <img src="images/icons/warning.png" title="Le dossier a �t� modifi�, il faut le pr�parer"/>
                            {{/if}}
                        </form>
                    {{else}}
                        {{mb_value object=$sejour field="entree_preparee"}}
                    {{/if}}
                </td>
                <td style="{{$cell_style}}">
                    {{if $sejour->_couvert_c2s}}
                        {{me_img_title src="tick.png" icon="tick" class="me-success"}}
                            {{tr}}CPatient-message-c2s{{/tr}}
                        {{/me_img_title}}
                    {{else}}
                        -
                    {{/if}}
                </td>
                {{if $app->user_prefs.show_dh_admissions}}
                    {{mb_include module=admissions template=inc_operations_depassement operations=$sejour->_ref_operations sejour=$sejour}}
                {{/if}}
            {{elseif $sejour->annule}}
                <td colspan="4" class="cancelled">
                    {{tr}}Cancelled{{/tr}}
                </td>
            {{else}}
                <td colspan="4" class="button" style="{{$cell_style}}">
                    {{if $type_event == "COperation"}}
                        {{tr}}admissions-msg-Operation not associated with consultation{{/tr}}
                        {{if $canAdmissions->edit}}
                            <br/>
                            <form name="addOpFrm-{{$consultation->_id}}" action="?m={{$m}}" method="post">
                                <input type="hidden" name="dosql" value="do_consult_anesth_aed"/>
                                <input type="hidden" name="del" value="0"/>
                                <input type="hidden" name="m" value="dPcabinet"/>
                                {{mb_key object=$_dossier}}
                                <input type="hidden" name="operation_id"
                                       value="{{$preadmission.COperation->_id}}"/>
                                <input type="hidden" name="postRedirect" value="m={{$m}}"/>
                                <button type="submit" class="tick">
                                    {{tr}}admissions-action-link_operation_to_consultation{{/tr}}
                                </button>
                            </form>
                        {{/if}}
                    {{else}}
                        {{tr}}admissions-msg-Stay not associated with the consultation{{/tr}}
                        {{if $canAdmissions->edit}}
                            <br/>
                            <form name="addOpFrm-{{$consultation->_id}}" action="?m={{$m}}" method="post">
                                <input type="hidden" name="dosql" value="do_consult_anesth_aed"/>
                                <input type="hidden" name="del" value="0"/>
                                <input type="hidden" name="m" value="dPcabinet"/>
                                {{mb_key object=$_dossier}}
                                <input type="hidden" name="sejour_id" value="{{$sejour->_id}}"/>
                                <input type="hidden" name="postRedirect" value="m={{$m}}"/>
                                <button type="submit" class="tick">
                                    {{tr}}admissions-action-link_sejour_to_consultation{{/tr}}
                                </button>
                            </form>
                        {{/if}}
                    {{/if}}
                </td>
            {{/if}}
        {{else}}
            <td colspan="6" class="button">
                <span class="texticon texticon-stup texticon-stroke" style="white-space: nowrap;"
                      title="{{tr}}CConsultation-_etat_dhe_anesth-non_associe{{/tr}}">
                  {{tr}}COperation-event-dhe{{/tr}}
                </span>
                {{if $canPlanningOp->edit}}
                    :
                    <button onclick="openDHEModal('{{$consultation->patient_id}}');" class="button new">
                        {{tr}}admissions-action-create_dhe{{/tr}}
                    </button>
                {{/if}}
            </td>
        {{/if}}
    {{/foreach}}
</tr>
