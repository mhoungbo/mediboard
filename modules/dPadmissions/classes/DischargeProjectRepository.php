<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions;

use DateTimeImmutable;
use Exception;
use Ox\Core\CMbArray;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * A repository class for loading sejours for the discharge project views (Projet de sortie)
 */
class DischargeProjectRepository
{
    protected DateTimeImmutable $date_min;
    protected DateTimeImmutable $date_max;

    protected CGroups $group;
    /** @var CSejour Used for loading the objects, mostly useful for testing purpose */
    protected CSejour $sejour_object;

    protected ?int    $user_id_filter               = null;
    protected array   $handicap_filter              = [];
    protected array   $help_filter                  = [];
    protected bool    $custom_discharge_mode_config = false;
    protected array   $discharge_modes_filter       = [];
    protected ?string $patient_tutelle_filter       = null;

    protected array $sejour_types_include_filter = [];
    protected array $sejour_types_exclude_filter = [];

    protected CRequest $request;

    /**
     * @param CGroups           $group
     * @param DateTimeImmutable $date_min
     * @param DateTimeImmutable $date_max
     * @param ?CSejour          $sejour_object
     */
    public function __construct(
        CGroups $group,
        DateTimeImmutable $date_min,
        DateTimeImmutable $date_max,
        ?CSejour $sejour_object = null
    ) {
        $this->group = $group;

        if ($date_min > $date_max) {
            $this->date_min = $date_max;
            $this->date_max = $date_min;
        } else {
            $this->date_min = $date_min;
            $this->date_max = $date_max;
        }

        $this->sejour_object = $sejour_object ?? new CSejour();

        $this->request = new CRequest();
    }

    /**
     * Adds a filter on the user responsible for the sejours
     *
     * @param int $user_id
     *
     * @return void
     */
    public function setUserIdFilter(int $user_id): void
    {
        $this->user_id_filter = $user_id;
    }

    /**
     * Adds a filter on the handicap of the patients
     *
     * @param array $handicaps
     *
     * @return void
     */
    public function setHandicapFilter(array $handicaps): void
    {
        $this->handicap_filter = $handicaps;
    }

    /**
     * Adds a filter on the type of help or support that the patient will need at home
     *
     * @param array $helps
     *
     * @return void
     */
    public function setHelpFilter(array $helps): void
    {
        $this->help_filter = $helps;
    }

    /**
     * Adds a filter on the discharge mode (mode de sortie).
     * If $custom_discharge_modes is set to true, the $discharge_modes array must contain
     * the ids of the customs discharge modes
     *
     * @param array $discharge_mode
     * @param bool  $custom_discharge_modes
     *
     * @return void
     */
    public function setDischargeModesFilter(array $discharge_mode, bool $custom_discharge_modes = false): void
    {
        $this->discharge_modes_filter       = $discharge_mode;
        $this->custom_discharge_mode_config = $custom_discharge_modes;
    }

    /**
     * Adds a filter on the patient's field 'tutelle'
     *
     * @param string $tutelle
     *
     * @return void
     */
    public function setPatientTutelleFilter(string $tutelle): void
    {
        $this->patient_tutelle_filter = $tutelle;
    }

    /**
     * Adds a filter on the sejour types. If $include is true, the resulting SQL clause will use an IN condition,
     * or a NOT IN condition otherwise.
     *
     * @param array $types
     * @param bool  $include
     *
     * @return void
     */
    public function setSejourTypesFilter(array $types, bool $include = true): void
    {
        if ($include) {
            $this->sejour_types_include_filter = $types;
        } else {
            $this->sejour_types_exclude_filter = $types;
        }
    }

    /**
     * Sets the SQL clauses of the request
     *
     * @return CRequest
     */
    public function prepareLoadSejourRequest(): CRequest
    {
        $this->request->addLJoinClause('operations', '`operations`.`sejour_id` = `sejour`.`sejour_id`');
        $this->request->addLJoinClause('patients', '`patients`.`patient_id` = `sejour`.`patient_id`');

        if ($this->user_id_filter) {
            $this->request->addWhere("`sejour`.`praticien_id` = {$this->user_id_filter}");
        }

        if (count($this->sejour_types_include_filter)) {
            $this->request->addWhere(
                '`sejour`.`type` ' . CSQLDataSource::prepareIn($this->sejour_types_include_filter)
            );
        } elseif (count($this->sejour_types_exclude_filter)) {
            $this->request->addWhere(
                '`sejour`.`type` ' . CSQLDataSource::prepareNotIn($this->sejour_types_exclude_filter)
            );
        }

        if (count($this->handicap_filter)) {
            $this->request->addLJoinClause(
                'patient_handicap',
                '`patient_handicap`.`patient_id` = `sejour`.`patient_id`'
            );
            $this->request->addWhere(
                '`patient_handicap`.`handicap` ' . CSQLDataSource::prepareIn($this->handicap_filter)
            );
        }

        if (count($this->help_filter)) {
            if (in_array('', $this->help_filter) && count($this->help_filter) === 1) {
                $this->request->addWhere('`sejour`.`aide_organisee` IS NULL');
            } elseif (in_array('', $this->help_filter)) {
                CMbArray::removeValue('', $this->help_filter);
                $this->request->addWhere(
                    '`sejour`.`aide_organisee` ' . CSQLDataSource::prepareIn($this->help_filter)
                    . ' OR `sejour`.`aide_organisee` IS NULL'
                );
            } else {
                $this->request->addWhere('`sejour`.`aide_organisee` ' . CSQLDataSource::prepareIn($this->help_filter));
            }
        }

        if ($this->patient_tutelle_filter) {
            $this->request->addWhere("`patients`.`tutelle` = '{$this->patient_tutelle_filter}'");
        }

        if ($this->custom_discharge_mode_config && count($this->discharge_modes_filter)) {
            $this->request->addWhere(
                '`sejour`.`mode_sortie_id` ' . CSQLDataSource::prepareIn($this->discharge_modes_filter)
            );
        } elseif (count($this->discharge_modes_filter)) {
            $this->request->addWhere(
                '`sejour`.`mode_sortie` ' . CSQLDataSource::prepareIn($this->discharge_modes_filter)
            );
        }

        $this->request->addWhere("`sejour`.`group_id` = {$this->group->_id}");
        $this->request->addWhere(
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '"
            . $this->date_min->format('Y-m-d') . "' AND '" . $this->date_max->format('Y-m-d') . "')"
            . " OR (`operations`.`operation_id` IS NULL AND `sejour`.`sortie` >= '"
            . $this->date_min->format('Y-m-d 00:00:00') . "' AND `sejour`.`sortie` <= '"
            . $this->date_max->format('Y-m-d 23:59:59') . "')"
        );

        $this->request->addOrder('`sejour`.`sortie_prevue` ASC, `patients`.`nom`, `patients`.`prenom`');
        $this->request->addGroup('`sejour`.`sejour_id`');

        return $this->request;
    }

    /**
     * Load the sejours with a discharge (sortie) between the given dates, that matches the filters
     *
     * @return CSejour[]
     */
    public function loadSejours(): array
    {
        $this->prepareLoadSejourRequest();

        try {
            $sejours = $this->sejour_object->loadListByReq($this->request);
        } catch (Exception $e) {
            $sejours = [];
        }

        if (is_null($sejours)) {
            $sejours = [];
        }

        return $sejours;
    }
}
