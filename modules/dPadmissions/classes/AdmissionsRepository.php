<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions;

use DateTimeImmutable;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbObject;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\Module\CModule;
use Ox\Interop\Dmp\CDMP;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * A repository class for loading sejours for the admissions views
 */
class AdmissionsRepository
{
    public const SEJOUR_VIEW_ADMISSIONS = 'entree';
    public const SEJOUR_VIEW_REAL_ADMISSIONS = 'entree_reelle';
    public const SEJOUR_VIEW_SORTIES = 'sortie';
    public const SEJOUR_VIEW_SORTIES_SCHEDULED = 'sortie_prevue';

    public const FILTER_PAYMENT_EXCEEDING_FEES_PAYED = 'payed';
    public const FILTER_PAYMENT_EXCEEDING_FEES_UNPAID = 'unpaid';
    public const FILTER_PAYMENT_EXCEEDING_FEES_ALL = 'all';

    public const FILTER_PERIOD_MORNING   = 'matin';
    public const FILTER_PERIOD_AFTERNOON = 'soir';

    public const FILTER_STATUS_PEC_WAITING = 'attente';
    public const FILTER_STATUS_PEC_ONGOING = 'en_cours';
    public const FILTER_STATUS_PEC_OVER = 'termine';

    /** @var string The table on which the request will be made */
    protected string $table = 'sejour';

    /** @var CGroups */
    protected CGroups $group;

    /** @var CMediusers */
    protected CMediusers $user;

    protected ?CRequest $request = null;

    /** @var string Used to determine the date fields on which the sejours will be filtered */
    protected string $sejour_view_field;

    /** @var string|null A filter that checks if the sejours are of the given type */
    protected ?string $sejour_type_filter = null;
    /** @var array A filter that checks if the type of the sejours are in the given list of types */
    protected array $sejour_type_list_filter = [];
    /** @var array A filter that checks if the type of the sejours are not in the given list of types */
    protected array $sejour_type_exclude_list_filter = [];

    protected ?CService $service_filter = null;
    protected array $services_list_filter = [];
    protected ?CMediusers $user_filter = null;
    protected ?CFunctions $function_filter = null;

    protected ?int $recuse_filter = null;
    protected ?int $cancelled_filter = null;
    protected ?bool $confirmed_filter = null;
    protected array $type_pec_filter = [];
    protected array $circuit_ambu_filter = [];
    protected bool $date_operation_equals_entry_filter = false;
    protected string $payment_exceeding_fees_filter = self::FILTER_PAYMENT_EXCEEDING_FEES_ALL;
    protected array $prestations_filter = [];
    protected ?string $period_filter = null;
    protected ?string $time_change_period = null;
    protected array   $leave_modes_filter = [];
    protected ?string $status_pec_filter  = null;
    protected ?string $notification_status_filter = null;

    protected bool $unprepared_sejours_filter = false;
    protected bool $scheduled_sejours_filter  = false;
    protected bool $without_real_leave_date_filter = false;

    /**
     * @param CGroups    $group
     * @param CMediusers $user
     * @param string     $sejour_view Allow to choose on which datetime field the request will be made
     */
    public function __construct(CGroups $group, CMediusers $user, string $sejour_view = self::SEJOUR_VIEW_ADMISSIONS)
    {
        $this->group = $group;
        $this->user  = $user;

        $this->setSejourViewField($sejour_view);
    }

    /**
     * @param string $sejour_view
     *
     * @return void
     */
    public function setSejourViewField(string $sejour_view): void
    {
        switch ($sejour_view) {
            case self::SEJOUR_VIEW_REAL_ADMISSIONS:
            case self::SEJOUR_VIEW_SORTIES_SCHEDULED:
            case self::SEJOUR_VIEW_SORTIES:
                $this->sejour_view_field = $sejour_view;
                break;
            case self::SEJOUR_VIEW_ADMISSIONS:
            default:
                $this->sejour_view_field = 'entree';
        }
    }

    /**
     * Set a filter on the services (on the sejour or the affectation table)
     *
     * @param array $services_ids an array of services ids
     *
     * @return void
     */
    public function setServicesListFilter(array $services_ids): void
    {
        $this->services_list_filter = $services_ids;
    }

    /**
     * Sets a filter for the column praticien_id
     *
     * @param CMediusers $user
     *
     * @return void
     */
    public function setUserFilter(CMediusers $user): void
    {
        $this->user_filter = $user;
    }

    /**
     * Sets a filter for the primary function of the user responsible for the sejours
     *
     * @param CFunctions $function
     *
     * @return void
     */
    public function setFunctionFilter(CFunctions $function): void
    {
        $this->function_filter = $function;
    }

    /**
     * Sets a filter for the type of sejours
     *
     * @param string $type
     *
     * @return void
     */
    public function setSejourTypeFilter(string $type): void
    {
        if (in_array($type, CSejour::$types)) {
            $this->sejour_type_filter = $type;
        }
    }

    /**
     * Sets a lists of sejour types as a filter
     *
     * @param array $types
     *
     * @return void
     */
    public function setSejourTypeListFilter(array $types): void
    {
        $this->sejour_type_list_filter = $types;
    }

    /**
     * Sets an exclusion list as a filter for the type of sejour
     *
     * @param array $types
     *
     * @return void
     */
    public function setSejourTypeExcludeListFilter(array $types): void
    {
        $this->sejour_type_exclude_list_filter = $types;
    }

    /**
     * Sets a filter on the service for the sejour (either directly on the sejour, or on it's first affectation)
     *
     * @param CService $service
     *
     * @return void
     */
    public function setServiceFilter(CService $service): void
    {
        $this->service_filter = $service;
    }

    /**
     * Sets a filter on the field "recuse" of the sejour
     *
     * @param int $recuse
     *
     * @return void
     */
    public function setRecuseFilter(int $recuse): void
    {
        if (in_array($recuse, [-1, 0, 1])) {
            $this->recuse_filter = $recuse;
        }
    }

    /**
     * Sets a filter for the field "cancelled" of the sejours
     *
     * @param int $cancelled
     *
     * @return void
     */
    public function setCancelledFilter(int $cancelled): void
    {
        if (in_array($cancelled, [0, 1])) {
            $this->cancelled_filter = $cancelled;
        }
    }

    /**
     * Sets a filter for the field "confirme" of the sejours
     *
     * @param bool $confirmed
     *
     * @return void
     */
    public function setConfirmedFilter(bool $confirmed): void
    {
        $this->confirmed_filter = $confirmed;
    }

    /**
     * Sets a filter for the field "type_pec" of the sejours
     *
     * @param array $types
     *
     * @return void
     */
    public function setTypePecFilter(array $types): void
    {
        $this->type_pec_filter = $types;
    }

    /**
     * Sets a list of "circuit_ambu" as a filter for the sejours
     *
     * @param array $circuits
     *
     * @return void
     */
    public function setCircuitAmbuFilter(array $circuits): void
    {
        $this->circuit_ambu_filter = $circuits;
    }

    /**
     * Sets a filter that will return only the sejour where the entry date is equal to the operation date
     *
     * @param bool $filter
     *
     * @return void
     */
    public function setDateOperationEqualsToEntryFilter(bool $filter = true): void
    {
        $this->date_operation_equals_entry_filter = $filter;
    }

    /**
     * Sets a filter that checks whereas the exeeding fees on the operation are paid or not
     *
     * @param string $filter
     *
     * @return void
     */
    public function setPaymentExceedingFeesFilter(string $filter = 'all'): void
    {
        if (
            in_array($filter, [
                self::FILTER_PAYMENT_EXCEEDING_FEES_ALL,
                self::FILTER_PAYMENT_EXCEEDING_FEES_PAYED,
                self::FILTER_PAYMENT_EXCEEDING_FEES_UNPAID
            ])
        ) {
            $this->payment_exceeding_fees_filter = $filter;
        }
    }

    /**
     * Sets a list of CPrestations ids as filter on the sejours
     *
     * @param array $prestations An array of CPrestation ids
     *
     * @return void
     */
    public function setPrestationsFilter(array $prestations): void
    {
        $this->prestations_filter = $prestations;
    }

    /**
     * Set a filter on the unprepared state of a sejour (field "entree_preparee")
     *
     * @param bool $filter
     *
     * @return void
     */
    public function setUnpreparedSejoursFilter(bool $filter): void
    {
        $this->unprepared_sejours_filter = $filter;
    }

    /**
     * Sets a filter on the "entree_reelle" field. If true, only the scheduled sejours will be returned or counted.
     * That mean the sejours where the field "entree_reelle" is null
     *
     * @param bool $filter
     *
     * @return void
     */
    public function setScheduledSejoursFilter(bool $filter): void
    {
        $this->scheduled_sejours_filter = $filter;
    }

    /**
     * Sets a filter that will limit the admissions to the morning or the afternoon
     *
     * @param string $period      The period (either morning or afternoon)
     * @param string $change_time The exact time on which the period will change
     *
     * @return void
     */
    public function setPeriodFilter(string $period, string $change_time): void
    {
        if (in_array($period, [self::FILTER_PERIOD_AFTERNOON, self::FILTER_PERIOD_MORNING])) {
            $this->period_filter = $period;
            $this->time_change_period = $change_time;
        }
    }

    /**
     * Sets a filter on the modes of the patient's leave
     *
     * @param array $exit_modes
     *
     * @return void
     */
    public function setLeaveModesFilters(array $exit_modes): void
    {
        $this->leave_modes_filter = $exit_modes;
    }

    /**
     * @param string $status_pec
     *
     * @return void
     */
    public function setStatusPecFilter(string $status_pec): void
    {
        $this->status_pec_filter = $status_pec;
    }

    /**
     * @param string $status
     *
     * @return void
     */
    public function setNotificationStatusFilter(string $status): void
    {
        $this->notification_status_filter = $status;
    }

    /**
     * @param bool $filter
     *
     * @return void
     */
    public function setWithoutRealLeaveDateFilter(bool $filter): void
    {
        $this->without_real_leave_date_filter = $filter;
    }

    /**
     * Returns the list of services for the group
     *
     * @return CService[]
     */
    public function loadServicesList(bool $external = false, CService $service_object = null): array
    {
        $request = $this->getServicesListRequest($external);
        $service_object = $service_object ?? new CService();

        try {
            $services = $service_object->loadList($request->where, $request->order) ?? [];
        } catch (Exception $e) {
            $services = [];
        }

        return $services;
    }

    /**
     * @param bool $external
     *
     * @return CRequest
     */
    public function getServicesListRequest(bool $external = false): CRequest
    {
        $external = intval($external);

        $request = new CRequest();
        $request->addTable('service');
        $request->addWhereClause('group_id', " = {$this->group->_id}");
        $request->addWhereClause('externe', " = '{$external}'");
        $request->addWhereClause('cancelled', " = '0'");
        $request->addOrder('nom ASC');

        return $request;
    }

    /**
     * Returns the list of practitioners for the user
     *
     * @return CMediusers[]
     */
    public function loadPractitioners(): array
    {
        try {
            $users = $this->user->loadPraticiens() ?? [];
        } catch (Exception $e) {
            $users = [];
        }

        return $users;
    }

    /**
     * Returns a CSejour object with the fields service_id and praticien_id valued
     *
     * @param int|null $service_id
     * @param int|null $user_id
     *
     * @return CSejour
     */
    public function getSejourFilterObject(?int $service_id, ?int $user_id): CSejour
    {
        $sejour = new CSejour();
        $sejour->service_id = $service_id;
        $sejour->praticien_id    = $user_id;
        $sejour->_type_admission = $this->sejour_type_filter;

        return $sejour;
    }

    /**
     * Count the sejour that are marked as "waiting approbation" (fiel recuse equal to -1)
     *
     * @param DateTimeImmutable $date
     * @param ?CSejour          $sejour_object An optional CSejour object (mostly used for test purpose)
     *
     * @return int
     */
    public function countSejoursInWaiting(DateTimeImmutable $date, ?CSejour $sejour_object = null): int
    {
        $request = $this->getCountSejoursInWaitingRequest($date);
        $sejour_object = $sejour_object ?? new CSejour();

        try {
            $count = $sejour_object->countList($request->where);
            if (!is_int($count) && is_numeric($count)) {
                $count = intval($count);
            } elseif (!is_int($count)) {
                $count = 0;
            }
        } catch (Exception $e) {
            $count = 0;
        }

        return $count;
    }

    public function getCountSejoursInWaitingRequest(DateTimeImmutable $date): CRequest
    {
        $request = new CRequest();
        $request->addWhereClause('group_id', " = '{$this->group->_id}'");
        $request->addWhereClause('recuse', " = '-1'");
        $request->addWhereClause('annule', " = '0'");
        $request->addWhereClause(
            $this->sejour_view_field,
            " BETWEEN '" . $date->format('Y-m-d 00:00:00')
            . "' AND '" . $date->format('Y-m-d 23:59:59') . "'"
        );

        if ($this->sejour_type_filter) {
            $request->addWhereClause('type', " = '{$this->sejour_type_filter}'");
        }

        return $request;
    }

    /**
     * Initialize an array, with as many entries as days in the given date's month.
     *
     * @param DateTimeImmutable $date
     * @param array             $keys A list of keys to add in each entry of the counter
     *
     * @return array
     */
    public function getMonthlyCounters(DateTimeImmutable $date, array $keys): array
    {
        $entry = [];

        foreach ($keys as $key) {
            $entry[$key] = 0;
        }

        try {
            $counters = [];
            $begin = $date->modify('first day of this month');
            $end   = $date->modify('last day of this month');


            for ($day = $begin; $day <= $end; $day = $day->modify('+1 day')) {
                $counters[$day->format('Y-m-d')] = $entry;
            }
        } catch (Exception $e) {
            $counters = [];
        }

        return $counters;
    }

    /**
     * Count the number of sejours with an entry date equals to the given date
     *
     * @param DateTimeImmutable $date
     *
     * @return array
     */
    public function countSejoursPerDay(DateTimeImmutable $date): array
    {
        try {
            $ds = $this->group->getDS();

            $this->getCountSejoursPerDayRequest($date);
            $results = $ds->loadHashList($this->request->makeSelect());
            if (!is_array($results)) {
                $results = [];
            }
        } catch (Exception $e) {
            $results = [];
        }

        return $results;
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function getCountSejoursPerDayRequest(DateTimeImmutable $date): CRequest
    {
        $this->setSejourRequest();

        $begin = $date->modify('first day of this month');
        $end = $date->modify('last day of this month');
        $this->request->addWhere(
            "`sejour`.`{$this->sejour_view_field}` BETWEEN '" . $begin->format('Y-m-d 00:00:00')
            . "' AND '" . $end->format('Y-m-d 23:59:59') . "'"
        );

        $this->request->addColumn("DATE_FORMAT(`sejour`.`{$this->sejour_view_field}`, '%Y-%m-%d')", 'date');
        $this->request->addColumn('COUNT(`sejour`.`sejour_id`)', 'count');
        $this->request->addGroup('date');
        $this->request->addOrder('date');

        return $this->request;
    }

    /**
     * Count the number of sejours within a month
     *
     * @param DateTimeImmutable $date
     *
     * @return int
     */
    public function countMonthlySejours(DateTimeImmutable $date): int
    {
        try {
            $ds = $this->group->getDS();

            $this->getCountMonthlySejoursRequest($date);
            $results = $ds->loadResult($this->request->makeSelect());
            if (!is_int($results) && is_numeric($results)) {
                $results = intval($results);
            } elseif (!is_int($results)) {
                $results = 0;
            }
        } catch (Exception $e) {
            $results = 0;
        }

        return $results;
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function getCountMonthlySejoursRequest(DateTimeImmutable $date): CRequest
    {
        $this->setSejourRequest();

        $begin = $date->modify('first day of this month');
        $end = $date->modify('last day of this month');
        $this->request->addWhere(
            "`sejour`.`{$this->sejour_view_field}` BETWEEN '" . $begin->format('Y-m-d') . ' 00:00:00'
            . "' AND '" . $end->format('Y-m-d') . " 23:59:59'"
        );

        $this->request->addColumn('COUNT(`sejour`.`sejour_id`)', 'count');

        return $this->request;
    }

    /**
     * Load the sejours that have a date equals to the given date, and by using the filters.
     * Allow to order the sejours by patient, practitioner, operation date and entry date (and several others)
     *
     * The results can use a SQL limit by setting the start and count parameters
     *
     * @param DateTimeImmutable      $date
     * @param string                 $order_column
     * @param string                 $order_way
     * @param int|null               $start
     * @param int|null               $count
     * @param DateTimeImmutable|null $date_max
     * @param ?CMbObject             $sejour_object An optional sejour object, mostly used for testing purpose
     *
     * @return CSejour[]
     */
    public function loadSejours(
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC',
        ?int $start = null,
        ?int $count = null,
        ?DateTimeImmutable $date_max = null,
        ?CMbObject $sejour_object = null
    ): array {
        $this->getLoadSejoursRequest($date, $order_column, $order_way, $start, $count, $date_max);

        $sejour_object = $sejour_object ?? new CSejour();
        try {
            $sejours = $sejour_object->loadListByReq($this->request);

            if (!$sejours) {
                $sejours = [];
            }
        } catch (Exception $e) {
            $sejours = [];
        }

        return $sejours;
    }

    /**
     * Load the sejours that have a date equals to the given date, and by using the filters.
     * Allow to order the sejours by patient, practitioner, operation date and entry date (and several others).
     *
     * The sejour will be filtered by permission type
     *
     * The results can use a SQL limit by setting the start and count parameters
     *
     * @param int                    $perm_type*
     * @param DateTimeImmutable      $date
     * @param string                 $order_column
     * @param string                 $order_way
     * @param int|null               $start
     * @param int|null               $count
     * @param DateTimeImmutable|null $date_max
     * @param ?CMbObject             $sejour_object An optional CSejour object, mostly used for testing purpose
     *
     * @return CSejour[]
     */
    public function loadSejoursByPerms(
        int $perm_type,
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC',
        ?int $start = null,
        ?int $count = null,
        ?DateTimeImmutable $date_max = null,
        ?CMbObject $sejour_object = null
    ): array {
        $this->getLoadSejoursRequest($date, $order_column, $order_way, $start, $count, $date_max);

        $sejour_object = $sejour_object ?? new CSejour();
        try {
            $sejours = $sejour_object->loadListWithPerms(
                $perm_type,
                $this->request->where,
                $this->request->order,
                $this->request->limit,
                $this->request->group,
                $this->request->ljoin
            );

            if (!$sejours) {
                $sejours = [];
            }
        } catch (Exception $e) {
            $sejours = [];
        }

        return $sejours;
    }

    /**
     * Count the sejours that have an entry date equals to the given date, and by using the filters.
     *
     * @param DateTimeImmutable      $date
     * @param DateTimeImmutable|null $date_max
     * @param ?CMbObject             $sejour_object An optional CSejour object, mostly used for testing purpose
     *
     * @return int
     */
    public function countSejours(
        DateTimeImmutable $date,
        ?DateTimeImmutable $date_max = null,
        ?CMbObject $sejour_object = null
    ): int {
        $this->getLoadSejoursRequest($date, '', '', null, null, $date_max);

        $sejour_object = $sejour_object ?? new CSejour();
        try {
            $total = $sejour_object->countList(
                $this->request->where,
                null,
                $this->request->ljoin
            );

            if (!is_int($total) && is_numeric($total)) {
                $total = intval($total);
            } elseif (!is_int($total)) {
                $total = 0;
            }
        } catch (Exception $e) {
            $total = 0;
        }

        return $total;
    }

    /**
     * Load the list of the functions of all the practitioner responsible for a sejour for the given date
     *
     * @param DateTimeImmutable $date
     * @param ?Cfunctions $function_object The object used for executing the request,
     *                                     mostly used for testing purpose
     *
     * @return CFunctions[]
     */
    public function loadFunctionsByAdmissions(DateTimeImmutable $date, CFunctions $function_object = null): array
    {
        $this->getLoadFunctionsByAdmissionsRequest($date);

        $function_object = $function_object ?? new CFunctions();
        try {
            $ids = $this->group->getDS()->loadColumn($this->request->makeSelect());
            if (is_array($ids) && count($ids)) {
                /** @var CFunctions[] $functions */
                $functions = $function_object->loadAll($ids);

                if (!is_array($functions)) {
                    $functions = [];
                }
            } else {
                $functions = [];
            }
        } catch (Exception $e) {
            $functions = [];
        }

        if (
            $this->function_filter && $this->function_filter->_id
            && !array_key_exists($this->function_filter->_id, $functions)
        ) {
            $functions[$this->function_filter->_id] = $this->function_filter;
        }

        uasort($functions, function ($a, $b) {
            return strnatcmp($a->text, $b->text);
        });

        return $functions;
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function getLoadFunctionsByAdmissionsRequest(DateTimeImmutable $date): CRequest
    {
        $this->getLoadSejoursRequest($date);

        unset($this->request->ljoin['patients']);
        if ($this->function_filter && $this->function_filter->_id) {
            unset($this->request->where['users_mediboard.function_id']);
        } else {
            $this->request->addLJoinClause('users_mediboard', '`users_mediboard`.`user_id` = `sejour`.`praticien_id`');
        }

        $this->request->select = [];
        $this->request->addSelect('DISTINCT `users_mediboard`.`function_id`');
        $this->request->order = [];

        return $this->request;
    }

    /**
     * Returns the min datetime for filtering the sejours in string format from the given DateTimeImmutable object,
     * by taking into account the period filter
     *
     * @param DateTimeImmutable $date
     *
     * @return string
     */
    public function getDateMin(DateTimeImmutable $date): string
    {
        $date_min = $date->format('Y-m-d 00:00:00');
        if (
            $this->period_filter && $this->time_change_period
            && $this->period_filter === self::FILTER_PERIOD_AFTERNOON
        ) {
            $date_min = $date->format("Y-m-d {$this->time_change_period}");
        }

        return $date_min;
    }

    /**
     * Returns the max datetime for filtering the sejours in string format from the given DateTimeImmutable object,
     * by taking into account the period filter
     *
     * @param DateTimeImmutable $date
     *
     * @return string
     */
    public function getDateMax(DateTimeImmutable $date): string
    {
        $date_max = $date->format('Y-m-d 23:59:59');
        if ($this->period_filter && $this->time_change_period && $this->period_filter === self::FILTER_PERIOD_MORNING) {
            $date_max = $date->format("Y-m-d {$this->time_change_period}");
        }

        return $date_max;
    }

    /**
     * Prepare the CRequest for loading the sejours
     *
     * @param DateTimeImmutable      $date
     * @param string                 $order_column
     * @param string                 $order_way
     * @param int|null               $start
     * @param int|null               $count
     * @param DateTimeImmutable|null $date_max
     *
     * @return CRequest
     */
    public function getLoadSejoursRequest(
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC',
        ?int $start = null,
        ?int $count = null,
        ?DateTimeImmutable $date_max = null
    ): CRequest {
        $this->setSejourRequest();

        $this->request->addColumn('`sejour`.*');

        if (!$date_max) {
            $date_max = $date;
        } elseif ($date_max < $date) {
            $date_temp = $date;
            $date = $date_max;
            $date_max = $date_temp;
        }

        $this->request->addWhere(
            "`sejour`.`{$this->sejour_view_field}` BETWEEN '" . $this->getDateMin($date)
            . "' AND '" . $this->getDateMax($date_max) . "'"
        );

        $this->setOrder($order_column, $order_way);

        if (is_int($count)) {
            $this->request->setLimit(is_int($start) ? "{$start}, {$count}" : "{$count}");
        }

        return $this->request;
    }

    /**
     * Sets the order by clause of the request
     *
     * Allow to order by the following columns : entree_prevue, entree_reelle, pec_acceuil, pec_service, sortie_prevue,
     * by service name (on the affectation), by practitioner name, by patient name
     *
     * @param string $column
     * @param string $way
     *
     * @return void
     */
    protected function setOrder(string $column, string $way): void
    {
        if (!in_array($way, ['ASC', 'DESC'])) {
            $way = 'ASC';
        }

        switch ($column) {
            case 'entree_prevue':
            case 'entree_reelle':
            case 'pec_accueil':
            case 'pec_service':
            case 'sortie_prevue':
                $this->request->addLJoinClause('patients', '`patients`.`patient_id` = `sejour`.`patient_id`');
                $this->request->addOrder(
                    "`sejour`.`{$column}` {$way}, `patients`.`nom`, `patients`.`prenom`"
                );
                break;
            case 'service':
                if (!array_key_exists('affectation', $this->request->ljoin)) {
                    $this->request->addLJoinClause('affectation', '`affectation`.`sejour_id` = `sejour`.`sejour_id`');
                }

                if (!array_key_exists('service', $this->request->ljoin)) {
                    $this->request->addLJoinClause('service', '`service`.`service_id` = `affectation`.`service_id`');
                }

                $this->request->addOrder("`service`.`nom` {$way}, `sejour`.`{$this->sejour_view_field}`");
                break;
            case 'praticien_id':
                $this->request->addLJoinClause('users', '`users`.`user_id` = `sejour`.`praticien_id`');
                $this->request->addOrder(
                    "`users`.`user_last_name` {$way}, `users`.`user_first_name`, "
                    . "`sejour`.`{$this->sejour_view_field}`"
                );
                break;
            case 'passage_bloc':
                if (!array_key_exists('operations', $this->request->ljoin)) {
                    $this->request->addLJoinClause('operations', '`operations`.`sejour_id` = `sejour`.`sejour_id`');
                }

                $this->request->addOrder(
                    "`operations`.`date` {$way}, `operations`.`time_operation` {$way}"
                );
                break;
            case 'patient_name':
            case 'patient_id':
            default:
                $this->request->addLJoinClause('patients', '`patients`.`patient_id` = `sejour`.`patient_id`');
                $this->request->addOrder(
                    "`patients`.`nom` {$way}, `patients`.`prenom` {$way}, `sejour`.`{$this->sejour_view_field}`"
                );
                break;
        }
    }

    /**
     * Initialize the CRequest object for making queries on the sejour table.
     * Take all the filters into account
     *
     * @return void
     */
    protected function setSejourRequest(): void
    {
        $this->request = new CRequest();
        $this->request->addTable($this->table);

        $this->request->addWhere("`sejour`.`group_id` = '{$this->group->_id}'");

        $this->filterSejoursByUser();
        $this->filterSejoursByService();
        $this->filterByFunction();
        $this->filterSejoursByType();
        $this->filterSejoursByRecuse();
        $this->filterSejoursByLeaveModes();
        $this->filterCancelledSejours();
        $this->filterConfirmedSejours();
        $this->filterSejoursByTypePec();
        $this->filterSejoursByCircuitAmbu();
        $this->filterSejoursWithOperationDateEqualsToEntry();
        $this->filterSejoursByPaymentExceedingFees();
        $this->filterSejoursByPrestations();
        $this->filterUnpreparedSejours();
        $this->filterSejoursByStatusPec();
        $this->filterSejoursWithoutRealLeaveDate();
        $this->filterScheduledSejours();
        $this->filterSejoursByNotificationStatus();
    }

    /**
     * Adds the left join and where clauses for filtering the sejours by service.
     *
     * Filter by CService id, or by a list of CService ids
     *
     * @return void
     */
    protected function filterSejoursByService(): void
    {
        if ($this->service_filter && $this->service_filter->_id) {
            $this->request->addLJoinClause('affectation', '`affectation`.`sejour_id` = `sejour`.`sejour_id`');
            $this->request->addWhere(
                "`affectation`.`service_id` = {$this->service_filter->_id}" .
                " OR (`sejour`.`service_id` = {$this->service_filter->_id} AND `affectation`.`affectation_id` IS NULL)"
            );
        } elseif (count($this->services_list_filter)) {
            $affectation_field = $this->sejour_view_field;
            if ($this->sejour_view_field === 'entree_reelle') {
                $affectation_field = 'entree';
            } elseif ($this->sejour_view_field === 'sortie_prevue') {
                $affectation_field = 'sortie';
            }

            $this->request->addLJoinClause('affectation', '`affectation`.`sejour_id` = `sejour`.`sejour_id`');
            $this->request->addWhere(
                "`affectation`.`{$affectation_field}` = `sejour`.`{$affectation_field}`"
            );

            $not_placed = false;
            if (in_array('NP', $this->services_list_filter)) {
                $not_placed = true;
                $this->services_list_filter = array_diff($this->services_list_filter, ['NP']);
            }

            $in_clause = CSQLDataSource::prepareIn($this->services_list_filter);
            $clause = "`affectation`.`service_id` {$in_clause}";

            if ($not_placed) {
                $clause .= ' OR `affectation`.`service_id` IS NULL';
            } else {
                $clause .= " OR (`sejour`.`service_id` {$in_clause} AND `affectation`.`affectation_id` IS NULL)";
            }

            $this->request->addWhere($clause);
        }
    }

    /**
     * Adds the left join and where clauses for filtering the sejours by mediuser
     *
     * @return void
     */
    protected function filterSejoursByUser(): void
    {
        if ($this->user_filter && $this->user_filter->_id) {
            if ($this->user_filter->isAnesth()) {
                $this->request->addLJoinClause('operations', '`operations`.`sejour_id` = `sejour`.`sejour_id`');
                $this->request->addLJoinClause('plagesop', '`plagesop`.`plageop_id` = `operations`.`plageop_id`');
                $this->request->addWhere(
                    "`operations`.`anesth_id` = {$this->user_filter->_id}"
                    . " OR `plagesop`.`anesth_id` = {$this->user_filter->_id}"
                );
            } else {
                $this->request->addWhere("`sejour`.`praticien_id` = {$this->user_filter->_id}");
            }
        }
    }

    /**
     * Adds the where and left join clauses for filtering the sejours by the practitioner function
     *
     * @return void
     */
    protected function filterByFunction(): void
    {
        if ($this->function_filter && $this->function_filter->_id) {
            $this->request->addLJoinClause('users_mediboard', '`users_mediboard`.`user_id` = `sejour`.`praticien_id`');
            $this->request->addWhereClause('users_mediboard.function_id', " = {$this->function_filter->_id}");
        }
    }

    /**
     * Set the filters for the sejour type.
     * First uses an equal filter, then a in, and finally a not in.
     *
     * @return void
     */
    protected function filterSejoursByType(): void
    {
        if ($this->sejour_type_filter) {
            $this->request->addWhere("`sejour`.`type` = '{$this->sejour_type_filter}'");
        } elseif (count($this->sejour_type_list_filter)) {
            $this->request->addWhere('`sejour`.`type` ' . CSQLDataSource::prepareIn($this->sejour_type_list_filter));
        } elseif (count($this->sejour_type_exclude_list_filter)) {
            $this->request->addWhere(
                '`sejour`.`type` ' . CSQLDataSource::prepareNotIn($this->sejour_type_exclude_list_filter)
            );
        }
    }

    /**
     * Adds a where clause on the "recuse" field
     *
     * @return void
     */
    protected function filterSejoursByRecuse(): void
    {
        if (!is_null($this->recuse_filter)) {
            $this->request->addWhere("`sejour`.`recuse` = '{$this->recuse_filter}'");
        }
    }

    /**
     * Adds a where clause on the cancelled field
     *
     * @return void
     */
    protected function filterCancelledSejours(): void
    {
        if (!is_null($this->cancelled_filter)) {
            $this->request->addWhere("`sejour`.`annule` = '{$this->cancelled_filter}'");
        }
    }

    /**
     * Adds a where clause on the confirme field
     *
     * @return void
     */
    protected function filterConfirmedSejours(): void
    {
        if (!is_null($this->confirmed_filter)) {
            $this->request->addWhere('`sejour`.`confirme` ' . ($this->confirmed_filter ? 'IS NOT NULL' : 'IS NULL'));
        }
    }

    /**
     * Adds a where clause on the type_pec fields
     *
     * @return void
     */
    protected function filterSejoursByTypePec(): void
    {
        if (count($this->type_pec_filter)) {
            $this->request->addWhere(
                '`sejour`.`type_pec` ' . CSQLDataSource::prepareIn($this->type_pec_filter)
                . ' OR `sejour`.`type_pec` IS NULL'
            );
        }
    }

    /**
     * Adds a where clause on the circuit_ambu field
     *
     * @return void
     */
    protected function filterSejoursByCircuitAmbu(): void
    {
        if (count($this->circuit_ambu_filter)) {
            $this->request->addWhere(
                '`sejour`.`circuit_ambu` ' . CSQLDataSource::prepareIn($this->circuit_ambu_filter)
                . ' OR `sejour`.`circuit_ambu` IS NULL'
            );
        }
    }

    /**
     * Adds a where clause that checks if the operation's date is equal to the entry date of the sejour
     *
     * @return void
     */
    protected function filterSejoursWithOperationDateEqualsToEntry(): void
    {
        if ($this->date_operation_equals_entry_filter) {
            if (!array_key_exists('operations', $this->request->ljoin)) {
                $this->request->addLJoinClause('operations', '`operations`.`sejour_id` = `sejour`.`sejour_id`');
            }

            $this->request->addWhere('`operations`.`date` = DATE(`sejour`.`entree_prevue`)');
        }
    }

    /**
     * Adds where clauses on the payment of the exceeding fees of the operations
     *
     * @return void
     */
    protected function filterSejoursByPaymentExceedingFees(): void
    {
        if ($this->payment_exceeding_fees_filter !== self::FILTER_PAYMENT_EXCEEDING_FEES_ALL) {
            if (!array_key_exists('operations', $this->request->ljoin)) {
                $this->request->addLJoinClause('operations', '`operations`.`sejour_id` = `sejour`.`sejour_id`');
            }

            if ($this->payment_exceeding_fees_filter === self::FILTER_PAYMENT_EXCEEDING_FEES_PAYED) {
                $this->request->addWhere(<<<CLAUSE
(
    (`operations`.`depassement` > 0 AND `operations`.`reglement_dh_chir` != 'non_regle')
    OR `operations`.`depassement` = 0 OR `operations`.`depassement` IS NULL
) AND (
    (`operations`.`depassement_anesth` > 0 AND `operations`.`reglement_dh_anesth` != 'non_regle')
    OR `operations`.`depassement_anesth` = 0 OR `operations`.`depassement_anesth` IS NULL
) AND (`operations`.`depassement` > 0 OR `operations`.`depassement_anesth` > 0)
CLAUSE);
            } elseif ($this->payment_exceeding_fees_filter === self::FILTER_PAYMENT_EXCEEDING_FEES_UNPAID) {
                $this->request->addWhere(
                    "(`operations`.`depassement` > 0 AND `operations`.`reglement_dh_chir` = 'non_regle')" .
                    " OR (`operations`.`depassement_anesth` > 0 AND `operations`.`reglement_dh_anesth` = 'non_regle')"
                );
            }
        }
    }

    /**
     * Adds clauses on the prestations linked to the sejour
     *
     * @return void
     */
    protected function filterSejoursByPrestations(): void
    {
        if (count($this->prestations_filter)) {
            $this->request->addLJoinClause('item_liaison', '`item_liaison`.`sejour_id` = `sejour`.`sejour_id`');
            $this->request->addLJoinClause(
                'item_prestation',
                '`item_prestation`.`item_prestation_id` = `item_liaison`.`item_souhait_id`'
            );
            $this->request->addLJoinClause(
                'prestation_ponctuelle',
                '`prestation_ponctuelle`.`prestation_ponctuelle_id` = `item_prestation`.`object_id`'
            );
            $this->request->addWhere("`item_prestation`.`object_class` = 'CPrestationPonctuelle'");
            $this->request->addWhere(
                '`prestation_ponctuelle`.`prestation_ponctuelle_id` '
                . CSQLDataSource::prepareIn($this->prestations_filter)
            );
        }
    }

    /**
     * Adds a where clause on the field "mode_sortie" of the sejour
     *
     * @return void
     */
    protected function filterSejoursByLeaveModes(): void
    {
        if (count($this->leave_modes_filter) === 1 && in_array('', $this->leave_modes_filter)) {
            $this->request->addWhere('`sejour`.`mode_sortie` IS NULL');
        } elseif (count($this->leave_modes_filter)) {
            $clause = '';
            if (in_array('', $this->leave_modes_filter)) {
                CMbArray::removeValue('', $this->leave_modes_filter);
                $clause = '`sejour`.`mode_sortie` IS NULL OR ';
            }

            $clause .= '`sejour`.`mode_sortie` ' . CSQLDataSource::prepareIn($this->leave_modes_filter);
            $this->request->addWhere($clause);
        }
    }

    /**
     * Adds a where clause on the field "entree_preparee"
     *
     * @return void
     */
    protected function filterUnpreparedSejours(): void
    {
        if ($this->unprepared_sejours_filter) {
            $field = "{$this->sejour_view_field}_preparee";
            if ($this->sejour_view_field === 'sortie_prevue') {
                $field = 'sortie_preparee';
            } elseif ($this->sejour_view_field === 'entree_reelle') {
                $field = 'entree_preparee';
            }

            $this->request->addWhere("`sejour`.`{$field}` = '0'");
        }
    }

    /**
     * @return void
     */
    protected function filterSejoursByStatusPec(): void
    {
        switch ($this->status_pec_filter) {
            case self::FILTER_STATUS_PEC_WAITING:
                $this->request->addWhere('`sejour`.`pec_accueil` IS NULL');

                if ($this->sejour_view_field !== 'entree_reelle') {
                    $this->request->addWhere('`sejour`.`entree_reelle` IS NOT NULL');
                }
                break;
            case self::FILTER_STATUS_PEC_ONGOING:
                $this->request->addWhere('`sejour`.`pec_accueil` IS NOT NULL');
                $this->request->addWhere('`sejour`.`pec_service` IS NULL');
                break;
            case self::FILTER_STATUS_PEC_OVER:
                $this->request->addWhere('`sejour`.`pec_service` IS NOT NULL');
                break;
            default:
        }
    }

    /**
     * Adds a where clause that checks that the real entry date is empty
     *
     * @return void
     */
    protected function filterScheduledSejours(): void
    {
        if ($this->scheduled_sejours_filter) {
            switch ($this->sejour_view_field) {
                case self::SEJOUR_VIEW_SORTIES_SCHEDULED:
                case self::SEJOUR_VIEW_SORTIES:
                    $field = 'sortie_reelle';
                    break;
                case self::SEJOUR_VIEW_REAL_ADMISSIONS:
                case self::SEJOUR_VIEW_ADMISSIONS:
                default:
                    $field = 'entree_reelle';
            }
            $this->request->addWhere(
                "`sejour`.`{$field}` IS NULL OR `sejour`.`{$field}` = '0000-00-00'"
            );
        }
    }

    /**
     * Adds SQL clauses to check the notification status
     *
     * @return void
     */
    protected function filterSejoursByNotificationStatus(): void
    {
        if ($this->notification_status_filter && CModule::getActive('notifications')) {
            $this->request->addLJoinClause('notifications', '`notifications`.`context_id` = `sejour`.`sejour_id`');
            $this->request->addLJoinClause(
                'notification_events',
                '`notification_events`.`notification_event_id` = `notifications`.`event_id`'
            );
            $this->request->addLJoinClause(
                'sms',
                "`sms`.`sms_id` = `notifications`.`message_id` AND `notifications`.`message_class` = 'CSMS'"
            );
            $this->request->addLJoinClause(
                'mails',
                "`mails`.`mail_id` = `notifications`.`message_id` AND `notifications`.`message_class` = 'CMail'"
            );

            $this->request->addWhere("`notifications`.`context_class` = 'CSejour'");
            $this->request->addWhere("`notification_events`.`context_moment` = 'day_before'");
            $this->request->addWhere(
                "`sms`.`status` = '{$this->notification_status_filter}'"
                . " OR `mails`.`status` = '{$this->notification_status_filter}'"
            );
        }
    }

    /**
     * If the filter without_real_leave_date_filter is set to true,
     * only the sejours without a real leave date will be returned
     *
     * @return void
     */
    protected function filterSejoursWithoutRealLeaveDate(): void
    {
        if ($this->without_real_leave_date_filter) {
            $this->request->addWhere('`sejour`.`sortie_reelle` IS NULL');
        }
    }

    public static function flagContextualIcons(): bool
    {
        $flag_contextual_icons     = false;
        $systeme_prestations_tiers = CAppUI::gconf('dPhospi prestations systeme_prestations_tiers');

        if (
            CModule::getActive('web100T')
            || CModule::getActive('softway')
            || CModule::getActive('novxtelHospitality')
            || CModule::getActive('notifications')
            || CAppUI::gconf('dPadmissions General show_deficience')
            || !($systeme_prestations_tiers == 'Aucun' || !CModule::getActive($systeme_prestations_tiers))
            || !($systeme_prestations_tiers == 'softway' && CAppUI::gconf('softway presta send_presta_immediately'))
        ) {
            $flag_contextual_icons = true;
        }

        return $flag_contextual_icons;
    }

    public static function flagDMP(): bool
    {
        $flag_dmp = false;
        if (CModule::getActive('dmp')) {
            $auth_type = CDMP::getAuthentificationType();
            $flag_dmp  = ($auth_type === 'indirecte' || ($auth_type === 'directe' && CMediusers::get()->isPraticien()));
        }

        return $flag_dmp;
    }
}
