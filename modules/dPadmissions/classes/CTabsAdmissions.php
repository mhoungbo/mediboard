<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions;

use Ox\Core\CAppUI;
use Ox\Core\Module\AbstractTabsRegister;

/**
 * @codeCoverageIgnore
 */
class CTabsAdmissions extends AbstractTabsRegister
{
    public function registerAll(): void
    {
        if (CAppUI::conf('dPplanningOp CSejour use_recuse')) {
            $this->registerFile('viewSejourValidation', TAB_EDIT);
        }

        $this->registerFile('viewAdmissions', TAB_READ);
        $this->registerFile('viewSorties', TAB_READ);
        $this->registerFile('viewPreAdmissions', TAB_READ);
        $this->registerFile('viewSejourPermissions', TAB_READ);
        $this->registerFile('viewPresentPatients', TAB_READ);
        $this->registerFile('viewDischargeProject', TAB_READ);

        if (CAppUI::gconf('dPadmissions General view_sortie_masss')) {
            $this->registerFile('viewMassSorties', TAB_READ);
        }
        $this->registerFile('viewPatientReceptions', TAB_READ);

        $this->registerFile('viewIdentitoVigilance', TAB_ADMIN);

        $this->registerFile('configure', TAB_ADMIN, self::TAB_CONFIGURE);
    }
}
