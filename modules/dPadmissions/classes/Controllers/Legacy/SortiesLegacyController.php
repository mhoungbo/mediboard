<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\AppFine\Client\CAppFineClient;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Hospi\CLit;
use Ox\Mediboard\Hospi\CPrestation;
use Ox\Mediboard\Hospi\CPrestationPonctuelle;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CModeSortieSejour;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * The legacy controller for the 'Sorties' view
 */
class SortiesLegacyController extends CLegacyController
{
    private const FILTER_SCHEDULED  = 'scheduled';
    private const FILTER_UNPREPARED = 'unprepared';

    /**
     * The main view for the sorties
     *
     * @return void
     * @throws Exception
     */
    public function viewSorties(): void
    {
        $this->checkPermRead();

        $sejour = new CSejour();

        $selSortis         = CView::get(
            'selSortis',
            'enum list|' . self::FILTER_SCHEDULED . '|' . self::FILTER_UNPREPARED,
            true
        );
        $order_column      = CView::get('order_col', 'enum list|patient_id', true);
        $order_way         = CView::get('order_way', 'enum list|ASC|DESC default|ASC', true);
        $date              = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $prat_id           = CView::get('prat_id', 'ref class|CMediusers', true);
        $only_confirmed    = CView::get('only_confirmed', 'bool default|0', true);
        $period            = CView::get('period', 'enum list|soir|matin', true);
        $filterFunction    = CView::get('filterFunction', 'ref class|CFunctions', true);
        $enabled_service   = CView::get('active_filter_services', 'bool default|0', true);
        $type_pec          = CView::get('type_pec', ['str', 'default' => $sejour->_specs['type_pec']->_list], true);
        $circuits_ambu     = CView::get(
            'circuit_ambu',
            ['str', 'default' => $sejour->_specs['circuit_ambu']->_list],
            true
        );
        $prestations_p_ids = CView::get('prestations_ids', 'str', true);

        CService::getServicesIdsPref(CView::get('services_ids', 'str', true));
        CSejour::getTypeSejourIdsPref(CView::get('sejours_ids', 'str', true));

        CView::checkin();

        $prat = CMediusers::get();

        $sejour               = new CSejour();
        $sejour->praticien_id = $prat_id;

        $this->renderSmarty('sorties_view', [
            'sejour'                  => $sejour,
            'date_demain'             => $date->modify('+1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle'           => $date->format('Y-m-d 00:00:00'),
            'date'                    => $date->format('Y-m-d'),
            'selSortis'               => $selSortis,
            'order_way'               => $order_way,
            'order_col'               => $order_column,
            'prats'                   => $prat->loadPraticiens(),
            'hier'                    => $date->modify('-1 day')->format('Y-m-d'),
            'demain'                  => $date->modify('+1 day')->format('Y-m-d'),
            'period'                  => $period,
            'only_confirmed'          => $only_confirmed,
            'filterFunction'          => $filterFunction,
            'list_type_ad'            => $sejour->_specs['_type_admission']->_list,
            'enabled_service'         => $enabled_service,
            'type_pec'                => $type_pec,
            'circuits_ambu'           => $circuits_ambu,
            'prestations_ponctuelles' => CPrestationPonctuelle::loadCurrentList(),
            'prestations_p_ids'       => $prestations_p_ids,
        ]);
    }

    /**
     * Displays the monthly counters for the sorties
     *
     * @return void
     * @throws Exception
     */
    public function sortiesCounters(): void
    {
        $this->checkPermRead();

        $sejour = new CSejour();

        $current_m       = CView::get('current_m', 'str');
        $selSortis       = CView::get(
            'selSortis',
            'enum list|' . self::FILTER_SCHEDULED . '|' . self::FILTER_UNPREPARED,
            true
        );
        $date            = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $services_ids    = CView::get('services_ids', 'str', true);
        $sejours_ids     = CView::get('sejours_ids', 'str', true);
        $prat_id         = CView::get('prat_id', 'ref class|CMediusers', true);
        $enabled_service = CView::get('active_filter_services', 'bool default|0', true);
        $type_pec        = CView::get('type_pec', ['str', 'default' => $sejour->_specs['type_pec']->_list], true);
        $only_confirmed  = CView::get('only_confirmed', 'bool default|0', true);
        $modes_sortie    = CView::get('mode_sortie', ['str', 'default' => ['all']]);
        $circuits_ambu   = CView::get(
            'circuit_ambu',
            ['str', 'default' => $sejour->_specs['circuit_ambu']->_list],
            true
        );
        $reglement_dh    = CView::get('reglement_dh', 'enum list|all|payed|not_payed default|all');

        CView::checkin();
        CView::enforceSlave();

        $circuits_ambu = AdmissionsLegacyController::cleanArrayValues($circuits_ambu);
        $services_ids  = AdmissionsLegacyController::cleanArrayValues($services_ids);
        $sejours_ids   = AdmissionsLegacyController::cleanArrayValues($sejours_ids);

        $sejour_type_pref = AdmissionsLegacyController::getSejourTypesPreference($sejours_ids);

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_SORTIES
        );

        if (in_array('all', $modes_sortie)) {
            $modes_sortie = [];
        }

        $repository->setCancelledFilter(0);

        if (count($sejour_type_pref)) {
            $repository->setSejourTypeListFilter($sejour_type_pref);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        if (count($services_ids) && $enabled_service) {
            $repository->setServicesListFilter($services_ids);
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        if ($only_confirmed !== '') {
            $repository->setConfirmedFilter((bool)$only_confirmed);
        }

        if ($reglement_dh) {
            $repository->setPaymentExceedingFeesFilter($reglement_dh);
        }

        if (!(in_array('', $type_pec) && count($type_pec) == 1)) {
            $repository->setTypePecFilter($type_pec);
        }

        if (CAppUI::gconf('dPplanningOp CSejour show_circuit_ambu') && count($circuits_ambu)) {
            $repository->setCircuitAmbuFilter($circuits_ambu);
        }

        if (is_array($modes_sortie) && count($modes_sortie)) {
            $repository->setLeaveModesFilters($modes_sortie);
        }

        /* Get the list of days in the month, with the counters set to 0 */
        $days = $repository->getMonthlyCounters(
            $date,
            ['sorties', 'sorties_non_effectuees', 'sorties_non_preparees', 'sorties_non_facturees']
        );

        /* Get the number of sorties per day of the month */
        $sorties = $repository->countSejoursPerDay($date);

        /* Get the number of scheduled sorties per day of the month */
        $repository->setScheduledSejoursFilter(true);
        $scheduled = $repository->countSejoursPerDay($date);

        /* Get the number of unprepared sorties per day of the month */
        $repository->setScheduledSejoursFilter(false);
        $repository->setUnpreparedSejoursFilter(true);
        $unprepared = $repository->countSejoursPerDay($date);

        $totaux = [
            'sorties'                => 0,
            'sorties_non_effectuees' => 0,
            'sorties_non_preparees'  => 0,
        ];

        /* Set the counter foreach day */
        foreach ($days as $day => $data) {
            if (array_key_exists($day, $sorties)) {
                $days[$day]['sorties'] = $sorties[$day];
                $totaux['sorties']     += $sorties[$day];
            }
            if (array_key_exists($day, $scheduled)) {
                $days[$day]['sorties_non_effectuees'] = $scheduled[$day];
                $totaux['sorties_non_effectuees']     += $scheduled[$day];
            }
            if (array_key_exists($day, $unprepared)) {
                $days[$day]['sorties_non_preparees'] = $unprepared[$day];
                $totaux['sorties_non_preparees']     += $unprepared[$day];
            }
        }

        $this->renderSmarty('sorties_counters', [
            'hier'            => $date->modify('-1 day')->format('Y-m-d'),
            'demain'          => $date->modify('+1 day')->format('Y-m-d'),
            'selSortis'       => $selSortis,
            'bank_holidays'   => CMbDT::getHolidays($date->format('Y-m-d')),
            'date'            => $date->format('Y-m-d'),
            'lastmonth'       => $date->modify('last day of -1 month')->format('Y-m-d'),
            'nextmonth'       => $date->modify('last day of 1 month')->format('Y-m-d'),
            'days'            => $days,
            'enabled_service' => $enabled_service,
            'totaux'          => $totaux,
            'circuits_ambu'   => $circuits_ambu,
            'current_m'       => $current_m,
        ]);
    }

    /**
     * Displays the list of sejours with an exit date equal to the specified date
     *
     * @return void
     * @throws Exception
     */
    public function sortiesList(): void
    {
        $this->checkPermRead();

        $sejour = new CSejour();

        $type              = CView::get('type', 'str');
        $services_ids      = CView::get('services_ids', 'str', true);
        $sejours_ids       = CView::get('sejours_ids', 'str', true);
        $prat_id           = CView::get('prat_id', 'ref class|CMediusers', true);
        $only_confirmed    = CView::get('only_confirmed', 'bool default|0', true);
        $selSortis         = CView::get(
            'selSortis',
            'enum list|' . self::FILTER_SCHEDULED . '|' . self::FILTER_UNPREPARED,
            true
        );
        $order_col         = CView::get('order_col', 'str default|patient_id', true);
        $order_way         = CView::get('order_way', 'str default|ASC', true);
        $date              = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $filterFunction    = CView::get('filterFunction', 'ref class|CFunctions', true);
        $period            = CView::get('period', 'str', true);
        $enabled_service   = CView::get('active_filter_services', 'bool default|0', true);
        $type_pec          = CView::get('type_pec', ['str', 'default' => $sejour->_specs['type_pec']->_list], true);
        $modes_sortie      = CView::get('mode_sortie', ['str', 'default' => ['all']]);
        $print_global      = CView::get('print_global', 'bool default|0');
        $reglement_dh      = CView::get('reglement_dh', 'enum list|all|payed|not_payed');
        $circuits_ambu     = CView::get(
            'circuit_ambu',
            ['str', 'default' => $sejour->_specs['circuit_ambu']->_list],
            true
        );
        $prestations_p_ids = CView::get('prestations_p_ids', ['str', 'default' => []], true);
        $step              = CView::get(
            'step',
            'num default|' . CAppUI::gconf('dPadmissions General pagination_step')
        );
        $page              = CView::get('page', 'num default|0');

        CView::checkin();
        CView::enforceSlave();

        $this->cleanArrayParameter($prestations_p_ids);
        $this->cleanArrayParameter($modes_sortie);

        $circuits_ambu = AdmissionsLegacyController::cleanArrayValues($circuits_ambu);
        $services_ids  = AdmissionsLegacyController::cleanArrayValues($services_ids);
        $sejours_ids   = AdmissionsLegacyController::cleanArrayValues($sejours_ids);

        $sejour_type_pref = AdmissionsLegacyController::getSejourTypesPreference($sejours_ids);

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_SORTIES
        );

        $repository->setCancelledFilter(0);

        if (count($services_ids) && $enabled_service) {
            $repository->setServicesListFilter($services_ids);
        }

        $this->setSejourTypeFilter($sejour_type_pref, $repository);

        if (CAppUI::gconf('dPplanningOp CSejour show_circuit_ambu') && count($circuits_ambu)) {
            $repository->setCircuitAmbuFilter($circuits_ambu);
        }

        if (!(in_array('', $type_pec) && count($type_pec) == 1)) {
            $repository->setTypePecFilter($type_pec);
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        if ($only_confirmed !== '') {
            $repository->setConfirmedFilter((bool)$only_confirmed);
        }

        if (is_array($modes_sortie) && count($modes_sortie)) {
            $repository->setLeaveModesFilters($modes_sortie);
        }

        if ($reglement_dh) {
            $repository->setPaymentExceedingFeesFilter($reglement_dh);
        }

        if ($filterFunction) {
            $repository->setFunctionFilter(CFunctions::findOrNew($filterFunction));
        }

        if (count($prestations_p_ids)) {
            $repository->setPrestationsFilter($prestations_p_ids);
        }

        if ($selSortis === self::FILTER_SCHEDULED) {
            $repository->setScheduledSejoursFilter(true);
        } elseif ($selSortis === self::FILTER_UNPREPARED) {
            $repository->setUnpreparedSejoursFilter(true);
        }

        if ($period) {
            $hour = CAppUI::gconf('dPadmissions General hour_matin_soir');
            $repository->setPeriodFilter($period, $hour);
        }

        if ($print_global) {
            $page = null;
            $step = null;
        }

        $sejours = CAppUI::gconf('dPadmissions General use_perms')
            ? $repository->loadSejoursByPerms(PERM_READ, $date, $order_col, $order_way, $page, $step)
            : $repository->loadSejours($date, $order_col, $order_way, $page, $step);

        $total = $repository->countSejours($date);

        $functions = $repository->loadFunctionsByAdmissions($date);

        $this->prepareSejoursList($sejours);

        $type = $this->getTypeFromArray($sejour_type_pref);

        $this->renderSmarty('sorties_list', [
            'flag_contextual_icons' => AdmissionsRepository::flagContextualIcons(),
            'hier'                  => $date->modify('- 1 day')->format('Y-m-d'),
            'demain'                => $date->modify('+ 1 day')->format('Y-m-d'),
            'date_min'              => $repository->getDateMin($date),
            'date_max'              => $repository->getDateMax($date),
            'date_demain'           => $date->modify('- 1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle'         => $date->format('Y-m-d 00:00:00'),
            'date'                  => $date->format('Y-m-d'),
            'type'                  => $type,
            'selSortis'             => $selSortis,
            'order_col'             => $order_col,
            'order_way'             => $order_way,
            'sejours'               => $sejours,
            'prestations'           => CPrestation::loadCurrentList(),
            'canAdmissions'         => CModule::getCanDo('dPadmissions'),
            'canPatients'           => CModule::getCanDo('dPpatients'),
            'canPlanningOp'         => CModule::getCanDo('dPplanningOp'),
            'functions'             => $functions,
            'filterFunction'        => $filterFunction,
            'period'                => $period,
            'enabled_service'       => $enabled_service,
            'print_global'          => $print_global,
            'circuits_ambu'         => $circuits_ambu,
            'total'                 => $total,
            'step'                  => $step,
            'page'                  => $page,
        ]);
    }

    /**
     * Displays the line for a sortie in the list
     *
     * @return void
     * @throws Exception
     */
    public function sortieLine(): void
    {
        $this->checkPermRead();

        $sejour_id = CView::get('sejour_id', 'ref class|CSejour');
        $date      = new DateTimeImmutable(CView::get('date', 'date default|now', true));

        CView::checkin();
        CView::enforceSlave();

        $sejour = CSejour::findOrNew($sejour_id);

        $sejour->loadRefPraticien();
        $sejour->loadRefPatient(1)->loadIPP();
        $sejour->loadNDA();
        $sejour->loadRefsNotes();
        $sejour->countPrestationsSouhaitees();
        $sejour->loadRefsAppel('sortie');

        $sejour->loadRefsOperations(['annulee' => "= '0'"]);
        foreach ($sejour->_ref_operations as $operation) {
            $operation->loadRefsActes();
            $operation->loadRefPlageOp();
        }

        $sejour->loadRefsAffectations();

        if (CModule::getActive('maternite')) {
            $sejour->_sejours_enfants_ids = CMbArray::pluck($sejour->loadRefsNaissances(), 'sejour_enfant_id');
        }

        $sejour->loadRefEtablissementTransfert();
        $sejour->loadRefServiceMutation();

        if (CModule::getActive('appFineClient')) {
            CAppFineClient::loadIdex($sejour->_ref_patient, $sejour->group_id);
            CAppFineClient::loadIdex($sejour, $sejour->group_id);
            $sejour->_ref_patient->loadRefStatusPatientUser();
        }

        $this->renderSmarty('sorties_line', [
            '_sejour'               => $sejour,
            'flag_contextual_icons' => AdmissionsRepository::flagContextualIcons(),
            'date'                  => $date->format('Y-m-d'),
            'date_min'              => $date->format('Y-m-d 00:00:00'),
            'date_max'              => $date->format('Y-m-d 23:59:59'),
            'prestations'           => CPrestation::loadCurrentList(),
            'canAdmissions'         => CModule::getCanDo('dPadmissions'),
            'canPatients'           => CModule::getCanDo('dPpatients'),
            'canPlanningOp'         => CModule::getCanDo('dPplanningOp'),
        ]);
    }

    /**
     * Displays the printable view of the exits
     *
     * @return void
     * @throws Exception
     */
    public function printSorties(): void
    {
        $this->checkPermRead();

        $date            = new DateTimeImmutable(CView::get('date', 'date default|now'));
        $type            = CView::get('type', 'str');
        $services_ids    = CView::get('services_ids', 'str', true);
        $sejours_ids     = CView::get('sejours_ids', 'str', true);
        $period          = CView::get('period', 'str');
        $enabled_service = CView::get('active_filter_services', 'bool default|0', true);

        CView::checkin();
        CView::enforceSlave();

        $services_ids     = AdmissionsLegacyController::cleanArrayValues($services_ids);
        $sejour_type_pref = AdmissionsLegacyController::getSejourTypesPreference($sejours_ids);

        if (count($sejour_type_pref) === 1) {
            $type = $sejour_type_pref[0];
        }

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_SORTIES
        );
        $repository->setCancelledFilter(0);

        if (count($sejour_type_pref)) {
            $repository->setSejourTypeListFilter($sejour_type_pref);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        if (count($services_ids) && $enabled_service) {
            $repository->setServicesListFilter($services_ids);
        }

        if ($period) {
            $hour = CAppUI::gconf('dPadmissions General hour_matin_soir');
            $repository->setPeriodFilter($period, $hour);
        }

        $sejours = $repository->loadSejours($date, 'praticien_id', 'ASC');

        CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadFwdRef($sejours, 'prestation_id');
        $affectations = CStoredObject::massLoadBackRefs($sejours, 'affectations', 'sortie DESC');
        CAffectation::massUpdateView($affectations);
        CSejour::massLoadNDA($sejours);

        $listByPrat = [];
        foreach ($sejours as $sejour) {
            $sejour->loadRefPraticien();
            $sejour->loadRefsAffectations();
            $sejour->loadRefPatient();
            $sejour->loadRefPrestation();

            $curr_prat = $sejour->praticien_id;
            if (!isset($listByPrat[$curr_prat])) {
                $listByPrat[$curr_prat]['praticien'] = $sejour->_ref_praticien;
            }
            $listByPrat[$curr_prat]['sejours'][] = $sejour;
        }

        $this->renderSmarty('sorties_print', [
            'date'       => $date->format('Y-m-d'),
            'type'       => $type,
            'listByPrat' => $listByPrat,
            'total'      => count($sejours),
        ]);
    }

    /**
     * Displays a printable view listing the sejours of type ambu
     *
     * @return void
     * @throws Exception
     */
    public function printSortiesAmbu(): void
    {
        $this->checkPermRead();

        $date       = new DateTimeImmutable(CView::get('date', 'date default|now'));
        $type       = CView::get('type', 'str default|ambu');
        $service_id = CView::get('services_id', 'ref class|CService', true);

        CView::checkin();
        CView::enforceSlave();

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_SORTIES_SCHEDULED
        );

        $repository->setCancelledFilter(0);
        $repository->setSejourTypeFilter($type);

        $sejours = $repository->loadSejours($date, 'service');

        CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadFwdRef($sejours, 'praticien_id');

        $affectations = CStoredObject::massLoadBackRefs($sejours, 'affectations', 'sortie ASC');
        CAffectation::massUpdateView($affectations);
        CStoredObject::massLoadBackRefs($sejours, 'operations', 'date ASC');

        foreach ($sejours as $sejour) {
            $sejour->loadRefPatient();
            $sejour->loadRefPraticien();
            $sejour->loadRefsAffectations('sortie ASC');
            $sejour->loadRefsOperations();
            $sejour->_duree = CMbDT::subTime(
                CMbDT::time($sejour->entree_reelle),
                CMbDT::time($sejour->sortie_reelle)
            );

            $sejour->_ref_last_operation->loadRefSortieLocker()->loadRefFunction();
        }

        $this->renderSmarty('sorties_ambu_print', [
            'service_id' => $service_id,
            'sejours'    => $sejours,
            'services'   => $repository->loadServicesList(),
            'date'       => $date->format('Y-m-d'),
            'type'       => $type,
        ]);
    }

    /**
     * Massload the references and make all the preparations necessary for the template
     *
     * @param CSejour[] $sejours
     *
     * @return void
     * @throws Exception
     */
    private function prepareSejoursList(array $sejours): void
    {
        $group = CGroups::getCurrent();

        $patients = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadFwdRef($sejours, 'etablissement_sortie_id');
        CStoredObject::massLoadFwdRef($sejours, 'service_sortie_id');
        $praticiens = CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($praticiens, 'function_id');
        $affectations = CStoredObject::massLoadBackRefs($sejours, 'affectations', 'sortie DESC');
        CAffectation::massUpdateView($affectations);

        // Chargement optimis�e des prestations
        CSejour::massLoadPrestationRealisees($sejours);

        CStoredObject::massLoadBackRefs($sejours, 'notes');
        CStoredObject::massLoadBackRefs($patients, 'dossier_medical');
        CStoredObject::massLoadBackRefs($patients, 'bmr_bhre');

        $operations = CStoredObject::massLoadBackRefs($sejours, 'operations', 'date ASC', ['annulee' => "= '0'"]);
        CStoredObject::massLoadFwdRef($operations, 'plageop_id');
        CStoredObject::massLoadBackRefs($operations, 'actes_ngap', 'lettre_cle DESC');

        $order = 'code_association, code_acte,code_activite, code_phase, acte_id';
        CStoredObject::massLoadBackRefs($operations, 'actes_ccam', $order);

        if (CModule::getActive('maternite')) {
            $parent_affectations = CStoredObject::massLoadFwdRef($affectations, 'parent_affectation_id');
            CStoredObject::massLoadFwdRef($parent_affectations, 'sejour_id');
        }

        // Chargement des NDA
        CSejour::massLoadNDA($sejours);
        // Chargement des IPP
        CPatient::massLoadIPP($patients);

        $maternite_active = CModule::getActive('maternite');

        if (CModule::getActive('appFineClient')) {
            CAppFineClient::massloadIdex($sejours, $group->_id);
            CAppFineClient::massloadIdex($patients, $group->_id);
        }

        foreach ($sejours as $sejour) {
            // Filtre sur la fonction du praticien
            $sejour->loadRefPraticien(1);

            // Chargement du patient
            $sejour->loadRefPatient()->updateBMRBHReStatus($sejour);

            if (CModule::getActive('appFineClient')) {
                $sejour->_ref_patient->loadRefStatusPatientUser();
            }

            // Chargements des notes sur le s�jour
            $sejour->loadRefsNotes();

            // Chargement des interventions
            $sejour->loadRefsOperations(['annulee' => "= '0'"]);

            foreach ($sejour->_ref_operations as $operation) {
                $operation->loadRefsActes();
                $operation->loadRefPlageOp();
            }

            // Chargement des affectation
            $sejour->loadRefsAffectations();

            if ($maternite_active) {
                $sejour->loadRefFirstAffectation()->loadRefParentAffectation()->loadRefSejour();
                $sejour->_sejours_enfants_ids = CMbArray::pluck($sejour->loadRefsNaissances(), 'sejour_enfant_id');
            }

            // Chargement des modes de sortie
            $sejour->loadRefEtablissementTransfert();
            $sejour->loadRefServiceMutation();
            // Chargement des appels
            $sejour->loadRefsAppel('sortie');
        }
    }

    /**
     * Displays the view 'Sorties en masse'
     *
     * @return void
     * @throws Exception
     */
    public function viewMassSorties(): void
    {
        $this->checkPermRead();

        $date = CView::get('_date_entree', 'date default|now', true);

        CView::checkin();

        $filter               = new CSejour();
        $filter->_date_entree = $date;

        $this->renderSmarty('mass_sorties_view', [
            'filter' => $filter,
        ]);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function massSortiesList(): void
    {
        $this->checkPermRead();

        $date = new DateTimeImmutable(CView::get('_date_entree', 'date default|now', true));

        CView::checkin();
        CView::enforceSlave();

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_REAL_ADMISSIONS
        );

        $repository->setCancelledFilter(0);
        $repository->setWithoutRealLeaveDateFilter(true);
        $repository->setSejourTypeListFilter(['exte', 'seances']);

        $sejours = $repository->loadSejours($date);

        CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($sejours, 'mode_sortie_id');
        $affectations = CStoredObject::massLoadBackRefs($sejours, 'affectations');
        CAffectation::massUpdateView($affectations);
        foreach ($sejours as $sejour) {
            $sejour->loadRefPatient();
            $sejour->loadRefPraticien();
            $sejour->loadRefModeSortie();
            $sejour->loadRefsAffectations();
        }

        $filter               = new CSejour();
        $filter->_date_entree = $date->format('Y-m-d');

        $this->renderSmarty('mass_sorties_list', [
            'filter'        => $filter,
            'sejours'       => $sejours,
            'modes_sorties' => CModeSortieSejour::listModeSortie(),
        ]);
    }

    /**
     * Displays the view for editing the exit of a patient for the specified sejour
     *
     * @return void
     * @throws Exception
     */
    public function editSortieSejour(): void
    {
        $this->checkPerm();

        $sejour_id            = CView::get('sejour_id', 'ref class|CSejour');
        $module               = CView::get('module', 'str');
        $callback             = CView::get('callback', 'str');
        $modify_sortie_prevue = CView::get('modify_sortie_prevue', 'bool default|1');

        CView::checkin();

        $sejour = new CSejour();
        $sejour->load($sejour_id);

        $can_admission = CModule::getCanDo('dPadmissions');

        if (
            !$sejour->canDo()->edit && !$can_admission->edit
            && !CModule::getCanDo('dPhospi')->edit
            && !CModule::getCanDo('dPurgences')->edit
            && !CModule::getCanDo('soins')->edit
        ) {
            $can_admission->denied();
        }

        $sejour->loadRefServiceMutation();
        $sejour->loadRefEtablissementTransfert();
        $sejour->loadRefsOperations();

        // Cas des urgences
        if (CModule::getActive('dPurgences')) {
            $sejour->loadRefRPU()->loadRefSejourMutation();
        }

        $sejour->loadRefPatient()->loadIPP();

        if (CModule::getActive('maternite')) {
            if (!$sejour->_ref_patient->checkAnonymous()) {
                $sejour->loadRefsNaissances();
                foreach ($sejour->_ref_naissances as $_naissance) {
                    $_naissance->loadRefSejourEnfant()->loadRefPatient();
                }

                $sejour->_sejours_enfants_ids = CMbArray::pluck($sejour->_ref_naissances, 'sejour_enfant_id');
            }

            $sejour_mere = $sejour->loadRefNaissance()->loadRefSejourMaman();
            if ($sejour_mere->_id) {
                $sejour_mere->loadRefPatient();
                if ($sejour_mere->_ref_patient->checkAnonymous()) {
                    $sejour_mere = new CSejour();
                } else {
                    $sejour_mere->loadRefsNaissances();
                    foreach ($sejour_mere->_ref_naissances as $_naissance_mere) {
                        if ($_naissance_mere->sejour_enfant_id === $sejour->_id) {
                            unset($sejour_mere->_ref_naissances[$_naissance_mere->_id]);
                            continue;
                        }
                        $_naissance_mere->loadRefSejourEnfant()->loadRefPatient();
                    }
                    $sejour_mere->_sejours_enfants_ids = CMbArray::pluck(
                        $sejour_mere->_ref_naissances,
                        'sejour_enfant_id'
                    );
                }
            }
        } else {
            $sejour_mere = new CSejour();
        }

        $this->renderSmarty('sortie_edit', [
            'callback'             => stripslashes($callback),
            'modify_sortie_prevue' => $modify_sortie_prevue,
            'sejour'               => $sejour,
            'module'               => $module,
            'list_mode_sortie'     => CModeSortieSejour::listModeSortie(),
            'sejour_mere'          => $sejour_mere,
        ]);
    }

    /**
     * Displays the part of the editSortie view that shows the list of beds in a case of a mutation of sejour
     *
     * @return void
     * @throws Exception
     */
    public function reloadBedMutation(): void
    {
        $this->checkPermEdit();

        $sejour_id     = CView::get('sejour_id', 'ref class|CSejour');
        $sortie_reelle = CView::get('sortie_reelle', 'dateTime');

        CView::checkin();
        CView::enforceSlave();

        $sejour = CSejour::findOrNew($sejour_id);
        $sejour->loadRefPatient()->loadIPP();
        $sejour->loadNDA();
        $sejour->loadRefsConsultations();
        $sejour->_ref_patient->loadRefsAffectations();
        $sejour->sortie_reelle = $sortie_reelle;

        if (!$sejour->sortie_reelle) {
            $sejour->sortie_reelle = CMbDT::dateTime();
        }

        $service  = new CService();
        $services = $service->loadGroupList();

        $request = new CRequest();
        $request->addLJoinClause('chambre', '`chambre`.`chambre_id` = `lit`.`chambre_id`');
        $request->addLJoin('affectation FORCE INDEX (sortie) ON `affectation`.`lit_id` = `lit`.`lit_id`');

        $request->addWhere('`affectation`.`service_id` ' . CSQLDataSource::prepareIn(array_keys($services)));
        $request->addWhereClause('chambre.annule', " = '0'");
        $request->addWhereClause('lit.annule', " = '0'");
        $request->addWhere("`affectation`.`entree` <= '{$sejour->sortie_reelle}'");
        $request->addWhere("`affectation`.`sortie` >= '{$sejour->sortie_reelle}'");
        $request->addWhereClause('affectation.function_id', 'IS NOT NULL');
        $request->addWhereClause('affectation.sejour_id', 'IS NULL');

        /* R�cup�ration des lits associ� a un blocage d'urgence (function_id IS NOT NULL) */
        $emergency_bed = (new CLit())->loadListByReq($request);

        $request->addWhereClause('affectation.function_id', 'IS NULL');
        $request->addWhereClause('lit.lit_id', CSQLDataSource::prepareIn(array_keys($emergency_bed)));

        /* R�cup�ration des lits bloqu� (associ�s � une affectation sans sejour et sans fonction) */
        $blocked_bed = (new CLit())->loadListByReq($request);

        if ($sejour->_ref_patient->_ref_curr_affectation->lit_id) {
            $lit                      = $sejour->_ref_patient->_ref_curr_affectation->loadRefLit();
            $emergency_bed[$lit->_id] = $lit;
        }

        unset($request->where['lit.lit_id']);
        unset($request->where['affectation.sejour_id']);
        unset($request->where['chambre.annule']);
        unset($request->where['lit.annule']);

        /** @var CLit $bed */
        foreach ($emergency_bed as $bed) {
            $bed->loadRefService()->loadRefsChambres();
            if (array_key_exists($bed->_id, $blocked_bed)) {
                $bed->_view .= ' (bloqu�)';
                continue;
            }

            $affectation = new CAffectation();
            $request->addWhereClause('lit.lit_id', " = {$bed->_id}");
            if (!$affectation->loadObject($request->where)) {
                continue;
            }

            $patient = $affectation->loadRefSejour()->loadRefPatient();

            if ($patient->_id) {
                $bed->_view .= " ({$patient->_view} (" . strtoupper($patient->sexe) . '))';
            }
        }

        $this->renderSmarty('sortie_form_lit', [
            'sejour' => $sejour,
            'lits'   => $emergency_bed,
        ]);
    }

    /**
     * Clean the empty values from the given array, and empty it if it contains the 'all' value
     *
     * @param array $prestation_ids
     *
     * @return void
     */
    protected function cleanPrestationIds(array &$prestation_ids): void
    {
        $prestation_ids = AdmissionsLegacyController::cleanArrayValues($prestation_ids);
        if (in_array('all', $prestation_ids)) {
            $prestation_ids = [];
        }
    }

    /**
     * @param array                $types
     * @param AdmissionsRepository $repository
     *
     * @return void
     */
    protected function setSejourTypeFilter(array $types, AdmissionsRepository $repository): void
    {
        if (count($types)) {
            $repository->setSejourTypeListFilter($types);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }
    }

    /**
     * @param array $types
     *
     * @return string
     */
    protected function getTypeFromArray(array $types): string
    {
        $type = '';
        if (in_array('ambu', $types)) {
            $type = 'ambu';
        } elseif (in_array('exte', $types)) {
            $type = 'exte';
        }

        return $type;
    }

    /**
     * Clean the empty values from the given array, and empty it if it contains the 'all' value
     *
     * @param array $array
     *
     * @return void
     */
    protected function cleanArrayParameter(array &$array): void
    {
        $array = AdmissionsLegacyController::cleanArrayValues($array);
        if (in_array('all', $array)) {
            $array = [];
        }
    }
}
