<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\AppFine\Client\CAppFineClient;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Cabinet\CConsultAnesth;
use Ox\Mediboard\ESatis\CEsatisConsent;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Hospi\CPrestation;
use Ox\Mediboard\Hospi\CPrestationPonctuelle;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Hospi\CUniteFonctionnelle;
use Ox\Mediboard\Maternite\CNaissance;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Notifications\CNotification;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CModeEntreeSejour;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\SmsProviders\CAbstractMessage;

/**
 * The main controller for the Admissions module
 */
class AdmissionsLegacyController extends CLegacyController
{
    /**
     * The main view for the admissions
     *
     * @return void
     * @throws Exception
     */
    public function viewAdmissions(): void
    {
        $this->checkPermRead();

        $sejour = new CSejour();

        $selAdmis              = CView::get('selAdmis', 'bool default|0', true);
        $selSaisis             = CView::get('selSaisis', 'bool default|0', true);
        $order_column          = CView::get('order_col', 'enum list|patient_id', true);
        $order_way             = CView::get('order_way', 'enum list|ASC|DESC default|ASC', true);
        $date                  = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $prat_id               = CView::get('prat_id', 'ref class|CMediusers', true);
        $period                = CView::get('period', 'enum list|soir|matin', true);
        $filterFunction        = CView::get('filterFunction', 'ref class|CFunctions', true);
        $enabled_service       = CView::get('active_filter_services', 'bool default|0', true);
        $type_pec              = CView::get('type_pec', ['str', 'default' => $sejour->_specs['type_pec']->_list], true);
        $date_interv_eg_entree = CView::get('date_interv_eg_entree', 'bool default|0', true);
        $circuits_ambu         = CView::get(
            'circuit_ambu',
            ['str', 'default' => $sejour->_specs['circuit_ambu']->_list],
            true
        );
        $prestations_p_ids     = CView::get('prestations_ids', 'str', true);
        $sejours_ids           = CSejour::getTypeSejourIdsPref(CView::get('sejours_ids', 'str', true));
        CService::getServicesIdsPref(CView::get('services_ids', 'str', true));

        CView::checkin();
        CView::enforceSlave();

        $prat = CMediusers::get();

        $sejour               = new CSejour();
        $sejour->praticien_id = $prat_id;

        $this->renderSmarty('admissions_view', [
            'sejour'                   => $sejour,
            'date_demain'              => $date->modify('+1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle'            => $date->format('Y-m-d 00:00:00'),
            'date'                     => $date->format('Y-m-d'),
            'selAdmis'                 => $selAdmis,
            'selSaisis'                => $selSaisis,
            'order_way'                => $order_way,
            'order_col'                => $order_column,
            'prats'                    => $prat->loadPraticiens(),
            'hier'                     => $date->modify('-1 day')->format('Y-m-d'),
            'demain'                   => $date->modify('+1 day')->format('Y-m-d'),
            'period'                   => $period,
            'filterFunction'           => $filterFunction,
            'list_type_ad'             => $sejour->_specs['_type_admission']->_list,
            'enabled_service'          => $enabled_service,
            'type_pec'                 => $type_pec,
            'date_interv_eg_entree'    => $date_interv_eg_entree,
            'circuits_ambu'            => $circuits_ambu,
            'prestations_ponctuelles'  => CPrestationPonctuelle::loadCurrentList(),
            'prestations_p_ids'        => $prestations_p_ids,
            'notification_status_list' => CAbstractMessage::$status_list,
            'sejours_ids'              => $sejours_ids,
        ]);
    }

    /**
     * Display the monthly counters of the admissions
     *
     * @return void
     * @throws Exception
     */
    public function admissionsCounters(): void
    {
        $this->checkPermRead();

        $sejour = new CSejour();

        $selAdmis              = CView::get('selAdmis', 'bool default|0', true);
        $selSaisis             = CView::get('selSaisis', 'bool default|0', true);
        $date                  = CView::get('date', 'date default|now', true);
        $services_ids          = CView::get('services_ids', 'str', true);
        $sejours_ids           = CView::get('sejours_ids', 'str', true);
        $prat_id               = CView::get('prat_id', 'ref class|CMediusers', true);
        $enabled_service       = CView::get('active_filter_services', 'bool default|0', true);
        $type_pec              = CView::get('type_pec', ['str', 'default' => $sejour->_specs['type_pec']->_list], true);
        $date_interv_eg_entree = CView::get('date_interv_eg_entree', 'bool default|0', true);
        $circuits_ambu         = CView::get(
            'circuit_ambu',
            ['str', 'default' => $sejour->_specs['circuit_ambu']->_list],
            true
        );
        $reglement_dh          = CView::get('reglement_dh', 'enum list|all|payed|not_payed default|all');

        CView::checkin();
        CView::enforceSlave();

        $circuits_ambu = self::cleanArrayValues($circuits_ambu);
        $services_ids  = self::cleanArrayValues($services_ids);
        $sejours_ids   = self::cleanArrayValues($sejours_ids);

        $sejour_type_pref = self::getSejourTypesPreference($sejours_ids);

        $date = new DateTimeImmutable($date);

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS
        );

        /* Get the list of days in the month, with the counters set to 0 */
        $days = $repository->getMonthlyCounters(
            $date,
            ['admissions', 'admissions_non_effectuee', 'admissions_non_preparee']
        );

        /* Sets the filters for counting the sejours */
        $repository->setCancelledFilter(0);

        if (count($services_ids) && $enabled_service) {
            $repository->setServicesListFilter($services_ids);
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        if (!(in_array('', $type_pec) && count($type_pec) == 1)) {
            $repository->setTypePecFilter($type_pec);
        }

        if (count($sejour_type_pref)) {
            $repository->setSejourTypeListFilter($sejour_type_pref);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        if (CAppUI::gconf('dPplanningOp CSejour show_circuit_ambu') && count($circuits_ambu)) {
            $repository->setCircuitAmbuFilter($circuits_ambu);
        }

        if ($date_interv_eg_entree) {
            $repository->setDateOperationEqualsToEntryFilter();
        }

        if ($reglement_dh) {
            $repository->setPaymentExceedingFeesFilter($reglement_dh);
        }

        /* Get the number of admissions per day of the month */
        $admissions = $repository->countSejoursPerDay($date);

        /* Get the number of unprepared admissions per day of the month */
        $repository->setUnpreparedSejoursFilter(true);
        $unprepared = $repository->countSejoursPerDay($date);

        /* Get the number of scheduled admissions per day of the month */
        $repository->setUnpreparedSejoursFilter(false);
        $repository->setScheduledSejoursFilter(true);
        $scheduled = $repository->countSejoursPerDay($date);

        $totaux = [
            'admissions'               => 0,
            'admissions_non_effectuee' => 0,
            'admissions_non_preparee'  => 0,
        ];

        /* Set the counter foreach day */
        foreach ($days as $day => $data) {
            if (array_key_exists($day, $admissions)) {
                $days[$day]['admissions'] = $admissions[$day];
                $totaux['admissions']     += $admissions[$day];
            }
            if (array_key_exists($day, $unprepared)) {
                $days[$day]['admissions_non_preparee'] = $unprepared[$day];
                $totaux['admissions_non_preparee']     += $unprepared[$day];
            }
            if (array_key_exists($day, $scheduled)) {
                $days[$day]['admissions_non_effectuee'] = $scheduled[$day];
                $totaux['admissions_non_effectuee']     += $scheduled[$day];
            }
        }

        $this->renderSmarty('admissions_counters', [
            'hier'            => $date->modify('-1 day')->format('Y-m-d'),
            'demain'          => $date->modify('+1 day')->format('Y-m-d'),
            'selAdmis'        => $selAdmis,
            'selSaisis'       => $selSaisis,
            'bank_holidays'   => CMbDT::getHolidays($date->format('Y-m-d')),
            'date'            => $date->format('Y-m-d'),
            'lastmonth'       => $date->modify('last day of -1 month')->format('Y-m-d'),
            'nextmonth'       => $date->modify('last day of 1 month')->format('Y-m-d'),
            'days'            => $days,
            'totaux'          => $totaux,
            'enabled_service' => $enabled_service,
        ]);
    }

    /**
     * Display the list of admissions for the selected date, that matches the filters
     *
     * @return void
     * @throws Exception
     */
    public function admissionsList(): void
    {
        $this->checkPermRead();

        $sejour = new CSejour();

        $services_ids          = CView::get('services_ids', 'str', true);
        $sejours_ids           = CView::get('sejours_ids', 'str', true);
        $enabled_service       = CView::get('active_filter_services', 'bool default|0', true);
        $prat_id               = CView::get('prat_id', 'ref class|CMediusers', true);
        $prestations_p_ids     = CView::get('prestations_p_ids', ['str', 'default' => []], true);
        $selAdmis              = CView::get('selAdmis', 'str default|0', true);
        $selSaisis             = CView::get('selSaisis', 'str default|0', true);
        $order_col             = CView::get('order_col', 'str default|patient_id', true);
        $order_way             = CView::get('order_way', 'str default|ASC', true);
        $date                  = CView::get('date', 'date default|now', true);
        $filterFunction        = CView::get('filterFunction', 'ref class|CFunctions', true);
        $period                = CView::get('period', 'str', true);
        $type_pec              = CView::get('type_pec', ['str', 'default' => $sejour->_specs['type_pec']->_list], true);
        $print_global          = CView::get('print_global', 'bool default|0');
        $date_interv_eg_entree = CView::get('date_interv_eg_entree', 'bool default|0', true);
        $reglement_dh          = CView::get('reglement_dh', 'enum list|all|payed|not_payed');
        $circuits_ambu         = CView::get(
            'circuit_ambu',
            ['str', 'default' => $sejour->_specs['circuit_ambu']->_list],
            true
        );
        $notification_status   = CView::get(
            'notification_status',
            ['enum', 'list' => implode('|', CAbstractMessage::$status_list)],
            true
        );
        $step                  = CView::get(
            'step',
            'num default|' . CAppUI::gconf('dPadmissions General pagination_step')
        );
        $page                  = CView::get('page', 'num default|0');

        CView::checkin();
        CView::enforceSlave();

        $this->cleanPrestationsIds($prestations_p_ids);
        $circuits_ambu = self::cleanArrayValues($circuits_ambu);
        $services_ids  = self::cleanArrayValues($services_ids);
        $sejours_ids   = self::cleanArrayValues($sejours_ids);
        $type_pec = $this->cleanTypePecParameter($type_pec);

        $sejour_type_pref = self::getSejourTypesPreference($sejours_ids);

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS
        );

        $repository->setCancelledFilter(0);

        if (count($services_ids) && $enabled_service) {
            $repository->setServicesListFilter($services_ids);
        }

        $this->setSejourTypeFilter($sejour_type_pref, $repository);

        if (CAppUI::gconf('dPplanningOp CSejour show_circuit_ambu') && count($circuits_ambu)) {
            $repository->setCircuitAmbuFilter($circuits_ambu);
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        if ($date_interv_eg_entree) {
            $repository->setDateOperationEqualsToEntryFilter();
        }

        if ($reglement_dh) {
            $repository->setPaymentExceedingFeesFilter($reglement_dh);
        }

        if (!(in_array('', $type_pec) && count($type_pec) == 1)) {
            $repository->setTypePecFilter($type_pec);
        }

        if ($filterFunction) {
            $repository->setFunctionFilter(CFunctions::findOrNew($filterFunction));
        }

        if (count($prestations_p_ids)) {
            $repository->setPrestationsFilter($prestations_p_ids);
        }

        if ($selAdmis) {
            $repository->setScheduledSejoursFilter(true);
        }

        if ($selSaisis) {
            $repository->setUnpreparedSejoursFilter(true);
        }

        if ($period) {
            $hour = CAppUI::gconf('dPadmissions General hour_matin_soir');
            $repository->setPeriodFilter($period, $hour);
        }

        if ($notification_status) {
            $repository->setNotificationStatusFilter($notification_status);
        }

        if ($print_global) {
            $page = null;
            $step = null;
        }

        $date = new DateTimeImmutable($date);

        $sejours = CAppUI::gconf('dPadmissions General use_perms')
            ? $repository->loadSejoursByPerms(PERM_READ, $date, $order_col, $order_way, $page, $step)
            : $repository->loadSejours($date, $order_col, $order_way, $page, $step);

        $total = $repository->countSejours($date);

        $functions = $repository->loadFunctionsByAdmissions($date);

        $this->prepareSejours($sejours);

        $this->renderSmarty('admissions_list', [
            'flag_dmp'              => AdmissionsRepository::flagDMP(),
            'flag_contextual_icons' => AdmissionsRepository::flagContextualIcons(),
            'hier'                  => $date->modify('- 1 day')->format('Y-m-d'),
            'demain'                => $date->modify('+ 1 day')->format('Y-m-d'),
            'date_min'              => $repository->getDateMin($date),
            'date_max'              => $repository->getDateMax($date),
            'date_demain'           => $date->modify('- 1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle'         => $date->format('Y-m-d 00:00:00'),
            'date'                  => $date->format('Y-m-d'),
            'selAdmis'              => $selAdmis,
            'selSaisis'             => $selSaisis,
            'order_col'             => $order_col,
            'order_way'             => $order_way,
            'sejours'               => $sejours,
            'prestations'           => CPrestation::loadCurrentList(),
            'canAdmissions'         => CModule::getCanDo('dPadmissions'),
            'canPatients'           => CModule::getCanDo('dPpatients'),
            'canPlanningOp'         => CModule::getCanDo('dPplanningOp'),
            'functions'             => $functions,
            'filterFunction'        => $filterFunction,
            'period'                => $period,
            'list_mode_entree'      => $this->getListModesEntree(),
            'enabled_service'       => $enabled_service,
            'print_global'          => $print_global,
            'circuits_ambu'         => $circuits_ambu,
            'total'                 => $total,
            'step'                  => $step,
            'page'                  => $page,
        ]);
    }

    /**
     * Displays the line for an admission in the list
     *
     * @return void
     * @throws Exception
     */
    public function admissionLine(): void
    {
        $this->checkPermRead();

        $sejour_id    = CView::get('sejour_id', 'ref class|CSejour');
        $date         = CView::get('date', 'date', true);
        $print_global = CView::get('print_global', 'bool default|0');
        $reloadLine   = CView::get('reloadLine', 'bool default|0');

        CView::checkin();
        CView::enforceSlave();

        $sejour = CSejour::findOrNew($sejour_id);

        $sejour->loadRefEtablissementProvenance();
        $sejour->loadRefAdresseParPraticien();
        $sejour->loadRefPraticien()->loadRefFunction();
        $sejour->getPassageBloc();
        $patient = $sejour->loadRefPatient();

        $patient->loadIPP();
        $patient->countINS();
        $patient->loadStateDMP();
        $patient->loadRefsPatientHandicaps();

        // Dossier m�dical
        $dossier_medical = $patient->loadRefDossierMedical(false);

        // Chargement du num�ro de dossier
        $sejour->loadNDA();

        // Chargement des notes sur le s�jourw
        $sejour->loadRefsNotes();

        // Chargement des modes d'entr�e
        $sejour->loadRefEtablissementProvenance();

        // Chargement des appels
        $sejour->loadRefsAppel('admission');

        // Chargement de l'affectation
        $affectation = $sejour->loadRefFirstAffectation();
        $affectation->updateView();
        if (CModule::getActive('maternite')) {
            $affectation->loadRefParentAffectation()->loadRefSejour()->loadRefPatient();
        }

        // Chargement des interventions
        $whereOperations = ['annulee' => "= '0'"];
        $operations      = $sejour->loadRefsOperations($whereOperations);

        // Chargement optimis�e des prestations
        CSejour::massCountPrestationSouhaitees([$sejour]);
        CSejour::massLoadPrestationSouhaitees(["$sejour_id" => $sejour]);

        foreach ($operations as $operation) {
            $operation->loadRefsActes();
            $dossier_anesth = $operation->loadRefsConsultAnesth();
            $consultation   = $dossier_anesth->loadRefConsultation();
            $consultation->loadRefPlageConsult();
            $consultation->loadRefPraticien()->loadRefFunction();
            $dossier_anesth->_date_consult = $consultation->_date;
            $operation->loadRefPlageOp();
        }

        if (CAppUI::gconf('dPadmissions General show_deficience')) {
            CDossierMedical::massCountAntecedentsByType([$dossier_medical], 'deficience');
        }

        $list_mode_entree = [];
        if (CAppUI::conf('dPplanningOp CSejour use_custom_mode_entree')) {
            $mode_entree      = new CModeEntreeSejour();
            $where            = [
                'actif' => "= '1'",
            ];
            $list_mode_entree = $mode_entree->loadGroupList($where);
        }

        if (CModule::getActive('appFineClient')) {
            CAppFineClient::loadIdex($sejour->_ref_patient, $sejour->group_id);
            CAppFineClient::loadIdex($sejour, $sejour->group_id);
            $sejour->_ref_patient->loadRefStatusPatientUser();
            $sejour->loadRefFolderLiaison('pread');
        }

        $date = new DateTimeImmutable($date);

        $this->renderSmarty('admissions_line', [
            '_sejour'               => $sejour,
            'flag_dmp'              => AdmissionsRepository::flagDMP(),
            'flag_contextual_icons' => AdmissionsRepository::flagContextualIcons(),
            'print_global'          => $print_global,
            'date_min'              => $date->format('Y-m-d 00:00:00'),
            'date_max'              => $date->format('Y-m-d 23:59:59'),
            'date_actuelle'         => $date->format('Y-m-d 00:00:00'),
            'date_demain'           => $date->modify('+1 day')->format('Y-m-d 00:00:00'),
            'list_mode_entree'      => $list_mode_entree,
            'prestations'           => CPrestation::loadCurrentList(),
            'canAdmissions'         => CModule::getCanDo('dPadmissions'),
            'canPatients'           => CModule::getCanDo('dPpatients'),
            'canPlanningOp'         => CModule::getCanDo('dPplanningOp'),
            'single_line'           => true, // Indique qu'on charge la ligne seulement
            'reloadLine'            => $reloadLine,
        ]);
    }

    /**
     * Displays the print view for the admissions
     *
     * @return void
     * @throws Exception
     */
    public function printAdmissions(): void
    {
        $this->checkPermRead();

        $date            = new DateTimeImmutable(CView::get('date', 'date default|now'));
        $type            = CView::get('type', 'str');
        $services_ids    = CView::get('services_ids', 'str', true);
        $sejour_ids      = CView::get('sejours_ids', 'str', true);
        $period          = CView::get('period', 'enum list|matin|soir');
        $group_by        = CView::get('group_by_prat', 'bool default|1');
        $order_by        = CView::get('order_by', 'str default|entree_prevue');
        $enabled_service = CView::get('active_filter_services', 'bool default|0', true);

        CView::checkin();
        CView::enforceSlave();

        $services_ids     = self::cleanArrayValues($services_ids);
        $sejour_type_pref = self::getSejourTypesPreference($sejour_ids);

        if (count($sejour_type_pref) === 1) {
            $type = $sejour_type_pref[0];
        }

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS
        );

        $repository->setCancelledFilter(0);

        if ($period) {
            $repository->setPeriodFilter($period, CAppUI::gconf('dPadmissions General hour_matin_soir'));
        }

        if (count($sejour_type_pref)) {
            $repository->setSejourTypeListFilter($sejour_type_pref);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        if ($enabled_service && count($services_ids)) {
            $repository->setServicesListFilter($services_ids);
        }

        $sejours = $repository->loadSejours($date, $order_by);

        CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadFwdRef($sejours, 'prestation_id');
        $affectations = CStoredObject::massLoadBackRefs($sejours, 'affectations', 'sortie DESC');
        CAffectation::massUpdateView($affectations);
        CSejour::massLoadPrestationSouhaitees($sejours);
        CSejour::massLoadNDA($sejours);

        $list_by_users = [];
        foreach ($sejours as $sejour) {
            $sejour->loadRefPraticien();
            $sejour->loadRefsAffectations();
            $sejour->loadRefPatient();
            $sejour->loadRefPrestation();
            $sejour->_ref_first_affectation->loadRefLit();
            $sejour->_ref_first_affectation->_ref_lit->loadCompleteView();

            if (!array_key_exists($sejour->praticien_id, $list_by_users)) {
                $list_by_users[$sejour->praticien_id] = [
                    'praticien' => $sejour->_ref_praticien,
                    'sejours'   => [],
                ];
            }

            $list_by_users[$sejour->praticien_id]['sejours'][$sejour->_id] = $sejour;
        }

        $this->renderSmarty('admissions_print', [
            'sejours'    => $sejours,
            'date'       => $date->format('Y-m-d'),
            'type'       => $type,
            'listByPrat' => $list_by_users,
            'group_by'   => $group_by,
            'total'      => count($sejours),
        ]);
    }

    /**
     * Displays a printable view containing all the sejours linked to operations with exceeding fees,
     * by the anaesthetist or the surgeon.
     *
     * @return void
     * @throws Exception
     */
    public function printExceedingFeesRecovery(): void
    {
        $this->checkPermRead();

        $date           = new DateTimeImmutable(CView::get('date', 'date default|now'));
        $type           = CView::get('type', 'str');
        $services_ids   = CView::get('services_ids', 'str', true);
        $filter_service = CView::get('active_filter_services', 'bool default|0', true);
        $prat_id        = CView::get('prat_id', 'ref class|CMediusers');
        $period         = CView::get('period', 'enum list|matin|soir');
        $type_pec       = CView::get('type_pec', ['str', 'default' => (new CSejour())->_specs['type_pec']->_list]);
        $filterFunction = CView::get('filterFunction', 'ref class|CFunctions');
        $view           = CView::get('view', 'str default|inputs');
        $reglement_dh   = CView::get('reglement_dh', 'enum list|all|payed|not_payed default|all');

        CView::checkin();
        CView::enforceSlave();

        if (!is_array($services_ids)) {
            $services_ids = explode('|', $services_ids);
        }

        $services_ids = self::cleanArrayValues($services_ids);

        if (!is_array($type_pec)) {
            $type_pec = explode('|', $type_pec);
        }

        $type_pec = self::cleanArrayValues($type_pec);

        $view = $view === 'inputs' ? AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS
            : AdmissionsRepository::SEJOUR_VIEW_SORTIES;

        $repository = new AdmissionsRepository(CGroups::loadCurrent(), CMediusers::get(), $view);
        $repository->setCancelledFilter(0);

        if ($period) {
            $hour = CAppUI::gconf('dPadmissions General hour_matin_soir');
            $repository->setPeriodFilter($period, $hour);
        }

        if (count($services_ids) && $filter_service) {
            $repository->setServicesListFilter($services_ids);
        }

        if ($type == 'ambucomp') {
            $repository->setSejourTypeListFilter(['ambu', 'comp']);
        } elseif ($type == 'ambucompssr') {
            $repository->setServicesListFilter(['comp', 'comp', 'ssr']);
        } elseif ($type && $type !== 'tous') {
            $repository->setSejourTypeFilter($type);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        if (count($type_pec)) {
            $repository->setTypePecFilter($type_pec);
        }

        if ($filterFunction) {
            $repository->setFunctionFilter(CFunctions::findOrNew($filterFunction));
        }

        if ($reglement_dh) {
            $repository->setPaymentExceedingFeesFilter($reglement_dh);
        }

        $sejours = CAppUI::gconf('dPadmissions General use_perms')
            ? $repository->loadSejoursByPerms(PERM_READ, $date, 'praticien_id')
            : $repository->loadSejours($date, 'praticien_id');

        /** @var COperation[] $operations */
        $operations = CStoredObject::massLoadBackRefs($sejours, 'operations');
        $plages     = CStoredObject::massLoadFwdRef($operations, 'plageop_id');
        $praticiens = CStoredObject::massLoadFwdRef($operations, 'chir_id');
        $praticiens = array_merge(CStoredObject::massLoadFwdRef($operations, 'anesth_id'), $praticiens);
        $praticiens = array_merge(CStoredObject::massLoadFwdRef($plages, 'anesth_id'), $praticiens);
        CStoredObject::massLoadFwdRef($praticiens, 'function_id');
        $sejours = CStoredObject::massLoadFwdRef($operations, 'sejour_id');
        CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        COperation::massLoadActes($operations);

        $lines_by_user = $this->prepareExceedingFeesRecovery($operations, $prat_id ?: null);

        $this->renderSmarty('exceeding_fees_recovery_print', [
            'lines_by_user' => $lines_by_user,
            'date'          => $date->format('Y-m-d'),
        ]);
    }

    /**
     * Regroups the operations and sejours by users
     *
     * @param COperation[] $operations
     *
     * @return array
     * @throws Exception
     */
    protected function prepareExceedingFeesRecovery(array $operations, ?int $prat_id): array
    {
        $lines_by_user = [];

        foreach ($operations as $_operation) {
            $_chir = $_operation->loadRefChir();
            $_operation->_ref_chir->loadRefFunction();
            $_anesth = $_operation->loadRefAnesth();
            $_operation->_ref_anesth->loadRefFunction();
            $_patient = $_operation->loadRefPatient();
            $_operation->loadRefsActes();
            $_sejour = $_operation->loadRefSejour();
            $_sejour->loadNDA();

            $_total_dp        = 0;
            $_total_dp_anesth = 0;

            foreach ($_operation->_ref_actes as $_act) {
                if ($_act->montant_depassement && $_act->executant_id == $_chir->_id) {
                    $_total_dp += $_act->montant_depassement;
                } elseif ($_act->montant_depassement && $_act->executant_id == $_anesth->_id) {
                    $_total_dp_anesth += $_act->montant_depassement;
                }
            }

            if (($_total_dp || $_operation->depassement) && (($prat_id && $_chir->_id == $prat_id) || !$prat_id)) {
                if (!array_key_exists($_chir->_id, $lines_by_user)) {
                    $lines_by_user[$_chir->_id] = [
                        'user'  => $_chir,
                        'lines' => [],
                    ];
                }

                $_line = ['patient' => $_patient, 'dp' => $_total_dp, 'nda' => false, 'state' => null];

                if ($_sejour->_NDA) {
                    $_line['nda'] = $_sejour->_NDA;
                }

                if ($_operation->depassement && $_total_dp != $_operation->depassement) {
                    $_line['dp_prevu'] = $_operation->depassement;
                }

                if ($_operation->reglement_dh_chir != 'non_regle') {
                    $_line['state'] = $_operation->reglement_dh_chir;
                }

                $lines_by_user[$_chir->_id]['lines'][] = $_line;
            }

            if (
                ($_total_dp_anesth || $_operation->depassement_anesth)
                && (($prat_id && $_anesth->_id == $prat_id) || !$prat_id)
            ) {
                if (!array_key_exists($_anesth->_id, $lines_by_user)) {
                    $lines_by_user[$_anesth->_id] = [
                        'user'  => $_anesth,
                        'lines' => [],
                    ];
                }

                $_line = ['patient' => $_patient, 'dp' => $_total_dp_anesth, 'nda' => false, 'state' => null];

                if ($_sejour->_NDA) {
                    $_line['nda'] = $_sejour->_NDA;
                }

                if ($_operation->depassement_anesth && $_total_dp_anesth != $_operation->depassement_anesth) {
                    $_line['dp_prevu'] = $_operation->depassement_anesth;
                }

                if ($_operation->reglement_dh_anesth != 'non_regle') {
                    $_line['state'] = $_operation->reglement_dh_anesth;
                }

                $lines_by_user[$_anesth->_id]['lines'][] = $_line;
            }
        }

        return $lines_by_user;
    }

    /**
     * Displays the view for editing the admission of a patient for the specified sejour
     *
     * @return void
     * @throws Exception
     */
    public function editEntreeSejour(): void
    {
        $this->checkPermEdit();

        $sejour_id = CView::get('sejour_id', 'ref class|CSejour');
        $module    = CView::get('module', 'str');
        $callback  = CView::get('callback', 'str');


        CView::checkin();

        $group = CGroups::loadCurrent();

        $sejour = new CSejour();
        $sejour->load($sejour_id);
        $sejour->loadNDA();

        $sejour->loadRefServiceMutation();
        $sejour->loadRefEtablissementTransfert();
        $sejour->loadRefEtablissementProvenance();
        $sejour->loadRefsOperations();

        //Cas des urgences
        if (CModule::getActive('dPurgences')) {
            $sejour->loadRefRPU()->loadRefSejourMutation();
        }

        $patient = $sejour->loadRefPatient();
        $patient->loadIPP();

        // maternit�
        if (CModule::getActive('maternite') && $sejour->grossesse_id) {
            $sejour->loadRefsNaissances();
            foreach ($sejour->_ref_naissances as $_naissance) {
                /** @var CNaissance $_naissance */
                $_naissance->loadRefSejourEnfant()->loadRefPatient();
            }
            $sejour->_sejours_enfants_ids = CMbArray::pluck($sejour->_ref_naissances, 'sejour_enfant_id');

            // D�calage si l'admission est avanc�e :
            // Entr�e pr�vue => entr�e r�elle
            // Sortie pr�vue => entr�e r�elle + n nuits (dur�e initiale du s�jour)
            if (!$sejour->entree_reelle && CMbDT::daysRelative(CMbDT::dateTime(), $sejour->entree_prevue) > 0) {
                $days                  = CMbDT::daysRelative($sejour->entree_prevue, $sejour->sortie_prevue);
                $sejour->entree_prevue = CMbDT::dateTime();
                $sejour->sortie_prevue = CMbDT::dateTime("+$days DAYS", CMbDT::dateTime());
            }
        }

        // E-Satis
        $eSatis_active = CModule::getActive('eSatis');
        if ($eSatis_active) {
            $esatis_consent            = new CEsatisConsent();
            $esatis_consent->sejour_id = $sejour->_id;
            $esatis_consent->loadMatchingObject();
        }

        // Liste des modes d'entr�e
        $modes_entree = $this->getListModesEntree();

        // Liste des UF de soins SSI :
        // * pas d�j� saisie,
        // * pas d'affectation,
        // * que le mode est "obligatoire"
        // * qu'on en a param�tr� au moins une
        $list_uf_soin = [];
        $show_list_uf = !$sejour->uf_soins_id && CAppUI::gconf('dPplanningOp CSejour required_uf_soins') == 'obl';

        if ($show_list_uf) {
            $affectations = $sejour->loadRefsAffectations();

            if (count($affectations) > 0) {
                $show_list_uf = false;
            }
        }

        if ($show_list_uf) {
            $uf_soin           = new CUniteFonctionnelle();
            $uf_soin->group_id = $group->_id;
            $uf_soin->type     = 'soins';
            $list_uf_soin      = $uf_soin->loadMatchingList('libelle');

            if (count($list_uf_soin) == 0) {
                $show_list_uf = false;
            }
        }

        // Idem pour l'UF m�dicale
        $list_uf_med      = [];
        $show_list_uf_med = !$sejour->uf_medicale_id && CAppUI::gconf('dPplanningOp CSejour required_uf_med') === 'obl';

        if ($show_list_uf_med) {
            $affectations = $sejour->loadRefsAffectations();

            if (count($affectations) > 0) {
                $show_list_uf_med = false;
            }
        }

        if ($show_list_uf_med) {
            $uf_med           = new CUniteFonctionnelle();
            $uf_med->group_id = $group->_id;
            $uf_med->type     = 'medicale';
            $list_uf_med      = $uf_med->loadMatchingList('libelle');

            if (!count($list_uf_med)) {
                $show_list_uf_med = false;
            }
        }

        $service  = new CService();
        $services = $service->loadGroupList();

        $parameters = [
            'callback'         => stripslashes($callback),
            'sejour'           => $sejour,
            'module'           => $module,
            'list_mode_entree' => $modes_entree,
            'list_uf_soin'     => $list_uf_soin,
            'show_list_uf'     => $show_list_uf,
            'list_uf_med'      => $list_uf_med,
            'show_list_uf_med' => $show_list_uf_med,
            'services'         => $services,
        ];

        if ($eSatis_active) {
            $parameters['esatis_consent'] = $esatis_consent;
        }

        $this->renderSmarty('admission_edit', $parameters);
    }

    /**
     * Displays the view for editing the sejours fees, and exceeding fees of the surgeon and anaesthetist
     *
     * @return void
     * @throws Exception
     */
    public function editFraisReglementSejour(): void
    {
        $this->checkPermRead();

        $sejour_id = CView::get('sejour_id', 'ref class|CSejour notNull', true);

        CView::checkin();

        $sejour = new CSejour();
        $sejour->load($sejour_id);
        $sejour->loadRefLastOperation(true);

        $this->renderSmarty('frais_reglement_sejour_edit', [
            'sejour' => $sejour,
        ]);
    }

    /**
     * Displays the view for selecting the admission types (used for filtering the sejours)
     *
     * @return void
     * @throws Exception
     */
    public function selectAdmissionTypes(): void
    {
        $this->checkPermRead();

        $sejours_ids = CView::get('sejours_ids', 'str', true);
        $view        = CView::get('view', 'str');

        CView::checkin();

        $group = CGroups::loadCurrent();

        $this->renderSmarty('select_admission_types', [
            'view'                   => $view,
            'sejours_ids_admissions' => CAppUI::pref('sejours_ids_admissions') ?: '{}',
            'list_type_admission'    => (new CSejour())->_specs['_type_admission']->_list,
            'sejours_ids'            => $sejours_ids,
            'group_id'               => $group->_id,
        ]);
    }

    /**
     * Massload the references and make all the preparations necessary for the template
     *
     * @param CSejour[] $sejours
     *
     * @return void
     * @throws Exception
     */
    private function prepareSejours(array $sejours): void
    {
        $patients = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadFwdRef($sejours, 'etablissement_entree_id');
        $praticiens = CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($praticiens, 'function_id');
        $affectations = CStoredObject::massLoadBackRefs($sejours, 'affectations', 'sortie DESC');
        CAffectation::massUpdateView($affectations);
        if (CModule::getActive('maternite')) {
            $parent_affectations = CStoredObject::massLoadFwdRef($affectations, 'parent_affectation_id');
            CAffectation::massUpdateView($parent_affectations);
            $parent_sejours = CStoredObject::massLoadFwdRef($parent_affectations, 'sejour_id');
            CStoredObject::massLoadFwdRef($parent_sejours, 'patient_id');
        }
        CStoredObject::massLoadBackRefs(
            $sejours,
            'appels',
            'appel_id DESC, datetime DESC',
            ['type' => "= 'admission'"],
            null,
            'appels_admission'
        );

        // Optimize the loading of the prestations
        CSejour::massLoadPrestationSouhaitees($sejours);

        CStoredObject::massLoadBackRefs($sejours, 'notes');
        CStoredObject::massLoadBackRefs($patients, 'dossier_medical');
        CStoredObject::massLoadBackRefs($patients, 'bmr_bhre');
        CStoredObject::massLoadBackRefs($patients, 'patient_handicaps');
        CStoredObject::massCountBackRefs($patients, 'ins_patient');

        if (CModule::getActive('appFineClient') && CAppUI::gconf('appFineClient Sync allow_appfine_sync')) {
            CStoredObject::massLoadBackRefs($patients, 'status_patient_user');
            $group_id = CGroups::loadCurrent()->_id;
            CAppFineClient::massloadIdex($sejours, $group_id);
            CAppFineClient::massloadIdex($patients, $group_id);
            CStoredObject::massLoadBackRefs(
                $sejours,
                'folder_liaison',
                null,
                ['type' => " = 'pread' "],
                null,
                'folder_liaison_pread'
            );
        }

        $operations = CStoredObject::massLoadBackRefs(
            $sejours,
            'operations',
            'date ASC',
            ['annulee' => "= '0'"],
            null,
            'operations_not_cancelled'
        );
        CStoredObject::massLoadBackRefs($operations, 'actes_ngap', 'lettre_cle DESC');

        $order = 'code_association, code_acte,code_activite, code_phase, acte_id';
        CStoredObject::massLoadBackRefs($operations, 'actes_ccam', $order);

        if (CModule::getActive('dmp')) {
            CStoredObject::massLoadBackRefs($patients, 'state_dmp');
        }

        if (CModule::getActive('notifications')) {
            CNotification::massCountIsDayBeforeNotificationSent($sejours);
        }

        CSejour::massLoadNDA($sejours);
        CPatient::massLoadIPP($patients);
        foreach ($sejours as $sejour) {
            $sejour->loadRefPraticien();

            // Chargement du patient
            $patient = $sejour->loadRefPatient();
            $patient->loadRefsPatientHandicaps();

            if (CModule::getActive('appFineClient') && CAppUI::gconf('appFineClient Sync allow_appfine_sync')) {
                $sejour->_ref_patient->loadRefStatusPatientUser();
                $sejour->loadRefFolderLiaison();
            }
            $patient->countINS();

            $patient->updateBMRBHReStatus($sejour);

            $patient->loadStateDMP();

            // Dossier m�dical
            $patient->loadRefDossierMedical(false);

            // Chargement des notes sur le s�jourw
            $sejour->loadRefsNotes();

            // Chargement des modes d'entr�e
            $sejour->loadRefEtablissementProvenance();

            // Chargement du praticien r�f�rent
            $sejour->loadRefAdresseParPraticien();

            // Chargement de l'affectation
            $affectation = $sejour->loadRefFirstAffectation();
            if (CModule::getActive('maternite')) {
                $affectation->loadRefParentAffectation()->loadRefSejour()->loadRefPatient();
            }

            // Chargement des appels
            $sejour->loadRefsAppel('admission');

            // Chargement des interventions
            $whereOperations = ['annulee' => "= '0'"];
            $sejour->loadRefsOperations($whereOperations, 'date ASC', 'operations_not_cancelled');
            $sejour->getPassageBloc('operations_not_cancelled');
        }


        // Optimisation du chargement des interventions
        /** @var CConsultAnesth[] $dossiers_anesth_total */
        $dossiers_anesth_total = [];

        $ljoin = [
            'consultation' => 'consultation.consultation_id = consultation_anesth.consultation_id',
            'plageconsult' => 'consultation.plageconsult_id = plageconsult.plageconsult_id',
        ];
        CStoredObject::massLoadBackRefs($operations, 'dossiers_anesthesie', 'date DESC', null, $ljoin);
        CStoredObject::massLoadFwdRef($operations, 'plageop_id');

        /** @var COperation $operation */
        foreach ($operations as $operation) {
            $operation->loadRefsActes();
            $consult_anesth                              = $operation->loadRefsConsultAnesth();
            $dossiers_anesth_total[$consult_anesth->_id] = $consult_anesth;
            $operation->loadRefPlageOp();
        }

        // Optimisation du chargement des dossiers d'anesth�sie
        $consultations = CStoredObject::massLoadFwdRef($dossiers_anesth_total, 'consultation_id');
        CStoredObject::massLoadFwdRef($consultations, 'plageconsult_id');
        foreach ($dossiers_anesth_total as $dossier_anesth) {
            $consultation = $dossier_anesth->loadRefConsultation();
            $consultation->loadRefPlageConsult();
            $consultation->loadRefPraticien()->loadRefFunction();
        }

        if (CAppUI::gconf('dPadmissions General show_deficience')) {
            $dossiers = CMbArray::pluck($sejours, '_ref_patient', '_ref_dossier_medical');
            CDossierMedical::massCountAntecedentsByType($dossiers, 'deficience');
        }
    }

    /**
     * Returns the list of custom entry modes
     *
     * @return array
     * @throws Exception
     */
    private function getListModesEntree(): array
    {
        try {
            $modes = [];
            if (CAppUI::conf('dPplanningOp CSejour use_custom_mode_entree')) {
                $modes = (new CModeEntreeSejour())->loadGroupList(['actif' => " = '1'"]);
            }
        } catch (Exception $e) {
            $modes = [];
        }

        return $modes;
    }

    /**
     * @param array $sejours_ids
     *
     * @return array
     */
    public static function getSejourTypesPreference(array $sejours_ids): array
    {
        $type_pref           = [];
        $list_type_admission = (new CSejour())->_specs['_type_admission']->_list;
        if (is_array($sejours_ids)) {
            foreach ($sejours_ids as $key) {
                if ($key != 0) {
                    $type_pref[] = $list_type_admission[$key];
                }
            }
        }

        return $type_pref;
    }

    /**
     * If the given value is an array, removes the empty entries.
     * If the given value is an int or a non-empty string, will return an array containing the value.
     *
     * Otherwise, will return an empty array
     *
     * @param mixed $values
     *
     * @return array
     */
    public static function cleanArrayValues($values): array
    {
        if (is_array($values)) {
            CMbArray::removeValue('', $values);
        } elseif ((is_string($values) && $values !== '') || is_int($values)) {
            $values = [$values];
        } else {
            $values = [];
        }

        return $values;
    }

    /**
     * @param array                $types
     * @param AdmissionsRepository $repository
     *
     * @return void
     */
    protected function setSejourTypeFilter(array $types, AdmissionsRepository $repository): void
    {
        if (count($types)) {
            $repository->setSejourTypeListFilter($types);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }
    }

    /**
     * @param mixed $type_pec
     *
     * @return array
     */
    protected function cleanTypePecParameter($type_pec): array
    {
        if (!is_array($type_pec) && strpos($type_pec, '|') !== false) {
            $type_pec = explode('|', $type_pec);
        }

        return $type_pec;
    }

    /**
     * Clean the empty values from the given array, and empty it if it contains the 'all' value
     *
     * @param array $prestation_ids
     *
     * @return void
     */
    protected function cleanPrestationsIds(array &$prestation_ids): void
    {
        $prestation_ids = AdmissionsLegacyController::cleanArrayValues($prestation_ids);
        if (in_array('all', $prestation_ids)) {
            $prestation_ids = [];
        }
    }

    /**
     * Display the configuration view of the module
     *
     * @return void
     */
    public function configure(): void
    {
        $this->checkPermAdmin();

        $this->renderSmarty('configure');
    }

    /**
     * Display the captions that explains the different sejour colour codes
     *
     * @return void
     */
    public function sejourCaption(): void
    {
        $this->checkPermRead();

        $this->renderSmarty('caption');
    }
}
