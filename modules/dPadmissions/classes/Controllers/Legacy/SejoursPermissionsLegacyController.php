<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Admissions\SejourPermissionsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * The legacy controller for the view 'Permissions', that display the list of patients that have a permission to leave,
 * or have to return from a permission
 */
class SejoursPermissionsLegacyController extends CLegacyController
{
    /**
     * Displays the main view
     *
     * @return void
     * @throws Exception
     */
    public function viewSejourPermissions(): void
    {
        $this->checkPermRead();

        $date         = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $type_externe = CView::get('type_externe', 'enum list|depart|retour default|depart', true);

        CView::checkin();

        $this->renderSmarty('permissions_view', [
            'date_demain'   => $date->modify('+1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle' => $date->format('Y-m-d 00:00:00'),
            'date'          => $date->format('Y-m-d'),
            'hier'          => $date->modify('-1 day')->format('Y-m-d'),
            'demain'        => $date->modify('+1 day')->format('Y-m-d'),
            'type_externe'  => $type_externe,
        ]);
    }

    /**
     * Displays the daily counters
     *
     * @return void
     * @throws Exception
     */
    public function sejourPermissionsCounters(): void
    {
        $this->checkPermRead();

        $date         = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $type         = CView::get('type', 'str', true);
        $type_externe = CView::get('type_externe', 'str default|depart', true);

        CView::checkin();
        CView::enforceSlave();

        $repository = new SejourPermissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            SejourPermissionsRepository::SEJOUR_VIEW_ADMISSIONS
        );

        /* Get the list of days in the month, with the counters set to 0 */
        $days = $repository->getMonthlyCounters(
            $date,
            ['depart', 'retour']
        );

        $repository->setCancelledFilter(0);
        $repository->setServicesListFilter(array_keys($repository->loadServicesList(true)));

        $this->setSejourTypeFilter($type, $repository);

        /* Get the number of admissions per day of the month */
        $departs = $repository->countSejoursPerDay($date);

        /* Get the number of unprepared admissions per day of the month */
        $repository->setSejourViewField(AdmissionsRepository::SEJOUR_VIEW_SORTIES);
        $retours = $repository->countSejoursPerDay($date);

        /* Set the counter foreach day */
        foreach ($days as $day => $data) {
            if (array_key_exists($day, $departs)) {
                $days[$day]['depart'] = $departs[$day];
            }
            if (array_key_exists($day, $retours)) {
                $days[$day]['retour'] = $retours[$day];
            }
        }

        $this->renderSmarty('permissions_counters', [
            'hier'          => $date->modify('-1 day')->format('Y-m-d'),
            'demain'        => $date->modify('+1 day')->format('Y-m-d'),
            'lastmonth'     => $date->modify('last day of -1 month')->format('Y-m-d'),
            'nextmonth'     => $date->modify('first day of +1 month')->format('Y-m-d'),
            'date'          => $date->format('Y-m-d'),
            'type_externe'  => $type_externe,
            'bank_holidays' => CMbDT::getHolidays($date->format('Y-m-d')),
            'days'          => $days,
        ]);
    }

    /**
     * Displays the list of patients that either have a permission to leave, or have to return from a permission,
     * for the specified date
     *
     * @return void
     * @throws Exception
     */
    public function listSejourPermissions(): void
    {
        $this->checkPermRead();

        $type           = CView::get('type', 'str', true);
        $type_externe   = CView::get('type_externe', 'enum list|depart|retour default|depart', true);
        $date           = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $filterFunction = CView::get('filterFunction', 'ref class|CFunctions', true);
        $step           = CView::get('step', 'num default|' . CAppUI::gconf('dPadmissions General pagination_step'));
        $page           = CView::get('page', 'num default|0');

        CView::checkin();
        CView::enforceSlave();

        $repository = new SejourPermissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            $type_externe === 'depart' ? SejourPermissionsRepository::SEJOUR_VIEW_ADMISSIONS
                : SejourPermissionsRepository::SEJOUR_VIEW_SORTIES
        );

        $repository->setCancelledFilter(0);
        $repository->setServicesListFilter(array_keys($repository->loadServicesList(true)));
        $this->setSejourTypeFilter($type, $repository);

        if ($filterFunction) {
            $repository->setFunctionFilter(CFunctions::findOrNew($filterFunction));
        }

        $order = $type_externe === 'depart' ? 'entree' : 'sortie';

        $affectations = CAppUI::gconf('dPadmissions General use_perms')
            ? $repository->loadSejoursByPerms(PERM_READ, $date, $order, 'ASC', (int)$page, (int)$step)
            : $repository->loadSejours($date, $order, 'ASC', (int)$page, (int)$step);
        $total        = $repository->countSejours($date);
        $functions    = $repository->loadFunctionsByAdmissions($date);

        CAffectation::massUpdateView($affectations);
        $sejours    = CStoredObject::massLoadFwdRef($affectations, 'sejour_id');
        $patients   = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        $praticiens = CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($praticiens, 'function_id');
        $lits     = CStoredObject::massLoadFwdRef($affectations, 'lit_id');
        $chambres = CStoredObject::massLoadFwdRef($lits, 'chambre_id');
        CStoredObject::massLoadFwdRef($chambres, 'service_id');

        CStoredObject::massLoadBackRefs($sejours, 'notes');
        CSejour::massLoadNDA($sejours);
        CPatient::massLoadIPP($patients);

        foreach ($affectations as $affectation) {
            $affectation->loadView();
            $affectation->loadRefsAffectations();
            $affectation->_ref_prev->loadView();
            $affectation->_ref_next->loadView();
            $sejour = $affectation->loadRefSejour();
            $sejour->loadRefPraticien();

            // Chargement du patient
            $sejour->loadRefPatient();

            // Chargement des notes sur le s�jour
            $sejour->loadRefsNotes();
        }

        $this->renderSmarty('permissions_list', [
            'hier'           => $date->modify('-1 day')->format('Y-m-d'),
            'demain'         => $date->modify('+1 day')->format('Y-m-d'),
            'date_demain'    => $date->modify('+1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle'  => $date->format('Y-m-d 00:00:00'),
            'date'           => $date->format('Y-m-d'),
            'type_externe'   => $type_externe,
            'affectations'   => $affectations,
            'canAdmissions'  => CModule::getCanDo('dPadmissions'),
            'canPatients'    => CModule::getCanDo('dPpatients'),
            'canPlanningOp'  => CModule::getCanDo('dPplanningOp'),
            'functions'      => $functions,
            'filterFunction' => $filterFunction,
            'total'          => $total,
            'step'           => $step,
            'page'           => $page,
        ]);
    }

    /**
     * Displays a printable view of the patients with a permissions
     *
     * @return void
     * @throws Exception
     */
    public function printPermissions(): void
    {
        $this->checkPermRead();

        $type_externe   = CView::get('type_externe', 'str default|depart', true);
        $date           = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $filterFunction = CView::get('filterFunction', 'ref class|CFunctions', true);

        CView::checkin();
        CView::enforceSlave();

        if ($type_externe === 'depart') {
            $view = AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS;
            $order = 'entree';
        } else {
            $view = AdmissionsRepository::SEJOUR_VIEW_SORTIES;
            $order = 'sortie';
        }

        $repository = new SejourPermissionsRepository(CGroups::loadCurrent(), CMediusers::get(), $view);
        $repository->setCancelledFilter(0);
        $repository->setServicesListFilter(array_keys($repository->loadServicesList(true)));

        if ($filterFunction) {
            $repository->setFunctionFilter(CFunctions::findOrNew($filterFunction));
        }

        $affectations = CAppUI::gconf('dPadmissions General use_perms') ?
            $repository->loadSejoursByPerms(PERM_READ, $date, $order)
            : $repository->loadSejours($date, $order);

        CAffectation::massUpdateView($affectations);
        $sejours      = CStoredObject::massLoadFwdRef($affectations, 'sejour_id');
        $patients     = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        $praticiens   = CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        $functions    = CStoredObject::massLoadFwdRef($praticiens, 'function_id');
        $lits         = CStoredObject::massLoadFwdRef($affectations, 'lit_id');
        $chambres     = CStoredObject::massLoadFwdRef($lits, 'chambre_id');
        CStoredObject::massLoadFwdRef($chambres, 'service_id');

        CStoredObject::massLoadBackRefs($sejours, 'notes');
        CSejour::massLoadNDA($sejours);
        CPatient::massLoadIPP($patients);

        foreach ($affectations as $affectation) {
            $affectation->loadView();
            $affectation->loadRefsAffectations();
            $affectation->_ref_prev->loadView();
            $affectation->_ref_prev->updateView();
            $affectation->_ref_next->loadView();
            $affectation->_ref_next->updateView();

            $sejour    = $affectation->loadRefSejour();
            $sejour->loadRefPraticien();
            $sejour->loadRefPatient();
            $sejour->loadRefsNotes();
        }

        if ($filterFunction && !array_key_exists($filterFunction, $functions)) {
            $functions[$filterFunction] = CFunctions::findOrNew($filterFunction);
        }

        $this->renderSmarty('permissions_print', [
            'date'           => $date->format('Y-m-d'),
            'type_externe'   => $type_externe,
            'affectations'   => $affectations,
            'functions'      => $functions,
            'filterFunction' => $filterFunction,
        ]);
    }

    /**
     * Set the sejour type filter on the repository based on the given string value.
     *
     * @param ?string                     $type
     * @param SejourPermissionsRepository $repository
     *
     * @return void
     */
    protected function setSejourTypeFilter(?string $type, SejourPermissionsRepository $repository): void
    {
        if ($type == 'ambucomp') {
            $repository->setSejourTypeListFilter(['ambu', 'comp']);
        } elseif ($type == 'ambucompssr') {
            $repository->setServicesListFilter(['comp', 'comp', 'ssr']);
        } elseif ($type) {
            $repository->setSejourTypeFilter($type);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }
    }
}
