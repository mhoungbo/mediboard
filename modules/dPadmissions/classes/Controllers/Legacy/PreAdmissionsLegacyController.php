<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\AppFine\Client\CAppFineClient;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Admissions\PreAdmissionsRepository;
use Ox\Mediboard\Cabinet\CConsultAnesth;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Hospi\CPrestation;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * The legacy controller for the view 'Preadmissions', that displays the list of anaesthesia consultations
 */
class PreAdmissionsLegacyController extends CLegacyController
{
    /**
     * Displays the main view
     *
     * @return void
     * @throws Exception
     */
    public function viewPreAdmissions(): void
    {
        $this->checkPermRead();

        $order_way_pre   = CView::get('order_way_pre', 'enum list|ASC|DESC default|ASC', true);
        $order_col_pre   = CView::get('order_col_pre', 'str default|heure', true);
        $date            = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $filter          = CView::get('filter', 'str', true);
        $sejour_prepared = CView::get('sejour_prepared', 'bool', true);

        CView::checkin();

        $this->renderSmarty('preadmissions_view', [
            'date_demain'     => $date->modify('+1 day')->format('Y-m-d'),
            'date_actuelle'   => (new DateTimeImmutable())->format('Y-m-d 00:00:00'),
            'date'            => $date->format('Y-m-d'),
            'hier'            => $date->modify('-1 day')->format('Y-m-d'),
            'demain'          => $date->modify('+1 day')->format('Y-m-d'),
            'order_way_pre'   => $order_way_pre,
            'order_col_pre'   => $order_col_pre,
            'filter'          => $filter,
            'sejour_prepared' => $sejour_prepared,
            'sejour'          => new CSejour(),
        ]);
    }

    /**
     * Displays the daily counters of the preadmissions
     *
     * @return void
     * @throws Exception
     */
    public function preAdmissionsCounters(): void
    {
        $this->checkPermRead();

        $date = new DateTimeImmutable(CView::get('date', 'date default|now', true));

        CView::checkin();
        CView::enforceSlave();

        $user  = CMediusers::get();
        $group = CGroups::loadCurrent();

        $days       = (new AdmissionsRepository($group, $user))->getMonthlyCounters($date, ['total']);
        $repository = new PreAdmissionsRepository($group, $user);

        $total = $repository->countAnaesthesiaConsultations($date);

        $preadmissions = $repository->countAnaesthesiaConsultationsPerDay($date);

        foreach ($preadmissions as $preadmission) {
            if (array_key_exists($preadmission['date'], $days)) {
                $days[$preadmission['date']]['total'] = $preadmission['total'];
            }
        }

        $this->renderSmarty('preadmissions_counters', [
            'hier'          => $date->modify('-1 day')->format('Y-m-d'),
            'demain'        => $date->modify('+1 day')->format('Y-m-d'),
            'lastmonth'     => $date->modify('last day of -1 month')->format('Y-m-d'),
            'nextmonth'     => $date->modify('first day of +1 month')->format('Y-m-d'),
            'date'          => $date->format('Y-m-d'),
            'bank_holidays' => CMbDT::getHolidays($date->format('Y-m-d')),
            'days'          => $days,
            'total'         => $total,
        ]);
    }

    /**
     * Dipslays the list of anaesthesia consultations (and of sejour or operations without one) for the specified date
     *
     * @return void
     * @throws Exception
     */
    public function preAdmissionsList(): void
    {
        $this->checkPermRead();

        $sejour_prepared = CView::get('sejour_prepared', 'bool', true);
        $order_col_pre   = CView::get('order_col_pre', 'enum list|patient_id|heure default|heure', true);
        $order_way_pre   = CView::get('order_way_pre', 'enum list|ASC|DESC default|ASC', true);
        $date            = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $filter          = CView::get('filter', 'str');
        $is_modal        = CView::get('is_modal', 'bool default|0');
        $type_pec        = CView::get('type_pec', [
            'str',
            'default' => (new CSejour())->_specs['type_pec']->_list,
        ]);
        $period          = CView::get('period', 'enum list|matin|soir');

        CView::checkin();
        CView::enforceSlave();

        $repository = new PreAdmissionsRepository(CGroups::loadCurrent(), CMediusers::get());

        if ($period) {
            $hour = CAppUI::gconf('dPadmissions General hour_matin_soir');
            $repository->setPeriodFilter($period, $hour);
        }

        $consultations = $repository->getAnaesthesiaConsultations($date, $order_col_pre, $order_way_pre);

        $this->massLoadConsultations($consultations);

        [
            'sejours'       => $sejours,
            'preadmissions' => $preadmissions,
        ] = $this->filterConsultations($consultations, $date, $type_pec, $filter);

        if ($sejour_prepared) {
            $prepared_sejours = $repository->getPreparedSejoursWithoutAnaesthesiaRecords(
                $date,
                array_keys($sejours),
                $type_pec
            );

            $this->prepareSejoursWithoutAnaesthesiaRecords($prepared_sejours, $preadmissions);

            $sejours = array_replace($sejours, $prepared_sejours);

            if ($order_col_pre === 'patient_id') {
                usort($preadmissions, function ($a, $b) use ($order_way_pre) {
                    $order = strnatcmp($a['CConsultation']->_ref_patient->nom, $b['CConsultation']->_ref_patient->nom);

                    return $order_way_pre === 'ASC' ? $order : $order * -1;
                });
            }
        }

        $this->massLoadSejours($sejours);

        $this->renderSmarty('preadmissions_list', [
            'hier'            => $date->modify('-1 day')->format('Y-m-d'),
            'demain'          => $date->modify('+1 day')->format('Y-m-d'),
            'date_min'        => $repository->getDateMin($date),
            'date_max'        => $repository->getDateMax($date),
            'date_demain'     => $date->modify('+1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle'   => $date->format('Y-m-d 00:00:00'),
            'date'            => $date->format('Y-m-d'),
            'filter'          => $filter,
            'is_modal'        => $is_modal,
            'order_col_pre'   => $order_col_pre,
            'order_way_pre'   => $order_way_pre,
            'preadmissions'   => $preadmissions,
            'prestations'     => CPrestation::loadCurrentList(),
            'canAdmissions'   => CModule::getCanDo('dPadmissions'),
            'canPatients'     => CModule::getCanDo('dPpatients'),
            'canPlanningOp'   => CModule::getCanDo('dPplanningOp'),
            'sejour_prepared' => $sejour_prepared,
            'sejour'          => new CSejour(),
            'type_pec'        => $type_pec,
            'period'          => $period,
        ]);
    }

    /**
     * Displays a printable view of the preadmissions for the specified date
     *
     * @return void
     * @throws Exception
     */
    public function printPreAdmissions(): void
    {
        $this->checkPermRead();

        $sejour_prepared = CView::get("sejour_prepared", "bool", true);
        $date            = new DateTimeImmutable(CView::get("date", "date default|now", true));
        $filter          = CView::get("filter", "str");
        $spec_type_pec   = [
            "str",
            "default" => (new CSejour())->_specs["type_pec"]->_list,
        ];
        $type_pec        = CView::get("type_pec", $spec_type_pec);
        $period          = CView::get("period", "enum list|matin|soir");

        CView::checkin();
        CView::enforceSlave();

        $repository = new PreAdmissionsRepository(CGroups::loadCurrent(), CMediusers::get());

        if ($period) {
            $hour = CAppUI::gconf('dPadmissions General hour_matin_soir');
            $repository->setPeriodFilter($period, $hour);
        }

        $consultations = $repository->getAnaesthesiaConsultations($date, 'heure');

        $this->massLoadConsultations($consultations);

        [
            'sejours'       => $sejours,
            'preadmissions' => $preadmissions,
        ] = $this->filterConsultations($consultations, $date, $type_pec, $filter);

        if ($sejour_prepared) {
            $prepared_sejours = $repository->getPreparedSejoursWithoutAnaesthesiaRecords(
                $date,
                array_keys($sejours),
                $type_pec
            );

            $this->prepareSejoursWithoutAnaesthesiaRecords($prepared_sejours, $preadmissions);

            $sejours = array_replace($sejours, $prepared_sejours);
        }

        $this->massLoadSejours($sejours);

        $this->renderSmarty('preadmissions_print', [
            'filter'            => $filter,
            'date'              => $date->format('Y-m-d'),
            'preadmissions'     => $preadmissions,
            'total'             => count($preadmissions),
            'sejour_prepared'   => $sejours,
            'sejour'            => new CSejour(),
            'type_pec'          => $type_pec,
            'period'            => $period,
        ]);
    }

    /**
     * Makes the necessary massloads for the consultations
     *
     * @param CConsultation[] $consultations
     *
     * @return void
     * @throws Exception
     */
    protected function massLoadConsultations(array $consultations): void
    {
        $dossiers_anesth = CStoredObject::massLoadBackRefs($consultations, 'consult_anesth');
        $patients        = CStoredObject::massLoadFwdRef($consultations, 'patient_id');
        $plages          = CStoredObject::massLoadFwdRef($consultations, 'plageconsult_id');
        $users           = CStoredObject::massLoadFwdRef($plages, 'chir_id');
        CStoredObject::massLoadFwdRef($users, 'function_id');
        CStoredObject::massLoadBackRefs($patients, 'bmr_bhre');
        $operations = CStoredObject::massLoadFwdRef($dossiers_anesth, 'operation_id');
        CStoredObject::massLoadFwdRef($dossiers_anesth, 'sejour_id');
        CStoredObject::massLoadFwdRef($operations, 'plageop_id');
        $sejours = CStoredObject::massLoadFwdRef($operations, 'sejour_id');
        CStoredObject::massLoadFwdRef($sejours, 'patient_id');
    }

    /**
     * Check that the given consultations match the given filters and have anaesthesia records.
     *
     * If the anaesthesia record is not linked to a CSejour or COperation,
     * get the next CSejour and COperation of the patient
     *
     * @param CConsultation[]   $consultations
     * @param DateTimeImmutable $date
     * @param array             $type_pec
     * @param string            $filter
     *
     * @return array
     * @throws Exception
     */
    protected function filterConsultations(
        array $consultations,
        DateTimeImmutable $date,
        array $type_pec,
        string $filter
    ): array {
        $sejours       = [];
        $preadmissions = [];
        foreach ($consultations as $consultation) {
            $preadmission = [
                'CConsultation' => $consultation,
                'CSejour'       => new CSejour(),
                'COperation'    => new COperation(),
            ];

            $dossiers_anesth_consult = $consultation->loadRefsDossiersAnesth();

            /* Removes consultation without anaesthesia record */
            if (!count($dossiers_anesth_consult)) {
                unset($consultations[$consultation->_id]);
                continue;
            }

            $consultation->loadRefPlageConsult();

            $consultation->loadRefPatient()->updateBMRBHReStatus($consultation);

            if (CModule::getActive('appFineClient')) {
                CAppFineClient::loadIdex($consultation->_ref_patient, CGroups::loadCurrent()->_id);
                $consultation->_ref_patient->loadRefStatusPatientUser();
                $consultation->loadRefFolderLiaison('pread');
            }

            $consultation->_ref_chir->loadRefFunction();

            $consultation->checkDHE($date->format('Y-m-d'));

            $empty_record = false;
            /* Get the CSejour from the Anaesthesia Record */
            foreach ($consultation->_refs_dossiers_anesth as $dossier) {
                $sejour = $dossier->_ref_sejour;

                $sejour->loadRefsOperations();
                if ($sejour->_id) {
                    if (!in_array($sejour->type_pec, $type_pec)) {
                        unset($consultations[$consultation->_id]);
                        continue 2;
                    }

                    $sejours[$sejour->_id] = $sejour;
                } else {
                    $empty_record = true;
                }
            }

            if ($empty_record) {
                $next = $consultation->_ref_patient->getNextSejourAndOperation($consultation->_ref_plageconsult->date);

                if ($next['COperation']->_id) {
                    $next['COperation']->loadRefSejour();
                    $next['COperation']->_ref_sejour->loadRefPraticien();
                    $next['COperation']->_ref_sejour->loadNDA();
                    $next['COperation']->_ref_sejour->loadRefsNotes();
                    if ($filter == 'dhe') {
                        unset($consultations[$consultation->_id]);
                        continue;
                    }

                    $preadmission['COperation'] = $next['COperation'];
                }

                if ($next['CSejour']->_id) {
                    $next['CSejour']->loadRefPraticien();
                    $next['CSejour']->loadNDA();
                    $next['CSejour']->loadRefsNotes();
                    if ($filter == 'dhe') {
                        unset($consultations[$consultation->_id]);
                        continue;
                    }

                    $sejours[$next['CSejour']->_id] = $next['CSejour'];
                    $preadmission['CSejour']        = $next['CSejour'];
                }
            } elseif ($filter == 'dhe') {
                unset($consultations[$consultation->_id]);
            }

            $preadmissions[] = $preadmission;
        }

        return ['sejours' => $sejours, 'preadmissions' => $preadmissions];
    }

    /**
     * Adds empty CConsultation objects for the CSejours without anaesthesia records
     *
     * @param array $sejours
     * @param array $preadmissions
     *
     * @return void
     * @throws Exception
     */
    protected function prepareSejoursWithoutAnaesthesiaRecords(array $sejours, array &$preadmissions): void
    {
        CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        $users = CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($users, 'function_id');
        CSejour::massLoadNDA($sejours);

        foreach ($sejours as $sejour) {
            $sejour->loadRefPraticien();
            $sejour->loadRefsNotes();

            $this->loadAppFineData($sejour);

            $consult_anesth                          = new CConsultAnesth();
            $consult_anesth->_ref_sejour             = $sejour;
            $consult_sejour                          = new CConsultation();
            $consult_sejour->_ref_plageconsult       = new CPlageconsult();
            $consult_sejour->_refs_dossiers_anesth[] = $consult_anesth;
            $patient                                 = $sejour->loadRefPatient();
            $patient->countINS();
            $consult_sejour->_ref_patient = $patient;

            $preadmissions[] = [
                'CConsultation' => $consult_sejour,
                'COperation'    => new COperation(),
                'CSejour'       => $sejour,
            ];
        }
    }

    /**
     * Load the AppFine client data if the module is active
     *
     * @param CSejour $sejour
     *
     * @return void
     */
    protected function loadAppFineData(CSejour $sejour): void
    {
        if (CModule::getActive('appFineClient')) {
            CAppFineClient::loadIdex($sejour, $sejour->group_id);
            $patient = $sejour->loadRefPatient();
            $patient->loadRefStatusPatientUser();
            $sejour->loadRefFolderLiaison('pread');
        }
    }

    /**
     * Make the necessary massloads for the CSejour
     *
     * @param array $sejours
     *
     * @return void
     * @throws Exception
     */
    protected function massLoadSejours(array $sejours): void
    {
        $patients = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadBackRefs($sejours, 'notes');
        $affectations = CStoredObject::massLoadBackRefs($sejours, 'affectations', 'sortie DESC');
        CAffectation::massUpdateView($affectations);

        if (CModule::getActive('dmp')) {
            CStoredObject::massLoadBackRefs($patients, 'state_dmp');
        }

        // Chargement des NDA
        CSejour::massLoadNDA($sejours);

        // Chargement optimis� des prestations
        CSejour::massLoadPrestationSouhaitees($sejours);

        foreach ($sejours as $sejour) {
            $sejour->loadRefPatient();
            $sejour->loadRefPraticien();
            $sejour->loadRefsNotes();
            $sejour->loadRefFirstAffectation();
            $sejour->getDroitsC2S();
            $this->loadAppFineData($sejour);
        }
    }
}
