<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\Core\CLegacyController;
use Ox\Core\CView;
use Ox\Mediboard\Admissions\IdentitoVigilanceRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * A legacy controller for the view 'IdentitoVigilance'
 */
class IdentitoVigilanceLegacyController extends CLegacyController
{
    /**
     * The index of the view IdentitoVigilance
     *
     * @return void
     * @throws Exception
     */
    public function viewIdentitoVigilance(): void
    {
        $this->checkPermAdmin();

        $date = CView::get('date', 'date default|now', true);

        CView::checkin();

        $this->renderSmarty('identito_vigilance_view', [
            'date' => $date,
        ]);
    }

    /**
     * List the patients and their status (mergeable or not)
     *
     * @return void
     * @throws Exception
     */
    public function listPatientsIdentitoVigilance(): void
    {
        $this->checkPermAdmin();

        $display_mergeable = CView::get('display_mergeable', 'bool default|1', true);
        $display_yesterday = boolval(CView::get('display_yesterday', 'bool default|1', true));
        $display_cancelled = boolval(CView::get('display_cancelled', 'bool default|1', true));
        $module            = CView::get('module', 'str default|dPadmissions', true);
        $date              = new DateTimeImmutable(CView::get('date', 'date default|now', true));

        CView::checkin();
        CView::enforceSlave();

        $repository = new IdentitoVigilanceRepository(CGroups::loadCurrent(), $date, $display_yesterday);

        if ($module === 'dPurgences') {
            $repository->setHandleEmergencies(true);
        }

        $repository->setCancelledFilter($display_cancelled);

        $guesses = $repository->guessPossibleMerges();

        $this->renderSmarty('identito_vigilance_list', [
            'mergeables_count'  => $repository->getMergeableCount(),
            'display_mergeable' => $display_mergeable,
            'display_yesterday' => $display_yesterday,
            'display_cancelled' => $display_cancelled,
            'date'              => $date->format('Y-m-d'),
            'patients'          => $repository->getPatients(),
            'guesses'           => $guesses,
            'module'            => $module,
            'allow_merge'       => CSejour::isMergeAllowed(),
        ]);
    }
}
