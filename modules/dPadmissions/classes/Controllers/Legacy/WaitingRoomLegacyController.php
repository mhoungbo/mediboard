<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * Legacy controller for the view "Accueil presentation"
 */
class WaitingRoomLegacyController extends CLegacyController
{
    /**
     * Displays the index of the view
     *
     * @return void
     * @throws Exception
     */
    public function viewWaitingRoom(): void
    {
        $this->checkPermRead();

        $status_pec       = CView::get('_statut_pec', 'str', true);
        $praticien_id     = CView::get('praticien_id', 'ref class|CMediusers', true);
        $type_pec         = CView::get(
            'type_pec',
            'str default|' . (new CSejour())->_specs['type_pec']->list
        );
        $enabled_services = CView::get('active_filter_services', 'bool default|0', true);
        $period           = CView::get('period', 'enum list|matin|soir', true);

        CView::checkin();

        $this->renderSmarty('waiting_room_view', [
            '_statut_pec'      => $status_pec,
            'praticien_id'     => $praticien_id,
            'type_pec'         => $type_pec,
            'enabled_services' => $enabled_services,
            'period'           => $period,
        ]);
    }

    /**
     * Displays the list of patients in the waiting room
     *
     * @return void
     * @throws Exception
     */
    public function waitingRoomPatientsList(): void
    {
        $this->checkPermRead();

        $status_pec       = CView::get('_statut_pec', 'str', true);
        $praticien_id     = CView::get('praticien_id', 'ref class|CMediusers', true);
        $type_pec         = CView::get(
            'type_pec',
            'str default|' . (new CSejour())->_specs['type_pec']->list
        );
        $mode_bandeau     = CView::get('mode_bandeau', 'bool default|0');
        $enabled_services = CView::get('enabled_services', 'bool default|0');
        $period           = CView::get('period', 'enum list|matin|soir', true);

        $types_admission = CSejour::getTypeSejourIdsPref();
        $services_filter = CService::getServicesIdsPref();

        CView::checkin();
        CView::enforceSlave();

        $date = new DateTimeImmutable();

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_REAL_ADMISSIONS
        );

        if (!is_array($type_pec) && $type_pec !== '') {
            $type_pec = explode('|', $type_pec);
        } elseif ($type_pec === '') {
            $type_pec = [];
        }

        if (count($type_pec)) {
            $repository->setTypePecFilter($type_pec);
        }

        if ($status_pec === 'attente' || $status_pec === 'en_cours') {
            $repository->setStatusPecFilter($status_pec);
        }

        if (is_array($types_admission) && count($types_admission)) {
            $types = [];
            $list_types = (new CSejour())->_specs['_type_admission']->_list;

            foreach ($types_admission as $type_index) {
                $types[] = $list_types[$type_index];
            }

            $repository->setSejourTypeListFilter($types);
        }

        if ($enabled_services && is_array($services_filter)) {
            $repository->setServicesListFilter($services_filter);
        }

        if ($praticien_id) {
            $repository->setUserFilter(CMediusers::findOrNew($praticien_id));
        }

        if ($period) {
            $repository->setPeriodFilter($period, CAppUI::gconf('dPadmissions General hour_matin_soir'));
        }

        $sejours = $repository->loadSejours($date, 'entree_reelle');

        CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CSejour::massLoadCurrAffectation($sejours);

        $data_bandeau = $mode_bandeau ? [] : null;
        $sejours_pages = [0 => []];

        $this->prepareSejours($sejours, $date, $sejours_pages, $data_bandeau);

        if ($mode_bandeau) {
            $this->renderJson($data_bandeau);
        } else {
            $this->renderSmarty('waiting_room_lines', [
                'sejours_pages' => $sejours_pages,
            ]);
        }
    }

    /**
     * @param array             $sejours
     * @param DateTimeImmutable $date
     * @param array             $sejours_pages
     * @param array|null        $data_bandeau
     *
     * @return void
     */
    protected function prepareSejours(
        array $sejours,
        DateTimeImmutable $date,
        array &$sejours_pages,
        ?array &$data_bandeau
    ): void {

        $sejours_per_page = (int)CAppUI::gconf('dPadmissions presentation nb_elements_affiche');
        $current_page_index = 0;
        foreach ($sejours as $sejour) {
            if (count($sejours_pages[$current_page_index]) === $sejours_per_page) {
                $current_page_index++;
                $sejours_pages[$current_page_index] = [];
            }

            $patient = $sejour->loadRefPatient();
            $patient->nom = substr($patient->nom, 0, 3);
            $patient->prenom = substr($patient->prenom, 0, 3);
            $patient->naissance = CMbDT::format($patient->naissance, '%d/%m');

            $affectation = $sejour->_ref_curr_affectation;
            $affectation->updateView();
            $bed = $affectation->_ref_lit;
            $room = $bed->loadRefChambre();
            $room->loadRefService();

            if (CModule::getActive('hotellerie')) {
                $cleanup                = $bed->loadLastCleanup($date->format('Y-m-d'));
                $cleanup->_waiting_time = false;

                if ($cleanup->_id) {
                    if (
                        $cleanup->datetime_end
                        && CMbDT::minutesRelative($cleanup->datetime_end, $date->format('Y-m-d H:M:s')) >= 0
                    ) {
                        $time_diff_cleanup      = CMbDT::minutesRelative(
                            $cleanup->datetime_end,
                            $date->format('Y-m-d H:M:s')
                        );
                        $time_diff_sejour       = CMbDT::minutesRelative(
                            $sejour->entree_reelle,
                            $date->format('Y-m-d H:M:s')
                        );
                        $time_diff              = min($time_diff_cleanup, $time_diff_sejour);

                        $cleanup->_status       = 'ready';
                        $cleanup->_waiting_time = (($time_diff > 60) ? (floor($time_diff / 60)) . 'h ' : '')
                            . ($time_diff % 60) . 'm';
                        if (is_array($data_bandeau)) {
                            $data_bandeau[] = CAppUI::tr(
                                'admissions-presentation waited-to-the-service',
                                [
                                    $patient->prenom,
                                    $patient->nom,
                                    (($patient->sexe === 'f') ? 'e' : ''),
                                    $room->_ref_service->_view,
                                    $room->_view
                                ]
                            );
                        }
                    } else {
                        $cleanup->_status = 'cleaning';
                    }
                } else {
                    $cleanup->_status = 'assigning';
                }
            }

            $sejours_pages[$current_page_index][] = $sejour;
        }
    }
}
