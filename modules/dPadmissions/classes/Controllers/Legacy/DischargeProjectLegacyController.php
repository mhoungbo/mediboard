<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Mediboard\Admissions\DischargeProjectRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CPatientHandicap;
use Ox\Mediboard\PlanningOp\CModeSortieSejour;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * The legacy controller for the view "projet sortie"
 */
class DischargeProjectLegacyController extends CLegacyController
{
    /**
     * Displays the main view
     *
     * @return void
     * @throws Exception
     */
    public function viewDischargeProject(): void
    {
        $this->checkPermRead();

        $praticien_id   = CView::get('praticien_id', 'ref class|CMediusers', true);
        $date_min       = CView::get('_date_min_stat', ['date', 'default' => CMbDT::date('- 1 month')], true);
        $date_max       = CView::get('_date_max_stat', 'date default|now', true);
        $tutelle        = CView::get('tutelle', 'enum list|aucune|tutelle|curatelle default|aucune', true);
        $handicap       = CView::get('handicap', 'str', true);
        $aide_organisee = CView::get('aide_organisee', 'str', true);
        $mode_sortie    = CView::get('mode_sortie', 'str', true);
        $sejours_ids    = CView::get('sejours_ids', 'str', true);

        CSejour::getTypeSejourIdsPref($sejours_ids);

        CView::checkin();

        $filter = new CSejour();
        $filter->praticien_id = $praticien_id;
        $filter->_date_min_stat = $date_min;
        $filter->_date_max_stat = $date_max;

        $patient = new CPatient();
        $patient->tutelle = $tutelle;

        $handicap_selected = AdmissionsLegacyController::cleanArrayValues(explode(',', $handicap));
        $aide_selected = explode(',', $aide_organisee);
        $mode_sortie_selected = AdmissionsLegacyController::cleanArrayValues(explode(',', $mode_sortie));

        $list_mode_sortie = [];
        if (CAppUI::conf('dPplanningOp CSejour use_custom_mode_sortie')) {
            $list_mode_sortie = (new CModeSortieSejour())->loadGroupList(['actif' => "= '1'"]);
        }

        $prat = CMediusers::get();
        $prats = $prat->loadPraticiens();

        $this->renderSmarty('discharge_project_view', [
            'filter'             => $filter,
            'patient'            => $patient,
            'patient_handicap'   => new CPatientHandicap(),
            'sejours'            => [],
            'handicap_select'    => $handicap_selected,
            'aide_select'        => $aide_selected,
            'prats'              => $prats,
            'list_mode_sortie'   => $list_mode_sortie,
            'mode_sortie_select' => $mode_sortie_selected,
        ]);
    }

    /**
     * Displays the list of sejours
     *
     * @return void
     * @throws Exception
     */
    public function dischargeProjectList(): void
    {
        $this->checkPermRead();

        $praticien_id   = CView::get('praticien_id', 'ref class|CMediusers', true);
        $date_min       = new DateTimeImmutable(
            CView::get('_date_min_stat', ['date', 'default' => CMbDT::date('- 1 month')], true)
        );
        $date_max       = new DateTimeImmutable(CView::get('_date_max_stat', 'date default|now', true));
        $tutelle        = CView::get('tutelle', 'enum list|aucune|tutelle|curatelle default|aucune', true);
        $handicap       = CView::get('handicap', 'str', true);
        $aide_organisee = CView::get('aide_organisee', 'str', true);
        $mode_sortie    = CView::get('mode_sortie', 'str', true);
        $sejours_ids    = CView::get('sejours_ids', 'str', true);

        $sejours_ids  = CSejour::getTypeSejourIdsPref($sejours_ids);

        CView::checkin();
        CView::enforceSlave();

        $type_pref = AdmissionsLegacyController::getSejourTypesPreference($sejours_ids);

        $handicap_selected = AdmissionsLegacyController::cleanArrayValues(explode(',', $handicap));
        $aide_selected = explode(',', $aide_organisee);
        $mode_sortie_selected = AdmissionsLegacyController::cleanArrayValues(explode(',', $mode_sortie));

        $repository = new DischargeProjectRepository(CGroups::loadCurrent(), $date_min, $date_max);

        if ($praticien_id) {
            $repository->setUserIdFilter($praticien_id);
        }

        if (CAppUI::gconf('dPplanningOp CSejour show_tutelle')) {
            $repository->setPatientTutelleFilter($tutelle);
        }

        if (
            CAppUI::gconf('dPplanningOp CSejour show_aide_organisee')
            && !in_array('1', $aide_selected) && count($aide_selected)
        ) {
            $repository->setHelpFilter($aide_selected);
        }

        if (count($type_pref)) {
            $repository->setSejourTypesFilter($type_pref);
        } else {
            $repository->setSejourTypesFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']), false);
        }

        $repository->setDischargeModesFilter(
            $mode_sortie_selected,
            CAppUI::conf('dPplanningOp CSejour use_custom_mode_sortie')
        );

        $repository->setHandicapFilter($handicap_selected);
        $sejours = $repository->loadSejours();

        $patients = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($sejours, 'mode_sortie_id');

        CStoredObject::massLoadBackRefs($patients, 'bmr_bhre');
        CStoredObject::massLoadBackRefs($patients, 'patient_handicaps');

        foreach ($sejours as $sejour) {
            /* @var CSejour $sejour*/
            $sejour->loadRefPatient()->updateBMRBHReStatus($sejour);
            $sejour->loadRefPraticien();
            $sejour->loadRefModeSortie();
            $sejour->loadRefCurrAffectation();
            $sejour->loadRefLastOperation();
            $sejour->_ref_patient->loadRefsPatientHandicaps();
        }

        $this->renderSmarty('discharge_project_list', [
            'sejours' => $sejours
        ]);
    }
}
