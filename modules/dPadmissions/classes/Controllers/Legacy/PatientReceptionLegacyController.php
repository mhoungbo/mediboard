<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * A legacy controller for the patient reception view
 */
class PatientReceptionLegacyController extends CLegacyController
{
    /**
     * Displays the main view for the patient receptions
     *
     * @throws Exception
     */
    public function viewPatientReceptions(): void
    {
        $this->checkPermRead();

        $filter                 = new CSejour();
        $filter->_date_min_stat = CView::get('_date_min_stat', 'date default|now', true);
        $filter->_date_max_stat = CView::get('_date_max_stat', 'date default|now', true);
        $filter->type           = CView::get('type', 'str', true);
        $filter->_statut_pec    = CView::get('_statut_pec', 'str', true);
        $filter->praticien_id   = CView::get('praticien_id', 'ref class|CMediusers', true);

        $type_pec        = CView::get('type_pec', [
            'str',
            'default' => $filter->_specs['type_pec']->list
        ], true);
        $enabled_service = CView::get('active_filter_services', 'bool default|0', true);
        $period          = CView::get('period', 'enum list|matin|soir', true);
        $date_interv_eg_entree = CView::get('date_interv_eg_entree', 'bool default|0', true);
        $order_col       = CView::get(
            'order_col',
            'enum list|patient_id|entree_prevue|praticien_id|entree_reelle|pec_accueil|pec_service default|patient_id'
        );
        $order_way       = CView::get('order_way', 'enum list|DESC|ASC default|ASC');

        CSejour::getTypeSejourIdsPref(CView::get('sejours_ids', 'str', true));

        CView::checkin();

        if (is_string($type_pec)) {
            $type_pec = explode('|', $type_pec);
        }

        $this->renderSmarty('patient_receptions_view', [
            'filter'          => $filter,
            'period'          => $period,
            'order_col'       => $order_col,
            'order_way'       => $order_way,
            'prats'           => (new AdmissionsRepository(
                CGroups::loadCurrent(),
                CMediusers::get()
            ))->loadPractitioners(),
            'type_pec'        => $type_pec,
            'enabled_service' => $enabled_service,
            'date_interv_eg_entree' => $date_interv_eg_entree,
        ]);
    }

    /**
     * Displays the list of patients
     *
     * @return void
     * @throws Exception
     */
    public function listPatientReceptions(): void
    {
        $this->checkPermRead();

        $date_min = new DateTimeImmutable(CView::get('_date_min_stat', 'date default|now', true));
        $date_max = new DateTimeImmutable(CView::get('_date_max_stat', 'date default|now', true));
        $statut_pec    = CView::get('_statut_pec', 'str', true);
        $praticien_id   = CView::get('praticien_id', 'ref class|CMediusers', true);

        $type_pec        = CView::get('type_pec', [
            'str',
            'default' => (new CSejour())->_specs['type_pec']->list
        ], true);
        $enabled_service = CView::get('active_filter_services', 'bool default|0', true);
        $period          = CView::get('period', 'enum list|matin|soir', true);
        $date_interv_eg_entree = CView::get('date_interv_eg_entree', 'bool default|0', true);
        $order_col       = CView::get(
            'order_col',
            'enum list|patient_id|entree_prevue|praticien_id|entree_reelle|pec_accueil|pec_service default|patient_id'
        );
        $order_way       = CView::get('order_way', 'enum list|DESC|ASC default|ASC');

        $services_ids = CService::getServicesIdsPref(CView::get('services_ids', 'str', true));
        $sejours_ids  = CSejour::getTypeSejourIdsPref(CView::get('sejours_ids', 'str', true));

        CView::checkin();
        CView::enforceSlave();

        $sejours_ids = AdmissionsLegacyController::cleanArrayValues($sejours_ids);
        $type_pef = AdmissionsLegacyController::getSejourTypesPreference($sejours_ids);

        $repository = new AdmissionsRepository(CGroups::loadCurrent(), CMediusers::get());
        $repository->setCancelledFilter(0);

        if (count($services_ids) && $enabled_service) {
            $repository->setServicesListFilter($services_ids);
        }

        if (count($type_pef)) {
            $repository->setSejourTypeListFilter($type_pef);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        if (!(in_array('', $type_pec) && count($type_pec) == 1)) {
            $repository->setTypePecFilter($type_pec);
        }

        if ($statut_pec) {
            $repository->setStatusPecFilter($statut_pec);
        }

        if ($praticien_id) {
            $repository->setUserFilter(CMediusers::findOrNew($praticien_id));
        }

        if ($period) {
            $hour = CAppUI::gconf('dPadmissions General hour_matin_soir');
            $repository->setPeriodFilter($period, $hour);
        }

        if ($date_interv_eg_entree) {
            $repository->setDateOperationEqualsToEntryFilter((bool) $date_interv_eg_entree);
        }

        $sejours = $repository->loadSejours($date_min, $order_col, $order_way, null, null, $date_max);

        $patients = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadBackRefs($patients, 'bmr_bhre');
        $affectations = CStoredObject::massLoadBackRefs($sejours, 'affectations');
        CAffectation::massUpdateView($affectations);
        $praticiens = CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($praticiens, 'function_id');
        foreach ($sejours as $_sejour) {
            $_sejour->loadRefPraticien();
            $_sejour->loadRefPatient()->updateBMRBHReStatus($_sejour);
            $_sejour->loadRefFirstAffectation()->updateView();
        }

        $this->renderSmarty('patient_receptions_list', [
            'sejours' => $sejours,
            'order_col'       => $order_col,
            'order_way'       => $order_way,
        ]);
    }

    /**
     * Displays the row for a patient
     *
     * @return void
     * @throws Exception
     */
    public function showPatientReception(): void
    {
        $this->checkPermRead();

        $sejour_id = CView::get('sejour_id', 'ref class|CSejour notNull');

        CView::checkin();

        /** @var CSejour $sejour */
        $sejour = CSejour::findOrNew($sejour_id);
        $sejour->loadRefPraticien();
        $sejour->loadRefPatient()->updateBMRBHReStatus($sejour);
        $sejour->loadRefFirstAffectation();

        $this->renderSmarty('patient_receptions_line', [
            'sejour' => $sejour,
        ]);
    }
}
