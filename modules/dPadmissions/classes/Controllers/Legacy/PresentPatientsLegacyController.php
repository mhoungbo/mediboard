<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admissions\PresentPatientsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Hospi\CPrestation;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * The legacy controller for the view 'Patients pr�sents"
 */
class PresentPatientsLegacyController extends CLegacyController
{
    /**
     * Displays the main view
     *
     * @return void
     * @throws Exception
     */
    public function viewPresentPatients(): void
    {
        $this->checkPermRead();

        $order_way          = CView::get('order_way', 'enum list|ASC|DESC default|ASC', true);
        $order_col          = CView::get('order_col', 'str default|patient_id', true);
        $date               = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $services_ids       = CView::get('services_ids', 'str', true);
        $sejours_ids        = CView::get('sejours_ids', 'str', true);
        $prat_id            = CView::get('prat_id', 'ref class|CMediusers', true);
        $heure              = CView::get('heure', 'time', true);
        $enabled_service    = CView::get('active_filter_services', 'bool default|0', true);
        $only_entree_reelle = CView::get('only_entree_reelle', 'bool default|0', true);

        CService::getServicesIdsPref($services_ids);
        CSejour::getTypeSejourIdsPref($sejours_ids);

        CView::checkin();

        $sejour               = new CSejour();
        $sejour->praticien_id = $prat_id;

        $this->renderSmarty('present_patients_view', [
            'sejour'             => $sejour,
            'date_demain'        => $date->modify('+1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle'      => $date->format('Y-m-d 00:00:00'),
            'hier'               => $date->modify('-1 day')->format('Y-m-d'),
            'demain'             => $date->modify('+1 day')->format('Y-m-d'),
            'date'               => $date->format('Y-m-d'),
            'order_way'          => $order_way,
            'order_col'          => $order_col,
            'prats'              => CMediusers::get()->loadPraticiens(),
            'heure'              => $heure,
            'list_type_ad'       => $sejour->_specs['_type_admission']->_list,
            'enabled_service'    => $enabled_service,
            'only_entree_reelle' => $only_entree_reelle,
        ]);
    }

    /**
     * Displays the daily counters
     *
     * @return void
     * @throws Exception
     */
    public function presentPatientsCounters(): void
    {
        $this->checkPermRead();

        $date               = new DateTimeImmutable(CView::get('date', 'date default|now', true));
        $services_ids       = CView::get('services_ids', 'str', true);
        $admission_type_ids = CView::get('sejours_ids', 'str', true);
        $enabled_service    = CView::get('active_filter_services', 'bool default|0', true);
        $prat_id            = CView::get('prat_id', 'ref class|CMediusers', true);
        $type_pec           = CView::get('type_pec', ['str', 'default' => (new CSejour())->_specs['type_pec']->_list]);
        $only_entree_reelle = CView::get('only_entree_reelle', 'bool default|0', true);

        CView::checkin();
        CView::enforceSlave();

        if (!is_array($type_pec) && strpos($type_pec, '|') !== false) {
            $type_pec = explode('|', $type_pec);
        }

        $services_ids     = AdmissionsLegacyController::cleanArrayValues($services_ids);
        $sejour_type_pref = AdmissionsLegacyController::getSejourTypesPreference($admission_type_ids);

        $repository = new PresentPatientsRepository(CGroups::loadCurrent(), CMediusers::get());

        /* Get the list of days in the month, with the counters set to 0 */
        $days = $repository->getMonthlyCounters(
            $date,
            ['presents']
        );

        $repository->setCancelledFilter(0);

        if (count($sejour_type_pref)) {
            $repository->setSejourTypeListFilter($sejour_type_pref);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        if (count($services_ids) && $enabled_service) {
            $repository->setServicesListFilter($services_ids);
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        if (!(in_array('', $type_pec) && count($type_pec))) {
            $repository->setTypePecFilter($type_pec);
        }

        if ($only_entree_reelle) {
            $repository->setRealEntrySejoursFilter(true);
        }

        /* Get the number of admissions per day of the month */
        $presents = $repository->countSejoursPerDay($date);
        $total    = 0;

        /* Set the counter foreach day */
        foreach ($days as $day => $data) {
            if (array_key_exists($day, $presents)) {
                $days[$day]['presents'] = $presents[$day];
                $total                  += $presents[$day];
            }
        }

        $this->renderSmarty('present_patients_counters', [
            'hier'            => $date->modify('-1 day')->format('Y-m-d'),
            'demain'          => $date->modify('+1 day')->format('Y-m-d'),
            'bank_holidays'   => CMbDT::getHolidays($date->format('Y-m-d')),
            'date'            => $date->format('Y-m-d'),
            'lastmonth'       => $date->modify('last day of -1 month')->format('Y-m-d'),
            'nextmonth'       => $date->modify('last day of 1 month')->format('Y-m-d'),
            'days'            => $days,
            'enabled_service' => $enabled_service,
        ]);
    }

    /**
     * Displays the list of patients currently present at the specified date, depending on the specified filters
     *
     * @return void
     * @throws Exception
     */
    public function listPresentPatients(): void
    {
        $this->checkPermRead();

        $date               = CView::get('date', 'date default|now', true);
        $services_ids       = CView::get('services_ids', 'str', true);
        $sejours_ids        = CView::get('sejours_ids', 'str', true);
        $prat_id            = CView::get('prat_id', 'ref class|CMediusers', true);
        $heure              = CView::get('heure', 'time', true);
        $enabled_service    = CView::get('active_filter_services', 'bool default|0', true);
        $filterFunction     = CView::get('filterFunction', 'ref class|CFunctions', true);
        $only_entree_reelle = CView::get('only_entree_reelle', 'bool default|0', true);
        $type_pec           = CView::get(
            'type_pec',
            ['str', 'default' => (new CSejour())->_specs['type_pec']->_list],
            true
        );
        $order_way          = CView::get('order_way', 'enum list|ASC|DESC default|ASC', true);
        $order_col          = CView::get('order_col', 'str default|patient_id', true);
        $step               = CView::get(
            'step',
            'num default|' . CAppUI::gconf('dPadmissions General pagination_step')
        );
        $page               = CView::get('page', 'num default|0');

        CView::checkin();
        CView::enforceSlave();

        $date = new DateTimeImmutable($heure ? "{$date} {$heure}" : $date);

        $services_ids     = AdmissionsLegacyController::cleanArrayValues($services_ids);
        $sejour_type_pref = AdmissionsLegacyController::getSejourTypesPreference($sejours_ids);

        if (!is_array($type_pec) && strpos($type_pec, '|') !== false) {
            $type_pec = explode('|', $type_pec);
        }

        $repository = new PresentPatientsRepository(CGroups::loadCurrent(), CMediusers::get());

        $repository->setCancelledFilter(0);

        if (count($services_ids) && $enabled_service) {
            $repository->setServicesListFilter($services_ids);
        }

        if (count($sejour_type_pref)) {
            $repository->setSejourTypeListFilter($sejour_type_pref);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        if ($only_entree_reelle) {
            $repository->setRealEntrySejoursFilter(true);
        }

        if ($filterFunction) {
            $repository->setFunctionFilter(CFunctions::findOrNew($filterFunction));
        }

        if (!(in_array('', $type_pec) && count($type_pec) == 1)) {
            $repository->setTypePecFilter($type_pec);
        }

        $total_sejours = null;
        if ($heure) {
            /* Count the total number of sjeours for the day, without taking into account the specified hour */
            $total_sejours = $repository->countSejours($date);
            $repository->setHourFilter($heure);
        }


        $sejours = CAppUI::gconf('dPadmissions General use_perms')
            ? $repository->loadSejoursByPerms(PERM_READ, $date, $order_col, $order_way, $page, $step)
            : $repository->loadSejours($date, $order_col, $order_way, $page, $step);

        $total = $repository->countSejours($date);

        $functions = $repository->loadFunctionsByAdmissions($date);

        $this->prepareSejours($sejours, $date);

        $this->renderSmarty('present_patients_list', [
            'hier'            => $date->modify('-1 day')->format('Y-m-d'),
            'demain'          => $date->modify('+1 day')->format('Y-m-d'),
            'date_min'        => $date->format($heure ? "Y-m-d {$heure}" : 'Y-m-d 00:00:00'),
            'date_max'        => $date->format($heure ? "Y-m-d {$heure}" : 'Y-m-d 23:59:59'),
            'date_demain'     => $date->modify('+1 day')->format('Y-m-d 00:00:00'),
            'date_actuelle'   => $date->format('Y-m-d 00:00:00'),
            'date'            => $date->format('Y-m-d'),
            'order_col'       => $order_col,
            'order_way'       => $order_way,
            'sejours'         => $sejours,
            'total_sejours'   => $total_sejours,
            'prestations'     => CPrestation::loadCurrentList(),
            'canAdmissions'   => CModule::getCanDo('dPadmissions'),
            'canPatients'     => CModule::getCanDo('dPpatients'),
            'canPlanningOp'   => CModule::getCanDo('dPplanningOp'),
            'functions'       => $functions,
            'filterFunction'  => $filterFunction,
            'which'           => CAppUI::gconf('dPadmissions General show_curr_affectation') ? 'curr' : 'first',
            'heure'           => $heure,
            'enabled_service' => $enabled_service,
            'total'           => $total,
            'step'            => $step,
            'page'            => $page,
        ]);
    }

    /**
     * Displays a view that show the number of patients present in each services
     *
     * @return void
     * @throws Exception
     */
    public function countPatientsPresentByService(): void
    {
        $this->checkPermRead();

        $type = CView::get('type', 'str');
        $date = new DateTimeImmutable(CView::get('date', 'date default|now', true));

        CView::checkin();
        CView::enforceSlave();

        $repository = new PresentPatientsRepository(CGroups::loadCurrent(), CMediusers::get());
        $repository->setCancelledFilter(0);

        if ($type == 'ambucomp') {
            $repository->setSejourTypeListFilter(['ambu', 'comp']);
        } elseif ($type == 'ambucompssr') {
            $repository->setServicesListFilter(['comp', 'comp', 'ssr']);
        } elseif ($type && $type !== 'tous') {
            $repository->setSejourTypeFilter($type);
        } else {
            $repository->setSejourTypeExcludeListFilter(array_merge(CSejour::getTypesSejoursUrgence(), ['seances']));
        }

        $results = $repository->countSejoursByServicePerDay($date);

        $this->renderSmarty('present_patients_services', [
            'results'       => $results['counts'],
            'lastmonth'     => $date->modify('last day of -1 month')->format('Y-m-d'),
            'nextmonth'     => $date->modify('last day of 1 month')->format('Y-m-d'),
            'date'          => $date->format('Y-m-d'),
            'services'      => $results['services'],
            'bank_holidays' => CMbDT::getHolidays($date->format('Y-m-d')),
        ]);
    }

    /**
     * Makes the necessary loading and massloading for the sejours
     *
     * @param CSejour[] $sejours
     *
     * @return void
     * @throws Exception
     */
    private function prepareSejours(array $sejours, DateTimeImmutable $date): void
    {
        $show_curr_affectation = CAppUI::gconf('dPadmissions General show_curr_affectation');
        /** @var CPatient[] $patients */
        $patients              = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        $praticiens            = CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        CStoredObject::massLoadFwdRef($praticiens, 'function_id');

        // Chargement optimis�e des prestations
        CSejour::massCountPrestationSouhaitees($sejours);

        CStoredObject::massLoadBackRefs($sejours, 'notes');
        CStoredObject::massLoadBackRefs($patients, 'dossier_medical');
        CStoredObject::massLoadBackRefs($patients, 'bmr_bhre');
        CStoredObject::massLoadBackRefs($patients, 'patient_handicaps');

        // Chargement des NDA
        CSejour::massLoadNDA($sejours);
        // Chargement des IPP
        CPatient::massLoadIPP($patients);

        // Chargement optimis� des prestations
        CSejour::massCountPrestationSouhaitees($sejours);

        $operations = CStoredObject::massLoadBackRefs($sejours, 'operations', 'date ASC', ['annulee' => "= '0'"]);

        CStoredObject::massLoadBackRefs($sejours, 'billing_periods');
        CStoredObject::massLoadBackRefs($operations, 'actes_ngap', 'lettre_cle DESC');

        $order = 'code_association, code_acte,code_activite, code_phase, acte_id';
        CStoredObject::massLoadBackRefs($operations, 'actes_ccam', $order);

        $affectations = $show_curr_affectation ?
            CSejour::massLoadCurrAffectation($sejours, $date->format('Y-m-d H:i:s'))
            : CStoredObject::massLoadBackRefs($sejours, 'affectations', 'sortie DESC');
        CAffectation::massUpdateView($affectations);

        if (CModule::getActive('maternite')) {
            $parent_affectations = CStoredObject::massLoadFwdRef($affectations, 'parent_affectation_id');
            $parent_sejours      = CStoredObject::massLoadFwdRef($parent_affectations, 'sejour_id');
            CStoredObject::massLoadFwdRef($parent_sejours, 'patient_id');
        }

        foreach ($sejours as $_sejour) {
            $_sejour->loadRefPraticien();
            $_sejour->checkDaysRelative($date->format('Y-m-d'));

            // Chargement du patient
            $patient = $_sejour->loadRefPatient();

            $patient->updateBMRBHReStatus($_sejour);
            $patient->loadRefsPatientHandicaps();
            $patient->loadRefDossierMedical(false);

            // Chargement des notes du s�jour
            $_sejour->loadRefsNotes();

            // Chargement des interventions
            $_sejour->loadRefsOperations(['annulee' => "= '0'"]);
            foreach ($_sejour->_ref_operations as $operation) {
                $operation->loadRefsActes();
            }

            // Chargement de l'affectation
            if ($show_curr_affectation) {
                $affectation = $_sejour->loadRefCurrAffectation($date->format('Y-m-d H:i:s'));
            } else {
                $_sejour->loadRefsAffectations();
                $affectation = $_sejour->_ref_first_affectation;
            }

            $_sejour->loadRefFirstAffectation();
            if (CModule::getActive('maternite')) {
                $affectation->loadRefParentAffectation()->loadRefSejour()->loadRefPatient();
            }
        }

        if (CAppUI::gconf('dPadmissions General show_deficience')) {
            $dossiers = CMbArray::pluck($sejours, '_ref_patient', '_ref_dossier_medical');
            CDossierMedical::massCountAntecedentsByType($dossiers, 'deficience');
        }
    }
}
