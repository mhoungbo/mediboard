<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Core\CMbPDFMerger;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\CompteRendu\CHtmlToPDF;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * Impression dans les admissions
 */
class AdmissionsPrintLegacyController extends CLegacyController
{
    public function printFichesAnesth(): void
    {
        CApp::setMemoryLimit('4G');
        CApp::setTimeLimit(300);
        CCanDo::checkRead();

        $sejours_ids = CView::post('sejours_ids', 'str');

        CView::checkin();
        CView::enforceSlave();

        // Chargement des s�jours
        /** @var CSejour[] $sejours */
        $sejours = (new CSejour())->loadAll(explode(',', $sejours_ids));

        CStoredObject::massLoadFwdRef($sejours, 'patient_id');

        foreach ($sejours as $_sejour) {
            $_sejour->loadRefPatient();
        }

        // Tri par nom de patient
        $sorter_nom    = CMbArray::pluck($sejours, '_ref_patient', 'nom');
        $sorter_prenom = CMbArray::pluck($sejours, '_ref_patient', 'prenom');
        array_multisort($sorter_nom, SORT_ASC, $sorter_prenom, SORT_ASC, $sejours);

        $pdf_merger = new CMbPDFMerger();
        $htmltopdf  = new CHtmlToPDF();
        $files = [];

        $style = file_get_contents('./style/mediboard_ext/standard.css');
        $style = preg_replace('#\/\*.*(\*)*\*\/#msU', '', $style);

        $consult_anesths = [];

        foreach ($sejours as $_sejour) {
            $_operation = $_sejour->loadRefLastOperation();

            if (!$_operation->_id) {
                continue;
            }

            $consult_anesth = $_operation->loadRefsConsultAnesth();

            if ($consult_anesth->_id) {
                $consult_anesth->_ref_operation = $_operation;
                $consult_anesth->_fwd['operation_id'] = $_operation;
                $consult_anesths[$consult_anesth->_id] = $consult_anesth;
            }
        }

        if (!count($consult_anesths)) {
            CAppUI::stepAjax('CConsultAnesth.none', UI_MSG_WARNING);
            return;
        }

        $empty_cr   = new CCompteRendu();
        $empty_file = new CFile();

        $root_dir = CAppUI::conf('root_dir');

        $consults = CStoredObject::massLoadFwdRef($consult_anesths, 'consultation_id');
        $plages_consult = CStoredObject::massLoadFwdRef($consults, 'plageconsult_id');
        CStoredObject::massLoadFwdRef($plages_consult, 'chir_id');
        $patients = CStoredObject::massLoadFwdRef($consults, 'patient_id');
        $dossiers_medicaux_patient = CStoredObject::massLoadBackRefs($patients, 'dossier_medical');
        CStoredObject::massLoadBackRefs($dossiers_medicaux_patient, 'traitements', 'fin DESC, debut DESC');

        $operations = array_column($consult_anesths, '_ref_operation');
        CStoredObject::massLoadFwdRef($operations, 'plageop_id');
        CStoredObject::massLoadFwdRef($operations, 'chir_id');

        $sejours = CStoredObject::massLoadFwdRef($operations, 'sejour_id');
        $dossiers_medicaux_sejour = CStoredObject::massLoadBackRefs($sejours, 'dossier_medical');
        CStoredObject::massLoadBackRefs($dossiers_medicaux_sejour, 'traitements', 'fin DESC, debut DESC');

        if (CModule::getActive("dPprescription")) {
            CStoredObject::massLoadBackRefs($sejours, 'prescriptions');
        }

        foreach ($consult_anesths as $consult_anesth) {
            $result = CApp::fetch(
                'dPcabinet',
                'print_fiche',
                [
                    'dossier_anesth_id' => $consult_anesth->_id,
                    'offline'           => 1,
                    'multi'             => 1,
                    'display'           => 1,
                ]
            );

            if (strpos($result, '%PDF') === false) {
                $result = "
                  <html>
                    <head>
                      <style type='text/css'>
                        {$style}
                      </style>
                    </head>
                    <body>
                      {$result}
                    </body>
                  </html>";

                // Remplace du src de l'image de la condition d'intubation
                $result = preg_replace('#<img src="#', '<img src="' . $root_dir . '/', $result);

                $result = $htmltopdf->generatePDF($result, 0, $empty_cr, $empty_file, true, false);
            }

            $temp_file = tempnam('./tmp', 'fiche');
            file_put_contents($temp_file, $result);

            $pdf_merger->addPDF($temp_file);

            $files[] = $temp_file;
        }

        $pdf_merger->merge();

        foreach ($files as $_file) {
            unlink($_file);
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function selectOperationToPrint(): void
    {
        $this->checkPermRead();

        $sejour_id = CView::get('sejour_id', 'ref class|CSejour');

        CView::checkin();

        $sejour = new CSejour();
        $sejour->load($sejour_id);

        // Chargement des interventions
        $sejour->loadRefsOperations(['annulee' => "= '0'"]);
        $sejour->loadRefPraticien();

        $this->renderSmarty('operation_select', ['sejour' => $sejour]);
    }
}
