<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Controllers\Legacy;

use DateTimeImmutable;
use Exception;
use Ox\Core\CLegacyController;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CPrestation;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * The legacy controller for the view 'Validation s�jours'
 */
class SejourValidationLegacyController extends CLegacyController
{
    /**
     * Displays the sejours validation view.
     *
     * Is also used in the modules Ssr and Psy (hence the optional parameter)
     *
     * @param string $module
     *
     * @return void
     * @throws Exception
     */
    public function viewSejourValidation(string $module = 'admissions'): void
    {
        $this->checkPermRead();

        $recuse     = CView::get('recuse', 'num default|-1', true);
        $date       = CView::get('date', 'date default|now', true);
        $service_id = CView::get('service_id', 'ref class|CService', true);
        $prat_id    = CView::get('prat_id', 'ref class|CMediusers', true);

        CView::checkin();

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS
        );

        if (in_array($module, ['ssr', 'psy'])) {
            $repository->setSejourTypeFilter($module);
        }

        if ($prat_id === '') {
            $prat_id = null;
        }

        if ($service_id === '') {
            $service_id = null;
        }

        $this->renderSmarty('sejours_validation_view', [
            'module_view'        => $module,
            'sejour'             => $repository->getSejourFilterObject($service_id, $prat_id),
            'date'               => $date,
            'recuse'             => $recuse,
            'services'           => $repository->loadServicesList(),
            'prats'              => $repository->loadPractitioners(),
            'nb_sejours_attente' => $repository->countSejoursInWaiting(new DateTimeImmutable()),
        ], 'modules/dPadmissions');
    }

    /**
     * Displays the daily counters
     *
     * @return void
     * @throws Exception
     */
    public function sejoursValidationCounter(): void
    {
        $this->checkPermRead();

        $module = CView::get('module_view', 'str');

        $date       = CView::get('date', 'date default|now', true);
        $recuse     = CView::get('recuse', 'str default|-1', true);
        $service_id = CView::get('service_id', 'ref class|CService', true);
        $prat_id    = CView::get('prat_id', 'ref class|CMediusers', true);

        CView::checkin();
        CView::enforceSlave();

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS
        );

        if (in_array($module, ['ssr', 'psy'])) {
            $repository->setSejourTypeFilter($module);
        }

        if ($service_id) {
            $repository->setServiceFilter(CService::findOrNew($service_id));
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        $date = new DateTimeImmutable($date);

        $days = $repository->getMonthlyCounters($date, ['waiting', 'validated', 'contested']);

        $repository->setCancelledFilter(0);
        $repository->setRecuseFilter(-1);
        $waiting   = $repository->countSejoursPerDay($date);

        $repository->setRecuseFilter(0);
        $validated = $repository->countSejoursPerDay($date);

        $repository->setRecuseFilter(1);
        $contested = $repository->countSejoursPerDay($date);

        foreach ($days as $day => $data) {
            if (array_key_exists($day, $waiting)) {
                $days[$day]['waiting'] = $waiting[$day];
            }
            if (array_key_exists($day, $validated)) {
                $days[$day]['validated'] = $validated[$day];
            }
            if (array_key_exists($day, $contested)) {
                $days[$day]['contested'] = $contested[$day];
            }
        }

        $this->renderSmarty('sejours_validation_counters', [
            'module_view'   => $module,
            'recuse'        => $recuse,
            'bank_holidays' => CMbDT::getHolidays($date->format('Y-m-d')),
            'date'          => $date->format('Y-m-d'),
            'last_month'    => CMbDT::date('last day of -1 month', $date->format('Y-m-d')),
            'next_month'    => CMbDT::date('first day of +1 month', $date->format('Y-m-d')),
            'days'          => $days,
        ], 'modules/dPadmissions');
    }

    /**
     * Displays the list of the sejours that need validation for the specified date
     *
     * @return void
     * @throws Exception
     */
    public function listSejoursValidation(): void
    {
        $this->checkPermRead();

        $module         = CView::get('module_view', 'str');
        $service_id     = CView::get('service_id', 'ref class|CService', true);
        $prat_id        = CView::get('prat_id', 'ref class|CMediusers', true);
        $recuse         = CView::get('recuse', 'enum list|-1|0|1 default|-1', true);
        $order_column   = CView::get(
            'order_column',
            'enum list|patient_id|entree_prevue|praticien_id default|patient_id',
            true
        );
        $order_way      = CView::get('order_way', 'enum list|ASC|DESC default|ASC', true);
        $date           = CView::get('date', 'date default|now', true);
        $filterFunction = CView::get('filterFunction', 'ref class|CFunctions', true);

        CView::checkin();
        CView::enforceSlave();

        $repository = new AdmissionsRepository(
            CGroups::loadCurrent(),
            CMediusers::get(),
            AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS
        );

        if (in_array($module, ['ssr', 'psy'])) {
            $repository->setSejourTypeFilter($module);
        }

        if ($service_id) {
            $repository->setServiceFilter(CService::findOrNew($service_id));
        }

        if ($prat_id) {
            $repository->setUserFilter(CMediusers::findOrNew($prat_id));
        }

        $repository->setCancelledFilter(0);
        $repository->setRecuseFilter((int)$recuse);
        $sejours  = $repository->loadSejours(new DateTimeImmutable($date), $order_column, $order_way);
        $patients = CStoredObject::massLoadFwdRef($sejours, 'patient_id');
        CStoredObject::massLoadBackRefs($sejours, 'affectations', 'sortie DESC');
        CStoredObject::massLoadBackRefs($patients, 'bmr_bhre');
        $praticiens = CStoredObject::massLoadFwdRef($sejours, 'praticien_id');
        $functions  = CStoredObject::massLoadFwdRef($praticiens, 'function_id');

        CSejour::massLoadNDA($sejours);
        CPatient::massLoadIPP($patients);

        foreach ($sejours as $sejour) {
            $sejour->loadRefPraticien();
            $sejour->loadRefFicheAutonomie();

            if ($filterFunction && $filterFunction != $sejour->_ref_praticien->function_id) {
                unset($sejours[$sejour->_id]);
                continue;
            }

            $sejour->loadRefPatient()->updateBMRBHReStatus($sejour);
            $sejour->loadRefsAffectations();
            if ($sejour->_ref_first_affectation->_id) {
                $sejour->_ref_first_affectation->updateView();
            }
        }

        if ($filterFunction && !array_key_exists($filterFunction, $functions)) {
            $functions[$filterFunction] = CFunctions::findOrNew($filterFunction);
        }

        $this->renderSmarty('sejours_validation_list', [
            'module_view'    => $module,
            'yesterday'      => CMbDT::date('-1 day', $date),
            'tomorrow'       => CMbDT::date('+1 day', $date),
            'date'           => $date,
            'recuse'         => $recuse,
            'order_column'   => $order_column,
            'order_way'      => $order_way,
            'sejours'        => $sejours,
            'prestations'    => CPrestation::loadCurrentList(),
            'canAdmissions'  => CModule::getCanDo('dPadmissions'),
            'canPatients'    => CModule::getCanDo('dPpatients'),
            'canPlanningOp'  => CModule::getCanDo('dPplanningOp'),
            'functions'      => $functions,
            'filterFunction' => $filterFunction,
        ], 'modules/dPadmissions');
    }
}
