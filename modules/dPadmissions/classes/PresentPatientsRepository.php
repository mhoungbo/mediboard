<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions;

use DateTimeImmutable;
use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * A repository class, based on the AdmissionsRepository class, that loads the sejours
 * where the patient is present at the specified date
 */
class PresentPatientsRepository extends AdmissionsRepository
{
    protected bool $real_entry_filter = false;
    protected ?string $hour_filter = null;

    /**
     * @param CGroups    $group
     * @param CMediusers $user
     */
    public function __construct(CGroups $group, CMediusers $user)
    {
        $this->group = $group;
        $this->user = $user;
    }

    /**
     * If true, only the sejours where the field entree_reelle is not null will be returned or counted.
     *
     * @param bool $filter
     *
     * @return void
     */
    public function setRealEntrySejoursFilter(bool $filter): void
    {
        $this->real_entry_filter = $filter;
    }

    /**
     * Set a filter on the hour
     *
     * @param string $filter
     *
     * @return void
     */
    public function setHourFilter(string $filter): void
    {
        $this->hour_filter = $filter;
    }

    /**
     * Count the number of sejours with an entry date equals to the given date, and by using the filters
     *
     * @param DateTimeImmutable $date
     *
     * @return array
     */
    public function countSejoursPerDay(DateTimeImmutable $date, ?CMbObject $sejour_object = null): array
    {
        $this->setSejourRequest();

        try {
            $begin = $date->modify('first day of this month');
            $end = $date->modify('last day of this month');
        } catch (Exception $e) {
            return [];
        }

        $results = [];
        $sejour_object = $sejour_object ?? new CSejour();
        $i = $begin;
        while ($i <= $end) {
            try {
                if (array_key_exists('affectation', $this->request->ljoin)) {
                    $this->getCountSejoursPerDayByAffectationRequest($i);
                    $result = $sejour_object->countMultipleList(
                        $this->request->where,
                        null,
                        'affectation.sejour_id',
                        $this->request->ljoin
                    );

                    $count = 0;
                    if (is_array($result)) {
                        $count = count($result);
                    }
                } else {
                    $this->getCountSejoursPerDayRequest($i);
                    $count = $sejour_object->countList($this->request->where, null, $this->request->ljoin);

                    if (!is_int($count) && is_numeric($count)) {
                        $count = intval($count);
                    } elseif (!is_int($count)) {
                        $count = 0;
                    }
                }
            } catch (Exception $e) {
                $count = 0;
            }

            $results[$i->format('Y-m-d')] = $count;
            $i = $i->modify('+1 day');
        }

        return $results;
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function getCountSejoursPerDayByAffectationRequest(DateTimeImmutable $date): CRequest
    {
        if (!$this->request) {
            $this->setSejourRequest();
        }

        $this->request->addWhereClause('affectation.entree', " <= '" . $date->format('Y-m-d 23:59:59') . "'");
        $this->request->addWhereClause('affectation.sortie', " >= '" . $date->format('Y-m-d 00:00:00') . "'");

        return $this->request;
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function getCountSejoursPerDayRequest(DateTimeImmutable $date): CRequest
    {
        if (!$this->request) {
            $this->setSejourRequest();
        }

        $this->request->addWhereClause('sejour.entree', " <= '" . $date->format('Y-m-d 23:59:59') . "'");
        $this->request->addWhereClause('sejour.sortie', " >= '" . $date->format('Y-m-d 00:00:00') . "'");

        return $this->request;
    }

    /**
     * Count the number of patients presents by services for each day of the month
     *
     * @param DateTimeImmutable $date
     * @param CSejour           $sejour_object
     *
     * @return array
     * @throws Exception
     */
    public function countSejoursByServicePerDay(DateTimeImmutable $date, ?CSejour $sejour_object = null): array
    {
        try {
            $begin = $date->modify('first day of this month');
            $end = $date->modify('last day of this month');
        } catch (Exception $e) {
            return ['counts' => [], 'services' => []];
        }

        $counts = [];
        $services = [];
        $sejour_object = $sejour_object ?? new CSejour();
        for ($i = $begin; $i <= $end; $i = $i->modify('+1 day')) {
            try {
                $this->getCountSejoursByServicePerDayRequest($i);
                $results = $sejour_object->getDS()->loadList($this->request->makeSelect());
            } catch (Exception $e) {
                continue;
            }

            if (!is_array($results)) {
                continue;
            }

            if (!array_key_exists($i->format('Y-m-d'), $counts)) {
                $counts[$i->format('Y-m-d')] = [];
            }

            foreach ($results as $result) {
                if (!array_key_exists('service', $result) || !array_key_exists('total', $result)) {
                    continue;
                }

                if (!array_key_exists($result['service'], $services)) {
                    $services[$result['service']] = 0;
                }

                $counts[$i->format('Y-m-d')][$result['service']] = $result['total'];
                $services[$result['service']] += $result['total'];
            }

            ksort($counts[$i->format('Y-m-d')]);
        }

        ksort($services);

        return ['counts' => $counts, 'services' => $services];
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return void
     */
    protected function prepareCountSejoursByServiceRequest(DateTimeImmutable $date): void
    {
        $this->setSejourRequest();

        if (!array_key_exists('affectation', $this->request->ljoin)) {
            $this->request->addLJoinClause('affectation', '`affectation`.`sejour_id` = `sejour`.`sejour_id`');
        }
        $this->request->addLJoinClause('service', '`service`.`service_id` = `affectation`.`service_id`');

        $this->request->addWhere('`affectation`.`affectation_id` IS NOT NULL');

        $begin = $date->modify('first day of this month');
        $end = $date->modify('last day of this month');
        $this->request->addWhere(
            "`sejour`.`entree` <= '" . $end->format('Y-m-d 00:00:00')
            . "' AND `sejour`.`sortie` >= '" . $begin->format('Y-m-d 23:59:59') . "'"
        );

        $this->request->addColumn('COUNT(`affectation`.`affectation_id`)', 'total');
        $this->request->addColumn('`service`.`nom`', 'service');
        $this->request->addGroup('`affectation`.`service_id`');
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function getCountSejoursByServicePerDayRequest(DateTimeImmutable $date): CRequest
    {
        if (!$this->request) {
            $this->prepareCountSejoursByServiceRequest($date);
        }

        $this->request->addWhereClause('affectation.entree', "<= '" . $date->format('Y-m-d 23:59:59') . "'");
        $this->request->addWhereClause('affectation.sortie', ">= '" . $date->format('Y-m-d 00:00:00') . "'");

        return $this->request;
    }

    /**
     * Prepare the CRequest for loading the admissions
     *
     * @param DateTimeImmutable      $date
     * @param string                 $order_column
     * @param string                 $order_way
     * @param int|null               $start
     * @param int|null               $count
     * @param DateTimeImmutable|null $date_max
     *
     * @return CRequest
     */
    public function getLoadSejoursRequest(
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC',
        ?int $start = null,
        ?int $count = null,
        ?DateTimeImmutable $date_max = null
    ): CRequest {
        $this->setSejourRequest();

        $this->request->addColumn('`sejour`.*');

        if (!$date_max) {
            $date_max = $date;
        } elseif ($date_max < $date) {
            $date_temp = $date;
            $date = $date_max;
            $date_max = $date_temp;
        }

        $table = 'sejour';
        if (
            (is_array($this->services_list_filter) && count($this->services_list_filter))
            || ($this->service_filter && $this->service_filter->_id)
        ) {
            $table = 'affectation';
        }

        $this->request->addWhere("`{$table}`.`entree` <= '" . $this->getDateMax($date_max) . "'");
        $this->request->addWhere("`{$table}`.`sortie` >= '" . $this->getDateMin($date) . "'");

        $this->request->addLJoinClause('patients', '`patients`.`patient_id` = `sejour`.`patient_id`');

        $this->setOrder($order_column, $order_way);

        if (is_int($start) || is_int($count)) {
            $this->request->setLimit(is_int($start) ? "{$start}, {$count}" : "{$count}");
        }

        return $this->request;
    }

    /**
     * Initialize the CRequest object for making queries on the sejour table.
     * Take all the filters into account
     *
     * @return void
     */
    protected function setSejourRequest(): void
    {
        parent::setSejourRequest();

        $this->filterSejoursByRealEntry();
    }

    /**
     * Returns the min datetime for filtering the admissions in string format from the given DateTimeImmutable object,
     * by taking into account the period filter
     *
     * @param DateTimeImmutable $date
     *
     * @return string
     */
    public function getDateMin(DateTimeImmutable $date): string
    {
        $date_min = parent::getDateMin($date);

        if ($this->hour_filter) {
            $date_min = $date->format("Y-m-d {$this->hour_filter}");
        }

        return $date_min;
    }

    /**
     * Returns the max datetime for filtering the admissions in string format from the given DateTimeImmutable object,
     * by taking into account the period filter
     *
     * @param DateTimeImmutable $date
     *
     * @return string
     */
    public function getDateMax(DateTimeImmutable $date): string
    {

        $date_max = parent::getDateMax($date);

        if ($this->hour_filter) {
            $date_max = $date->format("Y-m-d {$this->hour_filter}");
        }


        return $date_max;
    }

    /**
     * Adds the left join and where clauses for filtering the sejours by service.
     *
     * Filter by CService id, or by a list of CService ids
     *
     * @return void
     */
    protected function filterSejoursByService(): void
    {
        if ($this->service_filter && $this->service_filter->_id) {
            $this->request->addLJoinClause('affectation', '`affectation`.`sejour_id` = `sejour`.`sejour_id`');
            $this->request->addWhere("`affectation`.`service_id` = {$this->service_filter->_id}");
        } elseif (count($this->services_list_filter)) {
            $this->request->addLJoinClause('affectation', '`affectation`.`sejour_id` = `sejour`.`sejour_id`');
            $this->request->addWhere(
                '`affectation`.`service_id` ' . CSQLDataSource::prepareIn($this->services_list_filter)
            );
        }
    }

    /**
     * Adds a where clause that checks is the real entry date is set
     *
     * @return void
     */
    protected function filterSejoursByRealEntry(): void
    {
        if ($this->real_entry_filter) {
            $this->request->addWhere('`sejour`.`entree_reelle` IS NOT NULL');
        }
    }

    /**
     * Sets the order by clause of the request
     *
     * Allow to order by the following columns : entree_prevue, entree_reelle, pec_acceuil, pec_service, sortie_prevue,
     * by service name (on the affectation), by practitioner name, by patient name
     *
     * @param string $column
     * @param string $way
     *
     * @return void
     */
    protected function setOrder(string $column, string $way): void
    {
        if (!in_array($way, ['ASC', 'DESC'])) {
            $way = 'ASC';
        }

        switch ($column) {
            case 'entree':
            case 'sortie':
                $this->request->addOrder(
                    "`sejour`.`{$column}` {$way}, `patients`.`nom`, `patients`.`prenom`"
                );
                break;
            case '_chambre':
                if (!array_key_exists('affectation', $this->request->ljoin)) {
                    $this->request->addLJoinClause('affectation', '`affectation`.`sejour_id` = `sejour`.`sejour_id`');
                }

                $this->request->addLJoinClause('lit', '`lit`.`lit_id` = `affectation`.`lit_id`');
                $this->request->addLJoinClause('chambre', '`chambre`.`chambre_id` = `lit`.`chambre_id`');

                $this->request->addOrder(
                    "ISNULL(`chambre`.`rank`), `chambre`.`rank` {$way}, `chambre`.`nom` {$way},"
                    . ' `patients`.`nom`, `patients`.`prenom`, `sejour`.`entree`'
                );
                break;
            case 'praticien_id':
                $this->request->addLJoinClause('users', '`users`.`user_id` = `sejour`.`praticien_id`');
                $this->request->addOrder(
                    "`users`.`user_last_name` {$way}, `users`.`user_first_name`,"
                    . " `sejour`.`entree`"
                );
                break;
            case 'patient_name':
            case 'patient_id':
            default:
                $this->request->addOrder(
                    "`patients`.`nom` {$way}, `patients`.`prenom` {$way}, `sejour`.`entree`"
                );
                break;
        }
    }
}
