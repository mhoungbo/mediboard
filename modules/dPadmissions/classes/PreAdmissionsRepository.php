<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions;

use DateTimeImmutable;
use Exception;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * A repository class that loads the anaesthesia consultations for a given date
 */
class PreAdmissionsRepository
{
    public const FILTER_PERIOD_MORNING   = 'matin';
    public const FILTER_PERIOD_AFTERNOON = 'soir';

    protected CGroups $group;
    protected CMediusers $user;

    /** @var CSejour Used for loading the objects, mostly useful for testing purpose */
    protected CConsultation $consultation_object;
    /** @var CSejour Used for loading the objects, mostly useful for testing purpose */
    protected CSejour $sejour_object;

    /** @var CMediusers[]  */
    protected array $anesthesists = [];

    protected ?CRequest $request = null;

    protected ?string $period_filter = null;
    protected ?string $time_change_period = null;

    /**
     * @param CGroups    $group
     * @param CMediusers $user
     */
    public function __construct(
        CGroups $group,
        CMediusers $user,
        ?CConsultation $consultation_object = null,
        ?CSejour $sejour_object = null
    ) {
        $this->group = $group;
        $this->user = $user;

        $this->consultation_object = $consultation_object ?? new CConsultation();
        $this->sejour_object = $sejour_object ?? new CSejour();
    }

    /**
     * Sets a filter that will limit the preadmissions to the morning or the afternoon
     *
     * @param string $period      The period (either morning or afternoon)
     * @param string $change_time The exact time on which the period will change
     *
     * @return void
     */
    public function setPeriodFilter(string $period, string $change_time): void
    {
        if (in_array($period, [self::FILTER_PERIOD_AFTERNOON, self::FILTER_PERIOD_MORNING])) {
            $this->period_filter = $period;
            $this->time_change_period = $change_time;
        }
    }

    /**
     * Loads the anaesthetists for which the user has the specified right
     *
     * @return CMediusers[]
     */
    protected function loadAnaesthetists(int $perm_type = PERM_READ): array
    {
        if (!count($this->anesthesists)) {
            $this->anesthesists = $this->user->loadAnesthesistes($perm_type);
        }

        return $this->anesthesists;
    }

    /**
     * Counts the anaesthesia consultations for the month of the specified date
     *
     * @param DateTimeImmutable $date
     *
     * @return int
     */
    public function countAnaesthesiaConsultations(DateTimeImmutable $date): int
    {
        $this->prepareCountAnesthesiaConsultationsRequest($date);

        try {
            $count = $this->consultation_object->countList($this->request->where, null, $this->request->ljoin);

            if (!is_int($count) && is_numeric($count)) {
                $count = intval($count);
            } elseif (!is_int($count)) {
                $count = 0;
            }
        } catch (Exception $e) {
            $count = 0;
        }

        return $count;
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function prepareCountAnesthesiaConsultationsRequest(DateTimeImmutable $date): CRequest
    {
        $this->prepareRequest();

        $begin = $date->modify('first day of this month');
        $end   = $date->modify('last day of this month');

        $this->request->addWhere(
            "`plageconsult`.`date` BETWEEN '"
            . $begin->format('Y-m-d') . "' AND '" . $end->format('Y-m-d') . "'"
        );

        return $this->request;
    }

    /**
     * Counts the number of anaesthesia consultations for each day of the month (from the specified date)
     *
     * @param DateTimeImmutable $date
     *
     * @return array
     */
    public function countAnaesthesiaConsultationsPerDay(DateTimeImmutable $date): array
    {
        $this->prepareCountAnaesthesiaConsultationsPerDayRequest($date);

        try {
            $counters = $this->consultation_object->countMultipleList(
                $this->request->where,
                $this->request->order,
                $this->request->group,
                $this->request->ljoin,
                ['plageconsult.date']
            );

            if (!is_array($counters)) {
                $counters = [];
            }
        } catch (Exception $e) {
            $counters = [];
        }

        return $counters;
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function prepareCountAnaesthesiaConsultationsPerDayRequest(DateTimeImmutable $date): CRequest
    {
        $this->prepareCountAnesthesiaConsultationsRequest($date);

        $this->request->addOrder('`plageconsult`.`date`');
        $this->request->addGroup('`plageconsult`.`date`');

        return $this->request;
    }

    /**
     * Loads the anaesthesia consultations for the specified date, matching the filters
     *
     * @param DateTimeImmutable $date
     * @param string            $order_column Order by patient or by hour (patient_id or heure)
     * @param string            $order_way
     *
     * @return CConsultation[]
     */
    public function getAnaesthesiaConsultations(
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC'
    ): array {

        $this->prepareGetAnaesthesiaConsultationsRequest($date, $order_column, $order_way);

        try {
            $consultations = $this->consultation_object->loadListByReq($this->request);

            if (!is_array($consultations)) {
                $consultations = [];
            }
        } catch (Exception $e) {
            $consultations = [];
        }

        return $consultations;
    }

    /**
     * @param DateTimeImmutable $date
     * @param string            $order_column
     * @param string            $order_way
     *
     * @return CRequest
     */
    public function prepareGetAnaesthesiaConsultationsRequest(
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC'
    ): CRequest {
        $this->prepareRequest();

        $this->request->addWhere("`plageconsult`.`date` = '" . $date->format('Y-m-d') . "'");

        if (!in_array($order_way, ['ASC', 'DESC'])) {
            $order_way = 'ASC';
        }

        if ($this->period_filter && $this->time_change_period) {
            switch ($this->period_filter) {
                case self::FILTER_PERIOD_MORNING:
                    $this->request->addWhere("`consultation`.`heure` < '{$this->time_change_period}'");
                    break;
                case self::FILTER_PERIOD_AFTERNOON:
                default:
                    $this->request->addWhere("`consultation`.`heure` >= '{$this->time_change_period}'");
            }
        }

        switch ($order_column) {
            case 'heure':
                $this->request->addOrder("`consultation`.`heure` {$order_way}");
                break;
            case 'patient_id':
            default:
                $this->request->addLJoinClause('patients', '`consultation`.`patient_id` = `patients`.`patient_id`');
                $this->request->addOrder(
                    "`patients`.`nom` {$order_way}, `patients`.`prenom` {$order_way}, `consultation`.`heure`"
                );
        }

        return $this->request;
    }

    /**
     * Load the list of sejours that are not linked to an anaesthesia consultation, but have their admission prepared
     *
     * @param DateTimeImmutable $date
     * @param array             $sejours_ids A list of CSejour ids to exclude from the request
     * @param array             $type_pec
     *
     * @return array
     */
    public function getPreparedSejoursWithoutAnaesthesiaRecords(
        DateTimeImmutable $date,
        array $sejours_ids = [],
        array $type_pec = []
    ): array {
        $this->getPreparedSejoursWithoutAnaesthesiaRecordsRequest($date, $sejours_ids, $type_pec);

        try {
            $sejours = $this->sejour_object->loadListByReq($this->request);

            if (!is_array($sejours)) {
                $sejours = [];
            }
        } catch (Exception $e) {
            $sejours = [];
        }

        return $sejours;
    }

    /**
     * @param DateTimeImmutable $date
     * @param array             $sejours_ids
     * @param array             $type_pec
     *
     * @return CRequest
     */
    public function getPreparedSejoursWithoutAnaesthesiaRecordsRequest(
        DateTimeImmutable $date,
        array $sejours_ids = [],
        array $type_pec = []
    ): CRequest {
        $this->request = new CRequest();
        $this->request->addLJoinClause('operations', '`operations`.`sejour_id` = `sejour`.`sejour_id`');
        $this->request->addLJoin('`consultation_anesth` AS c1 ON c1.`sejour_id` = `sejour`.`sejour_id`');
        $this->request->addLJoin('`consultation_anesth` AS c2 ON c2.`operation_id` = `operations`.`operation_id`');

        $this->request->addWhere("`sejour`.`entree_preparee` = '1'");
        $this->request->addWhere(
            "`sejour`.`entree_preparee_date` BETWEEN '" . $date->format('Y-m-d 00:00:00')
            . "' AND '" . $date->format('Y-m-d 23:59:59') . "'"
        );
        $this->request->addWhere('c1.`consultation_anesth_id` IS NULL');
        $this->request->addWhere('c2.`consultation_anesth_id` IS NULL');
        $this->request->addWhere("`sejour`.`group_id` = {$this->group->_id}");

        if (count($sejours_ids)) {
            $this->request->addWhere('`sejour`.`sejour_id` ' . CSQLDataSource::prepareNotIn($sejours_ids));
        }

        if (count($type_pec)) {
            $this->request->addWhere('`sejour`.`type_pec` ' . CSQLDataSource::prepareIn($type_pec));
        }

        return $this->request;
    }

    /**
     * @return void
     */
    protected function prepareRequest(): void
    {
        $this->request = new CRequest();

        $this->request->addTable('consultation');

        $this->request->addLJoinClause(
            'plageconsult',
            '`consultation`.`plageconsult_id` = `plageconsult`.`plageconsult_id`'
        );

        $this->request->addWhere('`consultation`.`patient_id` IS NOT NULL');
        $this->request->addWhere("`consultation`.`annule` = '0'");

        $this->loadAnaesthetists();

        $this->request->addWhere(
            '`plageconsult`.`chir_id` ' . CSQLDataSource::prepareIn(array_keys($this->anesthesists))
        );
    }

    /**
     * Returns the min datetime for filtering the admissions in string format from the given DateTimeImmutable object,
     * by taking into account the period filter
     *
     * @param DateTimeImmutable $date
     *
     * @return string
     */
    public function getDateMin(DateTimeImmutable $date): string
    {
        $date_min = $date->format('Y-m-d 00:00:00');
        if (
            $this->period_filter && $this->time_change_period
            && $this->period_filter === AdmissionsRepository::FILTER_PERIOD_AFTERNOON
        ) {
            $date_min = $date->format("Y-m-d {$this->time_change_period}");
        }

        return $date_min;
    }

    /**
     * Returns the max datetime for filtering the admissions in string format from the given DateTimeImmutable object,
     * by taking into account the period filter
     *
     * @param DateTimeImmutable $date
     *
     * @return string
     */
    public function getDateMax(DateTimeImmutable $date): string
    {
        $date_max = $date->format('Y-m-d 23:59:59');
        if (
            $this->period_filter && $this->time_change_period
            && $this->period_filter === AdmissionsRepository::FILTER_PERIOD_MORNING
        ) {
            $date_max = $date->format("Y-m-d {$this->time_change_period}");
        }

        return $date_max;
    }
}
