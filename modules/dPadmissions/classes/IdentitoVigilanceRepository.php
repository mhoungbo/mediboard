<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions;

use DateTimeImmutable;
use Exception;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Urgences\CRPU;

/**
 * A class that checks if the patients present at the given date (and their sejours or operations)
 * are potential duplicates or not
 */
class IdentitoVigilanceRepository
{
    protected CSejour $sejour_object;

    protected DateTimeImmutable $date;
    protected DateTimeImmutable $date_min;
    protected DateTimeImmutable $date_max;

    protected bool $cancelled_filter = false;

    protected CGroups $group;
    protected bool $handle_emergencies = false;

    protected ?CRequest $request = null;

    /** @var CSejour[] */
    protected array $sejours = [];

    /** @var CPatient[] */
    protected array $patients = [];

    protected array $guesses = [];
    protected int $mergeable_count = 0;

    /** @var bool A behavior field used for disabling massloads during tests */
    protected bool $enable_massloads = true;

    /**
     * @param CGroups           $group
     * @param DateTimeImmutable $date
     * @param bool              $display_yesterday If true, the minimum date to filter the sejours will be set
     *                                             to the previous day, from the given date
     * @param ?CSejour          $sejour_object     Used for injecting a mock of CSejour during tests
     */
    public function __construct(
        CGroups $group,
        DateTimeImmutable $date,
        bool $display_yesterday = false,
        ?CSejour $sejour_object = null
    ) {
        $this->group = $group;

        $this->date = $date;
        $this->date_min = $display_yesterday ? $date->modify('-1 day') : $date;
        $this->date_max = $date->modify('+1 day');

        $this->sejour_object = $sejour_object ?? new CSejour();
    }

    /**
     * If true, the cancelled sejours will be loaded
     *
     * @param bool $cancelled
     *
     * @return void
     */
    public function setCancelledFilter(bool $cancelled): void
    {
        $this->cancelled_filter = $cancelled;
    }

    /**
     * If set to true, only the sejours of type emergency will be loaded.
     * The duplicates search will also cover CRPU
     *
     * @param bool $value
     *
     * @return void
     */
    public function setHandleEmergencies(bool $value): void
    {
        $this->handle_emergencies = $value;
    }

    /**
     * @return void
     */
    public function disableMassLoads(): void
    {
        $this->enable_massloads = false;
    }

    /**
     * Checks for each patient if there are potential duplicates of patient, sejours, operations and rpus
     *
     * @return array
     * @throws Exception
     */
    public function guessPossibleMerges(): array
    {
        $this->loadSejours();
        $this->prepareSejours();
        $this->loadPatients();

        foreach ($this->patients as $patient_id => $patient) {
            $guess = [];

            $guess['mergeable'] = array_key_exists($patient_id, $this->guesses);

            $siblings = $patient->getSiblings();
            $guess['siblings'] = array_keys($siblings);
            foreach ($guess['siblings'] as $sibling_id) {
                if (array_key_exists($sibling_id, $this->patients)) {
                    $this->guesses[$sibling_id]['mergeable'] = true;
                    $guess['mergeable'] = true;
                }
            }

            $phoning = $patient->getPhoning($this->date->format('Y-m-d'));
            $guess['phonings'] = array_keys($phoning);
            foreach ($guess['phonings'] as $phoning_id) {
                if (array_key_exists($phoning_id, $this->patients)) {
                    $this->guesses[$phoning_id]['mergeable'] = true;
                    $guess['mergeable'] = true;
                }
            }

            if (count($patient->_ref_sejours) > 1) {
                $guess['mergeable'] = true;
            }

            foreach ($patient->_ref_sejours as $sejour) {
                $operations = $sejour->loadRefsOperations();
                foreach ($operations as $operation) {
                    $operation->loadRefPlageOp();
                }

                if (count($operations) > 1) {
                    $guess['mergeable'] = true;
                }

                if ($this->handle_emergencies && count($sejour->_back['rpu']) > 1) {
                    $guess['mergeable'] = true;
                }
            }

            if ($guess['mergeable']) {
                $this->mergeable_count++;
            }

            $this->guesses[$patient_id] = $guess;
        }

        return $this->guesses;
    }

    /**
     * @return CPatient[]
     */
    public function getPatients(): array
    {
        return $this->patients;
    }

    /**
     * @return CSejour[]
     */
    public function getSejours(): array
    {
        return $this->sejours;
    }

    /**
     * @return int
     */
    public function getMergeableCount(): int
    {
        return $this->mergeable_count;
    }

    /**
     * Loads the sejours with the given filters (group, date, cancelled, and emergency)
     *
     * @return CSejour[]
     * @throws Exception
     */
    protected function loadSejours(): array
    {
        $this->getLoadSejoursRequest();

        try {
            $sejours = $this->sejour_object->loadListByReq($this->request);

            if (!is_array($sejours)) {
                $sejours = [];
            }
        } catch (Exception $e) {
            $sejours = [];
        }

        $this->sejours = $sejours;

        return $this->sejours;
    }

    /**
     * @return CRequest
     */
    public function getLoadSejoursRequest(): CRequest
    {
        $this->request = new CRequest();
        $this->request->addWhereClause(
            'entree',
            "BETWEEN '" . $this->date_min->format('Y-m-d') . "' AND '" . $this->date_max->format('Y-m-d') . "'"
        );

        $this->request->addWhereClause('group_id', " = {$this->group->_id}");

        if ($this->handle_emergencies) {
            $this->request->addWhereClause('type', CSQLDataSource::prepareIn(CSejour::getTypesSejoursUrgence()));
        }

        if (!$this->cancelled_filter) {
            $this->request->addWhereClause('annule', " = '0'");
        }

        $this->request->addOrder('entree');

        return $this->request;
    }

    /**
     * Massloads the references needed for guessing the duplicates
     *
     * @return void
     * @throws Exception
     */
    protected function prepareSejours(): void
    {
        if ($this->enable_massloads) {
            CSejour::massLoadNDA($this->sejours);
            CStoredObject::massLoadFwdRef($this->sejours, 'patient_id');
            $operations = CStoredObject::massLoadBackRefs(
                $this->sejours,
                'operations',
                'date ASC',
                ['annulee' => " = '0'"]
            );
            CStoredObject::massLoadFwdRef($operations, 'plageop_id');
        }
    }

    /**
     * Loads the patient for each sejour, and sets the backref sejour only from the list of sejour
     * (aka does not load all the sejours of the patient).
     * Also order the patient by name.
     *
     * @return CPatient[]
     * @throws Exception
     */
    protected function loadPatients(): array
    {
        foreach ($this->sejours as $sejour) {
            if ($this->handle_emergencies) {
                $this->loadRpuForSejour($sejour);
            }

            $sejour->loadNDA();
            $sejour->loadRefPatient();
            if (!array_key_exists($sejour->patient_id, $this->patients)) {
                $sejour->_ref_patient->_ref_sejours = [];
                $this->patients[$sejour->patient_id] = $sejour->_ref_patient;
            }

            $sejour->_ref_patient->_ref_sejours[$sejour->_id] = $sejour;
        }

        if ($this->enable_massloads) {
            CPatient::massLoadIPP($this->patients);
        }

        uasort($this->patients, function ($a, $b) {
            return strnatcmp($a->nom, $b->nom);
        });

        return $this->patients;
    }

    /**
     * Simulate the loading of the RPU because the loading of a CRPU is resource consuming
     * (because of some objects loading of the CSejour in CRPU->updateFormFields()
     *
     * @param CSejour $sejour
     *
     * @return void
     * @throws Exception
     */
    protected function loadRpuForSejour(CSejour $sejour): void
    {
        $sejour->_back['rpu'] = [];
        foreach ($sejour->loadBackIds('rpu') as $rpu_id) {
            $rpu = new CRPU();
            $rpu->_id = $rpu_id;
            $sejour->_back['rpu'][$rpu->_id] = $rpu;
        }
    }
}
