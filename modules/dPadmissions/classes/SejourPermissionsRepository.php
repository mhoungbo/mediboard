<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions;

use DateTimeImmutable;
use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * A class that loads Affectations, based on the AdmissionsService class
 */
class SejourPermissionsRepository extends AdmissionsRepository
{
    /** @var string The table on which the request will be made */
    protected string $table = 'affectation';

    /**
     * Count the number of affectations with an entry date equals to the given date, and by using the filters
     *
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function getCountSejoursPerDayRequest(DateTimeImmutable $date): CRequest
    {
        $this->setSejourRequest();

        $begin = $date->modify('first day of this month');
        $end = $date->modify('last day of this month');
        $this->request->addWhere(
            "`affectation`.`{$this->sejour_view_field}` BETWEEN '" . $begin->format('Y-m-d') . ' 00:00:00'
            . "' AND '" . $end->format('Y-m-d') . " 23:59:59'"
        );

        $this->request->addColumn("DATE_FORMAT(`affectation`.`{$this->sejour_view_field}`, '%Y-%m-%d')", 'date');
        $this->request->addColumn('COUNT(DISTINCT `affectation`.`sejour_id`)', 'count');
        $this->request->addGroup('date');
        $this->request->addOrder('date');

        return $this->request;
    }

    /**
     * @param DateTimeImmutable $date
     *
     * @return CRequest
     */
    public function getCountMonthlySejoursRequest(DateTimeImmutable $date): CRequest
    {
        $this->setSejourRequest();

        $begin = $date->modify('first day of this month');
        $end = $date->modify('last day of this month');
        $this->request->addWhere(
            "`affectation`.`{$this->sejour_view_field}` BETWEEN '" . $begin->format('Y-m-d') . ' 00:00:00'
            . "' AND '" . $end->format('Y-m-d') . " 23:59:59'"
        );

        $this->request->addColumn('COUNT(DISTINCT `affectation`.`sejour_id`)', 'count');

        return $this->request;
    }

    /**
     * Load the sejours that have an entry date equals to the given date, and by using the filters.
     * Allow to order the sejours by patient, practitioner, operation date and entry date.
     *
     * The results can use a SQL limit by setting the start and count parameters
     *
     * @param DateTimeImmutable      $date
     * @param string                 $order_column
     * @param string                 $order_way
     * @param int|null               $start
     * @param int|null               $count
     * @param DateTimeImmutable|null $date_max
     * @param ?CMbObject                $sejour_object An optional CAffectation object, mostly used for testing purpose
     *
     * @return CAffectation[]
     */
    public function loadSejours(
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC',
        ?int $start = null,
        ?int $count = null,
        ?DateTimeImmutable $date_max = null,
        ?CMbObject $sejour_object = null
    ): array {
        $this->getLoadSejoursRequest($date, $order_column, $order_way, $start, $count, $date_max);
        $sejour_object = $sejour_object ?? new CAffectation();

        try {
            $affectations = $sejour_object->loadListByReq($this->request);

            if (!$affectations) {
                $affectations = [];
            }
        } catch (Exception $e) {
            $affectations = [];
        }

        return $affectations;
    }

    /**
     * Load the sejours that have an entry date equals to the given date, and by using the filters.
     * Allow to order the sejours by patient, practitioner, operation date and entry date.
     *
     * The sejour will be filtered by permission type
     *
     * The results can use a SQL limit by setting the start and count parameters
     *
     * @param int                    $perm_type*
     * @param DateTimeImmutable      $date
     * @param string                 $order_column
     * @param string                 $order_way
     * @param int|null               $start
     * @param int|null               $count
     * @param DateTimeImmutable|null $date_max
     * @param ?CMbObject             $sejour_object An optional CSejour object, mostly used for testing purpose
     *
     * @return CAffectation[]
     */
    public function loadSejoursByPerms(
        int $perm_type,
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC',
        ?int $start = null,
        ?int $count = null,
        ?DateTimeImmutable $date_max = null,
        ?CMbObject $sejour_object = null
    ): array {
        $this->getLoadSejoursRequest($date, $order_column, $order_way, $start, $count, $date_max);

        $sejour_object = $sejour_object ?? new CAffectation();
        try {
            $affectations = $sejour_object->loadListWithPerms(
                $perm_type,
                $this->request->where,
                $this->request->order,
                $this->request->limit,
                $this->request->group,
                $this->request->ljoin
            );

            if (!$affectations) {
                $affectations = [];
            }
        } catch (Exception $e) {
            $affectations = [];
        }

        return $affectations;
    }

    /**
     * Count the sejours that have an entry date equals to the given date, and by using the filters.
     *
     * @param DateTimeImmutable      $date
     * @param DateTimeImmutable|null $date_max
     * @param ?CMbObject             $sejour_object An optional CAffectation object, mostly used for testing purpose
     *
     * @return int
     */
    public function countSejours(
        DateTimeImmutable $date,
        ?DateTimeImmutable $date_max = null,
        ?CMbObject $sejour_object = null
    ): int {
        $this->getLoadSejoursRequest($date, '', '', null, null, $date_max);

        $sejour_object = $sejour_object ?? new CAffectation();
        try {
            $total = $sejour_object->countList(
                $this->request->where,
                null,
                $this->request->ljoin
            );

            if (!is_int($total) && is_numeric($total)) {
                $total = intval($total);
            } elseif (!is_int($total)) {
                $total = 0;
            }
        } catch (Exception $e) {
            $total = 0;
        }

        return $total;
    }


    /**
     * Prepare the CRequest for loading the admissions
     *
     * @param DateTimeImmutable      $date
     * @param string                 $order_column
     * @param string                 $order_way
     * @param int|null               $start
     * @param int|null               $count
     * @param DateTimeImmutable|null $date_max
     *
     * @return void
     */
    public function getLoadSejoursRequest(
        DateTimeImmutable $date,
        string $order_column = 'patient_id',
        string $order_way = 'ASC',
        ?int $start = null,
        ?int $count = null,
        ?DateTimeImmutable $date_max = null
    ): CRequest {
        $this->setSejourRequest();

        $this->request->addColumn('`affectation`.*');

        if (!$date_max) {
            $date_max = $date;
        } elseif ($date_max < $date) {
            $date_temp = $date;
            $date = $date_max;
            $date_max = $date_temp;
        }

        $this->request->addWhere(
            "`affectation`.`{$this->sejour_view_field}` BETWEEN '" . $this->getDateMin($date)
            . "' AND '" . $this->getDateMax($date_max) . "'"
        );

        $this->request->addLJoinClause('patients', '`patients`.`patient_id` = `sejour`.`patient_id`');

        $this->setOrder($order_column, $order_way);

        if (is_int($count)) {
            $this->request->setLimit(is_int($start) ? "{$start}, {$count}" : "{$count}");
        }

        return $this->request;
    }

    /**
     * Initialize the CRequest object for making queries on the sejour table.
     * Take all the filters into account
     *
     * @return void
     */
    protected function setSejourRequest(): void
    {
        $this->request = new CRequest();
        $this->request->addTable($this->table);

        $this->request->addWhere("`sejour`.`group_id` = '{$this->group->_id}'");
        $this->request->addLJoinClause('sejour', '`sejour`.`sejour_id` = `affectation`.`sejour_id`');

        $this->filterSejoursByUser();
        $this->filterSejoursByService();
        $this->filterByFunction();
        $this->filterSejoursByType();
        $this->filterSejoursByRecuse();
        $this->filterSejoursByLeaveModes();
        $this->filterCancelledSejours();
        $this->filterConfirmedSejours();
        $this->filterSejoursByTypePec();
        $this->filterSejoursByCircuitAmbu();
        $this->filterSejoursWithOperationDateEqualsToEntry();
        $this->filterSejoursByPaymentExceedingFees();
        $this->filterSejoursByPrestations();
        $this->filterUnpreparedSejours();
        $this->filterSejoursByStatusPec();
        $this->filterSejoursWithoutRealLeaveDate();
        $this->filterScheduledSejours();
        $this->filterSejoursByNotificationStatus();
    }

    /**
     * Adds the left join and where clauses for filtering the sejours by service.
     *
     * Filter by CService id, or by a list of CService ids
     *
     * @return void
     */
    protected function filterSejoursByService(): void
    {
        if ($this->service_filter && $this->service_filter->_id) {
            $this->request->addWhere("`affectation`.`service_id` = {$this->service_filter->_id}");
        } elseif (count($this->services_list_filter)) {
            $this->request->addWhere(
                '`affectation`.`service_id` ' . CSQLDataSource::prepareIn($this->services_list_filter)
            );
        }
    }

    /**
     * @param string $column
     * @param string $way
     *
     * @return void
     */
    protected function setOrder(string $column, string $way): void
    {
        if (!in_array($way, ['ASC', 'DESC'])) {
            $way = 'ASC';
        }

        switch ($column) {
            case 'entree':
            case 'sortie':
                $this->request->addOrder("`affectation`.`{$column}` {$way}, `patients`.`nom`, `patients`.`prenom`");
                break;
            case 'praticien_id':
                $this->request->addLJoinClause('users', '`users`.`user_id` = `sejour`.`praticien_id`');
                $this->request->addOrder(
                    "`users`.`user_last_name` {$way}, `users`.`user_first_name` {$way}, "
                    . '`patients`.`nom`, `patients`.`prenom`'
                );
                break;
            case 'patient_id':
            default:
                $this->request->addOrder(
                    "`patients`.`nom` {$way}, `patients`.`prenom` {$way}, `affectation`.`{$this->sejour_view_field}`"
                );
                break;
        }
    }
}
