/**
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

PatientReception = {
    form: null,

    initialize: function (form) {
        this.form = form;
        this.list();
    },

    list: function () {
        new Url('admissions', 'listPatientReceptions')
            .addFormData(this.form)
            .addParam('type_pec[]', $V(this.form.elements['type_pec[]']), true)
            .addParam('only_list', 1)
            .addParam('only_list', 1)
            .addParam('date_interv_eg_entree', $V(this.form.elements['_date_interv_eg_entree']) ? 1 : 0)
            .requestUpdate('patient_receptions_list', {
                onComplete: $('patient_receptions_list').fixedTableHeaders.bind($('patient_receptions_list'))
            });
    },

    sortBy: function (order_col, order_way) {
        $V(this.form.elements['order_col'], order_col);
        $V(this.form.elements['order_way'], order_way);
        this.list();
    },

    reloadLine: function (sejour_id) {
        new Url('admissions', 'showPatientReception')
            .addParam('sejour_id', sejour_id)
            .requestUpdate('line_CSejour-' + sejour_id);
    },

    selectServices: function (view, show_np) {
        new Url('hospi', 'ajax_select_services')
            .addParam('view', view)
            .addParam('ajax_request', 0)
            .addParam('show_np', show_np)
            .requestModal(null, null, {
                maxHeight: '95%',
                onClose: this.list.bind(this)
            });
    }
};
