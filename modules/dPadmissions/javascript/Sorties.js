/**
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Sorties = {
    form:                              null,
    totalUpdater:                      null,
    listUpdater:                       null,
    table_id:                          null,

    sejour_enfants_ids: null,

    initializeSortiesView: function (auto_refresh_frequency) {
        this.form = getForm('selType');
        const listSortiesId = 'listSorties';
        const counterSortiesId = 'allSorties';
        this.table_id = Admissions.table_id = 'listSorties';

        let totalUrl = new Url('admissions', 'sortiesCounters');
        let listUrl = new Url('admissions', 'sortiesList');

        if (auto_refresh_frequency !== 'never') {
            this.totalUpdater = totalUrl.periodicalUpdate(counterSortiesId, {frequency: auto_refresh_frequency});
            this.listUpdater = listUrl.periodicalUpdate(listSortiesId, {
                frequency: auto_refresh_frequency,
                onCreate: function () {
                    WaitingMessage.cover($(listSortiesId));
                    Admissions.rememberSelection();
                }
            });
        } else {
            totalUrl.requestUpdate(counterSortiesId);
            listUrl.requestUpdate(listSortiesId, {
                onCreate: function () {
                    WaitingMessage.cover($(listSortiesId));
                    Admissions.rememberSelection();
                }
            });
        }

        $(listSortiesId).fixedTableHeaders();
        $(counterSortiesId).fixedTableHeaders();
    },

    reloadSortiesCounter: function () {
        const url = new Url('admissions', 'sortiesCounters');
        this.setReloadAdmissionsParams(url);
        url.requestUpdate('allSorties');
        this.reloadListSorties();
    },

    reloadListSorties : function (page = 0) {
        const url = new Url('admissions', 'sortiesList');
        this.setReloadAdmissionsParams(url);
        url.addParam('order_col', $V(this.form.order_col));
        url.addParam('order_way', $V(this.form.order_way));
        url.addParam('page', page);
        url.addParam('period', $V(this.form.period));
        url.addParam('filterFunction', $V(this.form.filterFunction));
        url.requestUpdate('listSorties');
    },

    reloadListSortiesByDate: function (elt, date) {
        $V(this.form.date, date);
        let old_selected = elt.up('table').down('tr.selected');
        old_selected.select('td').each(function (td) {
            // Supprimer le style appliqu� sur le nombre d'admissions
            var style = td.readAttribute('style');
            if (/bold/.match(style)) {
                td.writeAttribute('style', '');
            }
        });
        old_selected.removeClassName('selected');

        // Mettre en gras le nombre d'admissions
        var elt_tr = elt.up('tr');
        elt_tr.addClassName('selected');
        var pos = 1;
        if ($V(this.form.selSortis) == 'unprepared') {
            pos = 2;
        } else if ($V(this.form.selSortis) == 'scheduled') {
            pos = 3;
        }
        var td = elt_tr.down('td', pos);
        td.writeAttribute('style', 'font-weight: bold');

        this.reloadListSorties();
    },

    setReloadAdmissionsParams: function (url) {
        url.addParam('date', $V(this.form.date))
            .addParam('selSortis', $V(this.form.selSortis))
            .addParam('type', $V(this.form._type_admission))
            .addParam('service_id', [$V(this.form.service_id)].flatten().join(','))
            .addParam('prat_id', $V(this.form.prat_id))
            .addParam('only_confirmed', $V(this.form.only_confirmed))
            .addParam('active_filter_services', $V(this.form.elements['active_filter_services']))
            .addParam('type_pec[]', $V(this.form.elements['type_pec[]']), true)
            .addParam('circuit_ambu[]', $V(this.form.elements['circuit_ambu[]']), true)
            .addParam('mode_sortie[]', $V(this.form.elements['mode_sortie[]']), true)
            .addParam('prestations_p_ids[]', $V(this.form.prestations_p_ids), true);


        if ($(this.form.name + '_reglement_dh')) {
            url.addParam('reglement_dh', $V(this.form.elements['reglement_dh']));
        }
    },

    reloadSortieLine: function (sejour_id) {
        new Url('admissions', 'sortieLine')
            .addParam('sejour_id', sejour_id)
            .requestUpdate('CSejour-' + sejour_id);
    },

    submitSortie: function (form) {
        if (!Object.isUndefined(form.elements['_sejours_enfants_ids']) && $V(form._modifier_sortie) == 1) {
            this.sejours_enfants_ids = $V(form._sejours_enfants_ids);
            this.sejours_enfants_ids.split(',').each(function (elt) {
                const formSejour = getForm('editFrmCSejour-' + elt);
                if (!Object.isUndefined(formSejour) && formSejour.down('button.tick')) {
                    if (confirm('Voulez-vous effectuer dans un m�me temps la sortie de l\'enfant ' + formSejour.get('patient_view'))) {
                        formSejour.down('button.tick').onclick();
                    }
                }
            });

            this.sejours_enfants_ids = null;
            return onSubmitFormAjax(form, this.reloadSortieLine.bind(this, $V(form.sejour_id)));
        }

        if (this.sejours_enfants_ids !== null && this.sejours_enfants_ids.indexOf($V(form.sejour_id)) != -1) {
            return onSubmitFormAjax(form);
        }

        return onSubmitFormAjax(form, this.reloadSortieLine.bind(this, $V(form.sejour_id)));
    },

    submitMultiple: function (form) {
        return onSubmitFormAjax(form, Sorties.reloadSortiesCounter.bind(Sorties));
    },

    sortBy: function (order_col, order_way) {
        $V(this.form.order_col, order_col);
        $V(this.form.order_way, order_way);
        this.reloadListSorties($V(this.form.page));
    },

    changePage: function (page) {
        $V(this.form.page, page)
        this.reloadListSorties(page);
    },

    filterAdm: function (selSortis) {
        $V(this.form.selSortis, selSortis);
        this.reloadSortiesCounter();
    },

    sortiePreparee: function (sejour_id, value, trigger) {
        const form = getForm('edit_sejour_sortie_preparee');
        $V(form.sejour_id, sejour_id);
        $V(form.sortie_preparee, '' + value);
        $V(form._sortie_preparee_trigger, trigger);
        form.onsubmit();
    },

    printPlanning: function () {
        new Url('admissions', 'printSorties')
            .addParam('date', $V(this.form.date))
            .addParam('type', $V(this.form._type_admission))
            .addParam('service_id', [$V(this.form.service_id)].flatten().join(','))
            .addParam('period', $V(this.form.period))
            .popup(700, 550, 'Sorties');
    },
};
