/**
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

IdentitoVigilance = {
    updater: null,
    guesses: [],
    date:    null,

    init: function (frequency) {
        const url = new Url('dPadmissions', 'listPatientsIdentitoVigilance');
        const form = getForm('Merger');

        // Get extra filter params
        if (form) {
            url.addParam('display_mergeable', form.display_mergeable.checked ? 1 : 0);
            url.addParam('display_yesterday', form.display_yesterday.checked ? 1 : 0);
            url.addParam('display_cancelled', form.display_cancelled.checked ? 1 : 0);
        }

        if (this.date) {
            url.addParam('date', this.date);
        }

        url.addParam('module', App.m);

        this.updater = url.periodicalUpdate('identito_vigilance', {frequency: frequency});
    },

    start: function (delay, frequency) {
        this.stop();
        this.init.delay(delay, frequency);
    },

    stop: function () {
        if (this.updater) {
            this.updater.stop();
        }
    },

    resume: function () {
        if (this.updater) {
            this.updater.resume();
        }
    },

    highlite: function (checkbox) {
        const checked = checkbox.checked;
        IdentitoVigilance[checked ? 'stop' : 'resume']();

        // Hide and empty all inputs
        $$('tbody.CPatient input[type=radio]').each(function (e) {
            e.checked = false;
            e.setVisibility(false);
        });

        // Uncheck and unselect all other inputs
        $$('tbody.CPatient input[type=checkbox]').each(function (e) {
            e.checked = false;
            e.setVisibility(!checked);
        });

        // Recheck checkbox
        checkbox.setVisibility(true);
        if (checked) {
            checkbox.checked = true;
        }

        // Show all possible radios
        const object_class = checkbox.name.split('-')[0];
        const tbody = $(checkbox).up('tbody');
        const inputFirst = 'input[name=' + object_class + '-first]';
        const inputSecond = 'input[name=' + object_class + '-second]';

        // Remove highligths
        $$('.merge-selected').invoke('removeClassName', 'merge-selected');
        $$('.merge-possible').invoke('removeClassName', 'merge-possible');
        $$('.merge-probable').invoke('removeClassName', 'merge-probable');

        if (checked) {
            if (object_class == 'CPatient') {
                $$(inputSecond).each(function (e) {
                    e.setVisibility(!e.descendantOf(tbody));
                });

                const object_id = checkbox.value;

                // Show phoning guesses
                this.guesses[object_id]['phonings'].each(function (phoning_id) {
                    const phoning_guid = object_class + '-' + phoning_id;
                    const div_guessed = $(phoning_guid);
                    if (div_guessed) {
                        div_guessed.addClassName('merge-possible');
                    }
                });

                // Show sibling guesses
                this.guesses[object_id]['siblings'].each(function (sibling_id) {
                    const sibling_guid = object_class + '-' + sibling_id;
                    const div_guessed = $(sibling_guid);
                    if (div_guessed) {
                        div_guessed.addClassName('merge-probable');
                    }
                });
            } else {
                const container = $(checkbox).up();
                $$(inputSecond).each(function (e) {
                    e.setVisibility(e.descendantOf(tbody) && !e.descendantOf(container));
                });
            }
        }

        if (checkbox.checked) {
            tbody.addClassName('merge-selected');
        }
    },

    merge: function (radio) {
        const object_class = radio.name.split('-')[0];
        const first_id = $V(document.Merger[object_class + '-first'])[0];
        const second_id = radio.value;
        new Url('system', 'object_merger')
            .addParam('objects_class', object_class)
            .addParam('objects_id', [first_id, second_id].join('-'))
            .popup(900, 700);
    },

    togglePlayPause: function (button) {
        button.toggleClassName("play");
        button.toggleClassName("pause");
        if (button.hasClassName("play")) {
            this.stop();
        } else {
            this.resume();
        }
    }
};
