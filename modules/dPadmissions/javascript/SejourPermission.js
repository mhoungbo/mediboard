/**
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

SejourPermission = {
    totalUpdater: null,
    listUpdater:  null,
    date:         null,

    initialize: function (date, frequency) {
        this.date = date;
        this.totalUpdater = new Url('dPadmissions', 'sejourPermissionsCounters');
        this.listUpdater = new Url('dPadmissions', 'listSejourPermissions');

        this.totalUpdater.addParam('date', date);
        this.listUpdater.addParam('date', date);

        if (frequency != 'never') {
            this.totalUpdater.periodicalUpdate('allPermissions', {frequency: frequency});
            this.listUpdater.periodicalUpdate('listPermissions', {frequency: frequency});
        } else {
            this.totalUpdater.requestUpdate('allPermissions');
            this.listUpdater.requestUpdate('listPermissions');
        }

        $('listPermissions').fixedTableHeaders();
        $('allPermissions').fixedTableHeaders();
    },

    reloadPermissionCounters: function (filterFunction) {
        const oForm = getForm('selType');
        const url = new Url('dPadmissions', 'sejourPermissionsCounters');
        url.addParam('date', this.date);
        url.requestUpdate('allAdmissions');
        this.reloadPermissions(filterFunction);
    },

    changePage: function (page) {
        $V(getForm('permissionFilters').page, page);
        this.reloadPermissions();
    },

    changeFunction: function (filterFunction) {
        let form = getForm('permissionFilters');
        $V(form.filterFunction, filterFunction);
        $V(form.page, 0);
        this.reloadPermissions();
    },

    reloadPermissions: function () {
        new Url('dPadmissions', 'listSejourPermissions')
            .addFormData(getForm('permissionFilters'))
            .addParam('date', this.date)
            .requestUpdate('listPermissions');
    }
};
