/**
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

MassSorties = {
    form: null,
    sejours: {},

    initialize: function (form) {
        this.form = form;
    },

    listMassSorties: function () {
        new Url('admissions', 'massSortiesList')
            .addFormData(this.form)
            .requestUpdate('see_sejour_masse');
    },

    selectAllSejours: function (checkbox) {
        $('tbl_sejours_masse').select('input[type=checkbox]').each(function (e) {
            if (e.name.indexOf('box-') !== -1 && !e.disabled) {
                e.checked = checkbox.checked;
                e.onchange();
            }
        })
    },

    showCheckedSejours: function () {
        let checked = 0;
        let count = 0;

        $('tbl_sejours_masse').select('input[type=checkbox]').each(function (e) {
            if (e.name.indexOf('box-') >= 0) {
                count++;
                if ($V(e)) {
                    checked++;
                }
            }
        });

        let check_all = $('tbl_sejours_masse').down('input[name=check_all]');
        const valide_sejours = $('btt_valide_sejours');
        valide_sejours.disabled = "disabled";
        check_all.checked = '';
        check_all.style.opacity = '1';

        if (checked) {
            valide_sejours.disabled = '';
            check_all.checked = '1';
            if (checked < count) {
                check_all.style.opacity = '0.5';
            }
        }
    },

    onCheckSejour: function (checkbox, sejour_id) {
        if (!this.sejours.hasOwnProperty(sejour_id)) {
            this.sejours[sejour_id] = {_checked: 0}
        }

        this.sejours[sejour_id]._checked = checkbox.checked ? 1 : 0;
        this.showCheckedSejours();
    },

    validationSejours: function () {
        Modal.confirm($T('mass_sorties-msg-confirm_validation'), {onOK: function () {
            const form = getForm('selectedSejours');
            $V(form.sejours, Object.toJSON(MassSorties.sejours));
            return onSubmitFormAjax(form, MassSorties.emptySejours.bind(MassSorties));
        }});
    },

    emptySejours: function () {
        this.sejours = {};
    }
};
