/**
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

SejourValidation = {
    sejoursCounterUpdater: null,
    sejoursListUpdater: null,
    currentModule: null,
    date: null,
    form: null,

    initializeView: function (date, currentModule, recuse) {
        this.date = date;
        this.currentModule = currentModule;
        this.recuse = recuse ? recuse : '0';
        this.form = getForm('selType');

        this.sejoursCounterUpdater = new Url('admissions', 'sejoursValidationCounter');
        this.sejoursCounterUpdater.addParam('module_view', this.currentModule);
        this.sejoursCounterUpdater.addParam('date', this.date);
        this.sejoursCounterUpdater.periodicalUpdate('allAdmissions', {frequency: 120});

        this.sejoursListUpdater = new Url('admissions', 'listSejoursValidation');
        this.sejoursListUpdater.addParam('recuse', this.recuse);
        this.sejoursListUpdater.addParam('module_view', this.currentModule);
        this.sejoursListUpdater.addParam('date', this.date);
        this.sejoursListUpdater.periodicalUpdate('listAdmissions', {frequency: 120});
    },

    printPlanning: function () {
        const url = new Url('admissions', 'printAdmissions');
        url.addParam('date', this.date);
        url.addParam('service_id', $V(this.form.elements['service_id']));
        url.popup(700, 550, 'Entrees');
    },

    reloadView: function (filterFunction) {
        this.reloadSejoursCounters();
        this.reloadListSejours(filterFunction);
    },

    reloadSejoursCounters: function () {
        new Url('admissions', 'sejoursValidationCounter')
            .addParam('module_view', this.currentModule)
            .addParam('date', this.date)
            .addParam('service_id', $V(this.form.elements['service_id']))
            .addParam('prat_id', $V(this.form.elements['prat_id']))
            .requestUpdate('allAdmissions');
    },

    orderSejours: function (column, way) {
        let filter_function = null;
        if ($V($('filterFunction')) !== '') {
            filter_function = $V($('filterFunction'));
        }

        this.reloadListSejours(filter_function, column, way);
    },

    reloadListSejours : function (filterFunction, order_column, order_way) {
        let url = new Url('admissions', 'listSejoursValidation');
        url.addParam('current_m', this.currentModule);
        url.addParam('recuse', this.recuse);
        url.addParam('date', this.date);
        url.addParam('service_id', $V(this.form.elements['service_id']));
        url.addParam('prat_id', $V(this.form.elements['prat_id']));
        if (!Object.isUndefined(filterFunction)) {
            url.addParam('filterFunction', filterFunction);
        }

        if (order_column) {
            url.addParam('order_column', order_column);
        }

        if (order_way) {
            url.addParam('order_way', order_way);
        }

        url.requestUpdate('listAdmissions');
    }
};
