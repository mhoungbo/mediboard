<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Tests\Unit;

use _PHPStan_7a922a511\Nette\Utils\DateTime;
use DateTimeImmutable;
use Exception;
use Ox\Core\CPDOMySQLDataSource;
use Ox\Mediboard\Admissions\PresentPatientsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Validator\Constraints\Date;

/**
 * A class test for the PresentPatientsRepository
 */
class PresentPatientsRepositoryTest extends OxUnitTestCase
{
    /**
     * @dataProvider countSejoursPerDayProvider
     *
     * @param PresentPatientsRepository $repository
     * @param DateTimeImmutable         $date
     * @param CSejour                   $sejour
     * @param array                     $expected
     *
     * @return void
     */
    public function testCountSejoursPerDay(
        PresentPatientsRepository $repository,
        DateTimeImmutable $date,
        CSejour $sejour,
        array $expected
    ): void {
        $this->assertEquals($expected, $repository->countSejoursPerDay($date, $sejour));
    }

    /**
     * @return array[]
     */
    public function countSejoursPerDayProvider(): array
    {
        $date         = new DateTimeImmutable();
        $service      = new CService();
        $service->_id = $service->service_id = 1;

        $repository_sejour = new PresentPatientsRepository(new CGroups(), new CMediusers());

        $repository_affectation = new PresentPatientsRepository(new CGroups(), new CMediusers());
        $repository_affectation->setServiceFilter($service);

        $sejour_1 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countList'])
            ->getMock();
        $sejour_1->method('countList')->willReturn(10);

        $sejour_2 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countList'])
            ->getMock();
        $sejour_2->method('countList')->willReturn('10');

        $sejour_3 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countList'])
            ->getMock();
        $sejour_3->method('countList')->willReturn(null);

        $sejour_4 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countList'])
            ->getMock();
        $sejour_4->method('countList')->willThrowException(new Exception('SQL error'));

        $sejour_5 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countMultipleList'])
            ->getMock();
        $sejour_5->method('countMultipleList')->willReturn([1, 2, 3]);

        $sejour_6 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countMultipleList'])
            ->getMock();
        $sejour_6->method('countMultipleList')->willReturn(null);

        $expected_12  = [];
        $expected_346 = [];
        $expected_5   = [];

        $begin = $date->modify('first day of this month');
        $end   = $date->modify('last day of this month');
        $i     = $begin;
        while ($i <= $end) {
            $expected_12[$i->format('Y-m-d')]  = 10;
            $expected_346[$i->format('Y-m-d')] = 0;
            $expected_5[$i->format('Y-m-d')]   = 3;
            $i                                 = $i->modify('+1 day');
        }

        $date_exception = $this->getMockBuilder(DateTimeImmutable::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['modify'])
            ->getMock();
        $date_exception->method('modify')->willThrowException(new Exception('DateTime error'));


        return [
            /* Case 1: Filter on sejours, countList returns numeric string */
            [$repository_sejour, $date, $sejour_1, $expected_12],
            /* Case 2: Filter on sejours, countList returns int */
            [$repository_sejour, $date, $sejour_2, $expected_12],
            /* Case 3: Filter on sejours, countList returns null */
            [$repository_sejour, $date, $sejour_3, $expected_346],
            /* Case 4: Filter on sejours, countList throws exception */
            [$repository_sejour, $date, $sejour_4, $expected_346],
            /* Case 5: Filter on affectations, countMultipleList returns array */
            [$repository_affectation, $date, $sejour_5, $expected_5],
            /* Case 6: Filter on affectations, countMultipleList returns null */
            [$repository_affectation, $date, $sejour_6, $expected_346],
            /* Case 7: exception on DateTimeImmutable->modify */
            [$repository_sejour, $date_exception, new CSejour(), []],
        ];
    }

    /**
     * @return void
     */
    public function testGetCountSejoursPerDayByAffectationRequest(): void
    {
        $group        = new CGroups();
        $group->_id   = $group->group_id = 1;
        $service      = new CService();
        $service->_id = $service->service_id = 1;

        $repository = new PresentPatientsRepository($group, new CMediusers());
        $repository->setServiceFilter($service);

        $date_str = (new DateTimeImmutable())->format('Y-m-d');
        $expected = [
            "`sejour`.`group_id` = '1'",
            '`affectation`.`service_id` = 1',
            'affectation.entree' => " <= '{$date_str} 23:59:59'",
            'affectation.sortie' => " >= '{$date_str} 00:00:00'",
        ];

        $this->assertEquals(
            $expected,
            $repository->getCountSejoursPerDayByAffectationRequest(new DateTimeImmutable())->where
        );
    }

    /**
     * @return void
     */
    public function testGetCountSejoursPerDayRequest(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 1;

        $repository = new PresentPatientsRepository($group, new CMediusers());

        $date_str = (new DateTimeImmutable())->format('Y-m-d');
        $expected = [
            "`sejour`.`group_id` = '1'",
            'sejour.entree' => " <= '{$date_str} 23:59:59'",
            'sejour.sortie' => " >= '{$date_str} 00:00:00'",
        ];

        $this->assertEquals(
            $expected,
            $repository->getCountSejoursPerDayRequest(new DateTimeImmutable())->where
        );
    }

    /**
     * @dataProvider countSejoursByServicePerDayProvider
     *
     * @param DateTimeImmutable $date
     * @param CSejour           $sejour
     * @param array             $expected
     *
     * @return void
     * @throws Exception
     */
    public function testCountSejoursByServicePerDay(DateTimeImmutable $date, CSejour $sejour, array $expected): void
    {
        $repository = new PresentPatientsRepository(new CGroups(), new CMediusers());
        $this->assertEquals($expected, $repository->countSejoursByServicePerDay($date, $sejour));
    }

    /**
     * @return array[]
     */
    public function countSejoursByServicePerDayProvider(): array
    {
        $date_exception = $this->getMockBuilder(DateTimeImmutable::class)
            ->onlyMethods(['modify'])
            ->getMock();
        $date_exception->method('modify')->willThrowException(new Exception('DateTime error'));

        $date = new DateTimeImmutable();

        $expected_errors = [
            'counts'   => [],
            'services' => [],
        ];

        $ds_2 = $this->getMockBuilder(CPDOMySQLDataSource::class)
            ->onlyMethods(['loadList'])
            ->getMock();
        $ds_2->method('loadList')->willThrowException(new Exception('SQL Error'));
        $sejour_2 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getDS'])
            ->getMock();
        $sejour_2->method('getDS')->willReturn($ds_2);

        $ds_3 = $this->getMockBuilder(CPDOMySQLDataSource::class)
            ->onlyMethods(['loadList'])
            ->getMock();
        $ds_3->method('loadList')->willReturn(null);
        $sejour_3 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getDS'])
            ->getMock();
        $sejour_3->method('getDS')->willReturn($ds_3);

        $ds_4 = $this->getMockBuilder(CPDOMySQLDataSource::class)
            ->onlyMethods(['loadList'])
            ->getMock();
        $ds_4->method('loadList')->willReturn([
                                                  ['service' => 'AMBU 1'],
                                                  ['total' => 10],
                                                  ['service' => 'HOSP 2', 'total' => 5],
                                                  ['service' => 'HOSP 1', 'total' => 10],
                                                  ['service' => 'AMBU 2', 'total' => 4],
                                                  ['service' => 'AMBU 1', 'total' => 7],
                                              ]);
        $sejour_4 = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getDS'])
            ->getMock();
        $sejour_4->method('getDS')->willReturn($ds_4);

        $expected_success = [
            'counts'   => [],
            'services' => [
                'AMBU 1' => 0,
                'AMBU 2' => 0,
                'HOSP 1' => 0,
                'HOSP 2' => 0,
            ],
        ];
        $begin            = $date->modify('first day of this month');
        $end              = $date->modify('last day of this month');
        $i                = $begin;
        while ($i <= $end) {
            $expected_success['counts'][$i->format('Y-m-d')] = [
                'AMBU 1' => 7,
                'AMBU 2' => 4,
                'HOSP 1' => 10,
                'HOSP 2' => 5,
            ];

            $expected_success['services']['AMBU 1'] += 7;
            $expected_success['services']['AMBU 2'] += 4;
            $expected_success['services']['HOSP 1'] += 10;
            $expected_success['services']['HOSP 2'] += 5;

            $i = $i->modify('+1 day');
        }

        return [
            /* Case 1: Exception on DateTimeImmutable->modify */
            [$date_exception, new CSejour(), $expected_errors],
            /* Case 2: Exception thrown by CPDOMySQLDataSource->loadList */
            [$date, $sejour_2, $expected_errors],
            /* Case 3: Null results */
            [$date, $sejour_3, $expected_errors],
            /* Case 4: Results returned, one without "service" entry, another without 'total' entry,
               the others services in disorder */
            [$date, $sejour_4, $expected_success],
        ];
    }

    /**
     * @return void
     */
    public function testGetCountSejoursByservicePerDayRequest(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $repository = new PresentPatientsRepository($group, new CMediusers());

        $date     = new DateTimeImmutable();
        $date_str = $date->format('Y-m-d');
        $begin    = $date->modify('first day of this month')->format('Y-m-d 23:59:59');
        $end      = $date->modify('last day of this month')->format('Y-m-d 00:00:00');

        $expected = <<<SQL
SELECT COUNT(`affectation`.`affectation_id`) AS `total`,
`service`.`nom` AS `service`
FROM `sejour`
LEFT JOIN `affectation` ON `affectation`.`sejour_id` = `sejour`.`sejour_id`
LEFT JOIN `service` ON `service`.`service_id` = `affectation`.`service_id`
WHERE (`sejour`.`group_id` = '10')
AND (`affectation`.`affectation_id` IS NOT NULL)
AND (`sejour`.`entree` <= '$end' AND `sejour`.`sortie` >= '$begin')
AND (`affectation`.`entree` <= '$date_str 23:59:59')
AND (`affectation`.`sortie` >= '$date_str 00:00:00')
GROUP BY `affectation`.`service_id`
SQL;

        $this->assertEquals($expected, $repository->getCountSejoursByServicePerDayRequest($date)->makeSelect());
    }

    /**
     * @dataProvider getLoadSejoursRequestProvider
     *
     * @param PresentPatientsRepository $repository
     * @param DateTimeImmutable         $date
     * @param DateTimeImmutable|null    $date_max
     * @param int|null                  $start
     * @param int|null                  $count
     * @param string                    $expected
     *
     * @return void
     */
    public function testGetLoadSejoursRequest(
        PresentPatientsRepository $repository,
        DateTimeImmutable $date,
        ?DateTimeImmutable $date_max,
        ?int $start,
        ?int $count,
        string $expected
    ): void {
        $this->assertEquals(
            $expected,
            $repository->getLoadSejoursRequest(
                $date,
                'patient_id',
                'ASC',
                $start,
                $count,
                $date_max
            )->makeSelect()
        );
    }

    /**
     * @return array[]
     */
    public function getLoadSejoursRequestProvider(): array
    {
        $group                  = new CGroups();
        $group->_id             = $group->group_id = 10;
        $repository             = new PresentPatientsRepository($group, new CMediusers());
        $repository_affectation = new PresentPatientsRepository($group, new CMediusers());
        $repository_affectation->setServiceFilter(new CService());

        $date         = new DateTimeImmutable();
        $date_max     = $date->modify('+10 days');
        $date_str     = $date->format('Y-m-d');
        $date_max_str = $date_max->format('Y-m-d');

        $count = 50;
        $start = 0;

        $query_1 = <<<SQL
SELECT `sejour`.*
FROM `sejour`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`sejour`.`entree` <= '{$date_str} 23:59:59')
AND (`sejour`.`sortie` >= '{$date_str} 00:00:00')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `sejour`.`entree`
SQL;

        $query_2 = <<<SQL
SELECT `sejour`.*
FROM `sejour`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`sejour`.`entree` <= '{$date_max_str} 23:59:59')
AND (`sejour`.`sortie` >= '{$date_str} 00:00:00')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `sejour`.`entree`
LIMIT {$count}
SQL;

        $query_3 = <<<SQL
SELECT `sejour`.*
FROM `sejour`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`sejour`.`entree` <= '{$date_max_str} 23:59:59')
AND (`sejour`.`sortie` >= '{$date_str} 00:00:00')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `sejour`.`entree`
LIMIT {$start}, {$count}
SQL;

        return [
            /* Case 1 : no date max, no limit */
            [$repository, $date, null, null, null, $query_1],
            /* Case 2: date max, only count */
            [$repository, $date, $date_max, null, $count, $query_2],
            /* Case 3: check on affectation, date max smaller than date, start and count */
            [$repository_affectation, $date_max, $date, $start, $count, $query_3],
        ];
    }

    /**
     * @return void
     */
    public function testGetDateMin(): void
    {
        $repository = new PresentPatientsRepository(new CGroups(), new CMediusers());
        $date       = new DateTimeImmutable();

        $this->assertEquals($date->format('Y-m-d 00:00:00'), $repository->getDateMin($date));
        $repository->setHourFilter('16:05:00');
        $this->assertEquals($date->format('Y-m-d 16:05:00'), $repository->getDateMin($date));
    }

    /**
     * @return void
     */
    public function testGetDateMax(): void
    {
        $repository = new PresentPatientsRepository(new CGroups(), new CMediusers());
        $date       = new DateTimeImmutable();

        $this->assertEquals($date->format('Y-m-d 23:59:59'), $repository->getDateMax($date));
        $repository->setHourFilter('16:05:00');
        $this->assertEquals($date->format('Y-m-d 16:05:00'), $repository->getDateMax($date));
    }

    /**
     * @dataProvider filterSejoursByServiceProvider
     *
     * @param CGroups $group
     * @param mixed   $filter
     * @param array   $expected_where
     * @param array   $expected_ljoin
     *
     * @return void
     */
    public function testFilterSejoursByService(
        CGroups $group,
        $filter,
        array $expected_where,
        array $expected_ljoin
    ): void {
        $repository = new PresentPatientsRepository($group, new CMediusers());
        if ($filter instanceof CService) {
            $repository->setServiceFilter($filter);
        } elseif (is_array($filter)) {
            $repository->setServicesListFilter($filter);
        }

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @return array
     */
    public function filterSejoursByServiceProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $ljoin = [
            'affectation' => '`affectation`.`sejour_id` = `sejour`.`sejour_id`',
            'patients'    => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $service_1      = new CService();
        $service_1->_id = $service_1->service_id = 1;

        $group_clause = "`sejour`.`group_id` = '{$group->_id}'";
        $date         = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            $group_clause,
            '`affectation`.`service_id` = 1',
            "`affectation`.`entree` <= '{$date} 23:59:59'",
            "`affectation`.`sortie` >= '{$date} 00:00:00'",
        ];

        /* Case 2: Check on real entry, with a list of services, without filtering not placed sejours */
        $service_2 = [1, 2];

        $where_2 = [
            $group_clause,
            "`affectation`.`service_id` IN ('1', '2')",
            "`affectation`.`entree` <= '{$date} 23:59:59'",
            "`affectation`.`sortie` >= '{$date} 00:00:00'",
        ];

        return [
            [$group, $service_1, $where_1, $ljoin],
            [$group, $service_2, $where_2, $ljoin],
        ];
    }

    /**
     * @return void
     */
    public function testFilterSejoursByRealEntry(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $expected_where = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`entree_reelle` IS NOT NULL',
            "`sejour`.`entree` <= '{$date} 23:59:59'",
            "`sejour`.`sortie` >= '{$date} 00:00:00'",
        ];

        $repository = new PresentPatientsRepository($group, new CMediusers());
        $repository->setRealEntrySejoursFilter(true);
        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @dataProvider orderByProvider
     *
     * @param string $column
     * @param string $way
     * @param array  $expected_order
     * @param array  $expected_ljoin
     *
     * @return void
     */
    public function testOrderBy(
        string $column,
        string $way,
        array $expected_order,
        array $expected_ljoin
    ): void {
        $repository = new PresentPatientsRepository(new CGroups(), new CMediusers());
        $request    = $repository->getLoadSejoursRequest(new DateTimeImmutable(), $column, $way);

        $this->assertEquals($expected_order, $request->order);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @return array[]
     */
    public function orderByProvider(): array
    {
        $order_1 = [
            '`sejour`.`entree` DESC, `patients`.`nom`, `patients`.`prenom`',
        ];

        $ljoin_patients = [
            'patients' => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $order_2 = [
            '`sejour`.`sortie` ASC, `patients`.`nom`, `patients`.`prenom`',
        ];

        $order_3 = [
            "ISNULL(`chambre`.`rank`), `chambre`.`rank` ASC, `chambre`.`nom` ASC,"
            . ' `patients`.`nom`, `patients`.`prenom`, `sejour`.`entree`',
        ];

        $ljoin_3 = [
            'affectation' => '`affectation`.`sejour_id` = `sejour`.`sejour_id`',
            'lit'         => '`lit`.`lit_id` = `affectation`.`lit_id`',
            'chambre'     => '`chambre`.`chambre_id` = `lit`.`chambre_id`',
            'patients'    => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $order_4 = [
            '`users`.`user_last_name` ASC, `users`.`user_first_name`, `sejour`.`entree`',
        ];

        $ljoin_4 = [
            'users'    => '`users`.`user_id` = `sejour`.`praticien_id`',
            'patients' => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $order_5 = [
            '`patients`.`nom` ASC, `patients`.`prenom` ASC, `sejour`.`entree`',
        ];

        return [
            ['entree', 'DESC', $order_1, $ljoin_patients],
            ['sortie', 'ASC', $order_2, $ljoin_patients],
            ['_chambre', 'ASC', $order_3, $ljoin_3],
            ['praticien_id', 'ASC', $order_4, $ljoin_4],
            ['patient_name', 'ASC', $order_5, $ljoin_patients],
            ['patient_id', 'ASC', $order_5, $ljoin_patients],
            ['operation', 'TEST', $order_5, $ljoin_patients],
        ];
    }
}
