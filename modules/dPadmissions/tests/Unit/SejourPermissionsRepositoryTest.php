<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Tests\Unit;

use DateTimeImmutable;
use Exception;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Admissions\SejourPermissionsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;

/**
 * A class test for the SejourPermissionsRepository
 */
class SejourPermissionsRepositoryTest extends OxUnitTestCase
{
    /**
     * @return void
     */
    public function testGetCountSejoursPerDayRequest(): void
    {
        $group = new CGroups();
        $group->_id = $group->group_id = 110;
        $repository = new SejourPermissionsRepository(
            $group,
            new CMediusers(),
            AdmissionsRepository::SEJOUR_VIEW_SORTIES
        );

        $date = new DateTimeImmutable();
        $begin = $date->modify('first day of this month')->format('Y-m-d');
        $end = $date->modify('last day of this month')->format('Y-m-d');

        $request = $repository->getCountSejoursPerDayRequest($date);
        $expected_query = <<<SQL
SELECT DATE_FORMAT(`affectation`.`sortie`, '%Y-%m-%d') AS `date`,
COUNT(DISTINCT `affectation`.`sejour_id`) AS `count`
FROM `affectation`
LEFT JOIN `sejour` ON `sejour`.`sejour_id` = `affectation`.`sejour_id`
WHERE (`sejour`.`group_id` = '110')
AND (`affectation`.`sortie` BETWEEN '{$begin} 00:00:00' AND '{$end} 23:59:59')
GROUP BY date
ORDER BY date
SQL;

        $this->assertEquals($expected_query, $request->makeSelect());
    }

    /**
     * @return void
     */
    public function testGetCountMonthlySejoursRequest(): void
    {
        $group = new CGroups();
        $group->_id = $group->group_id = 110;
        $repository = new SejourPermissionsRepository(
            $group,
            new CMediusers(),
            AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS
        );

        $date = new DateTimeImmutable();
        $begin = $date->modify('first day of this month')->format('Y-m-d');
        $end = $date->modify('last day of this month')->format('Y-m-d');

        $request = $repository->getCountMonthlySejoursRequest($date);
        $expected_query = <<<SQL
SELECT COUNT(DISTINCT `affectation`.`sejour_id`) AS `count`
FROM `affectation`
LEFT JOIN `sejour` ON `sejour`.`sejour_id` = `affectation`.`sejour_id`
WHERE (`sejour`.`group_id` = '110')
AND (`affectation`.`entree` BETWEEN '{$begin} 00:00:00' AND '{$end} 23:59:59')
SQL;

        $this->assertEquals($expected_query, $request->makeSelect());
    }

    /**
     * @dataProvider loadSejoursProviders
     *
     * @param CAffectation $affectation
     * @param array        $expected
     *
     * @return void
     */
    public function testLoadSejours(CAffectation $affectation, array $expected): void
    {
        $repository = new SejourPermissionsRepository(new CGroups(), new CMediusers());
        $this->assertEquals(
            $expected,
            $repository->loadSejours(
                new DateTimeImmutable(),
                'patient_id',
                'ASC',
                null,
                null,
                null,
                $affectation
            )
        );
    }

    /**
     * @return array[]
     */
    public function loadSejoursProviders(): array
    {
        $affectation = $this->getMockBuilder(CAffectation::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadListByReq'])
            ->getMock();
        $affectation->method('loadListByReq')->willReturnOnConsecutiveCalls(
            [new CAffectation()],
            null,
            $this->throwException(new Exception('SQL error'))
        );

        return [
            [$affectation, [new CAffectation()]],
            [$affectation, []],
            [$affectation, []],
        ];
    }

    /**
     * @dataProvider loadSejoursByPermsProviders
     *
     * @param CAffectation $affectation
     * @param array        $expected
     *
     * @return void
     */
    public function testLoadSejoursByPerms(CAffectation $affectation, array $expected): void
    {
        $repository = new SejourPermissionsRepository(new CGroups(), new CMediusers());
        $this->assertEquals(
            $expected,
            $repository->loadSejoursByPerms(
                PERM_READ,
                new DateTimeImmutable(),
                'patient_id',
                'ASC',
                null,
                null,
                null,
                $affectation
            )
        );
    }

    /**
     * @return array[]
     */
    public function loadSejoursByPermsProviders(): array
    {
        $affectation = $this->getMockBuilder(CAffectation::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadListWithPerms'])
            ->getMock();
        $affectation->method('loadListWithPerms')->willReturnOnConsecutiveCalls(
            [new CAffectation()],
            null,
            $this->throwException(new Exception('SQL error'))
        );

        return [
            [$affectation, [new CAffectation()]],
            [$affectation, []],
            [$affectation, []],
        ];
    }

    /**
     * @dataProvider countSejoursProviders
     *
     * @param CAffectation $affectation
     * @param int          $expected
     *
     * @return void
     */
    public function testCountSejours(CAffectation $affectation, int $expected): void
    {
        $repository = new SejourPermissionsRepository(new CGroups(), new CMediusers());
        $this->assertEquals(
            $expected,
            $repository->countSejours(
                new DateTimeImmutable(),
                null,
                $affectation
            )
        );
    }

    /**
     * @return array[]
     */
    public function countSejoursProviders(): array
    {
        $affectation = $this->getMockBuilder(CAffectation::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countList'])
            ->getMock();
        $affectation->method('countList')->willReturnOnConsecutiveCalls(
            26,
            '9',
            null,
            $this->throwException(new Exception('SQL error'))
        );

        return [
            [$affectation, 26],
            [$affectation, 9],
            [$affectation, 0],
            [$affectation, 0],
        ];
    }

    /**
     * @dataProvider getLoadSejoursRequestProvider
     *
     * @param CGroups                $group
     * @param DateTimeImmutable      $date
     * @param DateTimeImmutable|null $date_max
     * @param int|null               $start
     * @param int|null               $count
     * @param string                 $expected
     *
     * @return void
     */
    public function testGetLoadSejoursRequest(
        CGroups $group,
        DateTimeImmutable $date,
        ?DateTimeImmutable $date_max,
        ?int $start,
        ?int $count,
        string $expected
    ): void {
        $repository = new SejourPermissionsRepository($group, new CMediusers());
        $this->assertEquals($expected, $repository->getLoadSejoursRequest(
            $date,
            'patient_id',
            'ASC',
            $start,
            $count,
            $date_max
        )->makeSelect());
    }

    /**
     * @return array[]
     */
    public function getLoadSejoursRequestProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date         = new DateTimeImmutable();
        $date_max     = $date->modify('+10 days');
        $date_str     = $date->format('Y-m-d');
        $date_max_str = $date_max->format('Y-m-d');

        $count = 50;
        $start = 0;

        $query_1 = <<<SQL
SELECT `affectation`.*
FROM `affectation`
LEFT JOIN `sejour` ON `sejour`.`sejour_id` = `affectation`.`sejour_id`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`affectation`.`entree` BETWEEN '{$date_str} 00:00:00' AND '{$date_str} 23:59:59')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `affectation`.`entree`
SQL;

        $query_2 = <<<SQL
SELECT `affectation`.*
FROM `affectation`
LEFT JOIN `sejour` ON `sejour`.`sejour_id` = `affectation`.`sejour_id`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`affectation`.`entree` BETWEEN '{$date_str} 00:00:00' AND '{$date_max_str} 23:59:59')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `affectation`.`entree`
LIMIT {$count}
SQL;

        $query_3 = <<<SQL
SELECT `affectation`.*
FROM `affectation`
LEFT JOIN `sejour` ON `sejour`.`sejour_id` = `affectation`.`sejour_id`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`affectation`.`entree` BETWEEN '{$date_str} 00:00:00' AND '{$date_max_str} 23:59:59')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `affectation`.`entree`
LIMIT {$start}, {$count}
SQL;

        return [
            /* Case 1 : no date max, no limit */
            [$group, $date, null, null, null, $query_1],
            /* Case 2: date max, only count */
            [$group, $date, $date_max, null, $count, $query_2],
            /* Case 3: date max smaller than date, start and count */
            [$group, $date_max, $date, $start, $count, $query_3]
        ];
    }

    /**
     * @dataProvider filterSejoursByServiceProvider
     *
     * @param SejourPermissionsRepository $repository
     * @param array                       $expected_where
     *
     * @return void
     */
    public function testFilterSejoursByService(SejourPermissionsRepository $repository, array $expected_where): void
    {
        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array
     */
    public function filterSejoursByServiceProvider(): array
    {
        $group = new CGroups();
        $group->_id = $group->group_id = 10;
        $user = new CMediusers();

        $service_1 = new CService();
        $service_1->_id = $service_1->service_id = 1;

        /* Case 1: Filter by a unique service */
        $repository_1 = new SejourPermissionsRepository($group, $user);
        $repository_1->setServiceFilter($service_1);

        $group_clause = "`sejour`.`group_id` = '{$group->_id}'";
        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            $group_clause,
            '`affectation`.`service_id` = 1',
            "`affectation`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        /* Case 2: Check on real entry, with a list of services, without filtering not placed sejours */
        $repository_2 = new SejourPermissionsRepository($group, $user);
        $repository_2->setServicesListFilter([1, 2]);

        $where_2 = [
            $group_clause,
            "`affectation`.`service_id` IN ('1', '2')",
            "`affectation`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'"
        ];

        return [
            [$repository_1, $where_1],
            [$repository_2, $where_2],
        ];
    }

    /**
     * @dataProvider setOrderProvider
     *
     * @param string $column
     * @param string $way
     * @param array  $expected_order
     * @param array  $expected_ljoin
     *
     * @return void
     */
    public function testSetOrder(string $column, string $way, array $expected_order, array $expected_ljoin): void
    {
        $repository = new SejourPermissionsRepository(new CGroups(), new CMediusers());
        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable(), $column, $way);
        $this->assertEquals($expected_order, $request->order);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @return array
     */
    public function setOrderProvider(): array
    {
        $order_patient = ['`patients`.`nom` ASC, `patients`.`prenom` ASC, `affectation`.`entree`'];
        $ljoin_patient = [
            'patients' => '`patients`.`patient_id` = `sejour`.`patient_id`',
            'sejour'   => '`sejour`.`sejour_id` = `affectation`.`sejour_id`'
        ];

        $order_entree = ['`affectation`.`entree` DESC, `patients`.`nom`, `patients`.`prenom`'];
        $order_sortie = ['`affectation`.`sortie` DESC, `patients`.`nom`, `patients`.`prenom`'];

        $order_praticien = ['`users`.`user_last_name` ASC, `users`.`user_first_name` ASC, `patients`.`nom`, `patients`.`prenom`'];
        $ljoin_praticien = [
            'patients' => '`patients`.`patient_id` = `sejour`.`patient_id`',
            'sejour'   => '`sejour`.`sejour_id` = `affectation`.`sejour_id`',
            'users'    => '`users`.`user_id` = `sejour`.`praticien_id`'
        ];

        return [
            ['entree', 'DESC', $order_entree, $ljoin_patient],
            ['sortie', 'DESC', $order_sortie, $ljoin_patient],
            ['praticien_id', 'ASC', $order_praticien, $ljoin_praticien],
            ['patient_id', 'ASC', $order_patient, $ljoin_patient],
        ];
    }
}
