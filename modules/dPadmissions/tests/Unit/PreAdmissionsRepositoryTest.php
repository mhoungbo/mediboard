<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Tests\Unit;

use DateTimeImmutable;
use Exception;
use Ox\Mediboard\Admissions\PreAdmissionsRepository;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;

/**
 * A class test for the PreAdmissionsRepositoryTest
 */
class PreAdmissionsRepositoryTest extends OxUnitTestCase
{
    /**
     * @dataProvider countAnaesthesiaConsultationsProvider
     *
     * @param CMediusers $user
     * @param CConsultation $consultation
     * @param int $expected
     *
     * @return void
     */
    public function testCountAnaesthesiaConsultations(
        CMediusers $user,
        CConsultation $consultation,
        int $expected
    ): void {
        $repository = new PreAdmissionsRepository(new CGroups(), $user, $consultation);

        $this->assertEquals($expected, $repository->countAnaesthesiaConsultations(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function countAnaesthesiaConsultationsProvider(): array
    {
        $user = $this->getMediuserMock();

        $consultation = $this->getObjectLoaderMock(
            CConsultation::class,
            'countList',
            [12, '28', null, $this->throwException(new Exception('SQL Error'))]
        );

        return [
            [$user, $consultation, 12],
            [$user, $consultation, 28],
            [$user, $consultation, 0],
            [$user, $consultation, 0],
        ];
    }

    /**
     * @return void
     */
    public function testPrepareCountAnesthesiaConsultationsRequest(): void
    {
        $date = new DateTimeImmutable();
        $begin = $date->modify('first day of this month')->format('Y-m-d');
        $end   = $date->modify('last day of this month')->format('Y-m-d');

        $expected_where = [
            '`consultation`.`patient_id` IS NOT NULL',
            "`consultation`.`annule` = '0'",
            "`plageconsult`.`chir_id` IN ('1', '2')",
            "`plageconsult`.`date` BETWEEN '{$begin}' AND '{$end}'"
        ];

        $expected_ljoin = [
            'plageconsult' => '`consultation`.`plageconsult_id` = `plageconsult`.`plageconsult_id`'
        ];

        $repository = new PreAdmissionsRepository(new CGroups(), $this->getMediuserMock());
        $request = $repository->prepareCountAnesthesiaConsultationsRequest($date);

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @dataProvider countAnaesthesiaConsultationsPerDayProvider
     *
     * @param CMediusers $user
     * @param CConsultation $consultation
     * @param array $expected
     *
     * @return void
     */
    public function testCountAnaesthesiaConsultationsPerDay(
        CMediusers $user,
        CConsultation $consultation,
        array $expected
    ): void {
        $repository = new PreAdmissionsRepository(new CGroups(), $user, $consultation);

        $this->assertEquals($expected, $repository->countAnaesthesiaConsultationsPerDay(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function countAnaesthesiaConsultationsPerDayProvider(): array
    {
        $user = $this->getMediuserMock();

        $date = new DateTimeImmutable();
        $date_1 = $date->format('Y-m-d');
        $date_2 = $date->modify('-1 days')->format('Y-m-d');
        $date_3 = $date->modify('-2 days')->format('Y-m-d');

        $consultation = $this->getObjectLoaderMock(
            CConsultation::class,
            'countMultipleList',
            [[$date_1 => 12, $date_2 => 28, $date_3 => 7], null, $this->throwException(new Exception('SQL Error'))]
        );

        return [
            [$user, $consultation, [$date_1 => 12, $date_2 => 28, $date_3 => 7]],
            [$user, $consultation, []],
            [$user, $consultation, []],
        ];
    }

    /**
     * @return void
     */
    public function testPrepareCountAnesthesiaConsultationsPerDayRequest(): void
    {
        $date = new DateTimeImmutable();
        $begin = $date->modify('first day of this month')->format('Y-m-d');
        $end   = $date->modify('last day of this month')->format('Y-m-d');

        $expected_where = [
            '`consultation`.`patient_id` IS NOT NULL',
            "`consultation`.`annule` = '0'",
            "`plageconsult`.`chir_id` IN ('1', '2')",
            "`plageconsult`.`date` BETWEEN '{$begin}' AND '{$end}'"
        ];

        $expected_ljoin = [
            'plageconsult' => '`consultation`.`plageconsult_id` = `plageconsult`.`plageconsult_id`'
        ];

        $repository = new PreAdmissionsRepository(new CGroups(), $this->getMediuserMock());
        $request = $repository->prepareCountAnaesthesiaConsultationsPerDayRequest($date);

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
        $this->assertEquals(['`plageconsult`.`date`'], $request->order);
        $this->assertEquals(['`plageconsult`.`date`'], $request->group);
    }

    /**
     * @dataProvider getAnaesthesiaConsultationsProvider
     *
     * @param CMediusers $user
     * @param CConsultation $consultation
     * @param array $expected
     *
     * @return void
     */
    public function testGetAnaesthesiaConsultations(
        CMediusers $user,
        CConsultation $consultation,
        array $expected
    ): void {
        $repository = new PreAdmissionsRepository(new CGroups(), $user, $consultation);

        $this->assertEquals($expected, $repository->getAnaesthesiaConsultations(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function getAnaesthesiaConsultationsProvider(): array
    {
        $user = $this->getMediuserMock();

        $consultation = $this->getObjectLoaderMock(
            CConsultation::class,
            'loadListByReq',
            [[1 => new CConsultation()], null, $this->throwException(new Exception('SQL Error'))]
        );

        return [
            [$user, $consultation, [1 => new CConsultation()]],
            [$user, $consultation, []],
            [$user, $consultation, []],
        ];
    }

    /**
     * @dataProvider prepareGetAnaesthesiaConsultationsRequestPeriodFilterProvider
     *
     * @param DateTimeImmutable $date
     * @param string            $period
     * @param string            $hour
     * @param array             $expected_where
     *
     * @return void
     */
    public function testPrepareGetAnaesthesiaConsultationsRequestPeriodFilter(
        DateTimeImmutable $date,
        string $period,
        string $hour,
        array $expected_where
    ): void {
        $repository = new PreAdmissionsRepository(new CGroups(), $this->getMediuserMock());
        $repository->setPeriodFilter($period, $hour);
        $request = $repository->prepareGetAnaesthesiaConsultationsRequest($date);

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function prepareGetAnaesthesiaConsultationsRequestPeriodFilterProvider(): array
    {
        $date = new DateTimeImmutable();
        $date_str = $date->format('Y-m-d');

        $expected_where_1 = [
            '`consultation`.`patient_id` IS NOT NULL',
            "`consultation`.`annule` = '0'",
            "`plageconsult`.`chir_id` IN ('1', '2')",
            "`plageconsult`.`date` = '{$date_str}'",
            "`consultation`.`heure` < '14:00:00'",
        ];

        $expected_where_2 = [
            '`consultation`.`patient_id` IS NOT NULL',
            "`consultation`.`annule` = '0'",
            "`plageconsult`.`chir_id` IN ('1', '2')",
            "`plageconsult`.`date` = '{$date_str}'",
            "`consultation`.`heure` >= '12:00:00'",
        ];

        return [
            [$date, PreAdmissionsRepository::FILTER_PERIOD_MORNING, '14:00:00', $expected_where_1],
            [$date, PreAdmissionsRepository::FILTER_PERIOD_AFTERNOON, '12:00:00', $expected_where_2],
        ];
    }

    /**
     * @dataProvider prepareGetAnaesthesiaConsultationsRequestOrderByProvider
     *
     * @param string $order_column
     * @param string $order_way
     * @param array  $expected_ljoin
     * @param array  $expected_order
     *
     * @return void
     */
    public function testPrepareGetAnaesthesiaConsultationsRequestOrderBy(
        string $order_column,
        string $order_way,
        array $expected_ljoin,
        array $expected_order
    ): void {
        $repository = new PreAdmissionsRepository(new CGroups(), $this->getMediuserMock());
        $request = $repository->prepareGetAnaesthesiaConsultationsRequest(
            new DateTimeImmutable(),
            $order_column,
            $order_way
        );

        $this->assertEquals($expected_order, $request->order);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @return array[]
     */
    public function prepareGetAnaesthesiaConsultationsRequestOrderByProvider(): array
    {
        $ljoin_1 = [
            'plageconsult' => '`consultation`.`plageconsult_id` = `plageconsult`.`plageconsult_id`',
        ];

        $order_1 = ['`consultation`.`heure` DESC'];

        $ljoin_2 = [
            'plageconsult' => '`consultation`.`plageconsult_id` = `plageconsult`.`plageconsult_id`',
            'patients' => '`consultation`.`patient_id` = `patients`.`patient_id`',
        ];

        $order_2 = ['`patients`.`nom` ASC, `patients`.`prenom` ASC, `consultation`.`heure`'];

        return [
            ['heure', 'DESC', $ljoin_1, $order_1],
            ['patient_id', 'ASC', $ljoin_2, $order_2],
            ['praticien_id', 'TEST', $ljoin_2, $order_2],
        ];
    }

    /**
     * @dataProvider getPreparedSejoursWithoutAnaesthesiaRecordsProvider
     *
     * @param CMediusers $user
     * @param CSejour $sejour
     * @param array $expected
     *
     * @return void
     */
    public function testGetPreparedSejoursWithoutAnaesthesiaRecords(
        CMediusers $user,
        CSejour $sejour,
        array $expected
    ): void {
        $repository = new PreAdmissionsRepository(new CGroups(), $user, null, $sejour);

        $this->assertEquals(
            $expected,
            $repository->getPreparedSejoursWithoutAnaesthesiaRecords(new DateTimeImmutable())
        );
    }

    /**
     * @return array[]
     */
    public function getPreparedSejoursWithoutAnaesthesiaRecordsProvider(): array
    {
        $user = $this->getMediuserMock();

        $consultation = $this->getObjectLoaderMock(
            CSejour::class,
            'loadListByReq',
            [[1 => new CSejour()], null, $this->throwException(new Exception('SQL Error'))]
        );

        return [
            [$user, $consultation, [1 => new CSejour()]],
            [$user, $consultation, []],
            [$user, $consultation, []],
        ];
    }

    /**
     * @return void
     */
    public function testGetPreparedSejoursWithoutAnaesthesiaRecordsRequest(): void
    {
        $date = new DateTimeImmutable();
        $date_str = $date->format('Y-m-d');

        $expected_where = [
            "`sejour`.`entree_preparee` = '1'",
            "`sejour`.`entree_preparee_date` BETWEEN '{$date_str} 00:00:00' AND '{$date_str} 23:59:59'",
            'c1.`consultation_anesth_id` IS NULL',
            'c2.`consultation_anesth_id` IS NULL',
            '`sejour`.`group_id` = 10',
            "`sejour`.`sejour_id` NOT IN ('28', '33')",
            "`sejour`.`type_pec` IN ('M', 'O')"
        ];

        $expected_ljoin = [
            'operations' => '`operations`.`sejour_id` = `sejour`.`sejour_id`',
            '`consultation_anesth` AS c1 ON c1.`sejour_id` = `sejour`.`sejour_id`',
            '`consultation_anesth` AS c2 ON c2.`operation_id` = `operations`.`operation_id`',
        ];

        $group = new CGroups();
        $group->_id = $group->group_id = 10;
        $repository = new PreAdmissionsRepository($group, $this->getMediuserMock());
        $request = $repository->getPreparedSejoursWithoutAnaesthesiaRecordsRequest($date, [28, 33], ['M', 'O']);

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @dataProvider getDateMinProvider
     *
     * @param string $period
     * @param string $hour
     * @param string $expected
     *
     * @return void
     */
    public function testGetDateMin(string $period, string $hour, string $expected): void
    {
        $repository = new PreAdmissionsRepository(new CGroups(), new CMediusers());
        $repository->setPeriodFilter($period, $hour);

        $this->assertEquals($expected, $repository->getDateMin(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function getDateMinProvider(): array
    {
        $date = new DateTimeImmutable();

        return [
            /* Case 1: period morning */
            [PreAdmissionsRepository::FILTER_PERIOD_MORNING, '12:00:00', $date->format('Y-m-d 00:00:00')],
            /* Case 1: period afternoon */
            [PreAdmissionsRepository::FILTER_PERIOD_AFTERNOON, '14:00:00', $date->format('Y-m-d 14:00:00')],
        ];
    }

    /**
     * @dataProvider getDateMaxProvider
     *
     * @param string $period
     * @param string $hour
     * @param string $expected
     *
     * @return void
     */
    public function testGetDateMax(string $period, string $hour, string $expected): void
    {
        $repository = new PreAdmissionsRepository(new CGroups(), new CMediusers());
        $repository->setPeriodFilter($period, $hour);

        $this->assertEquals($expected, $repository->getDateMax(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function getDateMaxProvider(): array
    {
        $date = new DateTimeImmutable();

        return [
            [PreAdmissionsRepository::FILTER_PERIOD_MORNING, '12:00:00', $date->format('Y-m-d 12:00:00')],
            [PreAdmissionsRepository::FILTER_PERIOD_AFTERNOON, '12:00:00', $date->format('Y-m-d 23:59:59')],
        ];
    }

    /**
     * @return CMediusers
     */
    protected function getMediuserMock(): CMediusers
    {
        $user = $this->getMockBuilder(CMediusers::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadAnesthesistes'])
            ->getMock();
        $user->method('loadAnesthesistes')->willReturn([
            1 => new CMediusers(),
            2 => new CMediusers(),
        ]);

        return $user;
    }

    /**
     * @param string $class
     * @param string $method
     * @param array  $consecutive_calls_return
     *
     * @return object
     */
    protected function getObjectLoaderMock(string $class, string $method, array $consecutive_calls_return): object
    {
        $consultation = $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->onlyMethods([$method])
            ->getMock();
        $consultation->method($method)->willReturnOnConsecutiveCalls(...$consecutive_calls_return);

        return $consultation;
    }
}
