<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Tests\Unit;

use DateTimeImmutable;
use Exception;
use Ox\Core\CPDOMySQLDataSource;
use Ox\Mediboard\Admissions\AdmissionsRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;

/**
 * A class test for the AdmissionRepository
 */
class AdmissionsRepositoryTest extends OxUnitTestCase
{
    /**
     * @dataProvider getServicesListRequestProvider
     *
     * @param bool $external_parameter
     * @param int  $external_actual_value
     *
     * @return void
     */
    public function testGetServicesListRequest(bool $external_parameter, int $external_actual_value): void
    {
        $group           = new CGroups();
        $group->group_id = $group->_id = 1;

        $repository = new AdmissionsRepository($group, new CMediusers());
        $request    = $repository->getServicesListRequest($external_parameter);

        $this->assertEquals([
            'group_id'  => ' = 1',
            'externe'   => " = '{$external_actual_value}'",
            'cancelled' => " = '0'",
        ], $request->where);
        $this->assertEquals(['nom ASC'], $request->order);
    }

    /**
     * @return array[]
     */
    public function getServicesListRequestProvider(): array
    {
        return [
            [false, 0],
            [true, 1],
        ];
    }

    /**
     * @dataProvider loadServicesListProvider
     *
     * @param CService $service_object
     * @param array    $expected_values
     *
     * @return void
     */
    public function testLoadServicesList(CService $service_object, array $expected_values): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());

        $this->assertEquals($expected_values, $repository->loadServicesList(false, $service_object));
    }

    /**
     * @return array[]
     */
    public function loadServicesListProvider(): array
    {
        $mock = $this->getMockBuilder(CService::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadList'])
            ->getMock();

        $service      = new CService();
        $service->_id = $service->service_id = 1;
        $service->nom = 'AMBU 1';

        $mock->method('loadList')->will(
            $this->onConsecutiveCalls(
                [1 => $service],
                null,
                $this->throwException(new Exception('test'))
            )
        );

        return [
            [$mock, [1 => $service]],
            [$mock, []],
            [$mock, []],
        ];
    }

    /**
     * @dataProvider loadPractitionersProvider
     *
     * @param CMediusers $user
     * @param array      $expected_values
     *
     * @return void
     */
    public function testLoadPractitioners(CMediusers $user, array $expected_values): void
    {
        $repository = new AdmissionsRepository(new CGroups(), $user);

        $this->assertEquals($expected_values, $repository->loadPractitioners());
    }

    /**
     * @return array[]
     */
    public function loadPractitionersProvider(): array
    {
        $mock = $this->getMockBuilder(CMediusers::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadPraticiens'])
            ->getMock();

        $user                   = new CMediusers();
        $user->_id              = $user->user_id = 1;
        $user->_user_last_name  = 'GENE';
        $user->_user_first_name = 'Alain';
        $user->_user_username   = 'agene';

        $mock->method('loadPraticiens')->will(
            $this->onConsecutiveCalls(
                [1 => $user],
                null,
                $this->throwException(new Exception('test'))
            )
        );

        return [
            [$mock, [1 => $user]],
            [$mock, []],
            [$mock, []],
        ];
    }

    /**
     * @dataProvider getSejourFilterObjectProvider
     *
     * @param string      $type
     * @param string|null $expected
     *
     * @return void
     */
    public function testGetSejourFilterObject(string $type, ?string $expected): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());
        $repository->setSejourTypeFilter($type);
        $sejour = $repository->getSejourFilterObject(1, 1);

        $this->assertEquals($expected, $sejour->_type_admission);
    }

    /**
     * @return array
     */
    public function getSejourFilterObjectProvider(): array
    {
        return [
            ['ambu', 'ambu'],
            ['ambulatoire', null],
        ];
    }

    /**
     * @dataProvider countSejoursInWaitingProvider
     *
     * @param CSejour $sejour_object
     * @param int     $expected_value
     *
     * @return void
     */
    public function testCountSejoursInWaiting(CSejour $sejour_object, int $expected_value): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());

        $this->assertEquals(
            $expected_value,
            $repository->countSejoursInWaiting(new DateTimeImmutable(), $sejour_object)
        );
    }

    /**
     * @return array[]
     */
    public function countSejoursInWaitingProvider(): array
    {
        $mock = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countList'])
            ->getMock();

        $mock->method('countList')->will(
            $this->onConsecutiveCalls(
                28,
                '12',
                'aaaa',
                $this->throwException(new Exception('test'))
            )
        );

        return [
            [$mock, 28],
            [$mock, 12],
            [$mock, 0],
            [$mock, 0],
        ];
    }

    /**
     * @dataProvider getCountSejoursInWaitingRequestProvider
     *
     * @param DateTimeImmutable    $date
     * @param AdmissionsRepository $repository
     * @param array                $expected
     *
     * @return void
     */
    public function testGetCountSejoursInWaitingRequest(
        DateTimeImmutable $date,
        AdmissionsRepository $repository,
        array $expected
    ): void {
        $request = $repository->getCountSejoursInWaitingRequest($date);
        $this->assertEquals($expected, $request->where);
    }

    /**
     * @return array[]
     */
    public function getCountSejoursInWaitingRequestProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 1;
        $repo1      = new AdmissionsRepository($group, new CMediusers());
        $repo1->setSejourTypeFilter('ambu');

        $date = new DateTimeImmutable();

        $repo2 = new AdmissionsRepository($group, new CMediusers(), AdmissionsRepository::SEJOUR_VIEW_SORTIES);
        $repo2->setSejourTypeFilter('ambulatoire');

        return [
            [
                $date,
                $repo1,
                [
                    'group_id' => " = '1'",
                    'recuse'   => " = '-1'",
                    'annule'   => " = '0'",
                    'entree'   => " BETWEEN '" . $date->format('Y-m-d 00:00:00') . "' AND '"
                        . $date->format('Y-m-d 23:59:59') . "'",
                    'type'     => " = 'ambu'",
                ],
            ],
            [
                $date,
                $repo2,
                [
                    'group_id' => " = '1'",
                    'recuse'   => " = '-1'",
                    'annule'   => " = '0'",
                    'sortie'   => " BETWEEN '" . $date->format('Y-m-d 00:00:00') . "' AND '"
                        . $date->format('Y-m-d 23:59:59') . "'",
                ],
            ],
        ];
    }

    /**
     * @dataProvider getMonthlyCountersProvider
     *
     * @param DateTimeImmutable $date
     * @param array             $keys
     * @param array             $expected
     *
     * @return void
     */
    public function testGetMontlhyCounters(DateTimeImmutable $date, array $keys, array $expected): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());
        $this->assertEquals($expected, $repository->getMonthlyCounters($date, $keys));
    }

    /**
     * @return array
     */
    public function getMonthlyCountersProvider(): array
    {
        $datetime_exception = $this->getMockBuilder(DateTimeImmutable::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['modify'])
            ->getMock();

        $datetime_exception->method('modify')->willThrowException(new Exception('DateTime error'));

        $date             = new DateTimeImmutable();
        $datetime_success = $this->getMockBuilder(DateTimeImmutable::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['modify'])
            ->getMock();

        $datetime_success->method('modify')->willReturnOnConsecutiveCalls(
            $date,
            $date->modify('+5 day'),
            $date->modify('+1 day'),
            $date->modify('+2 day'),
            $date->modify('+3 day'),
            $date->modify('+4 day'),
            $date->modify('+5 day'),
            $date->modify('+6 day'),
        );

        $entries          = ['waiting' => 0, 'validated' => 0];
        $expected_success = [
            $date->format('Y-m-d')                   => $entries,
            $date->modify('+1 day')->format('Y-m-d') => $entries,
            $date->modify('+2 day')->format('Y-m-d') => $entries,
            $date->modify('+3 day')->format('Y-m-d') => $entries,
            $date->modify('+4 day')->format('Y-m-d') => $entries,
            $date->modify('+5 day')->format('Y-m-d') => $entries,
        ];

        return [
            [$datetime_exception, ['waiting'], []],
            [$datetime_success, ['waiting', 'validated'], $expected_success],
        ];
    }

    /**
     * @dataProvider countSejoursPerDayProvider
     *
     * @param CGroups $group
     * @param array   $expected
     *
     * @return void
     */
    public function testCountSejoursPerDay(CGroups $group, array $expected): void
    {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $this->assertEquals($expected, $repository->countSejoursPerDay(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function countSejoursPerDayProvider(): array
    {
        $date = new DateTimeImmutable();

        $ds = $this->getMockBuilder(CPDOMySQLDataSource::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadHashList'])
            ->getMock();

        $ds->method('loadHashList')->willReturnOnConsecutiveCalls(
            [
                $date->format('Y-m-d')                   => 5,
                $date->modify('+1 day')->format('Y-m-d') => 1,
            ],
            false,
            $this->throwException(new Exception('SQL error'))
        );

        $group = $this->getMockBuilder(CGroups::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getDS'])
            ->getMock();
        $group->method('getDS')->willReturn($ds);

        return [
            [
                $group,
                [
                    $date->format('Y-m-d')                   => 5,
                    $date->modify('+1 day')->format('Y-m-d') => 1,
                ],
            ],
            [$group, []],
            [$group, []],
        ];
    }

    /**
     * @return void
     */
    public function testGetCountSejoursPerDayRequest(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 110;
        $repository = new AdmissionsRepository(
            $group,
            new CMediusers(),
            AdmissionsRepository::SEJOUR_VIEW_SORTIES_SCHEDULED
        );

        $date  = new DateTimeImmutable();
        $begin = $date->modify('first day of this month')->format('Y-m-d');
        $end   = $date->modify('last day of this month')->format('Y-m-d');

        $request        = $repository->getCountSejoursPerDayRequest($date);
        $expected_query = <<<SQL
SELECT DATE_FORMAT(`sejour`.`sortie_prevue`, '%Y-%m-%d') AS `date`,
COUNT(`sejour`.`sejour_id`) AS `count`
FROM `sejour`
WHERE (`sejour`.`group_id` = '110')
AND (`sejour`.`sortie_prevue` BETWEEN '{$begin} 00:00:00' AND '{$end} 23:59:59')
GROUP BY date
ORDER BY date
SQL;

        $this->assertEquals($expected_query, $request->makeSelect());
    }

    /**
     * @dataProvider countMonthlySejoursProvider
     *
     * @param CGroups $group
     * @param int     $expected
     *
     * @return void
     */
    public function testCountMonthlySejours(CGroups $group, int $expected): void
    {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $this->assertEquals($expected, $repository->countMonthlySejours(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function countMonthlySejoursProvider(): array
    {
        $ds = $this->getMockBuilder(CPDOMySQLDataSource::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadResult'])
            ->getMock();

        $ds->method('loadResult')->willReturnOnConsecutiveCalls(
            52,
            '17',
            false,
            $this->throwException(new Exception('SQL error'))
        );

        $group = $this->getMockBuilder(CGroups::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getDS'])
            ->getMock();
        $group->method('getDS')->willReturn($ds);

        return [
            [$group, 52],
            [$group, 17],
            [$group, 0],
            [$group, 0],
        ];
    }

    /**
     * @return void
     */
    public function testGetCountMonthlySejoursRequest(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 110;
        $repository = new AdmissionsRepository(
            $group,
            new CMediusers(),
            AdmissionsRepository::SEJOUR_VIEW_REAL_ADMISSIONS
        );

        $date  = new DateTimeImmutable();
        $begin = $date->modify('first day of this month')->format('Y-m-d');
        $end   = $date->modify('last day of this month')->format('Y-m-d');

        $request        = $repository->getCountMonthlySejoursRequest($date);
        $expected_query = <<<SQL
SELECT COUNT(`sejour`.`sejour_id`) AS `count`
FROM `sejour`
WHERE (`sejour`.`group_id` = '110')
AND (`sejour`.`entree_reelle` BETWEEN '{$begin} 00:00:00' AND '{$end} 23:59:59')
SQL;

        $this->assertEquals($expected_query, $request->makeSelect());
    }

    /**
     * @dataProvider loadSejoursProviders
     *
     * @param CSejour $sejour
     * @param array   $expected
     *
     * @return void
     */
    public function testLoadSejours(CSejour $sejour, array $expected): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());
        $this->assertEquals(
            $expected,
            $repository->loadSejours(new DateTimeImmutable(), 'patient_id', 'ASC', null, null, null, $sejour)
        );
    }

    /**
     * @return array[]
     */
    public function loadSejoursProviders(): array
    {
        $sejour = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadListByReq'])
            ->getMock();
        $sejour->method('loadListByReq')->willReturnOnConsecutiveCalls(
            [new CSejour()],
            null,
            $this->throwException(new Exception('SQL error'))
        );

        return [
            [$sejour, [new CSejour()]],
            [$sejour, []],
            [$sejour, []],
        ];
    }

    /**
     * @dataProvider loadSejoursByPermsProviders
     *
     * @param CSejour $sejour
     * @param array   $expected
     *
     * @return void
     */
    public function testLoadSejoursByPerms(CSejour $sejour, array $expected): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());
        $this->assertEquals(
            $expected,
            $repository->loadSejoursByPerms(
                PERM_READ,
                new DateTimeImmutable(),
                'patient_id',
                'ASC',
                null,
                null,
                null,
                $sejour
            )
        );
    }

    /**
     * @return array[]
     */
    public function loadSejoursByPermsProviders(): array
    {
        $sejour = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadListWithPerms'])
            ->getMock();
        $sejour->method('loadListWithPerms')->willReturnOnConsecutiveCalls(
            [new CSejour()],
            null,
            $this->throwException(new Exception('SQL error'))
        );

        return [
            [$sejour, [new CSejour()]],
            [$sejour, []],
            [$sejour, []],
        ];
    }

    /**
     * @dataProvider countSejoursProviders
     *
     * @param CSejour $sejour
     * @param int     $expected
     *
     * @return void
     */
    public function testCountSejours(CSejour $sejour, int $expected): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());
        $this->assertEquals(
            $expected,
            $repository->countSejours(
                new DateTimeImmutable(),
                null,
                $sejour
            )
        );
    }

    /**
     * @return array[]
     */
    public function countSejoursProviders(): array
    {
        $sejour = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['countList'])
            ->getMock();
        $sejour->method('countList')->willReturnOnConsecutiveCalls(
            26,
            '9',
            null,
            $this->throwException(new Exception('SQL error'))
        );

        return [
            [$sejour, 26],
            [$sejour, 9],
            [$sejour, 0],
            [$sejour, 0],
        ];
    }

    /**
     * @dataProvider loadFunctionsByAdmissionsProvider
     *
     * @param CFunctions           $function
     * @param AdmissionsRepository $repository
     * @param array                $expected
     *
     * @return void
     */
    public function testLoadFunctionsByAdmissions(
        CFunctions $function,
        AdmissionsRepository $repository,
        array $expected
    ): void {
        $this->assertEquals($expected, $repository->loadFunctionsByAdmissions(new DateTimeImmutable(), $function));
    }

    /**
     * @return array
     */
    public function loadFunctionsByAdmissionsProvider(): array
    {
        $ds = $this->getMockBuilder(CPDOMySQLDataSource::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadColumn'])
            ->getMock();
        $ds->method('loadColumn')->willReturnOnConsecutiveCalls(
            [1, 3],
            [1, 3],
            $this->throwException(new Exception('SQL error')),
            null,
            [1, 3],
            [1, 3],
        );

        $group = $this->getMockBuilder(CGroups::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getDS'])
            ->getMock();

        $group->method('getDS')->willReturn($ds);

        $function_a       = new CFunctions();
        $function_a->_id  = $function_a->function_id = 1;
        $function_a->text = 'aaaa';

        $function_b       = new CFunctions();
        $function_b->_id  = $function_b->function_id = 2;
        $function_b->text = 'bbbb';

        $function_c       = new CFunctions();
        $function_c->_id  = $function_c->function_id = 3;
        $function_c->text = 'cccc';

        $function = $this->getMockBuilder(CFunctions::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadAll'])
            ->getMock();
        $function->method('loadAll')->willReturnOnConsecutiveCalls(
            [1 => $function_a, 3 => $function_c],
            [1 => $function_a, 3 => $function_c],
            $this->throwException(new Exception('SQL error')),
            null
        );

        /* Case 1: repository without filter function, no exceptions or errors */
        $repository_1 = new AdmissionsRepository($group, new CMediusers());
        $expected_1   = [1 => $function_a, 3 => $function_c];

        /* Case 2: repository with filter function, no exceptions or errors */
        $repository_2 = new AdmissionsRepository($group, new CMediusers());
        $repository_2->setFunctionFilter($function_b);
        $expected_2 = [1 => $function_a, 2 => $function_b, 3 => $function_c];

        /* Case 3: Exception thrown when loading the ids of the functions, no filter function */
        $repository_3 = new AdmissionsRepository($group, new CMediusers());
        $expected_3   = [];

        /* Case 4: Null returned when loading the ids of the functions, filter function */
        $repository_4 = new AdmissionsRepository($group, new CMediusers());
        $repository_4->setFunctionFilter($function_b);
        $expected_4 = [2 => $function_b];

        /* Case 5: Exception thrown when loading the functions, no filter function */
        $repository_5 = new AdmissionsRepository($group, new CMediusers());
        $expected_5   = [];

        /* Case 6: Null returned when loading the functions, filter function */
        $repository_6 = new AdmissionsRepository($group, new CMediusers());
        $repository_6->setFunctionFilter($function_b);
        $expected_6 = [2 => $function_b];

        return [
            [$function, $repository_1, $expected_1],
            [$function, $repository_2, $expected_2],
            [$function, $repository_3, $expected_3],
            [$function, $repository_4, $expected_4],
            [$function, $repository_5, $expected_5],
            [$function, $repository_6, $expected_6],
        ];
    }

    /**
     * @dataProvider getLoadFunctionsByAdmissionsRequestProvider
     *
     * @param AdmissionsRepository $repository
     * @param string               $expected
     *
     * @return void
     */
    public function testGetLoadFunctionsByAdmissionsRequest(AdmissionsRepository $repository, string $expected): void
    {
        $this->assertEquals(
            $expected,
            $repository->getLoadFunctionsByAdmissionsRequest(new DateTimeImmutable())->makeSelect()
        );
    }

    /**
     * @return array[]
     */
    public function getLoadFunctionsByAdmissionsRequestProvider(): array
    {
        $function      = new CFunctions();
        $function->_id = $function->function_id = 3;
        $group         = new CGroups();
        $group->_id    = $group->group_id = 10;

        $repository_1 = new AdmissionsRepository($group, new CMediusers());
        $repository_2 = new AdmissionsRepository($group, new CMediusers());
        $repository_2->setFunctionFilter($function);

        $date  = (new DateTimeImmutable())->format('Y-m-d');
        $query = <<<SQL
SELECT DISTINCT `users_mediboard`.`function_id`
FROM `sejour`
LEFT JOIN `users_mediboard` ON `users_mediboard`.`user_id` = `sejour`.`praticien_id`
WHERE (`sejour`.`group_id` = '10')
AND (`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59')
SQL;

        return [
            /* Case 1: period morning */
            [$repository_1, $query],
            /* Case 1: period afternoon */
            [$repository_2, $query],
        ];
    }

    /**
     * @dataProvider getDateMinProvider
     *
     * @param string $period
     * @param string $hour
     * @param string $expected
     *
     * @return void
     */
    public function testGetDateMin(string $period, string $hour, string $expected): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());
        $repository->setPeriodFilter($period, $hour);

        $this->assertEquals($expected, $repository->getDateMin(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function getDateMinProvider(): array
    {
        $date = new DateTimeImmutable();

        return [
            /* Case 1: period morning */
            [AdmissionsRepository::FILTER_PERIOD_MORNING, '12:00:00', $date->format('Y-m-d 00:00:00')],
            /* Case 1: period afternoon */
            [AdmissionsRepository::FILTER_PERIOD_AFTERNOON, '14:00:00', $date->format('Y-m-d 14:00:00')],
        ];
    }

    /**
     * @dataProvider getDateMaxProvider
     *
     * @param string $period
     * @param string $hour
     * @param string $expected
     *
     * @return void
     */
    public function testGetDateMax(string $period, string $hour, string $expected): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());
        $repository->setPeriodFilter($period, $hour);

        $this->assertEquals($expected, $repository->getDateMax(new DateTimeImmutable()));
    }

    /**
     * @return array[]
     */
    public function getDateMaxProvider(): array
    {
        $date = new DateTimeImmutable();

        return [
            [AdmissionsRepository::FILTER_PERIOD_MORNING, '12:00:00', $date->format('Y-m-d 12:00:00')],
            [AdmissionsRepository::FILTER_PERIOD_AFTERNOON, '12:00:00', $date->format('Y-m-d 23:59:59')],
        ];
    }

    /**
     * @dataProvider getLoadSejoursRequestProvider
     *
     * @param CGroups                $group
     * @param DateTimeImmutable      $date
     * @param DateTimeImmutable|null $date_max
     * @param int|null               $start
     * @param int|null               $count
     * @param string                 $expected
     *
     * @return void
     */
    public function testGetLoadSejoursRequest(
        CGroups $group,
        DateTimeImmutable $date,
        ?DateTimeImmutable $date_max,
        ?int $start,
        ?int $count,
        string $expected
    ): void {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $this->assertEquals(
            $expected,
            $repository->getLoadSejoursRequest(
                $date,
                'patient_id',
                'ASC',
                $start,
                $count,
                $date_max
            )->makeSelect()
        );
    }

    /**
     * @return array[]
     */
    public function getLoadSejoursRequestProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date         = new DateTimeImmutable();
        $date_max     = $date->modify('+10 days');
        $date_str     = $date->format('Y-m-d');
        $date_max_str = $date_max->format('Y-m-d');

        $count = 50;
        $start = 0;

        $query_1 = <<<SQL
SELECT `sejour`.*
FROM `sejour`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`sejour`.`entree` BETWEEN '{$date_str} 00:00:00' AND '{$date_str} 23:59:59')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `sejour`.`entree`
SQL;

        $query_2 = <<<SQL
SELECT `sejour`.*
FROM `sejour`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`sejour`.`entree` BETWEEN '{$date_str} 00:00:00' AND '{$date_max_str} 23:59:59')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `sejour`.`entree`
LIMIT {$count}
SQL;

        $query_3 = <<<SQL
SELECT `sejour`.*
FROM `sejour`
LEFT JOIN `patients` ON `patients`.`patient_id` = `sejour`.`patient_id`
WHERE (`sejour`.`group_id` = '10')
AND (`sejour`.`entree` BETWEEN '{$date_str} 00:00:00' AND '{$date_max_str} 23:59:59')
ORDER BY `patients`.`nom` ASC, `patients`.`prenom` ASC, `sejour`.`entree`
LIMIT {$start}, {$count}
SQL;

        return [
            /* Case 1 : no date max, no limit */
            [$group, $date, null, null, null, $query_1],
            /* Case 2: date max, only count */
            [$group, $date, $date_max, null, $count, $query_2],
            /* Case 3: date max smaller than date, start and count */
            [$group, $date_max, $date, $start, $count, $query_3],
        ];
    }

    /**
     * @dataProvider setOrderProvider
     *
     * @param string $column
     * @param string $way
     * @param array  $expected_order
     * @param array  $expected_ljoin
     *
     * @return void
     */
    public function testSetOrder(string $column, string $way, array $expected_order, array $expected_ljoin): void
    {
        $repository = new AdmissionsRepository(new CGroups(), new CMediusers());
        $request    = $repository->getLoadSejoursRequest(new DateTimeImmutable(), $column, $way);
        $this->assertEquals($expected_order, $request->order);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @return array
     */
    public function setOrderProvider(): array
    {
        $order_patient = ['`patients`.`nom` DESC, `patients`.`prenom` DESC, `sejour`.`entree`'];
        $ljoin_patient = ['patients' => '`patients`.`patient_id` = `sejour`.`patient_id`'];

        $order_entree_prevue = ['`sejour`.`entree_prevue` ASC, `patients`.`nom`, `patients`.`prenom`'];
        $order_entree_reelle = ['`sejour`.`entree_reelle` ASC, `patients`.`nom`, `patients`.`prenom`'];
        $order_pec_accueil   = ['`sejour`.`pec_accueil` ASC, `patients`.`nom`, `patients`.`prenom`'];
        $order_pec_service   = ['`sejour`.`pec_service` ASC, `patients`.`nom`, `patients`.`prenom`'];
        $order_sortie_prevue = ['`sejour`.`sortie_prevue` ASC, `patients`.`nom`, `patients`.`prenom`'];

        $order_service = ['`service`.`nom` ASC, `sejour`.`entree`'];
        $ljoin_service = [
            'affectation' => '`affectation`.`sejour_id` = `sejour`.`sejour_id`',
            'service'     => '`service`.`service_id` = `affectation`.`service_id`',
        ];

        $order_praticien = ['`users`.`user_last_name` ASC, `users`.`user_first_name`, `sejour`.`entree`'];
        $ljoin_praticien = ['users' => '`users`.`user_id` = `sejour`.`praticien_id`'];

        $order_passage = ['`operations`.`date` ASC, `operations`.`time_operation` ASC'];
        $ljoin_passage = ['operations' => '`operations`.`sejour_id` = `sejour`.`sejour_id`'];

        return [
            ['patient_name', 'DESC', $order_patient, $ljoin_patient],
            ['default', 'DESC', $order_patient, $ljoin_patient],
            ['entree_prevue', 'NONE', $order_entree_prevue, $ljoin_patient],
            ['entree_reelle', 'ASC', $order_entree_reelle, $ljoin_patient],
            ['pec_accueil', 'ASC', $order_pec_accueil, $ljoin_patient],
            ['pec_service', 'ASC', $order_pec_service, $ljoin_patient],
            ['sortie_prevue', 'ASC', $order_sortie_prevue, $ljoin_patient],
            ['service', 'ASC', $order_service, $ljoin_service],
            ['praticien_id', 'ASC', $order_praticien, $ljoin_praticien],
            ['passage_bloc', 'ASC', $order_passage, $ljoin_passage],
        ];
    }

    /**
     * @dataProvider filterSejoursByServiceProvider
     *
     * @param CGroups $group
     * @param string  $view_field
     * @param mixed   $filter
     * @param array   $expected_where
     * @param array   $expected_ljoin
     *
     * @return void
     */
    public function testFilterSejoursByService(
        CGroups $group,
        string $view_field,
        $filter,
        array $expected_where,
        array $expected_ljoin
    ): void {
        $repository = new AdmissionsRepository($group, new CMediusers(), $view_field);
        if ($filter instanceof CService) {
            $repository->setServiceFilter($filter);
        } elseif (is_array($filter)) {
            $repository->setServicesListFilter($filter);
        }

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @return array
     */
    public function filterSejoursByServiceProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $ljoin = [
            'affectation' => '`affectation`.`sejour_id` = `sejour`.`sejour_id`',
            'patients'    => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $service_1      = new CService();
        $service_1->_id = $service_1->service_id = 1;

        $group_clause = "`sejour`.`group_id` = '{$group->_id}'";
        $date         = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            $group_clause,
            '`affectation`.`service_id` = 1 OR (`sejour`.`service_id` = 1 AND `affectation`.`affectation_id` IS NULL)',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        /* Case 2: Check on real entry, with a list of services, without filtering not placed sejours */
        $service_2 = [1, 2];

        $where_2 = [
            $group_clause,
            '`affectation`.`entree` = `sejour`.`entree`',
            "`affectation`.`service_id` IN ('1', '2') OR "
            . "(`sejour`.`service_id` IN ('1', '2') AND `affectation`.`affectation_id` IS NULL)",
            "`sejour`.`entree_reelle` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        /* Case 3: Check on real leave, with a list of services, with filtering not placed sejours */
        $service_3 = [1, 2, 'NP'];

        $where_3 = [
            $group_clause,
            '`affectation`.`sortie` = `sejour`.`sortie`',
            "`affectation`.`service_id` IN ('1', '2') OR `affectation`.`service_id` IS NULL",
            "`sejour`.`sortie_prevue` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        return [
            [$group, AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS, $service_1, $where_1, $ljoin],
            [$group, AdmissionsRepository::SEJOUR_VIEW_REAL_ADMISSIONS, $service_2, $where_2, $ljoin],
            [$group, AdmissionsRepository::SEJOUR_VIEW_SORTIES_SCHEDULED, $service_3, $where_3, $ljoin],
        ];
    }

    /**
     * @dataProvider filterSejoursByUserProvider
     *
     * @param CGroups    $group
     * @param CMediusers $user
     * @param array      $expected_where
     * @param array      $expected_ljoin
     *
     * @return void
     */
    public function testFilterSejoursByUser(
        CGroups $group,
        CMediusers $user,
        array $expected_where,
        array $expected_ljoin
    ): void {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setUserFilter($user);

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @return array[]
     */
    public function filterSejoursByUserProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $chir      = new CMediusers();
        $chir->_id = $chir->user_id = 28;

        $anesth             = new CMediusers();
        $anesth->_id        = $anesth->user_id = 32;
        $anesth->_user_type = 4;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $repository_1 = new AdmissionsRepository($group, new CMediusers());
        $repository_1->setUserFilter($chir);

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`praticien_id` = {$chir->_id}",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        $ljoin_1 = [
            'patients' => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $repository_2 = new AdmissionsRepository($group, new CMediusers());
        $repository_2->setUserFilter($anesth);

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`operations`.`anesth_id` = {$anesth->_id} OR `plagesop`.`anesth_id` = {$anesth->_id}",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        $ljoin_2 = [
            'operations' => '`operations`.`sejour_id` = `sejour`.`sejour_id`',
            'plagesop'   => '`plagesop`.`plageop_id` = `operations`.`plageop_id`',
            'patients'   => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        return [
            [$group, $chir, $where_1, $ljoin_1],
            [$group, $anesth, $where_2, $ljoin_2],
        ];
    }

    /**
     * @return void
     */
    public function testFilterSejoursByFunction(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $function      = new CFunctions();
        $function->_id = $function->function_id = 12;

        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setFunctionFilter($function);

        $date  = (new DateTimeImmutable())->format('Y-m-d');
        $where = [
            "`sejour`.`group_id` = '{$group->_id}'",
            'users_mediboard.function_id' => ' = 12',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $ljoin = [
            'users_mediboard' => '`users_mediboard`.`user_id` = `sejour`.`praticien_id`',
            'patients'        => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($where, $request->where);
        $this->assertEquals($ljoin, $request->ljoin);
    }

    /**
     * @dataProvider filterSejoursByTypeProvider
     *
     * @param CGroups $group
     * @param string  $filter_method
     * @param mixed   $filter
     * @param array   $expected_where
     *
     * @return void
     */
    public function testFilterSejoursByType(CGroups $group, string $filter_method, $filter, array $expected_where): void
    {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->{$filter_method}($filter);
        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function filterSejoursByTypeProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`type` = 'ambu'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`type` IN ('ambu', 'comp')",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_3 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`type` NOT IN ('urg', 'seances')",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        return [
            [$group, 'setSejourTypeFilter', 'ambu', $where_1],
            [$group, 'setSejourTypeListFilter', ['ambu', 'comp'], $where_2],
            [$group, 'setSejourTypeExcludeListFilter', ['urg', 'seances'], $where_3],
        ];
    }

    /**
     * @dataProvider filterSejoursByRecuseProvider
     *
     * @param CGroups $group
     * @param int     $recuse
     * @param array   $expected_where
     *
     * @return void
     */
    public function testFilterSejoursByRecuse(CGroups $group, int $recuse, array $expected_where): void
    {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setRecuseFilter($recuse);

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function filterSejoursByRecuseProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`recuse` = '-1'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`recuse` = '0'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_3 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`recuse` = '1'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_4 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        return [
            [$group, -1, $where_1],
            [$group, 0, $where_2],
            [$group, 1, $where_3],
            [$group, 8, $where_4],
        ];
    }

    /**
     * @dataProvider filterCancelledSejoursProvider
     *
     * @param CGroups $group
     * @param int     $cancelled
     * @param array   $expected_where
     *
     * @return void
     */
    public function testFilterCancelledSejours(Cgroups $group, int $cancelled, array $expected_where): void
    {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setCancelledFilter($cancelled);

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function filterCancelledSejoursProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`annule` = '0'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`annule` = '1'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_3 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        return [
            [$group, 0, $where_1],
            [$group, 1, $where_2],
            [$group, 8, $where_3],
        ];
    }

    /**
     * @dataProvider filterConfirmedSejoursProvider
     *
     * @param CGroups $group
     * @param bool    $confirmed
     * @param array   $expected_where
     *
     * @return void
     */
    public function testConfirmedSejoursFilter(CGroups $group, bool $confirmed, array $expected_where): void
    {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setConfirmedFilter($confirmed);

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function filterConfirmedSejoursProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`confirme` IS NULL',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`confirme` IS NOT NULL',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        return [
            [$group, false, $where_1],
            [$group, true, $where_2],
        ];
    }

    /**
     * @return void
     */
    public function testFilterSejoursByTypePec(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setTypePecFilter(['M', 'C']);

        $expected_where = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`type_pec` IN ('M', 'C') OR `sejour`.`type_pec` IS NULL",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return void
     */
    public function testFilterSejoursByCircuitAmbu(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setCircuitAmbuFilter(['court', 'long']);

        $expected_where = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`circuit_ambu` IN ('court', 'long') OR `sejour`.`circuit_ambu` IS NULL",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return void
     */
    public function testFilterSejoursWithOperationDateEqualsToEntry(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setDateOperationEqualsToEntryFilter(true);

        $expected_where = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`operations`.`date` = DATE(`sejour`.`entree_prevue`)',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        $expected_ljoin = [
            'operations' => '`operations`.`sejour_id` = `sejour`.`sejour_id`',
            'patients'   => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @dataProvider filterSejoursByPaymentExceedingFeesProvider
     *
     * @param CGroups $group
     * @param string  $filter
     * @param array   $expected_where
     * @param array   $expected_ljoin
     *
     * @return void
     */
    public function testFilterSejoursByPaymentExceedingFees(
        CGroups $group,
        string $filter,
        array $expected_where,
        array $expected_ljoin
    ): void {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setPaymentExceedingFeesFilter($filter);
        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @return array[]
     */
    public function filterSejoursByPaymentExceedingFeesProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "(
    (`operations`.`depassement` > 0 AND `operations`.`reglement_dh_chir` != 'non_regle')
    OR `operations`.`depassement` = 0 OR `operations`.`depassement` IS NULL
) AND (
    (`operations`.`depassement_anesth` > 0 AND `operations`.`reglement_dh_anesth` != 'non_regle')
    OR `operations`.`depassement_anesth` = 0 OR `operations`.`depassement_anesth` IS NULL
) AND (`operations`.`depassement` > 0 OR `operations`.`depassement_anesth` > 0)",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "(`operations`.`depassement` > 0 AND `operations`.`reglement_dh_chir` = 'non_regle')"
            . " OR (`operations`.`depassement_anesth` > 0 AND `operations`.`reglement_dh_anesth` = 'non_regle')",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $ljoin = [
            'operations' => '`operations`.`sejour_id` = `sejour`.`sejour_id`',
            'patients'   => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        return [
            [$group, AdmissionsRepository::FILTER_PAYMENT_EXCEEDING_FEES_PAYED, $where_1, $ljoin],
            [$group, AdmissionsRepository::FILTER_PAYMENT_EXCEEDING_FEES_UNPAID, $where_2, $ljoin],
        ];
    }

    /**
     * @return void
     */
    public function testFilterSejoursByPrestations(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setPrestationsFilter([1, 2]);

        $expected_where = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`item_prestation`.`object_class` = 'CPrestationPonctuelle'",
            "`prestation_ponctuelle`.`prestation_ponctuelle_id` IN ('1', '2')",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        $expected_ljoin = [
            'item_liaison'    => '`item_liaison`.`sejour_id` = `sejour`.`sejour_id`',
            'item_prestation' => '`item_prestation`.`item_prestation_id` = `item_liaison`.`item_souhait_id`',
            'prestation_ponctuelle'
                              => '`prestation_ponctuelle`.`prestation_ponctuelle_id` = `item_prestation`.`object_id`',
            'patients'        => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @dataProvider filterSejoursByLeaveModesProvider
     *
     * @param CGroups $group
     * @param array   $filter
     * @param array   $expected_where
     *
     * @return void
     */
    public function testFilterSejoursByLeaveModes(CGroups $group, array $filter, array $expected_where): void
    {
        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setLeaveModesFilters($filter);
        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function filterSejoursByLeaveModesProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`mode_sortie` IS NULL',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`mode_sortie` IN ('normal', 'transfert')",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_3 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`mode_sortie` IS NULL OR `sejour`.`mode_sortie` IN ('normal', 'transfert')",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        return [
            [$group, [''], $where_1],
            [$group, ['normal', 'transfert'], $where_2],
            [$group, ['', 'normal', 'transfert'], $where_3],
        ];
    }

    /**
     * @dataProvider filterUnpreparedSejoursProvider
     *
     * @param CGroups $group
     * @param string  $sejour_view
     * @param array   $expected_where
     *
     * @return void
     */
    public function testFilterUnpreparedSejours(CGroups $group, string $sejour_view, array $expected_where): void
    {
        $repository = new AdmissionsRepository($group, new CMediusers(), $sejour_view);
        $repository->setUnpreparedSejoursFilter(true);

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function filterUnpreparedSejoursProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`entree_preparee` = '0'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`entree_preparee` = '0'",
            "`sejour`.`entree_reelle` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_3 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`sortie_preparee` = '0'",
            "`sejour`.`sortie_prevue` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        return [
            [$group, AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS, $where_1],
            [$group, AdmissionsRepository::SEJOUR_VIEW_REAL_ADMISSIONS, $where_2],
            [$group, AdmissionsRepository::SEJOUR_VIEW_SORTIES_SCHEDULED, $where_3],
        ];
    }

    /**
     * @dataProvider filterSejoursByStatusPecProvider
     *
     * @param CGroups $group
     * @param string  $sejour_view
     * @param string  $filter
     * @param array   $expected_where
     *
     * @return void
     */
    public function testFilterSejoursByStatusPec(
        CGroups $group,
        string $sejour_view,
        string $filter,
        array $expected_where
    ): void {
        $repository = new AdmissionsRepository($group, new CMediusers(), $sejour_view);
        $repository->setStatusPecFilter($filter);

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function filterSejoursByStatusPecProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $where_1 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`pec_accueil` IS NULL',
            '`sejour`.`entree_reelle` IS NOT NULL',
            "`sejour`.`sortie` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_2 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`pec_accueil` IS NULL',
            "`sejour`.`entree_reelle` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_3 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`pec_accueil` IS NOT NULL',
            '`sejour`.`pec_service` IS NULL',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        $where_4 = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`pec_service` IS NOT NULL',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",

        ];

        return [
            [
                $group,
                AdmissionsRepository::SEJOUR_VIEW_SORTIES,
                AdmissionsRepository::FILTER_STATUS_PEC_WAITING,
                $where_1,
            ],
            [
                $group,
                AdmissionsRepository::SEJOUR_VIEW_REAL_ADMISSIONS,
                AdmissionsRepository::FILTER_STATUS_PEC_WAITING,
                $where_2,
            ],
            [
                $group,
                AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS,
                AdmissionsRepository::FILTER_STATUS_PEC_ONGOING,
                $where_3,
            ],
            [
                $group,
                AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS,
                AdmissionsRepository::FILTER_STATUS_PEC_OVER,
                $where_4,
            ],
        ];
    }

    /**
     * @dataProvider filterScheduledSejoursProvider
     *
     * @param CGroups           $group
     * @param DateTimeImmutable $date
     * @param string            $sejour_view
     * @param array             $expected_where
     *
     * @return void
     */
    public function testFilterScheduledSejours(
        CGroups $group,
        DateTimeImmutable $date,
        string $sejour_view,
        array $expected_where
    ): void {
        $repository = new AdmissionsRepository($group, new CMediusers(), $sejour_view);
        $repository->setScheduledSejoursFilter(true);
        $request = $repository->getLoadSejoursRequest($date);

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function filterScheduledSejoursProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = new DateTimeImmutable();
        $date_str = $date->format('Y-m-d');

        $where_entree = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`entree_reelle` IS NULL OR `sejour`.`entree_reelle` = '0000-00-00'",
            "`sejour`.`entree` BETWEEN '{$date_str} 00:00:00' AND '{$date_str} 23:59:59'",
        ];

        $where_sortie = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`sejour`.`sortie_reelle` IS NULL OR `sejour`.`sortie_reelle` = '0000-00-00'",
            "`sejour`.`sortie` BETWEEN '{$date_str} 00:00:00' AND '{$date_str} 23:59:59'",
        ];

        return [
            [$group, $date, AdmissionsRepository::SEJOUR_VIEW_ADMISSIONS, $where_entree],
            [$group, $date, AdmissionsRepository::SEJOUR_VIEW_SORTIES, $where_sortie],
        ];
    }


    /**
     * @return void
     */
    public function testFilterSejoursWithoutRealLeaveDate(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setWithoutRealLeaveDateFilter(true);

        $expected_where = [
            "`sejour`.`group_id` = '{$group->_id}'",
            '`sejour`.`sortie_reelle` IS NULL',
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return void
     */
    public function testFilterSejoursByNotificationStatus(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = (new DateTimeImmutable())->format('Y-m-d');

        $repository = new AdmissionsRepository($group, new CMediusers());
        $repository->setNotificationStatusFilter('pending');

        $expected_where = [
            "`sejour`.`group_id` = '{$group->_id}'",
            "`notifications`.`context_class` = 'CSejour'",
            "`notification_events`.`context_moment` = 'day_before'",
            "`sms`.`status` = 'pending' OR `mails`.`status` = 'pending'",
            "`sejour`.`entree` BETWEEN '{$date} 00:00:00' AND '{$date} 23:59:59'",
        ];

        $expected_ljoin = [
            'notifications' => '`notifications`.`context_id` = `sejour`.`sejour_id`',
            'notification_events' => '`notification_events`.`notification_event_id` = `notifications`.`event_id`',
            'sms' => "`sms`.`sms_id` = `notifications`.`message_id` AND `notifications`.`message_class` = 'CSMS'",
            'mails' => "`mails`.`mail_id` = `notifications`.`message_id` AND `notifications`.`message_class` = 'CMail'",
            'patients' => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];

        $request = $repository->getLoadSejoursRequest(new DateTimeImmutable());

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }
}
