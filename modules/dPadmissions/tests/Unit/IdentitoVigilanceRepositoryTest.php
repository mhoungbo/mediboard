<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Tests\Unit;

use DateTimeImmutable;
use Exception;
use Ox\Core\CSQLDataSource;
use Ox\Mediboard\Admissions\IdentitoVigilanceRepository;
use Ox\Mediboard\Bloc\CPlageOp;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;

class IdentitoVigilanceRepositoryTest extends OxUnitTestCase
{
    /**
     * @dataProvider getLoadSejoursRequestProvider
     *
     * @param CGroups           $group
     * @param DateTimeImmutable $date
     * @param bool              $display_yesterday
     * @param bool              $cancelled_sejours
     * @param bool              $handle_emergencies
     * @param array             $expected_where
     *
     * @return void
     */
    public function testGetLoadSejoursRequest(
        CGroups $group,
        DateTimeImmutable $date,
        bool $display_yesterday,
        bool $cancelled_sejours,
        bool $handle_emergencies,
        array $expected_where
    ): void {
        $repository = new IdentitoVigilanceRepository($group, $date, $display_yesterday);
        $repository->setCancelledFilter($cancelled_sejours);
        $repository->setHandleEmergencies($handle_emergencies);
        $request = $repository->getLoadSejoursRequest();

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function getLoadSejoursRequestProvider(): array
    {
        $group = new CGroups();
        $group->_id = $group->group_id = 10;

        $date = new DateTimeImmutable();

        $where_1 = [
            'entree' => "BETWEEN '" . $date->format('Y-m-d') . "' AND '"
                . $date->modify('+1 day')->format('Y-m-d') . "'",
            'group_id' => " = {$group->_id}",
            'type' => CSQLDataSource::prepareIn(CSejour::getTypesSejoursUrgence()),
        ];

        $where_2 = [
            'entree' => "BETWEEN '" . $date->modify('-1 day')->format('Y-m-d') . "' AND '"
                . $date->modify('+1 day')->format('Y-m-d') . "'",
            'group_id' => " = {$group->_id}",
            'annule' => " = '0'"
        ];

        return [
            [$group, $date, false, true, true, $where_1],
            [$group, $date, true, false, false, $where_2],
        ];
    }

    /**
     * @dataProvider loadSejoursErrorsProvider
     *
     * @param CSejour $sejour
     *
     * @return void
     * @throws Exception
     */
    public function testLoadSejoursErrors(CSejour $sejour): void
    {
        $repository = new IdentitoVigilanceRepository(new CGroups(), new DateTimeImmutable(), false, $sejour);
        $repository->disableMassLoads();
        $repository->guessPossibleMerges();

        $this->assertEquals([], $repository->getSejours());
    }

    /**
     * @return array[]
     */
    public function loadSejoursErrorsProvider(): array
    {
        $sejour = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadListByReq'])
            ->getMock();
        $sejour->method('loadListByReq')->willReturnOnConsecutiveCalls(
            null,
            $this->throwException(new Exception('SQL Error'))
        );

        return [
            [$sejour],
            [$sejour],
        ];
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testGuessPossibleMerges(): void
    {
        $operation_1 = $this->getMockOperation();
        $operation_2 = $this->getMockOperation();

        /* Patient sans sibling, sans phoning, sejour avec rpu, avec une operation */
        $patient_1 = $this->getMockPatient(1, 'bbbb', [], []);
        $sejour_1 = $this->getMockSejour(1, $patient_1, [$operation_1], [3]);

        /* Patient sans sibling, sans phoning, sejou avec 2 rpu, avec une operation */
        $patient_2 = $this->getMockPatient(2, 'aaaa', [], []);
        $sejour_2 = $this->getMockSejour(2, $patient_2, [$operation_1], [1, 2]);

        /* Patient sans sibling, sans phoning, sejour sans rpu, avec 2 operation */
        $patient_3 = $this->getMockPatient(3, 'cccc', [], []);
        $sejour_3 = $this->getMockSejour(3, $patient_3, [$operation_1, $operation_2], []);

        /* Patient avec siblings, sans phoning, sejour sans rpu, avec 1 operation */
        $patient_4 = $this->getMockPatient(4, 'dddd', [], []);
        $sejour_4 = $this->getMockSejour(4, $patient_4, [$operation_1], []);
        $patient_5 = $this->getMockPatient(5, 'dddd', [4 => $patient_4], []);
        $sejour_5 = $this->getMockSejour(5, $patient_5, [$operation_1], []);

        /* Patient sans siblings, avec phoning, sejour sans rpu, avec 1 operation */
        $patient_6 = $this->getMockPatient(6, 'eeef', [], []);
        $sejour_6 = $this->getMockSejour(6, $patient_6, [$operation_1], [1]);
        $patient_7 = $this->getMockPatient(7, 'eeee', [], [6 => $patient_6]);
        $sejour_7 = $this->getMockSejour(7, $patient_7, [$operation_1], [1]);

        /* Patient sans sibling, sans phoning, sejour sans rpu, avec une operation */
        $patient_8 = $this->getMockPatient(8, 'ffff', [], []);
        $sejour_8 = $this->getMockSejour(8, $patient_8, [$operation_1], []);

        /* Un autre sejour avec le patient précédent */
        $sejour_9 = $this->getMockSejour(9, $patient_8, [$operation_1], []);

        $sejour_loader = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadListByReq'])
            ->getMock();
        $sejour_loader->method('loadListByReq')->willReturn([
            1 => $sejour_1,
            2 => $sejour_2,
            3 => $sejour_3,
            4 => $sejour_4,
            5 => $sejour_5,
            6 => $sejour_6,
            7 => $sejour_7,
            8 => $sejour_8,
            9 => $sejour_9,
        ]);

        $expected_results = [
            1 => [
                'mergeable' => false,
                'siblings'  => [],
                'phonings'  => [],
            ],
            2 => [
                'mergeable' => true,
                'siblings'  => [],
                'phonings'  => [],
            ],
            3 => [
                'mergeable' => true,
                'siblings'  => [],
                'phonings'  => [],
            ],
            5 => [
                'mergeable' => true,
                'siblings'  => [4],
                'phonings'  => [],
            ],
            6 => [
                'mergeable' => true,
                'siblings'  => [],
                'phonings'  => [],
            ],
            7 => [
                'mergeable' => true,
                'siblings'  => [],
                'phonings'  => [6],
            ],
            8 => [
                'mergeable' => true,
                'phonings'  => [],
                'siblings'  => [],
            ],
            4 => [
                'mergeable' => true,
                'phonings'  => [],
                'siblings'  => [],
            ],
        ];
        $expected_count = 6;
        $expected_patients = [
            2 => $patient_2,
            1 => $patient_1,
            3 => $patient_3,
            5 => $patient_5,
            7 => $patient_7,
            8 => $patient_8,
            4 => $patient_4,
            6 => $patient_6,
        ];

        $repository = new IdentitoVigilanceRepository(new CGroups(), new DateTimeImmutable(), false, $sejour_loader);
        $repository->disableMassLoads();
        $repository->setHandleEmergencies(true);

        $this->assertEquals($expected_results, $repository->guessPossibleMerges());
        $this->assertEquals($expected_count, $repository->getMergeableCount());
        $this->assertEquals($expected_patients, $repository->getPatients());
    }

    protected function getMockPatient(int $id, string $name, array $siblings, array $phonings): CPatient
    {
        $patient = $this->getMockBuilder(CPatient::class)
            ->disableOriginalConstructor()
            ->onlyMethods([
                'getSiblings',
                'getPhoning',
            ])
            ->getMock();

        $patient->method('getSiblings')->willReturn($siblings);
        $patient->method('getPhoning')->willReturn($phonings);

        $patient->patient_id = $patient->_id = $id;
        $patient->nom = $name;

        return $patient;
    }

    /**
     * @return COperation
     */
    protected function getMockOperation(): COperation
    {
        $operation = $this->getMockBuilder(COperation::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadRefPlageOp'])
            ->getMock();

        $operation->method('loadRefPlageOp')->willReturn(new CPlageOp());

        return $operation;
    }

    /**
     * @param int          $id
     * @param CPatient     $patient
     * @param COperation[] $operations
     * @param array        $rpu_ids
     *
     * @return CSejour
     */
    protected function getMockSejour(int $id, CPatient $patient, array $operations, array $rpu_ids): CSejour
    {
        $sejour = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods([
                              'loadBackIds',
                              'loadNDA',
                              'loadRefPatient',
                              'loadRefsOperations'
                          ])
            ->getMock();

        $sejour->sejour_id = $sejour->_id = $id;
        $sejour->patient_id = $patient->_id;
        $sejour->_ref_patient = $patient;

        $sejour->method('loadBackIds')->willReturn($rpu_ids);
        $sejour->method('loadNDA')->willReturn(null);
        $sejour->method('loadRefsOperations')->willReturn($operations);
        $sejour->method('loadRefPatient')->willReturn($patient);

        return $sejour;
    }
}
