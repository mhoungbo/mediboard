<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admissions\Tests\Unit;

use DateTimeImmutable;
use Exception;
use Ox\Mediboard\Admissions\DischargeProjectRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;

/**
 *
 */
class DischargeProjectRepositoryTest extends OxUnitTestCase
{
    /**
     * @dataProvider loadSejoursProviders
     *
     * @param CSejour $sejour
     * @param array   $expected
     *
     * @return void
     */
    public function testLoadSejours(CSejour $sejour, array $expected): void
    {
        $repository = new DischargeProjectRepository(
            new CGroups(),
            new DateTimeImmutable(),
            new DateTimeImmutable(),
            $sejour
        );

        $this->assertEquals($expected, $repository->loadSejours());
    }

    /**
     * @return array[]
     */
    public function loadSejoursProviders(): array
    {
        $sejour = $this->getMockBuilder(CSejour::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadListByReq'])
            ->getMock();
        $sejour->method('loadListByReq')->willReturnOnConsecutiveCalls(
            [new CSejour()],
            null,
            $this->throwException(new Exception('SQL error'))
        );

        return [
            [$sejour, [new CSejour()]],
            [$sejour, []],
            [$sejour, []],
        ];
    }

    /**
     * @return void
     */
    public function testUserIdFilter(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date_max     = new DateTimeImmutable();
        $date_max_str = $date_max->format('Y-m-d');
        $date_min     = $date_max->modify('-5 days');
        $date_min_str = $date_min->format('Y-m-d');

        $expected_where = [
            '`sejour`.`praticien_id` = 28',
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_min_str}'"
            . " AND '{$date_max_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_min_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_max_str} 23:59:59')",
        ];
        $expected_ljoin = [
            'operations' => '`operations`.`sejour_id` = `sejour`.`sejour_id`',
            'patients'   => '`patients`.`patient_id` = `sejour`.`patient_id`',
        ];
        $expected_order = ['`sejour`.`sortie_prevue` ASC, `patients`.`nom`, `patients`.`prenom`'];


        $repository = new DischargeProjectRepository($group, $date_min, $date_max);
        $repository->setUserIdFilter(28);
        $request = $repository->prepareLoadSejourRequest();

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
        $this->assertEquals($expected_order, $request->order);
    }

    /**
     * @dataProvider sejourTypesFilterProvider
     *
     * @param CGroups           $group
     * @param DateTimeImmutable $date_min
     * @param DateTimeImmutable $date_max
     * @param array             $types
     * @param bool              $include
     * @param array             $expected_where
     *
     * @return void
     */
    public function testSejourTypesFilter(
        CGroups $group,
        DateTimeImmutable $date_min,
        DateTimeImmutable $date_max,
        array $types,
        bool $include,
        array $expected_where
    ): void {
        $repository = new DischargeProjectRepository($group, $date_min, $date_max);
        $repository->setSejourTypesFilter($types, $include);
        $request = $repository->prepareLoadSejourRequest();

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function sejourTypesFilterProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;


        $date_max     = new DateTimeImmutable();
        $date_max_str = $date_max->format('Y-m-d');
        $date_min     = $date_max->modify('+5 days');
        $date_min_str = $date_min->format('Y-m-d');

        $where_1 = [
            "`sejour`.`type` IN ('ambu', 'comp')",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_max_str}'"
            . " AND '{$date_min_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_max_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_min_str} 23:59:59')",
        ];

        $where_2 = [
            "`sejour`.`type` NOT IN ('seances', 'urg')",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_max_str}'"
            . " AND '{$date_min_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_max_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_min_str} 23:59:59')",
        ];

        return [
            [$group, $date_min, $date_max, ['ambu', 'comp'], true, $where_1],
            [$group, $date_min, $date_max, ['seances', 'urg'], false, $where_2],
        ];
    }

    /**
     * @return void
     */
    public function testHandicapFilter(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date     = new DateTimeImmutable();
        $date_str = $date->format('Y-m-d');

        $expected_where = [
            "`patient_handicap`.`handicap` IN ('malvoyant', 'malentendant')",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_str}'"
            . " AND '{$date_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_str} 23:59:59')",
        ];
        $expected_ljoin = [
            'operations' => '`operations`.`sejour_id` = `sejour`.`sejour_id`',
            'patients'   => '`patients`.`patient_id` = `sejour`.`patient_id`',
            'patient_handicap' => '`patient_handicap`.`patient_id` = `sejour`.`patient_id`'
        ];

        $repository = new DischargeProjectRepository($group, $date, $date);
        $repository->setHandicapFilter(['malvoyant', 'malentendant']);
        $request = $repository->prepareLoadSejourRequest();

        $this->assertEquals($expected_where, $request->where);
        $this->assertEquals($expected_ljoin, $request->ljoin);
    }

    /**
     * @dataProvider helpFilterProvider
     *
     * @param CGroups           $group
     * @param DateTimeImmutable $date
     * @param array             $filter
     * @param array             $expected_where
     *
     * @return void
     */
    public function testHelpFilter(
        CGroups $group,
        DateTimeImmutable $date,
        array $filter,
        array $expected_where
    ): void {
        $repository = new DischargeProjectRepository($group, $date, $date);
        $repository->setHelpFilter($filter);
        $request = $repository->prepareLoadSejourRequest();

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function helpFilterProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date     = new DateTimeImmutable();
        $date_str = $date->format('Y-m-d');

        $where_1 = [
            '`sejour`.`aide_organisee` IS NULL',
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_str}'"
            . " AND '{$date_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_str} 23:59:59')",
        ];

        $where_2 = [
            "`sejour`.`aide_organisee` IN ('repas')",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_str}'"
            . " AND '{$date_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_str} 23:59:59')",
        ];

        $where_3 = [
            "`sejour`.`aide_organisee` IN ('repas', 'entretien')",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_str}'"
            . " AND '{$date_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_str} 23:59:59')",
        ];

        $where_4 = [
            "`sejour`.`aide_organisee` IN ('repas', 'entretien') OR `sejour`.`aide_organisee` IS NULL",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_str}'"
            . " AND '{$date_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_str} 23:59:59')",
        ];

        return [
            [$group, $date, [''], $where_1],
            [$group, $date, ['repas'], $where_2],
            [$group, $date, ['repas', 'entretien'], $where_3],
            [$group, $date, ['repas', 'entretien', ''], $where_4],
        ];
    }

    /**
     * @return void
     */
    public function testPatientTutelleFilter(): void
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date     = new DateTimeImmutable();
        $date_str = $date->format('Y-m-d');

        $expected_where = [
            "`patients`.`tutelle` = 'curatelle'",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_str}'"
            . " AND '{$date_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_str} 23:59:59')",
        ];

        $repository = new DischargeProjectRepository($group, $date, $date);
        $repository->setPatientTutelleFilter('curatelle');
        $request = $repository->prepareLoadSejourRequest();

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @dataProvider dischargeModesFilterProvider
     *
     * @param CGroups           $group
     * @param DateTimeImmutable $date
     * @param array             $filter
     * @param array             $expected_where
     *
     * @return void
     */
    public function testDischargeModesFilter(
        CGroups $group,
        DateTimeImmutable $date,
        array $filter,
        bool $config,
        array $expected_where
    ): void {
        $repository = new DischargeProjectRepository($group, $date, $date);
        $repository->setDischargeModesFilter($filter, $config);
        $request = $repository->prepareLoadSejourRequest();

        $this->assertEquals($expected_where, $request->where);
    }

    /**
     * @return array[]
     */
    public function dischargeModesFilterProvider(): array
    {
        $group      = new CGroups();
        $group->_id = $group->group_id = 10;

        $date     = new DateTimeImmutable();
        $date_str = $date->format('Y-m-d');

        $where_1 = [
            "`sejour`.`mode_sortie_id` IN ('1', '2')",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_str}'"
            . " AND '{$date_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_str} 23:59:59')",
        ];

        $where_2 = [
            "`sejour`.`mode_sortie` IN ('normal', 'transfert')",
            "`sejour`.`group_id` = {$group->_id}",
            "(`operations`.`operation_id` IS NOT NULL AND `operations`.`date` BETWEEN '{$date_str}'"
            . " AND '{$date_str}') OR (`operations`.`operation_id` IS NULL AND "
            . "`sejour`.`sortie` >= '{$date_str} 00:00:00' AND `sejour`.`sortie` <= '{$date_str} 23:59:59')",
        ];

        return [
            [$group, $date, [1, 2], true, $where_1],
            [$group, $date, ['normal', 'transfert'], false, $where_2],
        ];
    }
}
