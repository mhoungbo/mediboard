/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Group = window.Group || {
  form_name:             null,
  field_etab_id:         null,
  field_etab_view:       null,
  field_etab_adresse:    null,
  path_export_external:  '',
  path_external_groups:  '',
  path_external:         '',
  path_manage_edit:      '',
  path_manage_structure: '',
  path_import_group:     '',
  path_delete:           '',
  path_list:           '',

  addedit: function (group_id, callback) {
    const url = new Url('etablissement', 'etablissement_manage_edit').setRoute(Group.path_manage_edit);
    url.addParam('type', 'group');

    if (group_id) {
      url.addParam('group_id', group_id);
    }

    url.requestModal("50%", "90%", callback);
  },

  viewStructure: function (group_id) {
    new Url('etablissement', 'etablissement_manage_structure').setRoute((Group.path_manage_structure).replace('999999', group_id))
      .popup(500, 500, 'structure_etab');
  },

  addeditLegalEntity: function (legal_entity_id) {
    const url = new Url('etablissement', 'etablissement_manage_edit').setRoute(Group.path_manage_edit);
    url.addParam('type', 'legal_entity');

    if (legal_entity_id) {
      url.addParam('legal_entity_id', legal_entity_id);
    }

    url.requestModal("50%", "70%");
  },

  editCEtabExterne: function (etab_id, selected) {
    new Url('etablissement', 'etablissement_externals_external').setRoute(Group.path_external)
      .addParam("edit", '1')
      .addParam("etab_id", etab_id)
      .addParam("selected", selected)
      .requestModal("50%", "70%");
  },

  uploadSaveUID: function (uid, route_path_import) {
    const uploadForm = getForm("upload-import-file-form");

    new Url().setRoute(route_path_import)
      .addParam("uid", uid)
      .addParam("service", $V(uploadForm.elements.type_service))
      .addParam("function", $V(uploadForm.elements.type_function))
      .addParam("user", $V(uploadForm.elements.type_user))
      .addParam("bloc", $V(uploadForm.elements.type_bloc))
      .addParam("salle", $V(uploadForm.elements.type_salle))
      .addParam("uf", $V(uploadForm.elements.type_uf))
      .requestUpdate("import-steps");

    uploadForm.down(".upload-ok").show();
    uploadForm.down(".upload-error").hide();
  },
  uploadError:   function () {
    const uploadForm = getForm("upload-import-file-form");

    uploadForm.down(".upload-ok").hide();
    uploadForm.down(".upload-error").show();
  },
  uploadReset:   function () {
    const uploadForm = getForm("upload-import-file-form");

    uploadForm.down(".upload-ok").hide();
    uploadForm.down(".upload-error").hide();
  },
  /**
   * Change page
   */
  changePage: function (page) {
    new Url().setRoute(Group.path_external_groups)
      .addParam('page', page)
      .requestUpdate("list_etab_externe");
  },
  /**
   * Reload external etablishment list
   */
  reloadListEtabExternes: function (selected) {
    new Url().setRoute(Group.path_external_groups)
      .addParam("selected", selected)
      .requestUpdate("list_etab_externe");
  },
  /**
   * Get external etablishment list with some filters
   */
  listEtabExternes: function (form) {
    new Url().setRoute(Group.path_external_groups)
      .addParam('nom', $V(form.nom))
      .addParam('cp', $V(form.cp))
      .addParam('ville', $V(form.ville))
      .addParam('finess', $V(form.finess))
      .addParam('selected', $V(form.selected))
      .requestUpdate("list_etab_externe");
  },
  listLegalEntities: function () {
    new Url().setRoute(Group.path_list)
      .addParam('type', 'legal_entity')
      .requestUpdate('list_legal_entity');
  },
  listGroups: function () {
    new Url().setRoute(Group.path_list)
      .addParam('type', 'group')
      .requestUpdate('list_groups');
  },
  /**
   * Reload external etablishment line
   */
  reloadEtabExterneLine: function (etab_guid, selected) {
    const etab_id = etab_guid.split("-")[1];

    new Url().setRoute(Group.path_external)
      .addParam("reload", '1')
      .addParam("etab_id", etab_id)
      .addParam("selected", selected)
      .requestUpdate(etab_guid + "-row");
  },
  /**
   * Select external establishment
   */
  selectEtabExterne: function (element) {
    const form = document[Group.form_name];
    const etab_id = element.get('id');
    const etab_nom = element.get('nom');
    const adresse = element.get('adresse');
    const adresse_complet = element.get('adresse_complet');
    const dest_address_etab = $("dest_address_etab");

    $V(form[Group.field_etab_id], etab_id);
    $V(form[Group.field_etab_view], etab_nom);

    // Module Transport
    if (dest_address_etab) {
      $V(form["etablissement_destination_class"], "CEtabExterne");

      if (!adresse) {
        $('save_ask_transport').disabled = true;
        alert($T('CTransport-msg-Your establishment does not have an address, please fill in one or change your establishment'));
        return false;
      } else {
        $('save_ask_transport').disabled = false;

        $('labelFor_editTransport_transport_output_transfer').className = "notNullOK";

        dest_address_etab.down("td").innerHTML = adresse_complet;

        Control.Modal.close();
      }
    }
  },
  /**
   * Export des établissements externes au format CSV
   */
  exportExternalGroup: function () {
    new Url().setRoute(Group.path_export_external).popup(400, 150);
  },
  deleteEntity:        function (entity_id, type) {
    Modal.confirm(
      'Confirmer la suppression ?',
      {
        onOK: function () {
          new Url().setRoute(Group.path_delete)
            .addParam('entity_id', entity_id)
            .addParam('type', type)
            .requestUpdate("systemMsg", {
              method: 'post'
            });
          Control.Modal.close();

          if (type === 'legal_entity') {
            Group.listLegalEntities();
          }

          if (type === 'group') {
            Group.listGroups();
          }
        }
      }
    );
  }
};
