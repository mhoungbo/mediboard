{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main tbl">
  <tr>
    <th class="title" colspan="5">
        {{if $can->edit}}
          <button onclick="Group.addedit()" class="new me-primary" style="float: left">
              {{tr}}CGroups-button new{{/tr}}
          </button>
        {{/if}}
        {{tr}}CGroups all{{/tr}}
    </th>
  </tr>
  <tr>
  <tr>
    <th class="category"></th>
    <th class="category">{{tr}}CGroups-title name{{/tr}}</th>
    <th class="category">{{tr}}CGroups-title associated functions{{/tr}}</th>
    <th class="category">{{tr}}CGroups-title associated legal entity{{/tr}}</th>
    <th class="category">{{tr}}CGroups-title structure{{/tr}}</th>
  </tr>
    {{foreach from=$groups item=_group}}
      <tr id="row-{{$_group->_guid}}">
          {{if $can->edit}}
            <td class="narrow">
              <button class="edit notext" onclick="Group.addedit('{{$_group->_id}}')"></button>
            </td>
          {{/if}}
        <td>
            {{mb_value object=$_group field=text}}
        </td>
        <td class="text narrow">
            {{if $_group->_ref_functions}}
                {{$_group->_ref_functions|@count}}
            {{/if}}
        </td>
        <td class="narrow text">
            {{if $_group->legal_entity_id}}
                {{$_group->_ref_legal_entity->name}}
            {{else}}
              <span class="empty">{{tr}}CLegalEntity.none{{/tr}}</span>
            {{/if}}
        </td>
        <td class="narrow">
          <button type="button" class="lookup" onclick="Group.viewStructure('{{$_group->_id}}');">
              {{tr}}CGroups-button see structure{{/tr}}
          </button>
        </td>
      </tr>
    {{/foreach}}
</table>
