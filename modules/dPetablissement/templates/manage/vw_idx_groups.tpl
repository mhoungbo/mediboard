{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=patients script=autocomplete}}
{{mb_script module=dPetablissement script=Group}}

<script>
  Main.add(function () {
    Group.path_manage_edit = '{{url name=etablissement_manage_edit}}';
    Group.path_manage_structure = '{{url name=etablissement_manage_structure group_id='999999'}}';
    Group.path_delete = '{{url name=etablissement_manage_delete}}';
    Group.path_list = '{{url name=etablissement_manage_list}}';

    Group.listLegalEntities();
    Group.listGroups();
  });
</script>

<table class="main layout">
  <tr>
    <td class="halfPane">
      <!-- Liste des entitÚs juridiques -->
      <div id="list_legal_entity" class="me-padding-0"></div>
    </td>

    <td class="halfPane">
      <!-- Liste des Útablissements -->
      <div id="list_groups" class="me-padding-0"></div>
    </td>
  </tr>
</table>


