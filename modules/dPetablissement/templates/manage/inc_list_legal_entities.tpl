{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main tbl">
  <tr>
    <th class="title" colspan="3">
      <button type="button" class="new me-primary" onclick="Group.addeditLegalEntity()"
              style="float: left">{{tr}}CLegalEntity new{{/tr}}</button>
        {{tr}}CLegalEntity all{{/tr}}
    </th>
  </tr>
  <tr>
    <th class="category"></th>
    <th class="category">{{mb_label class=CLegalEntity field=name}}</th>
    <th class="category">{{mb_label class=CLegalEntity field=code}}</th>
  </tr>
    {{foreach from=$legal_entities item=_legal_entity}}
      <tr>
        <td class="narrow">
          <button class="edit notext" onclick="Group.addeditLegalEntity('{{$_legal_entity->_id}}')"></button>
        </td>
        <td>
            {{mb_value object=$_legal_entity field=name}}
        </td>
        <td>
            {{mb_value object=$_legal_entity field=code}}
        </td>
      </tr>
        {{foreachelse}}
      <tr>
        <td class="empty" colspan="3">{{tr}}CLegalEntity.none{{/tr}}</td>
      </tr>
    {{/foreach}}
</table>
