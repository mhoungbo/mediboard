{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{csrf_token id=etablissement_import var=csrf_token}}

<script>
  function importSae() {
    new Url().setRoute('{{url name=etablissement_import}}')
      .addParam("type", 'sae')
      .addParam("token", '{{$csrf_token}}')
      .requestUpdate("import-log", {
        method: "post"
      });
  }
</script>

<h2>Paramètres de la base SAE</h2>

{{mb_include module=system template=datasources/configure_dsn dsn=sae}}

<table class="main tbl">
  <tr>
    <th class="title" colspan="2">
      Import de la base
    </th>
  <tr>
    <td class="narrow">
      <button
        onclick="importSae();"
        class="change">
          {{tr}}Import{{/tr}}
      </button>
    </td>
    <td id="import-log"></td>
  </tr>
</table>
