<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Etablissement;

use Ox\Mediboard\System\AbstractConfigurationRegister;
use Ox\Mediboard\System\ConfigurationManager;

/**
 * @codeCoverageIgnore
 */
class CConfigurationEtablissement extends AbstractConfigurationRegister
{
    public function registerStatic(ConfigurationManager $manager): void
    {
        $manager->registerStatic(
            [
                'general' => [
                    'dossiers_medicaux_shared' => 'bool default|0',
                ],
            ]
        );
    }
}
