<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Etablissement\Controllers;

use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CMbPath;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Etablissement\CGroupsImport;
use Ox\Mediboard\Etablissement\Import\GroupsXMLExport;
use Ox\Mediboard\Forms\CExClassImport;
use Ox\Mediboard\Mediusers\CFunctions;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class GroupsImportExportController extends Controller
{
    public function vwImportGroup(): Response
    {
        return $this->renderSmarty('import/vw_import_group.tpl');
    }

    /**
     * @throws Exception
     */
    public function uploadImportGroup(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('etablissement_import_group_upload', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        /** @var UploadedFile $tmp_file */
        $tmp_file     = ($params->getRequest()->files->get('import'));
        $tmp_filename = $tmp_file->getPathname();

        try {
            $dom = new DOMDocument();
            $dom->load($tmp_filename);

            $xpath = new DOMXPath($dom);
            if ($xpath->query("/mediboard-export")->length == 0) {
                CAppUI::js("window.parent.Group.uploadError()");

                return $this->renderEmptyResponse();
            }
        } catch (Exception $e) {
            CAppUI::js("window.parent.Group.uploadError()");

            return $this->renderEmptyResponse();
        }

        $temp = CAppUI::getTmpPath("group_import");
        $uid  = preg_replace('/[^\d]/', '', uniqid("", true));
        CMbPath::forceDir($temp);

        move_uploaded_file($tmp_filename, "{$temp}/{$uid}");

        // Cleanup old files (more than 4 hours old)
        $other_files = glob("{$temp}/*");
        $now         = time();
        foreach ($other_files as $_other_file) {
            if (filemtime($_other_file) < $now - (3600 * 4)) {
                unlink($_other_file);
            }
        }

        $route_path_import = $this->generateUrl('etablissement_import_group_ajax');

        CAppUI::js("window.parent.Group.uploadSaveUID('{$uid}', '{$route_path_import}')");

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    public function importGroup(RequestParams $params): Response
    {
        $uid     = $params->post("file_uid", "str notNull");
        $options = $params->post("options", "str");

        if (!$this->isCsrfTokenValid('etablissement_import_group_import', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if (!($import_tag = trim($params->post("import_tag", "str")))) {
            $this->addUiMsgError("CPatientXMLImport-Error-Import tag missing");
        }

        if (is_array($options)) {
            $options = CMbArray::mapRecursive("stripslashes", $options);
        }

        if (!$options) {
            $options = null;
        }

        $temp = CAppUI::getTmpPath("group_import");
        $uid  = preg_replace('/\D/', '', $uid);

        try {
            $import = new CGroupsImport("{$temp}/{$uid}");
            if ($import_tag) {
                $import->setImportTag($import_tag);
            }
            $import->import($params->post("fromdb", "str"), $options);
        } catch (Exception $e) {
            $this->addUiMsgWarning($e->getMessage());
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function exportGroup(CGroups $group, RequestParams $params): Response
    {
        $function_select               = $params->get("function_select", "str");
        CStoredObject::$useObjectCache = false;
        $object                        = $group;

        if (!$group || !$group->_id) {
            throw new Exception('A group is mandatory for export');
        }

        if (($function_select !== null) && ($function_select !== 'all')) {
            $function = CFunctions::findOrFail($function_select);

            // If group_id is passed, check if function is in this group
            $ids = [];
            if ($group && $group->_id) {
                foreach ($group->loadFunctions() as $_group_function) {
                    $ids[] = $_group_function->_id;
                }

                if (!in_array($function->_id, $ids)) {
                    throw new Exception('This function is not in this group');
                }
            }

            $object = $function;
        }

        try {
            $export = new GroupsXMLExport($object);
            $export->streamXML();
        } catch (Exception $e) {
            $this->addUiMsgError($e->getMessage());
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    public function ajaxImportGroup(RequestParams $params): Response
    {
        $uid  = $params->get("uid", 'str');
        $uid  = preg_replace('/\D/', '', $uid);
        $temp = CAppUI::getTmpPath("group_import");
        $file = "{$temp}/{$uid}";

        if (!file_exists($file)) {
            $this->addUiMsgError("CFile-not-exists", false, $file);
        }

        $data       = [];
        $group_name = '';

        try {
            $import = new CExClassImport($file);

            /** @var DOMElement $group */
            $group      = $import->getElementsByClass("CGroups")->item(0);
            $group_name = $import->getNamedValueFromElement($group, "text");

            if ($params->get("service", 'str') == "true") {
                $data["CService"] = $import->getObjectsList("CService", "nom");
            }

            if ($params->get("function", 'str') == "true") {
                $data["CFunctions"] = $import->getObjectsList("CFunctions", "text");
            }

            if ($params->get("user", 'str') == "true") {
                $data["CUser"] = $import->getObjectsList("CUser", "user_username", true, false, true);
            }

            if ($params->get("bloc", 'str') == "true") {
                $data["CBlocOperatoire"] = $import->getObjectsList("CBlocOperatoire", "nom");
            }

            if ($params->get("salle", 'str') == "true") {
                $data["CSalle"] = $import->getObjectsList("CSalle", "nom");
            }

            if ($params->get("uf", 'str') == "true") {
                $data["CUniteFonctionnelle"] = $import->getObjectsList("CUniteFonctionnelle", "libelle");
            }
        } catch (Exception $e) {
            $this->addUiMsgError($e->getMessage());
        }

        return $this->renderSmarty(
            'import/inc_import_group.tpl',
            [
                'group_name'        => $group_name,
                'uid'               => $uid,
                'current_user_guid' => $this->getCMediuser()->_guid,
                'data'              => $data,
            ]
        );
    }
}
