<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Etablissement\Controllers;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CMbObject;
use Ox\Core\Controller;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CEtabExterne;
use Ox\Mediboard\Etablissement\CSaeImport;
use Ox\Mediboard\Etablissement\Import\ExternalFacilitiesImporter;
use Ox\Mediboard\PlanningOp\CSejour;
use Symfony\Component\HttpFoundation\Response;

class GroupsExternalController extends Controller
{
    /**
     * @throws Exception
     */
    public function index(RequestParams $params): Response
    {
        return $this->renderSmarty(
            'external/vw_etab_externe.tpl',
            [
                'page'                 => $params->get("page", "num default|0"),
                'filter'               => new CEtabExterne(),
                'selected'             => $params->get("selected", "bool default|0"),
                'path_external_groups' => $this->generateUrl('etablissement_externals_list'),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function list(RequestParams $params): Response
    {
        $where = [];

        if ($nom = trim($params->get("nom", "str", true))) {
            $where['nom'] = "LIKE '%{$nom}%'";
        }

        if ($cp = trim($params->get("cp", "numchar", true))) {
            $where['cp'] = "LIKE '%{$cp}%'";
        }

        if ($ville = trim($params->get("ville", "str", true))) {
            $where['ville'] = "LIKE '%{$ville}%'";
        }

        if ($finess = str_replace(' ', '', $params->get("finess", "numchar", true))) {
            $where['finess'] = "LIKE '%{$finess}%'";
        }

        $etab_externe = new CEtabExterne();
        $page         = $params->get("page", "num default|0");
        $step         = 40;

        return $this->renderSmarty(
            'external/inc_vw_etab_externe.tpl',
            [
                'page'          => $page,
                'step'          => $step,
                'total'         => $etab_externe->countList($where),
                'etab_externes' => $etab_externe->loadList($where, "nom, cp, ville", "$page, $step"),
                'selected'      => $params->get("selected", "bool default|0"),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function external(RequestParams $params): Response
    {
        $etab_id = $params->get("etab_id", "ref class|CEtabExterne");

        if ($params->get("reload", "bool default|0")) {
            return $this->reloadLine($etab_id, $params);
        }

        if ($params->get("edit", "bool default|0")) {
            return $this->edit($etab_id, $params);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    private function reloadLine(int $etab_id, RequestParams $params): Response
    {
        return $this->renderSmarty(
            'external/inc_line_etab_externe.tpl',
            [
                '_etab'    => (new CEtabExterne())->load($etab_id),
                'selected' => $params->get("selected", "bool default|0"),
            ]
        );
    }

    /**
     * @throws Exception
     */
    private function edit(int $etab_id, RequestParams $params): Response
    {
        $etab_externe = new CEtabExterne();

        if ($etab_id) {
            $etab_externe->load($etab_id);
            $etab_externe->loadRefsNotes($etab_id);
        }

        return $this->renderSmarty(
            'external/inc_etab_externe.tpl',
            [
                'etab_externe' => $etab_externe,
                'selected'     => $params->get("selected", "bool default|0"),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function showImportTypes(RequestParams $params): Response
    {
        if (($type = $params->get('type', 'enum list|prov|dest default|prov')) === 'dest') {
            $trads = array_merge(["0"], CSejour::$destination_values);
        } else {
            $trads = explode('|', (new CEtabExterne())->_specs['provenance']->list);
        }

        return $this->renderSmarty(
            'import/vw_types_prov_dest.tpl',
            [
                'type'  => ($type == 'dest') ? 'destination' : 'provenance',
                'trads' => $trads,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function import(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('etablissement_import', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if ($params->post('type', "str") === 'external') {
            return $this->importExternal($params);
        } else {
            return $this->importSaeBase();
        }
    }

    /**
     * @throws Exception
     */
    public function exportExternal(): Response
    {
        $line = [
            $this->translator->tr('CEtabExterne-finess'),
            $this->translator->tr('CEtabExterne-siret'),
            $this->translator->tr('CEtabExterne-ape'),
            $this->translator->tr('CEtabExterne-nom'),
            $this->translator->tr('CEtabExterne-raison_sociale'),
            $this->translator->tr('CEtabExterne-adresse'),
            $this->translator->tr('CEtabExterne-cp'),
            $this->translator->tr('CEtabExterne-ville'),
            $this->translator->tr('CEtabExterne-tel'),
            $this->translator->tr('CEtabExterne-fax'),
            $this->translator->tr('CEtabExterne-provenance'),
            $this->translator->tr('CEtabExterne-destination'),
            $this->translator->tr('CEtabExterne-priority'),
        ];

        $csv = new CCSVFile(null, CCSVFile::PROFILE_EXCEL);
        $csv->writeLine($line);
        $csv->setColumnNames($line);

        foreach ((new CEtabExterne())->loadList() as $etab) {
            $csv->writeLine(
                [
                    $etab->finess,
                    $etab->siret,
                    $etab->ape,
                    $etab->nom,
                    $etab->raison_sociale,
                    $etab->adresse,
                    $etab->cp,
                    $etab->ville,
                    $etab->tel,
                    $etab->fax,
                    $etab->provenance,
                    $etab->destination,
                    $etab->priority,
                ]
            );
        }

        return $this->renderFileResponse($csv->getContent(), 'export-etab-externes.csv', 'application/csv');
    }

    /**
     * @throws Exception
     */
    private function importExternal(RequestParams $params): Response
    {
        CMbObject::$useObjectCache = false;
        CApp::setTimeLimit(3600);

        if (!($files = $params->getRequest()->files->get('formfile'))) {
            $this->addUiMsgWarning($this->translator->tr('common-error-No file found.'));

            return $this->renderEmptyResponse();
        }

        if (!($form_file = $files[0])) {
            return $this->renderEmptyResponse();
        }

        try {
            $importer = new ExternalFacilitiesImporter($form_file);
            $importer->doImport();
        } catch (Exception $e) {
            $this->addUiMsgError($e->getMessage());

            return $this->renderEmptyResponse();
        }

        $result = $importer->getImportResult();

        if ($count_created = $result["created"]) {
            $this->addUiMsgOk($this->translator->tr("CEtabExterne-import_created", $count_created));
        }

        if ($count_updated = $result["updated"]) {
            $this->addUiMsgOk($this->translator->tr("CEtabExterne-import_updated", $count_updated));
        }

        if ($count_error = $result["error"]) {
            $this->addUiMsgError($this->translator->tr("CEtabExterne-import_error", $count_error));
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    private function importSaeBase(): Response
    {
        $import = new CSaeImport();
        $import->import();

        foreach ($import->getMessages() as $message) {
            $this->addUiMsgOk(reset($message), false, ...$message);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    public function listExternalEtabAutocomplete(RequestParams $params): Response
    {
        $field       = $params->get('field', 'str');
        $view_field  = $params->get('view_field', "str default|{$field}");
        $input_field = $params->get('input_field', "str default|{$view_field}");
        $show_view   = $params->get('show_view', 'str default|false') == "true";
        $keywords    = $params->get($input_field, 'str');

        $this->enforceSlave();

        $where = [];
        if ($keywords) {
            $where['nom'] = " LIKE '%$keywords%'";
        }

        $etab_externe = new CEtabExterne();

        return $this->renderSmarty(
            'inc_field_autocomplete.tpl',
            [
                'matches'    => $etab_externe->loadList($where, '`priority` DESC, `nom` ASC', '50'),
                'field'      => $field,
                'view_field' => $view_field,
                'show_view'  => $show_view,
                'template'   => $etab_externe->getTypedTemplate("autocomplete"),
                'input'      => '',
                'nodebug'    => true,
            ],
            'system'
        );
    }
}
