<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Etablissement\Controllers;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFieldsets;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Bloc\CBlocOperatoire;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Etablissement\CLegalEntity;
use Ox\Mediboard\Etablissement\CLegalStatus;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class GroupsController extends Controller
{
    public const WITH_ROLES = 'with_roles';

    public const ROLE_MAIN      = 'is_main';
    public const ROLE_SECONDARY = 'is_secondary';

    public const GROUP_FIELD_NAME = '_name';

    /**
     * @throws ApiException
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     * @api
     */
    public function listGroups(RequestApi $request_api): Response
    {
        /** @var Collection $resource */
        $resource = Collection::createFromRequest($request_api, CGroups::loadGroups(PERM_EDIT));

        if ($request_api->getRequest()->query->get(self::WITH_ROLES, false)) {
            if ($this->getCUser()->loadRefMediuser()->_id) {
                $this->setRoles($resource);
            }
        }

        return $this->renderApiResponse($resource);
    }

    /**
     * @throws Exception
     * @api
     */
    public function listFunctions(CGroups $group, RequestApi $request_api): Response
    {
        $functions = $group->loadBackRefs(
            'functions',
            $request_api->getSortAsSql(),
            $request_api->getLimitAsSql(),
            null,
            null,
            null,
            null,
            $request_api->getFilterAsSQL($group->getDS())
        );

        /** @var Collection $resource */
        $resource = Collection::createFromRequest($request_api, $functions);
        $resource->createLinksPagination($request_api->getOffset(), $request_api->getLimit(), count($functions));

        return $this->renderApiResponse($resource);
    }

    /**
     * @throws ApiException
     * @api
     */
    public function showGroup(CGroups $group, RequestApi $request_api): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request_api, $group));
    }

    /**
     * @throws ApiException
     * @api
     */
    public function showFunction(CFunctions $function, RequestApi $request_api): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request_api, $function));
    }

    /**
     * @throws ApiException|CMbException
     * @api
     */
    public function createGroups(RequestApi $request_api): Response
    {
        $groups = $request_api->getModelObjectCollection(
            CGroups::class,
            [RequestFieldsets::QUERY_KEYWORD_ALL],
            [self::GROUP_FIELD_NAME]
        );

        $collection = $this->storeCollection($groups);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }

    /**
     * @throws ApiException|CMbException
     * @api
     */
    public function createFunctions(CGroups $group, RequestApi $request_api): Response
    {
        $functions = $request_api->getModelObjectCollection(
            CFunctions::class,
            [RequestFieldsets::QUERY_KEYWORD_ALL]
        );

        /** @var CFunctions $func */
        foreach ($functions as $func) {
            $func->group_id = $group->_id;
        }

        $collection = $this->storeCollection($functions);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }

    /**
     * @throws ApiException|CMbException
     * @api
     */
    public function updateGroup(CGroups $group, RequestApi $request_api): Response
    {
        /** @var CGroups $group_from_request */
        $group_from_request = $request_api->getModelObject(
            $group,
            [RequestFieldsets::QUERY_KEYWORD_ALL],
            [self::GROUP_FIELD_NAME]
        );

        $item = $this->storeObject($group_from_request);

        return $this->renderApiResponse($item, Response::HTTP_OK);
    }

    /**
     * @throws ApiException|CMbException
     * @api
     */
    public function updateFunction(CFunctions $function, RequestApi $request_api): Response
    {
        /** @var CFunctions $function_from_request */
        $function_from_request = $request_api->getModelObject(
            $function,
            [RequestFieldsets::QUERY_KEYWORD_ALL]
        );

        return $this->renderApiResponse($this->storeObject($function_from_request), Response::HTTP_OK);
    }

    /**
     * @throws CMbException
     * @api
     */
    public function deleteGroup(CGroups $group): Response
    {
        $this->deleteObject($group);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @throws CMbException
     * @api
     */
    public function deleteFunction(CFunctions $function): Response
    {
        $this->deleteObject($function);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Add is_main and is_secondary to groups depending on the calling user.
     * is_main = true : mean the group is the main group for the user
     * is_secondary = true : mean the group is a secondary group for the user
     *
     * @throws ApiException
     * @throws Exception
     */
    private function setRoles(Collection $groups): void
    {
        $current_user        = CMediusers::get();
        $main_function       = $current_user->loadRefFunction();
        $secondary_functions = $current_user->loadRefsSecondaryFunctions();
        $secondary_groups    = array_unique(CMbArray::pluck($secondary_functions, 'group_id'));

        /** @var Item $item */
        foreach ($groups as $item) {
            $group = $item->getDatas();

            $item->addAdditionalDatas([self::ROLE_MAIN => $main_function->group_id === $group->_id]);
            $item->addAdditionalDatas([self::ROLE_SECONDARY => in_array($group->_id, $secondary_groups)]);
        }
    }

    public function configure(): Response
    {
        return $this->renderSmarty('configure.tpl');
    }

    /**
     * @throws Exception
     */
    public function index(): Response
    {
        return $this->renderSmarty('manage/vw_idx_groups.tpl');
    }

    /**
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public function list(RequestParams $params): Response
    {
        if (($type = $params->get("type", 'enum notNull list|legal_entity|group')) === 'legal_entity') {
            return $this->renderSmarty(
                'manage/inc_list_legal_entities.tpl',
                [
                    'legal_entities' => (new CLegalEntity())->loadList(),
                ]
            );
        }

        if ($type === 'group') {
            // Récupération des fonctions
            $groups = CGroups::loadGroups();
            CStoredObject::massLoadFwdRef($groups, "legal_entity_id");

            foreach ($groups as $_group) {
                $_group->loadFunctions();
                $_group->loadRefLegalEntity();
            }

            return $this->renderSmarty(
                'manage/inc_list_groups.tpl',
                [
                    'groups' => $groups,
                ]
            );
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    public function edit(RequestParams $params): Response
    {
        if (($type = $params->get("type", 'enum notNull list|legal_entity|group')) === 'legal_entity') {
            // Retrieve selected legal entity
            $legal_entity = new CLegalEntity();

            if ($legal_entity_id = $params->get("legal_entity_id", 'ref class|CLegalEntity')) {
                $legal_entity->load($legal_entity_id);
                $legal_entity->loadRefUser();
            }

            $legal_status = [];
            if (CSQLDataSource::get('sae', true)) {
                $legal_status = new CLegalStatus();
                $legal_status = $legal_status->loadList();
            }

            $token = AntiCsrf::prepare()
                             ->addParam('type', $type)
                             ->addParam('_name')
                             ->addParam('code')
                             ->addParam('short_name')
                             ->addParam('description')
                             ->addParam('opening_reason')
                             ->addParam('opening_date')
                             ->addParam('opening_date_da')
                             ->addParam('closing_reason')
                             ->addParam('closing_date')
                             ->addParam('closing_date_da')
                             ->addParam('activation_date')
                             ->addParam('activation_date_da')
                             ->addParam('inactivation_date')
                             ->addParam('inactivation_date_da')
                             ->addParam('mediuser_view')
                             ->addParam('finess')
                             ->addParam('rmess')
                             ->addParam('address')
                             ->addParam('zip_code')
                             ->addParam('city')
                             ->addParam('country')
                             ->addParam('insee')
                             ->addParam('siren')
                             ->addParam('nic')
                             ->addParam('legal_status_code')
                             ->addParam('legal_entity_id', $legal_entity->_id);

            return $this->renderSmarty(
                'manage/inc_vw_legal_entity.tpl',
                [
                    'legal_entity' => $legal_entity,
                    'legal_status' => $legal_status,
                    'token'        => $token->getToken(),
                ]
            );
        }

        if ($type === 'group') {
            $group = new CGroups();

            // Retrieve selected group
            if ($group_id = $params->get("group_id", 'ref class|CGroups')) {
                $group->load($group_id);

                if ($group && $group->_id) {
                    $group->loadFunctions();
                    $group->loadRefsNotes();
                    $group->loadRefLegalEntity();
                    $group->loadRefLogo();

                    if (CModule::getActive('medimail')) {
                        $group->loadRefMedimailAccount();
                    }
                }
            }

            $token = AntiCsrf::prepare()
                             ->addParam('type', $type)
                             ->addParam('_name')
                             ->addParam('oid')
                             ->addParam('code')
                             ->addParam('description')
                             ->addParam('raison_sociale')
                             ->addParam('adresse')
                             ->addParam('cp')
                             ->addParam('ville')
                             ->addParam('tel')
                             ->addParam('fax')
                             ->addParam('tel_anesth')
                             ->addParam('mail')
                             ->addParam('mail_apicrypt')
                             ->addParam('web')
                             ->addParam('directeur')
                             ->addParam('domiciliation')
                             ->addParam('siret')
                             ->addParam('finess')
                             ->addParam('ape')
                             ->addParam('lat')
                             ->addParam('lon')
                             ->addParam('service_urgences_id')
                             ->addParam('pharmacie_id')
                             ->addParam('chambre_particuliere')
                             ->addParam('legal_entity_id')
                             ->addParam('opening_date')
                             ->addParam('opening_date_da')
                             ->addParam('opening_reason')
                             ->addParam('closing_date')
                             ->addParam('closing_date_da')
                             ->addParam('closing_reason')
                             ->addParam('activation_date')
                             ->addParam('activation_date_da')
                             ->addParam('inactivation_date')
                             ->addParam('inactivation_date_da')
                             ->addParam('group_id', $group->_id);

            return $this->renderSmarty(
                'manage/inc_vw_groups.tpl',
                [
                    'group'          => $group,
                    'token'          => $token->getToken(),
                    'legal_entities' => (new CLegalEntity())->loadList(),
                ]
            );
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function update(RequestParams $params): Response
    {
        if (($type = $params->post("type", 'str notNull')) === 'legal_entity') {
            $legal_entity = CLegalEntity::findOrNew($params->post('legal_entity_id', 'ref class|CLegalEntity'));
            $msg_action   = ($legal_entity->_id) ? 'modify' : 'create';

            $params = AntiCsrf::validateSfRequest($params->getRequest());

            $legal_entity->_name             = $params['_name'];
            $legal_entity->code              = $params['code'];
            $legal_entity->short_name        = $params['short_name'];
            $legal_entity->description       = $params['description'];
            $legal_entity->opening_reason    = $params['opening_reason'];
            $legal_entity->opening_date      = $params['opening_date'];
            $legal_entity->closing_reason    = $params['closing_reason'];
            $legal_entity->closing_date      = $params['closing_date'];
            $legal_entity->activation_date   = $params['activation_date'];
            $legal_entity->inactivation_date = $params['inactivation_date'];
            $legal_entity->user_id           = $params['mediuser_view'];
            $legal_entity->finess            = $params['finess'];
            $legal_entity->rmess             = $params['rmess'];
            $legal_entity->address           = $params['address'];
            $legal_entity->zip_code          = $params['zip_code'];
            $legal_entity->city              = $params['city'];
            $legal_entity->country           = $params['country'];
            $legal_entity->insee             = $params['insee'];
            $legal_entity->siren             = $params['siren'];
            $legal_entity->nic               = $params['nic'];
            $legal_entity->legal_status_code = $params['legal_status_code'];

            if ($msg = $legal_entity->store()) {
                $this->addUiMsgWarning($msg);
            } else {
                $this->addUiMsgOk("CLegalEntity-msg-{$msg_action}");
            }
        }

        if ($type === 'group') {
            $group      = CGroups::findOrNew($params->post('group_id', 'ref class|CGroups'));
            $msg_action = ($group->_id) ? 'modify' : 'create';

            $params = AntiCsrf::validateSfRequest($params->getRequest());

            $group->_name                = $params['_name'];
            $group->oid                  = $params['oid'];
            $group->code                 = $params['code'];
            $group->description          = $params['description'];
            $group->raison_sociale       = $params['raison_sociale'];
            $group->adresse              = $params['adresse'];
            $group->cp                   = $params['cp'];
            $group->ville                = $params['ville'];
            $group->tel                  = $params['tel'];
            $group->fax                  = $params['fax'];
            $group->tel_anesth           = $params['tel_anesth'];
            $group->mail                 = $params['mail'];
            $group->mail_apicrypt        = $params['mail_apicrypt'];
            $group->web                  = $params['web'];
            $group->directeur            = $params['directeur'];
            $group->domiciliation        = $params['domiciliation'];
            $group->siret                = $params['siret'];
            $group->finess               = $params['finess'];
            $group->ape                  = $params['ape'];
            $group->lat                  = $params['lat'];
            $group->lon                  = $params['lon'];
            $group->service_urgences_id  = $params['service_urgences_id'] ?? null;
            $group->pharmacie_id         = $params['pharmacie_id'] ?? null;
            $group->chambre_particuliere = $params['chambre_particuliere'];
            $group->legal_entity_id      = $params['legal_entity_id'];
            $group->opening_date         = $params['opening_date'];
            $group->opening_reason       = $params['opening_reason'];
            $group->closing_date         = $params['closing_date'];
            $group->closing_reason       = $params['closing_reason'];
            $group->activation_date      = $params['activation_date'];
            $group->inactivation_date    = $params['inactivation_date'];

            if ($msg = $group->store()) {
                $this->addUiMsgWarning($msg);
            } else {
                $this->addUiMsgOk("CGroups-msg-{$msg_action}");
            }
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function delete(RequestParams $params): Response
    {
        $type      = $params->post("type", 'str notNull');
        $entity_id = $params->post('entity_id', 'str notNull');

        if ($type === 'legal_entity') {
            $legal_entity = CLegalEntity::findOrFail($entity_id);
            if ($msg = $legal_entity->delete()) {
                $this->addUiMsgWarning($msg);
            } else {
                $this->addUiMsgOk("CLegalEntity-msg-delete");
            }
        }

        if ($type === 'group') {
            $group = CGroups::findOrFail($entity_id);
            if ($msg = $group->delete()) {
                $this->addUiMsgWarning($msg);
            } else {
                $this->addUiMsgOk("CGroups-msg-delete");
            }
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    public function structure(CGroups $etab): Response
    {
        // Services
        $service            = new CService();
        $service->group_id  = $etab->_id;
        $service->cancelled = 0;
        $service->externe   = 0;

        /** @var CService[] $services */
        $services = $service->loadMatchingList("nom");
        $chambres = CStoredObject::massLoadBackRefs($services, 'chambres');
        CStoredObject::massLoadBackRefs($chambres, 'lits');
        foreach ($services as $_service) {
            $_service->loadRefsChambres(false);
            foreach ($_service->_ref_chambres as $_chambre) {
                $_chambre->loadRefsLits();
            }
        }

        // Blocs opératoires
        $bloc           = new CBlocOperatoire();
        $bloc->group_id = $etab->_id;

        /** @var CBlocOperatoire[] $blocs */
        $blocs = $bloc->loadMatchingList("nom");
        CStoredObject::massLoadBackRefs($blocs, 'salles');
        foreach ($blocs as $_bloc) {
            $_bloc->loadRefsSalles();
        }

        return $this->renderSmarty(
            'manage/vw_structure.tpl',
            [
                'etab'     => $etab,
                'services' => $services,
                'blocs'    => $blocs,
            ]
        );
    }

    /**
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public function listGroupsAutocomplete(RequestParams $params): Response
    {
        $field       = $params->get('field', 'str');
        $view_field  = $params->get('view_field', "str default|{$field}");
        $input_field = $params->get('input_field', "str default|{$view_field}");
        $keywords    = $params->get($input_field, 'str');

        $this->enforceSlave();

        $groups = CGroups::loadGroups($params->get('edit', 'bool default|0') ? PERM_EDIT : PERM_READ);

        if ($keywords) {
            foreach ($groups as $_group) {
                if (!preg_match("/^$keywords/i", $_group->text)) {
                    unset($groups[$_group->_id]);
                }
            }
        }

        return $this->renderSmarty(
            'inc_field_autocomplete.tpl',
            [
                'matches'    => $groups,
                'field'      => $field,
                'view_field' => $view_field,
                'show_view'  => $params->get('show_view', 'str default|false') == "true",
                'template'   => (new CGroups())->getTypedTemplate("autocomplete"),
                'input'      => '',
                'nodebug'    => true,
            ],
            'system'
        );
    }
}
