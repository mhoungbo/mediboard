<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Etablissement;

use Ox\Core\Module\AbstractTabsRegister;

/**
 * @codeCoverageIgnore
 */
class CTabsEtablissement extends AbstractTabsRegister
{
    public function registerAll(): void
    {
        $this->registerRoute('etablissement_manage_index', TAB_READ);
        $this->registerRoute('etablissement_externals_index', TAB_READ);
        $this->registerRoute('etablissement_configure', TAB_ADMIN, self::TAB_CONFIGURE);
    }
}
