/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class Group extends OxObject {
    constructor () {
        super()
        this.type = "group"
    }

    get name (): string {
        return super.get("text")
    }

    set name (value: string) {
        super.set("text", value)
    }

    get isMain (): boolean {
        return super.get("is_main")
    }

    set isMain (value: boolean) {
        super.set("is_main", value)
    }

    get isSecondary (): boolean {
        return super.get("is_secondary")
    }

    set isSecondary (value: boolean) {
        super.set("is_secondary", value)
    }

    get groupsUrl (): OxAttr<string> {
        return this.links.groups
    }

    set groupsUrl (value: OxAttr<string>) {
        this.links.groups = value
    }
}
