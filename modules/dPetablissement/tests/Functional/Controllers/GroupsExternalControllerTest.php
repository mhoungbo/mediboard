<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Etablissement\Tests\Functional\Controllers;

use Exception;
use Ox\Mediboard\Etablissement\CEtabExterne;
use Ox\Tests\OxWebTestCase;

class GroupsExternalControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testViewIndex(): CEtabExterne
    {
        $etab = $this->createEtabExterne();

        $crawler = self::createClient()->request('GET', '/gui/groups/externals/list');

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(
            'Lorem Ipsum',
            $crawler->filterXPath('//table[@class="tbl"][1]//tbody//tr[1]//td[1]')->innerText()
        );

        return $etab;
    }

    /**
     * @depends testViewIndex
     * @throws Exception
     */
    public function testEditExternal(CEtabExterne $etab): void
    {
        $crawler = self::createClient()->request(
            'GET',
            '/gui/groups/externals/external',
            ['edit' => '1', 'etab_id' => $etab->_id]
        );

        $this->deleteOrFailed($etab);
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(
            'Lorem Ipsum',
            $crawler->filterXPath('//input[@name="nom"]')->attr('value')
        );
    }

    private function createEtabExterne(): CEtabExterne
    {
        $etab         = new CEtabExterne();
        $etab->nom    = 'Lorem Ipsum';
        $etab->cp     = '17000';
        $etab->ville  = 'La Rochelle';
        $etab->finess = '111111111';
        $this->storeOrFailed($etab);

        return $etab;
    }
}
