/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
export default class ExObject extends OxObject {
    readonly CLASSNAME = "CExObject"
    constructor () {
        super()
        this.type = "ex_object"
    }

    get name (): OxAttr<string> {
        return super.get("_name")
    }

    set name (name: OxAttr<string>) {
        super.set("_name", name)
    }

    get exClassId (): OxAttr<string> {
        return super.get("_ex_class_id")
    }

    get objectClass (): OxAttr<string> {
        return super.get("type")
    }

    get objectId (): OxAttr<number> {
        return super.get("object_id")
    }

    get objectGuid (): string {
        return this.objectClass + "-" + this.objectId
    }

    get creationDate (): OxAttr<string> {
        return super.get("datetime_create")
    }

    get guid (): string {
        return "CExObject_" + this.exClassId + "-" + this.id
    }
}
