<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tag;

use Ox\Core\Autoload\IShortNameAutoloadable;
use Ox\Core\CAppUI;
use Ox\Mediboard\System\Forms\CExClassField;

abstract class AbstractCExClassFieldTag implements IShortNameAutoloadable
{
    /** @var string */
    private $tag;

    final public function __construct(string $tag)
    {
        $this->tag = $tag;
    }

    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * Todo: Not testable
     */
    public function getName(): string
    {
        return CAppUI::tr("AbstractCExClassFieldTag.name.{$this->tag}");
    }

    abstract public function validate(CExClassField $ex_class_field): bool;
}
