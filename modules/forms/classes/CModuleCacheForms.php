<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms;

use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\CacheManager;
use Ox\Core\Module\AbstractModuleCache;
use Ox\Mediboard\System\Forms\CExObject;
use Psr\SimpleCache\InvalidArgumentException;

class CModuleCacheForms extends AbstractModuleCache
{
    protected array $dshm_patterns = [
        CExObject::CACHE_KEY,
    ];

    public function getModuleName(): string
    {
        return 'forms';
    }

    /**
     * @inheritdoc
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function clearSpecialActions(): void
    {
        CExObject::clearLocales(true);

        CacheManager::output("module-forms-msg-cache-ex_class-suppr", UI_MSG_OK);
    }
}
