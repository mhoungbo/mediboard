<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Controllers;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Cache;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Forms\CExClassRefChecker;
use Ox\Mediboard\System\Forms\CExClass;
use Ox\Mediboard\System\Forms\CExObject;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class ReferencesController extends Controller
{
    /**
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public function index(): Response
    {
        $ex_classes = (new CExClass())->loadList();

        $ex_class_check = (new CExClassRefChecker())->getKeys(CMbArray::pluck($ex_classes, 'ex_class_id'));

        CExObject::checkLocales();

        return $this->renderSmarty(
            'vw_ref_checker.tpl',
            [
                'ex_classes'     => $ex_classes,
                'ex_class_check' => $ex_class_check,
                'prefix'         => CExClassRefChecker::PREFIX,
                'pre_tbl'        => CExClassRefChecker::PRE_TBL,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function check(RequestParams $params): Response
    {
        $ex_class_id = $params->post('ex_class_id', 'ref class|CExClass notNull');
        $start       = $params->post('start', 'num default|0');
        $step        = $params->post('step', 'num default|100');

        if (!$this->isCsrfTokenValid('forms_gui_references_errors_check', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        (new CExClassRefChecker())->check($ex_class_id, $start, $step);

        return $this->renderEmptyResponse();
    }

    /**
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public function vwErrors(RequestParams $params): Response
    {
        $ex_class_id = $params->get('ex_class_id', 'ref class|CExClass notNull');
        $del         = $params->get('del', 'bool default|0');

        if (!$ex_class_id) {
            $this->addUiMsgError('common-error-An error occurred', true);

            return $this->renderEmptyResponse();
        }

        $cache = new Cache(CExClassRefChecker::PREFIX, CExClassRefChecker::PRE_TBL . $ex_class_id, Cache::DISTR);
        if (!$cache->exists()) {
            $this->addUiMsgError("Vérification des références non commencée", true);

            return $this->renderEmptyResponse();
        }

        if ($del) {
            $cache->rem();
            $this->addUiMsgOk("Réinitialisation de la vérification", true);

            return $this->renderEmptyResponse();
        }

        $data = $cache->get();
        if (!is_countable($data['errors']) || !count($data['errors'])) {
            $this->addUiMsgError("Aucune erreur pour le formulaire", true);

            return $this->renderEmptyResponse();
        }

        return $this->renderSmarty(
            'vw_ref_errors.tpl',
            [
                'data'        => $data,
                'ex_class_id' => $ex_class_id,
            ]
        );
    }

    /**
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function repair(RequestParams $params): Response
    {
        $ex_class_id = $params->post('ex_class_id', 'ref class|CExClass notNull');
        $start       = $params->post('start', 'num default|0');
        $step        = $params->post('step', 'num default|100');
        $continue    = $params->post('continue', 'bool default|0');

        if (!$this->isCsrfTokenValid('forms_gui_references_errors_repair', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if (!$ex_class_id) {
            $this->addUiMsgError('common-error-An error occurred', true);

            return $this->renderEmptyResponse();
        }

        $cache = new Cache(CExClassRefChecker::PREFIX, CExClassRefChecker::PRE_TBL . $ex_class_id, Cache::DISTR);
        if (!$cache->exists()) {
            $this->addUiMsgError("Vérification des références non commencée", true);

            return $this->renderEmptyResponse();
        }

        $data = $cache->get();

        $start = (count($data['errors']) < $start) ? count($data['errors']) : $start;
        $ids   = array_slice($data['errors'], $start, $step);

        $ex_objects = (new CExObject($ex_class_id))->loadAll($ids);

        $failed_repair = 0;
        $ids_corrected = [];
        foreach ($ex_objects as $_ex_object) {
            $repaired = false;
            foreach (CExClassRefChecker::FIELDS as $_field_class => $_field_id) {
                $object_guid = $_ex_object->{$_field_class} . '-' . $_ex_object->{$_field_id};
                if (CExObject::repairReferences($ex_class_id, $_ex_object->_id, $object_guid)) {
                    $repaired = true;
                }
            }

            if ($repaired) {
                $ids_corrected[] = $_ex_object->_id;
                CAppUI::setMsg("CExObject-msg-repaired", UI_MSG_OK);
            } else {
                $failed_repair++;
                CAppUI::setMsg("CExObject-msg-Repair failed", UI_MSG_WARNING);
            }
        }

        $data['errors'] = array_diff($data['errors'], $ids_corrected);

        $cache->put($data);

        $start     = $start + $failed_repair;
        $new_count = (count($data['errors']) - count($ids)) + $failed_repair;

        CAppUI::js("nextCorrection('{$start}', '{$continue}', '{$new_count}')");

        return $this->renderEmptyResponse();
    }

    /**
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public function cronCheckRefs(RequestParams $params): Response
    {
        $default_step = $params->get('step', 'num default|100');

        ['ex_class_id' => $ex_class_id, 'start' => $start, 'step' => $step] = (new CExClassRefChecker(
        ))->getNextExClassIdToCheck();

        (new CExClassRefChecker())->check($ex_class_id, $start, $step ?: $default_step);


        return $this->renderEmptyResponse();
    }
}
