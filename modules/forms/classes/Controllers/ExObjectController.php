<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Exception\ControllerException;
use Ox\Mediboard\System\Forms\CExClass;
use Ox\Mediboard\System\Forms\CExClassField;
use Ox\Mediboard\System\Forms\CExLink;
use Ox\Mediboard\System\Forms\CExObject;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class ExObjectController extends Controller
{
    /**
     * @throws InvalidArgumentException|ApiException
     * @throws Exception
     * @api
     */
    public function list(RequestApi $request, string $resource_type, string $resource_id): Response
    {
        $context = $this->getObjectFromResourceTypeAndId($resource_type, $resource_id);
        // DO NOT check the permission on context (permissions are handled into CExObject::getPerm).

        $ex_link = new CExLink();
        $ds      = $ex_link->getDS();

        $where = [
            'object_class' => $ds->prepare('= ?', $context->_class),
            'object_id'    => $ds->prepare('= ?', $context->_id),
        ];

        $ex_links = $ex_link->loadList($where);

        CExLink::massLoadExObjects($ex_links);

        /** @var CExObject[] $ex_objects */
        $ex_objects = array_column($ex_links, '_ref_ex_object');
        $ex_objects = array_filter($ex_objects, fn($ex_object) => $ex_object->getPerm(PERM_READ));

        $ex_class_ids = array_unique(array_column($ex_objects, '_ex_class_id'));

        if ($request->hasRelation('owner')) {
            // Massload before rendering response
            CExObject::massLoadFwdRef($ex_objects, 'owner_id');
        }

        if ($request->hasRelation('object')) {
            CExObject::massLoadFwdRef($ex_objects, 'object_id');
        }

        if ($request->hasRelation('reference1')) {
            CExObject::massLoadFwdRef($ex_objects, 'reference_id');
        }

        if ($request->hasRelation('reference2')) {
            CExObject::massLoadFwdRef($ex_objects, 'reference2_id');
        }

        $collection = Collection::createFromRequest($request, $ex_objects);

        if (count($ex_class_ids) > 0) {
            $ex_class_field = new CExClassField();

            $ljoin = [
                'ex_class_field_group' => 'ex_class_field_group.ex_class_field_group_id = ex_class_field.ex_group_id',
                'ex_class'             => 'ex_class_field_group.ex_class_id ' . $ds::prepareIn($ex_class_ids),
            ];

            $where = [
                'ex_class_field.result_in_title' => "= '1'",
                'ex_class_field.disabled'        => "= '0'",
            ];

            /** @var CExClassField[] $formulae_fields */
            $formulae_fields = $ex_class_field->loadList(
                $where,
                null,
                null,
                'ex_class_field.ex_class_field_id',
                $ljoin
            );

            if (count($formulae_fields) > 0) {
                $fields_by_ex_class = [];

                foreach ($formulae_fields as $ex_field) {
                    $fields_by_ex_class[$ex_field->_ex_class_id] = $ex_field->name;
                }

                /** @var Item $item */
                foreach ($collection as $item) {
                    /** @var CExObject $ex_object */
                    $ex_object = $item->getDatas();

                    $item->addAdditionalDatas(['_name' => $ex_object->_ref_ex_class->name]);

                    $formulae = $fields_by_ex_class[$ex_object->_ex_class_id] ?? null;

                    if ($formulae !== null) {
                        $item->addAdditionalDatas(['score' => $ex_object->{$formulae}]);
                    }
                }
            }
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws Exception
     * @api
     */
    public function delete(CExClass $ex_class, string $resource_id): Response
    {
        $ex_object = new CExObject($ex_class->_id);
        $ex_object->load($resource_id);

        if (!$ex_object->_id) {
            throw new ControllerException(
                Response::HTTP_NOT_FOUND,
                $this->translator->tr('common-error-Object not found')
            );
        }

        $can_delete = ($ex_object->owner_id == $this->getCUser()->_id) || $this->checkModulePermAdmin();

        if (!$can_delete) {
            throw new AccessDeniedException();
        }

        $this->deleteObject($ex_object);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }
}
