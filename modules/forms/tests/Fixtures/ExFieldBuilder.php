<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tests\Fixtures;

use Ox\Mediboard\System\Forms\CExClassField;
use Ox\Mediboard\System\Forms\CExClassFieldGroup;
use Ox\Mediboard\System\Forms\CExClassFieldTranslation;
use Ox\Mediboard\System\Forms\CExConcept;
use Ox\Tests\Fixtures\FixturesException;

class ExFieldBuilder
{
    private CExClassField            $ex_field;
    private CExClassFieldTranslation $ex_translation;
    private ?ExConceptBuilder        $concept = null;

    public function __construct()
    {
        $this->ex_field       = new CExClassField();
        $this->ex_translation = new CExClassFieldTranslation();

        $this->withName(CExClassField::getUniqueName());
    }

    public function withName(string $name): self
    {
        $this->ex_field->_make_unique_name = false;

        $this->ex_field->name = $name;

        // Building a field translation
        $this->ex_translation->lang  = 'fr';
        $this->ex_translation->std   = $name;
        $this->ex_translation->court = $name;
        $this->ex_translation->desc  = $name;

        return $this;
    }

    public function withConcept(ExConceptBuilder $concept): self
    {
        $this->concept = $concept;

        return $this;
    }

    public function withProp(string $prop): self
    {
        $this->ex_field->prop = $prop;

        return $this;
    }

    public function withFormula(string $formula): self
    {
        $this->ex_field->formula = $formula;

        return $this;
    }

    public function withResultInTitle(): self
    {
        $this->ex_field->result_in_title = '1';

        return $this;
    }

    public function setExGroup(CExClassFieldGroup $field_group): void
    {
        $this->ex_field->ex_group_id = $field_group->_id;
    }

    public function setExConcept(CExConcept $ex_concept): void
    {
        $this->ex_field->concept_id = $ex_concept->_id;
    }

    public function build(): CExClassField
    {
        if ($this->concept !== null) {
            $ex_concept = $this->concept->build();
            $this->withProp($ex_concept->prop);
            $this->setExConcept($ex_concept);
        }

        if ($msg = $this->ex_field->store()) {
            throw new FixturesException($msg);
        }

        $this->ex_translation->ex_class_field_id = $this->ex_field->_id;
        if ($msg = $this->ex_translation->store()) {
            throw new FixturesException($msg);
        }

        return $this->ex_field;
    }
}
