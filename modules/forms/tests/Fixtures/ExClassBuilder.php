<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tests\Fixtures;

use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\Forms\CExClass;
use Ox\Tests\Fixtures\FixturesException;

class ExClassBuilder
{
    private CExClass $ex_class;

    /** @var ExFieldGroupBuilder[] */
    private array $field_groups = [];

    public function __construct()
    {
        $this->ex_class = new CExClass();
        $this->withName(uniqid());
        $this->ex_class->permissions                = '{}';
        $this->ex_class->_dont_create_default_group = true;
    }

    public function withName(string $name): self
    {
        $this->ex_class->name = $name;

        return $this;
    }

    public function withGroup(CGroups $group): self
    {
        $this->ex_class->group_id = $group->_id;

        return $this;
    }

    public function withOtherPermission(bool $edit, bool $view, bool $deny): self
    {
        return $this->withPermType(-10, false, $edit, $view, $deny);
    }

    public function withDefaultPermission(bool $create, bool $edit, bool $view, bool $deny): self
    {
        return $this->withPermType(10_000, $create, $edit, $view, $deny);
    }

    public function withPermission(string $type, bool $create, bool $edit, bool $view, bool $deny): self
    {
        $perm_type = $this->mapType($type);

        return $this->withPermType($perm_type, $create, $edit, $view, $deny);
    }

    private function withPermType(int $perm_type, bool $create, bool $edit, bool $view, bool $deny): self
    {
        $permissions = ($this->ex_class->permissions) ? json_decode($this->ex_class->permissions, true) : [];

        $permissions = $permissions + [
                $perm_type => [
                    'c' => $create,
                    'e' => $edit,
                    'v' => $view,
                    'd' => $deny,
                ],
            ];

        $this->ex_class->permissions = json_encode($permissions);

        return $this;
    }

    private function mapType(string $type): int
    {
        static $types = null;

        if ($types === null) {
            $types = $this->ex_class::getAllPermTypes();
            $types = array_flip($types);
        }

        $perm_type = ($types[$type]) ?? null;

        if ($perm_type === null) {
            throw new FixturesException(sprintf('Unable to find type: %s', $type));
        }

        return $perm_type;
    }

    public function withFieldGroup(ExFieldGroupBuilder $field_group): self
    {
        $this->field_groups[] = $field_group;

        return $this;
    }

    public function build(): CExClass
    {
        if ($msg = $this->ex_class->store()) {
            throw new FixturesException($msg);
        }

        foreach ($this->field_groups as $group) {
            $group->setExClass($this->ex_class);
            $group->build();
        }

        return $this->ex_class;
    }
}
