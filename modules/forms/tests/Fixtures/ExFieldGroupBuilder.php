<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tests\Fixtures;

use Ox\Mediboard\System\Forms\CExClass;
use Ox\Mediboard\System\Forms\CExClassFieldGroup;
use Ox\Tests\Fixtures\FixturesException;

class ExFieldGroupBuilder
{
    private CExClassFieldGroup $ex_group;

    /** @var ExFieldBuilder[] */
    private array $fields = [];

    public function __construct()
    {
        $this->ex_group = new CExClassFieldGroup();
        $this->withName(uniqid());
    }

    public function withName(string $name): self
    {
        $this->ex_group->name = $name;

        return $this;
    }

    public function withField(ExFieldBuilder $field): self
    {
        $this->fields[] = $field;

        return $this;
    }

    public function setExClass(CExClass $ex_class): void
    {
        $this->ex_group->ex_class_id = $ex_class->_id;
    }

    public function build(): CExClassFieldGroup
    {
        if ($msg = $this->ex_group->store()) {
            throw new FixturesException($msg);
        }

        foreach ($this->fields as $field) {
            $field->setExGroup($this->ex_group);
            $field->build();
        }

        return $this->ex_group;
    }
}
