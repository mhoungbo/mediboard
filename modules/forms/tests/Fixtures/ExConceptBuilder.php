<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tests\Fixtures;

use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\Forms\CExConcept;
use Ox\Tests\Fixtures\FixturesException;

class ExConceptBuilder
{
    public const NAME_PREFIX = 'fixture-';

    private CExConcept $ex_concept;

    public function __construct()
    {
        $this->ex_concept = new CExConcept();
        $this->withName(uniqid());
    }

    public function withName(string $name): self
    {
        $this->ex_concept->name = static::NAME_PREFIX . $name;

        return $this;
    }

    public function withProp(string $prop): self
    {
        $this->ex_concept->prop = $prop;

        return $this;
    }

    public function withGroup(CGroups $group): self
    {
        $this->ex_concept->group_id = $group->_id;

        return $this;
    }

    public function build(): CExConcept
    {
        if ($msg = $this->ex_concept->store()) {
            throw new FixturesException($msg);
        }

        return $this->ex_concept;
    }
}
