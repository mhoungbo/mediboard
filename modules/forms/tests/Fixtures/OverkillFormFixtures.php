<?php

/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\Forms\Tests\Fixtures;

use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * @description The purpose of this fixture is to create a massive form with a sql row_size closest to the innodb limit
 */
class OverkillFormFixtures extends AbstractFormsFixtures implements GroupFixturesInterface
{
    public const REF_EX_CLASS_OVERKILL = "ref_ex_class_overkill";
    public const REF_EX_CLASS_FIELD_EMAIL  = "ref_ex_class_field_email";

    /** @var int This value is set so that adding one more field would reach the inno_db limit */
    private const FIELDS_COUNT = 28;

    /**
     * @inheritDoc
     */
    public function load(): void
    {
        /** Overkill form generation **/
        $this->createOverkillForm();
    }

    private function createOverkillForm(): void
    {
        $ex_class = $this->generateExClass(self::REF_EX_CLASS_OVERKILL);
        for ($i = 1; $i <= self::FIELDS_COUNT; $i++) {
            $ex_class_field_str = $this->generateExClassField(
                $this->getFirstClassGroup($ex_class),
                $this->generateExConcept("str maxLength|255"),
                self::REF_EX_CLASS_FIELD_EMAIL . '-' . $i
            );
            $this->generateExClassFieldTranslation($ex_class_field_str, "Champ text long varchar 255");
        }
    }

    /**
     * @inheritDoc
     */
    public function purge()
    {
        $this->purgeEx();
        parent::purge();
    }

    public static function getGroup(): array
    {
        return ['forms_fixtures', 210];
    }
}
