<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tests\Fixtures;

use Ox\Core\CModelObjectException;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\System\Forms\CExClassField;
use Ox\Mediboard\System\Forms\CExObject;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

class PermissionFormFixtures extends AbstractFormsFixtures implements GroupFixturesInterface
{
    public const MB_MEDECIN         = 'form-user-mb-medecin';
    public const NO_PERM_MB_MEDECIN = 'form-user-noperm-medecin';
    public const OTHER_MB_MEDECIN   = 'form-user-other-mb-medecin';
    public const OTHER_TAMM_MEDECIN = 'form-user-other-tamm-medecin';

    public const CS         = 'form-consult';
    public const NO_PERM_CS = 'form-noperm-consult';

    /**
     * @inheritDoc
     */
    public function load(): void
    {
        $this->loadStandard();
        $this->loadNoFormsModulePermission();
        $this->loadMedecin(static::OTHER_TAMM_MEDECIN, true);
    }

    private function loadStandard(): void
    {
        [$group, $medecin, $patient, $consultation] = $this->loadConsultation(
            static::MB_MEDECIN,
            static::CS,
            true
        );

        $this->loadMedecinFrom($medecin, static::OTHER_MB_MEDECIN, true);

        $formula_field = CExClassField::getUniqueName();
        $score_field   = CExClassField::getUniqueName();

        $ex_class = (new ExClassBuilder())
            ->withGroup($group)
            ->withOtherPermission(false, false, true)
            ->withFieldGroup(
                (new ExFieldGroupBuilder())
                    ->withField(
                        (new ExFieldBuilder())
                            ->withName($formula_field)
                            ->withConcept(
                                (new ExConceptBuilder())
                                    ->withGroup($group)
                                    ->withProp('num')
                            )
                    )
                    ->withField(
                        (new ExFieldBuilder())
                            ->withName($score_field)
                            ->withConcept(
                                (new ExConceptBuilder())
                                    ->withGroup($group)
                                    ->withProp('float')
                            )
                            ->withFormula("[{$formula_field}] + 1.1")
                            ->withResultInTitle()
                    )
            )
            ->build();

        $this->store($ex_class);

        $ex_object                   = new CExObject($ex_class->_id);
        $ex_object->owner_id         = $medecin->_id;
        $ex_object->{$formula_field} = 123;
        $ex_object->{$score_field}   = 123 + 1.1;

        $ex_object->setObject($consultation);
        $ex_object->reference_class = 'CSejour';
        $ex_object->setReferenceObject_2($patient);
        $ex_object->group_id = $group->_id;
        $this->store($ex_object);

        $ex_object                   = new CExObject($ex_class->_id);
        $ex_object->owner_id         = $medecin->_id;
        $ex_object->{$formula_field} = 456;

        $ex_object->setObject($consultation);
        $ex_object->reference_class = 'CSejour';
        $ex_object->setReferenceObject_2($patient);
        $ex_object->group_id = $group->_id;
        $this->store($ex_object);
    }

    private function loadNoFormsModulePermission(): void
    {
        $this->loadConsultation(static::NO_PERM_MB_MEDECIN, static::NO_PERM_CS, false);
    }

    /**
     * @param string $user_tag
     * @param string $cs_tag
     * @param bool   $perm
     *
     * @return array{CGroups, CMediusers, CPatient, CConsultation}
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function loadConsultation(string $user_tag, string $cs_tag, bool $perm): array
    {
        [$group, $function, $medecin] = $this->loadMedecin($user_tag, $perm);

        /** @var CPatient $patient */
        $patient              = CPatient::getSampleObject();
        $patient->group_id    = $group->_id;
        $patient->function_id = $function->_id;
        $this->store($patient);

        /** @var CPlageconsult $plage */
        $plage          = CPlageconsult::getSampleObject();
        $plage->chir_id = $medecin->_id;
        $this->store($plage);

        /** @var CConsultation $consultation */
        $consultation                  = CConsultation::getSampleObject();
        $consultation->plageconsult_id = $plage->_id;
        $consultation->patient_id      = $patient->_id;
        $this->store($consultation, $cs_tag);

        return [
            $group,
            $medecin,
            $patient,
            $consultation,
        ];
    }

    /**
     * @param string $tag
     * @param bool   $perm
     *
     * @return array{CGroups, CFunctions, CMediusers}
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function loadMedecin(string $tag, bool $perm): array
    {
        /** @var CGroups $group */
        $group = CGroups::getSampleObject();
        $this->store($group);

        /** @var CFunctions $function */
        $function           = CFunctions::getSampleObject();
        $function->group_id = $group->_id;
        $this->store($function);

        /** @var CMediusers $medecin */
        $medecin              = $this->getUser(false);
        $medecin->function_id = $function->_id;
        $medecin->_user_type  = CUser::$types['M�decin'] ?? 13;
        $this->store($medecin, $tag);

        if ($perm) {
            $perm_module             = new CPermModule();
            $perm_module->user_id    = $medecin->_id;
            $perm_module->mod_id     = CModule::getActive('forms')->mod_id;
            $perm_module->permission = '1'; # Read
            $perm_module->view       = '0'; # Hidden
            $this->store($perm_module);
        }

        return [
            $group,
            $function,
            $medecin,
        ];
    }

    /**
     * @param CMediusers $medecin
     * @param string     $tag
     * @param bool       $perm
     *
     * @return CMediusers
     * @throws FixturesException
     */
    private function loadMedecinFrom(CMediusers $medecin, string $tag, bool $perm): CMediusers
    {
        /** @var CMediusers $other */
        $other              = $this->getUser(false);
        $other->function_id = $medecin->function_id;
        $other->_user_type  = CUser::$types['M�decin'] ?? 13;
        $this->store($other, $tag);

        if ($perm) {
            $perm_module             = new CPermModule();
            $perm_module->user_id    = $other->_id;
            $perm_module->mod_id     = CModule::getActive('forms')->mod_id;
            $perm_module->permission = '1'; # Read
            $perm_module->view       = '0'; # Hidden
            $this->store($perm_module);
        }

        return $other;
    }

    /**
     * @inheritDoc
     */
    public function purge()
    {
        $this->purgeEx();
        parent::purge();
    }

    public static function getGroup(): array
    {
        return ['forms_fixtures', 200];
    }
}
