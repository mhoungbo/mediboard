<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tests\Unit;

use Exception;
use Ox\Mediboard\Forms\Tests\Fixtures\OverkillFormFixtures;
use Ox\Mediboard\System\Forms\CExClass;
use Ox\Mediboard\System\Forms\CExClassField;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

class ExClassFieldTest extends OxUnitTestCase
{
    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testCheckRowSizeLock(): void
    {
        /** @var CExClass $ex_class */
        $ex_class = $this->getObjectFromFixturesReference(
            CExClass::class,
            OverkillFormFixtures::REF_EX_CLASS_OVERKILL
        );

        $fields = $ex_class->loadRefsAllFields();

        /** @var CExClassField $field */
        $field = reset($fields);

        $field->prop .= ' notNull';

        $this->assertEquals(
            'CExClassFieldCMbObject-msg-check-failedCExClassField-error-The property of the field is locked',
            $field->store()
        );
    }
}
