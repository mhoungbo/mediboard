<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tests\Unit;

use Exception;
use Ox\Mediboard\Forms\Tests\Fixtures\OverkillFormFixtures;
use Ox\Mediboard\System\Forms\CExClass;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

class ExClassTest extends OxUnitTestCase
{
    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testDuplicateLock(): void
    {
        /** @var CExClass $ex_class */
        $ex_class = $this->getObjectFromFixturesReference(
            CExClass::class,
            OverkillFormFixtures::REF_EX_CLASS_OVERKILL
        );

        $this->assertEquals(
            'CExClass-error-The form cannot be duplicated',
            $ex_class->duplicate()
        );
    }
}
