<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Forms\Tests\Functional\Controllers;

use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Forms\Tests\Fixtures\PermissionFormFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\Forms\CExObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class ExObjectControllerTest extends OxWebTestCase
{
    /**
     * @config dPpatients CPatient function_distinct 0
     *
     * @return void
     * @throws TestsException
     */
    public function testListingForm(): void
    {
        /** @var CMediusers $user */
        $user   = $this->getObjectFromFixturesReference(CMediusers::class, PermissionFormFixtures::MB_MEDECIN);
        $client = static::createClient([], [], $user->_ref_user);

        $cs = $this->getObjectFromFixturesReference(CConsultation::class, PermissionFormFixtures::CS);

        $resource_type = CConsultation::RESOURCE_TYPE;
        $client->request(
            'GET',
            "/api/forms/exObjects/{$resource_type}/{$cs->_id}",
        );

        $this->assertResponseIsSuccessful();

        $collection = $this->getJsonApiCollection($client);
        $this->assertCount(2, $collection);

        $this->assertEquals(CExObject::RESOURCE_TYPE, $collection->getFirstItem()->getType());
        $this->assertFalse($collection->hasLink('self'));
        $this->assertFalse($collection->hasLink('history'));
        $this->assertFalse($collection->hasLink('schema'));

        /** @var Item $item */
        foreach ($collection as $item) {
            if ($item->getId() === '1') {
                // First item has a "result in title" field of 123+1.1
                $this->assertEquals(124.1, $item->getAttributes()['score']);
            } else {
                // Second item does not have a "result in title" field
                $this->assertNull($item->getAttributes()['score']);
            }
        }
    }

    /**
     * @config dPpatients CPatient function_distinct 0
     *
     * @return void
     * @throws TestsException
     */
    public function testListingFormWithoutFormsModulePermissionFails(): void
    {
        /** @var CMediusers $user */
        $user   = $this->getObjectFromFixturesReference(CMediusers::class, PermissionFormFixtures::NO_PERM_MB_MEDECIN);
        $client = static::createClient([], [], $user->_ref_user);

        $cs = $this->getObjectFromFixturesReference(CConsultation::class, PermissionFormFixtures::NO_PERM_CS);

        $resource_type = CConsultation::RESOURCE_TYPE;
        $client->request(
            'GET',
            "/api/forms/exObjects/{$resource_type}/{$cs->_id}",
        );

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * @depends testListingForm
     *
     * @config  dPpatients CPatient function_distinct 2
     *
     * @return void
     */
    public function testHermeticMode(): void
    {
        /** @var CMediusers $user */
        $user   = $this->getObjectFromFixturesReference(CMediusers::class, PermissionFormFixtures::OTHER_TAMM_MEDECIN);
        $client = static::createClient([], [], $user->_ref_user);

        // Loading a consultation from another groups
        $cs = $this->getObjectFromFixturesReference(CConsultation::class, PermissionFormFixtures::CS);

        $resource_type = CConsultation::RESOURCE_TYPE;
        $client->request(
            'GET',
            "/api/forms/exObjects/{$resource_type}/{$cs->_id}",
        );

        $this->assertResponseIsSuccessful();

        $collection = $this->getJsonApiCollection($client);
        $this->assertCount(0, $collection);
    }

    /**
     * @depends testListingForm
     *
     * @config  dPpatients CPatient function_distinct 0
     *
     * @return void
     */
    public function testCustomFormPermissionDeny(): void
    {
        /** @var CMediusers $user */
        $user   = $this->getObjectFromFixturesReference(CMediusers::class, PermissionFormFixtures::OTHER_MB_MEDECIN);
        $client = static::createClient([], [], $user->_ref_user);

        $cs = $this->getObjectFromFixturesReference(CConsultation::class, PermissionFormFixtures::CS);

        $resource_type = CConsultation::RESOURCE_TYPE;
        $client->request(
            'GET',
            "/api/forms/exObjects/{$resource_type}/{$cs->_id}",
        );

        $this->assertResponseIsSuccessful();

        $collection = $this->getJsonApiCollection($client);
        $this->assertCount(0, $collection);
    }
}
