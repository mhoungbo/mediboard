{{*
 * @package Mediboard\Admissions
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}
{{mb_script module=board script=board}}
{{mb_script module="patients"     script="patient"}}
{{mb_script module="patients"     script="pat_selector"}}

<script>
    Main.add(function () {
        const form = getForm("selectPraticien");
        Calendar.regField(form._date_min);
        Board.reloadDocuments({{$praticiens|@array_keys|@json_encode}}, form);
        Board.selectPraticien(form.praticien_id_view, form);
        Board.selectFunction(form._date_min);

    });
    emptyFunction = function () {
        const form = getForm("selectPraticien");
        $V(form.function_id, '', false);
        $V(form._function_view, '');
        Board.reloadTdbSecretaire(null, form);
    };
    emptyContext = function () {
      const form = getForm("selectPraticien");
      $V(form.context, '');
      Board.reloadDocuments({{$praticiens|@array_keys|@json_encode}}, form);
    };
</script>
<div id="reload_tdb">
    <form name="selectPraticien" id="selectPraticien" method="get">
        <input type="hidden" name="praticiens" value="{{$praticiens|@array_keys|@json_encode}}"/>
        <table class="main form" id="">
            <tr>
                <td>
                    <input type="text" name="praticien_id_view" class="autocomplete" style="width:15em;"
                           placeholder="&mdash; Choisir un praticien"
                           value=""/>
                </td>
                <td>
                    <input type="hidden" name="function_id" value="{{$function->_id}}">
                    <input type="text" class="autocomplete" name="_function_view"
                           placeholder="{{tr}}CFunctions-select{{/tr}}"
                           value="{{if $function->_id}}{{$function}}{{/if}}">
                    <button type="button" class="cancel notext"
                            onclick="emptyFunction();">{{tr}}Empty{{/tr}}</button>
                </td>
                <td>
                    <input type="hidden" name="patient_id" value=""/>
                    <input type="text" placeholder="Choisir un patient" readonly="readonly" name="patient" value="{{$patient->_view}}"
                           onclick="PatSelector.init();" onchange="Board.reloadDocuments({{$praticiens|@array_keys|@json_encode}}, this.form);" />
                    <button class="erase notext compact me-secondary" type="button"
                            onclick="Board.initPatient({{$praticiens|@array_keys|@json_encode}}, this.form)">{{tr}}Empty{{/tr}}</button>
                    <button class="search notext compact me-secondary" type="button" onclick="PatSelector.init()">{{tr}}Search{{/tr}}</button>
                    <script>
                        PatSelector.init = function(){
                            this.sForm = "selectPraticien";
                            this.sId   = "patient_id";
                            this.sView = "patient";
                            this.pop();
                        }
                    </script>
                </td>
                <td>
                  <select name="context" onchange="Board.reloadDocuments({{$praticiens|@array_keys|@json_encode}},this.form)">
                    <option class="empty" value="">&mdash; {{tr}}common-Context{{/tr}} &mdash;</option>
                      {{foreach from=$contexts item=_context}}
                    <option value="{{$_context}}" {{if $_context === $context}}selected{{/if}}>{{tr}}{{$_context}}{{/tr}}</option>
                    {{/foreach}}
                  </select>
                  <button type="button" class="cancel notext" onclick="emptyContext()">
                      {{tr}}Empty{{/tr}}
                  </button>
                </td>
                <th>{{tr}}date.From_long{{/tr}}</th>
                <td>
                    <input type="hidden" name="_date_min" class="date notNull" value="{{$date_min}}"
                           onchange=""/>
                </td>
              <td>
                    <button type="button" class="search"
                            onclick="Board.reloadDocuments({{$praticiens|@array_keys|@json_encode}},this.form)">
                        {{tr}}Search{{/tr}}
                    </button>
                    <button class="change notext compact" type="button" onclick="Board.reloadDocuments({{$praticiens|@array_keys|@json_encode}},this.form)"></button>
                </td>
            </tr>
        </table>
    </form>
    <div id="container_praticiens">
        {{if $praticiens}}
            {{foreach from =$praticiens key=_id item=_prat}}
                <button class='remove rtl' value='{{$_id}}' id="praticien{{$_id}}" onclick="
                        Board.removePraticien(this.id, {{$praticiens|@array_keys|@json_encode}},getForm(getForm('selectPraticien')))">
                    {{$_prat->_view}}</button>
            {{/foreach}}
        {{/if}}
    </div>


    <table class="main" id="refresh_list_documents"></table>
</div>
