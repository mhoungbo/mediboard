<?php
/**
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CMbDT;
use Ox\Core\CSmartyDP;
use Ox\Core\CValue;
use Ox\Mediboard\Maternite\CNaissance;
use Ox\Mediboard\Maternite\CNaissanceRea;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CConstantesMedicales;

/**
 * Suivi du nouveau n� en salle de naissance
 */
$naissance_id = CValue::get("naissance_id");

$naissance = new CNaissance();
$naissance->load($naissance_id);
$naissance->loadRefsResuscitators();

$naissance->loadRefEtat();
$naissance->loadRefMonitorage();
$naissance->loadRefReanimation();
$naissance->loadRefProphylaxie();
$naissance->loadRefMesurePreventive();
$naissance->loadRefSortieSalle();

$enfant    = $naissance->loadRefSejourEnfant()->loadRefPatient();
$grossesse = $naissance->loadRefGrossesse();
$grossesse->countBackRefs("naissances");
$dossier = $grossesse->loadRefDossierPerinat();
$dossier->loadComponents();
$mere    = $grossesse->loadRefParturiente();

// Liste des consultants
$user       = new CMediusers();
$anesths    = $user->loadAnesthesistes();
$praticiens = $user->loadPraticiens();
$profssante = $user->loadProfessionnelDeSante();

// Constantes du nouveau-n�
$constantes = $naissance->loadRefConstantesNouveauNe();
if (!$constantes->_id) {
  $sejour = $naissance->loadRefSejourEnfant();
  $constantes                = new CConstantesMedicales();
  $constantes->patient_id    = $enfant->_id;
  $constantes->context_class = $sejour->_class;
  $constantes->context_id    = $sejour->_id;
  $constantes->datetime      = CMbDT::dateTime();
  $constantes->user_id       = $user->_id;
}
//$constantes->updateFormFields();
$naissance->_ref_nouveau_ne_constantes = $constantes;

$smarty = new CSmartyDP();
$smarty->assign("naissance_rea", new CNaissanceRea());
$smarty->assign("naissance", $naissance);
$smarty->assign("grossesse", $grossesse);
$smarty->assign("anesths", $anesths);
$smarty->assign("praticiens", $praticiens);
$smarty->assign("profssante", $profssante);

$smarty->display("edit_nouveau_ne_salle_naissance.tpl");
