/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

/** Pregnancy class */
export default class Pregnancy extends OxObject {
    constructor () {
        super()
        this.type = "pregnancy"
    }

    get pregnancyWeek (): OxAttr<string> {
        return super.get("_semaine_grossesse")
    }

    get pregnancyDay (): OxAttr<string> {
        return super.get("_reste_semaine_grossesse")
    }

    get guid (): string {
        return "CGrossesse-" + super.id
    }
}
