{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=enfant value=$_naissance->_ref_sejour_enfant->_ref_patient}}
{{assign var=patient value=$grossesse->_ref_parturiente}}
{{assign var=sejour_enfant value=$_naissance->_ref_sejour_enfant}}

<table class="tbl">
  <tr>
    <th colspan="4">{{tr}}Dossier-nouveau-ne_New born{{/tr}}</th>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=sexe}}</th>
    <td>{{mb_value object=$enfant field=sexe}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=naissance}}</th>
    <td>{{mb_value object=$enfant field=naissance}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=nom}}</th>
    <td>{{mb_value object=$enfant field=nom}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=prenom}}</th>
    <td>{{mb_value object=$enfant field=prenom}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=prenoms}}</th>
    <td>{{mb_value object=$enfant field=prenoms}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=lieu_naissance}}</th>
    <td>{{mb_value object=$enfant field=lieu_naissance}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=commune_naissance_insee}}</th>
    <td>{{mb_value object=$enfant field=commune_naissance_insee}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=cp_naissance}}</th>
    <td>{{mb_value object=$enfant field=cp_naissance}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$enfant field=_pays_naissance_insee}}</th>
    <td>{{mb_value object=$enfant field=_pays_naissance_insee}}</td>
  </tr>
  {{if "dPpatients CPatient tutelle_mandatory"|gconf}}
    <tr>
      <th class="narrow">{{mb_label object=$enfant field=tutelle}}</th>
      <td>{{mb_value object=$enfant field=tutelle}}</td>
    </tr>
  {{/if}}
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=sejour_maman_id}}</th>
    <td>{{mb_value object=$_naissance field=sejour_maman_id}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=rques}}</th>
    <td>{{mb_value object=$_naissance field=rques}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=type_allaitement}}</th>
    <td>{{mb_value object=$_naissance field=type_allaitement}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$sejour_enfant field=praticien_id}}</th>
    <td>{{mb_value object=$sejour_enfant field=praticien_id}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$sejour_enfant field=service_id}}</th>
    <td>{{mb_value object=$sejour_enfant field=service_id}}</td>
  </tr>
  {{if $sejour_enfant->uf_soins_id}}
    <tr>
      <th class="narrow">{{mb_label object=$sejour_enfant field=uf_soins_id}}</th>
      <td>{{mb_value object=$sejour_enfant field=uf_soins_id}}</td>
    </tr>
  {{/if}}
  {{if $sejour_enfant->charge_id}}
    <tr>
      <th class="narrow">{{mb_label object=$sejour_enfant field=charge_id}}</th>
      <td>{{mb_value object=$sejour_enfant field=charge_id}}</td>
    </tr>
  {{/if}}
  {{if $sejour_enfant->uf_medicale_id}}
    <tr>
      <th class="narrow">{{mb_label object=$sejour_enfant field=uf_medicale_id}}</th>
      <td>{{mb_value object=$sejour_enfant field=uf_medicale_id}}</td>
    </tr>
  {{/if}}
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=num_naissance}}</th>
    <td>{{mb_value object=$_naissance field=num_naissance}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=hors_etab}}</th>
    <td>{{mb_value object=$_naissance field=hors_etab}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=_heure}}</th>
    <td>{{mb_value object=$_naissance field=_heure}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=rang}}</th>
    <td>{{mb_value object=$_naissance field=rang}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=by_caesarean}}</th>
    <td>{{mb_value object=$_naissance field=by_caesarean}}</td>
  </tr>
  {{if $_naissance->interruption || $_naissance->num_semaines}}
      <tr>
        <th>{{tr}}Dossier-nouveau-ne_Interruption{{/tr}}</th>
        <td>
          <table class="tbl">
            <tr>
              <th class="narrow">{{mb_label object=$_naissance field=interruption}}</th>
              <td>{{mb_value object=$_naissance field=interruption}}</td>
            </tr>
            <tr>
              <th class="narrow">{{mb_label object=$_naissance field=num_semaines}}</th>
              <td>{{mb_value object=$_naissance field=num_semaines}}</td>
            </tr>
          </table>
        </td>
      </tr>
  {{/if}}
  <tr>
    <th class="narrow">{{tr}}Dossier-nouveau-ne_Number of children born{{/tr}}</th>
    <td>{{$grossesse->_count.naissances}}</td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=presence_pediatre}}</th>
    <td>
        {{mb_value object=$_naissance field=presence_pediatre}}
        {{if $_naissance->pediatre_id}}
          - {{mb_value object=$_naissance field=pediatre_id}}
        {{/if}}
    </td>
  </tr>
  <tr>
    <th class="narrow">{{mb_label object=$_naissance field=presence_anesth}}</th>
    <td>
        {{mb_value object=$_naissance field=presence_anesth}}
         {{if $_naissance->anesth_id}}
           - {{mb_value object=$_naissance field=anesth_id}}
         {{/if}}
    </td>
  </tr>
</table>
