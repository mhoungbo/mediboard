{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=reanimation value=$_naissance->_ref_reanimation}}

<table class="tbl">
    <tr>
        <th colspan="4">{{mb_label object=$reanimation field=reanimation}}</th>
    </tr>
    <tr>
        <th class="narrow">
            {{mb_label object=$reanimation field=reanimation}}
        </th>
        <td colspan="3">
            {{mb_value object=$reanimation field=reanimation}}
        </td>
    </tr>
    {{if $_naissance->_ref_resuscitators|@count}}
        <tr>
            <th>Par</th>
            <td colspan="3">
                {{foreach from=$_naissance->_ref_resuscitators item=_resuscitator}}
                    {{$_resuscitator->_ref_rea_par}}
                    {{if $_resuscitator->rea_par}}
                        ({{tr}}CNaissanceRea.rea_par.{{$_resuscitator->rea_par}}{{/tr}})
                        <br/>
                    {{/if}}
                {{/foreach}}
            </td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_aspi_laryngo != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_aspi_laryngo}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=rea_aspi_laryngo}}</td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_ventil_masque != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_ventil_masque}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=rea_ventil_masque}}</td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_o2_sonde != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_o2_sonde}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=rea_o2_sonde}}</td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_ppc_nasale != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_ppc_nasale}}</th>
            <td>{{mb_value object=$reanimation field=rea_ppc_nasale}}</td>
            <th>{{mb_label object=$reanimation field=rea_duree_ppc_nasale}}</th>
            <td>{{mb_value object=$reanimation field=rea_duree_ppc_nasale}} min</td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_ventil_tube_endo != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_ventil_tube_endo}}</th>
            <td>{{mb_value object=$reanimation field=rea_ventil_tube_endo}}</td>
            <th>{{mb_label object=$reanimation field=rea_duree_ventil_tube_endo}}</th>
            <td>{{mb_value object=$reanimation field=rea_duree_ventil_tube_endo}} min</td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_intub_tracheale != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_intub_tracheale}}</th>
            <td>{{mb_value object=$reanimation field=rea_intub_tracheale}}</td>
            <th>{{mb_label object=$reanimation field=rea_min_vie_intub_tracheale}}</th>
            <td>{{mb_value object=$reanimation field=rea_min_vie_intub_tracheale}} min</td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_o2_sonde != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_massage_card}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=rea_massage_card}}</td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_injection_medic != null}}
        <tr>
            <th rowspan="5">{{mb_label object=$reanimation field=rea_injection_medic}}</th>
            <td rowspan="5">{{mb_value object=$reanimation field=rea_injection_medic}}</td>
        </tr>
        <tr>
            <th>{{mb_label object=$reanimation field=rea_injection_medic_adre}}</th>
            <td>{{mb_value object=$reanimation field=rea_injection_medic_adre}}</td>
        </tr>
        <tr>
            <th>{{mb_label object=$reanimation field=rea_injection_medic_surfa}}</th>
            <td>{{mb_value object=$reanimation field=rea_injection_medic_surfa}}</td>
        </tr>
        <tr>
            <th>{{mb_label object=$reanimation field=rea_injection_medic_gluc}}</th>
            <td>{{mb_value object=$reanimation field=rea_injection_medic_gluc}}</td>
        </tr>
        <tr>
            <th>{{mb_label object=$reanimation field=rea_injection_medic_autre}}</th>
            <td>
                {{mb_value object=$reanimation field=rea_injection_medic_autre}}
                <br/>
                {{mb_value object=$reanimation field=rea_injection_medic_autre_desc}}
            </td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_autre_geste != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_autre_geste}}</th>
            <td colspan="3">
                {{mb_value object=$reanimation field=rea_autre_geste}}
                <br/>
                {{mb_value object=$reanimation field=rea_autre_geste_desc}}
            </td>
        </tr>
    {{/if}}
    {{if $reanimation->duree_totale_rea != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=duree_totale_rea}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=duree_totale_rea}} min</td>
        </tr>
    {{/if}}
    {{if $reanimation->temp_fin_rea != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=temp_fin_rea}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=temp_fin_rea}} �</td>
        </tr>
    {{/if}}
    {{if $reanimation->gly_fin_rea != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=gly_fin_rea}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=gly_fin_rea}} mmol/l</td>
        </tr>
    {{/if}}
    {{if $reanimation->etat_fin_rea != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=etat_fin_rea}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=etat_fin_rea}}</td>
        </tr>
    {{/if}}
    {{if $reanimation->rea_remarques != null}}
        <tr>
            <th>{{mb_label object=$reanimation field=rea_remarques}}</th>
            <td colspan="3">{{mb_value object=$reanimation field=rea_remarques}}</td>
        </tr>
    {{/if}}
</table>
