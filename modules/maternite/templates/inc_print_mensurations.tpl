{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl">
    {{assign var=constantes value=$_naissance->_ref_nouveau_ne_constantes}}
  <tr>
    <th colspan="2" class="narrow">{{tr}}Dossier-nouveau-ne_Measurements{{/tr}}</th>
  </tr>
    {{if $constantes->_poids_g}}
      <tr>
        <th class="narrow">{{mb_label object=$constantes field=_poids_g}}</th>
        <td>{{mb_value object=$constantes field=_poids_g}}</td>
      </tr>
    {{/if}}
    {{if $constantes->taille}}
      <tr>
        <th class="narrow">{{mb_label object=$constantes field=taille}}</th>
        <td>{{mb_value object=$constantes field=taille}}</td>
      </tr>
    {{/if}}
    {{if $constantes->perimetre_cranien}}
      <tr>
        <th class="narrow">{{mb_label object=$constantes field=perimetre_cranien}}</th>
        <td>{{mb_value object=$constantes field=perimetre_cranien}}</td>
      </tr>
    {{/if}}
</table>
