{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl">
  <tr>
    <th colspan="2">{{mb_label object=$_naissance field=monitorage}}</th>
  </tr>
  <tr>
    <th class="narrow">
        {{mb_label object=$_naissance field=monitorage}}
    </th>
    <td>
        {{mb_value object=$_naissance field=monitorage}}
    </td>
  </tr>
    {{if $_naissance->monit_frequence_cardiaque != null}}
      <tr>
        <th>{{mb_label object=$_naissance field=monit_frequence_cardiaque}}</th>
        <td>{{mb_value object=$_naissance field=monit_frequence_cardiaque}}</td>
      </tr>
    {{/if}}
    {{if $_naissance->monit_saturation != null}}
      <tr>
        <th>{{mb_label object=$_naissance field=monit_saturation}}</th>
        <td>{{mb_value object=$_naissance field=monit_saturation}}</td>
      </tr>
    {{/if}}
    {{if $_naissance->monit_glycemie != null}}
      <tr>
        <th>{{mb_label object=$_naissance field=monit_glycemie}}</th>
        <td>{{mb_value object=$_naissance field=monit_glycemie}}</td>
      </tr>
    {{/if}}
    {{if $_naissance->monit_incubateur != null}}
      <tr>
        <th>{{mb_label object=$_naissance field=monit_incubateur}}</th>
        <td>{{mb_value object=$_naissance field=monit_incubateur}}</td>
      </tr>
    {{/if}}
    {{if $_naissance->monit_remarques != null}}
      <tr>
        <th>{{mb_label object=$_naissance field=monit_remarques}}</th>
        <td>{{mb_value object=$_naissance field=monit_remarques}}</td>
      </tr>
    {{/if}}
</table>
