{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=mesure_preventive value=$_naissance->_ref_mesure_preventive}}

<table class="tbl">
    <tr>
        <th colspan="2" class="narrow">{{tr}}Dossier-nouveau-ne_Prenatal or postnatal preventive measures{{/tr}}</th>
    </tr>
    {{if $mesure_preventive->cortico != null}}
        <tr>
            <td class="narrow">{{mb_label object=$mesure_preventive field=cortico}}</td>
            <td>
                <table class="tbl">
                    <tr>
                        <th colspan="2" class="narrow">{{mb_label object=$mesure_preventive field=cortico}}</th>
                        <td>{{mb_value object=$mesure_preventive field=cortico}}</td>
                    </tr>
                    {{if $mesure_preventive->cortico}}
                        {{if $mesure_preventive->nb_cures_cortico}}
                            <tr>
                                <th colspan="2"
                                    class="narrow">{{mb_label object=$mesure_preventive field=nb_cures_cortico}}</th>
                                <td>{{mb_value object=$mesure_preventive field=nb_cures_cortico}}</td>
                            </tr>
                        {{/if}}
                        {{if $mesure_preventive->dern_cure_cortico}}
                            <tr>
                                <th colspan="2"
                                    class="narrow">{{mb_label object=$mesure_preventive field=dern_cure_cortico}}</th>
                                <td>{{mb_value object=$mesure_preventive field=dern_cure_cortico}}</td>
                            </tr>
                        {{/if}}
                        {{if $mesure_preventive->delai_cortico_acc_j}}
                            <tr>
                                <th colspan="2"
                                    class="narrow">{{mb_label object=$mesure_preventive field=delai_cortico_acc_j}}</th>
                                <td>
                                    {{mb_value object=$mesure_preventive field=delai_cortico_acc_j}} j
                                    {{mb_value object=$mesure_preventive field=delai_cortico_acc_h}} h
                                </td>
                            </tr>
                        {{/if}}
                        {{if $mesure_preventive->prev_cortico_remarques}}
                            <tr>
                                <th colspan="2"
                                    class="narrow">{{mb_label object=$mesure_preventive field=prev_cortico_remarques}}</th>
                                <td>{{mb_value object=$mesure_preventive field=prev_cortico_remarques}}</td>
                            </tr>
                        {{/if}}
                    {{/if}}
                </table>
            </td>
        </tr>
    {{/if}}
    {{if $mesure_preventive->contexte_infectieux != null}}
        <tr>
            <td class="narrow">{{mb_label object=$mesure_preventive field=contexte_infectieux}}</td>
            <td>
                <table class="tbl">
                    <tr>
                        <th class="narrow">{{mb_label object=$mesure_preventive field=contexte_infectieux}}</th>
                        <td>{{mb_value object=$mesure_preventive field=contexte_infectieux}}</td>
                    </tr>
                    {{if $mesure_preventive->contexte_infectieux}}
                        <tr>
                            <th
                              class="narrow">{{mb_label object=$mesure_preventive field=infect_facteurs_risque_infect}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_facteurs_risque_infect}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_rpm_sup_12h}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_rpm_sup_12h}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_liquide_teinte}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_liquide_teinte}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_strepto_b}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_strepto_b}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_fievre_mat}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_fievre_mat}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_maternelle}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_maternelle}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_autre}}</th>
                            <td>
                                {{mb_value object=$mesure_preventive field=infect_autre}} -
                                {{mb_value object=$mesure_preventive field=infect_autre_desc}}
                            </td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_prelev_bacterio}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_prelev_bacterio}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_prelev_gatrique}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_prelev_gatrique}}</td>
                        </tr>
                        <tr>
                            <th
                              class="narrow">{{mb_label object=$mesure_preventive field=infect_prelev_autre_periph}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_prelev_autre_periph}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_prelev_placenta}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_prelev_placenta}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_prelev_sang}}</th>
                            <td>{{mb_value object=$mesure_preventive field=infect_prelev_sang}}</td>
                        </tr>
                        <tr>
                            <th class="narrow">{{mb_label object=$mesure_preventive field=infect_antibio}}</th>
                            <td>
                                {{mb_value object=$mesure_preventive field=infect_antibio}}
                                {{if $mesure_preventive->infect_antibio_desc}}
                                    - {{mb_value object=$mesure_preventive field=infect_antibio_desc}}
                                {{/if}}
                            </td>
                        </tr>
                        {{if $mesure_preventive->infect_remarques}}
                            <tr>
                                <th class="narrow">{{mb_label object=$mesure_preventive field=infect_remarques}}</th>
                                <td>{{mb_value object=$mesure_preventive field=infect_remarques}}</td>
                            </tr>
                        {{/if}}
                    {{/if}}
                </table>
            </td>
        </tr>
    {{/if}}
    <tr>
        <td class="narrow">{{tr}}Dossier-nouveau-ne_Reminder to mother{{/tr}}</td>
        <td>
            <table class="tbl">
                <tr>
                    <th class="narrow">{{mb_label object=$mesure_preventive field=prelev_bacterio_mere}}</th>
                    <td>{{mb_value object=$mesure_preventive field=prelev_bacterio_mere}}</td>
                </tr>
                <tr>
                    <th class="narrow">{{mb_label object=$mesure_preventive field=prelev_bacterio_vaginal_mere}}</th>
                    <td>
                        {{mb_value object=$mesure_preventive field=prelev_bacterio_vaginal_mere}}
                        {{if $mesure_preventive->prelev_bacterio_vaginal_mere_germe}}
                            - {{mb_value object=$mesure_preventive field=prelev_bacterio_vaginal_mere_germe}}
                        {{/if}}
                    </td>
                </tr>
                <tr>
                    <th class="narrow">{{mb_label object=$mesure_preventive field=prelev_bacterio_urinaire_mere}}</th>
                    <td>
                        {{mb_value object=$mesure_preventive field=prelev_bacterio_urinaire_mere}}
                        {{if $mesure_preventive->prelev_bacterio_urinaire_mere_germe}}
                            - {{mb_value object=$mesure_preventive field=prelev_bacterio_urinaire_mere_germe}}
                        {{/if}}
                    </td>
                </tr>
                <tr>
                    <th class="narrow">{{mb_label object=$mesure_preventive field=antibiotherapie_antepart_mere}}</th>
                    <td>
                        {{mb_value object=$mesure_preventive field=antibiotherapie_antepart_mere}}
                        {{if $mesure_preventive->antibiotherapie_antepart_mere_desc}}
                            - {{mb_value object=$mesure_preventive field=antibiotherapie_antepart_mere_desc}}
                        {{/if}}
                    </td>
                </tr>
                <tr>
                    <th class="narrow">{{mb_label object=$mesure_preventive field=antibiotherapie_perpart_mere}}</th>
                    <td>
                        {{mb_value object=$mesure_preventive field=antibiotherapie_perpart_mere}}
                        {{if $mesure_preventive->antibiotherapie_perpart_mere_desc}}
                            - {{mb_value object=$mesure_preventive field=antibiotherapie_perpart_mere_desc}}
                        {{/if}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
