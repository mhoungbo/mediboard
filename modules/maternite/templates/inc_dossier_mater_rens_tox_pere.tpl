{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<form name="Tox-pere-{{$dossier->_guid}}" method="post"
      onsubmit="return onSubmitFormAjax(this);">
  {{mb_class object=$dossier}}
  {{mb_key   object=$dossier}}
  <input type="hidden" name="_count_changes" value="0" />
  <input type="hidden" name="grossesse_id" value="{{$grossesse->_id}}" />
  <table class="form me-no-align me-no-box-shadow me-small-form">
    <tr>
      <th class="title" colspan="2">P�re</th>
    </tr>
    {{if $grossesse->pere_id}}
      <tr>
        <th class="halfPane">{{mb_label object=$dossier field=_tabac_pere}}</th>
        <td>{{mb_field object=$dossier field=_tabac_pere typeEnum=checkbox}}</td>
      </tr>
      <tr>
        <th><span class="compact">{{mb_label object=$dossier field=_coexp_pere}}</span></th>
        <td>{{mb_field object=$dossier field=_coexp_pere}}</td>
      </tr>
      <tr>
        <th>{{mb_label object=$dossier field=_alcool_pere}}</th>
        <td>{{mb_field object=$dossier field=_alcool_pere typeEnum=checkbox}}</td>
      </tr>
      <tr>
        <th>{{mb_label object=$dossier field=_toxico_pere}}</th>
        <td>{{mb_field object=$dossier field=_toxico_pere typeEnum=checkbox}}</td>
      </tr>
    {{else}}
      <tr>
        <td colspan="2" class="empty">P�re non renseign�</td>
      </tr>
    {{/if}}
  </table>
</form>
