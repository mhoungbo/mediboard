{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl me-letter-spacing-tight">
  <tbody>
  {{foreach from=$grossesse->_ref_consultations item=consult}}
    {{assign var=suivi value=$consult->_ref_suivi_grossesse}}
    <tr>
      <td class="narrow {{if $consult->annule}}cancelled{{/if}}">
          {{if !$offline}}
            <button type="button" class="consultation notext not-printable me-tertiary" title="Modifier la consultation"
                    onclick="Tdb.editConsult('{{$consult->_id}}', refreshListeSuivis);"></button>
            <button type="button" class="clock notext not-printable me-tertiary" title="Modifier le RDV"
                    onclick="Tdb.editRdvConsult('{{$consult->_id}}', '{{$grossesse->_id}}', '{{$consult->patient_id}}', refreshListeSuivis);"></button>
          {{/if}}
      </td>
      <td class="{{if $consult->annule}}cancelled{{/if}}">
          <ul>
              <li> {{tr}}Consultation of{{/tr}}
                  {{mb_value object=$consult field=_datetime}},
                  {{mb_value object=$consult field=_sa}} SA  + {{mb_value object=$consult field=_ja}} j,
                  {{mb_include module=mediusers template=inc_vw_mediuser mediuser=$consult->_ref_praticien}}
                  {{if $suivi && $suivi->_id}}
                    , {{mb_value object=$suivi field=type_suivi}}
                  {{/if}}
              </li>
              {{if $consult->annule}}
                <li>
                    {{tr}}Cancelled{{/tr}}
                </li>
              {{elseif $suivi && $suivi->_id}}
                  <li>
                      {{mb_label class=CSuiviGrossesse field=evenements_anterieurs}} : {{$suivi->evenements_anterieurs}}
                  </li>
                  <li>
                    {{tr}}Current_functional_signs{{/tr}} :
                  <ul>
                  {{if $suivi->metrorragies !== null}}
                    <li>
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-check"}}
                      {{if $suivi->metrorragies === '1'}}
                        {{assign var=backgroundQuestion value="red"}}
                        {{assign var=iconQuestion value="fa-times"}}
                      {{/if}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=metrorragies}} :
                        {{mb_value object=$suivi field=metrorragies}}
                      </span>
                    </li>
                  {{/if}}
                  {{if $suivi->leucorrhees !== null}}
                    <li>
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-check"}}
                      {{if $suivi->leucorrhees === '1'}}
                        {{assign var=backgroundQuestion value="red"}}
                        {{assign var=iconQuestion value="fa-times"}}
                      {{/if}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=leucorrhees}} :
                        {{mb_value object=$suivi field=leucorrhees}}
                      </span>
                    </li>
                  {{/if}}
                  {{if $suivi->contractions_anormales !== null}}
                    <li>
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-check"}}
                      {{if $suivi->contractions_anormales === '1'}}
                        {{assign var=backgroundQuestion value="red"}}
                        {{assign var=iconQuestion value="fa-times"}}
                      {{/if}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=contractions_anormales}} :
                        {{mb_value object=$suivi field=contractions_anormales}}
                      </span>
                    </li>
                  {{/if}}
                  {{if $suivi->mouvements_foetaux !== null}}
                    <li>
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-times"}}
                      {{if $suivi->mouvements_foetaux === '1'}}
                        {{assign var=iconQuestion value="fa-check"}}
                      {{/if}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=mouvements_foetaux}} :
                        {{mb_value object=$suivi field=mouvements_foetaux}}
                      </span>
                    </li>
                  {{/if}}
                  {{if $suivi->mouvements_actifs !== null}}
                    <li>
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-times"}}
                      {{if $suivi->mouvements_actifs === 'percu'}}
                        {{assign var=iconQuestion value="fa-check"}}
                      {{/if}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=mouvements_actifs}} :
                        {{mb_value object=$suivi field=mouvements_actifs}}
                      </span>
                    </li>
                  {{/if}}
                  {{if $suivi->hypertension !== null}}
                    <li>
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-times"}}
                      {{if $suivi->hypertension === '1'}}
                        {{assign var=iconQuestion value="fa-check"}}
                      {{/if}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=hypertension}} :
                        {{mb_value object=$suivi field=hypertension}}
                      </span>
                    </li>
                  {{/if}}
                  {{if $suivi->troubles_digestifs !== null}}
                    <li>
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-check"}}
                      {{if $suivi->troubles_digestifs === '1'}}
                        {{assign var=backgroundQuestion value="red"}}
                        {{assign var=iconQuestion value="fa-times"}}
                      {{/if}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=troubles_digestifs}} :
                        {{mb_value object=$suivi field=troubles_digestifs}}
                      </span>
                    </li>
                  {{/if}}
                  {{if $suivi->troubles_urinaires !== null}}
                    <li>
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-check"}}
                      {{if $suivi->troubles_urinaires === '1'}}
                        {{assign var=backgroundQuestion value="red"}}
                        {{assign var=iconQuestion value="fa-times"}}
                      {{/if}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=troubles_urinaires}} :
                        {{mb_value object=$suivi field=troubles_urinaires}}
                      </span>
                    </li>
                  {{/if}}
                  {{if $suivi->autres_anomalies}}
                    <li>
                      {{assign var=backgroundQuestion value="red"}}
                      {{assign var=iconQuestion value="fa-circle"}}
                      <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                      <span style="color: {{$backgroundQuestion}}">
                        {{mb_label object=$suivi field=autres_anomalies}} : {{$suivi->autres_anomalies}}
                      </span>
                    </li>
                  {{/if}}
                  </ul>
                </li>
                <li>
                  {{tr}}General_examination{{/tr}} :
                  <ul>
                    {{if $suivi->auscultation_cardio_pulm !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->auscultation_cardio_pulm === 'anomalie'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=auscultation_cardio_pulm}} :
                          {{mb_value object=$suivi field=auscultation_cardio_pulm}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->examen_seins !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->examen_seins === 'mamomb' || $suivi->examen_seins === 'autre'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=examen_seins}} :
                          {{mb_value object=$suivi field=examen_seins}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->circulation_veineuse !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->circulation_veineuse === 'insmod'|| $suivi->circulation_veineuse === 'inssev'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=circulation_veineuse}} :
                          {{mb_value object=$suivi field=circulation_veineuse}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->oedeme_membres_inf !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-times"}}
                        {{if $suivi->oedeme_membres_inf === '1'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-check"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=oedeme_membres_inf}} :
                          {{mb_value object=$suivi field=oedeme_membres_inf}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->rques_examen_general}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-circle"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=rques_examen_general}} : {{$suivi->rques_examen_general}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $consult->_list_constantes_medicales}}
                      {{assign var=backgroundQuestion value="black"}}
                      {{assign var=iconQuestion value="fa-circle"}}
                      {{foreach from=$selection_constantes item=_name_cte}}
                        {{if $consult->_list_constantes_medicales->$_name_cte &&
                            ($_name_cte != "temperature" || ($suivi && $suivi->type_suivi == "urg"))}}
                          <li>
                            <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                            <span style="color: {{$backgroundQuestion}}">
                              {{mb_label object=$consult->_list_constantes_medicales field=$_name_cte}}:
                              {{if $_name_cte == "ta"}}
                                {{mb_value object=$consult->_list_constantes_medicales field=_ta_systole}} /
                                {{mb_value object=$consult->_list_constantes_medicales field=_ta_diastole}}
                              {{else}}
                                {{mb_value object=$consult->_list_constantes_medicales field=$_name_cte}}
                              {{/if}}
                              {{$liste_unites.$_name_cte.unit}}
                            </span>
                          </li>
                        {{/if}}
                      {{/foreach}}
                    {{/if}}
                  </ul>
                </li>
                <li>
                  {{tr}}Obstetric_examination{{/tr}} :
                  <ul>
                    {{if $suivi->bruit_du_coeur !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-times"}}
                        {{if $suivi->bruit_du_coeur === 'percu'}}
                          {{assign var=iconQuestion value="fa-check"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=bruit_du_coeur}} :
                          {{mb_value object=$suivi field=bruit_du_coeur}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->col_normal !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->col_normal === 'n'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=col_normal}} :
                          {{mb_value object=$suivi field=col_normal}}
                        </span>
                        <ul>
                          {{if $suivi->longueur_col !== null}}
                            <li>
                              {{mb_label object=$suivi field=longueur_col}} :
                              {{mb_value object=$suivi field=longueur_col}}
                            </li>
                          {{/if}}
                          {{if $suivi->position_col !== null}}
                            <li>
                              {{mb_label object=$suivi field=position_col}} :
                              {{mb_value object=$suivi field=position_col}}
                            </li>
                          {{/if}}
                          {{if $suivi->dilatation_col !== null}}
                            <li>
                              {{mb_label object=$suivi field=dilatation_col}} :
                              {{mb_value object=$suivi field=dilatation_col}} {{if $suivi->dilatation_col_num}}({{$suivi->dilatation_col_num}} cm){{/if}}
                            </li>
                          {{/if}}
                          {{if $suivi->consistance_col !== null}}
                            <li>
                              {{mb_label object=$suivi field=consistance_col}} :
                              {{mb_value object=$suivi field=consistance_col}}
                            </li>
                          {{/if}}
                        </ul>
                      </li>
                    {{/if}}
                    {{if $suivi->col_commentaire}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-circle"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=col_commentaire}} : {{$suivi->col_commentaire}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->presentation_position !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-question"}}
                        {{if $suivi->presentation_position === 'som'}}
                          {{assign var=iconQuestion value="fa-check"}}
                        {{elseif $suivi->presentation_position === 'sie' || $suivi->presentation_position === 'tra'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-check"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=presentation_position}} :
                          {{mb_value object=$suivi field=presentation_position}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->presentation_etat !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->presentation_etat === 'amo' ||
                            $suivi->presentation_etat === 'fix' ||
                            $suivi->presentation_etat === 'eng'}}
                          {{assign var=backgroundQuestion value="red"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=presentation_etat}} :
                          {{mb_value object=$suivi field=presentation_etat}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->segment_inferieur !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->segment_inferieur === 'namp'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=segment_inferieur}} :
                          {{mb_value object=$suivi field=segment_inferieur}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->membranes !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->membranes === 'romp' || $suivi->membranes === 'susrupt'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=membranes}} :
                          {{mb_value object=$suivi field=membranes}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->bassin !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->bassin === 'anomalie'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=bassin}} :
                          {{mb_value object=$suivi field=bassin}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->examen_genital !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->examen_genital === 'anomalie'}}
                          {{assign var=backgroundQuestion value="red"}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=examen_genital}} :
                          {{mb_value object=$suivi field=examen_genital}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->hauteur_uterine}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=hauteur_uterine}} :
                          {{mb_value object=$suivi field=hauteur_uterine}} cm
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->rques_exam_gyneco_obst}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-circle"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=rques_exam_gyneco_obst}} : {{$suivi->rques_exam_gyneco_obst}}
                        </span>
                      </li>
                    {{/if}}
                  </ul>
                </li>
                <li>
                  {{tr}}Additional_tests{{/tr}} :
                  <ul>
                    {{if $suivi->frottis !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->frottis === 'nfait'}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=frottis}} :
                          {{mb_value object=$suivi field=frottis}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->echographie !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->echographie === 'nfait'}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=echographie}} :
                          {{mb_value object=$suivi field=echographie}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->prelevement_bacterio !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        {{if $suivi->prelevement_bacterio === 'nfait'}}
                          {{assign var=iconQuestion value="fa-times"}}
                        {{/if}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=prelevement_bacterio}} :
                          {{mb_value object=$suivi field=prelevement_bacterio}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->glycosurie !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=glycosurie}} :
                          {{mb_value object=$suivi field=glycosurie}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->leucocyturie !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=leucocyturie}} :
                          {{mb_value object=$suivi field=leucocyturie}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->albuminurie !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=albuminurie}} :
                          {{mb_value object=$suivi field=albuminurie}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->nitrites !== null}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-check"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=nitrites}} :
                          {{mb_value object=$suivi field=nitrites}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->autre_exam_comp}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-circle"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=autre_exam_comp}} : {{$suivi->autre_exam_comp}}
                        </span>
                      </li>
                    {{/if}}
                    {{if $suivi->jours_arret_travail}}
                      <li>
                        {{assign var=backgroundQuestion value="black"}}
                        {{assign var=iconQuestion value="fa-circle"}}
                        <i class="fa {{$iconQuestion}}" style="color: {{$backgroundQuestion}};"></i>
                        <span style="color: {{$backgroundQuestion}}">
                          {{mb_label object=$suivi field=jours_arret_travail}}
                          : {{mb_value object=$suivi field=jours_arret_travail}}
                        </span>
                      </li>
                    {{/if}}
                  </ul>
                </li>
                <li>
                  {{tr}}Prescription{{/tr}} :
                  {{if $prescription_installed}}
                      {{if isset($consult->_ref_prescriptions.externe|smarty:nodefaults)}}
                          <ul>
                            {{assign var=prescription value=$consult->_ref_prescriptions.externe}}
                            {{if $prescription->_ref_prescription_lines|@count}}
                              <li>{{tr}}CPrescription._chapitres.med{{/tr}} :</li>
                              <ul>
                                  {{foreach from=$prescription->_ref_prescription_lines item=_line_med}}
                                    <li>
                                      <strong>
                                          {{$_line_med->_ucd_view}}
                                      </strong>
                                    </li>
                                  {{/foreach}}
                              </ul>
                            {{/if}}
                            {{if $prescription->_ref_prescription_lines_element_by_chap|@count}}
                                {{foreach from=$prescription->_ref_prescription_lines_element_by_chap key=chap item=lines_elt}}
                                  <li>{{tr}}CPrescription._chapitres.{{$chap}}{{/tr}} :</li>
                                  <ul>
                                      {{foreach from=$lines_elt item=_line_elt}}
                                        <li>
                                          <strong>
                                              {{$_line_elt}}
                                          </strong>
                                        </li>
                                      {{/foreach}}
                                  </ul>
                                {{/foreach}}
                            {{/if}}
                          </ul>
                      {{/if}}
                  {{/if}}
                </li>
                <li>
                  {{mb_label class=CSuiviGrossesse field=conclusion}} : {{$suivi->conclusion}}
                </li>
              {{else}}
                <li>
                  {{tr}}CSuiviGrossesse.none{{/tr}} :
                </li>
              {{/if}}
          </ul>
        </td>
    </tr>
  {{/foreach}}
  </tbody>
</table>
