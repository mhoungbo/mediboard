{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=dossier value=$grossesse->_ref_dossier_perinat}}
{{assign var=patient value=$grossesse->_ref_parturiente}}
{{assign var=sejour value=$naissance->_ref_sejour_enfant}}
{{assign var=enfant value=$sejour->_ref_patient}}

{{assign var=lesion value=$naissance->_ref_lesion}}
{{assign var=infection value=$naissance->_ref_infection}}
{{assign var=ictere value=$naissance->_ref_ictere}}
{{assign var=regulation_thermique value=$naissance->_ref_regulation_thermique}}
{{assign var=anomalie_congenitale value=$naissance->_ref_anomalie_congenitale}}
{{assign var=pathologie value=$naissance->_ref_pathologie}}
{{assign var=acte value=$naissance->_ref_acte}}
{{assign var=prophylaxie value=$naissance->_ref_prophylaxie_sejour}}
{{assign var=sortie_sejour value=$naissance->_ref_sortie_sejour}}

<script>
    listForms = [
        getForm("Pathologies-{{$naissance->_guid}}"),
        getForm("Pathologies-Lesions-{{$naissance->_guid}}"),
        getForm("Pathologies-Infection-{{$naissance->_guid}}"),
        getForm("Pathologies-Anomalie-{{$naissance->_guid}}"),
        getForm("Pathologies-Ictere-{{$naissance->_guid}}"),
        getForm("Pathologies-Thermique-{{$naissance->_guid}}"),
        getForm("Pathologies-Autres-{{$naissance->_guid}}"),
        getForm("Actes-{{$naissance->_guid}}"),
        getForm("Prophylaxie-{{$naissance->_guid}}"),
        getForm("Sortie-enfant-{{$naissance->_guid}}")
    ];

    includeForms = function () {
        DossierMater.listForms = listForms.clone();
    };

    submitAllForms = function (callBack) {
        includeForms();
        DossierMater.submitAllForms(callBack);
    };

    Main.add(function () {
        includeForms();
        DossierMater.prepareAllForms();
    });
</script>

{{mb_include module=maternite template=inc_dossier_mater_header}}

<table class="main layout">
    <tr>
        <td>
            <script>
                Main.add(function () {
                    Control.Tabs.create('tab-resume-sejour', true);
                });
            </script>
            <ul id="tab-resume-sejour" class="control_tabs">
                <li><a href="#pathologies">Pathologies</a></li>
                <li><a href="#actes_effectues">Actes</a></li>
                <li><a href="#mesures_prophylactiques">Mesures Prophylactiques</a></li>
                <li><a href="#sortie_enfant">Sortie de l'enfant de la maternit�</a></li>
            </ul>

            <div id="pathologies" style="display: none;">
                <table class="main layout">
                    <tr>
                        <td colspan="2">
                            <form name="Pathologies-{{$naissance->_guid}}" method="post"
                                  onsubmit="return onSubmitFormAjax(this);">
                                {{mb_class object=$naissance}}
                                {{mb_key   object=$naissance}}
                                <input type="hidden" name="_count_changes" value="0"/>
                                <table class="form">
                                    <tr>
                                        <th class="halfPane">
                                            <strong>{{mb_label object=$naissance field=pathologies}}</strong></th>
                                        <td><strong>{{mb_field object=$naissance field=pathologies default=""}}</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Si oui,</th>
                                        <td></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td class="halfPane">
                            <form name="Pathologies-Lesions-{{$naissance->_guid}}" method="post"
                                  onsubmit="return onSubmitFormAjax(this);">
                                {{mb_class object=$lesion}}
                                {{mb_key   object=$lesion}}

                                <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                                <input type="hidden" name="_count_changes" value="0"/>
                                <fieldset>
                                    <legend>
                                        {{mb_field object=$lesion field=lesion_traumatique typeEnum=checkbox}}
                                        {{mb_label object=$lesion field=lesion_traumatique}}
                                    </legend>
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$lesion field=lesion_faciale typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$lesion field=lesion_faciale}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$lesion field=paralysie_faciale typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$lesion field=paralysie_faciale}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$lesion field=cephalhematome typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$lesion field=cephalhematome}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$lesion field=paralysie_plexus_brachial_sup typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$lesion field=paralysie_plexus_brachial_sup}}</td>
                                            <th>{{mb_field object=$lesion field=paralysie_plexus_brachial_inf typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$lesion field=paralysie_plexus_brachial_inf}}</td>
                                            <th>{{mb_field object=$lesion field=lesion_cuir_chevelu typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$lesion field=lesion_cuir_chevelu}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$lesion field=fracture_clavicule typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$lesion field=fracture_clavicule}}</td>
                                            <th>{{mb_field object=$lesion field=autre_lesion typeEnum=checkbox}}</th>
                                            <td colspan="3">
                                                {{mb_label object=$lesion field=autre_lesion}}
                                                {{mb_label object=$lesion field=autre_lesion_desc style="display: none;"}}
                                                {{mb_field object=$lesion field=autre_lesion_desc}}
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                            <form name="Pathologies-Infection-{{$naissance->_guid}}" method="post"
                                  onsubmit="return onSubmitFormAjax(this);">
                                {{mb_class object=$infection}}
                                {{mb_key   object=$infection}}

                                <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                                <input type="hidden" name="_count_changes" value="0"/>
                                <fieldset>
                                    <legend>
                                        {{mb_field object=$infection field=infection typeEnum=checkbox}}
                                        {{mb_label object=$infection field=infection}}
                                    </legend>
                                    <table class="form">
                                        <tr>
                                            <th colspan="2">{{mb_label object=$infection field=degre}}</th>
                                            <td colspan="4">
                                                {{mb_field object=$infection field=degre
                                                style="width: 20em;" emptyLabel="Infection.degre."}}
                                            </td>
                                            <th colspan="2">{{mb_label object=$infection field=origine}}</th>
                                            <td colspan="4">
                                                {{mb_field object=$infection field=origine
                                                style="width: 20em;" emptyLabel="Infection.origine."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="category" colspan="12">Localisation</th>
                                        </tr>
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$infection field=sang typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=sang}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$infection field=lcr typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=lcr}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$infection field=poumon typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=poumon}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$infection field=urines typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=urines}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$infection field=digestif typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=digestif}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$infection field=ombilic typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=ombilic}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$infection field=oeil typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=oeil}}</td>
                                            <th>{{mb_field object=$infection field=os_articulations typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=os_articulations}}</td>
                                            <th>{{mb_field object=$infection field=peau typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=peau}}</td>
                                            <th>{{mb_field object=$infection field=autre typeEnum=checkbox}}</th>
                                            <td colspan="5">
                                                {{mb_label object=$infection field=autre}}
                                                {{mb_label object=$infection field=autre_desc style="display: none;"}}
                                                {{mb_field object=$infection field=autre_desc}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="category" colspan="12">Germe</th>
                                        </tr>
                                        <tr>
                                        <tr>
                                            <th>{{mb_field object=$infection field=strepto_b typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=strepto_b}}</td>
                                            <th>{{mb_field object=$infection field=autre_strepto typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=autre_strepto}}</td>
                                            <th>{{mb_field object=$infection field=staphylo_dore typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=staphylo_dore}}</td>
                                            <th>{{mb_field object=$infection field=autre_staphylo typeEnum=checkbox}}</th>
                                            <td colspan="5">{{mb_label object=$infection field=autre_staphylo}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$infection field=haemophilus typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=haemophilus}}</td>
                                            <th>{{mb_field object=$infection field=listeria typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=listeria}}</td>
                                            <th>{{mb_field object=$infection field=pneumocoque typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=pneumocoque}}</td>
                                            <th>{{mb_field object=$infection field=autre_gplus typeEnum=checkbox}}</th>
                                            <td colspan="5">{{mb_label object=$infection field=autre_gplus}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$infection field=coli typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=coli}}</td>
                                            <th>{{mb_field object=$infection field=proteus typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=proteus}}</td>
                                            <th>{{mb_field object=$infection field=klebsiele typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=klebsiele}}</td>
                                            <th>{{mb_field object=$infection field=autre_gmoins typeEnum=checkbox}}</th>
                                            <td colspan="5">{{mb_label object=$infection field=autre_gmoins}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$infection field=chlamydiae typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=chlamydiae}}</td>
                                            <th>{{mb_field object=$infection field=mycoplasme typeEnum=checkbox}}</th>
                                            <td colspan="9">{{mb_label object=$infection field=mycoplasme}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$infection field=candida typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=candida}}</td>
                                            <th>{{mb_field object=$infection field=toxoplasme typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=toxoplasme}}</td>
                                            <th>{{mb_field object=$infection field=autre_parasite typeEnum=checkbox}}</th>
                                            <td colspan="7">{{mb_label object=$infection field=autre_parasite}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              style="vertical-align: top;">{{mb_field object=$infection field=cmv typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=cmv}}</td>
                                            <th
                                              style="vertical-align: top;">{{mb_field object=$infection field=rubeole typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=rubeole}}</td>
                                            <th
                                              style="vertical-align: top;">{{mb_field object=$infection field=herpes typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=herpes}}</td>
                                            <th
                                              style="vertical-align: top;">{{mb_field object=$infection field=varicelle typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=varicelle}}</td>
                                            <th
                                              style="vertical-align: top;">{{mb_field object=$infection field=vih typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$infection field=vih}}</td>
                                            <th
                                              style="vertical-align: top;">{{mb_field object=$infection field=autre_virus typeEnum=checkbox}}</th>
                                            <td>
                                                {{mb_label object=$infection field=autre_virus}}
                                                <br/>
                                                {{mb_label object=$infection field=autre_virus_desc style="display: none;"}}
                                                {{mb_field object=$infection field=autre_virus_desc}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$infection field=germe_non_trouve typeEnum=checkbox}}</th>
                                            <td colspan="11">{{mb_label object=$infection field=germe_non_trouve}}</td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                            <form name="Pathologies-Ictere-{{$naissance->_guid}}" method="post"
                                  onsubmit="return onSubmitFormAjax(this);">
                                {{mb_class object=$ictere}}
                                {{mb_key   object=$ictere}}

                                <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                                <input type="hidden" name="_count_changes" value="0"/>
                                <fieldset>
                                    <legend>
                                        {{mb_field object=$ictere field=ictere typeEnum=checkbox}}
                                        {{mb_label object=$ictere field=ictere}}
                                    </legend>
                                    <table class="form">
                                        <tr>
                                            <th class="category" colspan="4">Origine</th>
                                        </tr>
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$ictere field=prema typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$ictere field=prema}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$ictere field=intense_terme typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$ictere field=intense_terme}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$ictere field=allo_immun_abo typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$ictere field=allo_immun_abo}}</td>
                                            <th>{{mb_field object=$ictere field=allo_immun_rh typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$ictere field=allo_immun_rh}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$ictere field=allo_immun_autre typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$ictere field=allo_immun_autre}}</td>
                                            <th>{{mb_field object=$ictere field=autre_origine typeEnum=checkbox}}</th>
                                            <td>
                                                {{mb_label object=$ictere field=autre_origine}}
                                                {{mb_label object=$ictere field=autre_origine_desc style="display: none;"}}
                                                {{mb_field object=$ictere field=autre_origine_desc}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="4" class="category halfPane">
                                                {{mb_label object=$ictere field=phototherapie}}
                                                {{mb_field object=$ictere field=phototherapie default=""}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Si
                                                oui, {{mb_label object=$ictere field=type_phototherapie}}</th>
                                            <td colspan="2">
                                                {{mb_field object=$ictere field=type_phototherapie
                                                style="width: 20em;" emptyLabel="Ictere.type_phototherapie."}}
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                            <form name="Pathologies-Thermique-{{$naissance->_guid}}" method="post"
                                  onsubmit="return onSubmitFormAjax(this);">
                                {{mb_class object=$regulation_thermique}}
                                {{mb_key   object=$regulation_thermique}}

                                <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                                <input type="hidden" name="_count_changes" value="0"/>
                                <fieldset>
                                    <legend>
                                        {{mb_field object=$regulation_thermique field=trouble_regul_thermique typeEnum=checkbox}}
                                        {{mb_label object=$regulation_thermique field=trouble_regul_thermique}}
                                    </legend>
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$regulation_thermique field=hyperthermie typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$regulation_thermique field=hyperthermie}}</td>
                                            <th
                                              class="narrow">{{mb_field object=$regulation_thermique field=hypothermie_grave typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$regulation_thermique field=hypothermie_grave}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$regulation_thermique field=hypothermie_legere typeEnum=checkbox}}</th>
                                            <td
                                              colspan="3">{{mb_label object=$regulation_thermique field=hypothermie_legere}}</td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                        </td>
                        <td class="halfPane">
                            <form name="Pathologies-Anomalie-{{$naissance->_guid}}" method="post"
                                  onsubmit="return onSubmitFormAjax(this);">
                                {{mb_class object=$anomalie_congenitale}}
                                {{mb_key   object=$anomalie_congenitale}}

                                <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                                <input type="hidden" name="_count_changes" value="0"/>
                                <fieldset>
                                    <legend>
                                        {{mb_field object=$anomalie_congenitale field=anom_cong typeEnum=checkbox}}
                                        {{mb_label object=$anomalie_congenitale field=anom_cong}}
                                    </legend>
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$anomalie_congenitale field=isolee typeEnum=checkbox}}</th>
                                            <td class="thirdPane">
                                                <strong>{{mb_label object=$anomalie_congenitale field=isolee}}</strong>
                                            </td>
                                            <th
                                              class="narrow">{{mb_field object=$anomalie_congenitale field=synd_polyformatif typeEnum=checkbox}}</th>
                                            <td class="thirdPane">
                                                <strong>{{mb_label object=$anomalie_congenitale field=synd_polyformatif}}</strong>
                                            </td>
                                            <td
                                              class="thirdPane">{{mb_label object=$anomalie_congenitale field=description_clair}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=tube_neural typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$anomalie_congenitale field=tube_neural}}</td>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=fente_labio_palatine typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$anomalie_congenitale field=fente_labio_palatine}}</td>
                                            <td
                                              rowspan="20">{{mb_field object=$anomalie_congenitale field=description_clair}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=atresie_oesophage typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$anomalie_congenitale field=atresie_oesophage}}</td>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=omphalocele typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$anomalie_congenitale field=omphalocele}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=reduc_absence_membres typeEnum=checkbox}}</th>
                                            <td
                                              class="compact text">{{mb_label object=$anomalie_congenitale field=reduc_absence_membres}}</td>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=hydrocephalie typeEnum=checkbox}}</th>
                                            <td class="compact">
                                                {{mb_label object=$anomalie_congenitale field=hydrocephalie}}
                                                {{mb_label object=$anomalie_congenitale field=hydrocephalie_type style="display: none;"}}
                                                {{mb_field object=$anomalie_congenitale field=hydrocephalie_type
                                                style="width: 10em;" emptyLabel="AnomalieCongenitale.hydrocephalie_type."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=malform_card typeEnum=checkbox}}</th>
                                            <td class="compact">
                                                {{mb_label object=$anomalie_congenitale field=malform_card}}
                                                {{mb_label object=$anomalie_congenitale field=malform_card_type style="display: none;"}}
                                                {{mb_field object=$anomalie_congenitale field=malform_card_type
                                                style="width: 10em;" emptyLabel="AnomalieCongenitale.malform_card_type."}}
                                            </td>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=malform_reinale typeEnum=checkbox}}</th>
                                            <td class="compact">
                                                {{mb_label object=$anomalie_congenitale field=malform_reinale}}
                                                {{mb_label object=$anomalie_congenitale field=malform_reinale_type style="display: none;"}}
                                                {{mb_field object=$anomalie_congenitale field=malform_reinale_type
                                                style="width: 10em;" emptyLabel="AnomalieCongenitale.malform_reinale_type."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=hanches_luxables typeEnum=checkbox}}</th>
                                            <td class="compact">
                                                {{mb_label object=$anomalie_congenitale field=hanches_luxables}}
                                                {{mb_label object=$anomalie_congenitale field=hanches_luxables_type style="display: none;"}}
                                                {{mb_field object=$anomalie_congenitale field=hanches_luxables_type
                                                style="width: 10em;" emptyLabel="AnomalieCongenitale.hanches_luxables_type."}}
                                            </td>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=autre typeEnum=checkbox}}</th>
                                            <td class="compact">
                                                {{mb_label object=$anomalie_congenitale field=autre}}
                                                {{mb_label object=$anomalie_congenitale field=autre_desc style="display: none;"}}
                                                {{mb_field object=$anomalie_congenitale field=autre_desc}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$anomalie_congenitale field=chromosomique typeEnum=checkbox}}</th>
                                            <td>
                                                <strong>{{mb_label object=$anomalie_congenitale field=chromosomique}}</strong>
                                            </td>
                                            <th
                                              class="narrow">{{mb_field object=$anomalie_congenitale field=genique typeEnum=checkbox}}</th>
                                            <td><strong>{{mb_label object=$anomalie_congenitale field=genique}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=trisomie_21 typeEnum=checkbox}}</th>
                                            <td class="compact">
                                                {{mb_label object=$anomalie_congenitale field=trisomie_21}}
                                                {{mb_label object=$anomalie_congenitale field=trisomie_type style="display: none;"}}
                                                {{mb_field object=$anomalie_congenitale field=trisomie_type
                                                style="width: 10em;" emptyLabel="AnomalieCongenitale.trisomie_type."}}
                                            </td>
                                            <th
                                              class="compact">{{mb_field object=$anomalie_congenitale field=chrom_gen_autre typeEnum=checkbox}}</th>
                                            <td class="compact">
                                                {{mb_label object=$anomalie_congenitale field=chrom_gen_autre}}
                                                {{mb_label object=$anomalie_congenitale field=chrom_gen_autre_desc style="display: none;"}}
                                                {{mb_field object=$anomalie_congenitale field=chrom_gen_autre_desc}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              colspan="2">{{mb_label object=$anomalie_congenitale field=moment_diag}}</th>
                                            <td colspan="2">
                                                {{mb_field object=$anomalie_congenitale field=moment_diag
                                                style="width: 20em;" emptyLabel="AnomalieCongenitale.moment_diag."}}
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                            <form name="Pathologies-Autres-{{$naissance->_guid}}" method="post"
                                  onsubmit="return onSubmitFormAjax(this);">
                                {{mb_class object=$pathologie}}
                                {{mb_key   object=$pathologie}}

                                <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                                <input type="hidden" name="_count_changes" value="0"/>
                                <fieldset>
                                    <legend>
                                        {{mb_field object=$pathologie field=autre_pathologie typeEnum=checkbox}}
                                        {{mb_label object=$pathologie field=autre_pathologie}}
                                    </legend>
                                    <table class="main layout">
                                        <tr>
                                            <td class="thirdPane">
                                                <table class="form">
                                                    <tr>
                                                        <th
                                                          class="narrow">{{mb_field object=$pathologie field=patho_resp typeEnum=checkbox}}</th>
                                                        <td class="text">
                                                            <strong>{{mb_label object=$pathologie field=patho_resp}}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=tachypnee typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=tachypnee}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=autre_detresse_resp_neonat typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=autre_detresse_resp_neonat}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=acces_cyanose typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=acces_cyanose}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=apnees_prema typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=apnees_prema}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=apnees_autre typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=apnees_autre}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=inhalation_meco_sans_pneumopath typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=inhalation_meco_sans_pneumopath}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=inhalation_meco_avec_pneumopath typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=inhalation_meco_avec_pneumopath}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=inhalation_lait typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=inhalation_lait}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow">{{mb_field object=$pathologie field=patho_cardiovasc typeEnum=checkbox}}</th>
                                                        <td class="text">
                                                            <strong>{{mb_label object=$pathologie field=patho_cardiovasc}}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=trouble_du_rythme typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=trouble_du_rythme}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hypertonie_vagale typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hypertonie_vagale}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=souffle_a_explorer typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=souffle_a_explorer}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow">{{mb_field object=$pathologie field=patho_neuro typeEnum=checkbox}}</th>
                                                        <td class="text">
                                                            <strong>{{mb_label object=$pathologie field=patho_neuro}}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hypothonie typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hypothonie}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hypertonie typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hypertonie}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=irrit_cerebrale typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=irrit_cerebrale}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=mouv_anormaux typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=mouv_anormaux}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=convulsions typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=convulsions}}</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="thirdPane">
                                                <table class="form">
                                                    <tr>
                                                        <th
                                                          class="narrow">{{mb_field object=$pathologie field=patho_dig typeEnum=checkbox}}</th>
                                                        <td class="text">
                                                            <strong>{{mb_label object=$pathologie field=patho_dig}}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=alim_sein_difficile typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=alim_sein_difficile}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=alim_lente typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=alim_lente}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=stagnation_pond typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=stagnation_pond}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=perte_poids_sup_10_pourc typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=perte_poids_sup_10_pourc}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=regurgitations typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=regurgitations}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=vomissements typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=vomissements}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=reflux_gatro_eoso typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=reflux_gatro_eoso}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=oesophagite typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=oesophagite}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hematemese typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hematemese}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=synd_occlusif typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=synd_occlusif}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=trouble_succion_deglut typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=trouble_succion_deglut}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow">{{mb_field object=$pathologie field=patho_hemato typeEnum=checkbox}}</th>
                                                        <td class="text">
                                                            <strong>{{mb_label object=$pathologie field=patho_hemato}}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=anemie_neonat typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=anemie_neonat}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=anemie_transf_foeto_mat typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=anemie_transf_foeto_mat}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=anemie_transf_foeto_foet typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=anemie_transf_foeto_foet}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=drepano_positif typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=drepano_positif}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=maladie_hemo typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=maladie_hemo}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=thrombopenie typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=thrombopenie}}</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="thirdPane">
                                                <table class="form">
                                                    <tr>
                                                        <th
                                                          class="narrow">{{mb_field object=$pathologie field=patho_metab typeEnum=checkbox}}</th>
                                                        <td class="text">
                                                            <strong>{{mb_label object=$pathologie field=patho_metab}}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hypogly_diab_mere_gest typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hypogly_diab_mere_gest}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hypogly_diab_mere_nid typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hypogly_diab_mere_nid}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hypogly_diab_mere_id typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hypogly_diab_mere_id}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hypogly_neonat_transitoire typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hypogly_neonat_transitoire}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=hypocalcemie typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=hypocalcemie}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow">{{mb_field object=$pathologie field=intoxication typeEnum=checkbox}}</th>
                                                        <td class="text">
                                                            <strong>{{mb_label object=$pathologie field=intoxication}}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=synd_sevrage_toxico typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=synd_sevrage_toxico}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=synd_sevrage_medic typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=synd_sevrage_medic}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=tabac_maternel typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=tabac_maternel}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=alcool_maternel typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=alcool_maternel}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow">{{mb_field object=$pathologie field=autre_patho_autre typeEnum=checkbox}}</th>
                                                        <td class="text">
                                                            <strong>{{mb_label object=$pathologie field=autre_patho_autre}}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=rhinite_neonat typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=rhinite_neonat}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=patho_dermato typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=patho_dermato}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th
                                                          class="narrow compact">{{mb_field object=$pathologie field=autre_patho_autre_thesaurus typeEnum=checkbox}}</th>
                                                        <td
                                                          class="compact text">{{mb_label object=$pathologie field=autre_patho_autre_thesaurus}}</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="actes_effectues" style="display: none;">
                <form name="Actes-{{$naissance->_guid}}" method="post"
                      onsubmit="return onSubmitFormAjax(this);">
                    {{mb_class object=$acte}}
                    {{mb_key   object=$acte}}

                    <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                    <input type="hidden" name="_count_changes" value="0"/>
                    <fieldset>
                        <legend>
                            {{mb_label object=$acte field=actes_effectues}}
                            {{mb_field object=$acte field=actes_effectues default=""}}
                        </legend>
                        <table class="form">
                            <tr>
                                <th colspan="3">Si oui,</th>
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <th class="narrow">{{mb_field object=$acte field=caryotype typeEnum=checkbox}}</th>
                                <td class="thirdPane">{{mb_label object=$acte field=caryotype}}</td>
                                <th
                                  class="narrow">{{mb_field object=$acte field=echographie_cardiaque typeEnum=checkbox}}</th>
                                <td class="thirdPane">{{mb_label object=$acte field=echographie_cardiaque}}</td>
                                <th class="narrow">{{mb_field object=$acte field=incubateur typeEnum=checkbox}}</th>
                                <td class="thirdPane">{{mb_label object=$acte field=incubateur}}</td>
                            </tr>
                            <tr>
                                <th>{{mb_field object=$acte field=etf typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=etf}}</td>
                                <th>{{mb_field object=$acte field=echographie_cerebrale typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=echographie_cerebrale}}</td>
                                <th>{{mb_field object=$acte field=injection_gamma_globulines typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=injection_gamma_globulines}}</td>
                            </tr>
                            <tr>
                                <th>{{mb_field object=$acte field=eeg typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=eeg}}</td>
                                <th>{{mb_field object=$acte field=echographie_hanche typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=echographie_hanche}}</td>
                                <th>{{mb_field object=$acte field=togd typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=togd}}</td>
                            </tr>
                            <tr>
                                <th>{{mb_field object=$acte field=ecg typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=ecg}}</td>
                                <th>{{mb_field object=$acte field=echographie_hepatique typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=echographie_hepatique}}</td>
                                <th>{{mb_field object=$acte field=radio_thoracique typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=radio_thoracique}}</td>
                            </tr>
                            <tr>
                                <th>{{mb_field object=$acte field=fond_oeil typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=fond_oeil}}</td>
                                <th>{{mb_field object=$acte field=echographie_reinale typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=echographie_reinale}}</td>
                                <th>{{mb_field object=$acte field=reeducation typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=reeducation}}</td>
                            </tr>
                            <tr>
                                <th>{{mb_field object=$acte field=antibiotherapie typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=antibiotherapie}}</td>
                                <th>{{mb_field object=$acte field=exsanguino_transfusion typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=exsanguino_transfusion}}</td>
                                <th rowspan="2"
                                    style="vertical-align: top;">{{mb_field object=$acte field=autre_acte typeEnum=checkbox}}</th>
                                <td rowspan="2">
                                    {{mb_label object=$acte field=autre_acte}}
                                    <br/>
                                    {{mb_label object=$acte field=autre_acte_desc style="display: none;"}}
                                    {{mb_field object=$acte field=autre_acte_desc}}
                                </td>
                            </tr>
                            <tr>
                                <th>{{mb_field object=$acte field=oxygenotherapie typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=oxygenotherapie}}</td>
                                <th>{{mb_field object=$acte field=intubation typeEnum=checkbox}}</th>
                                <td>{{mb_label object=$acte field=intubation}}</td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
            </div>
            <div id="mesures_prophylactiques" style="display: none;">
                <form name="Prophylaxie-{{$naissance->_guid}}" method="post"
                      onsubmit="return onSubmitFormAjax(this);">
                    {{mb_class object=$prophylaxie}}
                    {{mb_key   object=$prophylaxie}}

                    <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                    <input type="hidden" name="_count_changes" value="0"/>
                    <fieldset>
                        <legend>
                            {{mb_label object=$prophylaxie field=mesures_prophylactiques}}
                            {{mb_field object=$prophylaxie field=mesures_prophylactiques default=""}}
                        </legend>
                        <table class="main layout">
                            <tr>
                                <td colspan="3" class="button">Si oui,</td>
                            </tr>
                            <tr>
                                <td class="thirdPane">
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$prophylaxie field=hep_b_injection_immunoglob typeEnum=checkbox}}</th>
                                            <td>
                                                <strong>{{mb_label object=$prophylaxie field=hep_b_injection_immunoglob}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$prophylaxie field=vaccinations typeEnum=checkbox}}</th>
                                            <td><strong>{{mb_label object=$prophylaxie field=vaccinations}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$prophylaxie field=vacc_hep_b typeEnum=checkbox}}</th>
                                            <td class="compact">{{mb_label object=$prophylaxie field=vacc_hep_b}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$prophylaxie field=vacc_hepp_bcg typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$prophylaxie field=vacc_hepp_bcg}}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="thirdPane">
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$prophylaxie field=depistage_sanguin typeEnum=checkbox}}</th>
                                            <td>
                                                <strong>{{mb_label object=$prophylaxie field=depistage_sanguin}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$prophylaxie field=hyperphenylalanemie typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$prophylaxie field=hyperphenylalanemie}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$prophylaxie field=hypothyroidie typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$prophylaxie field=hypothyroidie}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$prophylaxie field=hyperplasie_cong_surrenales typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$prophylaxie field=hyperplasie_cong_surrenales}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$prophylaxie field=drepanocytose typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$prophylaxie field=drepanocytose}}</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_field object=$prophylaxie field=mucoviscidose typeEnum=checkbox}}</th>
                                            <td
                                              class="compact">{{mb_label object=$prophylaxie field=mucoviscidose}}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="thirdPane">
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="narrow">{{mb_field object=$prophylaxie field=test_audition typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$prophylaxie field=test_audition}}</td>
                                            <td>
                                                {{mb_label object=$prophylaxie field=etat_test_audition style="display: none;"}}
                                                {{mb_field object=$prophylaxie field=etat_test_audition
                                                style="width: 20em;" emptyLabel="CGrossesse.etat_test_audition."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$prophylaxie field=supp_vitaminique typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$prophylaxie field=supp_vitaminique}}</td>
                                            <td>
                                                {{mb_label object=$prophylaxie field=supp_vitaminique_desc style="display: none;"}}
                                                {{mb_field object=$prophylaxie field=supp_vitaminique_desc style="width: 20em;"}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_field object=$prophylaxie field=autre_mesure_proph typeEnum=checkbox}}</th>
                                            <td>{{mb_label object=$prophylaxie field=autre_mesure_proph}}</td>
                                            <td>
                                                {{mb_label object=$prophylaxie field=autre_mesure_proph_desc style="display: none;"}}
                                                {{mb_field object=$prophylaxie field=autre_mesure_proph_desc style="width: 20em;"}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
            </div>
            <div id="sortie_enfant" style="display: none;">
                <form name="Sortie-enfant-{{$naissance->_guid}}" method="post"
                      onsubmit="return onSubmitFormAjax(this);">
                    {{mb_class object=$sortie_sejour}}
                    {{mb_key   object=$sortie_sejour}}

                    <input type="hidden" name="naissance_id" value="{{$naissance->_id}}">
                    <input type="hidden" name="_count_changes" value="0"/>
                    <table class="main layout">
                        <tr>
                            <td class="halfPane">
                                <fieldset>
                                    <legend>Sortie</legend>
                                    <table class="main layout">
                                        <tr>
                                            <td id="dossier_mater_infos_sortie">
                                                {{mb_include module=maternite template=inc_dossier_mater_infos_sortie}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="form">
                                                    <tr>
                                                        <th
                                                          class="halfPane">{{mb_label object=$sortie_sejour field=mode_sortie_mater}}</th>
                                                        <td>
                                                            {{mb_field object=$sortie_sejour field=mode_sortie_mater
                                                            style="width: 20em;" emptyLabel="CGrossesse.mode_sortie_mater."}}
                                                            <br/>
                                                            {{mb_label object=$sortie_sejour field=mode_sortie_mater_autre style="display: none;"}}
                                                            {{mb_field object=$sortie_sejour field=mode_sortie_mater_autre style="width: 20em;"}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    <legend>Si transfert ou mutation diff�r�e</legend>
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="halfPane">{{mb_label object=$sortie_sejour field=jour_vie_transmut_mater}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=jour_vie_transmut_mater}} j
                                                {{mb_label object=$sortie_sejour field=heure_vie_transmut_mater style="display: none;"}}
                                                {{mb_field object=$sortie_sejour field=heure_vie_transmut_mater}} h de
                                                vie
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=resp_transmut_mater_id}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=resp_transmut_mater_id style="width: 20em;"
                                                options=$praticiens}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2"
                                                style="vertical-align: top;">{{mb_label object=$sortie_sejour field=motif_transmut_mater}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=motif_transmut_mater
                                                style="width: 20em;" emptyLabel="SortieSejour.motif_transmut_mater."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {{mb_label object=$sortie_sejour field=detail_motif_transmut_mater style="display: none;"}}
                                                {{mb_field object=$sortie_sejour field=detail_motif_transmut_mater}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=lieu_transf_mater}}</th>
                                            <td>{{mb_field object=$sortie_sejour field=lieu_transf_mater}}</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=type_etab_transf_mater}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=type_etab_transf_mater
                                                style="width: 20em;" emptyLabel="SortieSejour.type_etab_transf_mater."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2"
                                                style="vertical-align: top;">{{mb_label object=$sortie_sejour field=dest_transf_mater}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=dest_transf_mater
                                                style="width: 20em;" emptyLabel="SortieSejour.dest_transf_mater."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {{mb_label object=$sortie_sejour field=dest_transf_mater_autre style="display: none;"}}
                                                {{mb_field object=$sortie_sejour field=dest_transf_mater_autre}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=mode_transf_mater}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=mode_transf_mater
                                                style="width: 20em;" emptyLabel="SortieSejour.mode_transf_mater."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=delai_appel_arrivee_transp_mater}}</th>
                                            <td>{{mb_field object=$sortie_sejour field=delai_appel_arrivee_transp_mater}}
                                                min
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=dist_mater_transf_mater}}</th>
                                            <td>{{mb_field object=$sortie_sejour field=dist_mater_transf_mater}} km</td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2"
                                                style="vertical-align: top;">{{mb_label object=$sortie_sejour field=raison_transf_mater_report}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=raison_transf_mater_report
                                                style="width: 20em;" emptyLabel="SortieSejour.raison_transf_mater_report."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {{mb_label object=$sortie_sejour field=raison_transf_report_mater_autre style="display: none;"}}
                                                {{mb_field object=$sortie_sejour field=raison_transf_report_mater_autre}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2"
                                                style="vertical-align: top;">{{mb_label object=$sortie_sejour field=surv_part_sortie_mater}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=surv_part_sortie_mater
                                                style="width: 20em;" emptyLabel="SortieSejour.surv_part_sortie_mater."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {{mb_label object=$sortie_sejour field=surv_part_sortie_mater_desc style="display: none;"}}
                                                {{mb_field object=$sortie_sejour field=surv_part_sortie_mater_desc}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=remarques_transf_mater}}</th>
                                            <td>{{mb_field object=$sortie_sejour field=remarques_transf_mater}}</td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td class="halfPane">
                                <fieldset>
                                    <legend>Alimentation</legend>
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="halfPane">{{mb_label object=$sortie_sejour field=poids_fin_sejour}}</th>
                                            <td>{{mb_field object=$sortie_sejour field=poids_fin_sejour}} grammes</td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=alim_fin_sejour}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=alim_fin_sejour
                                                style="width: 20em;" emptyLabel="SortieSejour.alim_fin_sejour."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=comp_alim_fin_sejour}}</th>
                                            <td>{{mb_field object=$sortie_sejour field=comp_alim_fin_sejour default=""}}</td>
                                        </tr>
                                        <tr>
                                            <th>Si oui,</th>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_label object=$sortie_sejour field=nature_comp_alim_fin_sejour}}</th>
                                            <td class="compact">
                                                {{mb_field object=$sortie_sejour field=nature_comp_alim_fin_sejour
                                                style="width: 20em;" emptyLabel="SortieSejour.nature_comp_alim_fin_sejour."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_label object=$sortie_sejour field=moyen_comp_alim_fin_sejour}}</th>
                                            <td class="compact">
                                                {{mb_field object=$sortie_sejour field=moyen_comp_alim_fin_sejour
                                                style="width: 20em;" emptyLabel="SortieSejour.moyen_comp_alim_fin_sejour."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_label object=$sortie_sejour field=indic_comp_alim_fin_sejour}}</th>
                                            <td class="compact">
                                                {{mb_field object=$sortie_sejour field=indic_comp_alim_fin_sejour
                                                style="width: 20em;" emptyLabel="SortieSejour.indic_comp_alim_fin_sejour."}}
                                                <br/>
                                                {{mb_label object=$sortie_sejour field=indic_comp_alim_fin_sejour_desc style="display: none;"}}
                                                {{mb_field object=$sortie_sejour field=indic_comp_alim_fin_sejour_desc}}
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    <legend>
                                        {{mb_label object=$sortie_sejour field=retour_mater}}
                                        {{mb_field object=$sortie_sejour field=retour_mater default=""}}
                                    </legend>
                                    <table class="form">
                                        <tr>
                                            <td colspan="2">(si transfert ou mutation imm�dait ou diff�r�)</td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="halfPane">{{mb_label object=$sortie_sejour field=date_retour_mater}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=date_retour_mater form="Sortie-enfant-`$naissance->_guid`" register=true}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>{{mb_label object=$sortie_sejour field=duree_transfert}}</th>
                                            <td>{{mb_field object=$sortie_sejour field=duree_transfert}}</td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    <legend>D�c�s (synth�se)</legend>
                                    <table class="form">
                                        <tr>
                                            <th
                                              class="halfPane">{{mb_label object=$sortie_sejour field=moment_deces}}</th>
                                            <td>
                                                {{mb_field object=$sortie_sejour field=moment_deces
                                                style="width: 20em;" emptyLabel="SortieSejour.moment_deces."}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Si oui,</th>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="compact">{{mb_label object=$sortie_sejour field=date_deces}}</th>
                                            <td class="compact">
                                                {{mb_field object=$sortie_sejour field=date_deces form="Sortie-enfant-`$naissance->_guid`" register=true}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_label object=$sortie_sejour field=age_deces_jours}}</th>
                                            <td class="compact">{{mb_field object=$sortie_sejour field=age_deces_jours}}
                                                jours
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_label object=$sortie_sejour field=age_deces_heures}}</th>
                                            <td
                                              class="compact">{{mb_field object=$sortie_sejour field=age_deces_heures}}
                                                heures
                                            </td>
                                        </tr>
                                        <tr>
                                            <th
                                              class="compact">{{mb_label object=$sortie_sejour field=cause_deces}}</th>
                                            <td class="compact">
                                                {{mb_field object=$sortie_sejour field=cause_deces
                                                style="width: 20em;" emptyLabel="SortieSejour.cause_deces."}}
                                                <br/>
                                                {{mb_label object=$sortie_sejour field=cause_deces_desc style="display: none;"}}
                                                {{mb_field object=$sortie_sejour field=cause_deces_desc style="width: 20em;"}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="compact">{{mb_label object=$sortie_sejour field=autopsie}}</th>
                                            <td class="compact">
                                                {{mb_field object=$sortie_sejour field=autopsie
                                                style="width: 20em;" emptyLabel="SortieSejour.autopsie."}}</td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </td>
    </tr>
</table>
