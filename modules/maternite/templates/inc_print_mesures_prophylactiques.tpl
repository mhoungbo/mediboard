{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=prophylaxie value=$_naissance->_ref_prophylaxie}}

<table class="tbl">
    <tr>
        <th colspan="2">{{tr}}Dossier-nouveau-ne_Prophylactic measures{{/tr}}</th>
    </tr>
    {{if $prophylaxie->prophy_vit_k != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_vit_k}}</th>
            <td>
                {{mb_value object=$prophylaxie field=prophy_vit_k}}
                - {{mb_value object=$prophylaxie field=prophy_vit_k_type}}
            </td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_desinfect_occulaire != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_desinfect_occulaire}}</th>
            <td>{{mb_value object=$prophylaxie field=prophy_desinfect_occulaire}}</td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_asp_naso_phar != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_asp_naso_phar}}</th>
            <td>{{mb_value object=$prophylaxie field=prophy_asp_naso_phar}}</td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_perm_choanes != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_perm_choanes}}</th>
            <td>{{mb_value object=$prophylaxie field=prophy_perm_choanes}}</td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_perm_oeso != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_perm_oeso}}</th>
            <td>{{mb_value object=$prophylaxie field=prophy_perm_oeso}}</td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_perm_anale != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_perm_anale}}</th>
            <td>{{mb_value object=$prophylaxie field=prophy_perm_anale}}</td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_emission_urine != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_emission_urine}}</th>
            <td>{{mb_value object=$prophylaxie field=prophy_emission_urine}}</td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_emission_meconium != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_emission_meconium}}</th>
            <td>{{mb_value object=$prophylaxie field=prophy_emission_meconium}}</td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_autre != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_autre}}</th>
            <td>
                {{mb_value object=$prophylaxie field=prophy_autre}}
                - {{mb_value object=$prophylaxie field=prophy_autre_desc}}
            </td>
        </tr>
    {{/if}}
    {{if $prophylaxie->prophy_remarques != null}}
        <tr>
            <th class="narrow">{{mb_label object=$prophylaxie field=prophy_remarques}}</th>
            <td>{{mb_value object=$prophylaxie field=prophy_remarques}}</td>
        </tr>
    {{/if}}
</table>
