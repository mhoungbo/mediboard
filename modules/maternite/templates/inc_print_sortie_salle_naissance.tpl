{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=sortie_salle value=$_naissance->_ref_sortie_salle}}

<table class="tbl">
    <tr>
        <th colspan="2" class="narrow">{{tr}}Dossier-nouveau-ne_Leaving the birth room{{/tr}}</th>
    </tr>
    {{if $sortie_salle->mode_sortie}}
        <tr>
            <th class="narrow">{{mb_label object=$sortie_salle field=mode_sortie}}</th>
            <td>
                {{mb_value object=$sortie_salle field=mode_sortie}}
                {{if $sortie_salle->mode_sortie_autre}}
                    - {{mb_value object=$sortie_salle field=mode_sortie_autre}}
                {{/if}}
            </td>
        </tr>
    {{/if}}
    {{if in_array($sortie_salle->mode_sortie, array("mut", "transfres", "transfhres"))}}
        <tr>
            <th>{{tr}}Dossier-nouveau-ne_If immediate transfer or mutation{{/tr}}</th>
            <td>
                <table class="tbl">
                    {{if $sortie_salle->min_vie_transmut}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=min_vie_transmut}}</th>
                            <td>{{mb_value object=$sortie_salle field=min_vie_transmut}}</td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->resp_transmut_id}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=resp_transmut_id}}</th>
                            <td>{{mb_value object=$sortie_salle field=resp_transmut_id}}</td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->motif_transmut}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=motif_transmut}}</th>
                            <td>
                                {{mb_value object=$sortie_salle field=motif_transmut}}
                                {{if $sortie_salle->detail_motif_transmut}}
                                    - {{mb_value object=$sortie_salle field=detail_motif_transmut}}
                                {{/if}}
                            </td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->lieu_transf}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=lieu_transf}}</th>
                            <td>{{mb_value object=$sortie_salle field=lieu_transf}}</td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->type_etab_transf}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=type_etab_transf}}</th>
                            <td>{{mb_value object=$sortie_salle field=type_etab_transf}}</td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->dest_transf}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=dest_transf}}</th>
                            <td>
                                {{mb_value object=$sortie_salle field=dest_transf}}
                                {{if $sortie_salle->dest_transf_autre}}
                                    - {{mb_value object=$sortie_salle field=dest_transf_autre}}
                                {{/if}}
                            </td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->mode_transf}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=mode_transf}}</th>
                            <td>{{mb_value object=$sortie_salle field=mode_transf}}</td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->delai_appel_arrivee_transp}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=delai_appel_arrivee_transp}}</th>
                            <td>{{mb_value object=$sortie_salle field=delai_appel_arrivee_transp}} min</td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->dist_mater_transf}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=dist_mater_transf}}</th>
                            <td>{{mb_value object=$sortie_salle field=dist_mater_transf}} km</td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->raison_transf_report}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=raison_transf_report}}</th>
                            <td>
                                {{mb_value object=$sortie_salle field=raison_transf_report}}
                                {{if $sortie_salle->raison_transf_report_autre}}
                                    - {{mb_value object=$sortie_salle field=raison_transf_report_autre}}
                                {{/if}}
                            </td>
                        </tr>
                    {{/if}}
                    {{if $sortie_salle->remarques_transf}}
                        <tr>
                            <th class="narrow">{{mb_label object=$sortie_salle field=remarques_transf}}</th>
                            <td>{{mb_value object=$sortie_salle field=remarques_transf}}</td>
                        </tr>
                    {{/if}}
                </table>
            </td>
        </tr>
    {{/if}}
</table>
