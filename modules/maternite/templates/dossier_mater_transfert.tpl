{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=dossier value=$grossesse->_ref_dossier_perinat}}
{{assign var=patient value=$grossesse->_ref_parturiente}}

<script>
  listForms = [
    getForm("Transfert-{{$dossier->_guid}}")
  ];

  includeForms = function () {
    DossierMater.listForms = listForms.clone();
  };

  submitAllForms = function (callBack) {
    includeForms();
    DossierMater.submitAllForms(callBack);
  };

  Main.add(function () {
    {{if !$print}}
    includeForms();
    DossierMater.prepareAllForms();
    {{/if}}
  });
</script>

{{mb_include module=maternite template=inc_dossier_mater_header}}

<form name="Transfert-{{$dossier->_guid}}" method="post"
      onsubmit="return onSubmitFormAjax(this);">
  {{mb_class object=$dossier}}
  {{mb_key   object=$dossier}}
  <input type="hidden" name="_count_changes" value="0" />
  <input type="hidden" name="grossesse_id" value="{{$grossesse->_id}}" />

  <table class="main">
    <tr>
      <td colspan="2">
        <fieldset>
          <legend>
            Transfert *
          </legend>
          <table class="main layout">
            <tr>
              <td colspan="2">
                <table class="form me-no-align me-no-box-shadow me-small-form">
                  <tr>
                    <td colspan="2">*NB
                      : {{tr}}CGrossesse-msg-Patient hospitalized in an institution and transferred to another institution|f{{/tr}}</td>
                  </tr>
                  <tr>
                    <th class="halfPane">{{mb_label object=$dossier field=_transf_antenat}}</th>
                    <td>
                      {{mb_field object=$dossier field=_transf_antenat
                      style="width: 20em;" emptyLabel="CGrossesse._transf_antenat."}}
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="halfPane">
                <table class="form me-no-box-shadow me-no-align">
                  <tr>
                    <th class="halfPane">{{mb_label object=$dossier field=_date_transf_antenat}}</th>
                    <td>{{mb_field object=$dossier field=_date_transf_antenat form="Transfert-`$dossier->_guid`" register=true}}</td>
                  </tr>
                  <tr>
                    <th>{{mb_label object=$dossier field=_lieu_transf_antenat}}</th>
                    <td>
                      {{mb_field object=$dossier field=_lieu_transf_antenat
                      style="width: 20em;" emptyLabel="CGrossesse._lieu_transf_antenat."}}
                    </td>
                  </tr>
                  <tr>
                    <th>{{mb_label object=$dossier field=_etab_transf_antenat}}</th>
                    <td>{{mb_field object=$dossier field=_etab_transf_antenat style="width: 20em;"}}</td>
                  </tr>
                  <tr>
                    <th>{{mb_label object=$dossier field=_nivsoins_transf_antenat}}</th>
                    <td>
                      {{mb_field object=$dossier field=_nivsoins_transf_antenat
                      style="width: 20em;" emptyLabel="CGrossesse._nivsoins_transf_antenat."}}
                    </td>
                  </tr>
                  <tr>
                    <th>{{mb_label object=$dossier field=_raison_transf_antenat_hors_reseau}}</th>
                    <td>
                      {{mb_field object=$dossier field=_raison_transf_antenat_hors_reseau
                      style="width: 20em;" emptyLabel="CGrossesse._raison_transf_antenat_hors_reseau."}}
                    </td>
                  </tr>
                  <tr>
                    <th>{{mb_label object=$dossier field=_raison_imp_transf_antenat}}</th>
                    <td>
                      {{mb_field object=$dossier field=_raison_imp_transf_antenat
                      style="width: 20em;" emptyLabel="CGrossesse._raison_imp_transf_antenat."}}
                    </td>
                  </tr>
                </table>
              </td>
              <td class="halfPane">
                <table class="form me-no-align me-no-box-shadow">
                  <tr>
                    <th class="halfPane">{{mb_label object=$dossier field=_motif_tranf_antenat}}</th>
                    <td>
                      {{mb_field object=$dossier field=_motif_tranf_antenat
                      style="width: 20em;" emptyLabel="CGrossesse._motif_tranf_antenat."}}
                    </td>
                  </tr>
                  <tr>
                    <th>{{mb_label object=$dossier field=_type_patho_transf_antenat}}</th>
                    <td>
                      {{mb_field object=$dossier field=_type_patho_transf_antenat
                      style="width: 20em;" emptyLabel="CGrossesse._type_patho_transf_antenat."}}
                    </td>
                  </tr>
                  <tr>
                    <th>{{mb_label object=$dossier field=_rques_transf_antenat}}</th>
                    <td>
                      {{if !$print}}
                        {{mb_field object=$dossier field=_rques_transf_antenat form=Transfert-`$dossier->_guid`}}
                      {{else}}
                        {{mb_value object=$dossier field=_rques_transf_antenat}}
                      {{/if}}
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </fieldset>
      </td>
    </tr>
    <tr>
      <td class="halfPane">
        <fieldset>
          <legend>
            Conditions du transfert
          </legend>
          <table class="form me-no-align me-no-box-shadow me-small-form">
            <tr>
              <th class="halfPane">{{mb_label object=$dossier field=_mode_transp_transf_antenat}}</th>
              <td colspan="2">
                {{mb_field object=$dossier field=_mode_transp_transf_antenat
                style="width: 20em;" emptyLabel="CGrossesse._mode_transp_transf_antenat."}}
              </td>
            </tr>
            <tr>
              <th>{{mb_field object=$dossier field=_antibio_transf_antenat typeEnum=checkbox}}</th>
              <td class="narrow">{{mb_label object=$dossier field=_antibio_transf_antenat}}</td>
              <td>{{mb_field object=$dossier field=_nom_antibio_transf_antenat}}</td>
            </tr>
            <tr>
              <th>{{mb_field object=$dossier field=_cortico_transf_antenat typeEnum=checkbox}}</th>
              <td>{{mb_label object=$dossier field=_cortico_transf_antenat}}</td>
              <td>{{mb_field object=$dossier field=_nom_cortico_transf_antenat}}</td>
            </tr>
            <tr>
              <th>{{mb_label object=$dossier field=_datetime_cortico_transf_antenat}}</th>
              <td colspan="2">
                {{mb_field object=$dossier field=_datetime_cortico_transf_antenat form="Transfert-`$dossier->_guid`" register=true}}
              </td>
            </tr>
            <tr>
              <th>{{mb_field object=$dossier field=_tocolytiques_transf_antenat typeEnum=checkbox}}</th>
              <td>{{mb_label object=$dossier field=_tocolytiques_transf_antenat}}</td>
              <td>{{mb_field object=$dossier field=_nom_tocolytiques_transf_antenat}}</td>
            </tr>
            <tr>
              <th>{{mb_field object=$dossier field=_antihta_transf_antenat typeEnum=checkbox}}</th>
              <td>{{mb_label object=$dossier field=_antihta_transf_antenat}}</td>
              <td>{{mb_field object=$dossier field=_nom_antihta_transf_antenat}}</td>
            </tr>
            <tr>
              <th>{{mb_field object=$dossier field=_autre_ttt_transf_antenat typeEnum=checkbox}}</th>
              <td>{{mb_label object=$dossier field=_autre_ttt_transf_antenat}}</td>
              <td>{{mb_field object=$dossier field=_nom_autre_ttt_transf_antenat}}</td>
            </tr>
          </table>
        </fieldset>
      </td>
      <td class="halfPane">
        <fieldset>
          <legend>
            Retour � la maternit� d'origine
          </legend>
          <table class="form me-no-align me-no-box-shadow me-small-form">
            <tr>
              <th class="halfPane">{{mb_label object=$dossier field=_retour_mater_transf_antenat}}</th>
              <td>
                {{mb_field object=$dossier field=_retour_mater_transf_antenat
                style="width: 20em;" emptyLabel="CGrossesse._retour_mater_transf_antenat."}}
              </td>
            </tr>
            <tr>
              <th>{{mb_label object=$dossier field=_date_retour_transf_antenat}}</th>
              <td>{{mb_field object=$dossier field=_date_retour_transf_antenat form="Transfert-`$dossier->_guid`" register=true}}</td>
            </tr>
            <tr>
              <th>{{mb_label object=$dossier field=_devenir_retour_transf_antenat}}</th>
              <td>
                {{mb_field object=$dossier field=_devenir_retour_transf_antenat
                style="width: 20em;" emptyLabel="CGrossesse._devenir_retour_transf_antenat."}}
              </td>
            </tr>
            <tr>
              <th>{{mb_label object=$dossier field=_rques_retour_transf_antenat}}</th>
              <td>
                {{if !$print}}
                  {{mb_field object=$dossier field=_rques_retour_transf_antenat form=Transfert-`$dossier->_guid`}}
                {{else}}
                  {{mb_value object=$dossier field=_rques_retour_transf_antenat}}
                {{/if}}
              </td>
            </tr>
          </table>
        </fieldset>
      </td>
    </tr>
  </table>
</form>
