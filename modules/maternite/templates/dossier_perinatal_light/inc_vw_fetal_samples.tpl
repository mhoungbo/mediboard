{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=css value=""}}

<table class="main" style="{{$css}}">
  <tr>
    <th class="title_h6" colspan="6">
      <span class="title_h6_spacing">
        {{tr}}CDossierPerinat-action-Fetal samples{{/tr}}
      </span>
      <span style="float: right">
          <a href="#toxics"
             onclick="DossierMater.selectedMenu($('menu_toxics').down('a'));">
            <i class="fas fa-chevron-up actif_arrow"></i>
          </a>
          <a href="#ultrasounds"
             onclick="DossierMater.selectedMenu($('menu_ultrasounds').down('a'));">
            <i class="fas fa-chevron-down actif_arrow"></i>
          </a>
        </span>
    </th>
  </tr>
  <tr>
    {{me_form_field animated=false nb_cells=2 mb_object=$dossier mb_field=_indication_prelevements_foetaux class="card_input"}}
    {{mb_field object=$dossier field=_indication_prelevements_foetaux emptyLabel="common-Not specified" onchange="DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_field}}
  </tr>
  <tr>
    {{me_form_bool animated=false nb_cells=2 mb_object=$dossier mb_field=_biopsie_trophoblaste class="card_input"}}
    {{mb_field object=$dossier field=_biopsie_trophoblaste onchange="DossierMater.ShowElements(this, '_biopsie_trophoblaste'); DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_bool}}
  </tr>
  <tr id="_biopsie_trophoblaste" style="{{if !$dossier->_biopsie_trophoblaste}}display: none;{{/if}}">
    {{me_form_field animated=false nb_cells=2 mb_object=$dossier mb_field=_resultat_biopsie_trophoblaste class="card_input"}}
    {{mb_field object=$dossier field=_resultat_biopsie_trophoblaste onchange="DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_field}}

    {{me_form_field animated=false nb_cells=2 mb_object=$dossier mb_field=_rques_biopsie_trophoblaste class="card_input"}}
    {{mb_field object=$dossier field=_rques_biopsie_trophoblaste onchange="DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_field}}
  </tr>
  <tr>
    {{me_form_bool animated=false nb_cells=2 mb_object=$dossier mb_field=_amniocentese class="card_input"}}
    {{mb_field object=$dossier field=_amniocentese onchange="DossierMater.ShowElements(this, '_amniocentese'); DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_bool}}
  </tr>
  <tr id="_amniocentese" style="{{if !$dossier->_amniocentese}}display: none;{{/if}}">
    {{me_form_field animated=false nb_cells=2 mb_object=$dossier mb_field=_resultat_amniocentese class="card_input"}}
    {{mb_field object=$dossier field=_resultat_amniocentese onchange="DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_field}}

    {{me_form_field animated=false nb_cells=2 mb_object=$dossier mb_field=_rques_amniocentese class="card_input"}}
    {{mb_field object=$dossier field=_rques_amniocentese onchange="DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_field}}
  </tr>
  <tr>
    {{me_form_bool animated=false nb_cells=2 mb_object=$dossier mb_field=_cordocentese class="card_input"}}
    {{mb_field object=$dossier field=_cordocentese onchange="DossierMater.ShowElements(this, '_cordocentese'); DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_bool}}
  </tr>
  <tr id="_cordocentese" style="{{if !$dossier->_cordocentese}}display: none;{{/if}}">
    {{me_form_field animated=false nb_cells=2 mb_object=$dossier mb_field=_resultat_cordocentese class="card_input"}}
    {{mb_field object=$dossier field=_resultat_cordocentese onchange="DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_field}}

    {{me_form_field animated=false nb_cells=2 mb_object=$dossier mb_field=_rques_cordocentese class="card_input"}}
    {{mb_field object=$dossier field=_rques_cordocentese onchange="DossierMater.bindingDatas(this, getForm('edit_folder_light'));"}}
    {{/me_form_field}}
  </tr>
</table>
