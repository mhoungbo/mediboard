{{*
 * @package Mediboard\maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<tr>
  <th class="category" colspan="4">{{tr}}CDossierPerinat-Toxic products{{/tr}}</th>
</tr>
<tr>
  <th>{{mb_label object=$dossier_perinatal field=_tabac_avant_grossesse}}</th>
  <td>
      <span {{if $dossier_perinatal->_tabac_avant_grossesse}}style="color: darkred;"{{/if}}>
        {{mb_value object=$dossier_perinatal field=_tabac_avant_grossesse}}
      </span>
      {{if $dossier_perinatal->_tabac_avant_grossesse}}
        : {{mb_value object=$dossier_perinatal field=_qte_tabac_avant_grossesse}} {{tr}}CDossierPerinat-_qte_tabac_avant_grossesse_unite{{/tr}}
      {{/if}}
  </td>
</tr>
<tr>
  <th>{{mb_label object=$dossier_perinatal field=_tabac_debut_grossesse}}</th>
  <td>
      <span {{if $dossier_perinatal->_tabac_debut_grossesse}}style="color: darkred;"{{/if}}>
        {{mb_value object=$dossier_perinatal field=_tabac_debut_grossesse}}
      </span>
      {{if $dossier_perinatal->_tabac_debut_grossesse}}
        : {{mb_value object=$dossier_perinatal field=_qte_tabac_debut_grossesse}} {{tr}}CDossierPerinat-_qte_tabac_avant_grossesse_unite{{/tr}}
      {{/if}}
  </td>
</tr>
<tr>
  <th>{{mb_label object=$dossier_perinatal field=_alcool_debut_grossesse}}</th>
  <td>
      <span {{if $dossier_perinatal->_alcool_debut_grossesse}}style="color: darkred;"{{/if}}>
        {{mb_value object=$dossier_perinatal field=_alcool_debut_grossesse}}
      </span>
      {{if $dossier_perinatal->_alcool_debut_grossesse}}
        : {{mb_value object=$dossier_perinatal field=_qte_alcool_debut_grossesse}} {{tr}}CDossierPerinat-_qte_alcool_debut_grossesse_unite{{/tr}}
      {{/if}}
  </td>
</tr>
<tr>
  <th>{{mb_label object=$dossier_perinatal field=_canabis_debut_grossesse}}</th>
  <td>
      <span {{if $dossier_perinatal->_canabis_debut_grossesse}}style="color: darkred;"{{/if}}>
        {{mb_value object=$dossier_perinatal field=_canabis_debut_grossesse}}
      </span>
      {{if $dossier_perinatal->_canabis_debut_grossesse}}
        : {{mb_value object=$dossier_perinatal field=_qte_canabis_debut_grossesse}} {{tr}}CDossierPerinat-_qte_canabis_debut_grossesse_unite{{/tr}}
      {{/if}}
  </td>
</tr>
<tr>
  <th>{{mb_label object=$dossier_perinatal field=_subst_avant_grossesse}}</th>
  <td>
      <span {{if $dossier_perinatal->_subst_avant_grossesse}}style="color: darkred;"{{/if}}>
        {{mb_value object=$dossier_perinatal field=_subst_avant_grossesse}}
      </span>
      {{if $dossier_perinatal->_subst_avant_grossesse}}
        <br/>
        &mdash; {{tr}}CDossierPerinat-_mode_subst_avant_grossesse-court{{/tr}} :
          {{mb_value object=$dossier_perinatal field=_mode_subst_avant_grossesse}}
        <br/>
        &mdash; {{mb_label object=$dossier_perinatal field=_nom_subst_avant_grossesse}} :
          {{mb_value object=$dossier_perinatal field=_nom_subst_avant_grossesse}}

          {{if $dossier_perinatal->_subst_subst_avant_grossesse}}
            <br/>
            &mdash; {{mb_label object=$dossier_perinatal field=_subst_subst_avant_grossesse}} :
              {{mb_value object=$dossier_perinatal field=_subst_subst_avant_grossesse}}
          {{/if}}
      {{/if}}
  </td>
</tr>
<tr>
  <th>{{mb_label object=$dossier_perinatal field=_subst_debut_grossesse}}</th>
  <td>
      <span {{if $dossier_perinatal->_subst_debut_grossesse}}style="color: darkred;"{{/if}}>
        {{mb_value object=$dossier_perinatal field=_subst_debut_grossesse}}
      </span>
  </td>
</tr>

