{{*
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=etat_naissance value=$_naissance->_ref_etat}}

<table class="tbl">
    <tr>
        <th colspan="2">{{tr}}Dossier-nouveau-ne_State birth{{/tr}}</th>
    </tr>
    <tr>
        <td>{{tr}}Dossier-nouveau-ne_Score apgar{{/tr}}</td>
        <td>
            <table class="tbl">
                <tr>
                    <th></th>
                    <th class="category" style="width: 10em;">0</th>
                    <th class="category" style="width: 10em;">1</th>
                    <th class="category" style="width: 10em;">2</th>
                    <th class="category" style="width: 5em;"><strong>1 min</strong></th>
                    <th class="category" style="width: 5em;">3 min</th>
                    <th class="category" style="width: 5em;"><strong>5 min</strong></th>
                    <th class="category" style="width: 5em;">10 min</th>
                </tr>
                <tr>
                    <th><strong>{{mb_label object=$etat_naissance field=apgar_coeur_1}}</strong></th>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Absent{{/tr}} (< 60/min)</td>
                    <td class="text">< 100/min</td>
                    <td class="text">> 100/min</td>
                    <td
                      class="button">{{mb_value object=$etat_naissance field=apgar_coeur_1}}</td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_coeur_3 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_coeur_3}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_coeur_5 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_coeur_5}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_coeur_10 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_coeur_10}}
                    </td>
                </tr>
                <tr>
                    <th><strong>{{mb_label object=$etat_naissance field=apgar_respi_1}}</strong></th>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Absent|f{{/tr}}</td>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Hypoventilation{{/tr}}
                        <br/>{{tr}}Dossier-nouveau-ne_Weak cry{{/tr}}</td>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Good|f{{/tr}}
                        <br/>{{tr}}Dossier-nouveau-ne_Vigorous cry{{/tr}}</td>
                    <td
                      class="button">{{mb_value object=$etat_naissance field=apgar_respi_1}}</td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_respi_3 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_respi_3}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_respi_5 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_respi_5}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_respi_10 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_respi_10}}
                    </td>
                </tr>
                <tr>
                    <th><strong>{{mb_label object=$etat_naissance field=apgar_tonus_1}}</strong></th>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Flask{{/tr}}</td>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Slight bending extremities{{/tr}}</td>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Good{{/tr}}</td>
                    <td
                      class="button">{{mb_value object=$etat_naissance field=apgar_tonus_1}}</td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_tonus_3 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_tonus_3}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_tonus_5 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_tonus_5}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_tonus_10 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_tonus_10}}
                    </td>
                </tr>
                <tr>
                    <th><strong>{{mb_label object=$etat_naissance field=apgar_reflexes_1}}</strong></th>
                    <td class="text">{{tr}}Dossier-nouveau-ne_No answer{{/tr}}</td>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Slight movements{{/tr}}</td>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Shout{{/tr}}</td>
                    <td
                      class="button">{{mb_value object=$etat_naissance field=apgar_reflexes_1}}</td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_reflexes_3 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_reflexes_3}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_reflexes_5 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_reflexes_5}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_reflexes_10 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_reflexes_10}}
                    </td>
                </tr>
                <tr>
                    <th><strong>{{mb_label object=$etat_naissance field=apgar_coloration_1}}</strong></th>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Blue or white{{/tr}}</td>
                    <td class="text">{{tr}}Dossier-nouveau-ne_Pink body cyanotic tips{{/tr}}</td>
                    <td class="text">{{tr}}Dossier-nouveau-ne_All pink{{/tr}}</td>
                    <td
                      class="button">{{mb_value object=$etat_naissance field=apgar_coloration_1}}</td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_coloration_3 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_coloration_3}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_coloration_5 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_coloration_5}}
                    </td>
                    <td class="button">
                        {{mb_label object=$etat_naissance field=apgar_coloration_10 style="display: none;"}}
                        {{mb_value object=$etat_naissance field=apgar_coloration_10}}
                    </td>
                </tr>
                <tr>
                    <th colspan="4"><strong>{{mb_label object=$etat_naissance field=_apgar_1}}</strong></th>
                    <td class="button" id="apgar_value_1">{{mb_value object=$etat_naissance field=_apgar_1}}</td>
                    <td class="button" id="apgar_value_3">{{mb_value object=$etat_naissance field=_apgar_3}}</td>
                    <td class="button" id="apgar_value_5">{{mb_value object=$etat_naissance field=_apgar_5}}</td>
                    <td class="button" id="apgar_value_10">{{mb_value object=$etat_naissance field=_apgar_10}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>{{tr}}Dossier-nouveau-ne_Acid base balance{{/tr}}</td>
        <td>
            <table class="tbl">
                {{if $etat_naissance->ph_ao}}
                    <tr>
                        <th class="narrow">
                            {{mb_label object=$etat_naissance field=ph_ao}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=ph_ao}} AO
                        </td>
                    </tr>
                {{/if}}
                {{if $etat_naissance->ph_v}}
                    <tr>
                        <th>
                            {{mb_label object=$etat_naissance field=ph_v}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=ph_v}} V
                        </td>
                    </tr>
                {{/if}}
                {{if $etat_naissance->base_deficit}}
                    <tr>
                        <th>
                            {{mb_label object=$etat_naissance field=base_deficit}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=base_deficit}}
                        </td>
                    </tr>
                {{/if}}
                {{if $etat_naissance->pco2}}
                    <tr>
                        <th>
                            {{mb_label object=$etat_naissance field=pco2}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=pco2}} mm Hg
                        </td>
                    </tr>
                {{/if}}
                {{if $etat_naissance->lactates}}
                    <tr>
                        <th>
                            {{mb_label object=$etat_naissance field=lactates}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=lactates}}
                        </td>
                    </tr>
                {{/if}}
                {{if $etat_naissance->nouveau_ne_endormi}}
                    <tr>
                        <th>
                            {{mb_label object=$etat_naissance field=nouveau_ne_endormi}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=nouveau_ne_endormi}}
                        </td>
                    </tr>
                {{/if}}
                {{if $etat_naissance->accueil_peau_a_peau}}
                    <tr>
                        <th>
                            {{mb_label object=$etat_naissance field=accueil_peau_a_peau}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=accueil_peau_a_peau}}
                        </td>
                    </tr>
                {{/if}}
                {{if $etat_naissance->debut_allait_salle_naissance}}
                    <tr>
                        <th>
                            {{mb_label object=$etat_naissance field=debut_allait_salle_naissance}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=debut_allait_salle_naissance}}
                        </td>
                    </tr>
                {{/if}}
                {{if $etat_naissance->temp_salle_naissance}}
                    <tr>
                        <th>
                            {{mb_label object=$etat_naissance field=temp_salle_naissance}}
                        </th>
                        <td>
                            {{mb_value object=$etat_naissance field=temp_salle_naissance}}�
                        </td>
                    </tr>
                {{/if}}
            </table>
        </td>
    </tr>
</table>
