<?php
/**
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Mediboard\Maternite\CGrossesse;
use Ox\Mediboard\Maternite\CNaissanceRea;

CCanDo::checkEdit();

$grossesse_id = CView::get("grossesse_id", "ref class|CGrossesse");
$print        = CView::get("print", "bool default|0");

CView::checkin();

$grossesse = new CGrossesse();
$grossesse->load($grossesse_id);
$dossier    = $grossesse->loadRefDossierPerinat();
$dossier->loadComponents();
$naissances = $grossesse->loadRefsNaissances();
$patient    = $grossesse->loadRefParturiente();

$grossesse->loadRefsSejours();
$grossesse->countBackRefs("naissances");

$sejours_enfants = CStoredObject::massLoadFwdRef($naissances, "sejour_enfant_id");
CStoredObject::massLoadFwdRef($sejours_enfants, "patient_id");
$resuscitators = CStoredObject::massLoadBackRefs($naissances, "naissances_rea");
CStoredObject::massLoadFwdRef($resuscitators, "rea_par_id");
CStoredObject::massLoadFwdRef($naissances, "nouveau_ne_constantes_id");
foreach ($naissances as $_naissance) {
    $_naissance->loadRefSejourEnfant()->loadRefPatient()->updateNomPaysInsee();
    $_naissance->loadRefConstantesNouveauNe();
    $_naissance->loadRefEtat();
    $_naissance->loadRefReanimation();
    $_naissance->loadRefProphylaxie();
    $_naissance->loadRefMesurePreventive();
    $_naissance->loadRefSortieSalle();

    /** @var CNaissanceRea[] $resuscitators */
    $resuscitators = $_naissance->loadRefsResuscitators();
    foreach ($resuscitators as $_resuscitator) {
        $_resuscitator->loadRefReaPar();
    }
}

$smarty = new CSmartyDP();
$smarty->assign("grossesse", $grossesse);
$smarty->assign("print", $print);
$smarty->display("dossier_mater_nouveau_ne_salle_naissance.tpl");
