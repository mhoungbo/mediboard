<?php
/**
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CMbString;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Maternite\Naissance\Ictere;
use Ox\Mediboard\Maternite\Naissance\Lesion;
use Ox\Mediboard\Maternite\Naissance\Monitorage;
use Ox\Mediboard\Maternite\Naissance\ProphylaxieSejour;
use Ox\Mediboard\Maternite\Naissance\ActeMedical;
use Ox\Mediboard\Maternite\Naissance\AnomalieCongenitale;
use Ox\Mediboard\Maternite\Naissance\EtatNaissance;
use Ox\Mediboard\Maternite\Naissance\Infection;
use Ox\Mediboard\Maternite\Naissance\MesurePreventive;
use Ox\Mediboard\Maternite\Naissance\Pathologie;
use Ox\Mediboard\Maternite\Naissance\Prophylaxie;
use Ox\Mediboard\Maternite\Naissance\Reanimation;
use Ox\Mediboard\Maternite\Naissance\RegulationThermique;
use Ox\Mediboard\Maternite\Naissance\SortieSalle;
use Ox\Mediboard\Maternite\Naissance\SortieSejour;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\IGroupRelated;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Prescription\CPrescriptionLineElement;

/**
 * Gestion des dossiers de naissance associ�s aux grossesses.
 */
class CNaissance extends CMbObject implements IGroupRelated
{
    public const RESOURCE_TYPE = "birth";

    // DB Table key
    public $naissance_id;

    // DB References
    public $sejour_maman_id;
    public $sejour_enfant_id;
    public $operation_id;
    public $grossesse_id;

    // DB Fields
    public $hors_etab;
    public $date_time;
    public $rang;
    public $num_naissance;
    public $by_caesarean;

    public $_heure;
    public $_date_min;
    public $_date_max;
    public $_datetime_min;
    public $_datetime_max;

    public $interruption;
    public $num_semaines;
    public $rques;
    public $type_allaitement;

    // Ajout dossier p�rinatal
    // Nouveau n� en salle de naissance
    public $presence_pediatre;
    public $pediatre_id;
    public $presence_anesth;
    public $anesth_id;

    public $nouveau_ne_constantes_id;

    // R�sum� du s�jour du nouveau n�
    public $pathologies;

    // Dossier provisoire
    public $_provisoire;

    // dates
    public $_day_relative;


    // Service N�ona
    public $_service_neonatalogie;
    public $_consult_pediatre;

    /** @var COperation */
    public $_ref_operation;

    /** @var CGrossesse */
    public $_ref_grossesse;

    /** @var CExamenNouveauNe[] */
    public $_ref_examen_nouveau_ne;

    /** @var CExamenNouveauNe */
    public $_ref_last_examen_nouveau_ne;

    /** @var CSejour */
    public $_ref_sejour_enfant;

    /** @var CSejour */
    public $_ref_sejour_maman;

    /** @var CConstantesMedicales */
    public $_ref_nouveau_ne_constantes;

    /** @var CNaissanceRea[] */
    public $_ref_resuscitators;

    public ?EtatNaissance $_ref_etat = null;
    public ?Monitorage $_ref_monitorage = null;
    public ?Reanimation $_ref_reanimation = null;
    public ?Prophylaxie $_ref_prophylaxie = null;
    public ?MesurePreventive $_ref_mesure_preventive = null;
    public ?SortieSalle $_ref_sortie_salle  = null;
    public ?Lesion      $_ref_lesion = null;
    public ?Infection   $_ref_infection     = null;
    public ?Ictere      $_ref_ictere = null;
    public ?RegulationThermique $_ref_regulation_thermique = null;
    public ?AnomalieCongenitale $_ref_anomalie_congenitale = null;
    public ?Pathologie  $_ref_pathologie = null;
    public ?ActeMedical $_ref_acte = null;
    public ?ProphylaxieSejour $_ref_prophylaxie_sejour = null;
    public ?SortieSejour $_ref_sortie_sejour = null;

    /**
     * @inheritdoc
     */
    function getSpec()
    {
        $spec        = parent::getSpec();
        $spec->table = 'naissance';
        $spec->key   = 'naissance_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    function getProps()
    {
        $props                     = parent::getProps();
        $props["operation_id"]     = "ref class|COperation back|naissances";
        $props["grossesse_id"]     = "ref class|CGrossesse back|naissances";
        $props["sejour_maman_id"]  = "ref notNull class|CSejour back|naissances";
        $props["sejour_enfant_id"] = "ref notNull class|CSejour back|naissance";
        $props["hors_etab"]        = "bool default|0";
        $props["date_time"]        = "dateTime";
        $props["rang"]             = "num pos";
        $props["num_naissance"]    = "num pos";
        $props["interruption"]     = "enum list|fausse_couche|IMG|mort_in_utero";
        $props["num_semaines"]     = "enum list|inf_15|15_22|sup_22_sup_500g|sup_15";
        $props["rques"]            = "text helped";
        $props["type_allaitement"] = "enum list|maternel|artificiel|mixte";
        $props["by_caesarean"]     = "bool notNull default|0";

        $props["presence_pediatre"] = "enum list|non|avant|apres";
        $props["pediatre_id"]       = "ref class|CMediusers back|naissances_pediatre";
        $props["presence_anesth"]   = "enum list|non|avant|apres";
        $props["anesth_id"]         = "ref class|CMediusers back|naissances_anesth";

        $props['nouveau_ne_constantes_id'] = "ref class|CConstantesMedicales back|cstes_nouveau_ne";

        $props["pathologies"]                     = "bool";

        // Filter fields
        $props["_heure"]        = "time notNull";
        $props["_date_min"]     = "date";
        $props["_date_max"]     = "date moreThan|_date_min";
        $props["_datetime_min"] = "dateTime";
        $props["_datetime_max"] = "dateTime moreThan|_datetime_min";

        return $props;
    }

    /**
     * @inheritdoc
     */
    function check()
    {
        if ($msg = parent::check()) {
            return $msg;
        }

        $this->completeField("operation_id", "sejour_maman_id", "grossesse_id");

        // Operation has to be part of sejour
        if ($this->operation_id) {
            $operation = $this->loadRefOperation();
            if ($operation->sejour_id != $this->sejour_maman_id) {
                return "failed-operation-notin-sejour";
            }
        }

        // Sejour has to be part of grossesse
        $sejour = $this->loadRefSejourMaman();
        if ($sejour->grossesse_id != $this->grossesse_id) {
            return "failed-sejour-maman-notin-grossesse";
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    function updateFormFields()
    {
        parent::updateFormFields();
        if ($this->date_time) {
            $this->_view  = $this->getFormattedValue("date_time");
            $this->_heure = CMbDT::time($this->date_time);
        } else {
            $this->_view = "Dossier provisoire";
        }
        if ($this->rang) {
            $this->_view .= ", rang " . $this->rang;
        }

        $this->_day_relative = abs(CMbDT::daysRelative(CMbDT::date($this->date_time), CMbDT::date()));
    }

    /**
     * @inheritdoc
     */
    function loadRefsFwd()
    {
        $this->loadRefOperation();
        $this->loadRefGrossesse();
    }

    /**
     * Operation reference loader
     *
     * @return COperation
     */
    function loadRefOperation()
    {
        return $this->_ref_operation = $this->loadFwdRef("operation_id", true);
    }

    /**
     * Grossesse reference loader
     *
     * @return CGrossesse
     */
    function loadRefGrossesse()
    {
        return $this->_ref_grossesse = $this->loadFwdRef("grossesse_id", true);
    }

    /**
     * Chargement du suivi des examens du nouveau n�
     *
     * @return CExamenNouveauNe[]
     */
    function loadRefsExamenNouveauNe()
    {
        return $this->_ref_examen_nouveau_ne = $this->loadBackRefs("exams_bebe");
    }

    /**
     * Chargement du dernier suivi des examens du nouveau n�
     *
     * @return CExamenNouveauNe
     */
    function loadRefLastExamenNouveauNe()
    {
        return $this->_ref_last_examen_nouveau_ne = $this->loadLastBackRef("exams_bebe", "date ASC");
    }

    /**
     * Child's sejour reference loader
     *
     * @return CSejour
     */
    function loadRefSejourEnfant()
    {
        $this->_ref_sejour_enfant = $this->loadFwdRef("sejour_enfant_id", true);

        return $this->_ref_sejour_enfant;
    }

    /**
     * Mother's sejour reference loader
     *
     * @return CSejour
     */
    function loadRefSejourMaman()
    {
        return $this->_ref_sejour_maman = $this->loadFwdRef("sejour_maman_id", true);
    }

    /**
     * Chargement du relev�s de constantes du nouveau n� en salle de naissance
     *
     * @return CConstantesMedicales
     */
    public function loadRefConstantesNouveauNe()
    {
        return $this->_ref_nouveau_ne_constantes = $this->loadFwdRef('nouveau_ne_constantes_id', true);
    }

    /**
     * @return CStoredObject[]|null
     * @throws Exception
     */
    public function loadRefsResuscitators(): array
    {
        return $this->_ref_resuscitators = $this->loadBackRefs('naissances_rea');
    }

    /**
     * @return EtatNaissance|null
     * @throws Exception
     */
    public function loadRefEtat(): ?EtatNaissance
    {
        if (!$this->_ref_etat) {
            $this->_ref_etat = $this->loadUniqueBackRef('etat_naissance');
        }

        return $this->_ref_etat;
    }

    /**
     * @return Monitorage|null
     * @throws Exception
     */
    public function loadRefMonitorage(): ?Monitorage
    {
        if (!$this->_ref_monitorage) {
            $this->_ref_monitorage = $this->loadUniqueBackRef('monitorage');
        }

        return $this->_ref_monitorage;
    }

    /**
     * @return Reanimation|null
     * @throws Exception
     */
    public function loadRefReanimation(): ?Reanimation
    {
        if (!$this->_ref_reanimation) {
            $this->_ref_reanimation = $this->loadUniqueBackRef('reanimation');
        }

        return $this->_ref_reanimation;
    }

    /**
     * @return Prophylaxie|null
     * @throws Exception
     */
    public function loadRefProphylaxie(): ?Prophylaxie
    {
        if (!$this->_ref_prophylaxie) {
            $this->_ref_prophylaxie = $this->loadUniqueBackRef('prophylaxie');
        }

        return $this->_ref_prophylaxie;
    }

    /**
     * @return MesurePreventive|null
     * @throws Exception
     */
    public function loadRefMesurePreventive(): ?MesurePreventive
    {
        if (!$this->_ref_mesure_preventive) {
            $this->_ref_mesure_preventive = $this->loadUniqueBackRef('mesure_preventive');
        }

        return $this->_ref_mesure_preventive;
    }

    /**
     * @return SortieSalle|null
     * @throws Exception
     */
    public function loadRefSortieSalle(): ?SortieSalle
    {
        if (!$this->_ref_sortie_salle) {
            $this->_ref_sortie_salle = $this->loadUniqueBackRef('sortie_salle');
        }

        return $this->_ref_sortie_salle;
    }

    /**
     * @return Lesion|null
     * @throws Exception
     */
    public function loadRefLesion(): ?Lesion
    {
        if (!$this->_ref_lesion) {
            $this->_ref_lesion = $this->loadUniqueBackRef('lesion');
        }

        return $this->_ref_lesion;
    }

    /**
     * @return Infection|null
     * @throws Exception
     */
    public function loadRefInfection(): ?Infection
    {
        if (!$this->_ref_infection) {
            $this->_ref_infection = $this->loadUniqueBackRef('infection');
        }

        return $this->_ref_infection;
    }

    /**
     * @return Ictere|null
     * @throws Exception
     */
    public function loadRefIctere(): ?Ictere
    {
        if (!$this->_ref_ictere) {
            $this->_ref_ictere = $this->loadUniqueBackRef('ictere');
        }

        return $this->_ref_ictere;
    }

    /**
     * @return RegulationThermique|null
     * @throws Exception
     */
    public function loadRefRegulationThermique(): ?RegulationThermique
    {
        if (!$this->_ref_regulation_thermique) {
            $this->_ref_regulation_thermique = $this->loadUniqueBackRef('regulation_thermique');
        }

        return $this->_ref_regulation_thermique;
    }

    /**
     * @return AnomalieCongenitale|null
     * @throws Exception
     */
    public function loadRefAnomalieCongenitale(): ?AnomalieCongenitale
    {
        if (!$this->_ref_anomalie_congenitale) {
            $this->_ref_anomalie_congenitale = $this->loadUniqueBackRef('anomalie_congenitale');
        }

        return $this->_ref_anomalie_congenitale;
    }

    /**
     * @return Pathologie|null
     * @throws Exception
     */
    public function loadRefPathologie(): ?Pathologie
    {
        if (!$this->_ref_pathologie) {
            $this->_ref_pathologie = $this->loadUniqueBackRef('pathologie');
        }

        return $this->_ref_pathologie;
    }

    /**
     * @return ActeMedical|null
     * @throws Exception
     */
    public function loadRefActeMedical(): ?ActeMedical
    {
        if (!$this->_ref_acte) {
            $this->_ref_acte = $this->loadUniqueBackRef('acte');
        }

        return $this->_ref_acte;
    }

    /**
     * @return ProphylaxieSejour|null
     * @throws Exception
     */
    public function loadRefProphylaxieSejour(): ?ProphylaxieSejour
    {
        if (!$this->_ref_prophylaxie_sejour) {
            $this->_ref_prophylaxie_sejour = $this->loadUniqueBackRef('prophylaxie_sejour');
        }

        return $this->_ref_prophylaxie_sejour;
    }

    /**
     * @return SortieSejour|null
     * @throws Exception
     */
    public function loadRefSortieSejour(): ?SortieSejour
    {
        if (!$this->_ref_sortie_sejour) {
            $this->_ref_sortie_sejour = $this->loadUniqueBackRef('sortie_sejour');
        }

        return $this->_ref_sortie_sejour;
    }

    /**
     * Birth's counter
     *
     * @return int
     */
    static function countNaissances()
    {
        $group_id = CGroups::loadCurrent()->_id;
        $where    = [
            "naissance.num_semaines IS NULL OR naissance.num_semaines IN ('15_22', 'sup_22_sup_500g', 'sup_15')",
            "DATE_FORMAT(patients.naissance, '%Y') = " . CMbDT::transform(CMbDT::date(), null, "%Y"),
            "naissance.date_time IS NOT NULL",
            "naissance.num_naissance IS NOT NULL",
            "sejour.group_id = '$group_id'",
        ];
        $ljoin    = [
            "sejour"   => "naissance.sejour_enfant_id = sejour.sejour_id",
            "patients" => "sejour.patient_id = patients.patient_id",
        ];

        $naissance = new CNaissance();

        return $naissance->countList($where, null, $ljoin);
    }

    public function getNumNaissance()
    {
        $this->completeField("num_naissance", 'date_time');

        if ($this->_id && $this->num_naissance) {
            return $this->num_naissance;
        }

        if (!$this->date_time) {
            return null;
        }

        return CAppUI::gconf("maternite CNaissance num_naissance") + self::countNaissances();
    }

    /**
     * @see parent::fillLimitedTemplate()
     */
    function fillLimitedTemplate(&$template)
    {
        $this->loadRefSejourMaman()->loadRefPatient()->fillLimitedTemplate(
            $template,
            CAppUI::tr('CNaissance-Mother'),
            false
        );
    }

    function fillLiteLimitedTemplate(&$template, $champ)
    {
        $bebe = $this->loadRefSejourEnfant()->loadRefPatient();
        $template->addProperty("$champ - " . CAppUI::tr('common-name'), $bebe->nom);
        $template->addProperty("$champ - " . CAppUI::tr('common-first name'), $bebe->prenom);
        $template->addDateProperty("$champ - " . CAppUI::tr('CPatient-_p_birth_date'), $bebe->naissance);
        $template->addTimeProperty("$champ - " . CAppUI::tr('CNaissance-birth time'), $this->_heure);
        $date_naiss_word  = $bebe->naissance ?
            (CMbDT::format($bebe->naissance, "%A") . " " .
                CMbString::toWords(CMbDT::format($bebe->naissance, "%d")) . " " .
                CMbDT::format($bebe->naissance, "%B") . " " .
                CMbString::toWords(CMbDT::format($bebe->naissance, "%Y"))) : "";
        $heure_naiss_word = $this->_heure ?
            (CMbString::toWords(CMbDT::format($this->_heure, "%H")) . " " . CAppUI::tr('common-hour|pl') . " " .
                CMbString::toWords(CMbDT::format($this->_heure, "%M")) . " " . CAppUI::tr('common-minute|pl')) : "";
        $template->addProperty("$champ - " . CAppUI::tr('CNaissance-Date of birth (letter)'), $date_naiss_word);
        $template->addProperty("$champ - " . CAppUI::tr('CNaissance-Birth time (letter)'), $heure_naiss_word);

        $bebe->loadRefLatestConstantes($bebe->naissance, ["poids", "taille", "perimetre_cranien"]);
        $constantes       = CConstantesMedicales::getLatestFor(
            $bebe,
            null,
            ["taille", "perimetre_cranien"],
            $this->_ref_sejour_enfant,
            false,
            $bebe->naissance
        );
        $first_constantes = CConstantesMedicales::getFirstFor(
            $bebe,
            null,
            ["poids"],
            $this->_ref_sejour_enfant,
            false,
            $bebe->naissance
        );

        $template->addProperty(
            "$champ - " . CAppUI::tr('CConstantesMedicales-weight'),
            "{$first_constantes[0]->_poids_g} g"
        );
        $template->addProperty("$champ - " . CAppUI::tr('CConstantesMedicales-size'), "{$constantes[0]->taille} cm");
        $template->addProperty(
            "$champ - " . CAppUI::tr('CConstantesMedicales-cranial perimeter'),
            "{$constantes[0]->perimetre_cranien} cm"
        );

        $template->addProperty("$champ - " . CAppUI::tr('CPatient-sexe'), $bebe->getFormattedValue("sexe"));
        $template->addProperty("$champ - " . CAppUI::tr('CNaissance-rang'), $this->rang);
    }

    /**
     * @inheritDoc
     */
    function store()
    {
        $this->num_naissance = $this->getNumNaissance();
        $enfant              = $this->loadRefSejourEnfant()->loadRefPatient();

        // Save the birth rank in CPatient
        if (!$enfant->rang_naissance) {
            $enfant->rang_naissance = $this->rang;
            $enfant->store();
        }

        return parent::store();
    }

    public function loadRelGroup(): CGroups
    {
        return $this->loadRefSejourMaman()->loadRelGroup();
    }

    /**
     * Load Birth objects with guthrie date filter
     *
     * @param array  $naissances
     * @param string $guthrie_date_min
     * @param string $guthrie_date_max
     *
     * @return CNaissance[]
     */
    public function loadNaissancesByGuthrieDateFilter(
        array $naissances,
        string $guthrie_date_min,
        string $guthrie_date_max
    ): array {
        /** @var CNaissance $_naissance */
        foreach ($naissances as $_naissance_id => $_naissance) {
            $sejour             = $_naissance->loadRefSejourEnfant();
            $prescription       = $sejour->loadRefPrescriptionSejour();
            $examens_nouveau_ne = $_naissance->loadRefsExamenNouveauNe();

            $isGuthriePeriod = false;

            //Check Guthrie on new born exam
            if (count($examens_nouveau_ne)) {
                foreach ($examens_nouveau_ne as $_examen) {
                    if ($_examen->guthrie_datetime && ($_examen->guthrie_datetime >= "$guthrie_date_min 00:00:00") && ($_examen->guthrie_datetime <= "$guthrie_date_max 23:59:59")) {
                        $isGuthriePeriod = true;
                    }
                }

                if ($isGuthriePeriod) {
                    continue;
                }
            }

            //Check Guthrie by administration
            if (!$isGuthriePeriod && $prescription->_id) {
                $elt_guthrie_id = explode(":", CAppUI::gconf("maternite CNaissance elt_guthrie"))[0];

                $line_element = new CPrescriptionLineElement();
                $ds           = $line_element->getDS();

                $where = [
                    "prescription_id"         => $ds->prepare("= ?", $prescription->_id),
                    "element_prescription_id" => $ds->prepare("= ?", $elt_guthrie_id),
                ];

                $line_element->loadObject($where);

                if ($line_element->_id) {
                    $where_administrations = [
                        "planification" => $ds->prepare("= '0'"),
                    ];
                    $administrations       = $line_element->loadRefsAdministrations(
                        [CMbDT::date($guthrie_date_min), CMbDT::date($guthrie_date_max)],
                        $where_administrations,
                        "dateTime ASC"
                    );

                    if (!count($administrations)) {
                        unset($naissances[$_naissance_id]);
                        continue;
                    }
                    else {
                        $isGuthriePeriod = true;
                    }
                }
            }

            if (!$isGuthriePeriod) {
                unset($naissances[$_naissance_id]);
            }
        }

        return $naissances;
    }
}
