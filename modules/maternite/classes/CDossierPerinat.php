<?php

/**
 * @package Mediboard\Maternite
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Maternite\DossierPerinat\AbstractComponent;
use Ox\Mediboard\Maternite\DossierPerinat\AccouchementAnesthesie;
use Ox\Mediboard\Maternite\DossierPerinat\AccouchementDeclenchementTravail;
use Ox\Mediboard\Maternite\DossierPerinat\AccouchementDelivrance;
use Ox\Mediboard\Maternite\DossierPerinat\AccouchementPathologieAvantTravail;
use Ox\Mediboard\Maternite\DossierPerinat\AccouchementPathologiePendantTravail;
use Ox\Mediboard\Maternite\DossierPerinat\AccouchementSurveillanceTravail;
use Ox\Mediboard\Maternite\DossierPerinat\AccouchementTherapeutiqueTravail;
use Ox\Mediboard\Maternite\DossierPerinat\Admission;
use Ox\Mediboard\Maternite\DossierPerinat\AntecedentsFamiliaux;
use Ox\Mediboard\Maternite\DossierPerinat\AntecedentsGynecologiques;
use Ox\Mediboard\Maternite\DossierPerinat\AntecedentsMaternels;
use Ox\Mediboard\Maternite\DossierPerinat\AntecedentsObstetricaux;
use Ox\Mediboard\Maternite\DossierPerinat\AntecedentsPaternels;
use Ox\Mediboard\Maternite\DossierPerinat\CConduiteAccouchement;
use Ox\Mediboard\Maternite\DossierPerinat\ClotureSansAccouchement;
use Ox\Mediboard\Maternite\DossierPerinat\ComponentRepository;
use Ox\Mediboard\Maternite\DossierPerinat\ContextePsychoSocial;
use Ox\Mediboard\Maternite\DossierPerinat\DebutGrossesse;
use Ox\Mediboard\Maternite\DossierPerinat\PremierContact;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseContextePsychoSocial;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseExamenGrossesse;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseImmunisation;
use Ox\Mediboard\Maternite\DossierPerinat\SynthesePathologieFoetale;
use Ox\Mediboard\Maternite\DossierPerinat\SynthesePathologieMaternelle;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseSejour;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseSurveillance;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseTherapeutiqueFoetale;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseTherapeutiqueMaternelle;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseToxicologie;
use Ox\Mediboard\Maternite\DossierPerinat\Toxicologie;
use Ox\Mediboard\Maternite\DossierPerinat\TransfertAntenatal;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * Gestion du dossier de p�rinatalit� d'une grossesse
 */
class CDossierPerinat extends CMbObject
{
    public const FIELDS_MODELE_SKIPPED = [
        '_shortview',
        '_view',
    ];

    private ?ComponentRepository $component_repository = null;

    // DB Table key
    public $dossier_perinat_id;

    public $grossesse_id;

    /* References */
    public         $ant_mater_constantes_id;
    public         $pere_constantes_id;
    public ?string $consultant_premier_contact_id       = null;
    public ?string $mater_provenance_premier_contact_id = null;

    // Renseignements socio-demographiques
    public $activite_pro;
    public $activite_pro_pere;
    public $fatigue_travail;
    public $travail_hebdo;
    public $transport_jour;

    /* ContextPsychoSocial */
    public ?string $_rques_social               = null;
    public ?string $_enfants_foyer              = null;
    public ?string $_situation_part_enfance     = null;
    public ?string $_spe_perte_parent           = null;
    public ?string $_spe_maltraitance           = null;
    public ?string $_spe_mere_placee_enfance    = null;
    public ?string $_situation_part_adolescence = null;
    public ?string $_spa_anorexie_boulimie      = null;
    public ?string $_spa_depression             = null;
    public ?string $_situation_part_familiale   = null;
    public ?string $_spf_violences_conjugales   = null;
    public ?string $_spf_mere_isolee            = null;
    public ?string $_spf_absence_entourage_fam  = null;
    public ?string $_stress_agression           = null;
    public ?string $_sa_agression_physique      = null;
    public ?string $_sa_agression_sexuelle      = null;
    public ?string $_sa_harcelement_travail     = null;
    public ?string $_rques_psychologie          = null;
    public ?string $_situation_accompagnement   = null;
    public ?string $_rques_accompagnement       = null;

    /* Toxicologie */
    public ?string $_tabac_avant_grossesse       = null;
    public ?string $_qte_tabac_avant_grossesse   = null;
    public ?string $_tabac_debut_grossesse       = null;
    public ?string $_qte_tabac_debut_grossesse   = null;
    public ?string $_alcool_debut_grossesse      = null;
    public ?string $_qte_alcool_debut_grossesse  = null;
    public ?string $_canabis_debut_grossesse     = null;
    public ?string $_qte_canabis_debut_grossesse = null;
    public ?string $_subst_avant_grossesse       = null;
    public ?string $_mode_subst_avant_grossesse  = null;
    public ?string $_nom_subst_avant_grossesse   = null;
    public ?string $_subst_subst_avant_grossesse = null;
    public ?string $_subst_debut_grossesse       = null;
    public ?string $_subst_debut_grossesse_text  = null;
    public ?string $_tabac_pere                  = null;
    public ?string $_coexp_pere                  = null;
    public ?string $_alcool_pere                 = null;
    public ?string $_toxico_pere                 = null;
    public ?string $_rques_toxico                = null;

    /* AntecedentMaternel */
    public ?string $_patho_ant                = null;
    public ?string $_patho_ant_hta            = null;
    public ?string $_patho_ant_diabete        = null;
    public ?string $_patho_ant_epilepsie      = null;
    public ?string $_patho_ant_asthme         = null;
    public ?string $_patho_ant_pulm           = null;
    public ?string $_patho_ant_thrombo_emb    = null;
    public ?string $_patho_ant_cardio         = null;
    public ?string $_patho_ant_auto_immune    = null;
    public ?string $_patho_ant_hepato_dig     = null;
    public ?string $_patho_ant_thyroide       = null;
    public ?string $_patho_ant_uro_nephro     = null;
    public ?string $_patho_ant_infectieuse    = null;
    public ?string $_patho_ant_hemato         = null;
    public ?string $_patho_ant_cancer_non_gyn = null;
    public ?string $_patho_ant_psy            = null;
    public ?string $_patho_ant_autre          = null;

    public ?string $_chir_ant       = null;
    public ?string $_chir_ant_rques = null;

    /* AntecedentGynecologique */
    public ?string $_gyneco_ant_regles             = null;
    public ?string $_gyneco_ant_regul_regles       = null;
    public ?string $_gyneco_ant_fcv                = null;
    public ?string $_gyneco_ant                    = null;
    public ?string $_gyneco_ant_herpes             = null;
    public ?string $_gyneco_ant_lesion_col         = null;
    public ?string $_gyneco_ant_conisation         = null;
    public ?string $_gyneco_ant_cicatrice_uterus   = null;
    public ?string $_gyneco_ant_fibrome            = null;
    public ?string $_gyneco_ant_stat_pelv          = null;
    public ?string $_gyneco_ant_cancer_sein        = null;
    public ?string $_gyneco_ant_cancer_app_genital = null;
    public ?string $_gyneco_ant_malf_genitale      = null;
    public ?string $_gyneco_ant_condylomes         = null;
    public ?string $_gyneco_ant_distilbene         = null;
    public ?string $_gyneco_ant_autre              = null;

    public ?string $_gyneco_ant_infert         = null;
    public ?string $_gyneco_ant_infert_origine = null;

    /* AntecedentPaternel */
    public ?string $_pere_serologie_vih     = null;
    public ?string $_pere_electrophorese_hb = null;
    public ?string $_pere_patho_ant         = null;
    public ?string $_pere_ant_herpes        = null;
    public ?string $_pere_ant_autre         = null;

    /* AntecedentsFamiliaux */
    public ?string $_ant_fam                      = null;
    public ?string $_consanguinite                = null;
    public ?string $_ant_fam_mere_gemellite       = null;
    public ?string $_ant_fam_pere_gemellite       = null;
    public ?string $_ant_fam_mere_malformations   = null;
    public ?string $_ant_fam_pere_malformations   = null;
    public ?string $_ant_fam_mere_maladie_genique = null;
    public ?string $_ant_fam_pere_maladie_genique = null;
    public ?string $_ant_fam_mere_maladie_chrom   = null;
    public ?string $_ant_fam_pere_maladie_chrom   = null;
    public ?string $_ant_fam_mere_diabete         = null;
    public ?string $_ant_fam_pere_diabete         = null;
    public ?string $_ant_fam_mere_hta             = null;
    public ?string $_ant_fam_pere_hta             = null;
    public ?string $_ant_fam_mere_phlebite        = null;
    public ?string $_ant_fam_pere_phlebite        = null;
    public ?string $_ant_fam_mere_autre           = null;
    public ?string $_ant_fam_pere_autre           = null;

    /* AntecedentsObstetricaux */
    public ?string $_ant_obst_nb_gr_acc              = null;
    public ?string $_ant_obst_nb_gr_av_sp            = null;
    public ?string $_ant_obst_nb_gr_ivg              = null;
    public ?string $_ant_obst_nb_gr_geu              = null;
    public ?string $_ant_obst_nb_gr_mole             = null;
    public ?string $_ant_obst_nb_gr_img              = null;
    public ?string $_ant_obst_nb_gr_amp              = null;
    public ?string $_ant_obst_nb_gr_mult             = null;
    public ?string $_ant_obst_nb_gr_hta              = null;
    public ?string $_ant_obst_nb_gr_map              = null;
    public ?string $_ant_obst_nb_gr_diab             = null;
    public ?string $_ant_obst_nb_gr_cesar            = null;
    public ?string $_ant_obst_nb_gr_prema            = null;
    public ?string $_ant_obst_nb_enf_moins_2500g     = null;
    public ?string $_ant_obst_nb_enf_hypotroph       = null;
    public ?string $_ant_obst_nb_enf_macrosome       = null;
    public ?string $_ant_obst_nb_enf_morts_nes       = null;
    public ?string $_ant_obst_nb_enf_mort_neonat     = null;
    public ?string $_ant_obst_nb_enf_mort_postneonat = null;
    public ?string $_ant_obst_nb_enf_malform         = null;

    /* DebutGrossesse */
    public ?string $_souhait_grossesse                     = null;
    public ?string $_contraception_pre_grossesse           = null;
    public ?string $_grossesse_sous_contraception          = null;
    public ?string $_rques_contraception                   = null;
    public ?string $_grossesse_apres_traitement            = null;
    public ?string $_type_traitement_grossesse             = null;
    public ?string $_origine_ovule                         = null;
    public ?string $_origine_sperme                        = null;
    public ?string $_rques_traitement_grossesse            = null;
    public ?string $_traitement_peri_conceptionnelle       = null;
    public ?string $_type_traitement_peri_conceptionnelle  = null;
    public ?string $_arret_traitement_peri_conceptionnelle = null;
    public ?string $_rques_traitement_peri_conceptionnelle = null;

    // PremierContact
    public ?string $_date_premier_contact                = null;
    public ?string $_provenance_premier_contact          = null;
    public ?string $_nivsoins_provenance_premier_contact = null;
    public ?string $_motif_premier_contact               = null;
    public ?string $_nb_consult_ant_premier_contact      = null;
    public ?string $_sa_consult_ant_premier_contact      = null;
    public ?string $_surveillance_ant_premier_contact    = null;
    public ?string $_type_surv_ant_premier_contact       = null;
    public ?string $_date_declaration_grossesse          = null;
    public ?string $_rques_provenance                    = null;

    public ?string $_reco_aucune         = null;
    public ?string $_reco_tabac          = null;
    public ?string $_reco_rhesus_negatif = null;
    public ?string $_reco_toxoplasmose   = null;
    public ?string $_reco_alcool         = null;
    public ?string $_reco_vaccination    = null;
    public ?string $_reco_hygiene_alim   = null;
    public ?string $_reco_toxicomanie    = null;
    public ?string $_reco_brochure       = null;
    public ?string $_reco_autre          = null;

    public ?string $_souhait_arret_addiction = null;
    public ?string $_souhait_aide_addiction  = null;

    public ?string $_info_echographie        = null;
    public ?string $_info_despistage_triso21 = null;
    public ?string $_test_triso21_propose    = null;
    public ?string $_info_orga_maternite     = null;
    public ?string $_info_orga_reseau        = null;
    public ?string $_info_lien_pmi           = null;

    public ?string $_projet_lieu_accouchement     = null;
    public ?string $_projet_analgesie_peridurale  = null;
    public ?string $_projet_allaitement_maternel  = null;
    public ?string $_projet_preparation_naissance = null;
    public ?string $_projet_entretiens_proposes   = null;

    public ?string $_bas_risques                    = null;
    public ?string $_risque_atcd_maternel_med       = null;
    public ?string $_risque_atcd_obst               = null;
    public ?string $_risque_atcd_familiaux          = null;
    public ?string $_risque_patho_mater_grossesse   = null;
    public ?string $_risque_patho_foetale_grossesse = null;
    public ?string $_risque_psychosocial_grossesse  = null;
    public ?string $_risque_grossesse_multiple      = null;

    public ?string $_type_surveillance          = null;
    public ?string $_lieu_surveillance          = null;
    public ?string $_lieu_accouchement_prevu    = null;
    public ?string $_niveau_soins_prevu         = null;
    public ?string $_conclusion_premier_contact = null;

    /* TransfertAntenatal */
    public ?string $_transf_antenat                    = null;
    public ?string $_date_transf_antenat               = null;
    public ?string $_lieu_transf_antenat               = null;
    public ?string $_etab_transf_antenat               = null;
    public ?string $_nivsoins_transf_antenat           = null;
    public ?string $_raison_transf_antenat_hors_reseau = null;
    public ?string $_raison_imp_transf_antenat         = null;
    public ?string $_motif_tranf_antenat               = null;
    public ?string $_type_patho_transf_antenat         = null;
    public ?string $_rques_transf_antenat              = null;

    public ?string $_mode_transp_transf_antenat      = null;
    public ?string $_antibio_transf_antenat          = null;
    public ?string $_nom_antibio_transf_antenat      = null;
    public ?string $_cortico_transf_antenat          = null;
    public ?string $_nom_cortico_transf_antenat      = null;
    public ?string $_datetime_cortico_transf_antenat = null;
    public ?string $_tocolytiques_transf_antenat     = null;
    public ?string $_nom_tocolytiques_transf_antenat = null;
    public ?string $_antihta_transf_antenat          = null;
    public ?string $_nom_antihta_transf_antenat      = null;
    public ?string $_autre_ttt_transf_antenat        = null;
    public ?string $_nom_autre_ttt_transf_antenat    = null;

    public ?string $_retour_mater_transf_antenat   = null;
    public ?string $_date_retour_transf_antenat    = null;
    public ?string $_devenir_retour_transf_antenat = null;
    public ?string $_rques_retour_transf_antenat   = null;

    /* ClotureSansAccouchement */

    public ?string $_type_terminaison_grossesse = null;
    public ?string $_type_term_hors_etab        = null;
    public ?string $_type_term_inf_22sa         = null;

    // Synth�se de la grossesse

    public $date_validation_synthese;
    public $validateur_synthese_id;

    public $admission_id;
    public $adm_mater_constantes_id;
    public $adm_sage_femme_resp_id;

    /* SyntheseSurveillance */
    public ?string $_nb_consult_total_prenatal  = null;
    public ?string $_nb_consult_total_equipe    = null;
    public ?string $_entretien_prem_trim        = null;
    public ?string $_hospitalisation            = null;
    public ?string $_nb_sejours                 = null;
    public ?string $_nb_total_jours_hospi       = null;
    public ?string $_sage_femme_domicile        = null;
    public ?string $_transfert_in_utero         = null;
    public ?string $_consult_preanesth          = null;
    public ?string $_consult_centre_diag_prenat = null;
    public ?string $_preparation_naissance      = null;

    /* SyntheseGrossesseToxicologie */
    public ?string $_conso_toxique_pdt_grossesse = null;
    public ?string $_tabac_pdt_grossesse         = null;
    public ?string $_sevrage_tabac_pdt_grossesse = null;
    public ?string $_date_arret_tabac            = null;
    public ?string $_alcool_pdt_grossesse        = null;
    public ?string $_cannabis_pdt_grossesse      = null;
    public ?string $_autres_subst_pdt_grossesse  = null;
    public ?string $_type_subst_pdt_grossesse    = null;

    /* SyntheseContextePsychoSocial */
    public ?string $_profession_pdt_grossesse          = null;
    public ?string $_ag_date_arret_travail             = null;
    public ?string $_situation_pb_pdt_grossesse        = null;
    public ?string $_separation_pdt_grossesse          = null;
    public ?string $_deces_fam_pdt_grossesse           = null;
    public ?string $_autre_evenement_fam_pdt_grossesse = null;
    public ?string $_perte_emploi_pdt_grossesse        = null;
    public ?string $_autre_evenement_soc_pdt_grossesse = null;

    /* SyntheseExamenGrossesse */
    public ?string $_nb_total_echographies        = null;
    public ?string $_echo_1er_trim                = null;
    public ?string $_resultat_echo_1er_trim       = null;
    public ?string $_resultat_autre_echo_1er_trim = null;
    public ?string $_ag_echo_1er_trim             = null;
    public ?string $_echo_2e_trim                 = null;
    public ?string $_resultat_echo_2e_trim        = null;
    public ?string $_resultat_autre_echo_2e_trim  = null;
    public ?string $_ag_echo_2e_trim              = null;
    public ?string $_doppler_2e_trim              = null;
    public ?string $_resultat_doppler_2e_trim     = null;
    public ?string $_echo_3e_trim                 = null;
    public ?string $_resultat_echo_3e_trim        = null;
    public ?string $_resultat_autre_echo_3e_trim  = null;
    public ?string $_ag_echo_3e_trim              = null;
    public ?string $_doppler_3e_trim              = null;
    public ?string $_resultat_doppler_3e_trim     = null;

    public ?string $_prelevements_foetaux             = null;
    public ?string $_indication_prelevements_foetaux  = null;
    public ?string $_biopsie_trophoblaste             = null;
    public ?string $_resultat_biopsie_trophoblaste    = null;
    public ?string $_rques_biopsie_trophoblaste       = null;
    public ?string $_ag_biopsie_trophoblaste          = null;
    public ?string $_amniocentese                     = null;
    public ?string $_resultat_amniocentese            = null;
    public ?string $_rques_amniocentese               = null;
    public ?string $_ag_amniocentese                  = null;
    public ?string $_cordocentese                     = null;
    public ?string $_resultat_cordocentese            = null;
    public ?string $_rques_cordocentese               = null;
    public ?string $_ag_cordocentese                  = null;
    public ?string $_autre_prelevements_foetaux       = null;
    public ?string $_rques_autre_prelevements_foetaux = null;
    public ?string $_ag_autre_prelevements_foetaux    = null;

    public ?string $_prelevements_bacterio_mater   = null;
    public ?string $_prelevement_vaginal           = null;
    public ?string $_resultat_prelevement_vaginal  = null;
    public ?string $_rques_prelevement_vaginal     = null;
    public ?string $_ag_prelevement_vaginal        = null;
    public ?string $_prelevement_urinaire          = null;
    public ?string $_resultat_prelevement_urinaire = null;
    public ?string $_rques_prelevement_urinaire    = null;
    public ?string $_ag_prelevement_urinaire       = null;

    public ?string $_marqueurs_seriques           = null;
    public ?string $_resultats_marqueurs_seriques = null;
    public ?string $_rques_marqueurs_seriques     = null;

    public ?string $_depistage_diabete          = null;
    public ?string $_resultat_depistage_diabete = null;

    /* SyntheseImmunisation */
    public ?string $_rai_fin_grossesse                 = null;
    public ?string $_rubeole_fin_grossesse             = null;
    public ?string $_seroconv_rubeole                  = null;
    public ?string $_ag_seroconv_rubeole               = null;
    public ?string $_toxoplasmose_fin_grossesse        = null;
    public ?string $_seroconv_toxoplasmose             = null;
    public ?string $_ag_seroconv_toxoplasmose          = null;
    public ?string $_syphilis_fin_grossesse            = null;
    public ?string $_vih_fin_grossesse                 = null;
    public ?string $_hepatite_b_fin_grossesse          = null;
    public ?string $_hepatite_b_aghbspos_fin_grossesse = null;
    public ?string $_hepatite_c_fin_grossesse          = null;
    public ?string $_cmvg_fin_grossesse                = null;
    public ?string $_cmvm_fin_grossesse                = null;
    public ?string $_seroconv_cmv                      = null;
    public ?string $_ag_seroconv_cmv                   = null;
    public ?string $_autre_serodiag_fin_grossesse      = null;

    public $pathologie_grossesse;

    /* SynthesePathologieMaternelle */
    public ?string $_pathologie_grossesse_maternelle             = null;
    public ?string $_metrorragie_1er_trim                        = null;
    public ?string $_type_metrorragie_1er_trim                   = null;
    public ?string $_ag_metrorragie_1er_trim                     = null;
    public ?string $_metrorragie_2e_3e_trim                      = null;
    public ?string $_type_metrorragie_2e_3e_trim                 = null;
    public ?string $_ag_metrorragie_2e_3e_trim                   = null;
    public ?string $_menace_acc_premat                           = null;
    public ?string $_menace_acc_premat_modif_cerv                = null;
    public ?string $_pec_menace_acc_premat                       = null;
    public ?string $_ag_menace_acc_premat                        = null;
    public ?string $_ag_hospi_menace_acc_premat                  = null;
    public ?string $_rupture_premat_membranes                    = null;
    public ?string $_ag_rupture_premat_membranes                 = null;
    public ?string $_anomalie_liquide_amniotique                 = null;
    public ?string $_type_anomalie_liquide_amniotique            = null;
    public ?string $_ag_anomalie_liquide_amniotique              = null;
    public ?string $_autre_patho_gravidique                      = null;
    public ?string $_patho_grav_vomissements                     = null;
    public ?string $_patho_grav_herpes_gest                      = null;
    public ?string $_patho_grav_dermatose_pup                    = null;
    public ?string $_patho_grav_placenta_praevia_non_hemo        = null;
    public ?string $_patho_grav_chorio_amniotite                 = null;
    public ?string $_patho_grav_transf_foeto_mat                 = null;
    public ?string $_patho_grav_beance_col                       = null;
    public ?string $_patho_grav_cerclage                         = null;
    public ?string $_ag_autre_patho_gravidique                   = null;
    public ?string $_hypertension_arterielle                     = null;
    public ?string $_type_hypertension_arterielle                = null;
    public ?string $_ag_hypertension_arterielle                  = null;
    public ?string $_proteinurie                                 = null;
    public ?string $_type_proteinurie                            = null;
    public ?string $_ag_proteinurie                              = null;
    public ?string $_diabete                                     = null;
    public ?string $_type_diabete                                = null;
    public ?string $_ag_diabete                                  = null;
    public ?string $_infection_urinaire                          = null;
    public ?string $_type_infection_urinaire                     = null;
    public ?string $_ag_infection_urinaire                       = null;
    public ?string $_infection_cervico_vaginale                  = null;
    public ?string $_type_infection_cervico_vaginale             = null;
    public ?string $_autre_infection_cervico_vaginale            = null;
    public ?string $_ag_infection_cervico_vaginale               = null;
    public ?string $_autre_patho_maternelle                      = null;
    public ?string $_ag_autre_patho_maternelle                   = null;
    public ?string $_anemie_mat_pdt_grossesse                    = null;
    public ?string $_tombopenie_mat_pdt_grossesse                = null;
    public ?string $_autre_patho_hemato_mat_pdt_grossesse        = null;
    public ?string $_desc_autre_patho_hemato_mat_pdt_grossesse   = null;
    public ?string $_faible_prise_poid_mat_pdt_grossesse         = null;
    public ?string $_malnut_mat_pdt_grossesse                    = null;
    public ?string $_autre_patho_endo_mat_pdt_grossesse          = null;
    public ?string $_desc_autre_patho_endo_mat_pdt_grossesse     = null;
    public ?string $_cholestase_mat_pdt_grossesse                = null;
    public ?string $_steatose_hep_mat_pdt_grossesse              = null;
    public ?string $_autre_patho_hepato_mat_pdt_grossesse        = null;
    public ?string $_desc_autre_patho_hepato_mat_pdt_grossesse   = null;
    public ?string $_thrombophl_sup_mat_pdt_grossesse            = null;
    public ?string $_thrombophl_prof_mat_pdt_grossesse           = null;
    public ?string $_autre_patho_vein_mat_pdt_grossesse          = null;
    public ?string $_desc_autre_patho_vein_mat_pdt_grossesse     = null;
    public ?string $_asthme_mat_pdt_grossesse                    = null;
    public ?string $_autre_patho_resp_mat_pdt_grossesse          = null;
    public ?string $_desc_autre_patho_resp_mat_pdt_grossesse     = null;
    public ?string $_cardiopathie_mat_pdt_grossesse              = null;
    public ?string $_autre_patho_cardio_mat_pdt_grossesse        = null;
    public ?string $_desc_autre_patho_cardio_mat_pdt_grossesse   = null;
    public ?string $_epilepsie_mat_pdt_grossesse                 = null;
    public ?string $_depression_mat_pdt_grossesse                = null;
    public ?string $_autre_patho_neuropsy_mat_pdt_grossesse      = null;
    public ?string $_desc_autre_patho_neuropsy_mat_pdt_grossesse = null;
    public ?string $_patho_gyneco_mat_pdt_grossesse              = null;
    public ?string $_desc_patho_gyneco_mat_pdt_grossesse         = null;
    public ?string $_mst_mat_pdt_grossesse                       = null;
    public ?string $_desc_mst_mat_pdt_grossesse                  = null;
    public ?string $_synd_douleur_abdo_mat_pdt_grossesse         = null;
    public ?string $_desc_synd_douleur_abdo_mat_pdt_grossesse    = null;
    public ?string $_synd_infect_mat_pdt_grossesse               = null;
    public ?string $_desc_synd_infect_mat_pdt_grossesse          = null;

    /* SynthesePathologieFoetale */
    public ?string $_therapeutique_grossesse_maternelle    = null;
    public ?string $_antibio_pdt_grossesse                 = null;
    public ?string $_type_antibio_pdt_grossesse            = null;
    public ?string $_tocolyt_pdt_grossesse                 = null;
    public ?string $_mode_admin_tocolyt_pdt_grossesse      = null;
    public ?string $_cortico_pdt_grossesse                 = null;
    public ?string $_nb_cures_cortico_pdt_grossesse        = null;
    public ?string $_etat_dern_cure_cortico_pdt_grossesse  = null;
    public ?string $_gammaglob_anti_d_pdt_grossesse        = null;
    public ?string $_antihyp_pdt_grossesse                 = null;
    public ?string $_aspirine_a_pdt_grossesse              = null;
    public ?string $_barbit_antiepilept_pdt_grossesse      = null;
    public ?string $_psychotropes_pdt_grossesse            = null;
    public ?string $_subst_nicotine_pdt_grossesse          = null;
    public ?string $_autre_therap_mater_pdt_grossesse      = null;
    public ?string $_desc_autre_therap_mater_pdt_grossesse = null;

    /* SynthesePathologieFoetale */
    public ?string $_patho_foetale_in_utero              = null;
    public ?string $_anomalie_croiss_intra_uterine       = null;
    public ?string $_type_anomalie_croiss_intra_uterine  = null;
    public ?string $_ag_anomalie_croiss_intra_uterine    = null;
    public ?string $_signes_hypoxie_foetale_chronique    = null;
    public ?string $_ag_signes_hypoxie_foetale_chronique = null;
    public ?string $_hypoxie_foetale_anomalie_doppler    = null;
    public ?string $_hypoxie_foetale_anomalie_rcf        = null;
    public ?string $_hypoxie_foetale_alter_profil_biophy = null;
    public ?string $_anomalie_constit_foetus             = null;
    public ?string $_ag_anomalie_constit_foetus          = null;
    public ?string $_malformation_isolee_foetus          = null;
    public ?string $_anomalie_chromo_foetus              = null;
    public ?string $_synd_polymalform_foetus             = null;
    public ?string $_anomalie_genique_foetus             = null;
    public ?string $_rques_anomalies_foetus              = null;
    public ?string $_foetopathie_infect_acquise          = null;
    public ?string $_type_foetopathie_infect_acquise     = null;
    public ?string $_autre_foetopathie_infect_acquise    = null;
    public ?string $_ag_foetopathie_infect_acquise       = null;
    public ?string $_autre_patho_foetale                 = null;
    public ?string $_ag_autre_patho_foetale              = null;
    public ?string $_allo_immun_anti_rh_foetale          = null;
    public ?string $_autre_allo_immun_foetale            = null;
    public ?string $_anas_foeto_plac_non_immun           = null;
    public ?string $_trouble_rcf_foetus                  = null;
    public ?string $_foetopathie_alcoolique              = null;
    public ?string $_grosse_abdo_foetus_viable           = null;
    public ?string $_mort_foetale_in_utero_in_22sa       = null;
    public ?string $_autre_patho_foetale_autre           = null;
    public ?string $_desc_autre_patho_foetale_autre      = null;
    public ?string $_patho_foetale_gross_mult            = null;
    public ?string $_ag_patho_foetale_gross_mult         = null;
    public ?string $_avort_foetus_gross_mult             = null;
    public ?string $_mort_foetale_in_utero_gross_mutl    = null;
    public ?string $_synd_transf_transf_gross_mult       = null;

    /* SyntheseTherapeutiqueFoetale */
    public ?string $_therapeutique_foetale           = null;
    public ?string $_amnioinfusion                   = null;
    public ?string $_chirurgie_foetale               = null;
    public ?string $_derivation_foetale              = null;
    public ?string $_tranfusion_foetale_in_utero     = null;
    public ?string $_ex_sanguino_transfusion_foetale = null;
    public ?string $_autre_therapeutiques_foetales   = null;
    public ?string $_reduction_embryonnaire          = null;
    public ?string $_type_reduction_embryonnaire     = null;
    public ?string $_photocoag_vx_placentaires       = null;
    public ?string $_rques_therapeutique_foetale     = null;

    /* CConduiteAccouchement */
    public ?string $_presentation_fin_grossesse             = null;
    public ?string $_autre_presentation_fin_grossesse       = null;
    public ?string $_version_presentation_manoeuvre_ext     = null;
    public ?string $_rques_presentation_fin_grossesse       = null;
    public ?string $_etat_uterus_fin_grossesse              = null;
    public ?string $_autre_anomalie_uterus_fin_grossesse    = null;
    public ?string $_nb_cicatrices_uterus_fin_grossesse     = null;
    public ?string $_date_derniere_hysterotomie             = null;
    public ?string $_rques_etat_uterus                      = null;
    public ?string $_appreciation_clinique_etat_bassin      = null;
    public ?string $_desc_appreciation_clinique_etat_bassin = null;
    public ?string $_pelvimetrie                            = null;
    public ?string $_desc_pelvimetrie                       = null;
    public ?string $_diametre_transverse_median             = null;
    public ?string $_diametre_promonto_retro_pubien         = null;
    public ?string $_diametre_bisciatique                   = null;
    public ?string $_indice_magnin                          = null;
    public ?string $_date_echo_fin_grossesse                = null;
    public ?string $_sa_echo_fin_grossesse                  = null;
    public ?string $_bip_fin_grossesse                      = null;
    public ?string $_est_pond_fin_grossesse                 = null;
    public ?string $_est_pond_2e_foetus_fin_grossesse       = null;
    public ?string $_conduite_a_tenir_acc                   = null;
    public ?string $_niveau_alerte_cesar                    = null;
    public ?string $_date_decision_conduite_a_tenir_acc     = null;
    public ?string $_valid_decision_conduite_a_tenir_acc_id = null;
    public ?string $_motif_conduite_a_tenir_acc             = null;
    public ?string $_date_prevue_interv                     = null;
    public ?string $_rques_conduite_a_tenir                 = null;
    public ?string $_facteur_risque                         = null;

    /* Admission */
    public ?string $_ag_admission                 = null;
    public ?string $_ag_jours_admission           = null;
    public ?string $_motif_admission              = null;
    public ?string $_ag_ruptures_membranes        = null;
    public ?string $_ag_jours_ruptures_membranes  = null;
    public ?string $_delai_rupture_travail_jours  = null;
    public ?string $_delai_rupture_travail_heures = null;
    public ?string $_date_ruptures_membranes      = null;
    public ?string $_rques_admission              = null;

    public ?string $_exam_entree_oedeme            = null;
    public ?string $_exam_entree_bruits_du_coeur   = null;
    public ?string $_exam_entree_mvt_actifs_percus = null;
    public ?string $_exam_entree_contractions      = null;
    public ?string $_exam_entree_presentation      = null;
    public ?string $_exam_entree_col               = null;
    public ?string $_exam_entree_liquide_amnio     = null;
    public ?string $_exam_entree_indice_bishop     = null;

    public ?string $_exam_entree_prelev_urine        = null;
    public ?string $_exam_entree_proteinurie         = null;
    public ?string $_exam_entree_glycosurie          = null;
    public ?string $_exam_entree_prelev_vaginal      = null;
    public ?string $_exam_entree_prelev_vaginal_desc = null;
    public ?string $_exam_entree_rcf                 = null;
    public ?string $_exam_entree_rcf_desc            = null;
    public ?string $_exam_entree_amnioscopie         = null;
    public ?string $_exam_entree_amnioscopie_desc    = null;
    public ?string $_exam_entree_autres              = null;
    public ?string $_exam_entree_autres_desc         = null;
    public ?string $_rques_exam_entree               = null;

    // R�sum� d'accouchement
    public $lieu_accouchement;
    public $autre_lieu_accouchement;
    public $nom_maternite_externe;
    public $lieu_delivrance;
    public $ag_accouchement;
    public $ag_jours_accouchement;
    public $mode_debut_travail;
    public $rques_debut_travail;

    /* AccouchementDeclenchementTravail */
    public ?string $_datetime_declenchement    = null;
    public ?string $_motif_decl_conv           = null;
    public ?string $_motif_decl_gross_prol     = null;
    public ?string $_motif_decl_patho_mat      = null;
    public ?string $_motif_decl_patho_foet     = null;
    public ?string $_motif_decl_rpm            = null;
    public ?string $_motif_decl_mort_iu        = null;
    public ?string $_motif_decl_img            = null;
    public ?string $_motif_decl_autre          = null;
    public ?string $_motif_decl_autre_details  = null;
    public ?string $_moyen_decl_ocyto          = null;
    public ?string $_moyen_decl_prosta         = null;
    public ?string $_moyen_decl_autre_medic    = null;
    public ?string $_moyen_decl_meca           = null;
    public ?string $_moyen_decl_rupture        = null;
    public ?string $_moyen_decl_autre          = null;
    public ?string $_moyen_decl_autre_details  = null;
    public ?string $_score_bishop              = null;
    public ?string $_score_bishop_dilatation   = null;
    public ?string $_score_bishop_longueur     = null;
    public ?string $_score_bishop_consistance  = null;
    public ?string $_score_bishop_position     = null;
    public ?string $_score_bishop_presentation = null;
    public ?string $_remarques_declenchement   = null;

    /* AccouchementSurveillanceTravail */
    public ?string $_surveillance_travail                 = null;
    public ?string $_tocographie                          = null;
    public ?string $_type_tocographie                     = null;
    public ?string $_anomalie_contractions                = null;
    public ?string $_rcf                                  = null;
    public ?string $_type_rcf                             = null;
    public ?string $_desc_trace_rcf                       = null;
    public ?string $_anomalie_rcf                         = null;
    public ?string $_ecg_foetal                           = null;
    public ?string $_anomalie_ecg_foetal                  = null;
    public ?string $_prelevement_sang_foetal              = null;
    public ?string $_anomalie_ph_sang_foetal              = null;
    public ?string $_detail_anomalie_ph_sang_foetal       = null;
    public ?string $_valeur_anomalie_ph_sang_foetal       = null;
    public ?string $_anomalie_lactates_sang_foetal        = null;
    public ?string $_detail_anomalie_lactates_sang_foetal = null;
    public ?string $_valeur_anomalie_lactates_sang_foetal = null;
    public ?string $_oxymetrie_foetale                    = null;
    public ?string $_anomalie_oxymetrie_foetale           = null;
    public ?string $_detail_anomalie_oxymetrie_foetale    = null;
    public ?string $_autre_examen_surveillance            = null;
    public ?string $_desc_autre_examen_surveillance       = null;
    public ?string $_anomalie_autre_examen_surveillance   = null;
    public ?string $_rques_surveillance_travail           = null;

    /* AccouchementTherapeutiqueTravail */
    public ?string $_therapeutique_pdt_travail     = null;
    public ?string $_antibio_pdt_travail           = null;
    public ?string $_antihypertenseurs_pdt_travail = null;
    public ?string $_antispasmodiques_pdt_travail  = null;
    public ?string $_tocolytiques_pdt_travail      = null;
    public ?string $_ocytociques_pdt_travail       = null;
    public ?string $_opiaces_pdt_travail           = null;
    public ?string $_sedatifs_pdt_travail          = null;
    public ?string $_amnioinfusion_pdt_travail     = null;
    public ?string $_autre_therap_pdt_travail      = null;
    public ?string $_desc_autre_therap_pdt_travail = null;
    public ?string $_rques_therap_pdt_travail      = null;

    public $pathologie_accouchement;
    public $fievre_pdt_travail;
    public $fievre_travail_constantes_id;

    /* AccouchementPathologieAvantTravail */
    public ?string $_anom_av_trav                                   = null;
    public ?string $_anom_pres_av_trav                              = null;
    public ?string $_anom_pres_av_trav_siege                        = null;
    public ?string $_anom_pres_av_trav_transverse                   = null;
    public ?string $_anom_pres_av_trav_face                         = null;
    public ?string $_anom_pres_av_trav_anormale                     = null;
    public ?string $_anom_pres_av_trav_autre                        = null;
    public ?string $_anom_bassin_av_trav                            = null;
    public ?string $_anom_bassin_av_trav_bassin_retreci             = null;
    public ?string $_anom_bassin_av_trav_malform_bassin             = null;
    public ?string $_anom_bassin_av_trav_foetus                     = null;
    public ?string $_anom_bassin_av_trav_disprop_foetopelv          = null;
    public ?string $_anom_bassin_av_trav_disprop_difform_foet       = null;
    public ?string $_anom_bassin_av_trav_disprop_sans_prec          = null;
    public ?string $_anom_genit_hors_trav                           = null;
    public ?string $_anom_genit_hors_trav_uterus_cicat              = null;
    public ?string $_anom_genit_hors_trav_rupt_uterine              = null;
    public ?string $_anom_genit_hors_trav_fibrome_uterin            = null;
    public ?string $_anom_genit_hors_trav_malform_uterine           = null;
    public ?string $_anom_genit_hors_trav_anom_vaginales            = null;
    public ?string $_anom_genit_hors_trav_chir_ant_perinee          = null;
    public ?string $_anom_genit_hors_trav_prolapsus_vaginal         = null;
    public ?string $_anom_plac_av_trav                              = null;
    public ?string $_anom_plac_av_trav_plac_prae_sans_hemo          = null;
    public ?string $_anom_plac_av_trav_plac_prae_avec_hemo          = null;
    public ?string $_anom_plac_av_trav_hrp_avec_trouble_coag        = null;
    public ?string $_anom_plac_av_trav_hrp_sans_trouble_coag        = null;
    public ?string $_anom_plac_av_trav_autre_hemo_avec_trouble_coag = null;
    public ?string $_anom_plac_av_trav_autre_hemo_sans_trouble_coag = null;
    public ?string $_anom_plac_av_trav_transf_foeto_mater           = null;
    public ?string $_anom_plac_av_trav_infect_sac_membranes         = null;
    public ?string $_rupt_premat_membranes                          = null;
    public ?string $_rupt_premat_membranes_rpm_inf37sa_sans_toco    = null;
    public ?string $_rupt_premat_membranes_rpm_inf37sa_avec_toco    = null;
    public ?string $_rupt_premat_membranes_rpm_sup37sa              = null;
    public ?string $_patho_foet_chron                               = null;
    public ?string $_patho_foet_chron_retard_croiss                 = null;
    public ?string $_patho_foet_chron_macrosom_foetale              = null;
    public ?string $_patho_foet_chron_immun_antirh                  = null;
    public ?string $_patho_foet_chron_autre_allo_immun              = null;
    public ?string $_patho_foet_chron_anasarque_non_immun           = null;
    public ?string $_patho_foet_chron_anasarque_immun               = null;
    public ?string $_patho_foet_chron_hypoxie_foetale               = null;
    public ?string $_patho_foet_chron_trouble_rcf                   = null;
    public ?string $_patho_foet_chron_mort_foatale_in_utero         = null;
    public ?string $_patho_mat_foet_av_trav                         = null;
    public ?string $_patho_mat_foet_av_trav_hta_gravid              = null;
    public ?string $_patho_mat_foet_av_trav_preec_moderee           = null;
    public ?string $_patho_mat_foet_av_trav_preec_severe            = null;
    public ?string $_patho_mat_foet_av_trav_hellp                   = null;
    public ?string $_patho_mat_foet_av_trav_preec_hta               = null;
    public ?string $_patho_mat_foet_av_trav_eclamp                  = null;
    public ?string $_patho_mat_foet_av_trav_diabete_id              = null;
    public ?string $_patho_mat_foet_av_trav_diabete_nid             = null;
    public ?string $_patho_mat_foet_av_trav_steatose_grav           = null;
    public ?string $_patho_mat_foet_av_trav_herpes_genit            = null;
    public ?string $_patho_mat_foet_av_trav_condylomes              = null;
    public ?string $_patho_mat_foet_av_trav_hep_b                   = null;
    public ?string $_patho_mat_foet_av_trav_hep_c                   = null;
    public ?string $_patho_mat_foet_av_trav_vih                     = null;
    public ?string $_patho_mat_foet_av_trav_sida                    = null;
    public ?string $_patho_mat_foet_av_trav_fievre                  = null;
    public ?string $_patho_mat_foet_av_trav_gross_prolong           = null;
    public ?string $_patho_mat_foet_av_trav_autre                   = null;
    public ?string $_autre_motif_cesarienne                         = null;
    public ?string $_autre_motif_cesarienne_conv                    = null;
    public ?string $_autre_motif_cesarienne_mult                    = null;

    /* AccouchementPathologiePendantTravail */
    public ?string $_anom_pdt_trav                                 = null;
    public ?string $_hypox_foet_pdt_trav                           = null;
    public ?string $_hypox_foet_pdt_trav_rcf_isole                 = null;
    public ?string $_hypox_foet_pdt_trav_la_teinte                 = null;
    public ?string $_hypox_foet_pdt_trav_rcf_la                    = null;
    public ?string $_hypox_foet_pdt_trav_anom_ph_foet              = null;
    public ?string $_hypox_foet_pdt_trav_anom_ecg_foet             = null;
    public ?string $_hypox_foet_pdt_trav_procidence_cordon         = null;
    public ?string $_dysto_pres_pdt_trav                           = null;
    public ?string $_dysto_pres_pdt_trav_rot_tete_incomp           = null;
    public ?string $_dysto_pres_pdt_trav_siege                     = null;
    public ?string $_dysto_pres_pdt_trav_face                      = null;
    public ?string $_dysto_pres_pdt_trav_pres_front                = null;
    public ?string $_dysto_pres_pdt_trav_pres_transv               = null;
    public ?string $_dysto_pres_pdt_trav_autre_pres_anorm          = null;
    public ?string $_dysto_anom_foet_pdt_trav                      = null;
    public ?string $_dysto_anom_foet_pdt_trav_foetus_macrosome     = null;
    public ?string $_dysto_anom_foet_pdt_trav_jumeaux_soudes       = null;
    public ?string $_dysto_anom_foet_pdt_trav_difform_foet         = null;
    public ?string $_echec_decl_travail                            = null;
    public ?string $_echec_decl_travail_medic                      = null;
    public ?string $_echec_decl_travail_meca                       = null;
    public ?string $_echec_decl_travail_sans_prec                  = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav                  = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_deform_pelv      = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_bassin_retr      = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_detroit_sup_retr = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_detroit_moy_retr = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_dispr_foeto_pelv = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_fibrome_pelv     = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_stenose_cerv     = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_malf_uterine     = null;
    public ?string $_dysto_anom_pelv_mat_pdt_trav_autre            = null;
    public ?string $_dysto_dynam_pdt_trav                          = null;
    public ?string $_dysto_dynam_pdt_trav_demarrage                = null;
    public ?string $_dysto_dynam_pdt_trav_cerv_latence             = null;
    public ?string $_dysto_dynam_pdt_trav_arret_dilat              = null;
    public ?string $_dysto_dynam_pdt_trav_hypertonie_uter          = null;
    public ?string $_dysto_dynam_pdt_trav_dilat_lente_col          = null;
    public ?string $_dysto_dynam_pdt_trav_echec_travail            = null;
    public ?string $_dysto_dynam_pdt_trav_non_engagement           = null;
    public ?string $_patho_mater_pdt_trav                          = null;
    public ?string $_patho_mater_pdt_trav_hemo_sans_trouble_coag   = null;
    public ?string $_patho_mater_pdt_trav_hemo_avec_trouble_coag   = null;
    public ?string $_patho_mater_pdt_trav_choc_obst                = null;
    public ?string $_patho_mater_pdt_trav_eclampsie                = null;
    public ?string $_patho_mater_pdt_trav_rupt_uterine             = null;
    public ?string $_patho_mater_pdt_trav_embolie_amnio            = null;
    public ?string $_patho_mater_pdt_trav_embolie_pulm             = null;
    public ?string $_patho_mater_pdt_trav_complic_acte_obst        = null;
    public ?string $_patho_mater_pdt_trav_chorio_amnio             = null;
    public ?string $_patho_mater_pdt_trav_infection                = null;
    public ?string $_patho_mater_pdt_trav_fievre                   = null;
    public ?string $_patho_mater_pdt_trav_fatigue_mat              = null;
    public ?string $_patho_mater_pdt_trav_autre_complication       = null;

    public ?string $_anom_expuls                         = null;
    public ?string $_anom_expuls_non_progr_pres_foetale  = null;
    public ?string $_anom_expuls_dysto_pres_posterieures = null;
    public ?string $_anom_expuls_dystocie_epaules        = null;
    public ?string $_anom_expuls_retention_tete          = null;
    public ?string $_anom_expuls_soufrance_foet_rcf      = null;
    public ?string $_anom_expuls_soufrance_foet_rcf_la   = null;
    public ?string $_anom_expuls_echec_forceps_cesar     = null;
    public ?string $_anom_expuls_fatigue_mat             = null;

    public $anesth_avant_naiss_par_id;

    /* AccouchementAnesthesie */
    public ?string $_anesth_avant_naiss                  = null;
    public ?string $_datetime_anesth_avant_naiss         = null;
    public ?string $_suivi_anesth_avant_naiss_par        = null;
    public ?string $_alr_avant_naiss                     = null;
    public ?string $_alr_peri_avant_naiss                = null;
    public ?string $_alr_peri_avant_naiss_inj_unique     = null;
    public ?string $_alr_peri_avant_naiss_reinj          = null;
    public ?string $_alr_peri_avant_naiss_cat_autopousse = null;
    public ?string $_alr_peri_avant_naiss_cat_pcea       = null;
    public ?string $_alr_rachi_avant_naiss               = null;
    public ?string $_alr_rachi_avant_naiss_inj_unique    = null;
    public ?string $_alr_rachi_avant_naiss_cat           = null;
    public ?string $_alr_peri_rachi_avant_naiss          = null;
    public ?string $_ag_avant_naiss                      = null;
    public ?string $_ag_avant_naiss_directe              = null;
    public ?string $_ag_avant_naiss_apres_peri           = null;
    public ?string $_ag_avant_naiss_apres_rachi          = null;
    public ?string $_al_avant_naiss                      = null;
    public ?string $_al_bloc_avant_naiss                 = null;
    public ?string $_al_autre_avant_naiss                = null;
    public ?string $_al_autre_avant_naiss_desc           = null;
    public ?string $_autre_analg_avant_naiss             = null;
    public ?string $_autre_analg_avant_naiss_desc        = null;
    public ?string $_fibro_laryngee                      = null;
    public ?string $_asa_anesth_avant_naissance          = null;
    public ?string $_moment_anesth_avant_naissance       = null;
    public ?string $_anesth_spec_2eme_enfant             = null;
    public ?string $_anesth_spec_2eme_enfant_desc        = null;
    public ?string $_rques_anesth_avant_naiss            = null;
    public ?string $_comp_anesth_avant_naiss             = null;
    public ?string $_hypotension_alr_avant_naiss         = null;
    public ?string $_autre_comp_alr_avant_naiss          = null;
    public ?string $_autre_comp_alr_avant_naiss_desc     = null;
    public ?string $_mendelson_ag_avant_naiss            = null;
    public ?string $_comp_pulm_ag_avant_naiss            = null;
    public ?string $_comp_card_ag_avant_naiss            = null;
    public ?string $_comp_cereb_ag_avant_naiss           = null;
    public ?string $_comp_allerg_tox_ag_avant_naiss      = null;
    public ?string $_autre_comp_ag_avant_naiss           = null;
    public ?string $_autre_comp_ag_avant_naiss_desc      = null;
    public ?string $_anesth_apres_naissance              = null;
    public ?string $_anesth_apres_naissance_desc         = null;
    public ?string $_rques_anesth_apres_naissance        = null;

    /* AccouchementDelivrance */
    public ?string $_deliv_faite_par                       = null;
    public ?string $_datetime_deliv                        = null;
    public ?string $_type_deliv                            = null;
    public ?string $_prod_deliv                            = null;
    public ?string $_dose_prod_deliv                       = null;
    public ?string $_datetime_inj_prod_deliv               = null;
    public ?string $_voie_inj_prod_deliv                   = null;
    public ?string $_modalite_deliv                        = null;
    public ?string $_comp_deliv                            = null;
    public ?string $_hemorr_deliv                          = null;
    public ?string $_retention_plac_comp_deliv             = null;
    public ?string $_retention_plac_part_deliv             = null;
    public ?string $_atonie_uterine_deliv                  = null;
    public ?string $_trouble_coag_deliv                    = null;
    public ?string $_transf_deliv                          = null;
    public ?string $_nb_unites_transf_deliv                = null;
    public ?string $_autre_comp_deliv                      = null;
    public ?string $_retention_plac_comp_sans_hemorr_deliv = null;
    public ?string $_retention_plac_part_sans_hemorr_deliv = null;
    public ?string $_inversion_uterine_deliv               = null;
    public ?string $_autre_comp_autre_deliv                = null;
    public ?string $_autre_comp_autre_deliv_desc           = null;
    public ?string $_total_pertes_sang_deliv               = null;
    public ?string $_actes_pdt_deliv                       = null;
    public ?string $_deliv_artificielle                    = null;
    public ?string $_rev_uterine_isolee_deliv              = null;
    public ?string $_autres_actes_deliv                    = null;
    public ?string $_ligature_art_hypogast_deliv           = null;
    public ?string $_ligature_art_uterines_deliv           = null;
    public ?string $_hysterectomie_hemostase_deliv         = null;
    public ?string $_embolisation_arterielle_deliv         = null;
    public ?string $_reduct_inversion_uterine_deliv        = null;
    public ?string $_cure_chir_inversion_uterine_deliv     = null;
    public ?string $_poids_placenta                        = null;
    public ?string $_anomalie_placenta                     = null;
    public ?string $_anomalie_placenta_desc                = null;
    public ?string $_type_placentation                     = null;
    public ?string $_type_placentation_desc                = null;
    public ?string $_poids_placenta_1_bichorial            = null;
    public ?string $_poids_placenta_2_bichorial            = null;
    public ?string $_exam_anapath_placenta_demande         = null;
    public ?string $_rques_placenta                        = null;
    public ?string $_lesion_parties_molles                 = null;
    public ?string $_episiotomie                           = null;
    public ?string $_dechirure_perineale                   = null;
    public ?string $_dechirure_perineale_liste             = null;
    public ?string $_lesions_traumatiques_parties_molles   = null;
    public ?string $_dechirure_vaginale                    = null;
    public ?string $_dechirure_cervicale                   = null;
    public ?string $_lesion_urinaire                       = null;
    public ?string $_rupt_uterine                          = null;
    public ?string $_thrombus                              = null;
    public ?string $_autre_lesion                          = null;
    public ?string $_autre_lesion_desc                     = null;
    public ?string $_compte_rendu_delivrance               = null;
    public ?string $_consignes_suite_couches               = null;

    /* SyntheseSejour */
    public ?string $_pathologies_suite_couches                    = null;
    public ?string $_infection_suite_couches                      = null;
    public ?string $_infection_nosoc_suite_couches                = null;
    public ?string $_localisation_infection_suite_couches         = null;
    public ?string $_compl_perineales_suite_couches               = null;
    public ?string $_details_compl_perineales_suite_couches       = null;
    public ?string $_compl_parietales_suite_couches               = null;
    public ?string $_detail_compl_parietales_suite_couches        = null;
    public ?string $_compl_allaitement_suite_couches              = null;
    public ?string $_details_compl_allaitement_suite_couches      = null;
    public ?string $_details_comp_compl_allaitement_suite_couches = null;
    public ?string $_compl_thrombo_embo_suite_couches             = null;
    public ?string $_detail_compl_thrombo_embo_suite_couches      = null;
    public ?string $_compl_autre_suite_couches                    = null;
    public ?string $_anemie_suite_couches                         = null;
    public ?string $_incont_urin_suite_couches                    = null;
    public ?string $_depression_suite_couches                     = null;
    public ?string $_fract_obst_coccyx_suite_couches              = null;
    public ?string $_hemorragie_second_suite_couches              = null;
    public ?string $_retention_urinaire_suite_couches             = null;
    public ?string $_psychose_puerpuerale_suite_couches           = null;
    public ?string $_eclampsie_suite_couches                      = null;
    public ?string $_insuf_reinale_suite_couches                  = null;
    public ?string $_disjonction_symph_pub_suite_couches          = null;
    public ?string $_autre_comp_suite_couches                     = null;
    public ?string $_desc_autre_comp_suite_couches                = null;
    public ?string $_compl_anesth_suite_couches                   = null;
    public ?string $_compl_anesth_generale_suite_couches          = null;
    public ?string $_autre_compl_anesth_generale_suite_couches    = null;
    public ?string $_compl_anesth_locoregion_suite_couches        = null;
    public ?string $_autre_compl_anesth_locoregion_suite_couches  = null;
    public ?string $_traitements_sejour_mere                      = null;
    public ?string $_ttt_preventif_sejour_mere                    = null;
    public ?string $_antibio_preventif_sejour_mere                = null;
    public ?string $_desc_antibio_preventif_sejour_mere           = null;
    public ?string $_anticoag_preventif_sejour_mere               = null;
    public ?string $_desc_anticoag_preventif_sejour_mere          = null;
    public ?string $_antilactation_preventif_sejour_mere          = null;
    public ?string $_ttt_curatif_sejour_mere                      = null;
    public ?string $_antibio_curatif_sejour_mere                  = null;
    public ?string $_desc_antibio_curatif_sejour_mere             = null;
    public ?string $_anticoag_curatif_sejour_mere                 = null;
    public ?string $_desc_anticoag_curatif_sejour_mere            = null;
    public ?string $_vacc_gammaglob_sejour_mere                   = null;
    public ?string $_gammaglob_sejour_mere                        = null;
    public ?string $_vacc_sejour_mere                             = null;
    public ?string $_transfusion_sejour_mere                      = null;
    public ?string $_nb_unite_transfusion_sejour_mere             = null;
    public ?string $_interv_sejour_mere                           = null;
    public ?string $_datetime_interv_sejour_mere                  = null;
    public ?string $_revision_uterine_sejour_mere                 = null;
    public ?string $_interv_second_hemorr_sejour_mere             = null;
    public ?string $_type_interv_second_hemorr_sejour_mere        = null;
    public ?string $_autre_interv_sejour_mere                     = null;
    public ?string $_type_autre_interv_sejour_mere                = null;
    public ?string $_jour_deces_sejour_mere                       = null;
    public ?string $_deces_cause_obst_sejour_mere                 = null;
    public ?string $_autopsie_sejour_mere                         = null;
    public ?string $_resultat_autopsie_sejour_mere                = null;
    public ?string $_anomalie_autopsie_sejour_mere                = null;

    /** @var CGrossesse */
    public $_ref_grossesse;
    /** @var CSejour */
    public $_ref_sejour_accouchement;
    /** @var CConstantesMedicales */
    public $_ref_ant_mater_constantes;
    /** @var CConstantesMedicales */
    public $_ref_pere_constantes;
    /** @var CConstantesMedicales */
    public $_ref_adm_mater_constantes;
    /** @var CConstantesMedicales */
    public $_ref_fievre_travail_constantes;

    /** @var CConsultationPostNatale[] */
    public $_ref_consultations_post_natale;
    /** @var CAccouchement[] */
    public $_ref_accouchements;

    public ?ContextePsychoSocial                 $_ref_contexte_psycho_social            = null;
    public ?Toxicologie                          $_ref_toxicologie                       = null;
    public ?AntecedentsMaternels                 $_ref_antecedents_maternels             = null;
    public ?AntecedentsGynecologiques            $_ref_antecedents_gynecologiques        = null;
    public ?AntecedentsPaternels                 $_ref_antecedents_paternels             = null;
    public ?AntecedentsFamiliaux                 $_ref_antecedents_familiaux             = null;
    public ?AntecedentsObstetricaux              $_ref_antecedents_obstetricaux          = null;
    public ?DebutGrossesse                       $_ref_debut_grossesse                   = null;
    public ?PremierContact                       $_ref_premier_contact                   = null;
    public ?TransfertAntenatal                   $_ref_transfert_antenatal               = null;
    public ?ClotureSansAccouchement              $_ref_cloture_sans_accouchement         = null;
    public ?SyntheseSurveillance                 $_ref_synthese_surveillance             = null;
    public ?SyntheseToxicologie                  $_ref_synthese_toxicologie              = null;
    public ?SyntheseContextePsychoSocial         $_ref_synthese_contexte_psycho_social   = null;
    public ?SyntheseExamenGrossesse              $_ref_synthese_examen_grossesse         = null;
    public ?SyntheseImmunisation                 $_ref_synthese_immunisation             = null;
    public ?SynthesePathologieMaternelle         $_ref_synthese_pathologie_maternelle    = null;
    public ?SyntheseTherapeutiqueMaternelle      $_ref_synthese_therapeutique_maternelle = null;
    public ?SynthesePathologieFoetale            $_ref_synthese_pathologique_foetale     = null;
    public ?SyntheseTherapeutiqueFoetale         $_ref_synthese_therapeutique_foetale    = null;
    public ?CConduiteAccouchement                $_ref_conduite_accouchement             = null;
    public ?Admission                            $_ref_dossier_perinat_admission         = null;
    public ?AccouchementDeclenchementTravail     $_ref_declenchement_travail             = null;
    public ?AccouchementSurveillanceTravail      $_ref_surveillance_travail              = null;
    public ?AccouchementTherapeutiqueTravail     $_ref_therapeutique_travail             = null;
    public ?AccouchementPathologieAvantTravail   $_ref_pathologie_avant_travail          = null;
    public ?AccouchementPathologiePendantTravail $_ref_pathologie_pendant_travail        = null;
    public ?AccouchementAnesthesie               $_ref_accouchement_anesthesie           = null;
    public ?AccouchementDelivrance               $_ref_delivrance                        = null;
    public ?SyntheseSejour                       $_ref_synthese_sejour                   = null;

    // Organisation du dossier p�rinatal
    public $_listChapitres;

    /**
     * @see parent::getSpec()
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = 'dossier_perinat';
        $spec->key   = 'dossier_perinat_id';

        return $spec;
    }

    /**
     * @throws Exception
     * @see parent::getProps()
     */
    public function getProps(): array
    {
        $props                 = parent::getProps();
        $props['grossesse_id'] = 'ref notNull class|CGrossesse back|dossier_perinat cascade';

        $props['activite_pro']      = 'enum list|a|c|f|cp|e|i|r';
        $props['activite_pro_pere'] = 'enum list|a|c|f|cp|e|i|r';
        $props['fatigue_travail']   = 'bool';
        $props['travail_hebdo']     = 'num pos';
        $props['transport_jour']    = 'num pos';

        $props['ant_mater_constantes_id']             = 'ref class|CConstantesMedicales back|cstes_maternelles';
        $props['pere_constantes_id']                  = 'ref class|CConstantesMedicales back|cstes_paternelles';
        $props['consultant_premier_contact_id']       = 'ref class|CMediusers back|dossiers_perinat';
        $props['mater_provenance_premier_contact_id'] = 'ref class|CEtabExterne autocomplete|nom back|dossiers_perinat';

        $props['date_validation_synthese'] = 'date';
        $props['validateur_synthese_id']   = 'ref class|CMediusers back|syntheses_validees';

        $props['pathologie_grossesse'] = 'bool';

        $props['admission_id']            = 'ref class|CSejour back|dossiers_perinat';
        $props['adm_mater_constantes_id'] = 'ref class|CConstantesMedicales back|cstes_adm_mater';
        $props['adm_sage_femme_resp_id']  = 'ref class|CMediusers back|dossiers_perinat_adm';

        $props['lieu_accouchement']       = 'enum list|mater|dom|autremater|autre';
        $props['autre_lieu_accouchement'] = 'str';
        $props['nom_maternite_externe']   = 'str';
        $props['lieu_delivrance']         = 'str';
        $props['ag_accouchement']         = 'num';
        $props['ag_jours_accouchement']   = 'num';
        $props['mode_debut_travail']      = 'enum list|spon|decl|cesar';
        $props['rques_debut_travail']     = 'text helped';

        $props['pathologie_accouchement']      = 'bool';
        $props['fievre_pdt_travail']           = 'bool';
        $props['fievre_travail_constantes_id'] = 'ref class|CConstantesMedicales back|cstes_fievre_travail';

        $props['anesth_avant_naiss_par_id'] = 'ref class|CMediusers back|anesth_naissance';

        $this->setPropsFromComponents($props);

        return $props;
    }

    /**
     * @throws Exception
     */
    public function setPropsFromComponents(array &$props): void
    {
        foreach (ComponentRepository::getComponentClasses() as $component_class) {
            $component = new $component_class();
            foreach ($component->getProps() as $component_prop => $spec) {
                if (
                    ($component_prop[0] === '_')
                    || ($component_prop === $this->_spec->key)
                    || ($component_prop === $component->_spec->key)
                ) {
                    continue;
                }

                $props["_$component_prop"] = $spec;
            }
        }
    }

    /**
     * @return ComponentRepository
     */
    private function getComponentRepository(): ComponentRepository
    {
        if (!$this->component_repository) {
            $this->component_repository = new ComponentRepository($this);
        }

        return $this->component_repository;
    }

    /**
     * @inheritDoc
     */
    public function store(): ?string
    {
        if (!$this->_id) {
            $dossier               = new self();
            $dossier->grossesse_id = $this->grossesse_id;
            $this->_id             = $dossier->loadMatchingObject();
        }

        if ($msg = parent::store()) {
            return $msg;
        }


        $this->getComponentRepository();
        if (!$this->component_repository->storeComponents()) {
            return implode(', ', $this->component_repository->getMessages());
        }

        return null;
    }

    /**
     * Chargement de la grossesse
     *
     * @return CGrossesse
     * @throws Exception
     */
    public function loadRefGrossesse(): ?CGrossesse
    {
        return $this->_ref_grossesse = $this->loadFwdRef('grossesse_id', true);
    }

    /**
     * Chargement du s�jour de l'accouchement
     *
     * @return CSejour
     * @throws Exception
     */
    public function loadRefSejourAccouchement(): ?CSejour
    {
        return $this->_ref_sejour_accouchement = $this->loadFwdRef('admission_id', true);
    }

    /**
     * Chargement du relev�s de constantes des ant�c�dents maternels
     *
     * @return CConstantesMedicales
     * @throws Exception
     */
    public function loadRefConstantesAntecedentsMaternels(): ?CConstantesMedicales
    {
        return $this->_ref_ant_mater_constantes = $this->loadFwdRef('ant_mater_constantes_id');
    }

    /**
     * Chargement du relev�s de constantes des ant�c�dents paternels
     *
     * @return CConstantesMedicales
     * @throws Exception
     */
    public function loadRefConstantesAntecedentsPaternels()
    {
        return $this->_ref_pere_constantes = $this->loadFwdRef('pere_constantes_id');
    }

    /**
     * Chargement du relev�s de constantes maternels � l'examen d'entr�e
     *
     * @return CConstantesMedicales
     * @throws Exception
     */
    public function loadRefConstantesMaternelsAdmission(): ?CConstantesMedicales
    {
        return $this->_ref_adm_mater_constantes = $this->loadFwdRef('adm_mater_constantes_id');
    }

    /**
     * Chargement du relev�s de fi�vre pendant le travail
     *
     * @return CConstantesMedicales
     * @throws Exception
     */
    public function loadRefConstantesFievreTravail(): ?CConstantesMedicales
    {
        return $this->_ref_fievre_travail_constantes = $this->loadFwdRef('fievre_travail_constantes_id');
    }

    /**
     * Etat de remplissage d'un chapitre du dossier p�rinatal
     *
     * @throws Exception
     */
    public function loadEtatDossier(): void
    {
        $colors = [
            'ndef'    => 'transparent',
            'ok'      => 'lightCyan',
            'warning' => 'orange',
            'error'   => 'red',
        ];

        $this->_listChapitres = [
            'debut_grossesse'    => [
                'renseignements'  => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'depistages'      => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => '0',
                    'dev'   => 'ok',
                ],
                'antecedents'     => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'debut_grossesse' => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'premier_contact' => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
            ],
            'suivi_grossesse'    => [
                'tableau_suivi_grossesse'   => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => '0',
                    'dev'   => 'ok',
                ],
                'echographies'              => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => '0',
                    'dev'   => 'ok',
                ],
                'tableau_hospit_grossesse'  => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => '0',
                    'dev'   => 'ok',
                ],
                'transfert'                 => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'cloture_sans_accouchement' => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'synthese_grossesse'        => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'conduite_accouchement'     => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
            ],
            'accouchement'       => [
                'admission'           => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'partogramme'         => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'naissances'          => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => '0',
                    'dev'   => 'ok',
                ],
                'graphique_sspi'      => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'resume_accouchement' => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
            ],
            'suivi_accouchement' => [
                'nouveau_ne_salle_naissance' => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => '0',
                    'dev'   => 'ok',
                ],
                'examens_nouveau_ne'         => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => '0',
                    'dev'   => 'ok',
                ],
                'resume_sejour_nouveau_ne'   => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'resume_sejour_mere'         => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
                'consult_postnatale'         => [
                    'etat'  => 'empty',
                    'color' => $colors['ndef'],
                    'count' => 'NA',
                    'dev'   => 'ok',
                ],
            ],
        ];

        $this->loadComponents();

        $grossesse = $this->loadRefGrossesse();
        $grossesse->loadRefsGrossessesAnt();

        // Renseignements g�n�raux
        if (
            $this->activite_pro ||
            $this->activite_pro_pere ||
            $this->fatigue_travail ||
            $this->travail_hebdo ||
            $this->transport_jour
        ) {
            $this->_listChapitres['debut_grossesse']['renseignements']['color'] = $colors['ok'];
        }
        // D�pistages
        $this->_listChapitres['debut_grossesse']['depistages']['count'] = $grossesse->countBackRefs('depistages');
        if ($this->_listChapitres['debut_grossesse']['depistages']['count']) {
            $this->_listChapitres['debut_grossesse']['depistages']['color'] = $colors['ok'];
        }
        // Ant�c�dents
        if (
            $this->_patho_ant === '0' ||
            $this->_chir_ant === '0' ||
            $this->_gyneco_ant === '0' ||
            count($grossesse->_ref_grossesses_ant) > 0
        ) {
            $this->_listChapitres['debut_grossesse']['antecedents']['color'] = $colors['ok'];
        }
        if (
            $this->_patho_ant === '1' ||
            $this->_chir_ant === '1' ||
            $this->_gyneco_ant === '1'
        ) {
            $this->_listChapitres['debut_grossesse']['antecedents']['color'] = $colors['warning'];
        }
        // D�but de grossesse
        if ($this->_souhait_grossesse !== '') {
            $this->_listChapitres['debut_grossesse']['debut_grossesse']['color'] = $colors['ok'];
        }
        // Premier contact
        if ($this->_date_premier_contact) {
            $this->_listChapitres['debut_grossesse']['premier_contact']['color'] = $colors['ok'];
        }
        // Suivis de grossesse
        $this->_listChapitres['suivi_grossesse']['tableau_suivi_grossesse']['count'] = $grossesse->countBackRefs(
            'consultations'
        );
        if ($this->_listChapitres['suivi_grossesse']['tableau_suivi_grossesse']['count']) {
            $this->_listChapitres['suivi_grossesse']['tableau_suivi_grossesse']['color'] = $colors['ok'];
        }
        // Surveillance �chographique
        $this->_listChapitres['suivi_grossesse']['echographies']['count'] = $grossesse->countBackRefs('echographies');
        if ($this->_listChapitres['suivi_grossesse']['echographies']['count']) {
            $this->_listChapitres['suivi_grossesse']['echographies']['color'] = $colors['ok'];
        }
        // Hospitalisations
        $this->_listChapitres['suivi_grossesse']['tableau_hospit_grossesse']['count'] = $grossesse->countBackRefs(
            'sejours'
        );
        if ($this->_listChapitres['suivi_grossesse']['tableau_hospit_grossesse']['count']) {
            $this->_listChapitres['suivi_grossesse']['tableau_hospit_grossesse']['color'] = $colors['ok'];
        }
        // Transfert maternel ant�natal
        if ($this->_transf_antenat == 'n') {
            $this->_listChapitres['suivi_grossesse']['transfert']['color'] = $colors['ok'];
        }
        if ($this->_transf_antenat == 'imp') {
            $this->_listChapitres['suivi_grossesse']['transfert']['color'] = $colors['error'];
        }
        if ($this->_transf_antenat == 'reseau' || $this->_transf_antenat == 'hreseau') {
            $this->_listChapitres['suivi_grossesse']['transfert']['color'] = $colors['warning'];
        }
        // Cloture du dossier sans accouchement
        if ($this->_type_terminaison_grossesse) {
            $this->_listChapitres['suivi_grossesse']['cloture_sans_accouchement']['color'] = $colors['warning'];
        }
        // Synth�se de la grossesse
        if ($this->date_validation_synthese) {
            $this->_listChapitres['suivi_grossesse']['synthese_grossesse']['color'] = $colors['ok'];
        }
        // Conduite � tenir
        if ($this->_date_decision_conduite_a_tenir_acc) {
            $this->_listChapitres['suivi_grossesse']['conduite_accouchement']['color'] = $colors['ok'];
        }
        // Admission
        $sejour_accouchement = $this->loadRefSejourAccouchement();
        if ($sejour_accouchement->entree_reelle) {
            $this->_listChapitres['accouchement']['admission']['color'] = $colors['ok'];
        }
        // Diagramme d'accouchement
        if ($grossesse->datetime_debut_travail) {
            $this->_listChapitres['accouchement']['partogramme']['color'] = $colors['ok'];
        }
        // Naissances
        $this->_listChapitres['accouchement']['naissances']['count'] = $grossesse->countBackRefs('naissances');
        if ($this->_listChapitres['accouchement']['naissances']['count']) {
            $this->_listChapitres['accouchement']['naissances']['color'] = $colors['ok'];
        }
        // Surveillance de la m�re en salle de naissance
        $grossesse->loadLastSejour();
        $operation = new COperation();
        if ($grossesse->_ref_last_sejour) {
            $grossesse->_ref_last_sejour->loadRefsOperations();
            if ($grossesse->_ref_last_sejour->_ref_last_operation->_id) {
                $operation = $grossesse->_ref_last_sejour->_ref_last_operation;
            }
        }
        if ($operation->_id && $operation->graph_pack_sspi_id) {
            $this->_listChapitres['accouchement']['graphique_sspi']['color'] = $colors['ok'];
        }
        // R�sum� d'accouchement
        if ($this->lieu_accouchement) {
            $this->_listChapitres['accouchement']['resume_accouchement']['color'] = $colors['ok'];
        }
        // Nouveau-n� en salle de naissance
        $this->_listChapitres['suivi_accouchement']['nouveau_ne_salle_naissance']['count'] = $this->_listChapitres['accouchement']['naissances']['count'];
        $apgar1ok                                                                          = 0;
        $apgar1ko                                                                          = 0;
        $apgar3ok                                                                          = 0;
        $apgar3ko                                                                          = 0;
        $apgar5ok                                                                          = 0;
        $apgar5ko                                                                          = 0;
        $apgar10ok                                                                         = 0;
        $apgar10ko                                                                         = 0;
        foreach ($this->_ref_grossesse->loadRefsNaissances() as $naissance) {
            $naissance->loadRefEtat();
            if ($naissance->_ref_etat->_apgar_1 && $naissance->_ref_etat->_apgar_1 < 7) {
                $apgar1ko++;
            } elseif ($naissance->_ref_etat->_apgar_1) {
                $apgar1ok++;
            }
            if ($naissance->_ref_etat->_apgar_3 && $naissance->_ref_etat->_apgar_3 < 7) {
                $apgar3ko++;
            } elseif ($naissance->_ref_etat->_apgar_3) {
                $apgar3ok++;
            }
            if ($naissance->_ref_etat->_apgar_5 && $naissance->_ref_etat->_apgar_5 < 7) {
                $apgar5ko++;
            } elseif ($naissance->_ref_etat->_apgar_5) {
                $apgar5ok++;
            }
            if ($naissance->_ref_etat->_apgar_10 && $naissance->_ref_etat->_apgar_10 < 7) {
                $apgar10ko++;
            } elseif ($naissance->_ref_etat->_apgar_10) {
                $apgar10ok++;
            }
        }

        if ($apgar1ok == $this->_listChapitres['suivi_accouchement']['nouveau_ne_salle_naissance']['count'] && $this->_listChapitres['suivi_accouchement']['nouveau_ne_salle_naissance']['count'] != 0) {
            $this->_listChapitres['suivi_accouchement']['nouveau_ne_salle_naissance']['color'] = $colors['ok'];
        } elseif ($apgar1ko) {
            $this->_listChapitres['suivi_accouchement']['nouveau_ne_salle_naissance']['color'] = $colors['warning'];
        }
        // Examens du nouveau n�
        $this->_listChapitres['suivi_accouchement']['examens_nouveau_ne']['count'] = $grossesse->countBackRefs(
            'examens_nouveau_ne'
        );
        if ($this->_listChapitres['suivi_accouchement']['examens_nouveau_ne']['count']) {
            $this->_listChapitres['suivi_accouchement']['examens_nouveau_ne']['color'] = $colors['ok'];
        }
        // R�sum� du s�jour du nouveau n�
        foreach ($this->_ref_grossesse->loadRefsNaissances() as $naissance) {
            if ($naissance->loadRefSejourEnfant()->sortie_reelle) {
                $this->_listChapitres['suivi_accouchement']['resume_sejour_nouveau_ne']['color'] = $colors['ok'];
            }
        }
        // R�sum� du s�jour de la m�re
        if ($sejour_accouchement->sortie_reelle) {
            $this->_listChapitres['suivi_accouchement']['resume_sejour_mere']['color'] = $colors['ok'];
        }

        $this->loadRefsConsultationsPostNatale();
        // Consultation postnatale
        if (count($this->_ref_consultations_post_natale)) {
            $this->_listChapitres['suivi_accouchement']['consult_postnatale']['color'] = $colors['ok'];
        }
    }

    /**
     * Chargement des consultations post-natales
     *
     * @return CConsultationPostNatale[]|null
     * @throws Exception
     */
    public function loadRefsConsultationsPostNatale(): ?array
    {
        return $this->_ref_consultations_post_natale = $this->loadBackRefs('consultations_post_natales', 'date');
    }

    /**
     * Load the sub components of the object
     *
     * @return void
     * @throws Exception
     */
    public function loadComponents(): void
    {
        $this->loadRefContextePsychoSocial();
        $this->loadRefToxicologie();
        $this->loadRefAntecedentsMaternels();
        $this->loadRefAntecedentsGynecologiques();
        $this->loadRefAntecedentsPaternels();
        $this->loadRefAntecedentsFamiliaux();
        $this->loadRefAntecedentsObstetricaux();
        $this->loadRefDebutGrossesse();
        $this->loadRefPremierContact();
        $this->loadRefTransfertAntenatal();
        $this->loadRefClotureSansAccouchement();
        $this->loadRefSyntheseSurveillance();
        $this->loadRefSyntheseToxicologie();
        $this->loadRefSyntheseContextePsychoSocial();
        $this->loadRefSyntheseExamenGrossesse();
        $this->loadRefSyntheseImmunisation();
        $this->loadRefSynthesePathologieMaternelle();
        $this->loadRefSyntheseTherapeutiqueMaternelle();
        $this->loadRefSynthesePathologieFoetale();
        $this->loadRefSyntheseTherapeutiqueFoetale();
        $this->loadRefConduiteAccouchement();
        $this->loadRefAdmission();
        $this->loadRefAccouchementDeclenchementTravail();
        $this->loadRefAccouchementSurveillanceTravail();
        $this->loadRefAccouchementTherapeutiqueTravail();
        $this->loadRefAccouchementPathologieAvantTravail();
        $this->loadRefAccouchementPathologiePendantTravail();
        $this->loadRefAccouchementAnesthesie();
        $this->loadRefAccouchementDelivrance();
        $this->loadRefSyntheseSejour();
    }

    /**
     * @throws Exception
     */
    public function loadRefContextePsychoSocial(): ?ContextePsychoSocial
    {
        return $this->loadRefComponent('_ref_contexte_psycho_social', ContextePsychoSocial::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefToxicologie(): ?Toxicologie
    {
        return $this->loadRefComponent('_ref_toxicologie', Toxicologie::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAntecedentsMaternels(): ?AntecedentsMaternels
    {
        return $this->loadRefComponent('_ref_antecedents_maternels', AntecedentsMaternels::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAntecedentsGynecologiques(): ?AntecedentsGynecologiques
    {
        return $this->loadRefComponent('_ref_antecedents_gynecologiques', AntecedentsGynecologiques::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAntecedentsPaternels(): ?AntecedentsPaternels
    {
        return $this->loadRefComponent('_ref_antecedents_paternels', AntecedentsPaternels::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAntecedentsFamiliaux(): ?AntecedentsFamiliaux
    {
        return $this->loadRefComponent('_ref_antecedents_familiaux', AntecedentsFamiliaux::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAntecedentsObstetricaux(): ?AntecedentsObstetricaux
    {
        return $this->loadRefComponent('_ref_antecedents_obstetricaux', AntecedentsObstetricaux::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefDebutGrossesse(): ?DebutGrossesse
    {
        return $this->loadRefComponent('_ref_debut_grossesse', DebutGrossesse::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefPremierContact(): ?PremierContact
    {
        return $this->loadRefComponent('_ref_premier_contact', PremierContact::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefTransfertAntenatal(): ?TransfertAntenatal
    {
        return $this->loadRefComponent('_ref_transfert_antenatal', TransfertAntenatal::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefClotureSansAccouchement(): ?ClotureSansAccouchement
    {
        return $this->loadRefComponent('_ref_cloture_sans_accouchement', ClotureSansAccouchement::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefSyntheseSurveillance(): ?SyntheseSurveillance
    {
        return $this->loadRefComponent('_ref_synthese_surveillance', SyntheseSurveillance::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefSyntheseToxicologie(): ?SyntheseToxicologie
    {
        return $this->loadRefComponent('_ref_synthese_toxicologie', SyntheseToxicologie::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefSyntheseContextePsychoSocial(): ?SyntheseContextePsychoSocial
    {
        return $this->loadRefComponent(
            '_ref_synthese_contexte_psycho_social',
            SyntheseContextePsychoSocial::RESOURCE_TYPE
        );
    }

    /**
     * @throws Exception
     */
    public function loadRefSyntheseExamenGrossesse(): ?SyntheseExamenGrossesse
    {
        return $this->loadRefComponent('_ref_synthese_examen_grossesse', SyntheseExamenGrossesse::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefSyntheseImmunisation(): ?SyntheseImmunisation
    {
        return $this->loadRefComponent('_ref_synthese_immunisation', SyntheseImmunisation::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefSynthesePathologieMaternelle(): ?SynthesePathologieMaternelle
    {
        return $this->loadRefComponent(
            '_ref_synthese_pathologie_maternelle',
            SynthesePathologieMaternelle::RESOURCE_TYPE
        );
    }

    /**
     * @throws Exception
     */
    public function loadRefSyntheseTherapeutiqueMaternelle(): ?SyntheseTherapeutiqueMaternelle
    {
        return $this->loadRefComponent(
            '_ref_synthese_therapeutique_maternelle',
            SyntheseTherapeutiqueMaternelle::RESOURCE_TYPE
        );
    }

    /**
     * @throws Exception
     */
    public function loadRefSynthesePathologieFoetale(): ?SynthesePathologieFoetale
    {
        return $this->loadRefComponent(
            '_ref_synthese_pathologique_foetale',
            SynthesePathologieFoetale::RESOURCE_TYPE
        );
    }

    /**
     * @throws Exception
     */
    public function loadRefSyntheseTherapeutiqueFoetale(): ?SyntheseTherapeutiqueFoetale
    {
        return $this->loadRefComponent(
            '_ref_synthese_therapeutique_foetale',
            SyntheseTherapeutiqueFoetale::RESOURCE_TYPE
        );
    }

    /**
     * @throws Exception
     */
    public function loadRefConduiteAccouchement(): ?CConduiteAccouchement
    {
        return $this->loadRefComponent('_ref_conduite_accouchement', CConduiteAccouchement::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAdmission(): ?Admission
    {
        return $this->loadRefComponent('_ref_dossier_perinat_admission', Admission::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAccouchementDeclenchementTravail(): ?AccouchementDeclenchementTravail
    {
        return $this->loadRefComponent('_ref_declenchement_travail', AccouchementDeclenchementTravail::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAccouchementSurveillanceTravail(): ?AccouchementSurveillanceTravail
    {
        return $this->loadRefComponent('_ref_surveillance_travail', AccouchementSurveillanceTravail::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAccouchementTherapeutiqueTravail(): ?AccouchementTherapeutiqueTravail
    {
        return $this->loadRefComponent('_ref_therapeutique_travail', AccouchementTherapeutiqueTravail::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAccouchementPathologieAvantTravail(): ?AccouchementPathologieAvantTravail
    {
        return $this->loadRefComponent(
            '_ref_pathologie_avant_travail',
            AccouchementPathologieAvantTravail::RESOURCE_TYPE
        );
    }

    /**
     * @throws Exception
     */
    public function loadRefAccouchementPathologiePendantTravail(): ?AccouchementPathologiePendantTravail
    {
        return $this->loadRefComponent(
            '_ref_pathologie_pendant_travail',
            AccouchementPathologiePendantTravail::RESOURCE_TYPE
        );
    }

    /**
     * @throws Exception
     */
    public function loadRefAccouchementAnesthesie(): ?AccouchementAnesthesie
    {
        return $this->loadRefComponent('_ref_accouchement_anesthesie', AccouchementAnesthesie::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefAccouchementDelivrance(): ?AccouchementDelivrance
    {
        return $this->loadRefComponent('_ref_delivrance', AccouchementDelivrance::RESOURCE_TYPE);
    }

    /**
     * @throws Exception
     */
    public function loadRefSyntheseSejour(): ?SyntheseSejour
    {
        return $this->loadRefComponent('_ref_synthese_sejour', SyntheseSejour::RESOURCE_TYPE);
    }

    /**
     * @param string $ref_field
     * @param string $resource
     *
     * @return AbstractComponent|null
     * @throws Exception
     */
    private function loadRefComponent(string $ref_field, string $resource): ?AbstractComponent
    {
        if (!property_exists($this, $ref_field)) {
            return null;
        }

        if (!$this->$ref_field) {
            $this->getComponentRepository();
            $this->$ref_field = $this->component_repository->loadComponent($resource);
        }

        return $this->$ref_field;
    }

    /**
     * Chargement une consultation post natale vierge
     *
     * @return CConsultationPostNatale
     */
    public static function emptyConsultationPostNatale(): CConsultationPostNatale
    {
        $consult_postnatale = new CConsultationPostNatale();
        $consult_postnatale->loadRefConstantesMaternelles();

        return $consult_postnatale;
    }

    /**
     * Chargement des accouchements
     *
     * @return CAccouchement[]|null
     * @throws Exception
     */
    public function loadRefsAccouchement(): ?array
    {
        return $this->_ref_accouchements = $this->loadBackRefs('accouchements', 'date');
    }

    /**
     * Get the mother's pathologies fields (perinatal folder)
     *
     * @param string|null $keywords
     *
     * @return array
     */
    public function getMotherPathologiesFields(?string $keywords = null): array
    {
        $bool_pathologies_fields = [
            'metrorragie_1er_trim'                 => CAppUI::tr('CDossierPerinat-metrorragie_1er_trim'),
            'metrorragie_2e_3e_trim'               => CAppUI::tr('CDossierPerinat-metrorragie_2e_3e_trim'),
            'menace_acc_premat'                    => CAppUI::tr('CDossierPerinat-menace_acc_premat'),
            'rupture_premat_membranes'             => CAppUI::tr('CDossierPerinat-rupture_premat_membranes'),
            'anomalie_liquide_amniotique'          => CAppUI::tr('CDossierPerinat-anomalie_liquide_amniotique'),
            'autre_patho_gravidique'               => CAppUI::tr('CDossierPerinat-autre_patho_gravidique'),
            'patho_grav_vomissements'              => CAppUI::tr('CDossierPerinat-patho_grav_vomissements'),
            'patho_grav_herpes_gest'               => CAppUI::tr('CDossierPerinat-patho_grav_herpes_gest'),
            'patho_grav_dermatose_pup'             => CAppUI::tr('CDossierPerinat-patho_grav_dermatose_pup'),
            'patho_grav_placenta_praevia_non_hemo' => CAppUI::tr(
                'CDossierPerinat-patho_grav_placenta_praevia_non_hemo'
            ),
            'patho_grav_chorio_amniotite'          => CAppUI::tr('CDossierPerinat-patho_grav_chorio_amniotite'),
            'patho_grav_transf_foeto_mat'          => CAppUI::tr('CDossierPerinat-patho_grav_transf_foeto_mat'),
            'patho_grav_beance_col'                => CAppUI::tr('CDossierPerinat-patho_grav_beance_col'),
            'patho_grav_cerclage'                  => CAppUI::tr('CDossierPerinat-patho_grav_cerclage'),
            'hypertension_arterielle'              => CAppUI::tr('CDossierPerinat-hypertension_arterielle'),
            'proteinurie'                          => CAppUI::tr('CDossierPerinat-proteinurie'),
            'diabete'                              => CAppUI::tr('CDossierPerinat-diabete'),
            'infection_urinaire'                   => CAppUI::tr('CDossierPerinat-infection_urinaire'),
            'infection_cervico_vaginale'           => CAppUI::tr('CDossierPerinat-infection_cervico_vaginale'),
            'anemie_mat_pdt_grossesse'             => CAppUI::tr('CDossierPerinat-anemie_mat_pdt_grossesse'),
            'tombopenie_mat_pdt_grossesse'         => CAppUI::tr('CDossierPerinat-tombopenie_mat_pdt_grossesse'),
            'faible_prise_poid_mat_pdt_grossesse'  => CAppUI::tr('CDossierPerinat-faible_prise_poid_mat_pdt_grossesse'),
            'malnut_mat_pdt_grossesse'             => CAppUI::tr('CDossierPerinat-malnut_mat_pdt_grossesse'),
            'cholestase_mat_pdt_grossesse'         => CAppUI::tr('CDossierPerinat-cholestase_mat_pdt_grossesse'),
            'steatose_hep_mat_pdt_grossesse'       => CAppUI::tr('CDossierPerinat-steatose_hep_mat_pdt_grossesse'),
            'thrombophl_sup_mat_pdt_grossesse'     => CAppUI::tr('CDossierPerinat-thrombophl_sup_mat_pdt_grossesse'),
            'thrombophl_prof_mat_pdt_grossesse'    => CAppUI::tr('CDossierPerinat-thrombophl_prof_mat_pdt_grossesse'),
            'asthme_mat_pdt_grossesse'             => CAppUI::tr('CDossierPerinat-asthme_mat_pdt_grossesse'),
            'cardiopathie_mat_pdt_grossesse'       => CAppUI::tr('CDossierPerinat-cardiopathie_mat_pdt_grossesse'),
            'epilepsie_mat_pdt_grossesse'          => CAppUI::tr('CDossierPerinat-epilepsie_mat_pdt_grossesse'),
            'depression_mat_pdt_grossesse'         => CAppUI::tr('CDossierPerinat-depression_mat_pdt_grossesse'),
            'patho_gyneco_mat_pdt_grossesse'       => CAppUI::tr('CDossierPerinat-patho_gyneco_mat_pdt_grossesse'),
            'mst_mat_pdt_grossesse'                => CAppUI::tr('CDossierPerinat-mst_mat_pdt_grossesse'),
            'synd_douleur_abdo_mat_pdt_grossesse'  => CAppUI::tr('CDossierPerinat-synd_douleur_abdo_mat_pdt_grossesse'),
            'synd_infect_mat_pdt_grossesse'        => CAppUI::tr('CDossierPerinat-synd_infect_mat_pdt_grossesse'),
        ];

        foreach ($bool_pathologies_fields as $patho_key => $patho_name) {
            if ($this->$patho_key) {
                unset($bool_pathologies_fields[$patho_key]);
            }
        }

        if ($keywords) {
            foreach ($bool_pathologies_fields as $patho_key => $patho_name) {
                if (strpos(strtolower($patho_name), strtolower($keywords)) === false) {
                    unset($bool_pathologies_fields[$patho_key]);
                }
            }
        }

        ksort($bool_pathologies_fields);

        return $bool_pathologies_fields;
    }

    /**
     * Chargement un accouchement vierge
     *
     * @return CAccouchement
     */
    public static function emptyAccouchement(): CAccouchement
    {
        return new CAccouchement();
    }

    /**
     * @see parent::fillLimitedTemplate()
     */
    public function fillLimitedTemplate(&$template, $prefix = null)
    {
        $dossier_perinatal_section = CAppUI::tr('CDossierPerinat');

        $this->loadComponents();

        foreach ($this->_props as $_field => $_prop) {
            // Skip fields
            if (in_array($_field, self::FIELDS_MODELE_SKIPPED) || !property_exists($this, $_field)) {
                continue;
            }

            $value = $this->$_field;

            // Only for boolean
            if (strpos($_prop, 'bool') !== false) {
                if ($value == 1) {
                    $value = CAppUI::tr('common-Yes');
                } elseif ($value == 0) {
                    $value = CAppUI::tr('common-No');
                }
            }

            // Only for enum
            if (strpos($_prop, 'enum') !== false) {
                $value = CAppUI::tr("CDossierPerinat.$_field.$value");
            }

            $template->addProperty(
                "$prefix - $dossier_perinatal_section - " . CAppUI::tr("CDossierPerinat-$_field"),
                $value
            );
        }
    }
}
