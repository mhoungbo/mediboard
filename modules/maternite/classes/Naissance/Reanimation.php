<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du volet R�animation de la partie Salle de naissance du dossier p�rinatal
 */
class Reanimation extends CMbObject
{
    /** @var string Primary key */
    public ?string $naissance_reanimation_id = null;

    public ?string $naissance_id = null;

    public ?string $reanimation = null;
    public ?string $rea_aspi_laryngo = null;
    public ?string $rea_ventil_masque = null;
    public ?string $rea_o2_sonde = null;
    public ?string $rea_ppc_nasale = null;
    public ?string $rea_duree_ppc_nasale = null;
    public ?string $rea_ventil_tube_endo = null;
    public ?string $rea_duree_ventil_tube_endo = null;
    public ?string $rea_intub_tracheale = null;
    public ?string $rea_min_vie_intub_tracheale = null;
    public ?string $rea_massage_card = null;
    public ?string $rea_injection_medic = null;
    public ?string $rea_injection_medic_adre = null;
    public ?string $rea_injection_medic_surfa = null;
    public ?string $rea_injection_medic_gluc = null;
    public ?string $rea_injection_medic_autre = null;
    public ?string $rea_injection_medic_autre_desc = null;
    public ?string $rea_autre_geste = null;
    public ?string $rea_autre_geste_desc = null;
    public ?string $duree_totale_rea = null;
    public ?string $temp_fin_rea = null;
    public ?string $gly_fin_rea = null;
    public ?string $etat_fin_rea = null;
    public ?string $rea_remarques = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_reanimations';
        $spec->key   = 'naissance_reanimation_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id']        = 'ref class|CNaissance cascade notNull back|reanimation';

        $props['reanimation']                    = 'bool';
        $props['rea_aspi_laryngo']               = 'bool';
        $props['rea_ventil_masque']              = 'bool';
        $props['rea_o2_sonde']                   = 'bool';
        $props['rea_ppc_nasale']                 = 'bool';
        $props['rea_duree_ppc_nasale']           = 'num min|0 max|9999';
        $props['rea_ventil_tube_endo']           = 'bool';
        $props['rea_duree_ventil_tube_endo']     = 'num min|0 max|9999';
        $props['rea_intub_tracheale']            = 'bool';
        $props['rea_min_vie_intub_tracheale']    = 'num min|0 max|9999';
        $props['rea_massage_card']               = 'bool';
        $props['rea_injection_medic']            = 'bool';
        $props['rea_injection_medic_adre']       = 'bool';
        $props['rea_injection_medic_surfa']      = 'bool';
        $props['rea_injection_medic_gluc']       = 'bool';
        $props['rea_injection_medic_autre']      = 'bool';
        $props['rea_injection_medic_autre_desc'] = 'str';
        $props['rea_autre_geste']                = 'bool';
        $props['rea_autre_geste_desc']           = 'str';
        $props['duree_totale_rea']               = 'num min|0 max|9999';
        $props['temp_fin_rea']                   = 'float';
        $props['gly_fin_rea']                    = 'float';
        $props['etat_fin_rea']                   = 'enum list|sat|surv|mut|transf';
        $props['rea_remarques']                  = 'text';

        return $props;
    }
}
