<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du volet Actes du r�sum� du s�jour du nouveau-n�
 */
class ActeMedical extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $acte_id = null;

    public ?string $naissance_id = null;

    public ?string $actes_effectues = null;
    public ?string $caryotype = null;
    public ?string $etf = null;
    public ?string $eeg = null;
    public ?string $ecg = null;
    public ?string $fond_oeil = null;
    public ?string $antibiotherapie = null;
    public ?string $oxygenotherapie = null;
    public ?string $echographie_cardiaque = null;
    public ?string $echographie_cerebrale = null;
    public ?string $echographie_hanche = null;
    public ?string $echographie_hepatique = null;
    public ?string $echographie_reinale = null;
    public ?string $exsanguino_transfusion = null;
    public ?string $intubation = null;
    public ?string $incubateur = null;
    public ?string $injection_gamma_globulines = null;
    public ?string $togd = null;
    public ?string $radio_thoracique = null;
    public ?string $reeducation = null;
    public ?string $autre_acte = null;
    public ?string $autre_acte_desc = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_actes_medicaux';
        $spec->key   = 'acte_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance cascade notNull back|acte';

        $props['actes_effectues']            = 'bool';
        $props['caryotype']                  = 'bool';
        $props['etf']                        = 'bool';
        $props['eeg']                        = 'bool';
        $props['ecg']                        = 'bool';
        $props['fond_oeil']                  = 'bool';
        $props['antibiotherapie']            = 'bool';
        $props['oxygenotherapie']            = 'bool';
        $props['echographie_cardiaque']      = 'bool';
        $props['echographie_cerebrale']      = 'bool';
        $props['echographie_hanche']         = 'bool';
        $props['echographie_hepatique']      = 'bool';
        $props['echographie_reinale']        = 'bool';
        $props['exsanguino_transfusion']     = 'bool';
        $props['intubation']                 = 'bool';
        $props['incubateur']                 = 'bool';
        $props['injection_gamma_globulines'] = 'bool';
        $props['togd']                       = 'bool';
        $props['radio_thoracique']           = 'bool';
        $props['reeducation']                = 'bool';
        $props['autre_acte']                 = 'bool';
        $props['autre_acte_desc']            = 'str';

        return $props;
    }
}
