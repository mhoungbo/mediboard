<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es concernant les troubles de la r�gulation thermique du volet Pathologies du r�sum� du s�jour du nouveau-n�
 */
class RegulationThermique extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $regulation_thermique_id = null;

    public ?string $naissance_id = null;

    public ?string $trouble_regul_thermique = null;
    public ?string $hyperthermie = null;
    public ?string $hypothermie_legere = null;
    public ?string $hypothermie_grave = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_regulations_thermiques';
        $spec->key   = 'regulation_thermique_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance notNull cascade back|regulation_thermique';

        $props['trouble_regul_thermique']         = 'bool';
        $props['hyperthermie']                    = 'bool';
        $props['hypothermie_legere']              = 'bool';
        $props['hypothermie_grave']               = 'bool';

        return $props;
    }
}
