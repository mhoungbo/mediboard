<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du volet Monitorage de la partie Salle de naissance du dossier p�rinatal
 */
class Monitorage extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $monitorage_id = null;

    public ?string $naissance_id = null;

    public ?string $monitorage                = null;
    public ?string $frequence_cardiaque = null;
    public ?string $saturation          = null;
    public ?string $glycemie            = null;
    public ?string $incubateur          = null;
    public ?string $remarques           = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_monitorages';
        $spec->key   = 'monitorage_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance notNull cascade back|monitorage';

        $props['monitorage']                = 'bool';
        $props['frequence_cardiaque'] = 'bool';
        $props['saturation']          = 'bool';
        $props['glycemie']            = 'bool';
        $props['incubateur']          = 'bool';
        $props['remarques']           = 'text';

        return $props;
    }
}
