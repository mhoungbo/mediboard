<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es concernant les anomalies cong�nitales du volet Pathologies du r�sum� du s�jour du nouveau-n�
 */
class AnomalieCongenitale extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $anomalie_congenitale_id = null;

    public ?string $naissance_id = null;

    public ?string $anom_cong = null;
    public ?string $isolee = null;
    public ?string $synd_polyformatif = null;
    public ?string $tube_neural = null;
    public ?string $fente_labio_palatine = null;
    public ?string $atresie_oesophage = null;
    public ?string $omphalocele = null;
    public ?string $reduc_absence_membres = null;
    public ?string $hydrocephalie = null;
    public ?string $hydrocephalie_type = null;
    public ?string $malform_card = null;
    public ?string $malform_card_type = null;
    public ?string $hanches_luxables = null;
    public ?string $hanches_luxables_type = null;
    public ?string $malform_reinale = null;
    public ?string $malform_reinale_type = null;
    public ?string $autre = null;
    public ?string $autre_desc = null;
    public ?string $chromosomique = null;
    public ?string $genique = null;
    public ?string $trisomie_21 = null;
    public ?string $trisomie_type = null;
    public ?string $chrom_gen_autre = null;
    public ?string $chrom_gen_autre_desc = null;
    public ?string $description_clair = null;
    public ?string $moment_diag = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_anomalies_congenitales';
        $spec->key   = 'anomalie_congenitale_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance cascade notNull back|anomalie_congenitale';

        $props['anom_cong']             = 'bool';
        $props['isolee']                = 'bool';
        $props['synd_polyformatif']     = 'bool';
        $props['tube_neural']           = 'bool';
        $props['fente_labio_palatine']  = 'bool';
        $props['atresie_oesophage']     = 'bool';
        $props['omphalocele']           = 'bool';
        $props['reduc_absence_membres'] = 'bool';
        $props['hydrocephalie']         = 'bool';
        $props['hydrocephalie_type']    = 'enum list|susp|cert';
        $props['malform_card']          = 'bool';
        $props['malform_card_type']     = 'enum list|susp|cert';
        $props['hanches_luxables']      = 'bool';
        $props['hanches_luxables_type'] = 'enum list|susp|cert';
        $props['malform_reinale']       = 'bool';
        $props['malform_reinale_type']  = 'enum list|susp|cert';
        $props['autre']                 = 'bool';
        $props['autre_desc']            = 'str';
        $props['chromosomique']         = 'bool';
        $props['genique']               = 'bool';
        $props['trisomie_21']           = 'bool';
        $props['trisomie_type']         = 'enum list|susp|cert';
        $props['chrom_gen_autre']       = 'bool';
        $props['chrom_gen_autre_desc']  = 'str';
        $props['description_clair']     = 'text';
        $props['moment_diag']           = 'enum list|antenat|neonat|autop';

        return $props;
    }
}
