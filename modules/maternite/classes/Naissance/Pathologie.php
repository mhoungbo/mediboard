<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es concernant les pathologies diverses du volet Pathologies du r�sum� du s�jour du nouveau-n�
 */
class Pathologie extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $pathologie_id = null;

    public ?string $naissance_id = null;

    public ?string $autre_pathologie = null;
    public ?string $patho_resp = null;
    public ?string $tachypnee = null;
    public ?string $autre_detresse_resp_neonat = null;
    public ?string $acces_cyanose = null;
    public ?string $apnees_prema = null;
    public ?string $apnees_autre = null;
    public ?string $inhalation_meco_sans_pneumopath = null;
    public ?string $inhalation_meco_avec_pneumopath = null;
    public ?string $inhalation_lait = null;
    public ?string $patho_cardiovasc = null;
    public ?string $trouble_du_rythme = null;
    public ?string $hypertonie_vagale = null;
    public ?string $souffle_a_explorer = null;
    public ?string $patho_neuro = null;
    public ?string $hypothonie = null;
    public ?string $hypertonie = null;
    public ?string $irrit_cerebrale = null;
    public ?string $mouv_anormaux = null;
    public ?string $convulsions = null;
    public ?string $patho_dig = null;
    public ?string $alim_sein_difficile = null;
    public ?string $alim_lente = null;
    public ?string $stagnation_pond = null;
    public ?string $perte_poids_sup_10_pourc = null;
    public ?string $regurgitations = null;
    public ?string $vomissements = null;
    public ?string $reflux_gatro_eoso = null;
    public ?string $oesophagite = null;
    public ?string $hematemese = null;
    public ?string $synd_occlusif = null;
    public ?string $trouble_succion_deglut = null;
    public ?string $patho_hemato = null;
    public ?string $anemie_neonat = null;
    public ?string $anemie_transf_foeto_mat = null;
    public ?string $anemie_transf_foeto_foet = null;
    public ?string $drepano_positif = null;
    public ?string $maladie_hemo = null;
    public ?string $thrombopenie = null;
    public ?string $patho_metab = null;
    public ?string $hypogly_diab_mere_gest = null;
    public ?string $hypogly_diab_mere_nid = null;
    public ?string $hypogly_diab_mere_id = null;
    public ?string $hypogly_neonat_transitoire = null;
    public ?string $hypocalcemie = null;
    public ?string $intoxication = null;
    public ?string $synd_sevrage_toxico = null;
    public ?string $synd_sevrage_medic = null;
    public ?string $tabac_maternel = null;
    public ?string $alcool_maternel = null;
    public ?string $autre_patho_autre = null;
    public ?string $rhinite_neonat = null;
    public ?string $patho_dermato = null;
    public ?string $autre_patho_autre_thesaurus = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_pathologies';
        $spec->key   = 'pathologie_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance cascade notNull back|pathologie';

        $props['autre_pathologie']                = 'bool';
        $props['patho_resp']                      = 'bool';
        $props['tachypnee']                       = 'bool';
        $props['autre_detresse_resp_neonat']      = 'bool';
        $props['acces_cyanose']                   = 'bool';
        $props['apnees_prema']                    = 'bool';
        $props['apnees_autre']                    = 'bool';
        $props['inhalation_meco_sans_pneumopath'] = 'bool';
        $props['inhalation_meco_avec_pneumopath'] = 'bool';
        $props['inhalation_lait']                 = 'bool';
        $props['patho_cardiovasc']                = 'bool';
        $props['trouble_du_rythme']               = 'bool';
        $props['hypertonie_vagale']               = 'bool';
        $props['souffle_a_explorer']              = 'bool';
        $props['patho_neuro']                     = 'bool';
        $props['hypothonie']                      = 'bool';
        $props['hypertonie']                      = 'bool';
        $props['irrit_cerebrale']                 = 'bool';
        $props['mouv_anormaux']                   = 'bool';
        $props['convulsions']                     = 'bool';
        $props['patho_dig']                       = 'bool';
        $props['alim_sein_difficile']             = 'bool';
        $props['alim_lente']                      = 'bool';
        $props['stagnation_pond']                 = 'bool';
        $props['perte_poids_sup_10_pourc']        = 'bool';
        $props['regurgitations']                  = 'bool';
        $props['vomissements']                    = 'bool';
        $props['reflux_gatro_eoso']               = 'bool';
        $props['oesophagite']                     = 'bool';
        $props['hematemese']                      = 'bool';
        $props['synd_occlusif']                   = 'bool';
        $props['trouble_succion_deglut']          = 'bool';
        $props['patho_hemato']                    = 'bool';
        $props['anemie_neonat']                   = 'bool';
        $props['anemie_transf_foeto_mat']         = 'bool';
        $props['anemie_transf_foeto_foet']        = 'bool';
        $props['drepano_positif']                 = 'bool';
        $props['maladie_hemo']                    = 'bool';
        $props['thrombopenie']                    = 'bool';
        $props['patho_metab']                     = 'bool';
        $props['hypogly_diab_mere_gest']          = 'bool';
        $props['hypogly_diab_mere_nid']           = 'bool';
        $props['hypogly_diab_mere_id']            = 'bool';
        $props['hypogly_neonat_transitoire']      = 'bool';
        $props['hypocalcemie']                    = 'bool';
        $props['intoxication']                    = 'bool';
        $props['synd_sevrage_toxico']             = 'bool';
        $props['synd_sevrage_medic']              = 'bool';
        $props['tabac_maternel']                  = 'bool';
        $props['alcool_maternel']                 = 'bool';
        $props['autre_patho_autre']               = 'bool';
        $props['rhinite_neonat']                  = 'bool';
        $props['patho_dermato']                   = 'bool';
        $props['autre_patho_autre_thesaurus']      = 'bool';

        return $props;
    }
}
