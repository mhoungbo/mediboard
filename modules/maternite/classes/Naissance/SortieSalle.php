<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du volet Sortie de salle de la partie Salle de naissance du dossier p�rinatal
 */
class SortieSalle extends CMbObject
{
    /** @var string Primary key */
    public ?string $naissance_sortie_salle_id = null;

    public ?string $naissance_id = null;

    public ?string $mode_sortie = null;
    public ?string $mode_sortie_autre = null;
    public ?string $min_vie_transmut = null;
    public ?string $resp_transmut_id = null;
    public ?string $motif_transmut = null;
    public ?string $detail_motif_transmut = null;
    public ?string $lieu_transf = null;
    public ?string $type_etab_transf = null;
    public ?string $dest_transf = null;
    public ?string $dest_transf_autre = null;
    public ?string $mode_transf = null;
    public ?string $delai_appel_arrivee_transp = null;
    public ?string $dist_mater_transf = null;
    public ?string $raison_transf_report = null;
    public ?string $raison_transf_report_autre = null;
    public ?string $remarques_transf = null;


    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_sorties_salles';
        $spec->key   = 'naissance_sortie_salle_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance cascade notNull back|sortie_salle';

        $props['mode_sortie']                = 'enum list|mere|mut|transfres|transfhres|deces|autre';
        $props['mode_sortie_autre']          = 'str';
        $props['min_vie_transmut']           = 'num min|0 max|9999';
        $props['resp_transmut_id']           = 'ref class|CMediusers back|naissances_transmut';
        $props['motif_transmut']             = 'enum list|prema|hypotroph|detrespi|risqueinfect|malform|autrepatho';
        $props['detail_motif_transmut']      = 'str';
        $props['lieu_transf']                = 'str';
        $props['type_etab_transf']           = 'enum list|1|2';
        $props['dest_transf']                = 'enum list|rea|intens|neonat|chir|neonatmater|autre';
        $props['dest_transf_autre']          = 'str';
        $props['mode_transf']                = 'enum list|intra|extra|samuped|samuns|ambulance|voiture';
        $props['delai_appel_arrivee_transp'] = 'num min|0 max|9999';
        $props['dist_mater_transf']          = 'num min|0 max|9999';
        $props['raison_transf_report']       = 'enum list|place|transp|autre';
        $props['raison_transf_report_autre'] = 'str';
        $props['remarques_transf']           = 'text';

        return $props;
    }
}
