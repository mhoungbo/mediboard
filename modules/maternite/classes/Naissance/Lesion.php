<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es concernant les l�sions traumatiques du volet Pathologies du r�sum� du s�jour du nouveau-n�
 */
class Lesion extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $lesion_id = null;

    public ?string $naissance_id = null;

    public ?string $lesion_traumatique = null;
    public ?string $lesion_faciale = null;
    public ?string $paralysie_faciale = null;
    public ?string $cephalhematome = null;
    public ?string $paralysie_plexus_brachial_sup = null;
    public ?string $paralysie_plexus_brachial_inf = null;
    public ?string $lesion_cuir_chevelu = null;
    public ?string $fracture_clavicule = null;
    public ?string $autre_lesion = null;
    public ?string $autre_lesion_desc = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_lesions';
        $spec->key   = 'lesion_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance notNull cascade back|lesion';

        $props['lesion_traumatique']              = 'bool';
        $props['lesion_faciale']                  = 'bool';
        $props['paralysie_faciale']               = 'bool';
        $props['cephalhematome']                  = 'bool';
        $props['paralysie_plexus_brachial_sup']   = 'bool';
        $props['paralysie_plexus_brachial_inf']   = 'bool';
        $props['lesion_cuir_chevelu']             = 'bool';
        $props['fracture_clavicule']              = 'bool';
        $props['autre_lesion']                    = 'bool';
        $props['autre_lesion_desc']               = 'str';


        return $props;
    }
}
