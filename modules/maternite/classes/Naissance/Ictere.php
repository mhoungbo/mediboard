<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es concernant l'ict�re du volet Pathologies du r�sum� du s�jour du nouveau-n�
 */
class Ictere extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $ictere_id = null;

    public ?string $naissance_id = null;

    public ?string $ictere = null;
    public ?string $prema = null;
    public ?string $intense_terme = null;
    public ?string $allo_immun_abo = null;
    public ?string $allo_immun_rh = null;
    public ?string $allo_immun_autre = null;
    public ?string $autre_origine = null;
    public ?string $autre_origine_desc = null;
    public ?string $phototherapie = null;
    public ?string $type_phototherapie = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_icteres';
        $spec->key   = 'ictere_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance notNull cascade back|ictere';

        $props['ictere']                          = 'bool';
        $props['prema']                    = 'bool';
        $props['intense_terme']            = 'bool';
        $props['allo_immun_abo']           = 'bool';
        $props['allo_immun_rh']            = 'bool';
        $props['allo_immun_autre']         = 'bool';
        $props['autre_origine']            = 'bool';
        $props['autre_origine_desc']       = 'str';
        $props['phototherapie']            = 'bool';
        $props['type_phototherapie']       = 'enum list|conv|intens';

        return $props;
    }
}
