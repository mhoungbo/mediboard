<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du volet Etat � la naissance de la partie Salle de naissance du dossier p�rinatal
 */
class EtatNaissance extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $naissance_etat_id = null;

    public ?string $naissance_id = null;

    public ?string $apgar_coeur_1 = null;
    public ?string $apgar_coeur_3 = null;
    public ?string $apgar_coeur_5 = null;
    public ?string $apgar_coeur_10 = null;
    public ?string $apgar_respi_1 = null;
    public ?string $apgar_respi_3 = null;
    public ?string $apgar_respi_5 = null;
    public ?string $apgar_respi_10 = null;
    public ?string $apgar_tonus_1 = null;
    public ?string $apgar_tonus_3 = null;
    public ?string $apgar_tonus_5 = null;
    public ?string $apgar_tonus_10 = null;
    public ?string $apgar_reflexes_1 = null;
    public ?string $apgar_reflexes_3 = null;
    public ?string $apgar_reflexes_5 = null;
    public ?string $apgar_reflexes_10 = null;
    public ?string $apgar_coloration_1 = null;
    public ?string $apgar_coloration_3 = null;
    public ?string $apgar_coloration_5 = null;
    public ?string $apgar_coloration_10 = null;
    public ?string $ph_ao = null;
    public ?string $ph_v = null;
    public ?string $base_deficit = null;
    public ?string $pco2 = null;
    public ?string $lactates = null;
    public ?string $nouveau_ne_endormi = null;
    public ?string $accueil_peau_a_peau = null;
    public ?string $debut_allait_salle_naissance = null;
    public ?string $temp_salle_naissance = null;

    // Scores Apgar
    public ?string $_apgar_1 = null;
    public ?string $_apgar_3 = null;
    public ?string $_apgar_5 = null;
    public ?string $_apgar_10 = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_etats';
        $spec->key   = 'naissance_etat_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id']        = 'ref class|CNaissance cascade notNull back|etat_naissance';

        $props['apgar_coeur_1']       = 'num min|0 max|2';
        $props['apgar_coeur_3']       = 'num min|0 max|2';
        $props['apgar_coeur_5']       = 'num min|0 max|2';
        $props['apgar_coeur_10']      = 'num min|0 max|2';
        $props['apgar_respi_1']       = 'num min|0 max|2';
        $props['apgar_respi_3']       = 'num min|0 max|2';
        $props['apgar_respi_5']       = 'num min|0 max|2';
        $props['apgar_respi_10']      = 'num min|0 max|2';
        $props['apgar_tonus_1']       = 'num min|0 max|2';
        $props['apgar_tonus_3']       = 'num min|0 max|2';
        $props['apgar_tonus_5']       = 'num min|0 max|2';
        $props['apgar_tonus_10']      = 'num min|0 max|2';
        $props['apgar_reflexes_1']    = 'num min|0 max|2';
        $props['apgar_reflexes_3']    = 'num min|0 max|2';
        $props['apgar_reflexes_5']    = 'num min|0 max|2';
        $props['apgar_reflexes_10']   = 'num min|0 max|2';
        $props['apgar_coloration_1']  = 'num min|0 max|2';
        $props['apgar_coloration_3']  = 'num min|0 max|2';
        $props['apgar_coloration_5']  = 'num min|0 max|2';
        $props['apgar_coloration_10'] = 'num min|0 max|2';
        $props['_apgar_1']            = 'num min|0 max|10';
        $props['_apgar_3']            = 'num min|0 max|10';
        $props['_apgar_5']            = 'num min|0 max|10';
        $props['_apgar_10']           = 'num min|0 max|10';

        $props['ph_ao']                        = 'float';
        $props['ph_v']                         = 'float';
        $props['base_deficit']                 = 'str';
        $props['pco2']                         = 'num';
        $props['lactates']                     = 'str';
        $props['nouveau_ne_endormi']           = 'bool';
        $props['accueil_peau_a_peau']          = 'bool';
        $props['debut_allait_salle_naissance'] = 'bool';
        $props['temp_salle_naissance']         = 'float';

        return $props;
    }

    public function updateFormFields(): void
    {

        $this->_apgar_1  = $this->apgar_coeur_1 +
            $this->apgar_respi_1 +
            $this->apgar_tonus_1 +
            $this->apgar_reflexes_1 +
            $this->apgar_coloration_1;
        $this->_apgar_3  = $this->apgar_coeur_3 +
            $this->apgar_respi_3 +
            $this->apgar_tonus_3 +
            $this->apgar_reflexes_3 +
            $this->apgar_coloration_3;
        $this->_apgar_5  = $this->apgar_coeur_5 +
            $this->apgar_respi_5 +
            $this->apgar_tonus_5 +
            $this->apgar_reflexes_5 +
            $this->apgar_coloration_5;
        $this->_apgar_10 = $this->apgar_coeur_10 +
            $this->apgar_respi_10 +
            $this->apgar_tonus_10 +
            $this->apgar_reflexes_10 +
            $this->apgar_coloration_10;
    }
}
