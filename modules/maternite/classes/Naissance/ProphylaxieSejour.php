<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du volet Mesures Prophylactiques du r�sum� du s�jour du nouveau-n�
 */
class ProphylaxieSejour extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $prophylaxie_sejour_id = null;

    public ?string $naissance_id = null;

    public ?string $mesures_prophylactiques = null;
    public ?string $hep_b_injection_immunoglob = null;
    public ?string $vaccinations = null;
    public ?string $vacc_hep_b = null;
    public ?string $vacc_hepp_bcg = null;
    public ?string $depistage_sanguin = null;
    public ?string $hyperphenylalanemie = null;
    public ?string $hypothyroidie = null;
    public ?string $hyperplasie_cong_surrenales = null;
    public ?string $drepanocytose = null;
    public ?string $mucoviscidose = null;
    public ?string $test_audition = null;
    public ?string $etat_test_audition = null;
    public ?string $supp_vitaminique = null;
    public ?string $supp_vitaminique_desc = null;
    public ?string $autre_mesure_proph = null;
    public ?string $autre_mesure_proph_desc = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_prophylaxies_sejours';
        $spec->key   = 'prophylaxie_sejour_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance cascade notNull back|prophylaxie_sejour';

        $props['mesures_prophylactiques']     = 'bool';
        $props['hep_b_injection_immunoglob']  = 'bool';
        $props['vaccinations']                = 'bool';
        $props['vacc_hep_b']                  = 'bool';
        $props['vacc_hepp_bcg']               = 'bool';
        $props['depistage_sanguin']           = 'bool';
        $props['hyperphenylalanemie']         = 'bool';
        $props['hypothyroidie']               = 'bool';
        $props['hyperplasie_cong_surrenales'] = 'bool';
        $props['drepanocytose']               = 'bool';
        $props['mucoviscidose']               = 'bool';
        $props['test_audition']               = 'bool';
        $props['etat_test_audition']          = 'enum list|non|norm|anorm';
        $props['supp_vitaminique']            = 'bool';
        $props['supp_vitaminique_desc']       = 'str';
        $props['autre_mesure_proph']          = 'bool';
        $props['autre_mesure_proph_desc']     = 'str';

        return $props;
    }
}
