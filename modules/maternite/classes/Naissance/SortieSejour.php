<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es concernant du volet Sortie de la maternit� du r�sum� du s�jour du nouveau-n�
 */
class SortieSejour extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $sortie_sejour_id = null;

    public ?string $naissance_id = null;

    public ?string $mode_sortie_mater                = null;
    public ?string $mode_sortie_mater_autre          = null;
    public ?string $jour_vie_transmut_mater          = null;
    public ?string $heure_vie_transmut_mater         = null;
    public ?string $resp_transmut_mater_id           = null;
    public ?string $motif_transmut_mater             = null;
    public ?string $detail_motif_transmut_mater      = null;
    public ?string $lieu_transf_mater                = null;
    public ?string $type_etab_transf_mater           = null;
    public ?string $dest_transf_mater                = null;
    public ?string $dest_transf_mater_autre          = null;
    public ?string $mode_transf_mater                = null;
    public ?string $delai_appel_arrivee_transp_mater = null;
    public ?string $dist_mater_transf_mater          = null;
    public ?string $raison_transf_mater_report       = null;
    public ?string $raison_transf_report_mater_autre = null;
    public ?string $surv_part_sortie_mater           = null;
    public ?string $surv_part_sortie_mater_desc      = null;
    public ?string $remarques_transf_mater           = null;

    public ?string $poids_fin_sejour                = null;
    public ?string $alim_fin_sejour                 = null;
    public ?string $comp_alim_fin_sejour            = null;
    public ?string $nature_comp_alim_fin_sejour     = null;
    public ?string $moyen_comp_alim_fin_sejour      = null;
    public ?string $indic_comp_alim_fin_sejour      = null;
    public ?string $indic_comp_alim_fin_sejour_desc = null;

    public ?string $retour_mater      = null;
    public ?string $date_retour_mater = null;
    public ?string $duree_transfert   = null;

    public ?string $moment_deces     = null;
    public ?string $date_deces       = null;
    public ?string $age_deces_jours  = null;
    public ?string $age_deces_heures = null;
    public ?string $cause_deces      = null;
    public ?string $cause_deces_desc = null;
    public ?string $autopsie         = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_sorties_sejours';
        $spec->key   = 'sortie_sejour_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance notNull cascade back|sortie_sejour';

        $props['mode_sortie_mater']                = 'enum list|dom|mut|transfres|transfhres|poupon|deces|autre';
        $props['mode_sortie_mater_autre']          = 'str';
        $props['jour_vie_transmut_mater']          = 'num min|0 max|255';
        $props['heure_vie_transmut_mater']         = 'num min|0 max|255';
        $props['resp_transmut_mater_id']           = 'ref class|CMediusers back|naissances_transmut_mater';
        $props['motif_transmut_mater']             = 'enum list|malform|infect|ictere|pathrespi|pathcv|pathdig|pathhemato|pathmetab|pathneuro|syndsev|autre';
        $props['detail_motif_transmut_mater']      = 'str';
        $props['lieu_transf_mater']                = 'str';
        $props['type_etab_transf_mater']           = 'enum list|1|2';
        $props['dest_transf_mater']                = 'enum list|rea|intens|neonat|chir|neonatmater|autre';
        $props['dest_transf_mater_autre']          = 'str';
        $props['mode_transf_mater']                = 'enum list|intra|extra|samuped|samuns|ambulance|voiture';
        $props['delai_appel_arrivee_transp_mater'] = 'num min|0 max|255';
        $props['dist_mater_transf_mater']          = 'num min|0 max|255';
        $props['raison_transf_mater_report']       = 'enum list|place|transp|autre';
        $props['raison_transf_report_mater_autre'] = 'str';
        $props['surv_part_sortie_mater']           = 'enum list|non|survmed|survpmi|consultspec|autre';
        $props['surv_part_sortie_mater_desc']      = 'str';
        $props['remarques_transf_mater']           = 'text';

        $props['poids_fin_sejour']                = 'num min|0 max|9999';
        $props['alim_fin_sejour']                 = 'enum list|laitmat|mixte|artif|dietspec';
        $props['comp_alim_fin_sejour']            = 'bool';
        $props['nature_comp_alim_fin_sejour']     = 'enum list|eau|eausucree|prepalactee';
        $props['moyen_comp_alim_fin_sejour']      = 'enum list|tasse|cuill|bib';
        $props['indic_comp_alim_fin_sejour']      = 'enum list|pertepoids|patho';
        $props['indic_comp_alim_fin_sejour_desc'] = 'str';

        $props['retour_mater']      = 'bool';
        $props['date_retour_mater'] = 'date';
        $props['duree_transfert']   = 'num min|0 max|255';

        $props['moment_deces']     = 'enum list|avttrav|ensalle|pdttrav|img|sansprec|neonat';
        $props['date_deces']       = 'date';
        $props['age_deces_jours']  = 'num min|0 max|255';
        $props['age_deces_heures'] = 'num min|0 max|255';
        $props['cause_deces']      = 'enum list|foetneonat|obstmater';
        $props['cause_deces_desc'] = 'str';
        $props['autopsie']         = 'enum list|nf|resnd|resni|resi';

        return $props;
    }
}
