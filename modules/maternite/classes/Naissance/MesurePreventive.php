<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du volet Mesures pr�ventives de la partie Salle de naissance du dossier p�rinatal
 */
class MesurePreventive extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $naissance_mesure_preventive_id = null;

    public ?string $naissance_id = null;

    public ?string $cortico = null;
    public ?string $nb_cures_cortico = null;
    public ?string $dern_cure_cortico = null;
    public ?string $delai_cortico_acc_j = null;
    public ?string $delai_cortico_acc_h = null;
    public ?string $prev_cortico_remarques = null;

    public ?string $contexte_infectieux = null;
    public ?string $infect_facteurs_risque_infect = null;
    public ?string $infect_rpm_sup_12h = null;
    public ?string $infect_liquide_teinte = null;
    public ?string $infect_strepto_b = null;
    public ?string $infect_fievre_mat = null;
    public ?string $infect_maternelle = null;
    public ?string $infect_autre = null;
    public ?string $infect_autre_desc = null;
    public ?string $infect_prelev_bacterio = null;
    public ?string $infect_prelev_gatrique = null;
    public ?string $infect_prelev_autre_periph = null;
    public ?string $infect_prelev_placenta = null;
    public ?string $infect_prelev_sang = null;
    public ?string $infect_antibio = null;
    public ?string $infect_antibio_desc = null;
    public ?string $infect_remarques = null;

    public ?string $prelev_bacterio_mere = null;
    public ?string $prelev_bacterio_vaginal_mere = null;
    public ?string $prelev_bacterio_vaginal_mere_germe = null;
    public ?string $prelev_bacterio_urinaire_mere = null;
    public ?string $prelev_bacterio_urinaire_mere_germe = null;
    public ?string $antibiotherapie_antepart_mere = null;
    public ?string $antibiotherapie_antepart_mere_desc = null;
    public ?string $antibiotherapie_perpart_mere = null;
    public ?string $antibiotherapie_perpart_mere_desc = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_mesures_preventives';
        $spec->key   = 'naissance_mesure_preventive_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id']        = 'ref class|CNaissance cascade notNull back|mesure_preventive';

        $props['cortico']                             = 'bool';
        $props['nb_cures_cortico']                    = 'num min|0 max|255';
        $props['dern_cure_cortico']                   = 'enum list|comp|incomp';
        $props['delai_cortico_acc_j']                 = 'num min|0 max|255';
        $props['delai_cortico_acc_h']                 = 'num min|0 max|255';
        $props['prev_cortico_remarques']              = 'text';

        $props['contexte_infectieux']                 = 'bool';
        $props['infect_facteurs_risque_infect']       = 'bool';
        $props['infect_rpm_sup_12h']                  = 'bool';
        $props['infect_liquide_teinte']               = 'bool';
        $props['infect_strepto_b']                    = 'bool';
        $props['infect_fievre_mat']                   = 'bool';
        $props['infect_maternelle']                   = 'bool';
        $props['infect_autre']                        = 'bool';
        $props['infect_autre_desc']                   = 'str';
        $props['infect_prelev_bacterio']              = 'bool';
        $props['infect_prelev_gatrique']              = 'bool';
        $props['infect_prelev_autre_periph']          = 'bool';
        $props['infect_prelev_placenta']              = 'bool';
        $props['infect_prelev_sang']                  = 'bool';
        $props['infect_antibio']                      = 'bool';
        $props['infect_antibio_desc']                 = 'str';
        $props['infect_remarques']                    = 'text';

        $props['prelev_bacterio_mere']                = 'bool';
        $props['prelev_bacterio_vaginal_mere']        = 'bool';
        $props['prelev_bacterio_vaginal_mere_germe']  = 'str';
        $props['prelev_bacterio_urinaire_mere']       = 'bool';
        $props['prelev_bacterio_urinaire_mere_germe'] = 'str';
        $props['antibiotherapie_antepart_mere']       = 'bool';
        $props['antibiotherapie_antepart_mere_desc']  = 'str';
        $props['antibiotherapie_perpart_mere']        = 'bool';
        $props['antibiotherapie_perpart_mere_desc']   = 'str';

        return $props;
    }
}
