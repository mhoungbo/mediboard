<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du volet Mesures Prophylactiques de la partie Salle de naissance du dossier p�rinatal
 */
class Prophylaxie extends CMbObject
{
    /** @var string Primary key */
    public ?string $naissance_prophylaxie_id = null;

    public ?string $naissance_id = null;

    public ?string $prophy_vit_k = null;
    public ?string $prophy_vit_k_type = null;
    public ?string $prophy_desinfect_occulaire = null;
    public ?string $prophy_asp_naso_phar = null;
    public ?string $prophy_perm_choanes = null;
    public ?string $prophy_perm_oeso = null;
    public ?string $prophy_perm_anale = null;
    public ?string $prophy_emission_urine = null;
    public ?string $prophy_emission_meconium = null;
    public ?string $prophy_autre = null;
    public ?string $prophy_autre_desc = null;
    public ?string $prophy_remarques = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_prophylaxies';
        $spec->key   = 'naissance_prophylaxie_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance cascade notNull back|prophylaxie';

        $props['prophy_vit_k']               = 'bool';
        $props['prophy_vit_k_type']          = 'enum list|parent|oral';
        $props['prophy_desinfect_occulaire'] = 'bool';
        $props['prophy_asp_naso_phar']       = 'bool';
        $props['prophy_perm_choanes']        = 'bool';
        $props['prophy_perm_oeso']           = 'bool';
        $props['prophy_perm_anale']          = 'bool';
        $props['prophy_emission_urine']      = 'bool';
        $props['prophy_emission_meconium']   = 'bool';
        $props['prophy_autre']               = 'bool';
        $props['prophy_autre_desc']          = 'str';
        $props['prophy_remarques']           = 'text';

        return $props;
    }
}
