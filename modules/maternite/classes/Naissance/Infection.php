<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\Naissance;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es concernant les infections du volet Pathologies du r�sum� du s�jour du nouveau-n�
 */
class Infection extends CMbObject
{
    /** @var ?string Primary key */
    public ?string $infection_id = null;

    public ?string $naissance_id = null;

    public ?string $infection                  = null;
    public ?string $degre            = null;
    public ?string $origine          = null;
    public ?string $sang             = null;
    public ?string $lcr              = null;
    public ?string $poumon           = null;
    public ?string $urines           = null;
    public ?string $digestif         = null;
    public ?string $ombilic          = null;
    public ?string $oeil             = null;
    public ?string $os_articulations = null;
    public ?string $peau             = null;
    public ?string $autre            = null;
    public ?string $autre_desc       = null;
    public ?string $strepto_b        = null;
    public ?string $autre_strepto    = null;
    public ?string $staphylo_dore    = null;
    public ?string $autre_staphylo   = null;
    public ?string $haemophilus      = null;
    public ?string $listeria         = null;
    public ?string $pneumocoque      = null;
    public ?string $autre_gplus      = null;
    public ?string $coli             = null;
    public ?string $proteus          = null;
    public ?string $klebsiele        = null;
    public ?string $autre_gmoins     = null;
    public ?string $chlamydiae       = null;
    public ?string $mycoplasme       = null;
    public ?string $candida          = null;
    public ?string $toxoplasme       = null;
    public ?string $autre_parasite   = null;
    public ?string $cmv              = null;
    public ?string $rubeole          = null;
    public ?string $herpes           = null;
    public ?string $varicelle        = null;
    public ?string $vih              = null;
    public ?string $autre_virus      = null;
    public ?string $autre_virus_desc = null;
    public ?string $germe_non_trouve = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'naissances_infections';
        $spec->key   = 'infection_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['naissance_id'] = 'ref class|CNaissance cascade notNull back|infection';

        $props['infection']        = 'bool';
        $props['degre']            = 'enum list|risque|colo|prob|prouv';
        $props['origine']          = 'enum list|materfoet|nosoc|inc';
        $props['sang']             = 'bool';
        $props['lcr']              = 'bool';
        $props['poumon']           = 'bool';
        $props['urines']           = 'bool';
        $props['digestif']         = 'bool';
        $props['ombilic']          = 'bool';
        $props['oeil']             = 'bool';
        $props['os_articulations'] = 'bool';
        $props['peau']             = 'bool';
        $props['autre']            = 'bool';
        $props['autre_desc']       = 'str';
        $props['strepto_b']        = 'bool';
        $props['autre_strepto']    = 'bool';
        $props['staphylo_dore']    = 'bool';
        $props['autre_staphylo']   = 'bool';
        $props['haemophilus']      = 'bool';
        $props['listeria']         = 'bool';
        $props['pneumocoque']      = 'bool';
        $props['autre_gplus']      = 'bool';
        $props['coli']             = 'bool';
        $props['proteus']          = 'bool';
        $props['klebsiele']        = 'bool';
        $props['autre_gmoins']     = 'bool';
        $props['chlamydiae']       = 'bool';
        $props['mycoplasme']       = 'bool';
        $props['candida']          = 'bool';
        $props['toxoplasme']       = 'bool';
        $props['autre_parasite']   = 'bool';
        $props['cmv']              = 'bool';
        $props['rubeole']          = 'bool';
        $props['herpes']           = 'bool';
        $props['varicelle']        = 'bool';
        $props['vih']              = 'bool';
        $props['autre_virus']      = 'bool';
        $props['autre_virus_desc'] = 'str';
        $props['germe_non_trouve'] = 'bool';

        return $props;
    }
}
