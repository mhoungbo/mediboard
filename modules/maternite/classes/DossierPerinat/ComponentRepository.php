<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Exception;
use Ox\Core\CClassMap;
use Ox\Mediboard\Maternite\CDossierPerinat;

/**
 * A repository for loading and storing the components of the CDossierPerinat
 */
class ComponentRepository
{
    private CDossierPerinat $dossier;

    private array $messages = [];

    /**
     * @param CDossierPerinat $dossier
     */
    public function __construct(CDossierPerinat $dossier)
    {
        $this->dossier = $dossier;
    }

    /**
     * Load all the components linked to the CDossierPerinat object
     *
     * @param bool $map_fields If true, will map the field of the components in the properties of the CDossierPerinat
     *
     * @return array
     * @throws Exception
     */
    public function loadComponents(bool $map_fields = true): array
    {
        $components = [];

        foreach (self::getComponentClasses() as $class) {
            $components[] = $this->loadComponent($class::RESOURCE_TYPE, $map_fields);
        }

        return $components;
    }


    /**
     * Load the component with the given back ref name
     *
     * @param string $component_name
     * @param bool   $map_fields If true, will map the field of the component in the properties of the CDossierPerinat
     *
     * @return AbstractComponent|null
     * @throws Exception
     */
    public function loadComponent(string $component_name, bool $map_fields = true): ?AbstractComponent
    {
        /** @var AbstractComponent $component */
        $component = $this->dossier->loadUniqueBackRef($component_name);

        if ($map_fields && $component) {
            foreach ($component->getPlainFields() as $field => $value) {
                if (property_exists($this->dossier, "_$field")) {
                    $this->dossier->{"_$field"} = $value;
                }
            }
        }

        return $component;
    }

    /**
     * Returns the list of component classes
     *
     * @return array
     */
    public static function getComponentClasses(): array
    {
        try {
            return CClassMap::getInstance()->getDirectClassChildren(AbstractComponent::class);
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function storeComponents(): bool
    {
        $components = $this->loadComponents(false);

        foreach ($components as $component) {
            $this->storeComponent($component);
        }

        return !$this->hasMessages();
    }

    /**
     * Store the given component.
     *
     * Set the component's properties from the CDossierPerinat's properties
     *
     * @param AbstractComponent $component
     *
     * @return bool
     * @throws Exception
     */
    public function storeComponent(AbstractComponent $component): bool
    {
        $component->dossier_perinat_id = $this->dossier->_id;

        foreach ($component->getPlainFields() as $field => $value) {
            if (
                !property_exists($this->dossier, "_$field")
                || !isset($this->dossier->{"_$field"})
                || ($field === $this->dossier->_spec->key)
                || ($field === $component->_spec->key)
            ) {
                continue;
            }

            $component->$field = $this->dossier->{"_$field"};
        }

        if ($msg = $component->store()) {
            $this->messages[] = $msg;
            return false;
        }

        return true;
    }

    /**
     * @return void
     */
    public function resetMessages(): void
    {
        $this->messages = [];
    }

    /**
     * @return bool
     */
    public function hasMessages(): bool
    {
        return empty($this->messages);
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
