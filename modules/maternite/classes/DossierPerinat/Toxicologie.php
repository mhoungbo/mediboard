<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es toxicologiques de la m�re et du p�re avant la grossesse pour le dossier p�rinatal
 */
class Toxicologie extends AbstractComponent
{
    public const RESOURCE_TYPE = 'toxicologie';

    /** @var int Primary key */
    public ?int $toxicologie_id = null;

    public ?string $tabac_avant_grossesse       = null;
    public ?string $qte_tabac_avant_grossesse   = null;
    public ?string $tabac_debut_grossesse       = null;
    public ?string $qte_tabac_debut_grossesse   = null;
    public ?string $alcool_debut_grossesse      = null;
    public ?string $qte_alcool_debut_grossesse  = null;
    public ?string $canabis_debut_grossesse     = null;
    public ?string $qte_canabis_debut_grossesse = null;
    public ?string $subst_avant_grossesse       = null;
    public ?string $mode_subst_avant_grossesse  = null;
    public ?string $nom_subst_avant_grossesse   = null;
    public ?string $subst_subst_avant_grossesse = null;
    public ?string $subst_debut_grossesse       = null;
    public ?string $subst_debut_grossesse_text  = null;
    public ?string $tabac_pere                  = null;
    public ?string $coexp_pere                  = null;
    public ?string $alcool_pere                 = null;
    public ?string $toxico_pere                 = null;
    public ?string $rques_toxico                = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_toxicologies';
        $spec->key   = 'toxicologie_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['tabac_avant_grossesse']       = 'bool';
        $props['qte_tabac_avant_grossesse']   = 'num min|0 max|255';
        $props['tabac_debut_grossesse']       = 'bool';
        $props['qte_tabac_debut_grossesse']   = 'num min|0 max|255';
        $props['alcool_debut_grossesse']      = 'bool';
        $props['qte_alcool_debut_grossesse']  = 'num min|0 max|255';
        $props['canabis_debut_grossesse']     = 'bool';
        $props['qte_canabis_debut_grossesse'] = 'num min|0 max|255';
        $props['subst_avant_grossesse']       = 'bool';
        $props['mode_subst_avant_grossesse']  = 'enum list|iv|po|au';
        $props['nom_subst_avant_grossesse']   = 'str';
        $props['subst_subst_avant_grossesse'] = 'str';
        $props['subst_debut_grossesse']       = 'bool';
        $props['subst_debut_grossesse_text']  = 'text helped';
        $props['tabac_pere']                  = 'bool';
        $props['coexp_pere']                  = 'num min|0 max|255';
        $props['alcool_pere']                 = 'bool';
        $props['toxico_pere']                 = 'bool';
        $props['rques_toxico']                = 'text helped';

        return $props;
    }
}
