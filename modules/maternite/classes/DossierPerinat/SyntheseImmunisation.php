<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie immunisation et s�rodiagnostics
 */
class SyntheseImmunisation extends AbstractComponent
{
    public const RESOURCE_TYPE = 'syntheseImmunisation';

    /** @var ?int Primary key */
    public ?int $synthese_immunisation_id = null;

    public ?string $rai_fin_grossesse = null;
    public ?string $rubeole_fin_grossesse = null;
    public ?string $seroconv_rubeole = null;
    public ?string $ag_seroconv_rubeole = null;
    public ?string $toxoplasmose_fin_grossesse = null;
    public ?string $seroconv_toxoplasmose = null;
    public ?string $ag_seroconv_toxoplasmose = null;
    public ?string $syphilis_fin_grossesse = null;
    public ?string $vih_fin_grossesse = null;
    public ?string $hepatite_b_fin_grossesse = null;
    public ?string $hepatite_b_aghbspos_fin_grossesse = null;
    public ?string $hepatite_c_fin_grossesse = null;
    public ?string $cmvg_fin_grossesse = null;
    public ?string $cmvm_fin_grossesse = null;
    public ?string $seroconv_cmv = null;
    public ?string $ag_seroconv_cmv = null;
    public ?string $autre_serodiag_fin_grossesse = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_immunisations';
        $spec->key   = 'synthese_immunisation_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['rai_fin_grossesse']                 = 'enum list|neg|pos|inc';
        $props['rubeole_fin_grossesse']             = 'enum list|nonimmu|immu|inc';
        $props['seroconv_rubeole']                  = 'bool default|0';
        $props['ag_seroconv_rubeole']               = 'num min|0 max|255';
        $props['toxoplasmose_fin_grossesse']        = 'enum list|nonimmu|immu|inc';
        $props['seroconv_toxoplasmose']             = 'bool default|0';
        $props['ag_seroconv_toxoplasmose']          = 'num min|0 max|255';
        $props['syphilis_fin_grossesse']            = 'enum list|neg|pos|inc';
        $props['vih_fin_grossesse']                 = 'enum list|neg|pos|inc';
        $props['hepatite_b_fin_grossesse']          = 'enum list|aghbsm|aghbsp|achbsp|inc';
        $props['hepatite_b_aghbspos_fin_grossesse'] = 'enum list|aghbcp|achbcp|aghbep|achbep';
        $props['hepatite_c_fin_grossesse']          = 'enum list|neg|acvhcp|inc';
        $props['cmvg_fin_grossesse']                = 'enum list|pos|neg|inc';
        $props['cmvm_fin_grossesse']                = 'enum list|pos|neg|inc';
        $props['seroconv_cmv']                      = 'bool default|0';
        $props['ag_seroconv_cmv']                   = 'num min|0 max|255';
        $props['autre_serodiag_fin_grossesse']      = 'text helped';

        return $props;
    }
}
