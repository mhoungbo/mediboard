<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant les ant�c�dents m�dicaux du p�re
 */
class AntecedentsPaternels extends AbstractComponent
{
    public const RESOURCE_TYPE = 'antecedentPaternel';

    /** @var ?int Primary key */
    public ?int $antecedents_paternels_id = null;

    public ?string $pere_serologie_vih = null;
    public ?string $pere_electrophorese_hb = null;
    public ?string $pere_patho_ant = null;
    public ?string $pere_ant_herpes = null;
    public ?string $pere_ant_autre = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_antecedents_paternels';
        $spec->key   = 'antecedents_paternels_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['pere_serologie_vih']     = 'enum list|neg|pos|inc default|inc';
        $props['pere_electrophorese_hb'] = 'str';
        $props['pere_patho_ant']         = 'bool';
        $props['pere_ant_herpes']        = 'bool';
        $props['pere_ant_autre']         = 'text helped';

        return $props;
    }
}
