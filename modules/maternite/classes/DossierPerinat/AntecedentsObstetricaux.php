<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les données du dossier périnatal concernant les antécédents obstétriques de la parturiente
 */
class AntecedentsObstetricaux extends AbstractComponent
{
    public const RESOURCE_TYPE = 'antecedentsObstetricaux';

    /** @var ?string Primary key */
    public ?string $antecedents_obstetricaux_id = null;

    public ?string $ant_obst_nb_gr_acc              = null;
    public ?string $ant_obst_nb_gr_av_sp            = null;
    public ?string $ant_obst_nb_gr_ivg              = null;
    public ?string $ant_obst_nb_gr_geu              = null;
    public ?string $ant_obst_nb_gr_mole             = null;
    public ?string $ant_obst_nb_gr_img              = null;
    public ?string $ant_obst_nb_gr_amp              = null;
    public ?string $ant_obst_nb_gr_mult             = null;
    public ?string $ant_obst_nb_gr_hta              = null;
    public ?string $ant_obst_nb_gr_map              = null;
    public ?string $ant_obst_nb_gr_diab             = null;
    public ?string $ant_obst_nb_gr_cesar            = null;
    public ?string $ant_obst_nb_gr_prema            = null;
    public ?string $ant_obst_nb_enf_moins_2500g    = null;
    public ?string $ant_obst_nb_enf_hypotroph       = null;
    public ?string $ant_obst_nb_enf_macrosome       = null;
    public ?string $ant_obst_nb_enf_morts_nes       = null;
    public ?string $ant_obst_nb_enf_mort_neonat     = null;
    public ?string $ant_obst_nb_enf_mort_postneonat = null;
    public ?string $ant_obst_nb_enf_malform         = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_antecedents_obstetricaux';
        $spec->key   = 'antecedents_obstetricaux_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['ant_obst_nb_gr_acc']              = 'num min|0 max|20';
        $props['ant_obst_nb_gr_av_sp']            = 'num min|0 max|20';
        $props['ant_obst_nb_gr_ivg']              = 'num min|0 max|20';
        $props['ant_obst_nb_gr_geu']              = 'num min|0 max|20';
        $props['ant_obst_nb_gr_mole']             = 'num min|0 max|20';
        $props['ant_obst_nb_gr_img']              = 'num min|0 max|20';
        $props['ant_obst_nb_gr_amp']              = 'num min|0 max|20';
        $props['ant_obst_nb_gr_mult']             = 'num min|0 max|20';
        $props['ant_obst_nb_gr_hta']              = 'num min|0 max|20';
        $props['ant_obst_nb_gr_map']              = 'num min|0 max|20';
        $props['ant_obst_nb_gr_diab']             = 'num min|0 max|20';
        $props['ant_obst_nb_gr_cesar']            = 'num min|0 max|20';
        $props['ant_obst_nb_gr_prema']            = 'num min|0 max|20';
        $props['ant_obst_nb_enf_moins_2500g']     = 'num min|0 max|20';
        $props['ant_obst_nb_enf_hypotroph']       = 'num min|0 max|20';
        $props['ant_obst_nb_enf_macrosome']       = 'num min|0 max|20';
        $props['ant_obst_nb_enf_morts_nes']       = 'num min|0 max|20';
        $props['ant_obst_nb_enf_mort_neonat']     = 'num min|0 max|20';
        $props['ant_obst_nb_enf_mort_postneonat'] = 'num min|0 max|20';
        $props['ant_obst_nb_enf_malform']         = 'num min|0 max|20';

        return $props;
    }
}
