<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant les ant�c�dents familiaux de la m�re et du p�re
 */
class AntecedentsFamiliaux extends AbstractComponent
{
    public const RESOURCE_TYPE = 'antecedentsFamiliaux';

    /** @var ?int Primary key */
    public ?int $antecedents_familiaux_id = null;

    public ?string $ant_fam = null;
    public ?string $consanguinite = null;
    public ?string $ant_fam_mere_gemellite = null;
    public ?string $ant_fam_pere_gemellite = null;
    public ?string $ant_fam_mere_malformations = null;
    public ?string $ant_fam_pere_malformations = null;
    public ?string $ant_fam_mere_maladie_genique = null;
    public ?string $ant_fam_pere_maladie_genique = null;
    public ?string $ant_fam_mere_maladie_chrom = null;
    public ?string $ant_fam_pere_maladie_chrom = null;
    public ?string $ant_fam_mere_diabete = null;
    public ?string $ant_fam_pere_diabete = null;
    public ?string $ant_fam_mere_hta = null;
    public ?string $ant_fam_pere_hta = null;
    public ?string $ant_fam_mere_phlebite = null;
    public ?string $ant_fam_pere_phlebite = null;
    public ?string $ant_fam_mere_autre = null;
    public ?string $ant_fam_pere_autre = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_antecedents_familiaux';
        $spec->key   = 'antecedents_familiaux_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['ant_fam']                      = 'bool';
        $props['consanguinite']                = 'bool';
        $props['ant_fam_mere_gemellite']       = 'bool';
        $props['ant_fam_pere_gemellite']       = 'bool';
        $props['ant_fam_mere_malformations']   = 'bool';
        $props['ant_fam_pere_malformations']   = 'bool';
        $props['ant_fam_mere_maladie_genique'] = 'bool';
        $props['ant_fam_pere_maladie_genique'] = 'bool';
        $props['ant_fam_mere_maladie_chrom']   = 'bool';
        $props['ant_fam_pere_maladie_chrom']   = 'bool';
        $props['ant_fam_mere_diabete']         = 'bool';
        $props['ant_fam_pere_diabete']         = 'bool';
        $props['ant_fam_mere_hta']             = 'bool';
        $props['ant_fam_pere_hta']             = 'bool';
        $props['ant_fam_mere_phlebite']        = 'bool';
        $props['ant_fam_pere_phlebite']        = 'bool';
        $props['ant_fam_mere_autre']           = 'text helped';
        $props['ant_fam_pere_autre']           = 'text helped';

        return $props;
    }
}
