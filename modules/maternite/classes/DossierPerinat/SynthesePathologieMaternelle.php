<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie pathologies maternelles
 */
class SynthesePathologieMaternelle extends AbstractComponent
{
    public const RESOURCE_TYPE = 'synthesePathologieMaternelle';

    /** @var ?int Primary key */
    public ?int $synthese_pathologie_maternelle_id = null;

    public ?string $pathologie_grossesse_maternelle             = null;
    public ?string $metrorragie_1er_trim                        = null;
    public ?string $type_metrorragie_1er_trim                   = null;
    public ?string $ag_metrorragie_1er_trim                     = null;
    public ?string $metrorragie_2e_3e_trim                      = null;
    public ?string $type_metrorragie_2e_3e_trim                 = null;
    public ?string $ag_metrorragie_2e_3e_trim                   = null;
    public ?string $menace_acc_premat                           = null;
    public ?string $menace_acc_premat_modif_cerv                = null;
    public ?string $pec_menace_acc_premat                       = null;
    public ?string $ag_menace_acc_premat                        = null;
    public ?string $ag_hospi_menace_acc_premat                  = null;
    public ?string $rupture_premat_membranes                    = null;
    public ?string $ag_rupture_premat_membranes                 = null;
    public ?string $anomalie_liquide_amniotique                 = null;
    public ?string $type_anomalie_liquide_amniotique            = null;
    public ?string $ag_anomalie_liquide_amniotique              = null;
    public ?string $autre_patho_gravidique                      = null;
    public ?string $patho_grav_vomissements                     = null;
    public ?string $patho_grav_herpes_gest                      = null;
    public ?string $patho_grav_dermatose_pup                    = null;
    public ?string $patho_grav_placenta_praevia_non_hemo        = null;
    public ?string $patho_grav_chorio_amniotite                 = null;
    public ?string $patho_grav_transf_foeto_mat                 = null;
    public ?string $patho_grav_beance_col                       = null;
    public ?string $patho_grav_cerclage                         = null;
    public ?string $ag_autre_patho_gravidique                   = null;
    public ?string $hypertension_arterielle                     = null;
    public ?string $type_hypertension_arterielle                = null;
    public ?string $ag_hypertension_arterielle                  = null;
    public ?string $proteinurie                                 = null;
    public ?string $type_proteinurie                            = null;
    public ?string $ag_proteinurie                              = null;
    public ?string $diabete                                     = null;
    public ?string $type_diabete                                = null;
    public ?string $ag_diabete                                  = null;
    public ?string $infection_urinaire                          = null;
    public ?string $type_infection_urinaire                     = null;
    public ?string $ag_infection_urinaire                       = null;
    public ?string $infection_cervico_vaginale                  = null;
    public ?string $type_infection_cervico_vaginale             = null;
    public ?string $autre_infection_cervico_vaginale            = null;
    public ?string $ag_infection_cervico_vaginale               = null;
    public ?string $autre_patho_maternelle                      = null;
    public ?string $ag_autre_patho_maternelle                   = null;
    public ?string $anemie_mat_pdt_grossesse                    = null;
    public ?string $tombopenie_mat_pdt_grossesse                = null;
    public ?string $autre_patho_hemato_mat_pdt_grossesse        = null;
    public ?string $desc_autre_patho_hemato_mat_pdt_grossesse   = null;
    public ?string $faible_prise_poid_mat_pdt_grossesse         = null;
    public ?string $malnut_mat_pdt_grossesse                    = null;
    public ?string $autre_patho_endo_mat_pdt_grossesse          = null;
    public ?string $desc_autre_patho_endo_mat_pdt_grossesse     = null;
    public ?string $cholestase_mat_pdt_grossesse                = null;
    public ?string $steatose_hep_mat_pdt_grossesse              = null;
    public ?string $autre_patho_hepato_mat_pdt_grossesse        = null;
    public ?string $desc_autre_patho_hepato_mat_pdt_grossesse   = null;
    public ?string $thrombophl_sup_mat_pdt_grossesse            = null;
    public ?string $thrombophl_prof_mat_pdt_grossesse           = null;
    public ?string $autre_patho_vein_mat_pdt_grossesse          = null;
    public ?string $desc_autre_patho_vein_mat_pdt_grossesse     = null;
    public ?string $asthme_mat_pdt_grossesse                    = null;
    public ?string $autre_patho_resp_mat_pdt_grossesse          = null;
    public ?string $desc_autre_patho_resp_mat_pdt_grossesse     = null;
    public ?string $cardiopathie_mat_pdt_grossesse              = null;
    public ?string $autre_patho_cardio_mat_pdt_grossesse        = null;
    public ?string $desc_autre_patho_cardio_mat_pdt_grossesse   = null;
    public ?string $epilepsie_mat_pdt_grossesse                 = null;
    public ?string $depression_mat_pdt_grossesse                = null;
    public ?string $autre_patho_neuropsy_mat_pdt_grossesse      = null;
    public ?string $desc_autre_patho_neuropsy_mat_pdt_grossesse = null;
    public ?string $patho_gyneco_mat_pdt_grossesse              = null;
    public ?string $desc_patho_gyneco_mat_pdt_grossesse         = null;
    public ?string $mst_mat_pdt_grossesse                       = null;
    public ?string $desc_mst_mat_pdt_grossesse                  = null;
    public ?string $synd_douleur_abdo_mat_pdt_grossesse         = null;
    public ?string $desc_synd_douleur_abdo_mat_pdt_grossesse    = null;
    public ?string $synd_infect_mat_pdt_grossesse               = null;
    public ?string $desc_synd_infect_mat_pdt_grossesse          = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_pathologies_maternelles';
        $spec->key   = 'synthese_pathologie_maternelle_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['pathologie_grossesse_maternelle']             = 'bool';
        $props['metrorragie_1er_trim']                        = 'bool';
        $props['type_metrorragie_1er_trim']                   = 'enum list|menavort|autre';
        $props['ag_metrorragie_1er_trim']                     = 'num min|0 max|255';
        $props['metrorragie_2e_3e_trim']                      = 'bool';
        $props['type_metrorragie_2e_3e_trim']                 = 'enum list|placpraehemo|hrpavectrcoag|hrpsanstrcoag|hemoavectrcoag|hemosanstrcoag';
        $props['ag_metrorragie_2e_3e_trim']                   = 'num min|0 max|255';
        $props['menace_acc_premat']                           = 'bool';
        $props['menace_acc_premat_modif_cerv']                = 'bool';
        $props['pec_menace_acc_premat']                       = 'enum list|ttrepos|ttmedic|hospi';
        $props['ag_menace_acc_premat']                        = 'num min|0 max|255';
        $props['ag_hospi_menace_acc_premat']                  = 'num min|0 max|255';
        $props['rupture_premat_membranes']                    = 'bool';
        $props['ag_rupture_premat_membranes']                 = 'num min|0 max|255';
        $props['anomalie_liquide_amniotique']                 = 'bool';
        $props['type_anomalie_liquide_amniotique']            = 'enum list|exces|oligoamnios';
        $props['ag_anomalie_liquide_amniotique']              = 'num min|0 max|255';
        $props['autre_patho_gravidique']                      = 'bool';
        $props['patho_grav_vomissements']                     = 'bool';
        $props['patho_grav_herpes_gest']                      = 'bool';
        $props['patho_grav_dermatose_pup']                    = 'bool';
        $props['patho_grav_placenta_praevia_non_hemo']        = 'bool';
        $props['patho_grav_chorio_amniotite']                 = 'bool';
        $props['patho_grav_transf_foeto_mat']                 = 'bool';
        $props['patho_grav_beance_col']                       = 'bool';
        $props['patho_grav_cerclage']                         = 'bool';
        $props['ag_autre_patho_gravidique']                   = 'num min|0 max|255';
        $props['hypertension_arterielle']                     = 'bool';
        $props['type_hypertension_arterielle']                = 'enum list|htachro|htagrav|hellp|preecmod|preechta|preecsev|ecl';
        $props['ag_hypertension_arterielle']                  = 'num min|0 max|255';
        $props['proteinurie']                                 = 'bool';
        $props['type_proteinurie']                            = 'enum list|sansoed|avecoed|oedgen';
        $props['ag_proteinurie']                              = 'num min|0 max|255';
        $props['diabete']                                     = 'bool';
        $props['type_diabete']                                = 'enum list|gestid|gestnid|preexid|preexnid|sansprec';
        $props['ag_diabete']                                  = 'num min|0 max|255';
        $props['infection_urinaire']                          = 'bool';
        $props['type_infection_urinaire']                     = 'enum list|basse|pyelo|nonprec';
        $props['ag_infection_urinaire']                       = 'num min|0 max|255';
        $props['infection_cervico_vaginale']                  = 'bool';
        $props['type_infection_cervico_vaginale']             = 'enum list|streptob|autre';
        $props['autre_infection_cervico_vaginale']            = 'str';
        $props['ag_infection_cervico_vaginale']               = 'num min|0 max|255';
        $props['autre_patho_maternelle']                      = 'bool';
        $props['ag_autre_patho_maternelle']                   = 'num min|0 max|255';
        $props['anemie_mat_pdt_grossesse']                    = 'bool';
        $props['tombopenie_mat_pdt_grossesse']                = 'bool';
        $props['autre_patho_hemato_mat_pdt_grossesse']        = 'bool';
        $props['desc_autre_patho_hemato_mat_pdt_grossesse']   = 'str';
        $props['faible_prise_poid_mat_pdt_grossesse']         = 'bool';
        $props['malnut_mat_pdt_grossesse']                    = 'bool';
        $props['autre_patho_endo_mat_pdt_grossesse']          = 'bool';
        $props['desc_autre_patho_endo_mat_pdt_grossesse']     = 'str';
        $props['cholestase_mat_pdt_grossesse']                = 'bool';
        $props['steatose_hep_mat_pdt_grossesse']              = 'bool';
        $props['autre_patho_hepato_mat_pdt_grossesse']        = 'bool';
        $props['desc_autre_patho_hepato_mat_pdt_grossesse']   = 'str';
        $props['thrombophl_sup_mat_pdt_grossesse']            = 'bool';
        $props['thrombophl_prof_mat_pdt_grossesse']           = 'bool';
        $props['autre_patho_vein_mat_pdt_grossesse']          = 'bool';
        $props['desc_autre_patho_vein_mat_pdt_grossesse']     = 'str';
        $props['asthme_mat_pdt_grossesse']                    = 'bool';
        $props['autre_patho_resp_mat_pdt_grossesse']          = 'bool';
        $props['desc_autre_patho_resp_mat_pdt_grossesse']     = 'str';
        $props['cardiopathie_mat_pdt_grossesse']              = 'bool';
        $props['autre_patho_cardio_mat_pdt_grossesse']        = 'bool';
        $props['desc_autre_patho_cardio_mat_pdt_grossesse']   = 'str';
        $props['epilepsie_mat_pdt_grossesse']                 = 'bool';
        $props['depression_mat_pdt_grossesse']                = 'bool';
        $props['autre_patho_neuropsy_mat_pdt_grossesse']      = 'bool';
        $props['desc_autre_patho_neuropsy_mat_pdt_grossesse'] = 'str';
        $props['patho_gyneco_mat_pdt_grossesse']              = 'bool';
        $props['desc_patho_gyneco_mat_pdt_grossesse']         = 'str';
        $props['mst_mat_pdt_grossesse']                       = 'bool';
        $props['desc_mst_mat_pdt_grossesse']                  = 'str';
        $props['synd_douleur_abdo_mat_pdt_grossesse']         = 'bool';
        $props['desc_synd_douleur_abdo_mat_pdt_grossesse']    = 'str';
        $props['synd_infect_mat_pdt_grossesse']               = 'bool';
        $props['desc_synd_infect_mat_pdt_grossesse']          = 'str';

        return $props;
    }
}
