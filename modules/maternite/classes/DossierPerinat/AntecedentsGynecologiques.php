<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les données du dossier périnatal concernant les antécédents gynécologiques
 */
class AntecedentsGynecologiques extends AbstractComponent
{
    public const RESOURCE_TYPE = 'antecedentsGynecologiques';

    /** @var ?int Primary key */
    public ?int $antecedents_gynecologiques_id = null;

    public ?string $gyneco_ant_regles             = null;
    public ?string $gyneco_ant_regul_regles       = null;
    public ?string $gyneco_ant_fcv                = null;
    public ?string $gyneco_ant                    = null;
    public ?string $gyneco_ant_herpes             = null;
    public ?string $gyneco_ant_lesion_col         = null;
    public ?string $gyneco_ant_conisation         = null;
    public ?string $gyneco_ant_cicatrice_uterus   = null;
    public ?string $gyneco_ant_fibrome            = null;
    public ?string $gyneco_ant_stat_pelv          = null;
    public ?string $gyneco_ant_cancer_sein        = null;
    public ?string $gyneco_ant_cancer_app_genital = null;
    public ?string $gyneco_ant_malf_genitale      = null;
    public ?string $gyneco_ant_condylomes         = null;
    public ?string $gyneco_ant_distilbene         = null;
    public ?string $gyneco_ant_autre              = null;
    public ?string $gyneco_ant_infert             = null;
    public ?string $gyneco_ant_infert_origine     = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_antecedents_gynecologiques';
        $spec->key   = 'antecedents_gynecologiques_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['gyneco_ant_regles']             = 'num min|0 max|255';
        $props['gyneco_ant_regul_regles']       = 'enum list|regul|irregul';
        $props['gyneco_ant_fcv']                = 'date progressive';
        $props['gyneco_ant']                    = 'bool';
        $props['gyneco_ant_herpes']             = 'bool';
        $props['gyneco_ant_lesion_col']         = 'bool';
        $props['gyneco_ant_conisation']         = 'bool';
        $props['gyneco_ant_cicatrice_uterus']   = 'bool';
        $props['gyneco_ant_fibrome']            = 'bool';
        $props['gyneco_ant_stat_pelv']          = 'bool';
        $props['gyneco_ant_cancer_sein']        = 'bool';
        $props['gyneco_ant_cancer_app_genital'] = 'bool';
        $props['gyneco_ant_malf_genitale']      = 'bool';
        $props['gyneco_ant_condylomes']         = 'bool';
        $props['gyneco_ant_distilbene']         = 'bool';
        $props['gyneco_ant_autre']              = 'text helped';
        $props['gyneco_ant_infert']             = 'bool';
        $props['gyneco_ant_infert_origine']     = 'enum list|anov|uterine|cervic|idiopath|tubaire|fem|masc|femmasc';

        return $props;
    }
}
