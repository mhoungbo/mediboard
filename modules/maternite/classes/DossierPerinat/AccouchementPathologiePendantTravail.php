<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant les pathologies et anomalies survenues pendant le travail
 */
class AccouchementPathologiePendantTravail extends AbstractComponent
{
    public const RESOURCE_TYPE = 'accouchementPathologiePendantTravail';

    /** @var ?int Primary key */
    public ?int $pathologie_pendant_travail_id = null;

    public ?string $anom_pdt_trav                                 = null;
    public ?string $hypox_foet_pdt_trav                           = null;
    public ?string $hypox_foet_pdt_trav_rcf_isole                 = null;
    public ?string $hypox_foet_pdt_trav_la_teinte                 = null;
    public ?string $hypox_foet_pdt_trav_rcf_la                    = null;
    public ?string $hypox_foet_pdt_trav_anom_ph_foet              = null;
    public ?string $hypox_foet_pdt_trav_anom_ecg_foet             = null;
    public ?string $hypox_foet_pdt_trav_procidence_cordon         = null;
    public ?string $dysto_pres_pdt_trav                           = null;
    public ?string $dysto_pres_pdt_trav_rot_tete_incomp           = null;
    public ?string $dysto_pres_pdt_trav_siege                     = null;
    public ?string $dysto_pres_pdt_trav_face                      = null;
    public ?string $dysto_pres_pdt_trav_pres_front                = null;
    public ?string $dysto_pres_pdt_trav_pres_transv               = null;
    public ?string $dysto_pres_pdt_trav_autre_pres_anorm          = null;
    public ?string $dysto_anom_foet_pdt_trav                      = null;
    public ?string $dysto_anom_foet_pdt_trav_foetus_macrosome     = null;
    public ?string $dysto_anom_foet_pdt_trav_jumeaux_soudes       = null;
    public ?string $dysto_anom_foet_pdt_trav_difform_foet         = null;
    public ?string $echec_decl_travail                            = null;
    public ?string $echec_decl_travail_medic                      = null;
    public ?string $echec_decl_travail_meca                       = null;
    public ?string $echec_decl_travail_sans_prec                  = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav                  = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_deform_pelv      = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_bassin_retr      = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_detroit_sup_retr = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_detroit_moy_retr = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_dispr_foeto_pelv = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_fibrome_pelv     = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_stenose_cerv     = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_malf_uterine     = null;
    public ?string $dysto_anom_pelv_mat_pdt_trav_autre            = null;
    public ?string $dysto_dynam_pdt_trav                          = null;
    public ?string $dysto_dynam_pdt_trav_demarrage                = null;
    public ?string $dysto_dynam_pdt_trav_cerv_latence             = null;
    public ?string $dysto_dynam_pdt_trav_arret_dilat              = null;
    public ?string $dysto_dynam_pdt_trav_hypertonie_uter          = null;
    public ?string $dysto_dynam_pdt_trav_dilat_lente_col          = null;
    public ?string $dysto_dynam_pdt_trav_echec_travail            = null;
    public ?string $dysto_dynam_pdt_trav_non_engagement           = null;
    public ?string $patho_mater_pdt_trav                          = null;
    public ?string $patho_mater_pdt_trav_hemo_sans_trouble_coag   = null;
    public ?string $patho_mater_pdt_trav_hemo_avec_trouble_coag   = null;
    public ?string $patho_mater_pdt_trav_choc_obst                = null;
    public ?string $patho_mater_pdt_trav_eclampsie                = null;
    public ?string $patho_mater_pdt_trav_rupt_uterine             = null;
    public ?string $patho_mater_pdt_trav_embolie_amnio            = null;
    public ?string $patho_mater_pdt_trav_embolie_pulm             = null;
    public ?string $patho_mater_pdt_trav_complic_acte_obst        = null;
    public ?string $patho_mater_pdt_trav_chorio_amnio             = null;
    public ?string $patho_mater_pdt_trav_infection                = null;
    public ?string $patho_mater_pdt_trav_fievre                   = null;
    public ?string $patho_mater_pdt_trav_fatigue_mat              = null;
    public ?string $patho_mater_pdt_trav_autre_complication       = null;

    /* Anomalies expulsion */
    public ?string $anom_expuls                         = null;
    public ?string $anom_expuls_non_progr_pres_foetale  = null;
    public ?string $anom_expuls_dysto_pres_posterieures = null;
    public ?string $anom_expuls_dystocie_epaules        = null;
    public ?string $anom_expuls_retention_tete          = null;
    public ?string $anom_expuls_soufrance_foet_rcf      = null;
    public ?string $anom_expuls_soufrance_foet_rcf_la   = null;
    public ?string $anom_expuls_echec_forceps_cesar     = null;
    public ?string $anom_expuls_fatigue_mat             = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_pathologies_pendant_travail';
        $spec->key   = 'pathologie_pendant_travail_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id']                            .= ' back|' . self::RESOURCE_TYPE;

        $props['anom_pdt_trav']                                 = 'bool';
        $props['hypox_foet_pdt_trav']                           = 'bool';
        $props['hypox_foet_pdt_trav_rcf_isole']                 = 'bool';
        $props['hypox_foet_pdt_trav_la_teinte']                 = 'bool';
        $props['hypox_foet_pdt_trav_rcf_la']                    = 'bool';
        $props['hypox_foet_pdt_trav_anom_ph_foet']              = 'bool';
        $props['hypox_foet_pdt_trav_anom_ecg_foet']             = 'bool';
        $props['hypox_foet_pdt_trav_procidence_cordon']         = 'bool';
        $props['dysto_pres_pdt_trav']                           = 'bool';
        $props['dysto_pres_pdt_trav_rot_tete_incomp']           = 'bool';
        $props['dysto_pres_pdt_trav_siege']                     = 'bool';
        $props['dysto_pres_pdt_trav_face']                      = 'bool';
        $props['dysto_pres_pdt_trav_pres_front']                = 'bool';
        $props['dysto_pres_pdt_trav_pres_transv']               = 'bool';
        $props['dysto_pres_pdt_trav_autre_pres_anorm']          = 'bool';
        $props['dysto_anom_foet_pdt_trav']                      = 'bool';
        $props['dysto_anom_foet_pdt_trav_foetus_macrosome']     = 'bool';
        $props['dysto_anom_foet_pdt_trav_jumeaux_soudes']       = 'bool';
        $props['dysto_anom_foet_pdt_trav_difform_foet']         = 'bool';
        $props['echec_decl_travail']                            = 'bool';
        $props['echec_decl_travail_medic']                      = 'bool';
        $props['echec_decl_travail_meca']                       = 'bool';
        $props['echec_decl_travail_sans_prec']                  = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav']                  = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_deform_pelv']      = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_bassin_retr']      = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_detroit_sup_retr'] = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_detroit_moy_retr'] = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_dispr_foeto_pelv'] = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_fibrome_pelv']     = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_stenose_cerv']     = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_malf_uterine']     = 'bool';
        $props['dysto_anom_pelv_mat_pdt_trav_autre']            = 'bool';
        $props['dysto_dynam_pdt_trav']                          = 'bool';
        $props['dysto_dynam_pdt_trav_demarrage']                = 'bool';
        $props['dysto_dynam_pdt_trav_cerv_latence']             = 'bool';
        $props['dysto_dynam_pdt_trav_arret_dilat']              = 'bool';
        $props['dysto_dynam_pdt_trav_hypertonie_uter']          = 'bool';
        $props['dysto_dynam_pdt_trav_dilat_lente_col']          = 'bool';
        $props['dysto_dynam_pdt_trav_echec_travail']            = 'bool';
        $props['dysto_dynam_pdt_trav_non_engagement']           = 'bool';
        $props['patho_mater_pdt_trav']                          = 'bool';
        $props['patho_mater_pdt_trav_hemo_sans_trouble_coag']   = 'bool';
        $props['patho_mater_pdt_trav_hemo_avec_trouble_coag']   = 'bool';
        $props['patho_mater_pdt_trav_choc_obst']                = 'bool';
        $props['patho_mater_pdt_trav_eclampsie']                = 'bool';
        $props['patho_mater_pdt_trav_rupt_uterine']             = 'bool';
        $props['patho_mater_pdt_trav_embolie_amnio']            = 'bool';
        $props['patho_mater_pdt_trav_embolie_pulm']             = 'bool';
        $props['patho_mater_pdt_trav_complic_acte_obst']        = 'bool';
        $props['patho_mater_pdt_trav_chorio_amnio']             = 'bool';
        $props['patho_mater_pdt_trav_infection']                = 'bool';
        $props['patho_mater_pdt_trav_fievre']                   = 'bool';
        $props['patho_mater_pdt_trav_fatigue_mat']              = 'bool';
        $props['patho_mater_pdt_trav_autre_complication']       = 'bool';

        $props['anom_expuls']                         = 'bool';
        $props['anom_expuls_non_progr_pres_foetale']  = 'bool';
        $props['anom_expuls_dysto_pres_posterieures'] = 'bool';
        $props['anom_expuls_dystocie_epaules']        = 'bool';
        $props['anom_expuls_retention_tete']          = 'bool';
        $props['anom_expuls_soufrance_foet_rcf']      = 'bool';
        $props['anom_expuls_soufrance_foet_rcf_la']   = 'bool';
        $props['anom_expuls_echec_forceps_cesar']     = 'bool';
        $props['anom_expuls_fatigue_mat']             = 'bool';

        return $props;
    }
}
