<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant la cl�ture du dossier sans accouchement
 */
class ClotureSansAccouchement extends AbstractComponent
{
    public const RESOURCE_TYPE = 'clotureSansAccouchement';

    /** @var ?int Primary key */
    public ?int $cloture_sans_accouchement_id = null;

    public ?string $type_terminaison_grossesse = null;
    public ?string $type_term_hors_etab = null;
    public ?string $type_term_inf_22sa = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_clotures_sans_accouchements';
        $spec->key   = 'cloture_sans_accouchement_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['type_terminaison_grossesse'] = 'enum list|termhetab|term22|avsp';
        $props['type_term_hors_etab']        = 'enum list|vivant|mortiu|img';
        $props['type_term_inf_22sa']         = 'enum list|avsp|ivg|geu|mole|img';

        return $props;
    }
}
