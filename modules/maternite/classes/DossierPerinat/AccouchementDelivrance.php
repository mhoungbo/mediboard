<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant la d�livrance lors de l'accouchement
 */
class AccouchementDelivrance extends AbstractComponent
{
    public const RESOURCE_TYPE = 'accouchementDelivrance';

    /** @var ?int Primary key */
    public ?int $accouchement_delivrance_id = null;

    public ?string $deliv_faite_par                       = null;
    public ?string $datetime_deliv                        = null;
    public ?string $type_deliv                            = null;
    public ?string $prod_deliv                            = null;
    public ?string $dose_prod_deliv                       = null;
    public ?string $datetime_inj_prod_deliv               = null;
    public ?string $voie_inj_prod_deliv                   = null;
    public ?string $modalite_deliv                        = null;
    public ?string $comp_deliv                            = null;
    public ?string $hemorr_deliv                          = null;
    public ?string $retention_plac_comp_deliv             = null;
    public ?string $retention_plac_part_deliv             = null;
    public ?string $atonie_uterine_deliv                  = null;
    public ?string $trouble_coag_deliv                    = null;
    public ?string $transf_deliv                          = null;
    public ?string $nb_unites_transf_deliv                = null;
    public ?string $autre_comp_deliv                      = null;
    public ?string $retention_plac_comp_sans_hemorr_deliv = null;
    public ?string $retention_plac_part_sans_hemorr_deliv = null;
    public ?string $inversion_uterine_deliv               = null;
    public ?string $autre_comp_autre_deliv                = null;
    public ?string $autre_comp_autre_deliv_desc           = null;
    public ?string $total_pertes_sang_deliv               = null;
    public ?string $actes_pdt_deliv                       = null;
    public ?string $deliv_artificielle                    = null;
    public ?string $rev_uterine_isolee_deliv              = null;
    public ?string $autres_actes_deliv                    = null;
    public ?string $ligature_art_hypogast_deliv           = null;
    public ?string $ligature_art_uterines_deliv           = null;
    public ?string $hysterectomie_hemostase_deliv         = null;
    public ?string $embolisation_arterielle_deliv         = null;
    public ?string $reduct_inversion_uterine_deliv        = null;
    public ?string $cure_chir_inversion_uterine_deliv     = null;
    public ?string $poids_placenta                        = null;
    public ?string $anomalie_placenta                     = null;
    public ?string $anomalie_placenta_desc                = null;
    public ?string $type_placentation                     = null;
    public ?string $type_placentation_desc                = null;
    public ?string $poids_placenta_1_bichorial            = null;
    public ?string $poids_placenta_2_bichorial            = null;
    public ?string $exam_anapath_placenta_demande         = null;
    public ?string $rques_placenta                        = null;
    public ?string $lesion_parties_molles                 = null;
    public ?string $episiotomie                           = null;
    public ?string $dechirure_perineale                   = null;
    public ?string $dechirure_perineale_liste             = null;
    public ?string $lesions_traumatiques_parties_molles   = null;
    public ?string $dechirure_vaginale                    = null;
    public ?string $dechirure_cervicale                   = null;
    public ?string $lesion_urinaire                       = null;
    public ?string $rupt_uterine                          = null;
    public ?string $thrombus                              = null;
    public ?string $autre_lesion                          = null;
    public ?string $autre_lesion_desc                     = null;
    public ?string $compte_rendu_delivrance               = null;
    public ?string $consignes_suite_couches               = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_accouchement_delivrances';
        $spec->key   = 'accouchement_delivrance_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['deliv_faite_par']                       = 'str';
        $props['datetime_deliv']                        = 'dateTime';
        $props['type_deliv']                            = 'enum list|dir|nat';
        $props['prod_deliv']                            = 'str';
        $props['dose_prod_deliv']                       = 'str';
        $props['datetime_inj_prod_deliv']               = 'dateTime';
        $props['voie_inj_prod_deliv']                   = 'str';
        $props['modalite_deliv']                        = 'enum list|comp|incomp|retplac';
        $props['comp_deliv']                            = 'bool';
        $props['hemorr_deliv']                          = 'bool';
        $props['retention_plac_comp_deliv']             = 'bool';
        $props['retention_plac_part_deliv']             = 'bool';
        $props['atonie_uterine_deliv']                  = 'bool';
        $props['trouble_coag_deliv']                    = 'bool';
        $props['transf_deliv']                          = 'bool';
        $props['nb_unites_transf_deliv']                = 'num min|0 max|9999';
        $props['autre_comp_deliv']                      = 'bool';
        $props['retention_plac_comp_sans_hemorr_deliv'] = 'bool';
        $props['retention_plac_part_sans_hemorr_deliv'] = 'bool';
        $props['inversion_uterine_deliv']               = 'bool';
        $props['autre_comp_autre_deliv']                = 'bool';
        $props['autre_comp_autre_deliv_desc']           = 'str';
        $props['total_pertes_sang_deliv']               = 'num min|0 max|9999';
        $props['actes_pdt_deliv']                       = 'bool';
        $props['deliv_artificielle']                    = 'bool';
        $props['rev_uterine_isolee_deliv']              = 'bool';
        $props['autres_actes_deliv']                    = 'bool';
        $props['ligature_art_hypogast_deliv']           = 'bool';
        $props['ligature_art_uterines_deliv']           = 'bool';
        $props['hysterectomie_hemostase_deliv']         = 'bool';
        $props['embolisation_arterielle_deliv']         = 'bool';
        $props['reduct_inversion_uterine_deliv']        = 'bool';
        $props['cure_chir_inversion_uterine_deliv']     = 'bool';
        $props['poids_placenta']                        = 'num min|0 max|9999';
        $props['anomalie_placenta']                     = 'enum list|non|malf|autre';
        $props['anomalie_placenta_desc']                = 'str';
        $props['type_placentation']                     = 'enum list|monomono|monobi|bibi|tritri|autre';
        $props['type_placentation_desc']                = 'str';
        $props['poids_placenta_1_bichorial']            = 'num min|0 max|9999';
        $props['poids_placenta_2_bichorial']            = 'num min|0 max|9999';
        $props['exam_anapath_placenta_demande']         = 'bool';
        $props['rques_placenta']                        = 'text helped';
        $props['lesion_parties_molles']                 = 'bool';
        $props['episiotomie']                           = 'bool';
        $props['dechirure_perineale']                   = 'bool';
        $props['dechirure_perineale_liste']             = 'enum list|1|2|3|4';
        $props['lesions_traumatiques_parties_molles']   = 'bool';
        $props['dechirure_vaginale']                    = 'bool';
        $props['dechirure_cervicale']                   = 'bool';
        $props['lesion_urinaire']                       = 'bool';
        $props['rupt_uterine']                          = 'bool';
        $props['thrombus']                              = 'bool';
        $props['autre_lesion']                          = 'bool';
        $props['autre_lesion_desc']                     = 'str';
        $props['compte_rendu_delivrance']               = 'text helped';
        $props['consignes_suite_couches']               = 'text helped';

        return $props;
    }
}
