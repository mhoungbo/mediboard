<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie consommation de produits toxiques
 */
class SyntheseToxicologie extends AbstractComponent
{
    public const RESOURCE_TYPE = 'syntheseToxicologie';

    /** @var ?int Primary key */
    public ?int $synthese_toxicologie_id = null;

    public ?string $conso_toxique_pdt_grossesse = null;
    public ?string $tabac_pdt_grossesse = null;
    public ?string $sevrage_tabac_pdt_grossesse = null;
    public ?string $date_arret_tabac = null;
    public ?string $alcool_pdt_grossesse = null;
    public ?string $cannabis_pdt_grossesse = null;
    public ?string $autres_subst_pdt_grossesse = null;
    public ?string $type_subst_pdt_grossesse = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_toxicologies';
        $spec->key   = 'synthese_toxicologie_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['conso_toxique_pdt_grossesse'] = 'bool';
        $props['tabac_pdt_grossesse']         = 'num min|0 max|255';
        $props['sevrage_tabac_pdt_grossesse'] = 'bool';
        $props['date_arret_tabac']            = 'date';
        $props['alcool_pdt_grossesse']        = 'num min|0 max|255';
        $props['cannabis_pdt_grossesse']      = 'num min|0 max|255';
        $props['autres_subst_pdt_grossesse']  = 'bool';
        $props['type_subst_pdt_grossesse']    = 'text helped';

        return $props;
    }
}
