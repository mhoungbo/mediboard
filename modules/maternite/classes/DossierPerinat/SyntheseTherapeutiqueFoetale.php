<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie th�rapeutiques foetales
 */
class SyntheseTherapeutiqueFoetale extends AbstractComponent
{
    public const RESOURCE_TYPE = 'syntheseTherapeutiqueFoetale';

    /** @var ?int Primary key */
    public ?int $synthese_therapeutique_foetale_id = null;

    public ?string $therapeutique_foetale = null;
    public ?string $amnioinfusion = null;
    public ?string $chirurgie_foetale = null;
    public ?string $derivation_foetale = null;
    public ?string $tranfusion_foetale_in_utero = null;
    public ?string $ex_sanguino_transfusion_foetale = null;
    public ?string $autre_therapeutiques_foetales = null;
    public ?string $reduction_embryonnaire = null;
    public ?string $type_reduction_embryonnaire = null;
    public ?string $photocoag_vx_placentaires = null;
    public ?string $rques_therapeutique_foetale = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_therapeutiques_foetales';
        $spec->key   = 'synthese_therapeutique_foetale_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['therapeutique_foetale']           = 'bool';
        $props['amnioinfusion']                   = 'bool';
        $props['chirurgie_foetale']               = 'bool';
        $props['derivation_foetale']              = 'bool';
        $props['tranfusion_foetale_in_utero']     = 'bool';
        $props['ex_sanguino_transfusion_foetale'] = 'bool';
        $props['autre_therapeutiques_foetales']   = 'bool';
        $props['reduction_embryonnaire']          = 'bool';
        $props['type_reduction_embryonnaire']     = 'enum list|reducinf13|reducsup13|select';
        $props['photocoag_vx_placentaires']       = 'bool';
        $props['rques_therapeutique_foetale']     = 'text';

        return $props;
    }
}
