<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie th�rapeutiques maternelles
 */
class SyntheseTherapeutiqueMaternelle extends AbstractComponent
{
    public const RESOURCE_TYPE = 'syntheseTherapeutiqueMaternelle';

    /** @var ?int Primary key */
    public ?int $synthese_therapeutique_maternelle_id = null;

    public ?string $therapeutique_grossesse_maternelle    = null;
    public ?string $antibio_pdt_grossesse                 = null;
    public ?string $type_antibio_pdt_grossesse            = null;
    public ?string $tocolyt_pdt_grossesse                 = null;
    public ?string $mode_admin_tocolyt_pdt_grossesse      = null;
    public ?string $cortico_pdt_grossesse                 = null;
    public ?string $nb_cures_cortico_pdt_grossesse        = null;
    public ?string $etat_dern_cure_cortico_pdt_grossesse  = null;
    public ?string $gammaglob_anti_d_pdt_grossesse        = null;
    public ?string $antihyp_pdt_grossesse                 = null;
    public ?string $aspirine_a_pdt_grossesse              = null;
    public ?string $barbit_antiepilept_pdt_grossesse      = null;
    public ?string $psychotropes_pdt_grossesse            = null;
    public ?string $subst_nicotine_pdt_grossesse          = null;
    public ?string $autre_therap_mater_pdt_grossesse      = null;
    public ?string $desc_autre_therap_mater_pdt_grossesse = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_therapeutiques_maternelles';
        $spec->key   = 'synthese_therapeutique_maternelle_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['therapeutique_grossesse_maternelle']    = 'bool';
        $props['antibio_pdt_grossesse']                 = 'bool';
        $props['type_antibio_pdt_grossesse']            = 'str';
        $props['tocolyt_pdt_grossesse']                 = 'bool';
        $props['mode_admin_tocolyt_pdt_grossesse']      = 'enum list|perf|peros';
        $props['cortico_pdt_grossesse']                 = 'bool';
        $props['nb_cures_cortico_pdt_grossesse']        = 'num min|0 max|255';
        $props['etat_dern_cure_cortico_pdt_grossesse']  = 'enum list|comp|incomp';
        $props['gammaglob_anti_d_pdt_grossesse']        = 'bool';
        $props['antihyp_pdt_grossesse']                 = 'bool';
        $props['aspirine_a_pdt_grossesse']              = 'bool';
        $props['barbit_antiepilept_pdt_grossesse']      = 'bool';
        $props['psychotropes_pdt_grossesse']            = 'bool';
        $props['subst_nicotine_pdt_grossesse']          = 'bool';
        $props['autre_therap_mater_pdt_grossesse']      = 'bool';
        $props['desc_autre_therap_mater_pdt_grossesse'] = 'str';

        return $props;
    }
}
