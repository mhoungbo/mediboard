<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les données relatives au déclenchement du travail durant l'accouchement
 */
class AccouchementDeclenchementTravail extends AbstractComponent
{
    public const RESOURCE_TYPE = 'accouchementTravail';

    /** @var ?int Primary key */
    public ?int $accouchement_declenchement_travail_id = null;

    public ?string $datetime_declenchement    = null;
    public ?string $motif_decl_conv           = null;
    public ?string $motif_decl_gross_prol     = null;
    public ?string $motif_decl_patho_mat      = null;
    public ?string $motif_decl_patho_foet     = null;
    public ?string $motif_decl_rpm            = null;
    public ?string $motif_decl_mort_iu        = null;
    public ?string $motif_decl_img            = null;
    public ?string $motif_decl_autre          = null;
    public ?string $motif_decl_autre_details  = null;
    public ?string $moyen_decl_ocyto          = null;
    public ?string $moyen_decl_prosta         = null;
    public ?string $moyen_decl_autre_medic    = null;
    public ?string $moyen_decl_meca           = null;
    public ?string $moyen_decl_rupture        = null;
    public ?string $moyen_decl_autre          = null;
    public ?string $moyen_decl_autre_details  = null;
    public ?string $score_bishop              = null;
    public ?string $score_bishop_dilatation   = null;
    public ?string $score_bishop_longueur     = null;
    public ?string $score_bishop_consistance  = null;
    public ?string $score_bishop_position     = null;
    public ?string $score_bishop_presentation = null;
    public ?string $remarques_declenchement   = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_declenchements_travail';
        $spec->key   = 'accouchement_declenchement_travail_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['datetime_declenchement']    = 'dateTime';
        $props['motif_decl_conv']           = 'bool';
        $props['motif_decl_gross_prol']     = 'bool';
        $props['motif_decl_patho_mat']      = 'bool';
        $props['motif_decl_patho_foet']     = 'bool';
        $props['motif_decl_rpm']            = 'bool';
        $props['motif_decl_mort_iu']        = 'bool';
        $props['motif_decl_img']            = 'bool';
        $props['motif_decl_autre']          = 'bool';
        $props['motif_decl_autre_details']  = 'str';
        $props['moyen_decl_ocyto']          = 'bool';
        $props['moyen_decl_prosta']         = 'bool';
        $props['moyen_decl_autre_medic']    = 'bool';
        $props['moyen_decl_meca']           = 'bool';
        $props['moyen_decl_rupture']        = 'bool';
        $props['moyen_decl_autre']          = 'bool';
        $props['moyen_decl_autre_details']  = 'str';
        $props['score_bishop']              = 'num min|0 max|255';
        $props['score_bishop_dilatation']   = 'enum list|0|1|2|3';
        $props['score_bishop_longueur']     = 'enum list|0|1|2|3';
        $props['score_bishop_consistance']  = 'enum list|0|1|2';
        $props['score_bishop_position']     = 'enum list|0|1|2';
        $props['score_bishop_presentation'] = 'enum list|0|1|2|3';
        $props['remarques_declenchement']   = 'text helped';

        return $props;
    }
}
