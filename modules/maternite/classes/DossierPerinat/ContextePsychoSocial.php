<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du contexte psycho-social du dossier p�rinatal
 */
class ContextePsychoSocial extends AbstractComponent
{
    public const RESOURCE_TYPE = 'contextePsychoSocial';

    /** @var ?int Primary key */
    public ?int $contexte_psycho_social_id = null;

    public ?string $rques_social = null;
    public ?string $enfants_foyer = null;
    public ?string $situation_part_enfance = null;
    public ?string $spe_perte_parent = null;
    public ?string $spe_maltraitance = null;
    public ?string $spe_mere_placee_enfance = null;
    public ?string $situation_part_adolescence = null;
    public ?string $spa_anorexie_boulimie = null;
    public ?string $spa_depression = null;
    public ?string $situation_part_familiale = null;
    public ?string $spf_violences_conjugales = null;
    public ?string $spf_mere_isolee = null;
    public ?string $spf_absence_entourage_fam = null;
    public ?string $stress_agression = null;
    public ?string $sa_agression_physique = null;
    public ?string $sa_agression_sexuelle = null;
    public ?string $sa_harcelement_travail = null;
    public ?string $rques_psychologie = null;
    public ?string $situation_accompagnement = null;
    public ?string $rques_accompagnement = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_contextes_psycho_sociaux';
        $spec->key   = 'contexte_psycho_social_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['rques_social']               = 'text helped';
        $props['enfants_foyer']              = 'num min|0 max|20';
        $props['situation_part_enfance']     = 'bool';
        $props['spe_perte_parent']           = 'bool';
        $props['spe_maltraitance']           = 'bool';
        $props['spe_mere_placee_enfance']    = 'bool';
        $props['situation_part_adolescence'] = 'bool';
        $props['spa_anorexie_boulimie']      = 'bool';
        $props['spa_depression']             = 'bool';
        $props['situation_part_familiale']   = 'bool';
        $props['spf_violences_conjugales']   = 'bool';
        $props['spf_mere_isolee']            = 'bool';
        $props['spf_absence_entourage_fam']  = 'bool';
        $props['stress_agression']           = 'bool';
        $props['sa_agression_physique']      = 'bool';
        $props['sa_agression_sexuelle']      = 'bool';
        $props['sa_harcelement_travail']     = 'bool';
        $props['rques_psychologie']          = 'text helped';
        $props['situation_accompagnement']   = 'enum list|n|s|p|sp';
        $props['rques_accompagnement']       = 'text helped';

        return $props;
    }
}
