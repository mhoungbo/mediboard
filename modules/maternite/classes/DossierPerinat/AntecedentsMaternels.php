<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant les ant�c�dents m�dicaux de la parturiente
 */
class AntecedentsMaternels extends AbstractComponent
{
    public const RESOURCE_TYPE = 'antecedentMaternel';

    /** @var ?string Primary key */
    public ?string $antecedents_maternels_id = null;

    public ?string $patho_ant                = null;
    public ?string $patho_ant_hta            = null;
    public ?string $patho_ant_diabete        = null;
    public ?string $patho_ant_epilepsie      = null;
    public ?string $patho_ant_asthme         = null;
    public ?string $patho_ant_pulm           = null;
    public ?string $patho_ant_thrombo_emb    = null;
    public ?string $patho_ant_cardio         = null;
    public ?string $patho_ant_auto_immune    = null;
    public ?string $patho_ant_hepato_dig     = null;
    public ?string $patho_ant_thyroide       = null;
    public ?string $patho_ant_uro_nephro     = null;
    public ?string $patho_ant_infectieuse    = null;
    public ?string $patho_ant_hemato         = null;
    public ?string $patho_ant_cancer_non_gyn = null;
    public ?string $patho_ant_psy            = null;
    public ?string $patho_ant_autre          = null;

    public ?string $chir_ant       = null;
    public ?string $chir_ant_rques = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_antecedents_maternels';
        $spec->key   = 'antecedents_maternels_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['patho_ant']                = 'bool';
        $props['patho_ant_hta']            = 'bool';
        $props['patho_ant_diabete']        = 'bool';
        $props['patho_ant_epilepsie']      = 'bool';
        $props['patho_ant_asthme']         = 'bool';
        $props['patho_ant_pulm']           = 'bool';
        $props['patho_ant_thrombo_emb']    = 'bool';
        $props['patho_ant_cardio']         = 'bool';
        $props['patho_ant_auto_immune']    = 'bool';
        $props['patho_ant_hepato_dig']     = 'bool';
        $props['patho_ant_thyroide']       = 'bool';
        $props['patho_ant_uro_nephro']     = 'bool';
        $props['patho_ant_infectieuse']    = 'bool';
        $props['patho_ant_hemato']         = 'bool';
        $props['patho_ant_cancer_non_gyn'] = 'bool';
        $props['patho_ant_psy']            = 'bool';
        $props['patho_ant_autre']          = 'text helped';
        $props['chir_ant']                 = 'bool';
        $props['chir_ant_rques']           = 'text helped';

        return $props;
    }
}
