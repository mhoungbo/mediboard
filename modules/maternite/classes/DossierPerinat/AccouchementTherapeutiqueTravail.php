<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les données du dossier périnatal concernant l'administration de produits thérapeutiques lors de l'accouchement
 */
class AccouchementTherapeutiqueTravail extends AbstractComponent
{
    public const RESOURCE_TYPE = 'accouchementTherapeutiqueTravail';

    /** @var ?int Primary key */
    public ?int $accouchement_therapeutique_travail_id = null;

    public ?string $therapeutique_pdt_travail     = null;
    public ?string $antibio_pdt_travail           = null;
    public ?string $antihypertenseurs_pdt_travail = null;
    public ?string $antispasmodiques_pdt_travail  = null;
    public ?string $tocolytiques_pdt_travail      = null;
    public ?string $ocytociques_pdt_travail       = null;
    public ?string $opiaces_pdt_travail           = null;
    public ?string $sedatifs_pdt_travail          = null;
    public ?string $amnioinfusion_pdt_travail     = null;
    public ?string $autre_therap_pdt_travail      = null;
    public ?string $desc_autre_therap_pdt_travail = null;
    public ?string $rques_therap_pdt_travail      = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_therapeutiques_travail';
        $spec->key   = 'accouchement_therapeutique_travail_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['therapeutique_pdt_travail']     = 'bool';
        $props['antibio_pdt_travail']           = 'bool';
        $props['antihypertenseurs_pdt_travail'] = 'bool';
        $props['antispasmodiques_pdt_travail']  = 'bool';
        $props['tocolytiques_pdt_travail']      = 'bool';
        $props['ocytociques_pdt_travail']       = 'bool';
        $props['opiaces_pdt_travail']           = 'bool';
        $props['sedatifs_pdt_travail']          = 'bool';
        $props['amnioinfusion_pdt_travail']     = 'bool';
        $props['autre_therap_pdt_travail']      = 'bool';
        $props['desc_autre_therap_pdt_travail'] = 'str';
        $props['rques_therap_pdt_travail']      = 'text helped';

        return $props;
    }
}
