<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObject;

/**
 * Components of the perinatal folder
 */
abstract class AbstractComponent extends CMbObject
{
    /** @var ?int */
    public ?int $dossier_perinat_id = null;

    /**
     * @inheritDoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] = 'ref class|CDossierPerinat cascade notNull';

        return $props;
    }
}
