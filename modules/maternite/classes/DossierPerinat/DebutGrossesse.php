<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant le d�but de la grossesse
 */
class DebutGrossesse extends AbstractComponent
{
    public const RESOURCE_TYPE = 'debutGrossesse';

    /** @var ?int Primary key */
    public ?int $debut_grossesse_id = null;

    public ?string $souhait_grossesse = null;
    public ?string $contraception_pre_grossesse = null;
    public ?string $grossesse_sous_contraception = null;
    public ?string $rques_contraception = null;
    public ?string $grossesse_apres_traitement = null;
    public ?string $type_traitement_grossesse = null;
    public ?string $origine_ovule = null;
    public ?string $origine_sperme = null;
    public ?string $rques_traitement_grossesse = null;
    public ?string $traitement_peri_conceptionnelle = null;
    public ?string $type_traitement_peri_conceptionnelle = null;
    public ?string $arret_traitement_peri_conceptionnelle = null;
    public ?string $rques_traitement_peri_conceptionnelle = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_debuts_grossesses';
        $spec->key   = 'debut_grossesse_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['souhait_grossesse']                     = 'bool';
        $props['contraception_pre_grossesse']           = 'enum list|none|pilu|ster|pres|impl|prog|avag|autre';
        $props['grossesse_sous_contraception']          = 'enum list|none|ster|pilu|autre';
        $props['rques_contraception']                   = 'text helped';
        $props['grossesse_apres_traitement']            = 'bool';
        $props['type_traitement_grossesse']             = 'enum list|ind|fiv|iad|iac|icsi|autre';
        $props['origine_ovule']                         = 'str';
        $props['origine_sperme']                        = 'str';
        $props['rques_traitement_grossesse']            = 'text helped';
        $props['traitement_peri_conceptionnelle']       = 'bool';
        $props['type_traitement_peri_conceptionnelle']  = 'str';
        $props['arret_traitement_peri_conceptionnelle'] = 'num min|0 max|42';
        $props['rques_traitement_peri_conceptionnelle'] = 'text helped';

        return $props;
    }
}
