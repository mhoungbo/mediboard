<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Behaviour of the parturient meanwhile the childbirth
 */
class CConduiteAccouchement extends AbstractComponent
{
    public const RESOURCE_TYPE = 'conduiteAccouchement';

    /** @var int */
    public $conduite_accouchement_id;

    /** @var string */
    public $presentation_fin_grossesse;

    /** @var string */
    public $autre_presentation_fin_grossesse;

    /** @var string */
    public $version_presentation_manoeuvre_ext;

    /** @var string */
    public $rques_presentation_fin_grossesse;

    /** @var string */
    public $etat_uterus_fin_grossesse;

    /** @var string */
    public $autre_anomalie_uterus_fin_grossesse;

    /** @var int */
    public $nb_cicatrices_uterus_fin_grossesse;

    /** @var string */
    public $date_derniere_hysterotomie;

    /** @var string */
    public $rques_etat_uterus;

    /** @var string */
    public $appreciation_clinique_etat_bassin;

    /** @var string */
    public $desc_appreciation_clinique_etat_bassin;

    /** @var string */
    public $pelvimetrie;

    /** @var string */
    public $desc_pelvimetrie;

    /** @var float */
    public $diametre_transverse_median;

    /** @var float */
    public $diametre_promonto_retro_pubien;

    /** @var float */
    public $diametre_bisciatique;

    /** @var float */
    public $indice_magnin;

    /** @var string */
    public $date_echo_fin_grossesse;

    /** @var int */
    public $sa_echo_fin_grossesse;

    /** @var int */
    public $bip_fin_grossesse;

    /** @var int */
    public $est_pond_fin_grossesse;

    /** @var int */
    public $est_pond_2e_foetus_fin_grossesse;

    /** @var string */
    public $conduite_a_tenir_acc;

    /** @var string */
    public $niveau_alerte_cesar;

    /** @var string */
    public $rques_conduite_a_tenir;

    /** @var string */
    public $date_decision_conduite_a_tenir_acc;

    /** @var int */
    public $valid_decision_conduite_a_tenir_acc_id;

    /** @var string */
    public $motif_conduite_a_tenir_acc;

    /** @var string */
    public $facteur_risque;

    /** @var string */
    public $date_prevue_interv;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_conduite_accouchement';
        $spec->key   = 'conduite_accouchement_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id']                    .= ' back|' . self::RESOURCE_TYPE;
        $props['presentation_fin_grossesse']             = 'enum list|ceph|siege|transv|autre';
        $props['autre_presentation_fin_grossesse']       = 'str';
        $props['version_presentation_manoeuvre_ext']     = 'enum list|nontente|tenteok|tenteko';
        $props['rques_presentation_fin_grossesse']       = 'text';
        $props['etat_uterus_fin_grossesse']              = 'enum list|norm|cicat|autreano';
        $props['autre_anomalie_uterus_fin_grossesse']    = 'str';
        $props['nb_cicatrices_uterus_fin_grossesse']     = 'num';
        $props['date_derniere_hysterotomie']             = 'date';
        $props['rques_etat_uterus']                      = 'text helped';
        $props['appreciation_clinique_etat_bassin']      = 'enum list|norm|anorm';
        $props['desc_appreciation_clinique_etat_bassin'] = 'str';
        $props['pelvimetrie']                            = 'enum list|norm|anorm';
        $props['desc_pelvimetrie']                       = 'str';
        $props['diametre_transverse_median']             = 'float';
        $props['diametre_promonto_retro_pubien']         = 'float';
        $props['diametre_bisciatique']                   = 'float';
        $props['indice_magnin']                          = 'float';
        $props['date_echo_fin_grossesse']                = 'date';
        $props['sa_echo_fin_grossesse']                  = 'num';
        $props['bip_fin_grossesse']                      = 'num';
        $props['est_pond_fin_grossesse']                 = 'num';
        $props['est_pond_2e_foetus_fin_grossesse']       = 'num';
        $props['conduite_a_tenir_acc']                   = 'enum list|bassespon|bassedecl|cesar';
        $props['niveau_alerte_cesar']                    = 'enum list|1|2|3';
        $props['rques_conduite_a_tenir']                 = 'text helped';
        $props['date_decision_conduite_a_tenir_acc']     = 'date';
        $props['valid_decision_conduite_a_tenir_acc_id'] = 'ref class|CMediusers back|conduites_validees';
        $props['motif_conduite_a_tenir_acc']             = 'str';
        $props['facteur_risque']                         = 'text helped';
        $props['date_prevue_interv']                     = 'date';

        return $props;
    }
}
