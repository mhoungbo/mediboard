<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie pathologies foetales
 */
class SynthesePathologieFoetale extends AbstractComponent
{
    public const RESOURCE_TYPE = 'synthesePathologieFoetale';

    /** @var ?int Primary key */
    public ?int $synthese_pathologie_foetale_id = null;

    public ?string $patho_foetale_in_utero              = null;
    public ?string $anomalie_croiss_intra_uterine       = null;
    public ?string $type_anomalie_croiss_intra_uterine  = null;
    public ?string $ag_anomalie_croiss_intra_uterine    = null;
    public ?string $signes_hypoxie_foetale_chronique    = null;
    public ?string $ag_signes_hypoxie_foetale_chronique = null;
    public ?string $hypoxie_foetale_anomalie_doppler    = null;
    public ?string $hypoxie_foetale_anomalie_rcf        = null;
    public ?string $hypoxie_foetale_alter_profil_biophy = null;
    public ?string $anomalie_constit_foetus             = null;
    public ?string $ag_anomalie_constit_foetus          = null;
    public ?string $malformation_isolee_foetus          = null;
    public ?string $anomalie_chromo_foetus              = null;
    public ?string $synd_polymalform_foetus             = null;
    public ?string $anomalie_genique_foetus             = null;
    public ?string $rques_anomalies_foetus              = null;
    public ?string $foetopathie_infect_acquise          = null;
    public ?string $type_foetopathie_infect_acquise     = null;
    public ?string $autre_foetopathie_infect_acquise    = null;
    public ?string $ag_foetopathie_infect_acquise       = null;
    public ?string $autre_patho_foetale                 = null;
    public ?string $ag_autre_patho_foetale              = null;
    public ?string $allo_immun_anti_rh_foetale          = null;
    public ?string $autre_allo_immun_foetale            = null;
    public ?string $anas_foeto_plac_non_immun           = null;
    public ?string $trouble_rcf_foetus                  = null;
    public ?string $foetopathie_alcoolique              = null;
    public ?string $grosse_abdo_foetus_viable           = null;
    public ?string $mort_foetale_in_utero_in_22sa       = null;
    public ?string $autre_patho_foetale_autre           = null;
    public ?string $desc_autre_patho_foetale_autre      = null;
    public ?string $patho_foetale_gross_mult            = null;
    public ?string $ag_patho_foetale_gross_mult         = null;
    public ?string $avort_foetus_gross_mult             = null;
    public ?string $mort_foetale_in_utero_gross_mutl    = null;
    public ?string $synd_transf_transf_gross_mult       = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_pathologies_foetales';
        $spec->key   = 'synthese_pathologie_foetale_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['patho_foetale_in_utero']              = 'bool';
        $props['anomalie_croiss_intra_uterine']       = 'bool';
        $props['type_anomalie_croiss_intra_uterine']  = 'enum list|retard|macrosomie';
        $props['ag_anomalie_croiss_intra_uterine']    = 'num min|0 max|255';
        $props['signes_hypoxie_foetale_chronique']    = 'bool';
        $props['ag_signes_hypoxie_foetale_chronique'] = 'num min|0 max|255';
        $props['hypoxie_foetale_anomalie_doppler']    = 'bool';
        $props['hypoxie_foetale_anomalie_rcf']        = 'bool';
        $props['hypoxie_foetale_alter_profil_biophy'] = 'bool';
        $props['anomalie_constit_foetus']             = 'bool';
        $props['ag_anomalie_constit_foetus']          = 'num min|0 max|255';
        $props['malformation_isolee_foetus']          = 'bool';
        $props['anomalie_chromo_foetus']              = 'bool';
        $props['synd_polymalform_foetus']             = 'bool';
        $props['anomalie_genique_foetus']             = 'bool';
        $props['rques_anomalies_foetus']              = 'text';
        $props['foetopathie_infect_acquise']          = 'bool';
        $props['type_foetopathie_infect_acquise']     = 'enum list|cmv|toxo|rub|parvo|autre';
        $props['autre_foetopathie_infect_acquise']    = 'str';
        $props['ag_foetopathie_infect_acquise']       = 'num min|0 max|255';
        $props['autre_patho_foetale']                 = 'bool';
        $props['ag_autre_patho_foetale']              = 'num min|0 max|255';
        $props['allo_immun_anti_rh_foetale']          = 'bool';
        $props['autre_allo_immun_foetale']            = 'bool';
        $props['anas_foeto_plac_non_immun']           = 'bool';
        $props['trouble_rcf_foetus']                  = 'bool';
        $props['foetopathie_alcoolique']              = 'bool';
        $props['grosse_abdo_foetus_viable']           = 'bool';
        $props['mort_foetale_in_utero_in_22sa']       = 'bool';
        $props['autre_patho_foetale_autre']           = 'bool';
        $props['desc_autre_patho_foetale_autre']      = 'str';
        $props['patho_foetale_gross_mult']            = 'bool';
        $props['ag_patho_foetale_gross_mult']         = 'num min|0 max|255';
        $props['avort_foetus_gross_mult']             = 'bool';
        $props['mort_foetale_in_utero_gross_mutl']    = 'bool';
        $props['synd_transf_transf_gross_mult']       = 'bool';

        return $props;
    }
}
