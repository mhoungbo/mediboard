<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es concernant les pathologies et anomalies survenues avant le travail
 */
class AccouchementPathologieAvantTravail extends AbstractComponent
{
    public const RESOURCE_TYPE = 'accouchementPathologieAvantTravail';

    /** @var ?int Primary key */
    public ?int $pathologie_avant_travail_id = null;

    public ?string $anom_av_trav = null;
    public ?string $anom_pres_av_trav = null;
    public ?string $anom_pres_av_trav_siege = null;
    public ?string $anom_pres_av_trav_transverse = null;
    public ?string $anom_pres_av_trav_face = null;
    public ?string $anom_pres_av_trav_anormale = null;
    public ?string $anom_pres_av_trav_autre = null;
    public ?string $anom_bassin_av_trav = null;
    public ?string $anom_bassin_av_trav_bassin_retreci = null;
    public ?string $anom_bassin_av_trav_malform_bassin = null;
    public ?string $anom_bassin_av_trav_foetus = null;
    public ?string $anom_bassin_av_trav_disprop_foetopelv = null;
    public ?string $anom_bassin_av_trav_disprop_difform_foet = null;
    public ?string $anom_bassin_av_trav_disprop_sans_prec = null;
    public ?string $anom_genit_hors_trav = null;
    public ?string $anom_genit_hors_trav_uterus_cicat = null;
    public ?string $anom_genit_hors_trav_rupt_uterine = null;
    public ?string $anom_genit_hors_trav_fibrome_uterin = null;
    public ?string $anom_genit_hors_trav_malform_uterine = null;
    public ?string $anom_genit_hors_trav_anom_vaginales = null;
    public ?string $anom_genit_hors_trav_chir_ant_perinee = null;
    public ?string $anom_genit_hors_trav_prolapsus_vaginal = null;
    public ?string $anom_plac_av_trav = null;
    public ?string $anom_plac_av_trav_plac_prae_sans_hemo = null;
    public ?string $anom_plac_av_trav_plac_prae_avec_hemo = null;
    public ?string $anom_plac_av_trav_hrp_avec_trouble_coag = null;
    public ?string $anom_plac_av_trav_hrp_sans_trouble_coag = null;
    public ?string $anom_plac_av_trav_autre_hemo_avec_trouble_coag = null;
    public ?string $anom_plac_av_trav_autre_hemo_sans_trouble_coag = null;
    public ?string $anom_plac_av_trav_transf_foeto_mater = null;
    public ?string $anom_plac_av_trav_infect_sac_membranes = null;
    public ?string $rupt_premat_membranes = null;
    public ?string $rupt_premat_membranes_rpm_inf37sa_sans_toco = null;
    public ?string $rupt_premat_membranes_rpm_inf37sa_avec_toco = null;
    public ?string $rupt_premat_membranes_rpm_sup37sa = null;
    public ?string $patho_foet_chron = null;
    public ?string $patho_foet_chron_retard_croiss = null;
    public ?string $patho_foet_chron_macrosom_foetale = null;
    public ?string $patho_foet_chron_immun_antirh = null;
    public ?string $patho_foet_chron_autre_allo_immun = null;
    public ?string $patho_foet_chron_anasarque_non_immun = null;
    public ?string $patho_foet_chron_anasarque_immun = null;
    public ?string $patho_foet_chron_hypoxie_foetale = null;
    public ?string $patho_foet_chron_trouble_rcf = null;
    public ?string $patho_foet_chron_mort_foatale_in_utero = null;
    public ?string $patho_mat_foet_av_trav = null;
    public ?string $patho_mat_foet_av_trav_hta_gravid = null;
    public ?string $patho_mat_foet_av_trav_preec_moderee = null;
    public ?string $patho_mat_foet_av_trav_preec_severe = null;
    public ?string $patho_mat_foet_av_trav_hellp = null;
    public ?string $patho_mat_foet_av_trav_preec_hta = null;
    public ?string $patho_mat_foet_av_trav_eclamp = null;
    public ?string $patho_mat_foet_av_trav_diabete_id = null;
    public ?string $patho_mat_foet_av_trav_diabete_nid = null;
    public ?string $patho_mat_foet_av_trav_steatose_grav = null;
    public ?string $patho_mat_foet_av_trav_herpes_genit = null;
    public ?string $patho_mat_foet_av_trav_condylomes = null;
    public ?string $patho_mat_foet_av_trav_hep_b = null;
    public ?string $patho_mat_foet_av_trav_hep_c = null;
    public ?string $patho_mat_foet_av_trav_vih = null;
    public ?string $patho_mat_foet_av_trav_sida = null;
    public ?string $patho_mat_foet_av_trav_fievre = null;
    public ?string $patho_mat_foet_av_trav_gross_prolong = null;
    public ?string $patho_mat_foet_av_trav_autre = null;
    public ?string $autre_motif_cesarienne = null;
    public ?string $autre_motif_cesarienne_conv = null;
    public ?string $autre_motif_cesarienne_mult = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_pathologies_avant_travail';
        $spec->key   = 'pathologie_avant_travail_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['anom_av_trav']                                   = 'bool';
        $props['anom_pres_av_trav']                              = 'bool';
        $props['anom_pres_av_trav_siege']                        = 'bool';
        $props['anom_pres_av_trav_transverse']                   = 'bool';
        $props['anom_pres_av_trav_face']                         = 'bool';
        $props['anom_pres_av_trav_anormale']                     = 'bool';
        $props['anom_pres_av_trav_autre']                        = 'bool';
        $props['anom_bassin_av_trav']                            = 'bool';
        $props['anom_bassin_av_trav_bassin_retreci']             = 'bool';
        $props['anom_bassin_av_trav_malform_bassin']             = 'bool';
        $props['anom_bassin_av_trav_foetus']                     = 'bool';
        $props['anom_bassin_av_trav_disprop_foetopelv']          = 'bool';
        $props['anom_bassin_av_trav_disprop_difform_foet']       = 'bool';
        $props['anom_bassin_av_trav_disprop_sans_prec']          = 'bool';
        $props['anom_genit_hors_trav']                           = 'bool';
        $props['anom_genit_hors_trav_uterus_cicat']              = 'bool';
        $props['anom_genit_hors_trav_rupt_uterine']              = 'bool';
        $props['anom_genit_hors_trav_fibrome_uterin']            = 'bool';
        $props['anom_genit_hors_trav_malform_uterine']           = 'bool';
        $props['anom_genit_hors_trav_anom_vaginales']            = 'bool';
        $props['anom_genit_hors_trav_chir_ant_perinee']          = 'bool';
        $props['anom_genit_hors_trav_prolapsus_vaginal']         = 'bool';
        $props['anom_plac_av_trav']                              = 'bool';
        $props['anom_plac_av_trav_plac_prae_sans_hemo']          = 'bool';
        $props['anom_plac_av_trav_plac_prae_avec_hemo']          = 'bool';
        $props['anom_plac_av_trav_hrp_avec_trouble_coag']        = 'bool';
        $props['anom_plac_av_trav_hrp_sans_trouble_coag']        = 'bool';
        $props['anom_plac_av_trav_autre_hemo_avec_trouble_coag'] = 'bool';
        $props['anom_plac_av_trav_autre_hemo_sans_trouble_coag'] = 'bool';
        $props['anom_plac_av_trav_transf_foeto_mater']           = 'bool';
        $props['anom_plac_av_trav_infect_sac_membranes']         = 'bool';
        $props['rupt_premat_membranes']                          = 'bool';
        $props['rupt_premat_membranes_rpm_inf37sa_sans_toco']    = 'bool';
        $props['rupt_premat_membranes_rpm_inf37sa_avec_toco']    = 'bool';
        $props['rupt_premat_membranes_rpm_sup37sa']              = 'bool';
        $props['patho_foet_chron']                               = 'bool';
        $props['patho_foet_chron_retard_croiss']                 = 'bool';
        $props['patho_foet_chron_macrosom_foetale']              = 'bool';
        $props['patho_foet_chron_immun_antirh']                  = 'bool';
        $props['patho_foet_chron_autre_allo_immun']              = 'bool';
        $props['patho_foet_chron_anasarque_non_immun']           = 'bool';
        $props['patho_foet_chron_anasarque_immun']               = 'bool';
        $props['patho_foet_chron_hypoxie_foetale']               = 'bool';
        $props['patho_foet_chron_trouble_rcf']                   = 'bool';
        $props['patho_foet_chron_mort_foatale_in_utero']         = 'bool';
        $props['patho_mat_foet_av_trav']                         = 'bool';
        $props['patho_mat_foet_av_trav_hta_gravid']              = 'bool';
        $props['patho_mat_foet_av_trav_preec_moderee']           = 'bool';
        $props['patho_mat_foet_av_trav_preec_severe']            = 'bool';
        $props['patho_mat_foet_av_trav_hellp']                   = 'bool';
        $props['patho_mat_foet_av_trav_preec_hta']               = 'bool';
        $props['patho_mat_foet_av_trav_eclamp']                  = 'bool';
        $props['patho_mat_foet_av_trav_diabete_id']              = 'bool';
        $props['patho_mat_foet_av_trav_diabete_nid']             = 'bool';
        $props['patho_mat_foet_av_trav_steatose_grav']           = 'bool';
        $props['patho_mat_foet_av_trav_herpes_genit']            = 'bool';
        $props['patho_mat_foet_av_trav_condylomes']              = 'bool';
        $props['patho_mat_foet_av_trav_hep_b']                   = 'bool';
        $props['patho_mat_foet_av_trav_hep_c']                   = 'bool';
        $props['patho_mat_foet_av_trav_vih']                     = 'bool';
        $props['patho_mat_foet_av_trav_sida']                    = 'bool';
        $props['patho_mat_foet_av_trav_fievre']                  = 'bool';
        $props['patho_mat_foet_av_trav_gross_prolong']           = 'bool';
        $props['patho_mat_foet_av_trav_autre']                   = 'bool';
        $props['autre_motif_cesarienne']                         = 'bool';
        $props['autre_motif_cesarienne_conv']                    = 'bool';
        $props['autre_motif_cesarienne_mult']                    = 'bool';

        return $props;
    }
}
