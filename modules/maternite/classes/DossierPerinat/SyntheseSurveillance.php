<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie surveillance de la grossesse
 */
class SyntheseSurveillance extends AbstractComponent
{
    public const RESOURCE_TYPE = 'syntheseSurveillance';

    /** @var ?int Primary key */
    public ?int $synthese_surveillance_id = null;

    public ?string $nb_consult_total_prenatal = null;
    public ?string $nb_consult_total_equipe = null;
    public ?string $entretien_prem_trim = null;
    public ?string $hospitalisation = null;
    public ?string $nb_sejours = null;
    public ?string $nb_total_jours_hospi = null;
    public ?string $sage_femme_domicile = null;
    public ?string $transfert_in_utero = null;
    public ?string $consult_preanesth = null;
    public ?string $consult_centre_diag_prenat = null;
    public ?string $preparation_naissance = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_surveillances';
        $spec->key   = 'synthese_surveillance_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['nb_consult_total_prenatal']  = 'num min|0 max|255';
        $props['nb_consult_total_equipe']    = 'num min|0 max|255';
        $props['entretien_prem_trim']        = 'bool';
        $props['hospitalisation']            = 'enum list|non|mater|had|autre';
        $props['nb_sejours']                 = 'num min|0 max|255';
        $props['nb_total_jours_hospi']       = 'num min|0 max|255';
        $props['sage_femme_domicile']        = 'bool';
        $props['transfert_in_utero']         = 'bool';
        $props['consult_preanesth']          = 'bool';
        $props['consult_centre_diag_prenat'] = 'enum list|non|etab|horsetab';
        $props['preparation_naissance']      = 'enum list|non|int|ext|intext';

        return $props;
    }
}
