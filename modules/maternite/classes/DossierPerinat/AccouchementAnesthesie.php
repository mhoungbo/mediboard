<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant l'anesth�sie lors de l'accouchement
 */
class AccouchementAnesthesie extends AbstractComponent
{
    public const RESOURCE_TYPE = 'accouchementAnesthesie';

    /** @var ?int Primary key */
    public ?int $accouchement_anesthesie_id = null;

    public ?string $anesth_avant_naiss                  = null;
    public ?string $datetime_anesth_avant_naiss         = null;
    public ?string $suivi_anesth_avant_naiss_par        = null;
    public ?string $alr_avant_naiss                     = null;
    public ?string $alr_peri_avant_naiss                = null;
    public ?string $alr_peri_avant_naiss_inj_unique     = null;
    public ?string $alr_peri_avant_naiss_reinj          = null;
    public ?string $alr_peri_avant_naiss_cat_autopousse = null;
    public ?string $alr_peri_avant_naiss_cat_pcea       = null;
    public ?string $alr_rachi_avant_naiss               = null;
    public ?string $alr_rachi_avant_naiss_inj_unique    = null;
    public ?string $alr_rachi_avant_naiss_cat           = null;
    public ?string $alr_peri_rachi_avant_naiss          = null;
    public ?string $ag_avant_naiss                      = null;
    public ?string $ag_avant_naiss_directe              = null;
    public ?string $ag_avant_naiss_apres_peri           = null;
    public ?string $ag_avant_naiss_apres_rachi          = null;
    public ?string $al_avant_naiss                      = null;
    public ?string $al_bloc_avant_naiss                 = null;
    public ?string $al_autre_avant_naiss                = null;
    public ?string $al_autre_avant_naiss_desc           = null;
    public ?string $autre_analg_avant_naiss             = null;
    public ?string $autre_analg_avant_naiss_desc        = null;
    public ?string $fibro_laryngee                      = null;
    public ?string $asa_anesth_avant_naissance          = null;
    public ?string $moment_anesth_avant_naissance       = null;
    public ?string $anesth_spec_2eme_enfant             = null;
    public ?string $anesth_spec_2eme_enfant_desc        = null;
    public ?string $rques_anesth_avant_naiss            = null;
    public ?string $comp_anesth_avant_naiss             = null;
    public ?string $hypotension_alr_avant_naiss         = null;
    public ?string $autre_comp_alr_avant_naiss          = null;
    public ?string $autre_comp_alr_avant_naiss_desc     = null;
    public ?string $mendelson_ag_avant_naiss            = null;
    public ?string $comp_pulm_ag_avant_naiss            = null;
    public ?string $comp_card_ag_avant_naiss            = null;
    public ?string $comp_cereb_ag_avant_naiss           = null;
    public ?string $comp_allerg_tox_ag_avant_naiss      = null;
    public ?string $autre_comp_ag_avant_naiss           = null;
    public ?string $autre_comp_ag_avant_naiss_desc      = null;
    public ?string $anesth_apres_naissance              = null;
    public ?string $anesth_apres_naissance_desc         = null;
    public ?string $rques_anesth_apres_naissance        = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_accouchement_anesthesies';
        $spec->key   = 'accouchement_anesthesie_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['anesth_avant_naiss']                  = 'bool';
        $props['datetime_anesth_avant_naiss']         = 'dateTime';
        $props['suivi_anesth_avant_naiss_par']        = 'str';
        $props['alr_avant_naiss']                     = 'bool';
        $props['alr_peri_avant_naiss']                = 'bool';
        $props['alr_peri_avant_naiss_inj_unique']     = 'bool';
        $props['alr_peri_avant_naiss_reinj']          = 'bool';
        $props['alr_peri_avant_naiss_cat_autopousse'] = 'bool';
        $props['alr_peri_avant_naiss_cat_pcea']       = 'bool';
        $props['alr_rachi_avant_naiss']               = 'bool';
        $props['alr_rachi_avant_naiss_inj_unique']    = 'bool';
        $props['alr_rachi_avant_naiss_cat']           = 'bool';
        $props['alr_peri_rachi_avant_naiss']          = 'bool';
        $props['ag_avant_naiss']                      = 'bool';
        $props['ag_avant_naiss_directe']              = 'bool';
        $props['ag_avant_naiss_apres_peri']           = 'bool';
        $props['ag_avant_naiss_apres_rachi']          = 'bool';
        $props['al_avant_naiss']                      = 'bool';
        $props['al_bloc_avant_naiss']                 = 'bool';
        $props['al_autre_avant_naiss']                = 'bool';
        $props['al_autre_avant_naiss_desc']           = 'str';
        $props['autre_analg_avant_naiss']             = 'bool';
        $props['autre_analg_avant_naiss_desc']        = 'str';
        $props['fibro_laryngee']                      = 'bool';
        $props['asa_anesth_avant_naissance']          = 'num min|0 max|20';
        $props['moment_anesth_avant_naissance']       = 'enum list|debtrav|intervvb|cesar';
        $props['anesth_spec_2eme_enfant']             = 'enum list|non|ag|rachi|autre';
        $props['anesth_spec_2eme_enfant_desc']        = 'str';
        $props['rques_anesth_avant_naiss']            = 'text helped';
        $props['comp_anesth_avant_naiss']             = 'bool';
        $props['hypotension_alr_avant_naiss']         = 'bool';
        $props['autre_comp_alr_avant_naiss']          = 'bool';
        $props['autre_comp_alr_avant_naiss_desc']     = 'str';
        $props['mendelson_ag_avant_naiss']            = 'bool';
        $props['comp_pulm_ag_avant_naiss']            = 'bool';
        $props['comp_card_ag_avant_naiss']            = 'bool';
        $props['comp_cereb_ag_avant_naiss']           = 'bool';
        $props['comp_allerg_tox_ag_avant_naiss']      = 'bool';
        $props['autre_comp_ag_avant_naiss']           = 'bool';
        $props['autre_comp_ag_avant_naiss_desc']      = 'str';
        $props['anesth_apres_naissance']              = 'enum list|non|ag|al|autre';
        $props['anesth_apres_naissance_desc']         = 'str';
        $props['rques_anesth_apres_naissance']        = 'text helped';

        return $props;
    }
}
