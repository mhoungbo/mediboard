<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant l'admission de la parturiente
 */
class Admission extends AbstractComponent
{
    public const RESOURCE_TYPE = 'admission';

    /** @var ?int Primary key */
    public ?int $dossier_perinat_admission_id = null;

    public ?string $ag_admission                 = null;
    public ?string $ag_jours_admission           = null;
    public ?string $motif_admission              = null;
    public ?string $ag_ruptures_membranes        = null;
    public ?string $ag_jours_ruptures_membranes  = null;
    public ?string $delai_rupture_travail_jours  = null;
    public ?string $delai_rupture_travail_heures = null;
    public ?string $date_ruptures_membranes      = null;
    public ?string $rques_admission              = null;

    public ?string $exam_entree_oedeme = null;
    public ?string $exam_entree_bruits_du_coeur = null;
    public ?string $exam_entree_mvt_actifs_percus = null;
    public ?string $exam_entree_contractions = null;
    public ?string $exam_entree_presentation = null;
    public ?string $exam_entree_col = null;
    public ?string $exam_entree_liquide_amnio = null;
    public ?string $exam_entree_indice_bishop = null;

    public ?string $exam_entree_prelev_urine = null;
    public ?string $exam_entree_proteinurie = null;
    public ?string $exam_entree_glycosurie = null;
    public ?string $exam_entree_prelev_vaginal = null;
    public ?string $exam_entree_prelev_vaginal_desc = null;
    public ?string $exam_entree_rcf = null;
    public ?string $exam_entree_rcf_desc = null;
    public ?string $exam_entree_amnioscopie = null;
    public ?string $exam_entree_amnioscopie_desc = null;
    public ?string $exam_entree_autres = null;
    public ?string $exam_entree_autres_desc = null;
    public ?string $rques_exam_entree = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_admissions';
        $spec->key   = 'dossier_perinat_admission_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['ag_admission']                 = 'num min|0 max|255';
        $props['ag_jours_admission']           = 'num min|0 max|9999';
        $props['motif_admission']              = 'enum list|travsp|travspmbint|travspmbromp|ruptmb|decl|cesar|urg|admpostacc';
        $props['ag_ruptures_membranes']        = 'num min|0 max|255';
        $props['ag_jours_ruptures_membranes']  = 'num min|0 max|9999';
        $props['delai_rupture_travail_jours']  = 'num min|0 max|255';
        $props['delai_rupture_travail_heures'] = 'num min|0 max|255';
        $props['date_ruptures_membranes']      = 'dateTime';
        $props['rques_admission']              = 'text helped';

        $props['exam_entree_oedeme']            = 'str';
        $props['exam_entree_bruits_du_coeur']   = 'str';
        $props['exam_entree_mvt_actifs_percus'] = 'str';
        $props['exam_entree_contractions']      = 'str';
        $props['exam_entree_presentation']      = 'str';
        $props['exam_entree_col']               = 'str';
        $props['exam_entree_liquide_amnio']     = 'str';
        $props['exam_entree_indice_bishop']     = 'num min|0 max|255';

        $props['exam_entree_prelev_urine']        = 'bool';
        $props['exam_entree_proteinurie']         = 'str';
        $props['exam_entree_glycosurie']          = 'str';
        $props['exam_entree_prelev_vaginal']      = 'bool';
        $props['exam_entree_prelev_vaginal_desc'] = 'str';
        $props['exam_entree_rcf']                 = 'bool';
        $props['exam_entree_rcf_desc']            = 'str';
        $props['exam_entree_amnioscopie']         = 'bool';
        $props['exam_entree_amnioscopie_desc']    = 'str';
        $props['exam_entree_autres']              = 'bool';
        $props['exam_entree_autres_desc']         = 'str';
        $props['rques_exam_entree']               = 'text helped';

        return $props;
    }
}
