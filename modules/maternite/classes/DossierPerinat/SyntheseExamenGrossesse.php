<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie examens lors de la grossesse
 */
class SyntheseExamenGrossesse extends AbstractComponent
{
    public const RESOURCE_TYPE = 'syntheseExamenGrossesse';

    /** @var ?int Primary key */
    public ?int $synthese_examen_grossesse_id = null;

    public ?string $nb_total_echographies        = null;
    public ?string $echo_1er_trim                = null;
    public ?string $resultat_echo_1er_trim       = null;
    public ?string $resultat_autre_echo_1er_trim = null;
    public ?string $ag_echo_1er_trim             = null;
    public ?string $echo_2e_trim                 = null;
    public ?string $resultat_echo_2e_trim        = null;
    public ?string $resultat_autre_echo_2e_trim  = null;
    public ?string $ag_echo_2e_trim              = null;
    public ?string $doppler_2e_trim              = null;
    public ?string $resultat_doppler_2e_trim     = null;
    public ?string $echo_3e_trim                 = null;
    public ?string $resultat_echo_3e_trim        = null;
    public ?string $resultat_autre_echo_3e_trim  = null;
    public ?string $ag_echo_3e_trim              = null;
    public ?string $doppler_3e_trim              = null;
    public ?string $resultat_doppler_3e_trim     = null;

    public ?string $prelevements_foetaux             = null;
    public ?string $indication_prelevements_foetaux  = null;
    public ?string $biopsie_trophoblaste             = null;
    public ?string $resultat_biopsie_trophoblaste    = null;
    public ?string $rques_biopsie_trophoblaste       = null;
    public ?string $ag_biopsie_trophoblaste          = null;
    public ?string $amniocentese                     = null;
    public ?string $resultat_amniocentese            = null;
    public ?string $rques_amniocentese               = null;
    public ?string $ag_amniocentese                  = null;
    public ?string $cordocentese                     = null;
    public ?string $resultat_cordocentese            = null;
    public ?string $rques_cordocentese               = null;
    public ?string $ag_cordocentese                  = null;
    public ?string $autre_prelevements_foetaux       = null;
    public ?string $rques_autre_prelevements_foetaux = null;
    public ?string $ag_autre_prelevements_foetaux    = null;

    public ?string $prelevements_bacterio_mater   = null;
    public ?string $prelevement_vaginal           = null;
    public ?string $resultat_prelevement_vaginal  = null;
    public ?string $rques_prelevement_vaginal     = null;
    public ?string $ag_prelevement_vaginal        = null;
    public ?string $prelevement_urinaire          = null;
    public ?string $resultat_prelevement_urinaire = null;
    public ?string $rques_prelevement_urinaire    = null;
    public ?string $ag_prelevement_urinaire       = null;

    public ?string $marqueurs_seriques           = null;
    public ?string $resultats_marqueurs_seriques = null;
    public ?string $rques_marqueurs_seriques     = null;

    public ?string $depistage_diabete          = null;
    public ?string $resultat_depistage_diabete = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_examens_grossesses';
        $spec->key   = 'synthese_examen_grossesse_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['nb_total_echographies']            = 'num min|0 max|255';
        $props['echo_1er_trim']                    = 'bool';
        $props['resultat_echo_1er_trim']           = 'enum list|normal|anomorpho|corrterme|autre';
        $props['resultat_autre_echo_1er_trim']     = 'str';
        $props['ag_echo_1er_trim']                 = 'num min|0 max|255';
        $props['echo_2e_trim']                     = 'bool';
        $props['resultat_echo_2e_trim']            = 'enum list|normal|anomorpho|corrterme|autre';
        $props['resultat_autre_echo_2e_trim']      = 'str';
        $props['ag_echo_2e_trim']                  = 'num min|0 max|255';
        $props['doppler_2e_trim']                  = 'bool';
        $props['resultat_doppler_2e_trim']         = 'enum list|normal|anomalie';
        $props['echo_3e_trim']                     = 'bool';
        $props['resultat_echo_3e_trim']            = 'enum list|normal|anomorpho|corrterme|autre';
        $props['resultat_autre_echo_3e_trim']      = 'str';
        $props['ag_echo_3e_trim']                  = 'num min|0 max|255';
        $props['doppler_3e_trim']                  = 'bool';
        $props['resultat_doppler_3e_trim']         = 'enum list|normal|anomalie';

        $props['prelevements_foetaux']             = 'enum list|nonprescrits|refuses|faits';
        $props['indication_prelevements_foetaux']  = 'enum list|agemater|nuque|t21|appelecho|atcd|conv';
        $props['biopsie_trophoblaste']             = 'bool';
        $props['resultat_biopsie_trophoblaste']    = 'enum list|normal|anomalie';
        $props['rques_biopsie_trophoblaste']       = 'str';
        $props['ag_biopsie_trophoblaste']          = 'num min|0 max|255';
        $props['amniocentese']                     = 'bool';
        $props['resultat_amniocentese']            = 'enum list|normal|anomalie';
        $props['rques_amniocentese']               = 'str';
        $props['ag_amniocentese']                  = 'num min|0 max|255';
        $props['cordocentese']                     = 'bool';
        $props['resultat_cordocentese']            = 'enum list|normal|anomalie';
        $props['rques_cordocentese']               = 'str';
        $props['ag_cordocentese']                  = 'num min|0 max|255';
        $props['autre_prelevements_foetaux']       = 'bool';
        $props['rques_autre_prelevements_foetaux'] = 'str';
        $props['ag_autre_prelevements_foetaux']    = 'num min|0 max|255';

        $props['prelevements_bacterio_mater']   = 'enum list|nonprescrits|refuses|faits';
        $props['prelevement_vaginal']           = 'bool';
        $props['resultat_prelevement_vaginal']  = 'enum list|negatif|streptob|autre';
        $props['rques_prelevement_vaginal']     = 'str';
        $props['ag_prelevement_vaginal']        = 'num min|0 max|255';
        $props['prelevement_urinaire']          = 'bool';
        $props['resultat_prelevement_urinaire'] = 'enum list|negatif|streptob|autre';
        $props['rques_prelevement_urinaire']    = 'str';
        $props['ag_prelevement_urinaire']       = 'num min|0 max|255';

        $props['marqueurs_seriques']           = 'enum list|nonprescrits|refuses|faits1trim|faits2trim';
        $props['resultats_marqueurs_seriques'] = 'enum list|bas|t21|anfermtn|autre';
        $props['rques_marqueurs_seriques']     = 'str';

        $props['depistage_diabete']          = 'enum list|nonprescrit|refuse|fait';
        $props['resultat_depistage_diabete'] = 'enum list|normal|anormal';

        return $props;
    }
}
