<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es � la synth�se du s�jour d'accouchement
 */
class SyntheseSejour extends AbstractComponent
{
    public const RESOURCE_TYPE = 'syntheseSejour';

    /** @var ?int Primary key */
    public ?int $synthese_sejour_id = null;

    public ?string $pathologies_suite_couches                    = null;
    public ?string $infection_suite_couches                      = null;
    public ?string $infection_nosoc_suite_couches                = null;
    public ?string $localisation_infection_suite_couches         = null;
    public ?string $compl_perineales_suite_couches               = null;
    public ?string $details_compl_perineales_suite_couches       = null;
    public ?string $compl_parietales_suite_couches               = null;
    public ?string $detail_compl_parietales_suite_couches        = null;
    public ?string $compl_allaitement_suite_couches              = null;
    public ?string $details_compl_allaitement_suite_couches      = null;
    public ?string $details_comp_compl_allaitement_suite_couches = null;
    public ?string $compl_thrombo_embo_suite_couches             = null;
    public ?string $detail_compl_thrombo_embo_suite_couches      = null;
    public ?string $compl_autre_suite_couches                    = null;
    public ?string $anemie_suite_couches                         = null;
    public ?string $incont_urin_suite_couches                    = null;
    public ?string $depression_suite_couches                     = null;
    public ?string $fract_obst_coccyx_suite_couches              = null;
    public ?string $hemorragie_second_suite_couches              = null;
    public ?string $retention_urinaire_suite_couches             = null;
    public ?string $psychose_puerpuerale_suite_couches           = null;
    public ?string $eclampsie_suite_couches                      = null;
    public ?string $insuf_reinale_suite_couches                  = null;
    public ?string $disjonction_symph_pub_suite_couches          = null;
    public ?string $autre_comp_suite_couches                     = null;
    public ?string $desc_autre_comp_suite_couches                = null;
    public ?string $compl_anesth_suite_couches                   = null;
    public ?string $compl_anesth_generale_suite_couches          = null;
    public ?string $autre_compl_anesth_generale_suite_couches    = null;
    public ?string $compl_anesth_locoregion_suite_couches        = null;
    public ?string $autre_compl_anesth_locoregion_suite_couches  = null;
    public ?string $traitements_sejour_mere                      = null;
    public ?string $ttt_preventif_sejour_mere                    = null;
    public ?string $antibio_preventif_sejour_mere                = null;
    public ?string $desc_antibio_preventif_sejour_mere           = null;
    public ?string $anticoag_preventif_sejour_mere               = null;
    public ?string $desc_anticoag_preventif_sejour_mere          = null;
    public ?string $antilactation_preventif_sejour_mere          = null;
    public ?string $ttt_curatif_sejour_mere                      = null;
    public ?string $antibio_curatif_sejour_mere                  = null;
    public ?string $desc_antibio_curatif_sejour_mere             = null;
    public ?string $anticoag_curatif_sejour_mere                 = null;
    public ?string $desc_anticoag_curatif_sejour_mere            = null;
    public ?string $vacc_gammaglob_sejour_mere                   = null;
    public ?string $gammaglob_sejour_mere                        = null;
    public ?string $vacc_sejour_mere                             = null;
    public ?string $transfusion_sejour_mere                      = null;
    public ?string $nb_unite_transfusion_sejour_mere             = null;
    public ?string $interv_sejour_mere                           = null;
    public ?string $datetime_interv_sejour_mere                  = null;
    public ?string $revision_uterine_sejour_mere                 = null;
    public ?string $interv_second_hemorr_sejour_mere             = null;
    public ?string $type_interv_second_hemorr_sejour_mere        = null;
    public ?string $autre_interv_sejour_mere                     = null;
    public ?string $type_autre_interv_sejour_mere                = null;
    public ?string $jour_deces_sejour_mere                       = null;
    public ?string $deces_cause_obst_sejour_mere                 = null;
    public ?string $autopsie_sejour_mere                         = null;
    public ?string $resultat_autopsie_sejour_mere                = null;
    public ?string $anomalie_autopsie_sejour_mere                = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_syntheses_sejours';
        $spec->key   = 'synthese_sejour_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['pathologies_suite_couches']                    = 'bool';
        $props['infection_suite_couches']                      = 'bool';
        $props['infection_nosoc_suite_couches']                = 'bool';
        $props['localisation_infection_suite_couches']         = 'enum list|abcessein|lympsein|vagin|infur|endopuer|infperin|perit|septi|fievre|infpari|inc';
        $props['compl_perineales_suite_couches']               = 'bool';
        $props['details_compl_perineales_suite_couches']       = 'enum list|hematome|suture|abces';
        $props['compl_parietales_suite_couches']               = 'bool';
        $props['detail_compl_parietales_suite_couches']        = 'enum list|hematome|suture|abces';
        $props['compl_allaitement_suite_couches']              = 'bool';
        $props['details_compl_allaitement_suite_couches']      = 'enum list|crev|allait|autres';
        $props['details_comp_compl_allaitement_suite_couches'] = 'str';
        $props['compl_thrombo_embo_suite_couches']             = 'bool';
        $props['detail_compl_thrombo_embo_suite_couches']      = 'enum list|thrombophlebsup|phleb|thrombophlebpelv|embpulm|thrombveicereb|hemorr';
        $props['compl_autre_suite_couches']                    = 'bool';
        $props['anemie_suite_couches']                         = 'bool';
        $props['incont_urin_suite_couches']                    = 'bool';
        $props['depression_suite_couches']                     = 'bool';
        $props['fract_obst_coccyx_suite_couches']              = 'bool';
        $props['hemorragie_second_suite_couches']              = 'bool';
        $props['retention_urinaire_suite_couches']             = 'bool';
        $props['psychose_puerpuerale_suite_couches']           = 'bool';
        $props['eclampsie_suite_couches']                      = 'bool';
        $props['insuf_reinale_suite_couches']                  = 'bool';
        $props['disjonction_symph_pub_suite_couches']          = 'bool';
        $props['autre_comp_suite_couches']                     = 'bool';
        $props['desc_autre_comp_suite_couches']                = 'str';
        $props['compl_anesth_suite_couches']                   = 'bool';
        $props['compl_anesth_generale_suite_couches']          = 'enum list|mend|pulm|card|cereb|allerg|autre';
        $props['autre_compl_anesth_generale_suite_couches']    = 'str';
        $props['compl_anesth_locoregion_suite_couches']        = 'enum list|ceph|hypotens|autre';
        $props['autre_compl_anesth_locoregion_suite_couches']  = 'str';
        $props['traitements_sejour_mere']                      = 'bool';
        $props['ttt_preventif_sejour_mere']                    = 'bool';
        $props['antibio_preventif_sejour_mere']                = 'bool';
        $props['desc_antibio_preventif_sejour_mere']           = 'str';
        $props['anticoag_preventif_sejour_mere']               = 'bool';
        $props['desc_anticoag_preventif_sejour_mere']          = 'str';
        $props['antilactation_preventif_sejour_mere']          = 'bool';
        $props['ttt_curatif_sejour_mere']                      = 'bool';
        $props['antibio_curatif_sejour_mere']                  = 'bool';
        $props['desc_antibio_curatif_sejour_mere']             = 'str';
        $props['anticoag_curatif_sejour_mere']                 = 'bool';
        $props['desc_anticoag_curatif_sejour_mere']            = 'str';
        $props['vacc_gammaglob_sejour_mere']                   = 'bool';
        $props['gammaglob_sejour_mere']                        = 'bool';
        $props['vacc_sejour_mere']                             = 'bool';
        $props['transfusion_sejour_mere']                      = 'bool';
        $props['nb_unite_transfusion_sejour_mere']             = 'num min|0 max|9999';
        $props['interv_sejour_mere']                           = 'bool';
        $props['datetime_interv_sejour_mere']                  = 'dateTime';
        $props['revision_uterine_sejour_mere']                 = 'bool';
        $props['interv_second_hemorr_sejour_mere']             = 'bool';
        $props['type_interv_second_hemorr_sejour_mere']        = 'enum list|emboart|ligarthypogast|ligartuter|veinecave|hysterect';
        $props['autre_interv_sejour_mere']                     = 'bool';
        $props['type_autre_interv_sejour_mere']                = 'enum list|repriseparoi|repriseperinee|evacpariet|laparo|thrombhemorr|steril';
        $props['jour_deces_sejour_mere']                       = 'num min|0 max|9999';
        $props['deces_cause_obst_sejour_mere']                 = 'bool';
        $props['autopsie_sejour_mere']                         = 'enum list|nondem|ref|faite';
        $props['resultat_autopsie_sejour_mere']                = 'enum list|sansanom|anom';
        $props['anomalie_autopsie_sejour_mere']                = 'str';

        return $props;
    }
}
