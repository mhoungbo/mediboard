<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal concernant la surveillance du travail lors de l'accouchement
 */
class AccouchementSurveillanceTravail extends AbstractComponent
{
    public const RESOURCE_TYPE = 'accouchementSurveillanceTravail';

    /** @var ?int Primary key */
    public ?int $accouchement_surveillance_travail_id = null;

    public ?string $surveillance_travail = null;
    public ?string $tocographie = null;
    public ?string $type_tocographie = null;
    public ?string $anomalie_contractions = null;
    public ?string $rcf = null;
    public ?string $type_rcf = null;
    public ?string $desc_trace_rcf = null;
    public ?string $anomalie_rcf = null;
    public ?string $ecg_foetal = null;
    public ?string $anomalie_ecg_foetal = null;
    public ?string $prelevement_sang_foetal = null;
    public ?string $anomalie_ph_sang_foetal = null;
    public ?string $detail_anomalie_ph_sang_foetal = null;
    public ?string $valeur_anomalie_ph_sang_foetal = null;
    public ?string $anomalie_lactates_sang_foetal = null;
    public ?string $detail_anomalie_lactates_sang_foetal = null;
    public ?string $valeur_anomalie_lactates_sang_foetal = null;
    public ?string $oxymetrie_foetale = null;
    public ?string $anomalie_oxymetrie_foetale = null;
    public ?string $detail_anomalie_oxymetrie_foetale = null;
    public ?string $autre_examen_surveillance = null;
    public ?string $desc_autre_examen_surveillance = null;
    public ?string $anomalie_autre_examen_surveillance = null;
    public ?string $rques_surveillance_travail = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_surveillances_travail';
        $spec->key   = 'accouchement_surveillance_travail_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['surveillance_travail']                 = 'enum list|cli|paracli';
        $props['tocographie']                          = 'bool';
        $props['type_tocographie']                     = 'enum list|ext|int';
        $props['anomalie_contractions']                = 'enum list|aucune|hypoci|hyperci|hypoto|hyperto';
        $props['rcf']                                  = 'bool';
        $props['type_rcf']                             = 'enum list|ext|int';
        $props['desc_trace_rcf']                       = 'enum list|norm|suspect|patho';
        $props['anomalie_rcf']                         = 'enum list|bradyc|tachyc|plat|ralprec|raltard|ralvar';
        $props['ecg_foetal']                           = 'bool';
        $props['anomalie_ecg_foetal']                  = 'bool';
        $props['prelevement_sang_foetal']              = 'bool';
        $props['anomalie_ph_sang_foetal']              = 'bool';
        $props['detail_anomalie_ph_sang_foetal']       = 'str';
        $props['valeur_anomalie_ph_sang_foetal']       = 'float';
        $props['anomalie_lactates_sang_foetal']        = 'bool';
        $props['detail_anomalie_lactates_sang_foetal'] = 'str';
        $props['valeur_anomalie_lactates_sang_foetal'] = 'float';
        $props['oxymetrie_foetale']                    = 'bool';
        $props['anomalie_oxymetrie_foetale']           = 'bool';
        $props['detail_anomalie_oxymetrie_foetale']    = 'str';
        $props['autre_examen_surveillance']            = 'bool';
        $props['desc_autre_examen_surveillance']       = 'str';
        $props['anomalie_autre_examen_surveillance']   = 'bool';
        $props['rques_surveillance_travail']           = 'text helped';

        return $props;
    }
}
