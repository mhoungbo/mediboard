<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 * Contient les donn�es du dossier p�rinatal li�es au transfert ant�natal
 */
class TransfertAntenatal extends AbstractComponent
{
    public const RESOURCE_TYPE = 'transfertAntenatal';

    /** @var ?int Primary key */
    public ?int $transfert_antenatal_id = null;

    public ?string $transf_antenat = null;
    public ?string $date_transf_antenat = null;
    public ?string $lieu_transf_antenat = null;
    public ?string $etab_transf_antenat = null;
    public ?string $nivsoins_transf_antenat = null;
    public ?string $raison_transf_antenat_hors_reseau = null;
    public ?string $raison_imp_transf_antenat = null;
    public ?string $motif_tranf_antenat = null;
    public ?string $type_patho_transf_antenat = null;
    public ?string $rques_transf_antenat = null;

    public ?string $mode_transp_transf_antenat = null;
    public ?string $antibio_transf_antenat = null;
    public ?string $nom_antibio_transf_antenat = null;
    public ?string $cortico_transf_antenat = null;
    public ?string $nom_cortico_transf_antenat = null;
    public ?string $datetime_cortico_transf_antenat = null;
    public ?string $tocolytiques_transf_antenat = null;
    public ?string $nom_tocolytiques_transf_antenat = null;
    public ?string $antihta_transf_antenat = null;
    public ?string $nom_antihta_transf_antenat = null;
    public ?string $autre_ttt_transf_antenat = null;
    public ?string $nom_autre_ttt_transf_antenat = null;

    public ?string $retour_mater_transf_antenat = null;
    public ?string $date_retour_transf_antenat = null;
    public ?string $devenir_retour_transf_antenat = null;
    public ?string $rques_retour_transf_antenat = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_transferts_antenatals';
        $spec->key   = 'transfert_antenatal_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['transf_antenat']                    = 'enum list|n|reseau|hreseau|imp';
        $props['date_transf_antenat']               = 'date';
        $props['lieu_transf_antenat']               = 'enum list|amater|rea|autre';
        $props['etab_transf_antenat']               = 'str';
        $props['nivsoins_transf_antenat']           = 'enum list|1|2|3';
        $props['raison_transf_antenat_hors_reseau'] = 'enum list|place|choixmed|choixpat';
        $props['raison_imp_transf_antenat']         = 'enum list|place|patho|dilat|refuspat';
        $props['motif_tranf_antenat']               = 'enum list|pathfoet|pathmat';
        $props['type_patho_transf_antenat']         = 'enum list|map|rpm|vasc|mult|rciusf|malf|autre';
        $props['rques_transf_antenat']              = 'text helped';

        $props['mode_transp_transf_antenat']      = 'enum list|perso|samu|ambu|autre';
        $props['antibio_transf_antenat']          = 'bool';
        $props['nom_antibio_transf_antenat']      = 'str';
        $props['cortico_transf_antenat']          = 'bool';
        $props['nom_cortico_transf_antenat']      = 'str';
        $props['datetime_cortico_transf_antenat'] = 'dateTime';
        $props['tocolytiques_transf_antenat']     = 'bool';
        $props['nom_tocolytiques_transf_antenat'] = 'str';
        $props['antihta_transf_antenat']          = 'bool';
        $props['nom_antihta_transf_antenat']      = 'str';
        $props['autre_ttt_transf_antenat']        = 'bool';
        $props['nom_autre_ttt_transf_antenat']    = 'str';

        $props['retour_mater_transf_antenat']   = 'enum list|n|consult|hospi|acc|postacc';
        $props['date_retour_transf_antenat']    = 'date';
        $props['devenir_retour_transf_antenat'] = 'enum list|acc|transf';
        $props['rques_retour_transf_antenat']   = 'text helped';

        return $props;
    }
}
