<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal li�es � la synth�se de la grossesse - partie contexte psycho-social
 */
class SyntheseContextePsychoSocial extends AbstractComponent
{
    public const RESOURCE_TYPE = 'syntheseContextePsychoSocial';

    /** @var ?int Primary key */
    public ?int $synthese_contexte_psycho_social_id = null;

    public ?string $profession_pdt_grossesse = null;
    public ?string $ag_date_arret_travail = null;
    public ?string $situation_pb_pdt_grossesse = null;
    public ?string $separation_pdt_grossesse = null;
    public ?string $deces_fam_pdt_grossesse = null;
    public ?string $autre_evenement_fam_pdt_grossesse = null;
    public ?string $perte_emploi_pdt_grossesse = null;
    public ?string $autre_evenement_soc_pdt_grossesse = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_synthese_contextes_psycho_sociaux';
        $spec->key   = 'synthese_contexte_psycho_social_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['profession_pdt_grossesse']          = 'str autocomplete';
        $props['ag_date_arret_travail']             = 'num min|0 max|255';
        $props['situation_pb_pdt_grossesse']        = 'bool';
        $props['separation_pdt_grossesse']          = 'bool';
        $props['deces_fam_pdt_grossesse']           = 'bool';
        $props['autre_evenement_fam_pdt_grossesse'] = 'str';
        $props['perte_emploi_pdt_grossesse']        = 'bool';
        $props['autre_evenement_soc_pdt_grossesse'] = 'str';

        return $props;
    }
}
