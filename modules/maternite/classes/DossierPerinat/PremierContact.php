<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Maternite\DossierPerinat;

use Ox\Core\CMbObjectSpec;

/**
 *  Contient les donn�es du dossier p�rinatal concernant le premier contact avec la parturiente
 */
class PremierContact extends AbstractComponent
{
    public const RESOURCE_TYPE = 'premierContact';

    /** @var ?int Primary key */
    public ?int $premier_contact_id = null;

    public ?string $date_premier_contact = null;
    public ?string $provenance_premier_contact = null;
    public ?string $nivsoins_provenance_premier_contact = null;
    public ?string $motif_premier_contact = null;
    public ?string $nb_consult_ant_premier_contact = null;
    public ?string $sa_consult_ant_premier_contact = null;
    public ?string $surveillance_ant_premier_contact = null;
    public ?string $type_surv_ant_premier_contact = null;
    public ?string $date_declaration_grossesse = null;
    public ?string $rques_provenance = null;

    public ?string $reco_aucune = null;
    public ?string $reco_tabac = null;
    public ?string $reco_rhesus_negatif = null;
    public ?string $reco_toxoplasmose = null;
    public ?string $reco_alcool = null;
    public ?string $reco_vaccination = null;
    public ?string $reco_hygiene_alim = null;
    public ?string $reco_toxicomanie = null;
    public ?string $reco_brochure = null;
    public ?string $reco_autre = null;

    public ?string $souhait_arret_addiction = null;
    public ?string $souhait_aide_addiction = null;

    public ?string $info_echographie = null;
    public ?string $info_despistage_triso21 = null;
    public ?string $test_triso21_propose = null;
    public ?string $info_orga_maternite = null;
    public ?string $info_orga_reseau = null;
    public ?string $info_lien_pmi = null;

    public ?string $projet_lieu_accouchement = null;
    public ?string $projet_analgesie_peridurale = null;
    public ?string $projet_allaitement_maternel = null;
    public ?string $projet_preparation_naissance = null;
    public ?string $projet_entretiens_proposes = null;

    public ?string $bas_risques = null;
    public ?string $risque_atcd_maternel_med = null;
    public ?string $risque_atcd_obst = null;
    public ?string $risque_atcd_familiaux = null;
    public ?string $risque_patho_mater_grossesse = null;
    public ?string $risque_patho_foetale_grossesse = null;
    public ?string $risque_psychosocial_grossesse = null;
    public ?string $risque_grossesse_multiple = null;

    public ?string $type_surveillance = null;
    public ?string $lieu_surveillance = null;
    public ?string $lieu_accouchement_prevu = null;
    public ?string $niveau_soins_prevu = null;
    public ?string $conclusion_premier_contact = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'dossier_perinat_premiers_contacts';
        $spec->key   = 'premier_contact_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['dossier_perinat_id'] .= ' back|' . self::RESOURCE_TYPE;

        $props['date_premier_contact']                = 'dateTime';
        $props['provenance_premier_contact']          = 'enum list|pat|med|tra|aut';
        $props['nivsoins_provenance_premier_contact'] = 'enum list|1|2|3';
        $props['motif_premier_contact']               = 'enum list|survrout|survspec|consulturg|hospi|acc|autre';
        $props['nb_consult_ant_premier_contact']      = 'num min|0 max|255';
        $props['sa_consult_ant_premier_contact']      = 'num min|0 max|255';
        $props['surveillance_ant_premier_contact']    = 'str';
        $props['type_surv_ant_premier_contact']       = 'enum list|generaliste|gynecomater|gynecoautre|pmi|sagefemme|autre';
        $props['date_declaration_grossesse']          = 'date';
        $props['rques_provenance']                    = 'text helped';

        $props['reco_aucune']         = 'bool';
        $props['reco_tabac']          = 'bool';
        $props['reco_rhesus_negatif'] = 'bool';
        $props['reco_toxoplasmose']   = 'bool';
        $props['reco_alcool']         = 'bool';
        $props['reco_vaccination']    = 'bool';
        $props['reco_hygiene_alim']   = 'bool';
        $props['reco_toxicomanie']    = 'bool';
        $props['reco_brochure']       = 'bool';
        $props['reco_autre']          = 'str';

        $props['souhait_arret_addiction'] = 'bool';
        $props['souhait_aide_addiction']  = 'bool';

        $props['info_echographie']        = 'bool';
        $props['info_despistage_triso21'] = 'bool';
        $props['test_triso21_propose']    = 'enum list|n|a|r';
        $props['info_orga_maternite']     = 'bool';
        $props['info_orga_reseau']        = 'bool';
        $props['info_lien_pmi']           = 'bool';

        $props['projet_lieu_accouchement']     = 'str';
        $props['projet_analgesie_peridurale']  = 'enum list|o|n|s';
        $props['projet_allaitement_maternel']  = 'enum list|o|n|s';
        $props['projet_preparation_naissance'] = 'str';
        $props['projet_entretiens_proposes']   = 'bool';

        $props['bas_risques']                    = 'bool';
        $props['risque_atcd_maternel_med']       = 'bool';
        $props['risque_atcd_obst']               = 'bool';
        $props['risque_atcd_familiaux']          = 'bool';
        $props['risque_patho_mater_grossesse']   = 'bool';
        $props['risque_patho_foetale_grossesse'] = 'bool';
        $props['risque_psychosocial_grossesse']  = 'bool';
        $props['risque_grossesse_multiple']      = 'bool';

        $props['type_surveillance']          = 'enum list|routine|spec|antenat|autre';
        $props['lieu_surveillance']          = 'enum list|mater|amater|ville|sfdom|had';
        $props['lieu_accouchement_prevu']    = 'str';
        $props['niveau_soins_prevu']         = 'enum list|1|2|3';
        $props['conclusion_premier_contact'] = 'text helped';

        return $props;
    }
}
