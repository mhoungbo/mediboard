<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Import\Framework\Entity\CImportCampaign;
use Ox\Import\Framework\Entity\CImportEntity;
use Symfony\Component\HttpFoundation\Response;

class ImportedObjectsController extends Controller
{
    public const ERROR_TYPE = ['all', 'valid', 'with_errors', 'error'];

    /**
     * @throws Exception
     */
    public function index(RequestParams $params): Response
    {
        // Check read on campaign first
        $campaign = CImportCampaign::findOrNew(
            $params->get('campaign_id', 'ref class|CImportCampaign', PERM_READ)
        );

        return $this->renderSmarty('vw_campaign_objects.tpl', [
            'show_errors'  => $params->get(
                'show_errors',
                'enum list|' . implode('|', self::ERROR_TYPE) . ' default|valid'
            ),
            'campaign'     => $campaign,
            'all_campaign' => $campaign->loadList(),
        ]);
    }

    /**
     * @throws Exception
     */
    public function list(RequestParams $params): Response
    {
        $show_errors = $params->get(
            'show_errors',
            'enum list|' . implode('|', self::ERROR_TYPE) . ' default|valid'
        );

        $campaign = CImportCampaign::findOrFail(
            $params->get('campaign_id', 'ref class|CImportCampaign notNull')
        );

        return $this->renderSmarty('inc_vw_campaign_objects.tpl', [
            'campaign'    => $campaign,
            'classes'     => $campaign->getImportedEntities($show_errors, true),
            'show_errors' => $show_errors,
        ]);
    }

    /**
     * @throws Exception
     */
    public function listImported(RequestParams $params): Response
    {
        $show_errors = $params->get(
            'show_errors',
            'enum list|' . implode('|', self::ERROR_TYPE) . ' default|valid'
        );

        $campaign = CImportCampaign::findOrFail(
            $params->get('import_campaign_id', 'ref class|CImportCampaign notNull', PERM_READ)
        );

        $class_name = $params->get('class_name', 'str notNull');

        return $this->renderSmarty('inc_list_campaign_objects.tpl', [
            'class_name'  => $class_name,
            'total'       => $campaign->countEntityByClass($class_name, $show_errors),
            'start'       => $start = $params->get('start', 'num default|0'),
            'step'        => $step = $params->get('step', 'num default|20'),
            'entities'    => $campaign->loadEntityByClass($class_name, $show_errors, $start, $step),
            'campaign'    => $campaign,
            'show_errors' => $show_errors,
        ]);
    }

    /**
     * @throws Exception
     */
    public function reset(CImportCampaign $campaign, RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('import_gui_objects_reset', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $entity = new CImportEntity();
        $ds     = $entity->getDS();

        $ids_to_delete = $entity->loadIds(
            [
                'import_campaign_id' => $ds->prepare('= ?', $campaign->_id),
                'external_class'     => $ds->prepare('= ?', $params->post('type', 'str notNull')),
            ]
        );

        if ($msg = $entity->deleteAll($ids_to_delete)) {
            $this->addUiMsgError($msg, true);

            return $this->renderEmptyResponse();
        }

        $this->addUiMsgOk('CImportEntity-Msg-delete|pl', true);

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    public function export(CImportCampaign $campaign, RequestParams $params): Response
    {
        $line = [
            $this->translator->tr("CImportEntity-external_class-court"),
            $this->translator->tr("CImportEntity-external_id-court"),
            $this->translator->tr("CImportEntity-internal_id-court"),
            $this->translator->tr("CImportEntity-last_import_date-court"),
            $this->translator->tr("CImportEntity-last_error-court"),
        ];
        $csv  = new CCSVFile(null, CCSVFile::PROFILE_EXCEL);
        $csv->writeLine($line);
        $csv->setColumnNames($line);

        $entities = $campaign->loadEntityByClass(
            $params->get('class_name', 'str notNull'),
            $params->get(
                'show_errors',
                'enum list|' . implode('|', self::ERROR_TYPE) . ' default|valid'
            ),
            $params->get('start', 'num default|0'),
            $params->get('step', 'num default|50')
        );

        /** @var CImportEntity $entity */
        foreach ($entities as $entity) {
            $csv->writeLine(
                [
                    $entity->external_class,
                    $entity->external_id,
                    $entity->internal_id,
                    $entity->last_import_date,
                    $entity->last_error,
                ]
            );
        }

        return $this->renderFileResponse($csv->getContent(), 'export-imported_entities.csv', 'application/csv');
    }
}
