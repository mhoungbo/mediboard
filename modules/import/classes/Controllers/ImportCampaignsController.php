<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Import\Framework\Entity\CImportCampaign;
use Symfony\Component\HttpFoundation\Response;

class ImportCampaignsController extends Controller
{
    /**
     * @throws Exception
     */
    public function index(): Response
    {
        return $this->renderSmarty('vw_import_campaigns.tpl');
    }

    /**
     * @throws Exception
     */
    public function list(RequestParams $params): Response
    {
        $campaign = new CImportCampaign();
        $ds       = $campaign->getDS();

        $where = [];

        if ($name = $params->get('name', 'str')) {
            $where['name'] = $ds->prepareLike("%{$name}%");
        }

        if ($min_creation_date = $params->get('_creation_date_min', 'dateTime')) {
            $where[] = $ds->prepare('`creation_date` >= ?', $min_creation_date);
        }

        if ($max_creation_date = $params->get('_creation_date_max', 'dateTime')) {
            $where[] = $ds->prepare('`creation_date` <= ?', $max_creation_date);
        }

        if ($min_closing_date = $params->get('_closing_date_min', 'dateTime')) {
            $where[] = $ds->prepare('`closing_date` >= ?', $min_closing_date);
        }

        if ($max_closing_date = $params->get('_closing_date_max', 'dateTime')) {
            $where[] = $ds->prepare('`closing_date` <= ?', $max_closing_date);
        }

        return $this->renderSmarty('inc_list_import_campaigns.tpl', [
            'campaigns' => $campaign->loadList($where, 'creation_date ASC'),
        ]);
    }

    /**
     * @throws Exception
     */
    public function viewEdit(RequestParams $params): Response
    {
        return $this->renderSmarty('vw_edit_import_campaign.tpl', [
            'campaign' => CImportCampaign::findOrNew(
                $params->get('import_campaign_id', 'ref class|CImportCampaign', PERM_EDIT)
            ),
        ]);
    }

    /**
     * @throws Exception
     */
    public function doEdit(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('import_gui_campaigns_do_edit', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $campaign = CImportCampaign::findOrNew(
            $params->post('import_campaign_id', 'ref class|CImportCampaign', PERM_EDIT)
        );

        if ($params->post('del', 'bool default|0')) {
            return $this->deleteObjectAndReturnEmptyResponse($campaign);
        }

        $campaign->name = $params->post('name', 'str');

        if ($campaign->_id) {
            $campaign->creation_date = $params->post('creation_date', 'str' . ($campaign->_id ? '' : ' notNull'));
            $campaign->closing_date  = $params->post('closing_date', 'str');
        }

        return $this->storeObjectAndReturnEmptyResponse($campaign, !$campaign->_id);
    }
}
