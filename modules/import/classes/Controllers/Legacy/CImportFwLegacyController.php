<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Controllers\Legacy;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Logger\LoggerLevels;
use Ox\Import\Framework\CFwImport;
use Ox\Import\Framework\Entity\CImportCampaign;
use Ox\Import\Framework\Entity\Manager;
use Ox\Import\Framework\Entity\User;
use Ox\Import\Framework\Exception\ImportException;
use Ox\Import\GenericImport\AbstractOxPivotImportableObject;
use Ox\Import\GenericImport\OxImportPivot;
use Ox\Mediboard\Mediusers\CMediusers;
use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

abstract class CImportFwLegacyController extends CLegacyController
{
    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function do_bind_user(): void
    {
        $this->checkPermEdit();
        $campaign_id = CView::post('campaign_id', 'ref class|CImportCampaign notNull');
        $ext_id      = CView::post('ext_id', 'str notNull');
        $user_id     = CView::postRefCheckRead('user_id', 'ref class|CMediusers notNull');
        CView::checkin();

        $import_campaign = CImportCampaign::findOrFail($campaign_id);
        if (!$import_campaign->getPerm(PERM_READ) || $import_campaign->closing_date) {
            CAppUI::accessDenied();
        }

        $mediuser = CMediusers::findOrFail($user_id);

        $user = new User();
        $user->setExternalId($ext_id);

        try {
            $import_campaign->addImportedObject($user, $mediuser);
        } catch (ImportException $e) {
            CAppUI::stepAjax($e->getMessage());
            CApp::rip();
        }

        CAppUI::stepAjax('CImportEntity-msg-create');
        CApp::rip();
    }

    /**
     * @throws Exception
     */
    public function vw_users_fw(): void
    {
        $this->checkPermEdit();
        $last_import_campaign_id = CView::get('import_campaign_id', 'ref class|CImportCampaign', true);
        $import_type             = CView::get('import_type', 'str');
        CView::checkin();

        $this->renderSmarty(
            'vw_users.tpl',
            [
                'last_campaign_id' => $last_import_campaign_id,
                'campaigns'        => CImportCampaign::getCampaignsInProgress(),
                'module'           => $this->getModName(),
                'a_value'          => 'ajax_list_users_fw',
                'import_type'      => $import_type,
            ],
            'modules/import'
        );
    }

    abstract protected function getModName(): string;

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function ajax_list_users_fw(): void
    {
        $this->checkPermEdit();

        $campaign_id = CView::get('import_campaign_id', 'ref class|CImportCampaign notNull', true);
        $start       = CView::get('start', 'num default|0');
        $step        = CView::get('step', 'num default|50');
        $import_type = CView::get('import_type', 'str');

        $campaign = CImportCampaign::findOrFail($campaign_id);
        if (!$campaign->getPerm(PERM_EDIT) || $campaign->closing_date) {
            CAppUI::accessDenied();
        }

        CView::setSession('import_campaign_id', $campaign_id);

        CView::checkin();

        $import = $this->getImportInstance($import_type);
        $import->setCampaign($campaign);

        $this->renderSmarty(
            'inc_list_users.tpl',
            [
                'campaign'        => $campaign,
                'user_list'       => $import->listUsers($campaign, $start, $step),
                'total'           => $this->countUsers($import),
                'start'           => $start,
                'mod_name'        => $this->getModName(),
                'change_page_arg' => [
                    'm'                  => $this->getModName(),
                    'a'                  => 'ajax_list_users_fw',
                    'import_campaign_id' => $campaign->_id,
                    'refresh'            => 'result-list-users',
                    'import_type'        => $import_type,
                ],
            ],
            'modules/import'
        );
    }

    abstract protected function getImportInstance(?string $type = null): CFwImport;

    /**
     * @throws Exception
     */
    protected function countUsers(CFwImport $import): int
    {
        return $import->count($this->getUsersTable());
    }

    protected function getUsersTable(): string
    {
        return 'users';
    }

    /**
     * @throws Exception
     */
    public function vw_import_fw(): void
    {
        $this->checkPermEdit();
        $last_campaign_import_id = CView::get('import_campaign_id', 'ref class|CImportCampaign', true);
        $import_type             = CView::get('import_type', 'str');
        CView::checkin();

        $campaigns = CImportCampaign::getCampaignsInProgress();
        $import    = $this->getImportInstance($import_type);

        if ($last_campaign_import_id) {
            $campaign = CImportCampaign::find($last_campaign_import_id);
        } else {
            $campaign                = CImportCampaign::getLastCampaign();
            $last_campaign_import_id = $campaign->_id;
        }

        $import->setCampaign($campaign);
        $import_order = $import->getImportOrder();

        $this->renderSmarty(
            'vw_import.tpl',
            [
                'module'           => $this->getModName(),
                'a_value'          => 'ajax_vw_import_fw',
                'last_campaign_id' => $last_campaign_import_id,
                'campaigns'        => $campaigns,
                'import_order'     => $import_order,
                'import_type'      => $import_type,
            ],
            'modules/import'
        );
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public function ajax_vw_import_fw(): void
    {
        $this->checkPermEdit();

        $type        = CView::get('type', 'str notNull');
        $campaign_id = CView::get('import_campaign_id', 'ref class|CImportCampaign notNull', true);
        $import_type = CView::get('import_type', 'str');

        $campaign = CImportCampaign::findOrFail($campaign_id);
        if (!$campaign->getPerm(PERM_EDIT) || $campaign->closing_date) {
            CAppUI::accessDenied();
        }

        CView::setSession('import_campaign_id', $campaign_id);

        CView::checkin();

        $import = $this->getImportInstance($import_type);
        $import->setCampaign($campaign);
        $total = 0;

        try {
            $total = $this->count($import, $type);
        } catch (Throwable $e) {
            CApp::log($e->getMessage(), null, LoggerLevels::LEVEL_ERROR);

            CAppUI::stepAjax(
                'CImportFwLegacyController-Error-Mapper does not exist or cannot be instanciated for type',
                UI_MSG_ERROR,
                $type
            );
        }

        $fields = [];
        if (isset(OxImportPivot::MAPPING_PIVOT[$type])) {
            /** @var AbstractOxPivotImportableObject $mapping_pivot */
            $mapping_pivot = OxImportPivot::MAPPING_PIVOT[$type];
            $fields        = (new $mapping_pivot())->getFields();

            // Unset the field 0 because it's ID and we don't want to update the ID of an object
            unset($fields[0]);
        }

        $this->renderSmarty(
            'inc_vw_import.tpl',
            [
                'type'        => $type,
                'total'       => $total,
                'last_id'     => ($import->getLastId($type)) ?: 0,
                'module'      => $this->getModName(),
                'by_patient'  => $this->isImportByPatient($type),
                'import_type' => $import_type,
                'fields'      => $fields,
            ],
            'modules/import'
        );
    }

    /**
     * @throws Exception
     */
    protected function count(CFwImport $import, string $type, ?string $patient_id = null): int
    {
        return $import->count($type, $patient_id);
    }

    protected function isImportBypatient(string $type): bool
    {
        return true;
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function do_import_fw(): void
    {
        $this->checkPermEdit();

        $campaign_id = CView::post('import_campaign_id', 'ref class|CImportCampaign notNull');
        $type        = CView::post('type', 'str');
        $start       = CView::post('start', 'num default|0');
        $step        = CView::post('step', 'num default|100');
        $patient_id  = CView::post('patient_id', 'str');
        $update      = CView::post('update', 'bool default|0');
        $import_type = CView::post('import_type', 'str');

        // Retrieve selected fields to update only if in update mode
        $selected_fields = [];
        if ($update) {
            $selected_fields = explode("|", CView::post('chosen_fields', 'str'));
        }

        $campaign = CImportCampaign::findOrFail($campaign_id);
        if (!$campaign->getPerm(PERM_EDIT) || $campaign->closing_date) {
            CAppUI::accessDenied();
        }

        CView::setSession('import_campaign_id', $campaign_id);

        CView::checkin();

        $import = $this->getImportInstance($import_type);
        $import->setCampaign($campaign);

        if ($patient_id) {
            $patient_id = str_replace(' ', '', $patient_id);
            $patient_id = explode(',', $patient_id);

            foreach ($patient_id as $id) {
                $manager = $import->import($campaign, $type, $start, $step, $id, $update, $selected_fields);
            }
        } else {
            $manager = $import->import($campaign, $type, $start, $step, null, $update, $selected_fields);
        }

        if (!($manager instanceof Manager)) {
            CAppUI::stepAjax($manager, UI_MSG_ERROR);
        }

        $display_infos = false;
        foreach ($manager->getMessages() as $_msg) {
            if (CMbArray::countValues("AbstractStrategy-Msg-%s successfully imported", $_msg) > 0) {
                $display_infos = true;
            }
            CAppUI::setMsg(...$_msg);
        }

        foreach ($manager->getErrors() as $_error) {
            CAppUI::setMsg($_error, UI_MSG_WARNING);
        }

        if ($ended = ($import->getImportCount() < $step)) {
            CAppUI::setMsg('common-msg-Importation done.');
        }

        // With a step of 1, display object fields infos defined with getDisplayedInfo()
        if (($step === 1) && !$ended && $display_infos) {
            $info = $this->getLastImportedObjectDisplayedInfo($campaign);
            if (count($info) > 0) {
                CAppUI::setMsg('CImportFwLegacyController-msg-Information on last imported entity');

                foreach ($info as $key => $value) {
                    CAppUI::setMsg("<strong>" . strtoupper($key) . "</strong>" . " : " . $value);
                }
            }
        }

        echo CAppUI::getMsg();

        $start = $start + $step;
        CAppUI::js("ImportMapping.nextImport('{$type}', '{$start}', '{$ended}');");

        CApp::rip();
    }

    /**
     * @throws Exception
     */
    protected function getLastImportedObjectDisplayedInfo(CImportCampaign $campaign): array
    {
        $entity = $campaign->getLastImportedEntity();
        $object = CStoredObject::loadFromGuid($entity->internal_class . "-" . $entity->internal_id);

        if (!method_exists($object, "getDisplayedInfo")) {
            return [];
        }

        return array_merge(
            ['guid' => $object->_guid, 'view' => $object->_view],
            $object->getDisplayedInfo()
        );
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function ajax_count_with_condition(): void
    {
        $this->checkPermEdit();
        $type        = CView::get('type', 'str notNull');
        $campaign_id = CView::get('import_campaign_id', 'ref class|CImportCampaign notNull', true);
        $patient_id  = CView::get('patient_id', 'str');
        $import_type = CView::get('import_type', 'str');
        CView::checkin();

        $campaign = CImportCampaign::findOrFail($campaign_id);
        if (!$campaign->getPerm(PERM_EDIT) || $campaign->closing_date) {
            CAppUI::accessDenied();
        }

        $import = $this->getImportInstance($import_type);
        $import->setCampaign($campaign);

        echo $this->count($import, $type, $patient_id);
    }
}
