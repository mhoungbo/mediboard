<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Mapper;

use Exception;
use Generator;
use Ox\Import\Framework\Adapter\AdapterInterface;
use Ox\Import\Framework\Entity\EntityInterface;

interface MapperInterface
{
    /**
     * Get a single line from DB using $id
     *
     * @param mixed $id
     */
    public function retrieve($id): ?EntityInterface;

    /**
     * Get the n data from an optional Id
     *
     * @param mixed|null $id
     */
    public function get(int $count = 1, int $offset = 0, $id = null): ?Generator;

    public function count(): int;

    public function setMetadata(MapperMetadata $metadata): void;

    public function setAdapter(AdapterInterface $adapter): void;

    /**
     * @throws Exception
     */
    public function getMetadata(): MapperMetadata;

    /**
     * @throws Exception
     */
    public function getAdapter(): AdapterInterface;
}
