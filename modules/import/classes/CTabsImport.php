<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework;

use Ox\Core\Module\AbstractTabsRegister;

/**
 * @codeCoverageIgnore
 */
class CTabsImport extends AbstractTabsRegister
{
    public function registerAll(): void
    {
        $this->registerRoute('import_gui_campaigns_index', TAB_READ);
        $this->registerRoute('import_gui_objects_index', TAB_READ);
    }
}
