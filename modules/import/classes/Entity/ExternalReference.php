<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Entity;

use Ox\Import\Framework\Exception\ExternalReferenceException;

class ExternalReference
{
    /** @var string */
    private $name;

    /** @var mixed */
    private $id;

    /** @var bool */
    private $mandatory;

    public const UTILISATEUR = "utilisateur";

    public const PATIENT = "patient";

    public const PLAGE_CONSULTATION = "plage_consultation";

    public const PLAGE_CONSULTATION_AUTRE = "plage_consultation_autre";

    public const SEJOUR = "sejour";

    public const CONSULTATION = "consultation";

    public const CONSULTATION_AUTRE = "consultation_autre";

    public const MEDECIN = "medecin";

    public const MEDECIN_USER = "medecin_user";

    public const EVENEMENT = "evenement_patient";

    public const INJECTION = "injection";

    public const INTERVENTION = "intervention";

    public const OBSERVATION_RESULT_SET = "observation_result_set";

    public const IDENTIFIER = "observation_identifier";

    public const FILE = "file";

    public const LABEL = "label";

    public const VALIDATOR = "validator";

    public const RESPONSIBLE = "responsible";

    public const OBSERVATION_RESULT = "observation_result";

    public const UNIT = "observation_value_unit";

    public const OBSERVATION_EXAM = "observation_exam";

    public const OBSERVATION_RESPONSIBLE = "observation_responsible";

    public const OBSERVATION_PATIENT = "observation_patient";

    /**
     * ExternalReference constructor.
     *
     * @param mixed $id
     *
     * @throws ExternalReferenceException
     */
    public function __construct(string $name, $id, bool $mandatory)
    {
        if (is_null($id) && $mandatory) {
            throw new ExternalReferenceException('ExternalReference-error-Id is null for %s', $name);
        }

        $this->name      = $name;
        $this->id        = $id;
        $this->mandatory = $mandatory;
    }

    /**
     * @param mixed $id
     *
     * @throws ExternalReferenceException
     */
    public static function getMandatoryFor(string $name, $id): self
    {
        return new self($name, $id, true);
    }

    /**
     * @param mixed $id
     *
     * @throws ExternalReferenceException
     */
    public static function getNotMandatoryFor(string $name, $id): self
    {
        return new self($name, $id, false);
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function isMandatory(): bool
    {
        return $this->mandatory;
    }
}
