<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Entity;

use Ox\Core\CStoredObject;

class ExternalReferenceStash
{
    private array $references = [];

    public function addReference(ExternalReference $reference, CStoredObject $mb_object): void
    {
        $name = $reference->getName();

        if (!isset($this->references[$name])) {
            $this->references[$name] = [];
        }
        $this->references[$name][$reference->getId()] = $mb_object;
    }

    /**
     * @param mixed $id
     */
    private function findMbByExternalId(string $name, $id): ?CStoredObject
    {
        return ($this->references[$name][$id]) ?? null;
    }

    /**
     * @param mixed $id
     */
    public function getMbIdByExternalId(string $name, $id = null): ?int
    {
        if ($id === null) {
            return null;
        }
        $mb_object = $this->findMbByExternalId($name, $id);

        if ($mb_object && $mb_object->_id) {
            return $mb_object->_id;
        }

        return null;
    }
}
