<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Entity;

use Ox\Core\Specification\SpecificationViolation;
use Ox\Import\Framework\ImportableInterface;
use Ox\Import\Framework\Transformer\TransformerVisitorInterface;
use Ox\Import\Framework\Validator\ValidatorVisitorInterface;

/**
 * External medecin representation
 */
class Medecin extends AbstractEntity
{
    public const EXTERNAL_CLASS = 'MEDC';

    /** @var string */
    protected $nom;

    /** @var string */
    protected $prenom;

    /** @var string */
    protected $sexe;

    /** @var string */
    protected $titre;

    /** @var string */
    protected $email;

    /** @var string */
    protected $disciplines;

    /** @var string */
    protected $tel;

    /** @var string */
    protected $tel_autre;

    /** @var string */
    protected $adresse;

    /** @var string */
    protected $cp;

    /** @var string */
    protected $ville;

    /** @var string */
    protected $rpps;

    /** @var string */
    protected $adeli;

    /**
     * @inheritDoc
     */
    public function validate(ValidatorVisitorInterface $validator): ?SpecificationViolation
    {
        return $validator->validateMedecin($this);
    }

    /**
     * @inheritDoc
     */
    public function transform(
        TransformerVisitorInterface $transformer,
        ?ExternalReferenceStash     $reference_stash = null,
        ?CImportCampaign            $campaign = null
    ): ImportableInterface {
        return $transformer->transformMedecin($this, $reference_stash, $campaign);
    }

    /**
     * @inheritDoc
     */
    public function getDefaultRefEntities(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getExternalClass()
    {
        return static::EXTERNAL_CLASS;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getDisciplines(): ?string
    {
        return $this->disciplines;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function getTelAutre(): ?string
    {
        return $this->tel_autre;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function getRpps(): ?string
    {
        return $this->rpps;
    }

    public function getAdeli(): ?string
    {
        return $this->adeli;
    }
}
