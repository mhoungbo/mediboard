<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Transformer;

use Exception;
use Ox\Core\CMbDT;
use Ox\Import\Framework\Entity\ActeCCAM;
use Ox\Import\Framework\Entity\ActeNGAP;
use Ox\Import\Framework\Entity\Affectation;
use Ox\Import\Framework\Entity\Antecedent;
use Ox\Import\Framework\Entity\CImportCampaign;
use Ox\Import\Framework\Entity\Constante;
use Ox\Import\Framework\Entity\Consultation;
use Ox\Import\Framework\Entity\ConsultationAnesth;
use Ox\Import\Framework\Entity\Correspondant;
use Ox\Import\Framework\Entity\DossierMedical;
use Ox\Import\Framework\Entity\EvenementPatient;
use Ox\Import\Framework\Entity\ExternalReference;
use Ox\Import\Framework\Entity\ExternalReferenceStash;
use Ox\Import\Framework\Entity\File;
use Ox\Import\Framework\Entity\Injection;
use Ox\Import\Framework\Entity\Medecin;
use Ox\Import\Framework\Entity\Operation;
use Ox\Import\Framework\Entity\Patient;
use Ox\Import\Framework\Entity\PlageConsult;
use Ox\Import\Framework\Entity\Sejour;
use Ox\Import\Framework\Entity\Traitement;
use Ox\Import\Framework\Entity\User;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Cabinet\CActeNGAP;
use Ox\Mediboard\Cabinet\CConsultAnesth;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Cabinet\Vaccination\CInjection;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Files\CFilesCategory;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Hospi\CLit;
use Ox\Mediboard\Hospi\CPrestation;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\Hospi\CUniteFonctionnelle;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\ObservationResult\CObservationAbnormalFlag;
use Ox\Mediboard\ObservationResult\CObservationIdentifier;
use Ox\Mediboard\ObservationResult\CObservationResponsibleObserver;
use Ox\Mediboard\ObservationResult\CObservationResult;
use Ox\Mediboard\ObservationResult\CObservationResultExamen;
use Ox\Mediboard\ObservationResult\CObservationResultSet;
use Ox\Mediboard\ObservationResult\CObservationResultValue;
use Ox\Mediboard\ObservationResult\CObservationValueUnit;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationAbnormalFlag;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationExam;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationFile;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationIdentifier;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationPatient;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationResponsible;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationResult;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationResultSet;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationResultValue;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationValueUnit;
use Ox\Mediboard\Patients\CAntecedent;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\CCorrespondant;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CEvenementPatient;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CTraitement;
use Ox\Mediboard\PlanningOp\CChargePriceIndicator;
use Ox\Mediboard\PlanningOp\CModeEntreeSejour;
use Ox\Mediboard\PlanningOp\CModeSortieSejour;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\SalleOp\CActeCCAM;

class DefaultTransformer extends AbstractTransformer
{
    /** Objects in static cache to prevent multiple objects loading during import */
    private static array $object_cache = [];

    /**
     * @inheritDoc
     */
    public function transformUser(
        User                    $external_user,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CUser {
        $user = new CUser();

        $user->user_username   = $this->getFieldValue('username', $external_user->getUsername());
        $user->user_first_name = $this->getFieldValue('first_name', $external_user->getFirstName());
        $user->user_last_name  = $this->getFieldValue('last_name', $external_user->getLastName());
        $user->user_sexe       = $this->getFieldValue('gender', $external_user->getGender());
        $user->user_birthday   = $this->getFieldValue(
            'birthday',
            $this->formatDateTimeToStrDate($external_user->getBirthday())
        );
        $user->user_email      = $this->getFieldValue('email', $external_user->getEmail());
        $user->user_phone      = $this->getFieldValue('phone', $external_user->getPhone());
        $user->user_mobile     = $this->getFieldValue('mobile', $external_user->getMobile());
        $user->user_address1   = $this->getFieldValue('address', $external_user->getAddress());
        $user->user_zip        = $this->getFieldValue('zip', $external_user->getZip());
        $user->user_city       = $this->getFieldValue('city', $external_user->getCity());
        $user->user_country    = $this->getFieldValue('country', $external_user->getCountry());

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function transformPatient(
        Patient                 $external_patient,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CPatient {
        /** @var CPatient $patient */
        $patient = $external_patient->getMbObject() ?? new CPatient();

        $patient->nom                    = $this->getFieldValue('nom', $external_patient->getNom());
        $patient->prenom                 = $this->getFieldValue('prenom', $external_patient->getPrenom());
        $patient->deces                  = $this->getFieldValue(
            'deces',
            $this->formatDateTimeToStr($external_patient->getDeces())
        );
        $patient->naissance              = $this->getFieldValue(
            'date_naissance',
            $this->formatDateTimeToStrDate($external_patient->getNaissance())
        );
        $patient->cp_naissance           = $this->getFieldValue(
            'cp_naissance',
            $external_patient->getCpNaissance()
        );
        $patient->lieu_naissance         = $this->getFieldValue(
            'lieu_naissance',
            $external_patient->getLieuNaissance()
        );
        $patient->nom_jeune_fille        = $this->getFieldValue(
            'nom_naissance',
            $external_patient->getNomJeuneFille()
        );
        $patient->profession             = $this->getFieldValue('profession', $external_patient->getProfession());
        $patient->email                  = $this->getFieldValue('email', $external_patient->getEmail());
        $patient->tel                    = $this->getFieldValue(
            'tel',
            $this->sanitizeTel($external_patient->getTel())
        );
        $patient->tel2                   = $this->getFieldValue(
            'tel2',
            $this->sanitizeTel($external_patient->getTel2())
        );
        $patient->tel_autre              = $this->getFieldValue(
            'tel_autre',
            $this->sanitizeTel($external_patient->getTelAutre())
        );
        $patient->adresse                = $this->getFieldValue('adresse', $external_patient->getAdresse());
        $patient->cp                     = $this->getFieldValue('cp', $external_patient->getCp());
        $patient->ville                  = $this->getFieldValue('ville', $external_patient->getVille());
        $patient->pays                   = $this->getFieldValue('pays', $external_patient->getPays());
        $patient->matricule              = $this->getFieldValue('matricule', $external_patient->getMatricule());
        $patient->sexe                   = $this->getFieldValue('sexe', $external_patient->getSexe());
        $patient->civilite               = $this->getFieldValue('civilite', $external_patient->getCivilite());
        $patient->situation_famille      = $this->getFieldValue(
            'situation_famille',
            $external_patient->getSituationFamille()
        );
        $patient->activite_pro           = $this->getFieldValue(
            'activite_pro',
            $external_patient->getActivitePro()
        );
        $patient->rques                  = $this->getFieldValue('remarques', $external_patient->getRques());
        $patient->ald                    = $this->getFieldValue('ald', $external_patient->getAld());
        $patient->_IPP                   = $this->getFieldValue('ipp', $external_patient->getIpp());
        $patient->assure_nom             = $this->getFieldValue('nom_assure', $external_patient->getNomAssure());
        $patient->assure_prenom          = $this->getFieldValue(
            'prenom_assure',
            $external_patient->getPrenomAssure()
        );
        $patient->assure_nom_jeune_fille = $this->getFieldValue(
            'nom_naissance_assure',
            $external_patient->getNomNaissanceAssure()
        );
        $patient->assure_sexe            = $this->getFieldValue('sexe_assure', $external_patient->getSexeAssure());
        $patient->assure_civilite        = $this->getFieldValue(
            'civilite_assure',
            $external_patient->getCiviliteAssure()
        );
        $patient->assure_naissance       = $this->getFieldValue(
            'naissance_assure',
            $this->formatDateTimeToStrDate(
                $external_patient->getNaissanceAssure()
            )
        );
        $patient->assure_adresse         = $this->getFieldValue(
            'adresse_assure',
            $external_patient->getAdresseAssure()
        );
        $patient->assure_ville           = $this->getFieldValue(
            'ville_assure',
            $external_patient->getVilleAssure()
        );
        $patient->assure_cp              = $this->getFieldValue('cp_assure', $external_patient->getCpAssure());
        $patient->assure_pays            = $this->getFieldValue('pays_assure', $external_patient->getPaysAssure());
        $patient->assure_tel             = $this->getFieldValue(
            'tel_assure',
            $this->sanitizeTel(
                $external_patient->getTelAssure()
            )
        );
        $patient->assure_matricule       = $this->getFieldValue(
            'matricule_assure',
            $external_patient->getMatriculeAssure()
        );

        $patient->medecin_traitant = $this->getFieldValue(
            'medecin_traitant',
            $reference_stash->getMbIdByExternalId(ExternalReference::MEDECIN, $external_patient->getMedecinTraitant())
        );

        if ($patient->sexe === 'u') {
            $patient->guessSex();
            if ($patient->sexe === 'u') {
                $patient->sexe = null;
            }
        }

        // If date of death is invalid and we decide to null it
        if ($patient->deces === null) {
            $patient->deces = "";
        }

        return $patient;
    }

    /**
     * @inheritDoc
     */
    public function transformMedecin(
        Medecin                 $external_medecin,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CMedecin {
        /** @var CMedecin $medecin */
        $medecin = $external_medecin->getMbObject() ?? new CMedecin();

        $medecin->nom         = $this->getFieldValue('nom', $external_medecin->getNom());
        $medecin->prenom      = $this->getFieldValue('prenom', $external_medecin->getPrenom());
        $medecin->titre       = $this->getFieldValue('titre', $external_medecin->getTitre());
        $medecin->email       = $this->getFieldValue('email', $external_medecin->getEmail());
        $medecin->tel         = $this->getFieldValue('tel', $this->sanitizeTel($external_medecin->getTel()));
        $medecin->tel_autre   = $this->getFieldValue('tel_autre', $this->sanitizeTel($external_medecin->getTelAutre()));
        $medecin->adresse     = $this->getFieldValue('adresse', $external_medecin->getAdresse());
        $medecin->cp          = $this->getFieldValue('cp', $external_medecin->getCp());
        $medecin->ville       = $this->getFieldValue('ville', $external_medecin->getVille());
        $medecin->disciplines = $this->getFieldValue('disciplines', $external_medecin->getDisciplines());
        $medecin->sexe        = $this->getFieldValue('sexe', $external_medecin->getSexe());
        $medecin->rpps        = $this->getFieldValue('rpps', $external_medecin->getRpps());
        $medecin->adeli       = $this->getFieldValue('adeli', $external_medecin->getAdeli());

        return $medecin;
    }

    /**
     * @inheritDoc
     */
    public function transformPlageConsult(
        PlageConsult            $external_plage_consult,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CPlageconsult {
        /** @var CPlageconsult $plage_consult */
        $plage_consult = $external_plage_consult->getMbObject() ?? new CPlageconsult();

        // Never change the date of a CPlageconsult
        if (!$plage_consult->_id) {
            $plage_consult->date  = $this->getFieldValue(
                'date',
                $this->formatDateTimeToStrDate(
                    $external_plage_consult->getDate()
                )
            );
            $plage_consult->debut = $this->getFieldValue(
                'debut',
                $this->formatDateTimeToStrTime(
                    $external_plage_consult->getDebut()
                )
            );
            $plage_consult->fin   = $this->getFieldValue(
                'fin',
                $this->formatDateTimeToStrTime(
                    $external_plage_consult->getFin()
                )
            );
        }

        $plage_consult->freq    = $this->getFieldValue(
            'freq',
            $this->formatDateTimeToStrTime($external_plage_consult->getFreq())
        );
        $plage_consult->libelle = $this->getFieldValue('libelle', $external_plage_consult->getLibelle());

        $plage_consult->chir_id = $this->getFieldValue(
            'chir_id',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::UTILISATEUR,
                $external_plage_consult->getChirId()
            )
        );

        return $plage_consult;
    }

    /**
     * @inheritDoc
     */
    public function transformConsultation(
        Consultation            $external_consultation,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CConsultation {
        /** @var CConsultation $consultation */
        $consultation = $external_consultation->getMbObject() ?? new CConsultation();

        $consultation->heure            = $this->getFieldValue(
            'heure',
            $this->formatDateTimeToStrTime($external_consultation->getHeure())
        );
        $consultation->duree            = $this->getFieldValue('duree', $external_consultation->getDuree());
        $consultation->motif            = $this->getFieldValue('motif', $external_consultation->getMotif());
        $consultation->rques            = $this->getFieldValue('remarques', $external_consultation->getRques());
        $consultation->examen           = $this->getFieldValue('examen', $external_consultation->getExamen());
        $consultation->traitement       = $this->getFieldValue('traitement', $external_consultation->getTraitement());
        $consultation->histoire_maladie = $this->getFieldValue(
            'histoire_maladie',
            $external_consultation->getHistoireMaladie()
        );
        $consultation->conclusion       = $this->getFieldValue('conclusion', $external_consultation->getConclusion());
        $consultation->resultats        = $this->getFieldValue('resultats', $external_consultation->getResultats());
        $consultation->chrono           = $this->getFieldValue('chrono', $external_consultation->getChrono());

        $plage_consult_type = $external_consultation->getDefaultRefs()
            ? ExternalReference::PLAGE_CONSULTATION
            : ExternalReference::PLAGE_CONSULTATION_AUTRE;

        $consultation->plageconsult_id = $reference_stash->getMbIdByExternalId(
            $plage_consult_type,
            $external_consultation->getPlageconsultId()
        );

        $consultation->patient_id      = $this->getFieldValue(
            'patient',
            $reference_stash->getMbIdByExternalId(
                $plage_consult_type,
                $external_consultation->getPatientId()
            )
        );

        return $consultation;
    }

    /**
     * @inheritDoc
     */
    public function transformConsultationAnesth(
        ConsultationAnesth      $external_consultation,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CConsultAnesth {
        // TODO: Implement transformConsultationAnesth() method.
        return new CConsultAnesth();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function transformSejour(
        Sejour                  $external_sejour,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CSejour {
        /** @var CSejour $sejour */
        $sejour = $external_sejour->getMbObject() ?? new CSejour();

        $sejour->type          = $this->getFieldValue('type', $external_sejour->getType());
        $sejour->libelle       = $this->getFieldValue('libelle', $external_sejour->getLibelle());
        $sejour->entree_prevue = $this->getFieldValue(
            'entree_prevue',
            $this->formatDateTimeToStr($external_sejour->getEntreePrevue())
        );
        $sejour->entree_reelle = $this->getFieldValue(
            'entree_reelle',
            $this->formatDateTimeToStr($external_sejour->getEntreeReelle())
        );
        $sejour->sortie_prevue = $this->getFieldValue(
            'sortie_prevue',
            $this->formatDateTimeToStr($external_sejour->getSortiePrevue())
        );
        $sejour->sortie_reelle = $this->getFieldValue(
            'sortie_reelle',
            $this->formatDateTimeToStr($external_sejour->getSortieReelle())
        );

        $sejour->patient_id   = $this->getFieldValue(
            'patient',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::PATIENT,
                $external_sejour->getPatientId()
            )
        );
        $sejour->praticien_id = $this->getFieldValue(
            'praticien',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::UTILISATEUR,
                $external_sejour->getPraticienId()
            )
        );

        $sejour->_NDA = $this->getFieldValue('nda', $external_sejour->getNda());

        // prestation
        $prestation_field = $this->getFieldValue('prestation', $external_sejour->getPrestation());
        if ($prestation_field && $prestation = $this->getPrestation($prestation_field)) {
            $sejour->prestation_id = $prestation->_id;
        }

        // mode de traitement
        $traitement_field = $this->getFieldValue('mode_traitement', $external_sejour->getModeTraitement());
        if ($traitement_field && $cpi = $this->getModeTraitement($traitement_field)) {
            $sejour->charge_id = $cpi->_id;
            $sejour->type      = $cpi->type;
            $sejour->type_pec  = $cpi->type_pec;
        }

        // mode entree
        $entree_field = $this->getFieldValue('mode_entree', $external_sejour->getModeEntree());
        if ($entree_field && $mes = $this->getModeEntree($entree_field)) {
            $sejour->mode_entree_id = $mes->_id;
            $sejour->mode_entree    = $mes->mode;
        }

        // mode sortie
        $sortie_field = $this->getFieldValue('mode_sortie', $external_sejour->getModeSortie());
        if ($sortie_field && $mss = $this->getModeSortie($sortie_field)) {
            $sejour->mode_sortie_id = $mss->_id;
            $sejour->mode_sortie    = $mss->mode;
        }

        // Todo: Try to remove
        $sejour->group_id = CGroups::loadCurrent()->_id;

        return $sejour;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function transformFile(
        File                    $external_file,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CFile {
        /** @var CFile $file */
        $file = $external_file->getMbObject() ?? new CFile();

        $file->file_date = $this->getFieldValue('date', $this->formatDateTimeToStr($external_file->getFileDate()));
        $file->file_name = $this->getFieldValue('nom', $external_file->getFileName());
        $file->file_type = $this->getFieldValue('type', $external_file->getFileType());

        if ($content_field = $this->getFieldValue('contenu', $external_file->getFileContent())) {
            $file->setContent($content_field);
        } elseif ($path_field = $this->getFieldValue('chemin', $external_file->getFilePath())) {
            $file->setCopyFrom($path_field);
        }

        if ($reference_stash && $author_field = $this->getFieldValue('auteur', $external_file->getAuthorId())) {
            $file->author_id = $reference_stash->getMbIdByExternalId(ExternalReference::UTILISATEUR, $author_field);
        }

        if (!$file->author_id) {
            $file->author_id = CMediusers::get()->_id;
        }

        if (
            $sejour_id = $this->getFieldValue(
                'sejour',
                $this->getContextId(
                    $external_file->getSejourId(),
                    ExternalReference::SEJOUR,
                    $reference_stash
                )
            )
        ) {
            $file->object_class = 'CSejour';
            $file->object_id    = $sejour_id;
        } elseif (
            $consult_id = $this->getFieldValue(
                'consultation',
                $this->getContextId(
                    $external_file->getConsultationId(),
                    $external_file->getDefaultRefs(
                    ) ? ExternalReference::CONSULTATION : ExternalReference::CONSULTATION_AUTRE,
                    $reference_stash
                )
            )
        ) {
            $file->object_class = 'CConsultation';
            $file->object_id    = $consult_id;
        } elseif (
            $event_id = $this->getFieldValue(
                'evenement',
                $this->getEvenementId($external_file, $reference_stash)
            )
        ) {
            $file->object_class = 'CEvenementPatient';
            $file->object_id    = $event_id;
        } elseif ($patient_id = $this->getFieldValue('patient', $external_file->getPatientId())) {
            $file->object_class = 'CPatient';
            $file->object_id    = $reference_stash->getMbIdByExternalId(ExternalReference::PATIENT, $patient_id);
        }

        if ($cat_name = $this->getFieldValue('categorie', $external_file->getFileCatName())) {
            $file->file_category_id = $this->getCategorie($cat_name);
        }

        $file->fillFields();
        $file->updateFormFields();

        return $file;
    }

    /**
     * @inheritDoc
     */
    public function transformAntecedent(
        Antecedent              $external_atcd,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CAntecedent {
        /** @var CAntecedent $antecedent */
        $antecedent = $external_atcd->getMbObject() ?? new CAntecedent();

        $patient_id = $this->getFieldValue(
            'patient',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::PATIENT,
                $external_atcd->getPatientId()
            )
        );

        if ($patient_id) {
            $antecedent->rques   = $this->getFieldValue('text', $external_atcd->getText());
            $antecedent->comment = $this->getFieldValue('comment', $external_atcd->getComment());
            $antecedent->date    = ($antecedent->_id) ? null : CMbDT::date();
            if ($date = $this->getFieldValue('date', $external_atcd->getDate())) {
                $antecedent->date = $this->formatDateTimeToStrDate($date);
            }

            $antecedent->type     = $this->getFieldValue('type', $external_atcd->getType());
            $antecedent->appareil = $this->getFieldValue('appareil', $external_atcd->getAppareil());

            if (
                $user_id = $this->getFieldValue(
                    'praticien',
                    $reference_stash->getMbIdByExternalId(
                        ExternalReference::UTILISATEUR,
                        $external_atcd->getOwnerId()
                    )
                )
            ) {
                $antecedent->owner_id = $user_id;
            }

            $antecedent->dossier_medical_id = CDossierMedical::dossierMedicalId($patient_id, 'CPatient');
        }

        return $antecedent;
    }

    /**
     * @inheritDoc
     */
    public function transformTraitement(
        Traitement              $external_trt,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CTraitement {
        /** @var CTraitement $traitement */
        $traitement = $external_trt->getMbObject() ?? new CTraitement();

        $patient_id = $this->getFieldValue(
            'patient',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::PATIENT,
                $external_trt->getPatientId()
            )
        );

        if ($patient_id) {
            $traitement->traitement = $this->getFieldValue('text', $external_trt->getTraitement());
            $traitement->debut      = ($traitement->_id) ? null : CMbDT::date();
            if ($debut = $this->getFieldValue('date_debut', $external_trt->getDebut())) {
                $traitement->debut = $this->formatDateTimeToStrDate($debut);
            }

            if ($fin = $this->getFieldValue('date_fin', $external_trt->getFin())) {
                $traitement->fin = $this->formatDateTimeToStrDate($fin);
            }

            if (
                $user_id = $this->getFieldValue(
                    'praticien',
                    $reference_stash->getMbIdByExternalId(
                        ExternalReference::UTILISATEUR,
                        $external_trt->getOwnerId()
                    )
                )
            ) {
                $traitement->owner_id = $user_id;
            }

            $traitement->dossier_medical_id = CDossierMedical::dossierMedicalId($patient_id, 'CPatient');
        }

        return $traitement;
    }

    /**
     * @inheritDoc
     */
    public function transformCorrespondant(
        Correspondant           $external_correspondant,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CCorrespondant {
        /** @var CCorrespondant $correspondant */
        $correspondant = $external_correspondant->getMbObject() ?? new CCorrespondant();

        $correspondant->patient_id = $this->getFieldValue(
            'patient',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::PATIENT,
                $external_correspondant->getPatientId()
            )
        );
        $correspondant->medecin_id = $this->getFieldValue(
            'medecin',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::MEDECIN,
                $external_correspondant->getMedecinId()
            )
        );

        if ($correspondant->patient_id && $correspondant->medecin_id) {
            $correspondant->loadMatchingObjectEsc();
        }

        return $correspondant;
    }

    /**
     * @inheritDoc
     */
    public function transformEvenementPatient(
        EvenementPatient        $external_patient_event,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CEvenementPatient {
        /** @var CEvenementPatient $patient_event */
        $patient_event = $external_patient_event->getMbObject() ?? new CEvenementPatient();

        $patient_event->dossier_medical_id = CDossierMedical::dossierMedicalId(
            $this->getFieldValue(
                'patient_id',
                $reference_stash->getMbIdByExternalId(
                    ExternalReference::PATIENT,
                    $external_patient_event->getPatientId()
                )
            ),
            'CPatient'
        );
        $patient_event->praticien_id       = $this->getFieldValue(
            'practitioner_id',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::UTILISATEUR,
                $external_patient_event->getPractitionerId()
            )
        );
        $patient_event->date               = $this->getFieldValue(
            'datetime',
            $this->formatDateTimeToStr(
                $external_patient_event->getDatetime()
            )
        );
        $patient_event->libelle            = $this->getFieldValue('label', $external_patient_event->getLabel());
        $patient_event->type               = $this->getFieldValue('type', $external_patient_event->getType());
        $patient_event->description        = $this->getFieldValue(
            'description',
            $external_patient_event->getDescription()
        );

        return $patient_event;
    }

    /**
     * @inheritDoc
     */
    public function transformInjection(
        Injection               $external_injection,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CInjection {
        /** @var CInjection $injection */
        $injection = $external_injection->getMbObject() ?? new CInjection();

        $injection->patient_id        = $this->getFieldValue(
            'patient',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::PATIENT,
                $external_injection->getPatientId()
            )
        );
        $injection->practitioner_name = $this->getFieldValue(
            'practitioner_name',
            $external_injection->getPractitionerName()
        );
        $injection->injection_date    = $this->getFieldValue(
            'injection_date',
            $this->formatDateTimeToStr(
                $external_injection->getInjectionDate()
            )
        );
        $injection->batch             = $this->getFieldValue('batch', $external_injection->getBatch());
        $injection->speciality        = $this->getFieldValue('speciality', $external_injection->getSpeciality());
        $injection->remarques         = $this->getFieldValue('remarques', $external_injection->getRemarques());
        $injection->cip_product       = $this->getFieldValue('cip_product', $external_injection->getCipProduct());

        if ($expiration_date = $this->getFieldValue('expiration_date', $external_injection->getExpirationDate())) {
            $injection->expiration_date = $this->formatDateTimeToStrDate($expiration_date);
        }

        $injection->recall_age   = $this->getFieldValue('recall_age', $external_injection->getRecallAge());
        $injection->_type_vaccin = $this->getFieldValue(
            'type_vaccin',
            $external_injection->getTypeVaccin()
        ) ?? "Autre";

        return $injection;
    }

    /**
     * @inheritDoc
     */
    public function transformActeCCAM(
        ActeCCAM                $external_acte,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CActeCCAM {
        /** @var CActeCCAM $acte_ccam */
        $acte_ccam = $external_acte->getMbObject() ?? new CActeCCAM();

        $acte_ccam->code_acte           = $this->getFieldValue('code_acte', $external_acte->getCodeActe());
        $acte_ccam->execution           = ($datetime = $this->getFieldValue(
            'date_execution',
            $external_acte->getDateExecution()
        )) ? $this->formatDateTimeToStr(
            $datetime
        ) : null;
        $acte_ccam->code_activite       = $this->getFieldValue('code_activite', $external_acte->getCodeActivite());
        $acte_ccam->code_phase          = $this->getFieldValue('code_phase', $external_acte->getCodePhase());
        $acte_ccam->modificateurs       = $this->getFieldValue('modificateurs', $external_acte->getModificateurs());
        $acte_ccam->montant_base        = $this->getFieldValue('montant_base', $external_acte->getMontantBase());
        $acte_ccam->montant_depassement = $this->getFieldValue(
            'montant_depassement',
            $external_acte->getMontantDepassement()
        );

        $acte_ccam->executant_id = $this->getFieldValue(
            'executant',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::UTILISATEUR,
                $external_acte->getExecutantId()
            )
        );

        $acte_ccam->object_class = 'CConsultation';
        $acte_ccam->object_id    = $this->getFieldValue(
            'consultation',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::CONSULTATION,
                $external_acte->getConsultationId()
            )
        );

        return $acte_ccam;
    }

    /**
     * @inheritDoc
     */
    public function transformActeNGAP(
        ActeNGAP                $external_acte,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CActeNGAP {
        /** @var CActeNGAP $acte_ngap */
        $acte_ngap = $external_acte->getMbObject() ?? new CActeNGAP();

        $acte_ngap->code                = $this->getFieldValue('code_acte', $external_acte->getCodeActe());
        $acte_ngap->execution           = ($datetime = $this->getFieldValue(
            'date_execution',
            $external_acte->getDateExecution()
        )) ? $this->formatDateTimeToStr(
            $datetime
        ) : null;
        $acte_ngap->quantite            = $this->getFieldValue('quantite', $external_acte->getQuantite());
        $acte_ngap->coefficient         = $this->getFieldValue('coefficient', $external_acte->getCoefficient());
        $acte_ngap->montant_base        = $this->getFieldValue('montant_base', $external_acte->getMontantBase());
        $acte_ngap->montant_depassement = $this->getFieldValue(
            'montant_depassement',
            $external_acte->getMontantDepassement()
        );
        $acte_ngap->numero_dent         = $this->getFieldValue('numero_dent', $external_acte->getNumeroDent());

        $acte_ngap->executant_id = $this->getFieldValue(
            'executant',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::UTILISATEUR,
                $external_acte->getExecutantId()
            )
        );

        $acte_ngap->object_class = 'CConsultation';
        $acte_ngap->object_id    =
            $this->getFieldValue(
                'consultation',
                $reference_stash->getMbIdByExternalId(
                    ExternalReference::CONSULTATION,
                    $external_acte->getConsultationId()
                )
            );

        return $acte_ngap;
    }

    /**
     * @inheritDoc
     */
    public function transformConstante(
        Constante               $external_constante,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CConstantesMedicales {
        /** @var CConstantesMedicales $constante */
        $constante = $external_constante->getMbObject() ?? new CConstantesMedicales();

        $constante->datetime            = $this->getFieldValue(
            'date',
            $this->formatDateTimeToStr(
                $external_constante->getDatetime()
            )
        ) ?? CMbDT::dateTime();
        $constante->patient_id          = $this->getFieldValue(
            'patient',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::PATIENT,
                $external_constante->getPatientId()
            )
        );
        $constante->user_id             = $this->getFieldValue(
            'praticien',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::UTILISATEUR,
                $external_constante->getUserId()
            )
        );
        $constante->poids               = $this->getFieldValue('poids', $external_constante->getPoids());
        $constante->taille              = $this->getFieldValue('taille', $external_constante->getTaille());
        $constante->pouls               = $this->getFieldValue('pouls', $external_constante->getPouls());
        $constante->temperature         = $this->getFieldValue('temperature', $external_constante->getTemperature());
        $constante->_ta_droit_systole   = $this->getFieldValue(
            'ta_droit_systole',
            $external_constante->getTaDroitSystole()
        );
        $constante->_ta_droit_diastole  = $this->getFieldValue(
            'ta_droit_diastole',
            $external_constante->getTaDroitDiastole()
        );
        $constante->_ta_gauche_systole  = $this->getFieldValue(
            'ta_gauche_systole',
            $external_constante->getTaGaucheSystole()
        );
        $constante->_ta_gauche_diastole = $this->getFieldValue(
            'ta_gauche_diastole',
            $external_constante->getTaGaucheDiastole()
        );
        $constante->pointure            = $this->getFieldValue('pointure', $external_constante->getPointure());

        return $constante;
    }

    /**
     * @inheritDoc
     */
    public function transformDossierMedical(
        DossierMedical          $external_dossier,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CDossierMedical {
        /** @var CDossierMedical $dossier */
        $dossier = $external_dossier->getMbObject() ?? new CDossierMedical();

        $dossier->object_class   = 'CPatient';
        $dossier->object_id      = $this->getFieldValue(
            'patient',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::PATIENT,
                $external_dossier->getPatientId()
            )
        );
        $dossier->groupe_sanguin = $this->getFieldValue('groupe_sanguin', $external_dossier->getGroupSanguin());
        $dossier->rhesus         = $this->getFieldValue('rhesus', $external_dossier->getRhesus());

        return $dossier;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function transformAffectation(
        Affectation             $external_affectation,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CAffectation {
        /** @var CAffectation $affectation */
        $affectation = $external_affectation->getMbObject() ?? new CAffectation();

        $affectation->sejour_id = $this->getFieldValue(
            'sejour',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::SEJOUR,
                $external_affectation->getSejourId()
            )
        );
        $affectation->entree    = $this->getFieldValue(
            'entree',
            $this->formatDateTimeToStr($external_affectation->getEntree())
        );
        $affectation->sortie    = $this->getFieldValue(
            'sortie',
            $this->formatDateTimeToStr($external_affectation->getSortie())
        );
        $affectation->rques     = $this->getFieldValue('remarques', $external_affectation->getRemarques());
        $affectation->effectue  = $this->getFieldValue('effectue', $external_affectation->getEffectue());

        // service
        $service_field = $this->getFieldValue('nom_service', $external_affectation->getNomService());
        if ($service_field && $serv = $this->getService($service_field)) {
            $affectation->service_id = $serv->_id;
        }

        // lit
        $lit_field = $this->getFieldValue('nom_lit', $external_affectation->getNomLit());
        if ($lit_field && $lit = $this->getLit($lit_field, $serv->_id)) {
            $affectation->lit_id = $lit->_id;
        }

        // mode entree
        $entree_field = $this->getFieldValue('mode_entree', $external_affectation->getModeEntree());
        if ($entree_field && $me = $this->getModeEntree($entree_field)) {
            $affectation->mode_entree_id = $me->_id;
            $affectation->mode_entree    = $me->mode;
        }

        // mode sortie
        $sortie_field = $this->getFieldValue('mode_sortie', $external_affectation->getModeSortie());
        if ($sortie_field && $ms = $this->getModeSortie($sortie_field)) {
            $affectation->mode_sortie_id = $ms->_id;
            $affectation->mode_sortie    = $ms->code;
        }

        // code unite fonctionnelle
        $uf_field = $this->getFieldValue('code_unite_fonctionnelle', $external_affectation->getCodeUf());
        if ($uf_field && $uf = $this->getUf($uf_field)) {
            if ($uf->type === 'soins') {
                $affectation->uf_soins_id = $uf->_id;
            } elseif ($uf->type === 'hebergement') {
                $affectation->uf_hebergement_id = $uf->_id;
            } elseif ($uf->type === 'medicale') {
                $affectation->uf_medicale_id = $uf->_id;
            }
        }

        return $affectation;
    }

    public function transformOperation(
        Operation               $external_operation,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): COperation {
        /** @var COperation $operation */
        $operation = $external_operation->getMbObject() ?? new COperation();

        $operation->sejour_id      = $this->getFieldValue(
            'sejour',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::SEJOUR,
                $external_operation->getSejourId()
            )
        );
        $operation->chir_id        = $this->getFieldValue(
            'chir',
            $reference_stash->getMbIdByExternalId(
                ExternalReference::UTILISATEUR,
                $external_operation->getChirId()
            )
        );
        $operation->cote           = $this->getFieldValue('cote', $external_operation->getCote());
        $date_field                = $this->getFieldValue('date_intervention', $external_operation->getDateTime());
        $operation->date           = CMbDT::date($date_field);
        $operation->time_operation = CMbDT::time($date_field);
        $operation->libelle        = $this->getFieldValue('libelle', $external_operation->getLibelle());
        $operation->examen         = $this->getFieldValue('examen', $external_operation->getExamen());

        return $operation;
    }

    public function transformObservationResult(
        ObservationResult       $external_observation_result,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CObservationResult {
        // Define in OxLaboTransformer
    }

    public function transformObservationIdentifier(
        ObservationIdentifier   $external_observation_identifier,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CObservationIdentifier {
        // Define in OxLaboTransformer
    }

    public function transformObservationResultValue(
        ObservationResultValue  $external_observation_result_value,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CObservationResultValue {
        // Define in OxLaboTransformer
    }

    public function transformObservationResultSet(
        ObservationResultSet    $external_observation_result_set,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CObservationResultSet {
        // Define in OxLaboTransformer
    }

    public function transformObservationAbnormalFlag(
        ObservationAbnormalFlag $external_observation_flag,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CObservationAbnormalFlag {
        // Define in OxLaboTransformer
    }

    public function transformObservationValueUnit(
        ObservationValueUnit    $external_observation_value_unit,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CObservationValueUnit {
        // Define in OxLaboTransformer
    }

    public function transformObservationFile(
        ObservationFile         $observation_file,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CFile {
        // Define in OxLaboTransformer
    }

    public function transformObservationResponsible(
        ObservationResponsible  $observation_responsible,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CObservationResponsibleObserver {
        // Define in OxLaboTransformer
    }

    public function transformObservationExam(
        ObservationExam         $observation_responsible,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CObservationResultExamen {
        // Define in OxLaboTransformer
    }

    public function transformObservationPatient(
        ObservationPatient      $observation_patient,
        ?ExternalReferenceStash $reference_stash = null,
        ?CImportCampaign        $campaign = null
    ): CPatient {
        // Define in OxLaboTransformer
    }

    /**
     * Get prestation object by name
     * @throws Exception
     */
    protected function getPrestation(?string $code): ?CPrestation
    {
        if ($code === null) {
            return null;
        }

        if (!isset(static::$object_cache[CPrestation::class][$code])) {
            $presta           = new CPrestation();
            $presta->code     = $code;
            $presta->group_id = CGroups::loadCurrent()->_id;

            static::$object_cache[CPrestation::class][$code] = $presta->loadMatchingObjectEsc() ? $presta : null;
        }

        return static::$object_cache[CPrestation::class][$code];
    }

    /**
     * Get mode de traitement object by name
     * @throws Exception
     */
    protected function getModeTraitement(?string $code): ?CChargePriceIndicator
    {
        if ($code === null) {
            return null;
        }

        if (!isset(static::$object_cache[CChargePriceIndicator::class][$code])) {
            $cpi           = new CChargePriceIndicator();
            $cpi->code     = $code;
            $cpi->group_id = CGroups::loadCurrent()->_id;
            $cpi->actif    = 1;

            static::$object_cache[CChargePriceIndicator::class][$code] = $cpi->loadMatchingObjectEsc() ? $cpi : null;
        }

        return static::$object_cache[CChargePriceIndicator::class][$code];
    }

    /**
     * Get mode d'entr�e object by name
     * @throws Exception
     */
    protected function getModeEntree(?string $code): ?CModeEntreeSejour
    {
        if ($code === null) {
            return null;
        }

        if (!isset(static::$object_cache[CModeEntreeSejour::class][$code])) {
            $mes           = new CModeEntreeSejour();
            $mes->code     = $code;
            $mes->group_id = CGroups::loadCurrent()->_id;
            $mes->actif    = 1;

            static::$object_cache[CModeEntreeSejour::class][$code] = $mes->loadMatchingObjectEsc() ? $mes : null;
        }

        return static::$object_cache[CModeEntreeSejour::class][$code];
    }

    /**
     * Get mode de sortie object by name
     * @throws Exception
     */
    protected function getModeSortie(?string $code): ?CModeSortieSejour
    {
        if ($code === null) {
            return null;
        }

        if (!isset(static::$object_cache[CModeSortieSejour::class][$code])) {
            $mss           = new CModeSortieSejour();
            $mss->code     = $code;
            $mss->group_id = CGroups::loadCurrent()->_id;
            $mss->actif    = 1;

            static::$object_cache[CModeSortieSejour::class][$code] = $mss->loadMatchingObjectEsc() ? $mss : null;
        }

        return static::$object_cache[CModeSortieSejour::class][$code];
    }

    protected function getEvenementId(File $file, ExternalReferenceStash $reference_stash): ?string
    {
        return $reference_stash->getMbIdByExternalId(
            ExternalReference::EVENEMENT,
            $file->getEvenementId()
        );
    }

    protected function getContextId(
        ?string                $external_id,
        string                 $type,
        ExternalReferenceStash $reference_stash
    ): ?string {
        return $reference_stash->getMbIdByExternalId($type, $external_id);
    }

    protected function getCategorie(string $cat_name): ?int
    {
        $cat           = new CFilesCategory();
        $cat->nom      = $cat_name;
        $cat->group_id = CGroups::loadCurrent()->_id;
        $cat->loadMatchingObjectEsc();

        if (!$cat->_id) {
            $cat->loadObject(
                [
                    'nom'      => $cat->getDS()->prepare('= ?', $cat_name),
                    'group_id' => $cat->getDS()->prepare('IS NULL'),
                ]
            );
        }

        return $cat->_id;
    }

    /**
     * Get service object by name
     * @throws Exception
     */
    protected function getService(?string $nom): ?CService
    {
        if ($nom === null) {
            return null;
        }

        if (!isset(static::$object_cache[CService::class][$nom])) {
            $serv           = new CService();
            $serv->nom      = $nom;
            $serv->group_id = CGroups::loadCurrent()->_id;

            static::$object_cache[CService::class][$nom] = $serv->loadMatchingObjectEsc() ? $serv : null;
        }

        return static::$object_cache[CService::class][$nom];
    }

    /**
     * get lit object by name
     *
     * @throws Exception
     */
    protected function getLit(?string $nom, ?int $service_id): ?CLit
    {
        if ($nom === null) {
            return null;
        }

        $code = $service_id . "-" . $nom;

        if (!isset(static::$object_cache[CLit::class][$code])) {
            $lit   = new CLit();
            $ljoin = [
                "chambre" => "chambre.chambre_id = lit.chambre_id",
            ];

            $ds = $lit->getDS();

            $where = [
                "lit.nom"            => $ds->prepare("= ?", $nom),
                "chambre.service_id" => $ds->prepare("= ?", $service_id),
            ];

            static::$object_cache[CLit::class][$code] = $lit->loadObject($where, null, null, $ljoin) ? $lit : null;
        }

        return static::$object_cache[CLit::class][$code];
    }

    /**
     * Get unite fonctionnelle object by name
     * @throws Exception
     */
    protected function getUf(?string $code)
    {
        if ($code === null) {
            return null;
        }

        if (!isset(static::$object_cache[CUniteFonctionnelle::class][$code])) {
            $uf           = new CUniteFonctionnelle();
            $uf->code     = $code;
            $uf->group_id = CGroups::loadCurrent()->_id;

            static::$object_cache[CUniteFonctionnelle::class][$code] = $uf->loadMatchingObjectEsc() ? $uf : false;
        }

        return static::$object_cache[CUniteFonctionnelle::class][$code];
    }
}
