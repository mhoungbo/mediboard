<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Transformer;

use DateTime;
use Ox\Import\Framework\Configuration\ConfigurableInterface;
use Ox\Import\Framework\Configuration\ConfigurationTrait;
use Ox\Import\Framework\Entity\ExternalReferenceStash;

abstract class AbstractTransformer implements TransformerVisitorInterface, ConfigurableInterface
{
    use ConfigurationTrait;

    private const DATETIME_FORMAT = 'Y-m-d H:i:s';
    private const DATE_FORMAT     = 'Y-m-d';
    private const TIME_FORMAT     = 'H:i:s';

    /** @var ExternalReferenceStash|null */
    private $reference_stash;

    public function setReferenceStash(?ExternalReferenceStash $reference_stash = null): void
    {
        $this->reference_stash = $reference_stash;
    }

    public function getReferenceStash(): ?ExternalReferenceStash
    {
        return $this->reference_stash;
    }

    protected function sanitizeTel(?string $tel): ?string
    {
        return preg_replace('/\D+/', '', $tel ?? '');
    }

    protected function formatDateTimeToStr(?DateTime $datetime): ?string
    {
        return $this->formatDateTime($datetime, self::DATETIME_FORMAT);
    }

    protected function formatDateTimeToStrDate(?DateTime $datetime): ?string
    {
        return $this->formatDateTime($datetime, self::DATE_FORMAT);
    }

    protected function formatDateTimeToStrTime(?DateTime $datetime): ?string
    {
        return $this->formatDateTime($datetime, self::TIME_FORMAT);
    }

    protected function formatDateTime(?DateTime $datetime, string $format): ?string
    {
        if ($datetime === null) {
            return null;
        }

        return $datetime->format($format);
    }

    /**
     * @param mixed $field
     */
    protected function getFieldValue(string $field_name, $field)
    {
        return $field;
    }
}
