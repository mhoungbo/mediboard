<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Spec;

use DateTime;
use Ox\Core\CAppUI;
use Ox\Core\Specification\AndX;
use Ox\Core\Specification\Enum;
use Ox\Core\Specification\GreaterThanOrEqual;
use Ox\Core\Specification\InstanceOfX;
use Ox\Core\Specification\IsNull;
use Ox\Core\Specification\LessThanOrEqual;
use Ox\Core\Specification\MaxLength;
use Ox\Core\Specification\MinLength;
use Ox\Core\Specification\NotNull;
use Ox\Core\Specification\OrX;
use Ox\Core\Specification\SpecificationInterface;
use Ox\Core\Specification\SpecMatch;

/**
 * Specs builder for import
 */
trait SpecBuilderTrait
{
    public function getStringSpec(string $field_name): SpecificationInterface
    {
        return MaxLength::is($field_name, 255);
    }

    public function getNotNullSpec(string $field_name): SpecificationInterface
    {
        return NotNull::is($field_name);
    }

    public function getStringNotNullSpec(string $field_name): SpecificationInterface
    {
        return new AndX(
            NotNull::is($field_name),
            MaxLength::is($field_name, 255)
        );
    }

    public function getProfessionSpec(string $field_name): SpecificationInterface
    {
        return new OrX(
            MaxLength::is($field_name, 255),
            IsNull::is($field_name)
        );
    }

    public function getNameSpec(string $field_name, bool $mandatory = false): SpecificationInterface
    {
        if ($mandatory) {
            return new AndX(
                NotNull::is($field_name),
                MaxLength::is($field_name, 255)
            );
        }

        return new AndX(
            MaxLength::is($field_name, 255)
        );
    }

    public function getEmailSpec(string $field_name): SpecificationInterface
    {
        return new OrX(
            IsNull::is($field_name),
            new AndX(
                SpecMatch::is($field_name, '/^[-a-z0-9\._\+]+@[-a-z0-9\.]+\.[a-z]{2,4}$/i'),
                MaxLength::is($field_name, 255)
            )
        );
    }

    public function getAdresseSpec(string $field_name): ?SpecificationInterface
    {
        return (CAppUI::gconf(self::CONF_ADRESSE)) ? NotNull::is($field_name) : null;
    }

    public function getVilleSpec(string $field_name): ?SpecificationInterface
    {
        if (CAppUI::gconf(self::CONF_ADRESSE)) {
            return new AndX(
                NotNull::is($field_name),
                MaxLength::is($field_name, 80)
            );
        }

        return null;
    }

    public function getTelSpec(string $field_name): SpecificationInterface
    {
        $tel_spec = SpecMatch::is($field_name, '/^\d?(\d{2}[\s\.\-]?){5}$/');

        return new OrX(isNull::is($field_name), $tel_spec);
    }

    private function getCpSpec(string $field_name): SpecificationInterface
    {
        return new OrX(
            new AndX(
                MaxLength::is($field_name, 5),
                MinLength::is($field_name, 5)
            ),
            IsNull::is($field_name)
        );
    }

    public function getPaysSpec(string $field_name): ?SpecificationInterface
    {
        if (CAppUI::gconf(self::CONF_ADRESSE)) {
            return new AndX(
                NotNull::is($field_name),
                MaxLength::is($field_name, 80)
            );
        }

        return null;
    }

    private function getNaissanceSpec(string $spec_name, bool $mandatory = false): SpecificationInterface
    {
        if ($mandatory) {
            return new AndX(
                NotNull::is($spec_name),
                LessThanOrEqual::is($spec_name, new DateTime()),
                GreaterThanOrEqual::is($spec_name, new DateTime('1850-01-01')),
                InstanceOfX::is($spec_name, DateTime::class)
            );
        }

        return new OrX(
            new AndX(
                LessThanOrEqual::is($spec_name, new DateTime()),
                GreaterThanOrEqual::is($spec_name, new DateTime('1850-01-01')),
                InstanceOfX::is($spec_name, DateTime::class)
            ),
            IsNull::is($spec_name)
        );
    }

    public function getMatriculeSpec(string $field_name): ?SpecificationInterface
    {
        return new OrX(
            SpecMatch::is($field_name, '/^\d{13,15}$/'),
            IsNull::is($field_name)
        );
    }

    public function getCiviliteSpec(string $field_name): ?SpecificationInterface
    {
        return new OrX(
            Enum::is($field_name, ['m', 'mme', 'mlle', 'enf', 'dr', 'pr', 'me', 'vve']),
            IsNull::is($field_name)
        );
    }

    public function getSexeSpec(string $field_name): ?SpecificationInterface
    {
        return new OrX(
            Enum::is($field_name, ['m', 'f', 'u']),
            IsNull::is($field_name)
        );
    }
}
