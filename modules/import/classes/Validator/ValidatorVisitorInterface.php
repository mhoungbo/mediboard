<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Validator;

use Ox\Core\Specification\SpecificationViolation;
use Ox\Import\Framework\Entity\ActeCCAM;
use Ox\Import\Framework\Entity\ActeNGAP;
use Ox\Import\Framework\Entity\Affectation;
use Ox\Import\Framework\Entity\Antecedent;
use Ox\Import\Framework\Entity\Constante;
use Ox\Import\Framework\Entity\Consultation;
use Ox\Import\Framework\Entity\ConsultationAnesth;
use Ox\Import\Framework\Entity\Correspondant;
use Ox\Import\Framework\Entity\DossierMedical;
use Ox\Import\Framework\Entity\EvenementPatient;
use Ox\Import\Framework\Entity\File;
use Ox\Import\Framework\Entity\Injection;
use Ox\Import\Framework\Entity\Medecin;
use Ox\Import\Framework\Entity\Operation;
use Ox\Import\Framework\Entity\Patient;
use Ox\Import\Framework\Entity\PlageConsult;
use Ox\Import\Framework\Entity\Sejour;
use Ox\Import\Framework\Entity\Traitement;
use Ox\Import\Framework\Entity\User;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationAbnormalFlag;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationExam;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationFile;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationIdentifier;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationPatient;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationResponsible;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationResult;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationResultSet;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationResultValue;
use Ox\Mediboard\OxLaboServer\Import\Entity\ObservationValueUnit;

/**
 * Validate external entity
 */
interface ValidatorVisitorInterface
{
    public function validateUser(User $user): ?SpecificationViolation;

    public function validatePatient(Patient $patient): ?SpecificationViolation;

    public function validateMedecin(Medecin $medecin): ?SpecificationViolation;

    public function validatePlageConsult(PlageConsult $plage_consult): ?SpecificationViolation;

    public function validateConsultation(Consultation $consultation): ?SpecificationViolation;

    public function validateConsultationAnesth(ConsultationAnesth $consultation): ?SpecificationViolation;

    public function validateSejour(Sejour $sejour): ?SpecificationViolation;

    public function validateFile(File $file): ?SpecificationViolation;

    public function validateAffectation(Affectation $affectation): ?SpecificationViolation;

    public function validateAntecedent(Antecedent $antecedent): ?SpecificationViolation;

    public function validateTraitement(Traitement $traitement): ?SpecificationViolation;

    public function validateCorrespondant(Correspondant $correspondant): ?SpecificationViolation;

    public function validateEvenementPatient(EvenementPatient $evenement_patient): ?SpecificationViolation;

    public function validateInjection(Injection $injection): ?SpecificationViolation;

    public function validateActeCCAM(ActeCCAM $acte_ccam): ?SpecificationViolation;

    public function validateActeNGAP(ActeNGAP $acte_ngap): ?SpecificationViolation;

    public function validateConstante(Constante $constante): ?SpecificationViolation;

    public function validateDossierMedical(DossierMedical $dossier_medical): ?SpecificationViolation;

    public function validateOperation(Operation $operation): ?SpecificationViolation;

    public function validateObservationResult(ObservationResult $observation_result): ?SpecificationViolation;

    public function validateObservationIdentifier(ObservationIdentifier $observation_identifier
    ): ?SpecificationViolation;

    public function validateObservationResultValue(ObservationResultValue $observation_result_value
    ): ?SpecificationViolation;

    public function validateObservationResultSet(ObservationResultSet $observation_result_set): ?SpecificationViolation;

    public function validateObservationAbnormalFlag(ObservationAbnormalFlag $observation_flag): ?SpecificationViolation;

    public function validateObservationValueUnit(ObservationValueUnit $observation_value_unit): ?SpecificationViolation;

    public function validateObservationFile(ObservationFile $observation_file): ?SpecificationViolation;

    public function validateObservationResponsible(ObservationResponsible $observation_responsible
    ): ?SpecificationViolation;

    public function validateObservationExam(ObservationExam $observation_exam): ?SpecificationViolation;

    public function validateObservationPatient(ObservationPatient $observation_patient): ?SpecificationViolation;
}
