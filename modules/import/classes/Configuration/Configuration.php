<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Configuration;

use ArrayAccess;
use ReturnTypeWillChange;

class Configuration implements ArrayAccess
{
    private array $keys   = [];
    private array $values = [];

    /**
     * Configuration constructor.
     */
    public function __construct(array $values = [])
    {
        foreach ($values as $_key => $_value) {
            $this->offsetSet($_key, $_value);
        }
    }

    /**
     * @param mixed $offset
     */
    public function offsetExists($offset): bool
    {
        return (isset($this->keys[$offset]));
    }

    /**
     * @param mixed $offset
     *
     * @return mixed
     */
    #[ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            return null;
        }

        return $this->values[$offset];
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value): void
    {
        $this->keys[$offset]   = true;
        $this->values[$offset] = $value;
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset): void
    {
        if ($this->offsetExists($offset)) {
            unset($this->keys[$offset], $this->values[$offset]);
        }
    }
}
