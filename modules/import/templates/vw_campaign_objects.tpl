{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    getForm('search-campaign-objects').onsubmit();
  });
</script>

<form name="search-campaign-objects" method="get"
      onsubmit="return onSubmitFormAjax(this, null, 'result-campaign-objects')">
    {{mb_route name=import_gui_objects_list}}

  <table class="main form">
    <tr>
      <th>{{tr}}CImportCampaign{{/tr}}</th>
      <td>
        <select name="campaign_id" onchange="this.form.onsubmit();">
            {{foreach from=$all_campaign item=_campaign}}
              <option value="{{$_campaign->_id}}"
                      {{if $campaign->_id && $_campaign->_id === $campaign->_id}}selected{{/if}}>
                  {{$_campaign}}
              </option>
            {{/foreach}}
        </select>
      </td>

      <th>Affichage</th>
      <td>
        <select name="show_errors" onchange="this.form.onsubmit();">
          <option value="all" {{if $show_errors === 'all'}}selected{{/if}}>
              {{tr}}CImportEntity-label-All{{/tr}}
          </option>

          <option value="valid" {{if $show_errors === 'valid'}}selected{{/if}}>
              {{tr}}CImportEntity-label-Imported without error|pl{{/tr}}
          </option>

          <option value="with_errors" {{if $show_errors === 'with_errors'}}selected{{/if}}>
              {{tr}}CImportEntity-label-Imported with error|pl{{/tr}}
          </option>

          <option value="error" {{if $show_errors === 'error'}}selected{{/if}}>
              {{tr}}CImportEntity-label-Only in error|pl{{/tr}}
          </option>
        </select>
      </td>
    </tr>
  </table>
</form>

<div id="result-campaign-objects"></div>
