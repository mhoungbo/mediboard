{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<form name="edit-import-campaign" method="post"
      onsubmit="return onSubmitFormAjax(this, {onComplete: function () {Control.Modal.close();}})">
    {{csrf_token id=import_gui_campaigns_do_edit}}
    {{mb_route name=import_gui_campaigns_do_edit}}
    {{mb_key   object=$campaign}}
  <input type="hidden" name="del" value="0"/>

  <table class="main form">

      {{mb_include module=system template=inc_form_table_header object=$campaign}}

    <tr>
      <th>{{mb_title object=$campaign field=name}}</th>
      <th>{{mb_field object=$campaign field=name}}</th>
    </tr>

      {{if $campaign->_id}}
        <tr>
          <th>{{mb_title object=$campaign field=creation_date}}</th>
          <th>{{mb_field object=$campaign field=creation_date register=true form='edit-import-campaign'}}</th>
        </tr>
        <tr>
          <th>{{mb_title object=$campaign field=closing_date}}</th>
          <th>{{mb_field object=$campaign field=closing_date register=true form='edit-import-campaign'}}</th>
        </tr>
      {{/if}}

    <tr>
      <td colspan="2" class="button">
        <button type="submit" class="save">{{tr}}Save{{/tr}}</button>
          {{if $campaign->_id}}
            <button class="trash" type="button" onclick="ImportCampaign.confirmCampaignDeletion(this.form);">
                {{tr}}Delete{{/tr}}
            </button>
          {{/if}}
      </td>
    </tr>
  </table>
</form>
