<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Tests\Unit\Strategy;

use Exception;
use Ox\Core\CPDOMySQLDataSource;
use Ox\Core\CSQLDataSource;
use Ox\Import\Framework\Adapter\MySqlAdapter;
use Ox\Import\Framework\Entity\CImportCampaign;
use Ox\Import\Framework\Entity\Medecin;
use Ox\Import\Framework\Mapper\MapperBuilderInterface;
use Ox\Import\Framework\Mapper\MapperInterface;
use Ox\Import\Framework\Matcher\DefaultMatcher;
use Ox\Import\Framework\Persister\DefaultPersister;
use Ox\Import\Framework\Repository\GenericRepository;
use Ox\Import\Framework\Strategy\BFSStrategy;
use Ox\Import\Framework\Tests\Unit\GeneratorEntityTrait;
use Ox\Import\Framework\Transformer\DefaultTransformer;
use Ox\Import\Framework\Validator\DefaultValidator;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use PHPUnit\Framework\ExpectationFailedException;
use ReflectionException;
use SebastianBergmann\RecursionContext\InvalidArgumentException;

class BFSStrategyTest extends OxUnitTestCase
{
    use GeneratorEntityTrait;

    /*** @var BFSStrategy */
    private $strategy;

    /*** @var MapperBuilderInterface */
    private $mapper;

    /*** @var GenericRepository */
    private $repository;

    /*** @var DefaultValidator */
    private $validator;

    /*** @var DefaultTransformer */
    private $transformer;

    /*** @var DefaultMatcher */
    private $matcher;

    /*** @var DefaultPersister */
    private $persister;

    /*** @var CImportCampaign */
    private $campaign;

    /*** @var MySqlAdapter */
    private $adapter;

    /*** @var CSQLDataSource */
    private $ds;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        $this->ds          = new CPDOMySQLDataSource();
        $this->adapter     = new MySqlAdapter($this->ds);
        $this->mapper      = $this->createMock(MapperInterface::class);
        $this->repository  = $this->createMock(GenericRepository::class);
        $this->validator   = new DefaultValidator();
        $this->transformer = new DefaultTransformer();
        $this->matcher     = new DefaultMatcher();
        $this->persister   = new DefaultPersister();

        $this->campaign       = new CImportCampaign();
        $this->campaign->name = uniqid();
        $this->campaign->store();

        $this->strategy = new BFSStrategy(
            $this->repository,
            $this->validator,
            $this->transformer,
            $this->matcher,
            $this->persister,
            $this->campaign
        );
    }

    /**
     * @throws TestsException
     * @throws ReflectionException
     */
    public function testImportOneUser(): void
    {
        $user = $this->generateExternalUser();

        $user_after = $this->invokePrivateMethod($this->strategy, 'importOne', $user);

        $this->assertInstanceOf(CUser::class, $user_after);
        $this->assertNotNull($user_after->_id);
    }

    /**
     * @throws TestsException
     * @throws ExpectationFailedException
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function testImportOneMedecin(): void
    {
        /**@var  Medecin $medecin */
        $medecin = $this->generateExternalMedecin();

        $medecin_after = $this->invokePrivateMethod($this->strategy, 'importOne', $medecin);

        $this->assertInstanceOf(CMedecin::class, $medecin_after);
        $this->assertNotNull($medecin_after->_id);
    }
}
