<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Tests\Functional;

use Exception;
use Ox\Core\Locales\Translator;
use Ox\Import\Framework\Entity\CImportCampaign;
use Ox\Tests\OxWebTestCase;

class ImportedObjectsControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testViewList(): void
    {
        $campaign = $this->createImportCampaign();

        $crawler = self::createClient()->request('GET', '/gui/import/objects/list', [
            'campaign_id' => $campaign->_id,
        ]);

        $this->deleteOrFailed($campaign);
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(
            (new Translator())->tr('CImportEntity-msg-no results for this campaign'),
            $crawler->filterXPath('//ul[@id="tab-list-classes"]')->text()
        );
    }

    private function createImportCampaign(): CImportCampaign
    {
        $campaign       = new CImportCampaign();
        $campaign->name = 'Lorem ipsum campaign';
        $this->storeOrFailed($campaign);

        return $campaign;
    }
}
