<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Framework\Tests\Functional;

use Exception;
use Ox\Core\Locales\Translator;
use Ox\Import\Framework\Entity\CImportCampaign;
use Ox\Tests\OxWebTestCase;

class ImportCampaignsControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testViewList(): CImportCampaign
    {
        $campaign = $this->createImportCampaign();

        $crawler = self::createClient()->request('GET', '/gui/import/campaigns/list');

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(
            'Lorem ipsum campaign',
            $crawler->filterXPath('//table[contains(@class, "main")]/tr[last()]//td[2]')->text()
        );

        return $campaign;
    }

    /**
     * @depends testViewList
     * @throws Exception
     */
    public function testViewEditCampaign(CImportCampaign $campaign): CImportCampaign
    {
        $crawler = self::createClient()->request('GET', '/gui/import/campaigns/edit', [
            'import_campaign_id' => $campaign->_id,
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(
            'Lorem ipsum campaign',
            $crawler->filterXPath('//input[@name="name"]')->attr('value')
        );

        return $campaign;
    }

    /**
     * @depends testViewEditCampaign
     * @throws Exception
     */
    public function testDoEditCampaign(CImportCampaign $campaign): CImportCampaign
    {
        $client = self::createClient();
        $client->request('POST', '/gui/import/campaigns/edit', [
            'import_campaign_id' => $campaign->_id,
            'name'               => 'Lorem ipsum campaign edited',
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString(
            (new Translator())->tr('CImportCampaign-msg-modify'),
            html_entity_decode($client->getResponse()->getContent())
        );

        return $campaign;
    }

    /**
     * @depends testDoEditCampaign
     * @throws Exception
     */
    public function testDeleteCampaign(CImportCampaign $campaign): CImportCampaign
    {
        $client = self::createClient();
        $client->request('POST', '/gui/import/campaigns/edit', [
            'import_campaign_id' => $campaign->_id,
            'del'                => 1,
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString(
            (new Translator())->tr('CImportCampaign-msg-delete'),
            html_entity_decode($client->getResponse()->getContent())
        );

        return $campaign;
    }

    private function createImportCampaign(): CImportCampaign
    {
        $campaign       = new CImportCampaign();
        $campaign->name = 'Lorem ipsum campaign';
        $this->storeOrFailed($campaign);

        return $campaign;
    }
}
