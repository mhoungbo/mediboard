/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ImportCampaign = window.ImportCampaign || {
  showCreateCampaign:      function (url_show_create) {
    new Url('import', 'import_gui_campaigns_view_edit_create').setRoute(url_show_create)
      .requestModal('30%', '50%', {
        onClose: function () {
          getForm('search-import-campaign').onsubmit();
        }
      });
  },
  editCampaign:            function (import_campaign_id, url_edit) {
    new Url('import', 'import_gui_campaigns_view_edit').setRoute(url_edit)
      .addParam('import_campaign_id', import_campaign_id)
      .requestModal('30%', '50%', {
        onClose: function () {
          getForm('search-import-campaign').onsubmit();
        }
      });
  },
  confirmCampaignDeletion: function (form) {
    confirmDeletion(form, {
      typeName: 'campagne d\'importation',
      objName:  $V(form.import_campaign_id),
      ajax:     1
    }, {
      onComplete: function () {
        Control.Modal.close();
      }
    });
  },
  loadObjectTab:           function (container, start = 0) {
    new Url().setRoute(container.get('url_list'))
      .addParam('class_name', container.get('classe'))
      .addParam('import_campaign_id', container.get('campaign'))
      .addParam('show_errors', container.get('show_errors'))
      .addParam('start', start)
      .requestUpdate(container.id);
  },
  changeObjectPage:        function (page, elem_id) {
    ImportCampaign.loadObjectTab($(elem_id), page);
  },
  resetEntities:           function (type, url_reset, token) {
    if (confirm($T('CImportEntity-Ask-Delete all entities for type', type))) {
      Url().setRoute(url_reset)
        .addParam('type', type)
        .addParam('token', token)
        .requestUpdate('systemMsg', {
          method: 'post', onComplete: function () {
            getForm('search-campaign-objects').onsubmit()
          }
        });
    }
  },
  refreshCampaign:         function (module, import_campaign_id) {
    new Url(module, 'vw_import_fw')
      .addParam('import_campaign_id', import_campaign_id)
      .requestUpdate('vw_import_fw');
  },
  exportEntities:          function (class_name, show_errors, start, step, url_export) {
    new Url().setRoute(url_export)
      .addParam('class_name', class_name)
      .addParam('show_errors', show_errors)
      .addParam('start', start)
      .addParam('step', step)
      .popup(400, 150);
  }
};
