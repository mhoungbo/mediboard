<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Tests;

use Exception;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphAxis;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

class CSupervisionGraphAxisTest extends OxUnitTestCase
{
    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testGetSymbolCharReturnsNull(): void
    {
        /** @var CSupervisionGraphAxis $graph_axe */
        $graph_axe = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE_2
        );

        $this->assertNull($graph_axe->getSymbolChar());
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testGetSymbolCharReturnsNotNull(): void
    {
        /** @var CSupervisionGraphAxis $graph_axe */
        $graph_axe = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE
        );

        $this->assertNotNull($graph_axe->getSymbolChar());
    }

    /**
     * @param CSupervisionGraphAxis $graph_axis
     * @param array                 $expected_keys
     * @param bool                  $check_min_max
     *
     * @return void
     * @throws Exception
     * @dataProvider getAxisForFlotProvider
     */
    public function testGetAxisForFlotReturnsExpectedData(
        CSupervisionGraphAxis $graph_axis,
        array $expected_keys,
        bool $check_min_max = false
    ): void {
        $actual = $graph_axis->getAxisForFlot(1);
        foreach ($expected_keys as $key) {
            $this->assertArrayHasKey($key, $actual);
        }
        if ($check_min_max) {
            $this->assertNotNull($actual["min"]);
            $this->assertNotNull($actual["max"]);
        }
    }

    /**
     * @throws TestsException
     */
    public function getAxisForFlotProvider(): array
    {
        $graph_axe   = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE
        );
        $graph_axe_2 = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE_2
        );

        return [
            "graph_axe_1" => [$graph_axe, ["ticks", "alignTicksWithAxis"]],
            "graph_axe_2" => [$graph_axe_2, ["ticks", "alignTicksWithAxis", "min", "max"], true],
        ];
    }
}
