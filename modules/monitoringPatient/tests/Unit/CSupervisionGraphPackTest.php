<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Tests;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphPack;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

class CSupervisionGraphPackTest extends OxUnitTestCase
{
    protected CStoredObject|CSupervisionGraphPack $graph_pack;

    protected CStoredObject|CSupervisionGraphPack $graph_pack_table;

    protected CStoredObject|CSupervisionGraphPack $graph_pack_without_context_timings;

    /**
     * @throws TestsException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->graph_pack = $this->getObjectFromFixturesReference(
            CSupervisionGraphPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_PACK
        );

        $this->graph_pack_without_context_timings = $this->getObjectFromFixturesReference(
            CSupervisionGraphPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_PACK_2
        );

        $this->graph_pack_table = $this->getObjectFromFixturesReference(
            CSupervisionGraphPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_PACK_3
        );
    }

    public function testCanUseInContextReturnsTrue(): void
    {
        $this->assertTrue($this->graph_pack->canUseInContext("preop"));
        $this->assertTrue($this->graph_pack_without_context_timings->canUseInContext("preop"));
    }

    public function testGetTimingFieldsContainsEntreeSalle(): void
    {
        $actual = $this->graph_pack->getTimingFields();

        $this->assertArrayHasKey("entree_salle", $actual);
    }

    public function testGetTimingFieldsReturnsEmptyArray(): void
    {
        $actual = $this->graph_pack_without_context_timings->getTimingFields();

        $this->assertEmpty($actual);
    }

    public function testGetTimingValuesReturnNotEmptyArray(): void
    {
        $actual = $this->graph_pack->getTimingValues(new COperation());

        $this->assertNotEmpty($actual);
    }

    public function testGetTimingValuesReturnEmptyArray(): void
    {
        $actual = $this->graph_pack_without_context_timings->getTimingValues(new COperation());

        $this->assertEmpty($actual);
    }

    /**
     * @throws Exception
     */
    public function testIsProtocolMDStreamReturnFalse(): void
    {
        $this->assertFalse($this->graph_pack_without_context_timings->isProtocolMDStream());
    }

    /**
     * @throws Exception
     */
    public function testIsProtocolMDStreamReturnTrue(): void
    {
        $this->assertTrue($this->graph_pack->isProtocolMDStream());
        $this->assertTrue($this->graph_pack_table->isProtocolMDStream());
    }
}
