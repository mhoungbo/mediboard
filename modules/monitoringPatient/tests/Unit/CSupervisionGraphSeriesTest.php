<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Tests;

use Exception;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphSeries;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

class CSupervisionGraphSeriesTest extends OxUnitTestCase
{
    /**
     * @param CSupervisionGraphSeries $graph_series
     * @param string                  $expected
     *
     * @return void
     * @throws Exception
     * @dataProvider initGraphSeriesProvider
     */
    public function testInitGraphSeriesDataContainsExpectedKey(CSupervisionGraphSeries $graph_series, string $expected)
    {
        $actual = $graph_series->initSeriesData(1);
        $this->assertArrayHasKey($expected, $actual);
    }

    /**
     * @throws TestsException
     */
    public function initGraphSeriesProvider()
    {
        /** @var CSupervisionGraphSeries $graph_series */
        $graph_series = $this->getObjectFromFixturesReference(
            CSupervisionGraphSeries::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_SERIES
        );

        /** @var CSupervisionGraphSeries $graph_series_2 */
        $graph_series_2 = $this->getObjectFromFixturesReference(
            CSupervisionGraphSeries::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_SERIES_2
        );

        /** @var CSupervisionGraphSeries $graph_series_3 */
        $graph_series_3 = $this->getObjectFromFixturesReference(
            CSupervisionGraphSeries::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_SERIES_3
        );

        return [
            "points"    => [$graph_series, "points"],
            "stack"     => [$graph_series_2, "stack"],
            "bandwidth" => [$graph_series_3, "bandwidth"],
        ];
    }
}
