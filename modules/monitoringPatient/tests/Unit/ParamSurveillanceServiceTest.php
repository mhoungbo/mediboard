<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Tests;

use Ox\Core\CMbException;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\MonitoringPatient\ParamSurveillanceService;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

class ParamSurveillanceServiceTest extends OxUnitTestCase
{
    /**
     * @throws CMbException
     */
    public function testServiceThrowGroupIdIsNullException()
    {
        $this->expectExceptionMessage("ParamSurveillance-error-group id is null");
        new ParamSurveillanceService(new CGroups());
    }

    /**
     * @throws CMbException
     * @throws CModelObjectException
     * @throws TestsException
     */
    public function testServiceThrowStartNotValidException()
    {
        /** @var CGroups $group */
        $group = $this->getObjectFromFixturesReference(
            CGroups::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GROUP
        );
        $start = -1;
        $this->expectExceptionMessage("ParamSurveillance-error-start not valid");
        new ParamSurveillanceService($group, $start);
    }
}
