<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Tests;

use Ox\Core\CStoredObject;
use Ox\Mediboard\MonitoringPatient\CSupervisionTimedData;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

class CSupervisionTimedDataTest extends OxUnitTestCase
{
    private CStoredObject|CSupervisionTimedData $timed_data;

    private CStoredObject|CSupervisionTimedData $timed_data_enum;

    /**
     * @throws TestsException
     */
    public function loadTimedDataResultsProvider()
    {
        /** @var CSupervisionTimedData $timed_data */
        $timed_data = $this->getObjectFromFixturesReference(
            CSupervisionTimedData::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TIMED_DATA
        );
        return [
            "empty results"                    => [[]],
            "empty results with value type id" => [[$timed_data->value_type_id => []]],
            "value type id"                    => [[$timed_data->value_type_id => ["none" => []]]],
        ];
    }

    /**
     * @throws TestsException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->timed_data = $this->getObjectFromFixturesReference(
            CSupervisionTimedData::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TIMED_DATA
        );

        $this->timed_data_enum = $this->getObjectFromFixturesReference(
            CSupervisionTimedData::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TIMED_DATA_ENUM
        );
    }

    public function testMakeItemsReturnNull(): void
    {
        $this->assertNull($this->timed_data->makeItems());
    }

    public function testMakeItemsReturnArray(): void
    {
        $expected = ["Lorem", "Ipsum", "Dolor"];
        $this->assertEquals($expected, $this->timed_data_enum->makeItems());
    }

    /**
     * @param array $results
     *
     * @return void
     * @dataProvider loadTimedDataResultsProvider
     */
    public function testLoadTimedDataReturnsEmptyArray(array $results): void
    {
        $this->assertEmpty($this->timed_data->loadTimedData($results));
    }
}
