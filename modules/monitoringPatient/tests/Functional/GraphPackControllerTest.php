<?php

namespace Ox\Mediboard\MonitoringPatient\Tests\Functional;

use Exception;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphPack;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphToPack;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GraphPackControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testEditGraphPack(): void
    {
        $client = self::createClient();

        $graph_pack = $this->getObjectFromFixturesReference(
            CSupervisionGraphPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_PACK
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/packs/edit',
            [
                "supervision_graph_pack_id" => $graph_pack->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_graph_pack_id' and @value='$graph_pack->_id']"
            )
        );
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testListGraphToPack(): void
    {
        $client = self::createClient();

        $graph_pack = $this->getObjectFromFixturesReference(
            CSupervisionGraphPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_PACK
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/packs/' . $graph_pack->_id . '/graph_to_pack/list',
            [
                "supervision_graph_pack_id" => $graph_pack->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//button[@class='edit compact notext me-tertiary']"
            )
        );
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testEditGraphToPack(): void
    {
        $client = self::createClient();

        $graph_pack = $this->getObjectFromFixturesReference(
            CSupervisionGraphPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_PACK
        );

        $graph_to_pack = $this->getObjectFromFixturesReference(
            CSupervisionGraphToPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_TO_PACK
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/packs/' . $graph_pack->_id . '/graph_to_pack/edit',
            [
                "supervision_graph_pack_id" => $graph_pack->_id,
                "supervision_graph_to_pack_id" => $graph_to_pack->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_graph_to_pack_id' and @value='$graph_to_pack->_id']"
            )
        );
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testEditNewGraphToPack(): void
    {
        $client = self::createClient();

        $graph_pack = $this->getObjectFromFixturesReference(
            CSupervisionGraphPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_PACK
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/packs/' . $graph_pack->_id . '/graph_to_pack/edit',
            [
                "supervision_graph_pack_id" => $graph_pack->_id,
                "graph_class"               => "CSupervisionGraph",
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_graph_to_pack_id' and @value='']"
            )
        );
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testChangeGraphToPackRank(): void
    {
        $client = self::createClient();

        $graph_to_pack = $this->getObjectFromFixturesReference(
            CSupervisionGraphToPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_TO_PACK
        );

        $client->request(
            Request::METHOD_POST,
            '/gui/monitoringPatient/graph_to_pack/change_rank',
            [
                "supervision_graph_to_pack_id" => $graph_to_pack->_id,
                "rank"                         => 1,
                "wish_rank"                    => 2,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testChangeGraphToPackRankWithoutWishRank(): void
    {
        $client = self::createClient();

        $graph_to_pack = $this->getObjectFromFixturesReference(
            CSupervisionGraphToPack::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_TO_PACK
        );

        $client->request(
            Request::METHOD_POST,
            '/gui/monitoringPatient/graph_to_pack/change_rank',
            [
                "supervision_graph_to_pack_id" => $graph_to_pack->_id,
                "rank"                         => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
