<?php

namespace Ox\Mediboard\MonitoringPatient\Tests\Functional;

use Exception;
use Ox\Mediboard\MonitoringPatient\CSupervisionTimedData;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TimedElementsControllerTest extends OxWebTestCase
{
    /**
     * @return void
     * @throws TestsException
     * @throws Exception
     */
    public function testEditTimedData(): void
    {
        $client = self::createClient();

        $timed_data = $this->getObjectFromFixturesReference(
            CSupervisionTimedData::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TIMED_DATA
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/timed_data/edit',
            [
                "supervision_timed_data_id" => $timed_data->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_timed_data_id' and @value='$timed_data->_id']"
            )
        );
    }


    /**
     * @return void
     * @throws Exception
     */
    public function testEditTimedPicture(): void
    {
        $client = self::createClient();

        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/timed_picture/edit',
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testviewSupervisionPictures(): void
    {
        $client = self::createClient();

        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/timed_picture/pictures/list',
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testEditInstantData(): void
    {
        $client = self::createClient();

        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/instant_data/edit',
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testSelectPicture(): void
    {
        $client = self::createClient();

        $client->request(
            Request::METHOD_POST,
            '/gui/monitoringPatient/timed_picture/pictures/select',
            [
                "path" => "/lorem/ipsum"
            ]
        );
        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }
}
