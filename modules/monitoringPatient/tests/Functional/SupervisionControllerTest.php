<?php

namespace Ox\Mediboard\MonitoringPatient\Tests\Functional;

use Exception;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraph;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphAxis;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SupervisionControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testSupervisionIndex(): void
    {
        $client = self::createClient();

        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/index',
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testSupervisionList(): void
    {
        $client = self::createClient();

        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/list',
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    public function testEditSupervisionGraph(): void
    {
        $client = self::createClient();

        /** @var CSupervisionGraph $graph */
        $graph = $this->getObjectFromFixturesReference(
            CSupervisionGraph::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/graph/edit',
            [
                "supervision_graph_id" => $graph->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_graph_id' and @value='$graph->_id']"
            )
        );
    }

    /**
     * @throws Exception
     */
    public function testEditSupervisionGraphNewGraph(): void
    {
        $client = self::createClient();

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/graph/edit'
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_graph_id' and @value='']"
            )
        );
    }

    /**
     * @throws Exception
     */
    public function testListGraphAxes(): void
    {
        $client = self::createClient();

        /** @var CSupervisionGraph $graph */
        $graph = $this->getObjectFromFixturesReference(
            CSupervisionGraph::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH
        );

        /** @var CSupervisionGraph $graph_axis */
        $graph_axis = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/graph/' . $graph->_id . '/axes/list',
            [
                "supervision_graph_id" => $graph->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//tr[@data-axis_id='$graph_axis->_id']"
            )
        );
    }

    /**
     * @throws Exception
     */
    public function testEditGraphAxisNewAxis(): void
    {
        $client = self::createClient();

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/axes/edit'
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_graph_axis_id' and @value='']"
            )
        );
    }

    /**
     * @throws Exception
     */
    public function testListGraphAxisSeries(): void
    {
        $client = self::createClient();

        /** @var CSupervisionGraph $graph_axis */
        $graph_axis = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/axes/' . $graph_axis->_id . '/series/list',
            [
                "supervision_graph_axis_id" => $graph_axis->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            0,
            $crawler->filterXPath(
                "//td[@colspan='5' and @class='empty']"
            )
        );
    }

    /**
     * @throws Exception
     */
    public function testEditGraphAxisSeries(): void
    {
        $client = self::createClient();

        /** @var CSupervisionGraph $graph_axis */
        $graph_axis = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/axes/' . $graph_axis->_id . '/series/edit',
            [
                "supervision_graph_axis_id" => $graph_axis->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_graph_series_id' and @value='']"
            )
        );
    }

    /**
     * @throws Exception
     */
    public function testListAxisLabels(): void
    {
        $client = self::createClient();

        /** @var CSupervisionGraph $graph_axis */
        $graph_axis = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE
        );

        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/axes/' . $graph_axis->_id . '/labels/list',
            [
                "supervision_graph_axis_id" => $graph_axis->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    public function testEditAxisLabel(): void
    {
        $client = self::createClient();

        /** @var CSupervisionGraph $graph_axis */
        $graph_axis = $this->getObjectFromFixturesReference(
            CSupervisionGraphAxis::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH_AXE
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/axes/' . $graph_axis->_id . '/labels/edit',
            [
                "supervision_graph_axis_id" => $graph_axis->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_graph_value_label_id' and @value='']"
            )
        );
    }

    /**
     * @throws Exception
     */
    public function testPreviewGraph(): void
    {
        $client = self::createClient();

        /** @var CSupervisionGraph $graph */
        $graph = $this->getObjectFromFixturesReference(
            CSupervisionGraph::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GRAPH
        );

        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/supervision/' . $graph->_id . '/preview',
            [
                "supervision_graph_id" => $graph->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
