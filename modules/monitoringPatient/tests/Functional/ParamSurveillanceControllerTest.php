<?php

namespace Ox\Mediboard\MonitoringPatient\Tests\Functional;

use Exception;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Mediboard\ObservationResult\CObservationValueToConstant;
use Ox\Mediboard\ObservationResult\CObservationValueUnit;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParamSurveillanceControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testIndex()
    {
        $client  = self::createClient();
        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/surveillance/index',
            []
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    public function testListObservationValueUnit()
    {
        $client = self::createClient();

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/surveillance/list',
            [
                'object_class' => "CObservationValueUnit",
                'start'        => 0,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertNotNull($crawler->filterXPath("//button[@class=\"edit notext compact\"]")->count());
    }

    /**
     * @throws Exception
     */
    public function testEditWithDefaultValue()
    {
        $client = self::createClient();

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/surveillance/edit',
            [
                'param_guid' => "CObservationValueUnit-0",
            ]
        );

        $this->assertEquals(1, $crawler->filterXPath("//input[@value=\"MB\"]")->count());
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    public function testEditWithFixturesObject()
    {
        $client = self::createClient();

        /** @var CObservationValueUnit $observation_value_unit */
        $observation_value_unit = $this->getObjectFromFixturesReference(
            CObservationValueUnit::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_OBSERVATION_VALUE_UNIT
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/surveillance/edit',
            [
                'param_guid' => $observation_value_unit->_guid,
            ]
        );

        $this->assertEquals(
            1,
            $crawler->filterXPath("//input[@value=\"$observation_value_unit->coding_system\"]")->count()
        );
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    public function testEditWithWrongObjectTypeThrowsException()
    {
        $client = self::createClient();

        $group = $this->getObjectFromFixturesReference(
            CGroups::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_GROUP
        );

        $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/surveillance/edit',
            [
                'param_guid' => $group->_guid,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @throws Exception
     */
    public function testEditConversionNewItem()
    {
        $client  = self::createClient();
        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/surveillance/edit_conversion',
            []
        );

        $this->assertNotNull($crawler->filterXPath("//input[@name=\"conversion_ratio\" and @value=\"1\"]")->count());
    }

    /**
     * @throws Exception
     */
    public function testEditConversionWithFixtureItem()
    {
        $client = self::createClient();

        /** @var CObservationValueToConstant $obs_value_to_constant */
        $obs_value_to_constant = $this->getObjectFromFixturesReference(
            CObservationValueToConstant::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_OBSERVATION_VALUE_TO_CONSTANT
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/surveillance/edit_conversion',
            [
                "observation_value_to_constant_id" => $obs_value_to_constant->_id,
            ]
        );

        $this->assertNotNull(
            $crawler->filterXPath(
                "//input[@name=\"conversion_ratio\" and @value=\"$obs_value_to_constant->conversion_ratio\"]"
            )->count()
        );
    }

    /**
     * @throws Exception
     */
    public function testListConversion()
    {
        $client  = self::createClient();
        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/surveillance/list_conversion',
            [
                "start" => 0,
            ]
        );

        $this->assertNotNull($crawler->filterXPath("//tr[@class=\"alternate\"]")->count());
    }
}
