<?php

namespace Ox\Mediboard\MonitoringPatient\Tests\Functional;

use Exception;
use Ox\Mediboard\MonitoringPatient\CSupervisionTable;
use Ox\Mediboard\MonitoringPatient\CSupervisionTableRow;
use Ox\Mediboard\MonitoringPatient\Tests\Fixtures\MonitoringPatientSettingFixtures;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SupervisionTableControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testEditTable(): void
    {
        $client = self::createClient();

        $supervision_table = $this->getObjectFromFixturesReference(
            CSupervisionTable::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TABLE
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/table/edit',
            [
                "supervision_table_id" => $supervision_table->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(1, $crawler->filterXPath("//td[@id='supervision-table-rows-list']"));
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testListeTableRows(): void
    {
        $client = self::createClient();

        $supervision_table = $this->getObjectFromFixturesReference(
            CSupervisionTable::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TABLE
        );

        $supervision_table_row = $this->getObjectFromFixturesReference(
            CSupervisionTableRow::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TABLE_ROW
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/table/' . $supervision_table->_id . '/rows/list',
            [
                "supervision_table_id" => $supervision_table->_id,
            ]
        );
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(1, $crawler->filterXPath("//tr[@data-axis_id='$supervision_table_row->_id']"));
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testEditNewTableRow(): void
    {
        $client = self::createClient();

        $supervision_table = $this->getObjectFromFixturesReference(
            CSupervisionTable::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TABLE
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/table/' . $supervision_table->_id . '/rows/edit',
            [
                "supervision_table_id" => $supervision_table->_id,
            ]
        );
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(1, $crawler->filterXPath("//input[@name='supervision_table_row_id' and @value='']"));
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testEditTableRow(): void
    {
        $client = self::createClient();

        $supervision_table = $this->getObjectFromFixturesReference(
            CSupervisionTable::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TABLE
        );

        $supervision_table_row = $this->getObjectFromFixturesReference(
            CSupervisionTableRow::class,
            MonitoringPatientSettingFixtures::MONITORING_PATIENT_TABLE_ROW
        );

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/monitoringPatient/table/' . $supervision_table->_id . '/rows/edit',
            [
                "supervision_table_id" => $supervision_table->_id,
                "supervision_table_row_id" => $supervision_table_row->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertCount(
            1,
            $crawler->filterXPath(
                "//input[@name='supervision_table_row_id' and @value='$supervision_table_row->_id']"
            )
        );
    }
}
