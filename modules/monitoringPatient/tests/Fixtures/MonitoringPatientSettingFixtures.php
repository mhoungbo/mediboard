<?php

namespace Ox\Mediboard\MonitoringPatient\Tests\Fixtures;

use Ox\Core\CModelObjectException;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraph;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphAxis;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphAxisValueLabel;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphPack;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphSeries;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphToPack;
use Ox\Mediboard\MonitoringPatient\CSupervisionTable;
use Ox\Mediboard\MonitoringPatient\CSupervisionTableRow;
use Ox\Mediboard\MonitoringPatient\CSupervisionTimedData;
use Ox\Mediboard\ObservationResult\CObservationValueToConstant;
use Ox\Mediboard\ObservationResult\CObservationValueType;
use Ox\Mediboard\ObservationResult\CObservationValueUnit;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

class MonitoringPatientSettingFixtures extends Fixtures
{
    public const MONITORING_PATIENT_GRAPH = "monitoring_supervision_graph";

    public const MONITORING_PATIENT_GRAPH_AXE = "monitoring_supervision_graph_axe";

    public const MONITORING_PATIENT_GRAPH_AXE_2 = "monitoring_supervision_graph_axe_2";

    public const MONITORING_PATIENT_GRAPH_AXE_3 = "monitoring_supervision_graph_axe_3";

    public const MONITORING_PATIENT_TABLE = "monitoring_supervision_table";

    public const MONITORING_PATIENT_TABLE_ROW = "monitoring_supervision_table_row";

    public const MONITORING_PATIENT_GRAPH_PACK = "monitoring_supervision_graph_pack";

    public const MONITORING_PATIENT_GRAPH_TO_PACK = "monitoring_supervision_graph_to_pack";

    public const MONITORING_PATIENT_GRAPH_TO_PACK_2 = "monitoring_supervision_graph_to_pack_2";

    public const MONITORING_PATIENT_GRAPH_PACK_2 = "monitoring_supervision_graph_pack_2";

    public const MONITORING_PATIENT_GRAPH_PACK_3 = "monitoring_supervision_graph_pack_3";

    public const MONITORING_PATIENT_TIMED_DATA = "monitoring_supervision_timed_data";

    public const MONITORING_PATIENT_TIMED_DATA_ENUM = "monitoring_supervision_timed_data_enum";

    public const MONITORING_PATIENT_GRAPH_SERIES = "monitoring_supervision_graph_series";

    public const MONITORING_PATIENT_GRAPH_SERIES_2 = "monitoring_supervision_graph_series_2";

    public const MONITORING_PATIENT_GRAPH_SERIES_3 = "monitoring_supervision_graph_series_3";

    public const MONITORING_PATIENT_GROUP = "monitoring_patient_group";

    public const MONITORING_PATIENT_OBSERVATION_VALUE_UNIT = "monitoring_patient_observation_value_unit";

    public const MONITORING_PATIENT_OBSERVATION_VALUE_TYPE = "monitoring_patient_observation_value_type";

    public const MONITORING_PATIENT_OBSERVATION_VALUE_TO_CONSTANT = "monitoring_patient_observation_value_to_constant";

    public CSupervisionGraphPack $pack;

    public CSupervisionGraphPack $pack_table;

    public CSupervisionGraphPack $empty_pack;

    public CSupervisionTimedData $timed_data;

    public CSupervisionTimedData $timed_data_enum;

    private CGroups $group;

    private CObservationValueUnit $obs_value_unit;

    private CObservationValueType $obs_value_type;

    private CObservationValueToConstant $obs_value_to_constant;

    private CSupervisionGraph $graph;

    private CSupervisionTable $table;

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function load(): void
    {
        $this->group = CGroups::getSampleObject();
        $this->store($this->group, self::MONITORING_PATIENT_GROUP);

        $this->obs_value_type                = CObservationValueType::getSampleObject();
        $this->obs_value_type->group_id      = $this->group->_id;
        $this->obs_value_type->coding_system = "MD-Stream";
        $this->obs_value_type->label         = "MDC_Lorem";
        $this->store($this->obs_value_type, self::MONITORING_PATIENT_OBSERVATION_VALUE_TYPE);

        $this->obs_value_unit                = CObservationValueUnit::getSampleObject();
        $this->obs_value_unit->group_id      = $this->group->_id;
        $this->obs_value_unit->coding_system = "MD-Stream";
        $this->obs_value_unit->label         = "MDC_Lorem";
        $this->store($this->obs_value_unit, self::MONITORING_PATIENT_OBSERVATION_VALUE_UNIT);

        $this->obs_value_to_constant                   = CObservationValueToConstant::getSampleObject();
        $this->obs_value_to_constant->value_unit_id    = $this->obs_value_unit->_id;
        $this->obs_value_to_constant->value_type_id    = $this->obs_value_type->_id;
        $this->obs_value_to_constant->conversion_ratio = 12;
        $this->store($this->obs_value_to_constant, self::MONITORING_PATIENT_OBSERVATION_VALUE_TO_CONSTANT);

        $this->createGraph();
        if ($this->graph->_id) {
            $this->createGraphAxis();
        }

        $this->createDataTable();
        $this->createGraphPack();
        $this->createGraphPackWithoutTimingAndContexts();
        $this->createGraphPackForTable();
        $this->linkGraphToPack($this->graph, $this->pack, self::MONITORING_PATIENT_GRAPH_TO_PACK);
        $this->linkGraphToPack($this->table, $this->pack_table, self::MONITORING_PATIENT_GRAPH_TO_PACK_2);
        $this->createTimedData();
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createGraph(): void
    {
        $this->graph                     = CSupervisionGraph::getSampleObject();
        $this->graph->title              = "Lorem Ipsum";
        $this->graph->height             = 20;
        $this->graph->owner_class        = $this->group->_class;
        $this->graph->owner_id           = $this->group->_id;
        $this->graph->disabled           = 0;
        $this->graph->automatic_protocol = CSupervisionGraph::PROTOCOL_MD_STREAM;

        $this->store($this->graph, self::MONITORING_PATIENT_GRAPH);
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createGraphAxis(): void
    {
        $graph_axe = CSupervisionGraphAxis::getSampleObject();

        $graph_axe->supervision_graph_id = $this->graph->_id;
        $graph_axe->show_points          = 1;
        $graph_axe->display              = "points";
        $graph_axe->limit_low            = null;
        $graph_axe->limit_high           = null;

        $this->store($graph_axe, self::MONITORING_PATIENT_GRAPH_AXE);

        $series                            = CSupervisionGraphSeries::getSampleObject();
        $series->supervision_graph_axis_id = $graph_axe->_id;
        $series->title                     = self::MONITORING_PATIENT_GRAPH_AXE;
        $series->value_type_id             = $this->obs_value_type->_id;
        $series->value_unit_id             = $this->obs_value_unit->_id;
        $this->store($series, self::MONITORING_PATIENT_GRAPH_SERIES);

        $graph_axe_2 = CSupervisionGraphAxis::getSampleObject();

        $graph_axe_2->supervision_graph_id = $this->graph->_id;
        $graph_axe_2->show_points          = 0;
        $graph_axe_2->display              = "stack";
        $graph_axe_2->limit_low            = 1.0;
        $graph_axe_2->limit_high           = 10.0;

        $this->store($graph_axe_2, self::MONITORING_PATIENT_GRAPH_AXE_2);

        $graph_axis_label = CSupervisionGraphAxisValueLabel::getSampleObject();

        $graph_axis_label->supervision_graph_axis_id = $graph_axe_2->_id;
        $this->store($graph_axis_label);

        $series_2                            = CSupervisionGraphSeries::getSampleObject();
        $series_2->supervision_graph_axis_id = $graph_axe_2->_id;
        $series_2->title                     = self::MONITORING_PATIENT_GRAPH_AXE_2;
        $series_2->value_type_id             = $this->obs_value_type->_id;
        $series_2->value_unit_id             = $this->obs_value_unit->_id;
        $this->store($series_2, self::MONITORING_PATIENT_GRAPH_SERIES_2);

        $graph_axe_3 = CSupervisionGraphAxis::getSampleObject();

        $graph_axe_3->supervision_graph_id = $this->graph->_id;
        $graph_axe_3->show_points          = 0;
        $graph_axe_3->display              = "bandwidth";
        $graph_axe_3->limit_low            = 1.0;
        $graph_axe_3->limit_high           = 10.0;

        $this->store($graph_axe_3, self::MONITORING_PATIENT_GRAPH_AXE_3);

        $series_3                            = CSupervisionGraphSeries::getSampleObject();
        $series_3->supervision_graph_axis_id = $graph_axe_3->_id;
        $series_3->title                     = self::MONITORING_PATIENT_GRAPH_AXE_3;
        $series_3->value_type_id             = $this->obs_value_type->_id;
        $series_3->value_unit_id             = $this->obs_value_unit->_id;
        $this->store($series_3, self::MONITORING_PATIENT_GRAPH_SERIES_3);
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createDataTable(): void
    {
        $this->table = CSupervisionTable::getSampleObject();

        $this->table->owner_class        = $this->group->_class;
        $this->table->owner_id           = $this->group->_id;
        $this->table->disabled           = 0;
        $this->table->automatic_protocol = CSupervisionGraph::PROTOCOL_MD_STREAM;
        $this->store($this->table, self::MONITORING_PATIENT_TABLE);
        if ($this->table->_id) {
            $table_row = CSupervisionTableRow::getSampleObject();

            $table_row->supervision_table_id = $this->table->_id;

            $table_row->value_type_id = $this->obs_value_type->_id;
            $table_row->value_unit_id = $this->obs_value_unit->_id;
            $this->store($table_row, self::MONITORING_PATIENT_TABLE_ROW);
        }
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createGraphPack(): void
    {
        $this->pack                = CSupervisionGraphPack::getSampleObject();
        $this->pack->owner_class   = $this->group->_class;
        $this->pack->owner_id      = $this->group->_id;
        $this->pack->use_contexts  = "preop|perop";
        $this->pack->timing_fields = '{"entree_salle":"#FFFFFF"}';

        $this->store($this->pack, self::MONITORING_PATIENT_GRAPH_PACK);
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createGraphPackForTable(): void
    {
        $this->pack_table                = CSupervisionGraphPack::getSampleObject();
        $this->pack_table->owner_class   = $this->group->_class;
        $this->pack_table->owner_id      = $this->group->_id;
        $this->pack_table->use_contexts  = "preop|perop";
        $this->pack_table->timing_fields = '{"entree_salle":"#FFFFFF"}';

        $this->store($this->pack_table, self::MONITORING_PATIENT_GRAPH_PACK_3);
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createGraphPackWithoutTimingAndContexts(): void
    {
        $this->empty_pack                = CSupervisionGraphPack::getSampleObject();
        $this->empty_pack->owner_class   = $this->group->_class;
        $this->empty_pack->owner_id      = $this->group->_id;
        $this->empty_pack->timing_fields = null;
        $this->empty_pack->use_contexts  = null;

        $this->store($this->empty_pack, self::MONITORING_PATIENT_GRAPH_PACK_2);
    }

    /**
     * @throws FixturesException
     */
    private function linkGraphToPack(
        CStoredObject $object,
        CSupervisionGraphPack $graph_pack,
        string $reference
    ): void {
        $graph_to_pack              = new CSupervisionGraphToPack();
        $graph_to_pack->graph_class = $object->_class;
        $graph_to_pack->graph_id    = $object->_id;
        $graph_to_pack->pack_id     = $graph_pack->_id;
        $graph_to_pack->rank        = 1;
        $this->store($graph_to_pack, $reference);
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createTimedData(): void
    {
        // Type str
        $this->timed_data                = CSupervisionTimedData::getSampleObject();
        $this->timed_data->owner_class   = $this->group->_class;
        $this->timed_data->owner_id      = $this->group->_id;
        $this->timed_data->type          = "str";
        $this->timed_data->value_type_id = $this->obs_value_type->_id;
        $this->timed_data->column        = 1;
        $this->store($this->timed_data, self::MONITORING_PATIENT_TIMED_DATA);

        // Type enum with items
        $this->timed_data_enum                = CSupervisionTimedData::getSampleObject();
        $this->timed_data_enum->owner_class   = $this->group->_class;
        $this->timed_data_enum->owner_id      = $this->group->_id;
        $this->timed_data_enum->value_type_id = $this->obs_value_type->_id;
        $this->timed_data_enum->type          = "enum";
        $this->timed_data_enum->items         = "Lorem\r\nIpsum\r\nDolor";
        $this->timed_data_enum->column        = 1;
        $this->store($this->timed_data_enum, self::MONITORING_PATIENT_TIMED_DATA_ENUM);
    }
}
