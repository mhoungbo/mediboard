/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

SupervisionGraph = {
    currentGraphId:                null,
    list_url:                      null,
    edit_graph_url:                null,
    list_graph_axis_url:           null,
    edit_graph_axis_url:           null,
    list_axis_series:              null,
    edit_axis_series:              null,
    list_axis_labels_url:          null,
    edit_axis_label_url:           null,
    preview_graph_url:             null,
    edit_table_url:                null,
    list_table_rows_url:           null,
    edit_table_row_url:            null,
    edit_timed_data_url:           null,
    edit_timed_picture_url:        null,
    view_pictures_url:             null,
    edit_instant_data_url:         null,
    edit_pack_url:                 null,
    edit_graph_to_pack_url:        null,
    list_graph_to_pack_url:        null,
    change_graph_to_pack_rank_url: null,

    list: function (callback) {
        new Url()
            .setRoute(SupervisionGraph.list_url, "monitoringPatient_gui_supervision_list", "monitoringPatient")
            .requestUpdate("supervision-list", callback ? callback : Prototype.emptyFunction);
    },

    editGraph: function (id) {
        new Url()
            .setRoute(SupervisionGraph.edit_graph_url, "monitoringPatient_gui_supervision_edit", "monitoringPatient")
            .addParam("supervision_graph_id", id)
            .requestUpdate("supervision-graph-editor");
        return false;
    },

    callbackEditGraph: function (id) {
        SupervisionGraph.list(SupervisionGraph.editGraph.curry(id));
    },

    // Axes
    listAxes: function (graph_id) {
        SupervisionGraph.currentGraphId = graph_id;

        new Url()
            .setRoute(SupervisionGraph.list_graph_axis_url, "monitoringPatient_gui_list_graph_axes", "monitoringPatient")
            .addParam("supervision_graph_id", graph_id)
            .requestUpdate("supervision-graph-axes-list");

        SupervisionGraph.preview(SupervisionGraph.currentGraphId);
    },

    editAxis: function (id, graph_id) {
        let url = new Url()
            .setRoute(SupervisionGraph.edit_graph_axis_url, "monitoringPatient_gui_edit_graph_axis", "monitoringPatient")
            .addParam("supervision_graph_axis_id", id);

        if (graph_id) {
            SupervisionGraph.currentGraphId = graph_id;
            url.addParam("supervision_graph_id", graph_id);
        }

        url.requestUpdate("supervision-graph-axis-editor");

        return false;
    },

    callbackEditAxis: function (id, obj) {
        SupervisionGraph.listAxes(obj.supervision_graph_id);
        SupervisionGraph.editAxis(id, obj.supervision_graph_id);
        SupervisionGraph.preview(SupervisionGraph.currentGraphId);
    },

    editTimedData: function (id) {
        new Url()
            .setRoute(SupervisionGraph.edit_timed_data_url, "monitoringPatient_gui_edit_timed_data", "monitoringPatient")
            .addParam("supervision_timed_data_id", id)
            .requestUpdate("supervision-graph-editor");

        return false;
    },

    callbackEditTimedData: function (id) {
        SupervisionGraph.list(SupervisionGraph.editTimedData.curry(id));
    },

    editTimedPicture: function (id) {
        new Url()
            .setRoute(SupervisionGraph.edit_timed_picture_url, "monitoringPatient_gui_edit_timed_picture", "monitoringPatient")
            .addParam("supervision_timed_picture_id", id)
            .requestUpdate("supervision-graph-editor");

        return false;
    },

    callbackEditTimedPicture: function (id) {
        SupervisionGraph.list(SupervisionGraph.editTimedPicture.curry(id));
    },

    editInstantData: function (id) {
        new Url()
            .setRoute(SupervisionGraph.edit_instant_data_url, "monitoringPatient_gui_edit_instant_data", "monitoringPatient")
            .addParam("supervision_instant_data_id", id)
            .requestUpdate("supervision-graph-editor");

        return false;
    },

    callbackEditInstantData: function (id) {
        SupervisionGraph.list(SupervisionGraph.editInstantData.curry(id));
    },

    // Series
    listSeries: function (axis_id) {
        new Url()
            .setRoute(SupervisionGraph.list_axis_series, "monitoringPatient_gui_list_axes_series", "monitoringPatient")
            .addParam("supervision_graph_axis_id", axis_id)
            .requestUpdate("supervision-graph-series-list");
    },

    editSeries:         function (id, axis_id) {
        new Url()
            .setRoute(SupervisionGraph.edit_axis_series, "monitoringPatient_gui_edit_axes_series", "monitoringPatient")
            .addParam("supervision_graph_series_id", id)
            .addNotNullParam("supervision_graph_axis_id", axis_id)
            .requestModal(500, 530);

        return false;
    },
    callbackEditSeries: function (id, obj) {
        SupervisionGraph.listSeries(obj.supervision_graph_axis_id);
        SupervisionGraph.listAxes(SupervisionGraph.currentGraphId);
        Control.Modal.close();
    },

    // Axis labels
    listAxisLabels: function (axis_id) {
        new Url()
            .setRoute(SupervisionGraph.list_axis_labels_url, "monitoringPatient_gui_list_axes_labels", "monitoringPatient")
            .addParam("supervision_graph_axis_id", axis_id)
            .requestUpdate("supervision-graph-axis-labels-list");
    },

    editAxisLabel: function (id, axis_id) {
        new Url()
            .setRoute(SupervisionGraph.edit_axis_label_url, "monitoringPatient_gui_edit_axes_label", "monitoringPatient")
            .addParam("supervision_graph_axis_label_id", id)
            .addNotNullParam("supervision_graph_axis_id", axis_id)
            .requestModal(400, 300);

        return false;
    },

    callbackAxisLabel: function (id, obj) {
        SupervisionGraph.listAxisLabels(obj.supervision_graph_axis_id);
        Control.Modal.close();
    },

    preview: function (graph_id) {
        if (!graph_id) {
            return;
        }

        new Url()
            .setRoute(SupervisionGraph.preview_graph_url, "monitoringPatient_gui_preview_graph", "monitoringPatient")
            .addParam("supervision_graph_id", graph_id)
            .requestUpdate("supervision-graph-preview");
    },

    editTable: function (id) {
        new Url()
            .setRoute(SupervisionGraph.edit_table_url, "monitoringPatient_gui_table_edit", "monitoringPatient")
            .addParam("supervision_table_id", id)
            .requestUpdate("supervision-graph-editor");
        return false;
    },

    callbackEditTable: function (id) {
        SupervisionGraph.list(SupervisionGraph.editTable.curry(id));
    },

    listTableRows: function (table_id) {
        new Url()
            .setRoute(SupervisionGraph.list_table_rows_url, "monitoringPatient_gui_list_table_rows", "monitoringPatient")
            .addParam("supervision_table_id", table_id)
            .requestUpdate("supervision-table-rows-list");
    },

    editTableRow: function (row_id, table_id) {
        let url = new Url()
            .setRoute(SupervisionGraph.edit_table_row_url, "monitoringPatient_gui_edit_table_row", "monitoringPatient")
            .addParam("supervision_table_row_id", row_id);

        if (table_id) {
            url.addParam("supervision_table_id", table_id);
        }

        url.requestUpdate("supervision-table-row-editor");

        return false;
    },

    callbackEditTableRow: function (id, obj) {
        SupervisionGraph.listTableRows(obj.supervision_table_id);
        SupervisionGraph.editTableRow(id);
    },

    editPack: function (id) {
        new Url()
            .setRoute(SupervisionGraph.edit_pack_url, "monitoringPatient_gui_edit_pack", "monitoringPatient")
            .addParam("supervision_graph_pack_id", id)
            .requestUpdate("supervision-graph-editor");
        return false;
    },

    editPackTimings: function () {
        $$("#edit-timing-fields input.color").each(function (e) {
            e.colorPicker({
                change: function (color) {
                    var form = this.form;
                    var fieldsElement = form.timing_fields;
                    var fields = fieldsElement.value ? fieldsElement.value.evalJSON() : {};

                    if (color) {
                        fields[this.get("timing")] = color.toHexString();
                    } else {
                        delete fields[this.get("timing")];
                    }

                    fieldsElement.value = Object.toJSON(fields);
                }.bind(e)
            });
        });

        Modal.open("edit-timing-fields", {
            showClose: true, width: 620, height: 350
        });
    },

    callbackEditPack: function (id) {
        SupervisionGraph.list(SupervisionGraph.editPack.curry(id));
    },

    editGraphToPack: function (id, pack_id, graph_class) {
        new Url()
            .setRoute(SupervisionGraph.edit_graph_to_pack_url, "monitoringPatient_gui_edit_graph_to_pack", "monitoringPatient")
            .addParam("graph_class", graph_class)
            .addNotNullParam("supervision_graph_to_pack_id", id)
            .addNotNullParam("supervision_graph_pack_id", pack_id)
            .requestModal(400, 400);
        return false;
    },

    listGraphToPack: function (pack_id) {
        if (!pack_id) {
            return;
        }
        new Url()
            .setRoute(SupervisionGraph.list_graph_to_pack_url, "monitoringPatient_gui_list_graph_to_pack", "monitoringPatient")
            .addParam("supervision_graph_pack_id", pack_id)
            .requestUpdate("graph-to-pack-list");
        return false;
    },

    graphToPackCallback: function (id, obj) {
        Control.Modal.close();
        SupervisionGraph.listGraphToPack(obj.pack_id);
    },

    chosePredefinedPicture: function (timed_picture_id) {
        new Url()
            .setRoute(SupervisionGraph.view_pictures_url, "monitoringPatient_gui_view_pictures", "monitoringPatient")
            .addParam("timed_picture_id", timed_picture_id)
            .requestModal(400, 400, {
                onClose: SupervisionGraph.editTimedPicture.curry(timed_picture_id)
            });
    },
    /**
     * Check if the preop checkbox is checked when I selected preop or SSPI checkbox
     */
    showCheckboxPackshowCheckboxPack: function (element) {
        let type_preop = "preop";
        let type_sspi = "sspi";
        let element_value = element.value;

        $('show_main_pack').hide();

        if ((element_value.indexOf(type_preop) >= 0) || (element_value.indexOf(type_sspi) >= 0)) {
            $('show_main_pack').show();
        }
    },
    /**
     * Change elements' rank
     */
    changeRank: function (supervision_graph_to_pack_id, rank, sortable, pack_id, csrf_token) {
        let wish_rank = 0;

        if (sortable) {
            wish_rank = (sortable == 'down') ? parseInt(rank) + 1 : parseInt(rank) - 1;
        }

        new Url()
            .setRoute(SupervisionGraph.change_graph_to_pack_rank_url, "monitoringPatient_gui_change_graph_to_pack_rank", "monitoringPatient")
            .addParam('token', csrf_token)
            .addParam('supervision_graph_to_pack_id', supervision_graph_to_pack_id)
            .addParam('rank', rank)
            .addParam('wish_rank', wish_rank)
            .requestUpdate('systemMsg', {onComplete: SupervisionGraph.listGraphToPack.curry(pack_id), method: 'post'});
    },
    /**
     * Show the column field
     *
     * @param element
     */
    showColumn: function (element) {
        let form = element.form;

        if (element.value == 'set') {
            $('column_list').show();
        } else {
            $('column_list').hide();
        }
    }
};
