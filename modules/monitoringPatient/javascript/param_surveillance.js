/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ParamSurveillance = {
    edit_url:            null,
    list_url:            null,
    list_conversion_url: null,
    edit_conversion_url: null,

    edit:           function (param_guid) {
        new Url()
            .setRoute(ParamSurveillance.edit_url, "monitoringPatient_gui_surveillance_edit", "monitoringPatient")
            .addParam("param_guid", param_guid)
            .requestModal(500, 430, {
                onClose: ParamSurveillance.list.curry(param_guid.split(/-/)[0])
            });
    },
    list:           function (object_class, start = 0) {
        new Url()
            .setRoute(ParamSurveillance.list_url, "monitoringPatient_gui_surveillance_list", "monitoringPatient")
            .addParam("object_class", object_class)
            .addParam("start", start)
            .requestUpdate("list-" + object_class);
    },
    listConversion: function (start = 0) {
        new Url()
            .setRoute(ParamSurveillance.list_conversion_url, "monitoringPatient_gui_surveillance_list_conversion", "monitoringPatient")
            .addParam('start', start)
            .requestUpdate('list-CObservationValueToConstant');
    },
    editConversion: function (object_id) {
        new Url()
            .setRoute(ParamSurveillance.edit_conversion_url, "monitoringPatient_gui_surveillance_edit_conversion", "monitoringPatient")
            .addParam('observation_value_to_constant_id', object_id)
            .requestModal(500, 360, {
                onClose: ParamSurveillance.listConversion.curry()
            });
    },
};
