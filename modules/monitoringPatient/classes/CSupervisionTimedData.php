<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;

/**
 * A supervision timed data representation
 */
class CSupervisionTimedData extends CSupervisionTimedEntity
{
    /** @var int */
    public $supervision_timed_data_id;

    /** @var int */
    public $period;

    /** @var int */
    public $value_type_id;

    /** @var bool */
    public $in_doc_template;

    /** @var string */
    public $type;

    /** @var string */
    public $items;

    /** @var int  */
    public $column;

    /** @var array */
    public $_items;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = "supervision_timed_data";
        $spec->key   = "supervision_timed_data_id";

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props                    = parent::getProps();
        $props["owner_id"]        .= " back|supervision_timed_data";
        $props["period"]          = "enum list|1|5|10|15|20|30|60";
        $props["value_type_id"]   =
            "ref notNull class|CObservationValueType autocomplete|_view dependsOn|datatype back|supervision_timed_data";
        $props["in_doc_template"] = "bool notNull default|0";
        $props["type"]            = "enum list|str|bool|enum|set notNull default|str";
        $props["items"]           = "text";
        $props["column"]          = "num min|1 max|4 default|1";

        return $props;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function store()
    {
        if (!in_array($this->type, ["enum", "set"])) {
            // Si le type n'est pas une liste de choix, on vide le champ 'choix possibles'
            $this->items = "";
        }
        return parent::store();
    }

    /**
     * Get timed data, set it to empty array if not present
     *
     * @param array[][] $results Patient results
     *
     * @return array
     */
    public function loadTimedData(array $results): array
    {
        $type_id = $this->value_type_id;

        if (!isset($results[$type_id]["none"])) {
            return $this->_graph_data = [];
        }

        return $this->_graph_data = $results[$type_id]["none"];
    }

    /**
     * Make items
     *
     * @return string[]
     */
    public function makeItems(): ?array
    {
        if (!in_array($this->type, ["enum", "set"]) || !$this->items) {
            return $this->_items = null;
        }

        return $this->_items = preg_split('/[\r\n]+/', $this->items);
    }

    /**
     * Get all the timed data for an object
     *
     * @param CMbObject $object The object to get timed data of
     *
     * @return CSupervisionTimedData[]|null
     * @throws Exception
     */
    public static function getAllFor(CMbObject $object): array
    {
        $graph = new self();

        $where = [
            "owner_class" => $graph->getDS()->prepare("= ?", $object->_class),
            "owner_id"    => $graph->getDS()->prepare("= ?", $object->_id),
        ];

        return $graph->loadList($where, "title");
    }
}
