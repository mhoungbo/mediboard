<?php

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Etablissement\CGroups;

class SupervisionService
{
    protected CGroups $group;

    /**
     * @param CGroups $group
     *
     * @throws CMbException
     */
    public function __construct(CGroups $group)
    {
        if (!$group->_id) {
            throw new CMbException("SupervisionService-error-group id is null");
        }
        $this->group = $group;
    }

    /**
     * @throws Exception
     */
    public function loadAllSupervisionElements(): array
    {
        $graphs         = CSupervisionGraph::getAllFor($this->group);
        $tables         = CSupervisionTable::getAllFor($this->group);
        $timed_data     = CSupervisionTimedData::getAllFor($this->group);
        $timed_pictures = CSupervisionTimedPicture::getAllFor($this->group);
        $instant_data   = CSupervisionInstantData::getAllFor($this->group);
        $packs          = CSupervisionGraphPack::getAllFor($this->group);

        $axes = CStoredObject::massLoadBackRefs($graphs, "axes");
        CStoredObject::massLoadBackRefs($axes, "series");
        CStoredObject::massLoadBackRefs($tables, "rows");

        foreach ($graphs as $_graph) {
            $_axes = $_graph->loadRefsAxes();

            foreach ($_axes as $_axis) {
                $_axis->loadBackRefs("series");
            }
        }

        foreach ($tables as $table) {
            $table->loadRefsRows();
        }

        return [
            $graphs,
            $tables,
            $timed_data,
            $timed_pictures,
            $instant_data,
            $packs,
        ];
    }

    /**
     * @throws Exception
     */
    public static function buildPreviewGraphData(CSupervisionGraph $graph): array
    {
        $axes = $graph->loadRefsAxes();

        $sample = [];

        $minute = 60000;
        $start  = 1291196760000;
        $end    = $start + $minute * 45;
        $times  = range($start, $end, $minute);

        foreach ($axes as $_axis) {
            /** @var CSupervisionGraphAxis $_axis */
            if (!$_axis->actif) {
                continue;
            }

            $_series = $_axis->loadRefsSeries();

            foreach ($_series as $_serie) {
                /** @var CSupervisionGraphSeries $_serie */
                $sample[$_serie->value_type_id][$_serie->value_unit_id ?: "none"] = $_serie->getSampleData($times);
            }
        }

        $data = $graph->buildGraph($sample, $start - 2 * $minute, $end + 2 * $minute);

        return [$data, $times];
    }
}
