<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Mediboard\ObservationResult\CObservationValueUnit;

/**
 * A supervision instant data representation
 */
class CSupervisionInstantData extends CSupervisionTimedEntity
{
    /** @var int */
    public $supervision_instant_data_id;

    /** @var int */
    public $value_type_id;

    /** @var int */
    public $value_unit_id;

    /** @var int */
    public $size;

    /** @var string */
    public $color;

    /** @var CStoredObject return CObservationValueType */
    public $_ref_value_type;

    /** @var int */
    public $_ref_value_unit;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = "supervision_instant_data";
        $spec->key   = "supervision_instant_data_id";

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props                  = parent::getProps();
        $props["owner_id"]      .= " back|supervision_instant_data";
        $props["value_type_id"] = "ref notNull class|CObservationValueType autocomplete|_view dependsOn|coding_system back|supervision_instant_data";
        $props["value_unit_id"] = "ref notNull class|CObservationValueUnit autocomplete|_view dependsOn|coding_system back|supervision_instant_data";
        $props["size"]          = "num notNull min|10 max|60";
        $props["color"]         = "color";

        return $props;
    }

    /**
     * Load value type
     *
     * @param bool $cache Use object cache
     *
     * @return CStoredObject
     * @throws Exception
     */
    public function loadRefValueType(bool $cache = true): CStoredObject
    {
        return $this->_ref_value_type = $this->loadFwdRef("value_type_id", $cache);
    }

    /**
     * Load value unit
     *
     * @return CObservationValueUnit
     * @throws Exception
     */
    public function loadRefValueUnit(): CObservationValueUnit
    {
        return $this->_ref_value_unit = CObservationValueUnit::get($this->value_unit_id);
    }

    /**
     * Get all the instant data for an object
     *
     * @param CMbObject $object The object to get timed data of
     *
     * @return self[]
     * @throws Exception
     */
    public static function getAllFor(CMbObject $object): array
    {
        $graph = new self();

        $where = [
            "owner_class" => $graph->getDS()->prepare("= ?", $object->_class),
            "owner_id"    => $graph->getDS()->prepare("= ?", $object->_id),
        ];

        return $graph->loadList($where, "title");
    }
}
