<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\CMbPath;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\MonitoringPatient\CSupervisionInstantData;
use Ox\Mediboard\MonitoringPatient\CSupervisionTimedData;
use Ox\Mediboard\MonitoringPatient\CSupervisionTimedPicture;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

/**
 * Timed Data main controller
 */
class TimedElementsController extends Controller
{
    /**
     * @throws Exception
     */
    public function editTimedData(RequestParams $params): Response
    {
        $supervision_timed_data_id = $params->get('supervision_timed_data_id', 'ref class|CSupervisionTimedData', true);

        $timed_data = CSupervisionTimedData::findOrNew($supervision_timed_data_id);
        $timed_data->loadRefsNotes();

        return $this->renderSmarty(
            "inc_edit_supervision_timed_data",
            [
                "timed_data" => $timed_data,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function editTimedPicture(RequestParams $params): Response
    {
        $supervision_timed_picture_id = $params->get(
            'supervision_timed_picture_id',
            'ref class|CSupervisionTimedPicture',
            true
        );

        $picture = CSupervisionTimedPicture::findOrNew($supervision_timed_picture_id);
        $picture->loadRefsNotes();
        $picture->loadRefsFiles();

        $tree = CMbPath::getTree(CSupervisionTimedPicture::PICTURES_ROOT);

        return $this->renderSmarty(
            "inc_edit_supervision_timed_picture",
            [
                "picture" => $picture,
                "tree"    => $tree,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function viewSupervisionPictures(RequestParams $params): Response
    {
        $timed_picture_id = $params->get("timed_picture_id", "ref class|CSupervisionTimedPicture");

        $tree = CMbPath::getTree(CSupervisionTimedPicture::PICTURES_ROOT);

        return $this->renderSmarty(
            "inc_vw_supervision_pictures",
            [
                "tree"             => $tree,
                "timed_picture_id" => $timed_picture_id,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function editInstantData(RequestParams $params): Response
    {
        $supervision_instant_data_id = $params->get(
            "supervision_instant_data_id",
            "ref class|CSupervisionInstantData",
            true
        );

        $instant_data = CSupervisionInstantData::findOrNew($supervision_instant_data_id);
        $instant_data->loadRefsNotes();

        if (!$instant_data->_id) {
            $instant_data->size = 11;
        }

        return $this->renderSmarty(
            "inc_edit_supervision_instant_data",
            [
                "instant_data" => $instant_data,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function selectPicture(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('monitoring_patient_select_picture', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $path             = realpath($params->post("path", "str notNull"));
        $timed_picture_id = $params->post("timed_picture_id", "ref class|CSupervisionTimedPicture");
        $root             = dirname(__DIR__, 4) . "/" . CSupervisionTimedPicture::PICTURES_ROOT;

        if (is_file($path) && str_starts_with($path, $root)) {
            $timed_picture = new CSupervisionTimedPicture();
            $timed_picture->load($timed_picture_id);

            $file = new CFile();

            $file->object_class = $timed_picture->_class;
            $file->object_id    = $timed_picture->_id;
            $file->fillFields();
            $file->file_name = basename($path);
            $file->doc_size  = filesize($path);
            $file->file_type = CMbPath::guessMimeType($path);
            $file->setCopyFrom($path);

            try {
                $this->storeObject($file);
            } catch (CMbException $e) {
                $this->addUiMsgError($e->getMessage(), true);

                return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
            }
            $this->addUiMsgOk("CSupervisionTimedPicture-msg-picture saved");

            return $this->renderEmptyResponse();
        }
        $this->addUiMsgError("CSupervisionTimedPicture-error-picture not saved");

        return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
    }
}
