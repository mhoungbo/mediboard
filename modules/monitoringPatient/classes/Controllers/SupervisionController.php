<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Controllers;

use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CStoredObject;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraph;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphAxis;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphAxisValueLabel;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphSeries;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\MonitoringPatient\SupervisionService;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Monitoring patient main controller
 */
class SupervisionController extends Controller
{
    /**
     * @throws Exception
     */
    public function supervisionIndex(RequestParams $params): Response
    {
        $supervision_graph_id = $params->get('supervision_graph_id', 'ref class|CSupervisionGraph');

        return $this->renderSmarty(
            "vw_supervision_graph",
            [
                "supervision_graph_id" => $supervision_graph_id,
            ]
        );
    }

    /**
     * @return Response
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function supervisionList(): Response
    {
        $group = CGroups::get($this->getRequestContext()->getGroupId());

        $supervision_service = new SupervisionService($group);

        [
            $graphs,
            $tables,
            $timed_data,
            $timed_pictures,
            $instant_data,
            $packs,
        ] = $supervision_service->loadAllSupervisionElements();

        return $this->renderSmarty(
            "inc_list_supervision_graph",
            [
                "graphs"         => $graphs,
                "tables"         => $tables,
                "packs"          => $packs,
                "timed_data"     => $timed_data,
                "timed_pictures" => $timed_pictures,
                "instant_data"   => $instant_data,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function editSupervisionGraph(RequestParams $params): Response
    {
        $supervision_graph_id = $params->get("supervision_graph_id", "ref class|CSupervisionGraph");

        $graph = CSupervisionGraph::findOrNew($supervision_graph_id);
        $graph->loadRefsNotes();

        if (!$graph->_id) {
            $graph->height = 200;
        }

        return $this->renderSmarty(
            "inc_edit_supervision_graph",
            [
                "graph" => $graph,
            ]
        );
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function listGraphAxes(RequestParams $params): Response
    {
        $supervision_graph_id = $params->get("supervision_graph_id", "ref class|CSupervisionGraph");

        $graph = CSupervisionGraph::findOrFail($supervision_graph_id);

        $counter_axes_active = 0;

        $axes = $graph->loadRefsAxes();

        foreach ($axes as $_axe) {
            /** @var CSupervisionGraphAxis $_axe */
            if ($_axe->actif) {
                $counter_axes_active++;
            }
        }

        CStoredObject::massLoadBackRefs($axes, "series");

        return $this->renderSmarty(
            "inc_list_supervision_graph_axes",
            [
                "graph"               => $graph,
                "counter_axes_active" => $counter_axes_active,
                "axes"                => $axes,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function editGraphAxis(RequestParams $params): Response
    {
        $supervision_graph_axis_id = $params->get("supervision_graph_axis_id", "ref class|CSupervisionGraphAxis");
        $supervision_graph_id      = $params->get("supervision_graph_id", "ref class|CSupervisionGraph");

        $axis = CSupervisionGraphAxis::findOrNew($supervision_graph_axis_id);

        if (!$axis->_id) {
            $axis->supervision_graph_id = $supervision_graph_id;
        }
        $axis->loadRefsNotes();

        return $this->renderSmarty(
            "inc_edit_supervision_graph_axis",
            [
                "axis" => $axis,
            ]
        );
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function listGraphAxisSeries(RequestParams $params): Response
    {
        $supervision_graph_axis_id = $params->get("supervision_graph_axis_id", "ref class|CSupervisionGraphAxis");

        $axis = CSupervisionGraphAxis::findOrFail($supervision_graph_axis_id);

        $series = $axis->loadRefsSeries();

        CStoredObject::massLoadFwdRef($series, "value_unit_id");
        foreach ($series as $_series) {
            /**  @var CSupervisionGraphSeries $_series */
            $_series->loadRefValueUnit();
        }

        return $this->renderSmarty(
            "inc_list_supervision_graph_series",
            [
                "series" => $series,
                "axis"   => $axis,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function editGraphAxisSeries(RequestParams $params): Response
    {
        $supervision_graph_series_id = $params->get("supervision_graph_series_id", "ref class|CSupervisionGraphSeries");
        $supervision_graph_axis_id   = $params->get("supervision_graph_axis_id", "ref class|CSupervisionGraphAxis");

        $axis   = CSupervisionGraphAxis::findOrFail($supervision_graph_axis_id);
        $series = CSupervisionGraphSeries::findOrNew($supervision_graph_series_id);

        if (!$series->_id) {
            $series->supervision_graph_axis_id = $supervision_graph_axis_id;
        }
        $series->loadRefsNotes();

        return $this->renderSmarty(
            "inc_edit_supervision_graph_series",
            [
                "series" => $series,
                "axis"   => $axis,
            ]
        );
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function listAxisLabels(RequestParams $params): Response
    {
        $supervision_graph_axis_id = $params->get("supervision_graph_axis_id", "ref class|CSupervisionGraphAxis");

        $axis = CSupervisionGraphAxis::findOrFail($supervision_graph_axis_id);

        $labels = $axis->loadRefsLabels();

        return $this->renderSmarty(
            "inc_list_supervision_graph_labels",
            [
                "axis"   => $axis,
                "labels" => $labels,
            ]
        );
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function editAxisLabel(RequestParams $params): Response
    {
        $supervision_graph_axis_id       = $params->get("supervision_graph_axis_id", "ref class|CSupervisionGraphAxis");
        $supervision_graph_axis_label_id = $params->get(
            "supervision_graph_axis_label_id",
            "ref class|CSupervisionGraphAxisValueLabel"
        );

        $axis  = CSupervisionGraphAxis::findOrFail($supervision_graph_axis_id);
        $label = CSupervisionGraphAxisValueLabel::findOrNew($supervision_graph_axis_label_id);

        if (!$label->_id) {
            $label->supervision_graph_axis_id = $supervision_graph_axis_id;
        }
        $label->loadRefsNotes();

        return $this->renderSmarty(
            "inc_edit_supervision_graph_axis_label",
            [
                "axis"  => $axis,
                "label" => $label,
            ]
        );
    }

    /**
     * @param RequestParams $params
     *
     * @return Response
     * @throws Exception
     */
    public function previewGraph(RequestParams $params): Response
    {
        $supervision_graph_id = $params->get("supervision_graph_id", "ref class|CSupervisionGraph");

        $graph = CSupervisionGraph::findOrFail($supervision_graph_id);

        [$data, $times] = SupervisionService::buildPreviewGraphData($graph);

        return $this->renderSmarty(
            "inc_preview_supervision_graph",
            [
                "data"                 => $data,
                "times"                => $times,
                "supervision_graph_id" => $supervision_graph_id,
                "graph"                => $graph,
            ]
        );
    }
}
