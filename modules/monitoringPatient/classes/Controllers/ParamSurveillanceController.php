<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\MonitoringPatient\ParamSurveillanceService;
use Ox\Mediboard\ObservationResult\CObservationValueCodingSystem;
use Ox\Mediboard\ObservationResult\CObservationValueToConstant;
use Ox\Mediboard\ObservationResult\CObservationValueType;
use Ox\Mediboard\ObservationResult\CObservationValueUnit;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Surveillance main controller
 */
class ParamSurveillanceController extends Controller
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->renderSmarty("vw_config_param_surveillance");
    }

    /**
     * @param RequestParams $params
     *
     * @return Response
     * @throws InvalidArgumentException
     * @throws CMbException
     * @throws Exception
     */
    public function list(RequestParams $params): Response
    {
        $object_class = $params->get("object_class", "str class|CObservationValueCodingSystem notNull");
        $start        = (int)$params->get("start", "num");
        $group        = CGroups::get($this->getRequestContext()->getGroupId());

        $service = new ParamSurveillanceService($group, $start);

        [$params, $params_total] = $service->loadSurveillancePage($object_class);

        return $this->renderSmarty(
            "inc_list_config_param_surveillance",
            [
                "params"       => $params,
                "params_total" => $params_total,
                "object_class" => $object_class,
                "start"        => $start,
            ]
        );
    }

    /**
     * @param RequestParams $params
     *
     * @return Response
     * @throws Exception
     */
    public function edit(RequestParams $params): Response
    {
        $param_guid = $params->get('param_guid', 'str notNull default|CObservationValueUnit-0');

        /** @var CObservationValueUnit|CObservationValueType $param */
        $param = CStoredObject::loadFromGuid($param_guid);

        if ($param->_id && !$param instanceof CObservationValueCodingSystem) {
            $this->addUiMsgError("ParamSurveillanceController-error-wrong object");

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }

        if (!$param->_id) {
            $param->coding_system = "MB";
        } else {
            $param->loadRefsNotes();
            $param->countUsages();
        }

        $groups = (new CGroups())->loadList();

        return $this->renderSmarty(
            "inc_edit_config_param_surveillance",
            [
                "param"  => $param,
                "groups" => $groups,
            ]
        );
    }

    /**
     * @param RequestParams $params
     *
     * @return Response
     * @throws CMbException
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function listConversion(RequestParams $params): Response
    {
        $start = $params->get('start', 'num min|0 default|0');
        $group = CGroups::get($this->getRequestContext()->getGroupId());

        $service = new ParamSurveillanceService($group, $start);

        [$conversions, $total] = $service->listConversionPage();

        return $this->renderSmarty(
            "inc_list_observation_value_to_constant",
            [
                'conversions' => $conversions,
                'total'       => $total,
                'start'       => $start,
            ]
        );
    }

    /**
     * @param RequestParams $params
     *
     * @return Response
     * @throws Exception
     */
    public function editConversion(RequestParams $params): Response
    {
        $observation_value_to_constant_id = $params->get(
            'observation_value_to_constant_id',
            'ref class|CObservationValueToConstant'
        );

        $conversion = CObservationValueToConstant::findOrNew($observation_value_to_constant_id);
        if ($conversion->_id) {
            $conversion->loadRefValueType();
            $conversion->loadRefValueUnit();
        } else {
            $conversion->conversion_ratio = 1;
        }

        return $this->renderSmarty(
            "inc_edit_observation_value_to_constant",
            [
                'conversion' => $conversion,
            ]
        );
    }
}
