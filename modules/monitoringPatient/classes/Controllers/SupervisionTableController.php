<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Controllers;

use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CMbObject;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\MonitoringPatient\CSupervisionTable;
use Ox\Mediboard\MonitoringPatient\CSupervisionTableRow;
use Symfony\Component\HttpFoundation\Response;

/**
 * Table main controller
 */
class SupervisionTableController extends Controller
{
    /**
     * @throws Exception
     */
    public function editTable(RequestParams $params): Response
    {
        $supervision_table_id = $params->get('supervision_table_id', 'ref class|CSupervisionTable', true);

        $table = CSupervisionTable::findOrNew($supervision_table_id);
        $table->loadRefsNotes();

        return $this->renderSmarty(
            "inc_edit_supervision_table",
            [
                "table" => $table,
            ]
        );
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function listTableRows(RequestParams $params): Response
    {
        $supervision_table_id = $params->get('supervision_table_id', 'ref class|CSupervisionTable', true);

        $table = CSupervisionTable::findOrFail($supervision_table_id);

        $rows = $table->loadRefsRows();

        CMbObject::massLoadFwdRef($rows, 'value_type_id');
        CMbObject::massLoadFwdRef($rows, 'value_unit_id');
        foreach ($rows as $row) {
            /** @var CSupervisionTableRow $row */
            $row->loadRefValueType();
            $row->loadRefValueUnit();
        }

        return $this->renderSmarty(
            "inc_list_supervision_table_rows",
            [
                "table" => $table,
                "rows"  => $rows,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function editTableRow(RequestParams $params): Response
    {
        $supervision_table_row_id = $params->get('supervision_table_row_id', 'ref class|CSupervisionTableRow');
        $supervision_table_id     = $params->get('supervision_table_id', 'ref class|CSupervisionTable');

        $row = CSupervisionTableRow::findOrNew($supervision_table_row_id);

        if (!$row->_id) {
            $row->supervision_table_id = $supervision_table_id;
        }

        $row->loadRefValueType();
        $row->loadRefValueUnit();

        return $this->renderSmarty(
            "inc_edit_supervision_table_row",
            [
                "row" => $row,
            ]
        );
    }
}
