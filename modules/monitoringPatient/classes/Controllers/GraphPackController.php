<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphPack;
use Ox\Mediboard\MonitoringPatient\CSupervisionGraphToPack;
use Ox\Mediboard\PlanningOp\CTypeAnesth;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

/**
 * Timed Data main controller
 */
class GraphPackController extends Controller
{
    private const GRAPH_TYPES = [
        "CSupervisionGraph",
        "CSupervisionTimedData",
        "CSupervisionTimedPicture",
        "CSupervisionTable",
        "CSupervisionInstantData",
    ];

    /**
     * @throws Exception
     */
    public function editGraphPack(RequestParams $params): Response
    {
        $supervision_graph_pack_id = $params->get("supervision_graph_pack_id", "ref class|CSupervisionGraphPack");

        $pack = CSupervisionGraphPack::findOrNew($supervision_graph_pack_id);
        $pack->loadRefsNotes();
        $pack->getTimingFields();

        // Liste des types d'anesthésie
        $anesthesia_type  = new CTypeAnesth();
        $anesthesia_types = $anesthesia_type->loadGroupList();

        return $this->renderSmarty(
            "inc_edit_supervision_graph_pack",
            [
                "pack"             => $pack,
                "anesthesia_types" => $anesthesia_types,
            ]
        );
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function editGraphToPack(RequestParams $params): Response
    {
        $supervision_graph_pack_id    = $params->get("supervision_graph_pack_id", "ref class|CSupervisionGraphPack");
        $supervision_graph_to_pack_id = $params->get(
            "supervision_graph_to_pack_id",
            "ref class|CSupervisionGraphToPack"
        );

        $graph_class = $params->get(
            "graph_class",
            "enum list|" . implode("|", self::GRAPH_TYPES) . " default|CSupervisionGraph"
        );

        $group = CGroups::get($this->getRequestContext()->getGroupId());
        $pack  = CSupervisionGraphPack::findOrFail($supervision_graph_pack_id);
        $link  = new CSupervisionGraphToPack();
        if ($supervision_graph_to_pack_id) {
            $link->load($supervision_graph_to_pack_id);
            $link->loadRefsNotes();
            $graph_class = $link->graph_class;
        } else {
            $link->graph_class = $graph_class;
            $link->rank        = 1;
        }

        if ($supervision_graph_pack_id) {
            $link->pack_id = $supervision_graph_pack_id;
        }

        $items = $graph_class::getAllFor($group, false);

        return $this->renderSmarty(
            "inc_edit_supervision_graph_to_pack",
            [
                "pack"  => $pack,
                "items" => $items,
                "link"  => $link,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function listGraphToPack(RequestParams $params): Response
    {
        $supervision_graph_pack_id = $params->get("supervision_graph_pack_id", "ref class|CSupervisionGraphPack");

        $pack = new CSupervisionGraphPack();
        $pack->load($supervision_graph_pack_id);
        $links = $pack->loadRefsGraphLinks();

        foreach ($links as $_link) {
            /** @var CSupervisionGraphToPack $_link */
            $_link->loadRefGraph();
        }

        return $this->renderSmarty(
            "inc_list_supervision_graph_to_pack",
            [
                "pack" => $pack,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function changeGraphToPackRank(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('monitoring_patient_change_graph_pack_rank', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $supervision_graph_to_pack_id = $params->post(
            "supervision_graph_to_pack_id",
            "ref class|CSupervisionGraphToPack"
        );
        $rank                         = $params->post("rank", "num");
        $wish_rank                    = $params->post("wish_rank", "num");

        $old_rank = 0;

        $graph_to_pack = CSupervisionGraphToPack::findOrFail($supervision_graph_to_pack_id);

        if (!$wish_rank) {
            $old_rank = $graph_to_pack->rank;
        }

        $graph_to_pack->rank = $wish_rank ?: $rank;

        try {
            $this->storeObject($graph_to_pack);
        } catch (CMbException $e) {
            $this->addUiMsgError($e->getMessage(), true);

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }

        if ($wish_rank ?: $rank) {
            $other_graph_to_pack = $graph_to_pack->loadGraphToPackByRank($wish_rank ?: $rank);
            if ($other_graph_to_pack->_id) {
                $other_graph_to_pack->rank = $wish_rank ? $rank : $old_rank;

                try {
                    $this->storeObject($other_graph_to_pack);
                } catch (CMbException $e) {
                    $this->addUiMsgError($e->getMessage(), true);

                    return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
                }
            }
        }
        $this->addUiMsgOk("CSupervisionGraphToPack-Modified rank");

        return $this->renderEmptyResponse();
    }
}
