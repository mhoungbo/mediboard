<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;

/**
 * A supervision graph Y axis value label
 */
class CSupervisionGraphAxisValueLabel extends CMbObject
{
    /** @var int  */
    public $supervision_graph_value_label_id;

    /** @var int  */
    public $supervision_graph_axis_id;

    /** @var int  */
    public $value;

    /** @var string  */
    public $title;

    /** @var CStoredObject returns CSupervisionGraphAxis */
    public CStoredObject $_ref_axis;

    /**
     * @see parent::getSpec()
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = "supervision_graph_value_label";
        $spec->key   = "supervision_graph_value_label_id";

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    public function getProps(): array
    {
        $props                              = parent::getProps();
        $props["supervision_graph_axis_id"] = "ref notNull class|CSupervisionGraphAxis cascade back|labels";
        $props["value"]                     = "num notNull";
        $props["title"]                     = "str notNull";

        return $props;
    }

    /**
     * Load axis
     *
     * @param bool $cache Use object cache
     *
     * @return CStoredObject|null
     * @throws Exception
     */
    public function loadRefAxis(bool $cache = true): ?CStoredObject
    {
        return $this->_ref_axis = $this->loadFwdRef("supervision_graph_axis_id", $cache);
    }

    /**
     * @see parent::updateFormFields()
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->title;
    }
}
