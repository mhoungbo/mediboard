<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Files\CFile;

/**
 * A supervision timed data representation
 */
class CSupervisionTimedPicture extends CSupervisionTimedEntity
{
    public const PICTURES_ROOT = "modules/monitoringPatient/images/supervision";

    /** @var int  */
    public $supervision_timed_picture_id;

    /** @var bool  */
    public $in_doc_template;

    /** @var int  */
    public $value_type_id;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = "supervision_timed_picture";
        $spec->key   = "supervision_timed_picture_id";

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props                    = parent::getProps();
        $props["owner_id"]        .= " back|supervision_timed_picture";
        $props["value_type_id"]   = "ref notNull class|CObservationValueType autocomplete|_view dependsOn|datatype back|supervision_timed_pictures";
        $props["in_doc_template"] = "bool notNull default|0";

        return $props;
    }

    /**
     * Load pictures from results
     *
     * @param array[][] $results Patient results
     *
     * @return array
     * @throws Exception
     */
    public function loadTimedPictures(array $results): array
    {
        $type_id = $this->value_type_id;
        if (!isset($results[$type_id]["none"])) {
            return $this->_graph_data = [];
        }

        $data = $results[$type_id]["none"];

        foreach ($data as $_i => $_d) {
            if ($_d["file_id"]) {
                $file = new CFile();
                $file->load($_d["file_id"]);
                $data[$_i]["file"] = $file;
            }
        }

        return $this->_graph_data = $data;
    }

    /**
     * Get all the timed data for an object
     *
     * @param CMbObject $object The object to get timed data of
     *
     * @return array
     * @throws Exception
     */
    public static function getAllFor(CMbObject $object): array
    {
        $graph = new self();

        $where = [
            "owner_class" => "= '$object->_class'",
            "owner_id"    => "= '$object->_id'",
        ];

        return $graph->loadList($where, "title");
    }
}
