<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Etablissement\CGroups;

/**
 * A supervision graph
 */
class CSupervisionTimedEntity extends CMbObject implements ISupervisionTimelineItem
{
    /** @var string */
    public $owner_class;

    /** @var int */
    public $owner_id;

    /** @var string */
    public $title;

    /** @var bool */
    public $disabled;

    /** @var CMbObject */
    public $_ref_owner;

    /** @var array */
    public $_graph_data = [];

    /**
     * @see parent::getSpec()
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec                   = parent::getSpec();
        $spec->uniques["title"] = ["owner_class", "owner_id", "title"];

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    public function getProps(): array
    {
        $props                = parent::getProps();
        $props["owner_class"] = "enum notNull list|CGroups";
        $props["owner_id"]    = "ref notNull meta|owner_class class|CMbObject index|1";
        $props["title"]       = "str notNull index|1";
        $props["disabled"]    = "bool notNull default|1";

        return $props;
    }

    /**
     * Load the owner entity
     *
     * @param bool $cache Use object cache
     *
     * @return CGroups
     * @throws Exception
     */
    public function loadRefOwner(bool $cache = true): CStoredObject
    {
        return $this->_ref_owner = $this->loadFwdRef("owner_id", $cache);
    }

    /**
     * @see parent::updateFormFields()
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->title;
    }

    /**
     * Get identifier for the timeline
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->_guid;
    }

    /**
     * Get data for the timeline
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->_graph_data;
    }

    /**
     * @inheritdoc
     */
    public function getJsonFields(): array
    {
        $fields   = parent::getJsonFields();
        $fields[] = "_graph_data";

        return $fields;
    }
}
