<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbArray;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;

/**
 * A supervision graph Y axis
 */
class CSupervisionGraphAxis extends CMbObject
{
    /** @var int */
    public $supervision_graph_axis_id;

    /** @var int */
    public $supervision_graph_id;

    /** @var string */
    public $title;

    /** @var bool */
    public $actif;

    /** @var float */
    public $limit_low;

    /** @var float */
    public $limit_high;

    /** @var string */
    public $display;

    /** @var bool */
    public $show_points;

    /** @var bool */
    public $in_doc_template;

    /** @var string */
    public $symbol;

    /** @var CSupervisionGraphSeries[] */
    public array $_ref_series;

    /** @var CSupervisionGraphAxisValueLabel[] */
    public array $_ref_labels;

    /** @var CStoredObject return CSupervisionGraph */
    public CStoredObject $_ref_graph;

    /** @var array */
    public array $_labels = [];

    public static array $default_yaxis = [
        "position"     => "left",
        "labelWidth"   => 64, // FIXME
        "ticks"        => 6,
        "reserveSpace" => true,
        "label"        => "",
        "symbolChar"   => "",
        "axis_id"      => null,
        "color"        => null,
        "tickLength"   => 3,
    ];

    private static array $symbol_chars = [
        "circle"   => "&#x25CB;",
        "cross"    => "x",
        "diamond"  => "&#x25CA;",
        "square"   => "&#x25A1;",
        "triangle" => "&#x25B3;",
    ];

    /**
     * @see parent::getSpec()
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = "supervision_graph_axis";
        $spec->key   = "supervision_graph_axis_id";

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    public function getProps(): array
    {
        $props                         = parent::getProps();
        $props["supervision_graph_id"] = "ref notNull class|CSupervisionGraph cascade back|axes";
        $props["title"]                = "str notNull";
        $props["actif"]                = "bool default|1";
        $props["limit_low"]            = "float"; // null => auto
        $props["limit_high"]           = "float"; // null => auto
        $props["display"]              = "enum list|points|lines|bars|stack|bandwidth";
        $props["show_points"]          = "bool notNull default|1";
        $props["in_doc_template"]      = "bool notNull default|0";
        $props["symbol"]               = "enum notNull list|circle|square|diamond|cross|triangle";

        return $props;
    }

    /**
     * Get an HTML representation of the symbol char
     *
     * @return string The HTML symbol char
     * @throws Exception
     */
    public function getSymbolChar(): ?string
    {
        $this->completeField("symbol", "show_points", "display");

        if (!$this->show_points && $this->display !== "points") {
            return null;
        }

        return CMbArray::get(self::$symbol_chars, $this->symbol);
    }

    /**
     * Get Flot-formatted axis data
     *
     * @param int $count_yaxes Number of axes to build
     *
     * @return array
     * @throws Exception
     */
    public function getAxisForFlot(int $count_yaxes): array
    {
        $axis_data = [
                "symbolChar" => $this->getSymbolChar(),
                "label"      => $this->title,
                "min"        => null,
                "max"        => null,
                "axis_id"    => $this->_id,
            ] + self::$default_yaxis;

        $labels = $this->loadRefsLabels();
        if (count($labels) > 0) {
            $ticks = [];
            foreach ($labels as $_label) {
                $ticks[] = [floatval($_label->value), $_label->title];
            }
            $axis_data["ticks"] = $ticks;
        } else {
            /** @var CSupervisionGraph $graph */
            $graph = $this->loadRefGraph();
            $height             = $graph->height;
            $axis_data["ticks"] = round($height / 25);
        }

        if ($count_yaxes) {
            $axis_data["alignTicksWithAxis"] = 1;
        }

        if ($this->limit_low != null) {
            $axis_data["min"] = $this->limit_low;
        }

        if ($this->limit_high != null) {
            $axis_data["max"] = $this->limit_high;
        }

        return $axis_data;
    }

    /**
     * Load the graph
     *
     * @param bool $cache Use object cache
     *
     * @return CStoredObject|null
     * @throws Exception
     */
    public function loadRefGraph(bool $cache = true): ?CStoredObject
    {
        return $this->_ref_graph = $this->loadFwdRef("supervision_graph_id", $cache);
    }

    /**
     * Load series
     *
     * @return CStoredObject[]
     * @throws Exception
     */
    public function loadRefsSeries(): array
    {
        return $this->_ref_series = $this->loadBackRefs("series");
    }

    /**
     * Load value labels
     *
     * @return CSupervisionGraphAxisValueLabel[]
     * @throws Exception
     */
    public function loadRefsLabels(): array
    {
        /** @var CSupervisionGraphAxisValueLabel[] $ref_labels */
        $ref_labels = $this->loadBackRefs("labels");

        $labels = [];
        foreach ($ref_labels as $_label) {
            $labels[$_label->value] = $_label->title;
        }
        $this->_labels = $labels;

        return $this->_ref_labels = $ref_labels;
    }

    /**
     * @see parent::updateFormFields()
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->title;
    }
}
