<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Mediboard\PlanningOp\COperation;

/**
 * A supervision graph pack
 */
class CSupervisionGraphPack extends CMbObject
{
    /** @var int  */
    public $supervision_graph_pack_id;

    /** @var string  */
    public $owner_class;

    /** @var int  */
    public $owner_id;

    /** @var string  */
    public $title;

    /** @var bool  */
    public $disabled;

    /** @var string  */
    public $timing_fields;

    /** @var string  */
    public $use_contexts;

    /** @var string  */
    public $planif_display_mode;

    /** @var int  */
    public $anesthesia_type;

    /** @var bool  */
    public $main_pack;

    /** @var bool  */
    public $_protocol_md_stream = false;

    /** @var CSupervisionGraphToPack[] */
    public array $_ref_graph_links;

    /** @var CStoredObject return CGroups|CFunction */
    public CStoredObject $_ref_owner;

    /** @var CStoredObject return CTypeAnesth */
    public CStoredObject $_ref_anesthesia_type;

    /** @var array */
    public array $_timing_fields;

    /** @var array */
    public array $_timing_values;

    public static array $operation_timing_fields = [
        "debut_prepa_preop",
        "fin_prepa_preop",
        "entree_salle",
        "sortie_salle",
        "remise_chir",
        "tto",
        "pose_garrot",
        "prep_cutanee",
        "debut_op",
        "fin_op",
        "retrait_garrot",
        "entree_reveil",
        "sortie_reveil_possible",
        "sortie_reveil_reel",
        "induction_debut",
        "induction_fin",
        "suture_fin",
        "entree_bloc",
        "cleaning_start",
        "cleaning_end",
        "installation_start",
        "installation_end",
        "incision",
    ];

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec                   = parent::getSpec();
        $spec->table            = "supervision_graph_pack";
        $spec->key              = "supervision_graph_pack_id";
        $spec->uniques["title"] = ["owner_class", "owner_id", "title"];

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props                        = parent::getProps();
        $props["owner_class"]         = "enum notNull list|CGroups";
        $props["owner_id"]            = "ref notNull meta|owner_class class|CMbObject back|supervision_graph_packs";
        $props["title"]               = "str notNull index|1";
        $props["disabled"]            = "bool notNull default|1";
        $props["timing_fields"]       = "text";
        $props["use_contexts"]        = "set list|preop|perop|sspi|parto|post_partum";
        $props["planif_display_mode"] = "enum list|token|in_place default|token";
        $props['anesthesia_type']     = 'ref class|CTypeAnesth back|grah_pack_anesthesia_types';
        $props['main_pack']           = 'bool default|0';

        return $props;
    }

    /**
     * Load graph links
     *
     * @return CStoredObject[]|null
     * @throws Exception
     */
    public function loadRefsGraphLinks(): array
    {
        return $this->_ref_graph_links = $this->loadBackRefs("graph_links", "rank, supervision_graph_to_pack_id");
    }

    /**
     * @see parent::updateFormFields()
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->title;
    }

    /**
     * Load owner
     *
     * @return CStoredObject
     * @throws Exception
     */
    public function loadRefOwner(): CStoredObject
    {
        return $this->_ref_owner = $this->loadFwdRef("owner_id");
    }

    /**
     * Load the anesthesia type
     *
     * @return CStoredObject|null
     * @throws Exception
     */
    public function loadRefAnesthesiaType(): ?CStoredObject
    {
        return $this->_ref_anesthesia_type = $this->loadFwdRef('anesthesia_type');
    }

    /**
     * Get all pakcs from an object
     *
     * @param CMbObject   $object        The object to get the packs of
     * @param bool        $with_disabled List disabled items
     * @param string|null $context       Use context
     *
     * @return CSupervisionGraphPack[]
     * @throws Exception
     */
    public static function getAllFor(CMbObject $object, bool $with_disabled = false, string $context = null): array
    {
        $pack = new self();

        $where = [
            "owner_class" => "= '$object->_class'",
            "owner_id"    => "= '$object->_id'",
        ];

        if (!$with_disabled) {
            $where["disabled"] = "= '0'";
        }

        if ($context) {
            $where[] = "use_contexts IS NULL OR use_contexts " . $pack->getDS()->prepareLike("%$context%");
        }

        return $pack->loadList($where, "title");
    }

    /**
     * @return array
     */
    public function getTimingFields(): array
    {
        if (!$this->timing_fields) {
            return $this->_timing_fields = [];
        }

        return $this->_timing_fields = json_decode($this->timing_fields, true);
    }

    /**
     * Tells if it can be used in the specified context
     *
     * @param string $context Context
     *
     * @return bool
     */
    public function canUseInContext(string $context): bool
    {
        if (!$this->use_contexts) {
            return true;
        }

        $contexts = explode("|", $this->use_contexts);

        return in_array($context, $contexts);
    }

    /**
     * Get timing values
     *
     * @param COperation $interv Intervention to get timings of
     *
     * @return array
     */
    public function getTimingValues(COperation $interv): array
    {
        $fields = $this->getTimingFields();

        $timings = [];

        foreach ($fields as $_field => $_color) {
            $timings[] = [
                "field" => $_field,
                "label" => CAppUI::tr("$interv->_class-$_field"),
                "value" => $interv->{$_field},
                "color" => $_color,
            ];
        }

        return $this->_timing_values = $timings;
    }

    /**
     * Check if the graph is on the MD Stream protocol
     *
     * @return bool
     * @throws Exception
     */
    public function isProtocolMDStream(): bool
    {
        $is_csharp_version = false;

        $graph_links = $this->loadRefsGraphLinks();
        CStoredObject::massLoadFwdRef($graph_links, "graph_id");

        foreach ($graph_links as $_gl) {
            /** @var CSupervisionGraphToPack $_gl */
            $_go = $_gl->loadRefGraph();

            if ($_go->disabled) {
                continue;
            }

            if (
                $_go instanceof CSupervisionGraph
                && ($_go->automatic_protocol === CSupervisionGraph::PROTOCOL_MD_STREAM)
            ) {
                $is_csharp_version = true;
                break;
            }

            if (
                $_go instanceof CSupervisionTable
                && ($_go->automatic_protocol === CSupervisionGraph::PROTOCOL_MD_STREAM)
            ) {
                $is_csharp_version = true;
                break;
            }
        }

        return $this->_protocol_md_stream = $is_csharp_version;
    }
}
