<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbException;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\ObservationResult\CObservationValueCodingSystem;
use Ox\Mediboard\ObservationResult\CObservationValueToConstant;

/**
 * Param Surveillance Service
 */
class ParamSurveillanceService
{
    protected CGroups $group;

    protected int $start;

    private const PAGINATION_STEP = 30;

    /**
     * @throws CMbException
     */
    public function __construct(CGroups $group, int $start = 0)
    {
        if (!$group->_id) {
            throw new CMbException("ParamSurveillance-error-group id is null");
        }
        if ($start < 0) {
            throw new CMbException("ParamSurveillance-error-start not valid");
        }
        $this->group = $group;
        $this->start = $start;
    }

    /**
     * @throws Exception
     */
    public function loadSurveillancePage(string $object_class = "CObservationValueType"): array
    {
        /** @var CObservationValueCodingSystem $param */
        $param = new $object_class();
        $ds    = $param->getDS();

        $limit = "$this->start," . self::PAGINATION_STEP;

        $where = [
            "group_id" => $ds->prepare(" = ?", $this->group->_id) . " OR group_id IS NULL",
        ];

        $params       = $param->loadList($where, "coding_system, code", $limit);
        $params_total = $param->countList($where);

        return [$params, $params_total];
    }

    /**
     * @throws Exception
     */
    public function listConversionPage(): array
    {
        $conversion = new CObservationValueToConstant();
        $ljoin      = [
            'observation_value_type' =>
                'observation_value_type.observation_value_type_id = observation_values_to_constant.value_type_id',
        ];
        $where      = [
            "observation_value_type.group_id = " . $this->group->_id . " OR observation_value_type.group_id IS NULL",
        ];

        $total       = $conversion->countList($where, 'observation_value_to_constant_id', $ljoin);
        $conversions = $conversion->loadList(
            $where,
            null,
            "$this->start," . self::PAGINATION_STEP,
            'observation_value_to_constant_id',
            $ljoin
        );

        return [$conversions, $total];
    }
}
