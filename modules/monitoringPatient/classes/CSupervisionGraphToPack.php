<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;

/**
 * A supervision graph
 */
class CSupervisionGraphToPack extends CMbObject
{
    /** @var int  */
    public $supervision_graph_to_pack_id;

    /** @var string  */
    public $graph_class;

    /** @var int  */
    public $graph_id;

    /** @var int  */
    public $pack_id;

    /** @var int  */
    public $rank;

    /** @var CStoredObject
     * return CSupervisionTimedEntity|CSupervisionGraph|CSupervisionTimedData|CSupervisionTimedPicture
     */
    public CStoredObject $_ref_graph;

    /** @var CStoredObject return CSupervisionGraphPack */
    public CStoredObject $_ref_pack;

    private const GRAPH_CLASSES = [
        "CSupervisionGraph",
        "CSupervisionTimedData",
        "CSupervisionTimedPicture",
        "CSupervisionInstantData",
        "CSupervisionTable",
    ];

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec                   = parent::getSpec();
        $spec->table            = "supervision_graph_to_pack";
        $spec->key              = "supervision_graph_to_pack_id";
        $spec->uniques["title"] = ["graph_class", "graph_id", "pack_id"];

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props                = parent::getProps();
        $props["graph_class"] = "enum list|" . implode("|", self::GRAPH_CLASSES);
        $props["graph_id"]
              = "ref notNull class|CSupervisionTimedEntity meta|graph_class cascade back|pack_links index|1";
        $props["pack_id"]     = "ref notNull class|CSupervisionGraphPack back|graph_links";
        $props["rank"]        = "num notNull";

        return $props;
    }

    /**
     * Load graph to pack object by his rank
     *
     * @param int $rank Rank object
     *
     * @return CSupervisionGraphToPack
     * @throws Exception
     */
    public function loadGraphToPackByRank(int $rank): CSupervisionGraphToPack
    {
        $graph_to_pack = new self();

        $where = [
            "pack_id" => $graph_to_pack->getDS()->prepare(" = ?", $this->pack_id),
            "rank"    => $graph_to_pack->getDS()->prepare(" = ?", $rank),
        ];


        $graph_to_pack->loadObject($where);

        return $graph_to_pack;
    }

    /**
     * Get the graph
     *
     * @return CStoredObject
     * @throws Exception
     */
    public function loadRefGraph(): CStoredObject
    {
        return $this->_ref_graph = $this->loadFwdRef("graph_id");
    }

    /**
     * Get the pack
     *
     * @return CStoredObject
     * @throws Exception
     */
    public function loadRefPack(): CStoredObject
    {
        return $this->_ref_pack = $this->loadFwdRef("pack_id");
    }
}
