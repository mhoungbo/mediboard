<?php

/**
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\MonitoringPatient;

use Exception;
use Ox\Core\CRequest;
use Ox\Core\CSetup;
use Ox\Core\CSQLDataSource;
use Ox\Core\Module\CModule;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * @codeCoverageIgnore
 */
class CSetupMonitoringPatient extends CSetup
{
    /**
     * @return bool
     * @throws Exception|InvalidArgumentException
     */
    protected function addMonitoringPatientPerm(): bool
    {
        $patient = CModule::getActive("dPpatients");

        if (!$patient) {
            return true;
        }

        $ds = CSQLDataSource::get("std");

        $request = new CRequest();
        $request->addSelect("mod_id");
        $request->addTable("modules");
        $request->addWhere(["mod_name" => "= 'monitoringPatient'"]);

        $mod_id_monitoring_patient = $ds->loadResult($request->makeSelect());

        if (!$mod_id_monitoring_patient) {
            return true;
        }

        $mod_id_patient = $patient->_id;

        $query = "INSERT INTO `perm_module` (`user_id`, `mod_id`, `permission`, `view`)
                SELECT `user_id`, '$mod_id_monitoring_patient', `permission`, `view`
                FROM `perm_module`
                WHERE `mod_id` = '$mod_id_patient'";

        $ds->exec($query);

        return true;
    }

    /**
     * @see parent::__construct()
     */
    public function __construct()
    {
        parent::__construct();

        $this->mod_name = "monitoringPatient";
        $this->makeRevision("0.0");

        $this->makeRevision("0.01");

        $this->setModuleCategory("parametrage", "metier");

        if (CModule::getActive("dPpatients")) {
            $this->addMethod("addMonitoringPatientPerm");
        }

        $this->makeEmptyRevision('0.02');

        $this->makeRevision("0.03");
        $this->addDependency("dPpatients", "3.85");

        $query = "ALTER TABLE `supervision_graph`
                CHANGE `automatic_protocol` `automatic_protocol` ENUM ('Kheops-Concentrator', 'MD-Stream');";
        $this->addQuery($query, true);

        $this->makeRevision("0.04");
        $query = "ALTER TABLE `supervision_tables` 
                CHANGE `automatic_protocol` `automatic_protocol` ENUM ('Kheops-Concentrator', 'MD-Stream');";
        $this->addQuery($query, true);

        $this->makeRevision("0.05");
        $query = "ALTER TABLE `supervision_graph` 
                ADD INDEX owner (owner_class, owner_id),
                ADD INDEX (`title`);";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_graph_axis` 
                CHANGE `show_points` `show_points` ENUM ('0','1') NOT NULL DEFAULT '1';";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_graph_value_label` 
                CHANGE `supervision_graph_axis_id` `supervision_graph_axis_id` INT (11) UNSIGNED NOT NULL;";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_graph_pack` 
                CHANGE `use_contexts` `use_contexts` TEXT;";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_graph_pack` 
                ADD INDEX (`title`),
                ADD INDEX (`anesthesia_type`);";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_graph_series` 
                CHANGE `color` `color` VARCHAR (6) NOT NULL,
                CHANGE `integer_values` `integer_values` ENUM ('0','1') NOT NULL DEFAULT '0';";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_graph_to_pack` 
                ADD INDEX graph (graph_class, graph_id);";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_instant_data` 
                CHANGE `owner_id` `owner_id` INT (11) UNSIGNED NOT NULL,
                CHANGE `value_type_id` `value_type_id` INT (11) UNSIGNED NOT NULL,
                CHANGE `value_unit_id` `value_unit_id` INT (11) UNSIGNED NOT NULL,
                CHANGE `size` `size` TINYINT (4) UNSIGNED NOT NULL;";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_instant_data` 
                ADD INDEX (`title`);";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_tables` 
                CHANGE `owner_id` `owner_id` INT (11) UNSIGNED NOT NULL,
                CHANGE `sampling_frequency` `sampling_frequency` ENUM ('1','3','5','10','15');";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_tables` 
                ADD INDEX owner (owner_class, owner_id),
                ADD INDEX (`title`);";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_table_rows` 
                CHANGE `active` `active` ENUM ('0','1') DEFAULT '1',
                CHANGE `value_type_id` `value_type_id` INT (11) UNSIGNED NOT NULL,
                CHANGE `value_unit_id` `value_unit_id` INT (11) UNSIGNED NOT NULL;";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_table_rows` 
                ADD INDEX (`supervision_table_id`),
                ADD INDEX (`value_type_id`),
                ADD INDEX (`value_unit_id`);";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_timed_data` 
                CHANGE `type` `type` ENUM ('str','bool','enum','set') NOT NULL DEFAULT 'str';";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_timed_data` 
                ADD INDEX owner (owner_class, owner_id),
                ADD INDEX (`title`);";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_timed_picture` 
                CHANGE `owner_id` `owner_id` INT (11) UNSIGNED NOT NULL;";
        $this->addQuery($query, true);

        $query = "ALTER TABLE `supervision_timed_picture` 
                ADD INDEX (`title`),
                ADD INDEX (`value_type_id`);";
        $this->addQuery($query, true);

        $this->mod_version = "0.06";
    }
}
