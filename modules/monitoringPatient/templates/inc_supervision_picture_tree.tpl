{{*
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=id value=$name|md5}}

<div style="padding: 1px; padding-left: {{$depth*12}}px;">
  {{if $name|is_dir}}
    <img src="modules/ftp/images/directory.png" alt=""/>
    {{$name|basename}}
  {{else}}
    <img src="{{$name}}" style="height: 25px;" onmouseover="ObjectTooltip.createDOM(this, 'img-{{$id}}', {duration: 1})"  alt=""/>
    <img src="{{$name}}" style="display: none;" id="img-{{$id}}" alt=""/>
    <form name="select-picture-{{$id}}" method="post" onsubmit="return onSubmitFormAjax(this, Control.Modal.close)">
        {{mb_route name=monitoringPatient_gui_select_picture}}
        {{csrf_token id=monitoring_patient_select_picture}}
      <input type="hidden" name="path" value="{{$name}}" />
      <input type="hidden" name="timed_picture_id" value="{{$timed_picture_id}}" />

      <span onclick="this.up('form').onsubmit()" style="cursor: pointer">
        {{$name|basename}}
      </span>
    </form>
  {{/if}}
</div>

{{if $_subtree|@is_array}}
  {{foreach from=$tree key=_name item=_subtree}}
    {{mb_include module=monitoringPatient template=inc_supervision_picture_tree tree=$_subtree name=$_name depth=$depth+1}}
  {{/foreach}}
{{/if}}
