{{*
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=monitoringPatient script=supervision_graph          ajax=1}}
{{mb_script module=monitoringPatient script=supervision_graph_defaults ajax=1}}
{{mb_script module=files             script=file                       ajax=1}}

<script>
    Main.add(function () {
            SupervisionGraph.list_url               = '{{url name=monitoringPatient_gui_supervision_list}}';
            SupervisionGraph.edit_graph_url         = '{{url name=monitoringPatient_gui_edit_graph}}';
            SupervisionGraph.edit_table_url         = '{{url name=monitoringPatient_gui_edit_table}}';
            SupervisionGraph.edit_timed_data_url    = '{{url name=monitoringPatient_gui_edit_timed_data}}';
            SupervisionGraph.edit_timed_picture_url = '{{url name=monitoringPatient_gui_edit_timed_picture}}';
            SupervisionGraph.edit_instant_data_url  = '{{url name=monitoringPatient_gui_edit_instant_data}}';
            SupervisionGraph.edit_pack_url          = '{{url name=monitoringPatient_gui_edit_pack}}';
            SupervisionGraph.list(SupervisionGraph.editGraph({{$supervision_graph_id}}));
        }
    );
</script>

<table class="main layout">
  <tr>
    <td style="width: 400px;" id="supervision-list"></td>
    <td id="supervision-graph-editor" class="me-padding-left-4 me-padding-right-4">&nbsp;</td>
  </tr>
</table>
