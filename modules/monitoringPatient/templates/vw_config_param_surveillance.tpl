{{*
 * @package Mediboard\MonitoringPatient
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{if !"observationResult"|module_active}}
  <div class="small-warning">
      {{tr}}monitoringPatient-warning-observationResult not active{{/tr}}
  </div>
    {{mb_return}}
{{/if}}

{{mb_script module=monitoringPatient script=param_surveillance ajax=1}}
<div class="small-warning">
  {{tr}}monitoringPatient-message-config params warning message{{/tr}}
</div>

<script>
  Main.add(function () {
    Control.Tabs.create("main_tabs_types", true);
    ParamSurveillance.list_url = '{{url name=monitoringPatient_gui_surveillance_list}}';
    ParamSurveillance.edit_url = '{{url name=monitoringPatient_gui_surveillance_edit}}';
    ParamSurveillance.list_conversion_url = '{{url name=monitoringPatient_gui_surveillance_list_conversion}}';
    ParamSurveillance.edit_conversion_url = '{{url name=monitoringPatient_gui_surveillance_edit_conversion}}';
    ParamSurveillance.list('CObservationValueType');
    ParamSurveillance.list('CObservationValueUnit');
    ParamSurveillance.listConversion();
  });
</script>

<ul id="main_tabs_types" class="control_tabs">
  <li><a href="#list-CObservationValueType">{{tr}}CObservationValueType{{/tr}}</a></li>
  <li><a href="#list-CObservationValueUnit">{{tr}}CObservationValueUnit{{/tr}}</a></li>
  <li><a href="#list-CObservationValueToConstant">{{tr}}CObservationValueToConstant{{/tr}}</a></li>
</ul>

<div id="list-CObservationValueType" style="display: none;"></div>
<div id="list-CObservationValueUnit" style="display: none;"></div>
<div id="list-CObservationValueToConstant" style="display: none;"></div>
