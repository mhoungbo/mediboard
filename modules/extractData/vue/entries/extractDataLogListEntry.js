import ExtractDataLogList from "@modules/extractData/vue/views/ExtractDataLogList/ExtractDataLogList.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("ExtractDataLogList", ExtractDataLogList)
