/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"

export default class ExtractDataLog extends OxObject {
    constructor () {
        super()
        this.type = "extract_data_log"
    }

    protected _relationsTypes = {
        mediuser: Mediuser
    }

    get mediuser (): Mediuser | undefined {
        return this.loadForwardRelation<Mediuser>("mediuser")
    }

    set mediuser (value: Mediuser | undefined) {
        this.setForwardRelation("mediuser", value)
    }

    get fileName (): OxAttr<string> {
        return super.get("file_name")
    }

    set fileName (value: OxAttr<string>) {
        super.set("file_name", value)
    }

    get datetime (): OxAttr<string> {
        return super.get("datetime")
    }

    set datetime (value: OxAttr<string>) {
        super.set("datetime", value)
    }

    get userId (): OxAttr<number> {
        return super.get("user_id")
    }

    set userId (value: OxAttr<number>) {
        super.set("user_id", value)
    }

    get profile (): OxAttr<string> {
        return super.get("profile")
    }

    set profile (value: OxAttr<string>) {
        super.set("profile", value)
    }

    get content (): OxAttr<string> {
        return super.get("content")
    }

    set content (value: OxAttr<string>) {
        super.set("content", value)
    }
}
