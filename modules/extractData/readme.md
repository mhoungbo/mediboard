# ExtractData module

The principle being to submit a file (pdf document, identity card, passport, document with qrcode...), to carry out the
necessary pre-processing on the image, then to carry out an analysis of the OCR (Optical Character Recognition), then to
format the return of the OCR to produce a response.

This module offer multiple decoding services :

- MRZ (identity card, passport...)
- QRCode (decode a QRCode)
- ID picture (from identity card, passport...)
- Content (extract text content from document)

This module offers an API and logging of all processing.

# Requirements

- PHP server with PHP <= 7.4
- PHP GD extension enabled
- Tika server with Tesseract
- Modules required : Core modules + Files + Mediusers

# Example of MRZ decoding (ID card)

![MRZ](modules/extractData/resources/Images/mrz.png)

Return of this extract will be :

```json
{
  "nom_jeune_fille": "SAMPLE",
  "prenom": "SANDRA",
  "naissance": "830103",
  "sexe": "F",
  "end_validity": "2010-03-06"
}
```
