<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData;

use Ox\Core\Module\AbstractTabsRegister;

/**
 * @codeCoverageIgnore
 */
class CTabsExtractData extends AbstractTabsRegister
{
    public function registerAll(): void
    {
        $this->registerFile('vwExtractor', TAB_READ);
        $this->registerFile('vwExtractorState', TAB_READ);
        $this->registerFile('vwExtractorTest', TAB_READ);
        $this->registerFile('displayLogs', TAB_READ);

        $this->registerFile('configure', TAB_ADMIN, self::TAB_CONFIGURE);
    }
}
