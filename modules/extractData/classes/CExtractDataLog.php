<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Files\Extractor;

class CExtractDataLog extends CStoredObject
{
    public const RESOURCE_TYPE = 'extract_data_log';

    public const RELATION_MEDIUSER = 'mediuser';

    /** @var int Primary key */
    public $extract_data_log_id;

    /** @var string */
    public $file_name;

    /** @var string */
    public $datetime;

    /** @var int */
    public $user_id;

    /** @var string */
    public $content;

    /** @var string */
    public $profile;

    public string $_datetime_min;

    public string $_datetime_max;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table    = 'extract_data_log';
        $spec->key      = 'extract_data_log_id';
        $spec->loggable = false;

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['file_name'] = 'str notNull fieldset|' . self::FIELDSET_DEFAULT;
        $props['datetime']  = 'dateTime notNull fieldset|' . self::FIELDSET_DEFAULT;
        $props['user_id']   = 'ref class|CMediusers notNull cascade back|extract_data_logs fieldset|'
            . self::FIELDSET_DEFAULT;
        $props['content']   = 'text notNull seekable fieldset|' . self::FIELDSET_EXTRA;
        $props["profile"]   = "enum notNull list|" .
            implode(
                '|',
                Extractor::getProfiles()
            ) . ' fieldset|' . self::FIELDSET_DEFAULT;

        $props["_datetime_min"] = "dateTime";
        $props["_datetime_max"] = "dateTime";

        return $props;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function store(): ?string
    {
        $this->datetime = 'now';

        $this->purgeProbability();

        return parent::store();
    }

    /**
     * @param string $file_name
     * @param string $content
     * @param string $profile
     * @param int    $user_id
     *
     * @return void
     * @throws Exception
     */
    public function log(string $file_name, string $content, string $profile, int $user_id): void
    {
        if (!$this->canLog()) {
            return;
        }

        $log            = new self();
        $log->file_name = $file_name;
        $log->content   = $content;
        $log->profile   = $profile;
        $log->user_id   = $user_id;

        if ($msg = $log->store()) {
            throw new Exception($msg);
        }
    }

    /**
     * @throws Exception
     */
    private function purgeProbability(): void
    {
        CApp::doProbably(
            CAppUI::conf("extractData logs purge_probability"),
            function (): void {
                $limit = CAppUI::conf("extractData logs purge_delay");
                $this->purgeLogs($limit);
            }
        );
    }

    /**
     * @throws Exception
     */
    private function purgeLogs(int $limit): void
    {
        $extract_data     = new self();
        $date_limit       = CMbDT::dateTime("-{$limit} days");
        $extract_data_ids = $extract_data->loadIds(
            [
                'datetime' => $this->getDS()->prepare("< ?", $date_limit),
            ]
        );

        if ($msg = $extract_data->deleteAll($extract_data_ids)) {
            throw new Exception("Error when purge extract data logs : {$msg}");
        } else {
            CApp::log(sprintf("Suppression de %d extract data logs", count($extract_data_ids)));
        }
    }

    /**
     * @throws Exception
     */
    private function canLog(): bool
    {
        return (CAppUI::conf("extractData logs active") === "1");
    }

    /**
     * @throws ApiException
     * @throws Exception
     */
    public function getResourceMediuser(): Item
    {
        return new Item($this->loadFwdRef('user_id', true));
    }
}
