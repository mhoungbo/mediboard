<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Interpreters;

use Ox\Mediboard\Files\Extractor;

/**
 * Crop face picture from global image
 */
class IdImageCropper
{
    private const IMG_JPEG = "image/jpeg";
    private const IMG_PNG  = "image/png";
    private const IMG_GIF  = "image/gif";

    private array $image_ratios = [
        Extractor::PROFILE_MRZ        => [
            "start_x"   => 0.03,
            "start_y"   => 0.2,
            "width"     => 0.29,
            "height"    => 0.5,
            "new_width" => 200,
            "ratio"     => 1.4,
        ],
        Extractor::PROFILE_ID_PICTURE => [
            "start_x"   => 0.03,
            "start_y"   => 0.2,
            "width"     => 0.29,
            "height"    => 0.5,
            "new_width" => 200,
            "ratio"     => 1.4,
        ],
        "passport"                    => [
            "start_x"   => 0.02,
            "start_y"   => 0.59,
            "width"     => 0.28,
            "height"    => 0.25,
            "new_width" => 200,
            "ratio"     => 1.5,
        ],
        "residence_permit"            => [
            "start_x" => false,
            "ratio"   => 0.78,
        ],
    ];

    public string $filepath;
    public string $file_type;
    public string $mime;

    public $image_resource;
    public $image_face;

    /**
     * IdImageCropper constructor.
     *
     * @param string $filepath  Image filepath
     * @param string $file_type Image category
     */
    public function __construct(string $filepath, string $file_type)
    {
        $this->filepath  = $filepath;
        $this->file_type = $file_type;
        if (!$this->setImageResource()) {
            return false;
        }

        return $this;
    }

    /**
     * Extract face picture from an image
     *
     * @return resource|bool
     */
    public function getFacePicture()
    {
        if (
            !isset($this->image_ratios[$this->file_type])
            || !$this->image_ratios[$this->file_type]["start_x"]
        ) {
            return false;
        }
        $ratio = $this->image_ratios[$this->file_type];

        $img = $this->image_resource;

        $width   = imagesx($img);
        $height  = imagesy($img);
        $start_x = (int) ($width * $ratio["start_x"]);
        $start_y = (int) ($height * $ratio["start_y"]);
        $width   = (int) ($width * $ratio["width"]);
        $height  = (int) ($height * $ratio["height"]);

        $new_width  = $ratio["new_width"];
        $new_height = (int) ($height * ($new_width / $width));

        $returned_image = imagecreatetruecolor($new_width, $new_height);

        // crop the image with the given dimensions
        imagecopyresized(
            $returned_image,
            $img,
            0,
            0,
            $start_x,
            $start_y,
            $new_width,
            $new_height,
            $width,
            $height
        );

        return $this->image_face = $returned_image;
    }

    /**
     * Get the base64 face cropped image
     *
     * @return string|bool
     */
    public function getFaceBase64()
    {
        if (!$this->getFacePicture()) {
            return false;
        }

        return $this->getBase64($this->image_face);
    }

    /**
     * Get the base64 image
     *
     * @param Resource $image The image to return (default : self:image_resource)
     *
     * @return string
     */
    public function getBase64($image = null): string
    {
        if (!$image) {
            $image = $this->image_resource;
        }

        ob_start();
        $this->genImage($image);
        $image_content = ob_get_contents();
        ob_end_clean();

        return base64_encode($image_content);
    }

    /**
     * Give the Png, Gif or Jpeg Image resource
     *
     * @return bool|resource
     */
    public function setImageResource()
    {
        $image_info = getimagesize($this->filepath); // [] if you don't have exif you could use getImageSize()
        if (!isset($image_info["mime"])) {
            return false;
        }
        $this->mime     = $image_info["mime"];
        $image_resource = false;

        if ($image_info["mime"] === self::IMG_JPEG) {
            $image_resource = imagecreatefromjpeg($this->filepath);
        } elseif ($image_info["mime"] === self::IMG_GIF) {
            $image_resource = imagecreatefromgif($this->filepath);
        } elseif ($image_info["mime"] === self::IMG_PNG) {
            $image_resource = imagecreatefrompng($this->filepath);
        }

        return $this->image_resource = $image_resource;
    }

    /**
     * Remove the useless spaces
     *
     * @return bool
     */
    public function cropSpaces(): bool
    {
        // crop white spaces
        $this->image_resource = imagecropauto($this->image_resource, IMG_CROP_THRESHOLD, 0.95, 16777215);

        // crop black spaces
        $this->image_resource = imagecropauto($this->image_resource, IMG_CROP_THRESHOLD, 0.9, 0);

        // crop gray spaces
        $this->image_resource = imagecropauto($this->image_resource, IMG_CROP_THRESHOLD, 0.7, 6710886);

        return $this->genImage($this->image_resource, $this->filepath);
    }

    /**
     * Resize depending on the image profile
     *
     * @return bool
     */
    public function resizeByProfile(): bool
    {
        $width      = imagesx($this->image_resource);
        $height     = imagesy($this->image_resource);
        $new_height = (int) ($width / $this->image_ratios[$this->file_type]["ratio"]);
        $start_y    = max(0, $height - $new_height);
        $res        = imagecreatetruecolor($width, $new_height);
        imagecopyresized(
            $res,
            $this->image_resource,
            0,
            0,
            0,
            $start_y,
            $width,
            $new_height,
            $width,
            $new_height
        );

        $this->image_resource = $res;

        return $this->genImage($this->image_resource, $this->filepath);
    }

    /**
     * Crop depending on the image type
     *
     * @return bool
     */
    public function cropByProfile(): bool
    {
        $width      = imagesx($this->image_resource);
        $height     = imagesy($this->image_resource);
        $new_height = $height / 3;
        $start_y    = max(0, $height - $new_height);
        $res        = imagecreatetruecolor($width, $new_height);
        imagecopyresized(
            $res,
            $this->image_resource,
            0,
            0,
            0,
            $start_y,
            $width,
            $new_height,
            $width,
            $new_height
        );

        imagefilter($res, IMG_FILTER_GRAYSCALE);

        $this->image_resource = $res;

        return $this->genImage($this->image_resource, $this->filepath);
    }

    /**
     * Reduces the size of the picture
     *
     * @return void
     */
    public function scaleDown(): void
    {
        $width = imagesx($this->image_resource);
        if ($width > 1024) {
            $this->image_resource = imagescale($this->image_resource, 1024);
        }
    }

    /**
     * Generate Image using current mime
     *
     * @param Resource|null $image    Image to save/gen, default refer to the face image
     * @param string|null   $filename Filepath (in the save case)
     *
     * @return bool
     */
    public function genImage($image = null, $filename = null): bool
    {
        if (!$image) {
            $image = $this->image_face;
        }

        if (!$this->mime) {
            return false;
        }

        if ($this->mime === self::IMG_JPEG) {
            return imagejpeg($image, $filename);
        } elseif ($this->mime === self::IMG_GIF) {
            return imagegif($image, $filename);
        } elseif ($this->mime === self::IMG_PNG) {
            return imagepng($image, $filename);
        }

        return false;
    }
}
