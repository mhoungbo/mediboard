<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Interpreters;

use Ox\Core\CMbArray;
use Ox\Core\CMbDT;

/**
 * Id interpreter for file parsing
 */
class IdInterpreter
{
    private const LAST_NAME    = "lastname";
    private const FIRST_NAME   = "firstname";
    private const BIRTH_DATE   = "birthdate";
    private const GENDER       = "sex";
    private const END_VALIDITY = "end_validity";

    private const CHAR_TRANS = [
        "0" => "O",
        "1" => "I",
        "2" => "Z",
        "4" => "A",
        "5" => "S",
        "6" => "E",
        "7" => "T",
        "8" => "B",
    ];

    private const PREFIX_MRZ_FR = 'IDFRA';

    /**
     * Decode MRZ identity text then return an array
     *
     * @param string $text Text to convert
     *
     * @return array
     */
    public function decode(string $text): array
    {
        $text = $this->prepareText($text);

        return [
            self::LAST_NAME    => $this->decodeLastName($text),
            self::FIRST_NAME   => $this->decodeFirstName($text),
            self::BIRTH_DATE   => $this->decodeBirth($text),
            self::GENDER       => $this->decodeGender($text),
            self::END_VALIDITY => $this->decodeEndValidity($text),
        ];
    }

    /**
     * Decodes the last name of the person
     *
     * @param array $text - the text to detect
     *
     * @return string|null
     */
    private function decodeLastName(array $text): ?string
    {
        $line = $this->postTraitment($text[count($text) - 2], 'str');
        $line = preg_replace('/[^A-Za-z0-9<]/', '', $line); // Removes special chars a part from stripes
        // Try with IDFRA{name}, DFRA{name}, FRA{name} ...
        for ($i = strlen(self::PREFIX_MRZ_FR); $i > 1; $i--) {
            $substr_prefix = substr(self::PREFIX_MRZ_FR, -$i);

            // Remove stripes (<) and return
            if (strpos($line, $substr_prefix) !== false) {
                $substr_without_prefix = substr($line, $i);
                $pos_first_stripe      = strpos($substr_without_prefix, "<");

                // last name
                return substr($substr_without_prefix, 0, $pos_first_stripe);
            }
        }

        return null;
    }

    /**
     * Decodes the first name of the person
     *
     * @param array $text - the text to detect
     *
     * @return string|null
     */
    private function decodeFirstName(array $text): ?string
    {
        $line = $text[count($text) - 1];

        preg_match("/[0-9]+([A-Za-z]+)<+/", $line, $matches);
        if (count($matches) > 0) {
            return $this->postTraitment($matches[1], "str"); // Capture
        }

        return null;
    }

    /**
     * Decodes the birthday of the person
     *
     * @param array $text - the text to detect
     *
     * @return string|null
     */
    private function decodeBirth(array $text): ?string
    {
        $line    = $text[count($text) - 1];
        $matches = [];

        preg_match("/<+([A-Za-z0-9]{6})/", $line, $matches);

        // When there are several first names
        if (!count($matches)) {
            preg_match("/[<A-Za-z]+([0-9]{6})/", $line, $matches);
        }

        if (count($matches) > 0) {
            $matches[1] = str_replace("D", "0", $matches[1]);

            return $this->postTraitment($matches[1], "int"); // Capture
        }

        return null;
    }

    /**
     * Decodes the gender of the person
     *
     * @param array $text - the text to detect
     *
     * @return string|null
     */
    private function decodeGender(array $text): ?string
    {
        $line = $text[count($text) - 1];

        preg_match("/<+[A-Za-z0-9]+[MmF]/", $line, $matches);
        if ((count($matches) > 0) && isset($matches[0])) {
            return substr($matches[0], -1, 1); // Capture
        }

        return null;
    }

    /**
     * Decodes the end of validity
     *
     * @param array $text - the text to detect
     *
     * @return string|null
     */
    private function decodeEndValidity(array $text): ?string
    {
        $line = $text[count($text) - 1];

        preg_match("/(^[0-9]{4})/", $line, $matches);
        if (count($matches) > 0) {
            $year         = substr($matches[1], 0, 2);
            $month        = substr($matches[1], 2, 4);
            $created_date = CMbDT::date("20{$year}-{$month}-01");
            $end_validity = CMbDT::date("+ 10 YEARS", $created_date);

            // Validity of 15 years for identity cards after 2004
            if (($created_date > "2004-01-01") && ($created_date < "2050-01-01")) {
                $end_validity = CMbDT::date("+ 15 YEARS", $created_date);
            }

            return $end_validity;
        }

        return null;
    }

    /**
     * Add a traitment to enhance the data quality specific to identity documents
     *
     * @param string $string        String to transform
     * @param string $expected_type Type of string used (str, int or null)
     *
     * @return string
     */
    private function postTraitment(string $string, string $expected_type = null): string
    {
        if ($expected_type === "str") {
            $string = str_replace(
                array_keys(self::CHAR_TRANS),
                array_values(self::CHAR_TRANS),
                $string
            );
        } elseif ($expected_type === "int") {
            $string = str_replace(
                array_values(self::CHAR_TRANS),
                array_keys(self::CHAR_TRANS),
                $string
            );
        }

        return strtoupper($string);
    }

    private function prepareText(string $text): array
    {
        $text = str_replace(" ", "", $text);
        $text = explode("\n", $text);
        $text = CMbArray::mapRecursive("trim", $text, apply_on_keys: false);
        $text = array_filter($text);

        // Reset keys
        return array_values($text);
    }

    public static function convertMRZBirth(string $date = null): ?string
    {
        $birth = intval($date);
        if (strlen($birth) < 6) {
            $birth = "";
        }
        if ($birth) {
            $dataYear  = substr($birth, 0, 2);
            $dataYear  = (($dataYear > date("y")) ? "19" : "20") . $dataYear;
            $dataMonth = substr($birth, 2, 2);
            $dataDay   = substr($birth, 4, 2);

            if (($dataDay < 32) && ($dataMonth < 13)) {
                $birth = "{$dataDay}/{$dataMonth}/{$dataYear}";
            }
        }

        return $birth;
    }
}
