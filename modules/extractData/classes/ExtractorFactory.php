<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData;

use Exception;
use InvalidArgumentException;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Mediboard\ExtractData\Extractors\ContentExtractor;
use Ox\Mediboard\ExtractData\Extractors\ExtractorInterface;
use Ox\Mediboard\ExtractData\Extractors\IdPictureExtractor;
use Ox\Mediboard\ExtractData\Extractors\MRZExtractor;
use Ox\Mediboard\ExtractData\Extractors\QRCodeExtractor;
use Ox\Mediboard\ExtractData\Interpreters\IdInterpreter;
use Ox\Mediboard\ExtractData\Parsers\FileParser;
use Ox\Mediboard\Files\Extractor;

class ExtractorFactory
{
    /**
     * Return a data extractor depending on profile
     *
     * @param string $profile
     *
     * @return ExtractorInterface
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public static function create(string $profile): ExtractorInterface
    {
        switch ($profile) {
            case Extractor::PROFILE_MRZ:
                return new MRZExtractor(new FileParser(), new IdInterpreter());

            case Extractor::PROFILE_ID_PICTURE:
                return new IdPictureExtractor();

            case Extractor::PROFILE_CONTENT:
                return new ContentExtractor(new FileParser());

            case Extractor::PROFILE_QR_CODE:
                return new QRCodeExtractor();

            default:
                throw new InvalidArgumentException(CAppUI::tr('CExtractDataLog-msg-Profile is missing'));
        }
    }
}
