<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Extractors;

use Exception;
use Ox\Mediboard\ExtractData\Parsers\FileParser;

abstract class AbstractTikaExtractor implements ExtractorInterface
{
    private FileParser $file_parser;

    public function __construct(FileParser $file_parser)
    {
        $this->file_parser = $file_parser;
    }

    /**
     * Return parsed content of file with Tika
     *
     * @param string $file_path
     *
     * @return string
     * @throws Exception
     */
    protected function getContent(string $file_path): string
    {
        try {
            if (!$this->file_parser->isActive()) {
                throw new Exception('Tika server is not active');
            }

            $content = $this->file_parser->getContent($file_path);

            if ($content === false) {
                throw new Exception();
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $content;
    }
}
