<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Extractors;

interface ExtractorInterface
{
    /**
     * Return content extracted from file
     *
     * @param string $file_path Path to uploaded file
     * @param array  $options   File options (extension, file_name)
     * @param int    $user_id   User who uploaded file for logging
     *
     * @return array
     */
    public function extract(string $file_path, int $user_id, array $options = []): array;
}
