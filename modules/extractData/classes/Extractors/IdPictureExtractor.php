<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Extractors;

use Exception;
use Ox\Mediboard\ExtractData\Interpreters\IdImageCropper;
use Ox\Mediboard\Files\Extractor;

class IdPictureExtractor implements ExtractorInterface
{
    use ExtractorTrait;

    /**
     * @throws Exception
     */
    public function extract(string $file_path, int $user_id, array $options = []): array
    {
        $extension = $this->getExtension($options);

        if ($extension !== 'pdf') {
            $cropper = $this->prepareImage($file_path);
        } else {
            $cropper = new IdImageCropper($file_path, "id_picture");
        }

        $content = [
            "picture_face_base64" => $cropper->getFaceBase64(),
            "picture_base64"      => $cropper->getBase64(),
            "picture_mime"        => $cropper->mime,
        ];

        if ($extension !== 'pdf') {
            unlink($cropper->filepath);
        }

        return $content;
    }

    private function prepareImage(string $file_path): IdImageCropper
    {
        // Work with temporary file to prevent erase default file
        $tmp = tempnam(sys_get_temp_dir(), "id_picture_");
        copy($file_path, $tmp);

        $cropper = new IdImageCropper($tmp, Extractor::PROFILE_ID_PICTURE);
        $cropper->cropSpaces();
        $cropper->scaleDown();
        $cropper->resizeByProfile();

        return $cropper;
    }
}
