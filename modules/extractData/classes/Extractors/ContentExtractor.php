<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Extractors;

use Exception;
use Ox\Mediboard\Files\Extractor;

class ContentExtractor extends AbstractTikaExtractor
{
    use ExtractorTrait;

    /**
     * @throws Exception
     */
    public function extract(string $file_path, int $user_id, array $options = []): array
    {
        $name = $this->getName($options);

        try {
            $content = $this->getContent($file_path);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        $this->log($name, ["text content"], Extractor::PROFILE_CONTENT, $user_id);

        return [
            "content" => $content,
        ];
    }
}
