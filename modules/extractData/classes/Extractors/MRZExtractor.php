<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Extractors;

use Exception;
use Ox\Mediboard\ExtractData\Interpreters\IdImageCropper;
use Ox\Mediboard\ExtractData\Interpreters\IdInterpreter;
use Ox\Mediboard\ExtractData\Parsers\FileParser;
use Ox\Mediboard\Files\Extractor;

class MRZExtractor extends AbstractTikaExtractor
{
    use ExtractorTrait;

    private IdInterpreter $id_interpreter;

    public function __construct(FileParser $file_parser, IdInterpreter $id_interpreter)
    {
        parent::__construct($file_parser);

        $this->id_interpreter = $id_interpreter;
    }

    /**
     * @throws Exception
     */
    public function extract(string $file_path, int $user_id, array $options = []): array
    {
        $extension = $this->getExtension($options);
        $name      = $this->getName($options);

        if ($extension !== 'pdf') {
            $file_path = $this->prepareImage($file_path);
        }

        try {
            $content = $this->getContent($file_path);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        $content = $this->id_interpreter->decode($content);

        $this->log($name, $content, Extractor::PROFILE_MRZ, $user_id);

        if ($extension !== 'pdf') {
            unlink($file_path);
        }

        return $content;
    }

    private function prepareImage(string $file_path): string
    {
        // Work with temporary file to prevent erase default file
        $tmp = tempnam(sys_get_temp_dir(), "mrz_");
        copy($file_path, $tmp);

        $image_cropper = new IdImageCropper($tmp, Extractor::PROFILE_MRZ);
        $image_cropper->cropSpaces();
        $image_cropper->scaleDown();
        $image_cropper->resizeByProfile();
        $image_cropper->cropByProfile();
        $image_cropper->cropSpaces();

        return $tmp;
    }
}
