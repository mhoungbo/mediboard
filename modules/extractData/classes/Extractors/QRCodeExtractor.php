<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Extractors;

use Exception;
use Ox\Mediboard\Files\Extractor;
use Zxing\QrReader;

class QRCodeExtractor implements ExtractorInterface
{
    use ExtractorTrait;

    /**
     * @throws Exception
     */
    public function extract(string $file_path, int $user_id, array $options = []): array
    {
        $name = $this->getName($options);

        try {
            $content = (new QrReader($file_path))->text();

            if ($content === false) {
                throw new Exception();
            }
        } catch (Exception $e) {
            throw new Exception('An error occured when trying to use QRReader');
        }

        $this->log($name, [$content], Extractor::PROFILE_QR_CODE, $user_id);

        return [
            "qr_code_content" => $content,
        ];
    }
}
