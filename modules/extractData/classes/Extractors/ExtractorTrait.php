<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Extractors;

use Exception;
use InvalidArgumentException;
use Ox\Mediboard\ExtractData\CExtractDataLog;

trait ExtractorTrait
{
    /**
     * Get and check extension
     *
     * @throws InvalidArgumentException
     */
    private function getExtension(array $options): string
    {
        $extension = $options['extension'] ?? null;

        if ($extension === null) {
            throw new InvalidArgumentException('File extension is missing');
        }

        return $extension;
    }

    /**
     * Get and check name
     *
     * @throws InvalidArgumentException
     */
    private function getName(array $options): string
    {
        $name = $options['file_name'] ?? null;

        if ($name === null) {
            throw new InvalidArgumentException('File name is missing');
        }

        return $name;
    }

    /**
     * @throws Exception
     */
    protected function log(string $file_name, array $content, string $profile, int $user_id): void
    {
        (new CExtractDataLog())->log($file_name, $this->sanitizeLog($content), $profile, $user_id);
    }

    /**
     * sanitize content depending on profile to prevent storing scrap in database
     * @throws Exception
     */
    protected function sanitizeLog(array $content): string
    {
        $data = json_encode($content);

        if ($data === false) {
            throw new Exception('No data to store');
        }

        return $data;
    }
}
