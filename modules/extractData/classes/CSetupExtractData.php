<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData;

use Ox\Core\CSetup;

/**
 * @codeCoverageIgnore
 */
class CSetupExtractData extends CSetup
{
    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->mod_name = "extractData";

        $this->makeRevision("0.0");
        $this->setModuleCategory("autre", "ox");
        $this->addQuery(
            "CREATE TABLE `extract_data_log` (
                      `extract_data_log_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      `file_name` VARCHAR(255) NOT NULL,
                      `datetime` DATETIME NOT NULL,
                      `user_id` INT(11) UNSIGNED NOT NULL,
                      `content` TEXT NOT NULL,
                      `profile` VARCHAR(50) NOT NULL,
                      INDEX (`user_id`),
                      INDEX (`datetime`)
                    )/*! ENGINE=MyISAM */;"
        );
        $this->addDependency('dPfiles', '0.97');

        $this->mod_version = "0.01";
    }
}
