<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Parsers;

use Exception;
use Ox\Core\CAppUI;
use Throwable;
use Vaites\ApacheTika\Client;
use Vaites\ApacheTika\Clients\WebClient;
use Vaites\ApacheTika\Metadata\MetadataInterface;

/**
 * Parser class using Tika server with Tesseract to extract document data
 */
class FileParser
{
    protected const DEFAULT_LANG = 'fra';

    public Client $client;

    public array $options;

    public string $current_file;

    /**
     * @param string $lang Lang for tesseract OCR (fra/eng)
     *
     * @return void
     * @throws Exception
     */
    public function __construct(string $lang = self::DEFAULT_LANG)
    {
        $host = null;
        $port = null;

        if ($config = CAppUI::conf("extractData tika host")) {
            $host = trim($config);
        }

        if ($config = CAppUI::conf("extractData tika port")) {
            $port = trim($config);
        }

        if (!$port || !$host) {
            throw new Exception('extractData-msg-No config found');
        }

        $this->options = [
            CURLOPT_TIMEOUT    => (int)trim(CAppUI::conf("extractData tika timeout")),
            CURLOPT_HTTPHEADER => ["X-Tika-OCRLanguage: {$lang}"],
        ];

        $this->client = Client::make($host, $port, $this->options);
    }

    /**
     * Get the document content
     *
     * @param string $file path/to/filename.ext
     *
     * @return bool|mixed|string
     * @throws Exception
     */
    public function getContent(string $file)
    {
        if (!file_exists($file)) {
            return false;
        }

        // Init
        $this->current_file = $file;
        $_pathinfo          = pathinfo($file);
        [$_type, $_ext] = explode('/', mime_content_type($file));

        // Tika
        if ($content = $this->tikaContent(false)) {
            return $content;
        }

        // 2� chance image (title)
        if ($_type === 'image') {
            return $_pathinfo['filename'];
        }

        // 2� chance pdf (tika with PDFextractInlineImages)
        $active_ocr_pdf = (bool)CAppUI::conf("extractData tika active_ocr_pdf");
        if ((strtolower($_ext) === 'pdf') && $active_ocr_pdf) {
            return $this->tikaContent(true);
        }

        // 2� chance (regex)
        $content = file_get_contents($this->current_file);
        $content = preg_replace('/[[:^print:]]/', '', $content);

        return strip_tags($content);
    }

    /**
     * @throws Exception
     */
    public function isActive(): bool
    {
        foreach ($this->client->getAvailableParsers() as $root_parser) {
            if (isset($root_parser['children'])) {
                foreach ($root_parser['children'] as $parser) {
                    if (isset($parser['name']) && str_contains($parser['name'], 'TesseractOCRParser')) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Define if the class can be used
     *
     * @return bool
     * @throws Exception
     */
    public static function canBeUsed(): bool
    {
        return (CAppUI::conf("extractData tika host") !== "");
    }

    /**
     * Get the document metadata
     *
     * @param string $file path/to/filename.ext
     *
     * @return MetadataInterface|bool
     * @throws Exception
     */
    private function getMetadata(string $file)
    {
        if (!file_exists($file)) {
            return false;
        }

        return $this->client->getMetadata($file);
    }

    /**
     * @param bool $force_ocr_pdf Force OCR on inline images (tesseract)
     *
     * @return string Content of extracted document
     * @throws Exception
     */
    private function tikaContent(bool $force_ocr_pdf = false): string
    {
        if ($this->client instanceof WebClient) {
            $options = $this->options;

            if ($force_ocr_pdf) {
                $options[CURLOPT_HTTPHEADER][] = "X-Tika-PDFextractInlineImages: true";
            }

            $this->client->setOptions($options);
        }

        try {
            $content = $this->client->getText($this->current_file);
        } catch (Throwable $e) {
            throw new Exception($e->getMessage());
        }

        return $content;
    }
}
