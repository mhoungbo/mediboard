<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\Filter;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFilter;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\Controller;
use Ox\Core\Module\CModule;
use Ox\Mediboard\ExtractData\CExtractDataLog;
use Ox\Mediboard\ExtractData\ExtractorFactory;
use Ox\Mediboard\Mediusers\CMediusers;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * API Controller for extract data service
 */
class ExtractDataController extends Controller
{
    public const TYPE_STATUS    = 'extract_data_service';
    public const TYPE_EXTRACTED = 'extracted_data';

    /**
     * @param RequestApi $request_api
     *
     * @return Response
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function extractData(RequestApi $request_api): Response
    {
        // Get profile and uploaded file
        $profile       = $request_api->getRequest()->query->get('profile');
        $uploaded_file = $request_api->getRequest()->files->get('file');

        // If file is uploaded from an exchange source (or other ?), convert it to uploaded file
        if (!$uploaded_file) {
            $content       = json_decode($request_api->getRequest()->getContent(), true)['data']['attributes'];
            $uploaded_file = new UploadedFile($content['file_path'], $content['file_name'], $content['file_extension']);
        }

        // Get uploaded file path
        $file_path = $uploaded_file->getRealPath();

        if (!file_exists($file_path) || !$file_path) {
            throw new Exception(CAppUI::tr('CFile-not-exists', $file_path));
        }

        $options = [
            "extension" => strtolower(explode('/', $uploaded_file->getClientMimeType())[1]),
            "file_name" => $uploaded_file->getClientOriginalName(),
        ];

        // Use factory to return an extractor depending on profile
        try {
            $result = (ExtractorFactory::create($profile))->extract($file_path, CMediusers::get()->_id, $options);
        } catch (Exception $e) {
            $item = (new Item(
                ['response' => CAppUI::tr('extractData-msg-Error occured when parsing %s', $e->getMessage())]
            ))->setType('error');

            return $this->renderApiResponse($item, 500);
        }

        // Return decoded content
        $resource = (new Item($result))->setType(self::TYPE_EXTRACTED);

        return $this->renderApiResponse($resource);
    }

    /**
     * @return Response
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function status(): Response
    {
        $tika_server_status = (
            @CAppUI::conf("extractData tika host")
            || @CAppUI::conf("extractData tika port")
        ) ? true : false;

        $resource = new Item(
            [
                'factory_status'      => CModule::getActive('extractData')->mod_active ? true : false,
                'tika_server_status'  => $tika_server_status,
                'gd_extension_active' => extension_loaded('GD') ? true : false,
            ]
        );

        $resource->setType(self::TYPE_STATUS);

        return $this->renderApiResponse($resource);
    }

    /**
     * Return extract data logs
     * If user admin, return last 50 logs
     * If user not admin, return last 50 PERSONAL logs
     *
     * @param RequestApi $request_api
     *
     * @return Response
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function logs(RequestApi $request_api): Response
    {
        $extract_data_log = new CExtractDataLog();

        // If user is module admin, return all logs, else return only current user logs
        if (!$extract_data_log->_ref_module->canAdmin()) {
            $request_api->getRequestFilter()
                        ->addFilter(
                            new Filter('user_id', RequestFilter::FILTER_EQUAL, [CMediusers::get()->_id])
                        );
        }

        $logs = $extract_data_log->loadListFromRequestApi($request_api);

        $resource = Collection::createFromRequest($request_api, $logs);

        $resource->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            $extract_data_log->countListFromRequestApi($request_api)
        );

        return $this->renderApiResponse($resource);
    }
}
