<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Controllers\Legacy;

use Exception;
use Ox\Core\CLegacyController;
use Ox\Core\CMbException;
use Ox\Core\CMbString;
use Ox\Core\CView;
use Ox\Core\EntryPoint;
use Ox\Mediboard\ExtractData\Controllers\ExtractDataController;
use Ox\Mediboard\Files\Extractor;

class ExtractDataLegacyController extends CLegacyController
{
    private const README = 'readme.md';

    /**
     * @throws Exception
     */
    public function configure(): void
    {
        $this->checkPermAdmin();

        $this->renderSmarty('configure');
    }

    /**
     * Main page
     * @return void
     * @throws Exception
     */
    public function vwExtractor(): void
    {
        $this->checkPermRead();

        $readme = CMbString::markdown(
            file_get_contents(dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . self::README)
        );

        $this->renderSmarty('vw_extractor', ['readme' => $readme,]);
    }

    /**
     * @throws Exception
     */
    public function vwExtractorState(): void
    {
        $this->checkPermRead();

        $extract_data_controller = new ExtractDataController();
        $api_response            = $extract_data_controller->status();

        $status = json_decode($api_response->getContent(), true);

        $this->renderSmarty(
            'vw_extractor_state',
            [
                'active' => $status['data']['attributes'],
            ]
        );
    }

    public function vwExtractorTest(): void
    {
        $this->checkPermRead();

        $this->renderSmarty(
            'vw_extractor_test',
            [
                "profiles" => Extractor::getProfiles(),
            ],
        );
    }

    /**
     * Display a list of logs as json api.
     *
     * @throws CMbException|Exception
     */
    public function displayLogs(): void
    {
        $this->checkPermRead();
        CView::checkin();

        $entry = new EntryPoint('ExtractDataLogList');
        $entry->setScriptName('extractDataLogList')
              ->addLink('logList', 'extractData_logs');

        $this->renderEntryPoint($entry);
    }
}
