<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Tests\Functional\Controllers;

use Exception;
use Ox\Mediboard\ExtractData\CExtractDataLog;
use Ox\Mediboard\ExtractData\Controllers\ExtractDataController;
use Ox\Mediboard\Files\Extractor;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

/**
 * @group schedules
 */
class ExtractDataControllerTest extends OxWebTestCase
{
    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testStatus(): void
    {
        $client = static::createClient();
        $client->request(
            'GET',
            "/api/extractData/status"
        );

        $this->assertResponseIsSuccessful();
        $item = $this->getJsonApiItem($client);

        $this->assertEquals(ExtractDataController::TYPE_STATUS, $item->getType());
        $this->assertTrue($item->hasAttribute('factory_status'));
        $this->assertTrue($item->hasAttribute('tika_server_status'));
        $this->assertTrue($item->hasAttribute('gd_extension_active'));
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testExtractMRZData(): void
    {
        $file_path          = __DIR__ . '/../../Resources/Parser/sample-id-card-with-picture.png';
        $original_file_name = 'sample-id-card-with-picture.png';
        $extension          = 'image/png';

        $item = new Item(Extractor::TYPE);
        $item->setAttributes(
            [
                'file_path'      => $file_path,
                'file_name'      => $original_file_name,
                'file_extension' => $extension,
            ]
        );

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/extractData?profile=' . Extractor::PROFILE_MRZ,
            [],
            [],
            [],
            json_encode($item)
        );

        $this->assertResponseIsSuccessful();
        $item = $this->getJsonApiItem($client);

        $this->assertEquals(ExtractDataController::TYPE_EXTRACTED, $item->getType());
        $this->assertEquals('BERTHIER', $item->getAttribute('lastname'));
        $this->assertEquals('CORINNE', $item->getAttribute('firstname'));
        $this->assertEquals('651206', $item->getAttribute('birthdate'));
        $this->assertEquals('F', $item->getAttribute('sex'));
        $this->assertEquals('2098-06-01', $item->getAttribute('end_validity'));
    }

    /**
     * @throws TestsException
     * @throws Exception
     * @depends testExtractMRZData
     */
    public function testLogs(): void
    {
        $client = static::createClient();
        $client->request(
            'GET',
            "/api/extractData/logs"
        );

        $this->assertResponseIsSuccessful();
        $collection = $this->getJsonApiCollection($client);

        /** @var Item $item */
        foreach ($collection as $item) {
            $this->assertEquals(CExtractDataLog::RESOURCE_TYPE, $item->getType());
            $this->assertTrue($item->hasAttribute('file_name'));
            $this->assertTrue($item->hasAttribute('datetime'));
            $this->assertTrue($item->hasAttribute('user_id'));
            $this->assertTrue($item->hasAttribute('profile'));
        }
    }
}
