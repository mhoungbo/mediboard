<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Tests\Unit;

use Exception;
use Ox\Mediboard\ExtractData\CExtractDataLog;
use Ox\Mediboard\Files\Extractor;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;

/**
 * @group schedules
 */
class CExtractDataLogTest extends OxUnitTestCase
{
    /**
     * @throws Exception
     */
    public function testPurgeLogs(): void
    {
        $extract = new CExtractDataLog();

        $extract->log('lorem', 'ipsum', Extractor::PROFILE_CONTENT, CMediusers::get()->_id);

        $this->invokePrivateMethod($extract, 'purgeLogs', -1);

        $after = $extract->loadList();

        $this->assertCount(0, $after);
    }
}
