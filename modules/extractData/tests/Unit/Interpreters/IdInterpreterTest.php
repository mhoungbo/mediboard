<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Tests\Unit\Interpreters;

use Exception;
use Ox\Mediboard\ExtractData\Interpreters\IdInterpreter;
use Ox\Tests\OxUnitTestCase;

/**
 * @group schedules
 */
class IdInterpreterTest extends OxUnitTestCase
{
    private array $mrz_88 = [
        "IDFRABERTHIER<<<<<<<<<<<<<<<<<<<<<<<<",
        "8806923102858CORINNE<<<<<<<6512068F6",
    ];

    private array $mrz_12 = [
        "IDFRABERTHIER<<<<<<<<<<<<<<<<<<<<<<<<",
        "1203923102858CORINNE<<<<<<<6512068F6",
    ];

    /**
     * @throws Exception
     */
    public function testConvertMRZBirth(): void
    {
        $mrz_birth       = '6512068';
        $converted_birth = IdInterpreter::convertMRZBirth($mrz_birth);

        $this->assertEquals("06/12/1965", $converted_birth);
    }

    /**
     * Validity is 10 years because id card has been delivered before 2004
     * @throws Exception
     */
    public function testDecodeEndValidity10Years(): void
    {
        $end_validity = $this->invokePrivateMethod(new IdInterpreter(), 'decodeEndValidity', $this->mrz_88);

        $this->assertEquals("2098-06-01", $end_validity);
    }

    /**
     * Validity is 15 years because id card has been delivered after 2004
     * @throws Exception
     */
    public function testDecodeEndValidity15Years(): void
    {
        $end_validity = $this->invokePrivateMethod(new IdInterpreter(), 'decodeEndValidity', $this->mrz_12);

        $this->assertEquals("2027-03-01", $end_validity);
    }
}
