<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Tests\Unit\Extractors;

use Exception;
use InvalidArgumentException;
use Ox\Mediboard\ExtractData\Extractors\MRZExtractor;
use Ox\Mediboard\ExtractData\Interpreters\IdInterpreter;
use Ox\Mediboard\ExtractData\Parsers\FileParser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;

/**
 * @group schedules
 */
class MRZExtractorTest extends OxUnitTestCase
{
    private const ID_CARD_PATH = __DIR__ . '/../../Resources/Parser/sample-id-card-with-picture.png';

    /**
     * @throws Exception
     */
    public function testCreateValidExtractor(): void
    {
        $extractor = new MRZExtractor(new FileParser(), new IdInterpreter());

        $options = [
            "extension" => strtolower(explode('/', mime_content_type(self::ID_CARD_PATH))[1]),
            "file_name" => 'sample-id-card-with-picture.png',
        ];

        $result = $extractor->extract(self::ID_CARD_PATH, CMediusers::get()->_id, $options);

        $expected = [
            'lastname'     => "BERTHIER",
            "firstname"    => "CORINNE",
            "birthdate"    => "651206",
            "sex"          => "F",
            "end_validity" => "2098-06-01",
        ];

        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider invalidDataProvider
     * @throws Exception
     */
    public function testCreateInvalidExtractorAndThrowException(
        string $file_path,
        array  $options,
        string $expected_exception
    ): void {
        $extractor = new MRZExtractor(new FileParser(), new IdInterpreter());

        $this->expectException($expected_exception);
        $extractor->extract($file_path, CMediusers::get()->_id, $options);
    }

    /**
     * @return array
     */
    public function invalidDataProvider(): array
    {
        return [
            'without extension' => [
                self::ID_CARD_PATH,
                [
                    "file_name" => 'sample-id-card-with-picture.png',
                ],
                InvalidArgumentException::class,
            ],
            'without file name' => [
                self::ID_CARD_PATH,
                [
                    "extension" => strtolower(explode('/', mime_content_type(self::ID_CARD_PATH))[1]),
                ],
                InvalidArgumentException::class,
            ],
        ];
    }
}
