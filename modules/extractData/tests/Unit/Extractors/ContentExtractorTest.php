<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Tests\Unit\Extractors;

use Exception;
use Ox\Mediboard\ExtractData\Extractors\ContentExtractor;
use Ox\Mediboard\ExtractData\Parsers\FileParser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;

/**
 * @group schedules
 */
class ContentExtractorTest extends OxUnitTestCase
{
    private const MRZ_PATH = __DIR__ . '/../../Resources/Parser/sample-id-card-old-bottom-fr.png';

    /**
     * @throws Exception
     */
    public function testCreateValidExtractor(): void
    {
        $extractor = new ContentExtractor(new FileParser());

        $options = [
            'file_name' => 'sample-id-card-old-bottom-fr',
        ];

        $result = $extractor->extract(self::MRZ_PATH, CMediusers::get()->_id, $options);

        $expected = [
            'content' => "IDFRABERTHIER<<<<<<<<<<<<<<<<<92C001\n8806923102858CORINNE<<<<<<<6512068F8",
        ];

        $this->assertEquals($expected, $result);
    }
}
