<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Tests\Unit\Extractors;

use Exception;
use Ox\Mediboard\ExtractData\Extractors\QRCodeExtractor;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;

/**
 * @group schedules
 */
class QRCodeExtractorTest extends OxUnitTestCase
{
    private const QR_PATH = __DIR__ . '/../../Resources/Parser/sample-qr-code.png';

    /**
     * @throws Exception
     */
    public function testCreateValidExtractor(): void
    {
        $options = [
            'file_name' => 'sample-id-card-old-bottom-fr',
        ];

        $result = (new QRCodeExtractor())->extract(self::QR_PATH, CMediusers::get()->_id, $options);

        $expected = [
            "qr_code_content" => "http://localhost/mediboard/index.php?m=system&tab=about",
        ];

        $this->assertEquals($expected, $result);
    }

    /**
     * @throws Exception
     */
    public function testCreateInvalidExtractorInexistantFile(): void
    {
        $this->expectExceptionMessage('An error occured when trying to use QRReader');

        $options = [
            'file_name' => 'sample-id-card-old-bottom-fr',
        ];

        (new QRCodeExtractor())->extract('lorem.png', CMediusers::get()->_id, $options);
    }
}
