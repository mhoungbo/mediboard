<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Tests\Unit\Extractors;

use Exception;
use InvalidArgumentException;
use Ox\Mediboard\ExtractData\ExtractorFactory;
use Ox\Mediboard\ExtractData\Extractors\ContentExtractor;
use Ox\Mediboard\ExtractData\Extractors\IdPictureExtractor;
use Ox\Mediboard\ExtractData\Extractors\MRZExtractor;
use Ox\Mediboard\ExtractData\Extractors\QRCodeExtractor;
use Ox\Mediboard\Files\Extractor;
use Ox\Tests\OxUnitTestCase;

/**
 * @group schedules
 */
class ExtractorFactoryTest extends OxUnitTestCase
{
    /**
     * @dataProvider validProfilesProvider
     * @throws Exception
     */
    public function testFactoryCreateValidExtractor(
        string  $profile,
        ?string $expected_class = null
    ): void {
        $extractor = ExtractorFactory::create($profile);

        $this->assertInstanceOf($expected_class, $extractor);
    }

    /**
     * @dataProvider invalidProfilesProvider
     * @throws Exception
     */
    public function testFactoryCreateInvalidExtractorAndThrowException(
        string $profile
    ): void {
        $this->expectException(InvalidArgumentException::class);

        ExtractorFactory::create($profile);
    }

    public function testGetProfiles(): void
    {
        $factory = Extractor::getProfiles();

        $expected = [
            Extractor::PROFILE_MRZ,
            Extractor::PROFILE_ID_PICTURE,
            Extractor::PROFILE_QR_CODE,
            Extractor::PROFILE_CONTENT,
        ];

        $this->assertArrayContentsEquals($expected, $factory);
    }

    public function validProfilesProvider(): array
    {
        return [
            "return MRZ etractor"        => [
                Extractor::PROFILE_MRZ,
                MRZExtractor::class,
            ],
            "return ID PICTURE etractor" => [
                Extractor::PROFILE_ID_PICTURE,
                IdPictureExtractor::class,
            ],
            "return CONTENT etractor"    => [
                Extractor::PROFILE_CONTENT,
                ContentExtractor::class,
            ],
            "return QR CODE etractor"    => [
                Extractor::PROFILE_QR_CODE,
                QRCodeExtractor::class,
            ],
        ];
    }

    public function invalidProfilesProvider(): array
    {
        return [
            "profile not exists" => [
                "lorem",
            ],
        ];
    }
}
