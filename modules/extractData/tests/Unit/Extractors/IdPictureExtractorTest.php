<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\ExtractData\Tests\Unit\Extractors;

use Exception;
use InvalidArgumentException;
use Ox\Mediboard\ExtractData\Extractors\IdPictureExtractor;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;

/**
 * @group schedules
 */
class IdPictureExtractorTest extends OxUnitTestCase
{
    private const ID_CARD_PATH = __DIR__ . '/../../Resources/Parser/sample-id-card-with-picture.png';

    /**
     * @throws Exception
     */
    public function testCreateValidExtractor(): void
    {
        $extractor = new IdPictureExtractor();

        $options = [
            "extension" => strtolower(explode('/', mime_content_type(self::ID_CARD_PATH))[1]),
            "file_name" => 'sample-id-card-with-picture.png',
        ];

        $result = $extractor->extract(self::ID_CARD_PATH, CMediusers::get()->_id, $options);

        $expected = [
            "picture_face_base64" =>
                'iVBORw0KGgoAAAANSUhEUgAAAMgAAAD1CAIAAACa3i26AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nOy9y',
            "picture_base64"      =>
                'iVBORw0KGgoAAAANSUhEUgAAAboAAAE7CAIAAAAkedOvAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nOy7y64ty5IlZ',
            "picture_mime"        => 'image/png',
        ];

        $this->assertStringStartsWith($expected['picture_face_base64'], $result['picture_face_base64']);
        $this->assertStringStartsWith($expected['picture_base64'], $result['picture_base64']);
        $this->assertEquals($expected['picture_mime'], $result['picture_mime']);
    }

    /**
     * @dataProvider invalidDataProvider
     * @throws Exception
     */
    public function testCreateInvalidExtractorAndThrowException(
        string $file_path,
        array  $options,
        string $expected_exception
    ): void {
        $extractor = new IdPictureExtractor();

        $this->expectException($expected_exception);
        $extractor->extract($file_path, CMediusers::get()->_id, $options);
    }

    /**
     * @return array
     */
    public function invalidDataProvider(): array
    {
        return [
            'without extension' => [
                self::ID_CARD_PATH,
                [
                    "file_name" => 'sample-id-card-with-picture.png',
                ],
                InvalidArgumentException::class,
            ],
        ];
    }
}
