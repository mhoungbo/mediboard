{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main tbl">
  <tr>
    <td>
      <table class="main tbl">
        <tr>
          <th class="section" colspan="2">Etat</th>
        </tr>
        <tr>
          <td class="text">{{tr}}extractData-msg-Tika test connection{{/tr}}</td>
          <td class="text">
              {{if $active.tika_server_status}}
                <img title="Connect�" src="images/icons/note_green.png">
              {{else}}
                <img title="D�connect�" src="images/icons/note_red.png">
              {{/if}}
          </td>
        </tr>
        <tr>
          <td class="text">{{tr}}extractData-msg-OCR test connection{{/tr}}</td>
          <td class="text">
              {{if $active.tika_server_status}}
                <img title="Connect�" src="images/icons/note_green.png">
              {{else}}
                <img title="D�connect�" src="images/icons/note_red.png">
              {{/if}}
          </td>
        </tr>
        <tr>
          <td class="text">{{tr}}extractData-msg-GD test connection{{/tr}}</td>
          <td class="text">
              {{if $active.gd_extension_active}}
                <img title="Connect�" src="images/icons/note_green.png">
              {{else}}
                <img title="D�connect�" src="images/icons/note_red.png">
              {{/if}}
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
