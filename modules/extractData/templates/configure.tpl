{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<form name="editConfig" method="post" onsubmit="return onSubmitFormAjax(this);">
    {{mb_configure module=$m}}

  <table class="form">
    <tr>
      <th class="title" colspan="3">{{tr}}config-extractData-msg-Configuration{{/tr}}</th>
    </tr>

    <tr>
      <th class="category" colspan="3">{{tr}}config-extractData-msg-Tika server{{/tr}}</th>
    </tr>

      {{mb_include module=system template=inc_config_str class=tika var=host placeholder="tika_server"}}
      {{mb_include module=system template=inc_config_str class=tika var=port placeholder="9998"}}
      {{mb_include module=system template=inc_config_bool class=tika var=active_ocr_pdf}}
      {{mb_include module=system template=inc_config_str class=tika var=timeout}}

    <tr>
      <th class="category" colspan="3">{{tr}}module-extractData-court{{/tr}}</th>
    </tr>

      {{mb_include module=system template=inc_config_bool class=logs var=active}}
      {{mb_include module=system template=inc_config_num class=logs var=purge_probability numeric=true}}
      {{mb_include module=system template=inc_config_enum class=logs var=purge_delay values='30|60|90|120|365'}}

    <tr>
      <td class="button" colspan="3">
        <button class="submit" type="submit">{{tr}}Save{{/tr}}</button>
      </td>
    </tr>
  </table>
</form>
