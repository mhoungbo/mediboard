<?php

/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\Mediusers\Tests\Functional\Controllers;

use Ox\Core\CMbDT;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediusersControllerTest extends OxWebTestCase
{
    public function testListMediusers()
    {
        $client = static::createClient();
        $client->request('GET', '/api/mediuser/mediusers');

        $this->assertResponseIsSuccessful();
        $collection = $this->getJsonApiCollection($client);

        $this->assertEquals($collection->getFirstItem()->getType(), 'mediuser');
    }

    public function testShowMediuser()
    {
        $mediuser = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $client   = static::createClient();
        $client->request('GET', '/api/mediuser/mediusers/' . $mediuser->_id);

        $this->assertResponseIsSuccessful();

        $item = $this->getJsonApiItem($client);

        $this->assertEquals($item->getType(), 'mediuser');
        $this->assertEquals($item->getId(), $mediuser->_id);
        $this->assertEquals($item->getAttribute('_user_first_name'), $mediuser->_user_first_name);
        $this->assertEquals($item->getAttribute('_user_last_name'), $mediuser->_user_last_name);
    }

    public function testShowMediuserFunctions()
    {
        $mediuser = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $client   = static::createClient();
        $client->request('GET', '/api/mediuser/mediusers/' . $mediuser->_id . '/functions');


        $this->assertResponseIsSuccessful();
        $collection = $this->getJsonApiCollection($client);

        $this->assertGreaterThanOrEqual(1, $collection->count());
    }

    public function testImportMediuserNoFile()
    {
        $client = static::createClient();
        $client->request('POST', '/api/mediuser/mediusers', [
            'dry_run'            => 0,
            'update_found_users' => 1,
        ]);

        $this->assertEquals($client->getResponse()->getStatusCode(), 500);
        $error = $this->getJsonApiError($client);

        $this->assertStringStartsWith('Un fichier doit �tre upload� via le nom "import_file"', $error->getMessage());
    }

    public function testImportMediuserSucces()
    {
        $tmp_file = tempnam("./tmp", "import_file");
        if (file_exists($tmp_file)) {
            (new Filesystem())->remove($tmp_file);
        }
        $content = "nom;prenom;username;password;type;fonction;profil\n";
        $content .= "import_file_lorem;import_file_ipsum;import_file_lipsum;azerty123;1;OpenXtrem;SI";

        (new Filesystem())->dumpFile($tmp_file, $content);

        $client = static::createClient();

        $upload_file = new UploadedFile($tmp_file, 'import_file');

        $client->request(
            'POST',
            '/api/mediuser/mediusers',
            [
                'dry_run'            => true,
                'update_found_users' => 1,
            ],
            [
                'import_file' => $upload_file,
            ]
        );


        $this->assertResponseIsSuccessful();
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($item->getAttribute('created')[0], 'Utilisateur cr�� (x 1)');
    }

    public function testCreateMediusers(): CMediusers
    {
        $function = $this->getObjectFromFixturesReference(CFunctions::class, UsersFixtures::REF_FIXTURES_FUNCTION);

        $username = 'testCreateMediusers-' . uniqid();

        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/mediuser/mediusers/edit',
            [
                'function_id'      => $function->_id,
                '_user_username'   => $username,
                'actif'            => 1,
                'deb_activite'     => CMbDT::date(),
                '_user_last_name'  => 'last_name',
                '_user_first_name' => 'first_name',
                '_user_type'       => 14,
                'ajax'             => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            $this->translator->tr('CMediusers-msg-create'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $user                = new CUser();
        $user->user_username = $username;
        $user->loadMatchingObjectEsc();

        return $user->loadRefMediuser();
    }

    /**
     * @depends testCreateMediusers
     */
    public function testUpdateMediusers(CMediusers $mediuser): CMediusers
    {
        $new_date = CMbDT::date('+1 DAY');

        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/mediuser/mediusers/edit',
            [
                'user_id'      => $mediuser->_id,
                'actif'        => 0,
                'deb_activite' => $new_date,
                'ajax'         => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            $this->translator->tr('CMediusers-msg-modify'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $mediuser = CMediusers::findOrFail($mediuser->_id);
        $this->assertEquals(0, $mediuser->actif);
        $this->assertEquals($new_date, $mediuser->deb_activite);

        return $mediuser;
    }

    /**
     * @depends testUpdateMediusers
     */
    public function testDeleteMediusers(CMediusers $mediuser): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/mediuser/mediusers/edit',
            ['user_id' => $mediuser->_id, 'del' => 1, 'ajax' => 1]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            $this->translator->tr('CMediusers-msg-delete'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $this->assertFalse(CMediusers::find($mediuser->_id));
    }

    public function testUpdateMediusersWithoutPermObject(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        $current_user  = CMediusers::get();
        $original_perm = CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] ?? null;
        try {
            CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] = 0;

            $client = self::createClient();
            $client->request(
                'POST',
                '/gui/mediuser/mediusers/edit',
                [
                    'user_id'         => $user->_id,
                    '_user_first_name' => 'foo bar',
                ]
            );

            $this->assertResponseStatusCodeSame(403);

            $this->assertStringContainsString(
                str_replace(
                    '/',
                    '\\/',
                    $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
                ),
                html_entity_decode($client->getResponse()->getContent())
            );
        } finally {
            if (null === $original_perm) {
                unset(CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] = $original_perm;
            }
        }
    }

    public function testUpdateMediusersWithoutPermModule(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        $module = CModule::getActive('mediusers');

        $current_user  = CMediusers::get();
        $original_perm = CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] ?? null;
        $original_view = CPermModule::$users_cache[$current_user->_id][$module->_id]['view'] ?? null;
        try {
            CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] = 0;
            CPermModule::$users_cache[$current_user->_id][$module->_id]['view']       = 0;

            $client = self::createClient();
            $client->request(
                'POST',
                '/gui/mediuser/mediusers/edit',
                [
                    'user_id'         => $user->_id,
                    '_user_first_name' => 'foo bar',
                ]
            );

            $this->assertResponseStatusCodeSame(403);

            $this->assertStringContainsString(
                str_replace(
                    '/',
                    '\\/',
                    $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
                ),
                html_entity_decode($client->getResponse()->getContent())
            );
        } finally {
            if (null === $original_perm) {
                unset(CPermModule::$users_cache[$current_user->_id][$module->_id]);
            } else {
                CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] = $original_perm;
                CPermModule::$users_cache[$current_user->_id][$module->_id]['view']       = $original_view;
            }
        }
    }

    public function testDeleteNoUserId(): void
    {
        $client = self::createClient();
        $client->request('POST', '/gui/mediuser/mediusers/edit', ['del' => '1',]);

        $this->assertResponseStatusCodeSame(400);

        $this->assertStringContainsString(
            $this->translator->tr('CUser-error-You cannot delete an unknown user or yourself'),
            html_entity_decode($client->getResponse()->getContent())
        );
    }

    public function testDeleteSelf(): void
    {
        $client = self::createClient();
        $client->request('POST', '/gui/mediuser/mediusers/edit', ['user_id' => CMediusers::get()->_id, 'del' => '1',]);

        $this->assertResponseStatusCodeSame(400);

        $this->assertStringContainsString(
            $this->translator->tr('CUser-error-You cannot delete an unknown user or yourself'),
            html_entity_decode($client->getResponse()->getContent())
        );
    }
}
