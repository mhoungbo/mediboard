<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CMbString;
use Ox\Core\CValue;
use Ox\Mediboard\Mediusers\CMediusers;

$user_id = CValue::get('user_id');

$user = CMediusers::get($user_id);


if ($user->getPerm(PERM_READ)) {
  $data = array(
    'last_name' => CMbString::utf8Encode($user->_user_last_name),
    'name'      => CMbString::utf8Encode($user->_user_first_name),
    'address'   => CMbString::utf8Encode($user->_user_adresse),
    'pc'        => CMbString::utf8Encode($user->_user_cp),
    'city'      => CMbString::utf8Encode($user->_user_ville),
    'phone'     => CMbString::utf8Encode($user->_user_phone),
    'email'     => CMbString::utf8Encode($user->_user_email),
    'apicrypt'  => CMbString::utf8Encode($user->mail_apicrypt),
    'mssante'   => CMbString::utf8Encode($user->mssante_address),
    'type'      => CMbString::utf8Encode($user->_user_type),
    'adeli'     => CMbString::utf8Encode($user->adeli),
    'rpps'      => CMbString::utf8Encode($user->rpps)
  );
}
else {
  $data = array();
}
$str = json_encode($data);
echo json_encode($data);
