/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class Mediuser extends OxObject {
    constructor () {
        super()
        this.type = "mediuser"
    }

    // Mediuser's info
    get initials (): string {
        let initials = ""

        if (super.get("initials")) {
            initials = super.get("initials")
        }
        else if (this.lastName) {
            initials = this.firstName
                ? this.firstName.charAt(0) + this.lastName.charAt(0)
                : this.lastName.charAt(0)
        }

        return initials
    }

    set initials (value: string) {
        super.set("initials", value)
    }

    get color (): OxAttr<string> {
        // _color is the possibly inherited from function color, color is the specific user color
        const color = super.get<OxAttr<string>>("_color") ?? super.get<OxAttr<string>>("color")
        return color ? "#" + color : undefined
    }

    /**
     * @param color - Hex color without #
     */
    set color (color: OxAttr<string>) {
        super.set("_color", color)
    }

    get specificColor (): OxAttr<string> {
        return super.get("color")
    }

    /**
     * @param value - Hex color without #
     */
    set specificColor (value: OxAttr<string>) {
        super.set("color", value)
    }

    get fontColor (): OxAttr<string> {
        return super.get("_font_color") !== undefined ? "#" + super.get("_font_color") : undefined
    }

    /**
     * @param color - Hex color without #
     */
    set fontColor (color: OxAttr<string>) {
        super.set("_font_color", color)
    }

    get lastName (): OxAttr<string> {
        return super.get("_user_last_name")
    }

    set lastName (value: OxAttr<string>) {
        super.set("_user_last_name", value)
    }

    get firstName (): OxAttr<string> {
        return super.get("_user_first_name")
    }

    set firstName (value: OxAttr<string>) {
        super.set("_user_first_name", value)
    }

    get fullName (): string {
        return (super.get("_user_last_name") ?? "") + (super.get("_user_first_name") ? " " + super.get("_user_first_name") : "")
    }

    get view (): string {
        return super.get("_view")
    }

    get username (): OxAttr<string> {
        return super.get("_user_username")
    }

    set username (value: OxAttr<string>) {
        super.set("_user_username", value)
    }

    get sex (): OxAttr<string> {
        return super.get("_user_sexe")
    }

    set sex (value: OxAttr<string>) {
        super.set("_user_sexe", value)
    }

    get actif (): OxAttr<boolean> {
        return super.get("actif")
    }

    set actif (value: OxAttr<boolean>) {
        super.set("actif", value)
    }

    get debActivite (): OxAttr<string> {
        return super.get("deb_activite")
    }

    set debActivite (value: OxAttr<string>) {
        super.set("deb_activite", value)
    }

    get finActivite (): OxAttr<string> {
        return super.get("fin_activite")
    }

    set finActivite (value: OxAttr<string>) {
        super.set("fin_activite", value)
    }

    get guid (): string {
        return "CMediusers-" + super.id
    }

    get userType (): OxAttr<number> {
        return super.get("_user_type")
    }

    set userType (value: OxAttr<number>) {
        super.set("_user_type", value)
    }

    get userTypeView (): OxAttr<string> {
        return super.get("_user_type_view")
    }

    // Security info
    get canChangePassword (): OxAttr<boolean> {
        return super.get("_can_change_password")
    }

    set canChangePassword (value: OxAttr<boolean>) {
        super.set("_can_change_password", value)
    }

    get isAdmin (): OxAttr<boolean> {
        return super.get("_is_admin")
    }

    set isAdmin (value: OxAttr<boolean>) {
        super.set("_is_admin", value)
    }

    get isPatient (): OxAttr<boolean> {
        return super.get("_is_patient")
    }

    set isPatient (value: OxAttr<boolean>) {
        super.set("_is_patient", value)
    }

    get isSSO (): OxAttr<boolean> {
        return super.get("_is_sso")
    }

    get impersonator (): OxAttr<{ firstname: string, lastname: string }> {
        return super.get("impersonator")
    }

    set impersonator (value: OxAttr<{ firstname: string, lastname: string }>) {
        super.set("impersonator", value)
    }

    get passwordRemainingDays (): OxAttr<string> {
        return super.get("psw_remaining_days")
    }

    set passwordRemainingDays (value: OxAttr<string>) {
        super.set("psw_remaining_days", value)
    }

    // Links
    get defaultUrl (): string {
        return this.links.default ?? ""
    }

    set defaultUrl (value: OxAttr<string>) {
        this.links.default = value
    }

    get editInfosUrl (): OxAttr<string> {
        return this.links.edit_infos
    }

    set editInfosUrl (value: OxAttr<string>) {
        this.links.edit_infos = value
    }

    get impersonationUrl (): OxAttr<string> {
        return this.links.impersonation
    }

    set impersonationUrl (value: OxAttr<string>) {
        this.links.impersonation = value
    }

    get exitImpersonationUrl (): OxAttr<string> {
        return this.links.exit_impersonation
    }

    set exitImpersonationUrl (value: OxAttr<string>) {
        this.links.exit_impersonation = value
    }

    get listUrl (): OxAttr<string> {
        return this.links.list
    }

    set listUrl (value: OxAttr<string>) {
        this.links.list = value
    }

    get changePasswordUrl (): OxAttr<string> {
        return this.links.change_password
    }

    set changePasswordUrl (value: OxAttr<string>) {
        this.links.change_password = value
    }

    get dismissReminderUrl (): OxAttr<string> {
        return this.links.dismiss_reminder
    }

    set dismissReminderUrl (value: OxAttr<string>) {
        this.links.dismiss_reminder = value
    }
}
