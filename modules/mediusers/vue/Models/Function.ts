/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class Function extends OxObject {
    constructor () {
        super()
        this.type = "function"
    }

    get groupId (): OxAttr<number> {
        return super.get("group_id")
    }

    set groupId (value: OxAttr<number>) {
        super.set("group_id", value)
    }

    // Specific appbar attr
    get isMain (): boolean {
        return super.get("is_main")
    }

    set isMain (value: boolean) {
        super.set("is_main", value)
    }

    get name (): string {
        return super.get("text")
    }

    set name (value: string) {
        super.set("text", value)
    }
}
