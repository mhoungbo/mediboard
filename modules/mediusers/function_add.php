<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CMbArray;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;

/**
 * Add mediuser's function, group or/and permission form
 */

CCanDo::checkEdit();

$group_id = CView::get("group_id", "ref class|CGroups");
$user_id  = CView::get("user_id", "ref class|CMediusers");

CView::checkin();

if ($user_id) {
    $user = CMediusers::findOrFail($user_id);
    $user->needsEdit();
}

$group     = new CGroups();
$groups    = CGroups::loadGroups(CPermObject::READ);
$functions = [];

if ($group->load($group_id)) {
    $functions = $group->loadFunctions(CPermObject::READ);
}

$perm_object = new CPermObject();

$token = null;

if ($group_id) {
    $token = AntiCsrf::prepare()
        ->addParam('user_id', $user_id)
        ->addParam('object_id', array_merge(
            CMbArray::pluck($groups, 'group_id'),
            CMbArray::pluck($functions, 'function_id') ?? []
        ))
        ->addParam('object_class', ['CGroups', 'CFunctions'])
        ->addParam('permission')
        ->getToken();
}

$smarty = new CSmartyDP();
$smarty->assign("groups", $groups);
$smarty->assign("functions", $functions);
$smarty->assign("group_id", $group_id);
$smarty->assign("user_id", $user_id);
$smarty->assign("perm_object", $perm_object);
$smarty->assign('token', $token);
$smarty->display("inc_function_add");
