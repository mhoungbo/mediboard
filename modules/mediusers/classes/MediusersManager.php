<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Mediusers;

use Exception;
use Ox\Core\CApp;
use Ox\Import\Rpps\Entity\CPersonneExercice;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Mediboard\System\CUserAuthentication;

class MediusersManager
{
    public function createReactivationAuth(CMediusers $mediuser): void
    {
        $auth                 = new CUserAuthentication();
        $auth->user_id        = $mediuser->_id;
        $auth->auth_method    = CUserAuthentication::AUTH_METHOD_REACTIVE;
        $auth->datetime_login = "now";
        $auth->ip_address     = $_SERVER['REMOTE_ADDR'];
        $auth->session_id     = CApp::getSessionHelper()->getId();

        if ($msg = $auth->store()) {
            throw new Exception($msg);
        }
    }

    public function createFunctionAndGroupPermissions(CMediusers $mediuser): void
    {
        $mediuser->insFunctionPermission();
        $mediuser->insGroupPermission();
    }

    public function removeFunctionAndGroupPermissions(CMediusers $mediuser): void
    {
        $mediuser->delFunctionPermission();
        $mediuser->delGroupPermission();
    }

    public function bindMediuserToMedecin(int $medecin_id, CMediusers $mediuser): void
    {
        $medecin = new CMedecin();
        $medecin->load($medecin_id);
        $medecin->user_id = $mediuser->_id;
        if ($msg = $medecin->store()) {
            throw new Exception($msg);
        }
    }

    public function bindMediuserToStructure(string $identifiant_structure, CMediusers $mediuser): void
    {
        $idex_personne_exercice = CIdSante400::getMatch(
            $mediuser->_class,
            CPersonneExercice::TAG_RPPS_IDENTIFIANT_STRUCTURE,
            $identifiant_structure,
            $mediuser->_id
        );

        if (!$idex_personne_exercice->_id) {
            if ($msg = $idex_personne_exercice->store()) {
                throw new Exception($msg);
            }
        }
    }

    public function duplicate(string $duplicate_login, CMediusers $mediuser): void
    {
        $user = $mediuser->loadRefUser();

        if ($user && $user->_id) {
            $user->_duplicate          = true;
            $user->_duplicate_username = $duplicate_login;

            if ($msg = $user->store()) {
                throw new Exception($msg);
            }
        }
    }

    public function createSecondaryUsers(array $secondary_users_ids, array $secondary_users_adelis): array
    {
        $errors = [];
        foreach ($secondary_users_ids as $index => $user_id) {
            $user = CMediusers::get($user_id);
            if ($user->_id && array_key_exists($index, $secondary_users_adelis)) {
                $user->adeli = $secondary_users_adelis[$index];

                $user->_user_password = null;
                if ($msg = $user->store()) {
                    $errors[] = $msg;
                }
            }
        }

        return $errors;
    }
}
