<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Mediusers;

use Exception;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;

class CSecondaryFunction extends CStoredObject
{
    /** @var string */
    public const RESOURCE_TYPE = 'secondaryFunction';

    // DB Table key
    public $secondary_function_id;

    // DB References
    public $function_id;
    public $user_id;

    /** @var CFunctions */
    public $_ref_function;

    /** @var CMediusers */
    public $_ref_user;

    /**
     * @see parent::getSpec()
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = 'secondary_function';
        $spec->key   = 'secondary_function_id';

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    public function getProps(): array
    {
        $specs                = parent::getProps();
        $specs["function_id"] = "ref notNull class|CFunctions back|secondary_functions fieldset|default";
        $specs["user_id"]     = "ref notNull class|CMediusers cascade back|secondary_functions";

        return $specs;
    }

    /**
     * @throws Exception
     * @see parent::updateFormFields()
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();
        $this->loadRefFunction();
        $this->loadRefUser();
        $this->_view      = $this->_ref_user->_view . " - " . $this->_ref_function->_view;
        $this->_shortview = $this->_ref_user->_shortview . " - " . $this->_ref_function->_shortview;
    }

    /**
     * @throws Exception
     * @deprecated
     * @see parent::loadRefsFwd()
     */
    function loadRefsFwd()
    {
        $this->loadRefFunction();
        $this->loadRefUser();
    }

    /**
     * Load function
     *
     * @return CFunctions
     * @throws Exception
     */
    public function loadRefFunction()
    {
        /** @var CFunctions */
        return $this->_ref_function = $this->loadFwdRef("function_id", true);
    }

    /**
     * Load mediuser
     *
     * @return CMediusers
     * @throws Exception
     */
    public function loadRefUser()
    {
        /** @var CMediusers */
        return $this->_ref_user = $this->loadFwdRef("user_id", true);
    }
}
