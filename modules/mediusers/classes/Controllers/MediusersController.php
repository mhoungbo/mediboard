<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Mediusers\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\CMbPath;
use Ox\Core\CMbSecurity;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Exception\HttpException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CCSVImportMediusers;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Mediusers\MediusersManager;
use Ox\Mediboard\System\Controllers\TabController;
use Ox\Mediboard\System\CTabHit;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MediusersController extends Controller
{
    public const LINK_EDIT_USER_INFOS = 'edit_infos';
    public const LINK_LOGOUT          = 'logout';
    public const LINK_DEFAULT_PAGE    = 'default';

    public const IS_MAIN_FUNCTION = 'is_main';

    public const MOST_CALLED_TABS_COUNT = 4;

    /**
     * @throws Exception
     * @api
     */
    public function listMediusers(RequestApi $request_api): Response
    {
        $type = $request_api->getRequest()->get("type", "prat");
        $name = $request_api->getRequest()->get("name");

        switch ($type) {
            case "prat":
            default:
                $mediusers = (new CMediusers())->loadPraticiens(
                    PERM_READ,
                    null,
                    $name,
                    false,
                    true,
                    true,
                    $request_api->getGroup()->_id
                );
                break;

            case "anesth":
                $mediusers = (new CMediusers())->loadAnesthesistes(PERM_READ, null, $name);
                break;
            case "pro_by_pref":
                $mediusers = (new CMediusers())->loadProfessionnelDeSanteByPref();
                break;
            case "all":
                $mediusers = (new CMediusers())->loadListFromType(
                    permType: CPermObject::READ,
                    name: $name
                );
        }

        $resource = Collection::createFromRequest($request_api, $mediusers);
        $resource->createLinksPagination($request_api->getOffset(), $request_api->getLimit(), count($mediusers));

        return $this->renderApiResponse($resource);
    }

    /**
     * @throws Exception
     * @api
     */
    public function showMediuser(RequestApi $request_api, CMediusers $mediuser): Response
    {
        $mediuser->loadRefFunction();

        return $this->renderApiResponse(Item::createFromRequest($request_api, $mediuser));
    }

    /**
     * @throws Exception
     * @throws HttpException
     * @api
     */
    public function showMediuserByRPPS(RequestApi $request_api): Response
    {
        $mediuser       = new CMediusers();
        $mediuser->rpps = $request_api->getRequest()->get("rpps");

        // Recherche par group_id et rpps
        if ($group_id = $request_api->getRequest()->get("group_id")) {
            $ljoin = ["functions_mediboard" => "functions_mediboard.function_id = users_mediboard.function_id"];
            $where = [
                "functions_mediboard.group_id" => $mediuser->getDS()->prepare("= ?", $group_id),
                "rpps"                         => $mediuser->getDS()->prepare("= ?", $mediuser->rpps),
            ];
            $users = $mediuser->loadList($where, ljoin: $ljoin);
            if ($users) {
                $mediuser = reset($users);
            }
        }

        // Par defaut ou si aucun mediuser trouv� sur le groupe cibl�, recherche g�n�rale
        if (!$mediuser->_id) {
            $mediuser->loadMatchingObject();
        }

        // Si aucun mediuser ne correspond au RPPS
        if (!$mediuser->_id) {
            throw new HttpException(Response::HTTP_NOT_FOUND, "Mediuser not found");
        }

        $mediuser->needsRead();
        $mediuser->loadRefFunction();

        return $this->renderApiResponse(Item::createFromRequest($request_api, $mediuser));
    }

    /**
     * @throws ApiException
     * @api
     */
    public function listFunctions(CMediusers $mediusers, RequestApi $request_api): Response
    {
        $main_function = $mediusers->loadRefFunction();

        $collection = Collection::createFromRequest(
            $request_api,
            array_merge([$main_function],
                        $mediusers->loadRefsSecondaryFunctions())
        );

        // Add group_id
        $collection->addModelFieldset(['default', 'target']);

        /** @var Item $item */
        foreach ($collection as $item) {
            /** @var CFunctions $function */
            $function = $item->getDatas();

            $item->addAdditionalDatas(
                [
                    self::IS_MAIN_FUNCTION => ($function->_id === $main_function->_id),
                ]
            );
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws Exception
     * @api
     */
    public function listHits(RequestApi $request_api): Response
    {
        $tabs = (new CTabHit())->getMostCalledTabs($this->getCMediuser(), self::MOST_CALLED_TABS_COUNT);

        $collection = Collection::createFromRequest($request_api, $tabs);

        /** @var Collection $item */
        foreach ($collection as $item) {
            $item->setType(TabController::TAB_RESOURCE_TYPE);
            $item->addLinks(
                [
                    TabController::LINK_TAB_URL => $item->getDatas()->getUrl(),
                ]
            );
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws CMbException
     * @throws ApiException
     * @api
     */
    public function importMediusers(RequestApi $request_api): Response
    {
        /** @var UploadedFile $uploaded_file */
        $uploaded_file = $request_api->getRequest()->files->get('import_file');
        if (!$uploaded_file) {
            throw new CMbException('MediusersController-Error-File is mandatory for import');
        }

        $file = $uploaded_file->getRealPath();

        $update  = $request_api->getRequest()->get('update_found_users', false);
        $dry_run = $request_api->getRequest()->get('dry_run', true);

        $import = new CCSVImportMediusers($file, $dry_run, $update, 0, CCSVImportMediusers::MAX_LINES);

        try {
            $import->import();
        } catch (Exception $e) {
            CMbPath::remove($file);

            throw $e;
        }

        CMbPath::remove($file);

        $data = $this->buildData($import->getFound(), $import->getCreated(), $import->getErrors());

        $return_code = count($data['created']) ? 201 : 200;

        return $this->renderApiResponse(new Item($data), $return_code);
    }

    /**
     * Create, Update or Delete a CMediusers.
     *
     * @param RequestParams    $params
     * @param MediusersManager $manager
     *
     * @return Response
     * @throws Exception
     */
    public function edit(RequestParams $params, MediusersManager $manager): Response
    {
        if (CApp::isReadonly()) {
            $this->addUiMsgError('Mode-readonly-title', true);
            $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $request = $params->getRequest();

        $current_user = $this->getCUser();
        $this->removeForbiddenFields($request, $current_user);

        $values = AntiCsrf::validateSfRequest($request);

        try {
            $mediuser = $this->instanciateMediuser(
                isset($values['user_id']) && $values['user_id']
                    ? $values['user_id']
                    : null,
                $current_user
            );
        } catch (AccessDeniedException) {
            $this->addUiMsgError('common-msg-You are not allowed to access this information (%s)', true);

            return $this->renderEmptyResponse(Response::HTTP_FORBIDDEN);
        }

        if ($params->post('del', 'bool default|0')) {
            return $this->delete($mediuser, (bool)$params->post('purge', 'bool default|0'));
        }

        /** @var CMediusers $old_user */
        $old_user = $mediuser->loadOldObject();

        $this->bindValues($mediuser, $values);

        if (!$mediuser->user_id) {
            $mediuser->_user_password = CMbSecurity::getRandomPassword($mediuser->_specs['_user_password']);
        }

        $function_modified = $mediuser->fieldModified("function_id");

        if ($msg = $mediuser->store()) {
            $this->addUiMsgError($msg, true);

            return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->addUiMsgOk($old_user->_id ? 'CMediusers-msg-modify' : 'CMediusers-msg-create', true);

        $this->handlePostStoreUpdates(
            $manager,
            $mediuser,
            $old_user,
            $function_modified,
            (int)$params->post('_medecin_id', 'ref class|CMedecin'),
            $params->post('_personne_exercice_identifiant_structure', 'str'),
            (bool)$params->post('_duplicate', 'bool default|0'),
            $params->post('_duplicate_username', 'str'),
            $request->request->get('_secondary_user_id') ?: [],
            $request->request->get('_secondary_user_adeli') ?: [],
        );

        if ($request->request->get('ajax')) {
            CAppUI::callbackAjax("Form.onSubmitComplete", $mediuser->_guid);

            return $this->renderEmptyResponse();
        }

        $response = $this->renderEmptyResponse(Response::HTTP_FOUND);

        $response->headers->set('Location', $request->server->get('HTTP_REFERER'));

        return $response;
    }

    private function buildData(array $found, array $created, array $errors): array
    {
        $data = [
            'created' => [],
            'found'   => [],
            'errors'  => $errors,
        ];

        foreach ($found as $short_class => $count) {
            $data['found'][] = $this->translator->tr($short_class . '-msg-found') . ' (x ' . $count . ')';
        }

        foreach ($created as $short_class => $count) {
            $data['created'][] = $this->translator->tr($short_class . '-msg-create') . ' (x ' . $count . ')';
        }

        return $data;
    }

    private function removeForbiddenFields(Request $request, CUser $current_user): void
    {
        // we don't allow anybody to change his user type or profile
        if (!$current_user->isAdmin() && !CModule::getCanDo("admin")->admin) {
            if ($request->request->get('_user_type' == 1)) {
                $request->request->set('_user_type', 14);
                $request->request->remove('_profile_id');
            } elseif ($request->request->get('user_id')) {
                $request->request->remove('_profile_id');
            }
        }
    }

    /**
     * TODO Check should redirect ?
     *
     * @param CMediusers $mediuser
     * @param bool       $purge
     *
     * @return Response
     * @throws Exception
     */
    private function delete(CMediusers $mediuser, bool $purge = false): Response
    {
        if (!$mediuser->_id || $mediuser->_id == $this->getCMediuser()->_id) {
            $this->addUiMsgError('CUser-error-You cannot delete an unknown user or yourself', true);

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }

        $user = $mediuser->loadRefUser();

        if ($purge) {
            CApp::setTimeLimit(120);

            if ($msg = $user->purge()) {
                $this->addUiMsgError($msg, true);

                return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->addUiMsgAlert('msg-purge', true);

            return $this->renderEmptyResponse();
        }

        if ($msg = $user->delete()) {
            $this->addUiMsgError($msg, true);

            return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->addUiMsgAlert('CMediusers-msg-delete', true);

        return $this->renderEmptyResponse();
    }

    private function instanciateMediuser(?int $user_id, CUser $current_user): CMediusers
    {
        if ($user_id) {
            $mb_user = CMediusers::findOrFail($user_id);

            // if user not itself, must have edit rights on module and on object
            if (
                $current_user->_id != $mb_user->_id
                && (
                    !$this->checkModulePermEdit()
                    || !$mb_user->getPerm(CPermObject::EDIT)
                )
            ) {
                throw new AccessDeniedException();
            }

            return $mb_user;
        }

        return new CMediusers();
    }

    /**Bind autorised fields to the $mediuser.
     *
     * @param CMediusers $mediuser
     * @param array      $values
     *
     * @return void
     */
    private function bindValues(CMediusers $mediuser, array $values): void
    {
        // Avoid trying to store the password loaded from DB.
        $mediuser->_user_password = null;

        $mediuser->_user_username          = $values['_user_username'] ?? null;
        $mediuser->_user_type              = $values['_user_type'] ?? null;
        $mediuser->_user_sexe              = $values['_user_sexe'] ?? null;
        $mediuser->_user_birthday          = $values['_user_birthday'] ?? null;
        $mediuser->color                   = $values['color'] ?? null;
        $mediuser->actif                   = $values['actif'] ?? null;
        $mediuser->deb_activite            = $values['deb_activite'] ?? null;
        $mediuser->fin_activite            = $values['fin_activite'] ?? null;
        $mediuser->activite                = $values['activite'] ?? null;
        $mediuser->use_bris_de_glace       = $values['use_bris_de_glace'] ?? null;
        $mediuser->remote                  = $values['remote'] ?? null;
        $mediuser->_force_change_password  = $values['_force_change_password'] ?? null;
        $mediuser->function_id             = $values['function_id'] ?? null;
        $mediuser->_profile_id             = $values['_profile_id'] ?? null;
        $mediuser->_user_last_name         = $values['_user_last_name'] ?? null;
        $mediuser->_user_first_name        = $values['_user_first_name'] ?? null;
        $mediuser->initials                = $values['initials'] ?? null;
        $mediuser->_user_email             = $values['_user_email'] ?? null;
        $mediuser->_user_phone             = $values['_user_phone'] ?? null;
        $mediuser->_internal_phone         = $values['_internal_phone'] ?? null;
        $mediuser->astreinte               = $values['astreinte'] ?? null;
        $mediuser->_user_astreinte         = $values['_user_astreinte'] ?? null;
        $mediuser->_user_astreinte_autre   = $values['_user_astreinte_autre'] ?? null;
        $mediuser->commentaires            = $values['commentaires'] ?? null;
        $mediuser->discipline_id           = $values['discipline_id'] ?? null;
        $mediuser->spec_cpam_id            = $values['spec_cpam_id'] ?? null;
        $mediuser->other_specialty_id      = $values['other_specialty_id'] ?? null;
        $mediuser->adeli                   = $values['adeli'] ?? null;
        $mediuser->rpps                    = $values['rpps'] ?? null;
        $mediuser->sub_psc                 = $values['sub_psc'] ?? null;
        $mediuser->cps                     = $values['cps'] ?? null;
        $mediuser->mail_apicrypt           = $values['mail_apicrypt'] ?? null;
        $mediuser->mssante_address         = $values['mssante_address'] ?? null;
        $mediuser->secteur                 = $values['secteur'] ?? null;
        $mediuser->pratique_tarifaire      = $values['pratique_tarifaire'] ?? null;
        $mediuser->ccam_context            = $values['ccam_context'] ?? null;
        $mediuser->mode_tp_acs             = $values['mode_tp_acs'] ?? null;
        $mediuser->cab                     = $values['cab'] ?? null;
        $mediuser->conv                    = $values['conv'] ?? null;
        $mediuser->zisd                    = $values['zisd'] ?? null;
        $mediuser->ik                      = $values['ik'] ?? null;
        $mediuser->inami                   = $values['inami'] ?? null;
        $mediuser->titres                  = $values['titres'] ?? null;
        $mediuser->compta_deleguee         = $values['compta_deleguee'] ?? null;
        $mediuser->compte                  = $values['compte'] ?? null;
        $mediuser->banque_id               = $values['banque_id'] ?? null;
        $mediuser->nom_destinataire_favori = $values['nom_destinataire_favori'] ?? null;
        $mediuser->destinataire_favori     = $values['destinataire_favori'] ?? null;
        $mediuser->main_user_id            = $values['main_user_id'] ?? null;
        $mediuser->code_intervenant_cdarr  = $values['code_intervenant_cdarr'] ?? null;
    }

    private function handlePostStoreUpdates(
        MediusersManager $manager,
        CMediusers       $mediuser,
        CMediusers       $old_user,
        bool             $function_modified,
        ?int             $medecin_id,
        ?string          $identifier_structure,
        bool             $duplicate,
        ?string          $new_username,
        array            $secondary_users_ids,
        array            $secondary_users_adelis,
    ): void {
        if (!$mediuser->fin_activite && $old_user->fin_activite) {
            try {
                $manager->createReactivationAuth($mediuser);
            } catch (Exception $e) {
                $this->addUiMsgWarning($e->getMessage(), true);
            }
        }

        if (!$old_user->_id || $function_modified) {
            if ($old_user->_id) {
                $manager->removeFunctionAndGroupPermissions($old_user);
            }

            $manager->createFunctionAndGroupPermissions($mediuser);
        }

        if ($medecin_id) {
            try {
                $manager->bindMediuserToMedecin($medecin_id, $mediuser);
            } catch (Exception $e) {
                $this->addUiMsgWarning($e->getMessage(), true);
            }
        }

        if ($identifier_structure) {
            try {
                $manager->bindMediuserToStructure($identifier_structure, $mediuser);
            } catch (Exception $e) {
                $this->addUiMsgWarning($e->getMessage(), true);
            }
        }

        if ($duplicate && $new_username) {
            try {
                $manager->duplicate($new_username, $mediuser);
            } catch (Exception $e) {
                $this->addUiMsgWarning($e->getMessage(), true);
            }
        }

        if ($secondary_users_ids && $secondary_users_adelis) {
            $errors = $manager->createSecondaryUsers($secondary_users_ids, $secondary_users_adelis);
            foreach ($errors as $error) {
                $this->addUiMsgWarning($error, true);
            }
        }
    }
}
