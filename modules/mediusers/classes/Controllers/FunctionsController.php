<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Mediusers\Controllers;

use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFieldsets;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Mediboard\Mediusers\CFunctions;
use Symfony\Component\HttpFoundation\Response;

class FunctionsController extends Controller
{
    /**
     * @throws ApiException|CMbException
     * @api
     */
    public function create(RequestApi $request_api): Response
    {
        $messages_supported = $request_api->getModelObjectCollection(
            CFunctions::class,
            [RequestFieldsets::QUERY_KEYWORD_ALL],
        );

        $collection = $this->storeCollection($messages_supported);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }
}
