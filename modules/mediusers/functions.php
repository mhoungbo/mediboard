<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CMbArray;
use Ox\Core\CSmartyDP;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Core\Security\Csrf\AntiCsrfToken;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Mediusers\CSecondaryFunction;

/**
 * Edit mediuser's functions
 */

CCanDo::checkRead();

$user_id = CView::get("user_id", "ref class|CMediusers");
CView::checkin();

// Fonction principale
$user = new CMediusers();
$user->load($user_id);
$function                    = $user->loadRefFunction();
$temporary_sf                = new CSecondaryFunction();
$group                       = $function->loadRefGroup();
$temporary_sf->function_id   = $function->_id;
$temporary_sf->_ref_function = $function;

$perm  = new CPermObject();
$where = [
    "user_id"      => " = '$user_id'",
    "object_class" => " IN ('CFunctions', 'CGroups')",
];

// Permissions hors-fonctions/�tablissements
$others_perms = $perm->loadList($where);
$perm_object_ids = CMbArray::pluck($others_perms, '_id');
$perms        = ["CFunctions" => [], "CGroups" => []];
foreach ($others_perms as $_perm) {
    $perms[$_perm->object_class][$_perm->object_id] = $_perm;
}

/*
 * Variable contenant l'ensemble des Groups, des Fonctions et des PermObject � afficher
 * Se pr�sente sous cette forme :
 *  root => [{
 *    "object"        => Objet CGroups,
 *    "perm_object"   => Objet CPermObject li� au CGroup,
 *    "functions"     => [{
 *      "type"        => Type de la fonction : primary|secondary|permission,
 *      "object"      => Objet CFunctions,
 *      "perm_object" => Object CPermObject li� au CFunctions
 *    }]
 *  }]
 */
$groups = [
    $function->group_id => [
        "object"      => $group,
        "perm_object" => ($perms["CGroups"][$function->group_id] ?? null),
        "functions"   => [
            [
                "type"        => "primary",
                "object"      => $temporary_sf,
                "perm_object" => ($perms["CFunctions"][$function->_id] ?? null),
            ],
        ],
    ],
];

// Retrait de la fonction des permissions hors-fonctions/�tablissements
foreach ($others_perms as $_key => $_perm) {
    if ($_perm->object_class === 'CFunctions' && $_perm->object_id === $function->_id) {
        unset($others_perms[$_key]);
    }
}

// Fonctions secondaires
$user->loadRefsSecondaryFunctions();
$secondary_functions = $user->_ref_secondary_functions;
// Tri de la collection
usort(
    $secondary_functions,
    function (CSecondaryFunction $a, CSecondaryFunction $b) {
        return strcmp($a->_ref_function->text, $b->_ref_function->text);
    }
);

// Flag : La fonction principale n'est pas pr�sente en fonction secondaire
$auto_down_pf = true;
foreach ($secondary_functions as $_sec_func) {
    if (!isset($groups[$_sec_func->_ref_function->group_id])) {
        $groups[$_sec_func->_ref_function->group_id] = [
            "object"      => $_sec_func->_ref_function->loadRefGroup(),
            "perm_object" => ($perms["CGroups"][$_sec_func->_ref_function->group_id] ?? null),
            "functions"   => [],
        ];
    }
    $groups[$_sec_func->_ref_function->group_id]["functions"][] = [
        "type"        => "secondary",
        "object"      => $_sec_func,
        "perm_object" => ($perms["CFunctions"][$_sec_func->function_id] ?? null),
    ];
    if ($user->function_id === $_sec_func->function_id) {
        $auto_down_pf = false;
    }

    foreach ($others_perms as $_key => $_perm) {
        if ($_perm->object_class === 'CFunctions' && $_perm->object_id === $_sec_func->function_id) {
            unset($others_perms[$_key]);
        }
    }
}

// Pr�paration des permissions hors-fonctions, hors-etablissement
$others_perms_by_type = ["CFunctions" => [], "CGroups" => []];
foreach ($others_perms as $_perm) {
    $others_perms_by_type[$_perm->object_class][] = $_perm->object_id;
}
$where              = [
    "function_id" => CSQLDataSource::prepareIn($others_perms_by_type["CFunctions"]),
];
$temporary_function = new CFunctions();
$others_functions   = $temporary_function->loadList($where);

// Permissions hors-fonctions
foreach ($others_functions as $_function) {
    if (!isset($groups[$_function->group_id])) {
        $groups[$_function->group_id] = [
            "object"      => $_function->loadRefGroup(),
            "perm_object" => ($perms["CGroups"][$_function->group_id] ?? null),
            "functions"   => [],
        ];
    }
    $secondary_function                          = new CSecondaryFunction();
    $secondary_function->function_id             = $_function->_id;
    $secondary_function->_ref_function           = $_function;
    $secondary_function->user_id                 = $user_id;
    $groups[$_function->group_id]["functions"][] = [
        "type"        => "permission",
        "object"      => $secondary_function,
        "perm_object" => $perms["CFunctions"][$_function->_id],
    ];
}

// Permissions hors-etablissement
$where           = [
    "group_id" => CSQLDataSource::prepareIn($others_perms_by_type["CGroups"]),
];
$temporary_group = new CGroups();
$others_groups   = $temporary_group->loadList($where);

foreach ($others_groups as $_group) {
    if (!isset($groups[$_group->_id])) {
        $groups[$_group->_id] = [
            "object"      => $_group,
            "perm_object" => null,
            "functions"   => [],
        ];
    }

    $groups[$_group->_id]["perm_object"] = $perms["CGroups"][$_group->_id];
}

$empty_perm = new CPermObject();

$token_add_perm = AntiCsrf::prepare()
    ->addParam('user_id', $user->_id)
    ->addParam('permission', 1)
    ->addParam('object_id')
    ->addParam('object_class', ['CGroups', 'CFunctions'])
    ->getToken();

$token_edit_perm = AntiCsrf::prepare()
    ->addParam('perm_object_id', $perm_object_ids)
    ->addParam('permission')
    ->getToken();

$user_token = AntiCsrf::prepare()
    ->addParam('user_id', $user->_id)
    ->addParam('function_id')
    ->getToken();

$smarty = new CSmartyDP();
$smarty->assign("user", $user);
$smarty->assign("groups", $groups);
$smarty->assign("auto_down_pf", $auto_down_pf);
$smarty->assign("empty_perm", $empty_perm);
$smarty->assign('token_add_perm', $token_add_perm);
$smarty->assign('token_edit_perm', $token_edit_perm);
$smarty->assign('user_token', $user_token);
$smarty->display("inc_functions");
