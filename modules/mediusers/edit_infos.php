<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CValue;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Mediusers\CDiscipline;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Mediusers\CSpecCPAM;

CCanDo::check();

$mediuser = CMediusers::get();
$mediuser->loadRefFunction();
$mediuser->loadRefSpecCPAM();
$mediuser->loadRefDiscipline();
$mediuser->loadRefBanque();
$mediuser->loadRefsSecondaryUsers();
$mediuser->_ref_user->isLDAPLinked();
$mediuser->loadNamedFile("identite.jpg");
$mediuser->loadNamedFile("signature.jpg");

// Récupération des disciplines
$disciplines = new CDiscipline();
$disciplines = $disciplines->loadList();

$affiche_nom = CValue::get("affiche_nom", 0);

$user_token = AntiCsrf::prepare()
    ->addParam('user_id', $mediuser->_id)
    ->addParam('_user_type', $mediuser->_user_type)
    ->addParam('_user_last_name')
    ->addParam('_user_first_name')
    ->addParam('_user_sexe')
    ->addParam('_user_birthday')
    ->addParam('discipline_id')
    ->addParam('spec_cpam_id')
    ->addParam('other_specialty_id')
    ->addParam('other_specialty_id_autocomplete_view')
    ->addParam('adeli')
    ->addParam('_secondary_user_id[]')
    ->addParam('_secondary_user_adeli[]')
    ->addParam('rpps')
    ->addParam('sub_psc')
    ->addParam('cps')
    ->addParam('mail_apicrypt')
    ->addParam('mssante_address')
    ->addParam('secteur')
    ->addParam('pratique_tarifaire')
    ->addParam('ccam_context')
    ->addParam('mode_tp_acs')
    ->addParam('cab')
    ->addParam('conv')
    ->addParam('zisd')
    ->addParam('ik')
    ->addParam('inami')
    ->addParam('titres')
    ->addParam('compta_deleguee')
    ->addParam('compte')
    ->addParam('banque_id')
    ->addParam('banque_id_autocomplete_view')
    ->addParam('_user_email')
    ->addParam('_user_phone')
    ->addParam('_user_astreinte')
    ->addParam('_user_astreinte_autre')
    ->addParam('nom_destinataire_favori')
    ->addParam('destinataire_favori')
    ->getToken();

$smarty = new CSmartyDP();
$smarty->assign("b2g", CAppUI::gconf("admin CBrisDeGlace enable_bris_de_glace"));
$smarty->assign("disciplines", $disciplines);
$smarty->assign("spec_cpam", CSpecCPAM::getList());
$smarty->assign("fonction", $mediuser->_ref_function);
$smarty->assign("user", $mediuser);
$smarty->assign("affiche_nom", $affiche_nom);
$smarty->assign("dPboard_name", CAppUI::tr('module-dPboard-court'));
$smarty->assign("user_token", $user_token);
$smarty->display("edit_infos.tpl");
