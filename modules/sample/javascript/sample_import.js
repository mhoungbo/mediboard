/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

SampleImport = window.SampleImport || {
  importCategories: function (import_url, token) {
    if (confirm($T('SampleCategoryImport-Ask-Import-category?'))) {
      new Url()
        .setRoute(import_url)
        .addParam('token',token)
        .requestUpdate('result-import-categories', {method: "post"});
    }
  },

  importNationalities: function (import_url, token) {
    if (confirm($T('SampleNationalityImport-Ask-Import-nationality?'))) {
      new Url()
        .setRoute(import_url)
        .addParam('token',token)
        .requestUpdate('result-import-nationalities', {method: "post"});
    }
  },

  importMovies: function (import_url, csrf_token) {
    if (confirm($T('SampleMovieImport-Ask-Import-movies?'))) {
      new Url()
        .setRoute(import_url)
        .addParam('token', csrf_token)
        .requestUpdate('result-import-movies', {method: 'POST'});
    }
  }
};
