<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Tests\Fixtures;

use Ox\Mediboard\Sample\Entities\CSampleMovie;
use Ox\Mediboard\Sample\Entities\CSampleRating;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Create rating for a random user on 2 movies.
 */
class SampleRatingFixtures extends Fixtures implements GroupFixturesInterface
{
    public const RATING_USER_FIXTURES = 'rating_user_fixtures';

    /**
     * @inheritDoc
     */
    public function load()
    {
        $user = $this->getUser(false);
        $this->store($user, self::RATING_USER_FIXTURES);

        $movie1 = $this->getReference(CSampleMovie::class, SampleMovieFixtures::MOVIE_1);
        $movie2 = $this->getReference(CSampleMovie::class, SampleMovieFixtures::MOVIE_2);

        $rating           = new CSampleRating();
        $rating->owner_id = $user->_id;
        $rating->movie_id = $movie1->_id;
        $rating->rating   = 4.0;
        $this->store($rating);

        $rating           = new CSampleRating();
        $rating->owner_id = $user->_id;
        $rating->movie_id = $movie2->_id;
        $rating->rating   = 1.0;
        $this->store($rating);
    }

    /**
     * @inheritDoc
     */
    public static function getGroup(): array
    {
        return ['sample_fixtures', 50];
    }
}
