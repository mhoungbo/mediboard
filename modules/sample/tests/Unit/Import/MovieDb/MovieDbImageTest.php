<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Tests\Unit\Import\MovieDb;

use Ox\Mediboard\Sample\Import\MovieDb\MovieDbImage;
use Ox\Tests\OxUnitTestCase;

class MovieDbImageTest extends OxUnitTestCase
{
    public function testSerialize(): void
    {
        $image = new MovieDbImage('foo', 'bar');

        $this->assertEquals(
            json_encode(
                ['id' => null, 'type' => 'file', 'attributes' => ['file_type' => 'foo', '_base64_content' => 'bar']]
            ),
            json_encode($image)
        );
    }
}
