<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Tests\Unit\Import\MovieDb;

use Ox\Core\CMbException;
use Ox\Mediboard\Sample\Import\MovieDb\MovieDbImageConfiguration;
use Ox\Tests\OxUnitTestCase;

class MovieDbImageConfigurationTest extends OxUnitTestCase
{
    /**
     * @dataProvider badConfigurationProvider
     */
    public function testBadConfiguration(array $config): void
    {
        $this->expectExceptionObject(
            new CMbException('MovieDbImageConfiguration-Error-The-request-does-not-contains-base_url')
        );

        new MovieDbImageConfiguration($config);
    }

    /**
     * @dataProvider configurationProvider
     */
    public function testConfiguration(array $config, string $profile, string $poster): void
    {
        $configuration = new MovieDbImageConfiguration($config);

        $this->assertEquals($profile, $configuration->getProfilePrefix());
        $this->assertEquals($poster, $configuration->getPosterPrefix());
    }

    public function badConfigurationProvider(): array
    {
        return [
            'empty_array'             => [[]],
            'config_no_url'           => [['images' => ['profile_sizes' => 'foo', 'poster_sizes' => 'bar']]],
            'config_no_profile_sizes' => [['images' => ['base_url' => 'foo', 'poster_sizes' => 'bar']]],
            'config_no_poster_sizes'  => [['images' => ['profile_sizes' => 'foo', 'base_url' => 'bar']]],
        ];
    }

    public function configurationProvider(): array
    {
        return [
            'with_prefered_size' => [
                [
                    'images' => [
                        'base_url' => 'foo', 'profile_sizes' => ['h632', 'bar'], 'poster_sizes' => ['w780', 'bar']
                    ]
                ],
                'fooh632',
                'foow780'
            ],
            'without_prefered_size' => [
                [
                    'images' => [
                        'base_url' => 'foo', 'profile_sizes' => ['size1', 'size2'], 'poster_sizes' => ['size3', 'size4']
                    ]
                ],
                'foosize2',
                'foosize4'
            ]
        ];
    }
}
