<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Tests\Unit\Import\MovieDb;

use Ox\Core\CMbString;
use Ox\Mediboard\Sample\Import\MovieDb\MovieDbImage;
use Ox\Mediboard\Sample\Import\MovieDb\MovieDbMovie;
use Ox\Tests\OxUnitTestCase;

class MovieDbMovieTest extends OxUnitTestCase
{
    public function testSerialize(): void
    {
        $movie = new MovieDbMovie(
            [
                'id'               => 'foo',
                'title'            => 'bl�bl�',
                'release_date'     => '2023-02-02',
                'director'         => 10,
                'runtime'          => 75,
                'spoken_languages' => [['iso_639_1' => 'es']],
                'poster'           => new MovieDbImage('foo', 'bar'),
            ]
        );

        $image = new MovieDbImage('foo', 'bar');

        $result = $movie->jsonSerialize();
        $this->assertNull($result['id']);
        $this->assertEquals('sample_movie', $result['type']);
        $this->assertEquals('2023-02-02', $result['attributes']['release']);
        $this->assertEquals(CMbString::utf8Encode('bl�bl�'), $result['attributes']['name']);
        $this->assertEquals('01:15:00', $result['attributes']['duration']);
        $this->assertEquals('es', $result['attributes']['languages']);
        $this->assertArrayHasKey('csa', $result['attributes']);
        $this->assertEquals(
            [
                'director' => ['data' => ['id' => 10, 'type' => 'sample_person']],
                'category' => ['data' => ['id' => null, 'type' => 'sample_category']],
                'cover'    => ['data' => $image],
            ],
            $result['relationships']
        );
    }

    public function testConvertCasting(): void
    {
        $movie = new MovieDbMovie(
            [
                'id' => 'foo',
                'casting' => [10, 20, 30]
            ]
        );

        $this->assertEquals(
            [
                [
                    'type' => 'sample_casting',
                    'id' => null,
                    'attributes' => ['is_main_actor' => '1'],
                    'relationships' => ['actor' => ['data' => ['type' => 'sample_person', 'id' => 10]]]
                ],
                [
                    'type' => 'sample_casting',
                    'id' => null,
                    'attributes' => ['is_main_actor' => '0'],
                    'relationships' => ['actor' => ['data' => ['type' => 'sample_person', 'id' => 20]]]
                ],
                [
                    'type' => 'sample_casting',
                    'id' => null,
                    'attributes' => ['is_main_actor' => '0'],
                    'relationships' => ['actor' => ['data' => ['type' => 'sample_person', 'id' => 30]]]
                ],
            ],
            $movie->convertCasting()
        );
    }
}
