<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Tests\Unit\Import\MovieDb;

use Nyholm\Psr7\Response;
use Ox\Core\Contracts\Client\HTTPClientInterface;
use Ox\Mediboard\Sample\Controllers\SampleMoviesController;
use Ox\Mediboard\Sample\Controllers\SamplePersonsController;
use Ox\Mediboard\Sample\Exceptions\Import\SampleMovieImportException;
use Ox\Mediboard\Sample\Import\MovieDb\MovieDbImage;
use Ox\Mediboard\Sample\Import\MovieDb\SampleMovieImport;
use Ox\Mediboard\System\CSourceHTTP;
use Ox\Tests\OxUnitTestCase;

class SampleMovieImportTest extends OxUnitTestCase
{
    public function testGetTrailerUrlWithoutHttpSource(): void
    {
        $import = new SampleMovieImport(new SamplePersonsController(), new SampleMoviesController(), new CSourceHTTP());

        $this->expectExceptionObject(SampleMovieImportException::httpSourceNotFound());

        $import->getTrailerUrl('foo');
    }

    public function testGetTrailerUrl(): void
    {
        $client = $this->getMockBuilder(HTTPClientInterface::class)
                       ->getMock();
        $client->method('request')->willReturn(
            new Response(
                      200,
                body: json_encode(
                          [
                              'results' => [
                                  [
                                      'iso_639_1'    => 'en',
                                      'published_at' => '2023-07-13T18:29:03.000Z',
                                      'type'         => 'Featurette',
                                      'site'         => 'YouTube',
                                      'key'          => 1,
                                  ],
                                  [
                                      'iso_639_1'    => 'en',
                                      'published_at' => '2023-07-13T16:29:03.000Z',
                                      'type'         => 'Trailer',
                                      'site'         => 'YouTube',
                                      'key'          => 2,
                                  ],
                                  [
                                      'iso_639_1'    => 'fr',
                                      'published_at' => '2023-07-13T20:29:03.000Z',
                                      'type'         => 'Trailer',
                                      'site'         => 'YouTube',
                                      'key'          => 3,
                                  ],
                              ],
                          ]
                      )
            )
        );

        $source          = new CSourceHTTP();
        $source->_client = $client;
        $source->_id     = 10;

        $import = new SampleMovieImport(new SamplePersonsController(), new SampleMoviesController(), $source);

        $this->assertEquals('https://youtube.com/watch?v=3', $import->getTrailerUrl('foo'));
    }

    public function testImportMoviesWithoutHttpSource(): void
    {
        $import = new SampleMovieImport(new SamplePersonsController(), new SampleMoviesController(), new CSourceHTTP());

        $this->expectExceptionObject(SampleMovieImportException::httpSourceNotFound());

        $import->importMovies();
    }
}
