<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Tests\Unit\Import\MovieDb;

use Ox\Mediboard\Sample\Import\MovieDb\MovieDbImage;
use Ox\Mediboard\Sample\Import\MovieDb\MovieDbPerson;
use Ox\Tests\OxUnitTestCase;

class MovieDbPersonTest extends OxUnitTestCase
{
    public function testSerialize(): void
    {
        $image = new MovieDbImage('foo', 'bar');

        $person = new MovieDbPerson(
            [
                'id'       => 'foo',
                'gender'   => 1,
                'name'     => 'Foo Bar',
                'birthday' => '1970-01-01',
                'director' => 1,
                'profile'  => $image,
            ]
        );

        $this->assertEquals(
            [
                'id'            => null,
                'type'          => 'sample_person',
                'attributes'    => [
                    'last_name'   => 'Bar',
                    'first_name'  => 'Foo',
                    'sex'         => 'f',
                    'birthdate'   => '1970-01-01',
                    'is_director' => '1',
                ],
                'relationships' => [
                    'profilePicture' => ['data' => $image],
                ],
            ],
            $person->jsonSerialize()
        );
    }
}
