<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Tests\Unit;

use Ox\Mediboard\Sample\RatingStatistic;
use Ox\Tests\OxUnitTestCase;

class RatingStatisticTest extends OxUnitTestCase
{
    public function testAddRating(): void
    {
        $stats = new RatingStatistic();
        $stats->addRating(1);
        $this->assertCount(1, $this->getPrivateProperty($stats, 'ratings'));
        $this->assertEquals([1 => 1], $this->getPrivateProperty($stats, 'counts'));

        $stats->addRating(1);
        $this->assertCount(2, $this->getPrivateProperty($stats, 'ratings'));
        $this->assertEquals([1 => 2], $this->getPrivateProperty($stats, 'counts'));

        $stats->addRating(20);
        $this->assertCount(3, $this->getPrivateProperty($stats, 'ratings'));
        $this->assertEquals([1 => 2, 20 => 1], $this->getPrivateProperty($stats, 'counts'));

        $stats->addRating(0);
        $this->assertCount(4, $this->getPrivateProperty($stats, 'ratings'));
        $this->assertEquals([0 => 1, 1 => 2, 20 => 1], $this->getPrivateProperty($stats, 'counts'));

        $stats->addRating(-10);
        $this->assertCount(5, $this->getPrivateProperty($stats, 'ratings'));
        $this->assertEquals([-10 => 1, 0 => 1, 1 => 2, 20 => 1], $this->getPrivateProperty($stats, 'counts'));
    }

    /**
     * @dataProvider toArrayProvider
     */
    public function testToArray(array $ratings, array $expected): void
    {
        $stats = new RatingStatistic();
        foreach ($ratings as $rating) {
            $stats->addRating($rating);
        }

        $this->assertEquals($expected, $stats->toArray());
    }

    public function toArrayProvider(): array
    {
        return [
            'empty_rating'    => [
                [],
                [
                    'rating_average' => 0,
                    'rating_count'   => 0,
                    'rating_1'       => 0,
                    'rating_2'       => 0,
                    'rating_3'       => 0,
                    'rating_4'       => 0,
                    'rating_5'       => 0,
                ],
            ],
            'one_rating'      => [
                [5],
                [
                    'rating_average' => 5.0,
                    'rating_count'   => 1,
                    'rating_1'       => 0,
                    'rating_2'       => 0,
                    'rating_3'       => 0,
                    'rating_4'       => 0,
                    'rating_5'       => 1,
                ],
            ],
            'multiple_rating' => [
                [5, 1, 1, 6, 2, 4],
                [
                    'rating_average' => 3.2,
                    'rating_count'   => 6,
                    'rating_1'       => 2,
                    'rating_2'       => 1,
                    'rating_3'       => 0,
                    'rating_4'       => 1,
                    'rating_5'       => 1,
                ],
            ],
        ];
    }
}
