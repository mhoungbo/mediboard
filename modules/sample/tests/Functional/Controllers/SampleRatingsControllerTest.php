<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Tests\Functional\Controllers;

use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Sample\Entities\CSampleCategory;
use Ox\Mediboard\Sample\Entities\CSampleMovie;
use Ox\Mediboard\Sample\Entities\CSamplePerson;
use Ox\Mediboard\Sample\Entities\CSampleRating;
use Ox\Mediboard\Sample\Tests\Fixtures\SampleMovieFixtures;
use Ox\Mediboard\Sample\Tests\Fixtures\SamplePersonFixtures;
use Ox\Mediboard\Sample\Tests\Fixtures\SampleRatingFixtures;
use Ox\Mediboard\Sample\Tests\Fixtures\SampleUtilityFixtures;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class SampleRatingsControllerTest extends OxWebTestCase
{
    public function testCreateBadRating(): void
    {
        $movie = $this->createMovie();
        $item  = new Item('sample_rating');
        $item->setRelationships([CSampleRating::RELATION_MOVIE => new Item('sample_movie', $movie->_id)]);
        $item->setAttributes(['rating' => 10]);

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)->request(
                     'POST',
                     '/api/sample/ratings',
            content: json_encode($item)
        );

        $this->assertResponseStatusCodeSame(500);

        $error = $this->getJsonApiError($client);
        $this->assertStringContainsString("<strong title='rating'>Note</strong>", $error->getMessage());
        $this->assertStringContainsString('Doit avoir une valeur maximale de 5 (val:"10"', $error->getMessage());
    }

    public function testCreate(): CSampleRating
    {
        $movie = $this->createMovie();
        $item  = new Item('sample_rating');
        $item->setRelationships([CSampleRating::RELATION_MOVIE => new Item('sample_movie', $movie->_id)]);
        $item->setAttributes(['rating' => 4]);

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)->request(
                     'POST',
                     '/api/sample/ratings',
            content: json_encode($item)
        );

        $this->assertResponseStatusCodeSame(201);

        $collection = $this->getJsonApiCollection($client);

        $this->assertEquals(1, $collection->getMeta('count'));

        $this->assertEquals('sample_rating', $collection->getFirstItem()->getType());

        $rating_id = $collection->getFirstItem()->getId();
        $this->assertNotNull($rating_id);

        return CSampleRating::findOrFail($rating_id);
    }

    /**
     * @depends testCreate
     */
    public function testShow(CSampleRating $rating): CSampleRating
    {
        $client = self::createClient();
        $client->request('GET', '/api/sample/ratings/' . $rating->_id);

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals($rating->_id, $item->getId());
        $this->assertEquals(4, $item->getAttribute('rating'));

        return $rating;
    }

    /**
     * @depends testShow
     */
    public function testUpdate(CSampleRating $rating): CSampleRating
    {
        $item = new Item($rating::RESOURCE_TYPE, $rating->_id);
        $item->setAttributes(['rating' => 5]);
        $item->setRelationships(['movie' => new Item('sample_movie', ($rating->movie_id + 1))]);

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)->request(
                     'PATCH',
                     '/api/sample/ratings/' . $rating->_id . '?relations=movie',
            content: json_encode($item)
        );

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals($rating->_id, $item->getId());
        $this->assertEquals(5, $item->getAttribute('rating'));
        $this->assertEquals($rating->movie_id, $item->getRelationship('movie')->getId());

        return $rating;
    }

    /**
     * @depends testUpdate
     */
    public function testDelete(CSampleRating $rating): void
    {
        self::createClient()->request('DELETE', '/api/sample/ratings/' . $rating->_id);

        $this->assertResponseStatusCodeSame(204);

        $this->assertFalse(CSampleRating::find($rating->_id));
    }


    public function testListForMovie(): void
    {
        $movie = $this->getObjectFromFixturesReference(CSampleMovie::class, SampleMovieFixtures::MOVIE_1);
        $user  = $this->getObjectFromFixturesReference(CMediusers::class, SampleRatingFixtures::RATING_USER_FIXTURES);

        $client = self::createClient();
        $client->request(
            'GET',
            '/api/sample/movies/' . $movie->_id . '/ratings',
            [
                'relations' => 'owner,movie',
                'filter' => 'owner_id.equal.' . $user->_id
            ]
        );
        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(1, $collection->getMeta('count'));

        $rating = $collection->getFirstItem();

        $item_movie = $rating->getRelationship('movie');
        $item_owner = $rating->getRelationship('owner');
        $this->assertEquals($movie->_id, $item_movie->getId());
        $this->assertEquals($user->_id, $item_owner->getId());
    }

    private function createMovie(): CSampleMovie
    {
        $category = $this->getObjectFromFixturesReference(CSampleCategory::class, SampleUtilityFixtures::CATEGORY);
        $director = $this->getObjectFromFixturesReference(CSamplePerson::class, SamplePersonFixtures::DIRECTOR_TAG);

        /** @var CSampleMovie $movie */
        $movie              = CSampleMovie::getSampleObject();
        $movie->category_id = $category->_id;
        $movie->director_id = $director->_id;

        if ($msg = $movie->store()) {
            throw new TestsException($msg);
        }

        return $movie;
    }
}
