{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<style>
  body {
    padding: 32px;
  }

  table {
    border: 1px solid grey;
    border-collapse: separate;
    border-spacing: 10px;
  }

  table tr:first-of-type td {
    border: 1px solid grey;
    text-align: center;
  }

  table tr td:first-of-type {
    font-weight: bold;
  }

  table tr td {
    vertical-align: top;
  }
</style>

<div>

    {{if $is_last_page}}
        {{assign var=table_style value=""}}
    {{else}}
        {{assign var=table_style value="page-break-after: always;"}}
    {{/if}}

  <table style="{{$table_style}}">
    <tr>
      <td colspan="3"><h1>Fiche {{$person}}</h1></td>
    </tr>
    <tr>
      <td rowspan="6"><img src="{{$src_img}}" width="200"/></td>
    </tr>
    <tr>
      <td>{{mb_title object=$person field=first_name}}</td>
      <td>{{mb_value object=$person field=first_name}}</td>
    </tr>
    <tr>
      <td>{{mb_title object=$person field=last_name}}</td>
      <td>{{mb_value object=$person field=last_name}}</td>
    </tr>
    <tr>
      <td>{{mb_title object=$person field=birthdate}}</td>
      <td>{{mb_value object=$person field=birthdate}}</td>
    </tr>
    <tr>
      <td>{{mb_title object=$person field=nationality_id}}</td>
      <td>{{$nationality}}</td>
    </tr>
    <tr>
      <td>{{mb_title object=$person field=activity_start}}</td>
      <td>{{mb_value object=$person field=activity_start}}</td>
    </tr>
  </table>
</div>
