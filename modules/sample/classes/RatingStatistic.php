<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample;

use Ox\Core\CMbArray;

/**
 * Aggregation of ratings. Allow the calculation of average and the count per rating.
 */
class RatingStatistic
{
    private array $ratings = [];
    private array $counts  = [];

    public function addRating(int $rating): void
    {
        $this->ratings[] = $rating;

        if (isset($this->counts[$rating])) {
            $this->counts[$rating]++;
        } else {
            $this->counts[$rating] = 1;
        }
    }

    public function getAverage(): float
    {
        return $this->ratings ? round(CMbArray::average($this->ratings), 1) : 0;
    }

    public function getCount(string $part = null): int
    {
        if (null === $part) {
            return count($this->ratings);
        }

        return $this->counts[$part] ?? 0;
    }

    public function toArray(): array
    {
        return [
            'rating_average' => $this->getAverage(),
            'rating_count'   => $this->getCount(),
            'rating_1'       => $this->getCount(1),
            'rating_2'       => $this->getCount(2),
            'rating_3'       => $this->getCount(3),
            'rating_4'       => $this->getCount(4),
            'rating_5'       => $this->getCount(5),
        ];
    }
}
