<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Import\MovieDb;

use JsonSerializable;
use Ox\Core\CMbString;
use Ox\Mediboard\Sample\Entities\CSampleMovie;
use Ox\Mediboard\Sample\Entities\CSamplePerson;
use Ox\Mediboard\Sante400\CIdSante400;

/**
 * Representation of a person that can be serialized into a JSON:API object ready to be posted.
 */
class MovieDbPerson implements JsonSerializable
{
    private string $id;

    private ?int $gender;

    private ?string $name;

    private ?string $birthday;

    private ?string $director;

    private ?MovieDbImage $profile;

    public function __construct(array $data)
    {
        $this->id       = $data['id'];
        $this->gender   = $data['gender'] ?? null;
        $this->name     = $data['name'] ?? null;
        $this->birthday = $data['birthday'] ?? null;
        $this->director = $data['director'] ?? null;
        $this->profile  = $data['profile'] ?? null;
    }

    public function jsonSerialize(): array
    {
        if (!str_contains($this->name, ' ')) {
            $first_name = $last_name = $this->name;
        } else {
            [$first_name, $last_name] = explode(' ', $this->name, 2);
        }

        return [
            'id'            => null,
            'type'          => CSamplePerson::RESOURCE_TYPE,
            'attributes'    => [
                'last_name'   => CMbString::utf8Encode($last_name),
                'first_name'  => CMbString::utf8Encode($first_name),
                'sex'         => !$this->gender ? null : ($this->gender === 1 ? 'f' : 'm'),
                'birthdate'   => $this->birthday,
                'is_director' => $this->director ? '1' : '0',
            ],
            'relationships' => [
                CSamplePerson::RELATION_FILES      => [
                    'data' => $this->profile,
                ],
            ],
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }
}
