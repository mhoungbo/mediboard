<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Repositories;

use Exception;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Mediboard\Sample\Entities\CSampleMovie;
use Ox\Mediboard\Sample\Entities\CSampleRating;
use Ox\Mediboard\Sample\RatingStatistic;

/**
 * Repository used to query CSampleRating.
 */
class SampleRatingsRepository extends AbstractRequestApiRepository
{
    /**
     * Find the ratings for a movie.
     *
     * @param CSampleMovie $movie
     *
     * @return array
     * @throws Exception
     */
    public function findForMovie(CSampleMovie $movie): array
    {
        return $movie->loadBackRefs(
                   'ratings',
                   $this->order,
                   $this->limit,
            where: $this->where
        );
    }

    /**
     * @param CSampleMovie $movie
     *
     * @return int
     * @throws Exception
     */
    public function countForMovie(CSampleMovie $movie): int
    {
        return $movie->countBackRefs('ratings', $this->where) ?? 0;
    }

    /**
     * @throws Exception
     */
    public function findStatsForMovie(CSampleMovie $movie): RatingStatistic
    {
        $stats = new RatingStatistic();
        /** @var CSampleRating $rating */
        foreach ($movie->loadBackRefs('ratings') as $rating) {
            $stats->addRating($rating->rating);
        }

        return $stats;
    }

    /**
     * @inheritDoc
     */
    protected function getObjectInstance(): CStoredObject
    {
        return new CSampleRating();
    }

    /**
     * @inheritDoc
     */
    protected function massLoadRelation(array $objects, string $relation): void
    {
        switch ($relation) {
            case RequestRelations::QUERY_KEYWORD_ALL:
                $this->massLoadOwners($objects);
                $this->massLoadMovies($objects);
                break;
            case CSampleRating::RELATION_MOVIE:
                $this->massLoadMovies($objects);
                break;
            case CSampleRating::RELATION_OWNER:
                $this->massLoadOwners($objects);
                break;
            default:
                // Do nothing
        }
    }

    private function massLoadMovies(array $ratings): void
    {
        CStoredObject::massLoadFwdRef($ratings, 'movie_id');
    }

    public function massLoadOwners(array $ratings): void
    {
        CStoredObject::massLoadFwdRef($ratings, 'owner_id');
    }
}
