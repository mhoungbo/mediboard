<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample;

use Ox\Core\Module\AbstractTabsRegister;

/**
 * @codeCoverageIgnore
 */
class CTabsSample extends AbstractTabsRegister
{
    public function registerAll(): void
    {
        $this->registerRoute('sample_readme', TAB_READ);
        $this->registerRoute('sample_movies_display', TAB_READ);
        $this->registerRoute('sample_bookmarks_display', TAB_READ);
        $this->registerRoute('sample_legacy_compat', TAB_READ);

        $this->registerRoute('sample_persons_display', TAB_ADMIN, self::TAB_SETTINGS);
        $this->registerRoute('sample_configure', TAB_ADMIN, self::TAB_CONFIGURE);
    }
}
