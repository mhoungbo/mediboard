<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Entities;

use Ox\Core\Api\Request\Content\JsonApiItem;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\Routing\RouterInterface;

/**
 * Rating contains a rate for a movie and a user. The rate can be between 0 and 5.
 */
class CSampleRating extends CMbObject
{
    public const RESOURCE_TYPE = 'sample_rating';

    public const RELATION_MOVIE = 'movie';
    public const RELATION_OWNER = 'owner';

    /** @var int */
    public $sample_rating_id;
    /** @var int */
    public $owner_id;
    /** @var int */
    public $movie_id;
    /** @var float */
    public $rating;
    /** @var string */
    public $datetime;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table     = 'sample_rating';
        $spec->key       = 'sample_rating_id';
        $spec->anti_csrf = true;

        $spec->uniques['user_movie'] = ['owner_id', 'movie_id'];

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['owner_id'] = 'ref class|CUser notNull index|datetime back|movies_rated';
        $props['movie_id'] = 'ref class|CSampleMovie notNull index|datetime back|ratings';
        $props['rating']   = 'float min|1 max|5 decimals|1 notNull fieldset|' . self::FIELDSET_DEFAULT;
        $props['datetime'] = 'dateTime notNull index|0 fieldset|' . self::FIELDSET_DEFAULT;

        return $props;
    }

    /**
     * @inheritDoc
     */
    public function store(): ?string
    {
        if (!$this->_id && !$this->owner_id) {
            $this->owner_id = CUser::get()->_id;
        }

        $this->datetime = CMbDT::dateTime();

        return parent::store();
    }

    /**
     * @inheritDoc
     *
     * Anyone with read permission on the module can see ratings.
     * The owner of a rating can edit it, or anyone with edit on the module.
     */
    public function getPerm($permType): bool
    {
        if ($permType < CPermObject::EDIT) {
            return parent::getPerm($permType);
        }

        if ($this->owner_id === CUser::get()->_id) {
            return true;
        }

        return parent::getPerm($permType);
    }

    public function getApiLink(RouterInterface $router): ?string
    {
        return $router->generate('sample_ratings_show', ['sample_rating_id' => $this->_id]);
    }

    public function getResourceMovie(): Item
    {
        return new Item($this->loadFwdRef('movie_id', true));
    }

    public function setResourceMovie(?JsonApiItem $item): void
    {
        $this->movie_id = $item === null
            ? ''
            : $item->createModelObject(CSampleMovie::class, false)->getModelObject()->_id;
    }

    public function getResourceOwner(): Item
    {
        /** @var CUser $owner */
        $owner = $this->loadFwdRef('owner_id', true);

        return new Item($owner->loadRefMediuser());
    }
}
