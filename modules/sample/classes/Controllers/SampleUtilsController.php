<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Controllers;

use Ox\Core\Controller;
use Ox\Core\CMbString;
use Ox\Core\EntryPoint;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class SampleUtilsController extends Controller
{
    private const README = 'readme.md';

    public function readme(): Response
    {
        $readme = CMbString::markdown(
            file_get_contents(dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . self::README)
        );

        return $this->renderSmarty('readme', ['readme' => $readme,]);
    }

    public function legacyCompat(RouterInterface $router): Response
    {
        $current_user = CUser::get();
        $entry_point  = new EntryPoint('legacy_compat', $router);

        $entry_point->setScriptName('sampleLegacyCompat');
        $entry_point->addData('user', [
            'name' => ucfirst($current_user->user_last_name) . ' ' . ucfirst($current_user->user_first_name),
            'guid' => $current_user->_guid,
        ]);

        return $this->renderSmarty('legacy_compat', [
            'legacy_compat' => $entry_point,
            'user'          => $current_user,
        ]);
    }
}
