<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\Filter;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFilter;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\EntryPoint;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Sample\Entities\CSampleBookmark;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * CRUD controller for CSamplebookmarks.
 */
class SampleBookmarksController extends Controller
{
    /**
     * @api
     *
     * List the bookmarks for the current user.
     * A filter for user_id = 'current_user_id' is added.
     */
    public function list(RequestApi $request): Response
    {
        $bookmark = new CSampleBookmark();

        $request->getRequestFilter()
            ->addFilter(new Filter('user_id', RequestFilter::FILTER_EQUAL, CMediusers::get()->_id));

        $collection = Collection::createFromRequest($request, $bookmark->loadListFromRequestApi($request));
        $collection->createLinksPagination(
            $request->getOffset(),
            $request->getLimit(),
            $bookmark->countListFromRequestApi($request)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * @api
     *
     * Display a single bookmark.
     */
    public function show(CSampleBookmark $bookmark, RequestApi $request): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request, $bookmark));
    }

    /**
     * @api
     *
     * Add a collection of bookmarks.
     * The field user_id is not in a fieldset which avoid the user setting it.
     */
    public function add(RequestApi $request): Response
    {
        $bookmarks = $request->getModelObjectCollection(CSampleBookmark::class);

        $collection = $this->storeCollection($bookmarks);
        $collection->setModelRelations(CSampleBookmark::RELATION_MOVIE);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }

    /**
     * @api
     */
    public function delete(CSampleBookmark $bookmark): Response
    {
        $this->deleteObject($bookmark);

        return $this->renderResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Legacy route to display bookmarks
     *
     * @throws ApiException|CMbException|Exception
     */
    public function displayBookmarks(RouterInterface $router): Response
    {
        $entry = new EntryPoint('SampleBookmarks', $router);
        $entry->setScriptName('sampleBookmarks')
            ->addLink('movies', 'sample_movies_list');

        return $this->renderSmarty('display_entry_point', ['entry_point' => $entry], 'system');
    }
}
