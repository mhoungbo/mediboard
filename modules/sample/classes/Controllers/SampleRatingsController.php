<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\Content\RequestContentException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Mediboard\Sample\Entities\CSampleMovie;
use Ox\Mediboard\Sample\Entities\CSampleRating;
use Ox\Mediboard\Sample\Repositories\SampleRatingsRepository;
use Symfony\Component\HttpFoundation\Response;

/**
 * Handle the CRUD for CSampleRating.
 */
class SampleRatingsController extends Controller
{
    /**
     * List the ratings for a movie.
     *
     * @param CSampleMovie            $movie
     * @param SampleRatingsRepository $repository
     * @param RequestApi              $request
     *
     * @return Response
     * @throws ApiException
     * @throws Exception
     * @api
     *
     */
    public function listForMovie(
        CSampleMovie            $movie,
        SampleRatingsRepository $repository,
        RequestApi              $request
    ): Response {
        $collection = Collection::createFromRequest($request, $repository->findForMovie($movie));
        $collection->createLinksPagination(
            $request->getOffset(),
            $request->getLimit(),
            $repository->countForMovie($movie)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * @param CSampleRating $rating
     * @param RequestApi    $request
     *
     * @return Response
     * @throws ApiException
     * @api
     *
     */
    public function show(CSampleRating $rating, RequestApi $request): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request, $rating));
    }

    /**
     * @param RequestApi $request
     *
     * @return Response
     * @api
     *
     */
    public function create(RequestApi $request): Response
    {
        $ratings = $request->getModelObjectCollection(CSampleRating::class);

        $collection = $this->storeCollection($ratings);
        $collection->setModelRelations(CSampleRating::RELATION_MOVIE);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }

    /**
     * The hydratation of the CSampleRating is done using "relations: []" to not hydrate the relations.
     *
     * @param CSampleRating $rating
     * @param RequestApi    $request
     *
     * @return Response
     * @throws ApiException
     * @throws RequestContentException
     * @throws CMbException
     * @api
     *
     */
    public function update(CSampleRating $rating, RequestApi $request): Response
    {
        /** @var CSampleRating $rating */
        $rating = $request->getModelObject($rating, fields: ['rating'], relations: []);
        $item   = $this->storeObject($rating);
        $item->setModelFieldsets($request->getFieldsets());
        $item->setModelRelations($request->getRelations());

        return $this->renderApiResponse($item);
    }

    /**
     * @param CSampleRating $rating
     *
     * @return Response
     * @api
     *
     */
    public function delete(CSampleRating $rating): Response
    {
        $this->deleteObject($rating);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }
}
