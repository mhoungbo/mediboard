<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Controllers;

use Exception;
use finfo;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\CSmartyDP;
use Ox\Core\CStoredObject;
use Ox\Core\EntryPoint;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\CompteRendu\CWkHtmlToPDFConverter;
use Ox\Mediboard\Sample\Entities\CSamplePerson;
use Ox\Mediboard\Sample\Repositories\SamplePersonsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * Controller that answer the api calls on persons.
 */
class SamplePersonsController extends Controller
{
    /**
     * @throws Exception
     * @see SamplePersonsRepository::initFromRequest
     *
     * @api
     *
     * Load a list of persons using the request parameters.
     * Before being autowire the SamplePersonsRepository is initialized using the RequestApi.
     * A collection of persons is created using the arguments of the RequestApi for fieldsets, relationships, ...
     *
     */
    public function listPersons(RequestApi $request_api, SamplePersonsRepository $persons_repository): Response
    {
        $persons = $persons_repository->find();

        // Massload relations to avoid loading them unitary later.
        $persons_repository->massLoadRelations($persons, $request_api->getRelations());

        $collection = Collection::createFromRequest($request_api, $persons);

        // Add the profile picture to the links
        /** @var Item $item */
        foreach ($collection as $item) {
            /** @var CSamplePerson $person */
            $person = $item->getDatas();
            $item->addLinks($person->buildProfilePictureLink());
        }
        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            $persons_repository->count()
        );

        return $this->renderApiResponse($collection);
    }

    public function export(): Response
    {
        $person  = new CSamplePerson();
        $persons = $person->loadList();
        CStoredObject::massLoadFwdRef($persons, "nationality_id");

        $date      = CMbDT::date();
        $file_name = "export-persons-{$date}.csv";

        $writer  = new CCSVFile();
        $headers = [
            'id',
            'last_name',
            'first_name',
            'birthdate',
            'nationality',
            'is_director',
        ];

        $writer->setColumnNames($headers);
        $writer->writeLine($headers);

        foreach ($persons as $person) {
            $line = [
                $person->_id,
                $person->last_name,
                $person->first_name,
                $person->birthdate,
                $person->loadFwdRef('nationality_id', true)->name,
                $person->is_director,
            ];
            $writer->writeLine($line);
        }

        $content = $writer->getContent();

        return $this->renderFileResponse($content, $file_name, 'application/csv');
    }

    /**
     * @throws ApiException
     * @see CStoredObjectAttributeValueResolver
     *
     * @api
     *
     * Use the Ox\Core\Kernel\Resolver\CStoredObjectAttributeValueResolver to inject the CSamplePerson from the
     * parameter sample_person_id.
     *
     */
    public function getPerson(CSamplePerson $person, RequestApi $request_api): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request_api, $person));
    }

    /**
     * @param RequestParams $params
     *
     * @return Response
     * @throws CMbException
     * @exemple gui/sample/persons/print?dialog=1&ids[]=1&ids[]=606&ids[]=607
     */
    public function print(RequestParams $params): Response
    {
        $ids = $params->get('ids', 'str notNull');

        foreach ($ids as $key => $id) {
            if (!is_numeric($id)) {
                unset($ids[$key]);
            }
        }
        if (empty($ids)) {
            throw new Exception("Missing ids to print");
        }

        // Load
        $person  = new CSamplePerson();
        $persons = $person->loadAll($ids);

        // Mass loading
        CStoredObject::massLoadFwdRef($persons, "nationality_id");
        CStoredObject::massLoadBackRefs($persons, 'files', null, [
            'file_name' => $person->getDS()->prepare('= ?', CSamplePerson::PROFILE_NAME),
        ]);

        $content_html = null;
        $count_person = count($persons);
        $i            = 0;
        foreach ($persons as $person) {
            $i++;
            $file = $person->getProfilePicture();


            $finfo = new finfo(FILEINFO_MIME_TYPE, '');
            $mime  = $finfo->file($file->_file_path);

            $content = $file->getBinaryContent();

            $src_img = "data:$mime;base64," . base64_encode($content);


            $smarty = new CSmartyDP('modules/sample');
            $smarty->assign([
                                'person'       => $person,
                                'src_img'      => $src_img,
                                'nationality'  => $person->loadFwdRef('nationality_id', true)->name,
                                'is_last_page' => $i === $count_person,
                            ]);
            $content_html .= $smarty->fetch('person.tpl');
        }

        CWkHtmlToPDFConverter::init('CWkHtmlToPDFConverter');
        $content_pdf = CWkHtmlToPDFConverter::convert($content_html, 'a4', 'portrait');

        return $this->renderFileResponse($content_pdf, $person->_guid . '.pdf', 'application/pdf');
    }

    /**
     * @throws ApiException|CMbException
     * @api
     *
     * Use the RequestApi to create a collection of persons from the body content.
     * Store the collection of persons en return it.
     *
     */
    public function createPerson(RequestApi $request_api): Response
    {
        $persons = $request_api->getModelObjectCollection(
            CSamplePerson::class,
            [CSamplePerson::FIELDSET_DEFAULT, CSamplePerson::FIELDSET_EXTRA],
            // Temporary for retro compatibility purpose.
            ['nationality_id']
        );

        $collection = $this->storeCollection($persons);
        $collection->setModelFieldsets($request_api->getFieldsets());
        $collection->setModelRelations($request_api->getRelations());

        return $this->renderApiResponse($collection, 201);
    }

    /**
     * @throws ApiException|CMbException
     * @api
     *
     * Update an existing person ($person) using the body of the request.
     * The user must have the edit permission on the person
     *
     */
    public function updatePerson(CSamplePerson $person, RequestApi $request_api): Response
    {
        /** @var CSamplePerson $person */
        $person = $request_api->getModelObject(
            $person,
            [CSamplePerson::FIELDSET_DEFAULT, CSamplePerson::FIELDSET_EXTRA],
            // Temporary for retro compatibility purpose.
            ['nationality_id']
        );

        $item = $this->storeObject($person);
        $item->setModelFieldsets($request_api->getFieldsets());
        $item->setModelRelations($request_api->getRelations());

        return $this->renderApiResponse($item, Response::HTTP_OK);
    }

    /**
     * @throws CMbException
     * @api
     *
     * Delete a person ($person). The user must have the edit permission on the person.
     *
     */
    public function deletePerson(CSamplePerson $person): Response
    {
        $this->deleteObject($person);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Display a list of persons as json api.
     *
     * @throws CMbException|Exception
     */
    public function displayPersons(RouterInterface $router): Response
    {
        $entry = new EntryPoint('SampleMovieSettings', $router);
        $entry->setScriptName('sampleMovieSettings')
            ->addLink('nationalities', 'sample_nationalities_list')
            ->addLink('personsList', 'sample_persons_list')
            ->addLink('personsExport', 'sample_persons_export', ['dialog' => 1])
            ->addLink('personsPrint', 'sample_persons_print', ['dialog' => 1]);

        if ($this->request_context->getCan()->edit) {
            $entry->addLink('personsCreate', 'sample_persons_create');
        }

        return $this->renderSmarty('display_entry_point', ['entry_point' => $entry], 'system');
    }
}
