<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFieldsets;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\Api\Resources\AbstractResource;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbException;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CModelObject;
use Ox\Core\Controller;
use Ox\Core\EntryPoint;
use Ox\Core\Kernel\Exception\HttpException;
use Ox\Mediboard\Sample\Entities\CSampleBookmark;
use Ox\Mediboard\Sample\Entities\CSampleCasting;
use Ox\Mediboard\Sample\Entities\CSampleMovie;
use Ox\Mediboard\Sample\Entities\CSampleNationality;
use Ox\Mediboard\Sample\Entities\CSamplePerson;
use Ox\Mediboard\Sample\Import\MovieDb\SampleMovieImport;
use Ox\Mediboard\Sample\Repositories\SampleMoviesRepository;
use Ox\Mediboard\Sample\Repositories\SampleRatingsRepository;
use Ox\Mediboard\System\CSourceHTTP;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * Controller that answer the api calls on movies.
 */
class SampleMoviesController extends Controller
{
    public const SEARCH_NATIONALITY        = 'nationality_id';
    public const MOVIE_CASTING_LINK        = 'casting';
    public const MOVIE_RATINGS_LINK        = 'ratings';
    public const MOVIE_TRAILER_LINK        = 'trailer';
    public const MOVIE_SEARCH_TRAILER_LINK = 'trailer_search';

    /**
     * @throws Exception
     *
     * @api
     *
     * Load a list of movies using the request parameters.
     * Before being autowire the SampleMoviesRepository is initialized using the RequestApi.
     * A collection of movies is created using the arguments of the RequestApi for fieldsets, relationships, ...
     *
     * @see SampleMoviesRepository::initFromRequest
     *
     */
    public function listMovies(
        RequestApi              $request_api,
        SampleMoviesRepository  $movies_repository,
        RouterInterface         $router,
        SampleRatingsRepository $ratings_repository
    ): Response {
        if ($bookmark_count = $request_api->getRequest()->query->get('top_bookmarked')) {
            return $this->listMostBookmarkedMovies($request_api, $movies_repository, $bookmark_count);
        }


        $ratings_stats = $request_api->getRequest()->query->getBoolean('ratingsStats');
        $bookmarked    = false;
        if ($nationality_id = $request_api->getRequest()->get(self::SEARCH_NATIONALITY)) {
            $nationality = CSampleNationality::findOrFail($nationality_id);

            $total  = $movies_repository->countMoviesByDirectorNationality($nationality);
            $movies = $movies_repository->findMoviesByDirectorNationality($nationality);
        } elseif ($request_api->getRequest()->get('bookmarked')) {
            $mediuser   = $this->getCMediuser();
            $bookmarked = true;
            $total      = $movies_repository->findBookmarked($mediuser, true);
            $movies     = $movies_repository->findBookmarked($mediuser);
        } else {
            $total  = $movies_repository->count();
            $movies = $movies_repository->find();
        }

        $relations = $request_api->getRelations();
        if ($ratings_stats) {
            $relations[] = CSampleMovie::RELATION_MY_RATING;
        }

        // Massload the relations that will be loaded from cache in the links later.
        $movies_repository->massLoadRelations($movies, $relations);

        /** @var Collection $collection */
        $collection = Collection::createFromRequest($request_api, $movies);

        // Build cover link + view link
        /** @var Item $item */
        foreach ($collection as $item) {
            /** @var CSampleMovie $movie */
            $movie = $item->getDatas();
            $item->addLinks(
                array_merge(
                    ['details' => $this->generateUrl('sample_movies_details', ['sample_movie_id' => $movie->_id])],
                    $movie->buildCoverLink()
                )
            );

            if ($bookmarked) {
                /** @var CSampleBookmark $bookmark */
                $bookmark = $movie->loadUniqueBackRef('bookmarked_by');
                $item->addAdditionalDatas(['bookmarked_date' => $bookmark->datetime]);
                $item->addLinks(['bookmark' => $bookmark->getApiLink($router)]);
            }

            if ($ratings_stats) {
                $item->addMetas($ratings_repository->findStatsForMovie($movie)->toArray());
                $item->addLinks(
                    [
                        self::MOVIE_RATINGS_LINK => $this->generateUrl(
                            'sample_ratings_list_for_movie',
                            ['sample_movie_id' => $movie->_id]
                        ),
                    ]
                );
            }
        }

        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            $total
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws ApiException
     * @throws Exception
     * @see CStoredObjectAttributeValueResolver
     * @api
     *
     * Use the Ox\Core\Kernel\Resolver\CStoredObjectAttributeValueResolver to inject the CSampleMovie from the
     * parameter sample_movie_id.
     */
    public function getMovie(
        CSampleMovie            $movie,
        RequestApi              $request_api,
        SampleRatingsRepository $repository,
        CSourceHTTP             $sample_import_source
    ): Response {
        $item = Item::createFromRequest($request_api, $movie);
        $item->addLinks(
            array_merge(
                $movie->buildCoverLink(),
                [
                    'details'                => $this->generateUrl(
                        'sample_movies_details',
                        ['sample_movie_id' => $movie->_id]
                    ),
                    // Add the link to the casting of the movie.
                    // This link can be used to list, add or delete actors from a movie.
                    self::MOVIE_CASTING_LINK => $this->generateUrl(
                        'sample_casting_list',
                        ['sample_movie_id' => $movie->_id]
                    ),
                ],
            )
        );

        if ($request_api->getRequest()->query->get('withTrailer')) {
            $item->addLinks([self::MOVIE_TRAILER_LINK => $movie->trailer_url]);

            if (!$movie->trailer_url && $sample_import_source->_id && $movie->getImportTag()) {
                $item->addLinks(
                    [
                        self::MOVIE_SEARCH_TRAILER_LINK => $this->generateUrl(
                            'sample_movies_search_trailer',
                            ['sample_movie_id' => $movie->_id]
                        ),
                    ]
                );
            }
        }

        if ($request_api->getRequest()->query->get('ratingsStats')) {
            $item->addMetas($repository->findStatsForMovie($movie)->toArray());
            $item->addLinks(
                [
                    self::MOVIE_RATINGS_LINK => $this->generateUrl(
                        'sample_ratings_list_for_movie',
                        ['sample_movie_id' => $movie->_id]
                    ),
                ]
            );
        }

        return $this->renderApiResponse($item);
    }

    /**
     * Get the link to the trailer of a movie by requesting TheMovieDb's API if necessary.
     *
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function getMovieTrailer(CSampleMovie $movie, SampleMovieImport $import): Response
    {
        $item = new Item($movie);
        $item->setModelFieldsets(RequestFieldsets::QUERY_KEYWORD_NONE);

        if ($movie->trailer_url) {
            $item->addLinks([self::MOVIE_TRAILER_LINK => $movie->trailer_url]);
        } elseif ($idx = $movie->getImportTag()) {
            $url = $import->getTrailerUrl($idx->id400);

            if (null !== $url) {
                $movie->trailer_url = $url;
                $this->storeObject($movie, false);

                $item->addLinks([self::MOVIE_TRAILER_LINK => $url]);
            }
        }

        return $this->renderApiResponse($item);
    }

    /**
     * @throws ApiException|CMbException
     * @throws Exception
     * @api
     *
     * Use the RequestApi to create a collection of movies from the body content.
     * Store the collection of movies en return it.
     *
     */
    public function createMovie(RequestApi $request_api): Response
    {
        $movies = $request_api->getModelObjectCollection(
            CSampleMovie::class,
            [
                CModelObject::FIELDSET_DEFAULT,
                CSampleMovie::FIELDSET_DETAILS,
            ],
            ['trailer_url']
        );

        $collection = $this->storeCollection($movies);
        $collection->setModelFieldsets($request_api->getFieldsets());
        $collection->setModelRelations($request_api->getRelations());

        $sample_movies = [];
        foreach ($movies as $movie) {
            $sample_movies[$movie->_id] = $movie;
        }

        $repository = new SampleMoviesRepository();
        $repository->massLoadRelations(
            $sample_movies,
            array_merge([CSampleMovie::RELATION_FILES], $request_api->getRelations())
        );

        // Add the link cover and casting to movies created
        foreach ($collection as $item) {
            /** @var CSampleMovie $movie */
            $movie = $item->getDatas();
            $item->addLinks($movie->buildCoverLink());
            $item->addLinks(
                [
                    self::MOVIE_CASTING_LINK => $this->generateUrl(
                        'sample_casting_list',
                        ['sample_movie_id' => $movie->_id]
                    ),
                ]
            );
        }

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }

    /**
     * Import multiple movies from TheMovieDb API.
     *
     * @throws InvalidArgumentException
     */
    public function importMovies(SampleMovieImport $import): Response
    {
        try {
            $count = $import->importMovies();
        } catch (CMbException $e) {
            $this->addUiMsgError($e->getMessage());

            return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->addUiMsgOk('SampleMovieImport-Msg-Count-movies-imported', false, $count);

        return $this->renderEmptyResponse();
    }

    /**
     * @throws ApiException|CMbException
     * @throws Exception
     * @api
     *
     * Update an existing movie ($movie) using the body of the request.
     * The user must have the edit permission on the movie
     *
     */
    public function updateMovie(CSampleMovie $movie, RequestApi $request_api): Response
    {
        /** @var CSampleMovie $movie_from_request */
        $movie_from_request = $request_api->getModelObject(
            $movie,
            [
                CModelObject::FIELDSET_DEFAULT,
                CSampleMovie::FIELDSET_DETAILS,
            ],
            ['trailer_url']
        );

        $item = $this->storeObject($movie_from_request);
        $item->addLinks($movie->buildCoverLink());
        $item->setModelFieldsets($request_api->getFieldsets());
        $item->setModelRelations($request_api->getRelations());

        return $this->renderApiResponse($item, Response::HTTP_OK);
    }

    /**
     * @throws CMbException
     * @api
     *
     * Delete a movie ($movie). The user must have the edit permission on the movie.
     */
    public function deleteMovie(CSampleMovie $movie): Response
    {
        $this->deleteObject($movie);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @throws Exception
     * @api
     *
     */
    public function listCasting(
        CSampleMovie           $movie,
        SampleMoviesRepository $repository,
        RequestApi             $request_api
    ): Response {
        $casting = $repository->findCasting($movie);

        if (
            in_array(RequestRelations::QUERY_KEYWORD_ALL, $request_api->getRelations())
            || in_array(CSampleCasting::RELATION_ACTOR, $request_api->getRelations())
        ) {
            $repository->massLoadActorsFromCasting($casting);
        }

        $collection = Collection::createFromRequest($request_api, $casting);

        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            $repository->countCasting($movie)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws ApiException|CMbException
     * @throws Exception
     * @api
     *
     */
    public function setCasting(CSampleMovie $movie, RequestApi $request_api): Response
    {
        $casting = $request_api->getModelObjectCollection(CSampleCasting::class);

        /** @var CSampleCasting $cast */
        foreach ($casting as $cast) {
            $cast->movie_id = $movie->_id;
        }

        // Delete old casting before storing new ones.
        // For bigger objects you should try to match the posted ones with the existing ones instead of deleting them.
        if ($old_casting_ids = $movie->loadBackIds('casting')) {
            $cast = new CSampleCasting();
            $cast->deleteAll($old_casting_ids);
        }

        $collection = $this->storeCollection($casting);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }

    /**
     * @throws CMbException|HttpException
     * @api
     *
     */
    public function deleteCasting(CSampleMovie $movie, CSamplePerson $actor): Response
    {
        $casting           = new CSampleCasting();
        $casting->movie_id = $movie->_id;
        $casting->actor_id = $actor->_id;
        $casting->loadMatchingObjectEsc();

        if (!$casting->_id) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                $this->translator->tr('CSampleCasting-error-The-actor-does-not-play-in-the-movie', $actor, $movie)
            );
        }

        $this->deleteObject($casting);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Find the top $count bookmarked movies and return them in order.
     *
     * @throws ApiException
     * @throws Exception
     */
    private function listMostBookmarkedMovies(
        RequestApi             $request,
        SampleMoviesRepository $repository,
        int                    $count
    ): Response {
        $movies_bookmarked = $repository->findMostBookmarked($count);

        $movies = (new CSampleMovie())->loadAll(array_keys($movies_bookmarked));

        usort($movies, function (CSampleMovie $elem1, CSampleMovie $elem2) use ($movies_bookmarked) {
            return $movies_bookmarked[$elem2->_id] <=> $movies_bookmarked[$elem1->_id];
        });

        $collection = Collection::createFromRequest($request, $movies);

        /** @var Item $item */
        foreach ($collection as $item) {
            $movie = $item->getDatas();
            $item->addMeta('bookmarked_count', $movies_bookmarked[$movie->_id]);
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * Legacy route to display movies
     *
     * @throws CMbException|Exception
     */
    public function displayMovies(RouterInterface $router): Response
    {
        $entry = new EntryPoint('SampleMovieList', $router);
        $entry->setScriptName('sampleMovieList')
              ->addLink('categories', 'sample_categories_list')
              ->addLink('nationalities', 'sample_nationalities_list')
              ->addLink('movies', 'sample_movies_list')
              ->addLink('persons', 'sample_persons_list');

        if ($this->request_context->getCan()->edit) {
            $entry->addLink('add-movie', 'sample_movies_create');
            $entry->addLink('bookmarks', 'sample_bookmarks_add');
        }

        return $this->renderSmarty('display_entry_point', ['entry_point' => $entry], 'system');
    }

    /**
     * Legacy route to display a movie with it's casting.
     *
     * @throws CMbModelNotFoundException|CMbException|Exception
     */
    public function displayMovieDetails(CSampleMovie $movie, RouterInterface $router): Response
    {
        $entry = new EntryPoint('SampleMovieDetails', $router);
        $entry->setScriptName('sampleMovieDetails')
              ->addLink(
                  'movie',
                  'sample_movies_show',
                  [
                      'sample_movie_id'                       => $movie->_id,
                      RequestFieldsets::QUERY_KEYWORD         => RequestFieldsets::QUERY_KEYWORD_ALL,
                      RequestRelations::QUERY_KEYWORD_INCLUDE => CSampleMovie::RELATION_CATEGORY
                          . RequestRelations::RELATION_SEPARATOR . CSampleMovie::RELATION_DIRECTOR,
                      AbstractResource::PERMISSIONS_KEYWORD   => true,
                  ]
              )
              ->addLink(
                  'casting',
                  'sample_casting_list',
                  [
                      'sample_movie_id'                       => $movie->_id,
                      RequestRelations::QUERY_KEYWORD_INCLUDE => CSampleCasting::RELATION_ACTOR,
                  ]
              )
              ->addLink('categories', 'sample_categories_list')
              ->addLink('nationalities', 'sample_nationalities_list')
              ->addLink('persons', 'sample_persons_list')
              ->addLink('bookmarks', 'sample_bookmarks_add')
              ->addLink('ratings', 'sample_ratings_create')
              ->addLink('back', 'sample_movies_display');

        return $this->renderSmarty('display_entry_point', ['entry_point' => $entry], 'system');
    }
}
