<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Controllers;

use Ox\Core\CApp;
use Ox\Core\Controller;
use Ox\Mediboard\System\CSourceHTTP;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that display configuration options
 */
class SampleConfigureController extends Controller
{
    public function configure(CSourceHTTP $sample_import_source): Response
    {
        return $this->renderSmarty(
            'configure',
            [
                'source_available'         => (bool) $sample_import_source->_id,
                'source'                   => $sample_import_source,
                'base_url'                 => rtrim(CApp::getBaseUrl(), '/'),
            ]
        );
    }
}
