<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sample\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Exceptions\ApiRequestException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Mediboard\Sample\Entities\CSampleCategory;
use Ox\Mediboard\Sample\Import\SampleCategoryImport;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that answer the api request on CSampleCategory
 */
class SampleCategoriesController extends Controller
{
    /**
     * @api
     *
     * @throws ApiRequestException|ApiException
     */
    public function listCategories(RequestApi $request_api): Response
    {
        $category   = new CSampleCategory();
        $categories = $category->loadListFromRequestApi($request_api);

        /** @var Collection $collection */
        $collection = Collection::createFromRequest($request_api, $categories);
        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            $category->countListFromRequestApi($request_api)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * Route for the admin that allow the import of categories.
     *
     * @throws Exception
     */
    public function importCategories(Request $req): Response
    {
        if (!$this->isCsrfTokenValid("sample_categories_import", $req->request->get('token'))) {
            throw new InvalidCsrfTokenException();
        }

        $import = new SampleCategoryImport();
        $count  = $import->import();

        $this->addUiMsgOk('CSampleCategory-msg-Imported', false, $count);

        foreach ($import->getErrors() as $msg) {
            $this->addUiMsgWarning($msg);
        }

        return $this->renderEmptyResponse();
    }
}
