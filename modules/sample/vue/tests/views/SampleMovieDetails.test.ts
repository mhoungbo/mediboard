/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import SampleMovieDetails from "@modules/sample/vue/views/SampleMovieDetails/SampleMovieDetails.vue"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import OxTranslator from "@/core/plugins/OxTranslator"
import oxApiService from "@/core/utils/OxApiService"
import { castingEmptyMock, castingMock, movieEmptyMock, movieMock } from "@modules/sample/vue/tests/mocks/SampleMocks"
import { isEmpty } from "lodash"
import SampleRating from "@modules/sample/vue/models/SampleRating"
import * as OxLoadingManager from "@/core/utils/OxLoadingManager"
import * as OxNotifyManager from "@/core/utils/OxNotifyManager"
import * as OxSchemaManager from "@/core/utils/OxSchemaManager"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * SampleMovieDetails tests
 */
export default class SampleMovieDetailsTest extends OxTest {
    protected component = SampleMovieDetails

    protected beforeTest () {
        super.beforeTest()

        jest.spyOn(OxSchemaManager, "prepareForm").mockImplementation()

        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: movieMock })
            .mockResolvedValueOnce({ data: castingMock })

        jest.spyOn(oxApiService, "delete").mockImplementation(() => Promise.resolve({ data: "" }))
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    protected async mountComponentAsync (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        if (isEmpty(stubs)) {
            stubs = {
                SampleMovieForm: true,
                SampleDirectorLine: true,
                SampleActorLine: true,
                SampleBookmark: true
            }
        }
        const wrapper = shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )

        await this.flushPromises()

        return wrapper
    }

    protected async vueComponentAsync (
        props: object = {
            links: {
                movie: "movie",
                casting: "casting",
                back: "back",
                categories: "categories",
                nationalities: "nationalities",
                persons: "persons",
                bookmarks: "bookmarks",
                ratings: "ratings"
            }
        },
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = await this.mountComponentAsync(props, stubs, slots)

        return wrapper.vm
    }

    public async testSampleMovieDetailsCreation () {
        await this.vueComponentAsync()

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "movie?relations=actors%2Cdirector%2Ccategory%2Cbookmarks%2CmyRating&fieldsets=default%2Cdetails&ratingsStats=true&withTrailer=true&schema=true"
            )
        )

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "casting"
            )
        )
    }

    public async testMovieHasCasting () {
        const sampleMovieDetails = await this.vueComponentAsync()
        expect(this.privateCall(sampleMovieDetails, "casting")).toMatchObject(castingMock.data)
    }

    public async testMovieHasNotCasting () {
        jest.resetAllMocks()
        jest.spyOn(OxSchemaManager, "prepareForm").mockImplementation()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: movieEmptyMock })
            .mockResolvedValueOnce({ data: castingEmptyMock })

        const sampleMovieDetails = await this.vueComponentAsync()
        this.assertEqual(this.privateCall(sampleMovieDetails, "casting"), [])
    }

    public async testMovieHasDirector () {
        const sampleMovieDetails = await this.vueComponentAsync()

        expect(this.privateCall(sampleMovieDetails, "director")).toMatchObject(
            {
                type: "sample_person",
                id: "1",
                attributes: {
                    last_name: "Doe",
                    first_name: "John",
                    is_director: true
                },
                links: {
                    profile_picture: "profilepic"
                }
            }
        )
    }

    public async testMovieHasNotDirector () {
        jest.resetAllMocks()
        jest.spyOn(OxSchemaManager, "prepareForm").mockImplementation()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: movieEmptyMock })
            .mockResolvedValueOnce({ data: castingEmptyMock })

        const sampleMovieDetails = await this.vueComponentAsync()
        this.assertEqual(this.privateCall(sampleMovieDetails, "director"), undefined)
    }

    public async testMovieHasActors () {
        const sampleMovieDetails = await this.vueComponentAsync()

        expect(this.privateCall(sampleMovieDetails, "actors")).toMatchObject(
            [
                {
                    type: "sample_person",
                    id: "1",
                    attributes: {
                        last_name: "Doe",
                        first_name: "John",
                        is_director: true
                    },
                    links: {
                        profile_picture: "profilepic"
                    }
                },
                {
                    type: "sample_person",
                    id: "2",
                    attributes: {
                        last_name: "Williams",
                        first_name: "Bob",
                        is_director: false
                    },
                    links: {
                        profile_picture: "profilepic"
                    }
                }
            ]
        )
    }

    public async testMovieHasNotActors () {
        jest.resetAllMocks()
        jest.spyOn(OxSchemaManager, "prepareForm").mockImplementation()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: movieEmptyMock })
            .mockResolvedValueOnce({ data: castingEmptyMock })

        const sampleMovieDetails = await this.vueComponentAsync()
        this.assertEqual(this.privateCall(sampleMovieDetails, "actors"), [])
    }

    public async testRemoveBookmarkIcon () {
        const sampleMovieDetails = await this.vueComponentAsync()
        this.assertEqual(this.privateCall(sampleMovieDetails, "bookmarkIcon"), "bookmarkRemove")
    }

    public async testAddBookmarkIcon () {
        jest.resetAllMocks()
        jest.spyOn(OxSchemaManager, "prepareForm").mockImplementation()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: movieEmptyMock })
            .mockResolvedValueOnce({ data: castingEmptyMock })

        const sampleMovieDetails = await this.vueComponentAsync()
        this.assertEqual(this.privateCall(sampleMovieDetails, "bookmarkIcon"), "bookmarkAdd")
    }

    public async testResetWithoutReload () {
        const sampleMovieDetails = await this.vueComponentAsync()
        jest.clearAllMocks()

        await this.privateCall(sampleMovieDetails, "reset")
        expect(oxApiService.get).toHaveBeenCalledTimes(0)
        this.assertEqual(this.privateCall(sampleMovieDetails, "showUpdateForm"), false)
    }

    public async testResetWithReload () {
        const sampleMovieDetails = await this.vueComponentAsync()
        jest.resetAllMocks()

        jest.spyOn(OxSchemaManager, "prepareForm").mockImplementation()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: movieMock })
            .mockResolvedValueOnce({ data: castingMock })

        await this.privateCall(sampleMovieDetails, "reset", "update")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?relations=actors%2Cdirector%2Ccategory%2Cbookmarks&fieldsets=default%2Cdetails&ratingsStats=true&withTrailer=true&permissions=true"
            )
        )

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "casting"
            )
        )

        expect(oxApiService.get).toHaveBeenCalledTimes(2)
        this.assertEqual(this.privateCall(sampleMovieDetails, "showUpdateForm"), false)
    }

    public async testAskDeleteMovie () {
        const sampleMovieDetails = await this.vueComponentAsync()

        this.assertEqual(this.privateCall(sampleMovieDetails, "showModal"), false)
        this.privateCall(sampleMovieDetails, "askDeleteMovie")
        this.assertEqual(this.privateCall(sampleMovieDetails, "showModal"), true)
    }

    public async testDeleteMovie () {
        const hrefSpy = jest.fn()
        const origin = window.location.origin
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            set: hrefSpy,
            get: () => "baseHref"
        })
        Object.defineProperty(window.location, "origin", {
            set: hrefSpy,
            get: () => origin
        })

        const sampleMovieDetails = await this.vueComponentAsync()

        await this.privateCall(sampleMovieDetails, "deleteMovie")

        expect(oxApiService.delete).toBeCalledWith(
            expect.stringContaining(
                "self"
            )
        )

        expect(hrefSpy).toHaveBeenCalledWith("back")
    }

    public async testRemoveBookmark () {
        const sampleMovieDetails = await this.vueComponentAsync()

        await this.privateCall(sampleMovieDetails, "toggleBookmark")

        expect(oxApiService.delete).toBeCalledWith(
            expect.stringContaining(
                "bookmark-self"
            )
        )
    }

    public mockOxApiServicePost () {
        jest.spyOn(oxApiService, "post").mockImplementation(() => Promise.resolve({
            data: [
                {
                    id: "updateRating",
                    status: 200,
                    body: {
                        data: {
                            id: "1",
                            type: "sample_rating",
                            attributes: {
                                rating: 4,
                                datetime: "2023-07-11 16:29:28"
                            },
                            links: {
                                self: "self",
                                schema: "schema",
                                history: "history"
                            }
                        }
                    },
                    meta: {
                        date: "2023-02-13 15:45:11+01:00",
                        copyright: "OpenXtrem-2022",
                        authors: "dev@openxtrem.com"
                    }
                },
                {
                    id: "updatedMovieMeta",
                    status: 200,
                    body: {
                        data: {
                            id: "1",
                            type: "sample_movie",
                            attributes: {},
                            links: {
                                self: "self",
                                schema: "schema",
                                history: "history"
                            },
                            meta: {
                                rating_average: 2.5,
                                rating_count: 43,
                                rating_1: 3,
                                rating_2: 28,
                                rating_3: 3,
                                rating_4: 5,
                                rating_5: 4
                            }
                        }
                    },
                    meta: {
                        date: "2023-02-13 15:45:11+01:00",
                        copyright: "OpenXtrem-2022",
                        authors: "dev@openxtrem.com"
                    }
                }
            ]
        }))
    }

    public async testUpdateRatingWithoutRating () {
        const sampleMovieDetails = await this.vueComponentAsync()

        this.mockOxApiServicePost()

        const spyAddInfo = jest.spyOn(OxNotifyManager, "addInfo")

        await this.privateCall(sampleMovieDetails, "updateRating", 4)

        expect(spyAddInfo).toHaveBeenCalledTimes(1)
        expect(oxApiService.post).toBeCalledWith(
            expect.stringContaining(
                OxUrlDiscovery.bulk() + "?stopOnFailure=true"
            ),
            {
                data: [
                    {
                        id: "updateRating",
                        method: "POST",
                        path: "ratings",
                        parameters: undefined,
                        body: {
                            data: {
                                id: "",
                                type: "sample_rating",
                                attributes: {
                                    rating: 4
                                },
                                relationships: {
                                    movie: {
                                        data: {
                                            id: "1",
                                            type: "sample_movie"
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        id: "updatedMovieMeta",
                        method: "GET",
                        path: "movie?ratingsStats=true",
                        parameters: undefined
                    }
                ]
            }
        )
    }

    public async testUpdateRatingWithRating () {
        const sampleMovieDetails = await this.vueComponentAsync()

        const rating = new SampleRating()
        rating.id = "1"
        rating.movie = sampleMovieDetails.$data.movie
        rating.rating = 3
        rating.links.self = "api/sample/ratings/1"

        sampleMovieDetails.$data.movie.myRating = rating

        this.mockOxApiServicePost()

        const spyAddInfo = jest.spyOn(OxNotifyManager, "addInfo")

        await this.privateCall(sampleMovieDetails, "updateRating", 4)

        expect(spyAddInfo).toHaveBeenCalledTimes(1)
        expect(oxApiService.post).toBeCalledWith(
            expect.stringContaining(
                OxUrlDiscovery.bulk() + "?stopOnFailure=true"
            ),
            {
                data: [
                    {
                        id: "updateRating",
                        method: "PATCH",
                        path: "api/sample/ratings/1",
                        parameters: undefined,
                        body: {
                            data: {
                                id: "1",
                                type: "sample_rating",
                                attributes: {
                                    rating: 4
                                },
                                relationships: {}
                            }
                        }
                    },
                    {
                        id: "updatedMovieMeta",
                        method: "GET",
                        path: "movie?ratingsStats=true",
                        parameters: undefined
                    }
                ]
            }
        )
    }

    public async testSearchTrailerLinkWhenATrailerIsFound () {
        const sampleMovieDetails = await this.vueComponentAsync()

        jest.resetAllMocks()

        sampleMovieDetails["movie"].trailerSearchLink = "trailer_search"

        const loadStartSpy = jest.spyOn(OxLoadingManager, "loadStart")
        const loadStopSpy = jest.spyOn(OxLoadingManager, "loadStop")
        const addInfoSpy = jest.spyOn(OxNotifyManager, "addInfo")
        const getSpy = jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({
                data: {
                    data: {
                        type: "sample_movie",
                        id: "1",
                        links: {
                            trailer: "trailer"
                        }
                    }
                }
            })

        await sampleMovieDetails["searchTrailerLink"]()

        expect(loadStartSpy).toHaveBeenCalledTimes(1)
        expect(loadStopSpy).toHaveBeenCalledTimes(1)
        expect(addInfoSpy).toHaveBeenCalledTimes(0)
        expect(getSpy).toHaveBeenCalledTimes(1)
        expect(sampleMovieDetails["movie"].trailerLink).toEqual("trailer")
        expect(sampleMovieDetails["movie"].trailerSearchLink).toBeUndefined()
    }

    public async testSearchTrailerLinkWhenNoTrailerIsFound () {
        const sampleMovieDetails = await this.vueComponentAsync()

        jest.resetAllMocks()

        sampleMovieDetails["movie"].trailerSearchLink = "trailer_search"

        const loadStartSpy = jest.spyOn(OxLoadingManager, "loadStart")
        const loadStopSpy = jest.spyOn(OxLoadingManager, "loadStop")
        const addInfoSpy = jest.spyOn(OxNotifyManager, "addInfo")
        const getSpy = jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({
                data: {
                    data: {
                        type: "sample_movie",
                        id: "1",
                        links: {
                            trailer: null
                        }
                    }
                }
            })

        await sampleMovieDetails["searchTrailerLink"]()

        expect(loadStartSpy).toHaveBeenCalledTimes(1)
        expect(loadStopSpy).toHaveBeenCalledTimes(1)
        expect(addInfoSpy).toHaveBeenCalledTimes(1)
        expect(getSpy).toHaveBeenCalledTimes(1)
        expect(sampleMovieDetails["movie"].trailerLink).toEqual(undefined)
        expect(sampleMovieDetails["movie"].trailerSearchLink).toBeUndefined()
    }

    public async testTrailerLoaded () {
        const sampleMovieDetails = await this.vueComponentAsync()
        sampleMovieDetails["trailerDialogLoaded"] = false
        sampleMovieDetails["trailerDialog"] = false

        const loadStopSpy = jest.spyOn(OxLoadingManager, "loadStop")

        sampleMovieDetails["trailerLoaded"]()

        expect(sampleMovieDetails["trailerDialogLoaded"]).toBeTruthy()
        expect(sampleMovieDetails["trailerDialog"]).toBeTruthy()
        expect(loadStopSpy).toHaveBeenCalledTimes(1)
    }

    public async testOpenTrailerWithoutLinks () {
        const sampleMovieDetails = await this.vueComponentAsync()

        sampleMovieDetails["movie"].trailerSearchLink = undefined
        sampleMovieDetails["movie"].trailerLink = undefined
        sampleMovieDetails["trailerDialog"] = false
        sampleMovieDetails["trailerDialogLoaded"] = false
        sampleMovieDetails["trailerDialogEager"] = false

        const loadStartSpy = jest.spyOn(OxLoadingManager, "loadStart")
        // @ts-ignore
        const searchTrailerSpy = jest.spyOn(sampleMovieDetails, "searchTrailerLink")

        await sampleMovieDetails["openTrailer"]()

        expect(loadStartSpy).toHaveBeenCalledTimes(0)
        expect(searchTrailerSpy).toHaveBeenCalledTimes(0)
        expect(sampleMovieDetails["trailerDialog"]).toBeFalsy()
        expect(sampleMovieDetails["trailerDialogLoaded"]).toBeFalsy()
        expect(sampleMovieDetails["trailerDialogEager"]).toBeFalsy()
    }

    public async testOpenTrailerWithSearchLink () {
        const sampleMovieDetails = await this.vueComponentAsync()

        sampleMovieDetails["movie"].trailerSearchLink = "trailer_search"
        sampleMovieDetails["movie"].trailerLink = undefined
        sampleMovieDetails["trailerDialog"] = false
        sampleMovieDetails["trailerDialogLoaded"] = false
        sampleMovieDetails["trailerDialogEager"] = false

        const loadStartSpy = jest.spyOn(OxLoadingManager, "loadStart")
        // @ts-ignore
        const searchTrailerSpy = jest.spyOn(sampleMovieDetails, "searchTrailerLink")
        searchTrailerSpy.mockImplementation(async () => {
            sampleMovieDetails["movie"].trailerLink = "trailer"
        })

        await sampleMovieDetails["openTrailer"]()

        expect(loadStartSpy).toHaveBeenCalledTimes(0)
        expect(searchTrailerSpy).toHaveBeenCalledTimes(1)
        expect(sampleMovieDetails["trailerDialog"]).toBeFalsy()
        expect(sampleMovieDetails["trailerDialogLoaded"]).toBeFalsy()
        expect(sampleMovieDetails["trailerDialogEager"]).toBeFalsy()
    }

    public async testOpenTrailerWithIncorrectLink () {
        const sampleMovieDetails = await this.vueComponentAsync()

        sampleMovieDetails["movie"].trailerSearchLink = undefined
        sampleMovieDetails["movie"].trailerLink = "trailer"
        sampleMovieDetails["trailerDialog"] = false
        sampleMovieDetails["trailerDialogLoaded"] = false
        sampleMovieDetails["trailerDialogEager"] = false

        const loadStartSpy = jest.spyOn(OxLoadingManager, "loadStart")
        // @ts-ignore
        const searchTrailerSpy = jest.spyOn(sampleMovieDetails, "searchTrailerLink")

        await sampleMovieDetails["openTrailer"]()

        expect(loadStartSpy).toHaveBeenCalledTimes(0)
        expect(searchTrailerSpy).toHaveBeenCalledTimes(0)
        expect(sampleMovieDetails["trailerDialog"]).toBeFalsy()
        expect(sampleMovieDetails["trailerDialogLoaded"]).toBeFalsy()
        expect(sampleMovieDetails["trailerDialogEager"]).toBeFalsy()
    }

    public async testOpenTrailerWithCorrectLink () {
        const sampleMovieDetails = await this.vueComponentAsync()

        sampleMovieDetails["movie"].trailerSearchLink = undefined
        sampleMovieDetails["movie"].trailerLink = "https://youtu.be/dQw4w9WgXcQ"
        sampleMovieDetails["trailerDialog"] = false
        sampleMovieDetails["trailerDialogLoaded"] = false
        sampleMovieDetails["trailerDialogEager"] = false

        const loadStartSpy = jest.spyOn(OxLoadingManager, "loadStart")
        // @ts-ignore
        const searchTrailerSpy = jest.spyOn(sampleMovieDetails, "searchTrailerLink")

        await sampleMovieDetails["openTrailer"]()

        expect(loadStartSpy).toHaveBeenCalledTimes(1)
        expect(searchTrailerSpy).toHaveBeenCalledTimes(0)
        expect(sampleMovieDetails["trailerDialog"]).toBeFalsy()
        expect(sampleMovieDetails["trailerDialogLoaded"]).toBeFalsy()
        expect(sampleMovieDetails["trailerDialogEager"]).toBeTruthy()
    }

    public async testOpenTrailerWhenAlreadyLoaded () {
        const sampleMovieDetails = await this.vueComponentAsync()

        sampleMovieDetails["movie"].trailerSearchLink = undefined
        sampleMovieDetails["movie"].trailerLink = "https://youtu.be/dQw4w9WgXcQ"
        sampleMovieDetails["trailerDialog"] = false
        sampleMovieDetails["trailerDialogLoaded"] = true
        sampleMovieDetails["trailerDialogEager"] = true

        const loadStartSpy = jest.spyOn(OxLoadingManager, "loadStart")
        // @ts-ignore
        const searchTrailerSpy = jest.spyOn(sampleMovieDetails, "searchTrailerLink")

        await sampleMovieDetails["openTrailer"]()

        expect(loadStartSpy).toHaveBeenCalledTimes(0)
        expect(searchTrailerSpy).toHaveBeenCalledTimes(0)
        expect(sampleMovieDetails["trailerDialog"]).toBeTruthy()
        expect(sampleMovieDetails["trailerDialogLoaded"]).toBeTruthy()
        expect(sampleMovieDetails["trailerDialogEager"]).toBeTruthy()
    }
}

(new SampleMovieDetailsTest()).launchTests()
