/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import OxTranslator from "@/core/plugins/OxTranslator"
import oxApiService from "@/core/utils/OxApiService"
import SampleMovieSettings from "@modules/sample/vue/views/SampleMovieSettings/SampleMovieSettings.vue"
import { personMock, personSchemaMock, personsMock } from "@modules/sample/vue/tests/mocks/SampleMocks"
import SamplePerson from "@modules/sample/vue/models/SamplePerson"
import OxDatagrid from "@/core/components/OxDatagrid/OxDatagrid.vue"
import { OxUrlBuilder } from "@/core/utils/OxUrlTools"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"
import OxTest from "@oxify/utils/OxTest"
import OxThemeCore from "@oxify/utils/OxThemeCore"
import { isEmpty } from "lodash"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

const mockRefresh = jest.fn()
/**
 * SampleMovieSettings tests
 */
export default class SampleMovieSettingsTest extends OxTest {
    protected component = SampleMovieSettings

    private person = new SamplePerson()
    private person2 = new SamplePerson()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.person.id = "1"
        this.person.type = "sample_person"
        this.person.attributes = {
            last_name: "Doe",
            first_name: "John",
            is_director: true,
            birthdate: "1990-02-01",
            sex: "m",
            activity_start: "2002-02-01"
        }
        this.person.relationships = {
            nationality: {
                data: {
                    type: "sample_nationality",
                    id: "1"
                }
            }
        }
        this.person.links = {
            self: "self",
            schema: "schema",
            history: "history",
            profile_picture: "profilepic"
        }

        this.person2.id = "2"
        this.person2.type = "sample_person"
        this.person2.attributes = {
            last_name: "Williams",
            first_name: "Bob",
            is_director: false,
            birthdate: "1990-03-01",
            sex: "m",
            activity_start: "2003-02-01"
        }
        this.person2.relationships = {
            nationality: {
                data: {
                    type: "sample_nationality",
                    id: "2"
                }
            }
        }
        this.person2.links = {
            self: "self",
            schema: "schema",
            history: "history",
            profile_picture: "profilepic"
        }
    }

    protected beforeTest () {
        super.beforeTest()

        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: personsMock })
            .mockResolvedValueOnce({ data: personSchemaMock })

        jest.spyOn(oxApiService, "delete").mockImplementation(() => Promise.resolve({ data: "" }))
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    protected async mountComponentAsync (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        if (isEmpty(stubs)) {
            stubs = {
                SamplePersonForm: true
            }
        }
        const wrapper = shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )

        await this.flushPromises()

        return wrapper
    }

    protected async vueComponentAsync (
        props: object = {
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        },
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = await this.mountComponentAsync(props, stubs, slots)

        return wrapper.vm
    }

    public async testSampleMovieSettingsCreation () {
        await this.vueComponentAsync()

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "persons-list?limit=20&relations=nationality&fieldsets=default%2Cextra&permissions=true"
            )
        )

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "api/schemas/sample_person?fieldsets=default%2Cextra"
            )
        )
    }

    public async testFormTitleAdd () {
        const sampleMovieSettings = await this.vueComponentAsync()
        this.assertEqual(this.privateCall(sampleMovieSettings, "formTitle"), "CSamplePerson-title-add")
    }

    public async testFormTitleFullname () {
        const sampleMovieSettings = await this.vueComponentAsync()

        sampleMovieSettings["person"] = this.person
        this.assertEqual(this.privateCall(sampleMovieSettings, "formTitle"), "John Doe")
    }

    public async testSexIconColorM () {
        const sampleMovieSettings = await this.vueComponentAsync()
        this.assertEqual(this.privateCall(sampleMovieSettings, "getSexIconColor", "m"), OxThemeCore.blueText)
    }

    public async testSexIconColorF () {
        const sampleMovieSettings = await this.vueComponentAsync()
        this.assertEqual(this.privateCall(sampleMovieSettings, "getSexIconColor", "f"), OxThemeCore.pinkText)
    }

    public async testUpdateItem () {
        const sampleMovieSettings = await this.vueComponentAsync()
        this.privateCall(sampleMovieSettings, "updateItem", this.person)
        this.assertEqual(this.privateCall(sampleMovieSettings, "person"), this.person)
        this.assertTrue(this.privateCall(sampleMovieSettings, "dialog"))
    }

    public async testClose () {
        const sampleMovieSettings = await this.vueComponentAsync()
        sampleMovieSettings["person"] = this.person
        sampleMovieSettings["dialog"] = true

        this.privateCall(sampleMovieSettings, "close")
        this.assertEqual(this.privateCall(sampleMovieSettings, "person"), undefined)
        this.assertFalse(this.privateCall(sampleMovieSettings, "dialog"))
    }

    public async testDataSaved () {
        const sampleMovieSettings = await this.mountComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })

        const mockClose = jest.fn()
        sampleMovieSettings.vm["close"] = mockClose

        await this.privateCall(sampleMovieSettings.vm, "dataSaved")
        expect(mockClose).toHaveBeenCalledTimes(1)
        expect(mockRefresh).toHaveBeenCalledTimes(1)
    }

    public async testAskDeleteItem () {
        const sampleMovieSettings = await this.vueComponentAsync()
        this.privateCall(sampleMovieSettings, "askDeleteItem", this.person)
        this.assertEqual(this.privateCall(sampleMovieSettings, "person"), this.person)
        this.assertTrue(this.privateCall(sampleMovieSettings, "showDeleteModal"))
    }

    public async testAskMassDeleteItem () {
        const sampleMovieSettings = await this.vueComponentAsync()
        this.privateCall(sampleMovieSettings, "askMassDeleteItem", [this.person])
        this.assertEqual(this.privateCall(sampleMovieSettings, "selectedPersons"), [this.person])
        this.assertTrue(this.privateCall(sampleMovieSettings, "showMassDeleteModal"))
    }

    public async testDeleteItemDialogOpened () {
        const sampleMovieSettings = await this.vueComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })
        sampleMovieSettings["person"] = this.person
        sampleMovieSettings["dialog"] = true

        const mockClose = jest.fn()
        sampleMovieSettings["close"] = mockClose

        await this.privateCall(sampleMovieSettings, "deleteItem")

        expect(oxApiService.delete).toBeCalledWith(
            expect.stringContaining(
                "self"
            )
        )
        this.assertEqual(this.privateCall(sampleMovieSettings, "person"), undefined)
        expect(mockClose).toHaveBeenCalledTimes(1)
        expect(mockRefresh).toHaveBeenCalledTimes(1)
    }

    public async testDeleteItemDialogClosed () {
        const sampleMovieSettings = await this.vueComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })
        sampleMovieSettings["person"] = this.person

        const mockClose = jest.fn()
        sampleMovieSettings["close"] = mockClose

        await this.privateCall(sampleMovieSettings, "deleteItem")

        expect(oxApiService.delete).toBeCalledWith(
            expect.stringContaining(
                "self"
            )
        )
        this.assertEqual(this.privateCall(sampleMovieSettings, "person"), undefined)
        expect(mockClose).toHaveBeenCalledTimes(0)
        expect(mockRefresh).toHaveBeenCalledTimes(1)
    }

    public async testDeleteItemPersonNull () {
        const sampleMovieSettings = await this.vueComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })

        await this.privateCall(sampleMovieSettings, "deleteItem")

        expect(oxApiService.delete).toHaveBeenCalledTimes(0)
        expect(mockRefresh).toHaveBeenCalledTimes(0)
    }

    public async testMassDeleteAction () {
        jest.spyOn(oxApiService, "post").mockImplementation(() => Promise.resolve({
            data: [
                {
                    id: "delete-1",
                    status: 204,
                    body: ""
                },
                {
                    id: "delete-2",
                    status: 204,
                    body: ""
                }
            ]
        }))

        const sampleMovieSettings = await this.vueComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })

        sampleMovieSettings["selectedPersons"] = [this.person, this.person2]
        await this.privateCall(sampleMovieSettings, "massDeleteAction")

        expect(oxApiService.post).toBeCalledWith(
            expect.stringContaining(
                OxUrlDiscovery.bulk() + "?stopOnFailure=true"
            ),
            {
                data: [
                    {
                        id: "delete-1",
                        method: "DELETE",
                        path: "self"
                    },
                    {
                        id: "delete-2",
                        method: "DELETE",
                        path: "self"
                    }
                ]
            }
        )

        expect(mockRefresh).toHaveBeenCalledTimes(1)
    }

    public async testMarkAsDirector () {
        jest.spyOn(oxApiService, "patch").mockImplementation(() => Promise.resolve({ data: personMock }))

        const sampleMovieSettings = await this.vueComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })

        await this.privateCall(sampleMovieSettings, "markAsDirector", this.person2)

        expect(oxApiService.patch).toBeCalledWith(
            expect.stringContaining(
                "self"
            ),
            {
                data: {
                    id: "2",
                    type: "sample_person",
                    attributes: {
                        is_director: 1
                    },
                    relationships: {}
                }
            }
        )
        expect(mockRefresh).toHaveBeenCalledTimes(1)
    }

    public async testMarkAsDirectorAction () {
        jest.spyOn(oxApiService, "post").mockImplementation(() => Promise.resolve({
            data: [
                {
                    id: "1",
                    status: 200,
                    body: {
                        data: {
                            id: "1",
                            type: "sample_person",
                            attributes: {
                                last_name: "Doe",
                                first_name: "John",
                                is_director: true
                            },
                            links: {
                                self: "self",
                                schema: "schema",
                                history: "history"
                            }
                        }
                    },
                    meta: {
                        date: "2023-02-13 15:45:11+01:00",
                        copyright: "OpenXtrem-2022",
                        authors: "dev@openxtrem.com"
                    }
                },
                {
                    id: "2",
                    status: 200,
                    body: {
                        data: {
                            id: "2",
                            type: "sample_person",
                            attributes: {
                                last_name: "Williams",
                                first_name: "Bob",
                                is_director: true
                            },
                            links: {
                                self: "self",
                                schema: "schema",
                                history: "history"
                            }
                        }
                    },
                    meta: {
                        date: "2023-02-13 15:45:11+01:00",
                        copyright: "OpenXtrem-2022",
                        authors: "dev@openxtrem.com"
                    }
                }
            ]
        }))

        const sampleMovieSettings = await this.vueComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })

        await this.privateCall(sampleMovieSettings, "massMarkAsDirectorAction", [this.person, this.person2])

        expect(oxApiService.post).toBeCalledWith(
            expect.stringContaining(
                OxUrlDiscovery.bulk() + "?stopOnFailure=true"
            ),
            {
                data: [
                    {
                        id: "1",
                        method: "PATCH",
                        path: "self",
                        parameters: undefined,
                        body: {
                            data: {
                                id: "1",
                                type: "sample_person",
                                attributes: {
                                    is_director: 1
                                },
                                relationships: {}
                            }
                        }
                    },
                    {
                        id: "2",
                        method: "PATCH",
                        path: "self",
                        parameters: undefined,
                        body: {
                            data: {
                                id: "2",
                                type: "sample_person",
                                attributes: {
                                    is_director: 1
                                },
                                relationships: {}
                            }
                        }
                    }
                ]
            }
        )

        expect(mockRefresh).toHaveBeenCalledTimes(1)
    }

    public async testFilterItemsByDirector () {
        const sampleMovieSettings = await this.vueComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })

        await this.privateCall(sampleMovieSettings, "filterItems", ["directors"])

        const expectedUrl = new OxUrlBuilder("self")
        expectedUrl.withFilters({ key: "is_director", operator: "equal", value: "1" })
        expect(mockRefresh).toBeCalledWith(expectedUrl)
    }

    public async testFilterItemsByActivityStart () {
        const sampleMovieSettings = await this.vueComponentAsync({
            links: {
                nationalities: "nationalities",
                personsCreate: "persons-create",
                personsList: "persons-list"
            }
        }, {
            OxDatagrid: {
                template: "<div ref='datagrid' />",
                methods: {
                    refresh: mockRefresh
                }
            },
            SamplePersonForm: true
        })

        await this.privateCall(sampleMovieSettings, "filterItems", ["activity_start_2000"])

        const expectedUrl = new OxUrlBuilder("self")
        expectedUrl.withFilters({ key: "activity_start", operator: "greaterOrEqual", value: "2001-01-01" })
        expect(mockRefresh).toBeCalledWith(expectedUrl)
    }
}

(new SampleMovieSettingsTest()).launchTests()
