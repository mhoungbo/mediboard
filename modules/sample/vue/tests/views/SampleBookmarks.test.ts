/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import OxTranslator from "@/core/plugins/OxTranslator"
import oxApiService from "@/core/utils/OxApiService"
import SampleBookmarks from "@modules/sample/vue/views/SampleBookmarks/SampleBookmarks.vue"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import OxPagination from "@/core/components/OxPagination/OxPagination.vue"
import { moviesEmptyMock, moviesMock } from "@modules/sample/vue/tests/mocks/SampleMocks"
import Vuetify from "vuetify"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * SampleBookmarks tests
 */
export default class SampleBookmarksTest extends OxTest {
    protected component = SampleBookmarks

    private movie = new SampleMovie()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.movie.id = "1"
        this.movie.type = "sample_movie"
        this.movie.attributes = {
            name: "Movie 1",
            release: "2002-01-01",
            duration: "01:55:00",
            csa: "18",
            languages: "fr"
        }
        this.movie.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            },
            bookmarks: {
                data: {
                    type: "sample_bookmark",
                    id: "1"
                }
            }
        }
        this.movie.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }
    }

    protected beforeTest () {
        super.beforeTest()

        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: moviesMock }))
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected async mountComponentAsync (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia,
                vuetify: new Vuetify()
            }
        )

        await this.flushPromises()

        return wrapper
    }

    protected async vueComponentAsync (
        props: object = {
            links: {
                movies: "movies"
            }
        },
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = await this.mountComponentAsync(props, stubs, slots)

        return wrapper.vm
    }

    public async testSampleBookmarksCreation () {
        await this.vueComponentAsync()

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "movies?limit=10&relations=category%2Cbookmarks&bookmarked=1"
            )
        )
    }

    public async testUsePlural () {
        const sampleBookmarks = await this.vueComponentAsync()
        this.assertTrue(this.privateCall(sampleBookmarks, "usePlural"))
    }

    public async testNotUsePlural () {
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: moviesEmptyMock }))

        const sampleBookmarks = await this.vueComponentAsync()
        this.assertFalse(this.privateCall(sampleBookmarks, "usePlural"))
    }

    public async testShowNoDataIllustration () {
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: moviesEmptyMock }))

        const sampleBookmarks = await this.vueComponentAsync()
        this.assertTrue(this.privateCall(sampleBookmarks, "showNoDataIllustration"))
    }

    public async testNotShowNoDataIllustration () {
        const sampleBookmarks = await this.vueComponentAsync()
        this.assertFalse(this.privateCall(sampleBookmarks, "showNoDataIllustration"))
    }

    public async testRemoveBookmark () {
        // How to mock stubs => https://stackoverflow.com/a/64781728
        const mockRefresh = jest.fn()
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        OxPagination.options.methods.refresh = mockRefresh
        jest.spyOn(oxApiService, "delete").mockImplementation(() => Promise.resolve({ data: "" }))

        const sampleBookmarks = await this.vueComponentAsync({
            links: {
                movies: "movies"
            }
        }, { OxPagination })

        await this.privateCall(sampleBookmarks, "removeBookmark", this.movie)
        expect(oxApiService.delete).toBeCalledWith(
            expect.stringContaining(
                "bookmark-self-1"
            )
        )
        expect(mockRefresh).toHaveBeenCalledTimes(1)
    }
}

(new SampleBookmarksTest()).launchTests()
