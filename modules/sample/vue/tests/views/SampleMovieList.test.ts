/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import OxTranslator from "@/core/plugins/OxTranslator"
import oxApiService from "@/core/utils/OxApiService"
import SampleMovieList from "@modules/sample/vue/views/SampleMovieList/SampleMovieList.vue"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import SampleCategory from "@modules/sample/vue/models/SampleCategory"
import SampleNationality from "@modules/sample/vue/models/SampleNationality"
import {
    castingEmptyMock,
    movieMock,
    movieSchemaMock,
    moviesEmptyMock,
    moviesMock
} from "@modules/sample/vue/tests/mocks/SampleMocks"
import SampleCasting from "@modules/sample/vue/models/SampleCasting"
import { isEmpty } from "lodash"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * SampleMovieList tests
 */
export default class SampleMovieListTest extends OxTest {
    protected component = SampleMovieList

    private movie = new SampleMovie()
    private category = new SampleCategory()
    private nationality = new SampleNationality()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.movie.id = "1"
        this.movie.type = "sample_movie"
        this.movie.attributes = {
            name: "Movie 1",
            release: "2002-01-01",
            duration: "01:55:00",
            csa: "18",
            languages: "fr"
        }
        this.movie.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            },
            bookmarks: {
                data: {
                    type: "sample_bookmark",
                    id: "1"
                }
            }
        }
        this.movie.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }

        this.category.id = "1"
        this.category.type = "sample_category"
        this.category.attributes = {
            name: "Category 1",
            color: null,
            active: true
        }

        this.nationality.id = "1"
        this.nationality.type = "sample_nationality"
        this.nationality.attributes = {
            name: "Nationality 1",
            code: "NA",
            flag: null
        }
    }

    protected beforeTest () {
        super.beforeTest()

        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: moviesMock })
            .mockResolvedValueOnce({ data: movieSchemaMock })
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    protected async mountComponentAsync (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        if (isEmpty(stubs)) {
            stubs = {
                SampleMovieForm: true
            }
        }
        const wrapper = shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )

        await this.flushPromises()

        return wrapper
    }

    protected async vueComponentAsync (
        props: object = {
            links: {
                "add-movie": "add-movie",
                bookmarks: "bookmarks",
                categories: "categories",
                movies: "movies",
                nationalities: "nationalities",
                persons: "persons"
            }
        },
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = await this.mountComponentAsync(props, stubs, slots)

        return wrapper.vm
    }

    public async testSampleMovieListCreation () {
        await this.vueComponentAsync()

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "movies?limit=20&relations=category%2Cbookmarks"
            )
        )

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "api/schemas/sample_movie?fieldsets=default%2Cdetails"
            )
        )
    }

    public async testUsePlural () {
        const sampleMovieList = await this.vueComponentAsync()
        this.assertTrue(this.privateCall(sampleMovieList, "usePlural"))
    }

    public async testNotUsePlural () {
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: moviesEmptyMock })
            .mockResolvedValueOnce({ data: movieSchemaMock })

        const sampleMovieList = await this.vueComponentAsync()
        this.assertFalse(this.privateCall(sampleMovieList, "usePlural"))
    }

    public async testShowResetButtonHavingCategory () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["categoryFilter"] = this.category
        this.assertTrue(this.privateCall(sampleMovieList, "showResetButton"))
    }

    public async testShowResetButtonHavingNationality () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["nationalityFilter"] = this.nationality
        this.assertTrue(this.privateCall(sampleMovieList, "showResetButton"))
    }

    public async testShowResetButtonHavingCSA () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["csaFilter"] = ["12", "16"]
        this.assertTrue(this.privateCall(sampleMovieList, "showResetButton"))
    }

    public async testShowResetButtonDateMin () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["dateMinFilter"] = "2023-02-01"
        this.assertTrue(this.privateCall(sampleMovieList, "showResetButton"))
    }

    public async testShowResetButtonDateMax () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["dateMaxFilter"] = "2023-02-06"
        this.assertTrue(this.privateCall(sampleMovieList, "showResetButton"))
    }

    public async testNotShowResetButton () {
        const sampleMovieList = await this.vueComponentAsync()
        this.assertFalse(this.privateCall(sampleMovieList, "showResetButton"))
    }

    public async testIsNotCurrentMovieExist () {
        const sampleMovieList = await this.vueComponentAsync()
        this.assertFalse(this.privateCall(sampleMovieList, "isCurrentMovieExist"))
    }

    public async testIsCurrentMovieExist () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["currentMovie"] = this.movie
        this.assertTrue(this.privateCall(sampleMovieList, "isCurrentMovieExist"))
    }

    public async testShowData () {
        const sampleMovieList = await this.vueComponentAsync()
        this.assertFalse(this.privateCall(sampleMovieList, "showNoData"))
    }

    public async testShowNoData () {
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: moviesEmptyMock })
            .mockResolvedValueOnce({ data: movieSchemaMock })

        const sampleMovieList = await this.vueComponentAsync()
        this.assertTrue(this.privateCall(sampleMovieList, "showNoData"))
    }

    public async testShowDataHavingSearch () {
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: moviesEmptyMock })
            .mockResolvedValueOnce({ data: movieSchemaMock })

        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["noSearchResult"] = true
        this.assertFalse(this.privateCall(sampleMovieList, "showNoData"))
    }

    public async testIsGridModeByDefault () {
        const sampleMovieList = await this.vueComponentAsync()
        expect(this.privateCall(sampleMovieList, "isGridMode")).toBe(true)
    }

    public async testIsNotGridMode () {
        const sampleMovieList = await this.vueComponentAsync()
        sampleMovieList["viewMode"] = "list"
        expect(this.privateCall(sampleMovieList, "isGridMode")).toBe(false)
    }

    public async testMovieSkeletonTypeViewGrid () {
        const sampleMovieList = await this.vueComponentAsync()
        expect(this.privateCall(sampleMovieList, "movieSkeletonType")).toBe("card")
    }

    public async testMovieSkeletonTypeViewList () {
        const sampleMovieList = await this.vueComponentAsync()
        sampleMovieList["viewMode"] = "list"
        expect(this.privateCall(sampleMovieList, "movieSkeletonType")).toBe("image, article")
    }

    public async testMoviesClassesViewGrid () {
        const sampleMovieList = await this.vueComponentAsync()
        expect(this.privateCall(sampleMovieList, "moviesClasses")).toMatchObject({
            "SampleMovieList-movies": true,
            list: false
        })
    }

    public async testMoviesClassesViewList () {
        const sampleMovieList = await this.vueComponentAsync()
        sampleMovieList["viewMode"] = "list"
        expect(this.privateCall(sampleMovieList, "moviesClasses")).toMatchObject({
            "SampleMovieList-movies": true,
            list: true
        })
    }

    public async testGridMovieSkeletonHeight () {
        const sampleMovieList = await this.vueComponentAsync()
        expect(this.privateCall(sampleMovieList, "movieSkeletonHeight")).toBe(256)
    }

    public async testListMovieSkeletonHeight () {
        const sampleMovieList = await this.vueComponentAsync()
        sampleMovieList["viewMode"] = "list"
        expect(this.privateCall(sampleMovieList, "movieSkeletonHeight")).toBe(210)
    }

    public async testCardMovieSkeletonClass () {
        const sampleMovieList = await this.vueComponentAsync()
        expect(sampleMovieList["movieSkeletonClasses"]).toBe("SampleMovieList-cardSkeleton")
    }

    public async testListMovieSkeletonClass () {
        const sampleMovieList = await this.vueComponentAsync()
        sampleMovieList["viewMode"] = "list"
        expect(sampleMovieList["movieSkeletonClasses"]).toBe("SampleMovieList-cardExtendedSkeleton")
    }

    public async testChangeDateMin () {
        const sampleMovieList = await this.vueComponentAsync()
        // Only clears the mock.calls and mock.instances properties of all mocks.
        jest.clearAllMocks()

        await this.privateCall(sampleMovieList, "changeDateMin", "2023-02-01")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?filter=release.greaterOrEqual.2023-02-01"
            )
        )
    }

    public async testChangeDateMax () {
        const sampleMovieList = await this.vueComponentAsync()
        jest.clearAllMocks()

        await this.privateCall(sampleMovieList, "changeDateMax", "2023-02-01")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?filter=release.lessOrEqual.2023-02-01"
            )
        )
    }

    public async testChangeCategory () {
        const sampleMovieList = await this.vueComponentAsync()
        jest.clearAllMocks()

        await this.privateCall(sampleMovieList, "changeCategory", this.category)

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?filter=category_id.equal.1"
            )
        )
    }

    public async testChangeNationality () {
        const sampleMovieList = await this.vueComponentAsync()
        jest.clearAllMocks()

        await this.privateCall(sampleMovieList, "changeNationality", this.nationality)

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?nationality_id=1"
            )
        )
    }

    public async testChangeCSA () {
        const sampleMovieList = await this.vueComponentAsync()
        jest.clearAllMocks()

        await this.privateCall(sampleMovieList, "changeCSA", ["12", "16"])

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?filter=csa.in.12.16"
            )
        )
    }

    public async testResetFilters () {
        const sampleMovieList = await this.vueComponentAsync()
        jest.clearAllMocks()

        sampleMovieList["categoryFilter"] = this.category
        sampleMovieList["nationalityFilter"] = this.nationality
        sampleMovieList["csaFilter"] = ["12", "16"]
        sampleMovieList["dateMinFilter"] = "2023-02-02"
        sampleMovieList["dateMaxFilter"] = "2023-02-06"

        await this.privateCall(sampleMovieList, "resetFilters")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self"
            )
        )
    }

    public async testMakeSearchWithResult () {
        const sampleMovieList = await this.vueComponentAsync()
        jest.clearAllMocks()

        await this.privateCall(sampleMovieList, "makeSearch", "search")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?filter=name.contains.search"
            )
        )

        this.assertFalse(this.privateCall(sampleMovieList, "noSearchResult"))
    }

    public async testMakeSearchWithoutResult () {
        const sampleMovieList = await this.vueComponentAsync()
        /*
         * Resets the state of all mocks. Have to redefine mock implementation
         *   => necessary if we need to modify the return value on the same mocked function (oxApiService.get here)
         */
        jest.resetAllMocks()

        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: moviesEmptyMock }))

        await this.privateCall(sampleMovieList, "makeSearch", "search")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?filter=name.contains.search"
            )
        )

        this.assertTrue(this.privateCall(sampleMovieList, "noSearchResult"))
    }

    public async testUpdateMovie () {
        const sampleMovieList = await this.vueComponentAsync()

        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: movieMock })
            .mockResolvedValueOnce({ data: castingEmptyMock })

        await this.privateCall(sampleMovieList, "updateMovie", this.movie)

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self?relations=director%2Ccategory&fieldsets=all"
            )
        )

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "casting?relations=actor"
            )
        )

        this.assertTrue(sampleMovieList["showUpdateForm"])
    }

    public async testDeleteMovie () {
        jest.spyOn(oxApiService, "delete").mockImplementation(() => Promise.resolve({ data: "" }))

        const sampleMovieList = await this.vueComponentAsync()
        sampleMovieList["currentMovie"] = this.movie

        await this.privateCall(sampleMovieList, "deleteMovie")

        expect(oxApiService.delete).toBeCalledWith(
            expect.stringContaining(
                "self"
            )
        )
    }

    public async testAddMovie () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["currentMovie"] = this.movie
        sampleMovieList["currentMovieCasting"] = [
            new SampleCasting(),
            new SampleCasting()
        ]

        this.privateCall(sampleMovieList, "addMovie")
        this.assertEqual(sampleMovieList["currentMovie"].id, "")
        this.assertEqual(sampleMovieList["currentMovieCasting"], [])
        this.assertTrue(sampleMovieList["showUpdateForm"])
    }

    public async testResetEditFormAfterCreate () {
        const sampleMovieList = await this.vueComponentAsync()
        jest.clearAllMocks()

        sampleMovieList["currentMovie"] = this.movie
        sampleMovieList["currentMovieCasting"] = [
            new SampleCasting(),
            new SampleCasting()
        ]

        await this.privateCall(sampleMovieList, "resetEditForm", "create", this.movie)
        this.assertEqual(sampleMovieList["currentMovie"].id, "")
        this.assertEqual(sampleMovieList["currentMovieCasting"], [])
        this.assertFalse(sampleMovieList["showUpdateForm"])

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "self"
            )
        )
    }

    public async testResetEditFormAfterUpdate () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["currentMovie"] = this.movie
        sampleMovieList["currentMovieCasting"] = [
            new SampleCasting(),
            new SampleCasting()
        ]

        await this.privateCall(sampleMovieList, "resetEditForm", "update", this.movie)
        this.assertEqual(sampleMovieList["currentMovie"].id, "")
        this.assertEqual(sampleMovieList["currentMovieCasting"], [])
        this.assertFalse(sampleMovieList["showUpdateForm"])
        expect(sampleMovieList["movies"].objects).toMatchObject(
            [
                {
                    type: "sample_movie",
                    id: "1",
                    attributes: {
                        name: "Movie 1",
                        release: "2002-01-01",
                        duration: "01:55:00",
                        csa: "18",
                        languages: "fr"
                    },
                    relationships: {
                        category: {
                            data: {
                                type: "sample_category",
                                id: "1"
                            }
                        },
                        bookmarks: {
                            data: {
                                type: "sample_bookmark",
                                id: "1"
                            }
                        }
                    },
                    links: {
                        self: "self",
                        schema: "schema",
                        history: "history",
                        cover: "cover",
                        details: "details"
                    }
                },
                {
                    type: "sample_movie",
                    id: "2",
                    attributes: {
                        name: "Movie 2",
                        release: "2002-01-01",
                        duration: "01:55:00",
                        csa: "18",
                        languages: "fr"
                    },
                    relationships: {
                        category: {
                            data: {
                                type: "sample_category",
                                id: "1"
                            }
                        },
                        bookmarks: {
                            data: {
                                type: "sample_bookmark",
                                id: "2"
                            }
                        }
                    },
                    links: {
                        self: "self",
                        schema: "schema",
                        history: "history",
                        cover: "cover",
                        details: "details"
                    }
                }
            ]
        )
    }

    public async testResetDeleteMovie () {
        const sampleMovieList = await this.vueComponentAsync()

        sampleMovieList["currentMovie"] = this.movie
        sampleMovieList["currentMovieCasting"] = [
            new SampleCasting(),
            new SampleCasting()
        ]

        this.privateCall(sampleMovieList, "resetDeleteMovie")
        this.assertEqual(sampleMovieList["currentMovie"].id, "")
        this.assertEqual(sampleMovieList["currentMovieCasting"], [])
    }

    public async testAskDeleteMovie () {
        const sampleMovieList = await this.vueComponentAsync()

        this.assertEqual(this.privateCall(sampleMovieList, "showDeleteModal"), false)
        this.privateCall(sampleMovieList, "askDeleteMovie", this.movie)
        this.assertEqual(this.privateCall(sampleMovieList, "showDeleteModal"), true)
    }

    public async testDisplayMovieDetails () {
        const hrefSpy = jest.fn()
        const origin = window.location.origin
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            set: hrefSpy,
            get: () => "baseHref"
        })
        Object.defineProperty(window.location, "origin", {
            set: hrefSpy,
            get: () => origin
        })

        const sampleMovieList = await this.vueComponentAsync()

        this.privateCall(sampleMovieList, "displayMovieDetails", this.movie)
        expect(hrefSpy).toHaveBeenCalledWith("details")
    }
}

(new SampleMovieListTest()).launchTests()
