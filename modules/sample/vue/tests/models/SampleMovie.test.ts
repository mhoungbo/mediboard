/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import SampleRating from "@modules/sample/vue/models/SampleRating"

/**
 * SampleMovie tests
 */
export default class SampleMovieTest extends OxTest {
    protected component = "SampleMovie"

    public testMovieGetLanguagesWithOneLanguage () {
        const movie = new SampleMovie()
        movie.languagesData = "fr"
        expect(movie.languages).toEqual("CSampleMovie.languages.fr")
    }

    public testMovieGetLanguagesWithMultiLanguage () {
        const movie = new SampleMovie()
        movie.languagesData = "fr|es"
        expect(movie.languages).toEqual("CSampleMovie.languages.fr, CSampleMovie.languages.es")
    }

    public testMovieHasPermEdit () {
        const movie = new SampleMovie()
        movie.meta = {
            permissions: {
                perm: "edit"
            }
        }
        expect(movie.permEdit).toBeTruthy()
    }

    public testMovieHasNotPermEdit () {
        const movie = new SampleMovie()
        expect(movie.permEdit).toBeFalsy()
    }

    public testMovieEmptyDetailLink () {
        const movie = new SampleMovie()
        movie.links = {
            self: "/api/sample/movies/1",
            schema: "/api/schemas/sample_movie",
            history: "/api/history/sample_movie/1",
            cover: "?m=files&raw=thumbnail&document_id=42916&thumb=0"
        }
        expect(movie.detailLink).toEqual("")
    }

    public testMovieNotEmptyDetailLink () {
        const movie = new SampleMovie()
        movie.links = {
            self: "/api/sample/movies/1",
            schema: "/api/schemas/sample_movie",
            history: "/api/history/sample_movie/1",
            cover: "?m=files&raw=thumbnail&document_id=42916&thumb=0",
            details: "/mediboard/gui/sample/movies/1"
        }
        expect(movie.detailLink).toEqual("/mediboard/gui/sample/movies/1")
    }
}

(new SampleMovieTest()).launchTests()

describe("SampleMovie",
    () => {
        test.each(
            [
                { trailerLink: "https://www.youtube.com/watch?v=dQw4w9WgXcQ&feature=feedrec_grec_index" },
                { trailerLink: "https://www.youtube.com/v/dQw4w9WgXcQ?fs=1&amp;hl=en_US&amp;rel=0" },
                { trailerLink: "https://www.youtube.com/watch?v=dQw4w9WgXcQ#t=0m10s" },
                { trailerLink: "https://www.youtube.com/embed/dQw4w9WgXcQ?rel=0" },
                { trailerLink: "https://www.youtube.com/watch?v=dQw4w9WgXcQ" },
                { trailerLink: "https://youtu.be/dQw4w9WgXcQ" }
            ]
        )("embeddedTrailerLink with $trailerLink should have an embedLink",
            ({ trailerLink }) => {
                const sampleMovie = new SampleMovie()
                sampleMovie.trailerLink = trailerLink

                expect(sampleMovie.embeddedTrailerLink).toEqual("https://www.youtube.com/embed/dQw4w9WgXcQ?enablejsapi=1&version=3&autoplay=1")
            }
        )
    }
)
