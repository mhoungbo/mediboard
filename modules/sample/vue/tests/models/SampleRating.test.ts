/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import SampleRating from "@modules/sample/vue/models/SampleRating"

/* eslint-disable dot-notation */

describe("SampleRating",
    () => {
        test("formattedDate should be formatted to DD/MM/YYYY",
            async () => {
                const sampleRating = new SampleRating()
                sampleRating.datetime = "2023-07-10 12:35:43"
                expect(sampleRating.formattedDate).toEqual("10/07/2023")
            }
        )
    }
)
