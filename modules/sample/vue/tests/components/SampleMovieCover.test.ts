/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import SampleRatingTab from "@modules/sample/vue/components/SampleRatingTab/SampleRatingTab.vue"
import * as OxApiManager from "@/core/utils/OxApiManager"
import OxCollection from "@/core/models/OxCollection"
import SampleRating from "@modules/sample/vue/models/SampleRating"
import { flushPromises } from "@oxify/utils/OxTest"
import SampleMovieCover from "@modules/sample/vue/components/SampleMovieCover/SampleMovieCover.vue"

/* eslint-disable dot-notation */

const mountComponent = (props?: object) => {
    return shallowMount(SampleMovieCover, {
        propsData: { ...props }
    })
}

describe("SampleRatingTab",
    () => {
        afterEach(() => {
            jest.resetAllMocks()
        })

        test("If hasTrailer is true then coverClass should BE animated",
            () => {
                const movieCover = mountComponent({ hasTrailer: true }).vm

                expect(movieCover["coverClass"]).toEqual({
                    SampleMovieCover: true,
                    animated: true
                })
            }
        )

        test("If hasTrailer is false then coverClass should NOT BE animated",
            () => {
                const movieCover = mountComponent({ hasTrailer: false }).vm

                expect(movieCover["coverClass"]).toEqual({
                    SampleMovieCover: true,
                    animated: false
                })
            }
        )

        test("emit click",
            () => {
                const movieCover = mountComponent({ hasTrailer: false })

                movieCover.vm["click"]({} as Event)
                expect(movieCover.emitted()).toHaveProperty("click")
            }
        )
    }
)
