/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import SampleMovieCardExtended from "@modules/sample/vue/components/SampleMovieCardExtended/SampleMovieCardExtended.vue"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import { storeObject } from "@/core/utils/OxStorage"
import { setActivePinia } from "pinia"
import SampleCategory from "@modules/sample/vue/models/SampleCategory"
import SampleBookmark from "@modules/sample/vue/models/SampleBookmark"
import OxTranslator from "@/core/plugins/OxTranslator"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * SampleMovieCardExtended tests
 */
export default class SampleMovieCardExtendedTest extends OxTest {
    protected component = SampleMovieCardExtended

    private movie1 = new SampleMovie()
    private movie2 = new SampleMovie()
    private category = new SampleCategory()
    private bookmark = new SampleBookmark()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.movie1.id = "1"
        this.movie1.type = "sample_movie"
        this.movie1.attributes = {
            name: "Movie 1",
            release: "2002-01-01",
            duration: "01:55:00",
            csa: "18",
            languages: "fr"
        }
        this.movie1.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            }
        }
        this.movie1.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }

        this.movie2.id = "2"
        this.movie2.type = "sample_movie"
        this.movie2.attributes = {
            name: "Movie 2",
            release: "2004-01-01",
            duration: "01:30:00",
            csa: "16",
            languages: "fr"
        }
        this.movie2.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            },
            bookmarks: {
                data: {
                    type: "sample_bookmark",
                    id: "1"
                }
            }
        }
        this.movie2.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }

        this.category.id = "1"
        this.category.type = "sample_category"
        this.category.attributes = {
            name: "Category 1",
            color: null,
            active: true
        }

        this.bookmark.id = "1"
        this.bookmark.type = "sample_bookmark"
        this.bookmark.attributes = {
            datetime: "2023-02-10 16:03:04"
        }

        setActivePinia(pinia)
        storeObject(this.category)
        storeObject(this.bookmark)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testHaveIsBookmarkedClass () {
        const movieCardExt = this.vueComponent({ movie: this.movie2 })
        expect(this.privateCall(movieCardExt, "cardClasses")).toMatchObject({ isBookmarked: true })
    }

    public testNotHaveIsBookmarkedClass () {
        const movieCardExt = this.vueComponent({ movie: this.movie1 })
        expect(this.privateCall(movieCardExt, "cardClasses")).toMatchObject({ isBookmarked: false })
    }

    public testCsaIsShowed () {
        const movieCardExt = this.vueComponent({ movie: this.movie1 })
        expect(this.privateCall(movieCardExt, "showCSA")).toBe(true)
    }

    public testCsaIsNotShowed () {
        const movieCardExt = this.vueComponent({ movie: this.movie2 })
        expect(this.privateCall(movieCardExt, "showCSA")).toBe(false)
    }

    public testToggleBookmark (): void {
        const movieCardExt = this.mountComponent({ movie: this.movie1 })
        this.privateCall(movieCardExt.vm, "toggleBookmark")

        const events = movieCardExt.emitted("toggleBookmark")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [this.movie1]
        )
    }

    public testAskDelete (): void {
        const movieCardExt = this.mountComponent({ movie: this.movie1 })
        this.privateCall(movieCardExt.vm, "askDelete")

        const events = movieCardExt.emitted("delete")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [this.movie1]
        )
    }

    public testEdit (): void {
        const movieCardExt = this.mountComponent({ movie: this.movie1 })
        this.privateCall(movieCardExt.vm, "edit")

        const events = movieCardExt.emitted("edit")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [this.movie1]
        )
    }
}

(new SampleMovieCardExtendedTest()).launchTests()
