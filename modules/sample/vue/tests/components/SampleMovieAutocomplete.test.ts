/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import SampleMovieAutocomplete from "@modules/sample/vue/components/SampleMovieAutocomplete/SampleMovieAutocomplete.vue"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import { storeObject } from "@/core/utils/OxStorage"
import { setActivePinia } from "pinia"
import SampleCategory from "@modules/sample/vue/models/SampleCategory"

/* eslint-disable dot-notation */

/**
 * SampleMovieAutocomplete tests
 */
export default class SampleMovieAutocompleteTest extends OxTest {
    protected component = SampleMovieAutocomplete

    private movie = new SampleMovie()
    private category = new SampleCategory()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.movie.id = "1"
        this.movie.type = "sample_movie"
        this.movie.attributes = {
            name: "Movie 1",
            release: "2002-01-01",
            duration: "01:55:00",
            csa: "18",
            languages: "fr"
        }
        this.movie.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            }
        }
        this.movie.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }

        this.category.id = "1"
        this.category.type = "sample_category"
        this.category.attributes = {
            name: "Category 1",
            color: null,
            active: true
        }

        setActivePinia(pinia)
        storeObject(this.category)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testSampleMovieLineDefault () {
        const movieAutocomplete = this.vueComponent({ movie: this.movie })

        this.assertEqual(movieAutocomplete["movie"].title, "Movie 1")
    }
}

(new SampleMovieAutocompleteTest()).launchTests()
