/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import SampleMovieCard from "@modules/sample/vue/components/SampleMovieCard/SampleMovieCard.vue"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import { storeObject } from "@/core/utils/OxStorage"
import { setActivePinia } from "pinia"
import SampleCategory from "@modules/sample/vue/models/SampleCategory"
import SampleBookmark from "@modules/sample/vue/models/SampleBookmark"
import OxTranslator from "@/core/plugins/OxTranslator"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * SampleMovieCard tests
 */
export default class SampleMovieCardTest extends OxTest {
    protected component = SampleMovieCard

    private movie1 = new SampleMovie()
    private movie2 = new SampleMovie()
    private category = new SampleCategory()
    private bookmark = new SampleBookmark()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.movie1.id = "1"
        this.movie1.type = "sample_movie"
        this.movie1.attributes = {
            name: "Movie 1",
            release: "2002-01-01",
            duration: "01:55:00",
            csa: "18",
            languages: "fr"
        }
        this.movie1.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            }
        }
        this.movie1.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }
        this.movie1.meta = {
            rating_count: 5,
            rating_average: 3.6,
            rating_5: 2,
            rating_4: 1,
            rating_3: 0,
            rating_2: 2,
            rating_1: 0
        }

        this.movie2.id = "2"
        this.movie2.type = "sample_movie"
        this.movie2.attributes = {
            name: "Movie 2",
            release: "2004-01-01",
            duration: "01:30:00",
            csa: "16",
            languages: "fr"
        }
        this.movie2.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            },
            bookmarks: {
                data: {
                    type: "sample_bookmark",
                    id: "1"
                }
            }
        }
        this.movie2.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }

        this.category.id = "1"
        this.category.type = "sample_category"
        this.category.attributes = {
            name: "Category 1",
            color: null,
            active: true
        }

        this.bookmark.id = "1"
        this.bookmark.type = "sample_bookmark"
        this.bookmark.attributes = {
            datetime: "2023-02-10 16:03:04"
        }

        setActivePinia(pinia)
        storeObject(this.category)
        storeObject(this.bookmark)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testSampleMovieCardCreation () {
        const movieCard = this.vueComponent({ movie: this.movie1 })

        this.assertEqual(movieCard["movie"].title, "Movie 1")
    }

    public testHaveIsBookmarkedClass () {
        const movieCard = this.vueComponent({ movie: this.movie2 })
        this.assertEqual(this.privateCall(movieCard, "cardClasses"), { isBookmarked: true })
    }

    public testNotHaveIsBookmarkedClass () {
        const movieCard = this.vueComponent({ movie: this.movie1 })
        this.assertEqual(this.privateCall(movieCard, "cardClasses"), { isBookmarked: false })
    }

    public testCsaIsShowed () {
        const movieCard = this.vueComponent({ movie: this.movie1 })
        this.assertTrue(this.privateCall(movieCard, "showCSA"))
    }

    public testCsaIsNotShowed () {
        const movieCard = this.vueComponent({ movie: this.movie2 })
        this.assertFalse(this.privateCall(movieCard, "showCSA"))
    }

    public testHasRating () {
        const movieCard = this.vueComponent({ movie: this.movie1 })
        this.assertTrue(this.privateCall(movieCard, "hasRating"))
    }

    public testHasNoRating () {
        const movieCard = this.vueComponent({ movie: this.movie2 })
        this.assertFalse(this.privateCall(movieCard, "hasRating"))
    }

    public testMovieUrlWithDetails () {
        const movieCard = this.vueComponent({ movie: this.movie1 })
        this.assertEqual(
            this.privateCall(movieCard, "movieUrlWithDetails"),
            "self?relations=category&fieldsets=default%2Cdetails"
        )
    }

    public testToggleBookmark (): void {
        const movieCard = this.mountComponent({ movie: this.movie1 })
        this.privateCall(movieCard.vm, "toggleBookmark")

        const events = movieCard.emitted("toggleBookmark")
        this.assertHaveLength(events, 1)
    }
}

(new SampleMovieCardTest()).launchTests()
