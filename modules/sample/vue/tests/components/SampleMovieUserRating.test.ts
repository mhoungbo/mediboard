/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import SampleMovieUserRating from "@modules/sample/vue/components/SampleMovieUserRating/SampleMovieUserRating.vue"
import OxTranslator from "@/core/plugins/OxTranslator"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

const mountComponent = (props?: object) => {
    return shallowMount(SampleMovieUserRating, {
        propsData: { ...props },
        localVue
    })
}

describe("SampleMovieUserRating",
    () => {
        beforeAll(() => {
            jest.useFakeTimers()
        })

        afterEach(() => {
            jest.resetAllMocks()
        })

        test.each([
            { ratingState: "button" },
            { ratingState: "modify" },
            { ratingState: "rated" }
        ])(
            "testContainerClass has $ratingState class",
            ({ ratingState }) => {
                const userRating = mountComponent().vm
                userRating.$data.ratingState = ratingState as "button" | "modify" | "rated"

                expect(userRating["containerClass"]).toEqual({
                    "SampleMovieUserRating-container": true,
                    [ratingState]: true
                })
            }
        )

        test("iconStyle should be star",
            () => {
                const userRating = mountComponent().vm
                userRating.$data.ratingState = "rated"

                expect(userRating["iconStyle"]).toEqual("star")
            }
        )

        test("iconStyle should be starOutline",
            () => {
                const userRating = mountComponent().vm
                userRating.$data.ratingState = "button"

                expect(userRating["iconStyle"]).toEqual("starOutline")
            }
        )

        test("displayButton should be true",
            () => {
                const userRating = mountComponent().vm

                expect(userRating["displayButton"]).toBeTruthy()
            }
        )

        test("displayButton should be false when a value is set",
            () => {
                const userRating = mountComponent({ value: 3 }).vm

                expect(userRating["displayButton"]).toBeFalsy()
            }
        )

        test("displayButton should be false",
            () => {
                const userRating = mountComponent().vm
                userRating.$data.ratingState = "modify"

                expect(userRating["displayButton"]).toBeFalsy()
            }
        )

        test("displayModify should be true when ratingState is modify",
            () => {
                const userRating = mountComponent().vm
                userRating.$data.ratingState = "modify"

                expect(userRating["displayModify"]).toBeTruthy()
            }
        )

        test("displayModify should be true when ratingState is rated but there is no value",
            () => {
                const userRating = mountComponent().vm
                userRating.$data.ratingState = "rated"

                expect(userRating["displayModify"]).toBeTruthy()
            }
        )

        test("displayModify should be false when ratingState is rated and there is a value",
            () => {
                const userRating = mountComponent({ value: 3 }).vm
                userRating.$data.ratingState = "rated"

                expect(userRating["displayModify"]).toBeFalsy()
            }
        )

        test("displayModify should be false when ratingState is button",
            () => {
                const userRating = mountComponent().vm

                expect(userRating["displayModify"]).toBeFalsy()
            }
        )

        test("displayRated should be true when ratingState is rated and value is set",
            () => {
                const userRating = mountComponent({ value: 3 }).vm
                userRating.$data.ratingState = "rated"

                expect(userRating["displayRated"]).toBeTruthy()
            }
        )

        test("displayRated should be false when ratingState is button",
            () => {
                const userRating = mountComponent().vm

                expect(userRating["displayRated"]).toBeFalsy()
            }
        )

        test("displayGroup should be true when ratingState isn't button",
            () => {
                const userRating = mountComponent().vm
                userRating.$data.ratingState = "rated"

                expect(userRating["displayGroup"]).toBeTruthy()
            }
        )

        test("displayGroup should be true when value is set",
            () => {
                const userRating = mountComponent({ value: 3 }).vm

                expect(userRating["displayGroup"]).toBeTruthy()
            }
        )

        test("updateRating should change the ratingState to rated and emit input",
            () => {
                const userRating = mountComponent()
                userRating.vm["updateRating"](4)
                jest.runOnlyPendingTimers()

                expect(userRating.vm.$data.ratingState).toEqual("rated")
                expect(userRating.emitted().input).toEqual([[4]])
            }
        )

        test("enableRating should change the ratingState to modify",
            () => {
                const userRating = mountComponent().vm
                userRating.$data.ratingState = "button"
                userRating["enableRating"]()

                expect(userRating.$data.ratingState).toEqual("modify")
            }
        )

        test("v-rating should transition from create when value is undefined",
            () => {
                const userRating = mountComponent().vm

                expect(userRating["transitionName"]).toEqual("SampleMovieUserRating-vratingTransitionCreate")
            }
        )

        test("v-rating should transition from modify when value is defined",
            () => {
                const userRating = mountComponent({ value: 3 }).vm

                expect(userRating["transitionName"]).toEqual("SampleMovieUserRating-vratingTransitionModify")
            }
        )
    }
)
