/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import SampleRatingTab from "@modules/sample/vue/components/SampleRatingTab/SampleRatingTab.vue"
import * as OxApiManager from "@/core/utils/OxApiManager"
import OxCollection from "@/core/models/OxCollection"
import SampleRating from "@modules/sample/vue/models/SampleRating"
import { flushPromises } from "@oxify/utils/OxTest"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

const mountComponent = (props?: object) => {
    return shallowMount(SampleRatingTab, {
        propsData: { link: "ratings", ...props },
        localVue
    })
}

describe("SampleRatingTab",
    () => {
        afterEach(() => {
            jest.resetAllMocks()
        })

        test("isEmpty should be true",
            async () => {
                const collection = new OxCollection()
                collection.meta = {
                    count: 0,
                    total: 0
                }

                jest.spyOn(OxApiManager, "getCollectionFromJsonApiRequest")
                    .mockImplementation(() => Promise.resolve(collection))

                const ratingTab = mountComponent({ rating: 4 }).vm
                await flushPromises()

                expect(ratingTab["isEmpty"]).toBeTruthy()
            }
        )

        test("isEmpty should be false",
            async () => {
                const collection = new OxCollection()
                collection.meta = {
                    count: 10,
                    total: 18
                }

                jest.spyOn(OxApiManager, "getCollectionFromJsonApiRequest")
                    .mockImplementation(() => Promise.resolve(collection))

                const ratingTab = mountComponent({ rating: 4 }).vm
                await flushPromises()

                expect(ratingTab["isEmpty"]).toBeFalsy()
            }
        )
    }
)
