/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import SampleDirectorLine from "@modules/sample/vue/components/SampleDirectorLine/SampleDirectorLine.vue"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import SamplePerson from "@modules/sample/vue/models/SamplePerson"

/* eslint-disable dot-notation */

/**
 * Test pour SampleDirectorLine
 */
export default class SampleDirectorLineTest extends OxTest {
    protected component = SampleDirectorLine

    private director = new SamplePerson()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.director.id = "1"
        this.director.type = "sample_person"
        this.director.attributes = {
            last_name: "Doe",
            first_name: "John",
            is_director: true,
            birthdate: "1990-02-01",
            sex: "m",
            activity_start: "2002-02-01"
        }
        this.director.relationships = {
            nationality: {
                data: {
                    type: "sample_nationality",
                    id: "1"
                }
            }
        }
        this.director.links = {
            self: "self",
            schema: "schema",
            history: "history",
            profile_picture: "profilepic"
        }
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testSampleDirectorLineDefault () {
        const directorLine = this.vueComponent({ director: this.director })

        this.assertEqual(directorLine["director"].fullName, "John Doe")
    }
}

(new SampleDirectorLineTest()).launchTests()
