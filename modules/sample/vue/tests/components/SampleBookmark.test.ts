/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import SampleBookmarkModel from "@modules/sample/vue/models/SampleBookmark"
import SampleBookmark from "@modules/sample/vue/components/SampleBookmark/SampleBookmark.vue"
import pinia from "@/core/plugins/OxPiniaCore"
import { setActivePinia } from "pinia"
import OxTranslator from "@/core/plugins/OxTranslator"
import { storeObject } from "@/core/utils/OxStorage"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * SampleBookmark tests
 */
export default class SampleBookmarkTest extends OxTest {
    protected component = SampleBookmark

    private movie = new SampleMovie()
    private movie2 = new SampleMovie()
    private bookmark = new SampleBookmarkModel()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.movie.id = "1"
        this.movie.type = "sample_movie"
        this.movie.attributes = {
            name: "Movie 1",
            release: "2002-01-01",
            duration: "01:55:00",
            csa: "18",
            languages: "fr"
        }
        this.movie.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            },
            bookmarks: {
                data: {
                    type: "sample_bookmark",
                    id: "1"
                }
            }
        }
        this.movie.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }

        this.movie2.id = "2"
        this.movie2.type = "sample_movie"
        this.movie2.attributes = {
            name: "Movie 2",
            release: "2004-01-01",
            duration: "01:30:00",
            csa: "16",
            languages: "fr"
        }
        this.movie2.relationships = {
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            },
            bookmarks: {
                data: null
            }
        }
        this.movie2.links = {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details"
        }

        this.bookmark.id = "1"
        this.bookmark.type = "sample_bookmark"
        this.bookmark.attributes = {
            datetime: "2023-02-10 16:03:04"
        }

        setActivePinia(pinia)
        storeObject(this.bookmark)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testAddBookmarkLabel () {
        const bookmark = this.vueComponent({ movie: this.movie })
        this.assertEqual(this.privateCall(bookmark, "bookmarkLabel"), "CSampleBookmark-title-delete")
    }

    public testRemoveBookmarkLabel () {
        const bookmark = this.vueComponent({ movie: this.movie2 })
        this.assertEqual(this.privateCall(bookmark, "bookmarkLabel"), "CSampleBookmark-title-create")
    }
}

(new SampleBookmarkTest()).launchTests()
