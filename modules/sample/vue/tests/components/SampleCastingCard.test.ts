/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import { setActivePinia } from "pinia"
import SampleCastingCard from "@modules/sample/vue/components/SampleCastingCard/SampleCastingCard.vue"
import SampleCasting from "@modules/sample/vue/models/SampleCasting"
import SamplePerson from "@modules/sample/vue/models/SamplePerson"
import OxTranslator from "@/core/plugins/OxTranslator"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * SampleCastingCard tests
 */
export default class SampleCastingCardTest extends OxTest {
    protected component = SampleCastingCard

    private casting1 = new SampleCasting()
    private casting2 = new SampleCasting()
    private actor1 = new SamplePerson()
    private actor2 = new SamplePerson()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.casting1.id = "1"
        this.casting1.type = "sample_casting"
        this.casting1.attributes = {
            is_main_actor: true
        }
        this.casting1.links = {
            self: "self",
            schema: "schema",
            history: "history"
        }

        this.casting2.id = "2"
        this.casting2.type = "sample_casting"
        this.casting2.attributes = {
            is_main_actor: false
        }
        this.casting2.links = {
            self: "self",
            schema: "schema",
            history: "history"
        }

        setActivePinia(pinia)

        this.actor1.id = "1"
        this.casting1.actor = this.actor1
        this.actor2.id = "2"
        this.casting2.actor = this.actor2
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testHaveIsMainClass () {
        const castingCard = this.vueComponent({ casting: this.casting1 })
        this.assertEqual(this.privateCall(castingCard, "cardClasses"), { isMain: true })
    }

    public testNotHaveIsMainClass () {
        const castingCard = this.vueComponent({ casting: this.casting2 })
        this.assertEqual(this.privateCall(castingCard, "cardClasses"), { isMain: false })
    }

    public testIsMainActorLabel () {
        const castingCard = this.vueComponent({ casting: this.casting1 })
        this.assertEqual(this.privateCall(castingCard, "mainActorLabel"), "CSampleCasting-is_main_actor")
    }

    public testIsNotMainActorLabel () {
        const castingCard = this.vueComponent({ casting: this.casting2 })
        this.assertEqual(this.privateCall(castingCard, "mainActorLabel"), "CSampleCasting-set-main-actor")
    }

    public testChangeMainActor () {
        const castingCard = this.mountComponent({ casting: this.casting1 })
        this.privateCall(castingCard.vm, "changeMainActor")

        const events = castingCard.emitted("changeMainActor")
        this.assertHaveLength(events, 1)

        const lastEventParam1 = Array.isArray(events) ? events[0][0] : false
        const lastEventParam2 = Array.isArray(events) ? events[0][1] : false
        this.assertEqual(lastEventParam1, this.casting1)
        this.assertEqual(lastEventParam2, false)
    }

    public testRemoveCasting (): void {
        const castingCard = this.mountComponent({ casting: this.casting1 })
        this.privateCall(castingCard.vm, "removeCasting")

        const events = castingCard.emitted("removeCasting")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [this.casting1]
        )
    }
}

(new SampleCastingCardTest()).launchTests()
