/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import SamplePersonAutocomplete
    from "@modules/sample/vue/components/SamplePersonAutocomplete/SamplePersonAutocomplete.vue"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import SamplePerson from "@modules/sample/vue/models/SamplePerson"

/* eslint-disable dot-notation */

/**
 * Test pour SamplePersonAutocomplete
 */
export default class SamplePersonAutocompleteTest extends OxTest {
    protected component = SamplePersonAutocomplete

    private person = new SamplePerson()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.person.id = "1"
        this.person.type = "sample_person"
        this.person.attributes = {
            last_name: "Doe",
            first_name: "John",
            is_director: true,
            birthdate: "1990-02-01",
            sex: "m",
            activity_start: "2002-02-01"
        }
        this.person.relationships = {
            nationality: {
                data: {
                    type: "sample_nationality",
                    id: "1"
                }
            }
        }
        this.person.links = {
            self: "self",
            schema: "schema",
            history: "history",
            profile_picture: "profilepic"
        }
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testSamplePersonAutocompleteDefault () {
        const personAutocomplete = this.vueComponent({ person: this.person })

        this.assertEqual(personAutocomplete["person"].fullName, "John Doe")
    }
}

(new SamplePersonAutocompleteTest()).launchTests()
