/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import SampleUserCard from "@modules/sample/vue/components/SampleUserCard/SampleUserCard.vue"
import OxThemeCore from "@oxify/utils/OxThemeCore"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import { useUserStore } from "@/core/stores/user"
import { clone } from "lodash"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

const mountComponent = (props?: object) => {
    return shallowMount(SampleUserCard, {
        propsData: { ...props },
        localVue
    })
}

describe("SampleUserCard",
    () => {
        beforeAll(() => {
            setActivePinia(pinia)
        })

        test("userColor should be by default gray 100",
            () => {
                const userCard = mountComponent({ user: new Mediuser() }).vm

                expect(userCard["userColor"]).toEqual(OxThemeCore.grey100)
            }
        )

        test("userColor should be the color from the user",
            () => {
                const user = new Mediuser()
                user.specificColor = "88FF12"

                const userCard = mountComponent({ user }).vm

                expect(userCard["userColor"]).toEqual("#88FF12")
            }
        )

        test("isCurrentUser should be true",
            () => {
                const userStore = useUserStore()
                const user = new Mediuser()
                user.id = "1"

                userStore.user = user
                const userCard = mountComponent({ user }).vm

                expect(userCard["isCurrentUser"]).toBeTruthy()
            }
        )

        test("isCurrentUser should be false",
            () => {
                const userStore = useUserStore()
                const user = new Mediuser()
                user.id = "1"

                userStore.user = clone(user)
                user.id = "2"
                const userCard = mountComponent({ user }).vm

                expect(userCard["isCurrentUser"]).toBeFalsy()
            }
        )
    }
)
