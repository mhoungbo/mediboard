/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import SampleMovieGlobalRating from "@modules/sample/vue/components/SampleMovieGlobalRating/SampleMovieGlobalRating.vue"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import OxCollection from "@/core/models/OxCollection"
import * as OxApiManager from "@/core/utils/OxApiManager"
import SampleRating from "@modules/sample/vue/models/SampleRating"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

const movieMock = new SampleMovie()
movieMock.id = "1"
movieMock.title = "Movie 1"
movieMock.releaseData = "2002-01-01"
movieMock.duration = "01:55:00"
movieMock.csa = "10"
movieMock.languagesData = "fr"
movieMock.description = "Movie 1 description."
movieMock.meta = {
    rating_average: 3.7,
    rating_count: 68,
    rating_1: 8,
    rating_2: 0,
    rating_3: 15,
    rating_4: 28,
    rating_5: 17
}
movieMock.links = {
    ratings: "ratings"
}

const mountComponent = (props?: object) => {
    return shallowMount(SampleMovieGlobalRating, {
        propsData: { ...props },
        localVue
    })
}

describe("SampleMovieGlobalRating",
    () => {
        afterEach(() => {
            jest.resetAllMocks()
        })

        test("hasRatings should be true if rating_count > 0",
            () => {
                const globalRating = mountComponent({ movie: movieMock }).vm

                expect(globalRating["hasRatings"]).toBeTruthy()
            }
        )

        test("hasRatings should be false if rating_count = 0",
            () => {
                const m = new SampleMovie()
                m.id = "1"
                m.meta = {
                    rating_average: 0,
                    rating_count: 0,
                    rating_1: 0,
                    rating_2: 0,
                    rating_3: 0,
                    rating_4: 0,
                    rating_5: 0
                }

                const globalRating = mountComponent({ movie: m }).vm

                expect(globalRating["hasRatings"]).toBeFalsy()
            }
        )

        test("hasRatings should be false if rating_count is undefined",
            () => {
                const globalRating = mountComponent({ movie: new SampleMovie() }).vm

                expect(globalRating["hasRatings"]).toBeFalsy()
            }
        )

        test("openDialog should change showDialog to true",
            async () => {
                const globalRating = mountComponent({ movie: movieMock }).vm

                globalRating.$data.showDialog = false
                globalRating["openDialog"]()

                expect(globalRating.$data.showDialog).toBeTruthy()
            }
        )

        test("hideDialog should change showDialog to false",
            async () => {
                const globalRating = mountComponent({ movie: movieMock }).vm

                globalRating.$data.showDialog = true
                globalRating["hideDialog"]()

                expect(globalRating.$data.showDialog).toBeFalsy()
            }
        )
    }
)
