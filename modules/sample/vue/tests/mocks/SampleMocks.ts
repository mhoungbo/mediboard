/**
 * Mock for getSchemas request from a sample_movie
 */
export const movieSchemaMock = {
    data: [
        {
            type: "schema",
            id: "429b23f96004afaa4ee3b31e7ca34c0e",
            attributes: {
                owner: "sample_movie",
                field: "name",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Titre",
                label: "Titre",
                description: "Titre du film"
            }
        },
        {
            type: "schema",
            id: "0a9092e87e8ddb890799273a6c8e8596",
            attributes: {
                owner: "sample_movie",
                field: "release",
                type: "date",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Date de sortie",
                label: "Sortie",
                description: "Date de sortie du film"
            }
        },
        {
            type: "schema",
            id: "efd8ccabedcdca0b495941110eb85d63",
            attributes: {
                owner: "sample_movie",
                field: "duration",
                type: "time",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Dur�e",
                label: "Dur�e",
                description: "Dur�e du film"
            }
        },
        {
            type: "schema",
            id: "19795aefbd316133ad109635c8dad0ae",
            attributes: {
                owner: "sample_movie",
                field: "csa",
                type: "enum",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                values: [
                    "10",
                    "12",
                    "16",
                    "18"
                ],
                translations: {
                    10: "-10",
                    12: "-12",
                    16: "-16",
                    18: "-18"
                },
                libelle: "Csa",
                label: "Signal�tique jeunesse",
                description: "Signal�tique jeunesse du film"
            }
        },
        {
            type: "schema",
            id: "99457059e40536032606aff4a4d8ff33",
            attributes: {
                owner: "sample_movie",
                field: "languages",
                type: "set",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: "fr",
                values: [
                    "en",
                    "es",
                    "fr",
                    "ger",
                    "it"
                ],
                translations: {
                    en: "Anglais",
                    es: "Espagn�l",
                    fr: "Fran�ais",
                    ger: "Allemand",
                    it: "Italien"
                },
                libelle: "Langues",
                label: "Langues",
                description: "Langues disponibles"
            }
        },
        {
            type: "schema",
            id: "847f7aa830e06013fa44298843f20aae",
            attributes: {
                owner: "sample_movie",
                field: "description",
                type: "text",
                fieldset: "details",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                helped: true,
                markdown: true,
                libelle: "Synopsis",
                label: "Synopsis",
                description: "Synopsis du film"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 6
    }
}

/**
 * Mock for getSchemas request from a sample_person
 */
export const personSchemaMock = {
    data: [
        {
            type: "schema",
            id: "429b23f96004afaa4ee3b31e7ca34c0e",
            attributes: {
                owner: "sample_person",
                field: "last_name",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Nom",
                label: "Nom",
                description: "Nom de famille"
            }
        },
        {
            type: "schema",
            id: "0a9092e87e8ddb890799273a6c8e8596",
            attributes: {
                owner: "sample_person",
                field: "first_name",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Pr�nom",
                label: "Pr�nom",
                description: "Pr�nom"
            }
        },
        {
            type: "schema",
            id: "efd8ccabedcdca0b495941110eb85d63",
            attributes: {
                owner: "sample_person",
                field: "is_director",
                type: "bool",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: "0",
                libelle: "R�alisateur",
                label: "R�alisateur",
                description: "Est un r�alisateur"
            }
        },
        {
            type: "schema",
            id: "19795aefbd316133ad109635c8dad0ae",
            attributes: {
                owner: "sample_person",
                field: "birthdate",
                type: "birthDate",
                fieldset: "extra",
                autocomplete: null,
                placeholder: "99/99/9999",
                notNull: null,
                confidential: null,
                default: null,
                libelle: "Date de naissance",
                label: "Naissance",
                description: "Date de naissance"
            }
        },
        {
            type: "schema",
            id: "99457059e40536032606aff4a4d8ff33",
            attributes: {
                owner: "sample_person",
                field: "sex",
                type: "enum",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                values: [
                    "m",
                    "f"
                ],
                translations: {
                    m: "Masculin",
                    f: "F�minin"
                },
                libelle: "Sexe",
                label: "Sexe",
                description: "Sexe"
            }
        },
        {
            type: "schema",
            id: "847f7aa830e06013fa44298843f20aae",
            attributes: {
                owner: "sample_person",
                field: "activity_start",
                type: "date",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                libelle: "D�but d'activit�",
                label: "D�but d'activit�",
                description: "Date de d�but d'activit�"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 6
    }
}

/**
 * Mock for sample_movie collection with category & bookmarks relations
 */
export const moviesMock = {
    data: [
        {
            type: "sample_movie",
            id: "1",
            attributes: {
                name: "Movie 1",
                release: "2002-01-01",
                duration: "01:55:00",
                csa: "18",
                languages: "fr",
                bookmarked_date: "2023-02-13 15:40:40"
            },
            relationships: {
                category: {
                    data: {
                        type: "sample_category",
                        id: "1"
                    }
                },
                bookmarks: {
                    data: {
                        type: "sample_bookmark",
                        id: "1"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                cover: "cover",
                details: "details",
                bookmark: "bookmark"
            }
        },
        {
            type: "sample_movie",
            id: "2",
            attributes: {
                name: "Movie 2",
                release: "2002-01-01",
                duration: "01:55:00",
                csa: "18",
                languages: "fr",
                bookmarked_date: "2023-02-13 15:40:42"
            },
            relationships: {
                category: {
                    data: {
                        type: "sample_category",
                        id: "1"
                    }
                },
                bookmarks: {
                    data: {
                        type: "sample_bookmark",
                        id: "2"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                cover: "cover",
                details: "details",
                bookmark: "bookmark"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2,
        total: 2
    },
    links: {
        self: "self",
        first: "first",
        last: "last"
    },
    included: [
        {
            type: "sample_category",
            id: "1",
            attributes: {
                name: "Category 1",
                color: null,
                active: true
            }
        },
        {
            type: "sample_bookmark",
            id: "1",
            attributes: {
                datetime: "2023-02-01 15:40:40"
            },
            links: {
                self: "bookmark-self-1"
            }
        },
        {
            type: "sample_bookmark",
            id: "2",
            attributes: {
                datetime: "2023-02-01 15:40:42"
            },
            links: {
                self: "bookmark-self-2"
            }
        }
    ]
}

/**
 * Mock for empty sample_movie collection
 */
export const moviesEmptyMock = {
    data: [],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 0,
        total: 0
    },
    links: {
        self: "self",
        first: "first",
        last: "last"
    }
}

/**
 * Mock for a sample_movie
 *   - with schema
 *   - with relations : actors, director, category & bookmarks
 */
export const movieMock = {
    data: {
        type: "sample_movie",
        id: "1",
        attributes: {
            name: "Movie 1",
            release: "2002-01-01",
            duration: "01:55:00",
            csa: "18",
            languages: "fr",
            description: "Movie 1 description."
        },
        relationships: {
            actors: {
                data: [
                    {
                        type: "sample_person",
                        id: "1"
                    },
                    {
                        type: "sample_person",
                        id: "2"
                    }
                ]
            },
            director: {
                data: {
                    type: "sample_person",
                    id: "1"
                }
            },
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            },
            bookmarks: {
                data: {
                    type: "sample_bookmark",
                    id: "1"
                }
            }
        },
        links: {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details",
            casting: "casting"
        },
        meta: {
            schema: [
                {
                    id: "429b23f96004afaa4ee3b31e7ca34c0e",
                    owner: "sample_movie",
                    field: "name",
                    type: "str",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: true,
                    confidential: null,
                    default: null,
                    libelle: "Titre",
                    label: "Titre",
                    description: "Titre du film"
                },
                {
                    id: "0a9092e87e8ddb890799273a6c8e8596",
                    owner: "sample_movie",
                    field: "release",
                    type: "date",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: true,
                    confidential: null,
                    default: null,
                    libelle: "Date de sortie",
                    label: "Sortie",
                    description: "Date de sortie du film"
                },
                {
                    id: "efd8ccabedcdca0b495941110eb85d63",
                    owner: "sample_movie",
                    field: "duration",
                    type: "time",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: true,
                    confidential: null,
                    default: null,
                    libelle: "Dur�e",
                    label: "Dur�e",
                    description: "Dur�e du film"
                },
                {
                    id: "19795aefbd316133ad109635c8dad0ae",
                    owner: "sample_movie",
                    field: "csa",
                    type: "enum",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: null,
                    confidential: null,
                    default: null,
                    values: [
                        "10",
                        "12",
                        "16",
                        "18"
                    ],
                    translations: {
                        10: "-10",
                        12: "-12",
                        16: "-16",
                        18: "-18"
                    },
                    libelle: "Csa",
                    label: "Signal�tique jeunesse",
                    description: "Signal�tique jeunesse du film"
                },
                {
                    id: "99457059e40536032606aff4a4d8ff33",
                    owner: "sample_movie",
                    field: "languages",
                    type: "set",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: null,
                    confidential: null,
                    default: "fr",
                    values: [
                        "en",
                        "es",
                        "fr",
                        "ger",
                        "it"
                    ],
                    translations: {
                        en: "Anglais",
                        es: "Espagn�l",
                        fr: "Fran�ais",
                        ger: "Allemand",
                        it: "Italien"
                    },
                    libelle: "Langues",
                    label: "Langues",
                    description: "Langues disponibles"
                },
                {
                    id: "847f7aa830e06013fa44298843f20aae",
                    owner: "sample_movie",
                    field: "description",
                    type: "text",
                    fieldset: "details",
                    autocomplete: null,
                    placeholder: null,
                    notNull: null,
                    confidential: null,
                    default: null,
                    libelle: "Synopsis",
                    label: "Synopsis",
                    description: "Synopsis du film"
                }
            ]
        }
    },
    meta: {
        date: "2022-09-15 10:43:37+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com"
    },
    included: [
        {
            type: "sample_person",
            id: "1",
            attributes: {
                last_name: "Doe",
                first_name: "John",
                is_director: true
            },
            links: {
                profile_picture: "profilepic"
            }
        },
        {
            type: "sample_person",
            id: "2",
            attributes: {
                last_name: "Williams",
                first_name: "Bob",
                is_director: false
            },
            links: {
                profile_picture: "profilepic"
            }
        },
        {
            type: "sample_category",
            id: "1",
            attributes: {
                name: "Category 1",
                color: null,
                active: true
            }
        },
        {
            type: "sample_bookmark",
            id: "1",
            attributes: {
                datetime: "2023-02-13 15:40:40"
            },
            links: {
                self: "bookmark-self"
            }
        }
    ]
}

/**
 * Mock for a sample_movie :
 *   - with schema
 *   - without actors
 *   - without director
 *   - with category
 *   - without bookmarks
 *   - with relations : actors, director, category & bookmarks
 */
export const movieEmptyMock = {
    data: {
        type: "sample_movie",
        id: "1",
        attributes: {
            name: "Movie 1",
            release: "2002-01-01",
            duration: "01:55:00",
            csa: "18",
            languages: "fr",
            description: "Movie 1 description."
        },
        relationships: {
            actors: {
                data: []
            },
            director: {
                data: null
            },
            category: {
                data: {
                    type: "sample_category",
                    id: "1"
                }
            },
            bookmarks: {
                data: null
            }
        },
        links: {
            self: "self",
            schema: "schema",
            history: "history",
            cover: "cover",
            details: "details",
            casting: "casting"
        },
        meta: {
            schema: [
                {
                    id: "429b23f96004afaa4ee3b31e7ca34c0e",
                    owner: "sample_movie",
                    field: "name",
                    type: "str",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: true,
                    confidential: null,
                    default: null,
                    libelle: "Titre",
                    label: "Titre",
                    description: "Titre du film"
                },
                {
                    id: "0a9092e87e8ddb890799273a6c8e8596",
                    owner: "sample_movie",
                    field: "release",
                    type: "date",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: true,
                    confidential: null,
                    default: null,
                    libelle: "Date de sortie",
                    label: "Sortie",
                    description: "Date de sortie du film"
                },
                {
                    id: "efd8ccabedcdca0b495941110eb85d63",
                    owner: "sample_movie",
                    field: "duration",
                    type: "time",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: true,
                    confidential: null,
                    default: null,
                    libelle: "Dur�e",
                    label: "Dur�e",
                    description: "Dur�e du film"
                },
                {
                    id: "19795aefbd316133ad109635c8dad0ae",
                    owner: "sample_movie",
                    field: "csa",
                    type: "enum",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: null,
                    confidential: null,
                    default: null,
                    values: [
                        "10",
                        "12",
                        "16",
                        "18"
                    ],
                    translations: {
                        10: "-10",
                        12: "-12",
                        16: "-16",
                        18: "-18"
                    },
                    libelle: "Csa",
                    label: "Signal�tique jeunesse",
                    description: "Signal�tique jeunesse du film"
                },
                {
                    id: "99457059e40536032606aff4a4d8ff33",
                    owner: "sample_movie",
                    field: "languages",
                    type: "set",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: null,
                    confidential: null,
                    default: "fr",
                    values: [
                        "en",
                        "es",
                        "fr",
                        "ger",
                        "it"
                    ],
                    translations: {
                        en: "Anglais",
                        es: "Espagn�l",
                        fr: "Fran�ais",
                        ger: "Allemand",
                        it: "Italien"
                    },
                    libelle: "Langues",
                    label: "Langues",
                    description: "Langues disponibles"
                },
                {
                    id: "847f7aa830e06013fa44298843f20aae",
                    owner: "sample_movie",
                    field: "description",
                    type: "text",
                    fieldset: "details",
                    autocomplete: null,
                    placeholder: null,
                    notNull: null,
                    confidential: null,
                    default: null,
                    libelle: "Synopsis",
                    label: "Synopsis",
                    description: "Synopsis du film"
                }
            ]
        }
    },
    meta: {
        date: "2022-09-15 10:43:37+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com"
    },
    included: [
        {
            type: "sample_category",
            id: "1",
            attributes: {
                name: "Category 1",
                color: null,
                active: true
            }
        }
    ]
}

/**
 * Mock for sample_casting with actor relation
 */
export const castingMock = {
    data: [
        {
            type: "sample_casting",
            id: "1",
            attributes: {
                is_main_actor: false
            },
            relationships: {
                actor: {
                    data: {
                        type: "sample_person",
                        id: "1"
                    }
                }
            },
            links: {
                schema: "schema"
            }
        },
        {
            type: "sample_casting",
            id: "2",
            attributes: {
                is_main_actor: true
            },
            relationships: {
                actor: {
                    data: {
                        type: "sample_person",
                        id: "2"
                    }
                }
            },
            links: {
                schema: "schema"
            }
        }
    ],
    meta: {
        date: "2022-09-15 11:39:14+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2,
        total: 2
    },
    links: {
        self: "self",
        first: "first",
        last: "last"
    },
    included: [
        {
            type: "sample_person",
            id: "1",
            attributes: {
                last_name: "Doe",
                first_name: "John",
                is_director: true
            },
            links: {
                profile_picture: "profilepic"
            }
        },
        {
            type: "sample_person",
            id: "2",
            attributes: {
                last_name: "Williams",
                first_name: "Bob",
                is_director: false
            },
            links: {
                profile_picture: "profilepic"
            }
        }
    ]
}

/**
 * Mock for an empty sample_casting
 */
export const castingEmptyMock = {
    data: [],
    meta: {
        date: "2022-09-15 11:39:14+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 0,
        total: 0
    },
    links: {
        self: "self",
        first: "first",
        last: "last"
    }
}

/**
 * Mock for a sample_person collection with nationality relation
 */
export const personsMock = {
    data: [
        {
            type: "sample_person",
            id: "1",
            attributes: {
                last_name: "Doe",
                first_name: "John",
                is_director: true,
                birthdate: "1990-02-01",
                sex: "m",
                activity_start: "2002-02-01"
            },
            relationships: {
                nationality: {
                    data: {
                        type: "sample_nationality",
                        id: "1"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                profile_picture: "profilepic"
            }
        },
        {
            type: "sample_person",
            id: "2",
            attributes: {
                last_name: "Williams",
                first_name: "Bob",
                is_director: false,
                birthdate: "1990-03-01",
                sex: "m",
                activity_start: "2003-02-01"
            },
            relationships: {
                nationality: {
                    data: {
                        type: "sample_nationality",
                        id: "2"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                profile_picture: "profilepic"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2,
        total: 2
    },
    links: {
        self: "self",
        next: "next",
        first: "first",
        last: "last"
    },
    included: [
        {
            type: "sample_nationality",
            id: "1",
            attributes: {
                name: "Nationality 1",
                code: "N1",
                flag: null
            }
        },
        {
            type: "sample_nationality",
            id: "2",
            attributes: {
                name: "Nationality 2",
                code: "N2",
                flag: null
            }
        }
    ]
}

/**
 * Mock for a sample_person with nationality relation
 */
export const personMock = {
    data: {
        type: "sample_person",
        id: "2",
        attributes: {
            last_name: "Williams",
            first_name: "Bob",
            is_director: true,
            birthdate: "1990-03-01",
            sex: "m",
            activity_start: "2003-02-01"
        },
        relationships: {
            nationality: {
                data: {
                    type: "sample_nationality",
                    id: "2"
                }
            }
        },
        links: {
            self: "self",
            schema: "schema",
            history: "history",
            profile_picture: "profilepic"
        }
    },
    meta: {
        date: "2022-09-15 10:43:37+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com"
    },
    included: [
        {
            type: "sample_nationality",
            id: "2",
            attributes: {
                name: "Nationality 2",
                code: "N2",
                flag: null
            }
        }
    ]
}
