import SampleBookmark from "@modules/sample/vue/models/SampleBookmark"
import { createJsonApiObjects, deleteJsonApiObject } from "@/core/utils/OxApiManager"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import { addInfo } from "@/core/utils/OxNotifyManager"
import { tr } from "@/core/utils/OxTranslator"

export async function toggleBookmark (movie: SampleMovie, bookmarkUrl?: string): Promise<void> {
    let notifMsgKey = ""

    if (!movie.bookmark && bookmarkUrl) {
        const bookmark = new SampleBookmark()
        // Only set the movie cause user and datetime automatically set by backend
        bookmark.movie = movie

        const newBookmark = await createJsonApiObjects(bookmark, bookmarkUrl)
        movie.bookmark = newBookmark
        notifMsgKey = "CSampleBookmark-msg-create"
    }
    else if (movie.bookmark) {
        await deleteJsonApiObject(movie.bookmark)
        movie.bookmark = undefined
        notifMsgKey = "CSampleBookmark-msg-delete"
    }

    addInfo(tr(notifMsgKey))
}
