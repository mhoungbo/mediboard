import SampleMovieList from "@modules/sample/vue/views/SampleMovieList/SampleMovieList.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("SampleMovieList", SampleMovieList)
