import SampleBookmarks from "@modules/sample/vue/views/SampleBookmarks/SampleBookmarks.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("SampleBookmarks", SampleBookmarks)
