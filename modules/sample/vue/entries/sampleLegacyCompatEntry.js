import SampleLegacyCompat from "../views/SampleLegacyCompat/SampleLegacyCompat"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("legacy_compat", SampleLegacyCompat)
