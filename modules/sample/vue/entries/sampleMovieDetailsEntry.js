import SampleMovieDetails from "@modules/sample/vue/views/SampleMovieDetails/SampleMovieDetails.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("SampleMovieDetails", SampleMovieDetails)
