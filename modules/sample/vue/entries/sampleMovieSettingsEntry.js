import SampleMovieSettings from "@modules/sample/vue/views/SampleMovieSettings/SampleMovieSettings.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("SampleMovieSettings", SampleMovieSettings)
