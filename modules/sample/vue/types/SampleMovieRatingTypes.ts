import OxCollection from "@/core/models/OxCollection"
import SampleRating from "@modules/sample/vue/models/SampleRating"

export interface SampleMovieRatingGlobalTab {
    title: string
    icon: string
    count: number
    link: string
    loading: boolean
    collection: OxCollection<SampleRating>
}
