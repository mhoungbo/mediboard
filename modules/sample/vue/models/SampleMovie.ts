/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import SamplePerson from "./SamplePerson"
import SampleCategory from "./SampleCategory"
import { tr } from "@/core/utils/OxTranslator"
import SampleCasting from "@modules/sample/vue/models/SampleCasting"
import { OxAttr, OxAttrNullable } from "@/core/types/OxObjectTypes"
import SampleBookmark from "@modules/sample/vue/models/SampleBookmark"
import SampleRating from "@modules/sample/vue/models/SampleRating"

export default class SampleMovie extends OxObject {
    constructor () {
        super()
        this.type = "sample_movie"
    }

    protected _relationsTypes = {
        sample_person: SamplePerson,
        sample_category: SampleCategory,
        sample_casting: SampleCasting,
        sample_bookmark: SampleBookmark,
        sample_rating: SampleRating,
        file: OxObject
    }

    get image (): string | undefined {
        return this.links.cover
    }

    get languages (): string {
        let translator = ""

        if (this.languagesData) {
            const languages = this.languagesData.split("|")

            languages.forEach((language, index) => {
                if (index === 0) {
                    translator += tr("CSampleMovie.languages." + language)
                    return
                }
                translator += ", " + tr("CSampleMovie.languages." + language)
            })
        }
        return translator
    }

    get languagesData (): OxAttrNullable<string> {
        return super.get("languages")
    }

    set languagesData (value: OxAttrNullable<string>) {
        this.set("languages", value)
    }

    get title (): OxAttr<string> {
        return super.get("name")
    }

    set title (value: OxAttr<string>) {
        super.set("name", value)
    }

    get description (): OxAttrNullable<string> {
        return super.get("description")
    }

    set description (value: OxAttrNullable<string>) {
        this.set("description", value)
    }

    get release (): string {
        return new Date(this.attributes.release).toLocaleDateString("fr")
    }

    get releaseData (): OxAttr<string> {
        return super.get("release")
    }

    set releaseData (value: OxAttr<string>) {
        this.set("release", value)
    }

    get duration (): OxAttr<string> {
        return this.get("duration")
    }

    set duration (value: OxAttr<string>) {
        this.set("duration", value)
    }

    get csa (): OxAttrNullable<string> {
        return this.get("csa")
    }

    set csa (value: OxAttrNullable<string>) {
        this.set("csa", value)
    }

    get embeddedTrailerLink (): string | undefined {
        if (!this.trailerLink) {
            return undefined
        }

        // This regex will match different kinds of youtube links (watch?v=ID, embed/ID, youtu.be/ID, ...)
        const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/
        const match = this.trailerLink.match(regExp)
        // Match at index 7 always represent the video ID if present, so we check its length
        const videoId = (match && match[7].length === 11) ? match[7] : undefined

        if (!videoId) {
            return undefined
        }

        return `https://www.youtube.com/embed/${videoId}?enablejsapi=1&version=3&autoplay=1`
    }

    get hasTrailer (): boolean {
        return !!this.embeddedTrailerLink || !!this.trailerSearchLink
    }

    get trailerLink (): string | undefined {
        return this.links.trailer ?? undefined
    }

    set trailerLink (value: string | undefined) {
        this.links.trailer = value
    }

    get trailerSearchLink (): string | undefined {
        return this.links.trailer_search ?? undefined
    }

    set trailerSearchLink (value: string | undefined) {
        this.links.trailer_search = value
    }

    get trailerUrl (): OxAttrNullable<string> {
        return this.trailerLink
    }

    set trailerUrl (value: OxAttrNullable<string>) {
        this.trailerLink = value ?? undefined
        this.set("trailer_url", value)
    }

    get category (): SampleCategory | undefined {
        return this.loadForwardRelation<SampleCategory>("category")
    }

    set category (value: SampleCategory | undefined) {
        this.setForwardRelation("category", value)
    }

    get director (): SamplePerson | undefined {
        return this.loadForwardRelation<SamplePerson>("director")
    }

    set director (value: SamplePerson | undefined) {
        this.setForwardRelation("director", value)
    }

    get permEdit (): boolean {
        return this.meta?.permissions?.perm === "edit"
    }

    get releaseYear (): string {
        return new Date(this.attributes.release).getFullYear().toString()
    }

    get castingUrl (): string | undefined {
        return this.links.casting
    }

    get detailLink (): string {
        return this.links.details ?? ""
    }

    get actors (): SamplePerson[] {
        return this.loadBackwardRelation("actors")
    }

    get casting (): SampleCasting[] {
        return this.loadBackwardRelation("casting")
    }

    get bookmark (): SampleBookmark | undefined {
        // We treat "bookmarks" relation as forward because it's a unique backref
        return this.loadForwardRelation<SampleBookmark>("bookmarks")
    }

    set bookmark (value: SampleBookmark | undefined) {
        this.setForwardRelation("bookmarks", value)
    }

    get myRating (): SampleRating | undefined {
        return this.loadForwardRelation<SampleRating>("myRating")
    }

    set myRating (value: SampleRating | undefined) {
        this.setForwardRelation("myRating", value)
    }

    toString (): string {
        return `${this.title}`
    }
}
