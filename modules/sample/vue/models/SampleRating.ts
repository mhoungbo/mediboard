/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"
import OxMoment from "@/core/utils/OxMoment"

export default class SampleRating extends OxObject {
    constructor () {
        super()
        this.type = "sample_rating"
    }

    protected _relationsTypes = {
        sample_movie: SampleMovie,
        mediuser: Mediuser
    }

    get rating (): OxAttr<number> {
        return super.get("rating")
    }

    set rating (value: OxAttr<number>) {
        super.set("rating", value)
    }

    get datetime (): OxAttr<string> {
        return super.get("datetime")
    }

    set datetime (value: OxAttr<string>) {
        super.set("datetime", value)
    }

    get formattedDate (): string {
        return new OxMoment(this.datetime).format("DD/MM/YYYY")
    }

    get owner (): Mediuser | undefined {
        return this.loadForwardRelation<Mediuser>("owner")
    }

    set owner (value: Mediuser | undefined) {
        this.setForwardRelation("owner", value)
    }

    get movie (): SampleMovie | undefined {
        return this.loadForwardRelation<SampleMovie>("movie")
    }

    set movie (value: SampleMovie | undefined) {
        this.setForwardRelation("movie", value)
    }
}
