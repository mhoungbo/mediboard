/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import SampleMovie from "@modules/sample/vue/models/SampleMovie"

export default class SampleBookmark extends OxObject {
    constructor () {
        super()
        this.type = "sample_bookmark"
    }

    protected _relationsTypes = {
        sample_movie: SampleMovie
    }

    get movie (): SampleMovie {
        return this.loadForwardRelation<SampleMovie>("movie") as SampleMovie
    }

    set movie (value: SampleMovie) {
        this.setForwardRelation("movie", value)
    }
}
