<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Mediboard\Admin\Controllers\AccountController;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\HttpFoundation\RedirectResponse;

CCanDo::check();

$current_user = CUser::get();

// This feature is not allowed on LDAP-related users.
if (CAppUI::$token_session && $current_user->force_change_password && !$current_user->isLDAPLinked()) {
    CApp::getSessionHelper()->set('account_activation', true);

    $res = new RedirectResponse(
        CApp::generateUrl(AccountController::CHANGE_PASSWORD_VIEW, ['action' => 'activate_account'])
    );
} else {
    $res = new RedirectResponse(
        CApp::generateUrl(AccountController::CHANGE_PASSWORD_VIEW, ['action' => 'change_password'])
    );
}

$res->send();
die();
