{{*
 * @package Mediboard\Admin
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  submitActivationForm = function (form) {
    new Url().setRoute('{{url name=admin_gui_account_generate_token}}')
      .addFormData(form)
      .requestUpdate('systemMsg', {
        onComplete: function () {
          Control.Modal.close();
          Control.Modal.close();
        },
        method: 'post'
      });
    return false;
  };
</script>

<form name="activate-user-{{$object->_id}}" method="post" onsubmit="return submitActivationForm(this);">
  <input type="hidden" name="user_id" value="{{$object->_id}}" />

  <input type="hidden" name="@token" value="{{$activation_token}}"/>

  <table class="main form">
    <tr>
      <td>
        <label>
          <input type="radio" name="type" value="token" checked onclick="this.form.email.disabled = true"/>

          G�n�rer un jeton
        </label>
      </td>
    </tr>

    <tr>
      <td>
        <label>
          <input type="radio" name="type" value="email" onclick="this.form.email.disabled = false"/>

          Envoyer par email � :
        </label>

        <label>
          <input type="text" name="email" value="" size="35" disabled/>
        </label>
      </td>
    </tr>

    <tr>
      <td class="button">
        <button type="submit" class="save">
          Valider
        </button>
      </td>
    </tr>
  </table>
</form>

