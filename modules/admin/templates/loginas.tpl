{{*
 * @package Mediboard\Admin
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=switch_allowed value=false}}

{{if
  ($app->_ref_user->isAdmin() || $app->_ref_user->isSuperAdmin())
  && ($app->user_id != $loginas_user->_id)
  && !$loginas_user->template
  && (!$loginas_mediuser->_id || ($loginas_mediuser->isActive() && !$loginas_mediuser->isSecondary()))
}}
    {{assign var=switch_allowed value=true}}
{{/if}}

{{if $switch_allowed}}
  <form name="loginas-{{$loginas_user->_id}}" method="post" onsubmit="return UserSwitch.login(this, '{{url name=admin_gui_switch_user}}');">
    <input type="hidden" name="username" value="{{$loginas_user->user_username}}"/>
    <button type="button" class="tick compact" onclick="this.form.onsubmit()">
        {{tr}}Substitute{{/tr}}
    </button>
  </form>
{{/if}}
