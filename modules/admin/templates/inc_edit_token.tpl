{{*
 * @package Mediboard\Admin
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=mediusers script=CMediusers ajax=true}}

<script>
  checkParams = function () {
    const form = getForm('edit-token');

    if ($V(form.params) || $V(form._route)) {
      $('label-params').removeClassName('notNull').addClassName('notNullOK');
      $('label-routes_names').removeClassName('notNull').addClassName('notNullOK');
    } else {
      $('label-params').removeClassName('notNullOK').addClassName('notNull');
      $('label-routes_names').removeClassName('notNullOK').addClassName('notNull');
    }
  };

  checkParamsAndRoute = function (form) {
    if (!$V(form.params) && !$V(form._route)) {
      alert($T('CViewAccessToken-error-Either params or route names must be valued'));
      return false;
    }

    return true;
  };

  Main.add(function () {
    checkParams();

    const form = getForm('edit-token');
    ViewAccessToken.autocomplete(form, '{{url name=developpement_routes_autocomplete}}', 'checkParams');
  });
</script>

<form name="edit-token" method="post"
      onsubmit="return onSubmitFormAjax(this, function(){ Control.Modal.close(); getForm('search-token').onsubmit(); });">
    {{mb_key object=$token}}

  <input type="hidden" name="m" value="admin"/>
  <input type="hidden" name="dosql" value="do_token_aed"/>

  <input type="hidden" name="_route" value="{{'|'|implode:$token->_routes_names}}" onchange="checkParams()"/>

  <table class="main form" style="max-width: 840px; width: 840px;">
    <col style="width: 30%"/>

      {{mb_include module=system template=inc_form_table_header object=$token colspan=4}}

      {{if $token->_id}}
        <tr>
          <th>{{mb_label object=$token field="hash"}}</th>
          <td colspan="3"><tt>{{mb_value object=$token field="hash"}}</tt></td>
        </tr>
      {{/if}}

    <tr>
      <th colspan="4" class="category">
          {{tr}}CViewAccessToken-title-behaviour{{/tr}}
      </th>
    </tr>

    <tr>
      <th>{{mb_label object=$token field=user_id}}</th>
      <td colspan="3">
        <script>
          Main.add(CMediusers.standardAutocomplete.curry('edit-token', 'user_id', '_user_view'));
        </script>
          {{mb_field object=$token field=user_id hidden=1}}
        <input type="text" name="_user_view" style="width: 16em;" class="autocomplete"
               value="{{$token->_ref_user}}"
               onchange="if(!this.value) { this.form.user_id.value='' }"/>
      </td>
    </tr>

    <tr>
      <th>
          {{mb_label object=$token field=label}}
      </th>

      <td colspan="3">
          {{mb_field object=$token field=label size=60}}
      </td>
    </tr>

    <tr>
      <th>
          <label id="label-params" for="params" class="notNull" title="{{tr}}CViewAccessToken-params-desc{{/tr}}">
              {{tr}}CViewAccessToken-params{{/tr}}
          </label>
      </th>

      <td colspan="3">
          {{mb_field object=$token field="params" size=50 onchange="checkParams()"}}
      </td>
    </tr>

    <tr>
      <th>
        <label id="label-routes_names" for="routes_names" class="notNull" title="{{tr}}CViewAccessToken-routes_names-desc{{/tr}}">
            {{tr}}CViewAccessToken-routes_names{{/tr}}
        </label>
      </th>

      <td colspan="3">
        <div>
          <input type="text" name="search" value=""/>

          <ul id="token-routes" class="tags me-ws-wrap me-inline-block">
              {{foreach from=$token->_routes key=name item=route}}
                <li data-route_name="{{$name}}" class="tag">
                <span onmouseover="ObjectTooltip.createDOM(this, 'token-route-{{$name}}')">
                    {{$route->getRouteName()}}
                </span>

                  <button type="button" class="delete" onclick="ViewAccessToken.removeRoute(this);checkParams();"></button>
                </li>

                <table id="token-route-{{$name}}" class="tbl" style="display: none;">
                  <tr>
                    <th colspan="4" class="title">D�tail de la route</th>
                  </tr>

                  <tr>
                    <th>Name</th>
                    <td>{{$route->getRouteName()}}</td>

                    <th>Module</th>
                    <td>{{$route->getModule()}}</td>
                  </tr>

                  <tr>
                    <th>Controller</th>
                    <td>{{$route->getController()}}</td>

                    <th>Action</th>
                    <td>{{$route->getAction()}}</td>
                  </tr>

                  <tr>
                    <th>Path</th>
                    <td>{{$route->getPath()}}</td>

                    <th>Method</th>
                    <td>{{$route->getMethods()}}</td>
                  </tr>
                </table>
              {{/foreach}}
          </ul>
        </div>

        {{if $token->_routes|@count > 1}}
            <div class="small-warning">
              Plusieurs routes sont renseign�es au sein de ce jeton ; seule la premi�re sera effective.
            </div>
        {{/if}}
      </td>
    </tr>

    <tr>
      <th>{{mb_label object=$token field="restricted"}}</th>
      <td>{{mb_field object=$token field="restricted"}}</td>

      <th>{{mb_label object=$token field='purgeable'}}</th>
      <td>{{mb_field object=$token field='purgeable'}}</td>
    </tr>

    <tr>
      <th colspan="4" class="category">
          {{tr}}CViewAccessToken-title-validity{{/tr}}
      </th>
    </tr>

      {{mb_ternary var=readonly test=$token->_id value=true other=false}}
    <tr>
      <th>{{mb_label object=$token field=_hash_length}}</th>
      <td>
          {{if $token->_id}}
              {{mb_field object=$token field=_hash_length readonly=true}}
          {{else}}
              {{mb_field object=$token field=_hash_length form='edit-token' increment=true}}
          {{/if}}
      </td>

      <th>{{mb_label object=$token field=hash}}</th>
      <td>
          {{if $token->_id}}
              {{mb_field object=$token field=hash readonly=$readonly}}
          {{else}}
              {{mb_field object=$token field=hash canNull=true}}
          {{/if}}
      </td>
    </tr>

    <tr>
      <th>{{mb_label object=$token field="datetime_start"}}</th>
      <td>{{mb_field object=$token field="datetime_start" register=true form="edit-token"}}</td>
      <th>{{mb_label object=$token field="max_usages"}}</th>
      <td>{{mb_field object=$token field="max_usages" increment=true form="edit-token"}}</td>
    </tr>

    <tr>
      <th>{{mb_label object=$token field="datetime_end"}}</th>
      <td>{{mb_field object=$token field="datetime_end" register=true form="edit-token"}}</td>
      <th>{{tr}}common-Validity{{/tr}}</th>
      <td>{{if $token->_validity_duration}}{{$token->_validity_duration.locale}}{{/if}}</td>
    </tr>

    <tr>
      <th colspan="4" class="category">
          {{tr}}CViewAccessToken-validator{{/tr}}
      </th>
    </tr>

    <tr>
      <th>{{mb_label object=$token field=validator}}</th>
      <td colspan="3">
        <select name="validator">
          <option value="" selected>{{tr}}CViewAccessToken-validator.select{{/tr}}</option>

            {{foreach from=$token->_validators item=_validator}}
              <option value="{{$_validator}}" {{if $token->validator == $_validator}}selected{{/if}}>
                  {{$_validator}}
              </option>
            {{/foreach}}
        </select>
      </td>
    </tr>

      {{if $token->_id}}
        <tr>
          <th colspan="4" class="category">
              {{tr}}CViewAccessToken-title-statistics{{/tr}}
          </th>
        </tr>
        <tr>
          <th>{{mb_label object=$token field="first_use"}}</th>
          <td>{{mb_value object=$token field="first_use"}}</td>
          <th>{{mb_label object=$token field="total_use"}}</th>
          <td>{{mb_value object=$token field="total_use"}}</td>
        </tr>
        <tr>
          <th>{{mb_label object=$token field="latest_use"}}</th>
          <td>{{mb_value object=$token field="latest_use"}}</td>
          <th>{{mb_label object=$token field="_mean_usage_duration"}}</th>
          <td>
              {{if $token->_mean_usage_duration}}
                  {{$token->_mean_usage_duration.locale}}
              {{/if}}
          </td>
        </tr>
      {{/if}}

    <tr>
      <td class="button" colspan="4">
          {{if $token->_id}}
            <button class="modify" type="submit">{{tr}}Save{{/tr}}</button>
            <button type="button" class="search"
                    onclick="prompt($T('common-msg-Copy then paste given URL'), '{{$token->_url|smarty:nodefaults|JSAttribute}}');">
                {{tr}}CViewAccessToken-action-Show link{{/tr}}
            </button>
            <a class="button link" target="_blank" href="{{$token->_url|smarty:nodefaults|JSAttribute}}">
                {{tr}}CViewAccessToken-action-Open link{{/tr}}
            </a>
            <button type="button" class="trash"
                    onclick="confirmDeletion(
                      this.form,
                      {typeName:'',objName:'{{$token->_view|smarty:nodefaults|JSAttribute}}'},
                      function(){ Control.Modal.close(); getForm('search-token').onsubmit(); }
                      )"
            >
                {{tr}}Delete{{/tr}}
            </button>
          {{else}}
            <button class="submit" type="submit" onclick="return checkParamsAndRoute(this.form)">{{tr}}Create{{/tr}}</button>
          {{/if}}
      </td>
    </tr>
  </table>
</form>
