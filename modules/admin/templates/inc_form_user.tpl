{{*
 * @package Mediboard\Admin
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=activation_token value=null}}

<script>
  Main.add(function () {
    var form = getForm('Edit-user');
    warnRobot(form);
    changeTemplateState(form);
  });

  duplicateUser = function (form) {
    let login = window.prompt($T('CUser-msg-Please, give a login name'));

    if (login === null) {
      return;
    }

    login = login.trim();

    if (!login) {
      alert($T('common-error-Missing parameter: %s', $T('CUser-user_username-desc')));
      return;
    }

    $V(form._duplicate, '1');
    $V(form._duplicate_username, login);

    if (checkForm(form)) {
      form.onsubmit();
    }

    $V(form._duplicate, '');
    $V(form._duplicate_username, '');

    UserPermission.callback();
  };

  warnRobot = function (form) {
    var is_robot = ($V(form.elements.is_robot) === '1');
    var dont_log_connection = ($V(form.elements.dont_log_connection) === '1');

    toggleRobotWarning((is_robot && !dont_log_connection));
  };

  toggleRobotWarning = function (show) {
    var elt = $('robotMsg');
    elt.setVisible(show);
  };

  showGenerateActivation = function () {
    $V(getForm('activate-user-{{$user->_id}}').email, $V(getForm('Edit-user').user_email));

    Modal.open('activate-user-{{$user->_id}}', {showClose: true, title: $T('CUser-label-Generate an activation link')});
  };

  changeTemplateState = function (form) {
    {{if !$user->user_id}}
      var is_template = ($V(form.elements.template) === '1');
      var fields = $$("input[name='is_robot'").concat($$("input[name='dont_log_connection'"));

      if (is_template) {
        $V(form.elements.is_robot, '1');
        $V(form.elements.dont_log_connection, '1');
      } else {
        $V(form.elements.is_robot, '0');
        $V(form.elements.dont_log_connection, '0');
      }

      fields.each(function (input) {
        if (is_template) {
          input.disable();
        } else {
          input.enable();
        }
      });
    {{/if}}
  };
</script>

<table class="main form">
  <tr>
    <th>{{mb_label object=$user field="user_username"}}</th>
    <td>
        {{if !$readOnlyLDAP}}
            {{mb_field object=$user field="user_username"}}
        {{else}}
            {{mb_value object=$user field="user_username"}}
            {{mb_field object=$user field="user_username" hidden=true}}
        {{/if}}
    </td>
  </tr>

  <tr>
    <th>{{mb_label object=$user field="user_type"}}</th>
    <td>
      <select name="user_type" class="{{$user->_props.user_type}}"
              {{if !$is_admin && ($object && $object->_id && $object->isTypeAdmin())}} disabled{{/if}}>
        <option value="">&mdash; {{tr}}Choose{{/tr}}</option>
          {{foreach from=$utypes key=_key item=type}}
            <option value="{{$_key}}" {{if $_key == $user->user_type}}selected="selected"{{/if}}
                    {{if !$is_admin && $_key == 1}} disabled> {{$type}} ({{tr}}CMbFieldSpec.perm{{/tr}})
                {{else}} > {{$type}}{{/if}}
            </option>
          {{/foreach}}
      </select>

        {{if !$is_admin && ($object && $object->_id && $object->isTypeAdmin())}}
          <div class="small-info">
              {{tr}}CUser-error-You are not allowed to modify an admin user{{/tr}}
          </div>
        {{/if}}
    </td>
  </tr>

  <tr>
    <th>{{mb_label object=$user field="template"}}</th>
    <td>{{mb_field object=$user field="template" onchange="changeTemplateState(this.form);"}}</td>
  </tr>

    {{if $user->_id && !$readOnlyLDAP && $activation_token}}
      <tr>
        <th>
            {{mb_label object=$user field=user_password}}
        </th>
        <td>
          <button type="button" class="change" onclick="showGenerateActivation();">
              {{tr}}common-action-Generate account activation link{{/tr}}
          </button>
        </td>
      </tr>
    {{/if}}

  <tr>
    <th>{{mb_label object=$user field="user_last_name"}}</th>
    <td>
        {{if !$readOnlyLDAP}}
            {{mb_field object=$user field="user_last_name"}}
        {{else}}
            {{mb_value object=$user field="user_last_name"}}
            {{mb_field object=$user field="user_last_name" hidden=true}}
        {{/if}}
    </td>
  </tr>

  <tr>
    <th>{{mb_label object=$user field="user_first_name"}}</th>
    <td>
        {{if !$readOnlyLDAP}}
            {{mb_field object=$user field="user_first_name"}}
        {{else}}
            {{mb_value object=$user field="user_first_name"}}
            {{mb_field object=$user field="user_first_name" hidden=true}}
        {{/if}}
    </td>
  </tr>

  <tr>
    <th>{{mb_label object=$user field="user_email"}}</th>
    <td>
        {{if !$readOnlyLDAP}}
            {{mb_field object=$user field="user_email"}}
        {{else}}
            {{mb_value object=$user field="user_email"}}
            {{mb_field object=$user field="user_email" hidden=true}}
        {{/if}}
    </td>
  </tr>

  <tr>
    <th>{{mb_label object=$user field=is_robot}}</th>
    <td>
      <div id="robotMsg" class="small-warning" style="display: none;">
          {{tr}}CUser-msg-This user is a robot, but its connections are logged, are you sure?{{/tr}}
      </div>

        {{mb_field object=$user field=is_robot onchange='warnRobot(this.form);'}}
    </td>
  </tr>

  <tr>
    <th>{{mb_label object=$user field="dont_log_connection"}}</th>
    <td>{{mb_field object=$user field="dont_log_connection" onchange='warnRobot(this.form);'}}</td>
  </tr>

  <tr>
    <th>{{mb_label object=$user field=force_change_password}}</th>
    <td>{{mb_field object=$user field=force_change_password}}</td>
  </tr>

  <tr>
    <td class="button" colspan="2">
        {{if $user->_id}}
            {{if $user->isRobot()}}
              <button class="modify"
                      type="button"
                      onclick="CMediusers.confirmMediuserEdition(this.form)">
                  {{tr}}Save{{/tr}}
              </button>
            {{else}}
              <button class="modify" type="submit">{{tr}}Save{{/tr}}</button>
            {{/if}}
          <button class="duplicate" type="button" onclick="duplicateUser(this.form);">
              {{tr}}Duplicate{{/tr}}
          </button>
          <button class="trash" type="button"
                  onclick="CMediusers.confirmMediuserDeletion(this.form, '{{$user->isRobot()}}')">
              {{tr}}Delete{{/tr}}
          </button>
        {{else}}
          <button class="submit" type="submit">{{tr}}Create{{/tr}}</button>
        {{/if}}
    </td>
  </tr>

  <!-- Link to CMediusers -->
  <tr>
    <td colspan="2" class="button">
        {{if $user->_ref_mediuser && $user->_ref_mediuser->_id}}
          <div class="small-success">
              {{tr}}CMediusers-msg-This user is well integrated into the organization chart{{/tr}}
            <br/>
            <a class="button edit" href="?m=mediusers&tab=vw_idx_mediusers&user_id={{$user->_id}}">
                {{tr}}CMediusers-action-Manage this user in the organization chart{{/tr}}
            </a>
          </div>
        {{else}}
            {{if $user->template}}
              <div class="small-info">
                  {{tr}}CMediusers-msg-This user is not in the organization chart. This is normal for a Profile{{/tr}}
              </div>
            {{else}}
              <div class="small-warning">
                  {{tr}}CMediusers-msg-This user is not in the organization chart, This is abnormal for a real user{{/tr}}
                <br/>
                <a class="button new" href="?m=mediusers&tab=vw_idx_mediusers&user_id={{$user->_id}}&no_association=1">
                    {{tr}}CMediusers-action-Associate this user with the organization chart{{/tr}}
                </a>
              </div>
            {{/if}}
        {{/if}}
    </td>
  </tr>
</table>
