{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<div class="me-autocomplete-mediusers"
     style="padding-left: 2px; margin: -1px;">
  <div></div>
  <span class="view">{{if $show_view || !$f}}{{$match}}{{else}}{{$match->$f|emphasize:$input}}{{/if}}</span>
  <div style="text-align: right; color: #999; font-size: 0.8em;">
      {{if $match->_ref_mediuser}}{{$match->_ref_mediuser->_ref_function}}{{/if}}
  </div>
</div>
