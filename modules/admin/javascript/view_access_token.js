/**
 * @package Mediboard\Admin
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ViewAccessToken = {
  tokenField: {},

  edit: function (token_id) {
    ViewAccessToken.tokenField = {};

    var url = new Url('admin', 'ajax_edit_token');
    url.addParam('token_id', token_id);
    url.requestModal('700', null, {onClose: ViewAccessToken.list});
  },

  generate: function (params) {
    var url = new Url('admin', 'ajax_generate_token');
    url.addParam('token_default_params', JSON.stringify(params));
    url.addParam('ajax', '1');
    url.requestModal('70%', 'auto');
  },

  autocomplete: function (form, url, callback) {
    ViewAccessToken.tokenField = new TokenField(form._route, {
      onChange: () => {}
    });

    new Url().setRoute(url)
      .autoComplete(form.search, null, {
        minChars:      3,
        method:        'get',
        dropdown:      true,
        updateElement: function (selected) {
          $V(form.search, '');

          const name = selected.get('route_name');

          if (name !== null && ($V(form._route) === '')) {
            const added = !ViewAccessToken.tokenField.contains(name);
            ViewAccessToken.tokenField.add(name);

            if (added) {
              ViewAccessToken.addRoute(name, callback);
            }
          }

          if (callback) {
            window[callback]();
          }
        }
      });
  },

  removeRoute: function (input) {
    const container = $(input).up('li');

    ViewAccessToken.tokenField.remove(container.get('route_name'));
    container.remove();
  },

  addRoute: function (route, callback) {
    let onclick = 'ViewAccessToken.removeRoute(this);';
    if (callback) {
      onclick += callback + '();';
    }

    const btn = DOM.button({
      type:      'button',
      className: 'delete',
      onclick: onclick
    });

    const span = DOM.span({}, route);

    const li = DOM.li({
      'data-route_name': route,
      className:         'tag',
    }, span, btn);

    $('token-routes').insert(li);
  }
};
