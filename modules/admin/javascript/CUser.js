/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

CUser = {
  makeUserAutocomplete: function (url_route, form_name, input_name, id_name) {
    var form = getForm(form_name);
    var id_element    = form[id_name];
    var input_element = form[input_name];

    new Url().setRoute(url_route)
      .autoComplete(input_element, null, {
        minChars:      2,
        method:        'get',
        dropdown:      true,
        afterUpdateElement: function (field, selected) {
          const id = selected.getAttribute('id').split("-")[2];
          $V(id_element, id);

          if ($V(input_element)) {
            $V(input_element, selected.down('.view').innerHTML);
          }
        }
      });
  },
};
