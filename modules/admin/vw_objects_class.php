<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CApp;
use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Admin\CPermObject;

CCanDo::checkEdit();
$user_id  = CView::getRefCheckEdit("user_id", "ref class|CUser notNull");
$mod_name = CView::get("mod_name", "str");
CView::checkin();

$perm_object = new CPermObject();

$classes        = CApp::getInstalledClasses([], true);
$module_classes = CApp::groupClassesByModule($classes);

$token = AntiCsrf::prepare()
                 ->addParam('user_id', $user_id)
                 ->addParam('object_class', $module_classes[$mod_name] ?? [])
                 ->addParam('perm_object_id')
                 ->addParam('permission')
                 ->addParam('object_id')
                 ->addParam('_object_view')
                 ->getToken();

$smarty = new CSmartyDP();
$smarty->assign("module_classes", $module_classes);
$smarty->assign("mod_name", $mod_name);
$smarty->assign("user_id", $user_id);
$smarty->assign("permObject", $perm_object);
$smarty->assign('token', $token);
$smarty->display("vw_objects_class.tpl");
