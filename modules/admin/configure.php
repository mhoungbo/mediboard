<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Mediboard\Admin\PasswordSpecs\PasswordSpec;
use Ox\Mediboard\Admin\PasswordSpecs\PasswordSpecBuilder;
use Ox\Mediboard\Admin\Services\AccountActivationService;

CCanDo::checkAdmin();

$special_chars = implode(' ', array_map('chr', array_values(PasswordSpec::getSpecialChars())));

$smarty = new CSmartyDP();
$smarty->assign('reset_account_source', (new AccountActivationService())->getSMTPSource());
$smarty->assign('std_min_length', PasswordSpecBuilder::MIN_LENGTH);
$smarty->assign('ldap_min_length', PasswordSpecBuilder::MIN_LENGTH);
$smarty->assign('admin_min_length', PasswordSpecBuilder::ADMIN_MIN_LENGTH);
$smarty->assign('special_chars', $special_chars);
$smarty->display('configure.tpl');
