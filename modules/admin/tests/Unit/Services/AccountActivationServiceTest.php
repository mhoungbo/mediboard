<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Tests\Unit\Services;

use Ox\Core\CSmartyDP;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Exception\CouldNotActivateAccount;
use Ox\Mediboard\Admin\Generators\CUserGenerator;
use Ox\Mediboard\Admin\Services\AccountActivationService;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CSourceSMTP;
use Ox\Tests\TestsException;
use Ox\Tests\OxUnitTestCase;

class AccountActivationServiceTest extends OxUnitTestCase
{
    private const EMAIL = 'toolbox@openxtrem.com';

    /** @var CUser */
    private static $user;

    /**
     * @inheritDoc
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$user = (new CUserGenerator())->generate();
    }

    public function invalidEmailProvider(): array
    {
        return [
            'empty'              => [''],
            'invalid characters' => ['test()'],
            'invalid format'     => ['test@@test'],
        ];
    }

    public function testWhenUserDoesNotExist(): void
    {
        $this->expectExceptionObject(CouldNotActivateAccount::userNotFound());

        $user = new CUser();

        (new AccountActivationService())->generateToken($user);
    }

    public function testWhenUserIsSuperAdmin(): void
    {
        $user = $this->getMockBuilder(CUser::class)->getMock();
        $user->expects($this->once())->method('isSuperAdmin')->willReturn(true);

        $user->_id = 'DoesNotReallyExist';

        $this->expectExceptionObject(CouldNotActivateAccount::superAdminNotAllowed());
        (new AccountActivationService())->generateToken($user);
    }

    public function testRealTokenGeneration(): void
    {
        // Real user
        $service = new AccountActivationService();

        // Real token
        $token = $service->generateToken(self::$user);

        // Asserting that a valid token is generated is acceptable here
        $this->assertNotNull($token->_id);
    }

    public function testFakeTokenGeneration(): void
    {
        // Fake user
        $user      = $this->getMockBuilder(CUser::class)->getMock();
        $user->_id = 'DoesNotReallyExist';

        // Mocking storeToken
        $service = $this->getMockBuilder(AccountActivationService::class)
            ->onlyMethods(['storeToken'])
            ->getMock();

        // Fake token
        $token = $service->generateToken($user);

        // Asserting that token is a CViewAccessToken instance is implicit. We just check ID is null (because of mock).
        $this->assertNull($token->_id);
    }

    public function testTokenGenerationFailsIfUnableToResetPassword(): void
    {
        $user = $this->getMockBuilder(CUser::class)->getMock();
        $user->expects($this->any())->method('store')->willReturn('Unable to reset TEST password');

        $user->_id = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_SECRETAIRE)->_id;

        $service = new AccountActivationService();

        $this->expectExceptionObject(CouldNotActivateAccount::unableToResetPassword('Unable to reset TEST password'));
        $service->generateToken($user);
    }

    public function testTokenGenerationFailsIfUnableToCreateToken(): void
    {
        $user = $this->getMockBuilder(CUser::class)->getMock();
        $user->expects($this->any())->method('store')->willReturn('An error during test occurred');

        $user->_id = 'DoesNotReallyExist';

        $service = $this->getMockBuilder(AccountActivationService::class)
            ->onlyMethods(['storeToken'])
            ->getMock();

        $service->expects($this->once())->method('storeToken')->willThrowException(
            new TestsException('Unable to store TEST token')
        );

        $this->expectExceptionObject(CouldNotActivateAccount::unableToCreateToken('Unable to store TEST token'));
        $service->generateToken(self::$user);
    }

    /**
     * @depends testRealTokenGeneration
     *
     * @throws CouldNotActivateAccount
     */
    public function testSendingEmail(): void
    {
        $source         = $this->getMockBuilder(CSourceSMTP::class)->getMock();
        $source->_id    = 'DoesNotReallyExist';
        $source->active = 1;

        $smarty = $this->getMockBuilder(CSmartyDP::class)->getMock();

        $service = new AccountActivationService();
        $result  = $service->sendTokenViaEmail(self::$user, self::EMAIL, $smarty, $source);

        $this->assertTrue($result);
    }

    public function testSendEmailWithDisabledSourceThrowsAnException(): void
    {
        $source      = $this->getMockBuilder(CSourceSMTP::class)->getMock();

        $smarty = $this->getMockBuilder(CSmartyDP::class)->getMock();

        $service = new AccountActivationService();

        $this->expectExceptionObject(CouldNotActivateAccount::sourceNotFound());
        $service->sendTokenViaEmail(self::$user, self::EMAIL, $smarty, $source);
    }

    /**
     * @dataProvider invalidEmailProvider
     *
     * @param string $email
     *
     * @throws CouldNotActivateAccount
     */
    public function testSendEmailWithInvalidEmailFails(string $email): void
    {
        $source         = $this->getMockBuilder(CSourceSMTP::class)->getMock();
        $source->_id    = 'DoesNotReallyExist';
        $source->active = 1;

        $smarty = $this->getMockBuilder(CSmartyDP::class)->getMock();

        $service = new AccountActivationService();

        $this->expectExceptionObject(CouldNotActivateAccount::invalidEmail($email));
        $service->sendTokenViaEmail(self::$user, $email, $smarty, $source);
    }

    public function testSendEmailHandleSourceException(): void
    {
        $source         = $this->getMockBuilder(CSourceSMTP::class)->getMock();
        $source->_id    = 'DoesNotReallyExist';
        $source->active = 1;

        $source->expects($this->once())->method('send')->willThrowException(
            new TestsException('Unable to send TEST email')
        );

        $smarty = $this->getMockBuilder(CSmartyDP::class)->getMock();

        $service = new AccountActivationService();

        $this->expectExceptionObject(CouldNotActivateAccount::unableToSendEmail('Unable to send TEST email'));
        $service->sendTokenViaEmail(self::$user, self::EMAIL, $smarty, $source);
    }

    public function testCanGenerateActivationTokenNoEditOnMediusers(): void
    {
        $current_user = CMediusers::get();
        $module = CModule::getActive('mediusers');
        $perm = CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] ?? null;
        try {
            CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] = CPermModule::READ;

            $this->assertFalse(
                (new AccountActivationService())->canGenerateActivationToken(self::$user, 1)
            );
        } finally {
            if ($perm === null) {
                unset(CPermModule::$users_cache[$current_user->_id][$module->_id]);
            } else {
                CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] = $perm;
            }
        }
    }

    public function testCanGenerateActivationTokenNotSameGroup(): void
    {
        $current_user = CMediusers::get();

        $this->assertFalse(
            (new AccountActivationService())->canGenerateActivationToken($current_user->loadRefUser(), -1)
        );
    }

    public function testCanGenerateActivationTokenOk(): void
    {
        $current_user = CMediusers::get();
        $function = $current_user->loadRefFunction();

        $module = CModule::getActive('mediusers');
        $perm = CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] ?? null;
        try {
            CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] = CPermModule::EDIT;

            $this->assertTrue(
                (new AccountActivationService())
                    ->canGenerateActivationToken($current_user->loadRefUser(), $function->group_id)
            );
        } finally {
            if ($perm === null) {
                unset(CPermModule::$users_cache[$current_user->_id][$module->_id]);
            } else {
                CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] = $perm;
            }
        }
    }
}
