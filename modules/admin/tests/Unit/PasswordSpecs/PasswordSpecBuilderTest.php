<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Tests\Unit\PasswordSpecs;

use Exception;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\PasswordSpecs\PasswordSpec;
use Ox\Mediboard\Admin\PasswordSpecs\PasswordSpecBuilder;
use Ox\Mediboard\Admin\PasswordSpecs\PasswordSpecConfiguration;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;

class PasswordSpecBuilderTest extends OxUnitTestCase
{
    public function stdProvider(): array
    {
        $configuration = $this->getPasswordConfiguration();

        return [
            'non admin user'                       => [
                $this->buildUser(true, false, false),
                $configuration,
            ],
            'ldap user without ldap configuration' => [
                $this->buildUser(true, false, true),
                $configuration,
            ],
        ];
    }

    public function adminProvider(): array
    {
        $configuration = $this->getAdminPasswordConfiguration();

        return [
            'admin user without ldap' => [
                $this->buildUser(true, true, false),
                $configuration,
            ],
            'admin user with ldap'    => [
                $this->buildUser(true, true, true),
                $configuration,
            ],
        ];
    }

    public function userLDAPProvider(): array
    {
        $configuration = $this->getLdapPasswordConfiguration();

        return [
            'ldap non admin user' => [
                $this->buildUser(false, false, true),
                $configuration,
            ],
            'ldap admin user'     => [
                $this->buildUser(false, true, true),
                $configuration,
            ],
        ];
    }

    /**
     * @dataProvider stdProvider
     *
     * @param CUser|CMediusers          $user
     * @param PasswordSpecConfiguration $configuration
     *
     * @throws Exception
     */
    public function testUserHasStrongPasswordSpecification(CUser $user, PasswordSpecConfiguration $configuration): void
    {
        $factory = new PasswordSpecBuilder($user, $configuration);
        $spec    = $factory->build();

        $this->assertTrue($spec->isStrong());
    }

    /**
     * @dataProvider adminProvider
     *
     * @param CUser|CMediusers          $user
     * @param PasswordSpecConfiguration $configuration
     *
     * @throws Exception
     */
    public function testUserHasAdminPasswordSpecification(CUser $user, PasswordSpecConfiguration $configuration): void
    {
        $factory = new PasswordSpecBuilder($user, $configuration);
        $spec    = $factory->build();

        $this->assertTrue($spec->isAdmin());
    }

    /**
     * @dataProvider userLDAPProvider
     *
     * @param CUser|CMediusers          $user
     * @param PasswordSpecConfiguration $configuration
     *
     * @throws Exception
     */
    public function testUserHasLDAPPasswordSpecification(CUser $user, PasswordSpecConfiguration $configuration): void
    {
        $factory = new PasswordSpecBuilder($user, $configuration);
        $spec    = $factory->build();

        $this->assertTrue($spec->isLDAP());
    }

    public function testMinimalPasswordSpecifications(): void
    {
        // STANDARD
        $builder = new PasswordSpecBuilder($this->buildUser(true, false, false), new PasswordSpecConfiguration());
        $spec    = $builder->build();

        $this->assertTrue($spec->isStrong());
        $this->assertPasswordSpecifications($spec, PasswordSpecBuilder::MIN_LENGTH);

        // ADMIN
        $builder = new PasswordSpecBuilder($this->buildUser(true, true, false), new PasswordSpecConfiguration());
        $spec    = $builder->build();

        $this->assertTrue($spec->isAdmin());
        $this->assertPasswordSpecifications($spec, PasswordSpecBuilder::ADMIN_MIN_LENGTH);

        // LDAP disabled by default, so a non-admin
        $builder = new PasswordSpecBuilder($this->buildUser(true, false, true), new PasswordSpecConfiguration());
        $spec    = $builder->build();

        $this->assertTrue($spec->isStrong());
        $this->assertPasswordSpecifications($spec, PasswordSpecBuilder::MIN_LENGTH);

        // LDAP disabled by default, so an admin
        $builder = new PasswordSpecBuilder($this->buildUser(true, true, true), new PasswordSpecConfiguration());
        $spec    = $builder->build();

        $this->assertTrue($spec->isAdmin());
        $this->assertPasswordSpecifications($spec, PasswordSpecBuilder::ADMIN_MIN_LENGTH);

        // LDAP enabled
        $ldap_config                  = new PasswordSpecConfiguration();
        $ldap_config['ldap_specific'] = true;

        $builder = new PasswordSpecBuilder($this->buildUser(true, true, true), $ldap_config);
        $spec    = $builder->build();

        $this->assertTrue($spec->isLDAP());
        $this->assertPasswordSpecifications($spec, PasswordSpecBuilder::MIN_LENGTH);
    }

    /**
     * @depends testMinimalPasswordSpecifications
     * @return void
     */
    public function testOverhauledPasswordSpecifications(): void
    {
        // Standard config, with lesser min length and no mandatory charsets
        $config                         = new PasswordSpecConfiguration();
        $config['strong_min_length']    = 6;
        $config['strong_alpha_chars']   = false;
        $config['strong_num_chars']     = false;
        $config['strong_upper_chars']   = false;
        $config['strong_special_chars'] = true;

        $builder = new PasswordSpecBuilder($this->buildUser(true, false, false), $config);

        $this->assertPasswordSpecifications($builder->build(), PasswordSpecBuilder::MIN_LENGTH, true);

        // Standard config, with greater min length
        $config                      = new PasswordSpecConfiguration();
        $config['strong_min_length'] = 35;

        $builder = new PasswordSpecBuilder($this->buildUser(true, false, false), $config);

        $this->assertPasswordSpecifications($builder->build(), 35);

        // Admin config, with lesser min length and no mandatory charsets
        $config                        = new PasswordSpecConfiguration();
        $config['admin_min_length']    = 6;
        $config['admin_alpha_chars']   = false;
        $config['admin_num_chars']     = false;
        $config['admin_upper_chars']   = false;
        $config['admin_special_chars'] = true;

        $builder = new PasswordSpecBuilder($this->buildUser(true, true, false), $config);

        $this->assertPasswordSpecifications($builder->build(), PasswordSpecBuilder::ADMIN_MIN_LENGTH, true);

        // Admin config, with greater min length
        $config                     = new PasswordSpecConfiguration();
        $config['admin_min_length'] = 53;

        $builder = new PasswordSpecBuilder($this->buildUser(true, true, false), $config);

        $this->assertPasswordSpecifications($builder->build(), 53);

        // LDAP config, with lesser min length and no mandatory charsets
        $config                       = new PasswordSpecConfiguration();
        $config['ldap_specific']      = true;
        $config['ldap_min_length']    = 6;
        $config['ldap_alpha_chars']   = false;
        $config['ldap_num_chars']     = false;
        $config['ldap_upper_chars']   = false;
        $config['ldap_special_chars'] = true;

        $builder = new PasswordSpecBuilder($this->buildUser(true, true, true), $config);

        $this->assertPasswordSpecifications($builder->build(), PasswordSpecBuilder::MIN_LENGTH, true);

        // Admin config, with greater min length
        $config                    = new PasswordSpecConfiguration();
        $config['ldap_specific']   = true;
        $config['ldap_min_length'] = 42;

        $builder = new PasswordSpecBuilder($this->buildUser(true, true, true), $config);

        $this->assertPasswordSpecifications($builder->build(), 42);
    }

    private function assertPasswordSpecifications(
        PasswordSpec $spec,
        int          $min_length,
        bool         $special_chars = false
    ): void {
        $this->assertEquals($min_length, $spec->getMinLength());
        $this->assertTrue($spec->mustHaveAlphaChars());
        $this->assertTrue($spec->mustHaveNumChars());
        $this->assertTrue($spec->mustHaveUpperChars());
        $this->assertEquals($special_chars, $spec->mustHaveSpecialChars());
    }

    private function getPasswordConfiguration(): PasswordSpecConfiguration
    {
        $configuration                      = new PasswordSpecConfiguration();
        $configuration['strong_min_length'] = 9;
        $configuration['ldap_specific']     = false;

        return $configuration;
    }

    private function getAdminPasswordConfiguration(): PasswordSpecConfiguration
    {
        $configuration                     = new PasswordSpecConfiguration();
        $configuration['admin_min_length'] = 14;
        $configuration['ldap_specific']    = false;

        return $configuration;
    }

    private function getLdapPasswordConfiguration(): PasswordSpecConfiguration
    {
        $configuration                      = new PasswordSpecConfiguration();
        $configuration['strong_min_length'] = 9;
        $configuration['admin_min_length']  = 14;
        $configuration['ldap_min_length']   = 9;
        $configuration['ldap_specific']     = true;

        return $configuration;
    }

    private function buildUser(bool $with_mediuser, bool $admin, bool $ldap): CUser
    {
        $user = $this->getMockBuilder(CUser::class)
                     ->disableOriginalConstructor()
                     ->onlyMethods(['isLDAPLinked'])
                     ->getMock();

        $user->expects($this->any())->method('isLDAPLinked')->willReturn($ldap);

        $user->user_type = '14';

        if ($admin) {
            $user->user_type = '1';
        }

        if ($with_mediuser) {
            $user->_ref_mediuser = $this->buildMediuser($admin, $ldap);
        }

        return $user;
    }

    private function buildMediuser(bool $admin, bool $ldap): CMediusers
    {
        $user = $this->getMockBuilder(CMediusers::class)
                     ->disableOriginalConstructor()
                     ->onlyMethods(['isLDAPLinked'])
                     ->getMock();

        $user->expects($this->any())->method('isLDAPLinked')->willReturn($ldap);

        return $user;
    }
}
