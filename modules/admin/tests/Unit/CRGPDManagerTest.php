<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Rgpd\Tests\Unit;

use Ox\Core\CClassMap;
use Ox\Mediboard\Admin\Rgpd\CRGPDConsent;
use Ox\Mediboard\Admin\Rgpd\CRGPDException;
use Ox\Mediboard\Admin\Rgpd\CRGPDManager;
use Ox\Mediboard\Admin\Rgpd\RGPDProofContentMaker;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Tests\TestsException;
use Ox\Tests\OxUnitTestCase;

class CRGPDManagerTest extends OxUnitTestCase {

  /**
   *
   * @throws CRGPDException
   * @throws TestsException
   */
  public function testConstruct() {
    $group   = CGroups::loadCurrent();
    $manager = new CRGPDManager($group->_id);
    $this->assertInstanceOf(CRGPDManager::class, $manager);
  }

  /**
   *
   * @throws CRGPDException
   */
  public function testConstructFailed() {
    $this->expectException(CRGPDException::class);
    new CRGPDManager(null);
  }

  public function testUnicityOfNameForIRGPDProofContentMaker(): void
  {
      $proof_content_makers = CRGPDConsent::getAvailableProofContentMakers();
      $list_names = [];
      foreach ($proof_content_makers as $content_maker) {
          $list_names[] = $content_maker->getProofContentMakerName();
      }

      $this->assertCount(
          count($list_names),
          array_unique($list_names),
          "The name of each RGPDProofContentMakerInterface should be unique"
      );
  }
}
