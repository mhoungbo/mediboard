<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Tests\Functional\Controllers;

use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxWebTestCase;

class UserControllerTest extends OxWebTestCase
{

    public function testCreateUser(): CUser
    {
        $user_username = 'UserControllerTest' . uniqid();

        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/admin/users/edit',
            [
                'user_username'   => $user_username,
                'user_type'       => 14,
                'user_last_name'  => 'last_name',
                'user_first_name' => 'first_name',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            $this->translator->tr('CUser-msg-create'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $user                = new CUser();
        $user->user_username = $user_username;
        $user->loadMatchingObjectEsc();

        return $user;
    }

    /**
     * @depends testCreateUser
     */
    public function testUpdateUser(CUser $user): CUser
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/admin/users/edit',
            [
                'user_id'         => $user->_id,
                'user_first_name' => 'foo bar',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            $this->translator->tr('CUser-msg-modify'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $user = CUser::findOrFail($user->_id);
        $this->assertEquals('foo bar', $user->user_first_name);

        return $user;
    }

    /**
     * @depends testUpdateUser
     */
    public function testDeleteUser(CUser $user): void
    {
        $client = self::createClient();
        $client->request('POST', '/gui/admin/users/edit', ['del' => '1', 'user_id' => $user->_id]);

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            $this->translator->tr('CUser-msg-delete'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $this->assertFalse(CUser::find($user->_id));
    }

    public function testUpdateUserWithoutPermOnUser(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        $current_user  = CMediusers::get();
        $original_perm = CPermObject::$users_cache[$current_user->_id]['CUser'][$user->_id] ?? null;
        try {
            CPermObject::$users_cache[$current_user->_id]['CUser'][$user->_id] = 0;

            $client = self::createClient();
            $client->request(
                'POST',
                '/gui/admin/users/edit',
                [
                    'user_id'         => $user->_id,
                    'user_first_name' => 'foo bar',
                ]
            );

            $this->assertResponseStatusCodeSame(403);

            $this->assertStringContainsString(
                str_replace(
                    '/',
                    '\\/',
                    $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
                ),
                html_entity_decode($client->getResponse()->getContent())
            );
        } finally {
            if (null === $original_perm) {
                unset(CPermObject::$users_cache[$current_user->_id]['CUser'][$user->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id]['CUser'][$user->_id] = $original_perm;
            }
        }
    }

    public function testUpdateUserWithoutPermOnModule(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        $module = CModule::getActive('admin');

        $current_user  = CMediusers::get();
        $original_perm = CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] ?? null;
        $original_view = CPermModule::$users_cache[$current_user->_id][$module->_id]['view'] ?? null;
        try {
            CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] = 0;
            CPermModule::$users_cache[$current_user->_id][$module->_id]['view']       = 0;

            $client = self::createClient();
            $client->request(
                'POST',
                '/gui/admin/users/edit',
                [
                    'user_id'         => $user->_id,
                    'user_first_name' => 'foo bar',
                ]
            );

            $this->assertResponseStatusCodeSame(403);

            $this->assertStringContainsString(
                str_replace(
                    '/',
                    '\\/',
                    $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
                ),
                html_entity_decode($client->getResponse()->getContent())
            );
        } finally {
            if (null === $original_perm) {
                unset(CPermModule::$users_cache[$current_user->_id][$module->_id]);
            } else {
                CPermModule::$users_cache[$current_user->_id][$module->_id]['permission'] = $original_perm;
                CPermModule::$users_cache[$current_user->_id][$module->_id]['view']       = $original_view;
            }
        }
    }

    public function testDeleteUserNotExists(): void
    {
        $client = self::createClient();
        $client->request('POST', '/gui/admin/users/edit', ['del' => '1',]);

        $this->assertResponseStatusCodeSame(400);

        $this->assertStringContainsString(
            $this->translator->tr('CUser-error-You cannot delete an unknown user or yourself'),
            html_entity_decode($client->getResponse()->getContent())
        );
    }

    public function testDeleteSelf(): void
    {
        $client = self::createClient();
        $client->request('POST', '/gui/admin/users/edit', ['user_id' => CMediusers::get()->_id, 'del' => '1',]);

        $this->assertResponseStatusCodeSame(400);

        $this->assertStringContainsString(
            $this->translator->tr('CUser-error-You cannot delete an unknown user or yourself'),
            html_entity_decode($client->getResponse()->getContent())
        );
    }
}
