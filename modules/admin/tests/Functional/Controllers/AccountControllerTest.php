<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Tests\Functional\Controllers;

use Ox\Core\Auth\User;
use Ox\Core\CMbString;
use Ox\Core\Config\Conf;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\Admin\Services\AccountActivationService;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\SwitchUserToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class AccountControllerTest extends OxWebTestCase
{
    public function testGenerateActivationTokenNoPerm(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        $mock = $this->getMockBuilder(AccountActivationService::class)
                     ->onlyMethods(['canGenerateActivationToken'])
                     ->getMock();
        $mock->method('canGenerateActivationToken')->willReturn(false);

        $client = self::createClient();
        $client->getContainer()->set(AccountActivationService::class, $mock);

        $crawler = $client->request(
            'POST',
            '/gui/admin/account/generate-token',
            ['user_id' => $user->_id]
        );

        $this->assertResponseStatusCodeSame(403);

        $this->assertEquals(
            CMbString::purifyHTML(
                $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
            ),
            mb_convert_encoding(
                $crawler->filterXPath('//div[@class="error"]')->text(),
                'ISO-8859-1',
                'UTF-8'
            )
        );
    }

    public function testGenerateActivationTokenWithToken(): void
    {
        $token       = new CViewAccessToken();
        $token->hash = 'foobar';

        $mock = $this->getMockBuilder(AccountActivationService::class)
                     ->onlyMethods(['generateToken'])
                     ->getMock();
        $mock->method('generateToken')->willReturn($token);

        $client = self::createClient();
        $client->getContainer()->set(AccountActivationService::class, $mock);

        $client->request(
            'POST',
            '/gui/admin/account/generate-token',
            ['type' => 'token', 'user_id' => CMediusers::get()->_id]
        );

        $this->assertResponseStatusCodeSame(200);

        $external_url = str_replace('/', '\\/', rtrim((new Conf())->get('external_url'), '/') . '/');

        $response_content = $client->getResponse()->getContent();

        $this->assertStringContainsString(
            $this->translator->tr('common-msg-Here is your account activation link :'),
            $response_content
        );

        $this->assertStringContainsString(
            $external_url . '?foobar',
            $response_content
        );
    }

    public function testGenerateActivationTokenWithEmail(): void
    {
        $mock = $this->getMockBuilder(AccountActivationService::class)
                     ->onlyMethods(['sendTokenViaEmail', 'getSMTPSource'])
                     ->getMock();
        $mock->method('sendTokenViaEmail')->willReturn(true);

        $client = self::createClient();
        $client->getContainer()->set(AccountActivationService::class, $mock);

        $crawler = $client->request(
            'POST',
            '/gui/admin/account/generate-token',
            ['type' => 'email', 'user_id' => CMediusers::get()->_id, 'email' => 'foo']
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertEquals(
            $this->translator->tr('AccountController-msg-Activation mail sent.'),
            mb_convert_encoding($crawler->filterXPath('//div[@class="info"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
    }

    public function testGenerateActivationTokenWithEmailWithEmptyEmail(): void
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/admin/account/generate-token',
            ['type' => 'email', 'user_id' => CMediusers::get()->_id]
        );

        $this->assertResponseStatusCodeSame(400);

        $this->assertEquals(
            $this->translator->tr('AccountController-msg-An email must be set.'),
            mb_convert_encoding($crawler->filterXPath('//div[@class="error"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
    }

    public function testShowChangePasswordWithImpersonation(): void
    {
        $client = self::createClient();
        $client->getContainer()->set(Security::class, $this->mockSecurity($client->getContainer()));

        $client->request('GET', '/gui/admin/account', ['action' => 'change_password', 'ajax' => 1]);

        $this->assertResponseStatusCodeSame(400);

        $this->assertStringContainsString(
            $this->translator->tr('AccountController-error-You cannot change password while being impersonated'),
            $client->getResponse()->getContent()
        );
    }

    public function testUpdatePasswordWithImpersonation(): void
    {
        $client = self::createClient();
        $client->getContainer()->set(Security::class, $this->mockSecurity($client->getContainer()));

        $client->request('POST', '/gui/admin/account_update_password');

        $this->assertResponseStatusCodeSame(400);

        $this->assertStringContainsString(
            $this->translator->tr('AccountController-error-You cannot change password while being impersonated'),
            html_entity_decode($client->getResponse()->getContent())
        );
    }

    private function mockSecurity(ContainerInterface $container): Security
    {
        $original_token = $this->getMockBuilder(TokenInterface::class)->getMock();
        $security       = $this->getMockBuilder(Security::class)
                               ->onlyMethods(['getToken'])
                               ->setConstructorArgs([$container])
                               ->getMock();
        $security->method('getToken')->willReturn(
            new SwitchUserToken(new User(CUser::get()), 'main', ['none'], $original_token)
        );

        return $security;
    }
}
