<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Tests\Fixtures;

use Ox\Core\CMbDT;
use Ox\Core\CSQLDataSource;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CKerberosLdapIdentifier;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\Files\CFile;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

class AuthenticationFixtures extends Fixtures implements GroupFixturesInterface
{
    public const PASSWORD = 'P@ssw0rd17Azerty';

    public const STANDARD_USER = 'auth-std-user';
    public const LDAP_USER     = 'auth-ldap-user';

    public const API_TOKEN_USER        = 'auth-api-token-user';
    public const API_TOKEN             = 'auth-api-token';
    public const API_RESTRICTED_TOKEN  = 'auth-api-restricted-token';
    public const API_MULTI_ROUTE_TOKEN = 'auth-api-multi-route-token';

    public const KRB_USER       = 'auth-krb-user';
    public const KRB_IDENTIFIER = 'auth-krb-identifier';
    public const TEST_KRB_NAME  = 'test@ox';

    public const FC_USER           = 'auth-fc-user';
    public const TEST_VALID_FC_SUB = 'b6048e95bb134ec5b1d1e1fa69f287172e91722b9354d637a1bcf2ebb0fd2ef5v2';

    public const RPPS_USER       = 'auth-rpps-user';
    public const TEST_VALID_RPPS = '998803488266';

    public const CPX_SIGNATURE_FILE      = 'cpx_file';
    public const CPX_CERT_SIGNATURE_FILE = 'cpx_cert_file';
    public const CPX_SIGNATURE           = 'h/Bv0q+MqGZtxTslYOgKDZWScheKglbZvP+QRYM1dvxD99v5ckZEV5/G3jS5cgtPfoqbF6Y5H00vN/WpK46PnjodnbhkEp5jhHP/tfS3KicN0L/z9EBeIVuQ1QOamDTvBVLkZULX8UqbJ4TdhJnzYZx0kvuAluQpzdNF30tqOuObr7hbORsyuVFwd3boT7qJu/fFnMz5TlrtYQgxVqiLPL7/+DJjNReBqGh1qGp+5UoV6wiawMnZts6IaJdW1C6qQXdhJCRvKUWqWNiYbu8TrSzbS8pGYclIJ193deeYGR5yeLICZrfIeBdIdXI+7vbbopEuL+q8GHeT6RXIOG10Zw==';
    public const CPX_CERT_SIGNATURE      = '-----BEGIN CERTIFICATE-----
MIIIiDCCBnCgAwIBAgIQY9r/z8gDS9yhRZEHoGjerjANBgkqhkiG9w0BAQsFADB/MQswCQYDVQQGEwJGUjETMBEGA1UECgwKQVNJUC1TQU5URTEXMBUGA1UECwwOMDAwMiAxODc1MTI3NTExFzAVBgNVBAsMDklHQy1TQU5URSBURVNUMSkwJwYDVQQDDCBURVNUIEFDIElHQy1TQU5URSBGT1JUIFBFUlNPTk5FUzAeFw0yMDA5MTUxMjE3NDRaFw0yMzA5MTUxMjE3NDRaMFUxCzAJBgNVBAYTAkZSMREwDwYDVQQMDAhNw6lkZWNpbjEzMAsGA1UEKgwETkFOQTAPBgNVBAQMCEJJU1RPVVJJMBMGA1UEAwwMODk5OTAwMDg4MTY0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvGtDaculLDgaoUKiDBQt/KgqIKM4xK9AxGCOo0ddm4sGBcCeU+E7B1lslfLpStSfChpOmF4hwQuxKo6W1V1tBMiqIazvOmNcSXvWEOJFi5yBFS+Po5lLOxLweA+7a2x0wMGsIopIMmzE+MiSErmvy/Sk6P3v8pg025/ryxL48bjoqb9zvsColta0Frwgx7DnrxbAl55fry2PxZsrfH7oc2R1eU65j4ziBpbhyII0b5PAR/70KJ/hnIbGv7R2Aj4Olg1E+ffd8rVzdOvi+aqicbkQV2pTs8TqeL459g7kqgwnzjMcbLgGwCwsNbpRnYiE170fxuUCGY/fRbuv0cqEEQIDAQABo4IEKDCCBCQwCQYDVR0TBAIwADAdBgNVHQ4EFgQUAtEenmkV35LKavy5QPlR+uetBDIwHwYDVR0jBBgwFoAUOvHn9c7r7feNycmyn+3UZSncH/0wDgYDVR0PAQH/BAQDAgZAMFMGA1UdIARMMEowSAYNKoF6AYFVAQcCAQEBATA3MDUGCCsGAQUFBwIBFilodHRwOi8vaWdjLXNhbnRlLmVzYW50ZS5nb3V2LmZyL1BDJTIwVEVTVDATBgNVHSUEDDAKBggrBgEFBQcDBDArBgNVHRAEJDAigA8yMDIwMDkxNTEyMTc0NFqBDzIwMjMwOTE1MTIxNzQ0WjCCAUAGA1UdHwSCATcwggEzMDygOqA4hjZodHRwOi8vaWdjLXNhbnRlLmVzYW50ZS5nb3V2LmZyL0NSTC9BQ0ktRk8tUFAtVEVTVC5jcmwwgfKgge+ggeyGgelsZGFwOi8vYW5udWFpcmUtaWdjLmVzYW50ZS5nb3V2LmZyL2NuPVRFU1QlMjBBQyUyMElHQy1TQU5URSUyMEZPUlQlMjBQRVJTT05ORVMsb3U9VEVTVCUyMEFDJTIwUkFDSU5FJTIwSUdDLVNBTlRFJTIwRk9SVCxvdT1JR0MtU0FOVEUlMjBURVNULG91PTAwMDIlMjAxODc1MTI3NTEsbz1BU0lQLVNBTlRFLGM9RlI/Y2VydGlmaWNhdGVyZXZvY2F0aW9ubGlzdDtiaW5hcnk/YmFzZT9vYmplY3RDbGFzcz1wa2lDQTCB+gYDVR0uBIHyMIHvMIHsoIHpoIHmhoHjbGRhcDovL2FubnVhaXJlLWlnYy5lc2FudGUuZ291di5mci9jbj1URVNUJTIwQUMlMjBJR0MtU0FOVEUlMjBGT1JUJTIwUEVSU09OTkVTLG91PVRFU1QlMjBBQyUyMFJBQ0lORSUyMElHQy1TQU5URSUyMEZPUlQsb3U9SUdDLVNBTlRFJTIwVEVTVCxvdT0wMDAyJTIwMTg3NTEyNzUxLG89QVNJUC1TQU5URSxjPUZSP2RlbHRhcmV2b2NhdGlvbmxpc3Q7YmluYXJ5P2Jhc2U/b2JqZWN0Q2xhc3M9cGtpQ0EwgYAGCCsGAQUFBwEBBHQwcjAmBggrBgEFBQcwAYYaaHR0cDovL29jc3AuZXNhbnRlLmdvdXYuZnIwSAYIKwYBBQUHMAKGPGh0dHA6Ly9pZ2Mtc2FudGUuZXNhbnRlLmdvdXYuZnIvQUMlMjBURVNUL0FDSS1GTy1QUC1URVNULmNlcjAPBggqgXoBRwECBQQDBAGAMA8GCCqBegFHAQICBAMCAQAwIwYIKoF6AUcBAgMEFxMVODAyNTAwMDAwMS8yODAwNjI4NzYxMA8GCCqBegFHAQIHBAMCAQowFAYIKoF6AUcEAgUECDAGDARTTTA1MA0GCSqGSIb3DQEBCwUAA4ICAQAsp/UMsz3idY/7hbLCuDtmrm61/yGWKcjzsmz8valwq1p4UXyaQhSISY9IyKeJnk3wg+Ni3biAUg5NR4ugwCTWa90HEe/AzcflGyhEwyi4OpVh8sGfj1SugtH1aFj/bH8aVHi1lDjX0hi5txHYa3vQS3YhGo+vgTc7Bz/yLHN09oLV0FquL5q3MWSlSUdpgd4QtY1SM9aKSQM2KzkBRRfS+cSEXt+XN06dPWYQ0LrGGzU2ANEut6jxRvgXbNdxfRlLDIyt9jOJvAycUxc8DjTiYnspmMk5bwIUTjfZOzGTXCGxftDCKiFMwHremHu5EVpWPgHBkUKwsXdw5XjN53ub9bs10MmCynJIX0CE0+Qoi37khS3f9bgSM0akMq12fMYzQFWZqstu5jP3nbfhY6fHVrFwX4FqF0lMydQROsLhfzk2yJKYqh376fjAmxxEUh0WnuivEQmA6pxGwA+nJW8P/+2+zUTu8kcSOMm6YM1goDl+Jc9CwK6iAQHyUv4tX01A3oOvJd8Hsk+k+IoJbIxz9LmKGMfPL0NEbEuFWEYi9giTDIf9xX5h5PBg3DNLyYmhr35dcAju/7VpmBZBbNeC7RK0874zXESCA2AKyjxsNR7IPI8ZLunXHgsHD6XDxMh+Px4+bBvyJKa1XSpNdLwK5TeDuU6Mr0mTlAzDZceSMQ==
-----END CERTIFICATE-----';

    public const TEMPLATE_USER      = 'auth-std-template';
    public const TEMPLATE_LDAP_USER = 'auth-ldap-template';

    public const PRIMARY_USER        = 'auth-primary';
    public const SECONDARY_USER      = 'auth-std-secondary';
    public const SECONDARY_LDAP_USER = 'auth-ldap-secondary';

    public const LOCKED_USER   = 'auth-std-locked';
    public const INACTIVE_USER = 'auth-std-inactive';
    public const LOCAL_USER    = 'auth-std-local';
    public const IP_USER       = 'auth-std-ip';

    public const GUI_TOKEN      = 'auth-gui-token';
    public const GUI_TOKEN_USER = 'auth-gui-token-user';

    public const GUI_RESTRICTED_TOKEN      = 'auth-gui-restricted-token';
    public const GUI_RESTRICTED_TOKEN_USER = 'auth-gui-restricted-token-user';

    public const GUI_MULTI_ROUTE_TOKEN      = 'auth-gui-multi-route-token';
    public const GUI_MULTI_ROUTE_TOKEN_USER = 'auth-gui-multi-route-token-user';

    public const GUI_NO_ROUTE_TOKEN      = 'auth-gui-no-route-token';
    public const GUI_NO_ROUTE_TOKEN_USER = 'auth-gui-no-route-token-user';

    private const PURGEABLE_USERS = [
        self::TEST_VALID_RPPS,
        self::PRIMARY_USER,
        self::SECONDARY_USER,
        self::SECONDARY_LDAP_USER,
        self::INACTIVE_USER,
        self::LOCAL_USER,
        self::IP_USER,
    ];

    /**
     * @inheritDoc
     * @throws FixturesException
     */
    public function load(): void
    {
        $this->loadValidUsers();
        $this->loadInvalidUsers();
    }

    /**
     * @return void
     * @throws FixturesException
     */
    private function loadValidUsers(): void
    {
        // Standard user
        $std                  = new CUser();
        $std->user_username   = static::STANDARD_USER;
        $std->user_first_name = 'test';
        $std->user_last_name  = 'test';
        $std->_user_password  = static::PASSWORD;
        $std->_is_changing    = true;
        $std->user_type       = 14;
        $this->store($std, static::STANDARD_USER);
        // Ugly hack for testability purpose (IdUserProvider uses ID as identifier but username is enforced in User)
        $std->user_username = $std->_id;
        $this->store($std);

        $this->storeKrbIdentifier($std);

        // LDAP-related user
        $ldap                  = new CUser();
        $ldap->user_username   = static::LDAP_USER;
        $ldap->user_first_name = 'test';
        $ldap->user_last_name  = 'test';
        $ldap->_user_password  = static::PASSWORD;
        $ldap->_is_changing    = true;
        $ldap->user_type       = 14;
        $this->store($ldap, static::LDAP_USER);
        // Ugly hack for testability purpose (IdUserProvider uses ID as identifier but username is enforced in User)
        $ldap->user_username = $ldap->_id;
        $ldap->ldap_uid      = $ldap->_id;
        $this->store($ldap);

        // API Token
        $this->createToken(static::API_TOKEN, static::API_TOKEN_USER, 'dummy', null, false);

        // KRB identifier & user
        $krb = new CUser();
        // Because KRB as identifier lost when using Ox\Core\Auth\User
        $krb->user_username   = static::TEST_KRB_NAME;
        $krb->user_first_name = 'test';
        $krb->user_last_name  = 'test';
        $krb->user_type       = 14;
        $this->store($krb, static::KRB_USER);

        $krb_identifier           = new CKerberosLdapIdentifier();
        $krb_identifier->user_id  = $krb->_id;
        $krb_identifier->username = static::TEST_KRB_NAME;
        $this->store($krb_identifier, static::KRB_IDENTIFIER);

        // FC sub user
        $fc = new CUser();
        // Because FC sub as identifier is lost when using Ox\Core\Auth\User
        $fc->user_username   = static::TEST_VALID_FC_SUB;
        $fc->user_first_name = 'test';
        $fc->user_last_name  = 'test';
        $fc->user_type       = 14;
        $fc->sub_fc          = static::TEST_VALID_FC_SUB;
        $this->store($fc, static::FC_USER);

        // RPPS mediuser
        $rpps = $this->getUser(false);
        // Because RPPS as identifier is lost when using Ox\Core\Auth\User
        $rpps->_user_username = static::TEST_VALID_RPPS;
        $rpps->rpps           = static::TEST_VALID_RPPS;
        $this->store($rpps, static::RPPS_USER);

        // CPX signature files & mediuser
        $cpx_signature_user = $this->getUser(false);

        $signature_file               = new CFile();
        $signature_file->file_name    = 'certificat_signature.crt';
        $signature_file->object_class = $cpx_signature_user->_class;
        $signature_file->object_id    = $cpx_signature_user->_id;
        $signature_file->private      = 1;
        $signature_file->file_date    = CMbDT::dateTime();
        $signature_file->setContent(static::CPX_CERT_SIGNATURE);
        $signature_file->updateFormFields();
        $signature_file->fillFields();
        $this->store($signature_file, static::CPX_CERT_SIGNATURE_FILE);

        $cert_signature_file               = new CFile();
        $cert_signature_file->file_name    = 'certificat_auth.crt';
        $cert_signature_file->object_class = $cpx_signature_user->_class;
        $cert_signature_file->object_id    = $cpx_signature_user->_id;
        $cert_signature_file->private      = 1;
        $cert_signature_file->file_date    = CMbDT::dateTime();
        $cert_signature_file->setContent(static::CPX_SIGNATURE);
        $cert_signature_file->updateFormFields();
        $cert_signature_file->fillFields();
        $this->store($cert_signature_file, static::CPX_SIGNATURE_FILE);

        $this->createToken(static::GUI_TOKEN, static::GUI_TOKEN_USER, 'dummy', 'system_gui_about', false);

        $this->createToken(
            static::GUI_RESTRICTED_TOKEN,
            static::GUI_RESTRICTED_TOKEN_USER,
            'dummy',
            'system_gui_about',
            true
        );

        $this->createToken(
            static::GUI_MULTI_ROUTE_TOKEN,
            static::GUI_MULTI_ROUTE_TOKEN_USER,
            'dummy',
            "system_gui_about\nadmin_get_token",
            false
        );

        $this->createToken(
            static::GUI_NO_ROUTE_TOKEN,
            static::GUI_NO_ROUTE_TOKEN_USER,
            'dummy',
            '',
            false
        );
    }

    private function loadInvalidUsers(): void
    {
        // Template
        $template                  = new CUser();
        $template->user_username   = static::TEMPLATE_USER;
        $template->user_first_name = 'test';
        $template->user_last_name  = 'test';
        $template->user_type       = 14;
        $template->_user_password  = static::PASSWORD;
        $template->_is_changing    = true;
        $template->template        = '1';
        $this->store($template, static::TEMPLATE_USER);

        $this->storeKrbIdentifier($template);

        // Template (LDAP related)
        $ldap_template                  = new CUser();
        $ldap_template->user_username   = static::TEMPLATE_LDAP_USER;
        $ldap_template->user_first_name = 'test';
        $ldap_template->user_last_name  = 'test';
        $ldap_template->user_type       = 14;
        $ldap_template->_user_password  = static::PASSWORD;
        $ldap_template->_is_changing    = true;
        $ldap_template->template        = '1';
        $ldap_template->ldap_uid        = 'ldap uid';
        $this->store($ldap_template, static::TEMPLATE_LDAP_USER);

        // Secondary
        $primary = $this->getUser(false);
        $this->store($primary, static::PRIMARY_USER);

        $secondary                 = $this->getUser(false);
        $secondary->main_user_id   = $primary->_id;
        $secondary->_user_password = static::PASSWORD;
        $this->store($secondary, static::SECONDARY_USER);

        $this->storeKrbIdentifier($secondary->loadRefUser());

        // Secondary (LDAP related)
        $ldap_secondary = $this->getUser(false);
        $ldap_secondary->loadRefUser();
        $ldap_secondary->main_user_id        = $primary->_id;
        $ldap_secondary->_user_password      = static::PASSWORD;
        $ldap_secondary->_ref_user->ldap_uid = 'ldap uid';
        $ldap_secondary->_ref_user->store();
        $this->store($ldap_secondary, static::SECONDARY_LDAP_USER);

        // Locked
        $locked                  = new CUser();
        $locked->user_username   = static::LOCKED_USER;
        $locked->user_first_name = 'test';
        $locked->user_last_name  = 'test';
        $locked->user_type       = 14;
        $locked->_user_password  = static::PASSWORD;
        $locked->_is_changing    = true;
        $this->store($locked, static::LOCKED_USER);
        // Storing these properties after password update
        $locked->user_login_errors = 100;
        $locked->lock_datetime     = CMbDT::dateTime();
        $locked->store();

        $this->storeKrbIdentifier($locked);

        // Inactive
        $inactive                 = $this->getUser(false);
        $inactive->_user_password = static::PASSWORD;
        $inactive->actif          = '0';
        $inactive->fin_activite   = CMbDT::date('-1 day');
        $this->store($inactive, static::INACTIVE_USER);

        $this->storeKrbIdentifier($inactive->loadRefUser());

        // No remote access
        $local                 = $this->getUser(false);
        $local->_user_password = static::PASSWORD;
        $local->remote         = '1';
        $this->store($local, static::LOCAL_USER);

        $this->storeKrbIdentifier($local->loadRefUser());

        // Ip not allowed
        $ip                 = $this->getUser(false);
        $ip->_user_password = static::PASSWORD;
        $this->store($ip, static::IP_USER);

        $this->storeKrbIdentifier($ip->loadRefUser());
    }

    private function storeKrbIdentifier(CUser $user): void
    {
        $krb_identifier           = new CKerberosLdapIdentifier();
        $krb_identifier->user_id  = $user->_id;
        $krb_identifier->username = $user->user_username;

        $this->store($krb_identifier);
    }

    /**
     * @param string      $token_tag
     * @param string      $user_tag
     * @param string      $params
     * @param string|null $route_name
     * @param bool        $restricted
     *
     * @return void
     * @throws FixturesException
     */
    private function createToken(
        string  $token_tag,
        string  $user_tag,
        string  $params,
        ?string $route_name,
        bool    $restricted
    ): void {
        $token_user                  = new CUser();
        $token_user->user_username   = $token_tag;
        $token_user->user_first_name = 'test';
        $token_user->user_last_name  = 'test';
        $token_user->user_type       = 14;
        $this->store($token_user, $user_tag);

        $perm             = new CPermModule();
        $perm->user_id    = $token_user->_id;
        $perm->mod_id     = CModule::getActive('system')->_id;
        $perm->permission = 2;
        $perm->view       = 2;
        $this->store($perm);

        $token               = new CViewAccessToken();
        $token->params       = $params;
        $token->user_id      = $token_user->_id;
        $token->restricted   = ($restricted) ? 1 : 0;
        $token->datetime_end = CMbDT::dateTime('+5 days');
        $token->routes_names = $route_name;
        $this->store($token, $token_tag);
        // Because token as identifier is lost when using Ox\Core\Auth\User
        $token_user->user_username = $token->hash;
        $this->store($token_user);
    }

    /**
     * @inheritDoc
     */
    public function purge(): void
    {
        parent::purge();

        $users = (new CUser())->loadList(['user_username' => CSQLDataSource::prepareIn(static::PURGEABLE_USERS)]);

        foreach ($users as $user) {
            if ($msg = $user->purge()) {
                throw new FixturesException($msg);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public static function getGroup(): array
    {
        return ['authentication', 100];
    }
}
