/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/* eslint-disable camelcase */

export interface AuthPageAssets {
    bg_src: string
    logo_src: string
}

export interface AuthPageBrowserCheck {
    is_login_browser_check: boolean
    server_side_error: boolean
}

export interface AuthPageBrandingButtons {
    cps_endpoint?: string
    krb_endpoint?: string
    psc_endpoint?: string
}

export interface AuthPageInstanceData {
    release_title: string | null
}
