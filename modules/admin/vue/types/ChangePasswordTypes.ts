/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/* eslint-disable camelcase */

export interface ChangePasswordSpec {
    alpha: boolean
    max_length: number
    min_length: number
    not_containing: string | null
    not_near: string | null
    num: boolean
    special: boolean
    upper: boolean
}

export interface ChangePasswordSpecDetailsAttr {
    description: string
    valid: boolean
    checkValidation: () => boolean
}

export interface ChangePasswordSpecDetails {
    alpha?: ChangePasswordSpecDetailsAttr
    minLength: ChangePasswordSpecDetailsAttr
    notContaining: ChangePasswordSpecDetailsAttr
    notNear: ChangePasswordSpecDetailsAttr
    num?: ChangePasswordSpecDetailsAttr
    special?: ChangePasswordSpecDetailsAttr
    upper?: ChangePasswordSpecDetailsAttr
}
