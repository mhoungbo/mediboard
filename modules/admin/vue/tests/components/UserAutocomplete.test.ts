/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { mount, shallowMount } from "@vue/test-utils"
import Vuetify from "vuetify"
import UserAutocomplete from "@modules/admin/vue/components/UserAutocomplete/UserAutocomplete.vue"
import User from "@modules/admin/vue/models/User"

/* eslint-disable dot-notation */

/**
 * Test for UserAutocomplete Component
 */
class UserAutocompleteTest extends OxTest {
    protected component = UserAutocomplete

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return shallowMount(this.component, {
            propsData: props,
            vuetify: new Vuetify()
        })
    }

    public testMount () {
        const user = new User()
        user.firstName = "Test"
        user.lastName = "Foo"
        user.username = "TFoo"
        const wrapper = mount(this.component, {
            propsData: {
                user
            }
        })
        expect(wrapper.html()).toBe(
            '<div class="UserAutocomplete">\n' +
            '  <div class="UserAutocomplete-content">\n' +
            '    <div class="UserAutocomplete-title">\n' +
            "      Test Foo\n" +
            "    </div>\n" +
            '    <div class="UserAutocomplete-infos">\n' +
            "      TFoo\n" +
            "    </div>\n" +
            "  </div>\n" +
            "</div>"
        )
    }
}

(new UserAutocompleteTest()).launchTests()
