/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { shallowMount } from "@vue/test-utils"
import SecurityIndicator from "@modules/admin/vue/components/SecurityIndicator/SecurityIndicator.vue"
import Vuetify from "vuetify"

/* eslint-disable dot-notation */

/**
 * Test for SecurityIndicator Component
 */
export default class SecurityIndicatorTest extends OxTest {
    protected component = SecurityIndicator

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return shallowMount(this.component, {
            propsData: props,
            vuetify: new Vuetify()
        })
    }

    /**
     * Matching "default" color test
     */
    public testMatchingDefaultColor () {
        const changePass = this.mountComponent({
            nbMatchingSteps: 0,
            totalSteps: 6
        })

        this.assertEqual(this.privateCall(changePass.vm, "matchingColor"), "default")
    }

    /**
     * Matching "weak" color test
     */
    public testMatchingWeakColor () {
        const changePass = this.mountComponent({
            nbMatchingSteps: 3,
            totalSteps: 6
        })

        this.assertEqual(this.privateCall(changePass.vm, "matchingColor"), "weak")
    }

    /**
     * Matching "average" color test
     */
    public testMatchingAverageColor () {
        const changePass = this.mountComponent({
            nbMatchingSteps: 4,
            totalSteps: 6
        })

        this.assertEqual(this.privateCall(changePass.vm, "matchingColor"), "average")
    }

    /**
     * Matching "good" color test
     */
    public testMatchingGoodColor () {
        const changePass = this.mountComponent({
            nbMatchingSteps: 6,
            totalSteps: 6
        })

        this.assertEqual(this.privateCall(changePass.vm, "matchingColor"), "good")
    }

    /**
     * Is colorized step test
     */
    public testIsColorizedStep () {
        const changePass = this.mountComponent({
            nbMatchingSteps: 3,
            totalSteps: 6
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "isColorized", 1),
            { colorized: true }
        )
    }

    /**
     * Is not colorized step test
     */
    public testIsNotColorizedStep () {
        const changePass = this.mountComponent({
            nbMatchingSteps: 3,
            totalSteps: 6
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "isColorized", 4),
            { colorized: false }
        )
    }
}

(new SecurityIndicatorTest()).launchTests()
