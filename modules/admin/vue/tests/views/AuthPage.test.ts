/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import AuthPage from "@modules/admin/vue/views/AuthPage/AuthPage.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import Vuetify from "vuetify"
import * as OxGUIManager from "@/core/utils/OxGUIManager"
import { keyConfigs } from "@/core/utils/OxInjector"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * Test for AuthPage Component
 */
export default class AuthPageTest extends OxTest {
    protected component = AuthPage

    private loginData1 = {
        assets: {
            logo_src: "style/mediboard_ext/images/pictures/logoMb.png",
            bg_src: "images/pictures/bg_custom.jpg"
        },
        browserCheck: {
            is_login_browser_check: true,
            server_side_error: true
        },
        brandingButtons: {
            psc_endpoint: "/gui/oauth2/psc/login",
            cps_endpoint: "/gui/cpx/login"
        },
        links: {
            call_url: "/gui/login",
            psc_cgu_link: "/psc_cgu"
        },
        meta: {
            release_title: "February 2022"
        },
        tokens: {
            login: "79006cf5419d457e986.ElmUf4ZdVLs28XMceraG4P_tEtFPk2-q1SLK0BYqoGA.Vm7YTvMPIvh8pURrM4fZqLeAeroA8Ryev0W75l5D7SQgCeMN7yce_VfDMg"
        },
        internalLogs: [],
        externalLogs: [],
        reconnect: ""
    }

    private loginData2 = {
        assets: {
            logo_src: "",
            bg_src: ""
        },
        browserCheck: {
            is_login_browser_check: true,
            server_side_error: false
        },
        brandingButtons: {},
        links: {
            call_url: "/gui/login"
        },
        meta: {
            release_title: "February 2022"
        },
        tokens: {
            login: "79006cf5419d457e986.ElmUf4ZdVLs28XMceraG4P_tEtFPk2-q1SLK0BYqoGA.Vm7YTvMPIvh8pURrM4fZqLeAeroA8Ryev0W75l5D7SQgCeMN7yce_VfDMg"
        },
        internalLogs: ["log 1", "log 2"],
        externalLogs: ["log 1", "log 2"],
        reconnect: ""
    }

    protected beforeTest () {
        super.beforeTest()

        // Resetting cookie between each test
        document.cookie = "login-username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return shallowMount(this.component, {
            propsData: props,
            provide: {
                [keyConfigs]: {
                    product_name: "Mediboard",
                    company_name: "OpenXtrem",
                    is_qualif: true
                }
            },
            localVue,
            vuetify: new Vuetify()
        })
    }

    /**
     * Custom background image is defined test
     */
    public testBgCustomDefined () {
        const authPage = this.mountComponent({
            assets: this.loginData1.assets,
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        this.assertEqual(
            this.privateCall(authPage.vm, "bgCustom"),
            { backgroundImage: "url('images/pictures/bg_custom.jpg')" }
        )
    }

    /**
     * Custom background image is not defined test
     */
    public testBgCustomNotDefined () {
        const authPage = this.mountComponent({
            assets: this.loginData2.assets,
            links: this.loginData2.links,
            meta: this.loginData2.meta,
            tokens: this.loginData2.tokens
        })

        this.assertEqual(this.privateCall(authPage.vm, "bgCustom"), {})
    }

    /**
     * Browser is not compatible test
     */
    public testIsNotBrowserCompatible () {
        const authPage = this.mountComponent({
            browserCheck: this.loginData2.browserCheck,
            links: this.loginData2.links,
            meta: this.loginData2.meta,
            tokens: this.loginData2.tokens
        })

        this.assertFalse(this.privateCall(authPage.vm, "isBrowserCompatible"))
    }

    /**
     * Browser is compatible test
     */
    public testIsBrowserCompatible () {
        const authPage = this.mountComponent({
            browserCheck: this.loginData1.browserCheck,
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        this.assertTrue(this.privateCall(authPage.vm, "isBrowserCompatible"))
    }

    /**
     * Branding buttons is not empty test
     */
    public testIsNotBrandingButtonsEmpty () {
        const authPage = this.mountComponent({
            browserCheck: this.loginData1.browserCheck,
            brandingButtons: this.loginData1.brandingButtons,
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        this.assertFalse(this.privateCall(authPage.vm, "isBrandingButtonsEmpty"))
    }

    /**
     * Branding buttons is undefined test
     */
    public testIsBrandingButtonsUndefined () {
        const authPage = this.mountComponent({
            browserCheck: this.loginData1.browserCheck,
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        this.assertTrue(this.privateCall(authPage.vm, "isBrandingButtonsEmpty"))
    }

    /**
     * Branding buttons is empty test
     */
    public testIsBrandingButtonsEmpty () {
        const authPage = this.mountComponent({
            browserCheck: this.loginData2.browserCheck,
            brandingButtons: this.loginData2.brandingButtons,
            links: this.loginData2.links,
            meta: this.loginData2.meta,
            tokens: this.loginData2.tokens
        })

        this.assertTrue(this.privateCall(authPage.vm, "isBrandingButtonsEmpty"))
    }

    /**
     * Login page has internal logs test
     */
    public testHasInternalLogs () {
        const authPage = this.mountComponent({
            links: this.loginData2.links,
            meta: this.loginData2.meta,
            tokens: this.loginData2.tokens,
            internalLogs: this.loginData2.internalLogs
        })

        this.assertTrue(this.privateCall(authPage.vm, "hasInternalLogs"))
    }

    /**
     * Login page has not internal logs test
     */
    public testHasNotInternalLogs () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens,
            internalLogs: this.loginData1.internalLogs
        })

        this.assertFalse(this.privateCall(authPage.vm, "hasInternalLogs"))
    }

    /**
     * Login page has external logs test
     */
    public testHasExternalLogs () {
        const authPage = this.mountComponent({
            links: this.loginData2.links,
            meta: this.loginData2.meta,
            tokens: this.loginData2.tokens,
            externalLogs: this.loginData2.internalLogs
        })

        this.assertTrue(this.privateCall(authPage.vm, "hasExternalLogs"))
    }

    /**
     * Login page has not external logs test
     */
    public testHasNotExternalLogs () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens,
            externalLogs: this.loginData1.internalLogs
        })

        this.assertFalse(this.privateCall(authPage.vm, "hasExternalLogs"))
    }

    /**
     * Screen size is not in mobile size (>= 600px)
     */
    public testScreenSizeIsNotMobileSize () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["screenWidth"] = 610

        this.assertFalse(this.privateCall(authPage.vm, "isMobileSize"))
    }

    /**
     * Screen size is in mobile size (< 600px)
     */
    public testScreenSizeIsMobileSize () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["screenWidth"] = 600

        this.assertTrue(this.privateCall(authPage.vm, "isMobileSize"))
    }

    /**
     * Forgot password icon's wrapper size in mobile size
     */
    public testForgotPassIconWrapperSizeMobile () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["screenWidth"] = 600

        this.assertEqual(this.privateCall(authPage.vm, "avatarSize"), 104)
    }

    /**
     * Forgot password icon's wrapper size in desktop size
     */
    public testForgotPassIconWrapperSizeDesktop () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["screenWidth"] = 610

        this.assertEqual(this.privateCall(authPage.vm, "avatarSize"), 76)
    }

    /**
     * Forgot password icon size in mobile size
     */
    public testForgotPassIconSizeMobile () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["screenWidth"] = 600

        this.assertEqual(this.privateCall(authPage.vm, "iconSize"), 50)
    }

    /**
     * Forgot password icon size in desktop size
     */
    public testForgotPassIconSizeDesktop () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["screenWidth"] = 610

        this.assertEqual(this.privateCall(authPage.vm, "iconSize"), 40)
    }

    /**
     * Forgot password dialog width in mobile size
     */
    public testForgotPassDialogWidthMobile () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["screenWidth"] = 600

        this.assertEqual(this.privateCall(authPage.vm, "forgotPasswordDialogWidth"), undefined)
    }

    /**
     * Forgot password dialog width in desktop size
     */
    public testForgotPassDialogWidthDesktop () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["screenWidth"] = 610

        this.assertEqual(this.privateCall(authPage.vm, "forgotPasswordDialogWidth"), 606)
    }

    /**
     * Set username in cookie if rememberMe checkbox is checked
     */
    public testAddUsernameInStorage () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["rememberMe"] = true
        authPage.vm["username"] = "johndoe"
        this.privateCall(authPage.vm, "setUsernameStorage")

        this.assertEqual(document.cookie, "login-username=johndoe")
    }

    /**
     * Remove username from cookie if rememberMe checkbox is not checked
     */
    public testDeleteUsernameFromStorage () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        authPage.vm["rememberMe"] = false
        authPage.vm["username"] = "johndoe"
        this.privateCall(authPage.vm, "setUsernameStorage")

        this.assertEqual(document.cookie, "")
    }

    /**
     * PSC URL redirection test
     */
    public testPscConnect () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        const hrefSpy = jest.fn()
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            set: hrefSpy,
            get: () => "baseHref"
        })

        this.privateCall(authPage.vm, "pscConnect", "/gui/psc/endpoint")
        this.assertTrue(this.privateCall(authPage.vm, "pscFormLoading"))
        expect(hrefSpy).toHaveBeenCalledWith("/gui/psc/endpoint")
    }

    /**
     * Script tag generation test
     */
    public testCreateScriptTag () {
        const authPage = this.mountComponent({
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })

        const scriptPath = "path/to/script.js"
        const script = document.createElement("script")
        script.src = scriptPath
        script.type = "text/javascript"

        this.assertEqual(
            this.privateCall(authPage.vm, "createScriptTag", scriptPath),
            script
        )
    }

    /**
     * PSC CGU display test
     */
    public async testDisplayPscCguModal () {
        const spyModal = jest.spyOn(OxGUIManager, "loadGUIEntrypointIframe")
        const authPage = this.mountComponent({
            brandingButtons: this.loginData1.brandingButtons,
            links: this.loginData1.links,
            meta: this.loginData1.meta,
            tokens: this.loginData1.tokens
        })
        await this.privateCall(authPage.vm, "displayPscCguModal")
        expect(spyModal).toHaveBeenCalledWith("/psc_cgu", expect.anything())
        expect(authPage.vm["cguDialog"]).toBe(true)
    }
}

(new AuthPageTest()).launchTests()
