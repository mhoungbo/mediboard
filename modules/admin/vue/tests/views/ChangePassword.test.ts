/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import ChangePassword from "@modules/admin/vue/views/ChangePassword/ChangePassword.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import Vuetify from "vuetify"
import { keyConfigs } from "@/core/utils/OxInjector"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * Test for ChangePassword Component
 */
export default class ChangePasswordTest extends OxTest {
    protected component = ChangePassword

    private cpData1 = {
        links: {
            call_url: "/gui/admin/account_update_password",
            logout: "/gui/logout"
        },
        spec: {
            min_length: 6,
            max_length: 80,
            alpha: true,
            upper: true,
            num: true,
            special: false,
            not_containing: "fdaubanes",
            not_near: "fdaubanes"
        },
        tokens: {
            token: "0383205b5.iHaA5hYVPNZLjYz_AyZXVmnBS3lLuzWkIx8Iyn6wF5c.2ADXqiQtfZEevvSNXGcPNCGVAEoC9F3sFGYlpC-Ddub6W_LSQmRriRH6uA"
        },
        domainLogs: [],
        errorLogs: [],
        requestChangePassword: ""
    }

    private cpData2 = {
        links: {
            call_url: "/gui/admin/account_update_password",
            logout: "/gui/logout"
        },
        spec: {
            min_length: 6,
            max_length: 80,
            alpha: true,
            upper: true,
            num: true,
            special: false,
            not_containing: "fdaubanes",
            not_near: "fdaubanes"
        },
        tokens: {
            token: "0383205b5.iHaA5hYVPNZLjYz_AyZXVmnBS3lLuzWkIx8Iyn6wF5c.2ADXqiQtfZEevvSNXGcPNCGVAEoC9F3sFGYlpC-Ddub6W_LSQmRriRH6uA"
        },
        domainLogs: ["log 1", "log 2"],
        errorLogs: ["log 1", "log 2"],
        requestChangePassword: ""
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return shallowMount(this.component, {
            propsData: props,
            provide: {
                [keyConfigs]: {
                    product_name: "Mediboard",
                    custom_recommendations: ""
                }
            },
            localVue,
            vuetify: new Vuetify()
        })
    }

    /**
     * Detail popin's title with invalid password test
     */
    public testDetailPopinTitleInvalid () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "detailPopinTitle"),
            "common-error-Invalid password"
        )
    }

    /**
     * Detail popin's title with correct password test
     */
    public testDetailPopinTitleCorrect () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        this.privateCall(changePass.vm, "checkPassword", "aB1234")
        this.assertEqual(
            this.privateCall(changePass.vm, "detailPopinTitle"),
            "common-msg-Correct password"
        )
    }

    /**
     * Screen size is not in mobile size (>= 600px)
     */
    public testScreenSizeIsNotMobileSize () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        changePass.vm["screenWidth"] = 610

        this.assertFalse(this.privateCall(changePass.vm, "isMobileSize"))
    }

    /**
     * Screen size is in mobile size (< 600px)
     */
    public testScreenSizeIsMobileSize () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        changePass.vm["screenWidth"] = 600

        this.assertTrue(this.privateCall(changePass.vm, "isMobileSize"))
    }

    /**
     * Screen size is not in tiny mobile size (>= 500px)
     */
    public testScreenSizeIsNotTinyMobileSize () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        changePass.vm["screenWidth"] = 510

        this.assertFalse(this.privateCall(changePass.vm, "isTinyMobileSize"))
    }

    /**
     * Screen size is in tiny mobile size (< 500px)
     */
    public testScreenSizeIsTinyMobileSize () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        changePass.vm["screenWidth"] = 500

        this.assertTrue(this.privateCall(changePass.vm, "isTinyMobileSize"))
    }

    /**
     * Detail popin's arrow direction in mobile size
     */
    public testArrowDirectionMobileSize () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        changePass.vm["screenWidth"] = 600

        this.assertEqual(this.privateCall(changePass.vm, "arrowDirection"), "down")
    }

    /**
     * Detail popin's arrow direction in desktop size
     */
    public testArrowDirectionDesktopSize () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        changePass.vm["screenWidth"] = 610

        this.assertEqual(this.privateCall(changePass.vm, "arrowDirection"), "left")
    }

    /**
     * Change password page has domain logs test
     */
    public testHasDomainLogs () {
        const changePass = this.mountComponent({
            links: this.cpData2.links,
            spec: this.cpData2.spec,
            tokens: this.cpData2.tokens,
            domainLogs: this.cpData2.domainLogs
        })

        this.assertTrue(this.privateCall(changePass.vm, "hasDomainLogs"))
    }

    /**
     * Change password page has not domain logs test
     */
    public testHasNotDomainLogs () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            domainLogs: this.cpData1.domainLogs
        })

        this.assertFalse(this.privateCall(changePass.vm, "hasDomainLogs"))
    }

    /**
     * Change password page has error logs test
     */
    public testHasErrorLogs () {
        const changePass = this.mountComponent({
            links: this.cpData2.links,
            spec: this.cpData2.spec,
            tokens: this.cpData2.tokens,
            errorLogs: this.cpData2.errorLogs
        })

        this.assertTrue(this.privateCall(changePass.vm, "hasErrorLogs"))
    }

    /**
     * Change password page has not error logs test
     */
    public testHasNotErrorLogs () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            errorLogs: this.cpData1.errorLogs
        })

        this.assertFalse(this.privateCall(changePass.vm, "hasErrorLogs"))
    }

    /**
     * Is change password forced test
     */
    public testIsChangeForced () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            requestChangePassword: ""
        })

        this.assertTrue(this.privateCall(changePass.vm, "isChangeForced"))
    }

    /**
     * Is change password not forced test
     */
    public testIsNotChangeForced () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            requestChangePassword: "1"
        })

        this.assertFalse(this.privateCall(changePass.vm, "isChangeForced"))
    }

    /**
     * Is account activation test
     */
    public testIsActivation () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            action: "activate_account"
        })

        expect(this.privateCall(changePass.vm, "isActivation")).toBe(true)
    }

    /**
     * Is not account activation test
     */
    public testIsNotActivation () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        expect(this.privateCall(changePass.vm, "isActivation")).toBe(false)
    }

    /**
     * Change password forced subtitle test
     */
    public testIsChangeForcedSubtitle () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            requestChangePassword: ""
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "subtitle"),
            "common-msg-Expiration of your password"
        )
    }

    /**
     * Requested change password subtitle test
     */
    public testRequestChangePasswordSubtitle () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            requestChangePassword: "1"
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "subtitle"),
            "CUser-user_password-new"
        )
    }

    public testAccountActivationSubtitle () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            action: "activate_account"
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "subtitle"),
            "common-msg-Initialize password"
        )
    }

    /**
     * Change password forced description test
     */
    public testIsChangeForcedDescription () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            requestChangePassword: ""
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "description"),
            "common-msg-Your actual password has expired."
        )
    }

    public testRequestedChangePasswordDescription () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            requestChangePassword: "1"
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "description"),
            "common-msg-Choose a new password"
        )
    }

    public testAccountActivationDescription () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens,
            action: "activate_account"
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "description"),
            "common-msg-Choose a new password"
        )
    }

    /**
     * Logout redirection test
     */
    public testLogout () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        const hrefSpy = jest.fn()
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            set: hrefSpy,
            get: () => "baseHref"
        })

        this.privateCall(changePass.vm, "logout")
        expect(hrefSpy).toBeCalledWith(expect.stringContaining("/logout"))
    }

    /**
     * Opening of the popin containing password spec details
     */
    public testShowDetailPopin () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        this.privateCall(changePass.vm, "openDetailPopin")
        this.assertTrue(this.privateCall(changePass.vm, "showDetailPopin"))
    }

    /**
     * Closing of the popin containing password spec details
     */
    public testCloseDetailPopin () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        this.privateCall(changePass.vm, "closeDetailPopin")
        this.assertFalse(this.privateCall(changePass.vm, "showDetailPopin"))
    }

    /**
     * Invalidated criteria test
     */
    public testIsCriteriaInvalid () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        this.assertEqual(
            this.privateCall(changePass.vm, "isCriteriaDone", "alpha"),
            { done: false }
        )
    }

    /**
     * Validated criteria test
     */
    public testIsCriteriaValid () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        this.privateCall(changePass.vm, "checkPassword", "aB1234")
        this.assertEqual(
            this.privateCall(changePass.vm, "isCriteriaDone", "alpha"),
            { done: true }
        )
    }

    /**
     * Reset all validation when password value is empty
     */
    public testResetAllValidation () {
        const changePass = this.mountComponent({
            links: this.cpData1.links,
            spec: this.cpData1.spec,
            tokens: this.cpData1.tokens
        })

        this.privateCall(changePass.vm, "checkPassword", "aB1234")
        this.privateCall(changePass.vm, "checkPassword", "")
        this.assertEqual(
            this.privateCall(changePass.vm, "isCriteriaDone", "alpha"),
            { done: false }
        )
    }
}

(new ChangePasswordTest()).launchTests()
