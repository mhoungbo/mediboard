import ChangePassword from "@modules/admin/vue/views/ChangePassword/ChangePassword.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("ChangePassword", ChangePassword)
