import AuthPage from "@modules/admin/vue/views/AuthPage/AuthPage.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("AuthPage", AuthPage)
