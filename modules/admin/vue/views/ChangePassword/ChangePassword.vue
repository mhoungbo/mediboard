<!--
  @author  SAS OpenXtrem <dev@openxtrem.com>
  @license https://www.gnu.org/licenses/gpl.html GNU General Public License
  @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
-->

<script lang="ts">
import Vue, { PropType } from "vue"
import { OxEntryPointLinks, OxEntryPointTokens } from "@/core/types/OxEntryPointTypes"
import OxToken from "@/core/components/OxToken/OxToken.vue"
import { ChangePasswordSpec, ChangePasswordSpecDetails } from "@modules/admin/vue/types/ChangePasswordTypes"
import SecurityIndicator from "@modules/admin/vue/components/SecurityIndicator/SecurityIndicator.vue"
import DetailPopin from "@modules/admin/vue/components/DetailPopin/DetailPopin.vue"
import { levenshtein } from "@/core/utils/OxFunctions"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"
import { configs, injectConfigs } from "@/core/utils/OxInjector"
import OxPassword from "@oxify/components/OxField/OxPassword/OxPassword.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"
import OxCaution from "@oxify/components/OxCaution/OxCaution.vue"
import OxForm from "@oxify/components/OxForm/OxForm.vue"
import OxTextField from "@oxify/components/OxField/OxTextField/OxTextField.vue"
import OxFormValidator from "@oxify/components/OxForm/OxFormValidator"
import { screenSmall } from "@oxify/utils/responsive"
import OxDivider from "@oxify/components/OxDivider/OxDivider.vue"
import { convertMarkdownToHTML } from "@/core/utils/OxMarkdown"

export default Vue.extend({
    name: "ChangePassword",
    components: {
        OxDivider,
        OxPassword,
        OxButton,
        OxIcon,
        OxCaution,
        OxToken,
        SecurityIndicator,
        DetailPopin,
        OxForm,
        OxTextField
    },
    inject: injectConfigs(),
    props: {
        links: {
            type: Object as PropType<OxEntryPointLinks>,
            required: true
        },
        spec: {
            type: Object as PropType<ChangePasswordSpec>,
            required: true
        },
        tokens: {
            type: Object as PropType<OxEntryPointTokens>,
            required: true
        },
        domainLogs: Array as PropType<Array<string>>,
        errorLogs: Array as PropType<Array<string>>,
        requestChangePassword: String,
        action: String,
        username: String
    },
    data () {
        return {
            confirmPassword: "",
            newPassword: "",
            oldPassword: "",
            totalStepsToDisplay: 0,
            screenWidth: 0,
            specDetails: {} as ChangePasswordSpecDetails,
            showDetailPopin: false,
            formValidator: new OxFormValidator(),
            configs: this[configs]
        }
    },
    computed: {
        detailPopinTitle (): string {
            return this.matchingSteps === this.totalStepsToDisplay
                ? this.$tr("common-msg-Correct password")
                : this.$tr("common-error-Invalid password")
        },
        matchingSteps (): number {
            return Object.values(this.specDetails).filter(criteria => criteria.valid).length
        },
        isMobileSize (): boolean {
            // Screen max width = 600px
            return this.screenWidth <= screenSmall["300"]
        },
        isTinyMobileSize (): boolean {
            // Screen max width = 500px
            return this.screenWidth <= screenSmall["200"]
        },
        arrowDirection (): string {
            return this.isMobileSize ? "down" : "left"
        },
        hasErrorLogs (): boolean {
            return !!this.errorLogs?.length
        },
        hasDomainLogs (): boolean {
            return !!this.domainLogs?.length
        },
        isChangeForced (): boolean {
            return this.requestChangePassword !== "1"
        },
        isActivation (): boolean {
            return this.action === "activate_account"
        },
        subtitle (): string {
            if (this.isActivation) {
                return this.$tr("common-msg-Initialize password")
            }
            if (this.isChangeForced) {
                return this.$tr("common-msg-Expiration of your password")
            }
            return this.$tr("CUser-user_password-new")
        },
        description (): string {
            if (this.isActivation) {
                return this.$tr("common-msg-Choose a new password")
            }
            if (this.isChangeForced) {
                return this.$tr("common-msg-Your actual password has expired.")
            }
            return this.$tr("common-msg-Choose a new password")
        },
        customRecommandationsMarkdown (): string {
            if (!this.configs.custom_recommendations) {
                return ""
            }
            return this.convertMarkdownToHTML(this.configs.custom_recommendations, {
                html: false,
                xhtmlOut: false,
                breaks: false,
                linkify: true,
                typographer: false
            })
        },
        showCustomRecommandations (): boolean {
            return this.customRecommandationsMarkdown !== ""
        }
    },
    created () {
        // Responsive layout
        window.addEventListener("resize", () => {
            this.screenWidth = window.innerWidth
        })

        this.screenWidth = window.innerWidth

        for (const criteria in this.spec) {
            /*
             * We only want to display "active" criteria
             *   => max_length is managed in the OxPassword component by a prop
             */
            if (this.spec[criteria] !== false &&
                this.spec[criteria] !== null &&
                criteria !== "max_length"
            ) {
                this.totalStepsToDisplay++
            }
        }

        this.formValidator.rules = {
            new_password1: [
                () => this.matchingSteps === this.totalStepsToDisplay || this.$tr("common-error-Invalid password")
            ],
            new_password2: [
                (v) => (v || "") === this.newPassword || this.$tr("CUser-user_password-nomatch")
            ]
        }

        this.initSpecDetails()
    },
    methods: {
        convertMarkdownToHTML,
        initSpecDetails (): void {
            if (this.spec.alpha) {
                this.$set(this.specDetails, "alpha", {
                    description: this.$tr("common-error-Must contain at least one character (without diacritic)"),
                    valid: false,
                    checkValidation: () => !!this.newPassword.match(/[A-z]/)
                })
            }

            if (this.spec.min_length) {
                this.$set(this.specDetails, "minLength", {
                    description: this.$tr("common-error-Invalid minimal length specification (length = %s)"),
                    valid: false,
                    checkValidation: () => this.newPassword.length >= this.spec.min_length
                })
            }

            if (this.spec.not_containing) {
                this.$set(this.specDetails, "notContaining", {
                    description: this.$tr("common-error-Must not contain %s"),
                    valid: false,
                    checkValidation: () => this.newPassword.indexOf(this.spec.not_containing as string) === -1
                })
            }

            if (this.spec.not_near) {
                this.$set(this.specDetails, "notNear", {
                    description: this.$tr("common-error-Looks like too much to %s"),
                    valid: false,
                    checkValidation: () => levenshtein(this.newPassword, this.spec.not_near as string) > 2
                })
            }

            if (this.spec.num) {
                this.$set(this.specDetails, "num", {
                    description: this.$tr("common-error-Must contain at least one number"),
                    valid: false,
                    checkValidation: () => !!this.newPassword.match(/[0-9]/)
                })
            }

            if (this.spec.special) {
                this.$set(this.specDetails, "special", {
                    description: this.$tr("common-error-Must contain at least one special character"),
                    valid: false,
                    checkValidation: () => !!this.newPassword.match(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/) // eslint-disable-line
                })
            }

            if (this.spec.upper) {
                this.$set(this.specDetails, "upper", {
                    description: this.$tr("common-error-Must contain at least one uppercase character (without diacritic)"),
                    valid: false,
                    checkValidation: () => !!this.newPassword.match(/[A-Z]/)
                })
            }
        },
        async submitChangePasswordForm (): Promise<void> {
            const changePasswordForm = (this.$refs.changePasswordForm as InstanceType<typeof OxForm>)

            const validate = await changePasswordForm.validate()
            if (!validate) {
                return
            }

            const changePasswordFormEl = changePasswordForm.$el as HTMLFormElement
            changePasswordFormEl.submit()
        },
        checkPassword (value: string): void {
            if (!value) {
                // We reset all validation if input value is empty
                Object.values(this.specDetails).forEach(criteria => {
                    criteria.valid = false
                })

                return
            }

            this.newPassword = value

            Object.values(this.specDetails).forEach(criteria => {
                criteria.valid = criteria.checkValidation()
            })
        },
        isCriteriaDone (criteria: string): { done: boolean } {
            return { done: this.specDetails[criteria].valid }
        },
        openDetailPopin (): void {
            this.showDetailPopin = true
        },
        closeDetailPopin (): void {
            this.showDetailPopin = false
        },
        logout (): void {
            window.location.href = OxUrlDiscovery.logout()
        }
    }
})
</script>

<template>
  <div class="ChangePassword">
    <div class="ChangePassword-logo">
      <svg
        v-if="configs.is_distinct_groups"
        class="ChangePassword-logoContent tamm"
        viewBox="0 0 9.525 9.525"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          class="ChangePassword-logoPart bottom"
          d="M5.316 1.834c-.819.002-1.374.365-1.697.729-.201.226-.21.316-.274.459-.285-.176-.582-.258-.819-.237-.314.029-.547.153-.547.153l-.004.002-.004.002c-.415.235-.601.591-.66.873-.031.145-.01.17-.002.264-.767.235-1.146.758-1.257 1.229a2.15 2.15 0 0 0 .006.966l.004.015c.209.626.61.976.955 1.152s.643.188.649.189c.096.017 2.243.379 3.662-1.309a.86.86 0 0 0 .289.054.9.9 0 0 0 .896-.896.9.9 0 0 0-.896-.894.9.9 0 0 0-.894.894c0 .197.067.377.176.526-1.259 1.464-3.169 1.1-3.169 1.1l-.019-.004h-.019s-.189-.006-.434-.132-.522-.354-.687-.84c-.002-.008-.086-.339-.002-.7.086-.367.295-.742 1.051-.899l.266-.055-.061-.264s-.027-.117.008-.285.119-.356.398-.517c.002-.002.14-.073.341-.092a.76.76 0 0 1 .668.251l.312.325.134-.431s.082-.266.329-.543.635-.551 1.303-.551h.011s.362-.01.718.174.706.519.706 1.378l.01.197.021.025c.061.067.153.109.251.103a.32.32 0 0 0 .243-.13l.019-.027-.023-.331-.002-.031c-.059-.897-.52-1.418-.982-1.655-.486-.256-.962-.237-.972-.237zm2.572 2.174l-.031.013c-.084.04-.151.119-.174.216s.002.195.057.27l.021.027.262.059.01.002.042.008.029.006.015.004a1.19 1.19 0 0 1 .498.266c.21.193.375.471.375.873v.019s.021.364-.115.712-.364.652-1.031.658l-2.704.019c-.23.182-.513.365-.922.532l3.624-.019c.859-.004 1.338-.517 1.527-.995.184-.467.153-.905.151-.928-.002-.551-.245-.984-.547-1.261-.233-.214-.482-.348-.729-.398l-.01-.002zM5.64 5.081a.38.38 0 0 1 .385.385.38.38 0 0 1-.385.385.38.38 0 0 1-.385-.385.38.38 0 0 1 .385-.385z"
          stroke-width=".019135"
        />
      </svg>
      <svg
        v-else
        class="ChangePassword-logoContent"
        fill="none"
        viewBox="0 0 36 36"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          class="ChangePassword-logoPart up"
          d="M16.2511 24.701C16.3478 24.6483 16.3987 24.5386 16.3763 24.4307C16.3539 24.3229 16.2636 24.2424 16.1539 24.2325C14.3817 24.0733 13.1839 23.6106 12.4216 22.9512C11.6665 22.2982 11.3117 21.4287 11.2705 20.3893C11.2289 19.3423 11.5068 18.1282 12.0189 16.8164C12.5302 15.507 13.2689 14.1154 14.1335 12.7179C17.5956 7.1218 23.0197 1.52161 23.6615 0.8992C23.7535 0.810046 23.7632 0.665834 23.684 0.565173C23.6048 0.464512 23.4624 0.439958 23.3541 0.5083C18.2578 3.72412 14.1752 7.73506 11.4172 11.5498C10.0381 13.4571 8.98735 15.3194 8.30711 17.0128C7.62855 18.7019 7.31182 20.2401 7.42105 21.4937C7.54324 23.0744 8.56571 24.5305 10.1527 25.2608C11.7459 25.9939 13.8876 25.9891 16.2511 24.701Z"
          stroke-linejoin="round"
          stroke-width="0.5"
        />
        <path
          class="ChangePassword-logoPart bottom"
          d="M19.7503 11.2972C19.6535 11.3498 19.6026 11.4596 19.625 11.5675C19.6473 11.6754 19.7377 11.7559 19.8474 11.7658C21.6151 11.9248 22.8096 12.3867 23.5695 13.045C24.322 13.6969 24.6754 14.5651 24.7158 15.6031C24.7565 16.6488 24.4785 17.8615 23.9665 19.1719C23.4555 20.48 22.7175 21.8704 21.8539 23.267C18.3959 28.8591 12.9805 34.4601 12.3357 35.1031C12.2453 35.1932 12.2373 35.3369 12.3171 35.4364C12.3968 35.536 12.5387 35.5596 12.6464 35.4911C17.7244 32.2612 21.798 28.2471 24.5565 24.4338C25.9357 22.527 26.989 20.6666 27.6741 18.9755C28.3577 17.2882 28.6816 15.7524 28.5821 14.501C28.4602 12.922 27.4373 11.4675 25.8497 10.7383C24.2562 10.0064 22.1143 10.0111 19.7503 11.2972Z"
          stroke-linejoin="round"
          stroke-width="0.5"
        />
      </svg>
    </div>
    <h3 class="ChangePassword-title">
      {{ configs.product_name }}
    </h3>
    <h5 class="ChangePassword-subtitle">
      {{ subtitle }}
    </h5>
    <p class="ChangePassword-description">
      {{ description }}
    </p>
    <p
      v-if="isChangeForced && !isActivation"
      class="ChangePassword-description"
    >
      {{ $tr('common-msg-You must define a new one in order to be able to connect.') }}
    </p>
    <div class="ChangePassword-form">
      <ox-form
        ref="changePasswordForm"
        :action="links.call_url"
        method="post"
        name="change-password"
        onsubmit="return false;"
        :validator="formValidator"
      >
        <input
          name="request_change_password"
          type="hidden"
          :value="requestChangePassword"
        >
        <div
          v-if="hasErrorLogs"
          class="ChangePassword-errorMessage"
        >
          <ox-caution
            v-for="(log, index) in errorLogs"
            :key="index"
            type="error"
          >
            {{ log }}
          </ox-caution>
        </div>
        <div
          v-if="hasDomainLogs"
          class="ChangePassword-errorMessage"
        >
          <ox-caution
            v-for="(log, index) in domainLogs"
            :key="index"
            type="error"
          >
            {{ log }}
          </ox-caution>
        </div>
        <div class="ChangePassword-oldPassword">
          <ox-password
            v-if="!isActivation"
            v-model="oldPassword"
            :label="$tr('CUser-user_password-current')"
            :maxlength="spec.max_length"
            name="old_password"
            :not-null="true"
            :validator="formValidator"
          />
          <ox-text-field
            v-else
            :disabled="true"
            :label="$tr('common-Username')"
            :value="username"
          />
        </div>
        <div
          v-click-outside="closeDetailPopin"
          class="ChangePassword-newPassword"
        >
          <ox-password
            :label="$tr('CUser-user_password-new')"
            :maxlength="spec.max_length"
            name="new_password1"
            :not-null="true"
            :validator="formValidator"
            @change="checkPassword"
            @focus="openDetailPopin"
          />
          <div class="ChangePassword-detailPopin">
            <detail-popin
              v-if="showDetailPopin"
              :arrow-direction="arrowDirection"
              :title="detailPopinTitle"
            >
              <p class="ChangePassword-detailTitle">
                {{ $tr("common-msg-Your password must contain") }}
              </p>
              <div
                v-for="(criteria, name, index) in specDetails"
                :key="index"
                class="ChangePassword-criteria"
                :class="isCriteriaDone(name)"
              >
                <div class="ChangePassword-criteriaChipWrapper">
                  <div class="ChangePassword-criteriaChip">
                    <ox-icon
                      class="ChangePassword-criteriaChipIcon"
                      icon="check"
                      :size="6"
                    />
                  </div>
                </div>
                <span class="ChangePassword-criteriaLabel">{{ criteria.description }}</span>
              </div>
              <ox-divider
                v-if="showCustomRecommandations"
                :inset-radial="16"
              />
              <!--  eslint-disable vue/no-v-html, purify content -->
              <div
                class="ChangePassword-customRecommandations"
                v-html="customRecommandationsMarkdown"
              />
              <!--  eslint-enable -->
            </detail-popin>
          </div>
        </div>
        <div class="ChangePassword-securityIndicator">
          <security-indicator
            :nb-matching-steps="matchingSteps"
            :total-steps="totalStepsToDisplay"
          />
        </div>
        <div class="ChangePassword-confirmPassword">
          <ox-password
            v-model="confirmPassword"
            :label="$tr('Repeat new password')"
            :maxlength="spec.max_length"
            name="new_password2"
            :not-null="true"
            :validator="formValidator"
          />
        </div>
        <div class="ChangePassword-formSubmit">
          <ox-button
            :block="isTinyMobileSize"
            button-style="primary"
            :label="$tr('common-action-Change password')"
            type="submit"
            @click="submitChangePasswordForm"
          />
          <div class="ChangePassword-logoutBtn">
            <ox-button
              v-if="isChangeForced"
              button-style="tertiary-dark"
              :label="$tr('menu-logout')"
              @click="logout"
            />
          </div>
        </div>
        <ox-token
          name="@token"
          :value="tokens.token"
        />
      </ox-form>
    </div>
  </div>
</template>

<style lang="scss" src="./ChangePassword.scss" />
