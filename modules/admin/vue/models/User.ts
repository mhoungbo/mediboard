/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"

export default class User extends OxObject {
    constructor () {
        super()
        this.type = "user"
    }

    get username (): string {
        return super.get("user_username")
    }

    set username (username: string) {
        super.set("user_username", username)
    }

    get firstName (): string {
        return super.get("user_first_name")
    }

    set firstName (firstName: string) {
        super.set("user_first_name", firstName)
    }

    get lastName (): string {
        return super.get("user_last_name")
    }

    set lastName (lastName: string) {
        super.set("user_last_name", lastName)
    }

    get shortView (): string {
        let firstName = ""
        if (this.firstName) {
            firstName = this.firstName.charAt(0).toUpperCase() + this.firstName.slice(1).toLowerCase()
        }
        return `${this.lastName} ${firstName}`
    }
}
