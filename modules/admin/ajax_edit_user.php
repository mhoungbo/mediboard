<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CMbArray;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Services\AccountActivationService;
use Ox\Mediboard\Etablissement\CGroups;

CCanDo::checkEdit();
// R�cuperation de l'utilisateur s�lectionn�
$user_id  = CView::get("user_id", "str", true);
$tab_name = CView::get("tab_name", "str default|identity");
CView::checkin();

CView::enforceSlave(false);

if ($user_id) {
    $user = CUser::findOrFail($user_id);

    if (CUser::get()->_id != $user->_id) {
        $user->needsEdit();
    }
} else {
    $user = new CUser();
}

// Chargement du d�tail de l'utilisateur
$user->loadRefMediuser();
$user->loadRefsNotes();
$user->isLDAPLinked();

// Chargement des utilateurs associ�s
if ($user->template) {
    $user->loadRefProfiledUsers();
}

CMbArray::naturalSort(CUser::$types);

$token = AntiCsrf::prepare()
                 ->addParam('user_id', ($user->_id) ?: null)
                 ->addParam('callback', 'UserPermission.callback')
                 ->addParam('_duplicate')
                 ->addParam('_duplicate_username')
                 ->addParam('user_username')
                 ->addParam('user_type')
                 ->addParam('template')
                 ->addParam('user_last_name')
                 ->addParam('user_first_name')
                 ->addParam('user_email')
                 ->addParam('is_robot')
                 ->addParam('dont_log_connection')
                 ->addParam('force_change_password')
                 ->getToken();

if (
    $user->_id
    && !$user->isLDAPLinked()
    && (new AccountActivationService())->canGenerateActivationToken($user, CGroups::loadCurrent()->_id)
) {
    $activation_token = AntiCsrf::prepare()
                                ->addParam('user_id', $user->_id)
                                ->addParam('type', ['token', 'email'])
                                ->addParam('email')
                                ->getToken();
}

$smarty = new CSmartyDP();
$smarty->assign('token', $token);
$smarty->assign("utypes", CUser::$types);
$smarty->assign("user", $user);
$smarty->assign("activation_token", $activation_token ?? null);
$smarty->assign("tab_name", $tab_name);
$smarty->assign("specs", $user->getProps());
$smarty->assign("is_admin", (CAppUI::$instance->_ref_user->isAdmin() || CUser::get(CAppUI::$instance->user_id)->isSuperAdmin()));
$smarty->display("inc_edit_user");
