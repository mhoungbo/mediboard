<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin;

use DomainException;
use Exception;

class LdapPasswordUpdater implements PasswordUpdaterInterface
{
    private CLDAP $ldap;

    public function __construct(CLDAP $ldap)
    {
        $this->ldap = $ldap;
    }

    /**
     * @throws Exception
     */
    public function update(CUser $user, string $new_password, string $old_password = null): void
    {
        if (!$user->_id || ($old_password === null)) {
            throw new Exception();
        }

        $this->updateLDAP($user, $old_password, $new_password);
    }

    /**
     * @throws Exception
     */
    private function updateLDAP(CUser $user, string $old_password, string $new_password): void
    {
        if (!$user->isLDAPLinked()) {
            return;
        }

        try {
            if (!$this->ldap::changePassword($user, $old_password, $new_password)) {
                throw new Exception();
            }
        } catch (LdapServerException $e) {
            throw new DomainException($e->getMessage());
        } catch (Exception $e) {
            throw new Exception("CLDAP-change_password_failed");
        }
    }
}
