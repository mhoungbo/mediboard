<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin;

use Exception;

class LdapServerException extends Exception
{
    private const UNWILLING = 'Server is unwilling to perform';

    public static function fromErrorMessage(string $message): self
    {
        if (str_contains($message, self::UNWILLING)) {
            return static::unwillingToPerform();
        }

        return static::serverError();
    }

    public static function unwillingToPerform(): self
    {
        return new static('LdapServerException-error-Server is unwilling to perform');
    }

    public static function serverError(): self
    {
        return new static('LdapServerException-error-Server returned an error');
    }
}
