<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Services;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbSecurity;
use Ox\Core\CMbString;
use Ox\Core\CSmartyDP;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\Admin\Exception\CouldNotActivateAccount;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Mediboard\System\CSourceSMTP;
use Throwable;

/**
 * Service handling the activation process of an account (token generation, email sending, etc.)
 */
class AccountActivationService
{
    private const SOURCE_NAME_ACCOUNT_ACTIVATION = 'account-activation';
    private const TOKEN_LIFETIME                 = '+ 3 weeks';

    /**
     * @throws CouldNotActivateAccount
     */
    public function generateToken(CUser $user): CViewAccessToken
    {
        $this->checkUser($user);

        try {
            $token                 = new CViewAccessToken();
            $token->user_id        = $user->_id;
            $token->datetime_start = CMbDT::dateTime();
            $token->datetime_end   = CMbDT::dateTime(self::TOKEN_LIFETIME);
            $token->purgeable      = 1;
            $token->params         = "action=activate_account";
            $token->routes_names   = "admin_account";

            $this->storeToken($token);
        } catch (Throwable $t) {
            throw CouldNotActivateAccount::unableToCreateToken($t->getMessage());
        }

        $user->resetLoginErrorsCounter();
        $this->resetPassword($user);

        return $token;
    }

    /**
     * @throws CouldNotActivateAccount
     */
    public function sendTokenViaEmail(CUser $user, string $email, CSmartyDP $smarty, CSourceSMTP $source = null): bool
    {
        if (null === $source) {
            $source = $this->getSMTPSource();
        }

        if (!$source instanceof CSourceSMTP || !$source->_id) {
            throw CouldNotActivateAccount::sourceNotFound();
        }

        if (!$source->active) {
            throw CouldNotActivateAccount::sourceNotEnabled();
        }

        // Should be done by Email service...
        if (!$email || !CMbString::checkEmailFormat($email)) {
            throw CouldNotActivateAccount::invalidEmail($email);
        }

        $token = $this->generateToken($user);

        try {
            $product = CAppUI::conf('product_name');
            $subject = "Bienvenue sur {$product}";

            $smarty->assign('user', $user);
            $smarty->assign('email', $email);
            $smarty->assign('product', $product);
            $smarty->assign('token', $token->getUrl());
            $body = $smarty->fetch('activation_link');

            return CApp::sendEmail($subject, $body, null, null, null, [$email], $source, false);
        } catch (Throwable $t) {
            throw CouldNotActivateAccount::unableToSendEmail($t->getMessage());
        }
    }

    /**
     * Check if the current user can generate an activation token for the selected user.
     *
     * @param CUser $user
     * @param int   $current_group_id
     *
     * @return bool
     * @throws Exception
     */
    public function canGenerateActivationToken(CUser $user, int $current_group_id): bool
    {
        $module = CModule::getActive('mediusers');
        if ($module && !$module->getPerm(CPermObject::EDIT)) {
            return false;
        }

        $mediuser = $user->loadRefMediuser();

        if ($mediuser->_id) {
            $function = $mediuser->loadRefFunction();

            return (((int)$function->group_id) === $current_group_id) && $mediuser->getPerm(CPermObject::EDIT);
        }

        return $user->getPerm(CPermObject::EDIT);
    }

    /**
     * @throws CouldNotActivateAccount
     */
    private function resetPassword(CUser $user): void
    {
        try {
            $user->_user_password        = CMbSecurity::getRandomPassword();
            $user->force_change_password = 1;
            $user->_is_changing          = 1;

            if ($msg = $user->store()) {
                throw new Exception($msg);
            }
        } catch (Throwable $t) {
            throw CouldNotActivateAccount::unableToResetPassword($t->getMessage());
        }
    }

    /**
     * @throws CouldNotActivateAccount
     */
    private function checkUser(CUser $user): void
    {
        if (!$user->_id) {
            throw CouldNotActivateAccount::userNotFound();
        }

        if ($user->isSuperAdmin()) {
            throw CouldNotActivateAccount::superAdminNotAllowed();
        }
    }

    /**
     * For mockability.
     *
     * @throws Exception
     */
    protected function storeToken(CViewAccessToken $token): void
    {
        if ($msg = $token->store()) {
            throw new Exception($msg);
        }
    }

    public function getSMTPSource(): CExchangeSource
    {
        return CExchangeSource::get(
            self::SOURCE_NAME_ACCOUNT_ACTIVATION,
            CSourceSMTP::TYPE,
            true,
            null,
            false
        );
    }
}
