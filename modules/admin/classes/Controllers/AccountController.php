<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Controllers;

use DomainException;
use Exception;
use InvalidArgumentException;
use LengthException;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Ox\Core\Auth\Exception\CredentialsCheckException;
use Ox\Core\Auth\RedirectDefaultUriTrait;
use Ox\Core\Auth\WeakPassword;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\Config\Conf;
use Ox\Core\Controller;
use Ox\Core\CSmartyDP;
use Ox\Core\EntryPoint;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Locales\Translator;
use Ox\Core\Security\Crypt\Hash;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Core\Security\Csrf\Exceptions\CouldNotValidateToken;
use Ox\Core\Security\Exception\CouldNotHash;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\DbPasswordUpdater;
use Ox\Mediboard\Admin\Exception\CouldNotActivateAccount;
use Ox\Mediboard\Admin\PasswordSpecs\PasswordSpecBuilder;
use Ox\Mediboard\Admin\PasswordUpdaterInterface;
use Ox\Mediboard\Admin\Services\AccountActivationService;
use Symfony\Bridge\Twig\Extension\CsrfRuntime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\SwitchUserToken;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\Token\PostAuthenticationToken;

class AccountController extends Controller
{
    use RedirectDefaultUriTrait;
    use RequestHelperTrait;

    public const CHANGE_PASSWORD_VIEW    = 'admin_account';
    public const ACCOUNT_ACTIVATE_SUBMIT = 'admin_account_activate';
    public const CHANGE_PASSWORD_SUBMIT  = 'admin_account_update_password';

    public const CHANGE_PASSWORD_ROUTES = [
        self::ACCOUNT_ACTIVATE_SUBMIT,
        self::CHANGE_PASSWORD_VIEW,
        self::CHANGE_PASSWORD_SUBMIT,
    ];

    // Todo: Handle password max length globally
    private const PASSWORD_MAX_LENGTH = 80;

    /**
     * @throws Exception
     */
    public function show(
        Request         $request,
        Conf            $conf,
        RouterInterface $router,
        Translator      $tr,
        CsrfRuntime     $csrf,
        Security        $security,
    ): Response {
        if ($security->getToken() instanceof SwitchUserToken) {
            $this->addUiMsgError('AccountController-error-You cannot change password while being impersonated');

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }

        switch ($request->query->get('action')) {
            case 'change_password':
                return $this->render(
                    '@admin/change_password.html.twig',
                    [
                        'change_password' => $this->buildChangePasswordEntryPoint($router, $request, $conf),
                        'page_title'      => $conf->get('product_name') . ' &mdash; '
                            . $tr->tr('common-action-Change password'),
                        'csrf_token'      => $csrf->getCsrfToken('update-password'),
                    ]
                );

            case 'activate_account':
                $auth_token = $security->getToken();
                if (
                    !$request->getSession()->get('account_activation', false)
                    && (
                        !($auth_token instanceof PostAuthenticationToken)
                        || ('token' !== $auth_token->getFirewallName())
                    )
                ) {
                    // Not allowed to activate account when not passing by m=system&a=chpwd or not from a token
                    throw new AccessDeniedException();
                }

                $csrf_token = AntiCsrf::prepare()
                    ->addParam('new_password1')
                    ->addParam('new_password2')
                    ->addParam('request_change_password')
                    ->getToken();

                return $this->render(
                    '@admin/change_password.html.twig',
                    [
                        'change_password' => $this->buildChangePasswordEntryPoint($router, $request, $conf),
                        'page_title'      => $conf->get('product_name') . ' &mdash; '
                            . $tr->tr('common-action-Change password'),
                        'csrf_token'      => $csrf_token,
                    ]
                );

            default:
                throw new Exception();
        }
    }

    /**
     * @throws CouldNotValidateToken
     * @throws CredentialsCheckException|CouldNotHash
     */
    public function updatePassword(
        Request                     $request,
        CredentialsCheckerInterface $credentials_checker,
        PasswordUpdaterInterface    $updater,
        WeakPassword                $weak_password,
        Hasher                      $hasher,
        Translator                  $tr,
        Security                    $security,
    ): Response {
        if ($security->getToken() instanceof SwitchUserToken) {
            $this->addUiMsgError('AccountController-error-You cannot change password while being impersonated', true);

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }

        $token = $request->request->get('@token');

        $old_password  = $request->request->get('old_password', '');
        $new_password1 = $request->request->get('new_password1', '');
        $new_password2 = $request->request->get('new_password2', '');

        $request_change_password = $request->request->get('request_change_password');

        try {
            if (!$this->isCsrfTokenValid('update-password', $token)) {
                throw CouldNotValidateToken::tokenDoesNotMatch();
            }

            if (($old_password === '') || ($new_password1 === '') || ($new_password2 === '')) {
                throw new InvalidArgumentException('common-error-Missing parameter');
            }

            if ($new_password1 !== $new_password2) {
                throw new InvalidArgumentException('AccountController-error-New password must match');
            }

            if (
                (mb_strlen($old_password) > self::PASSWORD_MAX_LENGTH)
                || (mb_strlen($new_password1) > self::PASSWORD_MAX_LENGTH)
            ) {
                throw new LengthException('AccountController-error-Password allowed max length reached');
            }

            if (!$credentials_checker->check($old_password, $this->getUser())) {
                throw new DomainException('AccountController-error-Old password does not match');
            }

            if (
                $hasher->equals(
                    $hasher->hash(Hash::SHA1, $old_password),
                    $hasher->hash(Hash::SHA1, $new_password1)
                )
            ) {
                throw new DomainException('DbPasswordUpdater-error-New password must be different');
            }

            // Todo: Prevent updating or "creating" a new password without DbPasswordUpdater
            // Todo: Take out CPasswordLog of CUser and use it inside DbPasswordUpdater

            $updater->update($this->getCUser(), $new_password1, $old_password);

            $weak_password->unset();
            $weak_password->resetRemainingDays();

            if ($request_change_password) {
                $response = $this->render('@admin/change_password_ok.html.twig');
            } else {
                // Send in a specific session key shared by SF and Legacy
                // => Legacy can unload flash messages via Appbar and need to unset the message
                $this->info($tr->tr('common-msg-Password successfully updated'));
                $response = $this->getRedirectResponse($request, $this->getCUser()->_id);
            }
        } catch (InvalidArgumentException|LengthException $e) {
            $this->error($tr->tr($e->getMessage()));

            $response = $this->redirectToRoute(
                self::CHANGE_PASSWORD_VIEW,
                ['action' => 'change_password', 'request_change_password' => $request_change_password]
            );
        } catch (DomainException $e) {
            $this->addFlash('domain-error', $tr->tr($e->getMessage()));

            $response = $this->redirectToRoute(
                self::CHANGE_PASSWORD_VIEW,
                ['action' => 'change_password', 'request_change_password' => $request_change_password]
            );
        }

        return $response;
    }

    /**
     * @throws Exception
     */
    public function activateAccount(
        Request           $request,
        DbPasswordUpdater $updater,
        WeakPassword      $weak_password,
        Translator        $tr
    ): Response {
        $params = AntiCsrf::validateSfRequest($request);

        $new_password1 = $params['new_password1'] ?? '';
        $new_password2 = $params['new_password2'] ?? '';

        try {
            if (($new_password1 === '') || ($new_password2 === '')) {
                throw new InvalidArgumentException('common-error-Missing parameter');
            }

            if ($new_password1 !== $new_password2) {
                throw new InvalidArgumentException('AccountController-error-New password must match');
            }

            if (mb_strlen($new_password1) > self::PASSWORD_MAX_LENGTH) {
                throw new LengthException('AccountController-error-Password allowed max length reached');
            }

            $updater->update($this->getCUser(), $new_password1);

            $weak_password->unset();
            $weak_password->resetRemainingDays();

            $request->getSession()->remove('account_activation');

            // Send in a specific session key shared by SF and Legacy
            // => Legacy can unload flash messages via Appbar and need to unset the message
            $this->info($tr->tr('common-msg-Password successfully updated'));
            $response = $this->getRedirectResponse($request, $this->getCUser()->_id);
        } catch (InvalidArgumentException|LengthException $e) {
            $this->error($tr->tr($e->getMessage()));

            $response = $this->redirectToRoute(
                self::CHANGE_PASSWORD_VIEW,
                ['action' => 'activate_account']
            );
        } catch (DomainException $e) {
            $this->addFlash('domain-error', $tr->tr($e->getMessage()));

            $response = $this->redirectToRoute(
                self::CHANGE_PASSWORD_VIEW,
                ['action' => 'activate_account']
            );
        }

        return $response;
    }

    /**
     * @api
     */
    public function dismissReminder(WeakPassword $weak_password): JsonResponse
    {
        $weak_password->resetRemainingDays();

        return new JsonResponse();
    }

    /**
     * Generate an activation token for the user.
     * The token is either displayed via JS or send by mail.
     *
     * @param RequestParams            $params
     * @param AccountActivationService $service
     *
     * @return Response
     * @throws CouldNotActivateAccount
     * @throws CMbModelNotFoundException
     */
    public function generateActivationToken(RequestParams $params, AccountActivationService $service): Response
    {
        $params = AntiCsrf::validateSfRequest($params->getRequest());

        $user = CUser::findOrFail($params['user_id']);

        if (!$service->canGenerateActivationToken($user, $this->request_context->getGroupId())) {
            $this->addUiMsgError('common-msg-You are not allowed to access this information (%s)');

            return $this->renderEmptyResponse(Response::HTTP_FORBIDDEN);
        }

        switch ($params['type']) {
            default:
            case 'token':
                $token = $service->generateToken($user);

                CAppUI::callbackAjax(
                    'window.prompt',
                    $this->translator->tr('common-msg-Here is your account activation link :'),
                    $token->getUrl()
                );
                break;

            case 'email':
                if (!isset($params['email']) || !$params['email']) {
                    $this->addUiMsgError('AccountController-msg-An email must be set.');

                    return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
                }

                try {
                    if ($service->sendTokenViaEmail($user, $params['email'], new CSmartyDP('modules/admin'))) {
                        $this->addUiMsgOk('AccountController-msg-Activation mail sent.');
                    } else {
                        $this->addUiMsgError('AccountController-msg-Unable to send activation mail.');
                        return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
                    }
                } catch (CouldNotActivateAccount $e) {
                    $this->addUiMsgError($e->getMessage());
                    return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
                }
                break;
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws CMbException
     * @throws Exception
     */
    private function buildChangePasswordEntryPoint(RouterInterface $router, Request $request, Conf $conf): EntryPoint
    {
        $user     = $this->getCUser();
        $mediuser = $user->loadRefMediuser();

        if (($mediuser !== null) && $mediuser->_id) {
            $specs = (new PasswordSpecBuilder($mediuser))->build();
        } else {
            $specs = (new PasswordSpecBuilder($user))->build();
        }

        // Todo: We must ensure 50-bits entropy but CNIL recommends at least 80 bits (JORF)
        $spec = [
            'min_length'     => $specs->getMinLength(),
            'max_length'     => self::PASSWORD_MAX_LENGTH,
            'alpha'          => $specs->mustHaveAlphaChars(),
            'upper'          => $specs->mustHaveUpperChars(),
            'num'            => $specs->mustHaveNumChars(),
            'special'        => $specs->mustHaveSpecialChars(),
            'not_containing' => $specs->mustNotContain() ? $user->user_username : null,
            'not_near'       => $specs->mustNotBeNear() ? $user->user_username : null,
        ];

        $flash_bag   = $request->getSession()->getFlashBag();
        $errors      = mb_convert_encoding($flash_bag->get('error'), 'UTF-8', 'ISO-8859-1');
        $domain_logs = mb_convert_encoding($flash_bag->get('domain-error'), 'UTF-8', 'ISO-8859-1');

        $action   = $request->query->get('action');
        $call_url = '';

        if ($action === 'change_password') {
            $call_url = 'admin_account_update_password';
        } elseif ($action === 'activate_account') {
            $call_url = 'admin_account_activate';
        }

        $request_change_password = $request->query->get('request_change_password');

        $entry = (new EntryPoint("ChangePassword", $router))
            ->setScriptName('changePassword')
            ->setLocales(
                [
                    'common-msg-Choose a new password',
                    'common-msg-Expiration of your password',
                    'common-msg-Initialize password',
                    'common-msg-Your actual password has expired.',
                    'common-msg-You must define a new one in order to be able to connect.',
                    'common-msg-Your password must contain',
                    'CUser-user_password-current',
                    'CUser-user_password-new',
                    'CUser-user_password-nomatch',
                    'Repeat new password',
                    'common-action-Change password',
                    'common-error-Invalid password',
                    'common-msg-Correct password',
                    'common-error-Must contain at least one character (without diacritic)',
                    'common-error-Must contain at least one uppercase character (without diacritic)',
                    'common-error-Must contain at least one number',
                    'common-error-Must contain at least one special character',
                    'common-Username',
                    'menu-logout',
                    [
                        'common-error-Invalid minimal length specification (length = %s)',
                        $spec['min_length'],
                    ],
                    [
                        'common-error-Must not contain %s',
                        $spec['not_containing'],
                    ],
                    [
                        'common-error-Looks like too much to %s',
                        $spec['not_near'],
                    ],
                ]
            )
            ->addData('action', $action)
            ->addData('spec', $spec)
            ->addData('error-logs', $errors)
            ->addData('domain-logs', $domain_logs)
            ->addData('request-change-password', $request_change_password)
            ->addData('username', $user->user_username)
            ->addConfigValue('product_name', $conf->get('product_name'))
            ->addConfigValue('custom_recommendations', $conf->get('admin CUser custom_password_recommendations'));

        if ($call_url !== '') {
            $entry->addLink('call_url', $call_url);
        }

        return $entry;
    }
}
