<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Controllers;

use Exception;
use Ox\Core\Auth\RedirectDefaultUriTrait;
use Ox\Core\Controller;
use Ox\Core\Module\CModule;
use Ox\Core\OAuth2\OIDC\FC\Client;
use Ox\Core\OAuth2\OIDC\FC\FcOptions;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class FCController extends Controller
{
    use RedirectDefaultUriTrait;

    public const LOGIN_ROUTE = 'admin_fc_login';

    /**
     * @throws Exception
     *
     * @api public
     */
    public function login(Request $request, Security $security, FcOptions $fc): Response
    {
        if (!$fc->isLoginEnabled() || !CModule::getActive('mediusers')) {
            return new Response(null, Response::HTTP_UNAUTHORIZED);
        }

        if ($redirect = $this->getRedirectIfLogged($request, $security)) {
            return $redirect;
        }

        $client = new Client($fc);

        $endpoint = $client->requestAuthorization();

        return new RedirectResponse($endpoint);
    }

    /**
     * @throws Exception
     *
     * @api
     */
    public function callback(Request $request, FcOptions $fc): Response
    {
        if (!$fc->isLoginEnabled()) {
            return new Response(null, Response::HTTP_UNAUTHORIZED);
        }

        return $this->getRedirectResponse($request, $this->getCUser()->_id);
    }

    /**
     * @throws Exception
     *
     * @api
     */
    public function logout(UrlGeneratorInterface $router, FcOptions $fc): Response
    {
        if (!$fc->isLoginEnabled()) {
            return new Response(null, Response::HTTP_UNAUTHORIZED);
        }

        return $this->redirect($router->generate(LoginController::LOGIN_ROUTE_NAME));
    }
}
