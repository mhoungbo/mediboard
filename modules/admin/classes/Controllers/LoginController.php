<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Controllers;

use Exception;
use LogicException;
use Ox\Core\Auth\RedirectDefaultUriTrait;
use Ox\Core\Config\Conf;
use Ox\Core\Controller;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\Admin\EntryPoint\Login;
use Ox\Mediboard\Admin\Repositories\AccessTokenRepository;
use Symfony\Bridge\Twig\Extension\CsrfRuntime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class LoginController extends Controller
{
    use RedirectDefaultUriTrait;

    public const SWITCH_USER_ROUTE_NAME = 'admin_gui_switch_user';
    public const LOGIN_ROUTE_NAME       = 'admin_gui_login';
    public const LOGOUT_ROUTE_NAME      = 'admin_gui_logout';

    public function login(
        Login       $login,
        Request     $request,
        Security    $security,
        Translator  $tr,
        Conf        $conf,
        CsrfRuntime $csrf
    ): Response {
        if ($redirect = $this->getRedirectIfLogged($request, $security)) {
            return $redirect;
        }

        $page_title = $conf->get('product_name') . ' &mdash; ' . $tr->tr('common-Connection');

        return $this->render(
            '@admin/login.html.twig',
            [
                'page_title' => $page_title,
                'login'      => $login,
                'csrf_token' => $csrf->getCsrfToken('login'),
            ],
        );
    }

    public function logout(Request $request): Response
    {
        throw new LogicException('Unable to logout');
    }

    public function switchUser(Request $request): Response
    {
        return $this->getRedirectResponse($request, $this->getCUser()->_id);
    }

    /**
     * @throws Exception
     *
     * @api
     */
    public function submitLogin(Request $request, Translator $tr, Conf $conf): Response
    {
        if ($request->request->get('reconnect')) {
            $success_message = $tr->tr('common-msg-You are authenticated on %s', $conf->get('product_name'));

            return $this->render(
                '@admin/login_reconnect_ok.html.twig',
                [
                    'user_id'         => $this->getCUser()->_id,
                    'success_message' => $success_message,
                ]
            );
        }

        return $this->getRedirectResponse($request, $this->getCUser()->_id, 'redirect_uri');
    }

    /**
     * @throws Exception
     */
    public function pscCgu(Conf $conf): Response
    {
        return $this->render("@admin/inc_psc_cgu.html.twig", [
            'product_name' => $conf->get('product_name'),
        ]);
    }

    public function useToken(
        Request               $request,
        AccessTokenRepository $repository,
        string                $token,
        string                $route_controller
    ): Response {
        $token = $repository->findByHash($token);
        $token->applyParamsToRequest($request);

        $attributes               = $query = $request->query->all();
        $attributes['permission'] = $request->attributes->get('route_permission');
        if ($token->isRestricted()) {
            $query = array_merge($query, ['dialog' => 1]);
        }

        // QUERY will be duplicated onto the sub-request, and QUERY will be added to ATTRIBUTES.
        return $this->forward($route_controller, $attributes, $query);
    }
}
