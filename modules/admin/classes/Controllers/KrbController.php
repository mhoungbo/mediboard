<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Controllers;

use Ox\Core\Auth\RedirectDefaultUriTrait;
use Ox\Core\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class KrbController extends Controller
{
    use RedirectDefaultUriTrait;

    public const LOGIN_ROUTE = 'admin_krb_login';

    public function login(Request $request): Response
    {
        return $this->getRedirectResponse($request, $this->getCUser()->_id, 'redirect_uri');
    }
}
