<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFilter;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbSecurity;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\Kernel\Exception\HttpException;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\CViewAccessToken;
use Symfony\Component\HttpFoundation\Response;

class PermissionController extends Controller
{
    /**
     * @throws Exception
     *
     * @api public
     */
    public function identicate(RequestApi $request): Response
    {
        $login = $request->getRequest()->query->get('login');
        if ($login === null) {
            throw new CMbException('No login send to test.');
        }

        $user                = new CUser();
        $user->user_username = $login;
        $user->loadMatchingObjectEsc();

        $data = [
            'login'         => $login,
            'is_identicate' => $user->_id ? true : false,
        ];

        $res = new Item($data);
        $res->setType('identicate');

        return $this->renderApiResponse($res);
    }

    /**
     * Get token
     *
     * @throws ApiException
     * @throws Exception
     *
     * @api
     */
    public function getTokens(RequestApi $request_api): Response
    {
        // search hash only use EQUAL filter
        $request_filters   = $request_api->getRequestFilter();
        $filter_hash_equal = $request_filters->getFilter('hash', RequestFilter::FILTER_EQUAL);
        if ($filter_hash_equal) {
            $ds                 = CSQLDataSource::get('std');
            $where_datetime_end = "datetime_end " . $ds->prepare('> ?', CMbDT::dateTime())
                . 'OR datetime_end IS NULL';

            $where = [
                $request_filters->getSqlFilter($filter_hash_equal, $ds),
                "($where_datetime_end)",
            ];

            $token = new CViewAccessToken();
            $token->loadObject($where, $request_api->getSortAsSql('datetime_start DESC'));
        } else {
            // load else create token
            $user  = CUser::get();
            $token = CUser::getAccessToken($user->user_id);
        }

        if (!$token->_id) {
            throw new HttpException(Response::HTTP_NOT_FOUND);
        }

        $private_key = CMbSecurity::hash(CMbSecurity::SHA1, $token->hash . '-' . $token->datetime_end);

        // Create item
        $item = Item::createFromRequest($request_api, $token);
        $item->addAdditionalDatas(['key' => $private_key ?? null]);

        return $this->renderApiResponse($item);
    }
}
