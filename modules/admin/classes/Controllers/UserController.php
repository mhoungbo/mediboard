<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Controllers;

use DateTimeImmutable;
use DomainException;
use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Exceptions\ApiRequestException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CMbSecurity;
use Ox\Core\CMbString;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Locales\Translator;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * @throws ApiException
     * @throws CMbModelNotFoundException
     *
     * @api
     */
    public function showUser(RequestApi $request_api): Response
    {
        $user_id = $request_api->getRequest()->attributes->get('user_id');
        $user    = CUser::findOrFail($user_id);

        if ($user->_id !== $this->getCUser()->_id) {
            if (!$this->checkPermEdit($user)) {
                throw new DomainException((new Translator())->tr('access-forbidden'));
            }
        }

        $item = Item::createFromRequest($request_api, $user);

        return $this->renderApiResponse($item);
    }

    /**
     * @throws ApiException
     * @throws ApiRequestException
     * @throws Exception
     *
     * @api
     */
    public function listUsers(RequestApi $request_api): Response
    {
        if ($request_api->getRequest()->query->has('autocomplete')) {
            return $this->autocomplete($request_api);
        }

        // Manual handling because of this controller is used as a crossroad between many other "routes".
        if (!$this->checkModulePermEdit()) {
            throw new AccessDeniedException(
                $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
            );
        }

        $user = new CUser();
        $ds   = $user->getDS();

        $where = [
            'template' => $ds->prepare('= ?', '0'),
        ];

        $where = array_merge($where, $request_api->getFilterAsSQL($ds));

        $users = $user->loadList($where, $request_api->getSortAsSql(), $request_api->getLimitAsSql());

        return $this->renderApiResponse(Collection::createFromRequest($request_api, $users));
    }

    /**
     * Create, Update or Delete a CUser.
     * Validate the POST data with AntiCsrf.
     *
     * @param RequestParams $params
     *
     * @return Response
     * @throws CMbModelNotFoundException
     */
    public function edit(RequestParams $params): Response
    {
        $values = AntiCsrf::validateSfRequest($params->getRequest());

        if (isset($values['user_id']) && $values['user_id']) {
            $user = CUser::findOrFail($values['user_id']);
        } else {
            $user = new CUser();
        }

        $self = $user->user_id == $this->getCUser()->_id;

        if ($user->user_id && !$self && (!$this->checkModulePermAdmin() || !$user->getPerm(CPermObject::EDIT))) {
            $this->addUiMsgError('common-msg-You are not allowed to access this information (%s)', true);

            if ($callback = $params->post('callback', 'str')) {
                CAppUI::callbackAjax($callback);
            }

            return $this->renderEmptyResponse(Response::HTTP_FORBIDDEN);
        }

        $return_code = Response::HTTP_OK;

        if ($params->post('del', 'bool default|0')) {
            if (!$user->user_id || $self) {
                $this->addUiMsgError('CUser-error-You cannot delete an unknown user or yourself', true);
                $return_code = Response::HTTP_BAD_REQUEST;
            } elseif ($msg = $user->delete()) {
                $this->addUiMsgError($msg, true);

                $return_code = Response::HTTP_INTERNAL_SERVER_ERROR;
            } else {
                $this->addUiMsgOk('CUser-msg-delete', true);
            }

            return $this->renderEmptyResponse($return_code);
        }

        $this->bindValues($user, $values);

        if (!$user->user_id) {
            $user->_user_password = CMbSecurity::getRandomPassword($user->_specs['_user_password']);
        }

        $new = !$user->_id;
        if ($msg = $user->store()) {
            $this->addUiMsgError($msg, true);
            $return_code = Response::HTTP_INTERNAL_SERVER_ERROR;
        } else {
            $this->addUiMsgOk($new ? 'CUser-msg-create' : 'CUser-msg-modify', true);
        }

        if ($callback = $params->post('callback', 'str')) {
            CAppUI::callbackAjax($callback);
        }

        return $this->renderEmptyResponse($return_code);
    }

    /**
     * Users autocomplete for GUI usage
     *
     * @throws Exception
     */
    public function listUsersAutocomplete(RequestParams $params): Response
    {
        $keywords = $params->get('_view', 'str');
        $users    = $this->loadUsers($keywords);

        /** @var CUser $_user */
        foreach ($users as $_user) {
            $_user->loadRefMediuser();
            $_user->_ref_mediuser->loadRefFunction();
        }

        CUser::filterByPerm($users, PERM_READ);

        return $this->renderSmarty(
            'inc_field_autocomplete.tpl',
            [
                'matches'    => $users,
                'field'      => '_view',
                'view_field' => true,
                'show_view'  => $params->get('show_view', 'str default|false') == "true",
                'template'   => (new CUser())->getTypedTemplate("autocomplete"),
                'input'      => '',
                'nodebug'    => true,
            ],
            'system'
        );
    }

    /**
     * Users autocomplete for API usage
     *
     * @throws ApiException
     * @throws Exception
     */
    private function autocomplete(RequestApi $request_api): Response
    {
        $keywords = $request_api->getRequest()->query->get('keywords');

        $users = $this->loadUsers($keywords, $request_api->getSortAsSql(), $request_api->getLimit());

        CUser::filterByPerm($users, PERM_READ);

        $collection = Collection::createFromRequest($request_api, $users);

        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws Exception
     */
    private function loadUsers(?string $keywords = null, ?string $order = null, ?int $limit = null): ?array
    {
        $user = new CUser();
        $ds   = $user->getDS();

        $now = (new DateTimeImmutable())->format('Y-m-d');

        $where = [
            'users.template'               => $ds->prepare('= ?', '0'),
            'users.user_type'              => $ds->prepare('!= ?', CUser::$types[22]), // Todo: Make it a constant...
            'users_mediboard.actif'        => $ds->prepare('IS NULL OR `users_mediboard`.`actif` = ?', '1'),
            'users_mediboard.deb_activite' => $ds->prepare(
                'IS NULL OR `users_mediboard`.`deb_activite` <= ?',
                $now
            ),
            'users_mediboard.fin_activite' => $ds->prepare(
                'IS NULL OR `users_mediboard`.`fin_activite` >= ?',
                $now
            ),
        ];

        if ($keywords) {
            $keywords = preg_replace('/[^\w%_]+/', '%', CMbString::removeDiacritics(trim($keywords)));

            $name = [
                '`users`.`user_first_name` ' . $ds->prepareLike("{$keywords}%"),
                '`users`.`user_last_name` ' . $ds->prepareLike("{$keywords}%"),
                '`users`.`user_username` ' . $ds->prepareLike("{$keywords}%"),
            ];

            $where[] = implode(' OR ', $name);
        }

        return $user->loadList(
            $where,
            $order,
            $limit,
            ['users.user_id'],
            ['users_mediboard' => 'users.user_id = users_mediboard.user_id',]
        );
    }

    /**
     * Bind the values to the CUser.
     *
     * @param CUser $user
     * @param array $values
     *
     * @return void
     */
    private function bindValues(CUser $user, array $values): void
    {
        $user->_duplicate            = $values['_duplicate'] ?? null;
        $user->_duplicate_username   = $values['_duplicate_username'] ?? null;
        $user->user_username         = $values['user_username'] ?? null;
        $user->user_type             = $values['user_type'] ?? null;
        $user->template              = $values['template'] ?? null;
        $user->user_last_name        = $values['user_last_name'] ?? null;
        $user->user_first_name       = $values['user_first_name'] ?? null;
        $user->user_email            = $values['user_email'] ?? null;
        $user->is_robot              = $values['is_robot'] ?? null;
        $user->dont_log_connection   = $values['dont_log_connection'] ?? null;
        $user->force_change_password = $values['force_change_password'] ?? null;
    }
}
