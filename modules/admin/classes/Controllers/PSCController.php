<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Controllers;

use Exception;
use Ox\Core\Auth\RedirectDefaultUriTrait;
use Ox\Core\Controller;
use Ox\Core\OAuth2\OIDC\PSC\Client;
use Ox\Core\OAuth2\OIDC\PSC\PscOptions;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class PSCController extends Controller
{
    use RedirectDefaultUriTrait;

    public const LOGIN_ROUTE = 'admin_psc_login';

    /**
     * @throws Exception
     *
     * @api public
     */
    public function login(Request $request, Security $security, PscOptions $psc): Response
    {
        if (!$psc->isLoginEnabled()) {
            return new Response(null, Response::HTTP_UNAUTHORIZED);
        }

        if ($redirect = $this->getRedirectIfLogged($request, $security)) {
            return $redirect;
        }

        $client = new Client($psc);

        $endpoint = $client->requestAuthorization();

        return new RedirectResponse($endpoint);
    }

    /**
     * @throws Exception
     *
     * @api
     */
    public function callback(Request $request, PscOptions $psc): Response
    {
        if (!$psc->isLoginEnabled()) {
            return new Response(null, Response::HTTP_UNAUTHORIZED);
        }

        return $this->getRedirectResponse($request, $this->getCUser()->_id);
    }
}
