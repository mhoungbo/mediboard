<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin;

use Exception;

interface PasswordUpdaterInterface
{
    /**
     * @throws Exception
     */
    public function update(CUser $user, string $new_password, string $old_password = null): void;
}
