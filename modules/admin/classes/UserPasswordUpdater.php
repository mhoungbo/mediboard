<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin;

use Exception;

class UserPasswordUpdater implements PasswordUpdaterInterface
{
    private DbPasswordUpdater   $db_updater;
    private LdapPasswordUpdater $ldap_updater;

    public function __construct(DbPasswordUpdater $db, LdapPasswordUpdater $ldap)
    {
        $this->db_updater   = $db;
        $this->ldap_updater = $ldap;
    }

    /**
     * @inheritDoc
     */
    public function update(CUser $user, string $new_password, string $old_password = null): void
    {
        if (!$user->_id) {
            throw new Exception();
        }

        $this->ldap_updater->update($user, $new_password, $old_password);
        $this->db_updater->update($user, $new_password, $old_password);
    }
}
