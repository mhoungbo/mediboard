<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\PasswordSpecs;

use Exception;
use Ox\Core\CAppUI;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;

class PasswordSpecBuilder
{
    public const MIN_LENGTH       = 9;
    public const ADMIN_MIN_LENGTH = 14;

    private CMediusers|CUser $user;

    private PasswordSpecConfiguration $configuration;

    private string $username_field;

    /**
     * PasswordSpecBuilder constructor.
     *
     * @param CUser|CMediusers               $user
     * @param PasswordSpecConfiguration|null $configuration
     *
     * @throws Exception
     */
    public function __construct(CUser|CMediusers $user, ?PasswordSpecConfiguration $configuration = null)
    {
        $this->user = $user;

        $this->username_field = '_user_username';
        if ($user instanceof CUser) {
            $this->username_field = 'user_username';
        }

        $this->normalizeConfiguration(($configuration) ?: self::createConfiguration());
    }

    private function normalizeConfiguration(PasswordSpecConfiguration $configuration): void
    {
        $default = $this->getDefaultConfiguration();

        // Standard
        if ($configuration['strong_min_length'] > $default['strong_min_length']) {
            $default['strong_min_length'] = $configuration['strong_min_length'];
        }
        $default['strong_special_chars'] = (bool)$configuration['strong_special_chars'];

        // Administrators
        if ($configuration['admin_min_length'] > $default['admin_min_length']) {
            $default['admin_min_length'] = $configuration['admin_min_length'];
        }
        $default['admin_special_chars'] = (bool)$configuration['admin_special_chars'];

        // LDAP-related
        $default['ldap_specific'] = (bool)$configuration['ldap_specific'];
        if ($configuration['ldap_min_length'] > $default['ldap_min_length']) {
            $default['ldap_min_length'] = $configuration['ldap_min_length'];
        }
        $default['ldap_special_chars'] = (bool)$configuration['ldap_special_chars'];

        // Setting the normalized configuration
        $this->configuration = $default;
    }

    private function getDefaultConfiguration(): PasswordSpecConfiguration
    {
        $configuration = new PasswordSpecConfiguration();

        // Standard
        $configuration['strong_min_length']    = self::MIN_LENGTH;
        $configuration['strong_alpha_chars']   = true;
        $configuration['strong_upper_chars']   = true;
        $configuration['strong_num_chars']     = true;
        $configuration['strong_special_chars'] = false;

        // Administrators
        $configuration['admin_min_length']    = self::ADMIN_MIN_LENGTH;
        $configuration['admin_alpha_chars']   = true;
        $configuration['admin_upper_chars']   = true;
        $configuration['admin_num_chars']     = true;
        $configuration['admin_special_chars'] = false;

        // LDAP-related
        $configuration['ldap_specific']             = false;
        $configuration['ldap_min_length']    = $configuration['strong_min_length'];
        $configuration['ldap_alpha_chars']   = $configuration['strong_alpha_chars'];
        $configuration['ldap_upper_chars']   = $configuration['strong_upper_chars'];
        $configuration['ldap_num_chars']     = $configuration['strong_num_chars'];
        $configuration['ldap_special_chars'] = $configuration['strong_special_chars'];

        return $configuration;
    }

    public function getStrongSpec(): PasswordSpec
    {
        return $this->buildStrong();
    }

    public function getAdminSpec(): PasswordSpec
    {
        return $this->buildAdmin();
    }

    /**
     * @return PasswordSpec
     * @throws Exception
     */
    public function build(): PasswordSpec
    {
        // LDAP take precedence over any other admin-specific policy
        if ($this->configuration['ldap_specific'] && $this->isUserLDAPLinked()) {
            return $this->buildLDAP();
        }

        // Password for admin
        if ($this->isUserAdmin()) {
            return $this->buildAdmin();
        }

        // For everyone
        return $this->buildStrong();
    }

    /**
     * @return PasswordSpec
     * @throws Exception
     */
    private function buildStrong(): PasswordSpec
    {
        $min_length = $this->configuration['strong_min_length'];
        $alpha      = $this->configuration['strong_alpha_chars'];
        $upper      = $this->configuration['strong_upper_chars'];
        $nums       = $this->configuration['strong_num_chars'];
        $special    = $this->configuration['strong_special_chars'];

        return PasswordSpec::createStrong(
            $this->user::class,
            $min_length,
            $alpha,
            $upper,
            $nums,
            $special,
            $this->username_field,
            $this->username_field
        );
    }

    /**
     * @return PasswordSpec
     * @throws Exception
     */
    private function buildLDAP(): PasswordSpec
    {
        $min_length = $this->configuration['ldap_min_length'];
        $alpha      = $this->configuration['ldap_alpha_chars'];
        $upper      = $this->configuration['ldap_upper_chars'];
        $nums       = $this->configuration['ldap_num_chars'];
        $special    = $this->configuration['ldap_special_chars'];

        return PasswordSpec::createLDAP(
            $this->user::class,
            $min_length,
            $alpha,
            $upper,
            $nums,
            $special,
            $this->username_field,
            $this->username_field
        );
    }

    /**
     * @return PasswordSpec
     * @throws Exception
     */
    private function buildAdmin(): PasswordSpec
    {
        $min_length = $this->configuration['admin_min_length'];
        $alpha      = $this->configuration['admin_alpha_chars'];
        $upper      = $this->configuration['admin_upper_chars'];
        $nums       = $this->configuration['admin_num_chars'];
        $special    = $this->configuration['admin_special_chars'];

        return PasswordSpec::createAdmin(
            $this->user::class,
            $min_length,
            $alpha,
            $upper,
            $nums,
            $special,
            $this->username_field,
            $this->username_field
        );
    }

    /**
     * @return bool
     */
    private function isUserAdmin(): bool
    {
        if ($this->user instanceof CUser) {
            return $this->user->isTypeAdmin();
        }

        return $this->user->isAdmin();
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function isUserLDAPLinked(): bool
    {
        if ($this->user instanceof CUser) {
            return $this->user->isLDAPLinked();
        }

        return $this->user->isLDAPLinked();
    }

    /**
     * For testability purposes.
     *
     * @return PasswordSpecConfiguration
     * @throws Exception
     */
    private static function createConfiguration(): PasswordSpecConfiguration
    {
        static $configuration = null;

        if ($configuration instanceof PasswordSpecConfiguration) {
            return $configuration;
        }

        $configuration = new PasswordSpecConfiguration();

        $configuration['ldap_specific'] = (bool)CAppUI::conf('admin CUser enable_ldap_specific_strong_password');

        $configuration->mergeWith(self::createConfigurationFor('strong'));
        $configuration->mergeWith(self::createConfigurationFor('ldap'));
        $configuration->mergeWith(self::createConfigurationFor('admin'));

        return $configuration;
    }

    /**
     * @param string|null $type strong|ldap|admin
     *
     * @return array
     * @throws Exception
     */
    private static function createConfigurationFor(string $type): array
    {
        $prefix = match ($type) {
            'strong' => 'strong',
            'ldap'   => 'ldap_strong',
            'admin'  => 'admin_strong',
            default  => throw new Exception('not supported'),
        };

        $min_length = (int)CAppUI::conf("admin CPasswordSpec {$prefix}_password_min_length");

        return [
            "{$type}_min_length"    => $min_length,
            "{$type}_special_chars" => (bool)CAppUI::conf(
                "admin CPasswordSpec {$prefix}_password_special_chars"
            ),
        ];
    }
}
