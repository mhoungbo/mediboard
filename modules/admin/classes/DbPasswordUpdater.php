<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin;

use DomainException;
use Exception;
use Ox\Mediboard\Admin\Repositories\UserRepository;

class DbPasswordUpdater implements PasswordUpdaterInterface
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function update(CUser $user, string $new_password, string $old_password = null): void
    {
        if (!$user->_id) {
            throw new Exception();
        }

        $this->updateDB($user, $new_password);
    }

    /**
     * @param CUser  $user
     * @param string $password
     *
     * @return void
     * @throws Exception
     */
    private function updateDB(CUser $user, string $password): void
    {
        $user->_user_password = $password;
        $user->_is_changing   = true;

        // If user was obliged to change and successfully changed, remove flag
        $user->force_change_password = '0';

        // Is directly handled into CUser (should be here)
        //$user->user_password_last_change = CMbDT::dateTime();

        // Todo: Should revert LDAP changes if DB fails
        try {
            $this->repository->save($user);
        } catch (Exception $e) {
            // Using a simple message comparison, should be ref with its own exception.
            if ($e->getMessage() === 'CPasswordLog-error-This password has already been used.') {
                throw new DomainException($e->getMessage());
            }

            throw $e;
        }
    }
}
