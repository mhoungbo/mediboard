<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Rgpd;

use Ox\Core\CStoredObject;

interface RGPDProofContentMakerInterface
{
    /**
     * Get unique name of proof content maker.
     * It's should be unique
     * It's used in configuration for m
     *
     * @return string
     */
    public function getProofContentMakerName(): string;

    /**
     * Proof content generated
     *
     * @param CStoredObject $object
     * @param array         $options
     *
     * @return string
     */
    public function makeProofContent(CStoredObject $object, array $options = []): string;
}
