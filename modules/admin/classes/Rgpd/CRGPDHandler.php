<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Rgpd;

use Ox\Core\CMbObject;
use Ox\Core\CStoredObject;
use Ox\Core\Handlers\ObjectHandler;

/**
 * RGPD observer
 */
class CRGPDHandler extends ObjectHandler
{
    static $first_store;

    /**
     * @inheritdoc
     */
    public static function isHandled(CStoredObject $object)
    {
        return (($object instanceof IRGPDEvent) && parent::isHandled($object));
    }

    /**
     * @inheritdoc
     */
    public function onBeforeStore(CStoredObject $object): bool
    {
        if (!static::isHandled($object)) {
            return false;
        }

        static::$first_store = (!$object->_id);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function onAfterStore(CStoredObject $object): bool
    {
        CMbObject::$useObjectCache = false;

        if (!static::isHandled($object)) {
            CMbObject::$useObjectCache = true;

            return false;
        }

        /** @var IRGPDEvent $object */
        if ($object->checkTrigger(static::$first_store)) {
            $object->triggerEvent();
        }

        CMbObject::$useObjectCache = true;

        return true;
    }
}
