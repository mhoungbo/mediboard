<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Rgpd;

use Ox\Core\CAppUI;
use Ox\Core\CSmartyDP;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Etablissement\CGroups;

class ProofContentMaker implements RGPDProofContentMakerInterface
{
    public const NAME = 'admin.proofContentMaker';

    /**
     * @inheritdoc
     */
    public function getProofContentMakerName(): string
    {
        return self::NAME;
    }

    /**
     * @inheritdoc
     */
    public function makeProofContent(CStoredObject $object, array $options = []): string
    {
        $object_class = $object->_class;
        $stylized     = $options['stylized'] ?? true;
        $group_id     = $options["group_id"] ?? null;

        $group = CGroups::get($group_id);
        if (!in_array($object->_class, CRGPDManager::getCompliantClasses()) || !$group || !$group->_id) {
            CAppUI::commonError();
        }

        $smarty = new CSmartyDP('modules/admin');
        $smarty->assign('manager', new CRGPDManager($group->_id));
        $smarty->assign('object_class', $object_class);
        $smarty->assign('stylized', $stylized);

        return $smarty->fetch('inc_vw_rgpd_document');
    }
}
