<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Rgpd;

use Ox\Mediboard\Admin\CTokenValidator;

class CRGPDTokenValidator extends CTokenValidator
{
    protected $patterns = [
        'get' => [
            'consent' => ['/(0|1)/', null],
        ],
    ];

    protected $authorized_methods = [
        'get',
    ];
}
