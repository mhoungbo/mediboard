<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Repositories;

use Exception;
use Ox\Mediboard\Admin\CKerberosLdapIdentifier;
use Ox\Mediboard\Admin\CUser;

class KerberosLdapIdentifierRepository
{
    private CKerberosLdapIdentifier $model;

    public function __construct()
    {
        $this->model = new CKerberosLdapIdentifier();
    }

    /**
     * @throws Exception
     */
    public function findUserByIdentifier(string $username): ?CUser
    {
        if (!$username || !$this->model::isReady()) {
            return null;
        }

        $identifier           = clone $this->model;
        $identifier->username = $username;

        if ($identifier->loadMatchingObjectEsc()) {
            return $identifier->loadRefUser();
        }

        return null;
    }
}
