<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\Repositories;

use Exception;
use Ox\Mediboard\Admin\CUser;
use Throwable;

class UserRepository
{
    /**
     * @throws Exception
     */
    public function save(CUser $user): void
    {
        try {
            if ($msg = $user->store()) {
                throw new Exception($msg);
            }
        } catch (Throwable $t) {
            // Todo: Should throw a more specific exception
            throw new Exception($t->getMessage());
        }
    }
}
