<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Admin\EntryPoint;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\Config\Conf;
use Ox\Core\EntryPoint;
use Ox\Core\Module\CModule;
use Ox\Core\OAuth2\OIDC\PSC\PscOptions;
use Ox\Mediboard\Admin\CKerberosLdapIdentifier;
use Ox\Mediboard\Admin\Controllers\KrbController;
use Ox\Mediboard\Admin\Controllers\PSCController;
use Ox\Mediboard\MbHost\Controllers\MbHostController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * Login data builder
 */
class Login extends EntryPoint
{
    public const LOGIN_ID    = 'AuthPage';
    public const SCRIPT_NAME = 'authpage';

    private Conf $config;

    private array    $assets           = [];
    private array    $browser_check    = [];
    private array    $branding_buttons = [];
    private array    $instance_data    = [];
    private array    $external_logs    = [];
    private array    $internal_logs    = [];
    private ?Request $request          = null;

    private PscOptions $psc;

    public function __construct(RouterInterface $router, Conf $config, PscOptions $psc)
    {
        parent::__construct(static::LOGIN_ID, $router);

        $this->config = $config;
        $this->psc    = $psc;
    }

    public function init(RequestStack $request_stack): void
    {
        $request       = $request_stack->getCurrentRequest();
        $this->request = $request;

        $reconnect = $request->query->get('reconnect');
        $this->addData('reconnect', $reconnect);

        $flash_bag = $request->getSession()->getFlashBag();

        $this->internal_logs = $flash_bag->get('auth-internal');
        $this->external_logs = $flash_bag->get('auth-external');

        $this->convertArrayOfStringsEncoding($this->internal_logs);
        $this->convertArrayOfStringsEncoding($this->external_logs);
    }

    /**
     * @throws Exception
     */
    public function build(): void
    {
        $this->setScriptName(static::SCRIPT_NAME);
        $this->buildAssets();
        $this->buildBrowserCheck();
        $this->buildBrandingButtons();
        $this->buildGenericLocales();
        $this->buildInstanceData();
        $this->buildLogs();
        $this->buildLinks();
    }

    /**
     * @throws Exception
     */
    private function buildAssets(): void
    {
        // Logo custom data
        $logo_custom_svg = $this->config->get('root_dir') . '/images/pictures/logo_custom.svg';
        $logo_custom_png = $this->config->get('root_dir') . '/images/pictures/logo_custom.png';
        $logo_custom_src = 'style/mediboard_ext/images/pictures/logoMb.png';

        if (file_exists($logo_custom_svg)) {
            $logo_custom_src = 'images/pictures/logo_custom.svg';
        } elseif (file_exists($logo_custom_png)) {
            $logo_custom_src = 'images/pictures/logo_custom.png';
        }

        // Bg custom data
        $bg_custom     = $this->config->get('root_dir') . '/images/pictures/bg_custom.jpg';
        $bg_custom_src = file_exists($bg_custom) ? 'images/pictures/bg_custom.jpg' : '';

        $this->assets['logo_src'] = $logo_custom_src;
        $this->assets['bg_src']   = $bg_custom_src;

        $this->addData('assets', $this->assets);
    }

    /**
     * @throws Exception
     */
    private function buildBrowserCheck(): void
    {
        // Browser/UA config & locales data
        $this->browser_check['is_login_browser_check'] = (bool)$this->config->get('login_browser_check');

        if (!$this->browser_check['is_login_browser_check']) {
            return;
        }

        $ua                                       = CAppUI::getUA();
        $this->browser_check['server_side_error'] = $ua->_obsolete || $ua->_too_recent || $ua->_badly_detected;
        $supported_browsers                       = $ua::SUPPORTED_BROWSERS;

        $this->addData('browser-check', $this->browser_check);

        $this->addLocale(
               'Browser-error-content-chrome',
            ...$supported_browsers['Chrome']
        );
        $this->addLocale(
               'Browser-error-content-firefox',
            ...$supported_browsers['Firefox']
        );
        $this->addLocale(
               'Browser-error-content-edge',
               'Browser-error-content-edge',
            ...$supported_browsers['Edge']
        );
        $this->addLocale('Browser-error-title');
        $this->addLocale('Browser-error-content', $ua->browser_name, $ua->browser_version);
        $this->addLocale('Browser-error-subcontent');
    }

    /**
     * @throws Exception
     */
    private function buildBrandingButtons(): void
    {
        $branding_buttons = [];

        if (CKerberosLdapIdentifier::isLoginButtonEnabled() && $this->isKrbRequest()) {
            $branding_buttons['krb_endpoint'] = $this->router->generate(KrbController::LOGIN_ROUTE);
        }

        if ($this->psc->isLoginEnabled()) {
            $branding_buttons['psc_endpoint'] = $this->router->generate(PSCController::LOGIN_ROUTE);
        }

        if (CModule::getActive('mbHost')) {
            $branding_buttons['cps_endpoint'] = $this->router->generate(MbHostController::LOGIN_ROUTE);
        }

        $this->branding_buttons = $branding_buttons;

        if (!empty($this->branding_buttons)) {
            $this->addData('branding-buttons', $this->branding_buttons);
        }
    }

    private function buildGenericLocales(): void
    {
        $this->addLocale('common-Qualif');
        $this->addLocale('common-Connection');
        $this->addLocale('common-User');
        $this->addLocale('CMbFieldSpec.type.password');
        $this->addLocale('common-Remember me');
        $this->addLocale('CUser-Password forgotten');
        $this->addLocale('common-Password reset request');
        $this->addLocale('common-Please contact your administration');
        $this->addLocale('common-Thanks to contact your referent in order to obtain a password reset');
        $this->addLocale('common-action-Back to login');
        $this->addLocale('common-error-Must not be empty');
        $this->addLocale('Login');
        $this->addLocale('Auth-failed-reconnection-needed');
        $this->addLocale('CKerberosLdapIdentifier-action-Login-Long');
        $this->addLocale('CPX-auth_CPX');
        $this->addLocale('error-mbHost-unreachable');
        $this->addLocale('CPX-error_certificat_reading');
        $this->addLocale('ProSanteConnect-action-Login');
        $this->addLocale('ProSanteConnect-cgu-long');
        $this->addLocale('ProSanteConnect-cgu-desc');
    }

    /**
     * @throws Exception
     */
    private function buildInstanceData(): void
    {
        $this->addConfigValue('product_name', $this->config->get('product_name'))
             ->addConfigValue('company_name', $this->config->get('company_name'));

        $this->instance_data['release_title'] = CApp::getVersion()->getReleaseTitle();

        $this->setMeta($this->instance_data);
    }

    public function buildLogs(): void
    {
        $this->addData('internal-logs', $this->internal_logs);
        $this->addData('external-logs', $this->external_logs);
    }

    /**
     * @throws CMbException
     */
    private function buildLinks(): void
    {
        $this->addLink('call_url', 'admin_gui_submit_login', $this->request->query->all());
        $this->addLink('psc_cgu_link', 'admin_psc_cgu');
    }

    private function isKrbRequest(): bool
    {
        if ($this->request === null) {
            return false;
        }

        $krb_auth_type = $this->request->server->get('AUTH_TYPE');

        return $this->request->server->has('REMOTE_USER') && ($krb_auth_type === 'Negotiate');
    }
}
