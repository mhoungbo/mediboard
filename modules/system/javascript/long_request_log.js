/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

LongRequestLog = window.LongRequestLog || {
  refresh: function () {
    var form = getForm("Filter-Log");
    var url = new Url('system', 'ajax_list_long_request_logs');
    url.addFormData(form);

    url.requestUpdate('list-logs');
    return false;
  },

  edit: function (log_id) {
    var options = {
      onClose: LongRequestLog.refresh
    };

    new Url('system', 'edit_long_request_log').addParam('log_id', log_id).requestModal(-100, null, options);
  },

  confirmDeletion: function (form) {
    var options = {
      typeName: 'log',
      objName:  $V(form.long_request_log_id)
    };

    confirmDeletion(form, options, Control.Modal.close);
  },

  confirmLogDeletion: function (message, url_route, token, form, limit) {
    Modal.confirm($T(message, limit), {
      onOK: function () {
        new Url().setRoute(url_route)
          .addElement(form.elements.user_id)
          .addElement(form.elements.duration)
          .addElement(form.elements.duration_operand)
          .addElement(form.elements._datetime_start_min)
          .addElement(form.elements._datetime_start_max)
          .addElement(form.elements._datetime_end_min)
          .addElement(form.elements._datetime_end_max)
          .addElement(form.elements.filter_module)
          .addElement(form.elements.module_action_id)
          .addParam('limit', limit)
          .addParam('purge', 0)
          .addParam('token', token)
          .requestUpdate("systemMsg", {
            onComplete: function () {
              LongRequestLog.refresh();
            }, method:  "post"
          })
      }
    })
  },

  confirmLogPurge: function (message, url_route, token) {
    Modal.confirm($T(message), {
      onOK: function () {
        new Url().setRoute(url_route)
          .addParam('purge', 1)
          .addParam('token', token)
          .requestUpdate("systemMsg", {
            onComplete: function () {
              LongRequestLog.refresh();
            }, method:  "post"
          })
      }
    })
  },

  changePageLongRequest: function (start) {
    var form = getForm("Filter-Log");
    $V(form.elements.start, start);
    form.onsubmit();
  },

  checkModule: function (input) {
    var form = input.form;
    ($V(input)) ? form.elements.filter_action.disabled = '' : form.elements.filter_action.disabled = '1';
  },

  explain: function (sample, token) {
    const url = new Url('system', 'explainQuery', 'dosql');
    url.addParam('query_sample', sample);
    url.addParam('@token', token);

    url.requestJSON(function (data) {
      if ((data.explain === undefined) || (data.explain === null)) {
        console.error('An error occurred');
      } else {
        let modal_creation = false;

        let modal = $(data.uid);

        if (modal === null) {
          modal_creation = true;
          modal = DOM.div({id: data.uid, style: 'display: none;'},);
        }

        let rows = [];

        data.explain.each(function (elt) {
          rows.push(
            DOM.tr(
              {},
              DOM.td({}, elt.select_type),
              DOM.td({}, elt.table),
              DOM.td({}, elt.type),
              DOM.td({}, elt.possible_keys),
              DOM.td({}, elt.key),
              DOM.td({}, elt.key_len),
              DOM.td({}, elt.ref),
              DOM.td({}, elt.rows),
              DOM.td({}, elt.Extra),
            )
          );
        });

        let replay = null;
        let replay_result = null;
        let replay_warning = null;

        if (data.query.toUpperCase().startsWith('SELECT')) {
          const method = printf("LongRequestLog.replay(this, '%s', '%s');", sample, token);
          replay = DOM.button({type: 'button', className: 'change', onclick: method}, 'Rejouer (sans cache)');

          replay_result = DOM.div({style: 'display: inline-block;'});
          replay_warning = DOM.div({className: 'small-warning'}, DOM.span({}, $T('CSQLDataSource-msg-This query will be executed onto %s datasource', data.real_dsn)));
        }

        let container = DOM.div(
          {},
          DOM.div(
            {},
            DOM.h3({}, 'Requ�te'),
            DOM.span({}, 'Source de donn�es : ', data.dsn),
            DOM.br({}),
            DOM.code({}, data.query),
            DOM.br({}),
            replay,
            replay_result,
            replay_warning,
          ),
          DOM.br({}),
          DOM.h3({}, 'EXPLAIN'),
          DOM.table(
            {className: 'main tbl'},
            DOM.tr(
              {},
              DOM.th({}, 'Select type'),
              DOM.th({}, 'Table'),
              DOM.th({}, 'Type'),
              DOM.th({}, 'Possible keys'),
              DOM.th({}, 'Key'),
              DOM.th({}, 'Key len'),
              DOM.th({}, 'Ref'),
              DOM.th({}, 'Rows'),
              DOM.th({}, 'Extra'),
            ),
            ...rows
          )
        );

        modal.update(container);

        if (modal_creation) {
          document.body.append(modal);
        }

        Modal.open(modal, {showClose: true, width: '800px', height: '400px;'});
      }
    }, {
      method: 'post',
    });

    return false;
  },

  replay: function (input, sample, token) {
    const url = new Url('system', 'replayQuery', 'dosql');
    url.addParam('query_sample', sample);
    url.addParam('@token', token);

    url.requestJSON(function (data) {
      if ((data.duration === undefined) || (data.duration === null)) {
        console.error('An error occurred');
      } else {
        input.next('div').update(
          DOM.div({className: 'small-info', style: 'white-space: nowrap'}, DOM.span({}, 'Duration: ' + data.duration + ' ms, Count rows: ' + data.rows))
        );
      }
    }, {
      method: 'post',
    });
  }
};
