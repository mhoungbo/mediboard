/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

CacheViewer = {
  showDetail: function (trigger) {
    const tbody = trigger.up("tbody").next(".cache-detail");

    if (!trigger.hasClassName("unfold")) {
      new Url().setRoute(trigger.get("url"))
        .addParam("type", trigger.get("type"))
        .addParam("prefix", trigger.get("prefix"))
        .requestUpdate(tbody);

      trigger.addClassName("unfold");
    } else {
      trigger.removeClassName("unfold");
      tbody.update();
    }
  },

  showKeyDetail: function (elt) {
    new Url().setRoute(elt.get("url"), 'system_gui_cache_view_details', 'system')
      .addParam("key", elt.get("key"))
      .addParam("type", elt.get("type"))
      .requestModal(600);
  },

  removeKey: function (elt) {
    const key = elt.get("key");

    new Url().setRoute(elt.get("url"))
      .addParam('type', elt.get("type"))
      .addParam('key', key)
      .addParam('token', elt.get("token"))
      .requestUpdate("systemMsg", {
        method:     'post',
        onComplete: function () {
          const trigger = $$("a[data-prefix='" + key.split('-')[0] + "']")[0];
          trigger.removeClassName("unfold");
          CacheViewer.showDetail(trigger);
        },
      });
  }
};
