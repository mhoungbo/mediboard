/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ObjectNavigation = window.ObjectNavigation || {
    openModalObject: function (url_elem, class_name, id) {
      if (!id) {
        return;
      }

      new Url().setRoute(url_elem.get('url'))
      .requestModal(1000, 800, {
        title: class_name + ' #' + id
      });
    },

    classShow: function (url_elem, class_name, class_id) {
      new Url().setRoute(url_elem.get('url'))
      .requestModal(1000, 800, {
        title: class_name + ' #' + class_id
      });
    },

    changePage: function (page, arg) {
      $V(getForm("filter_back_" + arg).start, page);
    },
  }
;
