/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ViewSenderSource = {
  edit: function(sender_source_id, url) {
    if (Object.isUndefined(sender_source_id)) {
      return;
    }

    new Url().setRoute(url, 'system_gui_view_senders_sources_view_edit', 'system')
      .addParam('type', 'source')
      .addParam('sender_source_id', sender_source_id)
      .requestModal(700, 900, {onClose: ViewSenderSource.refreshList});
  },

  confirmDeletion: function(form) {
    var options = {
      typeName: "source d'export",
      objName: $V(form.name),
      ajax: 1,
      callback: function() {
        $V(form.callback, "0");
        return onSubmitFormAjax(form, Control.Modal.close);
      }
    };

    confirmDeletion(form, options);
  },

  refreshList: function() {
    new Url().setRoute($('list-sources').get('url'))
      .addParam('type', 'source')
      .requestUpdate('list-sources');
  },

  showOrHidePassword: function(form) {
    var radio = form.elements.archive;
    var password = $('password-source-field');
    (radio.value === '0') ? password.hide() : password.show();
  }
};
