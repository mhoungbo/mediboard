/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

DSN = {
  load:                            function (dsn, container, url_load_dsn) {
    new Url().setRoute(url_load_dsn, 'system_gui_datasources_show', 'system')
      .addParam('type', 'sql')
      .addParam("dsn", dsn)
      .addParam("dsn_uid", $(container).id)
      .requestUpdate(container);
  },
  test:                            function (dsn, target, url_test_dsn) {
    new Url().setRoute(url_test_dsn)
      .addParam('dsn', dsn)
      .addParam('type', 'sql')
      .requestUpdate(target || ("dsn-status-" + dsn));
  },
  edit:                            function (dsn, container, url_edit_form, url_load_dsn) {
    new Url()
      .setRoute(url_edit_form, 'system_gui_datasources_view_edit_form', 'system')
      .addParam('type', 'sql')
      .addParam('dsn', dsn)
      .requestModal(500, 400, {
        onClose: function () {
          DSN.load(dsn, container, url_load_dsn);
        }
      });
  },
  save:                            function (form, element) {
    if (!element.disabled && $V(element) === '') {
      Modal.confirm($T('SQLDSNConfiguration-confirm-Are you sure to reset the password?'), {
        onOK: function () {
          return onSubmitFormAjax(form);
        }
      });
      return false;
    }
    return onSubmitFormAjax(form);
  },
  create:                          function (form) {
    return onSubmitFormAjax(form, null, "config-dsn-create-" + $V(form.dsn));
  },
  createDB:                        function (dsn, host, url_edit_db_form) {
    new Url()
      .setRoute(url_edit_db_form, 'system_gui_datasources_db_view_edit_form', 'system')
      .addParam("dsn", dsn)
      .addParam("host", host)
      .requestModal(500, 400);
  },
  togglePasswordModificationField: function (lock, field) {
    lock.classList.toggle('lock');
    lock.classList.toggle('unlock');
    field.disabled = !field.disabled;
  }
};
