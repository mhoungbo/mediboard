/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Module = {
  list:     [],
  show_url: '',

  refreshModuleLine: function (id) {
    new Url().setRoute(Module.show_url)
      .addParam('mod_id', id)
      .requestUpdate('mod_' + id, Module.updateInstalledControlTabs);
  },

  updateInstalledControlTabs: function () {
    const upgradableCount = $('installed').select('button.upgrade').length;
    const upgradeAllButton = $("upgrade-all-button");
    const count_zero = upgradableCount === 0;

    if (upgradeAllButton) {
      upgradeAllButton.down("span").update(upgradableCount);
      if (count_zero) {
        upgradeAllButton.disable();
      }
    }

    if (count_zero) {
      $$('a[href=#installed]')[0].removeClassName("wrong");
    }
  },

  updateAll: function () {
    Module.list = $("installed").select("form.upgrade").sort(function (formA, formB) {

      let intA = parseInt(formA.get("dependencies"), 10);
      let intB = parseInt(formB.get("dependencies"), 10);

      intA = isNaN(intA) ? 0 : intA;
      intB = isNaN(intB) ? 0 : intB;

      if (intA > intB) {
        return 1;
      }
      if (intB > intA) {
        return -1;
      }
      return 0;

    });

    Module.updateChain(Module.list.shift());
  },

  updateOne: function (form, callback) {
    WaitingMessage.cover(form.up("tr"));

    return onSubmitFormAjax(form, function () {
      Module.refreshModuleLine(form.get("id"));

      if (callback) {
        callback();
      }
    })
  },

  updateChain: function (form) {
    if (!form) {
      return;
    }

    Module.updateOne(form, Module.updateChain.curry(Module.list.shift()));
  },

  viewRequirements: function (route_requirements) {
    new Url().setRoute(route_requirements, 'system_gui_modules_requirements', 'system').requestModal('100%', '100%');
  },
};
