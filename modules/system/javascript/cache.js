/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

CacheManager = {
  url_view_confirm: '',
  getMessage:       function (cache, target, module) {
    let message = $T('CacheManager-msg-confirm delete');

    if (cache) {
      message += '<br> - ' + (!module ? $T('CacheManager-cache_values.' + cache) : cache);
    }

    if (module) {
      message += '<br>' + $T('CacheManager-msg-for module') + '<br> - ' + module;
    }

    if (target) {
      message += '<br>' + $T('CacheManager-msg-on targets') + '<br> - ' + target;
    }

    message += '<br>';

    return message;
  },
  clear:            function (cache, target, layer, url_clear_cache) {
    new Url().setRoute(url_clear_cache)
      .addParam('cache', cache)
      .addParam('target', target)
      .addParam('layer', layer)
      .requestUpdate('CacheManagerOutputs');
  },
  openModalConfirm: function (cache, target, layer, module, keys) {
    new Url().setRoute(CacheManager.url_view_confirm, 'system_gui_cache_view_confirm', 'system')
      .addParam('cache', cache)
      .addParam('target', target)
      .addParam('layer', layer)
      .addParam('module', module)
      .addParam('keys', keys)
      .requestModal('80%', '90%');
  },
};
