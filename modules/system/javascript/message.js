/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Message = {
  refresh_url: "",
  update_url:  "",
  edit:        function (message_id) {
    const url = new Url().setRoute(Message.update_url, 'system_gui_messages_edit', 'system');
    if (message_id) {
      url.addParam("message_id", message_id);
    }
    url.requestModal(500, "85%");
  },

  onSubmit: function (form) {
    return onSubmitFormAjax(form, {
      onComplete: function () {
        Message.refreshList();
        Control.Modal.close();
      }
    })
  },

  createUpdate: function () {
    new Url().setRoute(Message.update_url, 'system_gui_messages_edit', 'system')
      .addParam("is_update_message", 1)
      .requestModal(500)
  },

  onSubmitUpdate: function (form) {
    if (!checkForm(form)) {
      return false
    }

    Control.Modal.close();

    new Url().setRoute(Message.update_url, 'system_gui_messages_edit', 'system')
      .addElement(form._update_moment)
      .addElement(form._update_initiator)
      .addElement(form._update_benefits)
      .requestModal(500);

    return false
  },

  duplicate: function (form) {
    $V(form.message_id, "");
    $V(form.titre, "copie de " + $V(form.titre));
  },

  confirmDeletion: function (form) {
    const options = {
      typeName: "message",
      objName:  $V(form.titre),
      ajax:     1
    };

    const ajax = {
      onComplete: function () {
        Message.refreshList();
        Control.Modal.close();
      }
    };

    confirmDeletion(form, options, ajax);
  },

  refreshList: function (page) {
    const url = new Url().setRoute(Message.refresh_url);
    const form = getForm("refresh_messages");

    let status = '';
    $$('input.status').each(function (elt) {
      if (elt.checked) {
        if (status) {
          status += '|' + elt.value;
        } else {
          status = elt.value;
        }
      }
    });

    $V(form.elements.status, status);

    if (form && $V(form.status)) {
      url.addParam("status", $V(form.status));
    }

    if (page) {
      url.addParam("start", page);
    }

    url.requestUpdate("list-messages");
  },

  hideMessage: function (guid, element) {
    $(guid).hide();
  },

  acquittalsSwitchPage: function (start, url) {
    const div_acquittals = $("acquitment-list");
    new Url()
      .setRoute(url)
      .addParam('start', start)
      .addParam('message_id', div_acquittals.get('message-id'))
      .requestUpdate("acquitment-list");

    div_acquittals.dataset.page++
  },

  makeGroupAutocomplete: function (form, url) {
    new Url().setRoute(url)
      .addParam("edit", "1")
      .addParam("input_field", "group_id_view")
      .addParam("view_field", "text")
      .autoComplete(form.group_id_view, null, {
        minChars:           0,
        method:             "get",
        select:             "view",
        dropdown:           true,
        afterUpdateElement: function (field, selected) {
          $V(form.group_id, selected.getAttribute("id").split("-")[2]);
        }
      });
  },

  setCurrentGroup: function (form, group_id, group_view) {
    $V(form.group_id_view, group_view);
    $V(form.group_id, group_id);
  },
};
