/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * JS function Exchange Source
 */
ExchangeSource = {
    status_color: ["red", "orange", "limegreen"],

    resfreshImageStatus: function (element) {
        if (!element.get('id')) {
            return;
        }

        var url = new Url("system", "ajax_get_source_status");

        element.title = "";

        url.addParam("source_guid", element.get('guid'));
        url.requestJSON(function (status) {
            element.setStyle({color: ExchangeSource.status_color[status.reachable]});

            element.onmouseover = function () {
                ObjectTooltip.createDOM(element,
                    DOM.div(null,
                        DOM.table({className: "main tbl", style: "max-width:350px"},
                            DOM.tr(null,
                                DOM.th(null, status.type)
                            ),
                            DOM.tr(null,
                                DOM.td({className: "text"},
                                    DOM.strong(null, "Nom : "), status.name)
                            ),
                            DOM.tr(null,
                                DOM.td({className: "text"},
                                    DOM.strong(null, "Message : "), status.message)
                            ),
                            DOM.tr(null,
                                DOM.td({className: "text"},
                                    DOM.strong(null, "Temps de r�ponse : "), status.response_time, " ms")
                            )
                        )
                    ).hide())
            };
        });
    },

    manageFiles: function (source_guid) {
        new Url("system", "ajax_manage_files")
            .addParam("source_guid", source_guid)
            .requestModal(1000, 500);
    },

    showDirectory: function (source_guid) {
        new Url("system", "ajax_manage_directory")
            .addParam("source_guid", source_guid)
            .requestUpdate(
                "listDirectory",
                {onComplete: () => ExchangeSource.showFiles(source_guid)}
            );
    },

    changeDirectory: function (source_guid, directory) {
        new Url("system", "ajax_manage_directory")
            .addParam("source_guid", source_guid)
            .addParam("new_directory", directory)
            .requestUpdate(
                "listDirectory",
                {onComplete: () => ExchangeSource.showFiles(source_guid, directory)}
            );
    },

    showFiles: function (source_guid, current_directory) {
        new Url("system", "ajax_manage_file")
            .addParam("source_guid", source_guid)
            .addParam("current_directory", current_directory)
            .requestUpdate("listFiles");
    },

    deleteFile: function (source_guid, file, current_directory) {
        new Url("system", "ajax_manage_file")
            .addParam("source_guid", source_guid)
            .addParam("current_directory", current_directory)
            .addParam("delete", true)
            .addParam("file", file)
            .requestUpdate("listFiles");
    },

    renameFile: function (source_guid, file, current_directory) {
        var new_name = prompt("Etes-vous s�r de vouloir renommer le fichier '" + file + "'\nEntrez le nouveau nom du fichier", "");
        if (new_name === null || new_name === "") {
            return false;
        }

        new Url("system", "ajax_manage_file")
            .addParam("source_guid", source_guid)
            .addParam("current_directory", current_directory)
            .addParam("file", file)
            .addParam("new_name", new_name)
            .addParam("rename", true)
            .requestUpdate("listFiles");
        return true;
    },

    addFileForm: function (source_guid, current_directory) {
        new Url('system', 'ajaxAddFile')
            .addParam("source_guid", source_guid)
            .addParam("current_directory", current_directory)
            .requestModal(700, 300)
            .modalObject.observe("afterClose", function () {
            ExchangeSource.showFiles(source_guid, current_directory)
        });
    },

    closeAfterSubmit: function (message) {
        window.parent.$("systemMsg").update("");
        if (message["resultNumber"] != '0') {
            window.parent.SystemMessage.notify(DOM.div({class: "info"}, message["result"] + " x" + message["resultNumber"] + "<br/>"), true);
        }
        var length = message["error"].length;
        if (length !== 0) {
            for (var i = 0; i < length; i++) {
                window.parent.SystemMessage.notify(DOM.div({class: "error"}, message["error"][i] + "<br/>"), true);
            }
        }
        window.parent.Control.Modal.close();
    },

    addInputFile: function (elt) {
        var name = elt.name;
        var number_file = name.substring(name.lastIndexOf("[") + 1, name.lastIndexOf("]"));
        number_file = parseInt(number_file);
        number_file += 1;
        var form = elt.up();
        var br = form.insertBefore(DOM.br(), elt.nextSibling);
        form.insertBefore(DOM.input({
                type: "file",
                name: "import[" + number_file + "]",
                size: 0,
                onchange: "ExchangeSource.addInputFile(this); this.onchange=''"
            })
            , br.nextSibling);
    },

    editSource: function (guid, light, source_name, type, object_guid, callback) {
        new Url("eai", "ajax_edit_source")
            .addParam("source_guid", guid)
            .addParam("source_name", source_name)
            .addParam("light", light)
            .addParam("object_guid", object_guid)
            .requestModal(600)
            .modalObject.observe("afterClose", callback);
    },


    SourceReachable: function (container) {
        var list = container.select("i");
        if (!list[0].id) {
            return;
        }

        var list = container.select("i");
        ExchangeSource.testReachable(list[0], list[0].id);
    },

    testReachable: function (element, id_div) {
        var url = new Url("system", "ajaxGetSourceReachable");

        element.title = "";
        url.addParam("source_guid", element.get('guid'));
        url.requestJSON(function (status) {
            element.setStyle({color: ExchangeSource.status_color[status.reachable]});
            var tdtime = element.up().next();
            $(tdtime).update(status.message);
        })
    },

    refreshUserSources: function () {
        new Url('mediusers', 'ajax_edit_exchange_sources')
            .requestUpdate('edit-exchange_source');
    },

    refreshExchangeSource: function (source_name, type, dontCloseModal) {
        var url = new Url("system", "ajax_refresh_exchange_source")
            .addParam("type", type)
            .addParam("exchange_source_name", source_name);
        if (dontCloseModal) {
            url.addParam('dont_close_modal', 1);
        }
        url.requestUpdate('exchange_source-' + source_name);
    },

    unlock: function (exchange_source_name, exchange_source_class) {
        new Url("eai", "ajaxUnlockAdvancedSource")
            .addParam("exchange_source_name", exchange_source_name)
            .addParam("exchange_source_class", exchange_source_class)
            .requestModal(500, 400)
            .modalObject.observe("afterClose");
    },

    updateDateTime: function (source_name, receiver_name, value, reset) {
        source_name = source_name.substring(1);
        var element = document.getElementById('edit' + source_name + '-' + receiver_name);
        var initial_value = element.value;

        if (!initial_value) {
            initial_value = 0;
        }

        if (reset) {
            element.value = "";
            document.getElementById("result_" + receiver_name).value = "";
        } else {
            element.value = parseInt(initial_value * 24) + parseInt(value * 24);
            document.getElementById('edit' + source_name + "-" + receiver_name).value = parseInt(element.value) / 24;
        }
    }

};
