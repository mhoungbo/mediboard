/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

SourceToViewSender = {
  edit: function (sender_id, url_element, options) {
    new Url().setRoute(url_element.get('url'), 'system_gui_view_senders_view_edit_link', 'system')
      .addParam('sender_id', sender_id)
      .addParam('type', 'link')
      .requestModal(400, 250, options);
  },

  onSubmit: function (form) {
    return onSubmitFormAjax(form, {
      onComplete: function () {
        ViewSender.refreshList();
        Control.Modal.close();
      }
    })
  },

  confirmDeletion: function (form) {
    var options = {
      typeName: 'lien export - source d\'export',
      objName:  $V(form.source_to_view_sender_id),
      ajax:     1
    }
    var ajax = {
      onComplete: function () {
        ViewSender.refreshList();
        Control.Modal.close();
      }
    }

    confirmDeletion(form, options, ajax);
  }
};
