/**
 * @package Mediboard\
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

KeyMetadata = {
  generateKey: function (metadata_id, generate_route, refresh_route, token, callback) {
    callback = callback || function () {
    };

    Modal.confirm($T('KeyBuilder-warning-Key generation'), {
      onOK:      function () {
        const url = new Url().setRoute(generate_route);
        url.addParam('token', token);

        url.requestUpdate('systemMsg', {
          method:     'post',
          onComplete: function () {
            KeyMetadata.refreshMetadata(metadata_id, refresh_route);

            callback();
          }
        });
      },
      className: 'modal alert big-warning'
    });
  },

  uploadCert: function (form, renew, callback) {
    callback = callback || function () {
    };

    const msg = (renew === '1') ? $T('KeyBuilder-warning-Renewing certificate') : $T('KeyBuilder-warning-Uploading certificate');

    // Length of 2 seems the way to ensure we have only one file
    const file_check = (form.elements['formfile[]'].length === 2);

    if (!file_check) {
      Modal.alert($T('KeyBuilder-error-Only one file is allowed per certificate'));
      return false;
    }

    Modal.confirm(msg, {
      onOK:      function () {
        return onSubmitFormAjax(form, {onComplete: callback});
      },
      className: 'modal alert big-warning'
    });

    return false;
  },

  refreshMetadata: function (metadata_id, route) {
    const url = new Url().setRoute(route);

    url.requestUpdate('key-metadata-' + metadata_id);
  },

  showDetails: function (route, callback) {
    callback = callback || function () {
    };

    const url = new Url().setRoute(route, 'system_gui_key_metadata_details', 'system');

    url.requestModal(800, 800, {onClose: callback});
  },

  showForm: function (route, dom_id) {
    const url = new Url().setRoute(route);

    url.requestUpdate(dom_id);
  },

  showUsage: function (route, dom_id) {
    const url = new Url().setRoute(route);

    url.requestUpdate(dom_id);
  },

  showKeyInfo: function (route, dom_id) {
    const url = new Url().setRoute(route);

    url.requestUpdate(dom_id);
  }
};
