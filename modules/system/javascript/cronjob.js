/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

CronJob = {
  edit: function (cronjob_id, url_edit) {
    const url = new Url().setRoute(url_edit, 'system_gui_cronjob_view_edit', 'system');

    if (cronjob_id) {
      url.addParam("cronjob_id", cronjob_id);
    }

    url.requestModal('30%', '90%', {onClose: CronJob.refreshListCronjobs});
  },

  refreshListCronjobs: function () {
    const form = getForm("search_cronjob_list");
    form.onsubmit();
  },

  changeField: function (element) {
    let value = true;
    if ($V(element) === "") {
      value = false;
    }
    const form = element.form;
    form._second.disabled = value;
    form._minute.disabled = value;
    form._hour.disabled = value;
    form._day.disabled = value;
    form._month.disabled = value;
    form._week.disabled = value;
  },

  changePageList: function (page) {
    const form = getForm("search_cronjob_list");
    $V(form.page, page);
    form.onsubmit();
  },

  toggleActive: function (radio_button) {
    const list_elements_tp = radio_button.up(1).children;

    if (radio_button[2].checked === false && radio_button[3].checked === true) {
      for (const element of list_elements_tp) {
        element.style.opacity = "100%";
      }
    } else {
      for (const element of list_elements_tp) {
        element.style.opacity = "30%";
      }
      list_elements_tp[0].style.opacity = "100%";
    }
  },

  setServerAddress: function (element) {
    const tokenfield = new TokenField(element.form.servers_address);
    if ($V(element)) {
      tokenfield.add(element.value);
    } else {
      tokenfield.remove(element.value);
    }
  },

  refreshListLogs: function () {
    const form = getForm("search_cronjob");
    form.onsubmit();
  },

  changePageLog: function (page) {
    const form = getForm("search_cronjob");
    $V(form.page, page);
    form.onsubmit();
  },

  displayErrors: function (request_uid, url) {
    new Url().setRoute(url, 'developpement_gui_logs', 'dPdeveloppement')
      .addParam('request_uid', request_uid)
      .addParam('hide_filters', 1)
      .requestModal('90%', '90%');
  }
};
