/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ElasticDSN = {
  load:                            function (dsn, container, url_load_dsn) {
    new Url().setRoute(url_load_dsn, 'system_gui_datasources_show', 'system')
      .addParam('type', 'nosql')
      .addParam("dsn", dsn)
      .addParam("dsn_uid", $(container).id)
      .requestUpdate(container);
  },
  test:                            function (dsn, module, url_test_dsn) {
    new Url().setRoute(url_test_dsn, 'system_gui_datasources_test_elastic', 'system')
      .addParam('type', 'nosql')
      .addParam("dsn", dsn)
      .addParam("module", module)
      .requestModal("100%", "100%");
  },
  edit:                            function (dsn, container, url_edit_form, url_load_dsn) {
    new Url()
      .setRoute(url_edit_form, 'system_gui_datasources_view_edit_form_index', 'system')
      .addParam("dsn", dsn)
      .addParam('type', 'nosql')
      .requestModal(500, 400, {
        onClose: function () {
          ElasticDSN.load(dsn, container, url_load_dsn);
        }
      });
  },
  create:                          function (form) {
    return onSubmitFormAjax(form, null, "config-dsn-create-" + $V(form.dsn));
  },
  init:                            function (dsn, module, url_init_elastic, container) {
    new Url()
      .setRoute(url_init_elastic, 'system_gui_datasources_init_elastic', 'system')
      .addParam("dsn", dsn)
      .addParam("setup_module", module)
      .requestUpdate(container, {
        onComplete: function () {
          Control.Modal.refresh();
        }
      });
  },
  save:                            function (form, element) {
    if (!element.disabled && $V(element) === '') {
      Modal.confirm($T('ElasticDSNConfiguration-confirm-Are you sure to reset the password?'), {
        onOK: function () {
          return onSubmitFormAjax(form);
        }
      });
      return false;
    }
    return onSubmitFormAjax(form);
  },
  togglePasswordModificationField: function (lock, field) {
    lock.classList.toggle('lock');
    lock.classList.toggle('unlock');
    field.disabled = !field.disabled;
  }
};
