/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Translation = {
  doTranslations: function (url_import, csrf_token, module) {
    const key = (module) ? '.translation-' + module : '.translation';

    const translations = [];
    $$(key).each(function (elem) {
      if (elem.checked) {
        translations.push(
          {key: elem.value, trad: elem.get('trad'), lang: elem.get('lang')}
        );
      }
    });

    new Url().setRoute(url_import)
      .addParam('translations', Object.toJSON(translations))
      .requestUpdate('systemMsg', {
        method: 'post', onComplete: function () {
          getForm('import-translations-form').onsubmit();
        }
      });
  },

  checkAllTranslation: function (checkbox, check_class) {
    $$('.' + check_class).each(function (elem) {
      elem.checked = checkbox.checked;
    });
  },

  editTrad: function (id, url_edit) {
    const url = new Url().setRoute(url_edit, 'system_gui_locales_view_edit', 'system');

    if (id) {
      url.addParam("trad_id", id);
    }

    url.requestModal(700, 400);
  },

  changePage: function (start) {
    const form = getForm('search-translation');

    if (start !== undefined) {
      $V(form.elements.start, start);
    }

    form.onsubmit();
  },

  vwImportTranslations: function (elt) {
    new Url().setRoute(elt.get('url'), 'system_gui_locales_view_import', 'system')
      .requestModal('80%', '90%', {
        onClose: function () {
          Translation.changePage();
        }
      });
  },

  confirmPurgeTranslations: function (elt) {
    Modal.confirm(
      $T('CTranslationOverwrite-purge useless?'),
      {
        onOK: function () {
          new Url().setRoute(elt.get('url'))
            .addParam('start', getForm('search-translation').elements.start)
            .addParam('token', elt.get("token"))
            .requestUpdate('systemMsg', {
              method: 'post', onComplete: function () {
                Translation.changePage();
              }
            });
        }
      }
    );
  },

  confirmDeletion: function (form) {
    const options = {
      typeName: "traduction de remplacement",
      objName:  $V(form.name),
      ajax:     1,
      callback: function () {
        return onSubmitFormAjax(form, {
          onComplete: function () {
            Translation.changePage();
            Control.Modal.close;
          }
        });
      }
    };

    confirmDeletion(form, options);
  },

  searchTranslation: function () {
    const form = getForm('search-translation');
    $V(form.elements.start, 0);
    form.onsubmit();
  },

  export: function (elt) {
    new Url().setRoute(elt.get("url"))
      .addParam('export', 1)
      .popup(400, 150);
  }
};
