/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ViewSender = {
  status_images: ["images/icons/status_red.png", "images/icons/status_orange.png", "images/icons/status_green.png"],
  senders:       {},

  edit: function (sender_id, url_element) {
    new Url().setRoute(url_element.get('url'), 'system_gui_view_senders_view_edit', 'system')
      .addParam('sender_id', sender_id)
      .requestModal(450);
  },

  show: function (sender_id) {
    if (!this.senders[sender_id]) {
      return;
    }

    let [path, params] = this.senders[sender_id].split('?');

    let url = new Url();
    url.setRoute(path);

    params.split('&').each(function (elem) {
      let [name, value] = elem.split('=');
      url.addParam(name, value);
    });

    url.popup(1000, 700);
  },

  testSender: function (element, sender_id) {
    const route = element.getAttribute('data-route');
    if (!route) {
      return;
    }

    (new Url())
      .setRoute(route, 'system_gui_view_senders_view_edit-test', 'system')
      .addParam('sender_id', sender_id)
      .addParam('test', 1)
      .requestModal("100%", "100%");
  },

  purgeDirectories: function (element, sender_id) {
    const route = element.getAttribute('data-route');
    if (!route) {
      return;
    }

    (new Url())
      .setRoute(route, 'system_gui_view_senders_view_edit-purge', 'system')
      .addParam('sender_id', sender_id)
      .addParam('purge', 1)
      .requestModal("100%", "100%");
  },

  onSubmit: function(form) {
    return onSubmitFormAjax(form, {
      onComplete: function() {
        ViewSender.refreshList();
        ViewSender.modal.close();
      }
    })
  },

  duplicate: function (form) {
    $V(form.sender_id, '');
    $V(form.active, '0');
    $V(form.name, 'copie de ' + $V(form.name));
  },

  confirmDeletion: function (form) {
    var options = {
      typeName: 'export',
      objName:  $V(form.name)
    }

    var ajax = {
      onComplete: function () {
        ViewSender.refreshList();
        Control.Modal.close();
      }
    }

    confirmDeletion(form, options, ajax)
  },

  urlToParams: function (button) {
    const form = button.form;
    let area = form.params;
    area.value = "";
    form.route_name = '';
    form.search = '';
    let url = prompt('URL � importer');

    new Url().setRoute(button.get('url')).addParam('url', url).requestJSON(function (response) {
      // Set the query parameters
      if (response.query) {
        response.query.split('&').each(function (param) {
          if (param !== 'dialog=1') {
            area.value += param + '\n';
          }
        });
      }

      // If the url correspond to a route, set it.
      if (response.route_name) {
        $V(form.search, response.route_name);
        $V(form.route_name, response.route_name);
      }
    });
  },

  refreshList: function (plan_mode) {
    const form = getForm('senders-list');
    if (!form) {
      return;
    }

    $V(form.plan_mode, plan_mode);
    form.onsubmit();
  },

  refreshMonitor: function () {
    getForm('monitor-search').onsubmit();
  },

  changeMonitorOrder: function (field, way) {
    const form = getForm('monitor-search');
    $V(form.order_date, way);
    ViewSender.refreshMonitor();
  },

  changePageMonitoring: function (start) {
    const form = getForm('monitor-search');
    $V(form.start, start);
    ViewSender.refreshMonitor();
  },

  resetMonitoring: function () {
    const form = getForm('monitor-search');
    $V(form.start, 0);
  },

  doSend: function (send) {
    new Url().setRoute($('dosend').get('url'))
      .addParam('send', send)
      .requestUpdate('dosend');
    return false;
  },

  productAndSend: function (view_sender_id, url_element) {
    if (!view_sender_id) {
      return;
    }

    new Url().setRoute(url_element.get('url'))
      .addParam('view_sender_id', view_sender_id)
      .requestUpdate('systemMsg');
  },

  openSenderSourceLink: function (sender_id, url_element) {
    SourceToViewSender.edit(sender_id, url_element, {
      onClose: function () {
        Control.Modal.refresh()
      }
    })
  }
};
