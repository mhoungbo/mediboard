<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CMbDT;
use Ox\Core\CSmartyDP;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\System\CNote;

CCanDo::check();

$note_id     = CView::get('note_id', 'ref class|CNote');
$object_guid = CView::get('object_guid', 'str');

CView::checkin();

$note = new CNote();
$note->load($note_id);

$current_user = CUser::get();

if (!$note->_id) {
    $note->setObject(CStoredObject::loadFromGuid($object_guid));
    $note->user_id = $current_user->_id;
    $note->date    = CMbDT::dateTime();
}

$note->loadTargetObject()->needsRead();
$note->loadRefUser()->loadRefFunction();
$note->needsEdit();

$csrf_token = AntiCsrf::prepare()
    ->addParam('note_id', $note->_id)
    ->addParam('user_id', $note->_id ? $note->user_id : $current_user->_id)
    ->addParam('guid', $note->_id ? $note->_guid : 'CNote-none')
    ->addParam('object_id', $note->_id ? $note->object_id : null)
    ->addParam('object_class', $note->_id ? $note->object_class : null)
    ->addParam('_share_ids')
    ->addParam('date_da')
    ->addParam('date')
    ->addParam('public')
    ->addParam('degre')
    ->addParam('text')
    ->addParam('_user_autocomplete');

$smarty = new CSmartyDP();
$smarty->assign('note', $note);
$smarty->assign('csrf_token', $csrf_token->getToken());
$smarty->display('edit_note.tpl');
