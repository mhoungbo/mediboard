<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Mediboard\System\Log\CLongRequestLog;

CCanDo::checkAdmin();

$log_id = CView::get("log_id", "ref class|CLongRequestLog");

CView::checkin();

$log = new CLongRequestLog();
$log->load($log_id);
$log->getLink();
$log->getModuleAction();
$log->loadRefSession();

if (!$log->isPublic()) {
    $log->loadRefUser()->loadRefFunction();
}

$smarty = new CSmartyDP();
$smarty->assign("log", $log);
$smarty->display("edit_long_request_log.tpl");
