
export interface ConfigurationStructure {
    [configurationName: string]: string
}

export interface ContextConfiguration {
    [contextName: string]: {
        [contextIdentifier: string]: ConfigurationStructure
    } | ConfigurationStructure
}
