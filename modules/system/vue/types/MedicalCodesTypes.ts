export const MedicalCodeFilterHeaderLeft = "ArrowLeft"
export const MedicalCodeFilterHeaderRight = "ArrowRight"
export const MedicalCodeFilterHeaderShortcuts = [
    MedicalCodeFilterHeaderLeft,
    MedicalCodeFilterHeaderRight
]
