/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import { ConfigurationStructure, ContextConfiguration } from "@modules/system/vue/types/ConfigurationsTypes"

export default class Configurations extends OxObject {
    constructor () {
        super()
        this.type = "configurations"
    }

    get instance (): OxAttr<ConfigurationStructure | []> {
        return super.get("instance")
    }

    get static (): OxAttr<ConfigurationStructure | []> {
        return super.get("static")
    }

    get context (): OxAttr<ContextConfiguration | []> {
        return super.get("context")
    }

    get dateFormat (): string {
        return super.get("date_format")
    }

    get isDistinctGroups (): boolean {
        return super.get("is_distinct_groups")
    }

    get isQualif (): boolean {
        return super.get("is_qualif")
    }
}
