/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import HistoryFieldDiff from "@modules/system/vue/models/HistoryFieldDiff"
import OxMoment from "@/core/utils/OxMoment"

export default class History extends OxObject {
    constructor () {
        super()
        this.type = "history"
    }

    protected _relationsTypes = {
        mediuser: Mediuser,
        field_diff: HistoryFieldDiff
    }

    get datetime (): string {
        const moment = new OxMoment(this.dateData)
        return moment.format("DD/MM/YYYY HH:mm:ss")
    }

    get dateFromNow (): string {
        const moment = new OxMoment(this.dateData)
        return moment.fromNow(true)
    }

    get dateData (): OxAttr<string> {
        // @TODO: Need to get the right backend date format declared in config (maybe requires a converter)
        return super.get("date")
    }

    get historyType (): OxAttr<string> {
        return super.get("type")
    }

    set historyType (value: OxAttr<string>) {
        this.set("type", value)
    }

    get fields (): HistoryFieldDiff[] {
        return this.loadBackwardRelation("updatedFields")
    }

    public setUpdatedFields (fields: HistoryFieldDiff[]) {
        this.setBackwardRelation("updatedFields", fields)
    }

    get user (): Mediuser | undefined {
        return this.loadForwardRelation<Mediuser>("user")
    }

    set user (value: Mediuser | undefined) {
        this.setForwardRelation("user", value)
    }
}
