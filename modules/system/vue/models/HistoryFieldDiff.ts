/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"

export default class HistoryFieldDiff extends OxObject {
    constructor () {
        super()
        this.type = "field_diff"
    }

    get name (): string {
        return super.get("field_name")
    }

    set name (value: string) {
        this.set("field_name", value)
    }

    get oldValue (): string | null {
        return super.get("old_value")
    }

    set oldValue (value: string | null) {
        this.set("old_value", value)
    }

    get newValue (): string | null {
        return super.get("new_value")
    }

    set newValue (value: string | null) {
        this.set("new_value", value)
    }

    get diff (): string | null {
        return super.get("diff")
    }

    set diff (value: string | null) {
        this.set("diff", value)
    }
}
