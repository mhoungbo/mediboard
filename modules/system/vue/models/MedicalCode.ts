import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class MedicalCode extends OxObject {
    constructor () {
        super()
        this.type = "code"
    }

    get code (): OxAttr<string> {
        return super.get("code")
    }

    set code (value: OxAttr<string>) {
        super.set("code", value)
    }

    get description (): OxAttr<string> {
        return super.get("description")
    }

    set description (value: OxAttr<string>) {
        super.set("description", value)
    }

    get codeType (): OxAttr<string> {
        return super.get("type")
    }

    set codeType (value: OxAttr<string>) {
        super.set("type", value)
    }

    toString (): string {
        return `[${this.code}] ${this.description}`
    }
}
