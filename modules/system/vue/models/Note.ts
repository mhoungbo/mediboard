/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import { OxAttr } from "@/core/types/OxObjectTypes"
import OxMoment from "@/core/utils/OxMoment"

export default class Note extends OxObject {
    constructor () {
        super()
        this.type = "note"
    }

    protected _relationsTypes = {
        mediuser: Mediuser,
        [Note.RELATIONS_TYPE_OTHERS]: OxObject
    }

    get public (): OxAttr<boolean> {
        return super.get("public")
    }

    set public (value: OxAttr<boolean>) {
        this.set("public", value)
    }

    get degre (): "low" | "medium" | "high" {
        return super.get("degre")
    }

    set degre (value: OxAttr<string>) {
        this.set("degre", value)
    }

    get libelle (): OxAttr<string> {
        return super.get("libelle")
    }

    set libelle (value: OxAttr<string>) {
        this.set("libelle", value)
    }

    get text (): OxAttr<string> {
        return super.get("text")
    }

    set text (value: OxAttr<string>) {
        this.set("text", value)
    }

    get dateFromNow (): string {
        const moment = new OxMoment(this.dateData)
        return moment.fromNow(true)
    }

    get dateMoment (): OxMoment {
        return new OxMoment(this.dateData)
    }

    get dateData (): OxAttr<string> {
        // @TODO: Need to get the right backend date format declared in config (maybe requires a converter)
        return super.get("date")
    }

    set dateData (value: OxAttr<string>) {
        this.set("date", value)
    }

    get author (): Mediuser | undefined {
        return this.loadForwardRelation("author")
    }

    set author (value: Mediuser | undefined) {
        this.setForwardRelation("author", value)
    }

    get context (): OxObject | undefined {
        return this.loadForwardRelation("context")
    }

    set context (value: OxObject | undefined) {
        this.setForwardRelation("context", value)
    }

    get authorizedUsers (): Mediuser[] {
        return this.loadBackwardRelation("authorizedUsers")
    }

    public setAuthorizedUsers (users: Mediuser[]) {
        this.setBackwardRelation("authorizedUsers", users)
    }
}
