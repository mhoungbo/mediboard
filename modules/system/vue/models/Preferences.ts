/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"

export default class Preferences extends OxObject {
    constructor () {
        super()
        this.type = "preference"
    }

    get isDark (): boolean {
        return super.get("is_dark") === "1"
    }
}
