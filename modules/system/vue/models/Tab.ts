/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import { OxAttr, OxAttrNullable } from "@/core/types/OxObjectTypes"

export default class Tab extends OxObject {
    constructor () {
        super()
        this.type = "tab"
    }

    get name (): string {
        return super.get("tab_name")
    }

    set name (value: string) {
        super.set("tab_name", value)
    }

    get moduleName (): OxAttr<string> {
        return super.get("mod_name")
    }

    set moduleName (value: OxAttr<string>) {
        super.set("mod_name", value)
    }

    get url (): OxAttr<string> {
        return this.links.tab_url
    }

    set url (value: OxAttr<string>) {
        this.links.tab_url = value
    }

    get isStandard (): OxAttr<boolean> {
        return super.get("is_standard")
    }

    set isStandard (value: OxAttr<boolean>) {
        super.set("is_standard", value)
    }

    get isParam (): OxAttr<boolean> {
        return super.get("is_param")
    }

    set isParam (value: OxAttr<boolean>) {
        super.set("is_param", value)
    }

    get isConfig (): OxAttr<boolean> {
        return super.get("is_config")
    }

    set isConfig (value: OxAttr<boolean>) {
        super.set("is_config", value)
    }

    get pinnedOrder (): OxAttrNullable<number> {
        return super.get("pinned_order")
    }

    set pinnedOrder (value: OxAttrNullable<number>) {
        super.set("pinned_order", value)
    }
}
