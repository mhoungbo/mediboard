<!--
  @author  SAS OpenXtrem <dev@openxtrem.com>
  @license https://www.gnu.org/licenses/gpl.html GNU General Public License
  @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
-->

<script lang="ts">
import Vue, { PropType } from "vue"
import NoteSmallLine from "@modules/system/vue/components/NoteSmallLine/NoteSmallLine.vue"
import OxObject from "@/core/models/OxObject"
import OxCollection from "@/core/models/OxCollection"
import Note from "../../models/Note"
import { prepareForm } from "@/core/utils/OxSchemaManager"
import OxField from "@/core/components/OxField/OxField.vue"
import { addInfo } from "@/core/utils/OxNotifyManager"
import { createJsonApiObjects, getCollectionFromJsonApiRequest, updateJsonApiObject } from "@/core/utils/OxApiManager"
import { cloneObject } from "@/core/utils/OxObjectTools"
import OxDropdownButton from "@oxify/components/OxDropdownButton/OxDropdownButton.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"
import { OxDropdownButtonActionTypes } from "@oxify/types/OxDropdownButtonActionTypes"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"
import OxThemeCore from "@oxify/utils/OxThemeCore"
import OxAutocomplete from "@/core/components/OxAutocomplete/OxAutocomplete.vue"
import { OxUrlBuilder } from "@/core/utils/OxUrlTools"
import MediuserAvatar from "@modules/mediusers/vue/components/MediuserAvatar/MediuserAvatar.vue"
import OxMoment from "@/core/utils/OxMoment"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"
import OxForm from "@oxify/components/OxForm/OxForm.vue"
import { getSchema } from "@/core/utils/OxStorage"
import { OxSchema } from "@/core/types/OxSchema"
import { OxObjectLinks } from "@/core/types/OxObjectTypes"
import MediuserAutocomplete from "@modules/admin/vue/components/MediuserAutocomplete/MediuserAutocomplete.vue"

export default Vue.extend({
    name: "NoteDialog",
    components: {
        MediuserAutocomplete,
        MediuserAvatar,
        OxAutocomplete,
        NoteSmallLine,
        OxField,
        OxButton,
        OxDropdownButton,
        OxIcon
    },
    props: {
        object: {
            type: Object as PropType<OxObject>,
            required: true
        },
        user: {
            type: Object as PropType<Mediuser>,
            required: true
        },
        note: Object as PropType<Note>
    },
    data () {
        return {
            altActions: [
                {
                    label: this.$tr("CNote-action-Save and create"),
                    eventName: "saveandcreate"
                },
                {
                    label: this.$tr("CNote-action-Save and edit"),
                    eventName: "saveandedit"
                }
            ] as OxDropdownButtonActionTypes[],
            formReady: false,
            formValidated: false,
            mutatedNote: {} as Note,
            notesCollection: {} as OxCollection<Note>,
            visibilityMainColor: OxThemeCore.primary800,
            visibilityOtherColor: OxThemeCore.grey600,
            ownerRightColor: OxThemeCore.primary200,
            resourceName: "note",
            relatedNotes: [] as Note[],
            selectedNote: undefined as Note | undefined,
            url: "",
            showOwnerContent: false,
            showRelatedNotes: false,
            sharedUser: new Mediuser(),
            authorizedUsers: [] as Mediuser[],
            userAutocompleteUrl: new OxUrlBuilder(OxUrlDiscovery.listMediusers()).addParameter("type", "all").toString(),
            schemaLibelleDegre: "",
            schemaLibelleText: "",
            schemaTranslatedValueDegre: "",
            schemaDegre: {} as OxSchema,
            schemaText: {} as OxSchema,
            savedNoteId: "",
            savedLinks: {} as OxObjectLinks
        }
    },
    computed: {
        formTitle (): string {
            return this.selectedNote
                ? this.$tr("CNote-title-modify")
                : this.$tr("CNote-title-create")
        },
        hasEditPermissions (): boolean {
            return this.selectedNote?.meta?.permissions?.perm === "edit"
        },
        showForm (): boolean {
            return !this.selectedNote || this.hasEditPermissions
        },
        ownerRightIcon (): "chevronUp" | "chevronDown" {
            return this.showOwnerContent ? "chevronUp" : "chevronDown"
        },
        visibilityData (): {mainIcon: "accountGroup" | "lock", mainWording: string, otherIcon: "accountGroup" | "lock", otherWording: string } {
            return {
                mainIcon: this.mutatedNote.public ? "accountGroup" : "lock",
                mainWording: this.mutatedNote.public
                    ? this.$tr("system-msg-Public note")
                    : this.$tr("system-msg-Private note"),
                otherIcon: this.mutatedNote.public ? "lock" : "accountGroup",
                otherWording: this.mutatedNote.public
                    ? this.$tr("system-action-make private")
                    : this.$tr("system-action-make public")
            }
        },
        todayNotes (): { label: string, notes: Note[] } {
            return {
                label: this.$tr("Today"),
                notes: this.relatedNotes.filter(note => {
                    return note.dateMoment.isSame(Date.now(), "day")
                })
            }
        },
        yesterdayNotes (): { label: string, notes: Note[] } {
            return {
                label: this.$tr("Yesterday"),
                notes: this.relatedNotes.filter(note => {
                    const yesterday = new OxMoment(Date.now()).subtract(1, "day")
                    return note.dateMoment.isSame(yesterday, "day")
                })
            }
        },
        lastWeekNotes (): { label: string, notes: Note[] } {
            return {
                label: this.$tr("common-7 days before"),
                notes: this.relatedNotes.filter(note => {
                    const lastWeekDay = new OxMoment(Date.now()).subtract(7, "day")
                    return note.dateMoment.isSameOrAfter(lastWeekDay, "day") &&
                        !this.todayNotes.notes.includes(note) &&
                        !this.yesterdayNotes.notes.includes(note)
                })
            }
        },
        lastMonthNotes (): { label: string, notes: Note[] } {
            return {
                label: this.$tr("common-30 days before"),
                notes: this.relatedNotes.filter(note => {
                    const lastMonthDay = new OxMoment(Date.now()).subtract(30, "day")
                    return note.dateMoment.isSameOrAfter(lastMonthDay, "day") &&
                        !this.todayNotes.notes.includes(note) &&
                        !this.yesterdayNotes.notes.includes(note) &&
                        !this.lastWeekNotes.notes.includes(note)
                })
            }
        },
        sortedRelatedNotes (): { label: string, notes: Note[] }[] {
            let sortedNotes = [] as { label: string, notes: Note[] }[]

            if (this.todayNotes.notes.length > 0) {
                sortedNotes.push(this.todayNotes)
            }

            if (this.yesterdayNotes.notes.length > 0) {
                sortedNotes.push(this.yesterdayNotes)
            }

            if (this.lastWeekNotes.notes.length > 0) {
                sortedNotes.push(this.lastWeekNotes)
            }

            if (this.lastMonthNotes.notes.length > 0) {
                sortedNotes.push(this.lastMonthNotes)
            }

            const byYearNotes = this.relatedNotes.filter(note => {
                return !this.todayNotes.notes.includes(note) &&
                    !this.yesterdayNotes.notes.includes(note) &&
                    !this.lastWeekNotes.notes.includes(note) &&
                    !this.lastMonthNotes.notes.includes(note)
            })

            const sort = {} as { [year: number]: { label: string, notes: Note[] } }

            byYearNotes.forEach(note => {
                const year = new OxMoment(note.dateData).format("YYYY")
                if (!sort[year]) {
                    sort[year] = { label: year, notes: [note] }
                }
                else {
                    sort[year].notes.push(note)
                }
            })

            sortedNotes = [...sortedNotes, ...Object.values(sort)]

            return sortedNotes
        },
        isRelatedNotesEmpty (): boolean {
            return this.relatedNotes.length <= 0 && this.showRelatedNotes
        },
        hasAuthorizedUsers (): boolean {
            return this.authorizedUsers.length > 0
        }
    },
    async created () {
        this.selectedNote = this.note
        this.mutatedNote = this.note ? cloneObject(this.note) : new Note()
        this.initDataMutatedNote()

        this.formReady = await prepareForm(this.resourceName, ["default"])

        this.schemaDegre = getSchema(this.resourceName, "degre")
        this.schemaText = getSchema(this.resourceName, "text")

        this.schemaLibelleDegre = this.schemaDegre.libelle
        this.schemaLibelleText = this.schemaText.libelle
        if (this.schemaDegre.translations) {
            this.schemaTranslatedValueDegre = this.schemaDegre.translations[this.selectedNote?.degre]
        }

        this.url = new OxUrlBuilder(this.object.links.notes)
            .withRelations(["author", "authorizedUsers"]) // No need "context" relation cause it's the object himself
            .withSort({ sort: "DESC", choice: "date" })
            .withPermissions()
            .toString()
        this.notesCollection = await getCollectionFromJsonApiRequest(Note, this.url)
        this.relatedNotes = this.notesCollection.objects
        this.showRelatedNotes = true
    },
    methods: {
        initDataMutatedNote (): void {
            this.mutatedNote.context = this.object
            this.mutatedNote.dateData = this.selectedNote ? this.selectedNote.dateData : "now"
            this.mutatedNote.public = this.selectedNote ? this.selectedNote.public : true
            this.authorizedUsers = this.mutatedNote.authorizedUsers
            this.mutatedNote.meta = {
                permissions: {
                    perm: "edit"
                }
            }
        },
        async save (keepOpened = false): Promise<void> {
            const validate = (this.$refs.form as InstanceType<typeof OxForm>).validate()

            if (!validate) {
                return
            }

            let savedNote

            try {
                this.mutatedNote.setAuthorizedUsers(this.authorizedUsers)

                if (this.selectedNote) {
                    savedNote = await updateJsonApiObject(this.selectedNote, this.mutatedNote)
                    addInfo(this.$tr("CNote-msg-modify"))
                }
                else {
                    savedNote = await createJsonApiObjects(this.mutatedNote, OxUrlDiscovery.notes())
                    addInfo(this.$tr("CNote-msg-create"))
                }

                this.savedNoteId = savedNote.id
                this.savedLinks = savedNote.links

                this.$emit("dataSaved", keepOpened)
            }
            catch (e) {
                console.error(e)
            }

            // Requires to do a new request cause POST return value doesn't contains relationships
            this.notesCollection = await getCollectionFromJsonApiRequest(Note, this.url)
            this.relatedNotes = this.notesCollection.objects
        },
        async saveAndCreate (): Promise<void> {
            await this.save(true)
            this.selectedNote = undefined
            this.mutatedNote = new Note()
            this.initDataMutatedNote()
        },
        async saveAndEdit (): Promise<void> {
            await this.save(true)

            if (this.savedNoteId) {
                this.mutatedNote.id = this.savedNoteId
                this.mutatedNote.links = this.savedLinks
                this.selectedNote = cloneObject(this.mutatedNote)
            }
        },
        deleteNote (note: Note): void {
            this.$emit("deleteNote", note)
        },
        resetNote (): void {
            this.selectedNote = undefined
            this.mutatedNote = new Note()
            this.initDataMutatedNote()
        },
        updateCurrentNote (note: Note): void {
            this.selectedNote = note
            if (this.schemaDegre.translations) {
                this.schemaTranslatedValueDegre = this.schemaDegre.translations[this.selectedNote.degre]
            }
            this.mutatedNote = cloneObject(note)
            this.authorizedUsers = this.mutatedNote.authorizedUsers
        },
        isSelected (note: Note): { selected: boolean } {
            return { selected: note.id === this.selectedNote?.id }
        },
        toggleOwner (): void {
            this.showOwnerContent = !this.showOwnerContent
        },
        toggleVisibility (): void {
            this.mutatedNote.public = !this.mutatedNote.public
        },
        onClickOutside (): void {
            this.showOwnerContent = false
        },
        addAuthorizedUser (user: Mediuser): void {
            this.sharedUser = {} as Mediuser

            const sharedUserAlreadyexists = this.authorizedUsers.findIndex((_user) => {
                return _user.id === user.id
            })

            if (sharedUserAlreadyexists > -1) {
                addInfo(this.$tr("system-msg-User already in the share"))
                return
            }

            this.authorizedUsers.push(user)
        },
        removeAuthorizedUser (user: Mediuser): void {
            this.authorizedUsers = this.authorizedUsers.filter((_user) => {
                return _user.id !== user.id
            })
        },
        onCtrlEnter (): void {
            this.save()
        }
    }
})
</script>

<template>
  <div class="NoteDialog">
    <div class="NoteDialog-dialog">
      <aside class="NoteDialog-aside">
        <div class="NoteDialog-addBtn">
          <ox-button
            block
            button-style="secondary"
            icon="add"
            :label="$tr('CNote-title-create')"
            @click="resetNote"
          />
        </div>
        <div
          v-if="showRelatedNotes"
          class="NoteDialog-relatedNotes"
        >
          <div
            v-for="(notes, index) in sortedRelatedNotes"
            :key="index"
            class="NoteDialog-relatedNotesSection"
          >
            <div class="NoteDialog-creationDate">
              {{ notes.label }}
            </div>
            <note-small-line
              v-for="relatedNote in notes.notes"
              :key="relatedNote.id"
              class="NoteDialog-noteLine"
              :class="isSelected(relatedNote)"
              :note="relatedNote"
              @click.native="updateCurrentNote(relatedNote)"
            />
          </div>
        </div>
        <div v-else>
          <v-skeleton-loader type="list-item-avatar-two-line@5" />
        </div>
        <div
          v-if="isRelatedNotesEmpty"
          class="NoteDialog-empty"
        >
          {{ $tr("CMbObject-back-notes.empty") }}
        </div>
      </aside>
      <section class="NoteDialog-section">
        <div class="NoteDialog-sectionHeader">
          <div class="NoteDialog-sectionHeaderObject">
            <div class="NoteDialog-sectionHeaderObjectTitle">
              {{ object.type }}
            </div>
            <div class="NoteDialog-sectionHeaderObjectId">
              #{{ object.id }}
            </div>
          </div>
          <div
            v-if="showForm"
            class="NoteDialog-title"
          >
            <div class="NoteDialog-sectionHeaderTitle">
              {{ formTitle }}
            </div>
            <div
              v-click-outside="onClickOutside"
              class="NoteDialog-owner"
              :class="{ opened: showOwnerContent }"
            >
              <div
                class="NoteDialog-ownerHeader"
                @click="toggleOwner"
              >
                <div class="NoteDialog-visibility">
                  <div class="NoteDialog-visibilityMain">
                    <ox-icon
                      :color="visibilityMainColor"
                      :icon="visibilityData.mainIcon"
                      :size="20"
                    />
                    <div class="NoteDialog-visibilityMainWording">
                      {{ visibilityData.mainWording }}
                    </div>
                  </div>
                  <ox-button
                    v-if="showOwnerContent"
                    button-style="tertiary-dark"
                    :icon="visibilityData.otherIcon"
                    :label="visibilityData.otherWording"
                    @click.stop="toggleVisibility"
                  />
                </div>
                <div class="NoteDialog-ownerRight">
                  <ox-icon
                    :color="ownerRightColor"
                    :icon="ownerRightIcon"
                    :size="20"
                  />
                </div>
              </div>
              <div v-if="showOwnerContent">
                <div
                  v-if="!mutatedNote.public"
                  class="NoteDialog-relatedUserAutocomplete"
                >
                  <ox-autocomplete
                    item-text="fullName"
                    :max-result="-1"
                    :min-char-search="2"
                    :placeholder="$tr('system-action-Add users')"
                    search-field="name"
                    search-method="parameter"
                    :url="userAutocompleteUrl"
                    :value="sharedUser"
                    @input="addAuthorizedUser"
                  >
                    <template #item="{ item }">
                      <mediuser-autocomplete :user="item" />
                    </template>
                  </ox-autocomplete>
                </div>
                <div class="NoteDialog-ownerData">
                  <div
                    v-if="hasAuthorizedUsers"
                    class="NoteDialog-ownerDataSection"
                  >
                    <div class="NoteDialog-ownerDataLabel">
                      {{ $tr('system-msg-Note shared with') }}
                    </div>
                    <div
                      v-for="(authorizedUser, index) in authorizedUsers"
                      :key="index"
                      class="NoteDialog-ownerDataName"
                    >
                      <mediuser-avatar
                        :size="32"
                        :user="authorizedUser"
                      />
                      <div class="NoteDialog-user">
                        {{ authorizedUser.fullName }}
                      </div>
                      <ox-button
                        button-style="tertiary-dark"
                        icon="cancel"
                        @click="removeAuthorizedUser(authorizedUser)"
                      />
                    </div>
                  </div>
                  <div class="NoteDialog-ownerDataSection">
                    <div class="NoteDialog-ownerDataLabel">
                      {{ $tr('Owner') }}
                    </div>
                    <div class="NoteDialog-ownerDataName">
                      <mediuser-avatar
                        :size="32"
                        :user="user"
                      />
                      <div class="NoteDialog-user">
                        {{ user.fullName }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <v-form
          v-if="showForm"
          ref="form"
          v-model="formValidated"
          class="NoteDialog-sectionForm"
        >
          <div class="NoteDialog-sectionFormFields">
            <ox-field
              v-model="mutatedNote.degre"
              field-name="degre"
              :options="{expand: true, row: true}"
              :ready="formReady"
              :resource-name="resourceName"
            />
            <ox-field
              v-model="mutatedNote.text"
              autofocus
              field-name="text"
              :ready="formReady"
              :resource-name="resourceName"
              @keydown.meta.enter="onCtrlEnter"
              @keyup.ctrl.enter="onCtrlEnter"
            />
          </div>
          <div class="NoteDialog-sectionFormButtons">
            <ox-dropdown-button
              :alt-actions="altActions"
              button-style="primary"
              :label="$tr('CNote-action-Save and close')"
              split
              :title="$tr('CNote-action-Save and close')"
              @click="save"
              @saveandcreate="saveAndCreate"
              @saveandedit="saveAndEdit"
            />
            <ox-button
              v-if="selectedNote"
              button-style="tertiary"
              icon="delete"
              :label="$tr('Delete')"
              :title="$tr('Delete')"
              @click="deleteNote(selectedNote)"
            />
          </div>
        </v-form>
        <div v-else-if="formReady">
          <div class="NoteDialog-fieldData">
            <div>
              <div class="NoteDialog-fieldDataLabel">
                {{ schemaLibelleDegre }}
              </div>
              <div class="NoteDialog-fieldDataValue capitalized">
                {{ schemaTranslatedValueDegre }}
              </div>
            </div>
            <div>
              <div class="NoteDialog-fieldDataLabel">
                {{ schemaLibelleText }}
              </div>
              <div class="NoteDialog-fieldDataValue">
                {{ selectedNote.text }}
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</template>

<style lang="scss" src="./NoteDialog.scss" />
