import { createEntryPoint } from "@/core/utils/OxEntryPoint"
import ErrorPage from "@modules/system/vue/views/ErrorPage/ErrorPage.vue"

createEntryPoint("ErrorPage", ErrorPage)
