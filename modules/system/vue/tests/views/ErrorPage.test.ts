/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxTest from "@oxify/utils/OxTest"
import ErrorPage from "@modules/system/vue/views/ErrorPage/ErrorPage.vue"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

export default class ErrorPageTest extends OxTest {
    protected component = ErrorPage

    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    public testCode404IsNotError () {
        const page = this.mountComponent({ statusCode: "404", action: "Back" })
        expect(page.vm["isError"]).toEqual(false)
    }

    public testNoStatusCodeIsNotError () {
        const page = this.mountComponent({ action: "Back" })
        expect(page.vm["isError"]).toEqual(false)
    }

    public testCode500IsError () {
        const page = this.mountComponent({ statusCode: "500", action: "Back" })
        expect(page.vm["isError"]).toEqual(true)
    }

    public testDefaultDetailsClass () {
        const page = this.mountComponent({ action: "Back" })
        expect(page.vm["detailsClass"]).toEqual({ "ErrorPage-detail": true, displayed: false })
    }

    public testOpenDetailsClass () {
        const page = this.mountComponent({ action: "Back" })
        page.vm["toggleDetail"]()
        expect(page.vm["detailsClass"]).toEqual({ "ErrorPage-detail": true, displayed: true })
    }

    public testClosedDetailClass () {
        const page = this.mountComponent({ action: "Back" })
        page.vm["toggleDetail"]()
        page.vm["clickOutside"]()
        expect(page.vm["detailsClass"]).toEqual({ "ErrorPage-detail": true, displayed: false })
    }

    public testGoToHomeIfActionLink () {
        const hrefSpy = jest.fn()
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            set: hrefSpy,
            get: () => "baseHref"
        })
        const page = this.mountComponent({ action: "Back", links: { actionLink: "actionLink" } })
        page.vm["goToHome"]()
        expect(hrefSpy).toHaveBeenCalledWith("actionLink")
    }

    public testGoToHomeIfNoActionLink () {
        // @ts-ignore
        delete window.location
        // @ts-ignore
        Object.defineProperty(window, "location", {
            configurable: true,
            value: { reload: jest.fn() }
        })
        const page = this.mountComponent({ action: "Back" })
        page.vm["goToHome"]()
        expect(window.location.reload).toHaveBeenCalled()
    }
}

(new ErrorPageTest()).launchTests()
