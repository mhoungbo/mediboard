export const historyMock = {
    data: [
        {
            type: "history",
            id: "1",
            attributes: {
                date: "2002-01-01 15:00:00",
                type: "store",
                fields: "field1 field2"
            },
            relationships: {
                user: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                },
                updatedFields: {
                    data: [
                        {
                            type: "field_diff",
                            id: "1"
                        },
                        {
                            type: "field_diff",
                            id: "2"
                        }
                    ]
                }
            },
            links: {
                self: "self",
                schema: "schema"
            }
        },
        {
            type: "history",
            id: "2",
            attributes: {
                date: "2002-01-01 16:00:00",
                type: "store",
                fields: "field1"
            },
            relationships: {
                user: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                },
                updatedFields: {
                    data: [
                        {
                            type: "field_diff",
                            id: "3"
                        }
                    ]
                }
            },
            links: {
                self: "self",
                schema: "schema"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2
    },
    links: {
        self: "self",
        first: "first",
        last: "last"
    },
    included: [
        {
            type: "mediuser",
            id: "1",
            attributes: {
                _user_first_name: "John",
                _user_last_name: "Doe",
                _user_sexe: "u"
            }
        },
        {
            type: "field_diff",
            id: "1",
            attributes: {
                field_name: "field1",
                old_value: "oldValue1",
                new_value: "newValue1",
                diff: ""
            }
        },
        {
            type: "field_diff",
            id: "2",
            attributes: {
                field_name: "field2",
                old_value: "oldValue2",
                new_value: "newValue2",
                diff: ""
            }
        },
        {
            type: "field_diff",
            id: "3",
            attributes: {
                field_name: "field1",
                old_value: "oldValue3",
                new_value: "newValue3",
                diff: ""
            }
        }
    ]
}

export const historyEmptyMock = {
    data: [],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 0,
        total: 0
    },
    links: {
        self: "self",
        first: "first",
        last: "last"
    }
}
