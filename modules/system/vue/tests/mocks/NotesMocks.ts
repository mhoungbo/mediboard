export const notesSchemaMock = {
    data: [
        {
            type: "schema",
            id: "f6f0da48e2ac88878a4c4f6569945d16",
            attributes: {
                owner: "note",
                field: "public",
                type: "bool",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: "1",
                helped: null,
                libelle: "Note publique",
                label: "Note pub.",
                description: "Note visible par tout le monde"
            }
        },
        {
            type: "schema",
            id: "84d8d30eb95ac16f6de854d6b367cc78",
            attributes: {
                owner: "note",
                field: "degre",
                type: "enum",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: "low",
                helped: null,
                values: [
                    "low",
                    "medium",
                    "high"
                ],
                translations: {
                    low: "faible",
                    medium: "moyenne",
                    high: "haute"
                },
                libelle: "Importance",
                label: "Importance",
                description: "Degr\u00e9 d\u0027importance de la note"
            }
        },
        {
            type: "schema",
            id: "e6398b34e40c5c406b3065110340f7db",
            attributes: {
                owner: "note",
                field: "date",
                type: "dateTime",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                helped: null,
                hide_date: null,
                libelle: "Date",
                label: "Date",
                description: "Date de cr\u00e9ation de la note"
            }
        },
        {
            type: "schema",
            id: "36b338ff89791ae378877e33791493b6",
            attributes: {
                owner: "note",
                field: "libelle",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                helped: null,
                length: null,
                minLength: null,
                maxLength: null,
                libelle: "Libell\u00e9",
                label: "Libell\u00e9",
                description: "Libell\u00e9 de la note"
            }
        },
        {
            type: "schema",
            id: "72357e66b694eaf8eb355a38b07ea97e",
            attributes: {
                owner: "note",
                field: "text",
                type: "text",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                helped: null,
                markdown: null,
                libelle: "Contenu",
                label: "Contenu",
                description: "Contenu de la note"
            }
        }
    ],
    meta: {
        date: "2023-05-15 09:52:47+02:00",
        copyright: "OpenXtrem-2023",
        authors: "dev@openxtrem.com",
        count: 5
    }
}

export const notesMock = {
    data: [
        {
            type: "note",
            id: "1",
            attributes: {
                public: false,
                degre: "low",
                date: "2023-02-02 10:00:00",
                libelle: "libelle",
                text: "text"
            },
            relationships: {
                author: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                identifiers: "identifiers",
                notes: "notes"
            }
        },
        {
            type: "note",
            id: "2",
            attributes: {
                public: false,
                degre: "low",
                date: "2023-02-08 10:00:00",
                libelle: "libelle",
                text: "text"
            },
            relationships: {
                author: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                identifiers: "identifiers",
                notes: "notes"
            }
        },
        {
            type: "note",
            id: "3",
            attributes: {
                public: false,
                degre: "medium",
                date: "2023-03-04 10:00:00",
                libelle: "libelle",
                text: "text"
            },
            relationships: {
                author: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                identifiers: "identifiers",
                notes: "notes"
            }
        },
        {
            type: "note",
            id: "4",
            attributes: {
                public: false,
                degre: "medium",
                date: "2023-03-05 10:00:00",
                libelle: "libelle",
                text: "text"
            },
            relationships: {
                author: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                identifiers: "identifiers",
                notes: "notes"
            }
        },
        {
            type: "note",
            id: "5",
            attributes: {
                public: false,
                degre: "high",
                date: "2023-03-06 10:00:00",
                libelle: "libelle",
                text: "text"
            },
            relationships: {
                author: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                identifiers: "identifiers",
                notes: "notes"
            }
        },
        {
            type: "note",
            id: "6",
            attributes: {
                public: false,
                degre: "high",
                date: "2023-03-07 10:00:00",
                libelle: "libelle",
                text: "text"
            },
            relationships: {
                author: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                }
            },
            links: {
                self: "self",
                schema: "schema",
                history: "history",
                identifiers: "identifiers",
                notes: "notes"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2
    },
    links: {
        self: "self",
        first: "first",
        last: "last"
    },
    included: [
        {
            type: "mediuser",
            id: "1",
            attributes: {
                _color: "3d85c6",
                _user_first_name: "John",
                _user_last_name: "Doe",
                _user_sexe: "u"
            }
        }
    ]
}

export const notesEmptyMock = {
    data: [],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 0,
        total: 0
    },
    links: {
        self: "self",
        first: "first",
        last: "last"
    }
}

export const notesCreationMock = {
    data: [
        {
            type: "note",
            id: "1",
            attributes: {
                public: false,
                degre: "low",
                date: "2023-05-12 12:10:24",
                libelle: "libelle",
                text: "bonjour"
            },
            links: {
                self: "self",
                schema: "schema"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 1
    }
}
