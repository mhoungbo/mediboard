/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import OxObject from "@/core/models/OxObject"
import History from "@modules/system/vue/models/History"
import OxCollection from "@/core/models/OxCollection"
import HistoryFieldDiff from "@modules/system/vue/models/HistoryFieldDiff"
import pinia from "@/core/plugins/OxPiniaCore"
import { setActivePinia } from "pinia"
import oxApiService from "@/core/utils/OxApiService"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import HistoryDetails from "@modules/system/vue/components/HistoryDetails/HistoryDetails.vue"
import { historyEmptyMock, historyMock } from "@modules/system/vue/tests/mocks/HistoryMocks"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * Test for HistoryDetails
 */
export default class HistoryDetailsTest extends OxTest {
    protected component = HistoryDetails

    private object = new OxObject()
    private collection = new OxCollection<History>()
    private historyPanels = [] as History[]
    private history1 = new History()
    private history2 = new History()
    private history3 = new History()
    private historyField1 = new HistoryFieldDiff()
    private historyField2 = new HistoryFieldDiff()
    private user1 = new Mediuser()
    private user2 = new Mediuser()

    protected beforeAllTests () {
        super.beforeAllTests()

        setActivePinia(pinia)

        this.object.links = {
            history: "history"
        }

        this.historyField1.id = "1"
        this.historyField1.name = "field1"
        this.historyField1.oldValue = "oldValue1"
        this.historyField1.newValue = "newValue1"

        this.historyField2.id = "2"
        this.historyField2.name = "field2"
        this.historyField2.oldValue = "oldValue2"
        this.historyField2.newValue = "newValue2"

        this.user1.id = "1"
        this.user1.lastName = "Doe"
        this.user1.firstName = "John"

        this.user2.id = "2"
        this.user2.lastName = "Williams"
        this.user2.firstName = "Bob"

        this.history1.setUpdatedFields([this.historyField1, this.historyField2])
        this.history2.setUpdatedFields([this.historyField1])

        this.history1.id = "1"
        this.history1.user = this.user1
        this.history1.historyType = "store"
        this.history2.id = "2"
        this.history2.user = this.user2
        this.history2.historyType = "store"
        this.history3.id = "3"
        this.history3.user = this.user2
        this.history3.historyType = "create"

        this.collection.objects = [
            this.history1,
            this.history2
        ] as unknown as History[]

        this.historyPanels = [
            this.history1,
            this.history2,
            this.history3
        ]
    }

    protected beforeTest () {
        super.beforeTest()

        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: historyMock }))
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected async mountComponentAsync (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )

        await this.flushPromises()

        return wrapper
    }

    protected async vueComponentAsync (
        props: object = {
            object: this.object
        },
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = await this.mountComponentAsync(props, stubs, slots)

        return wrapper.vm
    }

    public async testBtnIconExpand () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["closeAllPanels"] = true
        expect(this.privateCall(historyDetails, "toggleBtnIcon")).toBe("expand")
    }

    public async testBtnIconCollapse () {
        const historyDetails = await this.vueComponentAsync()
        expect(this.privateCall(historyDetails, "toggleBtnIcon")).toBe("collapse")
    }

    public async testLabelBtnUnfoldAll () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["closeAllPanels"] = true
        expect(this.privateCall(historyDetails, "labelToggleBtn")).toBe("common-action-Unfold all")
    }

    public async testLabelBtnCollapseAll () {
        const historyDetails = await this.vueComponentAsync()
        expect(this.privateCall(historyDetails, "labelToggleBtn")).toBe("common-action-Collapse all")
    }

    public async testHasCurrentFilterCauseField () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["currentFilterField"] = "field2"
        expect(this.privateCall(historyDetails, "hasCurrentFilter")).toBeTruthy()
    }

    public async testHasCurrentFilterCauseUser () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["currentFilterUser"] = "Doe John"
        expect(this.privateCall(historyDetails, "hasCurrentFilter")).toBeTruthy()
    }

    public async testHasNotCurrentFilter () {
        const historyDetails = await this.vueComponentAsync()
        expect(this.privateCall(historyDetails, "hasCurrentFilter")).toBeFalsy()
    }

    public async testEmptyPanelsCauseNoData () {
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: historyEmptyMock }))

        const historyDetails = await this.vueComponentAsync()
        expect(this.privateCall(historyDetails, "panels")).toMatchObject([])
    }

    public async testEmptyPanelsCauseCloseAllPanels () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["closeAllPanels"] = true
        expect(this.privateCall(historyDetails, "panels")).toMatchObject([])
    }

    public async testPanels () {
        const historyDetails = await this.vueComponentAsync()
        expect(this.privateCall(historyDetails, "panels")).toMatchObject([0, 1])
    }

    public async testHistoryPanelsWithoutFilter () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["historyCollection"] = this.collection
        expect(
            this.privateCall(
                historyDetails,
                "historyPanels"
            )
        ).toMatchObject([this.history1, this.history2])
    }

    public async testHistoryPanelsWithFieldFilter () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["historyCollection"] = this.collection
        historyDetails["currentFilterField"] = "field2"
        expect(
            this.privateCall(
                historyDetails,
                "historyPanels"
            )
        ).toMatchObject([this.history1])
    }

    public async testFilterFieldValues () {
        const historyDetails = await this.vueComponentAsync()
        expect(this.privateCall(historyDetails, "filterFieldValues")).toMatchObject(
            [
                {
                    type: "field_diff",
                    id: "1",
                    attributes: {
                        field_name: "field1",
                        old_value: "oldValue1",
                        new_value: "newValue1",
                        diff: ""
                    }
                },
                {
                    type: "field_diff",
                    id: "2",
                    attributes: {
                        field_name: "field2",
                        old_value: "oldValue2",
                        new_value: "newValue2",
                        diff: ""
                    }
                },
                {
                    type: "field_diff",
                    id: "3",
                    attributes: {
                        field_name: "field1",
                        old_value: "oldValue3",
                        new_value: "newValue3",
                        diff: ""
                    }
                }
            ]
        )
    }

    public async testFilterUserValues () {
        const historyDetails = await this.vueComponentAsync()
        expect(this.privateCall(historyDetails, "filterUserValues")).toMatchObject(["Doe John"])
    }

    public async testCreated () {
        await this.vueComponentAsync()

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "history?limit=5&relations=user%2CupdatedFields"
            )
        )
    }

    public async testFilterUser () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["historyCollection"] = this.collection
        this.privateCall(historyDetails, "filterUser", "Doe John")
        expect(historyDetails["currentFilterUser"]).toBe("Doe John")
    }

    public async testFilterField () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["historyCollection"] = this.collection
        this.privateCall(historyDetails, "filterField", "field2")
        expect(historyDetails["currentFilterField"]).toBe("field2")
    }

    public async testApplyFilterUser () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["currentFilterUser"] = "Doe John"
        expect(this.privateCall(
            historyDetails,
            "applyFilterUser",
            [this.history1, this.history2]
        )).toMatchObject([this.history1])
    }

    public async testApplyFilterField () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["currentFilterField"] = "field2"
        expect(this.privateCall(
            historyDetails,
            "applyFilterField",
            [this.history1, this.history2]
        )).toMatchObject([this.history1])
    }

    public async testApplyFiltersWithUserFilter () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["currentFilterUser"] = "Doe John"
        expect(this.privateCall(
            historyDetails,
            "applyFilters",
            [this.history1, this.history2]
        )).toMatchObject([this.history1])
    }

    public async testApplyFiltersWithFieldFilter () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["currentFilterField"] = "field2"
        expect(this.privateCall(
            historyDetails,
            "applyFilters",
            [this.history1, this.history2]
        )).toMatchObject([this.history1])
    }

    public async testApplyFiltersWithAllFilters () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["currentFilterUser"] = "Williams Bob"
        historyDetails["currentFilterField"] = "field2"
        expect(this.privateCall(
            historyDetails,
            "applyFilters",
            [this.history1, this.history2]
        )).toMatchObject([])
    }

    public async testEnableTargetField () {
        const historyDetails = await this.vueComponentAsync()
        this.privateCall(historyDetails, "toggleTargetField", "field2")
        expect(historyDetails["targetedField"]).toBe("field2")
    }

    public async testDisableTargetField () {
        const historyDetails = await this.vueComponentAsync()
        historyDetails["targetedField"] = "field2"
        this.privateCall(historyDetails, "toggleTargetField", "field2")
        expect(historyDetails["targetedField"]).toBe("")
    }
}

(new HistoryDetailsTest()).launchTests()
