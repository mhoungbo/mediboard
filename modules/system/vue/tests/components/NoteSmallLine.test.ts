/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import NoteSmallLine from "@modules/system/vue/components/NoteSmallLine/NoteSmallLine.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import Note from "@modules/system/vue/models/Note"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * Test for NoteSmallLine
 */
export default class NoteSmallLineTest extends OxTest {
    protected component = NoteSmallLine

    private user = new Mediuser()
    private noteLow = new Note()
    private noteMedium = new Note()
    private noteHigh = new Note()

    protected beforeAllTests () {
        super.beforeAllTests()

        setActivePinia(pinia)

        this.user.id = "1"
        this.user.firstName = "John"
        this.user.lastName = "DOE"
        this.noteLow.id = "1"
        this.noteLow.degre = "low"
        this.noteLow.author = this.user
        this.noteMedium.id = "2"
        this.noteMedium.degre = "medium"
        this.noteMedium.author = this.user
        this.noteHigh.id = "3"
        this.noteHigh.degre = "high"
        this.noteHigh.author = this.user
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testClassesIsNotHigh () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteLow })
        expect(this.privateCall(noteSmallLineComponent, "classes")).toMatchObject(
            {
                NoteSmallLine: true,
                high: false
            }
        )
    }

    public testClassesIsHigh () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteHigh })
        expect(this.privateCall(noteSmallLineComponent, "classes")).toMatchObject(
            {
                NoteSmallLine: true,
                high: true
            }
        )
    }

    public testIsHigh () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteHigh })
        expect(this.privateCall(noteSmallLineComponent, "isHigh")).toBe(true)
    }

    public testIsNotHigh () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteLow })
        expect(this.privateCall(noteSmallLineComponent, "isHigh")).toBe(false)
    }

    public testIsMedium () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteMedium })
        expect(this.privateCall(noteSmallLineComponent, "isMedium")).toBe(true)
    }

    public testIsNotMedium () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteLow })
        expect(this.privateCall(noteSmallLineComponent, "isMedium")).toBe(false)
    }

    public testHighIsImportant () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteHigh })
        expect(this.privateCall(noteSmallLineComponent, "isImportant")).toBe(true)
    }

    public testMediumIsImportant () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteMedium })
        expect(this.privateCall(noteSmallLineComponent, "isImportant")).toBe(true)
    }

    public testLowIsNotImportant () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteLow })
        expect(this.privateCall(noteSmallLineComponent, "isImportant")).toBe(false)
    }

    public testCriticalityColorWarning () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteMedium })
        expect(this.privateCall(noteSmallLineComponent, "criticalityColor")).toBe("warning")
    }

    public testCriticalityColorError () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteHigh })
        expect(this.privateCall(noteSmallLineComponent, "criticalityColor")).toBe("error")
    }

    public testCriticalityTextImportant () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteMedium })
        expect(this.privateCall(noteSmallLineComponent, "criticalityText")).toBe("CNote.criticality.important")
    }

    public testCriticalityTextMajor () {
        const noteSmallLineComponent = this.vueComponent({ note: this.noteHigh })
        expect(this.privateCall(noteSmallLineComponent, "criticalityText")).toBe("CNote.criticality.high")
    }

    public testDeleteNote () {
        const noteSmallLineComponent = this.mountComponent({ note: this.noteHigh })
        this.privateCall(noteSmallLineComponent.vm, "deleteNote")
        const events = noteSmallLineComponent.emitted("deleteNote")
        this.assertHaveLength(events, 1)
    }

    public testEditNote () {
        const noteSmallLineComponent = this.mountComponent({ note: this.noteHigh })
        this.privateCall(noteSmallLineComponent.vm, "editNote")
        const events = noteSmallLineComponent.emitted("editNote")
        this.assertHaveLength(events, 1)
    }
}

(new NoteSmallLineTest()).launchTests()
