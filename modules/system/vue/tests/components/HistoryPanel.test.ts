/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import HistoryPanel from "@modules/system/vue/components/HistoryPanel/HistoryPanel.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import History from "@modules/system/vue/models/History"
import HistoryFieldDiff from "@modules/system/vue/models/HistoryFieldDiff"
import pinia from "@/core/plugins/OxPiniaCore"
import { setActivePinia } from "pinia"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import OxObject from "@/core/models/OxObject"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * Test for HistoryPanel
 */
export default class HistoryPanelTest extends OxTest {
    protected component = HistoryPanel

    private object = new OxObject()
    private history1 = new History()
    private history2 = new History()
    private history3 = new History()
    private historyField1 = new HistoryFieldDiff()
    private historyField2 = new HistoryFieldDiff()
    private user = new Mediuser()

    protected beforeAllTests () {
        super.beforeAllTests()

        setActivePinia(pinia)

        this.object.id = "1"

        this.history1.historyType = "store"
        this.history2.historyType = "create"
        this.history3.historyType = "merge"

        this.historyField1.id = "1"
        this.historyField1.name = "field1"
        this.historyField1.oldValue = "oldValue1"
        this.historyField1.newValue = "newValue1"

        this.historyField2.id = "2"
        this.historyField2.name = "field2"
        this.historyField2.oldValue = "oldValue2"
        this.historyField2.newValue = "newValue2"

        this.user.id = "1"
        this.user.lastName = "Doe"
        this.user.firstName = "John"

        this.history1.user = this.user
        this.history2.user = this.user
        this.history3.user = this.user

        this.history1.setUpdatedFields([this.historyField1, this.historyField2])
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testIsReadonly () {
        const histPanelComponent = this.vueComponent({ history: this.history2, object: this.object })
        expect(this.privateCall(histPanelComponent, "isReadonly")).toBeTruthy()
    }

    public testIsNotReadonly () {
        const histPanelComponent = this.vueComponent({ history: this.history1, object: this.object })
        expect(this.privateCall(histPanelComponent, "isReadonly")).toBeFalsy()
    }

    public testShowSidesLeft () {
        const histPanelComponent = this.vueComponent({ history: this.history2, object: this.object })
        expect(this.privateCall(histPanelComponent, "showSides")).toBe("left")
    }

    public testShowSidesBoth () {
        const histPanelComponent = this.vueComponent({ history: this.history1, object: this.object })
        expect(this.privateCall(histPanelComponent, "showSides")).toBe("both")
    }

    public testHistoryFieldsWithoutRestriction () {
        const histPanelComponent = this.vueComponent({ history: this.history1, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "historyFields"), this.history1.fields)
    }

    public testHistoryFieldsWithRestriction () {
        const histPanelComponent = this.vueComponent({
            history: this.history1,
            object: this.object,
            restrictedField: this.historyField1.name
        })
        this.assertEqual(this.privateCall(histPanelComponent, "historyFields"), [this.historyField1])
    }

    public testTypeLogoEdit () {
        const histPanelComponent = this.vueComponent({ history: this.history1, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "typeLogo"), "edit")
    }

    public testTypeLogoCreate () {
        const histPanelComponent = this.vueComponent({ history: this.history2, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "typeLogo"), "add")
    }

    public testTypeLogoMerge () {
        const histPanelComponent = this.vueComponent({ history: this.history3, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "typeLogo"), "call")
    }

    public testActionTextStore () {
        const histPanelComponent = this.vueComponent({ history: this.history1, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "actionText"), "common-msg-has edited")
    }

    public testActionTextCreate () {
        const histPanelComponent = this.vueComponent({ history: this.history2, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "actionText"), "common-msg-has created")
    }

    public testActionTextMerge () {
        const histPanelComponent = this.vueComponent({ history: this.history3, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "actionText"), "common-msg-has merged")
    }

    public testDetailTextStore () {
        const histPanelComponent = this.vueComponent({ history: this.history1, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "detailText"), "field|pl")
    }

    public testDetailTextCreate () {
        const histPanelComponent = this.vueComponent({ history: this.history2, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "detailText"), "")
    }

    public testDetailTextMerge () {
        const histPanelComponent = this.vueComponent({ history: this.history3, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "detailText"), "system-msg-With another object")
    }

    public testValueTextStore () {
        const histPanelComponent = this.vueComponent({ history: this.history1, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "valueText"), 2)
    }

    public testValueTextCreate () {
        const histPanelComponent = this.vueComponent({ history: this.history2, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "valueText"), "ox_object:1")
    }

    public testValueTextMerge () {
        const histPanelComponent = this.vueComponent({ history: this.history3, object: this.object })
        this.assertEqual(this.privateCall(histPanelComponent, "valueText"), "ox_object:1")
    }

    public testIsTargeted () {
        const histPanelComponent = this.vueComponent({
            history: this.history1,
            object: this.object,
            targetedField: "field1"
        })
        this.assertTrue(this.privateCall(histPanelComponent, "isTargeted", "field1"))
    }

    public testIsNotTargeted () {
        const histPanelComponent = this.vueComponent({
            history: this.history1,
            object: this.object,
            targetedField: "field1"
        })
        this.assertFalse(this.privateCall(histPanelComponent, "isTargeted", "field2"))
    }

    public testIsDisable () {
        const histPanelComponent = this.vueComponent({
            history: this.history1,
            object: this.object,
            targetedField: "field1"
        })
        this.assertTrue(this.privateCall(histPanelComponent, "isDisable", "field2"))
    }

    public testIsNotDisable () {
        const histPanelComponent = this.vueComponent({
            history: this.history1,
            object: this.object,
            targetedField: "field1"
        })
        this.assertFalse(this.privateCall(histPanelComponent, "isDisable", "field1"))
    }

    public testToggleTarget () {
        const histPanelComponent = this.mountComponent({
            history: this.history1,
            object: this.object
        })
        this.privateCall(histPanelComponent.vm, "toggleTarget", "field1")

        expect(histPanelComponent.emitted("toggleTargetField")).toBeTruthy()
    }
}

(new HistoryPanelTest()).launchTests()
