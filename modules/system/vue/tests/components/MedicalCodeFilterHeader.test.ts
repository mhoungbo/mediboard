/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import MedicalCode from "@modules/system/vue/models/MedicalCode"
import { shallowMount } from "@vue/test-utils"
import MedicalCodeFilterHeader from "@modules/system/vue/components/MedicalCodeFilterHeader/MedicalCodeFilterHeader.vue"

/* eslint-disable dot-notation */

const codes = [] as MedicalCode[]

const ccam = new MedicalCode()
ccam.codeType = "ccam"
ccam.code = "BAAA001"
ccam.description = "Allongement du muscle releveur de la paupi�re sup�rieure et/ou section de sa lame profonde [muscle de M�ller], avec interposition de mat�riau inerte ou autogreffe"

const cim10 = new MedicalCode()
cim10.codeType = "cim10"
cim10.code = "A00.0"
cim10.description = "Chol�ra classique"

const snomed = new MedicalCode()
snomed.codeType = "snomed"
snomed.code = "DE-35900"
snomed.description = "Affection coronavirus"

codes.push(...[ccam, cim10, snomed])

describe(
    "MedicalCodeFilterHeader",
    () => {
        test("test first selected code should be the first object",
            () => {
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                }).vm

                expect(medicalCodeFilter["selectedCode"]).toEqual("ccam")
            }
        )

        test("test codesTypes should have all codes from the objects",
            () => {
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                }).vm

                expect(medicalCodeFilter["codeTypes"]).toEqual(["ccam", "cim10", "snomed"])
            }
        )

        test("test chipColor should change",
            () => {
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                }).vm

                const initialChipColor = medicalCodeFilter["chipColor"]("snomed")
                medicalCodeFilter["selectCode"]("snomed")

                expect(initialChipColor).toEqual("")
                expect(medicalCodeFilter["chipColor"]("snomed")).toEqual("primary")
            }
        )

        test("test mounted should add listener",
            () => {
                const eventListenerSpy = jest.spyOn(window, "addEventListener")
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                }).vm

                expect(eventListenerSpy).toHaveBeenCalled()
                // @ts-ignore
                expect(eventListenerSpy).toHaveBeenCalledWith("keydown", medicalCodeFilter.manageKeydownListener)
            }
        )

        test("test destroyed should remove listener",
            () => {
                const eventListenerSpy = jest.spyOn(window, "removeEventListener")
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                })

                medicalCodeFilter.destroy()

                expect(eventListenerSpy).toHaveBeenCalled()
                // @ts-ignore
                expect(eventListenerSpy).toHaveBeenCalledWith("keydown", medicalCodeFilter.vm.manageKeydownListener)
            }
        )

        test("test manageKeydownListener should select Next on ArrowRight",
            () => {
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                }).vm

                const prevent = jest.fn()
                // @ts-ignore
                const selectNextSpy = jest.spyOn(medicalCodeFilter, "selectNext").mockImplementation()
                // @ts-ignore
                const selectPreviousSpy = jest.spyOn(medicalCodeFilter, "selectPrevious").mockImplementation()

                medicalCodeFilter["manageKeydownListener"]({ key: "ArrowRight", preventDefault: prevent } as unknown as KeyboardEvent)

                expect(selectNextSpy).toHaveBeenCalled()
                expect(selectPreviousSpy).not.toHaveBeenCalled()
                expect(prevent).toHaveBeenCalled()
            }
        )

        test("test manageKeydownListener should select Previous on ArrowLeft",
            () => {
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                }).vm

                const prevent = jest.fn()
                // @ts-ignore
                const selectNextSpy = jest.spyOn(medicalCodeFilter, "selectNext").mockImplementation()
                // @ts-ignore
                const selectPreviousSpy = jest.spyOn(medicalCodeFilter, "selectPrevious").mockImplementation()

                medicalCodeFilter["manageKeydownListener"]({ key: "ArrowLeft", preventDefault: prevent } as unknown as KeyboardEvent)

                expect(selectNextSpy).not.toHaveBeenCalled()
                expect(selectPreviousSpy).toHaveBeenCalled()
                expect(prevent).toHaveBeenCalled()
            }
        )

        test.each([
            { startIndex: 0, endIndex: 1 },
            { startIndex: 1, endIndex: 2 },
            { startIndex: 2, endIndex: 0 }
        ])(
            "test selectNext with start at $startIndex will go to $endIndex",
            ({ startIndex, endIndex }) => {
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                }).vm

                /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
                medicalCodeFilter["selectedCode"] = codes[startIndex].codeType!.toString()

                /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
                expect(medicalCodeFilter["selectNext"]()).toEqual(codes[endIndex].codeType!.toString())
            }
        )

        test.each([
            { startIndex: 0, endIndex: 2 },
            { startIndex: 1, endIndex: 0 },
            { startIndex: 2, endIndex: 1 }
        ])(
            "test selectPrevious with start at $startIndex will go to $endIndex",
            ({ startIndex, endIndex }) => {
                const medicalCodeFilter = shallowMount(MedicalCodeFilterHeader, {
                    propsData: {
                        codes
                    }
                }).vm

                /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
                medicalCodeFilter["selectedCode"] = codes[startIndex].codeType!.toString()

                /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
                expect(medicalCodeFilter["selectPrevious"]()).toEqual(codes[endIndex].codeType!.toString())
            }
        )
    }
)
