/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import NoteDialog from "@modules/system/vue/components/NoteDialog/NoteDialog.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import Note from "@modules/system/vue/models/Note"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import OxObject from "@/core/models/OxObject"
import oxApiService from "@/core/utils/OxApiService"
import {
    notesCreationMock,
    notesEmptyMock,
    notesMock,
    notesSchemaMock
} from "@modules/system/vue/tests/mocks/NotesMocks"
import { useSchemaStore } from "@/core/stores/schema"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * Test for NoteDialog
 */
export default class NoteDialogTest extends OxTest {
    protected component = NoteDialog

    private object = new OxObject()
    private defaultNote = new Note()
    private note = new Note()
    private note2 = new Note()
    private note3 = new Note()
    private note4 = new Note()
    private note5 = new Note()
    private note6 = new Note()
    private user = new Mediuser()
    private user2 = new Mediuser()
    private notes = [
        this.note,
        this.note2,
        this.note3,
        this.note4,
        this.note5,
        this.note6
    ]

    private schemaStore

    protected beforeAllTests () {
        super.beforeAllTests()

        this.object.links = {
            notes: "notes"
        }

        setActivePinia(pinia)
        this.schemaStore = useSchemaStore()

        this.user.id = "1"
        this.user.firstName = "John"
        this.user.lastName = "Doe"
        this.user2.id = "2"
        this.user2.firstName = "Bob"
        this.user2.lastName = "Williams"

        this.defaultNote.context = this.object
        this.defaultNote.dateData = "now"
        this.defaultNote.public = true
        this.defaultNote.meta = {
            permissions: {
                perm: "edit"
            }
        }

        this.note.id = "1"
        this.note.degre = "low"
        this.note.public = false
        this.note.dateData = "2023-02-02 10:00:00"
        this.note.author = this.user
        this.note.context = this.object
        this.note.text = ""
        this.note.meta = {
            permissions: {
                perm: "edit"
            }
        }
        this.note.setAuthorizedUsers([this.user2])

        this.note2.id = "2"
        this.note2.degre = "low"
        this.note2.dateData = "2023-02-08 10:00:00"
        this.note2.context = this.object
        this.note2.text = ""
        this.note2.meta = {
            permissions: {
                perm: "read"
            }
        }

        this.note3.id = "3"
        this.note3.degre = "medium"
        this.note3.dateData = "2023-03-04 10:00:00"
        this.note3.context = this.object
        this.note3.text = ""
        this.note4.id = "4"
        this.note4.degre = "medium"
        this.note4.dateData = "2023-03-05 10:00:00"
        this.note4.context = this.object
        this.note4.text = ""
        this.note5.id = "5"
        this.note5.degre = "high"
        this.note5.dateData = "2023-03-06 10:00:00"
        this.note5.context = this.object
        this.note5.text = ""
        this.note6.id = "6"
        this.note6.degre = "high"
        this.note6.dateData = "2023-03-07 10:00:00"
        this.note6.context = this.object
        this.note6.text = ""
    }

    protected beforeTest () {
        super.beforeTest()
        this.schemaStore.$reset()

        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: notesSchemaMock })
            .mockResolvedValue({ data: notesMock })

        jest.spyOn(Date, "now").mockImplementation(() => new Date("2023-03-07").getTime())
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    protected async mountComponentAsync (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )

        await this.flushPromises()

        return wrapper
    }

    protected async vueComponentAsync (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = await this.mountComponentAsync(props, stubs, slots)

        return wrapper.vm
    }

    public async testCreatedDefault () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        expect(oxApiService.get).toHaveBeenNthCalledWith(
            1,
            expect.stringContaining(
                "api/schemas/note?fieldsets=default"
            )
        )
        expect(oxApiService.get).toHaveBeenNthCalledWith(
            2,
            expect.stringContaining(
                "notes?sort=-date&relations=author%2CauthorizedUsers"
            )
        )
        expect(noteDialogComponent["showRelatedNotes"]).toBe(true)
        expect(noteDialogComponent["selectedNote"]).toBeUndefined()
        expect(noteDialogComponent["mutatedNote"]).toMatchObject(this.defaultNote)
        expect(noteDialogComponent["relatedNotes"]).toMatchObject([
            notesMock.data.find(note => note.id === "1"),
            notesMock.data.find(note => note.id === "2"),
            notesMock.data.find(note => note.id === "3"),
            notesMock.data.find(note => note.id === "4"),
            notesMock.data.find(note => note.id === "5"),
            notesMock.data.find(note => note.id === "6")
        ])
    }

    public async testFormTitleCreate () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        // Default case => selectedNote = undefined
        expect(this.privateCall(noteDialogComponent, "formTitle")).toBe("CNote-title-create")
    }

    public async testFormTitleModify () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        // Default case => selectedNote = undefined
        expect(this.privateCall(noteDialogComponent, "formTitle")).toBe("CNote-title-create")
    }

    public async testHasEditPermissions () {
        const notesComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note
        })
        expect(this.privateCall(notesComponent, "hasEditPermissions")).toBe(true)
    }

    public async testHasNotEditPermissions () {
        const notesComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note2
        })
        expect(this.privateCall(notesComponent, "hasEditPermissions")).toBe(false)
    }

    public async testShowFormCauseNoSelectedNote () {
        const notesComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })
        expect(this.privateCall(notesComponent, "showForm")).toBe(true)
    }

    public async testShowFormCausePermissions () {
        const notesComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note
        })
        expect(this.privateCall(notesComponent, "showForm")).toBe(true)
    }

    public async testNotShowForm () {
        const notesComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note2
        })
        expect(this.privateCall(notesComponent, "showForm")).toBe(false)
    }

    public async testOwnerRightIconDown () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        // Default case => showOwnerContent = false
        expect(this.privateCall(noteDialogComponent, "ownerRightIcon")).toBe("chevronDown")
    }

    public async testOwnerRightIconUp () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        noteDialogComponent["showOwnerContent"] = true
        expect(this.privateCall(noteDialogComponent, "ownerRightIcon")).toBe("chevronUp")
    }

    public async testVisibilityDataPublic () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        // Default case => mutatedValue.public = true
        expect(this.privateCall(noteDialogComponent, "visibilityData")).toMatchObject({
            mainIcon: "accountGroup",
            mainWording: "system-msg-Public note",
            otherIcon: "lock",
            otherWording: "system-action-make private"
        })
    }

    public async testVisibilityDataPrivate () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note
        })
        expect(this.privateCall(noteDialogComponent, "visibilityData")).toMatchObject({
            mainIcon: "lock",
            mainWording: "system-msg-Private note",
            otherIcon: "accountGroup",
            otherWording: "system-action-make public"
        })
    }

    public async testTodayNotes () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })

        expect(this.privateCall(noteDialogComponent, "todayNotes")).toMatchObject({
            label: "Today",
            notes: [notesMock.data.find(note => note.id === "6")]
        })
    }

    public async testYesterdayNotes () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })

        expect(this.privateCall(noteDialogComponent, "yesterdayNotes")).toMatchObject({
            label: "Yesterday",
            notes: [notesMock.data.find(note => note.id === "5")]
        })
    }

    public async testLastWeekNotes () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })

        expect(this.privateCall(noteDialogComponent, "lastWeekNotes")).toMatchObject({
            label: "common-7 days before",
            notes: [
                notesMock.data.find(note => note.id === "3"),
                notesMock.data.find(note => note.id === "4")
            ]
        })
    }

    public async testLastMonthNotes () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })

        expect(this.privateCall(noteDialogComponent, "lastMonthNotes")).toMatchObject({
            label: "common-30 days before",
            notes: [notesMock.data.find(note => note.id === "2")]
        })
    }

    public async testSortedRelatedNotes () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })

        expect(this.privateCall(noteDialogComponent, "sortedRelatedNotes")).toMatchObject([
            {
                label: "Today",
                notes: [notesMock.data.find(note => note.id === "6")]
            },
            {
                label: "Yesterday",
                notes: [notesMock.data.find(note => note.id === "5")]
            },
            {
                label: "common-7 days before",
                notes: [
                    notesMock.data.find(note => note.id === "3"),
                    notesMock.data.find(note => note.id === "4")
                ]
            },
            {
                label: "common-30 days before",
                notes: [notesMock.data.find(note => note.id === "2")]
            },
            {
                label: "2023",
                notes: [notesMock.data.find(note => note.id === "1")]
            }
        ])
    }

    public async testIsNotRelatedNotesEmpty () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })

        expect(this.privateCall(noteDialogComponent, "isRelatedNotesEmpty")).toBe(false)
    }

    public async testIsRelatedNotesEmpty () {
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: notesSchemaMock })
            .mockResolvedValue({ data: notesEmptyMock })

        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })

        expect(this.privateCall(noteDialogComponent, "isRelatedNotesEmpty")).toBe(true)
    }

    public async testHasAuthorizedUsers () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note
        })

        expect(this.privateCall(noteDialogComponent, "hasAuthorizedUsers")).toBe(true)
    }

    public async testHasNotAuthorizedUsers () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note2
        })

        expect(this.privateCall(noteDialogComponent, "hasAuthorizedUsers")).toBe(false)
    }

    public async testCreateNote () {
        const noteDialogComponent = await this.mountComponentAsync({
            object: this.object,
            user: this.user2
        })

        noteDialogComponent.vm["mutatedNote"].text = "bonjour"
        noteDialogComponent.vm["mutatedNote"].degre = "high"

        // @ts-ignore
        noteDialogComponent.vm.$refs.form["validate"] = () => new Promise<boolean>(() => true)
        jest.spyOn(oxApiService, "post").mockImplementation(() => Promise.resolve({ data: notesCreationMock }))

        // Default case => selectedNote = undefined
        await this.privateCall(noteDialogComponent.vm, "save")

        expect(oxApiService.post).toBeCalledWith("api/system/notes", {
            data: {
                id: "",
                type: "note",
                attributes: {
                    date: "now",
                    text: "bonjour",
                    degre: "high",
                    public: 1

                },
                relationships: {
                    context: {
                        data: {
                            id: "",
                            type: "ox_object"
                        }
                    },
                    authorizedUsers: {
                        data: []
                    }
                }
            }
        })

        expect(noteDialogComponent.vm["savedNoteId"]).toBe("1")
        expect(noteDialogComponent.vm["savedLinks"]).toMatchObject({
            self: "self",
            schema: "schema"
        })

        const events = noteDialogComponent.emitted("dataSaved")
        this.assertHaveLength(events, 1)

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "notes?sort=-date&relations=author%2CauthorizedUsers"
            )
        )
    }

    public async testUpdateNote () {
        const noteDialogComponent = await this.mountComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note
        })
        noteDialogComponent.vm["mutatedNote"].text = "bonjour"
        noteDialogComponent.vm["mutatedNote"].links = {
            self: "self"
        }

        // @ts-ignore
        noteDialogComponent.vm.$refs.form["validate"] = () => new Promise<boolean>(() => true)
        jest.spyOn(oxApiService, "patch").mockImplementation(() => Promise.resolve({ data: notesCreationMock }))

        await this.privateCall(noteDialogComponent.vm, "save")
        expect(oxApiService.patch).toBeCalledWith("self", {
            data: {
                id: "1",
                type: "note",
                attributes: {
                    text: "bonjour"
                },
                relationships: {}
            }
        })

        expect(noteDialogComponent.vm["savedNoteId"]).toBe("1")
        expect(noteDialogComponent.vm["savedLinks"]).toMatchObject({
            self: "self",
            schema: "schema"
        })

        const events = noteDialogComponent.emitted("dataSaved")
        this.assertHaveLength(events, 1)

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "notes?sort=-date&relations=author%2CauthorizedUsers"
            )
        )
    }

    public async testSaveAndCreate () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        const mockSave = jest.fn()
        noteDialogComponent["save"] = mockSave

        await this.privateCall(noteDialogComponent, "saveAndCreate")
        expect(mockSave).toBeCalledWith(true)
        expect(noteDialogComponent["selectedNote"]).toBeUndefined()
        expect(noteDialogComponent["mutatedNote"]).toMatchObject(this.defaultNote)
    }

    public async testSaveAndEdit () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        const mockSave = jest.fn()
        noteDialogComponent["save"] = mockSave

        noteDialogComponent["mutatedNote"].text = "bonjour"
        noteDialogComponent["mutatedNote"].degre = "high"

        noteDialogComponent["savedNoteId"] = "1"
        noteDialogComponent["savedLinks"] = {
            self: "self",
            schema: "schema"
        }

        await this.privateCall(noteDialogComponent, "saveAndEdit")
        expect(mockSave).toBeCalledWith(true)
        expect(noteDialogComponent["selectedNote"]).toMatchObject(noteDialogComponent["mutatedNote"])
    }

    public async testDeleteNote () {
        const noteDialogComponent = await this.mountComponentAsync({ object: this.object, user: this.user2 })
        this.privateCall(noteDialogComponent.vm, "deleteNote")
        const events = noteDialogComponent.emitted("deleteNote")
        this.assertHaveLength(events, 1)
    }

    public async testResetNote () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        noteDialogComponent["selectedNote"] = this.note
        this.privateCall(noteDialogComponent, "resetNote")
        expect(noteDialogComponent["selectedNote"]).toBeUndefined()
        expect(noteDialogComponent["mutatedNote"]).toMatchObject(this.defaultNote)
    }

    public async testUpdateCurrentNote () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        this.privateCall(noteDialogComponent, "updateCurrentNote", this.note)
        expect(noteDialogComponent["selectedNote"]).toBe(this.note)
        expect(noteDialogComponent["mutatedNote"]).toMatchObject(this.note)
        expect(noteDialogComponent["authorizedUsers"]).toMatchObject(this.note.authorizedUsers)
    }

    public async testIsSelected () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        noteDialogComponent["selectedNote"] = this.note
        expect(this.privateCall(noteDialogComponent, "isSelected", this.note)).toMatchObject({
            selected: true
        })
    }

    public async testIsNotSelected () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        noteDialogComponent["selectedNote"] = this.note
        expect(this.privateCall(noteDialogComponent, "isSelected", this.note2)).toMatchObject({
            selected: false
        })
    }

    public async testToggleOwner () {
        const noteDialogComponent = await this.vueComponentAsync({ object: this.object, user: this.user2 })
        this.privateCall(noteDialogComponent, "toggleOwner")
        expect(noteDialogComponent["showOwnerContent"]).toBe(true)
    }

    public async testToggleVisibility () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2,
            note: this.note
        })
        this.privateCall(noteDialogComponent, "toggleVisibility")
        expect(noteDialogComponent["mutatedNote"].public).toBe(true)
    }

    public async testOnClickOutside () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })
        this.privateCall(noteDialogComponent, "onClickOutside")
        expect(noteDialogComponent["showOwnerContent"]).toBe(false)
    }

    public async testAddAuthorizedUser () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })
        this.privateCall(noteDialogComponent, "addAuthorizedUser", this.user)
        expect(noteDialogComponent["authorizedUsers"]).toMatchObject([this.user])
    }

    public async testRemoveAuthorizedUser () {
        const noteDialogComponent = await this.vueComponentAsync({
            object: this.object,
            user: this.user2
        })
        noteDialogComponent["authorizedUsers"] = [this.user]
        this.privateCall(noteDialogComponent, "removeAuthorizedUser", this.user)
        expect(noteDialogComponent["authorizedUsers"]).toMatchObject([])
    }
}

(new NoteDialogTest()).launchTests()
