/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import MedicalCode from "@modules/system/vue/models/MedicalCode"
import { shallowMount } from "@vue/test-utils"
import MedicalCodeAutocomplete
    from "@modules/system/vue/components/MedicalCodeAutocomplete/MedicalCodeAutocomplete.vue"

/* eslint-disable dot-notation */

const ccam = new MedicalCode()
ccam.codeType = "ccam"
ccam.code = "BAAA001"
ccam.description = "Allongement du muscle releveur de la paupi�re sup�rieure et/ou section de sa lame profonde [muscle de M�ller], avec interposition de mat�riau inerte ou autogreffe"

describe(
    "MedicalCodeAutocomplete",
    () => {
        test("test build component",
            () => {
                const medicalCodeFilter = shallowMount(MedicalCodeAutocomplete, {
                    propsData: {
                        code: ccam
                    }
                })

                expect(medicalCodeFilter.find(".MedicalCodeAutocomplete").exists()).toBeTruthy()
            }
        )
    }
)
