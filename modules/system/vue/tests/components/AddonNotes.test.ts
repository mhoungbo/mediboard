/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import AddonNotes from "@modules/system/vue/components/AddonNotes/AddonNotes.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import OxObject from "@/core/models/OxObject"
import Note from "@modules/system/vue/models/Note"
import oxApiService from "@/core/utils/OxApiService"
import pinia from "@/core/plugins/OxPiniaCore"
import { setActivePinia } from "pinia"
import { notesCreationMock, notesMock } from "@modules/system/vue/tests/mocks/NotesMocks"
import OxThemeCore from "@oxify/utils/OxThemeCore"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * Test for AddonNotes
 */
export default class AddonNotesTest extends OxTest {
    protected component = AddonNotes

    private object = new OxObject()
    private note = new Note()
    private note2 = new Note()
    private note3 = new Note()
    private note4 = new Note()
    private note5 = new Note()
    private note6 = new Note()
    private notes = [
        this.note,
        this.note2,
        this.note3,
        this.note4,
        this.note5,
        this.note6
    ]

    protected beforeAllTests () {
        super.beforeAllTests()

        this.object.links = {
            notes: "notes"
        }

        // TODO: Set user in pinia userStore

        this.note.id = "1"
        this.note.degre = "low"
        this.note.dateData = "2023-02-01 10:00:00"
        this.note.links = {
            self: "self"
        }
        this.note.meta = {
            permissions: {
                perm: "edit"
            }
        }
        this.note2.id = "2"
        this.note2.degre = "low"
        this.note2.dateData = "2023-02-02 10:00:00"
        this.note2.meta = {
            permissions: {
                perm: "read"
            }
        }

        this.note3.id = "3"
        this.note3.degre = "medium"
        this.note3.dateData = "2023-01-01 10:00:00"
        this.note4.id = "4"
        this.note4.degre = "medium"
        this.note4.dateData = "2023-01-02 10:00:00"
        this.note5.id = "5"
        this.note5.degre = "high"
        this.note5.dateData = "2023-03-01 10:00:00"
        this.note6.id = "6"
        this.note6.degre = "high"
        this.note6.dateData = "2023-03-02 10:00:00"

        setActivePinia(pinia)
    }

    protected beforeTest () {
        super.beforeTest()
        jest.spyOn(oxApiService, "get").mockResolvedValue({ data: notesMock })
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    protected async mountComponentAsync (
        props: object = {
            object: this.object
        },
        stubs: { [key: string]: Component | string | boolean } | string[] = {
            OxAlert: true,
            NoteDialog: true
        },
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )

        await this.flushPromises()

        return wrapper
    }

    protected async vueComponentAsync (
        props: object = {
            object: this.object
        },
        stubs: { [key: string]: Component | string | boolean } | string[] = {
            OxAlert: true,
            NoteDialog: true
        },
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        const wrapper = await this.mountComponentAsync(props, stubs, slots)

        return wrapper.vm
    }

    public async testIconWithNotes () {
        const notesComponent = await this.vueComponentAsync()
        expect(this.privateCall(notesComponent, "icon")).toBe("postIt")
    }

    public async testIconWithoutNotes () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["notesLength"] = 0
        expect(this.privateCall(notesComponent, "icon")).toBe("postItOutline")
    }

    public async testIconColorWithNotes () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["sortedNotes"] = [this.note6]
        expect(this.privateCall(notesComponent, "iconColor")).toBe(OxThemeCore.errorSurfaceDefault)
    }

    public async testIconColorWithoutNotes () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["notesLength"] = 0
        expect(this.privateCall(notesComponent, "iconColor")).toBe(OxThemeCore.grey500)
    }

    public async testAltActionsSortedByCriticality () {
        const notesComponent = await this.vueComponentAsync()
        // Default case => sortedByDate = false
        expect(this.privateCall(notesComponent, "altActions")).toMatchObject(
            [
                {
                    label: "system-action-sort by date",
                    eventName: "sortdate"
                }
            ]
        )
    }

    public async testAltActionsSortedByDate () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["sortedByDate"] = true
        expect(this.privateCall(notesComponent, "altActions")).toMatchObject(
            [
                {
                    label: "system-action-sort by criticality",
                    eventName: "sortcriticality"
                }
            ]
        )
    }

    public async testNbLineBreakIfEmtpy () {
        const notesComponent = await this.vueComponentAsync()
        // Default case => text = ""
        expect(this.privateCall(notesComponent, "inputNbLineBreak")).toBe(0)
    }

    public async testNbLineBreakNotEmpty () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["text"] = "Bonjour\nCeci est un texte\navec break"
        expect(this.privateCall(notesComponent, "inputNbLineBreak")).toBe(2)
    }

    public async testInputStyleOverflowHidden () {
        const notesComponent = await this.vueComponentAsync()
        // Nb line break = 2
        notesComponent["text"] = "Bonjour\nCeci est un texte\navec break"
        expect(this.privateCall(notesComponent, "inputStyle")).toMatchObject({
            height: "68px",
            overflow: "hidden"
        })
    }

    public async testInputStyleOverflowAuto () {
        const notesComponent = await this.vueComponentAsync()
        // Nb line break > 3
        notesComponent["text"] = "Bonjour\nCeci est\nun texte\navec\nbreak"
        expect(this.privateCall(notesComponent, "inputStyle")).toMatchObject({
            overflow: "auto",
            height: "88px"
        })
    }

    public async testIsDialogOrDeleteModalOpenedDefault () {
        const notesComponent = await this.vueComponentAsync()
        // Default case => showDialog = false and showDeleteModal = false
        expect(this.privateCall(notesComponent, "isDialogOrDeleteModalOpened")).toBe(false)
    }

    public async testIsDialogOrDeleteModalOpenedWithDialog () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["showDialog"] = true
        expect(this.privateCall(notesComponent, "isDialogOrDeleteModalOpened")).toBe(true)
    }

    public async testIsDialogOrDeleteModalOpenedWithAlert () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["showDeleteModal"] = true
        expect(this.privateCall(notesComponent, "isDialogOrDeleteModalOpened")).toBe(true)
    }

    public async testHasNotes () {
        const notesComponent = await this.vueComponentAsync()
        expect(this.privateCall(notesComponent, "hasNotes")).toBe(true)
    }

    public async testHasNotNotes () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["notesLength"] = 0
        expect(this.privateCall(notesComponent, "hasNotes")).toBe(false)
    }

    public async testCriticalityColorHigh () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["sortedNotes"] = [this.note6]
        expect(this.privateCall(notesComponent, "criticalityColor")).toBe(OxThemeCore.errorSurfaceDefault)
    }

    public async testCriticalityColorMedium () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["sortedNotes"] = [this.note3]
        expect(this.privateCall(notesComponent, "criticalityColor")).toBe(OxThemeCore.warningSurfaceDefault)
    }

    public async testCriticalityColorLow () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["sortedNotes"] = [this.note]
        expect(this.privateCall(notesComponent, "criticalityColor")).toBe(OxThemeCore.secondary)
    }

    public async testSortByCriticality () {
        const notesComponent = await this.vueComponentAsync()
        jest.clearAllMocks()

        notesComponent["sortedByDate"] = true
        await this.privateCall(notesComponent, "sortByCriticality")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "notes?sort=-degre%2C-date&relations=author"
            )
        )
        expect(notesComponent["sortedNotes"]).toMatchObject([
            notesMock.data.find(note => note.id === "1"),
            notesMock.data.find(note => note.id === "2"),
            notesMock.data.find(note => note.id === "3"),
            notesMock.data.find(note => note.id === "4"),
            notesMock.data.find(note => note.id === "5"),
            notesMock.data.find(note => note.id === "6")
        ])
        expect(notesComponent["sortedByDate"]).toBe(false)
    }

    public async testSortByDate () {
        const notesComponent = await this.vueComponentAsync()
        await this.privateCall(notesComponent, "sortByDate", this.notes)

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "notes?sort=-date&relations=author"
            )
        )
        expect(notesComponent["sortedNotes"]).toMatchObject([
            notesMock.data.find(note => note.id === "1"),
            notesMock.data.find(note => note.id === "2"),
            notesMock.data.find(note => note.id === "3"),
            notesMock.data.find(note => note.id === "4"),
            notesMock.data.find(note => note.id === "5"),
            notesMock.data.find(note => note.id === "6")
        ])
        expect(notesComponent["sortedByDate"]).toBe(true)
    }

    public async testOpenModal () {
        const notesComponent = await this.vueComponentAsync()
        this.privateCall(notesComponent, "openModal")
        expect(notesComponent["showDialog"]).toBe(true)
    }

    public async testReset () {
        const notesComponent = await this.vueComponentAsync()
        this.privateCall(notesComponent, "reset")
        expect(notesComponent["currentNote"]).toBeUndefined()
        expect(notesComponent["tooltipLoaded"]).toBe(false)
        expect(notesComponent["showDialog"]).toBe(false)
    }

    public async testAskDeleteNote () {
        const notesComponent = await this.vueComponentAsync()
        this.privateCall(notesComponent, "askDeleteNote", this.note)
        expect(notesComponent["currentNote"]).toBe(this.note)
        expect(notesComponent["showDeleteModal"]).toBe(true)
    }

    public async testEditNote () {
        const notesComponent = await this.vueComponentAsync()
        this.privateCall(notesComponent, "editNote", this.note)
        expect(notesComponent["currentNote"]).toBe(this.note)
        expect(notesComponent["showDialog"]).toBe(true)
    }

    public async testDeleteNoteWithCurrentNote (): Promise<void> {
        const notesComponent = await this.vueComponentAsync()
        jest.spyOn(oxApiService, "delete").mockImplementation(() => Promise.resolve({ data: "" }))

        notesComponent["currentNote"] = this.note
        const mockReset = jest.fn()
        notesComponent["reset"] = mockReset

        await this.privateCall(notesComponent, "deleteNote")

        expect(oxApiService.delete).toBeCalledWith(
            expect.stringContaining(
                "self"
            )
        )
        expect(mockReset).toHaveBeenCalledTimes(1)
    }

    public async testDeleteNoteWithoutCurrentNote (): Promise<void> {
        const notesComponent = await this.vueComponentAsync()
        jest.spyOn(oxApiService, "delete").mockImplementation(() => Promise.resolve({ data: "" }))

        const mockReset = jest.fn()
        notesComponent["reset"] = mockReset

        await this.privateCall(notesComponent, "deleteNote")

        expect(oxApiService.delete).toHaveBeenCalledTimes(0)
        expect(mockReset).toHaveBeenCalledTimes(0)
    }

    public async testCreateNote (): Promise<void> {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["text"] = "bonjour"

        jest.spyOn(oxApiService, "post").mockImplementation(() => Promise.resolve({ data: notesCreationMock }))

        await this.privateCall(notesComponent, "createNote")

        expect(oxApiService.post).toBeCalledWith("api/system/notes", {
            data: {
                id: "",
                type: "note",
                attributes: {
                    date: "now",
                    text: "bonjour",
                    degre: "low",
                    public: 1

                },
                relationships: {
                    context: {
                        data: {
                            id: "",
                            type: "ox_object"
                        }
                    }
                }
            }
        })
        expect(notesComponent["text"]).toBe("")
    }

    public async testDisableCloseTooltip () {
        const notesComponent = await this.vueComponentAsync()
        this.privateCall(notesComponent, "disableCloseTooltip")
        expect(notesComponent["preventCloseTooltip"]).toBe(true)
    }

    public async testEnableCloseTooltip () {
        const notesComponent = await this.vueComponentAsync()
        this.privateCall(notesComponent, "enableCloseTooltip")
        expect(notesComponent["preventCloseTooltip"]).toBe(false)
    }

    public async testOnClickOutside () {
        const notesComponent = await this.vueComponentAsync()
        this.privateCall(notesComponent, "onClickOutside")
        expect(notesComponent["forceCloseTooltip"]).toBe(true)
    }

    public async testResetClickOutside () {
        const notesComponent = await this.vueComponentAsync()
        this.privateCall(notesComponent, "resetClickOutside")
        expect(notesComponent["forceCloseTooltip"]).toBe(false)
    }

    public async testHasEditPermissions () {
        const notesComponent = await this.vueComponentAsync()
        expect(this.privateCall(notesComponent, "hasEditPermissions", this.note)).toBe(true)
    }

    public async testHasNotEditPermissions () {
        const notesComponent = await this.vueComponentAsync()
        expect(this.privateCall(notesComponent, "hasEditPermissions", this.note2)).toBe(false)
    }

    public async testIsLast () {
        const notesComponent = await this.vueComponentAsync()
        expect(this.privateCall(notesComponent, "isLast", 5)).toBe(true)
    }

    public async testIsNotLast () {
        const notesComponent = await this.vueComponentAsync()
        expect(this.privateCall(notesComponent, "isLast", 4)).toBe(false)
    }

    public async testIsNotLastCauseOnlyOneNote () {
        const notesComponent = await this.vueComponentAsync()
        notesComponent["sortedNotes"] = [this.note]
        notesComponent["notesLength"] = 1
        expect(this.privateCall(notesComponent, "isLast", 0)).toBe(false)
    }
}

(new AddonNotesTest()).launchTests()
