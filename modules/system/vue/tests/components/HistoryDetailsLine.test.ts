/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import HistoryDetailsLine from "@modules/system/vue/components/HistoryDetailsLine/HistoryDetailsLine.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import HistoryFieldDiff from "@modules/system/vue/models/HistoryFieldDiff"
import OxThemeCore from "@oxify/utils/OxThemeCore"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * Test for HistoryDetailsLine
 */
export default class HistoryDetailsLineTest extends OxTest {
    protected component = HistoryDetailsLine

    private historyField1 = new HistoryFieldDiff()
    private historyField2 = new HistoryFieldDiff()
    private historyField3 = new HistoryFieldDiff()
    private historyField4 = new HistoryFieldDiff()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.historyField1.id = "1"
        this.historyField1.name = "field1"
        this.historyField1.oldValue = "oldValue1"
        this.historyField1.newValue = "newValue1"

        this.historyField2.id = "2"
        this.historyField2.name = "field2"
        this.historyField2.oldValue = ""
        this.historyField2.newValue = "newValue2"

        this.historyField3.id = "3"
        this.historyField3.name = "field1"
        this.historyField3.oldValue = "oldValue3"
        this.historyField3.newValue = ""

        this.historyField4.id = "4"
        this.historyField4.name = "field1"
        this.historyField4.oldValue = ""
        this.historyField4.newValue = ""
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testHasDisableClass () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField1,
            disable: true
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "classes"),
            {
                disable: true,
                targeted: false
            }
        )
    }

    public testHasTargetedClass () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField1,
            targeted: true
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "classes"),
            {
                disable: false,
                targeted: true
            }
        )
    }

    public testHasDisableTargetedClass () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField1,
            disable: true,
            targeted: true
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "classes"),
            {
                disable: true,
                targeted: true
            }
        )
    }

    public testHasNotClass () {
        const histDetailsLineComponent = this.vueComponent({ historyDetails: this.historyField1 })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "classes"),
            {
                disable: false,
                targeted: false
            }
        )
    }

    public testColorTargetedLogo () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField1,
            targeted: true
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "colorTargetLogo"),
            OxThemeCore.secondary
        )
    }

    public testColorNotTargetedLogo () {
        const histDetailsLineComponent = this.vueComponent({ historyDetails: this.historyField1 })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "colorTargetLogo"),
            OxThemeCore.grey600
        )
    }

    public testIconAdd () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField2,
            historyType: "store"
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "icon"),
            "add"
        )
    }

    public testIconDelete () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField3,
            historyType: "store"
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "icon"),
            "delete"
        )
    }

    public testIconEdit () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField1,
            historyType: "store"
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "icon"),
            "edit"
        )
    }

    public testActionTextAdd () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField2,
            historyType: "store"
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "actionText"),
            "common-msg-Has added"
        )
    }

    public testActionTextDelete () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField3,
            historyType: "store"
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "actionText"),
            "common-msg-Has deleted"
        )
    }

    public testActionTextEdit () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField1,
            historyType: "store"
        })
        this.assertEqual(
            this.privateCall(histDetailsLineComponent, "actionText"),
            "common-msg-Has edited"
        )
    }

    public testShowOldValue () {
        const histDetailsLineComponent = this.vueComponent({ historyDetails: this.historyField1 })
        this.assertTrue(this.privateCall(histDetailsLineComponent, "showOldValue"))
    }

    public testNotShowOldValue () {
        const histDetailsLineComponent = this.vueComponent({ historyDetails: this.historyField2 })
        this.assertFalse(this.privateCall(histDetailsLineComponent, "showOldValue"))
    }

    public testShowNewValue () {
        const histDetailsLineComponent = this.vueComponent({ historyDetails: this.historyField1 })
        this.assertTrue(this.privateCall(histDetailsLineComponent, "showNewValue"))
    }

    public testNotShowNewValue () {
        const histDetailsLineComponent = this.vueComponent({ historyDetails: this.historyField3 })
        this.assertFalse(this.privateCall(histDetailsLineComponent, "showNewValue"))
    }

    public testShowDiff () {
        const histDetailsLineComponent = this.vueComponent({ historyDetails: this.historyField4 })
        this.assertTrue(this.privateCall(histDetailsLineComponent, "showDiff"))
    }

    public testNotShowDiff () {
        const histDetailsLineComponent = this.vueComponent({ historyDetails: this.historyField1 })
        this.assertFalse(this.privateCall(histDetailsLineComponent, "showDiff"))
    }

    public testShowLinkText () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField1,
            historyType: "store"
        })
        this.assertTrue(this.privateCall(histDetailsLineComponent, "showLinkText"))
    }

    public testNotShowLinkTextCauseType () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField1,
            historyType: "create"
        })
        this.assertFalse(this.privateCall(histDetailsLineComponent, "showLinkText"))
    }

    public testNotShowLinkTextCauseValues () {
        const histDetailsLineComponent = this.vueComponent({
            historyDetails: this.historyField2,
            historyType: "store"
        })
        this.assertFalse(this.privateCall(histDetailsLineComponent, "showLinkText"))
    }

    public testClick (): void {
        const histDetailsLineComponent = this.mountComponent({
            historyDetails: this.historyField1
        })

        this.privateCall(histDetailsLineComponent.vm, "click")
        this.assertTrue(histDetailsLineComponent.emitted("click"))
    }
}

(new HistoryDetailsLineTest()).launchTests()
