/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import HistorySmallLine from "@modules/system/vue/components/HistorySmallLine/HistorySmallLine.vue"
import History from "@modules/system/vue/models/History"
import OxTranslator from "@/core/plugins/OxTranslator"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import pinia from "@/core/plugins/OxPiniaCore"
import { setActivePinia } from "pinia"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * Test for HistorySmallLine
 */
export default class HistorySmallLineTest extends OxTest {
    protected component = HistorySmallLine

    private history1 = new History()
    private history2 = new History()
    private history3 = new History()
    private user = new Mediuser()

    protected beforeAllTests () {
        super.beforeAllTests()

        setActivePinia(pinia)

        this.user.id = "1"
        this.user.lastName = "Doe"
        this.user.firstName = "John"

        this.history1.historyType = "store"
        this.history2.historyType = "create"
        this.history3.historyType = "merge"
        this.history1.user = this.user
        this.history2.user = this.user
        this.history3.user = this.user
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testTypeLogoAdd () {
        const histSmallLineComponent = this.vueComponent({ history: this.history2 })
        this.assertEqual(this.privateCall(histSmallLineComponent, "typeLogo"), "add")
    }

    public testTypeLogoEdit () {
        const histSmallLineComponent = this.vueComponent({ history: this.history1 })
        this.assertEqual(this.privateCall(histSmallLineComponent, "typeLogo"), "edit")
    }

    public testTypeLogoMerge () {
        const histPanelComponent = this.vueComponent({ history: this.history3 })
        this.assertEqual(this.privateCall(histPanelComponent, "typeLogo"), "call")
    }
}

(new HistorySmallLineTest()).launchTests()
