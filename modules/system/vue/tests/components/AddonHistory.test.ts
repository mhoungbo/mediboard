/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import AddonHistory from "@modules/system/vue/components/AddonHistory/AddonHistory.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import OxObject from "@/core/models/OxObject"
import History from "@modules/system/vue/models/History"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * Test for AddonHistory
 */
export default class AddonHistoryTest extends OxTest {
    protected component = AddonHistory

    private object = new OxObject()
    private history1 = new History()
    private history2 = new History()
    private history3 = new History()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.object.links = {
            history: "history"
        }

        this.history1.id = "1"
        this.history1.historyType = "store"
        this.history2.id = "2"
        this.history2.historyType = "store"
        this.history3.id = "3"
        this.history3.historyType = "create"
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public async testOpenModal () {
        const historyComponent = this.vueComponent({ object: this.object })
        await this.privateCall(historyComponent, "openModal")

        expect(historyComponent["forceCloseTooltip"]).toBe(true)
        expect(historyComponent["showDialog"]).toBe(true)
    }

    public testResetForceCloseTooltip () {
        const historyComponent = this.vueComponent({ object: this.object })
        historyComponent["forceCloseTooltip"] = true
        this.privateCall(historyComponent, "resetForceCloseTooltip")
        expect(historyComponent["forceCloseTooltip"]).toBe(false)
    }

    public testSortData () {
        const historyComponent = this.vueComponent({ object: this.object })
        this.privateCall(historyComponent, "sortData", [this.history1, this.history2, this.history3])
        expect(historyComponent["createLog"]).toMatchObject(this.history3)
        expect(historyComponent["storeLogs"]).toMatchObject([this.history1, this.history2])
    }

    public testMoreCount () {
        const historyComponent = this.vueComponent({ object: this.object })
        expect(this.privateCall(historyComponent, "moreCount", 10)).toBe(4)
    }

    public testHasMoreHistory () {
        const historyComponent = this.vueComponent({ object: this.object })
        expect(this.privateCall(historyComponent, "hasMoreHistory", 10)).toBeTruthy()
    }

    public testHasNotMoreHistory () {
        const historyComponent = this.vueComponent({ object: this.object })
        expect(this.privateCall(historyComponent, "hasMoreHistory", 5)).toBeFalsy()
    }
}

(new AddonHistoryTest()).launchTests()
