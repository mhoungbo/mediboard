/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest, { addActiveModules } from "@oxify/utils/OxTest"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import AddonsGeneric from "@modules/system/vue/components/AddonsGeneric/AddonsGeneric.vue"
import OxObject from "@/core/models/OxObject"
import * as OxModulesManager from "@/core/utils/OxModulesManager"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"

/* eslint-disable dot-notation */

/**
 * Test for AddonsGeneric
 */
export default class AddonsGenericTest extends OxTest {
    protected component = AddonsGeneric

    private object = new OxObject()

    protected beforeTest () {
        super.beforeTest()
        this.object = new OxObject()
    }

    protected beforeAllTests () {
        super.beforeAllTests()

        setActivePinia(oxPiniaCore)
        addActiveModules("dPsante400")
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {
            AddonIdex: true,
            AddonHistory: true
        },
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {
            AddonIdex: true,
            AddonHistory: true
        },
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testCanViewHistory () {
        const addons = this.vueComponent({ object: this.object })
        addons["prefs"] = {
            can_view_history: "1",
            is_dark: "0"
        }
        expect(this.privateCall(addons, "canViewHistory")).toBeTruthy()
    }

    public testCanNotViewHistory () {
        const addons = this.vueComponent({ object: this.object })
        addons["prefs"] = { is_dark: "0" }
        expect(this.privateCall(addons, "canViewHistory")).toBeFalsy()
    }

    public testCanViewIdex () {
        this.object.links.identifiers = "test"
        jest.spyOn(OxModulesManager, "isModuleActive").mockReturnValue(true)
        const addons = this.vueComponent({ object: this.object })

        expect(addons["canViewIdex"]).toBeTruthy()
    }

    public testCanNotViewIdexWithoutLink () {
        this.object.links.identifiers = undefined
        jest.spyOn(OxModulesManager, "isModuleActive").mockReturnValue(true)
        const addons = this.vueComponent({ object: this.object })

        expect(addons["canViewIdex"]).toBeFalsy()
    }

    public testCanNotViewIdexWithoutModule () {
        this.object.links.identifiers = "text"
        jest.spyOn(OxModulesManager, "isModuleActive").mockReturnValue(false)
        const addons = this.vueComponent({ object: this.object })

        expect(addons["canViewIdex"]).toBeFalsy()
    }

    public testCanNotViewIdexWithoutBoth () {
        this.object.links.identifiers = undefined
        jest.spyOn(OxModulesManager, "isModuleActive").mockReturnValue(false)
        const addons = this.vueComponent({ object: this.object })

        expect(addons["canViewIdex"]).toBeFalsy()
    }
}

(new AddonsGenericTest()).launchTests()
