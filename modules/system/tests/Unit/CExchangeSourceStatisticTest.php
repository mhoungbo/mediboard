<?php

namespace Ox\Mediboard\System\Tests\Unit;

use Ox\Core\CMbDT;
use Ox\Mediboard\System\CExchangeSourceStatistic;
use Ox\Tests\OxUnitTestCase;

class CExchangeSourceStatisticTest extends OxUnitTestCase
{
    public function testCreateStatisticFromNothing(): void
    {
        $new_statistics = CExchangeSourceStatistic::newFromLast(null);

        $this->assertEquals(1, $new_statistics->nb_call);
        $this->assertLessThanOrEqual(CMbDT::dateTime('+2 minutes'), $new_statistics->last_connexion_date);
        $this->assertLessThanOrEqual(CMbDT::dateTime('+2 minutes'), $new_statistics->last_verification_date);
        $this->assertEquals($new_statistics->last_connexion_date, $new_statistics->last_verification_date);
        $this->assertEquals(0, $new_statistics->failures);
    }

    public function providerCreateStatisticsFromExistingLastStats(): array
    {
        $stat_1 = CExchangeSourceStatistic::newFromLast(null);

        $stat_2                         = CExchangeSourceStatistic::newFromLast($stat_1);
        $stat_2->failures               = 3;
        $stat_2->last_connexion_date    = CMbDT::dateTime('-2 minutes');
        $stat_2->last_verification_date = CMbDT::dateTime('-2 minutes');

        return [
            [$stat_1],
            [$stat_2],
        ];
    }

    /**
     * @dataProvider providerCreateStatisticsFromExistingLastStats
     *
     * @return void
     */
    public function testCreateStatisticsFromExistingLastStats(CExchangeSourceStatistic $last_stats): void
    {
        $new_statistics = CExchangeSourceStatistic::newFromLast($last_stats);

        $this->assertEquals($last_stats->nb_call + 1, $new_statistics->nb_call);
        $this->assertEquals($last_stats->last_connexion_date, $new_statistics->last_connexion_date);
        $this->assertEquals($last_stats->failures, $new_statistics->failures);
        $this->assertLessThanOrEqual(CMbDT::dateTime('+2 minutes'), $new_statistics->last_verification_date);
    }
}
