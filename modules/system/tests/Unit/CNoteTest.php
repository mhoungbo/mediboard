<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit;

use Ox\Core\Api\Request\Content\JsonApiItem;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbException;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CNote;
use Ox\Mediboard\System\Tests\Fixtures\NoteFixtures;
use Ox\Tests\OxUnitTestCase;

class CNoteTest extends OxUnitTestCase
{
    public function testGetResourceWithtoutId(): void
    {
        $note = new CNote();
        $this->assertNull($note->getResourceAuthor());
        $this->assertNull($note->getResourceContext());
    }

    public function testGetResource(): void
    {
        /** @var CNote $note */
        $note = $this->getObjectFromFixturesReference(CNote::class, NoteFixtures::USER_NOTE_FIXTURES . '1');
        $this->assertInstanceOf(Item::class, $note->getResourceAuthor());
        $this->assertInstanceOf(Item::class, $note->getResourceContext());

        $user = $this->getObjectFromFixturesReference(CMediusers::class, NoteFixtures::USER_NOTE_FIXTURES);

        $this->assertEquals($user->_id, $note->_fwd['user_id']->_id);
        $this->assertEquals($user->_id, $note->_fwd['object_id']->_id);
    }

    public function testSetResourceAuthorEmptyAuthor(): void
    {
        /** @var CNote $note */
        $note = $this->getObjectFromFixturesReference(CNote::class, NoteFixtures::USER_NOTE_FIXTURES . '1');
        $note->setResourceAuthor(null);

        $this->assertEquals('', $note->user_id);
    }

    public function testSetResourceAuthor(): void
    {
        /** @var CNote $note */
        $note = $this->getObjectFromFixturesReference(CNote::class, NoteFixtures::USER_NOTE_FIXTURES . '1');

        $user = CMediusers::get();
        $note->setResourceAuthor(new JsonApiItem(['type' => $user::RESOURCE_TYPE, 'id' => $user->_id]));

        $this->assertEquals($user->_id, $note->user_id);
    }

    public function testSetResourceContextNull(): void
    {
        /** @var CNote $note */
        $note = $this->getObjectFromFixturesReference(CNote::class, NoteFixtures::USER_NOTE_FIXTURES . '1');
        $note->setResourceContext(null);
        $this->assertEquals('', $note->object_class);
        $this->assertEquals('', $note->object_id);
    }

    public function testSetResourceContext(): void
    {
        /** @var CNote $note */
        $note = $this->getObjectFromFixturesReference(CNote::class, NoteFixtures::USER_NOTE_FIXTURES . '1');

        $user = CMediusers::get();
        $note->setResourceContext(new JsonApiItem(['type' => $user::RESOURCE_TYPE, 'id' => $user->_id]));

        $this->assertEquals($user->_class, $note->object_class);
        $this->assertEquals($user->_id, $note->object_id);
    }

    public function testSetResourceAuthorizedUsers(): void
    {
        $current_user = CMediusers::get();
        /** @var CMediusers $other_user */
        $other_user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        /** @var CNote $note */
        $note = $this->getObjectFromFixturesReference(CNote::class, NoteFixtures::USER_NOTE_FIXTURES . '1');
        $note->setResourceAuthorizedUsers(
            [
                new JsonApiItem(['type' => $current_user::RESOURCE_TYPE, 'id' => $current_user->_id]),
                new JsonApiItem(['type' => $other_user::RESOURCE_TYPE, 'id' => $other_user->_id]),
            ]
        );

        $authorized = $this->getPrivateProperty($note, '_authorized_users');
        $this->assertArrayHasKey($current_user->_id, $authorized);
        $this->assertArrayHasKey($other_user->_id, $authorized);
    }

    public function testAddAuthorization(): CNote
    {
        /** @var CNote $note */
        $note = $this->getObjectFromFixturesReference(CNote::class, NoteFixtures::USER_NOTE_FIXTURES . '1');

        /** @var CMediusers $user */
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);
        $this->invokePrivateMethod($note, 'addAuthorization', $user);

        if (isset($note->_count['permissions'])) {
            unset($note->_count['permissions']);
        }

        /** @var CPermObject[] $perms */
        $perms = $note->loadBackRefs('permissions');

        $perm_exists = false;
        foreach ($perms as $perm) {
            if ($perm->user_id === $user->_id) {
                $perm_exists = true;
                break;
            }
        }

        $this->assertTrue($perm_exists);

        return $note;
    }

    /**
     * @depends testAddAuthorization
     */
    public function testGetResourceAuthorizedUsers(CNote $note): CNote
    {
        $ressource = $note->getResourceAuthorizedUsers();

        $this->assertInstanceOf(Collection::class, $ressource);

        return $note;
    }

    /**
     * @depends testGetResourceAuthorizedUsers
     */
    public function testPurgeAuthorizations(CNote $note): void
    {
        $this->invokePrivateMethod($note, 'purgeAuthorizations', []);

        if (isset($note->_count['permissions'])) {
            unset($note->_count['permissions']);
        }

        $this->assertCount(0, $note->loadBackRefs('permissions'));
    }

    public function testAddAuthorizationNoNoteId(): void
    {
        $note = new CNote();

        $this->expectExceptionObject(new CMbException('CNote-error-Cannot create authorization for non existing note'));
        $this->invokePrivateMethod($note, 'addAuthorization', new CMediusers());
    }

    public function testAddAuthorizationNoUserId(): void
    {
        /** @var CNote $note */
        $note = $this->getObjectFromFixturesReference(CNote::class, NoteFixtures::USER_NOTE_FIXTURES . '1');
        $this->expectExceptionObject(new CMbException('CNote-error-Cannot create authorization for non existing user'));
        $this->invokePrivateMethod($note, 'addAuthorization', new CMediusers());
    }

    public function testPurgeAuthorizationsNoNoteId(): void
    {
        $note = new CNote();
        $this->expectExceptionObject(new CMbException('CNote-error-Cannot purge authorizations for non existing note'));
        $this->invokePrivateMethod($note, 'purgeAuthorizations', []);
    }
}
