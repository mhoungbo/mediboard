<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit\Sources;

use Ox\Mediboard\System\Sources\ExchangeSourceReport;
use Ox\Tests\OxUnitTestCase;

class ExchangeSourceReportTest extends OxUnitTestCase
{
    public function test(): void
    {
        $report = new ExchangeSourceReport();

        $report->trace('test 1', 'this is a log', 1.5);
        $report->trace('test 1', 'this is a log', 2.5);

        // Traces are sorted by total duration: we are testing this first
        $report->trace('test 2', 'this is another log', 5);
        $report->trace('test 2', 'this is another log', 0.06);
        $report->trace('test 2', 'this is another log', 0.2);

        $i = 3;
        foreach ($report->toArray() as $sample => $trace) {
            $i--;

            $this->assertEquals("test {$i}", $trace['type']);

            if ($i === 1) {
                $this->assertEquals('this is a log', $sample);
                $this->assertEquals(4, $trace['duration']);
                $this->assertEquals(2, $trace['count']);
                $this->assertEquals(
                    [
                        2  => 0,
                        1  => 0,
                        0  => 2,
                        -1 => 0,
                        -2 => 0,
                        -3 => 0,
                        -4 => 0,
                        -5 => 0,
                        -6 => 0,
                    ],
                    $trace['distribution']
                );
            } else {
                $this->assertEquals('this is another log', $sample);
                $this->assertEquals(5.26, $trace['duration']);
                $this->assertEquals(3, $trace['count']);
                $this->assertEquals(
                    [
                        2  => 0,
                        1  => 0,
                        0  => 1,
                        -1 => 1,
                        -2 => 1,
                        -3 => 0,
                        -4 => 0,
                        -5 => 0,
                        -6 => 0,
                    ],
                    $trace['distribution']
                );
            }
        }
    }
}
