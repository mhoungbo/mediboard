<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit\Encryption;

use Ox\Core\CMbModelNotFoundException;
use Ox\Mediboard\Admin\CSourceLDAP;
use Ox\Mediboard\System\CEncryptedProperty;
use Ox\Tests\OxUnitTestCase;

class CEncryptedPropertyTest extends OxUnitTestCase
{
    private static ?string $password = null;

    private static ?CSourceLDAP $encrypted_source = null;

    public function testEncryptedPropertyIsStored(): void
    {
        self::$password = $password = uniqid('test', true);

        $source         = new CSourceLDAP();
        $source->name   = uniqid('test');
        $source->host   = 'ldap.test';
        $source->rootdn = 'DC=EXAMPLE,DC=COM';

        $source->_password = $password;

        // Storing is OK
        $this->assertNull($source->store());

        // Encrypted property is decrypted
        $this->assertEquals($password, $source->getPassword());

        // Ref is set
        $this->assertNotNull($source->password_id);

        // Checking that CEncryptedProperty exists
        CEncryptedProperty::findOrFail($source->password_id);

        // Form field is emptied
        $this->assertNull($source->_password);

        self::$encrypted_source = $source;
    }

    /**
     * @depends testEncryptedPropertyIsStored
     */
    public function testEncryptedObjectAfterLoad(): void
    {
        $source = CSourceLDAP::findOrFail(self::$encrypted_source->_id);

        $this->assertNotNull($source->password_id);
        $this->assertNull($source->_password);
        $this->assertEquals(self::$password, $source->getPassword());
    }

    /**
     * @depends testEncryptedObjectAfterLoad
     */
    public function testUpdatingEncryptedObjectProperty(): void
    {
        $source      = CSourceLDAP::findOrFail(self::$encrypted_source->_id);
        $password_id = $source->password_id;

        // Updating the password
        $source->_password = $new_password = uniqid('test', true);
        $this->assertNull($source->store());
        $this->assertEquals($password_id, $source->password_id);
        $this->assertEquals($new_password, $source->getPassword());
        $this->assertNull($source->_password);
    }

    /**
     * @depends testUpdatingEncryptedObjectProperty
     */
    public function testEmptyingEncryptedObjectProperty(): void
    {
        $source = CSourceLDAP::findOrFail(self::$encrypted_source->_id);

        $password = $source->getPassword();

        // We ensure that storing with form field no NULL does nothing
        $source->_password = null;
        $this->assertNull($source->store());
        $this->assertNotNull($source->password_id);
        $this->assertEquals($password, $source->getPassword());

        $password_id = $source->password_id;

        // Emptying the password
        $source->_password = '';
        $this->assertNull($source->store());
        $this->assertNull($source->password_id);
        $this->assertNull($source->getPassword());

        $this->expectException(CMbModelNotFoundException::class);
        CEncryptedProperty::findOrFail($password_id);
    }

    /**
     * @depends testEmptyingEncryptedObjectProperty
     */
    public function testDeletingObjectAlsoDeleteEncryptedProperties(): void
    {
        $source = CSourceLDAP::findOrFail(self::$encrypted_source->_id);

        // Setting a new password
        $source->_password = uniqid('test', true);
        $this->assertNull($source->store());

        $password_id = $source->password_id;

        // Deleting the object
        $this->assertNull($source->delete());
        $this->expectException(CMbModelNotFoundException::class);
        CEncryptedProperty::findOrFail($password_id);
    }

    /**
     * @depends testDeletingObjectAlsoDeleteEncryptedProperties
     */
    public function testPurgingObjectAlsoDeleteEncryptedProperties(): void
    {
        $source         = new CSourceLDAP();
        $source->name   = uniqid('test');
        $source->host   = 'ldap.test';
        $source->rootdn = 'DC=EXAMPLE,DC=COM';

        // Setting a new password
        $source->_password = uniqid('test', true);
        $this->assertNull($source->store());

        $password_id = $source->password_id;

        // Purging the object
        $this->assertNull($source->purge());
        $this->expectException(CMbModelNotFoundException::class);
        CEncryptedProperty::findOrFail($password_id);
    }
}
