<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit\Log;

use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CUserLog;
use Ox\Mediboard\System\Log\UserLogService;
use Ox\Tests\OxUnitTestCase;

class UserLogServiceTest extends OxUnitTestCase
{
    public function testRemoveForbiddenLogsWithAdmin(): void
    {
        $module = CModule::getInstalled('system');

        $current_user = CMediusers::get();
        $func         = $current_user->loadRefFunction();
        $actual_perm  = CPermModule::$users_cache[$current_user->_id][$module->_id]['view'] ?? null;
        try {
            CPermModule::$users_cache[$current_user->_id][$module->_id]['view'] = 2;

            $logs = $this->createLogs($current_user);
            $this->assertEquals($logs, (new UserLogService())->removeForbiddenLogs($logs, $func->group_id));
        } finally {
            if ($actual_perm === null) {
                unset(CPermModule::$users_cache[$current_user->_id]);
            } else {
                CPermModule::$users_cache[$current_user->_id][$module->_id]['view'] = $actual_perm;
            }
        }
    }

    /**
     * @config [static] dPetablissement general dossiers_medicaux_shared 1
     */
    public function testRemoveForbiddenLogsWithConfig(): void
    {
        $current_user = CMediusers::get();
        $func         = $current_user->loadRefFunction();

        $logs = $this->createLogs($current_user);
        $this->assertEquals($logs, (new UserLogService())->removeForbiddenLogs($logs, $func->group_id));
    }

    /**
     * @config [static] dPetablissement general dossiers_medicaux_shared 0
     */
    public function testRemoveForbiddenLogs(): void
    {
        $module = CModule::getInstalled('system');

        /** @var CMediusers $other_user */
        $other_user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $current_user = CMediusers::get();
        $func         = $current_user->loadRefFunction();
        $actual_perm  = CPermModule::$users_cache[$current_user->_id][$module->_id]['view'] ?? null;
        try {
            CPermModule::$users_cache[$current_user->_id][$module->_id]['view'] = 0;

            $logs = $this->createLogs($current_user, $other_user);
            $logs_after = (new UserLogService())->removeForbiddenLogs($logs, $func->group_id);
            unset($logs[2]);
            $this->assertEquals($logs, $logs_after);
        } finally {
            if ($actual_perm === null) {
                unset(CPermModule::$users_cache[$current_user->_id]);
            } else {
                CPermModule::$users_cache[$current_user->_id][$module->_id]['view'] = $actual_perm;
            }
        }
    }

    private function createLogs(CMediusers $current_user, CMediusers $other_user = null): array
    {
        $log1               = new CUserLog();
        $log1->_id          = 1;
        $log1->object_class = 'CMediusers';
        $log1->object_id    = 1;
        $log1->user_id      = $current_user->_id;

        $log2               = new CUserLog();
        $log2->_id          = 2;
        $log2->object_class = 'CUser';
        $log2->object_id    = 1;
        $log2->user_id      = $other_user ? $other_user->_id : null;

        return [2 => $log2, 1 => $log1];
    }
}
