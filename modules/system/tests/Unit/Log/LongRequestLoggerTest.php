<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit\Log;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppPerformance;
use Ox\Core\CClassMap;
use Ox\Core\Config\Conf;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\System\Controllers\SystemController;
use Ox\Mediboard\System\Log\LongRequestLogger;
use Ox\Tests\OxUnitTestCase;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LongRequestLoggerTest extends OxUnitTestCase
{
    /**
     * @dataProvider notSupportedProvider
     *
     * @param LongRequestLogger $logger
     *
     * @return void
     */
    public function testNotSupportedRequestIsNotLogged(LongRequestLogger $logger): void
    {
        $logger->logFromGlobals();
        $logger->logFromRequest(new Request());
        $logger->log(1, $this->createMock(CUser::class), false);
    }

    /**
     * @dataProvider invalidThresholdProvider
     *
     * @param LongRequestLogger $logger
     * @param CUser             $user
     *
     * @return void
     * @throws Exception
     */
    public function testInvalidThresholdConfigDoesNotPerformLogging(LongRequestLogger $logger, CUser $user): void
    {
        $logger->log(1, $user, false);
    }

    /**
     * @dataProvider validThresholdProvider
     *
     * @param LongRequestLogger $logger
     * @param CUser|null        $user
     *
     * @return void
     */
    public function testReachedThresholdTriggersLogging(LongRequestLogger $logger, ?CUser $user): void
    {
        $logger->log(1, $user, false);
    }

    /**
     * @dataProvider whitelistedProvider
     *
     * @param LongRequestLogger $logger
     * @param CUser|null        $user
     *
     * @return void
     */
    public function testWhitelistedTriggersLogging(LongRequestLogger $logger, ?CUser $user): void
    {
        $logger->log(1, $user, false);
    }

    /**
     * @dataProvider validProvider
     *
     * @param LongRequestLogger $logger
     * @param CUser|null        $user
     *
     * @return void
     */
    public function testLogIsBuiltCorrectly(LongRequestLogger $logger, ?CUser $user, bool $public = false): void
    {
        $log = $logger->log(1, $user, false);

        $this->assertNotNull($log);

        if ($public || ($user === null)) {
            $this->assertNull($log->user_id);
            $this->assertNull($log->session_id);
        } else {
            $this->assertEquals($user->_id, $log->user_id);
            $this->assertNotNull($log->session_id);
        }

        $this->assertNotEmpty($log->_session_data);
        $this->assertArrayNotHasKey('_security_main', $log->_session_data);
        $this->assertArrayNotHasKey('AppUi', $log->_session_data);
        $this->assertArrayNotHasKey('templateManager', $log->_session_data['dPcompteRendu']);
    }

    public function notSupportedProvider(): array
    {
        return [
            'app is readonly'             => [
                $this->mockLogger(
                    new Conf(),
                    $this->mockApp(['readonly' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
            ],
            'logging is not enabled'      => [
                $this->mockLogger(
                    $this->mockConf(['log' => false]),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
            ],
            'module action not ready'     => [
                $this->mockLogger(
                    new Conf(),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    [
                        'should_not_log' => true,
                        'module_action'  => false,
                    ]
                ),
            ],
            'no threshold configurations' => [
                $this->mockLogger(
                    $this->mockConf(['human_threshold' => false, 'bot_threshold' => false]),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
            ],
        ];
    }

    public function invalidThresholdProvider(): array
    {
        $bot      = $this->createMock(CUser::class);
        $bot->_id = 123;
        $bot->method('isRobot')->willReturn(true);

        $human      = $this->createMock(CUser::class);
        $human->_id = 123;
        $human->method('isRobot')->willReturn(false);

        return [
            'bot and empty threshold'      => [
                $this->mockLogger(
                    $this->mockConf(['bot_threshold' => '']),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $bot,
            ],
            'bot and null threshold'       => [
                $this->mockLogger(
                    $this->mockConf(['bot_threshold' => null]),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $bot,
            ],
            'bot and zero threshold'       => [
                $this->mockLogger(
                    $this->mockConf(['bot_threshold' => 0]),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $bot,
            ],
            'bot and negative threshold'   => [
                $this->mockLogger(
                    $this->mockConf(['bot_threshold' => -1]),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $bot,
            ],
            'bot and invalid threshold'    => [
                $this->mockLogger(
                    $this->mockConf(['bot_threshold' => 'invalid']),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $bot,
            ],
            'human and empty threshold'    => [
                $this->mockLogger(
                    $this->mockConf(['human_threshold' => '']),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $human,
            ],
            'human and null threshold'     => [
                $this->mockLogger(
                    $this->mockConf(['human_threshold' => null]),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $human,
            ],
            'human and zero threshold'     => [
                $this->mockLogger(
                    $this->mockConf(['human_threshold' => 0]),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $human,
            ],
            'human and negative threshold' => [
                $this->mockLogger(
                    $this->mockConf(['human_threshold' => -1]),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $human,
            ],
            'human and invalid threshold'  => [
                $this->mockLogger(
                    $this->mockConf(['human_threshold' => 'invalid']),
                    CApp::getInstance(),
                    CClassMap::getInstance(),
                    ['should_not_log' => true]
                ),
                $human,
            ],
        ];
    }

    public function validThresholdProvider(): array
    {
        $bot      = $this->createMock(CUser::class);
        $bot->_id = 123;
        $bot->method('isRobot')->willReturn(true);

        $human      = $this->createMock(CUser::class);
        $human->_id = 123;
        $human->method('isRobot')->willReturn(false);

        $anonymous = null;

        return [
            'bot'                                     => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '5',
                            'human_threshold' => '10',
                        ]
                    ),
                    $this->mockApp(['duration' => 6.0]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false]
                ),
                $bot,
            ],
            'human'                                   => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '5',
                        ]
                    ),
                    $this->mockApp(['duration' => 6.0]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false]
                ),
                $human,
            ],
            'anonymous should be considered as human' => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '5',
                        ]
                    ),
                    $this->mockApp(['duration' => 6.0, 'public' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false]
                ),
                $anonymous,
            ],
            'public should be considered as human'    => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '5',
                        ]
                    ),
                    $this->mockApp(['duration' => 6.0, 'public' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false]
                ),
                $bot,
            ],
        ];
    }

    public function whitelistedProvider(): array
    {
        $bot      = $this->createMock(CUser::class);
        $bot->_id = 132;
        $bot->method('isRobot')->willReturn(true);

        $human      = $this->createMock(CUser::class);
        $human->_id = 123;
        $human->method('isRobot')->willReturn(false);

        $anonymous = null;

        return [
            'bot'                                     => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '10',
                            'whitelist'       => 'system about 1',
                        ]
                    ),
                    $this->mockApp(['duration' => 1.0]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false]
                ),
                $bot,
            ],
            'human'                                   => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '10',
                            'whitelist'       => 'system about 1',
                        ]
                    ),
                    $this->mockApp(['duration' => 1.0]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false]
                ),
                $human,
            ],
            'anonymous should be considered as human' => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '10',
                            'whitelist'       => 'system about 1',
                        ]
                    ),
                    $this->mockApp(['duration' => 1.0, 'public' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false]
                ),
                $anonymous,
            ],
            'public should be considered as human'    => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '10',
                            'whitelist'       => 'system about 1',
                        ]
                    ),
                    $this->mockApp(['duration' => 1.0, 'public' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false]
                ),
                $bot,
            ],
        ];
    }

    public function validProvider(): array
    {
        $bot      = $this->createMock(CUser::class);
        $bot->_id = 123;
        $bot->method('isRobot')->willReturn(true);

        $human      = $this->createMock(CUser::class);
        $human->_id = 123;
        $human->method('isRobot')->willReturn(false);

        $anonymous = null;

        return [
            'threshold / bot'                                     => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '5',
                            'human_threshold' => '10',
                        ]
                    ),
                    $this->mockApp(['duration' => 6.0]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false, 'perform_log' => true]
                ),
                $bot,
            ],
            'threshold / human'                                   => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '5',
                        ]
                    ),
                    $this->mockApp(['duration' => 6.0]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false, 'perform_log' => true]
                ),
                $human,
            ],
            'threshold / anonymous should be considered as human' => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '5',
                        ]
                    ),
                    $this->mockApp(['duration' => 6.0, 'public' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false, 'perform_log' => true]
                ),
                $anonymous,
            ],
            'threshold / public should be considered as human'    => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '5',
                        ]
                    ),
                    $this->mockApp(['duration' => 6.0, 'public' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false, 'perform_log' => true]
                ),
                $bot,
                true,
            ],
            'whitelist / bot'                                     => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '10',
                            'whitelist'       => 'system about 1',
                        ]
                    ),
                    $this->mockApp(['duration' => 1.0]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false, 'perform_log' => true]
                ),
                $bot,
            ],
            'whitelist / human'                                   => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '10',
                            'whitelist'       => 'system about 1',
                        ]
                    ),
                    $this->mockApp(['duration' => 1.0]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false, 'perform_log' => true]
                ),
                $human,
            ],
            'whitelist / anonymous should be considered as human' => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '10',
                            'whitelist'       => 'system about 1',
                        ]
                    ),
                    $this->mockApp(['duration' => 1.0, 'public' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false, 'perform_log' => true]
                ),
                $anonymous,
            ],
            'whitelist / public should be considered as human'    => [
                $this->mockLogger(
                    $this->mockConf(
                        [
                            'bot_threshold'   => '10',
                            'human_threshold' => '10',
                            'whitelist'       => 'system about 1',
                        ]
                    ),
                    $this->mockApp(['duration' => 1.0, 'public' => true]),
                    CClassMap::getInstance(),
                    ['should_not_log' => false, 'perform_log' => true]
                ),
                $bot,
                true,
            ],
        ];
    }

    private function mockLogger(Conf $conf, CApp $app, CClassMap $map, array $options = []): LongRequestLogger
    {
        $perform_log = $options['perform_log'] ?? false;

        if ($options['should_not_log'] ?? false) {
            $logger = $this->getMockBuilder(LongRequestLogger::class)
                           ->setConstructorArgs([$conf, $app, $map, new NullLogger()])
                           ->onlyMethods(
                               [
                                   'getModuleFromGlobals',
                                   'getActionFromGlobals',
                                   'getControllerFromRequest',
                                   'getRouteFromRequest',
                                   'performLog',
                                   'isModuleActionReady',
                                   'isWhitelisted',
                               ]
                           )->getMock();

            $logger->method('isModuleActionReady')->willReturn($options['module_action'] ?? true);

            $logger->method('getModuleFromGlobals')->willReturn('system');
            $logger->method('getActionFromGlobals')->willReturn('about');
            $logger->method('getControllerFromRequest')->willReturn(SystemController::class . '::about');
            $logger->method('getRouteFromRequest')->willReturn('system_gui_about');

            // In order to theoretically enforce logging
            $logger->method('isWhitelisted')->willReturn(true);
            $logger->expects($this->never())->method('performLog');
        } else {
            $methods_to_mock = [
                'getModuleFromGlobals',
                'getActionFromGlobals',
                'getControllerFromRequest',
                'getRouteFromRequest',
                'performLog',
                'isModuleActionReady',
                'getModuleActionId',
                'getSessionId',
                'getSessionData',
            ];

            if ($perform_log) {
                unset($methods_to_mock[array_search('performLog', $methods_to_mock)]);
            }

            $logger = $this->getMockBuilder(LongRequestLogger::class)
                           ->setConstructorArgs([$conf, $app, $map, new NullLogger()])
                           ->onlyMethods($methods_to_mock)
                           ->getMock();

            $logger->method('isModuleActionReady')->willReturn($options['module_action'] ?? true);

            $logger->method('getModuleFromGlobals')->willReturn('system');
            $logger->method('getActionFromGlobals')->willReturn('about');
            $logger->method('getControllerFromRequest')->willReturn(SystemController::class . '::about');
            $logger->method('getRouteFromRequest')->willReturn('system_gui_about');
            $logger->method('getModuleActionId')->willReturn(1);

            $logger->method('getSessionId')->willReturn('1324');
            $logger->method('getSessionData')->willReturn(
                [
                    '_security_main' => 'something to remove',
                    '_csrf/login'    => 'csrf',
                    'AppUi'          => 'something to remove',
                    'dPcompteRendu'  => [
                        'templateManager' => 'something to remove',
                        'something'       => 'ok',
                    ],
                    'something else' => 'ok',
                ]
            );

            if (!$perform_log) {
                $logger->expects($this->once())->method('performLog');
            }
        }

        $logger->setResponse(new Response('foo'));

        return $logger;
    }

    private function mockConf(array $options): Conf
    {
        $log_access      = ($options['log']) ?? true;
        $human_threshold = ($options['human_threshold']) ?? null;
        $bot_threshold   = ($options['bot_threshold']) ?? null;
        $whitelist       = ($options['whitelist']) ?? '';

        $conf = $this->createMock(Conf::class);
        $conf->method('get')->willReturnMap(
            [
                ['log_access', $log_access],
                ['human_long_request_level', $human_threshold],
                ['bot_long_request_level', $bot_threshold],
                ['long_request_whitelist', $whitelist],
            ]
        );

        return $conf;
    }

    private function mockApp(array $options): CApp
    {
        $performance = new CAppPerformance();

        $app = $this->createMock(CApp::class);
        $app->method('isInReadOnlyState')->willReturn($options['readonly'] ?? false);
        $app->method('isPublic')->willReturn($options['public'] ?? false);
        $app->method('getPerformance')->willReturn($performance);
        $app->method('getRequestIdentifier')->willReturn('test');
        $app->method('getDuration')->willReturn($options['duration'] ?? 1.0);

        return $app;
    }
}
