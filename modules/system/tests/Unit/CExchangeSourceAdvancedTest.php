<?php

namespace Ox\Mediboard\System\Tests\Unit;

use Exception;
use Ox\Core\Chronometer;
use Ox\Core\CMbException;
use Ox\Interop\Eai\Resilience\ClientContext;
use Ox\Mediboard\System\CExchangeSourceAdvanced;
use Ox\Mediboard\System\CExchangeSourceStatistic;
use Ox\Mediboard\System\CSourceHTTP;
use Ox\Tests\OxUnitTestCase;

class CExchangeSourceAdvancedTest extends OxUnitTestCase
{
    /** @var CSourceHTTP */
    protected static $source;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$source = $source = CSourceHTTP::getSampleObject();
        $source->name = "Test : " . $source->name;
        if ($msg = $source->store()) {
            throw new CMbException($msg);
        }

        self::$source->_current_chronometer = new Chronometer();
    }

    protected function setUp(): void
    {
        parent::setUp();

        // reset before each test
        CExchangeSourceAdvanced::$elements_to_save_in_callback = [];
    }

    public function testOnlyUpdateFirstDateTimeCall(): void
    {
        $expected_host = self::$source->host;

        // remove attribute first_call_date
        if (self::$source->first_call_date) {
            self::$source->first_call_date = '';
            if ($msg = self::$source->store()) {
                throw new CMbException($msg);
            }
        }

        $this->assertNull(self::$source->first_call_date);

        self::$source->host = 'new path host (temp usage)';
        $context            = new ClientContext(self::$source->getClient(), self::$source);
        self::$source->onAfterRequest($context);

        $this->assertNotNull(self::$source->first_call_date);
        $this->assertNotEmpty(
            CExchangeSourceAdvanced::$elements_to_save_in_callback[self::$source->_guid][get_class(self::$source)]
        );

        self::$source->host = $expected_host;
    }

    public function testCreateNewStatsWithoutFailure(): void
    {
        $last_stats = self::$source->loadRefLastStatistic();
        $context    = new ClientContext(self::$source->getClient(), self::$source);
        self::$source->onAfterRequest($context);

        $new_statistics = self::$source->loadRefLastStatistic();
        $this->assertCreationOfNewStats($last_stats, $new_statistics);
        $this->assertEquals($new_statistics->last_status, CExchangeSourceStatistic::CONNEXION_STATUS_SUCCESS);
    }

    /**
     * @return void
     * @throws CMbException
     */
    public function testCreateNewStatsWithFailure(): void
    {
        $last_stats = self::$source->loadRefLastStatistic();
        $context    = new ClientContext(self::$source->getClient(), self::$source);
        $context->setThrowable(new Exception());
        self::$source->onAfterRequest($context);

        $new_statistics = self::$source->loadRefLastStatistic();
        $this->assertCreationOfNewStats($last_stats, $new_statistics);
        $this->assertEquals($new_statistics->last_status, CExchangeSourceStatistic::CONNEXION_STATUS_FAILED);
    }

    /**
     * @return void
     * @throws CMbException
     */
    public function testCreateNewStatsWithMultipleFailures(): void
    {
        $last_stats = self::$source->loadRefLastStatistic();
        $context    = new ClientContext(self::$source->getClient(), self::$source);
        $context->setResponse(false);

        // first call failure
        self::$source->onAfterRequest($context);
        $first_new_statistics = self::$source->loadRefLastStatistic();
        $this->assertCreationOfNewStats($last_stats, $first_new_statistics);
        $this->assertEquals($first_new_statistics->last_status, CExchangeSourceStatistic::CONNEXION_STATUS_FAILED);

        // second call failure
        $context = new ClientContext(self::$source->getClient(), self::$source);
        $context->setThrowable(new Exception());
        self::$source->onAfterRequest($context);

        $this->assertCount(
            2,
            CExchangeSourceAdvanced::$elements_to_save_in_callback[self::$source->_guid][CExchangeSourceStatistic::class]
        );

        $second_new_statistics = self::$source->loadRefLastStatistic();
        $this->assertCreationOfNewStats($first_new_statistics, $second_new_statistics);
        $this->assertEquals($second_new_statistics->last_status, CExchangeSourceStatistic::CONNEXION_STATUS_FAILED);
        $this->assertEquals($first_new_statistics->failures, $second_new_statistics->failures);
    }

    /**
     * @param CExchangeSourceStatistic|null $last
     * @param CExchangeSourceStatistic      $new
     *
     * @return void
     */
    private function assertCreationOfNewStats(?CExchangeSourceStatistic $last, CExchangeSourceStatistic $new): void
    {
        $this->assertSame(self::$source->_ref_last_statistics, $new);
        $this->assertNotSame($last, $new);
        $this->assertGreaterThan($last ? $last->nb_call : 0, $new->nb_call);
        $this->assertGreaterThanOrEqual(
            $last ? $last->last_verification_date : $new->last_verification_date,
            $new->last_verification_date
        );
    }

    public function testElementsStoredWhenCallbackWasTriggered(): void
    {
        $context = new ClientContext(self::$source->getClient(), self::$source);
        $context->setResponse(true);

        self::$source->onAfterRequest($context);
        $first_new_statistics = self::$source->loadRefLastStatistic();
        $this->assertNull($first_new_statistics->_id);

        self::$source::storeElementsInCallbacks();
        $this->assertNotNull($first_new_statistics->_id);
    }
}
