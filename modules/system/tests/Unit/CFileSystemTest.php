<?php
 
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit;

use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\Chronometer;
use Ox\Core\CMbException;
use Ox\Core\CMbPath;
use Ox\Core\Contracts\Client\FileSystemClientInterface;
use Ox\Interop\Eai\Resilience\ClientContext;
use Ox\Mediboard\System\CFileSystem;
use Ox\Mediboard\System\CSourceFileSystem;
use Ox\Mediboard\System\ResilienceFileSystemClient;
use Ox\Mediboard\System\Sources\ObjectPath;
use Ox\Tests\OxUnitTestCase;

class CFileSystemTest extends OxUnitTestCase
{
    /** @var CFileSystem */
    protected static $source;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        $source                 = new CSourceFileSystem();
        $source->loggable       = "0";
        $source->name           = 'test_TU_FileSystem';
        $source->host           = 'localhost';
        $source->active         = 1;
        $source->role           = CAppUI::conf('instance_role');
        $source->retry_strategy = "1|5 5|60 10|120 20|";

        if ($msg = $source->store()) {
            throw new CMbException($msg);
        }

        self::$source = $source;
    }

    /**
     * @return CFileSystem
     */
    public function testInit(): CFileSystem
    {
        $fs              = new CFileSystem();
        $exchange_source = self::$source;
        $fs->init($exchange_source);

        $this->assertInstanceOf(CSourceFileSystem::class, $exchange_source);

        return $fs;
    }

    /**
     * @return bool
     * @throws CMbException
     */
    public function testSend(): void
    {
        CMbPath::forceDir(CAppUI::getTmpPath("source"));
        $host_name             = CAppUI::getTmpPath("source");
        $exchange_source       = self::$source;
        $exchange_source->host = $host_name;
        $exchange_source->setData("test");
       
        $client = $exchange_source->getClient();
        $path   = $exchange_source->completePath("test.txt");
        $client->send($path);
        $this->assertFileExists($path);
    }

    /**
     * @return array
     * @throws CMbException
     */
    public function testReceive(): void
    {
        $source = self::$source;
        $fs     = $source->getClient();
        $res    = $fs->receive("/");
        $this->assertNotNull($res);
        $this->assertIsArray($res);
    }

    public function testCreateDirectory(): void
    {
        $dir_path = CAppUI::getTmpPath("source");
        CMbPath::forceDir($dir_path);
        $exchange_source          = self::$source;
        $exchange_source->_client = "";
        $exchange_source->host    = $dir_path;
        $exchange_source->setData("test");

        $client = $exchange_source->getClient();
        $client->createDirectory($exchange_source->completePath('directory_created')->toDirectory());
        $this->assertDirectoryExists($dir_path . "/directory_created");
    }

    public function testGetCurrentDirectory(): void
    {
        CMbPath::forceDir(CAppUI::getTmpPath("source"));
        $host_name             = CAppUI::getTmpPath("source");
        $exchange_source       = self::$source;
        $exchange_source->host = $host_name;
        $exchange_source->setData("test");
        $exchange_source->_client = "";
        $client                   = $exchange_source->getClient();
        $directory                = $client->getDirectory($exchange_source->completePath()->toDirectory());
        $this->assertDirectoryExists($directory);
    }

    public function testGetListFilesDetails(): void
    {
        $tmp_dir = CAppUI::getTmpPath("source_list_detail_" . uniqid());
        CMbPath::forceDir($tmp_dir);
        $host_name             = $tmp_dir;
        $exchange_source       = self::$source;
        $exchange_source->host = $host_name;
        $exchange_source->setData("test");
        $exchange_source->_client = "";
        $client                   = $exchange_source->getClient();
        $client->send($exchange_source->completePath("test0.txt"));
        $client->send($exchange_source->completePath("test1.txt"));
        $client->send($exchange_source->completePath("test2.txt"));
        $client->send($exchange_source->completePath("test3.txt"));
        $client->send($exchange_source->completePath("test4.txt"));

        $current_directory = $client->getDirectory($tmp_dir);
        $listfiles         = $client->getListFilesDetails($current_directory);

        $this->assertCount(5, $listfiles);
    }

    public function testGetListDirectory(): void
    {
        CMbPath::forceDir(CAppUI::getTmpPath("source"));
        $host_name             = CAppUI::getTmpPath("source");
        $exchange_source       = self::$source;
        $exchange_source->host = $host_name;
        $exchange_source->setData("test");
        $exchange_source->_client = "";
        $client                   = $exchange_source->getClient();
        $current_directory        = $client->getDirectory($exchange_source->completePath()->toDirectory());
        $listDirectory            = $client->getListDirectory($current_directory);

        $this->assertIsArray($listDirectory);
        $this->assertNotEmpty($listDirectory);
    }

    public function testAddFile(): void
    {
        $tmp_dir = CAppUI::getTmpPath("source_add_file_" . uniqid());
        CMbPath::forceDir($tmp_dir);
        $exchange_source       = self::$source;
        $exchange_source->host = $tmp_dir;
        $exchange_source->setData("test");
        $exchange_source->_client = null;
        $client                   = $exchange_source->getClient();

        $path_to_directory = $exchange_source->completePath()->toDirectory();
        $path_send = $path_to_directory->withFilePath("addedFile.txt");
        $client->send($path_send);
        $this->assertFileExists($path_send);

        $path_copy = $path_send->withFilePath("copy.txt");
        $client->addFile($path_send, $path_copy);
        $this->assertFileExists($path_copy);
        $this->assertStringStartsWith($path_to_directory, $path_copy);
    }

    public function testDelFile(): void
    {
        $tmp_dir = CAppUI::getTmpPath("source_del_file_" . uniqid());
        CMbPath::forceDir($tmp_dir);
        $exchange_source       = self::$source;
        $exchange_source->host = $tmp_dir;
        $exchange_source->setData('test');
        $exchange_source->_client = null;
        $client                   = $exchange_source->getClient();

        $name_file = "test_delfile.txt";
        $path      = $exchange_source->completePath($name_file);
        $client->send($path);
        $this->assertFileExists($path);

        $client->delFile($path);
        $this->assertFileDoesNotExist($path);
    }

    public function testGetData(): void
    {
        $tmp_dir = CAppUI::getTmpPath("source_get_data");
        CMbPath::forceDir($tmp_dir);
        $exchange_source       = self::$source;
        $exchange_source->host = $tmp_dir;
        $content               = "test " . uniqid();
        $exchange_source->setData($content);
        $exchange_source->_client = null;
        $client                   = $exchange_source->getClient();

        $path = $exchange_source->completePath("test_getdata.txt");
        $client->send($path);
        $this->assertFileExists($path);

        $data = $client->getData($path);
        $this->assertEquals($content, $data);
    }

    public function testRenameFile(): void
    {
        CMbPath::forceDir(CAppUI::getTmpPath("source"));
        $host_name             = CAppUI::getTmpPath("source");
        $exchange_source       = self::$source;
        $exchange_source->host = $host_name;
        $exchange_source->setData("test");
        $exchange_source->_client = "";

        $file_path     = $exchange_source->completePath('test.txt');
        $new_file_path = $file_path->withFilePath('renamed.txt');
        $client        = $exchange_source->getClient();
        $client->send($file_path);
        $client->renameFile($file_path, $new_file_path);

        $this->assertFileDoesNotExist($file_path);
        $this->assertFileExists($new_file_path);
    }

    public function testCallLoggable(): void
    {
        CApp::$chrono = new Chronometer();
        CApp::$chrono->start();

        $mock = $this->getMockBuilder(CFileSystem::class)
                     ->setMethods(['_connect'])
                     ->getMock();

        $mock->method('_connect')->willReturn(true);

        $exchange_source = self::$source;

        $mock->init($exchange_source);

        $this->assertTrue($mock->_connect());
    }

    public function testGenerateNameFile(): void
    {
        $res   = (new CSourceFileSystem())->generateFileName();
        $regex = "/(\d.+\_\d.+)/";
        $this->assertTrue((bool)preg_match($regex, $res));
    }

    public function testUpdateFormField(): void
    {
        $source       = self::$source;
        $source->host = "tmp/out/";

        $this->assertNotEmpty($source->_view);

        $source->updateFormFields();

        $this->assertNotNull($source->_view);
        $this->assertIsString($source->_view);
    }

    public function testGetClient(): void
    {
        $source = $this->getMockBuilder(CSourceFileSystem::class)->getMock();
        $client = $this->getMockBuilder(FileSystemClientInterface::class)->getMock();
        $source->method('getClient')->willReturn($client);
        $this->assertInstanceOf(FileSystemClientInterface::class, $client);
    }

    public function testGetClientCache(): void
    {
        $source          = self::$source;
        $source->_client = "";
        $client          = $source->getClient();
        $this->assertSame($client, $source->getClient());
    }

    public function testGetClientRetryable(): void
    {
        $source          = self::$source;
        $source->_client = "";
        $client          = $source->getClient();
        $this->assertInstanceOf(ResilienceFileSystemClient::class, $client);
    }

    public function testGetClientOx(): void
    {
        $source                 = self::$source;
        $source->retry_strategy = "";
        $source->_client        = "";
        $client                 = $source->getClient();
        $this->assertInstanceOf(CFileSystem::class, $client);
    }

    public function testOnBeforeRequestIsNotLoggable(): void
    {
        $source         = self::$source;
        $client         = $source->getClient();
        $client_context = new ClientContext($client, $source);
        $source->_dispatcher->dispatch($client_context, $client::EVENT_BEFORE_REQUEST);
        $this->assertNull($source->_current_echange);
    }

    public function testOnBeforeRequest(): void
    {
        $source           = self::$source;
        $source->_client  = "";
        $source->loggable = "1";
        $client           = $source->getClient();

        $client_context = new ClientContext($client, $source);

        $source->_dispatcher->dispatch($client_context, $client::EVENT_BEFORE_REQUEST);

        $this->assertNotNull($source->_current_echange);

        $this->assertNotNull($source->_current_echange->date_echange);
        $this->assertIsString($source->_current_echange->date_echange);

        $this->assertNotNull($source->_current_echange->destinataire);
        $this->assertIsString($source->_current_echange->destinataire);

        $this->assertNotNull($source->_current_echange->source_id);
        $this->assertIsInt($source->_current_echange->source_id);
    }

    public function testOnAfterRequest(): void
    {
        $source                 = self::$source;
        $source->loggable       = "1";
        $source->retry_strategy = "1|5 5|60 10|120 20|";
        $source->host           = "/";
        $client                 = $source->getClient();

        $client_context = new ClientContext($client, $source);

        $source->_dispatcher->dispatch($client_context, $client::EVENT_BEFORE_REQUEST);
        $source->_dispatcher->dispatch($client_context, $client::EVENT_AFTER_REQUEST);

        $this->assertNotNull($source->_current_echange);

        $this->assertNotNull($source->_current_echange->date_echange);
        $this->assertIsString($source->_current_echange->date_echange);

        $this->assertNotNull($source->_current_echange->destinataire);
        $this->assertIsString($source->_current_echange->destinataire);

        $this->assertNotNull($source->_current_echange->source_id);
        $this->assertIsInt($source->_current_echange->source_id);

        $this->assertNotNull($source->_current_echange->response_time);
        $this->assertIsFloat($source->_current_echange->response_time);
        $this->assertGreaterThan(0, $source->_current_echange->response_time);

        $this->assertNotNull($source->_current_echange->response_datetime);
        $this->assertIsString($source->_current_echange->response_datetime);

        if ($source->_current_echange->output !== null) {
            $this->assertIsString($source->_current_echange->output);
        } else {
            $this->assertNull($source->_current_echange->output);
        }
    }

    public function testOnException(): void
    {
        $source         = self::$source;
        $source->host   = "/";
        $client         = $source->getClient();
        $client_context = new ClientContext($client, $source);
        $client_context->setResponse("test methode onException");

        $source->_dispatcher->dispatch($client_context, $client::EVENT_BEFORE_REQUEST);
        $source->_dispatcher->dispatch($client_context, $client::EVENT_EXCEPTION);

        $this->assertNotNull($source->_current_echange);

        $this->assertNotNull($source->_current_echange->response_datetime);
        $this->assertIsString($source->_current_echange->response_datetime);

        $this->assertNotNull($source->_current_echange->output);
        $this->assertIsString($source->_current_echange->output);
    }
}
