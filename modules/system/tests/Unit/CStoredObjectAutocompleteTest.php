<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Test\Unit;

use Ox\Core\Cache;
use Ox\Core\CMbException;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CStoredObjectAutocomplete;
use Ox\Tests\OxUnitTestCase;

class CStoredObjectAutocompleteTest extends OxUnitTestCase
{
    public function testConstructFailWithNonCStoredObject(): void
    {
        $this->expectExceptionObject(
            new CMbException(
                'CStoredObjectAutocomplete-Error-Class must be a child of CStoredObject',
                'CStoredObjectAutocomplete'
            )
        );

        new CStoredObjectAutocomplete('CStoredObjectAutocomplete', 'foo');
    }

    public function testSearchWithTooShortKeyword(): void
    {
        $autocomplete = new CStoredObjectAutocomplete(
            CUser::class,
            'user_last_name',
            null,
            [CStoredObjectAutocomplete::OPTION_MIN_LENGTH => 2]
        );

        $this->assertEquals([], $autocomplete->search('a'));
    }

    public function testSearchCacheablePutInCache(): void
    {
        $autocomplete = $this->getMockBuilder(CStoredObjectAutocomplete::class)
                             ->onlyMethods(['loadListForCache'])
                             ->setConstructorArgs(
                                 [
                                     CUser::class,
                                     'user_last_name',
                                     null,
                                     [
                                         CStoredObjectAutocomplete::OPTION_MIN_OCCURRENCES => 2,
                                         CStoredObjectAutocomplete::OPTION_WHOLE_STRING    => true,
                                     ],
                                 ]
                             )->getMock();

        // Avoid making SQL query
        $autocomplete->method('loadListForCache')->willReturn([]);
        // Ensure the loadList method is only called once, this mean the second time we call search the cache is used.
        $autocomplete->expects($this->once())->method('loadListForCache');

        $key   = 'Autocomplete-CUser-user_last_name-2';
        $cache = Cache::getCache(Cache::OUTER | Cache::DISTR);

        // Ensure no remaining key exists
        if ($cache->has($key)) {
            $cache->delete($key);
        }

        try {
            $this->assertNull($cache->get($key));

            $this->assertEquals([], $autocomplete->search('foo'));
            $this->assertEquals([], $cache->get($key));
            $this->assertEquals([], $autocomplete->search('foo'));
        } finally {
            if ($cache->has($key)) {
                $cache->delete($key);
            }
        }
    }

    public function testSearchCacheableDoesNotPutInCache(): void
    {
        $autocomplete = $this->getMockBuilder(CStoredObjectAutocomplete::class)
                             ->onlyMethods(['loadList'])
                             ->setConstructorArgs([CUser::class, 'user_last_name'])
                             ->getMock();
        // Avoid making SQL query
        $autocomplete->method('loadList')->willReturn([]);
        // Ensure the loadList method is called twice, this mean the cache is not used.
        $autocomplete->expects($this->exactly(2))->method('loadList');

        $key   = 'Autocomplete-CUser-user_last_name-1';
        $cache = Cache::getCache(Cache::OUTER | Cache::DISTR);

        $this->assertNull($cache->get($key));
        $this->assertEquals([], $autocomplete->search('bar'));
        $this->assertNull($cache->get($key));
        $this->assertEquals([], $autocomplete->search('bar'));
        $this->assertNull($cache->get($key));
    }

    /**
     * @dataProvider makeRefSearchProvider
     */
    public function testMakeRefSearchWithNoViewFieldThrowException(CStoredObjectAutocomplete $autocomplete): void
    {
        $this->expectExceptionObject(
            new CMbException('CStoredObjectAutocomplete-Error-View field cannot be null for ref search')
        );

        $this->invokePrivateMethod($autocomplete, 'makeRefSearch', '', '');
    }

    public function makeRefSearchProvider(): array
    {
        return [
            'view_field is null'  => [new CStoredObjectAutocomplete(CUser::class, 'user_last_name')],
            'view_field is empty' => [new CStoredObjectAutocomplete(CUser::class, 'user_last_name', '')],
        ];
    }

    public function testSearchRefTemplateIsNotNull(): void
    {
        $autocomplete = $this->getMockBuilder(CStoredObjectAutocomplete::class)
                             ->onlyMethods(['getAutocompleteList'])
                             ->setConstructorArgs([CMediusers::class, 'discipline_id', '_view'])
                             ->getMock();
        $autocomplete->method('getAutocompleteList')->willReturn([]);

        $this->assertNull($autocomplete->getTemplate());

        $this->invokePrivateMethod($autocomplete, 'makeRefSearch', '', '');

        $this->assertNotNull($autocomplete->getTemplate());
    }

    /**
     * @dataProvider isSearchCacheableProvider
     */
    public function testIsSearchCacheable(
        CStoredObjectAutocomplete $autocomplete,
        string                    $search,
        bool                      $expected_result
    ): void {
        $this->assertEquals($expected_result, $this->invokePrivateMethod($autocomplete, 'isSearchCacheable', $search));
    }

    public function isSearchCacheableProvider(): array
    {
        $autocomplete_with_where = new CStoredObjectAutocomplete(CUser::class, 'user_last_name');
        $autocomplete_with_where->addWhere(['foo' => 'bar']);

        $autocomplete_with_min_occurence = new CStoredObjectAutocomplete(
            CUser::class,
            'user_last_name',
            null,
            [CStoredObjectAutocomplete::OPTION_MIN_OCCURRENCES => 1]
        );

        $autocomplete_without_whole_string = new CStoredObjectAutocomplete(
            CUser::class,
            'user_last_name'
        );

        $autocomplete_cacheable = new CStoredObjectAutocomplete(
            CUser::class,
            'user_last_name',
            null,
            [
                CStoredObjectAutocomplete::OPTION_MIN_OCCURRENCES => 2,
                CStoredObjectAutocomplete::OPTION_WHOLE_STRING    => true,
            ]
        );

        return [
            'where already set'            => [$autocomplete_with_where, 'foo', false],
            'min_occurence is less than 2' => [$autocomplete_with_min_occurence, 'foo', false],
            'not whole_string'             => [$autocomplete_without_whole_string, 'foo', false],
            'search is cacheable'          => [$autocomplete_cacheable, 'foo', true],
        ];
    }
}
