<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit\ViewSender;

use Ox\Core\CApp;
use Ox\Core\CMbDT;
use Ox\Mediboard\System\ViewSender\CViewSender;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use ReflectionException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CViewSenderTest extends OxUnitTestCase
{
    private array $files = [];

    public function tearDown(): void
    {
        parent::tearDown();

        foreach ($this->files as $_file_path) {
            if (file_exists($_file_path)) {
                unlink($_file_path);
            }
        }
    }

    public function testCanDeleteFile(): void
    {
        // Cannot use data provider because of time check
        $datas = [
            [CMbDT::dateTime(), '1234abcd', '1234abcd', true],
            [CMbDT::dateTime('-1 HOUR'), uniqid(), uniqid(), true],
            [CMbDT::dateTime('-1 HOUR'), '1234abcd', '1234abcd', true],
            [CMbDT::dateTime(), uniqid(), uniqid(), false],
        ];

        foreach ($datas as [$file_date, $unique, $static_unique, $expected]) {
            $sender = new CViewSender();
            $sender->setForceUniqueId($static_unique);
            $this->assertEquals($expected, $sender->canDeleteFile($file_date, $unique));
        }
    }

    public function testAddTempFileDoesNotExists(): void
    {
        $sender = new CViewSender();
        $this->invokePrivateMethod($sender, 'addTempFile', 'file_path');

        $files = ($sender->getRemainingFilesCache())->get();

        $this->assertNotEmpty($files);
        $this->assertArrayHasKey('file_path', $files);
        $this->assertEquals($sender->getUniqueId(), $files['file_path'][1]);
    }

    public function testAddTempFileReplace(): void
    {
        $sender = new CViewSender();
        $this->invokePrivateMethod($sender, 'addTempFile', 'file_path');

        $new_unique = uniqid();
        $sender->setForceUniqueId($new_unique);
        $this->invokePrivateMethod($sender, 'addTempFile', 'file_path');

        $files = ($sender->getRemainingFilesCache())->get();
        $this->assertEquals($new_unique, $files['file_path'][1]);
    }

    public function testClearRemainingFiles(): void
    {
        $sender   = new CViewSender();
        $temp_dir = dirname(__DIR__, 5) . '/tmp';

        $this->files[] = $still_existing = tempnam($temp_dir, 'test');
        $this->files[] = $test1 = tempnam($temp_dir, 'test');
        $this->files[] = $test2 = tempnam($temp_dir, 'test');
        $non_exists    = uniqid();

        $this->assertFileExists($still_existing);
        $this->assertFileExists($test1);
        $this->assertFileExists($test2);
        $this->assertFileDoesNotExist($non_exists);

        $this->invokePrivateMethod($sender, 'addTempFile', $still_existing);

        $sender->setForceUniqueId(uniqid());

        $this->invokePrivateMethod($sender, 'addTempFile', $test1);
        $this->invokePrivateMethod($sender, 'addTempFile', $test2);
        $this->invokePrivateMethod($sender, 'addTempFile', $non_exists);

        CViewSender::clearRemainingFiles();

        $this->assertFileDoesNotExist($test1);
        $this->assertFileDoesNotExist($test2);
        $files = ($sender->getRemainingFilesCache())->get();

        $this->assertArrayNotHasKey($test1, $files);
        $this->assertArrayNotHasKey($test2, $files);
        $this->assertArrayNotHasKey($non_exists, $files);
        $this->assertArrayHasKey($still_existing, $files);
        $this->assertFileExists($still_existing);
    }

    public function testCheck(): void
    {
        $sender             = new CViewSender();
        $sender->name       = 'test';
        $sender->route_name = 'system_gui_about';
        $sender->every      = 1;
        $sender->period     = CViewSender::MINUTES;

        $this->assertEquals('', $sender->check());
    }

    public function testCheckInvalidPeriod(): void
    {
        $sender = new CViewSender();
        $this->assertEquals('CViewSender-Error-Either params or route name must no be null', $sender->check());
    }

    public function testCheckNoParamOrRoute(): void
    {
        $sender         = new CViewSender();
        $sender->params = 'foo';
        $sender->every  = 2;
        $sender->period = CViewSender::HOURS;

        $this->assertEquals('CViewSender-failed-every-period-constraint', $sender->check());
    }

    public function testCheckInvalidRouteName(): void
    {
        $route_name = uniqid();

        $sender             = new CViewSender();
        $sender->route_name = $route_name;
        $sender->every      = 1;
        $sender->period     = CViewSender::MINUTES;

        $this->assertEquals(
            "Unable to generate a URL for the named route \"$route_name\" as such route does not exist.",
            $sender->check()
        );
    }

    /**
     * @dataProvider makeUrlProvider
     */
    public function testMakeUrl(CViewSender $sender, string $url): void
    {
        $this->assertStringEndsWith($url, $sender->makeUrl(false));
    }

    public function testMakeUrlRaw(): void
    {
        $sender         = new CViewSender();
        $sender->params = 'raw=bar';
        $sender->makeUrl(false);

        $this->assertEquals('csv', $this->getPrivateProperty($sender, '_file_extension'));
    }

    public function makeUrlProvider(): array
    {
        $gui_base_url = CApp::generateUrl('system_gui_about', [], UrlGeneratorInterface::RELATIVE_PATH);

        $multipart_route             = new CViewSender();
        $multipart_route->route_name = 'system_gui_about';
        $multipart_route->params     = 'foo=bar';
        $multipart_route->multipart  = 1;

        $multipart_legacy            = new CViewSender();
        $multipart_legacy->params    = "m=system\nbar=foo";
        $multipart_legacy->multipart = 1;

        $normal_route             = new CViewSender();
        $normal_route->route_name = 'system_gui_about';

        $normal_legacy         = new CViewSender();
        $normal_legacy->params = "m=system";

        return [
            'multipart_route'  => [$multipart_route, $gui_base_url . '?foo=bar&suppressHeaders=1&multipart=1'],
            'multipart_legacy' => [
                $multipart_legacy,
                '?m=system&bar=foo&suppressHeaders=1&multipart=1',
            ],
            'normal_route'     => [$normal_route, $gui_base_url . '?dialog=1&_aio=0'],
            'normal_legacy'    => [$normal_legacy, '?m=system&dialog=1&_aio=0'],
        ];
    }

    public function providerSanitizeSenderName(): array
    {
        return [
            ['test', 'test'],
            ['test/', 'test/'],
            ['test/toto', 'test/toto'],
            ['test-titi', 'test_titi'],
            ['test-titi/toto', 'test_titi/toto'],
            ['test-titi/t"t�', 'test_titi/t_t_'],
            ['test45/4-3', 'test45/4_3'],
            ['te\'st', 'te_st'],
            ['t�est', 't_est'],
            ['t��_est', 't___est'],
            ['t�~_\est', 't____est'],
        ];
    }

    /**
     * @dataProvider providerSanitizeSenderName
     *
     * @param string $name
     * @param string $expected_name
     *
     * @return void
     * @throws TestsException
     * @throws ReflectionException
     */
    public function testSanitizeSenderName(string $name, string $expected_name): void
    {
        $sender = new CViewSender();

        $actual_name = $this->invokePrivateMethod($sender, "sanitizeName", $name);

        $this->assertEquals($actual_name, $expected_name);
    }

    public function testNameIsSanitizeWhenCreation(): void
    {
        $sender = $this->getMockBuilder(CViewSender::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['sanitizeName', 'check', "updatePlainFields"])
            ->getMock();
        $sender->method('check')->willReturn('this value do stop the store of object');

        $sender->expects($this->exactly(1))->method('sanitizeName');

        $sender->name = "name value";
        $sender->store();
    }

    public function testNameIsSanitizeWhenNameModified(): void
    {
        $sender = $this->getMockBuilder(CViewSender::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['sanitizeName', 'check', "updatePlainFields", 'fieldModified'])
            ->getMock();
        $sender->method('check')->willReturn('this value do stop the store of object');
        $sender->method('fieldModified')->willReturn(true);

        $sender->expects($this->exactly(1))->method('sanitizeName');

        $sender->_id  = "999999";
        $sender->name = "sender name modified";
        $sender->store();
    }

    public function testNameIsNotSanitizeWhenNameIsNotModified(): void
    {
        $sender = $this->getMockBuilder(CViewSender::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['sanitizeName', 'check', 'fieldModified', "updatePlainFields"])
            ->getMock();
        $sender->method('check')->willReturn('this value do stop the store of object');
        $sender->method('fieldModified')->willReturn(false);

        $sender->_id  = 999999;
        $sender->name = 'non name modified';
        $sender->store();

        $sender->expects($this->never())->method('sanitizeName');
    }
}
