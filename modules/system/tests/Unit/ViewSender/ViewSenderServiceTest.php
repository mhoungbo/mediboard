<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit\ViewSender;

use Ox\Mediboard\System\ViewSender\CViewSender;
use Ox\Mediboard\System\ViewSender\ViewSenderService;
use Ox\Tests\OxUnitTestCase;

class ViewSenderServiceTest extends OxUnitTestCase
{
    public function testComputeHourlyReport(): void
    {
        $sender_1 = new CViewSender();
        $sender_1->active = 1;
        $sender_1->_hour_plan = array_fill_keys(range(0, 59), 60);

        $sender_2 = new CViewSender();
        $sender_2->active = 1;
        $sender_2->_hour_plan = array_fill_keys(range(0, 59), 120);

        $sender_3 = new CViewSender();
        $sender_3->active = 0;
        $sender_3->_hour_plan = array_fill_keys(range(0, 59), 120);

        ['hour_sum' => $hour_sum, 'total' => $total] = (new ViewSenderService())->computeHourlyReport([$sender_1, $sender_2, $sender_3]);

        $this->assertEquals(180, $total);
        $this->assertEquals(array_fill_keys(range(0, 59), 180), $hour_sum);
    }

    /**
     * @dataProvider getRelativeUrlProvider
     *
     * @param string $url
     * @param string $expected
     *
     * @return void
     */
    public function testGetRelativeUrl(string $url, string $expected): void
    {
        $this->assertEquals($expected, (new ViewSenderService())->getRelativeUri($url));
    }

    public function getRelativeUrlProvider(): array
    {
        return [
            'legacy_url' => ['http://127.0.0.1/foo/?foo=bar', '/foo/'],
            'gui_url'    => ['http://127.0.0.1/foo/gui/bar/test', '/gui/bar/test'],
            'api_url'    => ['http://127.0.0.1/api/foo/bar/', '/api/foo/bar/'],
            'token_url'    => ['http://127.0.0.1/token/azertyuiop/', '/token/azertyuiop/'],
        ];
    }
}
