<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit;

use Ox\Core\CMbDT;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CMessage;
use Ox\Tests\OxUnitTestCase;

class CMessageTest extends OxUnitTestCase
{
    /**
     * @dataProvider buildDateConditionProvider
     */
    public function testBuildDateCondition(string $datetime, string $status, array $expected): void
    {
        $message = new CMessage();
        $this->assertEquals(
            $expected,
            $this->invokePrivateMethod($message, 'buildDateCondition', $status, $datetime)
        );
    }

    public function testLoadPublicationsForUser(): void
    {
        $system       = CModule::getInstalled('system');
        $current_user = CMediusers::get();

        $message_all_modules = $this->buildMessage();
        $message_system      = $this->buildMessage($system->_id);

        try {
            $messages = $message_all_modules->loadPublicationsForUser(
                $current_user->_id,
                'dPdeveloppement',
                CGroups::loadCurrent()->_id,
            );

            // The message is returned
            $this->assertArrayHasKey($message_all_modules->_id, $messages);
            $this->assertArrayNotHasKey($message_system->_id, $messages);

            $messages = $message_all_modules->loadPublicationsForUser(
                $current_user->_id,
                'system',
                CGroups::loadCurrent()->_id,
            );

            $this->assertArrayHasKey($message_all_modules->_id, $messages);
            $this->assertArrayHasKey($message_system->_id, $messages);

            $ack           = $messages[$message_all_modules->_id]->_ref_current_ack;
            $ack->date_ack = CMbDT::dateTime();
            $this->storeOrFailed($ack);

            $messages = $message_all_modules->loadPublicationsForUser(
                $current_user->_id,
                'invalid_module',
                CGroups::loadCurrent()->_id,
            );

            $this->assertArrayNotHasKey($message_all_modules->_id, $messages);
            $this->assertArrayHasKey($message_system->_id, $messages);
        } finally {
            // Ensure to remove the created message
            $message_all_modules->purge();
            $message_system->purge();
        }
    }

    public function buildDateConditionProvider(): array
    {
        $now = CMbDT::dateTime();

        return [
            'past'    => [$now, CMessage::MESSAGE_STATUS_PAST, ['fin' => "< '$now'"]],
            'present' => [$now, CMessage::MESSAGE_STATUS_PRESENT, ["'$now' BETWEEN deb AND fin"]],
            'future'  => [$now, CMessage::MESSAGE_STATUS_FUTURE, ['deb' => "> '$now'"]],
            'invalid' => [$now, 'foo', []],
        ];
    }

    private function buildMessage(int $mod_id = null): CMessage
    {
        $message        = new CMessage();
        $message->deb   = CMbDT::dateTime('-1 MIN');
        $message->fin   = CMbDT::dateTime('+2 MIN');
        $message->titre = 'Unit test';
        $message->corps = 'Unit test corps';

        if ($mod_id) {
            $message->module_id = $mod_id;
        }

        $this->storeOrFailed($message);

        return $message;
    }
}
