<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Unit\Code;

use InvalidArgumentException;
use Ox\Core\CClassMap;
use Ox\Mediboard\System\Code\Code;
use Ox\Mediboard\System\Code\CodeInterface;
use Ox\Mediboard\System\Code\CodeRepository;
use Ox\Mediboard\System\Code\CodeRepositoryInterface;
use Ox\Tests\OxUnitTestCase;
use stdClass;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class CodeRepositoryTest extends OxUnitTestCase
{
    public function testConstructThrowException(): void
    {
        $repositories = [new stdClass()];

        $this->expectExceptionObject(
            new InvalidArgumentException('Expected an object of type CodeRepositoryInterface, got stdClass')
        );

        new CodeRepository(CClassMap::getInstance(), [], $repositories);
    }

    public function testFindWithSelectedRepositories(): void
    {
        $code1 = new Code('type1', 'code1', 'description1');
        $code2 = new Code('type2', 'code2', 'description2');
        $code3 = new Code('type3', 'code3', 'description3');

        $repo1 = $this->buildCodeRepositoryInterface('repo1', 'system', [$code1, $code2]);
        $repo2 = $this->buildCodeRepositoryInterface('repo2', 'system', [$code3]);

        $repository = new CodeRepository(CClassMap::getInstance(), [], [$repo1, $repo2]);
        $this->assertEquals(
            [$code1, $code2, $code3],
            $repository->find('foo')
        );
    }

    public function testFindWithDefaultRepositories(): void
    {
        $class_map = $this->getMockBuilder(CClassMap::class)
                          ->onlyMethods(['getClassChildren'])
                          ->disableOriginalConstructor()
                          ->getMock();
        $class_map->method('getClassChildren')->willReturn(['repo1', 'repo2', 'repo3']);

        $code1 = new Code('type1', 'code1', 'description1');
        $code2 = new Code('type2', 'code2', 'description2');
        $code3 = new Code('type3', 'code3', 'description3');
        $code4 = new Code('type4', 'code4', 'description4');
        $code5 = new Code('type5', 'code5', 'description5');

        $repo1 = $this->buildCodeRepositoryInterface('repo1', 'system', [$code1, $code2]);
        $repo2 = $this->buildCodeRepositoryInterface('repo2', 'system', [$code3]);
        $repo3 = $this->buildCodeRepositoryInterface('repo3', 'foobar', [$code4, $code5]);

        $repository = $this->getMockBuilder(CodeRepository::class)
                           ->onlyMethods(['createInstance'])
                           ->setConstructorArgs([$class_map, ['repo1', 'repo3']])
                           ->getMock();

        $repository->method('createInstance')->willReturnMap(
            [
                ['repo1', $repo1],
                ['repo2', $repo2],
                ['repo3', $repo3],
            ]
        );

        $this->invokePrivateMethod($repository, 'setRepositories', ['repo1', 'repo3']);
        $this->assertEquals([$code1, $code2], $repository->find('foo'));
    }

    private function buildCodeRepositoryInterface(string $type, string $module, array $codes): CodeRepositoryInterface
    {
        $repo = $this->getMockBuilder(CodeRepositoryInterface::class)->getMock();
        $repo->method('getType')->willReturn($type);
        $repo->method('getModuleName')->willReturn($module);
        $repo->method('find')->willReturn($codes);

        return $repo;
    }
}
