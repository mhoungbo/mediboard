<?php

namespace Ox\Mediboard\System\Tests\Fixtures;

use Ox\Core\CMbDT;
use Ox\Mediboard\System\CNote;
use Ox\Tests\Fixtures\Fixtures;

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
class NoteFixtures extends Fixtures
{
    public const USER_NOTE_FIXTURES = 'note_user';

    public function load()
    {
        $this->createUserWithNotes(2, self::USER_NOTE_FIXTURES);

        if ($this->isFullMode()) {
            for ($i = 0; $i < 10; $i++) {
                $this->createUserWithNotes(rand(0, 10));
            }
        }
    }

    private function createUserWithNotes(int $note_count, string $tag = null): void
    {
        $user = $this->getUser($tag === null);
        if ($tag) {
            $this->store($user, $tag);
        }

        for ($i = 0; $i < $note_count; $i++) {
            $note               = new CNote();
            $note->user_id      = $user->_id;
            $note->date         = CMbDT::dateTime();
            $note->text         = $i .'-Fixtrue ' . uniqid() . "\nText de note !";
            $note->object_class = $user->_class;
            $note->object_id    = $user->_id;
            // Create the last note private
            $note->public = $i % $note_count ? '0' : '1';
            $this->store($note, $tag ? $tag . $i : null);
        }
    }
}
