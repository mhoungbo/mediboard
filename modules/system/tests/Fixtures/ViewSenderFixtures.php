<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Fixtures;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\ViewSender\CViewSender;
use Ox\Mediboard\System\ViewSender\CViewSenderSource;
use Ox\Tests\Fixtures\Fixtures;

class ViewSenderFixtures extends Fixtures
{
    public const SENDER_TAG = 'sender_tag';
    public const SOURCE_TAG = 'source_tag';

    public function load()
    {
        $sender               = new CViewSender();
        $sender->name         = 'Fixtures_sender';
        $sender->active       = 1;
        $sender->description  = 'Sender from fixtures';
        $sender->params       = 'foo=bar';
        $sender->multipart    = 0;
        $sender->period       = 1;
        $sender->offset       = 0;
        $sender->every        = 1;
        $sender->max_archives = 1;
        $this->store($sender, self::SENDER_TAG);

        $source           = new CViewSenderSource();
        $source->name     = 'Fixtures source';
        $source->actif    = 1;
        $source->group_id = CGroups::loadCurrent()->_id;
        $source->archive = 0;
        $this->store($source, self::SOURCE_TAG);
    }
}
