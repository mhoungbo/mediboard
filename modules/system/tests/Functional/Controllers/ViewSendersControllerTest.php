<?php

/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\Tests\Fixtures\ViewSenderFixtures;
use Ox\Mediboard\System\ViewSender\CSourceToViewSender;
use Ox\Mediboard\System\ViewSender\CViewSender;
use Ox\Mediboard\System\ViewSender\CViewSenderSource;
use Ox\Mediboard\System\ViewSender\ViewSenderRepository;
use Ox\Mediboard\System\ViewSender\ViewSenderService;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class ViewSendersControllerTest extends OxWebTestCase
{
    /**
     * @throws TestsException
     * @throws CMbException
     */
    public function testListViewSenders(): void
    {
        $this->createView();

        $client = static::createClient();
        $client->request(
            'GET',
            "/api/system/viewSenders",
        );

        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);

        $this->assertEquals(CViewSender::RESOURCE_TYPE, $collection->getFirstItem()->getType());
        $this->assertTrue($collection->hasLink('self'));
        $this->assertTrue($collection->hasLink('first'));
        $this->assertTrue($collection->hasLink('last'));
    }

    public function testIndex(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/system/view_senders', ['ajax' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $script = $crawler->filterXPath('//script[position()=5]');
        $this->assertCount(1, $script);
        $this->assertStringContainsString(
            "Control.Tabs.create('tabs-main', true).activeLink.onmouseup();",
            $script->text()
        );
        $this->assertStringContainsString(".addParam('offline', 1)", $script->text());

        $menu = $crawler->filterXPath('//ul[@id="tabs-main"]');
        $this->assertCount(1, $menu);
        $translator = new Translator();

        $this->assertEquals(
            $translator->tr('CViewSender'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#senders"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSenderSource'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#sources"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-title-dosend'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#dosend"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-title-monitor'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#monitor"]')->text(), 'ISO-8859-1', 'UTF-8')
        );

        $this->assertCount(
            1,
            $crawler->filterXPath(
                '//div[@id="senders"]//div[@id="list-senders"]//form[@name="senders-list"]'
            )
        );
        $this->assertCount(1, $crawler->filterXPath('//div[@id="monitor"]//form[@name="monitor-search"]'));
    }

    public function testList(): void
    {
        $sender = $this->getObjectFromFixturesReference(CViewSender::class, ViewSenderFixtures::SENDER_TAG);

        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/view_senders/list',
            ['ajax' => 1, 'name' => 'Fixtures', 'params' => 'foo']
        );

        $this->assertResponseStatusCodeSame(200);

        $main_div = $crawler->filterXPath('//div[@id="view-senders-list"]');
        $this->assertCount(1, $main_div);

        $this->assertCount(
            1,
            $main_div->filterXPath("//table//tbody//tr[@id='view-sender-{$sender->_id}-line']")
        );
    }

    public function testListWithMoreThan10ViewSenderForAPhase(): void
    {
        $repository = $this->createRepositoryMock();

        $client    = self::createClient();
        $container = $client->getContainer();
        $container->set(ViewSenderRepository::class, $repository);

        $crawler = $client->request('GET', '/gui/system/view_senders/list', ['ajax' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $this->assertCount(11, $crawler->filterXPath("//td[@class='warning']"));
    }

    public function testDoEditCreate(): CViewSender
    {
        $name_suffix = uniqid();

        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/view_senders/edit',
            [
                'name'         => "Test sender $name_suffix",
                'active'       => 0,
                'description'  => 'Sender test description',
                'params'       => 'foo=bar',
                'route_name'   => 'system_gui_about',
                'multipart'    => 0,
                'period'       => 30,
                'offset'       => 5,
                'every'        => 1,
                'max_archives' => 10,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CViewSender-msg-create'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        $sender       = new CViewSender();
        $sender->name = str_replace(' ', '_', "Test sender $name_suffix");
        $sender->loadMatchingObjectEsc();

        $this->assertEquals(0, $sender->active);
        $this->assertEquals('Sender test description', $sender->description);
        $this->assertEquals('foo=bar', $sender->params);
        $this->assertEquals('system_gui_about', $sender->route_name);
        $this->assertEquals(0, $sender->multipart);
        $this->assertEquals(30, $sender->period);
        $this->assertEquals(5, $sender->offset);
        $this->assertEquals(1, $sender->every);
        $this->assertEquals(10, $sender->max_archives);

        return $sender;
    }

    /**
     * @depends testDoEditCreate
     */
    public function testDoEditDuplicate(CViewSender $sender): CViewSender
    {
        self::createClient()->request(
            'POST',
            '/gui/system/view_senders/edit',
            [
                'name'         => $sender->name,
                'active'       => 0,
                'description'  => 'Sender test description',
                'params'       => 'foo=bar',
                'route_name'   => 'system_gui_about',
                'multipart'    => 0,
                'period'       => 30,
                'offset'       => 5,
                'every'        => 1,
                'max_archives' => 10,
            ]
        );

        $this->assertResponseStatusCodeSame(400);

        return $sender;
    }

    /**
     * @depends testDoEditDuplicate
     */
    public function testDoEditUpdate(CViewSender $sender): CViewSender
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/view_senders/edit',
            [
                'sender_id'   => $sender->_id,
                'description' => 'New description !',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CViewSender-msg-modify'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        $sender = CViewSender::findOrFail($sender->_id);
        $this->assertEquals('New description !', $sender->description);

        return $sender;
    }

    /**
     * @depends testDoEditUpdate
     */
    public function testDoEditDel(CViewSender $sender): void
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/view_senders/edit',
            [
                'sender_id' => $sender->_id,
                'del'       => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CViewSender-msg-delete'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );
    }

    public function testDoEditLink(): CSourceToViewSender
    {
        $sender = $this->getObjectFromFixturesReference(CViewSender::class, ViewSenderFixtures::SENDER_TAG);
        $source = $this->getObjectFromFixturesReference(CViewSenderSource::class, ViewSenderFixtures::SOURCE_TAG);

        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/view_senders/edit',
            [
                'sender_id' => $sender->_id,
                'source_id' => $source->_id,
                'type'      => 'link',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CSourceToViewSender-msg-create'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        /** @var CSourceToViewSender $source_to_sender */
        $source_to_sender = $sender->loadBackRefs('sources_link', null, 1);
        $source_to_sender = reset($source_to_sender);
        $this->assertEquals($sender->_id, $source_to_sender->sender_id);
        $this->assertEquals($source->_id, $source_to_sender->source_id);

        return $source_to_sender;
    }

    /**
     * @depends testDoEditLink
     *
     * @param CSourceToViewSender $source_to_view_sender
     *
     * @return void
     */
    public function testDoEditLinkDelete(CSourceToViewSender $source_to_view_sender): void
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/view_senders/edit',
            [
                'source_to_view_sender_id' => $source_to_view_sender->_id,
                'del'                      => 1,
                'type'                     => 'link',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CSourceToViewSender-msg-delete'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );
    }

    public function testListSources(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/system/view_senders/sources/list');

        $this->assertResponseStatusCodeSame(200);

        $header = $crawler->filterXPath('//table//tr[position()=1]');

        $translator = new Translator();
        $this->assertEquals(
            $translator->tr('CViewSenderSource-name-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=1]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSenderSource-libelle-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=2]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSenderSource-group_id-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=3]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSenderSource-actif-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=4]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSenderSource-back-senders_link'),
            mb_convert_encoding($header->filterXPath('//th[position()=5]')->text(), 'ISO-8859-1', 'UTF-8')
        );
    }

    public function testVwEditSource(): void
    {
        $source = $this->getObjectFromFixturesReference(CViewSenderSource::class, ViewSenderFixtures::SOURCE_TAG);

        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/view_senders/sources/edit',
            ['sender_source_id' => $source->_id]
        );

        $this->assertResponseStatusCodeSame(200);

        $form = $crawler->filterXPath("//form[@name='Edit-$source->_guid' and @method='post']");
        $this->assertCount(1, $form);
        $this->assertStringEndsWith(
            '/gui/system/view_senders/sources/edit',
            $form->filterXPath('//input[@name="@route"]')->attr('value')
        );
        $this->assertEquals($source->_id, $form->filterXPath('//input[@name="source_id"]')->attr('value'));
    }

    public function testDoEditSourceCreate(): CViewSenderSource
    {
        $name_suffix = uniqid();

        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/view_senders/sources/edit',
            [
                'name'     => "Test source $name_suffix",
                'actif'    => 0,
                'libelle'  => 'Libelle source test',
                'group_id' => CGroups::loadCurrent()->_id,
                'archive'  => 0,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CViewSenderSource-msg-create'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        $source       = new CViewSenderSource();
        $source->name = "Test source $name_suffix";
        $source->loadMatchingObjectEsc();

        $this->assertEquals(0, $source->actif);
        $this->assertEquals('Libelle source test', $source->libelle);
        $this->assertEquals(CGroups::loadCurrent()->_id, $source->group_id);
        $this->assertEquals(0, $source->archive);

        return $source;
    }

    /**
     * @depends testDoEditSourceCreate
     *
     * @param CViewSenderSource $source
     *
     * @return CViewSenderSource
     */
    public function testDoEditSourceUpdate(CViewSenderSource $source): CViewSenderSource
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/view_senders/sources/edit',
            [
                'source_id' => $source->_id,
                'libelle'   => 'New Libelle test',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CViewSenderSource-msg-modify'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        $source = CViewSenderSource::findOrNew($source->_id);

        $this->assertEquals('New Libelle test', $source->libelle);

        return $source;
    }

    /**
     * @depends testDoEditSourceUpdate
     *
     * @param CViewSenderSource $source
     *
     * @return void
     */
    public function testDoEditSourceDelete(CViewSenderSource $source): void
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/view_senders/sources/edit',
            [
                'source_id' => $source->_id,
                'del'       => '1',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CViewSenderSource-msg-delete'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );
    }

    public function testMonitor(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/system/view_senders/monitor', ['order_date' => 'ASC']);

        $this->assertResponseStatusCodeSame(200);

        $this->assertEquals(
            'Production',
            $crawler->filterXPath('//table[position()=1]//tr[position()=2]//th[position()=1]')->text()
        );
        $this->assertEquals(
            'Envoi',
            $crawler->filterXPath('//table[position()=1]//tr[position()=2]//th[position()=2]')->text()
        );

        $header = $crawler->filterXPath('//table[position()=1]//tr[position()=3]');

        $translator = new Translator();
        $this->assertEquals(
            $translator->tr('CViewSender-name-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=1]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-last_duration-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=2]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-last_size-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=3]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-last_datetime-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=4]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-last_status-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=5]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-_last_age-court') . ' / ' . $translator->tr('CViewSender-period'),
            mb_convert_encoding($header->filterXPath('//th[position()=6]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-source_id-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=7]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-last_duration-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=8]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-last_size-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=9]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-last_status-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=10]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-last_count-court') . ' / ' . $translator->tr(
                'CViewSender-max_archives-court'
            ),
            mb_convert_encoding($header->filterXPath('//th[position()=11]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-_last_age-court') . ' / ' . $translator->tr(
                'CViewSender-period-court'
            ),
            mb_convert_encoding($header->filterXPath('//th[position()=12]')->text(), 'ISO-8859-1', 'UTF-8')
        );
    }

    public function testSendWithoutExport(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/system/view_senders/send', ['ajax' => 1]);

        $this->assertEquals(
            'return ViewSender.doSend(1);',
            $crawler->filterXPath('//form[@name="ViewSenderUser"]')->attr('onsubmit')
        );

        $header = $crawler->filterXPath('//table[@class="tbl"]//tr[position()=1]');

        $translator = new Translator();
        $this->assertEquals(
            $translator->tr('CViewSender-name-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=1]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-_url-court') . ' / ' . $translator->tr('CViewSender-_file-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=2]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-last_duration-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=3]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CViewSender-last_size-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=4]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-source_id-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=5]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-last_duration-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=6]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-last_size-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=7]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-last_status-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=8]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CSourceToViewSender-last_count-court'),
            mb_convert_encoding($header->filterXPath('//th[position()=9]')->text(), 'ISO-8859-1', 'UTF-8')
        );
    }

    /**
     * @dataProvider extractUrlProvider
     *
     * @param string      $url
     * @param string|null $route_name
     * @param string|null $params
     *
     * @return void
     */
    public function testExtractUrl(string $url, ?string $route_name, ?string $params): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/system/view_senders/url', ['url' => $url]);

        $this->assertResponseStatusCodeSame(200);

        $data = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals($route_name, $data['route_name']);
        $this->assertEquals($params, $data['query']);
    }

    public function extractUrlProvider(): array
    {
        return [
            'legacy_url' => ['http://127.0.0.1/index.php?foo=bar&test=ok', null, 'foo=bar&test=ok'],
            'gui_url'    => ['http://127.0.0.1/gui/system/about?foo=bar', 'system_gui_about', 'foo=bar'],
            'api_url'    => ['http://127.0.0.1/api/status', 'system_api_status', null],
        ];
    }

    /**
     * @throws CMbException
     * @throws Exception
     */
    private function createView(): void
    {
        $view         = new CViewSender();
        $view->params = implode(
            "\n",
            [
                'm=sample',
                'raw=lorem_ipsum',
                'query_id=1',
            ]
        );
        $view->period = 60;
        $view->active = true;

        $view->name = $view->getUniqueName('lorem_ipsum');

        if ($msg = $view->store()) {
            throw new CMbException($msg);
        }
    }

    private function createRepositoryMock(): ViewSenderRepository
    {
        $sender         = new CViewSender();
        $sender->offset = 10;
        $sender->every  = 1;
        $sender->period = 60;

        $repository = $this->getMockBuilder(ViewSenderRepository::class)
                           ->onlyMethods(['initFromRequestParams', 'find'])
                           ->disableOriginalConstructor()
                           ->getMock();
        $repository->method('initFromRequestParams')->willReturn($repository);
        $repository->method('find')->willReturn(
            [
                $sender,
                $sender,
                $sender,
                $sender,
                $sender,
                $sender,
                $sender,
                $sender,
                $sender,
                $sender,
                $sender,
            ]
        );

        return $repository;
    }
}
