<?php

/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CNote;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class HistoryControllerTest extends OxWebTestCase
{
    /**
     * @pref    system_show_history 1
     * @throws TestsException
     * @throws Exception
     */
    public function testListHistory(): Item
    {
        /** @var CUser $user */
        $user = $this->getObjectFromFixturesReference(CUser::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $uri  = "/api/history/user/{$user->_id}";

        $client = static::createClient();
        $client->request('GET', $uri);
        $this->assertResponseIsSuccessful();
        $collection    = $this->getJsonApiCollection($client);
        $count_history = $collection->count();

        $user->user_address1 = uniqid('adresse1_');
        $user->store();

        $client = static::resetClient();

        $client->request('GET', $uri);
        $collection = $this->getJsonApiCollection($client);
        $this->assertGreaterThan($count_history, $collection->count());

        return $collection->getFirstItem();
    }

    /**
     * @depends testListHistory
     * @pref    system_show_history 0
     */
    public function testShowHistoryWithoutPref(Item $item): void
    {
        $user   = $this->getObjectFromFixturesReference(CUser::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $client = self::createClient();
        $client->request('GET', "/api/history/user/$user->_id/{$item->getId()}");

        $this->assertResponseStatusCodeSame(403);
        $error = $this->getJsonApiError($client);

        $this->assertEquals(
            (new Translator())->tr('common-msg-You are not allowed to access this information (%s)'),
            $error->getMessage()
        );
    }

    /**
     * @pref    system_show_history 1
     * @depends testListHistory
     * @throws TestsException
     * @throws Exception
     */
    public function testShowHistory(Item $item_depends): void
    {
        $user = $this->getObjectFromFixturesReference(CUser::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $uri  = "/api/history/user/{$user->_id}/{$item_depends->getId()}?relations=user";

        $client = static::createClient();
        $client->request('GET', $uri);

        $item = $this->getJsonApiItem($client);
        $this->assertTrue($item->hasRelationship('user'));
    }

    /**
     * @pref system_show_history 0
     */
    public function testListHistoryWithoutPref(): void
    {
        $user   = $this->getObjectFromFixturesReference(CUser::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $client = self::createClient();
        $client->request('GET', "/api/history/user/{$user->_id}");

        $this->assertResponseStatusCodeSame(403);
        $error = $this->getJsonApiError($client);

        $this->assertEquals(
            (new Translator())->tr('common-msg-You are not allowed to access this information (%s)'),
            $error->getMessage()
        );
    }

    /**
     * @pref system_show_history 1
     * @return void
     * @throws TestsException
     */
    public function testShowNonExistingHistory(): void
    {
        $user   = $this->getObjectFromFixturesReference(CUser::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $client = self::createClient();
        $client->request('GET', "/api/history/user/{$user->_id}/0");

        $this->assertResponseStatusCodeSame(404);
        $error = $this->getJsonApiError($client);

        $this->assertEquals('Invalid resource identifiers.', $error->getMessage());
    }

    /**
     * @pref system_show_history 1
     */
    public function testListWithFields(): void
    {
        $current_user = CMediusers::get();

        $note               = new CNote();
        $note->date         = CMbDT::dateTime();
        $note->object_class = 'CMediusers';
        $note->object_id    = $current_user->_id;
        $note->public       = 1;
        $note->text         = "test\nlorem ipsum";
        $this->storeOrFailed($note);

        try {
            $note->text    = "foo bar\nlorem oups";
            $this->storeOrFailed($note);

            $client = self::createClient();
            $client->request('GET', "/api/history/note/{$note->_id}", ['relations' => 'updatedFields']);

            $this->assertResponseStatusCodeSame(200);

            $collection = $this->getJsonApiCollection($client);
            $this->assertEquals(2, $collection->getMeta('count'));

            /**
             * @var Item $update_log
             * @var Item $creation_log
             */
            [$update_log, $creation_log] = $collection;
            $this->assertEquals('create', $creation_log->getAttribute('type'));
            $this->assertEquals([], $creation_log->getRelationship('updatedFields'));

            $this->assertEquals('store', $update_log->getAttribute('type'));
            $this->assertEquals('text', $update_log->getAttribute('fields'));

            $updated_fields = $collection->getIncluded();
            /** @var Item $field */
            foreach ($updated_fields as $field) {
                $this->assertEquals('text', $field->getAttribute('field_name'));
                $this->assertNull($field->getAttribute('old_value'));
                $this->assertNull($field->getAttribute('new_value'));
                $this->assertEquals(
                    "<del>test<br />\n</del><ins>foo bar<br />\n</ins>lorem <del>ipsum</del><ins>oups</ins>",
                    $field->getAttribute('diff')
                );
            }
        } finally {
            $note->delete();
        }
    }
}
