<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Ox\Core\CMbArray;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CNote;
use Ox\Mediboard\System\Tests\Fixtures\NoteFixtures;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Collection;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;

class NoteControllerTest extends OxWebTestCase
{
    public function testCreate(): CNote
    {
        $user = CMediusers::get();

        $content = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID            => '',
                    Item::TYPE          => 'note',
                    Item::ATTRIBUTES    => [
                        'text' => "libelle test\nUn text plus long",
                        'date' => '2023-01-01 10:06:02',
                    ],
                    Item::RELATIONSHIPS => [
                        'context' => [
                            AbstractObject::DATA => [
                                Item::ID   => $user->_id,
                                Item::TYPE => $user::RESOURCE_TYPE,
                            ],
                        ],
                    ],
                ],
            ]
        );

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('POST', '/api/system/notes?relations=all', content: json_encode($content));

        $this->assertResponseStatusCodeSame(201);

        $coll = $this->getJsonApiCollection($client);
        $this->assertCount(1, $coll);

        $item = $coll->getFirstItem();

        $this->assertEquals('note', $item->getType());
        $this->assertNotNull($item->getId());
        $this->assertEquals("libelle test\nUn text plus long", $item->getAttribute('text'));
        $this->assertEquals('2023-01-01 10:06:02', $item->getAttribute('date'));

        $author = $item->getRelationship('author');
        $this->assertNotNull($author);
        $this->assertEquals(CMediusers::get()->_id, $author->getId());
        $this->assertEquals(CMediusers::RESOURCE_TYPE, $author->getType());

        $context = $item->getRelationship('context');
        $this->assertNotNull($context);
        $this->assertEquals($user->_id, $context->getId());
        $this->assertEquals($user::RESOURCE_TYPE, $context->getType());

        return CNote::findOrFail($item->getId());
    }

    /**
     * @depends testCreate
     */
    public function testShow(CNote $note): CNote
    {
        $client = self::createClient();
        $client->request('GET', "/api/system/notes/$note->_id");

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals('note', $item->getType());
        $this->assertEquals($note->_id, $item->getId());

        return $note;
    }

    /**
     * @depends testShow
     */
    public function testUpdate(CNote $note): CNote
    {
        $update = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID         => $note->_id,
                    Item::TYPE       => $note::RESOURCE_TYPE,
                    Item::ATTRIBUTES => [
                        'text' => 'new text !',
                    ],
                ],
            ]
        );

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('PATCH', "/api/system/notes/$note->_id", [], [], [], json_encode($update));

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals('note', $item->getType());
        $this->assertEquals($note->_id, $item->getId());
        $this->assertEquals('new text !', $item->getAttribute('text'));

        return CNote::findOrFail($note->_id);
    }

    /**
     * @depends testUpdate
     */
    public function testDelete(CNote $note): void
    {
        $client = self::createClient();
        $client->request('DELETE', "/api/system/notes/$note->_id");

        $this->assertResponseStatusCodeSame(204);
        $this->assertEquals('', $client->getResponse()->getContent());

        $this->assertFalse(CNote::find($note->_id));
    }

    public function testListWithoutReadOnContext(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, NoteFixtures::USER_NOTE_FIXTURES);

        $current_user = CMediusers::get();
        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] ?? null;

            CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] = PERM_DENY;

            $client = self::createClient();
            $client->request('GET', '/api/system/notes/' . $user::RESOURCE_TYPE . '/' . $user->_id);

            $this->assertResponseStatusCodeSame(403);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] = $actual_perm;
            }
        }
    }

    /**
     * Only one CNote item should be return because the other one is private with another user
     */
    public function testList(): void
    {
        $user       = $this->getObjectFromFixturesReference(CMediusers::class, NoteFixtures::USER_NOTE_FIXTURES);
        $first_note = $user->loadFirstBackRef('notes');

        $client = self::createClient();
        $client->request(
            'GET',
            '/api/system/notes/' . $user::RESOURCE_TYPE . '/' . $user->_id,
            ['relations' => 'context,author']
        );

        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(1, $collection->count());

        $note = $collection->getFirstItem();
        $this->assertEquals('note', $note->getType());
        $this->assertEquals($first_note->_id, $note->getId());
        $this->assertEquals($user->_id, $note->getRelationship('context')->getId());
    }

    public function testListWithFilter(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, NoteFixtures::USER_NOTE_FIXTURES);

        $client = self::createClient();
        $client->request(
            'GET',
            '/api/system/notes/' . $user::RESOURCE_TYPE . '/' . $user->_id,
            ['filter' => 'text.beginWith.0-Fixtrue']
        );

        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(1, $collection->count());

        $note = $collection->getFirstItem();
        $this->assertStringStartsWith('0-Fixtrue', $note->getAttribute('text'));
    }

    public function testCreateWithAuthorizations(): CNote
    {
        $current_user = CMediusers::get();
        $user1        = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);
        $user2        = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_MEDECIN);

        $content = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID            => '',
                    Item::TYPE          => 'note',
                    Item::ATTRIBUTES    => [
                        'text'    => "This is a note",
                        'date'    => '2023-01-01 10:06:02',
                    ],
                    Item::RELATIONSHIPS => [
                        'context'         => [
                            AbstractObject::DATA => [
                                Item::ID   => $current_user->_id,
                                Item::TYPE => $current_user::RESOURCE_TYPE,
                            ],
                        ],
                        'authorizedUsers' => [
                            AbstractObject::DATA => [
                                [Item::ID => $user1->_id, Item::TYPE => $user1::RESOURCE_TYPE],
                                [Item::ID => $user2->_id, Item::TYPE => $user2::RESOURCE_TYPE],
                            ],
                        ],
                    ],
                ],
            ]
        );

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('POST', '/api/system/notes', content: json_encode($content));

        $this->assertResponseStatusCodeSame(201);

        $item = $this->getJsonApiCollection($client)->getFirstItem();
        $this->assertEquals('note', $item->getType());
        $note = CNote::findOrFail($item->getId());

        $permissions = $note->loadBackRefs('permissions');
        $this->assertCount(2, $permissions);

        $this->assertArrayContentsEquals([$user1->_id, $user2->_id], CMbArray::pluck($permissions, 'user_id'));

        return $note;
    }

    /**
     * @depends testCreateWithAuthorizations
     */
    public function testUpdateWithAuthorizations(CNote $note): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_SECRETAIRE);

        $relation_authorized_users = new Item($user::RESOURCE_TYPE, $user->_id);
        $item                      = new Item($note::RESOURCE_TYPE, $note->_id);
        $item->setRelationships(['authorizedUsers' => new Collection([$relation_authorized_users])]);

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('PATCH', "/api/system/notes/$note->_id", content: json_encode($item));

        $this->assertResponseStatusCodeSame(200);

        if (isset($note->_count['permissions'])) {
            unset($note->_count['permissions']);
        }

        $perms = $note->loadBackRefs('permissions');
        $this->assertCount(1, $perms);

        $perm = reset($perms);

        $this->assertEquals($user->_id, $perm->user_id);

        $note->delete();
    }
}
