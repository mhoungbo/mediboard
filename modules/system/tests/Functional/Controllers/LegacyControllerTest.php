<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Ox\Core\CAppUI;
use Ox\Tests\OxWebTestCase;
use ReflectionProperty;

/**
 * Must run the tests in separate process because of request isolation and globals problem.
 */
class LegacyControllerTest extends OxWebTestCase
{
    /**
     * Must reset the CAppUI::$echo_step to off to avoid echoing messages
     *
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        $ref_appui = new ReflectionProperty(CAppUI::class, 'echo_step');
        $ref_appui->setAccessible(true);
        $ref_appui->setValue(false);
    }

    /**
     * @config locale_warn 1
     */
    public function testAccessToLegacyWithAjax(): void
    {
        $client  = self::createClient();
        $crawler = $client->request('GET', '/', ['m' => 'system', 'a' => 'about', 'ajax' => 1]);

        $this->assertResponseStatusCodeSame(200);

        // Header
        $this->assertCount(0, $crawler->filterXPath('//div[@class="AppbarSkeleton"]'));

        // Body
        $this->assertCount(1, $crawler->filterXPath('//div[@id="about"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="about"]//div[@id="about-logo"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="about"]//div[@id="about-product"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="about"]//div[@id="about-description"]'));

        // Unlocalized
        $this->assertCount(0, $crawler->filterXPath('//form[@name="UnlocForm"]'));

        // Footer
        $this->assertStringNotContainsString('MultiTabChecker.check', $client->getResponse()->getContent());
    }

    /**
     * @config locale_warn 1
     */
    public function testAccessToLegacyWithAppBar(): void
    {
        $client  = self::createClient();
        $crawler = $client->request('GET', '/', ['m' => 'system', 'tab' => 'about']);

        $this->assertResponseStatusCodeSame(200);

        // Header
        $this->assertCount(1, $crawler->filterXPath('//div[@class="AppbarSkeleton"]'));

        // Body
        $this->assertCount(1, $crawler->filterXPath('//div[@id="about"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="about"]//div[@id="about-logo"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="about"]//div[@id="about-product"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="about"]//div[@id="about-description"]'));

        // Unlocalized
        $this->assertCount(1, $crawler->filterXPath('//form[@name="UnlocForm"]'));

        // Footer
        $this->assertStringContainsString('MultiTabChecker.check', $client->getResponse()->getContent());
    }

    public function testRedirect(): void
    {
        $client = self::createClient();
        $client->request('GET', '/', ['m' => 'foobar']);

        $this->assertResponseStatusCodeSame(302);

        $this->assertStringEndsWith(
            '?m=system&a=module_missing&mod=foobar',
            $client->getResponse()->headers->get('Location')
        );
    }
}
