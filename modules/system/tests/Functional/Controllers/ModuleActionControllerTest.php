<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Ox\Mediboard\System\CModuleAction;
use Ox\Tests\OxWebTestCase;

class ModuleActionControllerTest extends OxWebTestCase
{
    public function testViewSearch(): void
    {
        $client  = self::createClient();
        $crawler = $client->request('get', '/gui/system/module_action/view_search', ['element_id' => 'foo']);

        $this->assertResponseStatusCodeSame(200);

        $route = $crawler->filterXPath('//input[@name="@route"]')->attr('value');
        $this->assertStringContainsString('/gui/system/module_action/search', $route);
    }

    public function testSearch(): void
    {
        CModuleAction::getID('system', 'system_gui_about', true);

        $client  = self::createClient();
        $crawler = $client->request(
            'get',
            '/gui/system/module_action/search',
            ['element_id' => 'foo', 'search' => 'about']
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertEquals(
            1,
            $crawler->filterXPath('//button[@data-mod_name="system" and @data-action="system_gui_about"]')->count()
        );
    }
}
