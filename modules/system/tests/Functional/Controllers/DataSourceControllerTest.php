<?php

/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Exception;
use Ox\Core\Locales\Translator;
use Ox\Tests\OxWebTestCase;

class DataSourceControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testIndex(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/system/datasources', ['ajax' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $menu = $crawler->filterXPath('//ul[@id="tabs-datasources"]');
        $this->assertCount(1, $menu);
        $translator = new Translator();

        $this->assertEquals(
            $translator->tr('CSQLDatasource|pl'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#sqlDatasources"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CElasticDatasource|pl'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#nosqlDatasources"]')->text(), 'ISO-8859-1', 'UTF-8')
        );

        $main_div = $crawler->filterXPath('//div[@id="sqlDatasources"]');
        $this->assertCount(1, $main_div);
    }

    /**
     * @throws Exception
     */
    public function testViewEditForm(): void
    {
        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/datasources/edit',
            ['type' => 'sql', 'dsn' => 'std']
        );

        $this->assertResponseStatusCodeSame(200);

        $form = $crawler->filterXPath("//form[@name='ConfigDSN-std' and @method='post']");
        $this->assertCount(1, $form);
        $this->assertStringEndsWith(
            '/gui/system/configurations?module=system',
            $form->filterXPath('//input[@name="@route"]')->attr('value')
        );
    }

    /**
     * @throws Exception
     */
    public function testLoadInexistantDsn(): void
    {
        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/datasources/load',
            ['type' => 'sql', 'dsn' => uniqid()]
        );

        $this->assertResponseStatusCodeSame(200);

        $result = $crawler->filterXPath('//span[@class="dsn-empty"]');
        $this->assertCount(1, $result);
        $this->assertEquals(
            (new Translator())->tr('CSQLDataSource-msg-DSN empty'),
            mb_convert_encoding($result->text(), 'ISO-8859-1', 'UTF-8')
        );
    }

    /**
     * @throws Exception
     */
    public function testTestInexistantSqlDsn(): void
    {
        $test_dsn = uniqid();
        $crawler  = self::createClient()->request(
            'GET',
            '/gui/system/datasources/test',
            ['type' => 'sql', 'dsn' => $test_dsn]
        );

        $this->assertResponseStatusCodeSame(200);

        $result = $crawler->filterXPath('//div[@class="error"]');
        $this->assertCount(1, $result);
        $this->assertStringStartsWith(
            mb_convert_encoding($result->text(), 'ISO-8859-1', 'UTF-8'),
            (new Translator())->tr('CSQLDataSource-msg-Failed to connect to %s on %s', $test_dsn, '')
        );
    }

    /**
     * @group schedules
     * @throws Exception
     */
    public function testTestNoSqlDsn(): void
    {
        $dsn = 'application-log';
        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/datasources/test',
            ['type' => 'nosql', 'module' => 'dPdeveloppement', 'dsn' => $dsn]
        );

        $this->assertResponseStatusCodeSame(200);

        $menu = $crawler->filterXPath('//ul[@id="tabs-info"]');
        $this->assertCount(1, $menu);
        $translator = new Translator();

        $this->assertEquals(
            $translator->tr('ElasticIndexManager-Cluster status'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#cluster"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('ElasticIndexManager-Node status'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#nodes"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('Indexes'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#index"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('ElasticIndexManager-Index lifecycle management'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#ilm"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
    }
}
