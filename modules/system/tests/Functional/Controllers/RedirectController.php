<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Ox\Mediboard\Admin\CUser;
use Ox\Tests\OxWebTestCase;

class RedirectController extends OxWebTestCase
{
    public function testRedirectDefaultPage(): void
    {
        $expected_url = CUser::get()->getDefaultPageLink();

        $client = self::createClient();
        $client->request('GET', '/');

        $this->assertResponseStatusCodeSame(302);

        $this->assertResponseHeaderSame('Location', $expected_url);
    }
}
