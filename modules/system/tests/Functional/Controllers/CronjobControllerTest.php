<?php

/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Exception;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\System\Cron\CCronJob;
use Ox\Mediboard\System\Cron\CCronJobLog;
use Ox\Tests\OxWebTestCase;

class CronjobControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testIndex(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/system/cronjobs');

        $this->assertResponseStatusCodeSame(200);

        $menu = $crawler->filterXPath('//ul[@id="tabs-cronjob"]');
        $this->assertCount(1, $menu);
        $translator = new Translator();

        $this->assertEquals(
            $translator->tr('CCronJob.list'),
            mb_convert_encoding(
                $menu->filterXPath('//li//a[@href="#tab_list_cronjobs"]')->text(),
                'ISO-8859-1',
                'UTF-8'
            )
        );
        $this->assertEquals(
            $translator->tr('CCronJobLog'),
            mb_convert_encoding($menu->filterXPath('//li//a[@href="#tab_log_cronjobs"]')->text(), 'ISO-8859-1', 'UTF-8')
        );

        $this->assertCount(
            1,
            $crawler->filterXPath('//div[@id="tab_list_cronjobs"]//form[@name="search_cronjob_list"]')
        );
        $this->assertCount(1, $crawler->filterXPath('//div[@id="tab_log_cronjobs"]//form[@name="search_cronjob"]'));
    }

    /**
     * @throws Exception
     */
    public function testListCronjobs(): void
    {
        $cron = $this->createCronjob();

        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/cronjobs/list',
            ['ajax' => 1, 'cronjob_id' => $cron->_id]
        );

        $this->deleteOrFailed($cron);

        $this->assertResponseStatusCodeSame(200);

        $main_div = $crawler->filterXPath('//div[@id="cronjobs-list"]');
        $this->assertCount(1, $main_div);

        $this->assertEquals(
            'system_gui_about',
            $main_div->filterXPath("//table//tr[position()=3]//td[position()=4]")->text()
        );
    }

    /**
     * @throws Exception
     */
    public function testListCronjobsLogs(): void
    {
        $cron = $this->createCronjob();
        $this->createCronjobLog($cron);

        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/cronjobs/logs',
            ['ajax' => 1, 'cronjob_id' => $cron->_id]
        );

        $this->deleteOrFailed($cron);

        $this->assertResponseStatusCodeSame(200);

        $main_div = $crawler->filterXPath('//div[@id="cronjobs-logs-list"]');
        $this->assertCount(1, $main_div);

        $this->assertEquals(
            '29/03/2023 12h11',
            $main_div->filterXPath("//table//tr[position()=2]//td[position()=6]")->text()
        );
    }

    /**
     * @throws Exception
     */
    public function testViewEditCronjob(): void
    {
        $cron    = $this->createCronjob();
        $cron_id = $cron->_id;

        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/cronjobs/edit',
            ['cronjob_id' => $cron_id]
        );

        $this->deleteOrFailed($cron);

        $this->assertResponseStatusCodeSame(200);

        $form = $crawler->filterXPath("//form[@name='editcronjob' and @method='post']");
        $this->assertCount(1, $form);
        $this->assertStringEndsWith(
            '/gui/system/cronjobs/edit',
            $form->filterXPath('//input[@name="@route"]')->attr('value')
        );
        $this->assertEquals($cron_id, $form->filterXPath('//input[@name="cronjob_id"]')->attr('value'));
    }

    /**
     * @throws Exception
     */
    public function testDoEditCreateCronjob(): CCronJob
    {
        $name_suffix = uniqid();
        $name        = "Test sender {$name_suffix}";

        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/cronjobs/edit',
            [
                'name'       => $name,
                'active'     => 0,
                'route_name' => 'system_gui_about',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CCronJob-msg-create'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        $cron       = new CCronJob();
        $cron->name = $name;
        $cron->loadMatchingObjectEsc();

        $this->assertEquals($name, $cron->name);
        $this->assertEquals(0, $cron->active);
        $this->assertEquals('system_gui_about', $cron->route_name);

        return $cron;
    }

    /**
     * @depends testDoEditCreateCronjob
     * @throws Exception
     * @throws Exception
     */
    public function testDoChangeCronJobState(CCronJob $cron): CCronJob
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/cronjobs/edit',
            [
                'ajax'       => 1,
                'cronjob_id' => $cron->_id,
                'state'      => 1,
                'active'     => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CCronJob-msg-modify'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        $cron = CCronJob::findOrFail($cron->_id);
        $this->assertEquals(1, $cron->active);

        return $cron;
    }

    /**
     * @depends testDoChangeCronJobState
     * @throws Exception
     * @throws Exception
     */
    public function testDoChangeCronJobName(CCronJob $cron): CCronJob
    {
        $name_suffix = uniqid();
        $name        = "Test sender {$name_suffix}";

        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/cronjobs/edit',
            [
                'ajax'       => 1,
                'cronjob_id' => $cron->_id,
                'name'       => $name,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CCronJob-msg-modify'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        $cron = CCronJob::findOrFail($cron->_id);
        $this->assertEquals($name, $cron->name);

        return $cron;
    }

    /**
     * @depends testDoChangeCronJobName
     * @throws Exception
     */
    public function testDoEditDelCronjob(CCronJob $cron): void
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/cronjobs/edit',
            [
                'cronjob_id' => $cron->_id,
                'del'        => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            (new Translator())->tr('CCronJob-msg-delete'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );
    }

    private function createCronjob(): CCronJob
    {
        $cron             = new CCronJob();
        $cron->name       = 'test_cronjob';
        $cron->active     = '1';
        $cron->route_name = 'system_gui_about';
        $this->storeOrFailed($cron);

        return $cron;
    }

    private function createCronjobLog(CCronJob $job): CCronJobLog
    {
        $log                 = new CCronJobLog();
        $log->start_datetime = '2023-03-29 12:11:10';
        $log->cronjob_id     = $job->_id;
        $log->status         = '200';
        $log->server_address = 'lorem';
        $log->severity       = 0;

        $this->storeOrFailed($log);

        return $log;
    }
}
