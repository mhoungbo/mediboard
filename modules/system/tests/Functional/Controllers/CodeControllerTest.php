<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Ox\Core\Locales\Translator;
use Ox\Mediboard\System\Code\Code;
use Ox\Mediboard\System\Code\CodeRepository;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;

class CodeControllerTest extends OxWebTestCase
{
    public function testListWithKeywordsTooShort(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/system/codes', ['keywords' => '1']);

        $this->assertResponseStatusCodeSame(400);

        $error = $this->getJsonApiError($client);
        $this->assertEquals(
            (new Translator())->tr('CodeController-Error-Keywords must be at least 2 chars long'),
            $error->getMessage()
        );
    }

    public function testListOk(): void
    {
        $client = self::createClient();

        $repository = $this->getMockBuilder(CodeRepository::class)
            ->onlyMethods(['find'])
            ->getMock();
        $repository->method('find')->willReturn(
            [
                new Code('code1', 'description1', 'type1'),
                new Code('code2', 'description2', 'type2'),
            ]
        );

        $client->getContainer()->set('ox.api.code_repository', $repository);

        $client->request('GET', '/api/system/codes', ['keywords' => 'foo']);

        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);

        $this->assertEquals(2, $collection->getMeta('count'));

        /** @var Item $code1 */
        $code1 = $collection[0];
        $this->assertEquals('code', $code1->getType());
        $this->assertEquals('code1', $code1->getAttribute('code'));
        $this->assertEquals('description1', $code1->getAttribute('description'));
        $this->assertEquals('type1', $code1->getAttribute('type'));

        /** @var Item $code2 */
        $code2 = $collection[1];
        $this->assertEquals('code', $code2->getType());
        $this->assertEquals('code2', $code2->getAttribute('code'));
        $this->assertEquals('description2', $code2->getAttribute('description'));
        $this->assertEquals('type2', $code2->getAttribute('type'));
    }
}
