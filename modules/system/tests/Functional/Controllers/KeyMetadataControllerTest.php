<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Tests\Functional;

use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\Security\Cert\Format;
use Ox\Core\Security\Crypt\Alg;
use Ox\Core\Security\Crypt\Mode;
use Ox\Mediboard\System\Keys\CKeyMetadata;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotPersistKey;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotUseKey;
use Ox\Mediboard\System\Keys\KeyStore;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Todo: More tests
 * Todo: Test showing key metadata details with real encrypted objects and properties
 * Todo: Use some XPath queries to assert the responses
 */
class KeyMetadataControllerTest extends OxWebTestCase
{
    private const CERT_P12                 = 'badssl.com-client.p12';
    private const CERT_PEM_WITH_PASSPHRASE = 'badssl.com-client.crt';
    private const CERT_PEM_WO_PASSPHRASE   = 'google.crt';
    private const CERT_DER                 = 'google.der';
    private const PASSPHRASE               = 'badssl.com';
    private static ?CKeyMetadata $not_generated_key_metadata = null;
    private static ?CKeyMetadata $not_uploaded_cert_metadata = null;

    private function initMetadata(): void
    {
        // AES key metadata
        $metadata       = new CKeyMetadata();
        $metadata->name = uniqid('test');
        $metadata->alg  = Alg::AES->value;
        $metadata->mode = Mode::CTR->value;

        if ($msg = $metadata->store()) {
            $this->fail($msg);
        }

        $this->assertFalse($metadata->hasBeenPersisted());

        self::$not_generated_key_metadata = $metadata;

        // PKCS#12 metadata
        $metadata         = new CKeyMetadata();
        $metadata->name   = uniqid('test');
        $metadata->format = Format::PKCS12->value;

        if ($msg = $metadata->store()) {
            $this->fail($msg);
        }

        $this->assertFalse($metadata->hasBeenPersisted());

        self::$not_uploaded_cert_metadata = $metadata;
    }

    public function testMetadataListing(): void
    {
        $this->initMetadata();

        $client = static::createClient();
        $client->request('GET', 'gui/system/keyMetadata');

        $this->assertResponseIsSuccessful();
    }

    /**
     * @depends testMetadataListing
     *
     * @return void
     */
    public function testGenerateKeySucceed(): void
    {
        $metadata = self::$not_generated_key_metadata;

        $client = static::createClient();
        $client->request('POST', "gui/system/keyMetadata/{$metadata->_id}/generate");

        $this->assertResponseIsSuccessful();

        // Reloading metadata after it has been persisted
        $metadata = CKeyMetadata::findOrFail($metadata->_id);
        $this->assertTrue($metadata->hasBeenPersisted());
    }

    /**
     * @depends testMetadataListing
     */
    public function testUploadingCertificateSucceed(): void
    {
        $store    = new KeyStore();
        $metadata = self::$not_uploaded_cert_metadata;

        try {
            $store->getCertificate($metadata->name);

            // Should be unreachable
            $this->fail('Certificate should not exist');
        } catch (CouldNotUseKey) {
        }

        $client = static::createClient();
        $client->request(
            'POST',
            "gui/system/keyMetadata/{$metadata->_id}/uploadCertificate",
            [
                '_passphrase' => self::PASSPHRASE,
            ],
            [
                'formfile' => [new UploadedFile($this->getCertPath(self::CERT_P12), self::CERT_P12, null, null, true)],
            ]
        );

        $this->assertResponseIsSuccessful();

        // Reloading metadata after it has been persisted
        $metadata = CKeyMetadata::findOrFail($metadata->_id);
        $this->assertTrue($metadata->hasBeenPersisted());

        $cert = $store->getCertificate($metadata->name);

        $this->assertEquals(Format::PKCS12, $cert->getFormat());
        $this->assertEquals(
            '/C=US/ST=California/L=San Francisco/O=BadSSL/CN=BadSSL Client Certificate',
            $cert->getName()
        );
    }

    /**
     * @depends      testUploadingCertificateSucceed
     * @dataProvider validCertRenewingProvider
     *
     * @param string      $filepath
     * @param Format      $format
     * @param string|null $passphrase
     *
     * @return void
     * @throws CouldNotUseKey
     * @throws CMbModelNotFoundException
     */
    public function testRenewingCertificateSucceed(string $filepath, Format $format, ?string $passphrase): void
    {
        $store = new KeyStore();

        // Use the same as testUploadingCertificateSucceed because it determine itself the cert should be renewed
        $metadata = self::$not_uploaded_cert_metadata;

        // Does not throw an exception if already existing
        $store->getCertificate($metadata->name);

        $client = static::createClient();
        $client->request(
            'POST',
            "gui/system/keyMetadata/{$metadata->_id}/uploadCertificate",
            [
                '_passphrase' => $passphrase,
                '_format'     => $format->value,
            ],
            [
                'formfile' => [
                    new UploadedFile(
                        $filepath,
                        basename($filepath),
                        null,
                        null,
                        true
                    ),
                ],
            ]
        );

        $this->assertResponseIsSuccessful();

        // Reloading metadata after it has been persisted
        $metadata = CKeyMetadata::findOrFail($metadata->_id);
        $this->assertTrue($metadata->hasBeenPersisted());

        $cert = $store->getCertificate($metadata->name);

        $this->assertEquals($format, $cert->getFormat());
    }

    /**
     * @depends      testRenewingCertificateSucceed
     * @dataProvider invalidCertRenewingProvider
     *
     * @param string      $filepath
     * @param Format      $format
     * @param string|null $passphrase
     *
     * @return void
     * @throws CouldNotUseKey
     * @throws Exception
     */
    public function testRenewingCertificateFails(string $filepath, Format $format, ?string $passphrase): void
    {
        $store = new KeyStore();

        // Use the same as testUploadingCertificateSucceed because it determine itself the cert should be renewed
        $metadata = self::$not_uploaded_cert_metadata;

        // Does not throw an exception if already existing
        $store->getCertificate($metadata->name);

        $client = static::createClient();
        $client->request(
            'POST',
            "gui/system/keyMetadata/{$metadata->_id}/uploadCertificate",
            [
                '_passphrase' => $passphrase,
                '_format'     => $format->value,
            ],
            [
                'formfile' => [
                    new UploadedFile(
                        $filepath,
                        basename($filepath),
                        null,
                        null,
                        true
                    ),
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(500);
    }

    /**
     * @depends testGenerateKeySucceed
     *
     * @return void
     */
    public function testGenerateKeyFailsIfAlreadyGenerated(): void
    {
        $metadata = self::$not_generated_key_metadata;

        $client = static::createClient();
        $client->request('POST', "gui/system/keyMetadata/{$metadata->_id}/generate");

        $this->assertResponseStatusCodeSame(500, CouldNotPersistKey::alreadyExists($metadata->name)->getMessage());
    }

    /**
     * @depends testMetadataListing
     *
     * @return void
     */
    public function testShowingMetadataSucceed(): void
    {
        $metadata = self::$not_generated_key_metadata;

        $client = static::createClient();
        $client->request('GET', "gui/system/keyMetadata/{$metadata->_id}");

        $this->assertResponseIsSuccessful();
    }

    private function getCertPath(string $filename): string
    {
        return dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . $filename;
    }

    public function validCertRenewingProvider(): array
    {
        return [
            'PEM with passphrase'    => [
                $this->getCertPath(self::CERT_PEM_WITH_PASSPHRASE),
                Format::PEM,
                self::PASSPHRASE,
            ],
            'PEM without passphrase' => [$this->getCertPath(self::CERT_PEM_WO_PASSPHRASE), Format::PEM, null],
            'DER without passphrase' => [$this->getCertPath(self::CERT_DER), Format::DER, null],
            'PKCS12'                 => [$this->getCertPath(self::CERT_P12), Format::PKCS12, self::PASSPHRASE],
        ];
    }

    public function invalidCertRenewingProvider(): array
    {
        return [
            'PEM with bad passphrase'                           => [
                $this->getCertPath(self::CERT_PEM_WITH_PASSPHRASE),
                Format::PEM,
                'bad passphrase',
            ],
            'DER with passphrase'                               => [
                $this->getCertPath(self::CERT_DER),
                Format::DER,
                'bad passphrase',
            ],
            // PEM with private key, but no passphrase provided are not supported
//            'PEM with empty passphrase'                         => [
//                $this->getCertPath(self::CERT_PEM_WITH_PASSPHRASE),
//                Format::PEM,
//                null,
//            ],
            'PEM without passphrase but providing a passphrase' => [
                $this->getCertPath(self::CERT_PEM_WO_PASSPHRASE),
                Format::PEM,
                self::PASSPHRASE,
            ],
            'PKCS12 with bad passphrase'                        => [
                $this->getCertPath(self::CERT_P12),
                Format::PKCS12,
                'bad passphrase',
            ],
            'PKCS12 with empty passphrase'                      => [
                $this->getCertPath(self::CERT_P12),
                Format::PKCS12,
                null,
            ],
        ];
    }
}
