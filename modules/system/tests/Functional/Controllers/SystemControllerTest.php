<?php
 
/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\Controllers\SystemController;
use Ox\Tests\OxWebTestCase;

class SystemControllerTest extends OxWebTestCase
{
    public function testStatus(): void
    {
        $client = static::createClient(force_auth_current_user: false);
        $client->request('GET', '/api/status');

        $this->assertResponseIsSuccessful();
        $item = $this->getJsonApiItem($client);
        $this->assertTrue($item->hasAttribute('status'));
        $this->assertEquals($item->getAttribute('status'), 'online');
    }

    public function testOAS()
    {
        $client = static::createClient(force_auth_current_user: false);
        $client->request('GET', '/api/doc');

        $this->assertResponseIsSuccessful();
    }

    public function testOffline(): void
    {
        $container  = static::getContainer();
        $controller = $container->get(SystemController::class);
        $response   = $controller->offline('Test offline');
        $this->assertEquals($response->getStatusCode(), 200);
        $this->assertStringContainsString('<body class="offline">', $response->getContent());
    }

    public function testAbout(): void
    {
        $client  = static::createClient();
        $crawler = $client->request('GET', '/gui/system/about');
        $this->assertResponseIsSuccessful();
        $title = $crawler->filterXPath('//head/title')->text();
        $this->assertStringContainsString('A propos', $title);
    }

    public function testDiscovery(): void
    {
        $client = static::createClient(force_auth_current_user: false);
        $client->request('GET', '/api/discovery');
        $this->assertResponseIsSuccessful();

        $item = $this->getJsonApiItem($client);

        $this->assertEquals('discovery_links', $item->getType());
        $this->assertArrayContentsEquals(
            [
                'login',
                'logout',
                'status',
                'openapi',
                'configurations',
                'module_configurations',
                'preferences',
                'locales',
                'bulk',
            ],
            array_keys($item->getAttributes())
        );
    }

    public function testObjectDetails(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        $crawler = self::createClient()->request(
            'GET',
            '/gui/object/' . $user::RESOURCE_TYPE . '/' . $user->_id,
            ['ajax' => 1]
        );

        $this->assertResponseStatusCodeSame(200);

        $main_ul = $crawler->filterXPath('//ul[@class="control_tabs"][1]');
        $this->assertCount(1, $main_ul);
        for ($i = 1; $i < 4; $i++) {
            $href = $main_ul->filterXPath("//li[position()=$i]//a")->attr('href');
            $href = str_replace('#', '', $href);
            $this->assertCount(1, $crawler->filterXPath("//div[@id='$href']"));
        }
    }

    public function testObjectDetailsCollection(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        $crawler = self::createClient()->request(
            'GET',
            '/gui/object/' . $user::RESOURCE_TYPE . '/' . $user->_id,
            ['ajax' => 1, 'collection_name' => 'user_actions']
        );

        $this->assertResponseStatusCodeSame(200);
    }
}
