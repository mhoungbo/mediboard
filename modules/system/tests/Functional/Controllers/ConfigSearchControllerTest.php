<?php

/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\System\Tests\Functional\Controllers;

use Exception;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\Configurations\ConfigSearchService;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ConfigSearchControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testIndex(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/system/config_search');

        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(1, $crawler->filterXPath('//form[@name="config-search" and @method="get"]'));
    }

    /**
     * @throws Exception
     */
    public function testListWithoutKeywords(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/system/config_search/list');

        $this->assertResponseStatusCodeSame(200);

        $result = $crawler->filterXPath('//div[@class="error"]');
        $this->assertCount(1, $result);
        $this->assertEquals(
            mb_convert_encoding($result->text(), 'ISO-8859-1', 'UTF-8'),
            $this->translator->tr('system-search-configs no keywords')
        );
    }

    /**
     * @throws Exception
     */
    public function testList(): void
    {
        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/config_search/list',
            [
                'keywords'   => 'patient',
                'configs'    => 1,
                'prefs'      => 1,
                'func_perms' => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertNotNull($crawler->filterXPath('//tr[position()=3]//td[position()=1]')->text());
    }

    /**
     * @throws Exception
     */
    public function testEditTranslationFeatureMandatory(): void
    {
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/config_search/translations',
            [
                'translation'      => 'Lorem ipsum',
                'translation_desc' => 'Lorem ipsum desc',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $result = $crawler->filterXPath('//div[@class="error"]');
        $this->assertCount(1, $result);
        $this->assertEquals(
            mb_convert_encoding($result->text(), 'ISO-8859-1', 'UTF-8'),
            $this->translator->tr('system-config-translations feature mandatory')
        );
    }

    /**
     * @throws Exception
     */
    public function testEditTranslation(): array
    {
        $feature = 'config-system-General-lorem_ipsum' . uniqid();
        $crawler = self::createClient()->request(
            'POST',
            '/gui/system/config_search/translations',
            [
                'translation'      => 'Lorem ipsum',
                'translation_desc' => 'Lorem ipsum desc',
                'feature'          => $feature,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            $this->translator->tr('CTranslationOverwrite-msg-create'),
            html_entity_decode($crawler->filterXPath('//script')->text())
        );

        $locale = $this->translator->getCurrentLocale();

        return [
            'feature' => $feature,
            'trans'   => (new ConfigSearchService())->getStoredTranslation($feature, $locale, []),
            'desc'    => (new ConfigSearchService())->getStoredTranslation($feature, $locale, [], true),
        ];
    }

    /**
     * @depends testEditTranslation
     * @throws Exception
     */
    public function testViewTranslation(array $translations): void
    {
        $crawler = self::createClient()->request(
            'GET',
            '/gui/system/config_search/translations',
            [
                'feature' => $translations['feature'],
            ]
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(1, $crawler->filterXPath('//form[@name="config-translations" and @method="post"]'));
        $this->assertEquals(
            $translations['feature'],
            $crawler->filterXPath(
                '//form[@name="config-translations"]//input[@type="hidden" and @name="feature"]'
            )->attr('value')
        );

        $this->deleteOrFailed($translations['trans']);
        $this->deleteOrFailed($translations['desc']);
    }

    /**
     * @throws Exception
     */
    public function testExport(): void
    {
        $client = self::createClient();
        ob_start();
        $client->request('GET', '/gui/system/config_search/export');
        $content = ob_get_contents();
        ob_end_clean();

        $this->assertResponseStatusCodeSame(200);
        $this->assertInstanceOf(StreamedResponse::class, $client->getResponse());

        $crawler = new Crawler();
        $crawler->addContent($content, 'application/xml');

        $this->assertCount(
            1,
            $crawler->filterXPath('//groups-configs[@real_name="' . CGroups::loadCurrent()->_name . '"]')
        );
    }
}
