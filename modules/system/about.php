<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;

CCanDo::check();
CView::checkin();

$smarty = new CSmartyDP();
$smarty->assign([
                    'version'      => (string)CApp::getVersion(),
                    'product_name' => CAppUI::conf('product_name'),
                    'description'  => CAppUI::tr('common-about'),
                ]);
$smarty->display('about');

if (!headers_sent()) {
    header("proxy:" . CApp::getProxyStatus());
}

