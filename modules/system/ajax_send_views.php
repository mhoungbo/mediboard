<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Mediboard\System\ViewSender\ViewSenderManager;

// DO NOT DELETE, USED IN TOKENS
// @deprecated Should use route system_gui_view_senders_send instead

CCanDo::checkRead();

$export = CView::get('export', 'bool default|1');

CView::checkin();

$manager = new ViewSenderManager($export);
$senders = $manager->prepareAndSend();

$smarty = new CSmartyDP();
$smarty->assign('senders', $senders);
$smarty->assign('time', $manager->getCurrentDateTime());
$smarty->assign('minute', $manager->getMinute());
$smarty->display('view_senders/inc_send_views.tpl');
