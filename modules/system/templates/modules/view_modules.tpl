{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=system script=Module}}

{{if $coreModules|@count}}
  <div class="big-warning">
      {{tr}}CModule-modules-core outdated{{/tr}}
  </div>
    {{mb_include template='modules/inc_modules.tpl' object=$coreModules installed=true}}

    {{mb_return}}
{{/if}}

<script type="text/javascript">
  Main.add(function () {
    Control.Tabs.create('tabs-modules', true);
    Control.Tabs.setTabCount('installed', '{{$mbmodules.installed|@count}}');
    Control.Tabs.setTabCount('notInstalled', '{{$mbmodules.notInstalled|@count}}');
  });
</script>

<ul id="tabs-modules" class="control_tabs">
  <li><a href="#installed">{{tr}}CModule-modules-installed{{/tr}}</a></li>
  <li><a href="#notInstalled">{{tr}}CModule-modules-notInstalled{{/tr}}</a></li>
</ul>

<div id="installed" style="display: none;">
    {{mb_include template='modules/inc_modules.tpl' object=$mbmodules.installed installed=true}}
</div>

<div id="notInstalled" style="display: none;">
    {{mb_include template='modules/inc_modules.tpl' object=$mbmodules.notInstalled installed=false}}
</div>
