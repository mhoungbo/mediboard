{{*
 * @package Mediboard\System
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  addModuleAction = function (button) {
    let elem = $('{{$element_id}}');
    elem.innerHTML += "\n" + button.get('mod_name') + " " + button.get('action') + " 100";
    Control.Modal.close();
  };
</script>

<table class="main tbl">
  <tr>
    <th>{{tr}}CModuleAction-module{{/tr}}</th>
    <th>{{tr}}CModuleAction-action{{/tr}}</th>
    <th class="narrow"></th>
  </tr>

  {{foreach from=$module_actions item=module_action}}
      <tr>
        <td>{{$module_action->module}}</td>
        <td>{{$module_action->action}}</td>
        <td>
          <button type="button" class="tick" data-mod_name="{{$module_action->module}}" data-action="{{$module_action->action}}" onclick="addModuleAction(this)">
              {{tr}}Select{{/tr}}
          </button>
        </td>
      </tr>
  {{/foreach}}
</table>
