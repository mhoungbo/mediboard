{{*
 * @package Mediboard\cim10
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl">
    <tr>
        <th class="title me-text-align-center" colspan="3">{{tr}}CSQLDataSource-versions{{/tr}}</th>
    </tr>
    <tr>
        <th class="me-text-align-center narrow">{{tr}}CSQLDataSource-name{{/tr}}</th>
        <th class="me-text-align-center narrow">{{tr}}CSQLDataSource-installed_version{{/tr}}</th>
        <th class="me-text-align-center narrow">{{tr}}CSQLDataSource-available_version{{/tr}}</th>
    </tr>
    {{foreach from=$datasources item=datasource}}
        <tr>
            <td>
                <div style="color: {{if $datasource.is_up_to_date}}forestgreen{{else}}firebrick{{/if}}">
                    {{tr}}{{$datasource.name}}{{/tr}}
                    {{if $datasource.is_up_to_date}}
                        <i class="fas fa-check" style="color: forestgreen" title="{{tr}}Uptodate{{/tr}}"></i>
                    {{else}}
                        <i class="fas fa-level-up-alt warning" title="{{tr}}Update available{{/tr}}"></i>
                        <i class="fa-"></i>
                    {{/if}}
                </div>
            </td>
            <td {{if !$datasource.installed_version}} class="empty"{{/if}}>
                {{if $datasource.installed_version}}
                    {{$datasource.installed_version}}
                {{else}}
                    {{tr}}CSQLDataSource-msg-not_installed{{/tr}}
                {{/if}}
            </td>
            <td>
                {{$datasource.available_version}}
            </td>
        </tr>
    {{/foreach}}
</table>
