{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=use_fallback value=false}}

{{if isset($app->user_prefs.FALLBACK_LOCALE|smarty:nodefaults) && $app->user_prefs.FALLBACK_LOCALE != $app->user_prefs.LOCALE}}
    {{assign var=use_fallback value=true}}
{{/if}}

{{if $conf.locale_warn}}
  <!-- Locales warns -->
    {{if !$ajax}}
      <script>
        Main.add(function () {
          var form = getForm('UnlocForm');

          ModuleAutocomplete.autocomplete('{{url name=system_gui_modules_autocomplete}}', form.module, form.module_autocomplete, {common: true});
        });
      </script>

      <form action="?m={{$m}}" name="UnlocForm" style="display: none" method="post" class="prepared"
            onsubmit="return Localize.onSubmit(this);">
        <input type="hidden" name="{{$actionType}}" value="{{$action}}"/>
        <input type="hidden" name="@route" value="{{url name=developpement_translations_save}}"/>
        {{csrf_token id=developpement_translations_save}}

        <div style="height: 700px; overflow-y: scroll;">
          <table class="form">
            <tr>
              <th class="title"
                  colspan="{{if $use_fallback}}3{{else}}2{{/if}}">{{tr}}system-title-unlocalized{{/tr}}</th>
            </tr>
            <tr>
              <th>{{tr}}Language{{/tr}}</th>
              <td {{if $use_fallback}}colspan="2"{{/if}}>
                <input type="text" readonly="readonly" name="language"
                       value="{{$app->user_prefs.LOCALE}}"/>
              </td>
            </tr>

              {{if $use_fallback}}
                <tr>
                  <th>{{tr}}pref-FALLBACK_LOCALE{{/tr}}</th>
                  <td colspan="2">
                    <input type="text" readonly="readonly" name="fallback_language"
                           value="{{$app->user_prefs.FALLBACK_LOCALE}}"/>
                  </td>
                </tr>
              {{/if}}

            <tr>
              <th>{{tr}}Module{{/tr}}</th>
              <td {{if $use_fallback}}colspan="2"{{/if}}>
                <input type="hidden" name="module" value="{{$m}}"/>

                <input type="text" name="module_autocomplete" value="{{tr}}module-{{$m}}-court{{/tr}}"/>
              </td>
            </tr>

            <tbody>
            </tbody>

          </table>
        </div>

        <div style="text-align: center">
          <button class="modify" type="submit">{{tr}}Save{{/tr}}</button>
          <button class="cancel" type="button" onclick="Control.Modal.close();">{{tr}}Close{{/tr}}</button>
        </div>
      </form>
    {{/if}}
  <script type="text/javascript">
    Main.add(Localize.populate.curry({{$app|static:unlocalized|@json}}));

    nameInputLocalisation = function (formElt) {
      const suppTran = $("suppTranslation");
      if (formElt.value) {
        suppTran.name = "s[" + formElt.value + "]";
        suppTran.enable();
      } else {
        suppTran.disable();
      }
    }
  </script>
{{/if}}
