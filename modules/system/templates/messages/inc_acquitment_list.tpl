{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{url name=system_gui_messages_ack message_id=$message->_id var=ack_path}}

<table class="tbl main">
  <tr>
    <td colspan="3">
        {{mb_include module=system template=inc_pagination total=$total
        change_page="Message.acquittalsSwitchPage"
        change_page_arg=$ack_path current=$limit step=$step}}
    </td>
  </tr>

  <tr>
    <th>{{tr}}CMessageAcquittement-date_display{{/tr}}</th>
    <th>{{tr}}CMessageAcquittement-date_ack{{/tr}}</th>
    <th>{{tr}}CMessageAcquittement-user_id{{/tr}}</th>
  </tr>

    {{foreach from=$acquittals item=_acquittal}}
      <tr>
        <td>{{$_acquittal->date_display|date_format:$conf.datetime}}</td>
        <td>{{$_acquittal->date_ack|date_format:$conf.datetime}}</td>
        <td>{{mb_include module=mediusers template='inc_vw_mediuser.tpl' mediuser=$_acquittal->_ref_user}}</td>
      </tr>
        {{foreachelse}}
      <tr>
        <td colspan="2" class="empty">{{tr}}CAcquittementMsgSystem.none{{/tr}}</td>
      </tr>
    {{/foreach}}
</table>
