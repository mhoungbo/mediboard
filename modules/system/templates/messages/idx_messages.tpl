{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=system script=message}}

<script type="text/javascript">
  Main.add(function () {
    Message.refresh_url = "{{url name=system_gui_messages_list}}";
    Message.update_url = "{{url name=system_gui_messages_edit}}";
    Message.refreshList();
  });
</script>

<button class="new singleclick" onclick="Message.edit(0);">
    {{tr}}CMessage-title-create{{/tr}}
</button>

<button class="new singleclick" onclick="Message.createUpdate();" style="float:right;">
    {{tr}}CMessage-title-create_update{{/tr}}
</button>

<form name="refresh_messages" method="get">
  <input type="hidden" name="status" value=""/>
  <table class="form">
    <tr>
      <th>Statuts</th>
      <td>
        <input type="checkbox" id="status_past" name="status[]" class="status" value="past"
               onchange="Message.refreshList()"/>
        <label for="status_past">{{tr}}CMessage._status.past{{/tr}}</label>

        <input type="checkbox" id="status_present" name="status[]" class="status" value="present" checked
               onchange="Message.refreshList()"/>
        <label for="status_present">{{tr}}CMessage._status.present{{/tr}}</label>

        <input type="checkbox" id="status_future" name="status[]" class="status" value="future" checked
               onchange="Message.refreshList()"/>
        <label for="status_future">{{tr}}CMessage._status.future{{/tr}}</label>
      </td>
    </tr>
  </table>
</form>

<div id="list-messages"></div>
