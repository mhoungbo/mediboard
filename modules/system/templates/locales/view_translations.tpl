{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=errors value=0}}
{{mb_script module=system script=translation}}
{{csrf_token id=system_gui_locales_purge var=csrf_token}}

<script type="text/javascript">
  Main.add(function () {
    const form = getForm('search-translation');
    form.onsubmit();
  });
</script>

<form name="search-translation" method="get" onsubmit="return onSubmitFormAjax(this, null, 'translation-overwrite-vw')">
    {{mb_route name=system_gui_locales_list}}
  <input type="hidden" name="start" value="0"/>

  <table class="main form">
    <tr>
      <td>
        <button onclick="Translation.editTrad(0, '{{url name=system_gui_locales_view_edit}}')" type="button"
                class="new">{{tr}}CTranslationOverwrite.new{{/tr}}</button>

          {{if $can->admin}}
            <button type="button" id="remove-old-translations" class="trash"
                    data-url="{{url name=system_gui_locales_purge}}"
                    data-token="{{$csrf_token}}"
                    onclick="Translation.confirmPurgeTranslations(this);">
                {{tr}}system-action-purge translations{{/tr}}
            </button>
          {{/if}}
      </td>

      <td align="right" colspan="3">
          {{if $can->admin}}
            <button type="button" class="import"
                    data-url="{{url name=system_gui_locales_view_import}}"
                    onclick="Translation.vwImportTranslations(this);">{{tr}}Import{{/tr}}</button>
          {{/if}}

        <button type="button" class="download" onclick="Translation.export(this)"
                data-url="{{url name=system_gui_locales_list}}">{{tr}}Export{{/tr}}</button>
      </td>
    </tr>

    <tr>
      <th>{{mb_title class=CTranslationOverwrite field=source}}</th>
      <td>{{mb_field class=CTranslationOverwrite field=source size=50 canNull=true}}</td>

      <th>{{mb_title class=CTranslationOverwrite field=language}}</th>
      <td>
        <select name="language">
          <option value="">{{tr}}All{{/tr}}</option>
            {{foreach from=$available_languages item=_lang}}
              <option value="{{$_lang}}">{{tr}}CTranslationOverwrite.language.{{$_lang}}{{/tr}}</option>
            {{/foreach}}
        </select>
      </td>
    </tr>

    <tr>
      <td colspan="4" class="button">
        <button type="button" class="search" onclick="Translation.searchTranslation()">{{tr}}Search{{/tr}}</button>
      </td>
    </tr>
  </table>
</form>

<div id="translation-overwrite-vw"></div>
