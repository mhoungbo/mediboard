{{*
 * @package Mediboard\System
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=thcolspan value=null}}
{{mb_default var=tdcolspan value=null}}
{{mb_default var=skip_locales value=null}}
{{mb_default var=m  value=null}}
{{mb_default var=class  value=null}}
{{mb_default var=cssClass  value=null}}

{{mb_config_base var=$var m=$m class=$class}}

<tr>
  <th {{if $thcolspan}}colspan="{{$thcolspan}}"{{else}}style="width: 50%"{{/if}}>
    <label for="{{$field}}" title="{{tr}}{{$locale}}-desc{{/tr}}">
      {{tr}}{{$locale}}{{/tr}}
    </label>  
  </th>

  <td {{if $tdcolspan}}colspan="{{$tdcolspan}}"{{/if}}>
    <select class="str" name="{{$field}}">
      {{if !is_array($values)}} 
      {{assign var=values value='|'|explode:$values}}
      {{/if}}
      
      {{foreach from=$values item=_value}}
        <option value="{{$_value}}" {{if $value == $_value}}selected{{/if}}>
          {{if $skip_locales}}
          {{$_value}}
          {{else}}
          {{tr}}{{$locale}}-{{$_value}}{{/tr}}
          {{/if}}
       </option>
      {{/foreach}}
    </select> 
  </td>
</tr>
