{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=system script=cache_viewer}}

<script>
  Main.add(function () {
    Control.Tabs.create('cache-tabs', true);
  });
</script>

<ul class="control_tabs" id="cache-tabs">
  <li>
    <a href="#tab-shm" style="position: relative;">
      <div class="tab-text">
        SHM<br/><small>{{$shm_global_info.total|decabinary}} ({{$shm_global_info.total_rate|round:1}}%)</small>
      </div>
    </a>
  </li>
  <li>
    <a href="#tab-dshm">
      <div class="tab-text">
        DSHM<br/><small>{{$dshm_global_info.entries}} / {{$dshm_global_info.instance_count}}</small>
      </div>
    </a>
  </li>
  <li>
    <a href="#tab-opcache" style="position: relative;">
      <div class="tab-text">
        Opcode cache<br/><small>{{$opcode_global_info.total|decabinary}} ({{$opcode_global_info.total_rate|round:1}}%)</small>
      </div>
    </a>
  </li>
  <li>
    <a href="#tab-jscss">
      <div class="tab-text">
        JS/CSS<br/><small>{{$assets_global_info.js_total|decabinary}} / {{$assets_global_info.css_total|decabinary}}</small>
      </div>
    </a>
  </li>
</ul>

<div style="display: none;" id="tab-shm">
    {{mb_include module=system template='cache/inc_cache_info.tpl' info=$shm_global_info type="shm"}}
</div>

<div style="display: none;" id="tab-dshm">
    {{mb_include module=system template='cache/inc_cache_info.tpl' info=$dshm_global_info type="dshm"}}
</div>

<div style="display: none;" id="tab-opcache">
    {{mb_include module=system template='cache/inc_cache_info.tpl' info=$opcode_global_info type="opcode"}}
</div>

<div style="display: none;" id="tab-jscss">
    {{mb_include module=system template='cache/inc_cache_assets_info.tpl' info=$assets_global_info}}
</div>
