{{*
 * @package Mediboard\System
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<div class="small-info">
  {{tr}}CModuleAction-Info-Search module action{{/tr}}
</div>

<div>
  <form name="search-module-action" method="get" onsubmit="return onSubmitFormAjax(this, null, 'result-search-module-action')">
      {{mb_route name=system_module_action_search}}
    <input type="hidden" name="element_id" value="{{$element_id}}"/>

    <input type="text" name="search" class="notNull"/>

    <button type="submit" class="search">{{tr}}Search{{/tr}}</button>
  </form>
</div>

<div id="result-search-module-action">
  <div class="empty" style="margin-top: 16px">{{tr}}CModuleAction-Info-Search result will be displayed here{{/tr}}</div>
</div>
