{{*
 * @package Mediboard\System
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=back_spec value=$object_select->_backSpecs.$back_name}}

<table class="main tbl" id="table_backs_{{$back_name}}">
<tr>
  <td colspan="2">
    {{mb_include module=system template=inc_pagination total=$counts.$back_name current=$start step=50
      change_page="ObjectNavigation.changePage" change_page_arg=$change_page_arg}}
  </td>
</tr>

<tr>
  <th class="category" colspan="2">
    {{tr}}{{$back_spec->_initiator}}-back-{{$back_name}}{{/tr}}
    {{if $object_select->_count.$back_name}}
      ( x {{$object_select->_count.$back_name}})
    {{/if}}
  </th>
</tr>

{{foreach from=$object_select->_back.$back_name key=_key_test item=back_ref}}
  <tr>
    <td class="text" style="vertical-align: top;">
      <span onmouseover="ObjectTooltip.createEx(this, '{{$back_ref->_guid}}')">
        {{$back_ref->_view}}
      </span>
      <br/>
    </td>

    <td class="narrow">
      <button type="button" class="link" data-url="{{url name=system_gui_object_details resource_type=$back_ref->_class resource_id=$back_ref->_id}}"
              onclick="ObjectNavigation.openModalObject(this, '{{$back_ref->_class}}', '{{$back_ref->_id}}')">
        {{tr}}mod-system-object-nav-object-link{{/tr}}
      </button>
    </td>
  </tr>
{{/foreach}}
</table>
