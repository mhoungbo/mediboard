{{*
 * @package Mediboard\System
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=system script=source_to_view_sender ajax=true}}

<script>
  Main.add(function () {
    let form = getForm('Edit-{{$sender->_guid}}');

    new Url().setRoute('{{url name=developpement_routes_autocomplete}}')
      .addParam('offline', 1)
      .autoComplete(form.search, null, {
        minChars: 3,
        method: 'get',
        dropdown: true,
        updateElement: function(selected) {
          const route_name = selected.get('route_name');
          $V(form.search, route_name);
          $V(form.route_name, route_name);
        }
      })
  });
</script>

<form name="Edit-{{$sender->_guid}}" action="?" method="post"
      onsubmit="return onSubmitFormAjax(this, {onComplete: function () {
        ViewSender.refreshList();
        Control.Modal.close();
      }})">
  {{mb_route name=system_gui_view_senders_do_edit}}
  {{mb_key   object=$sender}}
  {{csrf_token id=edit_view_sender}}
  <input type="hidden" name="del" value="0"/>

  <table class="form me-margin-bottom-10">

      {{mb_include template=inc_form_table_header object=$sender}}
    
    <tr>
      <th class="narrow">{{mb_label object=$sender field=name}}</th>
      <td>{{mb_field object=$sender field=name}}</td>
    </tr>

    <tr>
      <th>{{mb_label object=$sender field=active}}</th>
      <td>{{mb_field object=$sender field=active}}</td>
    </tr>
    
    <tr>
      <th>{{mb_label object=$sender field=description}}</th>
      <td>{{mb_field object=$sender field=description}}</td>
    </tr>
    
    <tr>
      <th>
          {{mb_label object=$sender field=params}}
        <br/>
        <button class="add" type="button"
                onclick="ViewSender.urlToParams(this);" data-url="{{url name=system_gui_view_senders_url}}">
          URL
        </button>
      </th>
      <td>{{mb_field object=$sender field=params}}</td>
    </tr>

    <tr>
      <th>{{mb_label object=$sender field=route_name}}</th>
      <td>
        <input type="hidden" name="route_name" value="{{$sender->route_name}}"/>
        <input class="autocomplete" type="text" placeholder="{{tr}}CViewSender-route_name-desc{{/tr}}" name="search"
               value="{{$sender->route_name}}" style="width: 320px"/>
      </td>
    </tr>

    <tr>
      <th>{{mb_label object=$sender field=multipart}}</th>
      <td>{{mb_field object=$sender field=multipart}}</td>
    </tr>
    
    <tr>
      <th>{{mb_label object=$sender field=period}}</th>
      <td>{{mb_field object=$sender field=period}}</td>
    </tr>
    
    <tr>
      <th>{{mb_label object=$sender field=offset}}</th>
      <td>{{mb_field object=$sender field=offset}}</td>
    </tr>

    <tr>
      <th>{{mb_label object=$sender field=every}}</th>
      <td>{{mb_field object=$sender field=every}}</td>
    </tr>
    
    <tr>
      <th>{{mb_label object=$sender field=max_archives}}</th>
      <td>{{mb_field object=$sender field=max_archives}}</td>
    </tr>

    <tr>
      <th>{{mb_label object=$sender field=day}}</th>
      <td>{{mb_field object=$sender field=day emptyLabel="CViewSender.day.every"}}</td>
    </tr>

      {{if $sender->_id}}
        <tr>
          <th>{{mb_label object=$sender field=last_size}}</th>
          <td>{{$sender->last_size|decabinary}}</td>
        </tr>
        <tr>
          <th>{{mb_label object=$sender field=last_duration}}</th>
          <td>{{$sender->last_duration|string_format:"%.3f"}}s</td>
        </tr>
        <tr>
          <th>{{mb_label object=$sender field=last_datetime}}</th>
          <td>
          <span title="{{$sender->last_datetime}}">
            {{$sender->last_datetime|date_format:$conf.datetime}}
          </span>
          </td>
        </tr>
        <tr>
          <th>{{mb_label object=$sender field=last_status}}</th>
          <td>
              {{if $sender->last_status}}
                  {{tr}}CViewSender-last_status.{{$sender->last_status}}{{/tr}}
              {{/if}}
          </td>
        </tr>
      {{/if}}

    <tr style="height: 20px;"></tr>
    <tr>
      <td class="button" colspan="2">
          {{if $sender->_id}}
            <button class="modify" type="submit">{{tr}}Save{{/tr}}</button>
            <button class="duplicate" type="submit" onclick="ViewSender.duplicate(this.form);">
                {{tr}}Duplicate{{/tr}}
            </button>
            <button class="trash" type="button" onclick="ViewSender.confirmDeletion(this.form);">
                {{tr}}Delete{{/tr}}
            </button>

              {{me_button label=Show icon=search old_class="notext compact" onclick="ViewSender.show('`$sender->_id`');"}}
              {{if $sender->active}}
                  {{if $sender->_ref_senders_source}}
                      {{url var="url_route_send_sender_view" name="system_gui_view_senders_send"}}
                      {{url var="url_route_edit_sender_view" name="system_gui_view_senders_view_edit"}}
                      {{me_button label="CViewSender-action-Product and send" icon=send old_class="notext compact" onclick="ViewSender.productAndSend('`$sender->_id`', this);" attr="data-url='$url_route_send_sender_view'"}}
                      {{me_button label="CViewSender-test-export" icon="fas fa-wrench" old_class="notext compact" onclick="ViewSender.testSender(this, '`$sender->_id`');" attr="data-route='$url_route_edit_sender_view'"}}
                      {{me_button label="CViewSender-msg-purge directories" icon="cleanup" old_class="notext compact" onclick="ViewSender.purgeDirectories(this, '`$sender->_id`');" attr="data-route='$url_route_edit_sender_view'"}}
                  {{else}}
                      {{url var="url_route_edit_sender_view" name=system_gui_view_senders_view_edit}}
                      {{me_button label="CViewSender-action-Link to source" icon=add old_class="notext compact" onclick="ViewSender.openSenderSourceLink('`$sender->_id`', this);" attr="data-url='$url_route_edit_sender_view'"}}
                  {{/if}}
              {{/if}}

              {{me_dropdown_button button_label=Actions button_icon="opt" button_class="notext me-tertiary"
              container_class="me-dropdown-button-right"}}
          {{else}}
            <button class="submit" type="submit">{{tr}}Create{{/tr}}</button>
          {{/if}}
      </td>
    </tr>
    <tr style="height:40px"></tr>
  </table>
</form>
