{{*
 * @package Mediboard\System
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=system script=view_sender}}
{{mb_script module=system script=view_sender_source}}
{{mb_script module=system script=source_to_view_sender}}
{{mb_script module=system script=exchange_source}}

<script>
  Main.add(function () {
    Control.Tabs.create('tabs-main', true).activeLink.onmouseup();


    let form = getForm('senders-list');

    new Url().setRoute('{{url name=developpement_routes_autocomplete}}')
      .addParam('offline', 1)
      .autoComplete(form.search, null, {
        minChars:      3,
        method:        'get',
        dropdown:      true,
        updateElement: function (selected) {
          $V(form.search, selected.get('route_name'));
        }
      })
  });
</script>

<ul id="tabs-main" class="control_tabs">
  <li><a href="#senders" onmouseup="ViewSender.refreshList();">{{tr}}CViewSender{{/tr}}</a></li>
  <li><a href="#sources" onmouseup="ViewSenderSource.refreshList();">{{tr}}CViewSenderSource{{/tr}}</a></li>
  <li><a href="#dosend" onmouseup="ViewSender.doSend(0);">{{tr}}CViewSender-title-dosend{{/tr}}</a></li>
  <li><a href="#monitor" onmouseup="ViewSender.refreshMonitor();">{{tr}}CViewSender-title-monitor{{/tr}}</a></li>
</ul>

<div id="senders" style="display: none;">

  <button class="new singleclick" data-url="{{url name=system_gui_view_senders_view_edit}}"
          onclick="ViewSender.edit(0, this);">
      {{tr}}CViewSender-title-create{{/tr}}
  </button>
  
  <div id="list-senders">
    <form name="senders-list" method="get" onsubmit="return onSubmitFormAjax(this, null, 'result-senders-list')">
        {{mb_route name=system_gui_view_senders_list}}
      <input type="hidden" name="plan_mode" value="production"/>

      <table class="main form">
        <tr>
          <th>{{mb_label class=CViewSender field=name}}</th>
          <td>{{mb_field class=CViewSender field=name canNull=true}}</td>

          <th>{{mb_label class=CViewSender field=params}}</th>
          <td><input type="text" name="params" value=""/></td>

          <th>{{mb_label class=CViewSender field=route_name}}</th>
          <td>
            <input type="hidden" name="route_name" value=""/>
            <input class="autocomplete" type="text" placeholder="{{tr}}CViewSender-route_name-desc{{/tr}}" name="search"
                   value="" style="width: 320px" onchange="$V(this.form.route_name, this.value)"/>
            <button type="button" class="erase notext" onclick="$V(this.form.route_name, ''); $V(this.form.search, '');"></button>
          </td>

          <th>{{mb_label class=CViewSender field=offset}}</th>
          <td>{{mb_field class=CViewSender field=offset canNull=true}}</td>
        </tr>

        <tr>
          <td class="button" colspan="8">
            <button type="submit" class="search">{{tr}}Search{{/tr}}</button>
          </td>
        </tr>
      </table>
    </form>

    <div id="result-senders-list"></div>
  </div>
</div>

<div id="sources" style="display: none;">
  <button class="new singleclick"
          onclick="ViewSenderSource.edit(0, '{{url name=system_gui_view_senders_sources_view_edit}}');">
      {{tr}}CViewSenderSource-title-create{{/tr}}
  </button>
  
  <div id="list-sources" data-url="{{url name=system_gui_view_senders_sources_list}}"></div>
</div>

<div id="dosend" style="display: none;" data-url="{{url name=system_gui_view_senders_send}}"></div>

<div id="monitor" style="display: none;">
  <form name="monitor-search" method="get" onsubmit="return onSubmitFormAjax(this, null, 'monitor-result')">
      {{mb_route name=system_gui_view_senders_monitor}}
    <input type="hidden" name="start" value="0"/>
    <input type="hidden" name="step" value="50"/>
    <input type="hidden" name="order_date" value=""/>

    <table class="main form">
      <tr>
        <th>{{mb_label class=CViewSender field=name}}</th>
        <td>{{mb_field class=CViewSender field=name canNull=true onchange="ViewSender.resetMonitoring()"}}</td>

        <th>{{mb_label class=CViewSender field=last_status}}</th>
        <td>
          <select name="view_sender_status" onchange="ViewSender.resetMonitoring()">
            <option value="">{{tr}}All{{/tr}}</option>
              {{foreach from=$view_sender_statuses item=status}}
                <option value="{{$status}}">{{tr}}CViewSender.last_status.{{$status}}{{/tr}}</option>
              {{/foreach}}
          </select>
        </td>

        <th>{{mb_label class=CSourceToViewSender field=last_status}}</th>
        <td>
          <select name="source_status" onchange="ViewSender.resetMonitoring()">
            <option value="">{{tr}}All{{/tr}}</option>
              {{foreach from=$source_statuses item=status}}
                <option value="{{$status}}">{{tr}}CSourceToViewSender.last_status.{{$status}}{{/tr}}</option>
              {{/foreach}}
          </select>
        </td>
      </tr>

      <tr>
        <td class="button" colspan="10">
          <button type="submit" class="search">{{tr}}Search{{/tr}}</button>
        </td>
      </tr>
    </table>
  </form>

  <div id="monitor-result"></div>
</div>
