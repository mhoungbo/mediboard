{{*
* @package Mediboard\System
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=eai script=source_file ajax=1}}

<table class="tbl">
{{foreach from=$data item=_data}}
    {{assign var=source value=$_data.source}}
    {{assign var=dir_path value=$_data.dir_path}}

    <tr>
      <th>[{{tr}}{{$source->_class}}{{/tr}}] {{$source->_view}}</th>
      <td {{if $_data.error }}class="error"{{/if}}>{{tr}}Directory{{/tr}} : {{$dir_path}}</td>

      {{if $_data.accessible}}
          {{if $_data.error}}
            <td class="error">{{$_data.error}}</td>
          {{else}}
            {{assign var=count_elements value=$_data.elements|@count}}
            <td title="{{tr}}List-of-file{{/tr}} / {{tr}}Directory{{/tr}}">
                {{tr}}common-Number of items{{/tr}} : {{$count_elements}}

                {{* Purge button *}}
              <button type="button" class="trash notext"
                      onclick="SourceFile.purgeDir(this, '{{$_data.dir_path}}', Control.Modal.refresh)"
                      data-route="{{$_data.purge_dir_route}}">
              </button>

                {{* See button *}}
                {{if $count_elements > 0}}
                  <button type="button" class="search notext"
                          onclick="ExchangeSource.manageFiles('{{$source->_guid}}')">
                  </button>
                {{/if}}
            </td>
          {{/if}}
      {{else}}
        <td class="warning">{{tr}}Directory-not_accessible{{/tr}}</td>
      {{/if}}
    </tr>
{{/foreach}}
</table>
