{{*
* @package Mediboard\System
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=eai script=source_file ajax=1}}

<script>
  Main.add(function () {
    Control.Tabs.create('tabs_report', true);
  });
</script>

<ul id="tabs_report" class="control_tabs small">
    {{foreach from=$data item=_data}}
        {{assign var=source value=$_data.source}}
        {{assign var=report value=$_data.report}}
      <li><a href="#report-{{$source->_guid}}" class="{{if $_data.has_errors}}wrong{{else}}special{{/if}}">{{tr}}{{$source->_class}}{{/tr}}</a></li>
    {{/foreach}}
</ul>

{{foreach from=$data item=_data}}
    {{assign var=source value=$_data.source}}
    {{assign var=report value=$_data.report}}
  <div id="report-{{$source->_guid}}" style="display: none;">
      {{mb_include module="eai" template="report/inc_report" report=$report}}

    {{if $_data.create_dir_route}}
        <table class="tbl">
          <tr>
            <th class="narrow">
                {{tr}}CSourceFile-msg-create dir{{/tr}}
            </th>
            <td>
                {{$_data.dir_path}}
              <button type="button" class="fas fa-wrench notext" onclick="SourceFile.createDir(this, '{{$_data.dir_path}}', Control.Modal.refresh)" data-route="{{$_data.create_dir_route}}"></button>
            </td>
          </tr>
        </table>
    {{/if}}
  </div>
{{/foreach}}

