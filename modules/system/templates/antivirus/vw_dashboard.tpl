{{*
 * @package Mediboard\System
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{csrf_token id=system_gui_antivirus_refresh var=csrf_token}}

<script>
  refreshDatabaseSignature = function () {
    if (!confirm($T('VirusCheckerInterface-Ask-Do you really want to refresh the database signature ?'))) {
      return;
    }

    new Url().setRoute('{{url name=system_gui_antivirus_refresh}}')
      .addParam('token', '{{$csrf_token}}')
      .requestUpdate('systemMsg', {method: "POST"});
  }
</script>

<table class="main tbl">
  <tr>
    <th>{{tr}}config-system-ClamAv-host{{/tr}}</th>
    <th>{{tr}}config-system-ClamAv-port{{/tr}}</th>
    <th>{{tr}}config-system-ClamAv-socket{{/tr}}</th>
    <th>{{tr}}State{{/tr}}</th>
    <th>{{tr}}VirusCheckerInterface-title-Database date{{/tr}}</th>
    <th>
      <button type="button" class="change notext" onclick="refreshDatabaseSignature();">
        {{tr}}VirusCheckerInterface-action-Refresh database signature{{/tr}}
      </button>
    </th>
  </tr>
  <tr>
    <td>{{$host}}</td>
    <td>{{$port}}</td>
    <td>{{$socket}}</td>
    <td class="{{if $available}}ok{{else}}error{{/if}}">
        {{if $available}}OK{{else}}KO{{/if}}
    </td>
    <td class="{{if $freshness === 0 || $freshness <= $last_update}}ok{{else}}error{{/if}}" colspan="2">
        {{$last_update}}
    </td>
  </tr>
</table>
