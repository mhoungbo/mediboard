{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<td>
  <button type="button" class="search notext compact"
          onclick="KeyMetadata.showDetails('{{url name=system_gui_key_metadata_details key_metadata_id=$metadata->_id}}', KeyMetadata.refreshMetadata.curry('{{$metadata->_id}}', '{{url name=system_gui_key_metadata key_metadata_id=$metadata->_id}}'));">
      {{tr}}common-action-See detail{{/tr}}
  </button>
</td>

<td>
    <span class="key-metadata-status key-metadata-status-{{$metadata->_status}}">
        {{mb_value object=$metadata field=_status}}
    </span>
</td>

<td><code>{{mb_value object=$metadata field=name}}</code></td>

<td>
  <span class="key-metadata-type key-metadata-type-{{$metadata->_type}}">
      {{mb_value object=$metadata field=_type}}
  </span>
</td>

<td><code style="text-transform: uppercase;">{{mb_value object=$metadata field=alg}}</code></td>
<td><code style="text-transform: uppercase;">{{mb_value object=$metadata field=mode}}</code></td>
<td><code style="text-transform: uppercase;">{{mb_value object=$metadata field=format}}</code></td>

<td {{if !$metadata->hasBeenPersisted()}} class="warning" {{/if}}>{{mb_value object=$metadata field=last_update}}</td>

<td class="text">{{mb_value object=$metadata field=description}}</td>
