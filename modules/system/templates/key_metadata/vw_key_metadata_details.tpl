{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(() => {
    Control.Tabs.create('key_metadata-tabs');

    KeyMetadata.showForm('{{url name=system_gui_key_metadata key_metadata_id=$metadata->_id type=form}}', 'metadata-details');
    KeyMetadata.showUsage('{{url name=system_gui_key_metadata key_metadata_id=$metadata->_id type=usage}}', 'key-usage');

    {{if $metadata->hasBeenPersisted() && $metadata->isCertificate()}}
      KeyMetadata.showKeyInfo('{{url name=system_gui_key_metadata key_metadata_id=$metadata->_id type=info}}', 'key-info');
    {{/if}}
  });
</script>

<ul id="key_metadata-tabs" class="control_tabs">
  <li><a href="#metadata-details">{{tr}}Details{{/tr}}</a></li>
  <li><a href="#key-usage">Usage</a></li>

    {{if $metadata->hasBeenPersisted() && $metadata->isCertificate()}}
      <li><a href="#key-info">{{tr}}Infos{{/tr}}</a></li>
    {{/if}}
</ul>

<div id="metadata-details" style="display: none;"></div>
<div id="key-usage" style="display: none;"></div>

{{if $metadata->hasBeenPersisted() && $metadata->isCertificate()}}
  <div id="key-info" style="display: none;"></div>
{{/if}}
