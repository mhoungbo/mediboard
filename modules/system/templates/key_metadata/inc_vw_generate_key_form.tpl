{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{csrf_token id=system_gui_key_metadata_generate_key var=csrf_token}}

<table class="main form">
  <col style="width: 20%;" />
  <tr>
    <th class="title" colspan="4">{{tr}}Key-title-Generate{{/tr}}</th>
  </tr>

  {{mb_include module=system template=key_metadata/inc_common_form metadata=$metadata}}

  <tr>
    <th>{{mb_label object=$metadata field=alg}}</th>
    <td style="text-transform: uppercase;">{{mb_value object=$metadata field=alg}}</td>

    <th>{{mb_label object=$metadata field=mode}}</th>
    <td style="text-transform: uppercase;">{{mb_value object=$metadata field=mode}}</td>
  </tr>

  <tr>
    <td class="button" colspan="4">
      {{if !$metadata->hasBeenPersisted() || $metadata->canBeRenewed()}}
        <button type="submit" class="fas fa-random me-primary"
                onclick="KeyMetadata.generateKey('{{$metadata->_id}}', '{{url name=system_gui_key_metadata_generate_key key_metadata_id=$metadata->_id}}', '{{url name=system_gui_key_metadata key_metadata_id=$metadata->_id}}', '{{$csrf_token}}', Control.Modal.close);">
            {{tr}}KeyBuilder-action-Generate key{{/tr}}
        </button>
      {{else}}
          <div class="small-info">{{tr}}Key-info-Already generated{{/tr}}</div>
      {{/if}}
    </td>
  </tr>
</table>
