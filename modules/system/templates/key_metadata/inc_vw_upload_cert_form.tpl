{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_ternary var=renew test=$metadata->hasBeenPersisted() value=1 other=0}}

<form name="upload-cert" method="post" enctype="multipart/form-data"
      onsubmit="return KeyMetadata.uploadCert(this, '{{$renew}}', Control.Modal.close);">
    {{mb_route name=system_gui_key_metadata_cert_upload key_metadata_id=$metadata->_id}}
    {{csrf_token id=system_gui_key_metadata_cert_upload}}

  <table class="main form">
    <col style="width: 20%;"/>

      <tr>
        <th class="title" colspan="4">
            {{if !$metadata->hasBeenPersisted()}}
              {{tr}}Certificate-title-Renewing{{/tr}}
            {{else}}
              {{tr}}Certificate-title-Uploading{{/tr}}
            {{/if}}
        </th>
      </tr>

      {{mb_include module=system template=key_metadata/inc_common_form metadata=$metadata}}

      {{if $metadata->hasBeenPersisted()}}
        <tr>
          <th>{{mb_label object=$metadata field=_format}}</th>
          <td>{{mb_field object=$metadata field=_format value=$metadata->format}}</td>

          <th>{{mb_label object=$metadata field=_passphrase}}</th>
          <td>{{mb_field object=$metadata field=_passphrase}}</td>
        </tr>
      {{else}}
        <tr>
          <th>{{mb_label object=$metadata field=_passphrase}}</th>
          <td colspan="3">{{mb_field object=$metadata field=_passphrase}}</td>
        </tr>
      {{/if}}

    <tr>
      <th>{{tr}}common-Certificate{{/tr}}</th>
      <td colspan="3">{{mb_include module=system template=inc_inline_upload multi=false paste=false lite=true}}</td>
    </tr>

    <tr>
      <td class="button" colspan="4">
          {{if $metadata->hasBeenPersisted()}}
            <div class="small-warning">
                {{tr}}Certificate-warning-Renewing{{/tr}}
            </div>
          {{/if}}

        <button type="submit" class="fas fa-upload me-primary">
            {{if $metadata->hasBeenPersisted()}}
                {{tr}}common-action-Renew{{/tr}}
            {{else}}
                {{tr}}common-action-Upload{{/tr}}
            {{/if}}
        </button>
      </td>
    </tr>
  </table>
</form>
