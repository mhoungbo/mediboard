{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{if $cert === null}}
    {{mb_return}}
{{/if}}

{{assign var=not_before value=$cert->getValidFrom()}}
{{assign var=not_after value=$cert->getValidTo()}}

<table class="main form">
  <col style="width: 20%;"/>

  <tr>
    <th class="title" colspan="2">{{tr}}Certificate-title-Metadata|pl{{/tr}}</th>
  </tr>

  <tr>
    <th>Name</th>
    <td><strong>{{$cert->getName()}}</strong></td>
  </tr>

  <tr>
    <th>Subject</th>
    <td>
      <pre><code>{{$cert->getSubjectDn()|@print_r:true}}</code></pre>
    </td>
  </tr>

  <tr>
    <th>Issuer</th>
    <td>
      <pre><code>{{$cert->getIssuerDn()|@print_r:true}}</code></pre>
    </td>
  </tr>

  <tr>
    <th>Valid from</th>
    <td>{{$not_before->format('d/m/Y H:i:s')}}</td>
  </tr>

  <tr>
    <th>Valid to</th>
    <td>{{$not_after->format('d/m/Y H:i:s')}}</td>
  </tr>
</table>
