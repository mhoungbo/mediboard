{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(() => {
    Control.Tabs.create('key_metadata-usage-tabs');

    Control.Tabs.setTabCount('encrypted-objects', '{{$total_encrypted_objects}}');
    Control.Tabs.setTabCount('encrypted-properties', '{{$total_encrypted_properties}}');
  });
</script>

<ul id="key_metadata-usage-tabs" class="control_tabs">
  <li><a href="#encrypted-objects">{{tr}}CObjectEncryption|pl{{/tr}}</a></li>
  <li><a href="#encrypted-properties">{{tr}}CEncryptedProperty|pl{{/tr}}</a></li>
</ul>

<div id="encrypted-objects" style="display: none;">
  <table class="main tbl">
    <tr>
      <th class="narrow">Nombre</th>
      <th>{{mb_title class=CObjectEncryption field=object_class}}</th>
    </tr>

    {{foreach from=$count_encrypted_objects_by_class key=_class item=_count}}
        <tr>
          <td>{{$_count}}</td>
          <td>{{$_class}}</td>
        </tr>
    {{foreachelse}}
        <tr>
          <td class="empty" colspan="2">
            {{tr}}None{{/tr}}
          </td>
        </tr>
    {{/foreach}}
  </table>
</div>

<div id="encrypted-properties" style="display: none;">
  <table class="main tbl">
    <tr>
      <th class="narrow">Nombre</th>
      <th>{{mb_title class=CEncryptedProperty field=object_class}}</th>
    </tr>

      {{foreach from=$count_encrypted_properties_by_class key=_class item=_count}}
        <tr>
          <td>{{$_count}}</td>
          <td>{{$_class}}</td>
        </tr>
          {{foreachelse}}
        <tr>
          <td class="empty" colspan="2">
              {{tr}}None{{/tr}}
          </td>
        </tr>
      {{/foreach}}
  </table>
</div>
