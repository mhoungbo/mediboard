{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<tr>
  <th>{{mb_label object=$metadata field=name}}</th>
  <td>{{mb_field object=$metadata field=name readonly=true}}</td>

  <th>{{mb_label object=$metadata field=last_update}}</th>
  <td>
      {{if $metadata->hasBeenPersisted()}}
          {{mb_value object=$metadata field=last_update}}
      {{else}}
        <span class="empty">{{tr}}common-Never{{/tr}}</span>
      {{/if}}
  </td>
</tr>

<tr>
  <th>{{mb_label object=$metadata field=description}}</th>
  <td colspan="3" class="text">{{mb_value object=$metadata field=description}}</td>
</tr>
