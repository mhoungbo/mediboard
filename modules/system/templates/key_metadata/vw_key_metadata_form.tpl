{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{if $metadata->isKey()}}
  {{mb_include module=system template=key_metadata/inc_vw_generate_key_form metadata=$metadata}}
{{elseif $metadata->isCertificate()}}
  {{mb_include module=system template=key_metadata/inc_vw_upload_cert_form metadata=$metadata}}
{{/if}}
