{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{if $is_last}}
  <script>
      reloadfield = function () {
          let timestamp = getForm("edit-configuration-{{$uid}}")['c[{{$_feature}}]'][1],
              user_split = User.view.split(" "),
              field = DateFormat.format(new Date(), timestamp.value).replace(/%p/g, user_split[1]);
          field = field.replace(/%n/g, user_split[0]);
          field = field.replace(/%i/g, user_split[1].charAt(0) + ". " + user_split[0].charAt(0) + ". ");
          $("preview").innerHTML = field;
      };
      addfield = function (name) {
          let timestamp = getForm("edit-configuration-{{$uid}}")['c[{{$_feature}}]'][1];
          if (!timestamp.disabled) {
              timestamp.value += name + " ";
          }
          reloadfield();
      };
      Main.add(function () {
          let timestamp = getForm("edit-configuration-{{$uid}}")['c[{{$_feature}}]'][1];
          timestamp.observe("keyup", reloadfield);
          reloadfield();
      });
  </script>
    <input type="text" name="c[{{$_feature}}]" value="{{$value}}" size="50" {{if $is_inherited}}disabled{{/if}}>
  <br>
  <div class="me-inline" id='preview'></div>
  <div id="horodatage">
    <table>
        {{assign var=horodatage value='Ox\Mediboard\CompteRendu\CConfigurationCompteRendu'|const:HORODATAGE}}
        {{foreach from=$horodatage item=_horodatage key=field}}
          <tr>
            <td>
              <a href="#1" onclick="addfield('{{$_horodatage}}');">{{$_horodatage}}</a>
            </td>
            <td>
                {{tr}}config-dPcompteRendu-CCompteRendu-{{$field}}{{/tr}}
            </td>
          </tr>
        {{/foreach}}
    </table>
  </div>
{{else}}
    {{if $value}}
        {{$value}}
    {{/if}}
{{/if}}
