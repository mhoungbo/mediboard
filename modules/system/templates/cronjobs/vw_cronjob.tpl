{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=system script=cronjob ajax=true}}

<script>
  Main.add(function () {
    Control.Tabs.create('tabs-cronjob', true, {
      afterChange: function (container) {
        switch (container.id) {
          case "tab_list_cronjobs":
            getForm('search_cronjob_list').onsubmit();
            break;

          case "tab_log_cronjobs":
            getForm('search_cronjob').onsubmit();
            break;
        }
      }
    });
  });

  submit_search = function (form) {
    $V(form.page, 0);
    return form.onsubmit();
  }
</script>

<ul id="tabs-cronjob" class="control_tabs">
  <li><a href="#tab_list_cronjobs">{{tr}}CCronJob.list{{/tr}}</a></li>
  <li><a href="#tab_log_cronjobs">{{tr}}CCronJobLog{{/tr}}</a></li>
</ul>

<div id="tab_list_cronjobs" style="display: none;">
  <button class="new me-margin-top-10" type="button"
          onclick="CronJob.edit(0, '{{url name=system_gui_cronjob_view_edit}}')">{{tr}}CCronJob.new{{/tr}}</button>

  <form name="search_cronjob_list" method="get" onsubmit="return onSubmitFormAjax(this, null, 'list_cronjobs')">
      {{mb_route name=system_gui_cronjob_list}}
    <input type="hidden" name="page" value="0">

    <table class="form">
      <tr>
        <th>{{mb_title object=$log_cron field="cronjob_id"}}</th>
        <td>
            {{mb_field object=$log_cron field="cronjob_id" canNull=true form="search_cronjob_list" autocomplete="true,1,50,true,true"}}
          <button type="button" class="erase notext"
                  onclick="$V(this.form.cronjob_id_autocomplete_view, ''); $V(this.form.cronjob_id, '');"></button>
        </td>

        <th>{{tr}}CCronJob-active{{/tr}}</th>
        <td>
          <select name="active_filter">
            <option value="1">{{tr}}Yes{{/tr}}</option>
            <option value="0">{{tr}}No{{/tr}}</option>
            <option value="all" selected>{{tr}}All{{/tr}}</option>
          </select>
        </td>
      </tr>

      <tr>
        <td colspan="10" class="button">
          <button type="submit" class="search" onclick="submit_search(this.form)">{{tr}}Search{{/tr}}</button>
        </td>
      </tr>
    </table>
  </form>

  <div id="list_cronjobs"></div>
</div>

<div id="tab_log_cronjobs">
  <form name="search_cronjob" method="get" onsubmit="return onSubmitFormAjax(this, null, 'search_log_cronjob')">
      {{mb_route name=system_gui_cronjob_logs}}
    <input type="hidden" name="page" value="0"/>

    <table class="form">
      <tr>
        <th>{{mb_title object=$log_cron field="cronjob_id"}}</th>
        <td>
            {{mb_field object=$log_cron field="cronjob_id" canNull=true form="search_cronjob" autocomplete="true,1,50,true,true"}}
          <button type="button" class="erase notext"
                  onclick="$V(this.form.cronjob_id_autocomplete_view, ''); $V(this.form.cronjob_id, '');"></button>
        </td>

        <th>{{mb_title object=$log_cron field="status"}}</th>
        <td>{{mb_field object=$log_cron field="status" canNull=true emptyLabel="Choose"}}</td>

        <th>{{mb_title object=$log_cron field="severity"}}</th>
        <td>{{mb_field object=$log_cron field="severity" canNull=true emptyLabel="Choose"}}</td>

        <th>Du</th>
        <td>{{mb_field object=$log_cron field="_date_min" form="search_cronjob" register=true}}</td>

        <th>jusqu'au</th>
        <td>{{mb_field object=$log_cron field="_date_max" form="search_cronjob" register=true}}</td>
      </tr>

      <tr>
        <td colspan="10" class="button">
          <button type="submit" class="search">{{tr}}Search{{/tr}}</button>
        </td>
      </tr>
    </table>
  </form>

  <div id="search_log_cronjob"></div>
</div>
