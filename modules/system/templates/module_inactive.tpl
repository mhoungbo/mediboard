{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<div class="big-warning">
  Ce module <strong>n'est pas activ�</strong> !
  <br/>
  Vous pouvez l'activer en allant dans le
  <a href="{{url name=system_gui_modules_index}}">Panneau d'administration des modules</a>
</div>
