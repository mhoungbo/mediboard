{{*
 * @package Mediboard\System
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=thcolspan value=null}}
{{mb_default var=tdcolspan value=null}}
{{mb_default var=value  value=null}}
{{mb_default var=m  value=null}}
{{mb_default var=class  value=null}}

{{mb_config_base var=$var m=$m class=$class}}

<tr>
  <th {{if $thcolspan}}colspan="{{$thcolspan}}"{{else}}style="width: 50%"{{/if}}>
    <label for="{{$field}}" title="{{tr}}{{$locale}}-desc{{/tr}}">
      {{tr}}{{$locale}}{{/tr}}
    </label>  
  </th>
  
  <td {{if $tdcolspan}}colspan="{{$tdcolspan}}"{{/if}}>
    {{unique_id var=uid}}

    <input name="{{$field}}" value="{{$value}}" type="hidden" />

    <script>
      Main.add(function(){
        var e = getForm('{{$form}}').elements['{{$field}}'];
        e.colorPicker({
          allowEmpty: true,
          change: function(color) {
            this.value = color ? color.toHexString() : '';
          }.bind(e)
        });
      });
    </script>
  </td>
</tr>
