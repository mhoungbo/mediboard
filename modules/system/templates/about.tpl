{{*
* @package Mediboard\System
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<style>
  body {
    background-color: var(--background-default-light);
  }

  #about {
    font-family: "Roboto", sans-serif;
    margin: 0 auto;
    padding-top: 50px;
    width: 750px;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
  }

  #about-logo {
    border-radius: 50%;
    display: inline-block;
    background: var(--primary-50);
    width: 100px;
    height: 100px;
  }

  #about-logo img {
    width: 70px;
    height: 70px;
    padding-top: 15px;
  }

  #about-product {
    color: var(--primary-500);
    font-size: 48px;
    font-weight: 400;
    line-height: 56px;
    padding-left: 15px;
    text-align: left;
  }

  #about-product-version {
    font-size: 20px;
    font-weight: 500;
    line-height: 28px;
    text-align: left;
    color: var(--primary-300);
  }

  #about-description {
    width: 625px;
    color: var(--color-on-background-medium);
    padding-left: 15px;
    text-align: left;
  }

  #about-description > span {
    color: var(--secondary-500);
  }
</style>

<div id="about">
  <div id="about-logo">
    <img src="./style/mediboard_ext/images/icons/logo.svg">
  </div>

  <div id="about-product">
      {{$product_name}}
    <div id="about-product-version">
        {{$version}}
    </div>
  </div>

  <div></div>

  <div id="about-description">
    <span> {{$product_name}} </span>
      {{$description|smarty:nodefaults}}
  </div>
</div>
