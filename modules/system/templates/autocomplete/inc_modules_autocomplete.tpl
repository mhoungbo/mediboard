{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<ul>
    {{if $common}}
      <li data-name="common" data-id="0" class="classAutocomplete-container">

        <div class="classAutocomplete">
          <div class="classAutocomplete-section expand">
            <div class="classAutocomplete-title">
              COMMON
            </div>

            <div class="classAutocomplete-subtitle">
              common
            </div>
          </div>

          <div class="classAutocomplete-section">
            <div class="classAutocomplete-extra">
              <div class="me-text-align-right opacity-50"></div>
            </div>
          </div>
        </div>

      </li>
    {{/if}}

    {{foreach from=$matches key=_key item=_match}}
      <li data-name="{{$_match->mod_name}}" data-id="{{$_match->mod_id}}" class="classAutocomplete-container">

        <div class="classAutocomplete">
          <div class="classAutocomplete-section expand">
            <div class="classAutocomplete-title">
                {{tr}}module-{{$_match->mod_name}}-court{{/tr}}
            </div>

            <div class="classAutocomplete-subtitle">
                {{tr}}module-{{$_match->mod_name}}-long{{/tr}}
            </div>
          </div>

          <div class="classAutocomplete-section">
            <div class="classAutocomplete-extra">
              <div class="me-text-align-right opacity-50">
                  {{if $profile !== 'active'}}
                      {{if $_match->mod_active}}
                        <i class="fas fa-check fa-lg fa-fw" style="color: #43A047FF;"></i>
                      {{else}}
                        <i class="fas fa-times fa-lg fa-fw" style="color: #F44336;"></i>
                      {{/if}}
                  {{/if}}
              </div>
            </div>
          </div>
        </div>

      </li>
    {{/foreach}}
</ul>






