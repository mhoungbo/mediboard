{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=page value=0}}

<script>
  Main.add(function () {
    const form = getForm('config-search');
    form.on('change', function () {
      $V(form.elements.start, 0)
    });
    form.on('ui:change', function () {
      $V(form.elements.start, 0)
    });
  });

  changePageConfig = function (page) {
    const form = getForm('config-search');
    $V(form.elements.start, page, false);
    form.onsubmit();
  };

  vwCompareConfigs = function () {
    new Url().setRoute('{{url name=system_config_search_compare}}', 'system_config_search_compare', 'system')
      .requestModal('90%', '90%');
  };

  exportConfigs = function () {
    new Url().setRoute('{{url name=system_config_search_export}}').popup(400, 150);
  }
</script>

<div align="right">
  <button type="button" class="download" onclick="exportConfigs()">{{tr}}CConfiguration-action-export{{/tr}}</button>
  <button type="button" class="hslip" onclick="vwCompareConfigs();">{{tr}}CConfiguration-action-compare{{/tr}}</button>
</div>

<form name="config-search" method="get" onsubmit="return onSubmitFormAjax(this, null, 'info')">
    {{mb_route name=system_config_search_list}}
  <input type="hidden" name="start" value="{{$page}}"/>

  <table class="main form">
    <tr>
      <th><label for="keywords">{{tr}}system-config-search keywords{{/tr}}</label></th>
      <td><input type="text" name="keywords" size="80" style="font-size: 1.4em;"/></td>
    </tr>

    <tr>
      <th>{{tr}}Type|pl{{/tr}}</th>
      <td>
        <input type="checkbox" name="configs" value="1" checked/>
        <label for="configs">{{tr}}Config|pl{{/tr}}</label>

        <input type="checkbox" name="prefs" value="1" checked/>
        <label for="prefs">{{tr}}Preferences{{/tr}}</label>

        <input type="checkbox" name="func_perms" value="1" checked/>
        <label for="func_perms">{{tr}}FunctionalPerms{{/tr}}</label>
      </td>
    </tr>

    <tr>
      <td class="button" colspan="2">
        <button class="button search me-primary">{{tr}}Search{{/tr}}</button>
      </td>
    </tr>
  </table>
</form>

<div id="info" class="me-padding-0"></div>
