<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CMbObject;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\System\CNote;

CCanDo::check();
$object_class = CView::get("object_class", "str");
$object_id    = CView::get("object_id", "ref class|$object_class");
$object_guid  = CView::get("object_guid", "str default|" . "$object_class-$object_id");

/** @var CMbObject $object */
$object = CMbObject::loadFromGuid($object_guid);

if (!$object || !$object->_id) {
    CAppUI::notFound($object_guid);
}
CView::checkin();
CView::enableSlave();

$object->needsRead();

$object->loadRefsNotes(PERM_READ);

$note_ids = [];
foreach ($object->_ref_notes as $_note) {
    $_note->loadRefUser()->loadRefFunction();

    if ($_note->getPerm(CPermObject::EDIT)) {
        $note_ids[] = $_note->_id;
    }
}

$delete_token = AntiCsrf::prepare()
    ->addParam('note_id', $note_ids)
    ->getToken();

// Cr�ation du template
$smarty = new CSmartyDP();
$smarty->assign("notes", $object->_ref_notes);
$smarty->assign("object", $object);
$smarty->assign("delete_token", $delete_token);
$smarty->display("vw_object_notes.tpl");
