<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CView;
use Ox\Mediboard\System\ViewSender\CViewSender;
use Ox\Mediboard\System\ViewSender\ViewSenderManager;

// DO NOT DELETE, MIGHT BE USED IN TOKENS
// @deprecated Should use route system_gui_view_senders_send instead

CCanDo::checkEdit();

$view_sender_id = CView::get("view_sender_id", "ref class|CViewSender notNull");

CView::checkin();

$sender = CViewSender::findOrFail($view_sender_id);

CApp::registerShutdown(ViewSenderManager::SHUTDOWN_CLEAR_FUNCTION);

if (!$sender->prepareAndSendFile()) {
    CAppUI::stepAjax('CViewSender-response-empty', CAppUI::UI_MSG_WARNING);
} else {
    CAppUI::stepAjax('CViewSender-msg-sent');
}

