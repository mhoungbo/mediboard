<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Factories;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\System\CPreferences;
use Ox\Mediboard\System\CUserAgent;
use Ox\Mediboard\System\CUserAuthentication;
use Ox\Mediboard\System\CUserAuthenticationError;

class UserAuthenticationFactory
{
    public function createSwitchLog(CUser $user): ?CUserAuthentication
    {
        if (!CUserAuthentication::authReady()) {
            return null;
        }

        CAppUI::isIntranet();

        return $this->create($user, CUserAuthentication::AUTH_METHOD_SUBSTITUTION, CAppUI::$instance->_ref_last_auth);
    }

    public function createSuccess(CUser $user, AuthMetadataBadge $metadata, ?string $uainfo): ?CUserAuthentication
    {
        if (!CUserAuthentication::authReady() || $user->dont_log_connection) {
            return null;
        }

        CAppUI::isIntranet();

        if (!$metadata->isStateful()) {
            return $this->createStateless($user, $metadata->getAuth());
        }

        return $this->create($user, $metadata->getAuth(), null, $uainfo);
    }

    private function create(
        CUser $user,
        string $method,
        CUserAuthentication $previous_auth = null,
        ?string $uainfo = null
    ): CUserAuthentication
    {
        $app          = CAppUI::$instance;

        // DO NOT use CAppUI::pref (CAppUI::$instance->user_id is not already set)
        $pref             = CPreferences::getPref('sessionLifetime', $user->_id);
        $session_lifetime = ($pref['used']) ? ($pref['used'] * 60) : null;
        $session_lifetime = ($session_lifetime) ?: (int)ini_get('session.gc_maxlifetime');

        $dtnow = CMbDT::dateTime();

        $auth                      = new CUserAuthentication();
        $auth->user_id             = $user->_id;
        $auth->previous_auth_id    = ($previous_auth && $previous_auth->_id) ? $previous_auth->_id : null;
        $auth->auth_method         = $method;
        $auth->datetime_login      = $dtnow;
        $auth->last_session_update = $dtnow;
        $auth->nb_update           = 0;
        $auth->ip_address          = $app->ip;

        $auth->session_id          = CApp::getSessionHelper()->getId();
        $auth->session_lifetime    = $session_lifetime;
        $auth->expiration_datetime = CMbDT::dateTime("+ {$session_lifetime} second");

        // Screen size
        if ($uainfo) {
            $uainfo = json_decode(stripslashes($uainfo), true);

            if (isset($uainfo["screen"])) {
                $screen              = $uainfo["screen"];
                $auth->screen_width  = (int)$screen[0];
                $auth->screen_height = (int)$screen[1];
            }
        }

        // User agent
        $user_agent            = CUserAgent::create(true);
        $auth->user_agent_id   = $user_agent->_id;
        $auth->_ref_user_agent = $user_agent;

        // In order
        CAppUI::$instance->user_prev_login = $user->getLastLogin();

        return $auth;
    }

    private function createStateless(CUser $user, string $auth_method): CUserAuthentication
    {
        // Aggregate per day stateless authentications
        $last_auth = $user->loadRefLastAuth();

        if (
            $last_auth
            && ($last_auth->auth_method === $auth_method)
            && str_starts_with($last_auth->session_id, 'stateless')
            && (CMbDT::date($last_auth->last_session_update) === CMbDT::date())
        ) {
            $last_auth->last_session_update = CMbDT::dateTime();
            $last_auth->nb_update++;
            $last_auth->expiration_datetime = CMbDT::dateTime();

            return $last_auth;
        }

        $dtnow = CMbDT::dateTime();

        $auth                      = new CUserAuthentication();
        $auth->user_id             = $user->_id;
        $auth->previous_auth_id    = null;
        $auth->auth_method         = $auth_method;
        $auth->datetime_login      = $dtnow;
        $auth->last_session_update = $dtnow;
        $auth->nb_update           = 0;
        $auth->ip_address          = CAppUI::$instance->ip;

        // For Legacy compat
        CAppUI::$instance->auth_method = $auth_method;

        // Api mode
        $auth->session_id          = uniqid('stateless', true);
        $auth->session_lifetime    = 0;
        $auth->expiration_datetime = $dtnow;

        // User agent
        $user_agent            = CUserAgent::create(true);
        $auth->user_agent_id   = $user_agent->_id;
        $auth->_ref_user_agent = $user_agent;

        // In order
        CAppUI::$instance->user_prev_login = $user->getLastLogin();

        return $auth;
    }

    /**
     * @param string   $identifier
     * @param string   $auth_method
     * @param int|null $user_id
     * @param string   $message
     *
     * @return CUserAuthenticationError
     */
    public function createError(
        string $identifier,
        string $auth_method,
        ?int   $user_id,
        string $message
    ): CUserAuthenticationError {
        CAppUI::isIntranet();

        $error              = new CUserAuthenticationError();
        $error->login_value = $identifier;
        $error->user_id     = $user_id;
        $error->message     = $message;
        $error->datetime    = CMbDT::dateTime();
        $error->auth_method = $auth_method;
        $error->ip_address  = CAppUI::$instance->ip;
        $error->identifier  = $error::makeIdentifier();

        return $error;
    }
}
