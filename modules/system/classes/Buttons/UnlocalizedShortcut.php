<?php

 /**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Buttons;

use Ox\Core\CAppUI;
use Ox\Core\Plugin\Button\AbstractAppBarButtonPlugin;
use Ox\Core\Plugin\Button\ButtonPluginManager;

class UnlocalizedShortcut extends AbstractAppBarButtonPlugin
{

    protected static function isActive(): bool
    {
        return CAppUI::conf('locale_warn');
    }

    public static function registerButtons(ButtonPluginManager $manager): void
    {
        if (self::isActive()) {
            $manager->registerComplex(
                'system-msg-unlocalized_warning',
                'translate',
                false,
                [self::LOCATION_APPBAR_SHORTCUT],
                0,
                'Localize.showForm',
                '',
                'Localize.initShortcut',
                0
            );
        }
    }
}
