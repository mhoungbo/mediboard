<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Keys;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use LogicException;
use Ox\Core\Security\Cert\Format;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotUseKey;
use Throwable;

/**
 * Todo: Handle PEM/DER formats with passphrase associated (and checking) when using private keys.
 */
class Certificate extends AbstractKey
{
    private const DATE_FORMAT = 'ymdHise';

    private string $file_path;

    private array $data = [];

    /**
     * @param CKeyMetadata $metadata
     * @param string       $value
     * @param string       $file_path
     *
     * @throws CouldNotUseKey
     */
    public function __construct(CKeyMetadata $metadata, string $value, string $file_path)
    {
        parent::__construct($metadata, $value);

        $this->file_path = $file_path;

        $this->parseRawData();
    }

    /**
     * @throws CouldNotUseKey
     */
    public static function createEmpty(string $name, Format $format, ?string $description = null): self
    {
        $metadata              = new CKeyMetadata();
        $metadata->name        = $name;
        $metadata->format      = $format->value;
        $metadata->description = $description;

        $metadata->updateFormFields();

        return new self($metadata, '', '');
    }

    public function getFormat(): Format
    {
        return $this->metadata->_format;
    }

    public function getPassphrase(): ?string
    {
        return $this->metadata->getPassphrase();
    }

    public function getPath(): string
    {
        return $this->file_path;
    }

    /**
     * Load the certificate with OpenSSL and keep the data.
     *
     * @return void
     * @throws CouldNotUseKey
     */
    private function parseRawData(): void
    {
        // When creating a fake object
        if ($this->value === '') {
            return;
        }

        // PKCS#12
        if ($this->isFormatPkcs12()) {
            try {
                $cert = [];

                openssl_pkcs12_read($this->value, $cert, ($this->getPassphrase()) ?? '');

                if (!isset($cert['cert'])) {
                    throw CouldNotUseKey::unableToLoad($this->metadata->name);
                }

                $data = openssl_x509_parse($cert['cert']);

                if ($data === false) {
                    throw new Exception();
                }

                $this->data = $data;
            } catch (Throwable) {
                throw CouldNotUseKey::unableToLoad($this->metadata->name);
            }
        } else {
            // PEM, DER
            try {
                // Converting DER to PEM in order to be able to use openssl_* functions
                $value = ($this->isFormatDer()) ? $this->derToPem($this->value, 'CERTIFICATE') : $this->value;
                $cert  = openssl_x509_read($value);

                if ($cert === false) {
                    throw new Exception();
                }

                $data = openssl_x509_parse($cert);

                if ($data === false) {
                    throw new Exception();
                }

                $this->data = $data;
            } catch (Throwable) {
                throw CouldNotUseKey::unableToLoad($this->metadata->name);
            }
        }
    }

    private function isFormatPkcs12(): bool
    {
        return $this->getFormat()->value === Format::PKCS12->value;
    }

    private function isFormatDer(): bool
    {
        return $this->getFormat()->value === Format::DER->value;
    }

    public function validateDate(DateTimeInterface $date = null): bool
    {
        $date = ($date) ?? new DateTimeImmutable();

        return (($date >= $this->getValidFrom()) && ($date <= $this->getValidTo()));
    }

    public function getName(): string
    {
        return $this->getAttribute('name');
    }

    public function getSubjectDn(): array
    {
        return $this->getAttribute('subject');
    }

    public function getIssuerDn(): array
    {
        return $this->getAttribute('issuer');
    }

    public function getValidFrom(): ?DateTimeImmutable
    {
        return $this->getExpirationAttribute('validFrom');
    }

    public function getValidTo(): ?DateTimeImmutable
    {
        return $this->getExpirationAttribute('validTo');
    }

    /**
     * @param string $attribute
     *
     * @return mixed|null
     */
    private function getAttribute(string $attribute): mixed
    {
        return ($this->data[$attribute]) ?? null;
    }

    private function getExpirationAttribute(string $attribute): ?DateTimeImmutable
    {
        $date = $this->getAttribute($attribute);

        // Todo: I do not think null is a possible value
        if ($date === null) {
            return null;
        }

        return DateTimeImmutable::createFromFormat(self::DATE_FORMAT, $date);
    }

    /**
     * Todo: Refactor with normal parsing.
     *
     * @param string      $certificate
     * @param Format      $format
     * @param string|null $passphrase
     *
     * @return bool
     */
    public static function checkPassphrase(string $certificate, Format $format, ?string $passphrase): bool
    {
        // We do not handle encrypted DER
        if (($format->value === Format::DER->value)) {
            if ($passphrase) {
                throw new LogicException('Certificate-error-Encrypted DER not supported');
            }

            return true;
        }

        // PKCS#12
        if ($format->value === Format::PKCS12->value) {
            // We refuse PKCS#12 without passphrase
            if (!$passphrase) {
                return false;
            }

            try {
                $cert = [];
                openssl_pkcs12_read($certificate, $cert, $passphrase);

                if (!isset($cert['cert'])) {
                    return false;
                }
            } catch (Throwable) {
                return false;
            }

            return true;
        }

        // PEM
        try {
            // If no passphrase is provided, we assume that the cert is not encrypted
            if (!$passphrase) {
                return true;
            }

            // Using only raw content of the certificate here. Seems not documented.
            if (openssl_pkey_get_private($certificate, $passphrase) === false) {
                return false;
            }
        } catch (Throwable) {
            return false;
        }

        return true;
    }

    private function derToPem(string $der, string $type): string
    {
        $pem = chunk_split(base64_encode($der), 64, "\n");

        return "-----BEGIN {$type}-----\n{$pem}-----END {$type}-----\n";
    }
}
