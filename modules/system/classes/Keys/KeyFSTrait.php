<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Keys;

use Ox\Core\CAppUI;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotGetKeysFromStorage;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotUseKey;
use Throwable;

/**
 * Simple trait for key on FS manipulations.
 */
trait KeyFSTrait
{
    /**
     * @param string $name
     *
     * @return string
     * @throws CouldNotGetKeysFromStorage
     */
    protected function getKeyPath(string $name): string
    {
        return $this->getFilePath($name, 'key');
    }

    /**
     * @param string $name
     *
     * @return string
     * @throws CouldNotGetKeysFromStorage
     */
    protected function getCertificatePath(string $name): string
    {
        return $this->getFilePath($name, 'cert');
    }

    /**
     * @param string $name
     * @param string $type
     *
     * @return string
     * @throws CouldNotGetKeysFromStorage
     */
    private function getFilePath(string $name, string $type): string
    {
        return $this->getDirectoryPath() . $name . DIRECTORY_SEPARATOR . $type;
    }

    /**
     * @return string
     * @throws CouldNotGetKeysFromStorage
     */
    protected function getDirectoryPath(): string
    {
        try {
            $dir = CAppUI::conf('system KeyChain directory_path', 'static');
        } catch (Throwable $t) {
            throw CouldNotGetKeysFromStorage::unableToRetrieveStorage();
        }

        if (!$dir) {
            throw CouldNotGetKeysFromStorage::unableToRetrieveStorage();
        }

        if (!is_dir($dir)) {
            throw CouldNotGetKeysFromStorage::storageIsNotADirectory($dir);
        }

        if (!is_writeable($dir)) {
            throw CouldNotGetKeysFromStorage::storageIsNotWriteable($dir);
        }

        return rtrim($dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }

    /**
     * @param string       $key_path
     * @param CKeyMetadata $metadata
     *
     * @return string
     * @throws CouldNotUseKey
     */
    protected function readFile(string $key_path, CKeyMetadata $metadata): string
    {
        if (!is_readable($key_path)) {
            throw CouldNotUseKey::notReadable($metadata);
        }

        $value = file_get_contents($key_path);

        if ($value === false) {
            throw CouldNotUseKey::isEmpty($metadata);
        }

        return $value;
    }

    /**
     * @param string $key_path
     *
     * @return bool
     */
    protected function fileExists(string $key_path): bool
    {
        return file_exists($key_path);
    }
}
