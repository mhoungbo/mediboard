<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Keys;

use Exception;
use Ox\Core\CApp;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotGetKeysFromStorage;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotUseKey;

/**
 * Key accessor.
 *
 * Allow us to retrieve keys from storage.
 */
class KeyStore
{
    use KeyFSTrait;

    /**
     * @param string $name
     *
     * @return Certificate|Key
     * @throws CouldNotUseKey
     * @throws Exception
     */
    public function load(string $name): Certificate|Key
    {
        try {
            return $this->getKey($name);
        } catch (CouldNotUseKey) {
        }

        try {
            return $this->getCertificate($name);
        } catch (CouldNotUseKey) {
        }

        throw CouldNotUseKey::unableToLoad($name);
    }

    /**
     * @param string $name
     *
     * @return Key
     * @throws CouldNotUseKey
     *
     * @deprecated Use KeyStore::getKey instead.
     */
    public static function loadKey(string $name): Key
    {
        return (new self())->getKey($name);
    }

    /**
     * @param string $name
     *
     * @return Certificate
     * @throws CouldNotUseKey
     *
     * @deprecated Use KeyStore::getCertificate instead.
     */
    public static function loadCertificate(string $name): Certificate
    {
        return (new self())->getCertificate($name);
    }

    /**
     * @param string $name
     *
     * @return Key
     * @throws CouldNotUseKey
     * @throws Exception
     */
    public function getKey(string $name): Key
    {
        $metadata = $this->getMetadata($name);

        if (!$metadata->isKey()) {
            throw CouldNotUseKey::invalidType($metadata, $metadata::TYPE_KEY);
        }

        try {
            $key_path = $this->getKeyPath($metadata);
        } catch (CouldNotGetKeysFromStorage) {
            throw CouldNotUseKey::doesNotExist($metadata);
        }

        $start = microtime(true);

        if (!$this->fileExists($key_path)) {
            throw CouldNotUseKey::doesNotExist($metadata);
        }

        $value = $this->readFile($key_path, $metadata);

        CApp::log(
            sprintf(
                "Getting key '%s' in %.2f ms",
                $name,
                (microtime(true) - $start) * 1000
            )
        );

        return new Key($metadata, $value);
    }

    /**
     * @param string $name
     *
     * @return Certificate
     * @throws CouldNotUseKey
     * @throws Exception
     */
    public function getCertificate(string $name): Certificate
    {
        $metadata = $this->getMetadata($name);

        if (!$metadata->isCertificate()) {
            throw CouldNotUseKey::invalidType($metadata, $metadata::TYPE_CERTIFICATE);
        }

        try {
            $key_path = $this->getCertificatePath($metadata);
        } catch (CouldNotGetKeysFromStorage) {
            throw CouldNotUseKey::doesNotExist($metadata);
        }

        $start = microtime(true);

        if (!$this->fileExists($key_path)) {
            throw CouldNotUseKey::doesNotExist($metadata);
        }

        $value = $this->readFile($key_path, $metadata);

        CApp::log(
            sprintf(
                "Getting certificate '%s' in %.2f ms",
                $name,
                (microtime(true) - $start) * 1000
            )
        );

        return new Certificate($metadata, $value, $key_path);
    }

    /**
     * @param string $name
     *
     * @return CKeyMetadata
     * @throws CouldNotUseKey
     */
    protected function getMetadata(string $name): CKeyMetadata
    {
        return CKeyMetadata::loadFromName($name);
    }
}
