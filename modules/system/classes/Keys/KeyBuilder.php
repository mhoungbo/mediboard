<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Keys;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CMbPath;
use Ox\Core\Security\Cert\Format;
use Ox\Core\Security\Crypt\Alg;
use Ox\Core\Security\Crypt\Hash;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Core\Security\Crypt\Mode;
use Ox\Core\Security\Exception\CouldNotHash;
use Ox\Core\Security\Random\Random;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotGenerateKey;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotGetKeysFromStorage;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotPersistKey;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotUseKey;
use Throwable;

/**
 * Todo: Simplify persisting methods between Setup, Controller and manual call.
 *
 * Key builder.
 *
 * Performs key metadata storage, key generation and key storage.
 */
class KeyBuilder
{
    use KeyFSTrait;

    // Should always use a key length of 32 bytes with AES.
    private const SYMMETRIC_KEY_LENGTH = 32;

    private Hasher $hasher;
    private Random $random;

    /**
     * @param Hasher $hasher
     * @param Random $random
     */
    public function __construct(Hasher $hasher, Random $random)
    {
        $this->hasher = $hasher;
        $this->random = $random;
    }

    /**
     * @param string      $name
     * @param Alg         $alg
     * @param Mode        $mode
     * @param string|null $description
     *
     * @return void
     * @throws CouldNotPersistKey
     * @internal Must only be used by CSetup.
     *
     */
    public function persistKeyMetadata(string $name, Alg $alg, Mode $mode, ?string $description = null): void
    {
        $metadata              = new CKeyMetadata();
        $metadata->name        = $name;
        $metadata->alg         = $alg->value;
        $metadata->mode        = $mode->value;
        $metadata->description = $description;

        // Keep creation date empty!

        try {
            // Let CKeyMetadata handle persistence checks
            $this->storeMetadata($metadata);
        } catch (Throwable) {
            throw CouldNotPersistKey::unableToStoreMetadata();
        }
    }

    /**
     * @param string      $name
     * @param Format      $format
     * @param string|null $description
     *
     * @return CKeyMetadata
     * @throws CouldNotPersistKey
     * @internal Must only be used by CSetup.
     */
    public function persistCertificateMetadata(string $name, Format $format, ?string $description = null): CKeyMetadata
    {
        $metadata              = new CKeyMetadata();
        $metadata->name        = $name;
        $metadata->format      = $format->value;
        $metadata->description = $description;

        // Keep creation date empty!

        try {
            // Let CKeyMetadata handle persistence checks
            $this->storeMetadata($metadata);
        } catch (Throwable) {
            throw CouldNotPersistKey::unableToStoreMetadata();
        }

        return $metadata;
    }

    /**
     * @param CKeyMetadata $metadata
     *
     * @throws CouldNotGenerateKey
     * @throws CouldNotGetKeysFromStorage
     * @throws CouldNotPersistKey|CouldNotHash
     */
    public function generateKey(CKeyMetadata $metadata): void
    {
        if ($metadata->isAlgSymmetric()) {
            $key = $this->generateRandom(self::SYMMETRIC_KEY_LENGTH);

            $this->persistSymmetricKey($metadata, $key);

            return;
        }

        throw CouldNotGenerateKey::invalidAlg($metadata);
    }

    /**
     * @param int $length
     *
     * @return string
     * @throws CouldNotGenerateKey
     */
    protected function generateRandom(int $length): string
    {
        try {
            return $this->random->getBytes($length);
        } catch (Throwable $t) {
            throw CouldNotGenerateKey::errorDuringGeneration($t->getMessage());
        }
    }

    /**
     * @param CKeyMetadata $metadata
     * @param string       $key
     *
     * @throws CouldNotGetKeysFromStorage
     * @throws CouldNotPersistKey|CouldNotHash
     * @throws Exception
     */
    protected function persistSymmetricKey(CKeyMetadata $metadata, string $key): void
    {
        $key_path = $this->getKeyPath($metadata);

        if (file_exists($key_path)) {
            throw CouldNotPersistKey::alreadyExists($metadata->name);
        }

        if (CMbPath::forceDir(dirname($key_path)) === false) {
            throw CouldNotPersistKey::unableToCreateKeyStorage($metadata);
        }

        if (!is_writeable(dirname($key_path))) {
            throw CouldNotPersistKey::dirNotWriteable($metadata);
        }

        if (file_put_contents($key_path, $key) === false) {
            throw CouldNotPersistKey::unableToWriteKey($metadata);
        }

        $metadata->last_update = CMbDT::dateTime();
        $metadata->hash        = $this->hasher->hash(Hash::SHA256, $key);

        try {
            $this->storeMetadata($metadata);
        } catch (Throwable) {
            CMbPath::remove($key_path, true);

            throw CouldNotPersistKey::unableToStoreMetadata();
        }
    }

    /**
     * @param CKeyMetadata $metadata
     * @param string       $certificate
     * @param string|null  $passphrase
     *
     * @throws CouldNotGetKeysFromStorage
     * @throws CouldNotHash
     * @throws CouldNotPersistKey
     * @throws Exception
     */
    public function persistCertificate(CKeyMetadata $metadata, string $certificate, ?string $passphrase = null): void
    {
        $cert_path = $this->getCertificatePath($metadata);

        if (file_exists($cert_path)) {
            throw CouldNotPersistKey::alreadyExists($metadata->name);
        }

        if (CMbPath::forceDir(dirname($cert_path)) === false) {
            throw CouldNotPersistKey::unableToCreateKeyStorage($metadata);
        }

        $this->writeFile($metadata, $cert_path, $certificate);

        $metadata->last_update = CMbDT::dateTime();
        $metadata->hash        = $this->hasher->hash(Hash::SHA256, $certificate);

        if ($passphrase) {
            $metadata->_passphrase = $passphrase;
        }

        try {
            $this->storeMetadata($metadata);
        } catch (Throwable) {
            CMbPath::remove($cert_path, true);

            // Passphrase may not have been stored, so we revert the metadata to its initial state
            $this->resetMetadata($metadata);

            throw CouldNotPersistKey::unableToStoreMetadata();
        }
    }

    /**
     * @throws CouldNotPersistKey
     * @throws CouldNotGetKeysFromStorage
     * @throws CouldNotHash
     * @throws Exception
     */
    public function renewCertificate(
        CKeyMetadata $metadata,
        string       $certificate,
        Format       $format,
        ?string      $passphrase = null
    ): void {
        // Todo: Check if $certificate is empty

        $cert_path = $this->getCertificatePath($metadata);

        // Do not check if file exists as we are renewing it
        // Do not try to create key directory as we are renewing it

        $this->writeFile($metadata, $cert_path, $certificate);

        $metadata->last_update = CMbDT::dateTime();
        $metadata->hash        = $this->hasher->hash(Hash::SHA256, $certificate);
        $metadata->format      = $format->value;

        if ($passphrase) {
            $metadata->_passphrase = $passphrase;
        }

        try {
            $this->storeMetadata($metadata);
        } catch (Throwable) {
            CMbPath::remove($cert_path, true);

            // Passphrase may not have been stored, so we revert the metadata to its initial state
            $this->resetMetadata($metadata);

            throw CouldNotPersistKey::unableToStoreMetadata();
        }
    }

    /**
     * @param CKeyMetadata $metadata
     * @param KeyStore     $store
     *
     * @return void
     * @throws CouldNotHash
     * @throws CouldNotPersistKey
     * @throws CouldNotUseKey
     * @internal Used for backward compatible hash computation.
     *
     */
    public function computeHash(CKeyMetadata $metadata, KeyStore $store): void
    {
        // Do nothing if already hashed
        if ($metadata->hash) {
            return;
        }

        $key = $store->load($metadata->name);

        $metadata->hash = $this->hasher->hash(Hash::SHA256, $key->getValue());

        try {
            $this->storeMetadata($metadata);
        } catch (Throwable) {
            throw CouldNotPersistKey::unableToComputeHash();
        }
    }

    /**
     * @param CKeyMetadata $metadata
     * @param string       $file_path
     * @param string       $content
     *
     * @return void
     * @throws CouldNotPersistKey
     */
    private function writeFile(CKeyMetadata $metadata, string $file_path, string $content): void
    {
        if (!is_writeable(dirname($file_path))) {
            throw CouldNotPersistKey::dirNotWriteable($metadata);
        }

        if (file_put_contents($file_path, $content) === false) {
            throw CouldNotPersistKey::unableToWriteKey($metadata);
        }
    }

    /**
     * @param CKeyMetadata $metadata
     *
     * @return void
     * @throws Exception
     */
    private function storeMetadata(CKeyMetadata $metadata): void
    {
        if ($msg = $metadata->store()) {
            throw new Exception($msg);
        }
    }

    /**
     * @param CKeyMetadata $metadata
     *
     * @return void
     * @throws Exception
     */
    private function resetMetadata(CKeyMetadata $metadata): void
    {
        $metadata->last_update = '';
        $metadata->hash        = '';

        $this->storeMetadata($metadata);
    }

    /**
     * Helper for persisting a given certificate manually.
     *
     * @throws CouldNotPersistKey
     * @throws CouldNotGetKeysFromStorage
     * @throws CouldNotHash
     */
    public function persistNewCert(
        string  $name,
        Format  $format,
        ?string $description,
        string  $certificate,
        ?string $passphrase = null
    ): void {
        if (!Certificate::checkPassphrase($certificate, $format, $passphrase)) {
            throw CouldNotPersistKey::invalidFormatOrPassphrase();
        }
        $metadata = $this->persistCertificateMetadata($name, $format, $description);
        $this->persistCertificate($metadata, $certificate, $passphrase);
    }

    /**
     * Helper for renewing an already persisted certificate manually.
     *
     * @throws CouldNotUseKey
     * @throws CouldNotPersistKey
     * @throws CouldNotGetKeysFromStorage
     * @throws CouldNotHash
     */
    public function renewCert(
        string  $name,
        Format  $format,
        string  $certificate,
        ?string $passphrase = null
    ): void {
        if (!Certificate::checkPassphrase($certificate, $format, $passphrase)) {
            throw CouldNotPersistKey::invalidFormatOrPassphrase();
        }

        $this->renewCertificate(CKeyMetadata::loadFromName($name), $certificate, $format, $passphrase);
    }
}
