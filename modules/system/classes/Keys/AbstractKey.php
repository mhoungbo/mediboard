<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Keys;

abstract class AbstractKey
{
    protected CKeyMetadata $metadata;

    protected string $value;

    /**
     * @param CKeyMetadata $metadata
     * @param string       $value
     */
    public function __construct(CKeyMetadata $metadata, string $value)
    {
        $this->metadata = $metadata;
        $this->value    = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getKeyName(): string
    {
        return $this->metadata->name;
    }

    public function getDescription(): ?string
    {
        return $this->metadata->description;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->metadata->name;
    }
}
