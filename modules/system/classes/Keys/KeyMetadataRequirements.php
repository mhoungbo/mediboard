<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Keys;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\Module\Requirements\CRequirementsManager;

class KeyMetadataRequirements extends CRequirementsManager
{
    /**
     * @throws Exception
     */
    public function checkKeyMetadataToPersist(): void
    {
        $keys_metadata = (new CKeyMetadata())->loadList() ?? [];
        foreach ($keys_metadata as $metadata) {
            $this->assertTrue(
                $metadata->hasBeenPersisted(),
                CAppUI::tr('CKeyMetadata-msg-Key is persisted', $metadata->name)
            );
        }
    }
}
