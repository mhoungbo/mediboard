<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Keys;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\EncryptedPropertiesInterface;
use Ox\Core\Exceptions\CanNotMerge;
use Ox\Core\Exceptions\CouldNotMerge;
use Ox\Core\Model\EncryptedPropertyAccessorTrait;
use Ox\Core\Security\Cert\Format;
use Ox\Core\Security\Crypt\Alg;
use Ox\Core\Security\Crypt\Hash;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Core\Security\Crypt\Mode;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\System\CMergeLog;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotUseKey;
use Throwable;

/**
 * Encryption key metadata.
 */
class CKeyMetadata extends CMbObject implements EncryptedPropertiesInterface
{
    use EncryptedPropertyAccessorTrait;

    public const NAME_MIN_LENGTH  = 4;
    public const TYPE_KEY         = 'key';
    public const TYPE_CERTIFICATE = 'certificate';

    private const TYPES = [
        self::TYPE_KEY,
        self::TYPE_CERTIFICATE,
    ];

    private const STATUS_TO_GENERATE  = 'to_generate';
    private const STATUS_OK           = 'ok';
    private const STATUS_MISSING      = 'missing';
    private const STATUS_HASH         = 'hash';
    private const STATUS_CERT_EXPIRED = 'cert_expired';

    private const STATUSES = [
        self::STATUS_TO_GENERATE,
        self::STATUS_OK,
        self::STATUS_MISSING,
        self::STATUS_HASH,
        self::STATUS_CERT_EXPIRED,
    ];

    /** @var int Primary key */
    public $key_metadata_id;

    /** @var string */
    public $name;

    /** @var string|null */
    public $alg;

    /** @var string|null */
    public $mode;

    /** @var string|null */
    public $format;

    /** @var int|null */
    public $passphrase_id;

    /** @var string */
    public $hash;

    /** @var string Datetime of last generation / renewing of the Key on the FS */
    public $last_update;

    /** @var string|null */
    public $description;

    /** @var Alg|null */
    public $_alg;

    /** @var Mode|null */
    public $_mode;

    /** @var Format|null */
    public $_format;

    public $_passphrase;

    public $_type;
    public $_status;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table           = 'key_metadata';
        $spec->key             = 'key_metadata_id';
        $spec->uniques['name'] = ['name'];
        $spec->anti_csrf       = true;

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props                  = parent::getProps();
        $props['name']          = 'str minLength|' . self::NAME_MIN_LENGTH . ' notNull';
        $props['alg']           = 'str';
        $props['mode']          = 'str';
        $props['format']        = 'str';
        $props['hash']          = 'str length|64';
        $props['passphrase_id'] = 'password key|master formField|_passphrase';
        $props['last_update']   = 'dateTime index|0';
        $props['description']   = 'text';

        $props['_passphrase'] = 'password';

        $props['_alg']    = 'enum list|' . implode('|', Alg::values());
        $props['_mode']   = 'enum list|' . implode('|', Mode::values());
        $props['_format'] = 'enum list|' . implode('|', Format::values());

        $props['_type']   = 'enum list|' . implode('|', self::TYPES);
        $props['_status'] = 'enum list|' . implode('|', self::STATUSES);

        return $props;
    }

    /**
     * @inheritDoc
     * @throws CouldNotUseKey
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->name;

        if ($this->alg) {
            $this->_alg = Alg::from($this->alg);
        }

        if ($this->mode) {
            $this->_mode = Mode::from($this->mode);
        }

        if ($this->format) {
            $this->_format = Format::from($this->format);
        }

        $this->_type = match (true) {
            $this->isKey()         => self::TYPE_KEY,
            $this->isCertificate() => self::TYPE_CERTIFICATE,
            default                => throw CouldNotUseKey::unknownType($this),
        };
    }

    public function isKey(): bool
    {
        return (($this->alg !== null) && ($this->mode !== null));
    }

    public function isCertificate(): bool
    {
        return (!$this->isKey() && ($this->format !== null));
    }

    public function getType(): string
    {
        return $this->_type;
    }

    public function hasBeenPersisted(): bool
    {
        return (($this->_id !== null) && ($this->last_update !== null));
    }

    /**
     * Tell whether a key is allowed to be re-generated/re-uploaded.
     *
     * @return bool
     */
    public function canBeRenewed(): bool
    {
        return $this->isCertificate();
    }

    /**
     * @param string $name
     *
     * @return static
     * @throws CouldNotUseKey
     */
    public static function loadFromName(string $name): self
    {
        if (!$name) {
            throw CouldNotUseKey::metadataNotFound($name);
        }

        $self       = new static();
        $self->name = $name;
        $self->loadMatchingObjectEsc();

        if (!$self->_id) {
            throw CouldNotUseKey::metadataNotFound($name);
        }

        return $self;
    }

    public function isAlgSymmetric(): bool
    {
        return ($this->_alg && $this->_alg->isSymmetric());
    }

    public function doesAlgNeedIv(): bool
    {
        return $this->_alg->needsIv();
    }

    /**
     * @inheritDoc
     */
    public function check(): ?string
    {
        if (
            !$this->name
            || !is_string($this->name)
            || str_contains($this->name, ' ')
            || !preg_match('/^[a-zA-Z0-9_\-]+$/', $this->name)
        ) {
            return 'CKeyMetadata-error-Invalid name';
        }

        return parent::check();
    }

    /**
     * @inheritDoc
     */
    public function store(): ?string
    {
        if (!CUser::get()->isAdmin()) {
            return 'CKeyMetadata-error-Only admin user can alter this object.';
        }

        return parent::store();
    }

    /**
     * @inheritDoc
     */
    public function delete(): ?string
    {
        if (!CUser::get()->isAdmin()) {
            return 'CKeyMetadata-error-Only admin user can alter this object.';
        }

        return parent::delete();
    }

    /**
     * @inheritDoc
     */
    public function rawStore(): bool
    {
        if (!CUser::get()->isAdmin()) {
            return false;
        }

        return parent::rawStore();
    }

    /**
     * @inheritDoc
     */
    public function checkMerge(array $objects = []): void
    {
        throw CanNotMerge::invalidObject();
    }

    /**
     * @inheritDoc
     */
    public function merge(array $objects, bool $fast, CMergeLog $merge_log): void
    {
        throw CouldNotMerge::mergeImpossible();
    }

    public function checkStatus(): void
    {
        if (!$this->hasBeenPersisted()) {
            $this->_status = self::STATUS_TO_GENERATE;

            return;
        }

        if (!$this->checkStorage()) {
            $this->_status = self::STATUS_MISSING;
        } elseif (!$this->checkIntegrity()) {
            $this->_status = self::STATUS_HASH;
        } elseif ($this->isCertificate() && !$this->checkCertificateValidity()) {
            $this->_status = self::STATUS_CERT_EXPIRED;
        } else {
            $this->_status = self::STATUS_OK;
        }
    }

    private function checkStorage(): bool
    {
        if (!$this->hasBeenPersisted()) {
            return false;
        }

        try {
            (new KeyStore())->load($this->name);

            return true;
        } catch (Throwable) {
            return false;
        }
    }

    private function checkIntegrity(): bool
    {
        if (!$this->hasBeenPersisted()) {
            return false;
        }

        $hasher = new Hasher();

        try {
            $key = (new KeyStore())->load($this->name);

            return ($this->hash && $hasher->equals($this->hash, $hasher->hash(Hash::SHA256, $key->getValue())));
        } catch (Throwable) {
            return false;
        }
    }

    private function checkCertificateValidity(): bool
    {
        if (!$this->hasBeenPersisted() || !$this->isCertificate()) {
            return false;
        }

        try {
            $cert = KeyStore::loadCertificate($this->name);

            return $cert->validateDate();
        } catch (Throwable) {
            return false;
        }
    }

    public function getPassphrase(): ?string
    {
        return self::getEncryptedProperty($this, 'passphrase_id');
    }
}
