<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Keys;

use Ox\Core\Security\Crypt\Alg;
use Ox\Core\Security\Crypt\Mode;

/**
 * Todo: Should a Key be able to generate IVs according to its specifications?
 *
 * Key representation.
 * Metadata are information about algorithm, encryption mode, etc.
 */
class Key extends AbstractKey
{
    /**
     * @param string      $name
     * @param Alg         $alg
     * @param Mode        $mode
     * @param string|null $description
     *
     * @return static
     */
    public static function createEmpty(string $name, Alg $alg, Mode $mode, ?string $description = null): self
    {
        $metadata              = new CKeyMetadata();
        $metadata->name        = $name;
        $metadata->alg         = $alg->value;
        $metadata->mode        = $mode->value;
        $metadata->description = $description;

        $metadata->updateFormFields();

        return new self($metadata, '');
    }

    public function getAlg(): Alg
    {
        return $this->metadata->_alg;
    }

    public function getMode(): Mode
    {
        return $this->metadata->_mode;
    }
}
