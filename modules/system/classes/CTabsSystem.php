<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Ox\Core\Module\AbstractTabsRegister;

/**
 * @codeCoverageIgnore
 */
class CTabsSystem extends AbstractTabsRegister
{
    public function registerAll(): void
    {
        $this->registerRoute('system_gui_modules_index', TAB_ADMIN);
        $this->registerRoute("system_gui_cache", TAB_ADMIN);
        $this->registerRoute("system_gui_messages", TAB_READ);
        $this->registerFile("object_merger", TAB_ADMIN);
        $this->registerFile("view_history", TAB_READ);
        $this->registerFile("vw_monitoring", TAB_ADMIN);
        $this->registerRoute("system_gui_locales", TAB_EDIT);
        $this->registerRoute("system_gui_view_senders_index", TAB_EDIT);
        $this->registerRoute("system_gui_cronjob_index", TAB_ADMIN);
        $this->registerRoute('system_gui_datasources_index', TAB_ADMIN);
        $this->registerRoute('system_config_search_index', TAB_ADMIN);
        $this->registerRoute('system_gui_key_metadata_list', TAB_ADMIN);
        $this->registerRoute("system_gui_about", TAB_NONE);
        $this->registerFile("configure", TAB_ADMIN, self::TAB_CONFIGURE);
    }
}
