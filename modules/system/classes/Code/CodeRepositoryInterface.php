<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Code;

/**
 * Should allow to search in Ox\Mediboard\System\CodeInterface objects and return an array of found Ox\Mediboard\System\Code.
 */
interface CodeRepositoryInterface
{
    /**
     * Return the type of code that will be search by this repository.
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Search codes in the repository.
     *
     * @param string $keywords
     * @param int    $offset
     * @param int    $limit
     *
     * @return Code[]
     */
    public function find(string $keywords, int $offset = 0, int $limit = 10): array;

    /**
     * The repository's module name.
     *
     * @return string
     */
    public function getModuleName(): string;
}
