<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Code;

/**
 * Codes that can be requested by an API.
 */
interface CodeInterface
{
    /**
     * Return the type of the code (ccam, cim10, postal code, ...).
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Return the code.
     *
     * @return string
     */
    public function getCodeValue(): string;

    /**
     * Return the description of the code.
     *
     * @return string
     */
    public function getDescription(): string;
}
