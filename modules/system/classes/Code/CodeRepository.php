<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Code;

use Exception;
use InvalidArgumentException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFilter;
use Ox\Core\CClassMap;
use Ox\Core\Module\CModule;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Gather repositories for codes of installed modules.
 */
class CodeRepository implements CodeRepositoryInterface
{
    /** @var CodeRepositoryInterface[] */
    private array     $repositories = [];
    private CClassMap $class_map;

    /**
     * @param CClassMap                 $class_map
     * @param array                     $types
     * @param CodeRepositoryInterface[] $repositories
     *
     * @throws Exception
     */
    public function __construct(
        ?CClassMap $class_map = null,
        array      $types = [],
        array      $repositories = []
    ) {
        $this->class_map = $class_map ?? CClassMap::getInstance();

        if ([] !== $repositories) {
            foreach ($repositories as $repository) {
                if (!$repository instanceof CodeRepositoryInterface) {
                    throw new InvalidArgumentException(
                        'Expected an object of type CodeRepositoryInterface, got ' . get_class($repository)
                    );
                }

                $this->repositories[] = $repository;
            }
        } else {
            $this->setRepositories($types);
        }
    }

    /**
     * Build using a GUI request with the parameter "types" for allowing only a subset of types.
     *
     * @param CClassMap    $class_map
     * @param RequestStack $stack
     *
     * @return static
     * @throws Exception
     */
    public static function fromRequestStack(CClassMap $class_map, RequestStack $stack): self
    {
        return new self($class_map, $stack->getCurrentRequest()->get('types', []));
    }

    /**
     * Build using a RequestApi with the filter type for allowing only a subset of types.
     *
     * @param CClassMap  $class_map
     * @param RequestApi $request
     *
     * @return static
     * @throws Exception
     */
    public static function fromRequestApi(CClassMap $class_map, RequestApi $request): self
    {
        $types = [];

        if ($filter = $request->getRequestFilter()->getFilter('types', RequestFilter::FILTER_IN)) {
            $types = $filter->getValues();
        }

        return new self($class_map, $types);
    }

    /**
     * @inheritdoc
     */
    public function find(string $keywords, int $offset = 0, int $limit = 10): array
    {
        $codes = [];
        foreach ($this->repositories as $repository) {
            $codes = array_merge($codes, $repository->find($keywords, $offset, $limit));
        }

        return $codes;
    }

    /**
     * Get all the classes that implement CodeRepositoryInterface in active modules.
     *
     * @param array $types Accepted repository types or empty array for all.
     *
     * @return void
     * @throws Exception
     */
    private function setRepositories(array $types = []): void
    {
        // Reset repositories
        $this->repositories = [];

        /** @var string[] $repositories */
        $repositories = $this->class_map->getClassChildren(CodeRepositoryInterface::class, false, true);

        foreach ($repositories as $repository) {
            if (self::class !== $repository) {
                $repository = $this->createInstance($repository);

                $module = CModule::getActive($repository->getModuleName());
                if (
                    $module instanceof CModule && $module->getPerm(PERM_READ)
                    && ([] === $types || in_array($repository->getType(), $types))
                ) {
                    $this->repositories[] = $repository;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getType(): string
    {
        return 'code';
    }

    /**
     * @inheritdoc
     */
    public function getModuleName(): string
    {
        return 'system';
    }

    protected function createInstance(string $repository): CodeRepositoryInterface
    {
        return new $repository();
    }
}
