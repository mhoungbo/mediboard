<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Code;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CModelObject;

/**
 * Representation of a code for API usage.
 */
class Code
{
    public const RESOURCE_TYPE = 'code';

    // TODO [PHP 8.1] Set readonly
    public string $api_type = self::RESOURCE_TYPE;
    public string $code;
    public string $description;
    public string $type;

    public function __construct(string $code, string $description, string $type)
    {
        $this->code        = $code;
        $this->description = $description;
        $this->type        = $type;
    }

    public static function fromCodeInterface(CodeInterface $code): self
    {
        return new self(
            $code->getCodeValue(),
            $code->getDescription(),
            $code->getType()
        );
    }
}
