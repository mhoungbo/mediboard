<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\ClientInterface;
use Ox\Core\Contracts\Client\HTTPClientInterface;
use Ox\Interop\Eai\Resilience\ClientContext;
use Ox\Mediboard\System\Client\HTTPClientCurlLegacy;
use Ox\Mediboard\System\Client\ResilienceHTTPClient;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Source HTTP, using cURL, allowing traceability
 */
class CSourceHTTP extends CExchangeSourceAdvanced
{
    // Source type
    public const TYPE = 'http';

    /** @var string */
    public const RESOURCE_TYPE = 'source_http';

    /** @var string[] */
    protected const CLIENT_MAPPING = [
        self::CLIENT_HTTP_LEGACY_CURL => HTTPClientCurlLegacy::class,
    ];

    /** @var string */
    protected const DEFAULT_CLIENT = self::CLIENT_HTTP_LEGACY_CURL;

    /** @var string */
    public const CLIENT_HTTP_LEGACY_CURL = 'curl_leg';

    /** @var int Primary key */
    public $source_http_id;
    /** @var string */
    public $token;

    /** @var HTTPClientInterface */
    public $_client;

    /**
     * @inheritdoc
     */
    public function getSpec()
    {
        $spec        = parent::getSpec();
        $spec->table = 'source_http';
        $spec->key   = 'source_http_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps()
    {
        $props          = parent::getProps();
        $props["token"] = "str";

        return $props;
    }

    /**
     * @param string|null $evenement_name
     *
     * @return string|null
     */
    public function getHost(?string $evenement_name = null): ?string
    {
        if (!$host = $this->host) {
            return null;
        }

        if ($evenement_name !== null) {
            $host = rtrim($host, "/") . "/" . ltrim($evenement_name, "/");
        }

        return $host;
    }

    /**
     * @return HTTPClientInterface
     * @throws CMbException
     */
    public function getClient(): HTTPClientInterface
    {
        /** @var HTTPClientInterface $client_http */
        $client_http = parent::getClient();

        return $client_http;
    }

    /**
     * @return HTTPClientInterface
     * @throws CMbException
     */
    public function disableResilience(): HTTPClientInterface
    {
        /** @var HTTPClientInterface $client_http */
        $client_http = parent::disableResilience();

        return $client_http;
    }

    /**
     * @return HTTPClientInterface
     * @throws CMbException
     */
    public function reactivateResilience(): HTTPClientInterface
    {
        /** @var HTTPClientInterface $client_http */
        $client_http = parent::reactivateResilience();

        return $client_http;
    }

    /**
     * @inheritDoc
     */
    protected function getResilientClient(ClientInterface $client): ?HTTPClientInterface
    {
        /** @var HTTPClientInterface $client */
        $client = parent::getResilientClient($client);

        return $client ? new ResilienceHTTPClient($client, $this) : null;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @inheritDoc
     */
    public function startLog(ClientContext $context): void
    {
        $input = $context->getRequest();
        $input = $this->formatRequestInput($input);

        $function_name = $context->getArguments()['function_name'] ?? null;

        $this->_current_echange      = $echange_http = new CExchangeHTTP();
        $echange_http->date_echange  = CMbDT::dateTime();
        $echange_http->emetteur      = CAppUI::conf("mb_id");
        $echange_http->destinataire  = $uri ?? $this->host;
        $echange_http->source_class  = $this->_class;
        $echange_http->source_id     = $this->_id;
        $echange_http->function_name = $function_name ?: '';
        $echange_http->input         = ($input !== null) ? serialize($input) : null;
    }

    /**
     * @inheritDoc
     */
    public function stopLog(ClientContext $context): void
    {
        /** @var CExchangeHTTP $echange_http */
        if (!($echange_http = $this->_current_echange)) {
            return;
        }

        $echange_http->response_time = $this->_current_chronometer->total;

        // response time
        $echange_http->response_datetime = CMbDT::dateTime();

        // Truncate input and output before storing
        $output = $context->getResponse();
        $output = $this->formatResponseOutput($output);

        $echange_http->output = ($output !== null) ? serialize($output) : null;

        $this->registerStoreInCallback($echange_http);
    }

    /**
     * Format http response to store on exchange_http
     *
     * @param mixed $response
     * @return mixed
     */
    private function formatResponseOutput($response)
    {
        if (!$response) {
            return null;
        }

        if ($response instanceof ResponseInterface) {
            $headers = [];
            foreach ($response->getHeaders() as $name => $values) {
                $headers[] = $name . ': ' . $response->getHeaderLine($name);
            }
            $headers = implode("\r\n", $headers);

            return implode("\r\n\r\n", [$headers, (string) $response->getBody()]);
        }

        return $response;
    }

    /**
     * Format http request to store on exchange_http
     *
     * @param $request
     * @return string|null
     */
    private function formatRequestInput($request): ?string
    {
        if (!$request) {
            return $request;
        }

        if ($request instanceof RequestInterface) {
            $headers = [];
            foreach ($request->getHeaders() as $name => $values) {
                $headers[] = $name . ': ' . $request->getHeaderLine($name);
            }
            $headers = implode("\r\n", $headers);
            $input   = [
                'uri: ' . $request->getUri(),
                'method: ' . $request->getMethod(),
                'headers: ' . $headers
            ];

            return implode("\r\n", [implode("\r\n", $input), (string) $request->getBody()]);
        }

        return $request;
    }

    /**
     * @inheritDoc
     */
    public function exceptionLog(ClientContext $context): void
    {
        /** @var CExchangeHTTP $echange_http */
        if (!$echange_http = $this->_current_echange) {
            return;
        }

        $echange_http->response_datetime = CMbDT::dateTime();
        $throwable                       = $context->getThrowable();
        $output                          = $context->getResponse();
        if (!$output && $throwable) {
            $output = $throwable->getMessage();
        }

        $echange_http->output = $output !== null ? serialize($output) : null;

        $this->registerStoreInCallback($echange_http);
    }

    /**
     * @inheritdoc
     */
    protected function getCallSignature(ClientContext $context): string
    {
        /** @var RequestInterface $request */
        $request = $context->getRequest() instanceof RequestInterface ? $context->getRequest() : null;
        if (!$request) {
            return '';
        }

        $host    = $request->getUri() ?? '';
        $methode = $request->getMethod();

        return "$methode : $host";
    }
}
