<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Ox\Core\Autoload\IShortNameAutoloadable;

/**
 * User agent graph
 */
class CUserAgentGraph implements IShortNameAutoloadable
{
    static function getBrowserNameSeries($browsers)
    {
        $series = [
            "title"   => "CUserAgent-browser_name",
            "data"    => [],
            "options" => [
                "series" => [
                    "pie" => [
                        "show"  => true,
                        "label" => [
                            "show"      => true,
                            "threshold" => 0.02,
                        ],
                    ],
                ],
                "legend" => [
                    "show" => false,
                ],
                "grid"   => [
                    "hoverable" => true,
                ],
            ],
        ];

        foreach ($browsers as $_browser) {
            $series["data"][] = [
                "label" => $_browser["browser_name"],
                "data"  => $_browser["total"],
            ];
        }

        return $series;
    }

    static function getBrowserVersionSeries($versions)
    {
        $browsers = [];
        foreach ($versions as $_version) {
            if (!isset($browsers[$_version["browser_name"]])) {
                $browsers[$_version["browser_name"]] = [];
            }

            $browsers[$_version["browser_name"]][$_version["browser_version"]] = $_version["total"];
        }

        $series = [
            "title"   => "CUserAgent-browser_version",
            "data"    => [],
            "options" => [],
        ];

        $ticks = [];
        $i     = 0;
        foreach ($browsers as $_browser => $_version) {
            $i += 1.5;

            $ticks[] = [
                "0" => $i,
                "1" => "<strong>$_browser</strong>",
            ];
        }
        $max = $i + 1;

        $series["options"] = [
            "xaxis"  => [
                "position" => "bottom",
                "min"      => 0,
                "max"      => $max,
                "ticks"    => $ticks,
            ],
            "yaxes"  => [
                "0" => [
                    "position"     => "left",
                    "tickDecimals" => false,
                ],
                "1" => [
                    "position" => "right",
                ],
            ],
            "legend" => [
                "show" => false,
            ],
            "series" => [
                "stack" => true,
            ],
            "grid"   => [
                "hoverable" => true,
            ],
        ];

        $i = 1;
        foreach ($browsers as $_versions) {
            foreach ($_versions as $_version => $_total) {
                $datum   = [];
                $datum[] = [
                    "0" => $i,
                    "1" => $_total,
                ];

                $series["data"][] = [
                    "data"  => $datum,
                    "yaxis" => 1,
                    "label" => "$_version",
                    "bars"  => [
                        "show" => true,
                    ],
                ];
            }

            $i += 1.5;
        }

        return $series;
    }

    static function getPlatformNameSeries($platforms)
    {
        $series = [
            "title"   => "CUserAgent-platform_name",
            "data"    => [],
            "options" => [
                "series" => [
                    "pie" => [
                        "show"  => true,
                        "label" => [
                            "show"      => true,
                            "threshold" => 0.02,
                        ],
                    ],
                ],
                "legend" => [
                    "show" => false,
                ],
                "grid"   => [
                    "hoverable" => true,
                ],
            ],
        ];

        foreach ($platforms as $_platform) {
            $series["data"][] = [
                "label" => $_platform["platform_name"],
                "data"  => $_platform["total"],
            ];
        }

        return $series;
    }

    static function getDeviceTypeSeries($devices)
    {
        $series = [
            "title"   => "CUserAgent-device_type",
            "data"    => [],
            "options" => [
                "series" => [
                    "pie" => [
                        "show"  => true,
                        "label" => [
                            "show"      => true,
                            "threshold" => 0.02,
                        ],
                    ],
                ],
                "legend" => [
                    "show" => false,
                ],
                "grid"   => [
                    "hoverable" => true,
                ],
            ],
        ];

        foreach ($devices as $_device) {
            $series["data"][] = [
                "label" => $_device["device_type"],
                "data"  => $_device["total"],
            ];
        }

        return $series;
    }

    static function getScreenSizeSeries($screens)
    {
        $series = [
            "title"   => "CUserAuthentication-screen_width",
            "data"    => [],
            "options" => [
                "series" => [
                    "pie" => [
                        "show"  => true,
                        "label" => [
                            "show"      => true,
                            "threshold" => 0.02,
                        ],
                    ],
                ],
                "legend" => [
                    "show" => false,
                ],
                "grid"   => [
                    "hoverable" => true,
                ],
            ],
        ];

        foreach ($screens as $_screen) {
            $series["data"][] = [
                "label" => $_screen["screen_width"],
                "data"  => $_screen["total"],
            ];
        }

        return $series;
    }

    static function getPointingMethodSeries($methods)
    {
        $series = [
            "title"   => "CUserAgent-pointing_method",
            "data"    => [],
            "options" => [
                "series" => [
                    "pie" => [
                        "show"  => true,
                        "label" => [
                            "show"      => true,
                            "threshold" => 0.02,
                        ],
                    ],
                ],
                "legend" => [
                    "show" => false,
                ],
                "grid"   => [
                    "hoverable" => true,
                ],
            ],
        ];

        foreach ($methods as $_method) {
            $series["data"][] = [
                "label" => $_method["pointing_method"],
                "data"  => $_method["total"],
            ];
        }

        return $series;
    }

    static function getNbConnectionsSeries($connections)
    {
        $series = [
            "title"   => "CUserAuthentication",
            "data"    => [],
            "options" => [
                "series" => [
                    "pie" => [
                        "show"  => true,
                        "label" => [
                            "show"      => true,
                            "threshold" => 0.02,
                        ],
                    ],
                ],
                "legend" => [
                    "show" => false,
                ],
                "grid"   => [
                    "hoverable" => true,
                ],
            ],
        ];

        foreach ($connections as $_connection) {
            $series["data"][] = [
                "label" => $_connection["datetime_login"],
                "data"  => $_connection["total"],
            ];
        }

        return $series;
    }
}
