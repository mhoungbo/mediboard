<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CMbSecurity;
use Ox\Core\CStoredObject;
use Ox\Core\FieldSpecs\CPasswordSpec;
use Ox\Core\Security\Exception\CouldNotEncrypt;
use Ox\Mediboard\System\Keys\CKeyMetadata;
use Ox\Mediboard\System\Keys\KeyStore;
use Throwable;

class CEncryptedProperty extends CStoredObject
{
    /** @var int */
    public $encrypted_property_id;

    /** @var int */
    public $key_id;

    /** @var string */
    public $last_update_date;

    /** @var string */
    public $object_class;

    /** @var int */
    public $object_id;

    /** @var string */
    public $property_name;

    /** @var string|null */
    public $iv;

    /** @var string */
    public $encrypted;

    /** @var CKeyMetadata|null */
    public $_ref_key_metadata;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = 'encrypted_property';
        $spec->key   = 'encrypted_property_id';

        $spec->loggable  = CMbObjectSpec::LOGGABLE_NEVER;
        $spec->anti_csrf = true;

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['key_id']           = 'ref class|CKeyMetadata notNull cascade back|encrypted_properties show|0';
        $props['last_update_date'] = 'dateTime notNull show|0 index|0';
        $props['object_class']     = 'str notNull index|object_id|property_name show|0';
        $props['object_id']        = 'ref class|CStoredObject meta|object_class notNull cascade back|encrypted_properties_object show|0';
        $props['property_name']    = 'str notNull show|0';
        $props['iv']               = 'str maxLength|80 show|0';
        $props['encrypted']        = 'binary notNull show|0';

        return $props;
    }

    /**
     * @inheritDoc
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = CAppUI::tr('CEncryptedProperty');
    }

    /**
     * @param CStoredObject $object
     * @param CPasswordSpec $spec
     * @param string        $clear
     *
     * @return static
     * @throws CouldNotEncrypt
     * @throws Keys\Exceptions\CouldNotGetKeysFromStorage
     * @throws Keys\Exceptions\CouldNotUseKey
     * @throws CMbException
     */
    public static function createFor(CStoredObject $object, CPasswordSpec $spec, string $clear): self
    {
        if (!$object->_id) {
            throw CouldNotEncrypt::contextDoesNotExist($object->_class);
        }

        $metadata = CKeyMetadata::loadFromName($spec->key);

        $encryption                = new static();
        $encryption->key_id        = $metadata->_id;
        $encryption->object_class  = $object->_class;
        $encryption->object_id     = $object->_id;
        $encryption->property_name = $spec->fieldName;

        return self::encrypt($metadata, $encryption, $clear);
    }

    /**
     * @param string $clear
     *
     * @return void
     * @throws CouldNotEncrypt
     * @throws Keys\Exceptions\CouldNotGetKeysFromStorage
     * @throws Keys\Exceptions\CouldNotUseKey
     * @throws CMbException
     */
    public function updateTo(string $clear): void
    {
        $metadata = $this->loadKeyMetadata();

        self::encrypt($metadata, $this, $clear);
    }

    /**
     * @param CKeyMetadata       $metadata
     * @param CEncryptedProperty $encryption
     * @param string             $clear
     *
     * @return static
     * @throws CouldNotEncrypt
     * @throws Keys\Exceptions\CouldNotGetKeysFromStorage
     * @throws Keys\Exceptions\CouldNotUseKey
     * @throws CMbException
     */
    private static function encrypt(CKeyMetadata $metadata, self $encryption, string $clear): self
    {
        if ($metadata->doesAlgNeedIv()) {
            $encryption->iv = CMbSecurity::generateIV(16, false);
        }

        $encrypted = CMbSecurity::encryptFromKey(
            KeyStore::loadKey($metadata->name),
            $clear,
            $encryption->iv,
            CMbSecurity::FORMAT_BIN
        );

        if (($encrypted === false) || ($encrypted === '')) {
            throw CouldNotEncrypt::anErrorOccurred();
        }

        $encryption->encrypted = $encrypted;

        // Updating generation date
        $encryption->last_update_date = CMbDT::dateTime();

        try {
            if ($msg = $encryption->store()) {
                throw new Exception($msg);
            }
        } catch (Throwable $t) {
            throw CouldNotEncrypt::anErrorOccurred($t->getMessage());
        }

        return $encryption;
    }

    /**
     * @return string
     * @throws Keys\Exceptions\CouldNotUseKey
     */
    public function decrypt(): string
    {
        return CMbSecurity::decryptFromKey(
            KeyStore::loadKey($this->loadKeyMetadata()->name),
            $this->encrypted,
            $this->iv,
            CMbSecurity::FORMAT_BIN
        );
    }

    private function loadKeyMetadata(): ?CKeyMetadata
    {
        return $this->_ref_key_metadata = $this->loadFwdRef('key_id', true);
    }
}
