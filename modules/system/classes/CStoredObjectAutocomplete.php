<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Cache;
use Ox\Core\CClassMap;
use Ox\Core\CMbException;
use Ox\Core\CMbFieldSpec;
use Ox\Core\CMbObject;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Core\Mutex\CMbMutex;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Autocomplete for CStoredObject.
 * If the searched field is not a reference the result may be cached to prevent multiple full scan on DB.
 */
class CStoredObjectAutocomplete
{
    public const OPTION_WHOLE_STRING    = 'whole_string';
    public const OPTION_MIN_LENGTH      = 'min_length';
    public const OPTION_MIN_OCCURRENCES = 'min_occurrences';
    public const OPTION_LIMIT           = 'limit';

    private const DEFAULT_OPTIONS = [
        self::OPTION_WHOLE_STRING    => false,
        self::OPTION_MIN_LENGTH      => 0,
        self::OPTION_MIN_OCCURRENCES => 1,
        self::OPTION_LIMIT           => 30,
    ];

    // Cache for 24 hours
    private const CACHE_TTL           = 60 * 60 * 24;
    private const CACHE_PREFIX        = 'Autocomplete';
    private const LIMIT_FOR_CACHE     = 1000;
    private const CACHE_MIN_OCCURENCE = 2;
    private const CACHE_MUTEX_TIME    = 10;

    private const VIEW       = '_view';
    private const PERM_TABLE = [
        "deny" => PERM_DENY,
        "read" => PERM_READ,
        "edit" => PERM_EDIT,
    ];

    private CSQLDataSource $ds;

    private CStoredObject $object;
    private CMbFieldSpec  $spec;
    private string        $field;
    private ?string       $view_field;

    private array   $options;
    private array   $where    = [];
    private ?string $template = null;

    /**
     * @throws CMbException
     */
    public function __construct(string $class_name, string $field, ?string $view_field = null, array $options = [])
    {
        if (!is_subclass_of($class_name, CStoredObject::class)) {
            throw new CMbException(
                'CStoredObjectAutocomplete-Error-Class must be a child of CStoredObject',
                $class_name
            );
        }

        $this->object     = new $class_name();
        $this->ds         = $this->object->getDS();
        $this->field      = $field;
        $this->spec       = $this->object->_specs[$this->field];
        $this->view_field = $view_field;

        $this->options = array_merge(self::DEFAULT_OPTIONS, $options);
    }

    /**
     * @param string $keyword Keyword to search for
     *
     * @return array An array of found objects
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function search(string $keyword): array
    {
        $keyword = str_replace("\\'", "'", $keyword);
        $search  = $this->options[self::OPTION_WHOLE_STRING] ? "%$keyword%" : "$keyword%";

        if ($this->options[self::OPTION_MIN_LENGTH] && strlen($keyword) < $this->options[self::OPTION_MIN_LENGTH]) {
            return [];
        }

        // Make a search in the target class
        if ($this->spec instanceof CRefSpec) {
            return $this->makeRefSearch($search, $keyword);
        }

        // If the search is cacheable try to find it in the cache instead of loading it from DB.
        if ($this->isSearchCacheable()) {
            return $this->makeCacheSearch($keyword);
        }

        return $this->makeBasicSearch($search, $this->options[self::OPTION_LIMIT]);
    }

    /**
     * Add where arguments to the search.
     * Using this will prevent the cache from beeing used.
     */
    public function addWhere(array $where): void
    {
        foreach ($where as $key => $value) {
            $this->where[$key] = $this->ds->prepare("= %", $value);
        }
    }

    /**
     * Make a simple search using LIKE condition on the table of $this->object.
     *
     * @return array An array of objects found of the same classe of $this->object.
     *
     * @throws Exception
     */
    private function makeBasicSearch(string $search, int $limit): array
    {
        $where    = array_merge($this->where, [$this->field => $this->ds->prepareLike($search)]);
        $group_by = $this->field;

        if ($this->options[self::OPTION_MIN_OCCURRENCES] >= self::CACHE_MIN_OCCURENCE) {
            $group_by .= $this->ds->prepare(' HAVING COUNT(*) > ?', $this->options[self::OPTION_MIN_OCCURRENCES]);
        }

        return $this->loadList($where, $limit, $group_by);
    }

    /**
     * Try to get data from the cache and search in this.
     *
     * @param string $keyword
     *
     * @return array
     * @throws CMbException
     * @throws InvalidArgumentException
     */
    private function makeCacheSearch(string $keyword): array
    {
        $cache = $this->getCache();
        $key   = $this->getCacheKey();

        // Get the data from cache or load it.
        if (($cache_search = $cache->get($key, null, self::CACHE_TTL)) === null) {
            // Wait in case of someone trying to build the cache.
            $mutex = new CMbMutex($key);
            $mutex->acquire(self::CACHE_MUTEX_TIME);

            if (($cache_search = $cache->get($key, null, self::CACHE_TTL)) === null) {
                $cache_search = $this->loadListForCache(
                    $this->options[self::OPTION_MIN_OCCURRENCES],
                    self::LIMIT_FOR_CACHE
                );

                asort($cache_search, SORT_NATURAL);

                $cache->set($key, $cache_search, self::CACHE_TTL);
            }

            $mutex->release();
        }

        // Search in the data returned for the onces that match the search.
        $object_ids = [];
        foreach ($cache_search as $object_id => $cache_data) {
            if (str_contains($cache_data, $keyword)) {
                $object_ids[] = $object_id;
            }

            if (count($object_ids) === (int)$this->options[self::OPTION_LIMIT]) {
                break;
            }
        }

        // Load the matching objects using their ids.
        return $this->object->loadAll($object_ids);
    }

    protected function loadListForCache(int $min_occurences, int $limit): array
    {
        if ($min_occurences < self::CACHE_MIN_OCCURENCE) {
            throw new CMbException(
                'CStoredObjectAutocomplete-Error-Cannot make search with a low occurence',
                self::CACHE_MIN_OCCURENCE
            );
        }

        $request = new CRequest(false);
        $request->addSelect([$this->object->_spec->key, $this->field]);
        $request->addTable($this->object->_spec->table);
        $request->addWhere([$this->field => 'IS NOT NULL']);
        $request->addGroup($this->field);
        $request->addOrder('COUNT(*) DESC');
        $request->addHaving($this->ds->prepare('COUNT(*) > ?', $min_occurences));
        $request->setLimit($limit);

        return $this->ds->loadHashList($request->makeSelect());
    }

    /**
     * Make a search on the class targeted by the spec.
     *
     * @param string $search  Use this argument for LIKE query.
     * @param string $keyword Use this argument for autocomplete (seek) query.
     *
     * @return array An array of objects found of the same class as $target_object.
     *
     * @throws Exception
     */
    private function makeRefSearch(string $search, string $keyword): array
    {
        if ($this->view_field === null || $this->view_field === '') {
            throw new CMbException('CStoredObjectAutocomplete-Error-View field cannot be null for ref search');
        }

        /** @var CMbObject $target_object */
        $target_object = new $this->spec->class();

        $this->template = $target_object->getTypedTemplate("autocomplete");

        // If the field is self::VIEW make an autocomplete using the FW (CStoredObject::seek)
        if ($this->view_field === self::VIEW) {
            return $this->getAutocompleteList($target_object, $keyword);
        }

        $this->where[$this->view_field] = $this->ds->prepareLike($search);

        // If the field spec have perms loadListWithPerms.
        if ($this->spec->perm) {
            $perm = self::PERM_TABLE[$this->spec->perm] ?? null;

            return $this->loadTargetListWithPerm($target_object, $perm);
        }

        return $this->loadTargetList($target_object);
    }

    /**
     * Tell if the search can use the cache or not.
     */
    private function isSearchCacheable(): bool
    {
        // If a custom where has been set we cannot use cache (too many keys)
        if ($this->where !== []) {
            return false;
        }

        // If the option min_occurences is less than 2 do not use cache
        if ($this->options[self::OPTION_MIN_OCCURRENCES] < self::CACHE_MIN_OCCURENCE) {
            return false;
        }

        // Better optimisation to not use cache if the field is indexed.
        if (!$this->options[self::OPTION_WHOLE_STRING]) {
            return false;
        }

        return true;
    }

    protected function loadList(
        array  $where,
        int    $limit,
        string $group_by
    ): array {
        return $this->object->loadList(
            $where,
            $this->field,
            $limit,
            $group_by,
            null,
            null,
            null,
            false
        );
    }

    protected function getAutocompleteList(CStoredObject $target_object, string $keyword): array
    {
        return $target_object->getAutocompleteList($keyword, $this->where, $this->options[self::OPTION_LIMIT]);
    }

    protected function loadTargetListWithPerm(CStoredObject $target_object, ?int $perm): array
    {
        return $target_object->loadListWithPerms(
            $perm,
            $this->where,
            $this->view_field,
            $this->options[self::OPTION_LIMIT],
            $this->view_field
        );
    }

    protected function loadTargetList(CStoredObject $target_object): array
    {
        return $target_object->loadList(
            $this->where,
            $this->view_field,
            $this->options[self::OPTION_LIMIT],
            $this->view_field
        );
    }

    private function getCache(): CacheInterface
    {
        return Cache::getCache(Cache::OUTER | Cache::DISTR);
    }

    private function getCacheKey(): string
    {
        return self::CACHE_PREFIX . '-' . CClassMap::getSN($this->object) . '-' . $this->field . '-'
            . $this->options[self::OPTION_MIN_OCCURRENCES];
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function isRefSpec(): bool
    {
        return $this->spec instanceof CRefSpec;
    }
}
