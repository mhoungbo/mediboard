<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use LogicException;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Cache;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CMbSemaphore;
use Ox\Core\CStoredObject;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * CConfiguration models manager
 *
 * Todo: Handle children where parent is modified (ie. CFunctions-2-group_id=1 to CFunctions-2-group_id=3)
 */
class ContextualConfigurationManager
{
    private const KEY_MODEL_CHECKED = 'config-checked-model-';
    private const TTL_MODEL_CHECKED = 60;

    private const KEY_MODEL_INTEGRITY = 'config-model-hash-';

    private const KEY_MODEL = 'config-model-';

    private const KEY_VALUES_CHECKED = 'config-checked-values-';

    private const KEY_LAST_BUILD            = 'config-last-build-';
    private const TTL_CHECKING_VALUES       = 60;
    private const LOCK_TIME                 = 10;
    public const  PATTERN_SANITIZED_INHERIT = '@([\w ]+ / )@';

    private static bool $ready = false;

    /** @var self[] */
    private static array $instances = [];

    /** @var string Current module name */
    private string $module;

    private array $raw_model = [];

    private CacheInterface $cache;

    /** @var array Root nodes */
    private array $roots = [];

    /** @var array Leaf nodes */
    private array $leaves = [];

    /** @var array Inherited raw model */
    private array $inherited_raw_model = [];

    /** @var array Model */
    private array $model = [];

    /** @var array Cache of object ids */
    private array $context_ids = [];

    /** @var array Cache of stored configurations */
    private array $cache_data = [];

    /** @var array Cache object storage */
    private array $storage = [];

    /**
     * @param string         $module
     * @param array          $raw_model
     * @param CacheInterface $cache
     */
    private function __construct(string $module, array $raw_model, CacheInterface $cache)
    {
        $this->module    = $module;
        $this->raw_model = $raw_model;
        $this->cache     = $cache;
    }

    /**
     * Initialize a ContextualConfigurationManager for a given module name.
     *
     * @param string         $module
     * @param array          $raw_model
     * @param CacheInterface $cache
     *
     * @return void
     */
    public static function init(string $module, array $raw_model, CacheInterface $cache): void
    {
        $instance = (self::$instances[$module]) ?? null;

        if ($instance === null) {
            self::$instances[$module] = new self($module, $raw_model, $cache);
        } else {
            $instance->appendRawModel($raw_model);
        }
    }

    /**
     * @param string $module
     *
     * @return static
     * @throws LogicException
     */
    public static function get(string $module): self
    {
        if (!isset(self::$instances[$module])) {
            throw new LogicException(sprintf("Unable to load configurations for module '%s'", $module));
        }

        return self::$instances[$module];
    }

    private function appendRawModel(array $raw_model): void
    {
        $this->raw_model = array_merge($this->raw_model, $raw_model);
    }

    /**
     * Check if configurations are ready
     *
     * @return bool
     */
    public static function isReady(): bool
    {
        return self::$ready;
    }

    /**
     * Mark the configuration manager as ready
     *
     * @return void
     */
    public static function setReady(): void
    {
        self::$ready = true;
    }

    /**
     * Get the configuration mode according to instance role
     *
     * @return string
     * @throws Exception
     */
    public static function getConfigurationMode(): string
    {
        switch (CAppUI::conf('instance_role')) {
            case 'prod':
                return 'std';

            default:
                return 'alt';
        }
    }

    /**
     * Get the configuration strategy according to mode
     *
     * @param string|null $mode Configuration mode
     *
     * @return IConfigurationStrategy
     * @throws Exception
     */
    public static function getStrategy(string $mode = null): IConfigurationStrategy
    {
        if (is_null($mode)) {
            $mode = self::getConfigurationMode();
        }

        switch ($mode) {
            case 'std':
                return new CConfigurationStdStrategy();

            case 'alt':
                return new CConfigurationAltStrategy();

            default:
                throw new Exception('Invalid configuration strategy mode');
        }
    }

    /**
     * Simplify an inheritance schema, removing the prefix
     *
     * @param string $inherit The inheritance schema
     *
     * @return string
     */
    private static function simplifyInherit(string $inherit): string
    {
        return preg_replace(self::PATTERN_SANITIZED_INHERIT, '', $inherit);
    }

    /**
     * Get the sorted by inherit raw model
     *
     * @return array
     */
    private function getSortedRawModel(): array
    {
        $raw_model = $this->raw_model;

        uksort(
            $raw_model,
            function ($_a, $_b) {
                $_count_a = substr_count($_a, ' ');
                $_count_b = substr_count($_b, ' ');

                return $_count_a <=> $_count_b;
            }
        );

        return $raw_model;
    }

    /**
     * Register a root node
     *
     * @param CConfigurationModelRoot $root Node to register
     *
     * @return void
     */
    private function registerRootNode(CConfigurationModelRoot $root): void
    {
        $this->roots[$root->getContextClass()] = $root;
    }

    /**
     * Register a leaf node
     *
     * @param CConfigurationModelLeaf $leaf Node to register
     *
     * @return void
     */
    private function registerLeafNode(CConfigurationModelLeaf $leaf): void
    {
        $this->leaves[$leaf->getInherit()] = $leaf;
    }

    /**
     * Get the root model nodes
     *
     * @return CConfigurationModelRoot[]
     * @throws CMbException
     */
    private function getRootNodes(): array
    {
        if (!$this->roots) {
            // No root nodes, a configuration is mostly asked before CConfiguration::register() (ie. during autoload)
            throw new CMbException('No root nodes in ' . $this->module);
        }

        return $this->roots;
    }

    /**
     * Get root node model count
     *
     * @param string $context_class Root node context class
     *
     * @return int
     */
    private function countRootNodes(string $context_class = null): int
    {
        if (!$context_class) {
            return count($this->roots);
        }

        // Todo: Wat? A bool?
        return isset($this->roots[$context_class]);
        //return (isset(self::$roots[self::getModule()][$context_class]) && self::$roots[self::getModule()][$context_class]);
    }

    /**
     * Get a root node
     *
     * @param string $context_class Class of the context
     * @param string $inherit       Inherit key
     *
     * @return CConfigurationModelRoot
     */
    private function getRootNode(string $context_class, string $inherit): CConfigurationModelRoot
    {
        $root = ($this->roots[$context_class]) ?? null;

        if ($root === null) {
            return new CConfigurationModelRoot($this->module, $context_class, $inherit);
        }

        return $root;
    }

    /**
     * Get a leaf node
     *
     * @param string $context_class Class of the context
     * @param string $inherit       Inherit key
     *
     * @return CConfigurationModelLeaf
     */
    private function getLeafNode(string $context_class, string $inherit): CConfigurationModelLeaf
    {
        $leaf = $this->leaves[$inherit] ?? null;

        if ($leaf === null) {
            return new CConfigurationModelLeaf($this->module, $context_class, $inherit);
        }

        return $leaf;
    }

    /**
     * Prepare the raw mode tree (Root nodes + leaves)
     *
     * @return void
     */
    private function registerModelNodes(): void
    {
        foreach ($this->getSortedRawModel() as $_inherit => $_model) {
            $_sanitized_inherit = self::simplifyInherit($_inherit);

            $_classes = explode(' ', $_sanitized_inherit);
            $_count   = count($_classes);

            // Removing module key
            $_simplified_model = [];

            array_walk(
                $_model,
                function ($v) use (&$_simplified_model): void {
                    $_simplified_model = $v;
                }
            );

            // Root
            if ($_count === 1) {
                $_model_object = $this->getRootNode($_classes[0], $_inherit);

                $_model_object->setModel($_simplified_model);
                $_model_object->setSanitizedInherit($_sanitized_inherit);

                $this->registerRootNode($_model_object);
                continue;
            }

            // Leaf
            [$_parent_class, $fwd] = explode('.', $_classes[1]);
            $_model_object = $this->getLeafNode($_classes[0], $_inherit);

            $_model_object->setModel($_simplified_model);
            $_model_object->setSanitizedInherit($_sanitized_inherit);
            $_model_object->setFwdField($fwd);

            $this->registerLeafNode($_model_object);

            $_parent_node = $this->getRootNode($_parent_class, $_parent_class);
            $_parent_node->setSanitizedInherit($_parent_class);
            $_parent_node->addChild($_model_object);

            /**
             * Here, if no root node is present,
             *   we are in a situation where the root node is built like LEAF ROOT.FWD_ID
             *
             * So, we register the virtual root node
             */

            if (!$this->countRootNodes($_parent_class)) {
                $this->registerRootNode($_parent_node);
            }
        }
    }

    /**
     * Build the raw model indexed by inherit keys according to nodes
     *
     * @return void
     * @throws CMbException
     */
    private function buildRawModel(): void
    {
        $this->registerModelNodes();

        $model = [];

        foreach ($this->getRootNodes() as $_model_object) {
            $model[$_model_object->getInherit()] = $_model_object->getModel();

            /** @var CConfigurationModelLeaf $_child */
            foreach ($_model_object->getChildren() as $_child) {
                $model[$_child->getInherit()] = $_child->getModel();
            }
        }

        $this->inherited_raw_model = $model;
    }

    /**
     * Build the model with specifications
     *
     * @return void
     * @throws CMbException|InvalidArgumentException
     * @throws Exception
     */
    private function buildModel(): void
    {
        $model = $this->cache->get(self::KEY_MODEL . $this->module);

        if ($model !== null) {
            if (!$this->countRootNodes()) {
                // Always register model nodes before consuming model
                $this->registerModelNodes();
            }

            $this->model = $model;

            return;
        }

        $t = microtime(true);

        $this->buildRawModel();

        $this->cache->set(self::KEY_MODEL_INTEGRITY . $this->module, md5(serialize($this->raw_model)));

        $model = [];
        foreach ($this->inherited_raw_model as $_inherit => $_model) {
            $list = [];

            $this->buildModelLeaf($list, [], $_model);

            if (!isset($model[$_inherit])) {
                $model[$_inherit] = [];
            }

            $model[$_inherit] = array_merge($model[$_inherit], $list);
        }

        CApp::log(
            sprintf("'config-model-%s' generated in %f ms", $this->module, (microtime(true) - $t) * 1000)
        );

        $this->cache->set(self::KEY_MODEL . $this->module, $model);
        $this->model = $model;
    }

    /**
     * Build a model leaf
     *
     * @param array $list Global tree
     * @param array $path Path keys
     * @param array $tree Sub tree
     *
     * @return void
     */
    private function buildModelLeaf(array &$list, array $path, array $tree): void
    {
        foreach ($tree as $_key => $_subtree) {
            $_path   = $path;
            $_path[] = $_key;

            // If a leaf (prop)
            if (is_string($_subtree)) {
                // Build spec
                $_parts = explode(' ', $_subtree);

                $_spec_options = [
                    'type'   => array_shift($_parts),
                    'string' => $_subtree,
                ];

                foreach ($_parts as $_part) {
                    $_options                              = explode('|', $_part, 2);
                    $_spec_options[array_shift($_options)] = count($_options) ? $_options[0] : true;
                }

                // Always have a default value
                if (!isset($_spec_options['default'])) {
                    $_spec_options['default'] = '';
                }

                $list[implode(' ', $_path)] = $_spec_options;
            } else {
                // ... else a subtree
                $this->buildModelLeaf($list, $_path, $_subtree);
            }
        }
    }

    /**
     * Load the object ids for each inherit schema
     *
     * @return void
     * @throws CMbException
     * @throws InvalidArgumentException
     */
    private function loadContextIDs(): void
    {
        $this->buildModel();

        //$date = CMbDT::dateTime();
        //$contexts = array('global' => $date);

        foreach ($this->getRootNodes() as $_root) {
            $_root->loadObjectIDs();

            $_root_ids = $_root->getObjectIDs();

            //foreach ($_root_ids as $_root_id) {
            //  $contexts["{$_root->getContextClass()}-{$_root_id}"] = $date;
            //}

            foreach ($_root->getChildren() as $_child) {
                $_inherit = $_child->getSanitizedInherit();

                if (isset($this->context_ids[$_inherit])) {
                    $_child->setObjectIDs($this->context_ids[$_inherit]);
                    continue;
                }

                $_child->loadObjectIDs($_root_ids);
                $_child_ids = $_child->getObjectIDs();

                //foreach ($_child_ids as $_child_id) {
                //  $contexts["{$_child->getContextClass()}-{$_child_id['id']}"] = $date;
                //}

                $this->context_ids[$_inherit] = $_child_ids;
            }
        }

        //$cache = self::getContextInheritanceCache(self::getModule());
        //$cache->put($contexts);
    }

    /**
     * Get the CConfiguration spec
     *
     * @return CMbObjectSpec
     */
    public static function getConfigurationSpec(): CMbObjectSpec
    {
        static $spec;

        if (!isset($spec)) {
            $self = new CConfiguration();
            $spec = $self->_spec;
        }

        return $spec;
    }

    /**
     * Get the default model values
     *
     * @param array $keys Keys to get, if provided
     *
     * @return array
     */
    private function getDefaultValues(array $keys = []): array
    {
        $values = [];

        // Backward compatibility mode
        if ($keys) {
            $module = $this->module;

            foreach ($this->model as $_inherit => $_models) {
                foreach ($_models as $_key => $_model) {
                    $_module_key = "{$module} {$_key}";

                    // < PHP 5.6
                    if (in_array($_module_key, $keys)) {
                        $values[$_module_key] = $_model['default'];
                    }
                }
            }

            // >= PHP 5.6
            //$values = array_filter(
            //$values,
            //function ($k) use ($keys) {
            //return (in_array($k, $keys));
            //},
            //ARRAY_FILTER_USE_KEY
            //);

            return $values;
        }

        foreach ($this->model as $_inherit => $_models) {
            foreach ($_models as $_key => $_model) {
                $values[$_key] = $_model['default'];
            }
        }

        return $values;
    }

    /**
     * Check if model has been modified and clear cache if needed
     *
     * @return void
     * @throws CMbException|InvalidArgumentException
     * @throws CouldNotGetCache
     */
    private function checkModel(): void
    {
        $lock = new CMbSemaphore("checking-model-{$this->module}");
        $lock->acquire(self::LOCK_TIME, 0.001);

        // Check if model has been modified only every n seconds
        if ($this->cache->has(self::KEY_MODEL_CHECKED . $this->module)) {
            $lock->release();

            return;
        }

        $hash = $this->cache->get(self::KEY_MODEL_INTEGRITY . $this->module);

        if ($hash === null) {
            $this->clearModelCache();

            // Store in cache that model has been checked AFTER clearing all the configuration module cache
            $this->cache->set(self::KEY_MODEL_CHECKED . $this->module, null, self::TTL_MODEL_CHECKED);
            $lock->release();

            return;
        }

        $this->buildRawModel();

        $new_hash = md5(serialize($this->raw_model));

        if ($new_hash !== $hash) {
            $this->clearModelCache();
        }

        // Store in cache that model has been checked AFTER clearing all the configuration module cache
        $this->cache->set(self::KEY_MODEL_CHECKED . $this->module, null, self::TTL_MODEL_CHECKED);
        $lock->release();
    }

    /**
     * Check if values have been modified and clear cache if needed
     *
     * @return void
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     */
    private function checkValues(): void
    {
        $lock = new CMbSemaphore('checking-values-' . $this->module);
        $lock->acquire(self::LOCK_TIME, 0.001);

        // Check if values has been modified only every n seconds
        if ($this->cache->has(self::KEY_VALUES_CHECKED . $this->module)) {
            $lock->release();

            return;
        }

        $last_built = $this->cache->get(self::KEY_LAST_BUILD . $this->module);

        if ($last_built === null) {
            $this->clearValuesCache();

            $this->cache->set(self::KEY_VALUES_CHECKED . $this->module, null, self::TTL_CHECKING_VALUES);

            $lock->release();

            return;
        }

        if ((new CTableStatus())->isInstalled()) {
            $status_result = CTableStatus::getInfo(self::getConfigurationSpec()->table, $this->module);

            if ($status_result['update_time'] > $last_built) {
                $this->clearValuesCache();
            }
        }

        $this->cache->set(self::KEY_VALUES_CHECKED . $this->module, null, self::TTL_CHECKING_VALUES);

        $lock->release();
    }

    /**
     * Get the configuration values for a given object, with inheritance
     *
     * @param string          $object_class Object class
     * @param string|int|null $object_id    Object ID
     *
     * @return array
     * @throws CMbException
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function getValues(string $object_class = null, $object_id = null): array
    {
        // Todo Each configuration set in a leaf is deported to other leaves values because of inheritance which doesn't
        //   check inherit keys

        // Check if model has been updated and must be regenerated
        $this->checkModel();

        // Check if values have been modified and must be regenerated
        $this->checkValues();

        /**
         * Here, all caches have been checked and preventively cleared if needed
         */

        $context = ($object_class && $object_id) ? "{$object_class}-{$object_id}" : 'global';

        // Check if given context configuration exists in cache
        $values = $this->cache->get("config-values-{$this->module}-{$context}");
        if ($values !== null) {
            return $values;
        }

        // Context does not exists in cache, before building context values, we check if given context exists
        if ($context !== 'global') {
            $object = CStoredObject::loadFromGuid($context);

            if (!$object || !$object->_id) {
                throw new CMbModelNotFoundException('common-error-Object %s not found', $context);
            }
        }

        /**
         * Value in cache does not exists:
         *  1. Possible new context
         *  2. OUTER Cache has been cleared checkModel() or checkValues()
         *  3. Another thread is currently building values tree
         */
        $lock = new CMbSemaphore('values-' . $this->module);
        $lock->acquire(self::LOCK_TIME, 0.001);

        // Cache may have been already rebuilt if we waited for lock acquisition
        $values = $this->cache->get("config-values-{$this->module}-{$context}");
        if ($values !== null) {
            CApp::log(
                sprintf("'config-values-%s-*' have been rebuilt during lock time", $this->module)
            );

            $lock->release();

            return $values;
        }

        // If not, so we build the values tree
        $values = $this->buildValues($context);

        // Do not forget to release the lock after building the tree
        $lock->release();

        return $values;
    }

    /**
     * Build the configuration values tree for a given context
     *
     * @param string $context Context ("global" or a GUID)
     *
     * @return array
     * @throws CMbException
     * @throws InvalidArgumentException
     * @throws Exception
     */
    private function buildValues(string $context): array
    {
        $t = microtime(true);

        // Load object IDs for all inherits
        $this->loadContextIDs();

        // Checking if given context class is allowed in the model
        if (!$this->checkInherit($context)) {
            return [];
        }

        // Global + stored default configuration cache
        $default_values = $this->cache->get("config-values-{$this->module}-global");

        if ($default_values === null) {
            $default_values = array_merge($this->getDefaultValues(), $this->getStoredConfigurations(null, null));
            $this->putInStore("config-values-{$this->module}-global", $default_values, 'global');
        }

        $values = [
            'global' => $default_values,
        ];

        // For each root node...
        foreach ($this->getRootNodes() as $_root) {
            // For each root context...
            foreach ($_root->getObjectIDs() as $_id) {
                $_context = "{$_root->getContextClass()}-{$_id}";

                // Check if context configuration in cache
                $cache = $this->cache->get("config-values-{$this->module}-{$_context}");

                if ($cache !== null) {
                    $values[$_context] = $cache;
                } else {
                    $values[$_context] =
                        array_merge(
                            $values['global'],
                            $this->getStoredConfigurations($_root->getContextClass(), $_id)
                        );

                    $this->putInStore(
                        "config-values-{$this->module}-{$_context}",
                        $values[$_context],
                        $_context
                    );
                }
            }

            $child_inherits = [];

            // For each child...
            foreach ($_root->getChildren() as $_child) {
                // We do not want to compute two children with same sanitized inherit twice
                if (array_key_exists($_child->getSanitizedInherit(), $child_inherits)) {
                    continue;
                }

                $child_inherits[$_child->getSanitizedInherit()] = null;

                // For each child context...
                foreach ($_child->getObjectIDs() as $_child_id) {
                    $_context = "{$_child->getContextClass()}-{$_child_id['id']}";

                    // Check if context configuration in cache
                    $cache = $this->cache->get("config-values-{$this->module}-{$_context}");

                    if ($cache !== null) {
                        $values[$_context] = $cache;
                    } else {
                        /**
                         * ELSE here because we do not want to regenerate children values if they are in cache
                         * Values in cache are automatically cleared when a configuration is modified
                         */

                        $_child_values = $this->getStoredConfigurations($_child->getContextClass(), $_child_id['id']);

                        // No need to recompute values tree because child has no redefined value
                        if (!$_child_values) {
                            $this->putInStore(
                                "config-values-{$this->module}-{$_context}",
                                $values["{$_root->getContextClass()}-{$_child_id['parent_id']}"],
                                "{$_root->getContextClass()}-{$_child_id['parent_id']}"
                            );
                        } else {
                            $values[$_context] =
                                array_merge(
                                    $values["{$_root->getContextClass()}-{$_child_id['parent_id']}"],
                                    $_child_values
                                );

                            $this->putInStore(
                                "config-values-{$this->module}-{$_context}",
                                $values[$_context],
                                $_context
                            );
                        }
                    }
                }
            }
        }

        CApp::log(
            sprintf("'config-values-%s-*' generated in %f ms", $this->module, (microtime(true) - $t) * 1000)
        );

        $this->storeValues();

        $this->cache->set(self::KEY_LAST_BUILD . $this->module, CMbDT::dateTime());

        return isset($values[$context]) ? CMbArray::unflatten($values[$context]) : [];
    }

    /**
     * Shared storing cache
     *
     * @param string $cache_key Cache key where to store the values
     * @param array  $values    Values to store in cache
     * @param string $context   Context of the value
     *
     * @return void
     */
    private function putInStore(string $cache_key, array &$values, string $context): void
    {
        if (!isset($this->storage[$context])) {
            $this->storage[$context] = [
                'caches' => [$cache_key],
                'values' => $values,
            ];
        } else {
            $this->storage[$context]['caches'][] = $cache_key;
        }
    }

    /**
     * Store in cache the values from storage
     *
     * @return void
     * @throws InvalidArgumentException
     * @throws Exception
     */
    private function storeValues(): void
    {
        $t = microtime(true);

        foreach ($this->storage as $_storage) {
            $_values = CMbArray::unflatten($_storage['values'] ?? []);

            foreach ($_storage['caches'] as $_cache_key) {
                $this->cache->set($_cache_key, $_values);
            }
        }

        CApp::log(
            sprintf("'config-values-%s-*' written in %f ms", $this->module, (microtime(true) - $t) * 1000)
        );

        $this->storage = [];
    }

    /**
     * Check if given context is in inheritance schema
     *
     * @param string $context Context ("global" or GUID)
     *
     * @return bool
     * @throws CMbException
     */
    private function checkInherit(string $context): bool
    {
        if ($context === 'global') {
            return true;
        }

        [$object_class,] = explode('-', $context);

        foreach ($this->getRootNodes() as $_root) {
            if ($_root->getContextClass() === $object_class) {
                return true;
            }

            foreach ($_root->getChildren() as $_child) {
                if ($_child->getContextClass() === $object_class) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get the configuration values of an object, without inheritance
     *
     * @param string                      $object_class Object class
     * @param string|int|null             $object_id    Object ID
     * @param array                       $keys         Keys to get, if provided
     * @param IConfigurationStrategy|null $strategy     Configuration strategy
     *
     * @return array The configuration values
     * @throws Exception
     */
    private function getStoredConfigurations(
        ?string                $object_class = null,
                               $object_id = null,
        ?array                 $keys = [],
        IConfigurationStrategy $strategy = null
    ): array {
        if (empty($this->cache_data)) {
            $spec             = self::getConfigurationSpec();
            $strategy         = new CConfigurationStrategy($strategy);
            $this->cache_data = $this->getSanitizedStoredConfigurations($strategy, $spec, false);
        }

        if ($object_class && $object_id) {
            $data = array_filter(
                $this->cache_data,
                fn($v) => (
                    ($v['object_class'] === $object_class)
                    && ($v['object_id'] == $object_id) // No strict comparison
                    && ($v['static'] === '0')
                )
            );
        } else {
            $data = array_filter(
                $this->cache_data,
                fn($v) => (($v['object_class'] === null) && ($v['object_id'] === null) && ($v['static'] === '0'))
            );
        }

        $final_data = [];

        // Backward compatibility mode
        if ($keys) {
            $module = $this->module;

            foreach ($data as $_data) {
                $_module_key = "{$module} {$_data['feature']}";

                // < PHP 5.6
                if (in_array($_module_key, $keys)) {
                    $final_data[$_module_key] = $_data['value'];
                }
            }

            // >= PHP 5.6
            //$final_data = array_filter(
            //  $final_data,
            //  function ($k) use ($keys) {
            //    return (in_array($k, $keys));
            //  },
            //  ARRAY_FILTER_USE_KEY
            //);

            return $final_data;
        }

        foreach ($data as $_data) {
            $final_data[$_data['feature']] = $_data['value'];
        }

        return $final_data;
    }

    /**
     * Get the filtered stored features (with feature name integrity check).
     *
     * @param CConfigurationStrategy $strategy
     * @param CMbObjectSpec          $spec
     * @param bool                   $static
     *
     * @return array
     */
    private function getSanitizedStoredConfigurations(
        CConfigurationStrategy $strategy,
        CMbObjectSpec          $spec,
        bool                   $static = false
    ): array {
        $stored_configurations = $strategy->getStoredConfigurations($this->module, $spec, $static);

        // Using the default values as model for feature integrity
        $model = $this->getDefaultValues();

        return array_filter(
            $stored_configurations,
            fn(array $configuration) => isset($model[$configuration['feature']])
        );
    }

    /**
     * Clear all configuration cache for a given module
     *
     * @param string $module Module name
     *
     * @return void
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     */
    public static function clearCache(string $module): void
    {
        try {
            $self = self::get($module);
        } catch (LogicException $e) {
            // If module has no configuration, do nothing
            return;
        }

        $self->clearModelCache();

        if (isset($self->cache_data[$module])) {
            unset($self->cache_data[$module]);
        }
    }

    /**
     * Clear the configuration model cache for a given module
     *
     * @return void
     * @throws InvalidArgumentException|CouldNotGetCache
     */
    private function clearModelCache(): void
    {
        $this->cache->deleteMultiple(
            [
                self::KEY_MODEL . $this->module,
                self::KEY_MODEL_CHECKED . $this->module,
                self::KEY_MODEL_INTEGRITY . $this->module,
            ]
        );

        // If model cleared, no need to keep values
        $this->clearValuesCache();
    }

    /**
     * Clear configuration values cache for a given module
     *
     * @return void
     *
     * @throws InvalidArgumentException|CouldNotGetCache
     */
    private function clearValuesCache(): void
    {
        $this->cache->deleteMultiple(
            [
                self::KEY_VALUES_CHECKED . $this->module,
                self::KEY_LAST_BUILD . $this->module,
            ]
        );

        // Todo: Niaaaaaah
        Cache::deleteKeys(Cache::OUTER, "config-values-{$this->module}-");
    }

    /**
     * Get the specs of a configuration
     *
     * @param string $feature Space separated feature name
     *
     * @return null|array
     * @throws CMbException|InvalidArgumentException
     */
    public static function getConfigSpec(string $feature): ?array
    {
        [$module, $_feature] = explode(' ', $feature, 2);

        $self = self::get($module);
        $self->buildModel();

        foreach ($self->model as $_inherit => $_model) {
            if (array_key_exists($_feature, $_model)) {
                return $_model[$_feature];
            }
        }

        return null;
    }

    /**
     * Get model within backward compatibility mode
     *
     * @param string       $module   Module name
     * @param array|string $inherits Inherits
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public static function _getModel(string $module, $inherits = []): array
    {
        $self = self::get($module);

        try {
            $self->buildModel();
            $model = $self->model;
        } catch (CMbException $e) {
            $model = [];
        }

        if ($inherits) {
            $inherits = (is_array($inherits)) ? $inherits : [$inherits];
        }

        $_final_model = [];

        // < PHP 5.6
        foreach ($model as $_inherit => &$_model) {
            if ($_model && (!$inherits || ($inherits && in_array($_inherit, $inherits)))) {
                $_final_model[$_inherit] = $_model;
            }
        }

        // >= PHP 5.6
        // Removing abstract root nodes
        //$model = array_filter(
        //  $model,
        //  function ($_model, $_inherit) use ($inherits) {
        //    return ($_model && (!$inherits || ($inherits && in_array($_inherit, $inherits))));
        //  },
        //  ARRAY_FILTER_USE_BOTH
        //);

        $final_model = [];

        // < PHP 7.0
        foreach ($_final_model as $_inherit => &$_model) {
            $final_model[$_inherit] = [];

            foreach ($_model as $_feature => $_value) {
                $final_model[$_inherit]["{$module} {$_feature}"] = $_value;
            }
        }

        // >= PHP 7.0
        // Adding extra module to feature for backward compatibility
        //foreach ($model as $_inherit => &$_model) {
        //  foreach ($_model as $_feature => $_value) {
        //    $_model["{$module} {$_feature}"] = $_value;
        //    unset($_model[$_feature]);
        //  }
        //}

        return $final_model;
    }

    /**
     * Get all the configs for an inheritance schema, with all the inherited values
     *
     * @param string                      $config_inherit Inheritance schema
     * @param string                      $module         Module to get config for
     * @param array                       $config_keys    Configuration keys to get, or null
     * @param CStoredObject|null          $object         Host object, if none, we'll get global values
     * @param IConfigurationStrategy|null $strategy       Configuration strategy
     *
     * @return array Configuration values
     * @throws CMbException
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public static function getAncestorsConfigs(
        string                 $config_inherit,
        string                 $module,
        array                  $config_keys = [],
        CStoredObject          $object = null,
        IConfigurationStrategy $strategy = null
    ): array {
        $self = self::get($module);
        $self->buildModel();

        $configs       = [];
        $parent_config = $self->getDefaultValues($config_keys);

        $configs[] = [
            'object'        => 'default',
            'config'        => $parent_config,
            'config_parent' => $parent_config,
        ];

        $configs[] = [
            'object'        => 'global',
            'config'        => $self->getStoredConfigurations(null, null, $config_keys, $strategy),
            'config_parent' => $parent_config,
        ];

        if ($object) {
            $ancestors            = [];
            $config_inherit_parts = explode(' ', $config_inherit);

            $fwd         = null;
            $prev_object = $object;
            foreach ($config_inherit_parts as $i => $class) {
                $class_fwd = explode('.', $class);

                // Never need the fwd field for the first item
                if ($i === 0) {
                    unset($class_fwd[1]);
                }

                if (count($class_fwd) === 2) {
                    [$class, $fwd] = $class_fwd;
                }

                if (($class === $prev_object->_class) && !$fwd) {
                    $ancestors[] = $prev_object;
                } elseif ($fwd) {
                    $object      = $prev_object->loadFwdRef($fwd, true);
                    $ancestors[] = $object;
                    $prev_object = $object;
                }
            }

            $ancestors = array_reverse($ancestors);

            foreach ($ancestors as $_ancestor) {
                $_config = $self->getStoredConfigurations(
                    $_ancestor->_class,
                    $_ancestor->_id,
                    $config_keys,
                    $strategy
                );

                $configs[] = [
                    'object'        => $_ancestor,
                    'config'        => $_config,
                    'config_parent' => $parent_config,
                ];

                $parent_config = array_merge($parent_config, $_config);
            }
        }

        return $configs;
    }

    /**
     * Get the usable configuration values of an object
     *
     * @param string         $config_inherit The inheritance path
     * @param string         $module         The module to get the configuration of
     * @param array          $config_keys    The keys to get the configuration value of
     * @param CMbObject|null $object         Object
     *
     * @return array The corresponding configs
     * @throws CMbException
     * @throws InvalidArgumentException
     */
    public static function getConfigs(
        string    $config_inherit,
        string    $module,
        array     $config_keys = [],
        CMbObject $object = null
    ): array {
        $ancestor_configs = self::getAncestorsConfigs($config_inherit, $module, $config_keys, $object);

        $configs = [];

        foreach ($ancestor_configs as $_ancestor) {
            $configs = array_merge($configs, $_ancestor['config']);
        }

        return $configs;
    }

    /**
     * Get module configurations
     *
     * @param string       $module  The module
     * @param array|string $inherit The inherit schema
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public static function getModuleConfigs(string $module, $inherit = []): array
    {
        $model = self::_getModel($module, $inherit);

        $configs      = [];
        $module_start = "{$module} ";

        foreach ($model as $_inherit => $_configs) {
            $_conf = [];

            foreach ($_configs as $_feature => $_spec) {
                if (strpos($_feature, $module_start) === false) {
                    continue;
                }

                $_conf[$_feature] = $_spec;
            }

            $configs[$_inherit] = $_conf;
        }

        return $configs;
    }

    /**
     * Get class configs
     *
     * @param string       $class   Class name
     * @param string       $module  Module
     * @param array|string $inherit Inheritance schema
     * @param bool         $flatten Flatten output
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public static function getClassConfigs(string $class, string $module, $inherit = [], bool $flatten = true): array
    {
        $configs = [];

        $model = self::getModuleConfigs($module, $inherit);

        $patterns = ["{$class} ", "{$class}."];

        foreach ($model as $_inherit => $_configs) {
            foreach ($patterns as $_patt) {
                $_inherit = self::simplifyInherit($_inherit);

                // Faster than preg_match
                if (($_inherit === $class) || (strpos($_inherit, $_patt) !== false)) {
                    if ($flatten) {
                        $configs = array_merge($configs, $_configs);
                    } else {
                        $configs[$_inherit] = $_configs;
                    }
                    break;
                }
            }
        }

        return $configs;
    }

    /**
     * Todo: Move implementation here
     * Get the object tree
     *
     * @param string $inherit Inheritance path
     *
     * @return array The object tree
     */
    public static function getObjectTree(string $inherit): array
    {
        return CConfiguration::getObjectTree($inherit);
    }

    ///**
    // * Get the context inherited list in cache
    // *
    // * @param string $module Module name
    // *
    // * @todo Use this to handle configuration regeneration by context (not compatible with distributed servers yet)
    // *
    // * @return Cache
    // */
    //static protected function getContextInheritanceCache($module) {
    //  return new Cache('config-context', $module, Cache::OUTER);
    //}
}
