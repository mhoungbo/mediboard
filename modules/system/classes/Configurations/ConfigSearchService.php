<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Configurations;

use DOMException;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbConfig;
use Ox\Core\CMbDT;
use Ox\Core\CMbString;
use Ox\Core\CMbXMLDocument;
use Ox\Core\Config\Conf;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Locales\Translator;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\CConfigSearch;
use Ox\Mediboard\System\CConfigurationCompare;
use Ox\Mediboard\System\ContextualConfigurationManager;
use Ox\Mediboard\System\CPreferences;
use Ox\Mediboard\System\CTranslationOverwrite;
use Ox\Mediboard\System\Pref;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Utility functions for Config searcher.
 */
class ConfigSearchService
{
    private ?string $keywords   = null;
    private ?int    $start      = null;
    private ?int    $step       = null;
    private int     $total      = 0;
    private ?bool   $configs    = null;
    private ?bool   $prefs      = null;
    private ?bool   $func_perms = null;

    public function __construct(
        private readonly ?RequestParams $params = null,
        private readonly ?Conf          $conf = null,
        private readonly ?Pref          $pref = null,
        private readonly ?Translator    $translator = null,
    ) {
    }

    /**
     * @throws Exception
     */
    public function initFromRequestParams(): self
    {
        if (!$this->params) {
            return $this;
        }

        $this->start      = $this->params->get('start', 'num default|0');
        $this->step       = $this->params->get('step', 'num default|30');
        $this->configs    = $this->params->get('configs', 'bool default|0');
        $this->prefs      = $this->params->get('prefs', 'bool default|0');
        $this->func_perms = $this->params->get('func_perms', 'bool default|0');
        $this->keywords   = $this->params->get('keywords', 'str');

        return $this;
    }

    /**
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function list(): array
    {
        if (!$this->configs && !$this->prefs && !$this->func_perms) {
            $this->configs = $this->prefs = $this->func_perms = true;
        }

        $all_locales   = [];
        $locales_files = CAppUI::getLocalesFromFiles();

        // Configurations
        if ($this->configs) {
            $all_locales = $this->getConfigs(
                CConfigSearch::getConfigs(),
                $locales_files,
                $all_locales,
                CModule::getInstalled()
            );
        }

        // Preferences
        if ($this->prefs) {
            CPreferences::loadModules();
            $all_locales = $this->getPrefs(
                CPreferences::$modules,
                $locales_files,
                $all_locales,
                CConfigSearch::TYPE_CONFIG_PREF
            );
        }

        // Functional permissions
        if ($this->func_perms) {
            CPreferences::$modules = [];
            CPreferences::loadModules(true);
            $all_locales = $this->getPrefs(
                CPreferences::$modules,
                $locales_files,
                $all_locales,
                CConfigSearch::TYPE_CONFIG_FUNC_PERM
            );
        }

        // Don't warn about unlocalized string (there are too many !)
        CAppUI::$unlocalized = [];

        $filtered = [];
        if ($this->keywords) {
            $keywords       = CMbString::removeDiacritics($this->keywords);
            $keywords_split = explode(' ', $keywords);

            $filtered = array_filter(
                $all_locales,
                function ($value) use ($keywords_split) {
                    foreach ($keywords_split as $_keyword) {
                        if (stripos($value['search'], $_keyword) === false) {
                            return false;
                        }
                    }

                    return true;
                }
            );

            uasort(
                $filtered,
                fn($a, $b) => strnatcmp($a['tr'], $b['tr'])
            );
        }

        $this->total  = count($filtered);
        $filter_limit = array_splice($filtered, $this->start, $this->step);

        foreach ($filter_limit as $_key => &$_filtered) {
            $key = substr($_key, strpos($_key, '-') + 1);

            $_filtered['value'] = match ((int)$_filtered['type']) {
                CConfigSearch::TYPE_CONFIG_INSTANCE => $this->conf->get(str_replace('-', ' ', $key)),
                CConfigSearch::TYPE_CONFIG_ETAB => $this->conf->getContextual(str_replace('-', ' ', $key), 'global'),
                CConfigSearch::TYPE_CONFIG_STATIC => $this->conf->getStatic(str_replace('-', ' ', $key)),
                CConfigSearch::TYPE_CONFIG_PREF, CConfigSearch::TYPE_CONFIG_FUNC_PERM => $this->pref->get($key),
            };
        }

        return $filter_limit;
    }

    /**
     * @throws InvalidArgumentException
     * @throws DOMException
     * @throws Exception
     */
    public function export(): array
    {
        $xml               = new CMbXMLDocument('utf-8');
        $xml->formatOutput = true;
        $root              = $xml->createElement('configurations-export');
        $root->setAttribute('date', CMbDT::dateTime());
        $xml->appendChild($root);

        $configs = $GLOBALS['dPconfig'];
        foreach ($configs as $_key => $_config) {
            if (in_array($_key, CMbConfig::$forbidden_values) || in_array($_key, ['db', 'php'])) {
                unset($configs[$_key]);
            }
        }

        $instance_confs = [];
        CMbConfig::buildConf($instance_confs, $configs, null);

        $instance_configs = $xml->createElement('instance-configs');
        $root->appendChild($instance_configs);

        foreach ($instance_confs as $_feature => $_value) {
            $_node = $xml->createElement('instance-config');
            $_node->setAttribute('feature', CConfigurationCompare::sanitizeString($_feature));
            $_node->setAttribute('value', CConfigurationCompare::sanitizeString($_value));
            $instance_configs->appendChild($_node);
        }

        // CConfigurations
        $group = CGroups::loadCurrent();

        $groups_configs = $xml->createElement('groups-configs');
        $groups_configs->setAttribute('context', CConfigurationCompare::sanitizeString($group->_guid));
        $groups_configs->setAttribute('real_name', CConfigurationCompare::sanitizeString($group->text));
        $root->appendChild($groups_configs);

        foreach (CModule::getInstalled() as $_mod) {
            // Get the configurations for the module
            // If no configuration for the module, continue
            try {
                $values = ContextualConfigurationManager::get($_mod->mod_name)->getValues($group->_class, $group->_id);
            } catch (Exception) {
                continue;
            }

            $_node_mod = $xml->createElement('module-config');
            $_node_mod->setAttribute('mod_name', CConfigurationCompare::sanitizeString($_mod->mod_name));

            if (!$values) {
                continue;
            }

            foreach ($values as $_sub_group => $_values) {
                if (!$_values || !is_array($_values)) {
                    continue;
                }

                // Add the module node
                $groups_configs->appendChild($_node_mod);

                foreach ($_values as $_sub_context => $_confs) {
                    $_sub_context = "{$_mod->mod_name} {$_sub_group} {$_sub_context}";
                    if (!is_array($_confs)) {
                        $_node_sub_context = $xml->createElement('config');
                        $_node_mod->appendChild($_node_sub_context);
                        $_node_sub_context->setAttribute(
                            'feature',
                            CConfigurationCompare::sanitizeString($_sub_context)
                        );
                        $_node_sub_context->setAttribute('value', CConfigurationCompare::sanitizeString($_confs));
                        continue;
                    }

                    foreach ($_confs as $_conf_name => $_conf_value) {
                        if (is_array($_conf_value)) {
                            foreach ($_conf_value as $_feature => $_value) {
                                if (is_array($_value)) {
                                    foreach ($_value as $_sub_feature => $_sub_value) {
                                        $_node_leaf = $xml->createElement('config');
                                        $_node_leaf->setAttribute(
                                            'feature',
                                            CConfigurationCompare::sanitizeString(
                                                "{$_sub_context} {$_conf_name} {$_feature} {$_sub_feature}"
                                            )
                                        );
                                        $_node_leaf->setAttribute(
                                            'value',
                                            CConfigurationCompare::sanitizeString($_sub_value)
                                        );
                                        $_node_mod->appendChild($_node_leaf);
                                    }
                                } else {
                                    $_node_leaf = $xml->createElement('config');
                                    $_node_leaf->setAttribute(
                                        'feature',
                                        CConfigurationCompare::sanitizeString(
                                            "{$_sub_context} {$_conf_name} {$_feature}"
                                        )
                                    );
                                    $_node_leaf->setAttribute('value', CConfigurationCompare::sanitizeString($_value));
                                    $_node_mod->appendChild($_node_leaf);
                                }
                            }
                        } else {
                            $_node = $xml->createElement('config');
                            $_node->setAttribute(
                                'feature',
                                CConfigurationCompare::sanitizeString("{$_sub_context} {$_conf_name}")
                            );
                            $_node->setAttribute('value', CConfigurationCompare::sanitizeString($_conf_value));
                            $_node_mod->appendChild($_node);
                        }
                    }
                }
            }
        }

        return ['content' => $xml->saveXML(), 'title' => CMbString::removeDiacritics($group->text)];
    }

    public function getStoredTranslation(
        string $feature,
        string $language,
        array  $locales,
        bool   $description = false
    ): CTranslationOverwrite {
        $translation           = new CTranslationOverwrite();
        $translation->language = $language;
        $translation->source   = $feature . (($description) ? '-desc' : null);
        $translation->loadMatchingObjectEsc();

        if ($translation && $translation->_id) {
            $translation->loadOldTranslation($locales);
        } elseif (isset($locales[$translation->source])) {
            $translation->_old_translation = $locales[$translation->source];
        }

        return $translation;
    }

    /**
     * @throws Exception
     */
    public function storeTranslation(
        string $translation,
        string $feature,
        string $language,
        bool   $description = false
    ): false|CTranslationOverwrite {
        $translation = trim(stripcslashes($translation));

        if ($translation) {
            $trans           = new CTranslationOverwrite();
            $trans->language = $language;
            $trans->source   = $feature . (($description) ? '-desc' : null);
            $trans->loadMatchingObjectEsc();

            if (!$trans->_id || ($trans->_id && ($trans->translation != $translation))) {
                $trans->translation = $translation;

                if ($msg = $trans->store()) {
                    throw new Exception($msg);
                } else {
                    return $trans;
                }
            }
        }

        return false;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getStart(): ?int
    {
        return $this->start;
    }

    public function getStep(): ?int
    {
        return $this->step;
    }

    private function getConfigs(array $all_configs, array $locales_files, array $all_locales, array $modules): array
    {
        foreach ($all_configs as $_key => $_type) {
            $_module    = null;
            $_first_sep = strpos($_key, ' ');
            if ($_first_sep !== false) {
                $_module = substr($_key, 0, $_first_sep);
            }

            $module = $modules[$_module] ?? '';

            $_key = str_replace(' ', '-', $_key);

            $config_key = "config-{$_key}";

            $tr       = $this->translator->tr($config_key);
            $tr_desc  = $this->translator->tr("{$config_key}-desc");
            $old_tr   = $locales_files[$config_key] ?? '';
            $old_desc = $locales_files["{$config_key}-desc"] ?? '';

            $all_locales[$config_key] = [
                'tr'                    => $tr,
                'desc'                  => $tr_desc,
                'old_tr'                => $old_tr,
                'old_desc'              => $old_desc,
                'search'                => CMbString::removeDiacritics("{$tr} {$tr_desc} {$old_tr} {$old_desc}"),
                'module'                => $_module,
                'module_configure_path' => ($module instanceof CModule) ? $module->getConfigureTab() : '',
                'type'                  => $_type,
                'value'                 => '',
            ];
        }

        return $all_locales;
    }

    private function getPrefs(array $modules, array $locales_files, array $all_locales, string $type): array
    {
        foreach ($modules as $_module => $_prefs) {
            foreach ($_prefs as $_pref) {
                $pref_key = "pref-{$_pref}";

                $tr       = $this->translator->tr($pref_key);
                $tr_desc  = $this->translator->tr("{$pref_key}-desc");
                $old_tr   = $locales_files[$pref_key] ?? '';
                $old_desc = $locales_files["{$pref_key}-desc"] ?? '';

                $all_locales[$pref_key] = [
                    'tr'                    => $tr,
                    'desc'                  => $tr_desc,
                    'old_tr'                => $old_tr,
                    'old_desc'              => $old_desc,
                    'search'                => CMbString::removeDiacritics(
                        "{$tr} {$tr_desc} {$old_tr} {$old_desc}"
                    ),
                    'module'                => $_module,
                    'module_configure_path' => '',
                    'type'                  => $type,
                    'value'                 => '',
                ];
            }
        }

        return $all_locales;
    }
}
