<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Configurations;

/**
 * Utility class enabling us to filter out admin-only configurations.
 */
class ConfigurationFilter
{
    private array $filtered_configs;
    private array $filtered_keys;

    public function __construct(array $configs, bool $admin)
    {
        if ($admin) {
            // We only keep admin configurations
            $this->filtered_configs = array_filter($configs, fn($v) => !(($v['onlyAdmin']) ?? false));
        } else {
            // We only keep non-admin configurations
            $this->filtered_configs = array_filter($configs, fn($v) => ($v['onlyAdmin']) ?? false);
        }

        $this->filtered_keys = array_keys($this->filtered_configs);
    }

    public function filterConfigs(array $configs): array
    {
        return array_diff_key($configs, $this->filtered_configs);
    }

    public function filterAncestors(array $ancestors): array
    {
        foreach ($ancestors as $k => $configs) {
            foreach ($configs['config'] as $key => $config_list) {
                if (in_array($key, $this->filtered_keys)) {
                    unset($ancestors[$k]['config'][$key]);
                }
            }

            foreach ($configs['config_parent'] as $key => $config_list) {
                if (in_array($key, $this->filtered_keys)) {
                    unset($ancestors[$k]['config_parent'][$key]);
                }
            }
        }

        return $ancestors;
    }

    public function filterFeatures(array $features): array
    {
        return array_filter(
            $features,
            fn($k) => !in_array($k, $this->filtered_keys),
            ARRAY_FILTER_USE_KEY
        );
    }
}
