<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Configurations;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbConfig;
use Ox\Core\CSQLDataSource;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Module\CModule;
use Ox\Core\Redis\CRedisClient;

class ConfigurationUpdater
{
    private string $module_name;
    private bool   $ajax = false;

    private array $configs_post      = [];
    private array $allowed_configs   = [];
    private array $forbidden_configs = [];

    /**
     * @throws Exception
     */
    public function __construct(string $module_name, array $configs_post)
    {
        $this->module_name  = $module_name;
        $this->configs_post = $configs_post;

        if (!$this->module_name) {
            throw new Exception('Missing module name');
        }

        if (!$this->configs_post) {
            throw new Exception('Missing configs to update');
        }

        $this->ajax = (bool)CMbArray::extract($this->configs_post, 'ajax');
    }

    /**
     * Update configurations from POST request (DB + file)
     *
     * @throws Exception
     */
    public function updateConfigs(): void
    {
        // Check if module is installed and if user is can admin this module
        $this->checkModule();

        // Sanitize posted configurations and exclude forbidden
        $this->sanitizeConfigsPost();

        // Filter forbidden and allowed configs
        $this->filterConfigs();

        // Update configs in DB
        $this->updateDbConfigs();

        // Update configs in file
        $this->updateFileConfigs();

        // Include config in DB
        CMbConfig::loadValuesFromDB();

        // If request ajax
        $this->disableLogs();
    }

    /**
     * Check if module is installed and check admin permission on the module
     *
     * @throws AccessDeniedException
     */
    private function checkModule(): void
    {
        $m = CModule::getInstalled(CModule::prefixModuleName($this->module_name));
        if (!$m || !$m->canAdmin()) {
            throw new AccessDeniedException('CModule-msg-no permission to edit configs');
        }
    }

    /**
     * Unset forbidden configurations
     */
    private function sanitizeConfigsPost(): void
    {
        CMbArray::extract($this->configs_post, 'm');
        CMbArray::extract($this->configs_post, 'module');
        CMbArray::extract($this->configs_post, 'dosql');
        CMbArray::extract($this->configs_post, 'suppressHeaders');
        CMbArray::extract($this->configs_post, '@config');
    }

    /**
     * Filter configs allowed to be store in DB, forbidden will only be store in file
     */
    private function filterConfigs(): void
    {
        foreach ($this->configs_post as $key => $_config) {
            if (in_array($key, CMbConfig::$forbidden_values) || ($key == 'db')) {
                $this->forbidden_configs[$key] = $_config;
            } else {
                $this->allowed_configs[$key] = $_config;
            }
        }
    }

    /**
     * Update database (allowed) configurations
     *
     * @throws Exception
     */
    private function updateDbConfigs(): void
    {
        $this->allowed_configs = CMbArray::mapRecursive('stripslashes', $this->allowed_configs, apply_on_keys: false);

        $list = [];
        CMbConfig::buildConf($list, $this->allowed_configs, null);

        foreach ($list as $key => $value) {
            if (!CMbConfig::setConfInDB($key, $value)) {
                CAppUI::setMsg('Configure-failed-modify', CAppUI::UI_MSG_ERROR);
            } else {
                CAppUI::setMsg('Configure-success-modify');
            }
        }
    }

    /**
     * Update file (forbidden) configurations
     *
     * @throws Exception
     */
    private function updateFileConfigs(): void
    {
        $mbConfig = new CMbConfig();

        try {
            if ($mbConfig->update($this->forbidden_configs)) {
                CAppUI::setMsg('Configure-success-modify');
            } else {
                CAppUI::setMsg('Configure-failed-modify', CAppUI::UI_MSG_ERROR);
            }
        } catch (Exception $e) {
            CAppUI::setMsg('Configure-failed-modify', CAppUI::UI_MSG_ERROR, $e->getMessage());
        }

        // Load configs from files
        $mbConfig->load();

        // Update global configs
        $GLOBALS['dPconfig'] = $mbConfig->values;
    }

    /**
     * @throws Exception
     */
    private function disableLogs(): void
    {
        if ($this->ajax) {
            CSQLDataSource::$log = false;
            CRedisClient::$log   = false;
        }
    }
}
