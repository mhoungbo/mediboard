<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Etablissement\CGroups;

/**
 * G�re les messages de l'administrateur
 */
class CMessage extends CMbObject
{
    public const MESSAGE_STATUS_PRESENT = 'present';
    public const MESSAGE_STATUS_PAST    = 'past';
    public const MESSAGE_STATUS_FUTURE  = 'future';

    // DB Table key
    public $message_id;

    // DB fields
    public $module_id;
    public $group_id;

    public $deb;
    public $fin;
    public $titre;
    public $corps;
    public $urgence;

    // Form fields
    public $_status;

    // Behaviour fields
    public $_email_send;
    public $_email_from;
    public $_email_to;
    public $_email_details;

    public $_update_moment;
    public $_update_initiator;
    public $_update_benefits;

    // Object references
    public $_ref_module_object;
    public $_ref_group;

    public                       $_ref_acquittals;
    public ?CMessageAcquittement $_ref_current_ack = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = 'message';
        $spec->key   = 'message_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props              = parent::getProps();
        $props["deb"]       = "dateTime notNull";
        $props["fin"]       = "dateTime notNull";
        $props["titre"]     = "str notNull maxLength|40";
        $props["corps"]     = "text notNull markdown";
        $props["urgence"]   = "enum notNull list|normal|urgent default|normal";
        $props["module_id"] = "ref class|CModule back|messages";
        $props["group_id"]  = "ref class|CGroups back|messages";

        $props["_status"]           = "enum list|past|present|future";
        $props["_email_from"]       = "str";
        $props["_email_to"]         = "str";
        $props["_email_details"]    = "text";
        $props["_update_moment"]    = "dateTime";
        $props["_update_initiator"] = "str";
        $props["_update_benefits"]  = "text";

        return $props;
    }

    /**
     * Loads messages from a publication date perspective
     *
     * @param string|null $status Wanted status, null for all
     *
     * @return self[] Published messages
     * @throws Exception
     */
    public function loadMessagesList(
        ?string $status,
        int     $start = 0,
        int     $step = 50
    ) {
        return $this->loadList($this->buildDateCondition($status, CMbDT::dateTime()), "deb DESC", "$start,$step") ?? [];
    }

    /**
     * Load publications to display to the user.
     *
     * @throws Exception
     */
    public function loadPublicationsForUser(int $user_id, string $mod_name, int $group_id): array
    {
        $ds = $this->getDS();

        $where = $this->buildDateCondition(self::MESSAGE_STATUS_PRESENT, CMbDT::dateTime());

        $where[] = $ds->prepare('group_id = ? OR group_id IS NULL', $group_id);

        $module = CModule::getInstalled($mod_name);
        if ($module instanceof CModule) {
            $where[] = $ds->prepare('module_id = ? OR module_id IS NULL', $module->_id);
        }

        $ljoin = [
            'acquittement_msg_system' => $ds->prepare(
                'acquittement_msg_system.message_id = message.message_id AND acquittement_msg_system.user_id = ?',
                $user_id
            ),
        ];

        $where['acquittement_msg_system.date_ack'] = 'IS NULL';

        $messages = $this->loadList($where, 'deb DESC', null, null, $ljoin);

        $this->prepareAcquittals($messages, $user_id, CMbDT::dateTime());

        return $messages ?: [];
    }

    /**
     * @throws Exception
     */
    public function countMessagesList(?string $status): int
    {
        return $this->countList($this->buildDateCondition($status, CMbDT::dateTime()));
    }

    private function buildDateCondition(?string $status, string $datetime): array
    {
        switch ($status) {
            case self::MESSAGE_STATUS_PAST:
                return ['fin' => $this->getDS()->prepare('< ?', $datetime)];
            case self::MESSAGE_STATUS_PRESENT:
                return [$this->getDS()->prepare('? BETWEEN deb AND fin', $datetime)];
            case self::MESSAGE_STATUS_FUTURE:
                return ['deb' => $this->getDS()->prepare('> ?', $datetime)];
            default:
                return [];
        }
    }

    /**
     * @inheritdoc
     */
    public function store(): ?string
    {
        $msg = parent::store();
        $this->sendEmail();

        return $msg;
    }

    /**
     * Sends the email
     *
     * @throws \PHPMailer\PHPMailer\Exception
     * @throws Exception
     */
    private function sendEmail(): void
    {
        if (!$this->_email_send) {
            return;
        }

        try {
            // Source init
            /** @var CSourceSMTP $source */
            $source = CExchangeSource::get("system-message", CSourceSMTP::TYPE);
            $source->init();
            $source->addTo($this->_email_to);
            $source->addBcc($this->_email_from);
            $source->addRe($this->_email_from);

            // Email subject
            $page_title = CAppUI::conf("page_title");
            $source->setSubject("$page_title - $this->titre");

            // Email body
            $info = CAppUI::tr("CMessage-info-published");
            $body = "<strong>$page_title</strong> $info<br />";
            $body .= $this->getFieldContent("titre");
            $body .= $this->getFieldContent("deb");
            $body .= $this->getFieldContent("fin");
            $body .= $this->getFieldContent("module_id");
            $body .= $this->getFieldContent("group_id");
            $body .= $this->getFieldContent("corps");
            $body .= $this->getFieldContent("_email_details");
            $source->setBody($body);
            // Do send
            $source->send();
        } catch (CMbException $e) {
            $e->stepAjax();

            return;
        }

        CAppUI::setMsg("CMessage-email_sent");
    }

    /**
     * Builds user readable data
     */
    private function getFieldContent($field_name): ?string
    {
        if (!$this->$field_name) {
            return null;
        }

        // Build content
        $label = $this->getLabelElement($field_name);
        $value = $this->getHtmlValue($field_name);

        return "<br/ >$label : <strong>$value</strong>\n";
    }

    /**
     * @inheritdoc
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();
        $this->_view = $this->titre;
    }

    /**
     * Load module references by the message
     *
     * @return null|CModule
     * @throws Exception
     */
    public function loadRefModuleObject()
    {
        $module      = $this->loadFwdRef("module_id", true);
        $this->_view = ($module->_id ? "[$module] - " : "") . $this->titre;

        /** @var CModule */
        return $this->_ref_module_object = $module;
    }

    /**
     * Load group
     *
     * @return null|CGroups
     * @throws Exception
     */
    public function loadRefGroup()
    {
        /** @var CGroups */
        return $this->_ref_group = $this->loadFwdRef("group_id", true);
    }

    /**
     * @return null|array
     * @throws Exception
     */
    public function loadRefAcquittals()
    {
        return $this->_ref_acquittals = $this->loadBackRefs("acquittals");
    }

    /**
     * Load or create acquittals for the messages list.
     *
     * @throws Exception
     */
    private function prepareAcquittals(array $messages, int $user_id, string $datetime): void
    {
        CStoredObject::massLoadBackRefs(
            $messages,
            'acquittals',
            null,
            ['user_id' => $this->getDS()->prepare('= ?', $user_id)]
        );

        /** @var CMessage $message */
        foreach ($messages as $message) {
            if ($acks = $message->_back['acquittals']) {
                $ack = reset($acks);
            } else {
                $ack               = new CMessageAcquittement();
                $ack->message_id   = $message->_id;
                $ack->user_id      = $user_id;
                $ack->date_display = $datetime;
                $ack->store();
            }

            $message->_ref_current_ack = $ack;
        }
    }
}
