<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\ClientInterface;
use Ox\Core\Contracts\Client\FileSystemClientInterface;
use Ox\Interop\Eai\Resilience\ClientContext;
use Ox\Mediboard\System\Sources\CSourceFile;
use Ox\Mediboard\System\Sources\ObjectPath;

/**
 * Class CSourceFileSystem
 */
class CSourceFileSystem extends CSourceFile
{
    // Source type
    public const TYPE = 'file_system';
    public const TYPE_FILE = 'FS';

    /** @var string */
    protected const DEFAULT_CLIENT = self::CLIENT_FILE_SYSTEM;

    /** @var array */
    protected const CLIENT_MAPPING = [
        self::CLIENT_FILE_SYSTEM => CFileSystem::class,
    ];

    /** @var string */
    public const CLIENT_FILE_SYSTEM = 'file_system';

    /** @var FileSystemClientInterface */
    public $_client;

    // DB Table key

    /** @var int */
    public $source_file_system_id;

    /** @var string */
    public $fileextension;

    /** @var string */
    public $fileextension_write_end;

    /** @var string */
    public $fileprefix;

    /** @var string */
    public $sort_files_by;

    /** @var bool */
    public $delete_file;

    /** @var string */
    public $ack_prefix;

    // Form fields

    /** @var int Legacy field */
    public $_limit;

    /**
     * @inheritdoc
     */
    public function getSpec()
    {
        $spec        = parent::getSpec();
        $spec->table = "source_file_system";
        $spec->key   = "source_file_system_id";

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps()
    {
        $props                            = parent::getProps();
        $props["fileextension"]           = "str";
        $props["fileextension_write_end"] = "str";
        $props["fileprefix"]              = "str";
        $props["sort_files_by"]           = "enum list|date|name|size default|name";
        $props["delete_file"]             = "bool default|1";
        $props["ack_prefix"]              = "str";

        return $props;
    }

    /**
     * @return FileSystemClientInterface
     * @throws CMbException
     */
    public function getClient(): FileSystemClientInterface
    {
        /** @var FileSystemClientInterface $client_fs */
        $client_fs = parent::getClient();

        return $client_fs;
    }

    /**
     * @return FileSystemClientInterface
     * @throws CMbException
     */
    public function disableResilience(): FileSystemClientInterface
    {
        /** @var FileSystemClientInterface $client_filesystem */
        $client_filesystem = parent::disableResilience();

        return $client_filesystem;
    }

    /**
     * @return FileSystemClientInterface
     * @throws CMbException
     */
    public function reactivateResilience(): FileSystemClientInterface
    {
        /** @var FileSystemClientInterface $client_filesystem */
        $client_filesystem = parent::reactivateResilience();

        return $client_filesystem;
    }

    /**
     * @inheritDoc
     */
    protected function getResilientClient(ClientInterface $client): ?FileSystemClientInterface
    {
        /** @var FileSystemClientInterface $client */
        $client = parent::getResilientClient($client);

        return $client ? new ResilienceFileSystemClient($client, $this) : null;
    }


    function updateFormFields()
    {
        parent::updateFormFields();

        $this->_view = $this->host;
    }

    /**
     * @inheritDoc
     */
    public function startLog(ClientContext $context): void
    {
        $function_name = $context->getArguments()['function_name'] ?? null;

        $input                     = $context->getRequest() ?: null;
        $this->_current_echange    = $echange_fs = new CExchangeFileSystem();
        $echange_fs->date_echange  = CMbDT::dateTime();
        $echange_fs->emetteur      = CAppUI::conf("mb_id");
        $echange_fs->destinataire  = $this->host;
        $echange_fs->source_class  = $this->_class;
        $echange_fs->source_id     = intval($this->_id);
        $echange_fs->function_name = $function_name ?: '';
        $echange_fs->input         = $input !== null ?
            serialize(CMbArray::mapRecursive([CSourceFile::class, "truncate"], $input, apply_on_keys: false)) : null;
    }

    /**
     * @inheritDoc
     */
    public function stopLog(ClientContext $context): void
    {
        /** @var CExchangeFileSystem $echange_fs */
        if (!($echange_fs = $this->_current_echange)) {
            return;
        }

        $echange_fs->response_time = $this->_current_chronometer->total;

        // response time
        $echange_fs->response_datetime = CMbDT::dateTime();

        // Truncate input and output before storing
        $output             = $context->getResponse();
        $echange_fs->output = $output !== null
            ? serialize(CMbArray::mapRecursive([CSourceFile::class, "truncate"], $output, apply_on_keys: false)) : null;

        $this->registerStoreInCallback($echange_fs);
    }

    /**
     * @inheritDoc
     */
    public function exceptionLog(ClientContext $context): void
    {
        /** @var CExchangeFileSystem $echange_fs */
        if (!$echange_fs = $this->_current_echange) {
            return;
        }

        $echange_fs->response_datetime = CMbDT::dateTime();
        $throwable                     = $context->getThrowable();
        $output                        = $context->getResponse();
        if (!$output && $throwable) {
            $output = $throwable->getMessage();
        }

        $echange_fs->output = $output !== null ?
            serialize(CMbArray::mapRecursive([CSourceFile::class, "truncate"], $output, apply_on_keys: false)) : null;

        $this->registerStoreInCallback($echange_fs);
    }

    /**
     * @param string $file_path
     *
     * @return ObjectPath
     */
    public function completePath(?string $file_path = null): ObjectPath
    {
        return new ObjectPath($this->host, $this->fileprefix, $file_path, $this->fileextension);
    }
}
