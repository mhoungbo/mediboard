<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use LogicException;
use Ox\Core\CMbException;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CStoredObject;
use Ox\Core\EncryptedPropertiesInterface;
use Ox\Core\FieldSpecs\CPasswordSpec;
use Ox\Core\Security\Exception\CouldNotEncrypt;
use Throwable;

/**
 * Handle an object before and after storing encrypted properties.
 */
class EncryptedPropertyManager
{
    private CStoredObject&EncryptedPropertiesInterface $object;

    /** @var array<array{0: CPasswordSpec, 1: string}> */
    private array $properties = [];

    /** @var CPasswordSpec[] */
    private array $properties_to_remove = [];

    public function __construct(CStoredObject&EncryptedPropertiesInterface $object)
    {
        $this->object = $object;
    }

    /**
     * /!\ MUST be called right BEFORE the CStoredObject::store.
     *
     * Compute which changes are made onto the object:
     *   - fields to encrypt
     *   - encrypted fields to update
     *   - encrypted fields to delete
     *
     * @return void
     */
    public function register(): void
    {
        /** @var CPasswordSpec $spec */
        foreach ($this->object::getEncryptedPropertiesSpecs($this->object) as $spec) {
            $form_field = $spec->formField;
            $form_value = $this->object->{$form_field};

            // Form field is NULL, no need to process that field
            if ($form_value === null) {
                continue;
            }

            // We want to empty the field
            if ($form_value === '') {
                $this->addPropertyForRemoving($spec);
            } else {
                // Adding the property in order to mark it as to be created or updated
                $this->addProperty($spec, $form_value);
            }
        }
    }

    private function addProperty(CPasswordSpec $spec, string $clear): void
    {
        $this->properties[] = [$spec, $clear];
    }

    private function addPropertyForRemoving(CPasswordSpec $spec): void
    {
        $this->properties_to_remove[] = $spec;
    }

    /**
     * /!\ MUST be called right AFTER the CStoredObject::store().
     *
     * Process the updates detected in self::register().
     *
     * @return void
     * @throws Keys\Exceptions\CouldNotGetKeysFromStorage
     * @throws Keys\Exceptions\CouldNotUseKey
     * @throws CMbException
     * @throws CMbModelNotFoundException
     * @throws CouldNotEncrypt
     */
    public function process(): void
    {
        if (!$this->object->_class || !$this->object->_id) {
            throw new LogicException('Not already persisted object cannot be encrypted');
        }

        // Nothing to do
        if (!$this->properties && !$this->properties_to_remove) {
            return;
        }

        // Properties to store
        foreach ($this->properties as $property) {
            [$spec, $value] = $property;

            $ref_field  = $spec->fieldName;
            $form_field = $spec->formField;

            // Fwd ref may not been set
            $this->object->completeField($ref_field);
            $ref_value = ($this->object->{$ref_field}) ?? null;

            if ($ref_value === null) {
                // Encrypted property does not exist yet - we firstly create it
                $encrypted = CEncryptedProperty::createFor($this->object, $spec, $value);
            } else {
                // Encrypted property already exists - we simply update it
                $encrypted = CEncryptedProperty::findOrFail($ref_value);
                $encrypted->updateTo($value);
            }

            // Setting the property
            $this->object->{$ref_field} = $encrypted->_id;

            // Emptying the form field in order to avoid infinite loops on object storing
            $this->object->{$form_field} = null;
        }

        // Properties to delete
        $encrypted_properties_to_delete = [];
        foreach ($this->properties_to_remove as $spec) {
            $ref_field  = $spec->fieldName;
            $form_field = $spec->formField;

            $ref_value = ($this->object->{$ref_field}) ?? null;

            // Fwd ref may not been set
            $this->object->completeField($ref_field);

            // In case where to form field is set (to be emptied), but we do not have a set plain field
            if ($ref_value) {
                // Adding the CEncryptedProperty in order to delete it
                $encrypted_properties_to_delete[] = CEncryptedProperty::findOrFail($ref_value);
            }

            // Marking the fwd ref for emptying
            $this->object->{$ref_field} = '';

            // Emptying the form field in order to avoid infinite loops on object storing
            $this->object->{$form_field} = null;
        }

        // Re-storing the object with updated fields
        try {
            if ($msg = $this->object->store()) {
                throw new Exception($msg);
            }
        } catch (Throwable $t) {
            throw new Exception($t->getMessage());
        }

        // Deleting encrypted properties that need to
        try {
            /** @var CEncryptedProperty $property */
            foreach ($encrypted_properties_to_delete as $property) {
                if ($msg = $property->delete()) {
                    throw new Exception($msg);
                }
            }
        } catch (Throwable $t) {
            // Nothing?
        }
    }
}
