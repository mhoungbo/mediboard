<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use DOMDocument;
use DOMElement;
use DOMXPath;
use Ox\Core\CMbString;
use Ox\Core\Locales\Translator;
use Ox\Core\Module\CModule;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CConfigurationCompare
{
    protected DOMXPath $xpath;
    protected string   $file;
    protected array    $files_names = [];
    protected array    $result      = [];

    private Translator $translator;

    protected static array $xpath_queries = [
        'check_file_type'  => '/configurations-export',
        'configs_instance' => [
            'root'   => 'instance-configs',
            'config' => 'instance-config',
        ],
        'configs_groups'   => [
            'root'    => 'groups-configs',
            'modules' => 'module-config',
            'configs' => 'config',
        ],
    ];

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Compare all the XML conf files from a directory
     *
     * @param UploadedFile[] $files
     */
    public function compare(array $files): void
    {
        foreach ($files as $file) {
            $this->file          = $file->getClientOriginalName();
            $this->files_names[] = self::unsanitizeString($this->file);
            $this->getConfigurations($file->getPathname());
        }
    }

    /**
     * Call the method to get all the configurations of a kind
     *
     * @param string $file_path Path to the file to parse
     */
    protected function getConfigurations(string $file_path): void
    {
        $this->openXMLFile($file_path);

        if (!$this->isFileValid()) {
            return;
        }

        $this->getInstanceConfigs();
        $this->getGroupsConfigs();
    }

    /**
     * Open an XML file
     *
     * @param string $file_path Path to the file
     */
    protected function openXMLFile(string $file_path): void
    {
        $dom = new DOMDocument();

        // Suppression des caractères invalides pour DOMDocument
        $dom->loadXML(CMbString::convertHTMLToXMLEntities(file_get_contents($file_path)));
        $this->xpath = new DOMXPath($dom);
    }

    /**
     * Check if a file id valid for comparison or not (if it contains the element get by
     * $xpath_queries['check_file_type'])
     */
    protected function isFileValid(): bool
    {
        return $this->xpath->query(self::$xpath_queries['check_file_type'])->length > 0;
    }

    /**
     * Get all the instance configurations and put them into $this->result_instance
     */
    protected function getInstanceConfigs(): void
    {
        $instance_root = $this->xpath->query(self::$xpath_queries['configs_instance']['root']);
        if ($instance_root->length == 0) {
            return;
        }

        $instance_configs = $this->xpath->query(
            self::$xpath_queries['configs_instance']['config'],
            $instance_root->item(0)
        );

        if ($instance_configs->length == 0) {
            return;
        }

        /** @var DOMElement $_config */
        foreach ($instance_configs as $_config) {
            $feature = self::unsanitizeString($_config->getAttribute('feature'));
            $value   = self::unsanitizeString($_config->getAttribute('value'));
            $parts   = explode(' ', $feature);

            if ((count($parts) == 1) || !CModule::exists($parts[0])) {
                if (!array_key_exists('none', $this->result)) {
                    $this->result['none'] = [
                        'instance' => [],
                        'groups'   => [],
                        'trad'     => $this->translator->tr('None'),
                    ];
                }

                if (!array_key_exists($feature, $this->result['none']['instance'])) {
                    $this->result['none']['instance'][$feature] = [];
                }

                if (!array_key_exists($this->file, $this->result['none']['instance'][$feature])) {
                    $this->result['none']['instance'][$feature][$this->file] = $value;
                }
            } else {
                if (!array_key_exists($parts[0], $this->result)) {
                    $this->result[$parts[0]] = [
                        'instance' => [],
                        'groups'   => [],
                        'trad'     => $this->translator->tr("module-{$parts[0]}-court"),
                    ];
                }

                if (!array_key_exists($feature, $this->result[$parts[0]]['instance'])) {
                    $this->result[$parts[0]]['instance'][$feature] = [];
                }

                if (!array_key_exists($this->file, $this->result[$parts[0]]['instance'][$feature])) {
                    $this->result[$parts[0]]['instance'][$feature][$this->file] = $value;
                }
            }
        }
    }

    /**
     * Get all the groups configs and put them into $this->result_groups
     */
    protected function getGroupsConfigs(): void
    {
        $groups_root = $this->xpath->query(self::$xpath_queries['configs_groups']['root']);
        if ($groups_root->length == 0) {
            return;
        }

        $modules = $this->xpath->query(self::$xpath_queries['configs_groups']['modules'], $groups_root->item(0));
        if ($modules->length == 0) {
            return;
        }

        /** @var DOMElement $_module */
        foreach ($modules as $_module) {
            $mod_name = self::unsanitizeString($_module->getAttribute('mod_name'));

            if (!array_key_exists($mod_name, $this->result)) {
                $this->result[$mod_name] = [
                    'instance' => [],
                    'groups'   => [],
                    'trad'     => $this->translator->tr("module-{$mod_name}-court"),
                ];
            }

            /** @var DOMElement $_conf */
            foreach ($this->xpath->query(self::$xpath_queries['configs_groups']['configs'], $_module) as $_conf) {
                $feature = self::unsanitizeString($_conf->getAttribute('feature'));

                if (!array_key_exists($feature, $this->result[$mod_name]['groups'])) {
                    $this->result[$mod_name]['groups'][$feature] = [];
                }

                $this->result[$mod_name]['groups'][$feature][$this->file] = self::unsanitizeString(
                    $_conf->getAttribute('value')
                );
            }
        }
    }

    public function getResult(): array
    {
        return $this->result;
    }

    /**
     * Get the names of the parsed files
     */
    public function getFilesNames(): array
    {
        return $this->files_names;
    }

    /**
     * Convert a string to utf8
     *
     * @param string|array $str String to convert
     *
     * @return string
     */
    public static function sanitizeString($str)
    {
        return CMbString::utf8Encode($str);
    }

    /**
     * Convert the value from utf8 to iso-8859-1
     *
     * @param string|array $str String to convert
     *
     * @return string
     */
    public static function unsanitizeString($str)
    {
        return trim(CMbString::utf8Decode($str));
    }
}
