<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use LogicException;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\Content\JsonApiItem;
use Ox\Core\Api\Request\Content\RequestContentException;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbMetaObjectPolyfill;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CModelObject;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\IPatientRelated;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Classe CNote.
 *
 * @abstract Permet de cr�er des notes sur n'importe quel objet
 */
class CNote extends CMbObject implements IPatientRelated
{
    public const RESOURCE_TYPE = 'note';

    public const RELATION_AUTHOR           = 'author';
    public const RELATION_CONTEXT          = 'context';
    public const RELATION_AUTHORIZED_USERS = 'authorizedUsers';

    // DB Table key
    public $note_id;

    // DB Fields
    public $user_id;
    public $public;
    public $degre;
    public $date;
    /** @deprecated Use only the text field */
    public $libelle;
    public $text;

    public                $object_class;
    public                $object_id;
    public ?CStoredObject $_ref_object = null;

    public ?CMediusers $_ref_user = null;

    /** @var CMediusers[]|null */
    private ?array $_authorized_users = null;

    public ?CPatient $_rel_patient;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec            = parent::getSpec();
        $spec->table     = 'note';
        $spec->key       = 'note_id';
        $spec->anti_csrf = true;

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props                 = parent::getProps();
        $props["user_id"]      = "ref class|CMediusers back|owned_notes";
        $props["public"]       = "bool notNull default|1 fieldset|" . CModelObject::FIELDSET_DEFAULT;
        $props["degre"]        = "enum notNull list|low|medium|high default|low fieldset|"
            . CModelObject::FIELDSET_DEFAULT;
        $props["date"]         = "dateTime notNull fieldset|" . CModelObject::FIELDSET_DEFAULT;
        $props["libelle"]      = "str";
        $props["text"]         = "text notNull fieldset|" . CModelObject::FIELDSET_DEFAULT;
        $props["object_id"]    = "ref notNull class|CMbObject meta|object_class cascade back|notes";
        $props["object_class"] = "str notNull class show|0";

        return $props;
    }

    public function store()
    {
        if ($msg = parent::store()) {
            return $msg;
        }

        if (null !== $this->_authorized_users) {
            try {
                // Purge the authorizations for users not present in the list $this->_authorized_users.
                $existing_user_ids = $this->purgeAuthorizations(
                    CMbArray::pluck($this->_authorized_users, 'user_id')
                );

                // Add authorization for users that does not already have the permission.
                foreach ($this->_authorized_users as $user) {
                    if (!in_array($user->_id, $existing_user_ids)) {
                        $this->addAuthorization($user);
                    }
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }

            $this->_authorized_users = null;
        }


        return null;
    }

    public function getApiLink(RouterInterface $router): ?string
    {
        return $router->generate('system_note_show', ['note_id' => $this->_id]);
    }

    public function loadRefUser(): ?CMediusers
    {
        /** @var ?CMediusers $user */
        $user = $this->loadFwdRef("user_id", true);
        if ($user && $user->_id) {
            $this->_view = "Note �crite par " . $user->_view;
        }

        return $this->_ref_user = $user;
    }

    public function getResourceAuthor(): ?Item
    {
        $author = $this->loadFwdRef('user_id', true);

        return $author && $author->_id ? new Item($author) : null;
    }

    /**
     * @throws RequestContentException|CMbModelNotFoundException
     */
    public function setResourceAuthor(?JsonApiItem $json_api_author): void
    {
        if ($json_api_author === null) {
            $this->user_id = '';

            return;
        }

        /** @var CMediusers $author */
        $author = $json_api_author->createModelObject(CMediusers::class, false)->getModelObject();
        $author->needsEdit();
        $this->user_id = $json_api_author->createModelObject(CMediusers::class, false)->getModelObject()->_id;
    }

    public function getResourceContext(): ?Item
    {
        $context = $this->loadFwdRef('object_id', true);

        return $context && $context->_id ? new Item($context) : null;
    }

    /**
     * @throws RequestContentException|CMbModelNotFoundException|InvalidArgumentException
     */
    public function setResourceContext(?JsonApiItem $json_api_context): void
    {
        $this->setResourceMetaObject($json_api_context);
    }

    /**
     * @throws ApiException
     * @throws Exception
     */
    public function getResourceAuthorizedUsers(): Collection|array
    {
        /** @var CPermObject[] $perms */
        $perms = $this->loadBackRefs('permissions');
        if (!$perms) {
            return [];
        }

        $users = [];
        foreach ($perms as $perm) {
            /** @var CUser $user */
            $user              = $perm->loadFwdRef('user_id', true);
            if ($mediuser = $user->loadRefMediuser()) {
                $mediuser->loadRefFunction();
                $users[$mediuser->_id] = $mediuser;
            }
        }

        return new Collection($users);
    }

    /**
     * @param JsonApiItem[] $json_api_items
     *
     * @return void
     */
    public function setResourceAuthorizedUsers(array $json_api_items): void
    {
        $this->_authorized_users = [];
        if ([] === $json_api_items) {
            return;
        }

        foreach ($json_api_items as $item) {
            /** @var CMediusers $user */
            $user = $item->createModelObject(CMediusers::class, false)->getModelObject();
            $user->needsEdit();

            $this->_authorized_users[$user->_id] = $user;
        }
    }

    /**
     * @inheritdoc
     */
    public function getPerm($perm): bool
    {
        $curr_user = CMediusers::get();

        // Owner has always permission
        if ($this->user_id === $curr_user->_id) {
            return true;
        }

        // Only note's author can edit it.
        if ($this->_id && $perm === CPermObject::EDIT) {
            return false;
        }

        // Existing private note : user can only view it if he has explicit object perm on the note.
        if ($this->_id && !$this->public) {
            $object_perms = $this->loadBackRefs('permissions');
            /** @var CPermObject $object_perm */
            foreach ($object_perms as $object_perm) {
                if ($object_perm->user_id === $curr_user->_id) {
                    return $object_perm->permission >= $perm;
                }
            }

            return false;
        }

        $target = $this->loadFwdRef('object_id', true);

        // Read permission an object for note edition
        return $target ? $target->getPerm(PERM_READ) : true;
    }

    /**
     * @param CStoredObject $object
     *
     * @return void
     * @todo redefine meta raf
     * @deprecated
     */
    public function setObject(CStoredObject $object): void
    {
        CMbMetaObjectPolyfill::setObject($this, $object);
    }

    /**
     * @throws Exception
     * @deprecated
     */
    public function loadTargetObject(bool $cache = true): ?CStoredObject
    {
        return CMbMetaObjectPolyfill::loadTargetObject($this, $cache);
    }

    /**
     * @inheritDoc
     * @todo remove
     */
    public function loadRefsFwd(): void
    {
        parent::loadRefsFwd();
        $this->loadTargetObject();
    }

    /**
     * CNote cannot have files linked
     * @return array
     */
    public function getResourceFiles()
    {
        return [];
    }

    /**
     * @throws CMbException
     * @throws Exception
     */
    private function purgeAuthorizations(array $keep_user_ids): array
    {
        if (!$this->_id) {
            throw new CMbException('CNote-error-Cannot purge authorizations for non existing note');
        }

        $existing_user_ids = [];
        /** @var CPermObject[] $authorizations */
        $authorizations = $this->loadBackRefs('permissions');
        foreach ($authorizations as $authorization) {
            if (in_array($authorization->user_id, $keep_user_ids)) {
                $existing_user_ids[] = $authorization->user_id;
            } else {
                $authorization->delete();
            }
        }

        return $existing_user_ids;
    }

    /**
     * @throws CMbException
     * @throws Exception
     */
    private function addAuthorization(CMediusers $user): void
    {
        if (!$this->_id) {
            throw new CMbException('CNote-error-Cannot create authorization for non existing note');
        }

        if (!$user->_id) {
            throw new CMbException('CNote-error-Cannot create authorization for non existing user');
        }

        $perm               = new CPermObject();
        $perm->user_id      = $user->_id;
        $perm->object_class = $this->_class;
        $perm->object_id    = $this->_id;
        $perm->permission   = CPermObject::READ;
        if ($msg = $perm->store()) {
            throw new Exception($msg);
        }
    }

    public function loadRelPatient(): ?CPatient
    {
        $context = $this->loadTargetObject();

        if ($context instanceof CPatient) {
            return $this->_rel_patient = $context;
        }

        if ($context instanceof IPatientRelated) {
            return $this->_rel_patient = $context->loadRelPatient();
        }

        return null;
    }
}
