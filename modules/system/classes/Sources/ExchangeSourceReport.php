<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Sources;

use Ox\Core\CAppUI;
use Ox\Core\CMbString;

/**
 * Represent the aggregate of Exchange Source calls during a hit.
 * Used to make performance, logging reports.
 */
class ExchangeSourceReport
{
    private const INTEGERS_IN_URI = [
        '/\/(?<number>\d+)\//i' => '/{d}/',
        '/\/(?<number>\d+)$/i'  => '/{d}',
    ];

    private array $traces = [];

    public static function fromState(array $state): self
    {
        $self         = new static();
        $self->traces = $state;

        return $self;
    }

    public function trace(string $type, string $log, float $duration): void
    {
        $signature = $this->normalize($log);

        if (!isset($this->traces[$signature])) {
            $this->traces[$signature] = [
                'type'         => $type,
                'count'        => 0,
                'duration'     => 0.0,
                // Initializing a distribution array, from 1E-6 (1us+) up to 1E2 (100s+)
                'distribution' => array_fill_keys(range(-6, 2, 1), 0),
            ];

            krsort($this->traces[$signature]['distribution']);
        }

        $this->traces[$signature]['count']++;
        $this->traces[$signature]['duration'] += $duration;

        // Distribution is sorted by the slowest first so as soon as the threshold has been reached, we leave the loop.
        foreach ($this->traces[$signature]['distribution'] as $threshold => $count) {
            if ($duration >= (10 ** $threshold)) {
                $this->traces[$signature]['distribution'][$threshold]++;
                break;
            }
        }

        // Sorting by the slowest groups of calls
        uasort(
            $this->traces,
            fn(array $a, array $b): int => $b['duration'] <=> $a['duration']
        );
    }

    public function toArray(): array
    {
        return $this->traces;
    }

    /**
     * Get the count of traces
     *
     * @param array|null $traces Sample to work on
     *
     * @return int
     */
    public function getCount(array $traces = null): int
    {
        return array_sum(array_column(($traces) ?: $this->traces, 'count'));
    }

    /**
     * Sums the duration of each trace
     *
     * @param array|null $traces Sample to work on
     *
     * @return float
     */
    public function getTotalDuration(array $traces = null): float
    {
        return array_sum(array_column(($traces) ?: $this->traces, 'duration'));
    }

    /**
     * Builds an HTML report (and print it).
     *
     * @param int|null $limit Limit the number of traces
     *
     * @return void
     */
    public function report(int $limit = null): void
    {
        $total_count    = $this->getCount();
        $total_duration = $this->getTotalDuration();

        $traces = $this->traces;

        if ($limit > 0) {
            $traces = array_slice($this->traces, 0, $limit);

            $limit_count    = $this->getCount($traces);
            $limit_duration = $this->getTotalDuration($traces);

            if ($total_count === 0) {
                CAppUI::stepMessage(CAppUI::UI_MSG_OK, "Report is empty.");
            } else {
                CAppUI::stepMessage(
                    CAppUI::UI_MSG_WARNING,
                    "Report is truncated to the %d most time consuming query signatures (%d%% of queries count, %d%% of queries duration).",
                    count($traces),
                    ($limit_count * 100) / $total_count,
                    ($limit_duration * 100) / $total_duration
                );
            }
        }

        $index = 0;
        foreach ($traces as $sample => &$signature) {
            $index++;

            // Reverse sorting in order to print fastest distribution first
            ksort($signature['distribution']);

            CAppUI::stepMessage(
                CAppUI::UI_MSG_OK,
                "Query %d: was called %d times [%d%%] for %01.3fms [%d%%]",
                $index,
                $signature["count"],
                ($signature["count"] * 100) / $total_count,
                $signature["duration"] * 1000,
                ($signature["duration"] * 100) / $total_duration
            );

            echo CMbString::utf8Decode(CMbString::highlightCode("http", $sample, false, "white-space: pre-wrap;"));

            // Computing and displaying distribution bars
            $lines = [];
            foreach ($signature['distribution'] as $threshold => $count) {
                if ($count <= 0) {
                    continue;
                }

                $max = 100;

                if ($threshold >= 0) {
                    // 1s+
                    $line = str_pad((10 ** $threshold), 3, ' ', STR_PAD_LEFT) . 's  ';
                } elseif ($threshold >= -3) {
                    // 1ms+
                    $line = str_pad((10 ** ($threshold + 3)), 3, ' ', STR_PAD_LEFT) . 'ms ';
                } else {
                    // 1us+
                    $line = str_pad((10 ** ($threshold + 6)), 3, ' ', STR_PAD_LEFT) . '&micro;s ';
                }

                while ($count > $max) {
                    $line  .= str_pad('', $max, '#') . "\n      ";
                    $count -= $max;
                }
                $line .= str_pad('', $count, '#');

                $lines[] = $line;
            }

            echo "<pre>" . implode("\n", $lines) . "</pre>";
        }
    }

    public function normalize(string $call): string
    {
        $params_delimiter = strpos($call, '?');

        if ($params_delimiter === false) {
            return preg_replace(array_keys(self::INTEGERS_IN_URI), self::INTEGERS_IN_URI, $call);
        }

        return preg_replace(
            array_keys(self::INTEGERS_IN_URI),
            self::INTEGERS_IN_URI,
            substr($call, 0, $params_delimiter)
        );
    }
}
