<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Sources;

use Ox\Mediboard\System\CExchangeSource;

trait ClientResilienceTrait
{
    /**
     * @param CExchangeSource $source
     *
     * @return void
     */
    public function init(CExchangeSource $source): void
    {
        $this->client->init($source);
    }

    /**
     * @inheritdoc
     */
    public function isReachableSource(): bool
    {
        $call = fn() => $this->client->isReachableSource();

        return(bool)$this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     */
    public function isAuthentificate(): bool
    {
        $call = fn() => $this->client->isAuthentificate();

        return(bool)$this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     */
    public function getResponseTime(): int
    {
        $call = fn() => $this->client->getResponseTime();

        return(int)$this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }
}
