<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Sources;

use Ox\Core\CMbPath;
use Stringable;

class ObjectPath implements Stringable
{
    private bool    $override_extension = false;
    private bool    $remove_extension   = false;
    private ?string $host               = null;
    private ?string $prefix             = null;
    private ?string $suffix             = null;
    private ?string $extension          = null;
    private ?string $file_name          = null;

    /**
     * Construct the path under an object
     *
     * @param string|null $host
     * @param string|null $prefix
     * @param string|null $file_name
     * @param string|null $extension
     */
    public function __construct(
        ?string $host = null,
        ?string $prefix = null,
        ?string $file_name = null,
        ?string $extension = null,
        ?string $suffix = null
    ) {
        $this->host      = $host;
        $this->prefix    = $prefix;
        $this->file_name = $file_name;
        $this->extension = $extension;
        $this->suffix    = $suffix;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        $host   = $this->formatHost();
        $prefix = $this->formatPrefix();
        $is_directory_prefix = str_ends_with($prefix, '/');

        if (!$this->file_name) {
            return $is_directory_prefix ? rtrim("$host/$prefix", '/') : "$host";
        }

        return "$host/" . $this->formatPrefix() . $this->formatFilePath();
    }

    /**
     * @param string|null $host
     *
     * @return ObjectPath
     */
    public function withHost(string $host): ObjectPath
    {
        $clone       = clone($this);
        $clone->host = $host;

        return $clone;
    }

    /**
     * @return ObjectPath
     */
    public function withoutHost(): ObjectPath
    {
        $clone       = clone($this);
        $clone->host = null;

        return $clone;
    }

    /**
     * @param string|null $prefix
     *
     * @return ObjectPath
     */
    public function withPrefix(string $prefix): ObjectPath
    {
        $clone         = clone($this);
        $clone->prefix = $prefix;

        return $clone;
    }

    /**
     * @return ObjectPath
     */
    public function withoutPrefix(): ObjectPath
    {
        $clone         = clone($this);
        $clone->prefix = null;

        return $clone;
    }

    /**
     * @return ObjectPath
     */
    public function withoutSuffix(): ObjectPath
    {
        $clone         = clone($this);
        $clone->suffix = null;

        return $clone;
    }

    /**
     * @return ObjectPath
     */
    public function withSuffix(string $suffix): ObjectPath
    {
        $clone         = clone($this);
        $clone->suffix = $suffix;

        return $clone;
    }

    /**
     * @param string|null $extension
     * @param bool        $override_extension
     *
     * @return ObjectPath
     */
    public function withExtension(string $extension, bool $override_extension = false): ObjectPath
    {
        $clone                     = clone($this);
        $clone->extension          = $extension;
        $clone->override_extension = $override_extension;
        $clone->remove_extension   = false;

        return $clone;
    }

    /**
     * @param bool $override_extension
     *
     * @return ObjectPath
     */
    public function withoutExtension(bool $remove_extension = false): ObjectPath
    {
        $clone                   = clone($this);
        $clone->extension        = null;
        $clone->remove_extension = $remove_extension;

        return $clone;
    }

    /**
     * @param string|null $file_path
     *
     * @return ObjectPath
     */
    public function withFilePath(string $file_path): ObjectPath
    {
        $clone = clone($this);

        $clone->file_name = $file_path;
        $clone->remove_extension = false;

        return $clone;
    }

    private function formatHost(): string
    {
        if (!$this->host) {
            return '';
        }

        return rtrim($this->host, '/');
    }

    private function formatPrefix(): string
    {
        if (!$this->prefix) {
            return '';
        }

        return $this->prefix;
    }

    private function formatFilePath(): string
    {
        if (!$this->file_name) {
            return '';
        }

        $file_name = CMbPath::getFilename($this->file_name);
        $extension = CMbPath::getExtension($this->file_name);
        $prefix    = str_replace($extension ? "$file_name.$extension" : "$file_name", '', $this->file_name);

        if ($this->remove_extension && $extension) {
            $extension = '';
        } elseif (($this->override_extension && $extension) || ($this->extension && !$extension)) {
            $extension = $this->extension;
        }

        $extension = $extension ? ("." . ltrim($extension, '.')) : '';

        return $prefix . $file_name . $this->suffix . $extension;
    }

    private function removeExtension(string $path): string
    {
        $end_point_pos = strrpos($path, '.');
        if ($end_point_pos === false) {
            return $path;
        }

        return substr($path, 0, $end_point_pos);
    }

    /**
     * Get ObjectPath to access at the directory
     * Extension will be removed
     * All informations are serialized like Host
     * It's possible to chain concatenation with file_path
     *
     * @return ObjectPath
     */
    public function toDirectory(): ObjectPath
    {
        $clone                   = clone($this);
        $clone->extension        = null;
        $clone->remove_extension = true;

        $directory = new ObjectPath((string)$clone, null, null, $this->extension);
        $directory->remove_extension = true;

        return $directory;
    }

    /**
     * Get Object path from a path
     *
     * @param string $path
     *
     * @return ObjectPath
     */
    public static function fromPath(string $path): ObjectPath
    {
        $infos = pathinfo($path);

        return new ObjectPath($infos['dirname'], null, $infos['filename'], $infos['extension'] ?? null);
    }

    /**
     * @return string|null
     */
    public function getPrefix(): ?string
    {
        return $this->prefix;
    }

    /**
     * @return string|null
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->file_name;
    }

    /**
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }
}
