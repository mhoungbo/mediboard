<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Sources;

use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbPath;
use Ox\Core\CMbString;
use Ox\Core\Contracts\Client\FileClientInterface;
use Ox\Interop\Ftp\CSourceFTP;
use Ox\Interop\Ftp\CSourceSFTP;
use Ox\Mediboard\System\CExchangeSourceAdvanced;
use Ox\Mediboard\System\CSourceFileSystem;

abstract class CSourceFile extends CExchangeSourceAdvanced
{
    public const TYPE_FILE = '';

    /** @var string[] */
    public const TYPE = [CSourceFTP::TYPE, CSourceSFTP::TYPE, CSourceFileSystem::TYPE];

    /**
     * @param $current_directory
     *
     * @return array
     */
    public function getRootDirectory($current_directory): array
    {
        $tabRoot = explode("/", $current_directory);
        $tabRoot[0] = "/";
        $root       = [];
        $i          = 0;
        foreach ($tabRoot as $_tabRoot) {
            if ($i === 0) {
                $path = "/";
            } else {
                $path = $root[count($root) - 1]["path"] . "$_tabRoot/";
            }
            $root[] = [
                "name" => $_tabRoot,
                "path" => $path,
            ];
            $i++;
        }

        return $root;
    }

    public static function truncate($string)
    {
        if (!is_string($string)) {
            return $string;
        }

        // Truncate
        $max    = 1024;
        $result = CMbString::truncate($string, $max);

        // Indicate true size
        $length = strlen($string);
        if ($length > 1024) {
            $result .= " [$length bytes]";
        }

        return $result;
    }

    /**
     * Generate file name
     *
     * @return string Filename
     */
    public function generateFileName(): string
    {
        $file_name = str_replace([" ", ":", "-"], ["_", "", ""], CMbDT::dateTime());

        if ($this->_exchange_data_format && $this->_exchange_data_format->_id) {
            $file_name = ObjectPath::fromPath($file_name)->withSuffix(
                "-" . $this->_exchange_data_format->_id
            );
        }

        return $file_name;
    }

    public function timestampFileName(string $filename): string
    {
        $extensions_filename = "";
        while (($extension = CMbPath::getExtension($filename))) {
            $extensions_filename = ".$extension{$extensions_filename}";
            $filename            = CMbPath::getFilename($filename);
        }

        return $filename . ($filename ? '_' : '') . $this->generateFileName() . $extensions_filename;
    }

    /**
     * @inheritdoc
     */
    public function getClient(): FileClientInterface
    {
        /** @var FileClientInterface $client_file */
        $client_file = parent::getClient();

        return $client_file;
    }

    /**
     * @return FileClientInterface
     * @throws CMbException
     */
    public function disableResilience(): FileClientInterface
    {
        /** @var FileClientInterface $client_file */
        $client_file = parent::disableResilience();

        return $client_file;
    }

    /**
     * @return FileClientInterface
     * @throws CMbException
     */
    public function reactivateResilience(): FileClientInterface
    {
        /** @var FileClientInterface $client_file */
        $client_file = parent::reactivateResilience();

        return $client_file;
    }

    /**
     * Get the file source object from type
     *
     * @param string $type
     *
     * @return CSourceFile|null
     */
    public static function getSourceFromType(string $type): ?CSourceFile
    {
        if (($type === CSourceFileSystem::TYPE) || ($type === CSourceFileSystem::TYPE_FILE)) {
            return new CSourceFileSystem();
        }

        if (($type === CSourceFTP::TYPE) || ($type === CSourceFTP::TYPE_FILE)) {
            return new CSourceFTP();
        }

        if (($type === CSourceSFTP::TYPE) || ($type === CSourceSFTP::TYPE_FILE)) {
            return new CSourceSFTP();
        }

        return null;
    }

    /**
     * Complete the path with informations set on source
     * The parameter $file_path could be only the filename with extension or not
     * It could be also the filename prefixed by a sub directory like this "sub/in/test.xml"
     *
     * @param string|null $file_path
     *
     * @return ObjectPath
     */
    abstract public function completePath(?string $file_path = null): ObjectPath;
}
