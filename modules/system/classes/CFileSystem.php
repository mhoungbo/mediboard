<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Error;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbPath;
use Ox\Core\CMbString;
use Ox\Core\Contracts\Client\FileSystemClientInterface;
use Ox\Interop\Eai\Resilience\ClientContext;
use Ox\Mediboard\System\Sources\ObjectPath;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Throwable;

class CFileSystem implements FileSystemClientInterface
{
    /** @var CSourceFileSystem */
    private $source;

    /** @var int */
    private $source_id;

    /** @var string */
    private $source_class;

    /** @var string */
    private $input;

    /** @var string */
    private $destinataire;

    /** @var mixed|null */
    private $emetteur;

    /** @var float|int|string */
    private $date_echange;

    /** @var int|null */
    public $_limit;

    /** @var EventDispatcher */
    protected $dispatcher;

    /**
     * @param string|null    $function_name
     * @param Throwable|null $throwable
     *
     * @return ClientContext
     */
    private function getContext(?string $function_name = null, ?Throwable $throwable = null): ClientContext
    {
        $arguments = [];
        if ($function_name) {
            $arguments['function_name'] = $function_name;
        }

        return (new ClientContext($this, $this->source))
            ->setArguments($arguments)
            ->setThrowable($throwable);
    }

    /**
     * @param callable $call
     * @param string   $function_name
     *
     * @return mixed
     */
    protected function dispatch(array $call_args, string $function_name)
    {
        $context = $this->getContext($function_name);
        if (is_array($call_args)) {
            $arguments = $call_args[1] ?? [];
            $callable  = $call_args[0];
            $context->setRequest($arguments);
        } else {
            $callable  = $call_args;
            $arguments = [];
        }

        $this->dispatcher->dispatch($context, self::EVENT_BEFORE_REQUEST);
        $result = call_user_func($callable, $arguments);
        $context->setResponse($result);
        $this->dispatcher->dispatch($context, self::EVENT_AFTER_REQUEST);

        return $result;
    }

    /**
     * @param CSourceFileSystem $source
     *
     * @return void
     * @throws Exception
     */
    public function init(CExchangeSource $source): void
    {
        $this->date_echange = CMbDT::dateTime();
        $this->emetteur     = CAppUI::conf("mb_id");
        $this->destinataire = $source->host;
        $this->input        = serialize($source->_data);
        $this->source_class = $source->_class;
        $this->source_id    = $source->_id;
        $this->source       = $source;
        $this->_limit       = $source->_limit;
        $this->dispatcher   = $source->_dispatcher;
    }

    /**
     * @return bool
     * @throws Exception|Error
     */
    public function isReachableSource(): bool
    {
        try {
            $call = [
                fn($args) => is_dir(...$args) && is_writable(...$args),
                [$this->source->host],
            ];

            if (!$this->dispatch($call, "test_is_dir")) {
                $this->source->_reachable = 0;
                $this->source->_message = CAppUI::tr("CSourceFileSystem-path-not-found", $this->source->host);

                return false;
            }

            return true;
        } catch (Exception|Error $e) {
            $this->dispatcher->dispatch($this->getContext('isReachableSource', $e), self::EVENT_EXCEPTION);

            throw $e;
        }
    }

    /**
     * @return bool
     * @throws Exception|Error
     */
    public function isAuthentificate(): bool
    {
        try {
            $call = [
                fn($args) => is_writable(...$args),
                [$this->source->host],
            ];

            if (!$this->dispatch($call, "test_writable")) {
                $this->source->_reachable = 1;
                $this->source->_message = CAppUI::tr("CSourceFileSystem-path-not-writable", $this->source->host);

                return false;
            }

            return true;
        } catch (Exception|Error $e) {
            $this->dispatcher->dispatch($this->getContext('isAuthentificate', $e), self::EVENT_EXCEPTION);

            throw $e;
        }
    }

    /**
     * @return int
     * @throws Exception|Error
     */
    public function getResponseTime(): int
    {
        $start = microtime(true);
        $this->isReachableSource();
        $end           = microtime(true);
        $response_time = $end - $start;

        return $this->source->response_time = intval($response_time);
    }

    /**
     * @param string|null $destination_basename
     *
     * @return bool
     * @throws CMbException
     */
    public function send(string $destination_basename): bool
    {
        $file_path = $destination_basename;

        try {
            $dir  = pathinfo($file_path)['dirname'];
            $call = [
                fn($args) => is_writable(...$args),
                [$dir],
            ];

            if (!$this->dispatch($call, "test_writable")) {
                $e = new CMbException("CSourceFileSystem-path-not-writable", $dir);
                $this->dispatcher->dispatch($this->getContext('isReachableSource', $e), self::EVENT_EXCEPTION);
                throw $e;
            }

            $call = [
                fn($args) => file_put_contents(...$args),
                [$file_path, $this->source->_data],
            ];

            $return = $this->dispatch($call, "send");

            if ($this->source->fileextension_write_end) {
                $path_ok = ObjectPath::fromPath($file_path)->withExtension('ok', true);

                $call = [
                    fn($args) => file_put_contents(...$args),
                    [$path_ok, ""],
                ];

                $return_ok = $this->dispatch($call, "send file ok");
            }
        } catch (CMbException $e) {
            $this->source->_message = $e->getMessage();
            $this->dispatcher->dispatch($this->getContext('send', $e), self::EVENT_EXCEPTION);

            throw $e;
        }

        if (($return === false) || (isset($return_ok) && ($return_ok === false))) {
            return false;
        }

        return $return;
    }

    /**
     * @return array
     * @throws CMbException
     * @throws Exception|Error
     */
    public function receive($path_directory): array
    {
        try {
            if (!is_dir($path_directory)) {
                throw new CMbException("CSourceFileSystem-path-not-found", $path_directory);
            }

            if (!is_readable($path_directory)) {
                throw new CMbException("CSourceFileSystem-path-not-readable", $path_directory);
            }

            if (!($handle = opendir($path_directory))) {
                throw new CMbException("CSourceFileSystem-path-not-readable", $path_directory);
            }

            $i              = 1;
            $files          = [];
            $path_directory = rtrim($path_directory, '/');
            while (false !== ($entry = readdir($handle))) {
                $limit = 5000;
                $entry = "$path_directory/$entry";
                if ($i == $limit) {
                    break;
                }

                /* We ignore folders */
                if (is_dir($entry)) {
                    continue;
                }

                $files[] = $entry;

                $i++;
            }

            closedir($handle);

            switch ($this->source->sort_files_by) {
                default:
                case "name":
                    sort($files);
                    break;
                case "date":
                    usort($files, [$this, "sortByDate"]);
                    break;
                case "size":
                    usort($files, [$this, "sortBySize"]);
                    break;
            }

            if ($this->_limit) {
                $files = array_slice($files, 0, $this->_limit);
            }
        } catch (Exception|Error $e) {
            $this->dispatcher->dispatch($this->getContext('receive', $e), self::EVENT_EXCEPTION);

            throw $e;
        }

        return $files;
    }

    /**
     * @param string $file_name
     *
     * @return int
     */
    public function getSize(string $file_name): int
    {
        $call = [
            fn($args) => filesize(...$args),
            [$file_name],
        ];

        return $this->dispatch($call, 'getSize');
    }

    /**
     * @inheritdoc
     * @throws CMbException
     */
    public function renameFile(string $file_path, string $new_name): bool
    {
        $call = [
            fn($args) => rename(...$args),
            [$file_path, $new_name],
        ];

        if (!$this->dispatch($call, 'renameFile')) {
            $exception = new CMbException("CSourceFileSystem-error-renaming", $file_path);
            $this->dispatcher->dispatch($this->getContext('renameFile', $exception), self::EVENT_EXCEPTION);
            throw $exception;
        }

        return true;
    }

    /**
     * @param string $directory_name
     *
     * @return bool
     * @throws CMbException
     */
    public function createDirectory(string $directory_name): bool
    {
        $path =  $directory_name;
        $call = [
            fn($args) => mkdir(...$args),
            [$path, 0777, true],
        ];

        if (is_dir($path)) {
            throw new CMbException("CSourceFileSystem-msg-directory already exist", $path);
        }

        if ($this->dispatch($call, 'createDirectory') === false) {
            $exception = new CMbException("CSourceFileSystem-error-createDirectory", $directory_name);
            $this->dispatcher->dispatch($this->getContext('createDirectory', $exception), self::EVENT_EXCEPTION);

            throw $exception;
        }

        return true;
    }

    /**
     * @param string|null $directory
     *
     * @return string
     * @throws CMbException
     */
    public function getDirectory(string $directory): string
    {
        if (!is_dir($directory)) {
            $exception = new CMbException("CSourceFile-msg-directory not exist", $directory);
            $this->dispatcher->dispatch(
                $this->getContext('getCurrentDirectory', $exception),
                self::EVENT_EXCEPTION
            );

            throw $exception;
        }

        return realpath($directory);
    }

    /**
     * @param string $current_directory
     * @param bool   $information
     *
     * @return array
     * @throws CMbException
     */
    public function getListFiles(string $directory, bool $information = false): array
    {
        $call = [
            fn($args) => is_dir(...$args),
            [$directory],
        ];

        if (!$this->dispatch($call, 'test directory exist [getListFiles]')) {
            $e = new CMbException("CSourceFileSystem-msg-Folder does not exist", UI_MSG_ERROR);
            $this->dispatcher->dispatch($this->getContext('isReachableSource', $e), self::EVENT_EXCEPTION);
            throw $e;
        }

        $call = [
            fn($args) => scandir(...$args),
            [$directory],
        ];

        $files = [];

        if (($list_files = $this->dispatch($call, 'getListFiles')) === false) {
            $list_files = [];
        }

        $directory = rtrim($directory, '/') . '/';
        foreach ($list_files as $_file) {
            if (!is_file("{$directory}{$_file}")) {
                continue;
            }

            $files[] = $_file;
        }

        return $files;
    }

    /**
     * @param string $current_directory
     *
     * @return array
     * @throws CMbException
     */
    public function getListFilesDetails(string $current_directory): array
    {
        $call = [
            fn($args) => file_exists(...$args),
            [$current_directory],
        ];

        if (!$this->dispatch($call, 'test directory exist [getListFilesDetails]')) {
            $e = new CMbException("CSourceFileSystem-msg-Folder does not exist", UI_MSG_ERROR);
            $this->dispatcher->dispatch($this->getContext('isReachableSource', $e), self::EVENT_EXCEPTION);
            throw $e;
        }

        $call = [
            fn($args) => scandir(...$args),
            [$current_directory],
        ];

        $fileInfo = [];
        $contain  = ($this->dispatch($call, 'getListFilesDetails')) ?: [];
        foreach ($contain as $_contain) {
            $full_path = rtrim($current_directory, '/') . '/' . $_contain;
            if (!is_dir($full_path) && @filetype($full_path) && !is_link($full_path)) {
                $fileInfo[] = [
                    "type"         => "f",
                    "path"         => $full_path,
                    "user"         => fileowner($full_path),
                    "size"         => CMbString::toDecaBinary(filesize($full_path)),
                    "date"         => CMbDT::strftime(CMbDT::ISO_DATETIME, filemtime($full_path)),
                    "name"         => $_contain,
                    "relativeDate" => CMbDT::daysRelative(fileatime($full_path), CMbDT::date()),
                ];
            }
        }

        return $fileInfo;
    }

    /**
     * @param string $current_directory
     *
     * @return array
     */
    public function getListDirectory(string $current_directory): array
    {
        $call = [
            fn($args) => scandir(...$args),
            [$current_directory],
        ];

        $dir     = [];
        $contain = $this->dispatch($call, 'getListDirectory') ?: [];
        foreach ($contain as $_contain) {
            $full_path = rtrim($current_directory, '/') . '/' . $_contain;
            if (is_dir($full_path) && ("$_contain/" !== "./") && ("$_contain/" !== "../")) {
                $dir[] = "$_contain/";
            }
        }

        return $dir;
    }

    /**
     * @param string $source_file
     * @param string $file_name
     *
     * @return bool
     * @throws CMbException
     */
    public function addFile(string $source_file, string $file_name): bool
    {
        $path = $file_name;
        $call = [
            fn($args) => copy(...$args),
            [$source_file, $path],
        ];

        if (!file_exists($source_file)) {
            $exception = new CMbException("CSourceFileSystem-error-add", $source_file);
            $this->dispatcher->dispatch($this->getContext('addFile', $exception), self::EVENT_EXCEPTION);

            throw $exception;
        }

        $result = $this->dispatch($call, 'addFile');
        if (false === $result) {
            $exception = new CMbException("CSourceFileSystem-error-add", $file_name);
            $this->dispatcher->dispatch($this->getContext('addFile', $exception), self::EVENT_EXCEPTION);

            throw $exception;
        }

        return $result;
    }

    /**
     * @param string $path
     *
     * @return bool
     * @throws CMbException
     */
    public function delFile(string $path): bool
    {
        if (!file_exists($path)) {
            $exception = new CMbException("CSourceFileSystem-file-not-deleted", $path);
            $this->dispatcher->dispatch($this->getContext('delFile', $exception), self::EVENT_EXCEPTION);
            throw $exception;
        }

        $call = [
            fn($args) => CMbPath::remove(...$args),
            [$path],
        ];

        $result = $this->dispatch($call, 'delFile');
        if (false === $result) {
            $exception = new CMbException("CSourceFileSystem-file-not-deleted", $path);
            $this->dispatcher->dispatch($this->getContext('delFile', $exception), self::EVENT_EXCEPTION);
            throw $exception;
        }

        return $result;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->source->_message;
    }

    /**
     * @param string      $path
     * @param string|null $dest
     *
     * @return string|null
     * @throws CMbException
     */
    public function getData(string $path, ?string $dest = null): ?string
    {
        $call = [
            function ($args) use ($dest) {
                $data = file_get_contents(...$args);
                if (($data !== false) && $dest) {
                    file_put_contents($dest, $data);
                }

                return $data;
            },
            [$path],
        ];

        if (false === file_exists($path)) {
            throw new CMbException("CSourceFileSystem-download-file-failed", $path);
        }

        $data = $this->dispatch($call, 'getData');

        return $data;
    }

    /**
     * Sort by date
     *
     * @param int $a Variable a
     * @param int $b Variable b
     *
     * @return int
     */
    private function sortByDate(string $a, string $b): int
    {
        return filemtime($a) - filemtime($b);
    }

    /**
     * Sort by size
     *
     * @param int $a Variable a
     * @param int $b Variable b
     *
     * @return int
     */
    private function sortBySize(string $a, string $b): int
    {
        return filesize($a) - filesize($b);
    }

    /**
     * @inheritdoc
     */
    public function removeDirectory(string $dir_path, bool $purge = false): bool
    {
        $method = $purge ? 'purgeDirectory' : 'removeDirectory';
        try {
            if (!is_dir($dir_path)) {
                throw new CMbException("CSourceFile-msg-directory not exist", $dir_path);
            }

            if ($purge) {
                $call = [
                    fn($args) => $this->purgeDirectory(...$args),
                    [$dir_path],
                ];
            } else {
                $call = [
                    fn ($args) => @rmdir(...$args),
                    [$dir_path]
                ];
            }

            if ($this->dispatch($call, $method) === false) {
                throw new CMbException("CSourceFile-msg-Remove directory failed", $dir_path);
            }

            return true;
        } catch (Exception|Error $exception) {
            $this->dispatcher->dispatch($this->getContext($method, $exception), self::EVENT_EXCEPTION);

            throw $exception;
        }
    }

    /**
     * Purge given directory
     * delete all files and directory inside the given directory
     *
     * @param string $directory
     *
     * @return bool
     * @throws CMbException
     */
    private function purgeDirectory(string $directory): bool
    {
        $files = array_diff(scandir($directory), ['.', '..']);
        foreach ($files as $file) {
            (is_dir("$directory/$file"))
                ? $this->purgeDirectory("$directory/$file")
                : $this->delFile("$directory/$file");
        }

        return rmdir($directory);
    }
}
