<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\ViewSender;

/**
 * Utility functions for CViewSender.
 */
class ViewSenderService
{
    /**
     * Compute the hourly report for a list of CViewSender. Tell for each minute of the hour if a CViewSender will be
     * executed and the total time used.
     *
     * @param array $view_senders
     *
     * @return array
     */
    public function computeHourlyReport(array $view_senders): array
    {
        // Tableau de charges
        $hour_sum   = [];
        $hour_total = 0;
        foreach (range(0, 59) as $min) {
            $hour_sum[$min] = 0;
            foreach ($view_senders as $sender) {
                if ($sender->active) {
                    $hour_sum[$min] += $sender->_hour_plan[$min];
                    $hour_total     += ($sender->_hour_plan[$min] / 60);
                }
            }
        }

        return [
            'hour_sum' => $hour_sum,
            'total'    => $hour_total,
        ];
    }

    /**
     * Test the execution of the zip command.
     *
     * @return bool
     */
    public function isZipEnabled(): bool
    {
        exec('zip --version 2>/dev/null', $zip);

        return (bool)$zip;
    }

    /**
     * Build the relativeUri using an url. (relative uri start with /gui, /api or /token for V2).
     *
     * @param string $url
     *
     * @return string
     */
    public function getRelativeUri(string $url): string
    {
        $uri = parse_url($url, PHP_URL_PATH);

        if (false === ($start_pos = strpos($uri, '/gui/'))) {
            if (false === ($start_pos = strpos($uri, '/api/'))) {
                 $start_pos = strpos($uri, '/token/');
            }
        }

        if (false !== $start_pos) {
            $uri = substr($uri, $start_pos);
        }

        return $uri;
    }
}
