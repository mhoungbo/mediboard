<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\ViewSender;

use Exception;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Routing\RequestParams;

/**
 * Repository to request multiple CViewSender objects.
 * It can use RequestParams to initialize its values.
 */
class ViewSenderRepository
{
    private CViewSender    $sender;
    private CSQLDataSource $ds;
    private ?RequestParams $params = null;
    private array          $where  = [];
    private array          $ljoin  = [];
    private ?string        $group  = null;
    private ?int           $limit  = null;
    private ?int           $offset = null;
    private string         $order  = 'view_sender.name';

    public function __construct(RequestParams $params = null, CViewSender $sender = null)
    {
        $this->sender = $sender ?? new CViewSender();
        $this->ds     = $this->sender->getDS();
        $this->params = $params;
    }

    /**
     * Initialize $where, $group, $ljoin and $order using the RequestParam object.
     *
     * @return $this
     * @throws Exception
     */
    public function initFromRequestParams(): self
    {
        if (!$this->params) {
            return $this;
        }

        if ('' !== ($active = $this->params->get('active', 'bool')) && null !== $active) {
            $this->where['view_sender.active'] = $this->ds->prepare('= ?', $active ? '1' : '0');
        }

        if ($name = $this->params->get('name', 'str')) {
            $this->where['view_sender.name'] = $this->ds->prepareLike("%$name%");
        }

        if ($params = $this->params->get('params', 'str')) {
            $this->where['view_sender.params'] = $this->ds->prepareLike("%$params%");
        }

        if ($route_name = $this->params->get('route_name', 'str')) {
            $this->where['view_sender.route_name'] = $this->ds->prepare('= ?', $route_name);
        }

        $phase = $this->params->get('offset', 'num');
        if ($phase !== null && $phase !== '') {
            $this->where['view_sender.offset'] = $this->ds->prepare('= ?', $phase);
        }

        if ($view_status = $this->params->get(
            'view_sender_status',
            'enum list|' . implode('|', CViewSender::LAST_STATUSES)
        )) {
            $this->where['view_sender.last_status'] = $this->ds->prepare('= ?', $view_status);
        }

        if ($source_status = $this->params->get(
            'source_status',
            'enum list|' . implode('|', CSourceToViewSender::LAST_STATUSES)
        )) {
            $this->group                                      = 'view_sender.sender_id';
            $this->ljoin['source_to_view_sender']             = '`view_sender`.sender_id = `source_to_view_sender`.sender_id';
            $this->where['source_to_view_sender.last_status'] = $this->ds->prepare('= ?', $source_status);
        }

        if ($order_date = $this->params->get('order_date', 'enum list|ASC|DESC')) {
            $this->setOrder('view_sender.last_datetime', $order_date);
        }

        return $this;
    }

    /**
     * Find a list of CViewSender.
     *
     * @return CViewSender[]
     * @throws Exception
     */
    public function find(): array
    {
        $limit = null;
        if ($this->offset !== null && $this->limit !== null) {
            $limit = "$this->offset,$this->limit";
        }

        return $this->sender->loadList(
            $this->where,
            $this->order,
            $limit,
            $this->group,
            $this->ljoin
        );
    }

    /**
     * Count view senders
     *
     * @return int
     * @throws Exception
     */
    public function count(): int
    {
        return $this->sender->countList($this->where, $this->group, $this->ljoin);
    }

    /**
     * Find the sources if the CViewSender objects (from $senders).
     *
     * @param CViewSender[] $senders
     * @param string|null   $source_status
     *
     * @return array
     */
    public function findSenderSources(array $senders, ?string $source_status = null): array
    {
        $where = [];

        if ($source_status) {
            $where['source_to_view_sender.last_status'] = $this->ds->prepare('= ?', $source_status);
        }

        $sources = CStoredObject::massLoadBackRefs($senders, 'sources_link', null, $where);
        CStoredObject::massLoadFwdRef($sources, 'source_id');

        foreach ($senders as $_sender) {
            $senders_source = $_sender->loadRefSendersSource();
            $_sender->getLastAge();

            foreach ($senders_source as $_sender_source) {
                $_sender_source->loadRefSender();
            }
        }

        return is_array($sources) ? $sources : [];
    }

    public function setOrder(string $order, string $way): void
    {
        $this->order = $order . ' ' . $way;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    public function setOnlyActive(): self
    {
        $this->where['active'] = '= "1"';

        return $this;
    }
}
