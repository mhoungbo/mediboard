<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Ox\Core\CAppUI;

class Pref
{
    /**
     * Get a preference of currently connected user.
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return string|null
     */
    public function get(string $name, $default = null): ?string
    {
        return CAppUI::pref($name, $default);
    }

    /**
     * Load a preference from database.
     *
     * @param string $name
     * @param int    $user_id
     * @param mixed  $default
     *
     * @return string|null
     */
    public function load(string $name, int $user_id, $default = null): ?string
    {
        $value = CAppUI::loadPref($name, $user_id);

        if ($value === false) {
            return $default;
        }

        return $value;
    }
}
