<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use DateTime;
use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Admin\CUser;

/**
 * RefreshToken is a generic class that handles each type of refreshToken, this can be attached to a user if needed
 */
class RefreshToken extends CMbObject
{
    /** Primary key */
    public ?int $refresh_token_id = null;

    public ?string $refresh_token = null;

    public ?string $scope = null;

    public ?int $user_id = null;

    /** @var DateTime */
    public $expiration_date;

    /** @var CUser|null */
    public ?CUser $_ref_user = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'refresh_token';
        $spec->key = 'refresh_token_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props["user_id"] = "ref class|CUser back|refresh_token_user fieldset|default";
        $props["refresh_token"] = "str notNull fieldset|default";
        $props["scope"] = "enum list|mes notNull fieldset|default";
        $props["expiration_date"] = "dateTime fieldset|extra";

        return $props;
    }

    /**
     * @return CUser|CStoredObject|null
     * @throws Exception
     */
    public function loadRefUser(): ?CUser
    {
        return $this->_ref_user = $this->loadFwdRef("user_id", true);
    }
}
