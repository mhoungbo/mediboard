<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Modules;

use Exception;
use InvalidArgumentException;
use Ox\Core\CApp;
use Ox\Core\CMbString;
use Ox\Core\CSetup;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Logger\LoggerLevels;
use Ox\Core\Module\CModule;
use Ox\Core\Module\Requirements\CRequirementsException;
use Ox\Core\Module\Requirements\CRequirementsManager;
use Psr\Log\LoggerInterface;
use ReflectionException;
use Throwable;

class ModulesRepository
{
    private RequestParams   $params;
    private LoggerInterface $logger;
    private array           $modules       = ['notInstalled' => [], 'installed' => []];
    private array           $core_modules  = [];
    private bool            $is_upgradable = false;

    public function __construct(RequestParams $params, LoggerInterface $logger)
    {
        $this->params = $params;
        $this->logger = $logger;
    }

    /**
     * @throws Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function initModules(): self
    {
        // Load modules
        CModule::loadModules(true);

        foreach (CApp::getChildClasses(CSetup::class) as $module_setup) {
            if (!class_exists($module_setup)) {
                continue;
            }

            $setup = new $module_setup();

            $module = new CModule();
            $module->compareToSetup($setup);
            $module->checkModuleFiles();
            $module->getUpdateMessages($setup, true);
            $module->updateFormFields();

            if ($module->_id) { // If module exist in DB
                if ($module->mod_active) { // If module active
                    try {
                        if (!$module->_tabs) {
                            $module->registerTabs();
                        }
                        $module->getDefaultTab();
                    } catch (Throwable $e) {
                        // Do nothing, default tab is null
                    }
                }

                $this->modules['installed'][$module->mod_name] = $module;

                if ($module->_upgradable) { // If module can be upgraded
                    $this->is_upgradable = true;
                }
            } else {
                $this->modules['notInstalled'][$module->mod_name] = $module;
            }

            // Get core modules
            if (($module->mod_type == 'core') && $module->_upgradable) {
                $this->core_modules[$module->mod_name] = $module;
            }
        }

        // Adding installed modules whose files are not present
        if (count(CModule::$absent)) {
            $this->modules['installed'] += CModule::$absent;
        }

        // Sort modules
        $sorter = fn(CModule $mod_1, CModule $mod_2): int => strcasecmp(
            CMbString::removeDiacritics($mod_1->_view),
            CMbString::removeDiacritics($mod_2->_view)
        );

        uasort($this->modules['installed'], $sorter);
        uasort($this->modules['notInstalled'], $sorter);

        // Check dependency on all modules
        $this->checkDependency();

        return $this;
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function checkDependency(): void
    {
        foreach ($this->modules as $type_modules) { // Installed / not installed
            /** @var CModule $module */
            foreach ($type_modules as $module) {
                if ($module->isFilesMissing()) {
                    continue;
                }

                // Check module dependency
                foreach ($module->_dependencies as $dependencies) {
                    foreach ($dependencies as $dependency) {
                        $installed = $this->modules['installed'];

                        $dependency->verified =
                            isset($installed[$dependency->module])
                            && ($installed[$dependency->module]->mod_version >= $dependency->revision);

                        if (!$dependency->verified) {
                            $module->_dependencies_not_verified++;
                        }
                    }
                }

                if (!$module->mod_active) {
                    continue;
                }

                // Check requirements if module is active
                try {
                    $this->checkRequirements($module);
                } catch (Exception $e) {
                    $this->logger->log(LoggerLevels::LEVEL_ERROR, $e->getMessage());
                }
            }
        }
    }

    /**
     * @throws Exception|InvalidArgumentException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getModule(): CModule
    {
        if (!($mod_id = $this->params->get('mod_id', 'ref class|CModule notNull'))) {
            throw new InvalidArgumentException('No mod_id passed');
        }

        $module = new CModule();
        $module->load($mod_id);
        $module->checkModuleFiles();

        // Check requirements
        if ($module->mod_active) {
            try {
                $this->checkRequirements($module);
            } catch (Exception $e) {
                $this->logger->log(LoggerLevels::LEVEL_ERROR, $e->getMessage());
            }
        }

        return $module;
    }

    /**
     * @throws ReflectionException
     * @throws CRequirementsException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function checkRequirements(CModule $module): void
    {
        /** @var CRequirementsManager $manager */
        if ($manager = $module->getRequirements()) {
            $manager->checkRequirements();
            $module->_requirements        = count($manager);
            $module->_requirements_failed = $manager->countErrors();
        }
    }

    public function getModules(): array
    {
        return $this->modules;
    }

    public function getCoreModules(): array
    {
        return $this->core_modules;
    }

    public function isUpgradable(): bool
    {
        return $this->is_upgradable;
    }
}
