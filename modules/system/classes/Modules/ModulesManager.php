<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Modules;

use Exception;
use Ox\Core\CSetup;
use Ox\Core\Module\CModule;

class ModulesManager
{
    private CModule $module;
    private string  $cmd;
    private ?CSetup $setup = null;

    public function __construct(CModule $module, string $cmd, ?CSetup $setup = null)
    {
        $this->module = $module;
        $this->cmd    = $cmd;
        $this->setup  = $setup;
    }

    /**
     * @throws Exception
     */
    public function execute(): bool
    {
        if (($this->cmd === 'toggle') || ($this->cmd === 'toggleMenu')) {
            // just toggle the active state of the table entry
            $field                  = ($this->cmd === "toggle") ? 'mod_active' : 'mod_ui_active';
            $this->module->{$field} = 1 - $this->module->{$field};

            if (!$this->module->store()) {
                return true;
            }
        }

        if ($this->cmd === 'remove') {
            if ($this->setup === null) {
                return false;
            }

            if (!$this->setup->remove()) {
                return false;
            }

            if (!$this->module->delete()) {
                return true;
            }
        }

        if (($this->cmd === 'install') || ($this->cmd === 'upgrade')) {
            if ($this->setup === null) {
                return false;
            }

            if ($this->setup->upgrade($this->module) !== null) {
                return true;
            }
        }

        return false;
    }
}
