<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Cache;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CMbMetaObjectPolyfill;
use Ox\Core\CMbObject;
use Ox\Core\CStoredObject;
use Ox\Mediboard\System\Forms\CExObject;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * General purpose configuration
 */
class CConfiguration extends CMbObject
{
    public const INHERIT = "@@INHERIT@@";

    public const RESOURCE_TYPE = "configuration";

    /** @var int Primary key */
    public $configuration_id;

    /** @var string Name of the configuration */
    public $feature;

    /** @var string Value of the configuration */
    public $value;

    /** @var string Alternative value of the configuration (multiple environments purposes) */
    public $alt_value;

    public $object_class;
    public $object_id;

    /** @var bool */
    public $static;

    public $_ref_object;

    // The BIG config model
    private static $model_raw = [];
    private static $model     = [];

    /**
     * @inheritdoc
     */
    public function getSpec()
    {
        $spec                     = parent::getSpec();
        $spec->key                = "configuration_id";
        $spec->table              = "configuration";
        $spec->uniques["feature"] = ["feature", "object_class", "object_id"];

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps()
    {
        $props                 = parent::getProps();
        $props["feature"]      = "str notNull fieldset|default";
        $props["value"]        = "str fieldset|default";
        $props["alt_value"]    = "str";
        $props["object_id"]    = "ref class|CMbObject meta|object_class cascade back|configurations"; // not notNull
        $props["object_class"] = "str class show|0"; // not notNull
        $props['static']       = 'bool notNull default|0 fieldset|default';

        return $props;
    }

    /**
     * @inheritdoc
     */
    public function updateFormFields()
    {
        parent::updateFormFields();

        $this->_view = "[$this->feature] - ";

        if ($this->object_class && $this->object_id) {
            $this->_view .= "$this->object_class-$this->object_id";
        } else {
            $this->_view .= "global";
        }
    }

    /**
     * @inheritdoc
     */
    public function store()
    {
        if ($msg = parent::store()) {
            return $msg;
        }

        $module_name = explode(' ', $this->feature);
        CConfiguration::updateTableStatus($module_name[0], CMbDT::dateTime());

        return null;
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        $module_name = explode(' ', $this->feature);
        if ($msg = parent::delete()) {
            return $msg;
        }

        CConfiguration::updateTableStatus($module_name[0], CMbDT::dateTime());

        return null;
    }

    /**
     * @return bool
     */
    public function isStatic(): bool
    {
        return $this->static;
    }

    /**
     * Register a configuration tree
     *
     * @param array $configs The config tree with the specs
     *
     * @return void
     */
    public static function register($configs)
    {
        // Standard mode
        foreach ($configs as $_ctx => $_cfg) {
            $modules = array_keys($_cfg);
            foreach ($modules as $_module) {
                if (isset(self::$model_raw[$_module]) && isset(self::$model_raw[$_module][$_ctx])) {
                    self::$model_raw[$_module][$_ctx] = array_merge_recursive(self::$model_raw[$_module][$_ctx], $_cfg);
                    continue;
                }
                self::$model_raw[$_module][$_ctx] = $_cfg;
            }
        }

        $cache = Cache::getCache(Cache::INNER_OUTER);

        foreach (self::$model_raw as $module => $raw_model) {
            ContextualConfigurationManager::init($module, $raw_model, $cache);
        }
    }

    /**
     * Build the configuration tree
     *
     * @param string $module Name of the module to get tree for
     *
     * @return void
     */
    private static function buildTree($module): void
    {
        if (isset(self::$model_raw[$module])) {
            foreach (self::$model_raw[$module] as $_inherit => $_tree) {
                $list = [];
                self::_buildConfigs($list, [], $_tree);

                if (!isset(self::$model[$module][$_inherit])) {
                    self::$model[$module][$_inherit] = [];
                }

                self::$model[$module][$_inherit] = array_merge(self::$model[$module][$_inherit], $list);
            }
        } else {
            self::$model_raw[$module] = [];
        }
    }

    /**
     * Build configuration subtree
     *
     * @param array $list List to fill
     * @param array $path Configuration path
     * @param array $tree Model tree
     *
     * @return void
     */
    private static function _buildConfigs(&$list, $path, $tree)
    {
        foreach ($tree as $key => $subtree) {
            $_path   = $path;
            $_path[] = $key;

            // If a leaf (prop)
            if (is_string($subtree)) {
                // Build spec
                $parts = explode(" ", $subtree);

                $spec_options = [
                    "type"   => array_shift($parts),
                    "string" => $subtree,
                ];

                foreach ($parts as $_part) {
                    $options                             = explode("|", $_part, 2);
                    $spec_options[array_shift($options)] = count($options) ? $options[0] : true;
                }

                // Always have a default value
                if (!isset($spec_options["default"])) {
                    $spec_options["default"] = "";
                }

                $list[implode(" ", $_path)] = $spec_options;
            } // ... else a subtree
            else {
                self::_buildConfigs($list, $_path, $subtree);
            }
        }
    }

    /**
     * Get hash of the data
     *
     * @param array $data The data to get the hash of (MD5 of serialization)
     *
     * @return string
     */
    private static function _getHash($data)
    {
        return md5(serialize($data));
    }

    /**
     * Tells if the model cache is out of date
     *
     * @param string $hash   The hash of the model in cache
     * @param string $module Name of the module to check model for
     *
     * @return bool True if cache is out of date
     */
    private static function _isModelCacheDirty($hash, $module)
    {
        return (isset(self::$model_raw[$module])) ? ($hash !== self::_getHash(self::$model_raw[$module])) : 0;
    }

    /**
     * Get the up to date model
     *
     * @param string $module   Name of the module to get model of
     * @param array  $inherits An optional selection of inheritance paths
     *
     * @return array The up to date model
     */
    public static function getModel($module, $inherits = [])
    {
        $cache = Cache::getCache(Cache::OUTER);

        if (empty(self::$model[$module])) {
            if (($model = $cache->get("config-module-$module")) && !self::_isModelCacheDirty($model["hash"], $module)) {
                self::$model[$module] = $model["content"];
            } else {
                self::buildTree($module);

                $cache->set(
                    "config-module-$module",
                    [
                        "date"    => CMbDT::strftime(CMbDT::ISO_DATETIME),
                        // Don't use CMbDT::dateTime because it may be offsetted
                        "hash"    => self::_getHash(self::$model_raw[$module]),
                        "content" => (isset(self::$model[$module])) ? self::$model[$module] : [],
                    ]
                );
            }
        }

        if (!empty($inherits)) {
            if (!is_array($inherits)) {
                $inherits = [$inherits];
            }

            $subset = [];
            foreach ($inherits as $_inherit) {
                $subset[$_inherit] = self::$model[$module][$_inherit] ?? [];
            }

            return $subset;
        }

        return (isset(self::$model[$module])) ? self::$model[$module] : [];
    }

    /**
     * Get a specific value or a subtree of values
     *
     * @param string $object_guid The object to get config values of
     * @param string $feature     The configuration key to get
     *
     * @return array|string The configuration value or the subtree
     */
    public static function getValue($object_guid, $feature)
    {
        if (!ContextualConfigurationManager::isReady()) {
            if (PHP_SAPI !== 'cli') {
                CAppUI::setMsg('ContextualConfigurationManager not initialized', UI_MSG_ERROR);
            }

            return '';
        }

        $parts  = explode(' ', $feature, 2);
        $module = $parts[0];

        $object_class = $object_id = null;

        if ($object_guid !== 'global') {
            [$object_class, $object_id] = explode('-', $object_guid);
        }

        // Cache for massively requested context / features
        // /!\ SHOULD only be INNER
        $cache = static::getFeatureCache($feature, $object_guid);
        $value = $cache->get();

        if ($value !== null) {
            return $value;
        }

        try {
            $values = ContextualConfigurationManager::get($module)->getValues($object_class, $object_id);
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);

            return '';
        }

        return $cache->put(($values) ? CMbArray::readFromPath($values, $parts[1]) : '');
    }

    /**
     * @param string $path
     * @param mixed  $value
     * @param string $context
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public static function setValueInCache(string $path, $value, string $context): void
    {
        $cache = ($context === 'static') ? self::getStaticCache($path) : self::getFeatureCache($path, $context);
        $cache->put($value);
    }

    private static function getStaticCache(string $path): Cache
    {
        return new Cache('config-static-value', $path, Cache::INNER);
    }

    /**
     * @param string $path
     * @param string $context
     *
     * @return Cache
     * @throws CouldNotGetCache
     */
    private static function getFeatureCache(string $path, string $context): Cache
    {
        return new Cache("config-value-{$path}", $context, Cache::INNER);
    }

    /**
     * @param array  $paths
     * @param string $context
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public static function removeValuesFromCache(array $paths, string $context): void
    {
        if (!$paths) {
            return;
        }

        if ($context === 'static') {
            array_walk(
                $paths,
                function (&$v): void {
                    $v = 'config-static-value-' . $v;
                }
            );
        } else {
            array_walk(
                $paths,
                function (&$v) use ($context): void {
                    $v = "config-value-{$v}-{$context}";
                }
            );
        }

        $cache = Cache::getCache(Cache::INNER);

        $cache->deleteMultiple($paths);
    }

    /**
     * Get the object tree
     *
     * @param string $inherit Inheritance path
     *
     * @return array The object tree
     */
    public static function getObjectTree($inherit)
    {
        $tree = [];

        $inherit = self::_simplifyInherit($inherit);
        $classes = explode(" ", $inherit);

        self::_getObjectTree($tree, $classes);

        return $tree;
    }

    /**
     * Simplify an inheritance schema, removing the prefix
     *
     * @param string $inherit The inheritance schema
     *
     * @return string
     */
    private static function _simplifyInherit($inherit)
    {
        return preg_replace('@([\w ]+ / )@', "", $inherit);
    }

    /**
     * Recursive method to build the object tree
     *
     * @param array  $subtree    Sub tree to fill
     * @param array  $classes    Classes or inheritance schema
     * @param string $parent_fwd Parent forward ref
     * @param int    $parent_id  Parent ID
     *
     * @return void
     */
    private static function _getObjectTree(&$subtree, $classes, $parent_fwd = null, $parent_id = null)
    {
        static $cache = [];

        if (empty($classes)) {
            return;
        }

        $class  = array_pop($classes);
        $_parts = explode(".", $class);

        $fwd = null;
        if (count($_parts) === 2) {
            [$class, $fwd] = $_parts;
        }

        $where = [];
        if ($parent_fwd && $parent_id) {
            $where[$parent_fwd] = "= '$parent_id'";
        }

        $_cache_key = "$parent_fwd-$parent_id-$class";
        if (isset($cache[$_cache_key])) {
            $_list = $cache[$_cache_key];
        } else {
            /** @var CMbObject $_obj */
            $_obj = new $class();

            // Attention il faut generer les configurations de TOUS les objets, donc ne pas utiliser loadListWitfPerms
            $_list = $_obj->loadList($where);
            $_list = self::naturalSort($_list, ["_view"]);

            $cache[$_cache_key] = $_list;
        }

        if ($_list) {
            foreach ($_list as $_object) {
                $subtree[$_object->_guid] = [
                    "object"   => $_object,
                    "children" => [],
                ];

                self::_getObjectTree($subtree[$_object->_guid]["children"], $classes, $fwd, $_object->_id);
            }
        }
    }

    /**
     * Change a particular configuration value
     *
     * @param string                      $feature  Feature
     * @param mixed                       $value    Value
     * @param CMbObject|null              $object   Host object
     * @param IConfigurationStrategy|null $strategy Configuration strategy
     * @param bool                        $static   Store as a "static" configuration
     *
     * @return string|null
     * @throws Exception
     */
    public static function setConfig(
        $feature,
        $value,
        CMbObject $object = null,
        IConfigurationStrategy $strategy = null,
        bool $static = false
    ) {
        $strategy = new CConfigurationStrategy($strategy);

        return $strategy->setConfig($feature, $value, $object, $static);
    }

    /**
     * Save the configuration values of an object
     *
     * @param array                       $configs  Configs
     * @param CMbObject                   $object   Object
     * @param IConfigurationStrategy|null $strategy Configuration strategy
     * @param bool                        $static   Store as a "static" configuration
     *
     * @return array A list of store messages if any error happens
     * @throws Exception
     */
    public static function setConfigs(
        $configs,
        CMbObject $object = null,
        IConfigurationStrategy $strategy = null,
        bool $static = false
    ) {
        $messages = [];

        foreach ($configs as $_feature => $_value) {
            if ($msg = self::setConfig($_feature, $_value, $object, $strategy, $static)) {
                $messages[] = $msg;
            }
        }

        return $messages;
    }

    /**
     * Update the table_status table to record the last modification date of the configuration for $module
     *
     * @param array|string $module   The module name or an array of modules names
     * @param string       $datetime The last modification datetime
     *
     * @return void
     */
    private static function updateTableStatus($module, $datetime)
    {
        $modules = (is_array($module)) ? $module : [$module];

        foreach ($modules as $_module) {
            $table_status       = new CTableStatus();
            $table_status->name = "configuration-{$_module}";

            $table_status->loadMatchingObjectEsc();

            $table_status->update_time = $datetime;
            $table_status->rawStore();
        }
    }

    /**
     * Todo: Only get active module ones
     * @return void
     * @throws Exception
     */
    public static function registerAllConfiguration()
    {
        $registers = CClassMap::getInstance()->getClassChildren(IConfigurationRegister::class, true, true);

        /** @var IConfigurationRegister $configuration */
        foreach ($registers as $configuration) {
            $configuration->register();
        }

        ContextualConfigurationManager::setReady();
    }

    /**
     * @param CStoredObject $object
     *
     * @return void
     * @todo redefine meta raf
     * @deprecated
     */
    public function setObject(CStoredObject $object)
    {
        CMbMetaObjectPolyfill::setObject($this, $object);
    }

    /**
     * @param bool $cache
     *
     * @return bool|CStoredObject|CExObject|null
     * @throws Exception
     * @deprecated
     * @todo redefine meta raf
     */
    public function loadTargetObject($cache = true)
    {
        return CMbMetaObjectPolyfill::loadTargetObject($this, $cache);
    }

    /**
     * @inheritDoc
     * @todo remove
     */
    public function loadRefsFwd()
    {
        parent::loadRefsFwd();
        $this->loadTargetObject();
    }
}
