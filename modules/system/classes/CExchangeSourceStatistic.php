<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CRequest;

/**
 * Exchange Source
 */
class CExchangeSourceStatistic extends CMbObject
{
    /** @var int */
    public const CONNEXION_STATUS_SUCCESS = 1;

    /** @var int */
    public const CONNEXION_STATUS_FAILED = 2;

    /** @var int */
    public $exchange_source_statistic_id;

    /** @var string */
    public $object_class;

    /** @var int */
    public $object_id;

    /** @var int */
    public $failures;

    /** @var string */
    public $retry_strategy;

    /** @var int */
    public $max_retry;

    /** @var int */
    public $failures_average;

    /** @var int */
    public $last_status;

    /** @var string */
    public $first_call_date;

    /** @var int */
    public $nb_call;

    /** @var string */
    public $last_verification_date;

    /** @var string */
    public $last_connexion_date;

    /** @var int */
    public $last_response_time;

    /** @var bool */
    public $active_circuit_breaker;

    /** @var CExchangeSource */
    public CExchangeSource $_source;

    public static function newFromLast(?CExchangeSourceStatistic $last_stat): self
    {
        $now                                    = CMbDT::dateTime();
        $new_statistics                         = new CExchangeSourceStatistic();
        $new_statistics->nb_call                = ($last_stat && $last_stat->nb_call) ? $last_stat->nb_call + 1 : 1;
        $new_statistics->last_connexion_date    = ($last_stat && $last_stat->last_connexion_date)
            ? $last_stat->last_connexion_date
            : $now;
        $new_statistics->failures               = ($last_stat && $last_stat->failures) ? $last_stat->failures : 0;
        $new_statistics->last_verification_date = $now;

        return $new_statistics;
    }

    /**
     * @inheritdoc
     */
    function getSpec()
    {
        $spec           = parent::getSpec();
        $spec->table    = 'exchange_source_statistic';
        $spec->key      = 'exchange_source_statistic_id';
        $spec->loggable = false;

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps()
    {
        $props = parent::getProps();

        $last_status_list = implode('|', [self::CONNEXION_STATUS_SUCCESS, self::CONNEXION_STATUS_FAILED]);

        $props["object_class"]           = "str notNull class show|0";
        $props["object_id"]              = "ref class|CExchangeSource meta|object_class cascade back|source_statistics";
        $props["failures"]               = "num"; //nombre d'�checs
        $props["failures_average"]       = "num"; //moyenne des �checs
        $props["last_status"]            = "enum list|$last_status_list"; //dernier status
        $props["nb_call"]                = "num default|0"; // nombre de call depuis la cr�ation de la source
        $props["last_verification_date"] = "dateTime"; //date de derni�re v�rification d'accessibilit�
        $props["last_connexion_date"]    = "dateTime"; //date de derni�re connexion r�ussie
        $props["last_response_time"]     = "num";  //temps de r�ponse en Ms

        // todo pas de setup pour cette prop
        $props["active_circuit_breaker"] = "bool default|0"; //active ou non le circuit breaker
        $props["retry_strategy"] = "str"; //seuils d'attente avant test de disponibilit� en Ms
        $props["first_call_date"] = "dateTime"; // date de premier call
        $props["max_retry"] = "num"; //nombre d'essais

        return $props;
    }

    public function loadView()
    {
        parent::loadView();

        $this->_view = CAppUI::tr(
            'CExchangeSourceStatistic-msg-View',
            $this->last_verification_date,
            $this->last_response_time
        );
    }

    /**
     * @return CExchangeSource
     * @throws Exception
     */
    public function loadRefSource(): CExchangeSource
    {
        return $this->_ref_source = $this->loadFwdRef("object_id");
    }

    /** @inheritDoc store() */
    function store()
    {
        /* Possible purge when creating CExchangeSourceStatistic */
        if (!$this->_id) {
            CApp::doProbably(
                CAppUI::conf('eai CExchangeSourceStatistic purge_probability', 'static'),
                [$this, 'purgeSome']
            );
        }

        return parent::store();
    }

    /**
     * Purge the CExchangeDataFormat older than the configured threshold
     *
     * @return bool|resource|void
     */
    function purgeSome()
    {
        $purge_delete_threshold = CAppUI::conf(
            'eai CExchangeSourceStatistic purge_delete_threshold',
            'static'
        );

        $date  = CMbDT::dateTime("- {$purge_delete_threshold} days");
        $limit = CAppUI::conf('eai CExchangeSourceStatistic purge_probability', 'static') * 100;

        if (!$limit) {
            return null;
        }

        $ds    = $this->getDS();
        $query = new CRequest();
        $query->addTable($this->_spec->table);
        $query->addWhereClause("last_verification_date", "< '$date'");
        $query->setLimit($limit);
        $ds->exec($query->makeDelete());
    }
}
