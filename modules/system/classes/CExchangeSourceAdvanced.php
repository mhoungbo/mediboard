<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\ClientInterface;
use Ox\Core\CStoredObject;
use Ox\Interop\Eai\CExchangeTransportLayer;
use Ox\Interop\Eai\Resilience\ClientContext;
use Ox\Mediboard\Etablissement\CGroups;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Exchange Source
 */
class CExchangeSourceAdvanced extends CExchangeSource
{
    public const DEFAULT_RETRY_STRATEGY = "1|5 5|60 10|120 20|";

    /** @var int */
    public const SOURCE_AVAILABLE = 0;

    /** @var int */
    public const SOURCE_BLOCKED = 1;

    /** @var string */
    public $retry_strategy;

    /** @var string */
    public $first_call_date;

    /** @var array<string, bool> */
    public static $failure = [];

    /** @var array */
    public static $elements_to_save_in_callback = [];

    private static array $_blacklist_circuit_breaker = [];

    /** @var CExchangeSourceStatistic[] */
    public $_ref_statistics;

    /** @var CExchangeSourceStatistic */
    public $_ref_last_statistics;

    /** @var bool */
    public $_blocked;

    /** @var array */
    public static $call_details = [];

    /**
     * @inheritdoc
     */
    public function getProps()
    {
        //Props utilis�es par le circuit breaker
        $props                    = parent::getProps();
        $props["retry_strategy"]  = "str"; //seuils d'attente avant test de disponibilit� en Ms exemple => "5,10,50,60"
        $props["first_call_date"] = "dateTime loggable|0"; // date de premier call effectu� par la source

        //si la source est bloqu� ou non par le circuit breaker
        $blocked_status    = implode('|', [self::SOURCE_AVAILABLE, self::SOURCE_BLOCKED]);
        $props["_blocked"] = "enum list|$blocked_status";

        return $props;
    }

    /**
     * @return ClientInterface
     * @throws CMbException
     * @throws Exception
     */
    public function getClient(): ClientInterface
    {
        if ($this->_client) {
            return $this->_client;
        }

        if (!$this->client_name) {
            $this->client_name = $this::DEFAULT_CLIENT;
        }

        if (!($client_class = ($this::CLIENT_MAPPING[$this->client_name] ?? null))) {
            throw new CMbException(
                'CExchangeSource-client_name-no mapping class',
                $this->client_name,
                CAppUI::tr($this->_class)
            );
        }

        /** @var ClientInterface $client */
        $client = new $client_class();
        $client->init($this);

        if ($client instanceof EventSubscriberInterface) {
            $this->_dispatcher->addSubscriber($client);
        }

        // trace des calls
        $this->_dispatcher->addListener(ClientInterface::EVENT_BEFORE_REQUEST, [$this, 'registerCallback'], 0);
        $this->_dispatcher->addListener(ClientInterface::EVENT_BEFORE_REQUEST, [$this, 'onBeforeRequest'], 200);
        $this->_dispatcher->addListener(ClientInterface::EVENT_BEFORE_REQUEST, [$this, 'startCallTrace'], -900);

        $this->_dispatcher->addListener(ClientInterface::EVENT_AFTER_REQUEST, [$this, 'stopCallTrace'], 900);
        $this->_dispatcher->addListener(ClientInterface::EVENT_AFTER_REQUEST, [$this, 'callDetails'], 800);
        $this->_dispatcher->addListener(ClientInterface::EVENT_AFTER_REQUEST, [$this, 'onAfterRequest'], 200);

        $this->_dispatcher->addListener(ClientInterface::EVENT_EXCEPTION, [$this, 'onException'], 0);

        if ($this->loggable) {
            $this->_dispatcher->addListener(ClientInterface::EVENT_BEFORE_REQUEST, [$this, 'startLog'], 300);
            $this->_dispatcher->addListener(ClientInterface::EVENT_AFTER_REQUEST, [$this, 'stopLog'], 300);
            $this->_dispatcher->addListener(ClientInterface::EVENT_EXCEPTION, [$this, 'exceptionLog'], 0);
        }

        return $this->_client = $this->getResilientClient($client) ?: $client;
    }

    /**
     * Load all statistics
     *
     * @return CExchangeSourceStatistic[]
     * @throws Exception
     */
    public function loadRefsStatistic(): array
    {
        return $this->_ref_statistics = $this->loadBackRefs("source_statistics");
    }

    /**
     * Load last statistics
     *
     * @return CExchangeSourceStatistic
     * @throws Exception
     */
    public function loadRefLastStatistic(): CExchangeSourceStatistic
    {
        if ($this->_ref_last_statistics) {
            return $this->_ref_last_statistics;
        }

        $last_stat = $this->loadBackRefs(
            "source_statistics",
            ["exchange_source_statistic_id DESC"],
            "1",
        );

        return $this->_ref_last_statistics = empty($last_stat) ? new CExchangeSourceStatistic() : reset($last_stat);
    }

    /**
     * unlock source when max failures
     *
     * @return void
     * @throws Exception
     */
    public function unlockSource(): void
    {
        //remise � z�ro du nombre de failure de la derni�re stat pour d�bloquer la source
        $last_stat = $this->loadRefLastStatistic();
        if ($last_stat) {
            $last_stat->failures = 0;

            if ($msg = $last_stat->store()) {
                throw new CMbException($msg);
            }
        }
    }

    /**
     * return true if source blocked
     * return false if source is available
     *
     * @return bool
     */
    public function getBlockedStatus(): bool
    {
        if (empty($this->retry_strategy)) {
            return false;
        } else {
            $max      = $this->getMaxRetryFromStrategy($this->retry_strategy);
            $stat     = $this->loadRefLastStatistic();
            $failures = ($stat === null) ? 0 : $stat->failures;

            return $this->_blocked = $failures >= $max;
        }
    }

    /**
     * give max retry from strategy
     *
     * @param $strategy
     *
     * @return string
     */
    public function getMaxRetryFromStrategy(string $strategy): string
    {
        $max = explode(" ", $strategy);
        $max = explode("|", end($max));

        return reset($max);
    }

    /**
     * * cr�� et enregistre une statistic en base apres l'�xecution d'un appel � la source
     *
     * @param ClientContext $context
     *
     * @return void
     */
    public function onAfterRequest(ClientContext $context): void
    {
        // update only first_call_date when this metrics is not set
        if ($this->_id && !$this->first_call_date) {
            if (!isset(self::$elements_to_save_in_callback[$this->_guid][get_class($this)])) {
                $source                  = new $this->_class();
                $source->_id             = $this->_id;
                $source->first_call_date = CMbDT::dateTime();

                // keep update datetime on source to prevent restore in future call
                $this->first_call_date = $source->first_call_date;

                $this->registerStoreInCallback($source);
            }
        }

        $last_stat = $this->loadRefLastStatistic();

        $new_statistics                     = CExchangeSourceStatistic::newFromLast($last_stat);
        $new_statistics->object_class       = $this->_class;
        $new_statistics->object_id          = $this->_id;
        $new_statistics->last_status        = CExchangeSourceStatistic::CONNEXION_STATUS_SUCCESS;
        $new_statistics->last_response_time = $this->_current_chronometer->total * 1000;

        $throwable = $context->getThrowable();
        $response  = $context->getResponse();

        // todo rendre $call_in_failure diff�rent en fonction de chaque source + surchargeable depuis un script
        $call_in_failure = isset($throwable) || $response === false;

        if ($call_in_failure) {
            $new_statistics->last_status = CExchangeSourceStatistic::CONNEXION_STATUS_FAILED;

            // increment only when first call fail
            if (!(self::$failure[$this->_guid] ?? false)) {
                $new_statistics->failures = intval($new_statistics->failures) + 1;
            }

            self::$failure[$this->_guid] = true;
        }

        if (!$call_in_failure && !(self::$failure[$this->_guid] ?? false)) {
            $new_statistics->failures = 0;
        }

        if (!(self::$failure[$this->_guid] ?? false)) {
            $new_statistics->last_connexion_date = CMbDT::dateTime();
        }

        $this->_ref_last_statistics = $new_statistics;

        $this->registerStoreInCallback($new_statistics);
    }

    /**
     * Save elements in master server at the end of requests
     *
     * @return void
     * @throws Exception
     */
    public static function storeElementsInCallbacks(): void
    {
        $elements_in_sources = self::$elements_to_save_in_callback;
        unset($elements_in_sources[self::class]);

        // reset to prevent double store
        self::$elements_to_save_in_callback = [];

        foreach ($elements_in_sources as $source_guid => $elements) {
            $source = CStoredObject::loadFromGuid($source_guid);
            if (!$source || !$source->_id) {
                continue;
            }

            // save source
            /** @var CExchangeSource[] $exchange_sources */
            $exchange_sources = $elements[get_class($source)] ?? [];
            foreach ($exchange_sources as $exchange_source) {
                if ($msg = $exchange_source->store()) {
                    CApp::log($msg, ['source' => $exchange_source]);
                }
            }

            // save statistics
            /** @var CExchangeSourceStatistic[] $statistics_to_save */
            $statistics_to_save = $elements[CExchangeSourceStatistic::class] ?? [];
            foreach ($statistics_to_save as $statistics) {
                if ($msg = $statistics->store()) {
                    CApp::log($msg, ['statistics' => $statistics]);
                }
            }

            // save traces
            $exchange_transport_to_save = $elements[CExchangeTransportLayer::class] ?? [];
            foreach ($exchange_transport_to_save as $exchange) {
                if ($msg = $exchange->store()) {
                    CApp::log($msg, ['exchange' => $exchange]);
                }
            }
        }
    }

    /**
     * Register callback for save elements (traces, stats...) at the end of request in the master server
     *
     * @return void
     */
    public function registerCallback(): void
    {
        if (empty(self::$elements_to_save_in_callback)) {
            self::$elements_to_save_in_callback[self::class] = true;
            CApp::registerCallback([self::class, "storeElementsInCallbacks"]);
        }
    }

    /**
     * Register object to store at the end of request
     *
     * @param CStoredObject $object
     *
     * @return void
     */
    protected function registerStoreInCallback(CStoredObject $object): void
    {
        $key = get_class($object);
        if ($object instanceof CExchangeTransportLayer) {
            $key = CExchangeTransportLayer::class;
        }

        self::$elements_to_save_in_callback[$this->_guid][$key][] = $object;
    }

    /**
     * Get signature of the call
     *
     * @param ClientContext $context
     *
     * @return string
     */
    protected function getCallSignature(ClientContext $context): string
    {
        $argument = $context->getArguments();

        $host          = $this->host ?? '';
        $port          = $this->port ?? '';
        $function_name = $argument['function_name'] ?? '';

        return "$function_name : $host";
    }

    /**
     * Collect each call with his signature and time duration
     *
     * @param ClientContext $context
     *
     * @return void
     */
    public function callDetails(ClientContext $context): void
    {
        $signature = $this->getCallSignature($context);
        if (!$signature) {
            return;
        }

        $_key_trace   = static::class;
        $request_time = $this->_current_chronometer->latestStep;

        self::$call_details[$_key_trace][] = ['signature' => $signature, 'request_time' => $request_time];
    }

    /**
     * Reactivate the protection of circuit breaker
     *
     * @return ClientInterface
     * @throws CMbException
     */
    public function reactivateResilience(): ClientInterface
    {
        $this->_client = null;
        unset(self::$_blacklist_circuit_breaker[$this->_guid]);

        return $this->getClient();
    }

    /**
     * Disable temporally the circuit breaker
     *
     * @return ClientInterface
     * @throws CMbException
     */
    public function disableResilience(): ClientInterface
    {
        $this->_client = null;
        self::$_blacklist_circuit_breaker[$this->_guid] = true;

        return $this->getClient();
    }

    /**
     * Encapsulate client with a protection to upgrade client to resilient client
     *
     * @param ClientInterface $client
     *
     * @return ClientInterface|null
     * @throws Exception
     */
    protected function getResilientClient(ClientInterface $client): ?ClientInterface
    {
        if (self::$_blacklist_circuit_breaker[$this->_guid] ?? false) {
            return null;
        }

        if (CApp::getInstance()->isPublic()) {
            $enable_config = true;
        } else {
            $enable_config = CAppUI::gconf("eai CircuitBreaker enable", CGroups::loadCurrent()->_id);
        }

        if ($this->retry_strategy && $enable_config) {
            return $client;
        }

        return null;
    }
}
