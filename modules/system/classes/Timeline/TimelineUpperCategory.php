<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Timeline;

/**
 * Class TimelineUpperCategory
 */
class TimelineUpperCategory
{
    public function __construct(private string $type, private string $logo, private string $name, private string $color)
    {
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function setLogo(string $logo): void
    {
        $this->logo = $logo;
    }

    public function getLogo(): string
    {
        return $this->logo;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getColor(): string
    {
        return $this->color;
    }
}
