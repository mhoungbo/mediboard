<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers\Legacy;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CLegacyController;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CMbObject;
use Ox\Core\CSmartyDP;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Core\ExplainQueryGenerator;
use Ox\Core\Index\ClassIndexer;
use Ox\Core\Module\CModule;
use Ox\Core\Security\Crypt\Hash;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Mediboard\System\CExchangeSourceAdvanced;
use Ox\Mediboard\System\CMergeLog;
use Ox\Mediboard\System\Keys\CKeyMetadata;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotPersistKey;
use Ox\Mediboard\System\Keys\KeyBuilder;
use Throwable;

class SystemLegacyController extends CLegacyController
{
    public function ajax_search_merge_logs(): void
    {
        $this->checkPermAdmin();

        $order_col = CView::get(
            'order_col',
            'enum list|date_start_merge|date_before_merge|date_after_merge|date_end_merge|duration'
        );
        $order_way = CView::get('order_way', 'enum list|ASC|DESC');

        $start = CView::get('start', 'num default|0');
        $step  = CView::get('step', 'num default|50');

        $min_date_start_merge = CView::get('_min_date_start_merge', 'dateTime');
        $max_date_start_merge = CView::get('_max_date_start_merge', 'dateTime');
        $min_date_end_merge   = CView::get('_min_date_end_merge', 'dateTime');
        $max_date_end_merge   = CView::get('_max_date_end_merge', 'dateTime');

        $object_class   = CView::get('object_class', 'str');
        $base_object_id = CView::getRefCheckRead('base_object_id', 'ref class|CStoredObject meta|object_class');
        $user_id        = CView::getRefCheckRead('user_id', 'ref class|CUser');
        $status         = CView::get('status', 'enum list|all|ok|ko');

        CView::checkin();

        $ds = CSQLDataSource::get('std');

        $order_col = ($order_col) ?: 'date_start_merge';
        $order_by  = $order_col;

        switch ($order_way) {
            case 'ASC':
                $order_by .= ' ASC';
                break;

            case 'DESC':
            default:
                $order_by .= ' DESC';
        }

        $start = ($start >= 0) ? $start : 0;
        $step  = ($step > 0) ? $step : 50;

        $limit = "{$start}, {$step}";

        $merge_log = new CMergeLog();

        $where = [];

        if ($min_date_start_merge) {
            $where[] = $ds->prepare('date_start_merge >= ?', $min_date_start_merge);
        }

        if ($max_date_start_merge) {
            $where[] = $ds->prepare('date_start_merge <= ?', $max_date_start_merge);
        }

        if ($min_date_end_merge) {
            $where[] = $ds->prepare('date_end_merge >= ?', $min_date_end_merge);
        }

        if ($max_date_end_merge) {
            $where[] = $ds->prepare('date_end_merge <= ?', $max_date_end_merge);
        }

        if ($object_class) {
            $where['object_class'] = $ds->prepare('= ?', $object_class);
        }

        if ($base_object_id) {
            $where['base_object_id'] = $ds->prepare('= ?', $base_object_id);
        }

        if ($user_id) {
            $where['user_id'] = $ds->prepare('= ?', $user_id);
        }

        $status = ($status) ?: 'all';

        if ($status === 'ko') {
            $where[] = 'date_end_merge IS NULL';
        } elseif ($status === 'ok') {
            $where[] = 'date_end_merge IS NOT NULL';
        }

        $total      = $merge_log->countList($where);
        $merge_logs = $merge_log->loadList($where, $order_by, $limit);

        $this->renderSmarty(
            'inc_vw_merge_logs',
            [
                'merge_logs' => $merge_logs,
                'order_col'  => $order_col,
                'order_way'  => $order_way,
                'total'      => $total,
                'start'      => $start,
                'step'       => $step,
            ]
        );
    }

    public function ajax_show_merge_log(): void
    {
        $this->checkPermAdmin();

        $merge_log_id = CView::getRefCheckRead('merge_log_id', 'ref class|CMergeLog notNull');

        CView::checkin();

        $merge_log = CMergeLog::findOrFail($merge_log_id);

        $merge_log->loadRefUser();
        $merge_log->loadBaseObject();
        $merge_log->loadObjects();

        $this->renderSmarty(
            'show_merge_log',
            [
                'merge_log' => $merge_log,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function autocompleteClasses(): void
    {
        $this->checkPerm();

        $input_field = CView::get("input_field", "str");
        $keywords    = CView::get($input_field, 'str');
        $profile     = CView::get("profile", "str");
        $mod_name    = CView::get("mod_name", "str");

        CView::checkin();

        $indexer = new ClassIndexer();
        $classes = $indexer->search($keywords);

        $modules = [];
        foreach ($classes as $_class) {
            $module           = $_class->getModule();
            $modules[$module] = CModule::getActive($module);
        }

        // Only return list of unique modules
        if ($profile === 'moduleName') {
            $modules_occurs = array_fill_keys(array_keys($modules), false);

            foreach ($classes as $k => $_class) {
                $module = $_class->getModule();

                if ($modules_occurs[$module] === false) {
                    $modules_occurs[$module] = true;
                } else {
                    unset($classes[$k]);
                }
            }
        }

        // If mod_name is valued, only return classes of this specific module
        if ($mod_name !== null) {
            foreach ($classes as $k => $_class) {
                $module = $_class->getModule();

                if ($module !== $mod_name) {
                    unset($classes[$k]);
                }
            }
        }

        $this->renderSmarty(
            'autocomplete/inc_classes_autocomplete',
            [
                'keywords' => $keywords,
                'matches'  => $classes,
                'profile'  => $profile,
                'modules'  => $modules,
            ]
        );
    }

    public function explainQuery(): void
    {
        $this->checkPermAdmin();

        $params = AntiCsrf::validatePOST();

        CView::checkin();

        CView::enforceSlave();

        $explain_generator = new ExplainQueryGenerator();
        [$dsn, $query] = $explain_generator->denormalizeQuery($params['query_sample']);

        $ds = CSQLDataSource::get($dsn);

        CApp::json(
            [
                'explain'  => $explain_generator->explainQuery($query, $dsn),
                'real_dsn' => $ds->dsn,
                'dsn'      => $dsn,
                'query'    => $query,
                'uid'      => (new Hasher())->hash(Hash::MD5, $params['query_sample']),
            ]
        );
    }

    public function replayQuery(): void
    {
        $this->checkPermAdmin();

        $params = AntiCsrf::validatePOST();

        CView::checkin();

        CView::enforceSlave();

        $explain_generator = new ExplainQueryGenerator();
        [$duration, $rows] = $explain_generator->replayNormalizedQuery($params['query_sample']);

        CApp::json(
            [
                'duration' => $duration,
                'rows'     => $rows,
            ]
        );
    }

    public function ajaxAddFile(): void
    {
        CCanDo::checkAdmin();

        $source_guid       = CView::get("source_guid", "str");
        $current_directory = CView::get("current_directory", "str");
        CView::checkin();

        $max_size = ini_get("upload_max_filesize");
        $source   = CMbObject::loadFromGuid($source_guid);

        //template
        $smarty = new CSmartyDP();
        $smarty->assign("source_guid", $source_guid);
        $smarty->assign("current_directory", $current_directory);
        $smarty->assign("max_size", $max_size);

        $smarty->display("inc_add_file.tpl");
    }

    public function ajaxGetSourceReachable(): void
    {
        CCanDo::check();
        $source_guid = CView::get("source_guid", "str");
        CView::checkin();

        /** @var CExchangeSource $source */
        $source = CMbObject::loadFromGuid($source_guid);
        if ($source instanceof CExchangeSourceAdvanced) {
            $source->disableResilience();
        }
        try {
            $source->isReachable();
        } catch (Exception $e) {
        }
        $source->_response_time = 0;


        $status = [
            'type' => CAppUI::tr($source->_class),
            'source_id' => $source->_id,
            'active' => $source->active,
            'reachable' => $source->_reachable,
            'message' => trim($source->_message),
            'name' => $source->name,
            'response_time' => ($source->_response_time > 0) ? ($source->_response_time . " ms") : $source->_response_time,
        ];

        dump($status);

        CApp::json($status);
    }
}
