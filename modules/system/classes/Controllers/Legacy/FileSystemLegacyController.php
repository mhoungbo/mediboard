<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers\Legacy;

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CLegacyController;
use Ox\Core\CMbException;
use Ox\Core\CSmartyDP;
use Ox\Core\CValue;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Mediboard\System\CSourceFileSystem;

class FileSystemLegacyController extends CLegacyController
{
    protected string            $type_action;
    protected string            $exchange_source_name;
    protected CSourceFileSystem $exchange_source;

    public function initParams(): void
    {
        CCanDo::checkAdmin();

        // Check params
        if (null == $this->exchange_source_name = CValue::get("exchange_source_name")) {
            CAppUI::stepMessage(UI_MSG_ERROR, "Aucun nom de source sp�cifi�");
        }

        if (null == $this->type_action = CValue::get("type_action")) {
            CAppUI::stepMessage(UI_MSG_ERROR, "Aucun type de test sp�cifi�");
        }

        $this->exchange_source = CExchangeSource::get($this->exchange_source_name, "file_system", true, null, false);
    }

    public function ajaxConnexionFileSystem(): void
    {
        $this->initParams();

        // Connexion
        if ($this->type_action != "connexion") {
            return;
        }

        try {
            $this->exchange_source->_client = null;
            $this->exchange_source->retry_strategy = null;
            $isreachable = $this->exchange_source->getClient()->isReachableSource();
            $isauthent = $this->exchange_source->getClient()->isAuthentificate();

            if (!$isreachable || !$isauthent) {
                $exception = new CMbException($this->exchange_source->_message);
                throw $exception;
            }

            CAppUI::stepMessage(UI_MSG_OK, "CSourceFileSystem-host-is-a-dir", $this->exchange_source->host);
        } catch (CMbException $e) {
            CAppUI::stepMessage(UI_MSG_ERROR, $e->getMessage());
        }
    }

    public function ajaxSendFileFileSystem(): void
    {
        $this->initParams();

        if ($this->type_action == "sendFile") {
            try {
                $this->exchange_source->setData("Test source file system in Mediboard", false);
                $path = $this->exchange_source->completePath($this->exchange_source->generateFileName());

                $this->exchange_source->getClient()->send($path);
                CAppUI::stepMessage(
                    UI_MSG_OK,
                    "Le fichier 'testSendFile" . $this->exchange_source->fileextension . "' a �t� copi� dans le dossier '" . $this->exchange_source->host . "'"
                );
            } catch (CMbException $e) {
                CAppUI::stepMessage(UI_MSG_ERROR, $e->getMessage());
            }
        }
    }

    public function ajaxGetFileFileSystem(): void
    {
        $this->initParams();

        // Cr�ation du template
        $smarty = new CSmartyDP();
        $smarty->assign("source_guid", $this->exchange_source->_guid);
        $smarty->display("inc_manage_files.tpl");
    }
}
