<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers\Legacy;

use Ox\Core\CLegacyController;
use Ox\Core\CView;
use Ox\Mediboard\System\CStoredObjectAutocomplete;

/**
 * Handle autocomplete calls
 */
class AutocompleteController extends CLegacyController
{
    public function httpreq_field_autocomplete(): void
    {
        $this->checkPerm();

        $class          = CView::get('class', 'str notNull');
        $field          = CView::get('field', 'str');
        $view_field     = CView::get('view_field', 'str default|' . $field);
        $show_view      = CView::get('show_view', 'str default|false') === 'true';
        $input_field    = CView::get('input_field', 'str default|' . $view_field);
        $input          = CView::get($input_field, 'str');
        $limit          = CView::get('limit', 'num default|30');
        $wholeString    = CView::get('wholeString', 'str default|false') === 'true';
        $where          = CView::get('where', 'str') ?: [];
        $min_occurences = CView::get('min_occurences', 'num default|1');
        $min_length     = CView::get('min_length', 'num');

        CView::checkin();

        CView::enforceSlave(false);

        $options = [
            CStoredObjectAutocomplete::OPTION_WHOLE_STRING    => $wholeString,
            CStoredObjectAutocomplete::OPTION_MIN_LENGTH      => $min_length,
            CStoredObjectAutocomplete::OPTION_MIN_OCCURRENCES => $min_occurences,
            CStoredObjectAutocomplete::OPTION_LIMIT           => $limit,
        ];

        $autocomplete = new CStoredObjectAutocomplete($class, $field, $view_field, $options);
        $autocomplete->addWhere($where);
        $matches = $autocomplete->search($input);

        $this->renderSmarty(
            'inc_field_autocomplete',
            [
                'matches'    => $matches,
                'count'      => 0,
                'input'      => $input,
                'field'      => $field,
                'view_field' => $view_field,
                'show_view'  => $show_view,
                'template'   => $autocomplete->getTemplate(),
                'nodebug'    => true,
                'ref_spec'   => $autocomplete->isRefSpec(),
            ]
        );
    }
}
