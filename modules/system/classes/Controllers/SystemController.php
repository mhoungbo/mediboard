<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CModelObject;
use Ox\Core\Config\Conf;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\EntryPoint;
use Ox\Core\Kernel\Exception\ObjectNotFoundException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Locales\Translator;
use Ox\Core\Security\Antivirus\Exception\InfectedFileException;
use Ox\Mediboard\System\CObjectNavigation;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Profiler\Profiler;
use Throwable;

class SystemController extends Controller
{

    /**
     * @throws Exception
     */
    public function offline(string $message, bool $ajax = false): Response
    {
        if ($ajax) {
            $this->addUiMsgError($message);
            $response = $this->renderEmptyResponse(Response::HTTP_SERVICE_UNAVAILABLE);
        } else {
            $vars     = [
                "src_logo"    => $this->getSrcLogo(),
                "message"     => $message,
                "application" => $this->conf->get("product_name"),
            ];
            $response = $this->render('offline.html.twig', $vars);
        }

        $headers = [
            "Retry-After"  => 300,
            "Content-Type" => "text/html; charset=iso-8859-1",
        ];
        $response->headers->add($headers);

        return $response;
    }

    public function notFound(Throwable $t, bool $ajax = false): Response
    {
        if ($ajax) {
            $this->addUiMsgError('common-This page does not exists', true);

            return $this->renderEmptyResponse(Response::HTTP_NOT_FOUND);
        }

        $title       = $this->translator->tr('common-Page not found');
        $description = $this->translator->tr('common-This page does not exists');

        if ($t instanceof ObjectNotFoundException) {
            $title       = $this->translator->tr('common-Object not found');
            $description = $t->getMessage();
        }

        $vars = [
            "src_logo"       => $this->getSrcLogo(),
            "application"    => $this->conf->get('product_name'),
            "title"          => $title,
            "description"    => $description,
            "call_to_action" => $this->translator->tr('common-Check the url'),
            "action_title"   => $this->translator->tr('common-Back to main page'),
            "action"         => $this->conf->get('external_url'),
        ];

        return $this->render('notFound.html.twig', $vars);
    }

    public function errorOccured(Throwable $exception, string $status_code, bool $ajax = false)
    {
        try {
            $user = $this->getCUser();
        } catch (Throwable) {
            $user = null;
        }

        // Template vars
        $title          = "common-error-An error occurred";
        $description    = "common-error-An error occurred-desc";
        $call_to_action = "common-error-Please try again later";
        $action         = "common-Back to main page";
        $illustration   = "images/illustrations/error.svg";

        if ($status_code == Response::HTTP_FORBIDDEN) {
            $title          = "common-Restricted access";
            $description    = "common-Page access is restricted";
            $call_to_action = "common-Contact an administrator or go back to main page";
            $illustration   = "images/illustrations/forbidden.svg";
        } elseif (
            $exception instanceof InfectedFileException
        ) {
            $description = $exception->getMessage();
        }

        if ($ajax) {
            $this->addUiMsgError($description, true);

            return $this->renderEmptyResponse($status_code);
        }

        $entry = new EntryPoint("ErrorPage");
        $entry->setScriptName("errorPage")
              ->addData("status-code", $status_code)
              ->addData(
                  "message",
                  htmlentities(mb_convert_encoding($exception->getMessage(), 'UTF-8', 'ISO-8859-1'), ENT_COMPAT)
              )->addData("title", $title)
              ->addData("description", $description)
              ->addData("call-to-action", $call_to_action)
              ->addData("action", $action)
              ->addData("illustration", $illustration)
              ->addLinkValue("logo", $this->getSrcLogo())
              ->addLinkValue("actionLink", ($user) ? $user->getDefaultPageLink() : '');

        $vars = [
            "entry_point" => $entry,
        ];

        return $this->renderSmarty('display_entry_point', $vars, "system");
    }

    /**
     * @api public
     */
    public function status(Conf $conf)
    {
        $status = CApp::getProxyStatus();

        $resource = new Item(
            [
                'id'          => CApp::getRequestUID(),
                'status'      => $status,
                'version'     => (string)CApp::getVersion(),
                'release'     => CApp::getVersion()->getCode() ?? 'undefined',
                'date'        => CApp::getVersion()->getCompleteDate() ?? 'undefined',
                'instance'    => $conf->get('instance_role'),
                'product'     => $conf->get('product_name'),
                'database'    => @CSQLDataSource::get("std") ? "up" : "down",
                'maintenance' => $conf->get('offline') || $conf->get('offline_non_admin') ? 'on' : 'off',
            ]
        );

        $resource->setType('api_status');

        return $this->renderApiResponse($resource, 200, ['proxy' => $status]);
    }

    /**
     * @return Response
     * @throws Exception
     * @api public
     */
    public function openapi(?Profiler $profiler)
    {
        if (null !== $profiler) {
            // if it exists, disable the profiler for this particular controller action
            $profiler->disable();
        }

        return $this->render("@system/swagger.html.twig", [
            "documentation" => "/../../includes/documentation.yml",
        ]);
    }

    /**
     * @param Conf       $conf
     * @param Translator $tr
     *
     * @return Response
     * @throws Exception
     */
    public function about(Conf $conf, Translator $tr): Response
    {
        return $this->renderSmarty(
            'about.tpl',
            [
                'version'      => (string)CApp::getVersion(),
                'product_name' => $conf->get('product_name'),
                'description'  => $tr->tr('common-about'),
            ],
            null,
            200,
            [
                'proxy' => CApp::getProxyStatus(),
            ]
        );
    }

    /**
     * @api public
     */
    public function discovery(): Response
    {
        $item = new Item(
            [
                'login'                 => [
                    'url'         => $this->generateUrl('admin_gui_login'),
                    'description' => 'Login page',
                    'method'      => 'GET',
                ],
                'logout'                => [
                    'url'         => $this->generateUrl('admin_gui_logout'),
                    'description' => 'Logout action',
                    'method'      => 'GET',
                ],
                'status'                => [
                    'url'         => $this->generateUrl('system_api_status'),
                    'description' => 'Application status info',
                    'method'      => 'GET',
                ],
                'openapi'               => [
                    'url'         => $this->generateUrl('system_openapi'),
                    'description' => 'Display the openapi of the application',
                    'method'      => 'GET',
                ],
                'configurations'        => [
                    'url'         => $this->generateUrl('system_get_configs'),
                    'description' => 'Get configurations which paths are specified in the request body',
                    'method'      => 'GET',
                ],
                'module_configurations' => [
                    'url'          => $this->generateUrl('system_configs_instance', ['mod_name' => 'mod_name']),
                    'description'  => 'Get the instance configurations for the module mod_name',
                    'method'       => 'GET',
                    'requirements' => [
                        'mod_name' => '\w+',
                    ],
                ],
                'preferences'           => [
                    'url'          => $this->generateUrl('system_preferences', ['mod_name' => 'mod_name']),
                    'description'  => 'Get the preferences of the current user for the module mod_name',
                    'method'       => 'GET',
                    'requirements' => [
                        'mod_name' => '\w+',
                    ],
                ],
                'locales'               => [
                    'url'          => $this->generateUrl(
                        'system_locales',
                        ['language' => 'fr', 'mod_name' => 'mod_name']
                    ),
                    'description'  => 'Get the locales for the language and mod_name in the parameters',
                    'method'       => 'GET',
                    'requirements' => [
                        'language' => 'fr|en|it|de|fr-be|nl-be',
                        'mod_name' => '\w+',
                    ],
                ],
                'bulk'                  => [
                    'url'         => $this->generateUrl('system_bulk_operations'),
                    'description' => 'Send a set of requests to handle',
                    'method'      => 'POST',
                ],
            ]
        );
        $item->setType('discovery_links');

        return $this->renderApiResponse($item);
    }

    /**
     * @param Conf $conf
     *
     * @return Response
     * @throws Exception
     */
    public function psCgu(Conf $conf): Response
    {
        return $this->render("@system/ps_cgu.html.twig", [
            'product_name' => $conf->get('product_name'),
        ]);
    }

    /**
     * For admin user display the object details or the collections of an object.
     *
     * @param string        $resource_type
     * @param string        $resource_id
     * @param RequestParams $params
     *
     * @return Response
     * @throws InvalidArgumentException
     */
    public function objectDetails(string $resource_type, string $resource_id, RequestParams $params): Response
    {
        $this->enforceSlave();

        if (!class_exists($resource_type)) {
            $resource_type = CModelObject::getClassNameByResourceType($resource_type, true);
        }

        $obj_nav = new CObjectNavigation($resource_type, $resource_id);

        if (!$obj_nav->object_select) {
            $this->addUiMsgError('SystemController-Error-No object found');

            return $this->renderEmptyResponse(Response::HTTP_NOT_FOUND);
        }

        if ($collection_name = $params->get('collection_name', 'str')) {
            $obj_nav->object_select->countAllBackRefs();

            $start = $params->get('start', 'num default|0');

            $obj_nav->object_select->loadBackRefs(
                $collection_name,
                null,
                $start . ',' . $params->get('step', 'num default|50')
            );

            return $this->renderSmarty(
                'inc_collection_items.tpl',
                [
                    "start"           => $start,
                    "counts"          => $obj_nav->object_select->_count,
                    "object_select"   => $obj_nav->object_select,
                    "back_name"       => $collection_name,
                    "change_page_arg" => $collection_name . '_' . $params->get('form_uid', 'str'),
                ]
            );
        }

        $grouped_fields = $obj_nav->sortFields();

        $obj_nav->object_select->loadAllFwdRefs();
        $obj_nav->object_select->countAllBackRefs();

        $counts = [
            "total" => 0,
            "form"  => 0,
            "plain" => 0,
        ];
        foreach ($obj_nav->object_select->_count as $_back => $_count) {
            if ($_count > 0) {
                $counts["total"] += $_count;
                $obj_nav->object_select->loadBackRefs($_back, null, 50);
            }
        }

        $counts["plain"] = 0;
        $counts["form"]  = 0;
        foreach ($grouped_fields as $fieldset => $fields) {
            $counts["plain"] += count($fields["plain"]["fields"]) + count($fields["plain"]["refs"]);
            $counts["form"]  += count($fields["form"]["fields"]) + count($fields["form"]["refs"]);
            if (!$fields["form"]["refs"]) {
                $counts["form"] += count($fields["form"]["fields"]);
            }
        }

        return $this->renderSmarty(
            'vw_class_details.tpl',
            [
                "object_select"  => $obj_nav->object_select,
                "grouped_fields" => $grouped_fields,
                "counts"         => $counts,
                "count_obj"      => $obj_nav->object_select->_count,
                "start"          => 0,
            ]
        );
    }

    private function getSrcLogo(): string
    {
        return file_exists(dirname(__DIR__, 4) . '/images/pictures/logo_custom.png')
            ? 'images/pictures/logo_custom.png'
            : 'images/pictures/logo.png';
    }
}
