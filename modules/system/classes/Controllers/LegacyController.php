<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use LogicException;
use Ox\Core\CApp;
use Ox\Core\CAppRipException;
use Ox\Core\CAppUI;
use Ox\Core\CDoObjectAddEdit;
use Ox\Core\Controller;
use Ox\Core\CSmartyMB;
use Ox\Core\CValue;
use Ox\Core\CView;
use Ox\Core\Exceptions\AppRedirectionException;
use Ox\Core\Kernel\Exception\ControllerStoppedException;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Etablissement\CGroups;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Handle legacy scripts :
 * - /modules/lorem/script.php
 * - /modules/lorem/controllers/dosql.php
 * - /modules/lorem/classes/Controllers/Legacy/CLegacyController.php
 * And generic controller $_POST['@class']
 */
class LegacyController extends Controller
{
    /**
     * Bufferize the response
     *
     * @param Request $request
     *
     * @return Response
     * @throws InvalidArgumentException
     */
    public function handleRequest(Request $request): Response
    {
        CAppUI::turnOnEchoStep();

        ob_start();
        try {
            $response = $this->run($request);
        } catch (CAppRipException) {
            // Display the content and let the kernel terminate with a HTTP 200 status code.
            // Calls with CApp::rip do not check for CView::checkin calls.
            CView::$checkedin = true;
        } catch (ControllerStoppedException $e) {
            // Display the content an let the kernel terminate with a custom status code
            $status_code = $e->getStatusCode();
        } catch (AppRedirectionException $e) {
            // Calls with CApp::redirect do not check for CView::checkin calls.
            CView::$checkedin = true;
            // Must close the buffer to terminate correctly the process
            ob_end_clean();

            return $this->redirect($e->getParameters());
        }

        $body = ob_get_clean();

        return $response ?? new Response($body, $status_code ?? Response::HTTP_OK);
    }

    /**
     * Old index.php procedural front-controller
     *
     * @param Request $request
     *
     * @return Response|null
     * @throws InvalidArgumentException
     * @throws CAppRipException
     * @throws AppRedirectionException
     */
    private function run(Request $request): ?Response
    {
        // Declare all globals
        global $m, $tab, $can, $index, $wsdl, $dialog, $ajax, $a, $g, $f, $suppressHeaders, $class, $dosql;

        // Default view
        $index = "index";
        $tab   = 1;
        $m     = $m_get = $request->query->get("m");

        // Don't output anything. Usefull for fileviewers, ajax requests, exports, etc.
        $suppressHeaders = $request->get("suppressHeaders");

        // WSDL if often stated as final with no value (&wsdl) wrt client compat
        $wsdl = $request->get("wsdl");
        if (isset($wsdl)) {
            $suppressHeaders = 1;
            $index           = $wsdl;
            $wsdl            = 1;
        }

        // Output the charset header in case of an ajax request
        if ($ajax = $request->get("ajax")) {
            $suppressHeaders = 1;
            $index           = $ajax;
            $ajax            = 1;
        }

        // Raw output for export purposes
        if ($raw = $request->get("raw")) {
            $suppressHeaders = 1;
            $index           = $raw;
        }

        // Check if we are in the dialog mode
        if ($dialog = $request->get("dialog")) {
            $index  = $dialog;
            $dialog = 1;
        }

        if ($request->isMethod('POST')) {
            $m = $request->request->get("m") ?: $m;
        }

        $m     = CAppUI::checkFileName($m);
        $class = CAppUI::checkFileName($request->request->get("@class", ""));
        if (null == $m) {
            if (!$class) {
                return $this->redirectToDefaultPage($request);
            }

            $m = $this->getDefaultModule($request);
        }

        // If user has no perms, even the first default module cannot be reached.
        if ((null == $m) || ('' === $m)) {
            throw $this->createAccessDeniedException();
        }

        if (null == $module = CModule::getInstalled($m)) {
            // dP remover super hack
            if (null == $module = CModule::getInstalled("dP$m")) {
                CAppUI::redirect('m=system&a=module_missing&mod=' . $m);
            }
            $m = "dP$m";
        }

        // Get current module permissions
        // these can be further modified by the included action files
        $can = $module->canDo();

        $a     = CAppUI::checkFileName($request->query->get("a", $index));
        $dosql = CAppUI::checkFileName($request->request->get("dosql", ""));

        $tab = $request->query->get('tab');

        // Set the group to use.
        // Must check in session to ensure the right group is retrieved.
        $g = CValue::getFromLegacySession('g', CAppUI::$instance->user_group);

        CApp::getSessionHelper()->set('g', $g);

        $indexGroup = CGroups::get($g);
        if ($indexGroup && !$indexGroup->getPerm(PERM_READ)) {
            $g = CAppUI::$instance->user_group;
            CApp::getSessionHelper()->set('g', $g);
        }

        try {
            // Set the function in use.
            // Must check in the session's legacy bridge to ensure the right function is retrieved.
            $f = CValue::getFromLegacySession('f', $this->getCMediuser()->function_id);
        } catch (LogicException) {
            // Probably only a CUser
        }

        CApp::getSessionHelper()->set('f', $f);

        CSmartyMB::setTokenManager($this->container->get('security.csrf.token_manager'));


        // do some db work if dosql is set
        if ($dosql) {
            // controller in controllers/ directory
            if (is_file("./modules/$m/controllers/$dosql.php")) {
                include "./modules/$m/controllers/$dosql.php";
            } elseif ($controller = $module->matchLegacyController($dosql)) {
                // or in CLegacyController
                $controller->$dosql();
            }

            // If the controller did not rip, continue the script execution.
        }

        // Permissions checked on POST $m, but we redirect to GET $m
        if ($request->isMethod('POST') && $m_get && ($m != $m_get) && ($m != "dP$m_get")) {
            $m = $m_get;
        }

        // Generic controller
        if ($class && !CAppUI::isPatient()) {
            $do = new CDoObjectAddEdit($class);
            $do->doIt();

            throw new CAppRipException();
        }

        // Load module tabs (AbstractTabsRegister)
        $module->registerTabs();

        if (!$a || $a === "index") {
            $tab = $module->getValidTab($tab);
        }

        $main = new MainController();

        if (!$suppressHeaders) {
            $main->header($m, $request->getSession(), $tab, (bool)$dialog);
        }

        // tabBox et inclusion du fichier demandé
        if ($tab !== null) {
            $module->showTabs();
        } else {
            $module->showAction();
        }

        // Flush registered JS scripts
        foreach (CAppUI::getScripts() as $script) {
            echo $script;
        }

        // Unlocalized strings
        if (!$suppressHeaders || $ajax) {
            $main->unlocalized();
        }

        // Inclusion du footer (nécessaire pour la web debug toolbar)
        if (!$suppressHeaders) {
            $main->footer();
        }

        // Ajax performance, messagerie
        if ($ajax) {
            $main->ajaxErrors();
        }

        return null;
    }

    private function getDefaultModule(Request $request): ?string
    {
        $m     = CPermModule::getFirstVisibleModule();
        $parts = explode('-', $this->pref->get('DEFMODULE'), 2);

        $pref_module = $parts[0];
        if ($pref_module && CPermModule::getViewModule(CModule::getInstalled($pref_module)->mod_id, PERM_READ)) {
            $m = $pref_module;
        }

        if (count($parts) == 2) {
            $pref_action = $parts[1];
            $request->getSession()->set('tab', $pref_action);
        }

        return $m;
    }

    /**
     * Return a RedirectResponse to the current user's default page.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws Exception
     */
    private function redirectToDefaultPage(Request $request): RedirectResponse
    {
        $redirect_url = $this->getCUser()->getDefaultPageLink(
            $request->getBasePath(),
            UrlGeneratorInterface::RELATIVE_PATH
        );
        $redirect_url = ($redirect_url) ?: '/'; // In case of no preference (even by default).

        return $this->redirect($redirect_url);
    }
}
