<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Api\Utility\FilterableTrait;
use Ox\Core\Cache;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbPath;
use Ox\Core\CMbString;
use Ox\Core\Controller;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Mediboard\System\CCSVImportTranslations;
use Ox\Mediboard\System\CTranslationOverwrite;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class LocalesController extends Controller
{
    use FilterableTrait;

    private const NO_MODULE_NAME = 'core';

    /**
     * @throws ApiException
     * @api
     */
    public function listLocales(string $language, string $mod_name, RequestApi $request_api): Response
    {
        $locales = $this->loadLocalesFiles($language, $mod_name);

        $overwrite = new CTranslationOverwrite();
        if ($overwrite->isInstalled()) {
            $locales = $overwrite->transformLocales($locales, $language);
        }

        $locales = $this->applyFilter($request_api, $locales);

        $locales = $this->sanitize($locales);

        $resource = new Item($locales);
        $resource->setType('locales');

        return $this->renderApiResponse($resource);
    }

    private function loadLocalesFiles(string $language, string $mod_name): array
    {
        $root_dir = $this->getRootDir();

        if ($mod_name !== self::NO_MODULE_NAME) {
            $mod_name = $this->getActiveModule($mod_name);
        }

        $files = [];
        if ($mod_name === self::NO_MODULE_NAME) {
            $files[] = $root_dir . '/locales/' . $language . '/common.php';
        } else {
            $base_path = $root_dir . '/modules/' . $mod_name . '/locales/';

            $files[] = $base_path . $language . '.php';
            $files[] = $base_path . $language . '.overload.php';
        }

        $locales = [];
        foreach ($files as $_file_path) {
            if (file_exists($_file_path)) {
                include $_file_path;
            }
        }

        return $locales;
    }

    private function sanitize(array $locales): array
    {
        foreach ($locales as &$_value) {
            $_value = trim(nl2br($_value));
        }

        return $locales;
    }

    public function index(): Response
    {
        return $this->renderSmarty('locales/view_translations.tpl', [
            'available_languages' => CAppUI::getAvailableLanguages(),
        ]);
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function list(RequestParams $params): Response
    {
        $start    = $params->get('start', 'num default|0');
        $step     = $params->get('step', 'num default|50');
        $source   = $params->get('source', 'str');
        $language = $params->get('language', 'str');


        // Export translations overwrite list
        if ($params->get('export', 'bool default|0')) {
            return $this->export();
        }

        // Get the list of translations made
        $translation = new CTranslationOverwrite();
        $ds          = $translation->getDS();

        $where = [];
        if ($source) {
            $where['source'] = $ds->prepareLike("$source%");
        }

        if ($language) {
            $where['language'] = $ds->prepare('= ?', $language);
        }

        $total            = $translation->countList($where);
        $translations_bdd = $translation->loadList($where, null, "$start,$step");

        //load old locales
        $locale = $this->pref->get("LOCALE", "fr");
        foreach (CAppUI::getLocaleFilesPaths($locale) as $_path) {
            include $_path;
        }

        $locales = CMbString::filterEmpty($locales);
        foreach ($locales as &$_locale) {
            $_locale = CMbString::unslash($_locale);
        }

        /** @var CTranslationOverwrite[] $translations_bdd */
        foreach ($translations_bdd as $_translation) {
            $_translation->loadOldTranslation($locales);
            $_translation->checkInCache();
        }

        return $this->renderSmarty(
            'locales/inc_vw_translations.tpl',
            [
                'translations_bdd'    => $translations_bdd,
                'start'               => $start,
                'step'                => $step,
                'total'               => $total,
                'source'              => $source,
                'language'            => $language,
                'available_languages' => CAppUI::getAvailableLanguages(),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function viewEditForm(RequestParams $params): Response
    {
        $translation_id = $params->get('trad_id', "ref class|CTranslationOverwrite", PERM_EDIT);
        $language       = $params->get('language', 'str default|' . $this->pref->get("LOCALE", "fr"));
        $source         = $params->get('source', 'str');

        if ($translation_id) {
            $translation = CTranslationOverwrite::findOrFail($translation_id);
            $translation->loadOldTranslation();
        } else {
            $translation = new CTranslationOverwrite();
        }

        if ($source) {
            $translation->source           = $source;
            $translation->_old_translation = $this->translator->tr($source);
            $translation->loadMatchingObjectEsc();
        }

        return $this->renderSmarty('locales/inc_edit_translation.tpl', [
            'translation' => $translation,
            'language'    => $language,
            'languages'   => CAppUI::getAvailableLanguages(),
        ]);
    }

    /**
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public function edit(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('edit_translations', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $translation_id = $params->post('translation_id', 'ref class|CTranslationOverwrite', PERM_EDIT);
        $del            = $params->post('del', 'bool default|0');
        $language       = $params->post('language', 'str' . ($translation_id ? '' : ' notNull'));

        if ($translation_id) {
            $translation = CTranslationOverwrite::findOrFail($translation_id);
        } else {
            $translation = new CTranslationOverwrite();
        }

        if ($del && $translation_id) {
            return $this->deleteObjectAndReturnEmptyResponse($translation);
        }

        // Get params
        $translation->language    = $language;
        $translation->source      = $params->post('source', 'str' . ($translation_id ? '' : ' notNull'));
        $translation->translation = $params->post('translation', 'text' . ($translation_id ? '' : ' notNull'));

        // Unset locale cache
        Cache::deleteKeys(Cache::OUTER, "locales-{$language}-");

        return $this->storeObjectAndReturnEmptyResponse($translation, !$translation->_id);
    }

    public function viewImportForm(): Response
    {
        return $this->renderSmarty('locales/vw_import_translations.tpl');
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function import(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('system_gui_locales_import', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if ($trads = $params->post('translations', 'str')) {
            if (!$trads) {
                $this->addUiMsgError('CTranslationOverwrite.none', true);

                return $this->renderEmptyResponse();
            }

            $trads = json_decode(mb_convert_encoding(stripcslashes($trads), 'UTF-8', 'ISO-8859-1'));

            foreach ($trads as $_trad) {
                $overwrite_trad           = new CTranslationOverwrite();
                $overwrite_trad->language = $_trad->lang;
                $overwrite_trad->source   = mb_convert_encoding($_trad->key, 'ISO-8859-1', 'UTF-8');

                $overwrite_trad->loadMatchingObjectEsc();

                $overwrite_trad->translation = mb_convert_encoding($_trad->trad, 'ISO-8859-1', 'UTF-8');

                if ($msg = $overwrite_trad->store()) {
                    $this->addUiMsgWarning($msg, true);
                } else {
                    $this->addUiMsgOk(
                        "CTranslationOverwrite-msg-" . ($overwrite_trad->_id ? 'modify' : 'create'),
                        true
                    );
                }
            }

            return $this->renderEmptyResponse();
        }

        try {
            /** @var UploadedFile $file */
            $file = $params->getRequest()->files->get('formfile')[0];
            if (!$file) {
                $this->addUiMsgWarning($this->translator->tr('common-error-No file found.'));

                return $this->renderEmptyResponse();
            }
        } catch (Exception $e) {
            $this->addUiMsgWarning($this->translator->tr('common-error-No file found.'));

            return $this->renderEmptyResponse();
        }

        $filename = rtrim($this->getRootDir(), '/\\') . "/tmp/{$file->getFilename()}";

        move_uploaded_file($file->getPathname(), $filename);

        $import = new CCSVImportTranslations($filename);
        $import->parseFile();

        $modules = array_keys(CModule::getInstalled());
        sort($modules);

        CMbPath::remove($filename);

        return $this->renderSmarty(
            'locales/inc_vw_import_translations.tpl',
            [
                'translations' => $import->getTranslations(),
                'modules'      => $modules,
                'file_name'    => basename($filename),
                'counts'       => $import->getNbHitsTotal(),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function purge(RequestParams $params): Response
    {
        $start = $params->post('start', 'num default|0');
        $step  = $params->post('step', 'num default|50');

        if (!$this->isCsrfTokenValid('system_gui_locales_purge', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $locales = CAppUI::getLocalesFromFiles();

        $trad  = new CTranslationOverwrite();
        $trads = $trad->loadList(null, null, "$start,$step");

        $trads_ids = [];
        /** @var CTranslationOverwrite $_trad */
        foreach ($trads as $_trad) {
            $_trad->loadOldTranslation($locales);

            if ($_trad->_old_translation == $_trad->translation) {
                $trads_ids[] = $_trad->_id;
            }
        }

        if ($trads_ids) {
            if ($msg = $trad->deleteAll($trads_ids)) {
                $this->addUiMsgError($msg, true);
            } else {
                $this->addUiMsgOk('CTraductionOverwrite-msg-delete-%d', true, count($trads_ids));
            }
        } else {
            $this->addUiMsgOk('CTraductionOverwrite-msg-nothing to purge', true);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception|InvalidArgumentException
     */
    public function autocomplete(RequestParams $params): Response
    {
        $keyword = $params->get('source', 'str default|%%');

        $trans = [];
        foreach (CAppUI::flattenCachedLocales(CAppUI::$lang) as $key => $val) {
            if (stripos($key, $keyword) !== false) {
                $trans[$key]["key"] = str_replace(
                    $keyword,
                    '<span style="text-decoration: underline;">' . $keyword . '</span>',
                    $key
                );
                $trans[$key]["val"] = str_replace(
                    $keyword,
                    '<span style="text-decoration: underline;">' . $keyword . '</span>',
                    $val
                );
            }
            if (stripos($val, $keyword) !== false) {
                $trans[$key]["key"] = str_replace(
                    $keyword,
                    '<span style="text-decoration: underline;">' . $keyword . '</span>',
                    $key
                );
                $trans[$key]["val"] = str_replace(
                    $keyword,
                    '<span style="text-decoration: underline;">' . $keyword . '</span>',
                    $val
                );
            }
        }

        return $this->renderSmarty('locales/inc_translation_autocomplete.tpl', [
            'trad' => $trans,
        ]);
    }

    /**
     * @throws Exception
     */
    private function export(): Response
    {
        $fields = [
            $this->translator->tr('CTranslationOverwrite-source'),
            $this->translator->tr('CTranslationOverwrite-_old_translation'),
            $this->translator->tr('CTranslationOverwrite-translation'),
            $this->translator->tr('CTranslationOverwrite-language'),
        ];

        $csv = new CCSVFile(null, CCSVFile::PROFILE_EXCEL);
        $csv->writeLine($fields);

        foreach ((new CTranslationOverwrite())->loadList() as $_translation) {
            $_translation->loadOldTranslation();

            $csv->writeLine(
                [
                    $_translation->source,
                    $_translation->_old_translation,
                    $_translation->translation,
                    $_translation->language,
                ]
            );
        }

        $date = CMbDT::date();

        return $this->renderFileResponse($csv->getContent(), "export-translations-{$date}.csv", 'application/csv');
    }
}
