<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\CApp;
use Ox\Core\Controller;
use Ox\Core\CSetup;
use Ox\Core\CSQLDataSource;
use Ox\Core\Elastic\ElasticIndexManager;
use Ox\Core\Elastic\ElasticObject;
use Ox\Core\Elastic\ElasticObjectManager;
use Ox\Core\Elastic\Exceptions\ElasticException;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class DataSourceController extends Controller
{
    /**
     * Gather datasource from setup dependencies
     * @throws Exception
     */
    public function index(): Response
    {
        // SQL Datasources
        $datasource_configs = $this->conf->get("db");

        $datasources     = [];
        $elastic_indices = [];
        foreach (CApp::getChildClasses(CSetup::class) as $setupClass) {
            if (!class_exists($setupClass)) {
                continue;
            }

            /** @var CSetup $setup */
            $setup = new $setupClass();

            if (count($setup->datasources)) {
                if (!isset($datasources[$setup->mod_name])) {
                    $datasources[$setup->mod_name] = [];
                }

                foreach ($setup->datasources as $_datasource => $_query) {
                    $datasources[$setup->mod_name][] = $_datasource;
                    unset($datasource_configs[$_datasource]);
                }
            }

            if (count($setup->getElasticDependencies())) {
                if (!isset($elastic_indices[$setup->mod_name])) {
                    $elastic_indices[$setup->mod_name] = [];
                }

                foreach ($setup->getElasticDependencies() as $_datasource) {
                    $elastic_indices[$setup->mod_name][] = $_datasource;
                }
            }
        }


        $datasources["_other_"] = array_keys($datasource_configs);

        return $this->renderSmarty(
            'datasources/vw_datasources.tpl',
            [
                "datasources"         => $datasources,
                "elastic_datasources" => $elastic_indices,
            ]
        );
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function viewEditForm(RequestParams $params): Response
    {
        $type = $params->get("type", "enum list|sql|nosql notNull");
        $dsn  = $params->get("dsn", "str notNull");

        return $this->renderSmarty(
            'datasources/' . (($type === 'sql') ? 'inc_configure_dsn.tpl' : 'inc_configure_elastic_dsn.tpl'),
            [
                'ds'      => CSQLDataSource::get($dsn, true),
                'dsn'     => $dsn,
                'engines' => CSQLDataSource::$engines,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function show(RequestParams $params): Response
    {
        $type = $params->get("type", "enum list|sql|nosql notNull");

        return $this->renderSmarty(
            'datasources/' . (($type === 'sql') ? 'inc_view_dsn.tpl' : 'inc_view_elastic_dsn.tpl'),
            [
                "dsn"     => $params->get("dsn", "str notNull"),
                "dsn_uid" => $params->get("dsn_uid", "str"),
            ]
        );
    }

    /**
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function test(RequestParams $params, CacheInterface $outer_cache): Response
    {
        $type = $params->get("type", "enum list|sql|nosql notNull");
        $dsn  = $params->get("dsn", "str notNull");

        if (!$dsn) {
            $this->addUiMsgError('CSQLDataSource-msg-No DSN specified');

            return $this->renderEmptyResponse();
        }

        if ($type === 'sql') {
            $outer_cache->delete("ds_metadata-{$dsn}");

            $hosts = [];
            if (!($ds = @CSQLDataSource::get($dsn, true))) {
                $db_host = $this->conf->get("db {$dsn} dbhost");
                $hosts   = preg_split('/\s*,\s*/', $db_host);
            }

            return $this->renderSmarty(
                'datasources/inc_dsn_status.tpl',
                [
                    "ds"      => $ds,
                    "dsn"     => $dsn,
                    "section" => 'db',
                    "hosts"   => $hosts,
                ]
            );
        }

        // Open modal
        if ($type === 'nosql') {
            $module = $params->get("module", "str notNull");
            $outer_cache->delete("ds_metadata-{$dsn}");

            $ds = ElasticIndexManager::get($dsn);

            $hosts = $ds->getConfig()->getConnectionParams();

            $elastic_object = $this->getElasticObjectFromCSetupModuleAndDsn($module, $dsn);
            $status         = $ds->getStatus($elastic_object);

            return $this->renderSmarty(
                'datasources/inc_elastic_dsn_status.tpl',
                [
                    "ds"      => $ds,
                    "dsn"     => $dsn,
                    "status"  => $status,
                    "module"  => $module,
                    "section" => "elastic",
                    "hosts"   => $hosts,
                ]
            );
        }

        return $this->renderEmptyResponse(Response::HTTP_NO_CONTENT);
    }

    private function getElasticObjectFromCSetupModuleAndDsn(string $module, string $dsn): ElasticObject
    {
        $setup_class = CSetup::getCSetupClass($module);

        /** @var CSetup $setup */
        $setup          = new $setup_class();
        $elastic_object = $setup->getElasticClassForDsn($dsn);
        if ($elastic_object === null) {
            trigger_error(
                $this->translator->tr(
                    "CDataSourceController-error-Did not find the specific class for the dsn (%s) in module (%s)",
                    $dsn,
                    $module
                )
            );
        }

        return $elastic_object;
    }

    /**
     * @throws Exception
     */
    public function viewEditDbForm(RequestParams $params): Response
    {
        if (!($dsn = $params->get('dsn', 'str'))) {
            $this->addUiMsgWarning('Aucun DSN sp�cifi�');

            return $this->renderEmptyResponse();
        }

        if (!($host = $params->get('host', 'str'))) {
            $this->addUiMsgWarning('Aucun h�te sp�cifi�');

            return $this->renderEmptyResponse();
        }

        return $this->renderSmarty(
            'datasources/inc_create_db.tpl',
            [
                'host' => $host,
                'dsn'  => $dsn,
            ]
        );
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function editDb(RequestParams $params): Response
    {
        $dsn         = $params->post("dsn", "str notNull");
        $master_user = $params->post("master_user", "str notNull");
        $master_pass = $params->post("master_pass", "str");
        $master_host = $params->post("master_host", "str");

        // Check params
        if (!$dsn) {
            $this->addUiMsgError("Aucun DSN sp�cifi�");
        }

        if (!$this->isCsrfTokenValid('system_gui_datasources_db_edit', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if (!isset($this->conf->get('db')['ASIP'])) {
            $this->addUiMsgError("Configuration pour le DSN '{$dsn}' inexistante");
        }

        $dsConfig =& $GLOBALS['dPconfig']["db"][$dsn];
        if (strpos($dsConfig["dbtype"], "mysql") === false) {
            $this->addUiMsgError("Seules les DSN MySQL peuvent �tre cr�es par un acc�s administrateur");
        }

        // Substitute admin access
        $user = $dsConfig["dbuser"];
        $pass = $dsConfig["dbpass"];
        $name = $dsConfig["dbname"];
        $host = $dsConfig["dbhost"];

        $dsConfig["dbuser"] = $master_user;
        $dsConfig["dbpass"] = $master_pass;
        $dsConfig["dbhost"] = $master_host;
        $dsConfig["dbname"] = "";

        if (!($ds = CSQLDataSource::get($dsn, true))) {
            $this->addUiMsgError("Connexion en tant qu'administrateur �chou�e");

            return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->addUiMsgOk("Connexion en tant qu'administrateur r�ussie");

        $client_host = "localhost";
        if (!in_array($host, ["127.0.0.1", "localhost"])) {
            $client_host = $params->getRequest()->server->get('SERVER_ADDR');
        }

        foreach ($ds->queriesForDSN($user, $pass, $name, $client_host) as $key => $query) {
            if (!$ds->exec($query)) {
                $this->addUiMsgWarning("Requ�te '{$key}' �chou�e");
                continue;
            }

            $this->addUiMsgOk("Requ�te '{$key}' effectu�e");
        }

        return $this->renderEmptyResponse();
    }


    /**
     * @throws Exception
     */
    public function initElastic(RequestParams $params): Response
    {
        $dsn          = $params->get("dsn", "str notNull");
        $setup_module = $params->get("setup_module", "str notNull");

        $elastic_object = $this->getElasticObjectFromCSetupModuleAndDsn($setup_module, $dsn);

        try {
            ElasticObjectManager::init($elastic_object);
        } catch (ElasticException $e) {
            $this->addUiMsgError("ElasticIndexManager-error-Could not initialize the Elasticsearch index", true);
        }

        return $this->renderEmptyResponse();
    }
}
