<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiRequestException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\System\CNote;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * CRUD for CNote items
 */
class NoteController extends Controller
{
    /**
     * @throws InvalidArgumentException
     * @throws ApiRequestException
     * @throws Exception
     * @api
     *
     * List the notes for the chosen context.
     * Permissions are checked on context and each note.
     *
     */
    public function list(
        string $resource_type,
        string $resource_id,
        RequestApi $request,
        RouterInterface $router
    ): Response
    {
        $context = $this->getObjectFromResourceTypeAndId($resource_type, $resource_id);
        $context->needsRead();

        $filters = $request->getFilterAsSQL($context->getDS());

        $available_notes = $context->loadBackRefs(
                   'notes',
                   $request->getSortAsSql(),
                   $request->getLimitAsSql(),
            where: $filters
        );

        /** @var CPermObject[] $perms */
        $perms = CStoredObject::massLoadBackRefs($available_notes, 'permissions');
        $notes = [];
        foreach ($available_notes as $note) {
            if ($note->getPerm(CPermObject::READ)) {
                $note->_fwd['object_id'] = $context;
                $notes[$note->_id]       = $note;
            }
        }

        // Remove the relation context from the asked relations because they are already massloaded
        $this->massLoadRelations($notes, array_diff($request->getRelations(), [CNote::RELATION_CONTEXT]), $perms);

        $collection = Collection::createFromRequest($request, $notes);
        $collection->createLinksPagination(
            $request->getOffset(),
            $request->getLimit(),
            $context->countBackRefs('notes', $filters)
        );

        /** @var Item $item */
        foreach ($collection as $item) {
            /** @var CNote $note */
            $note = $item->getDatas();
            if ($note->getPerm(CPermObject::EDIT)) {
                $item->addLinks(['edit' => $note->getApiLink($router)]);
            }
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * @api
     *
     * Display a single note.
     */
    public function show(CNote $note, RequestApi $request, RouterInterface $router): Response
    {
        $item = Item::createFromRequest($request, $note);
        if ($note->getPerm(CPermObject::EDIT)) {
            $item->addLinks(['edit' => $note->getApiLink($router)]);
        }

        return $this->renderApiResponse($item);
    }

    /**
     * @api
     *
     * Create one or multiple notes.
     */
    public function create(RequestApi $request): Response
    {
        $notes = $request->getModelObjectCollection(CNote::class);

        /** @var CNote $note */
        foreach ($notes as $note) {
            if (!$note->user_id) {
                $note->user_id = $this->getCMediuser()->_id;
            }
        }

        $notes = $this->storeCollection($notes, false);
        $this->massLoadRelations($notes, $request->getRelations());

        return $this->renderApiResponse(Collection::createFromRequest($request, $notes), Response::HTTP_CREATED);
    }

    /**
     * @api
     *
     * Update a note.
     */
    public function update(CNote $note, RequestApi $request): Response
    {
        /** @var CNote $note_from_request */
        $note_from_request = $request->getModelObject($note);

        $note = $this->storeObject($note_from_request, false);

        return $this->renderApiResponse(Item::createFromRequest($request, $note));
    }

    /**
     * @api
     *
     * Delete the note.
     */
    public function delete(CNote $note): Response
    {
        $this->deleteObject($note);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Massload the relations user_id and/or object_id from the notes.
     *
     * @param CNote[]       $notes
     * @param array         $relations
     * @param CPermObject[] $permissions
     *
     * @return void
     * @throws Exception
     */
    private function massLoadRelations(array $notes, array $relations, array $permissions = []): void
    {
        if (in_array(RequestRelations::QUERY_KEYWORD_ALL, $relations)) {
            CStoredObject::massLoadFwdRef($notes, 'user_id');
            CStoredObject::massLoadFwdRef($notes, 'object_id');
            CStoredObject::massLoadFwdRef($permissions, 'user_id');

            return;
        }

        if (in_array(CNote::RELATION_AUTHOR, $relations)) {
            CStoredObject::massLoadFwdRef($notes, 'user_id');
        }

        if (in_array(CNote::RELATION_CONTEXT, $relations)) {
            CStoredObject::massLoadFwdRef($notes, 'object_id');
        }

        if (in_array(CNote::RELATION_AUTHORIZED_USERS, $relations)) {
            if (!$permissions) {
                $permissions = CStoredObject::massLoadBackRefs($notes, 'permissions');
            }

            CStoredObject::massLoadFwdRef($permissions, 'user_id');
        }
    }
}
