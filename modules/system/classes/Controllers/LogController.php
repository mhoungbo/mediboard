<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\CMbArray;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\System\CModuleAction;
use Ox\Mediboard\System\Log\CLongRequestLog;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class LogController extends Controller
{
    /**
     * @throws Exception|InvalidArgumentException
     */
    public function deleteLongRequestLog(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('system_gui_long_request_log_delete', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if ($params->post('purge', 'bool')) {
            return $this->purgeLongRequestLog();
        }

        $_datetime_start_min = $params->post('_datetime_start_min', 'dateTime');
        $_datetime_start_max = $params->post('_datetime_start_max', 'dateTime');
        $_datetime_end_min   = $params->post('_datetime_end_min', 'dateTime');
        $_datetime_end_max   = $params->post('_datetime_end_max', 'dateTime');
        $user_id             = $params->post('user_id', 'ref class|CMediusers');
        $duration            = $params->post('duration', 'float');
        $duration_operand    = $params->post('duration_operand', 'str');
        $module              = $params->post('filter_module', 'str');
        $module_action_id    = $params->post('module_action_id', 'str');
        $purge_limit         = $params->post('limit', 'str');

        $ds    = CSQLDataSource::get('std');
        $where = [];

        if ($_datetime_start_min) {
            $where[] = $ds->prepare('`datetime_start` >= ?', $_datetime_start_min);
        }

        if ($_datetime_start_max) {
            $where[] = $ds->prepare('`datetime_start` <= ?', $_datetime_start_max);
        }

        if ($_datetime_end_min) {
            $where[] = $ds->prepare('`datetime_end` >= ?', $_datetime_end_min);
        }

        if ($_datetime_end_max) {
            $where[] = $ds->prepare('`datetime_end` <= ?', $_datetime_end_max);
        }

        if ($user_id) {
            $where['user_id'] = $ds->prepare('= ?', $user_id);
        }

        if ($duration && in_array($duration_operand, ['<', '<=', '=', '>', '>='])) {
            $where['duration'] = $ds->prepare("$duration_operand ?", $duration);
        }

        if ($module_action_id) {
            $where['module_action_id'] = $ds->prepare("= ?", $module_action_id);
        } elseif ($module) {
            $module_action         = new CModuleAction();
            $module_action->module = $module;

            $module_actions = $module_action->loadMatchingListEsc();
            if ($module_actions) {
                $where['module_action_id'] = $ds->prepareIn(CMbArray::pluck($module_actions, '_id'));
            }
        }

        $log      = new CLongRequestLog();
        $logs_ids = $log->loadIds($where, 'datetime_start ASC', $purge_limit);

        if (!$logs_ids) {
            $this->addUiMsgWarning("CLongRequestLog-msg-No CLongRequestLog to be removed.", true);

            return $this->renderEmptyResponse();
        }

        if ($msg = $log->deleteAll($logs_ids)) {
            $this->addUiMsgError($msg, true);
        } else {
            $this->addUiMsgOk('CLongRequestLog-msg-%s rows deleted', true, count($logs_ids));
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    public function purgeLongRequestLog(): Response
    {
        $log      = new CLongRequestLog();
        $logs_ids = $log->loadIds();

        if (!$logs_ids) {
            $this->addUiMsgWarning("CLongRequestLog-msg-No CLongRequestLog to be removed.", true);

            return $this->renderEmptyResponse();
        }

        if ($msg = $log->deleteAll($logs_ids)) {
            $this->addUiMsgError($msg, true);
        } else {
            $this->addUiMsgOk('CLongRequestLog-msg-%s rows deleted', true, count($logs_ids));
        }

        return $this->renderEmptyResponse();
    }
}
