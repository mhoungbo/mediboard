<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\System\CModuleAction;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that handle module actions
 */
class ModuleActionController extends Controller
{
    public function viewSearch(RequestParams $params): Response
    {
        return $this->renderSmarty(
            'vw_module_action_search',
            [
                'element_id' => $params->get('element_id', 'str notNull'),
            ]
        );
    }

    public function search(RequestParams $params): Response
    {
        $search     = $params->get('search', 'str notNull');
        $element_id = $params->get('element_id', 'str notNull');

        $module_action  = new CModuleAction();
        $module_actions = $module_action->seek($search, [], 30);

        return $this->renderSmarty(
            'inc_module_actions_list',
            [
                'module_actions' => $module_actions,
                'element_id'     => $element_id,
            ]
        );
    }
}
