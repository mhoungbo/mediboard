<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use DOMException;
use Exception;
use Ox\Core\Cache;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Mediboard\System\CConfigurationCompare;
use Ox\Mediboard\System\Configurations\ConfigSearchService;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ConfigSearchController extends Controller
{
    public function index(): Response
    {
        return $this->renderSmarty('config_search/vw_config_info.tpl');
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function list(ConfigSearchService $config_search): Response
    {
        $config_search = $config_search->initFromRequestParams();

        if (!($keywords = $config_search->getKeywords())) {
            $this->addUiMsgError('system-search-configs no keywords');

            return $this->renderEmptyResponse();
        }

        return $this->renderSmarty(
            'config_search/inc_search_configs.tpl',
            [
                'keywords' => $keywords,
                'filtered' => $config_search->list(),
                'start'    => $config_search->getStart(),
                'step'     => $config_search->getStep(),
                'total'    => $config_search->getTotal(),
            ]
        );
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function translations(RequestParams $params, ConfigSearchService $config_search): Response
    {
        if ($params->getRequest()->getMethod() === Request::METHOD_GET) {
            return $this->viewTranslations($params, $config_search);
        }

        if (!$this->isCsrfTokenValid('edit_config_translations', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        return $this->editTranslations($params, $config_search);
    }

    /**
     * @throws Exception
     */
    private function viewTranslations(RequestParams $params, ConfigSearchService $config_search): Response
    {
        if (!($feature = $params->get('feature', 'str'))) {
            $this->addUiMsgError('system-config-translations feature mandatory');

            return $this->renderEmptyResponse();
        }

        $feature = str_replace(' ', '-', $feature);
        $module  = explode('-', $feature)[1] ?? '';

        if (!$this->checkModule($module)) {
            return $this->renderEmptyResponse();
        }

        $language = $this->translator->getCurrentLocale();
        $locales  = CAppUI::getLocalesFromFiles();

        return $this->renderSmarty(
            'config_search/vw_translations_config.tpl',
            [
                'feature'        => $feature,
                'trans_bdd'      => $config_search->getStoredTranslation($feature, $language, $locales),
                'trans_bdd_desc' => $config_search->getStoredTranslation($feature, $language, $locales, true),
            ]
        );
    }

    private function checkModule(string $module): bool
    {
        if (!$module) {
            $this->addUiMsgError('system-search-configs no module passed');

            return false;
        }

        /** @var CModule $module */
        $module = CModule::getInstalled($module);
        if (($module instanceof CModule) && !$module->canDo()->admin) {
            $this->addUiMsgError('system-search-configs need admin perm');

            return false;
        }

        return true;
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    private function editTranslations(RequestParams $params, ConfigSearchService $config_search): Response
    {
        $translation      = $params->post('translation', 'str');
        $translation_desc = $params->post('translation_desc', 'str');

        if (!($feature = $params->post('feature', 'str'))) {
            $this->addUiMsgError('system-config-translations feature mandatory');

            return $this->renderEmptyResponse();
        }

        $feature_split = explode('-', $feature);
        if (!$feature_split[0] == 'config') {
            $this->addUiMsgError('system-config-translations not config feature');

            return $this->renderEmptyResponse();
        }

        if (!$this->checkModule($feature_split[1] ?? '')) {
            return $this->renderEmptyResponse();
        }

        $language   = $this->translator->getCurrentLocale();
        $trans      = $config_search->getStoredTranslation($feature, $language, []);
        $trans_desc = $config_search->getStoredTranslation($feature, $language, [], true);

        try {
            if ($config_search->storeTranslation($translation, $feature, $language)) {
                $this->addUiMsgOk('CTranslationOverwrite-msg-' . (($trans->_id) ? 'modify' : 'create'), true);
            }
        } catch (Exception $e) {
            $this->addUiMsgWarning($e->getMessage(), true);
        }

        try {
            if ($config_search->storeTranslation($translation_desc, $feature, $language, true)) {
                $this->addUiMsgOk('CTranslationOverwrite-msg-' . (($trans_desc->_id) ? 'modify' : 'create'), true);
            }
        } catch (Exception $e) {
            $this->addUiMsgWarning($e->getMessage(), true);
        }

        Cache::deleteKeys(Cache::OUTER, "locales-{$language}-");

        return $this->renderEmptyResponse();
    }

    /**
     * @throws DOMException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function export(ConfigSearchService $config_search): Response
    {
        $this->enforceSlave();

        $export = $config_search->export();

        return $this->renderFileResponse($export['content'], "configs-{$export['title']}.xml", 'text/xml');
    }

    /**
     * @throws Exception
     */
    public function compare(RequestParams $params): Response
    {
        if ($params->getRequest()->getMethod() === Request::METHOD_GET) {
            return $this->renderSmarty('config_search/vw_config_compare.tpl');
        }

        if (!$this->isCsrfTokenValid('system_config_search_compare', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        /** @var UploadedFile[] $files */
        if (!($files = $params->getRequest()->files->get('formfile'))) {
            $this->addUiMsgWarning($this->translator->tr('CConfigurationCompare-error no file'));

            return $this->renderEmptyResponse();
        }

        $comparator = new CConfigurationCompare($this->translator);
        $comparator->compare($files);

        $result = $comparator->getResult();
        CMbArray::pluckSort($result, SORT_LOCALE_STRING, 'trad');

        $files_names = $comparator->getFilesNames();
        $main_file   = reset($files_names);
        unset($files_names[0]);

        return $this->renderSmarty(
            'config_search/inc_compare_configs.tpl',
            [
                'result'    => $result,
                'main_file' => $main_file,
                'files'     => $files_names,
            ]
        );
    }
}
