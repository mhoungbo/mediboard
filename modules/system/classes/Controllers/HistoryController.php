<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CApp;
use Ox\Core\CMbArray;
use Ox\Core\Config\Conf;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Exception\ControllerException;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\System\Log\UserLogService;
use Ox\Mediboard\System\Pref;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class HistoryController extends Controller
{
    private UserLogService $log_service;

    public function __construct(
        CApp           $app = null,
        Conf           $conf = new Conf(),
        Pref           $pref = new Pref(),
        Translator     $tr = new Translator(),
        UserLogService $log_service = null
    ) {
        parent::__construct($app, $conf, $pref, $tr);

        $this->log_service = $log_service ?? new UserLogService($this->conf);
    }

    /**
     * @throws Exception|InvalidArgumentException
     * @api
     */
    public function list(
        string     $resource_type,
        int        $resource_id,
        RequestApi $request
    ): Response {
        ['object' => $object, 'logs' => $logs] = $this->loadObjectWithHistory($request, $resource_type, $resource_id);

        $total        = count($logs);
        $limited_logs = array_slice($logs, $request->getOffset(), $request->getLimit(), true);
        if (
            $request->getRequest()->query->getBoolean('include_creation_log')
            && $total !== count($limited_logs)
        ) {
            $creation_log                     = end($logs);
            $limited_logs[$creation_log->_id] = $creation_log;
        }

        foreach ($limited_logs as $log) {
            $log->_fwd['object_id'] = $object;
        }

        $collection = Collection::createFromRequest($request, $limited_logs);

        if ($this->getCUser()->isAdmin() && !str_starts_with($object->_class, 'CExObject_')) {
            $collection->addLinks(
                [
                    'object_details' => $this->generateUrl(
                        'system_gui_object_details',
                        ['resource_type' => $object->_class, 'resource_id' => $object->_id]
                    ),
                ]
            );
        }

        $collection->createLinksPagination(
            $request->getOffset(),
            $request->getLimit(),
            $total
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws ApiException
     * @throws ControllerException
     * @throws InvalidArgumentException
     * @throws Exception
     * @api
     */
    public function show(
        string     $resource_type,
        int        $resource_id,
        int        $history_id,
        RequestApi $request
    ): Response {
        ['object' => $object, 'logs' => $logs] = $this->loadObjectWithHistory($request, $resource_type, $resource_id);

        if (!isset($logs[$history_id])) {
            throw new ControllerException(
                Response::HTTP_NOT_FOUND,
                'Invalid resource identifiers.',
                [],
                2
            );
        }

        $item = Item::createFromRequest($request, $logs[$history_id]);
        if (!str_starts_with($object->_class, 'CExObject_') && $this->getCUser()->isAdmin()) {
            $item->addLinks(
                [
                    'object_details' => $this->generateUrl(
                        'system_gui_object_details',
                        ['resource_type' => $object->_class, 'resource_id' => $object->_id]
                    ),
                ]
            );
        }

        return $this->renderApiResponse($item);
    }

    /**
     * Load an object using resource_type and resource_id.
     * Then load the object history and remove forbidden logs.
     *
     * @param RequestApi $request
     * @param string     $resource_type
     * @param string     $resource_id
     *
     * @return array
     * @throws InvalidArgumentException
     */
    private function loadObjectWithHistory(RequestApi $request, string $resource_type, string $resource_id): array
    {
        if (!$this->pref->get('system_show_history')) {
            throw new AccessDeniedException(
                $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
            );
        }

        $this->enforceSlave();

        $object = $this->getObjectFromResourceTypeAndId(
            $resource_type,
            $resource_id,
            $request->getRequest()->get('ex_class_id')
        );
        $object->needsRead();

        $object->loadHistory();

        $logs = $this->log_service->removeForbiddenLogs(
            $object->_ref_logs,
            $request->getGroup()->_id
        );

        return ['object' => $object, 'logs' => $logs];
    }
}
