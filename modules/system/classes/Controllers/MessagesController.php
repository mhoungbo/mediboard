<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CMbString;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Mediboard\System\CMessage;
use Ox\Mediboard\System\CMessageAcquittement;
use Ox\Mediboard\System\CSourceSMTP;
use Symfony\Component\HttpFoundation\Response;

class MessagesController extends Controller
{
    public function index(): Response
    {
        return $this->renderSmarty("messages/idx_messages.tpl");
    }

    /**
     * @throws Exception
     */
    public function list(RequestParams $params): Response
    {
        $this->enforceSlave();

        $start  = $params->get('start', 'num default|0');
        $step   = $params->get('step', 'num default|50');
        $status = $params->get('status', 'str');
        $status = $status ? explode('|', $status) : [];

        $messages = [];
        $total    = 0;
        foreach ($status as $_status) {
            $filter          = new CMessage();
            $filter->_status = $_status;

            $messages = array_merge($messages, $filter->loadMessagesList($_status, $start, $step));
            $total    += $filter->countMessagesList($_status);
        }

        if ($messages) {
            CStoredObject::massLoadBackRefs($messages, 'notes');
            CStoredObject::massLoadFwdRef($messages, 'module_id');
            CStoredObject::massLoadFwdRef($messages, 'group_id');

            foreach ($messages as $_message) {
                $_message->loadRefsNotes();
                $_message->loadRefModuleObject();
                $_message->loadRefGroup();
            }
        }

        return $this->renderSmarty(
            "messages/inc_list_messages.tpl",
            [
                "messages" => $messages,
                "total"    => $total,
                "start"    => $start,
                "step"     => $step,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function viewEditForm(RequestParams $params): Response
    {
        $message_id = $params->get("message_id", "ref class|CMessage", PERM_EDIT);

        $update_moment     = $params->get("_update_moment", "dateTime");
        $update_initiator  = $params->get("_update_initiator", "str");
        $update_benefits   = $params->get("_update_benefits", 'str');
        $is_update_message = $params->get("is_update_message", 'bool default|0');

        // Specific instance update message
        if ($is_update_message) {
            if (!$this->checkModulePermEdit()) {
                throw new AccessDeniedException(
                    $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
                );
            }

            return $this->renderSmarty("messages/inc_form_message_update.tpl", [
                'message' => new CMessage(),
            ]);
        }

        // Load Message
        if ($message_id) {
            $message = CMessage::findOrFail($message_id);
            $message->loadRefsNotes();
        } else {
            $message      = new CMessage();
            $message->deb = CMbDT::dateTime();
        }

        // Specific update message
        if ($update_moment) {
            $message->deb   = CMbDT::dateTime("-8 hours", $update_moment);
            $message->fin   = CMbDT::dateTime("+15 minutes", $update_moment);
            $message->titre = $this->translator->tr("CMessage-create_update-titre");
            $message->corps = $this->translator->tr(
                "CMessage-create_update-corps",
                CMbDT::format($update_moment, $this->conf->get("datetime"))
            );

            $details                 = $this->translator->tr(
                "CMessage-create_update-details",
                stripslashes($update_initiator),
                stripslashes($update_benefits)
            );
            $message->_email_details = CMbString::br2nl($details);
        }

        // Source SMTP
        $message->_email_from = $this->conf->get("system CMessage default_email_from");
        $message->_email_to   = $this->conf->get("system CMessage default_email_to");

        return $this->renderSmarty(
            "messages/inc_form_message.tpl",
            [
                "message"                 => $message,
                "acquittals"              => $message->loadRefAcquittals(),
                "message_smtp"            => CExchangeSource::get(
                    "system-message",
                    CSourceSMTP::TYPE,
                    true,
                    null,
                    false
                ),
                'url_groups_autocomplete' => $this->generateUrl('etablissement_groups_autocomplete'),
                'current_group'           => CGroups::findOrFail($this->request_context->getGroupId()),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function ack(RequestParams $params, CMessage $message): Response
    {
        $this->enforceSlave();

        $start = $params->get("start", "num default|0");
        $step  = $params->get("step", "num default|10");

        $acquittals = $message->loadBackRefs("acquittals", "date_ack desc, date_display desc", "{$start},{$step}");
        $total      = $message->countBackRefs("acquittals");

        $users = CStoredObject::massLoadFwdRef($acquittals, "user_id");
        CStoredObject::massLoadFwdRef($users, "function_id");

        /** @var CMessageAcquittement $_acquittal */
        foreach ($acquittals as $_acquittal) {
            $_acquittal->loadRefUser()->loadRefFunction();
        }

        return $this->renderSmarty(
            "messages/inc_acquitment_list.tpl",
            [
                "acquittals" => $acquittals,
                "message"    => $message,
                "limit"      => $start,
                "step"       => $step,
                "total"      => $total,
            ]
        );
    }
}
