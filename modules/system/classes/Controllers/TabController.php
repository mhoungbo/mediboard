<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CApp;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\HttpException;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CModuleAction;
use Ox\Mediboard\System\CPinnedTab;
use Ox\Mediboard\System\CTab;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Controller to handle tabs
 */
class TabController extends Controller
{
    public const LINK_TAB_URL      = 'tab_url';
    public const TAB_RESOURCE_TYPE = 'tab';

    /**
     * @api
     */
    public function showPinnedTabs(string $mod_name): Response
    {
        $module = CModule::getActive($mod_name);
        if ($module === null) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'The module ' . $mod_name . ' is not active.');
        }

        if (!$module->getPerm(PERM_READ)) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Access denied');
        }

        return $this->renderApiResponse(new Collection($module->getPinnedTabs()));
    }

    /**
     * @api
     */
    public function setPinnedTab(string $mod_name, RequestApi $request_api, RouterInterface $router): Response
    {
        $pins = $request_api->getModelObjectCollection(CPinnedTab::class, [], ['_tab_name']);

        $module = CModule::getActive($mod_name);
        if ($module === null) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'The module ' . $mod_name . ' is not active.');
        }

        if (!$module->getPerm(PERM_READ)) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Access denied');
        }

        $current_user = $this->getCMediuser();

        try {
            /** @var CPinnedTab $pin */
            foreach ($pins as $pin) {
                $pin->_mod_name = $mod_name;
                $is_route       = true;
                try {
                    $router->generate($pin->_tab_name);
                } catch (RouteNotFoundException) {
                    $is_route = false;
                }
                $pin->createPin($current_user, $is_route);
            }

            CPinnedTab::removePinnedTabs($mod_name, $current_user);
        } catch (CMbException $e) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        return $this->renderApiResponse($this->storeCollection($pins), 201);
    }

    /**
     * @throws ApiException
     * @api
     *
     */
    public function listModuleTabs(string $mod_name, RequestApi $request_api): Response
    {
        $module = CModule::getActive($mod_name);
        if ($module === null) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'The module ' . $mod_name . ' is not installed.');
        }

        $tabs       = $module->getTabs();
        $module_url = $module->getDefaultTab();

        $collection = Collection::createFromRequest($request_api, $tabs);
        /** @var Item $item */
        foreach ($collection as $item) {
            /** @var CTab $tab */
            $tab = $item->getDatas();
            $item->addLinks([self::LINK_TAB_URL => $tab->getUrl()]);
            $item->setType(self::TAB_RESOURCE_TYPE);
            $item->addAdditionalDatas(['is_default' => $tab->getUrl() === $module_url]);
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * @api
     */
    public function showModulesDefaultTab(): Response
    {
        $item = new Item([]);
        $item->setType('default_tabs');

        $pinned_actions = $this->getFirstPinnedTabByModule();

        // Get only visible modules
        $modules = CModule::getVisible() ?? [];
        foreach ($modules as $module) {
            // Check if the user have at least the read permission on the module and have the view permission.
            if (!$module->getPerm(PERM_READ) || !$module->canView()) {
                continue;
            }

            // If the user have at least a pinned tab for the module use it.
            if (isset($pinned_actions[$module->_id])) {
                $url = $pinned_actions[$module->_id];
            } else {
                // Load module tabs withtout the pinned tabs.
                $module->registerTabs(false);
                $url = $module->getDefaultTab();
            }

            if ($url) {
                $item->addLinks([$module->mod_name => $url]);
            }
        }

        return $this->renderApiResponse($item);
    }

    /**
     * @return array {module_id => first_pinned_tab_url}
     *
     * @throws Exception
     */
    private function getFirstPinnedTabByModule(): array
    {
        $pinned_actions = (new CPinnedTab())->loadFirstByModuleForUser(CUser::get());

        $module_actions = (new CModuleAction())->loadAll(array_values($pinned_actions));

        $first_pins = [];
        foreach ($pinned_actions as $module_id => $mod_action_id) {
            /** @var CModuleAction $mod_action */
            $mod_action = $module_actions[$mod_action_id];
            if ($mod_action->is_route) {
                $first_pins[$module_id] = CApp::generateUrl($mod_action->action);
            } else {
                $first_pins[$module_id] = "?m={$mod_action->module}&tab={$mod_action->action}";
            }
        }

        return $first_pins;
    }
}
