<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\System\ViewSender\CSourceToViewSender;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Eai\ExchangeSources\Accessibility\AccessibilityReportMaker;
use Ox\Interop\Eai\ExchangeSources\Accessibility\ExchangeSourceFileAccessibility;
use Ox\Mediboard\System\Sources\CSourceFile;
use Ox\Mediboard\System\ViewSender\CViewSender;
use Ox\Mediboard\System\ViewSender\CViewSenderSource;
use Ox\Mediboard\System\ViewSender\ViewSenderManager;
use Ox\Mediboard\System\ViewSender\ViewSenderRepository;
use Ox\Mediboard\System\ViewSender\ViewSenderService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\NoConfigurationException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Controller for CViewSenders, CViewSenderSource and CSourceToViewSender objects.
 */
class ViewSendersController extends Controller
{
    /**
     * @throws ApiException
     * @throws Exception
     * @api
     *
     */
    public function getViewSendersList(RequestApi $request_api): Response
    {
        $sender         = new CViewSender();
        $sender->active = 1;
        $senders        = $sender->loadMatchingListEsc("name");

        /** @var Collection $resource */
        $resource = Collection::createFromRequest($request_api, $senders);

        $resource->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            count($senders)
        );

        return $this->renderApiResponse($resource);
    }

    /**
     * Main page for managing view senders
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->renderSmarty(
            'view_senders/idx_view_senders.tpl',
            [
                'view_sender_statuses' => CViewSender::LAST_STATUSES,
                'source_statuses'      => CSourceToViewSender::LAST_STATUSES,
            ]
        );
    }

    /**
     * Display the list of view senders.
     *
     * @param RequestParams        $params
     * @param ViewSenderService    $sender_service
     * @param ViewSenderRepository $repository
     *
     * @return Response
     * @throws Exception
     */
    public function list(
        RequestParams        $params,
        ViewSenderService    $sender_service,
        ViewSenderRepository $repository
    ): Response {
        $plan_mode = $params->get("plan_mode", "enum list|production|sending default|production");

        $date_time = CMbDT::dateTime();
        $minute    = intval(CMbDT::transform($date_time, null, "%M"));
        $day       = intval(CMbDT::transform($date_time, null, "%d"));

        $senders = $repository->initFromRequestParams()->find();
        $repository->findSenderSources($senders);

        $phases = [];

        foreach ($senders as $sender) {
            if (!isset($phases[$sender->offset])) {
                $phases[$sender->offset] = 0;
            }

            $phases[$sender->offset]++;

            $sender->getActive($minute, null, $day);
            $sender->makeHourPlan($plan_mode);
            $sender->makeUrl(false);
        }

        ['hour_sum' => $hour_sum, 'total' => $hour_total] = $sender_service->computeHourlyReport($senders);

        return $this->renderSmarty(
            'view_senders/inc_list_view_senders.tpl',
            [
                "plan_mode"  => $plan_mode,
                "senders"    => $senders,
                "hour_sum"   => $hour_sum,
                "hour_total" => $hour_total,
                "date_time"  => $date_time,
                "minute"     => $minute,
                "day"        => $day,
                'phases'     => $phases,
            ]
        );
    }

    /**
     * Display the edition (create / update / delete) view for CViewSenders.
     * Using the query parameter type=link will display the edition for CSourceToViewSender.
     *
     * @param RequestParams $params
     *
     * @return Response
     * @throws Exception
     */
    public function vwEdit(RequestParams $params, ExchangeSourceFileAccessibility $accessibility): Response
    {
        $type      = $params->get('type', 'enum list|sender|link default|sender');
        $sender_id = $params->get(
            "sender_id",
            'ref class|CViewSender' . ($type === 'link' ? ' notNull' : ''),
            PERM_EDIT
        );

        $sender = CViewSender::findOrNew($sender_id);

        if ($type === 'link') {
            return $this->vwEditSourceToViewSender($sender);
        }

        if ($sender->_id) {
            $sender->loadRefsNotes();
            $sender->loadRefSendersSource();

            if ($params->get('test', 'bool default|0')) {
                return $this->testSend($sender, $accessibility);
            }

            if ($params->get('purge', 'bool default|0')) {
                return $this->purgeDirectories($sender);
            }
        }

        return $this->renderSmarty('view_senders/inc_form_view_sender', ['sender' => $sender]);
    }

    /**
     * Create, update or delete CViewSender.
     * Using the query parameter type=link allow the creation, update or deletion of CSourceToViewSender.
     *
     * @param RequestParams $params
     *
     * @return Response
     * @throws Exception
     */
    public function doEdit(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('edit_view_sender', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $del              = $params->post('del', 'bool default|0');
        $type             = $params->post('type', 'enum list|sender|link default|sender');
        $mandatory_source = ($del xor ($type === 'link'));

        $sender_id = $params->post(
            "sender_id",
            'ref class|CViewSender' . ($mandatory_source ? ' notNull' : ''),
            PERM_EDIT
        );

        $sender = CViewSender::findOrNew($sender_id);

        if ($type === 'link') {
            return $this->editOrDeleteSourceToViewSender($params, $sender, $del);
        }

        if ($del) {
            return $this->deleteObjectAndReturnEmptyResponse($sender);
        }

        $sender->name         = $params->post('name', 'str' . ($sender_id ? '' : ' notNull'));
        $sender->active       = $params->post('active', 'bool' . ($sender_id ? '' : ' notNull'));
        $sender->description  = $params->post('description', 'str');
        $sender->params       = $params->post('params', 'str');
        $sender->route_name   = $params->post('route_name', 'str');
        $sender->multipart    = $params->post('multipart', 'bool' . ($sender_id ? '' : ' notNull'));
        $sender->period       = $params->post(
            'period',
            'enum list|1|2|3|4|5|6|10|15|20|30|60' . ($sender_id ? '' : ' notNull')
        );
        $sender->offset       = $params->post('offset', 'num min|0' . ($sender_id ? '' : ' notNull'));
        $sender->every        = $params->post('every', 'enum list|1|2|3|4|6|8|12|24' . ($sender_id ? '' : ' notNull'));
        $sender->max_archives = $params->post('max_archives', 'num min|1');
        $sender->day          = $params->post('day', 'enum list|' . implode('|', range(1, 28)));

        return $this->storeObjectAndReturnEmptyResponse($sender, !$sender->_id);
    }

    /**
     * List the CViewSenderSource present on the system.
     *
     * @return Response
     * @throws Exception
     */
    public function listSources(): Response
    {
        /** @var CViewSenderSource[] $senders_source */
        $senders_source = (new CViewSenderSource())->loadList(null, "name");
        CStoredObject::massLoadFwdRef($senders_source, 'group_id');
        foreach ($senders_source as $_source) {
            $_source->loadRefGroup();
            $_source->loadRefSource();
            $_source->loadRefSenders();
        }

        return $this->renderSmarty(
            'view_senders/inc_list_view_senders_source.tpl',
            ['senders_source' => $senders_source]
        );
    }

    /**
     * Display the view to edit (create, update, delete) CViewSenderSource.
     *
     * @param RequestParams     $params
     * @param ViewSenderService $sender_service
     *
     * @return Response
     * @throws Exception
     */
    public function vwEditSource(RequestParams $params, ViewSenderService $sender_service): Response
    {
        $sender_source_id = $params->get("sender_source_id", "ref class|CViewSenderSource", PERM_EDIT);

        $sender_source = CViewSenderSource::findOrNew($sender_source_id);

        if ($sender_source->_id) {
            $sender_source->loadRefsNotes();
            $sender_source->loadRefSource();
        }

        return $this->renderSmarty(
            'view_senders/inc_form_view_sender_source.tpl',
            [
                'sender_source' => $sender_source,
                'zip_exist'     => $sender_service->isZipEnabled(),
            ]
        );
    }

    /**
     * Create, update or delete a CViewSenderSource.
     *
     * @param RequestParams $params
     *
     * @return Response
     * @throws ApiException
     */
    public function doEditSource(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('edit_view_sender_source', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $del = $params->post('del', 'bool default|0');

        $source_id = $params->post(
            'source_id',
            'ref class|CViewSenderSource' . ($del ? ' notNull' : ''),
            CPermObject::EDIT
        );

        $source = CViewSenderSource::findOrNew($source_id);
        if ($del) {
            return $this->deleteObjectAndReturnEmptyResponse($source);
        }

        $source->name     = $params->post('name', 'str' . ($source_id ? '' : ' notNull'));
        $source->actif    = $params->post('actif', 'bool' . ($source_id ? '' : ' notNull'));
        $source->libelle  = $params->post('libelle', 'str');
        $source->group_id = $params->post(
            'group_id',
            'ref class|CGroups' . ($source_id ? '' : ' notNull'),
            CPermObject::READ
        );
        $source->archive  = $params->post('archive', 'bool' . ($source_id ? '' : ' notNull'));
        $source->password = $params->post('password', 'str');

        try {
            $source = $this->storeObject($source, false);
        } catch (CMbException $e) {
            $this->addUiMsgError($e->getMessage());

            return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->addUiMsgOk('CViewSenderSource-msg-' . ($source_id ? 'modify' : 'create'), true);

        if ($params->post('callback', 'bool default|0')) {
            CAppUI::js(
                sprintf(
                    "ViewSenderSource.edit('%d', '%s')",
                    $source->_id,
                    $this->generateUrl('system_gui_view_senders_sources_view_edit')
                )
            );
        }

        return $this->renderEmptyResponse();
    }

    /**
     * Display the monitoring view of the CViewSenders execution.
     *
     * @param RequestParams        $params
     * @param ViewSenderRepository $repository
     *
     * @return Response
     * @throws Exception
     */
    public function monitor(RequestParams $params, ViewSenderRepository $repository): Response
    {
        $this->enforceSlave();

        $order_date    = $params->get('order_date', 'enum list|ASC|DESC');
        $start         = $params->get('start', 'num default|0');
        $limit         = $params->get('step', 'num default|50');
        $source_status = $params->get(
            'source_status',
            'enum list|' . implode('|', CSourceToViewSender::LAST_STATUSES)
        );

        $repository->initFromRequestParams()
                   ->setOnlyActive()
                   ->setLimit($limit)
                   ->setOffset($start);

        $senders = $repository->find();
        $total   = $repository->count();
        $repository->findSenderSources($senders, $source_status);

        return $this->renderSmarty(
            'view_senders/inc_monitor_senders.tpl',
            [
                'senders'    => $senders,
                'order_date' => $order_date,
                'order_col'  => $order_date ? 'last_datetime' : '',
                'total'      => $total,
                'start'      => $start,
                'step'       => $limit,
            ]
        );
    }

    /**
     * Display the CViewSender objects that will be executed at the current minute.
     * If the query parameter send=1 is used, execute the CViewSender for the current minute.
     * If the query parameter view_sender_id=XXX is used, execute only the CViewSender identified by view_sender_id.
     *
     * @param RequestParams $params
     *
     * @return Response
     * @throws CMbModelNotFoundException
     */
    public function send(RequestParams $params): Response
    {
        $sender_id = $params->get('view_sender_id', 'ref class|CViewSender', PERM_READ);
        if ($sender_id) {
            return $this->sendOne(CViewSender::findOrFail($sender_id));
        }

        $manager = new ViewSenderManager($params->get('send', 'bool default|0'));
        $senders = $manager->prepareAndSend();

        return $this->renderSmarty(
            'view_senders/inc_send_views.tpl',
            [
                'senders' => $senders,
                'time'    => $manager->getCurrentDateTime(),
                'minute'  => $manager->getMinute(),
            ]
        );
    }

    /**
     * Test view export
     *
     * @param CViewSender                     $sender
     * @param ExchangeSourceFileAccessibility $accessibility
     *
     * @return Response
     */
    protected function testSend(
        CViewSender                     $sender,
        ExchangeSourceFileAccessibility $accessibility
    ): Response {
        $data                       = [];
        $accessibility_report_maker = new AccessibilityReportMaker($this->translator);

        foreach ($sender->loadRefSendersSource() as $source_view) {
            /** @var CSourceFile $source */
            $source = $source_view->_ref_sender_source->_ref_source;

            if (!$source || !$source->active) {
                continue;
            }

            $path       = $sender->getPath($source);
            $raw_report = $accessibility->fullTest($source, $path);
            $report     = $accessibility_report_maker->makeReport($raw_report);

            // give route to create directory
            $create_dir_route = null;
            if ($raw_report[ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE]['status'] === false) {
                $create_dir_route = $this->generateUrl(
                    'eai_sources_files_create_directory',
                    ['source_id' => $source->_id, 'source_type' => $source::TYPE_FILE]
                );
            }

            $data[] = [
                'source'           => $source,
                'report'           => $report,
                'has_errors'       => !empty($report->getItems(CItemReport::SEVERITY_ERROR)),
                'create_dir_route' => $create_dir_route,
                'dir_path'         => $raw_report[ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE]['dir_path'],
            ];
        }

        return $this->renderSmarty('view_senders/inc_show_view_sender_test_export', ['data' => $data]);
    }

    /**
     * See an IHM to purge View Export
     *
     * @param CViewSender $sender
     *
     * @return Response
     * @throws CMbException
     */
    protected function purgeDirectories(CViewSender $sender): Response
    {
        $data = [];
        foreach ($sender->loadRefSendersSource() as $source_view) {
            /** @var CSourceFile $source */
            $source = $source_view->_ref_sender_source->_ref_source;

            if (!$source || !$source->active) {
                continue;
            }

            $client = $source->getClient();

            $directory_parts = explode('/', $sender->name);
            $root_directory  = reset($directory_parts);
            $path            = $source->completePath($root_directory);
            try {
                $directory_accessible = (bool) $client->getDirectory($path);
            } catch (Exception $exception) {
                $directory_accessible = false;
            }

            $source_data = [
                'accessible' => $directory_accessible,
                'dir_path'   => (string)$path,
                'source'     => $source,
                'error'      => null,
            ];

            if ($directory_accessible) {
                try {
                    $files       = $client->getListFiles($path);
                    $directories = $client->getListDirectory($path);
                    $directories = array_map(fn(string $dir) => rtrim($dir, '/'), $directories);

                    $elements = array_unique(array_merge($files, $directories));

                    $source_data['elements']        = $elements;
                    $source_data['purge_dir_route'] = $this->generateUrl(
                        'eai_sources_files_delete_directory',
                        ['source_id' => $source->_id, 'source_type' => $source::TYPE_FILE]
                    );
                } catch (Exception $exception) {
                    $source_data['error'] = $exception->getMessage();
                }
            }

            $data[] = $source_data;
        }

        return $this->renderSmarty('view_senders/inc_show_view_sender_purge_export', ['data' => $data]);
    }

    /**
     * Extract information about an URL.
     * If the URL correspond to a route get it's route name.
     * Return the query parameters of the URL.
     *
     * @param RequestParams     $params
     * @param ViewSenderService $sender_service
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function extractUrl(RequestParams $params, ViewSenderService $sender_service): JsonResponse
    {
        $url = $params->get('url', 'url notNull');

        try {
            $route_matched = $this->container->get('router')->match($sender_service->getRelativeUri($url));
        } catch (NoConfigurationException|ResourceNotFoundException|MethodNotAllowedException $e) {
            // Do nothing the url is a legacy one
        }

        $route_data = [
            'route_name' => $route_matched['_route'] ?? null,
            'query'      => parse_url($url, PHP_URL_QUERY),
        ];

        return new JsonResponse($route_data);
    }

    /**
     * Send a single CViewSender.
     *
     * @param CViewSender $sender
     *
     * @return Response
     */
    private function sendOne(CViewSender $sender): Response
    {
        CApp::registerShutdown(ViewSenderManager::SHUTDOWN_CLEAR_FUNCTION);

        if (!$sender->prepareAndSendFile()) {
            $this->addUiMsgWarning('CViewSender-response-empty', true);
        } else {
            $this->addUiMsgOk('CViewSender-msg-sent', true);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * Display the view to create, update, delete a CSourceToViewSender.
     * Allow to link a CViewSender to a CViewSenderSource.
     *
     * @param CViewSender $sender
     *
     * @return Response
     */
    private function vwEditSourceToViewSender(CViewSender $sender): Response
    {
        $sender->loadRefSendersSource();

        $source_to_vw_sender            = new CSourceToViewSender();
        $source_to_vw_sender->sender_id = $sender->_id;

        return $this->renderSmarty(
            'view_senders/inc_form_source_to_view_sender.tpl',
            [
                'source_to_vw_sender' => $source_to_vw_sender,
                'view_sender'         => $sender,
            ]
        );
    }

    /**
     * Create, update or delete a link between a CViewSender and a CViewSenderSource.
     *
     * @param RequestParams $params
     * @param CViewSender   $sender
     * @param bool          $del
     *
     * @return Response
     * @throws Exception
     */
    private function editOrDeleteSourceToViewSender(RequestParams $params, CViewSender $sender, bool $del): Response
    {
        $source_to_view_sender_id = $params->post(
            'source_to_view_sender_id',
            'ref class|CSourceToViewSender' . ($del ? ' notNull' : ''),
            PERM_EDIT
        );

        $source_to_view_sender = CSourceToViewSender::findOrNew($source_to_view_sender_id);
        if ($del) {
            return $this->deleteObjectAndReturnEmptyResponse($source_to_view_sender);
        }

        $source_to_view_sender->sender_id = $sender->_id;
        $source_to_view_sender->source_id = $params->post(
            'source_id',
            'ref class|CViewSenderSource notNull',
            PERM_READ
        );

        return $this->storeObjectAndReturnEmptyResponse($source_to_view_sender, !$source_to_view_sender->_id);
    }
}
