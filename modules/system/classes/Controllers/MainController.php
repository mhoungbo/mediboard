<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CSmartyDP;
use Ox\Core\CValue;
use Ox\Core\Module\CModule;
use Ox\Core\ResourceLoaders\CCSSLoader;
use Ox\Core\ResourceLoaders\CFaviconLoader;
use Ox\Core\ResourceLoaders\CJSLoader;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Core\Twig\OxExtension;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\AppBar;
use Ox\Mediboard\System\CMessage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

/**
 * Used by LegacyController and Controller
 */
class MainController
{
    public function header(
        string   $mod_name,
        ?Session $session,
        ?string  $tab,
        bool     $dialog = false,
        bool     $fetch = false,
    ) {
        //current Group
        $current_group = CGroups::loadCurrent();
        $current_user  = CUser::get();

        $module = CModule::getActive($mod_name);
        if ($module === null) {
            $module           = new CModule();
            $module->mod_name = $mod_name;
        }

        $appbar = null;
        if (!$dialog) {
            $appbar = new AppBar(
                $module,
                $current_group,
                $current_user,
                CApp::getVersion(),
                $session?->getFlashBag(),
                $tab
            );
            $appbar->build();
            CJSLoader::$additionnal_files = array_merge(CJSLoader::$additionnal_files, $appbar->getScriptsJs());
        }

        // Cannot use CSqlDataSource::hasField because of cache without TTL.
        // Must disable error_handler to avoid having multiple errors
        $messages = $current_user->_id
            ? @(new CMessage())->loadPublicationsForUser($current_user->_id, $mod_name, $current_group->_id)
            : [];

        $messages_token = null;
        if ($messages) {
            $ack_ids = [];
            foreach ($messages as $message) {
                $ack_ids[] = $message->_ref_current_ack->_id;
            }

            $messages_token = AntiCsrf::prepare()
                                      ->addParam('acquittement_msg_system_id', $ack_ids)
                                      ->addParam('date_ack')
                                      ->getToken();
        }

        $aio      = (bool)CValue::get("_aio");
        $tpl_vars = [
            // common.tpl vars
            'localeInfo'         => CAppUI::$locale_info,
            'mediboardShortIcon' => CFaviconLoader::loadFile("style/mediboard_ext/images/icons/favicon.ico"),
            'mediboardStyle'     => CCSSLoader::loadAllFiles(),
            'mediboardScript'    => CJSLoader::loadAllFiles(!$aio),
            'cp_group'           => $current_group->cp,
            "allInOne"           => $aio,
            'current_group'      => $current_group->text,

            // obsolete_module.tpl vars
            'obsolete_module'    => CModule::getObsolete($mod_name),

            // message.tpl vars
            'messages'           => $messages,
            'messages_token'     => $messages_token,

            // offline_mode.tpl vars
            // <N/A>

            // headerV2.tpl vars
            'offline'            => false,
            'errorMessage'       => $fetch ? null : CAppUI::getMsg(),
            'showInfoSystem'     => CAppUI::pref("INFOSYSTEM"),

            // header.tpl + common.tpl
            'dialog'             => $dialog,

            // Appbar view component
            'appbar'             => $appbar,
        ];

        return $this->renderSmarty('header', $tpl_vars, "style/mediboard_ext", $fetch);
    }

    public function moduleInactive()
    {
        $this->renderSmarty("module_inactive", [], "modules/system");
    }

    public function viewInfo($props, $params)
    {
        $this->renderSmarty(
            "view_info",
            [
                "props"  => $props,
                "params" => $params,
            ],
            "modules/system"
        );
    }

    public function footer(bool $fetch = false)
    {
        $tpl_vars = [
            "infosystem"         => CAppUI::pref("INFOSYSTEM"),
            "errorMessage"       => CAppUI::getMsg(),
            "multi_tab_msg_read" => CAppUI::isMultiTabMessageRead(),
        ];

        return $this->renderSmarty('footer', $tpl_vars, "style/" . CAppUI::MEDIBOARD_EXT_THEME, $fetch);
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     * @throws Exception
     */
    public function offline(string $message): Response
    {
        $root_dir = CAppUI::conf("root_dir");;
        $path      = "./images/pictures";
        $bg_custom = "./images/pictures/bg_custom.jpg";

        $vars = [
            "bg_custom"   => $bg_custom,
            "bg"          => is_file($bg_custom),
            "src_logo"    => (file_exists(
                "$root_dir/$path/logo_custom.png"
            ) ? "$path/logo_custom.png" : "$path/logo.png"),
            "message"     => $message,
            "application" => CAppUI::conf("product_name"),
        ];

        $loader = new FilesystemLoader($root_dir . '/templates/');
        $twig   = new Environment($loader);
        $twig->addExtension(new OxExtension());
        $body = $twig->render("offline.html.twig", $vars);

        $response = new Response($body, Response::HTTP_SERVICE_UNAVAILABLE);
        $response->headers->add(
            [
                "Retry-After"  => 300,
                "Content-Type" => "text/html; charset=iso-8859-1",
            ]
        );

        return $response;
    }

    public function ajaxErrors(bool $fetch = false)
    {
        return $this->renderSmarty(
            'ajax_errors',
            [
                "requestID" => CValue::get("__requestID"),
                "login_url" => 'gui/login',
            ],
            'modules/system',
            $fetch
        );
    }

    public function unlocalized(bool $fetch = false)
    {
        return $this->renderSmarty("inc_unlocalized_strings", [], 'modules/system', $fetch);
    }

    public function tabboxOpen(array $tabs, ?string $tab, bool $fetch = false)
    {
        return $this->renderSmarty(
            'tabbox',
            [
                'tabs'         => $tabs,
                'tab'          => $tab,
                'statics_tabs' => CModule::TABS,
            ],
            'style/' . CAppUI::MEDIBOARD_EXT_THEME,
            $fetch
        );
    }

    public function tabboxClose(bool $fetch = false)
    {
        if ($fetch) {
            return '</div></div>';
        }
        echo '</div></div>';
    }

    /**
     * @param string      $tpl
     * @param array       $tpl_vars
     * @param string|null $dir
     * @param bool        $fetch
     *
     * @return mixed
     */
    private function renderSmarty(string $tpl, array $tpl_vars = [], string $dir = null, bool $fetch = false)
    {
        $smarty = new CSmartyDP($dir);

        if (!empty($tpl_vars)) {
            $smarty->assign($tpl_vars);
        }

        if ($fetch) {
            return $smarty->fetch($tpl);
        }

        $smarty->display($tpl);

        return null;
    }
}
