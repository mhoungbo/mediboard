<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\System\Cron\CCronJob;
use Ox\Mediboard\System\Cron\CCronJobLog;
use Ox\Mediboard\System\Cron\CCronJobManager;
use Ox\Mediboard\System\Cron\CronjobRepository;
use Symfony\Component\HttpFoundation\Response;

class CronjobController extends Controller
{
    private const MAX_CRONJOB = 15;

    public function index(): Response
    {
        $log_cron            = new CCronJobLog();
        $log_cron->_date_min = CMbDT::dateTime('-7 DAY');
        $log_cron->_date_max = CMbDT::dateTime('+1 DAY');

        return $this->renderSmarty('cronjobs/vw_cronjob.tpl', [
            'log_cron' => $log_cron,
        ]);
    }

    /**
     * @throws Exception
     */
    public function list(RequestParams $params, CronjobRepository $repository): Response
    {
        $repository->forObject(new CCronJob())
                   ->initFromRequestParams()
                   ->setOffset($params->get('page', 'num default|0'))
                   ->setLimit(self::MAX_CRONJOB)
                   ->setOrder('cronjob_id', 'DESC');

        $cronjobs = $repository->find();

        CStoredObject::massLoadFwdRef($cronjobs, 'token_id');
        /** @var CCronJob $_cronjob */
        foreach ($cronjobs as $_cronjob) {
            $_cronjob->loadRefToken();
            $_cronjob->getNextDate();
            $_cronjob->getPreviousDate();
            $_cronjob->loadLastsStatus();
            $_cronjob->loadLastLog();
            $_cronjob->isCorrectlyExecuted();
        }

        return $this->renderSmarty(
            'cronjobs/inc_list_cronjobs.tpl',
            [
                'cronjobs'        => $cronjobs,
                'total_exchanges' => $repository->count(),
                'page_cronjob'    => $params->get('page', 'num default|0'),
                'step'            => self::MAX_CRONJOB,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function logs(RequestParams $params, CronjobRepository $repository): Response
    {
        $repository->forObject(new CCronJobLog())
                   ->initFromRequestParams()
                   ->setOffset($params->get('page', 'num default|0'))
                   ->setLimit(self::MAX_CRONJOB)
                   ->setOrder('start_datetime', 'DESC');

        $logs = $repository->find();

        CCronJobLog::massLoadFwdRef($logs, 'cronjob_id');
        /** @var CCronJobLog $_log */
        foreach ($logs as $_log) {
            $_log->loadRefCronJob();
        }

        return $this->renderSmarty(
            'cronjobs/inc_cronjobs_logs.tpl',
            [
                'logs'     => $logs,
                'nb_log'   => $repository->count(),
                'page_log' => $params->get('page', 'num default|0'),
                'step'     => self::MAX_CRONJOB,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function viewEditForm(RequestParams $params): Response
    {
        $cronjob_id = $params->get('cronjob_id', 'ref class|CCronJob');
        $list_ip    = trim($this->conf->get('servers_ip'));

        $cronjob = new CCronJob();
        $cronjob->load($cronjob_id);
        $cronjob->loadRefToken();

        return $this->renderSmarty(
            'cronjobs/inc_edit_cronjob.tpl',
            [
                'cronjob' => $cronjob,
                'address' => ($list_ip) ? preg_split("/\s*,\s*/", $list_ip, -1, PREG_SPLIT_NO_EMPTY) : [],
            ]
        );
    }

    /**
     * Create or a update a CCronJob object.
     *
     * @throws CMbException
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function edit(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('system_gui_cronjob_edit', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $del        = $params->post('del', 'bool default|0');
        $cronjob_id = $params->post('cronjob_id', 'ref class|CCronJob' . ($del ? ' notNull' : ''), PERM_EDIT);
        $cronjob    = $cronjob_id ? CCronJob::findOrFail($cronjob_id) : new CCronJob();

        // Delete cronjob
        if ($del) {
            return $this->deleteObjectAndReturnEmptyResponse($cronjob);
        }

        // Change cronjob state
        $state  = (bool)$params->post('state', 'bool default|0');
        $active = (bool)$params->post('active', 'bool default|0');
        if ($state && $cronjob->_id) {
            $cronjob->active = ($active) ? 1 : 0;

            return $this->storeObjectAndReturnEmptyResponse($cronjob, !$cronjob->_id);
        }

        // Create or update a cronjob
        $cronjob->name            = stripslashes($params->post('name', 'str'));
        $cronjob->description     = stripslashes($params->post('description', 'str'));
        $cronjob->active          = ($active) ? 1 : 0;
        $cronjob->token_id        = $params->post('token_id', 'ref class|CViewAccessToken', PERM_READ);
        $cronjob->_user_id        = $params->post('_user_id', 'ref class|CUser', PERM_READ);
        $cronjob->params          = $params->post('params', 'str');
        $cronjob->route_name      = $params->post('route_name', 'str');
        $cronjob->servers_address = $params->post('servers_address', 'str');
        $cronjob->mode            = $params->post(
            'mode',
            'enum list|' . implode('|', CCronJob::MODES) . ' default|' . CCronJob::MODE_LOCK
        );
        $cronjob->_frequently     = $params->post('_frequently', 'enum list|@yearly|@monthly|@weekly|@daily|@hourly');
        $cronjob->_second         = $params->post('_second', 'str default|0');
        $cronjob->_minute         = $params->post('_minute', 'str default|*');
        $cronjob->_hour           = $params->post('_hour', 'str default|*');
        $cronjob->_day            = $params->post('_day', 'str default|*');
        $cronjob->_month          = $params->post('_month', 'str default|*');
        $cronjob->_week           = $params->post('_week', 'str default|*');
        $cronjob->_generate_token = $params->post('_generate_token', 'bool default|0');

        return $this->storeObjectAndReturnEmptyResponse($cronjob, !$cronjob->_id);
    }

    /**
     * @throws Exception
     */
    public function execute(): Response
    {
        $cronjob         = new CCronJob();
        $cronjob->active = '1';

        try {
            $jobs = $cronjob->loadMatchingList();
        } catch (Exception $e) {
            $jobs = [];
        }

        if (!$jobs) {
            return $this->renderEmptyResponse();
        }

        $manager = new CCronJobManager();

        foreach ($jobs as $_job) {
            $manager->registerJob($_job);
        }

        $manager->runJobs();

        return $this->renderEmptyResponse();
    }
}
