<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\CRequest;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Security\Cert\Format;
use Ox\Mediboard\System\CEncryptedProperty;
use Ox\Mediboard\System\CObjectEncryption;
use Ox\Mediboard\System\Keys\Certificate;
use Ox\Mediboard\System\Keys\CKeyMetadata;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotPersistKey;
use Ox\Mediboard\System\Keys\Exceptions\CouldNotUseKey;
use Ox\Mediboard\System\Keys\KeyBuilder;
use Ox\Mediboard\System\Keys\KeyStore;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class KeyMetadataController extends Controller
{
    /**
     * @throws Exception
     */
    public function list(): Response
    {
        $keys_metadata = (new CKeyMetadata())->loadList();
        foreach ($keys_metadata as $metadata) {
            $metadata->checkStatus();
        }

        return $this->renderSmarty(
            'key_metadata/vw_keys_metadata.tpl',
            [
                'keys_metadata' => $keys_metadata,
            ]
        );
    }

    /**
     * @throws CouldNotUseKey
     * @throws Exception
     */
    public function show(RequestParams $request_params, CKeyMetadata $metadata, KeyStore $store): Response
    {
        $usage = $request_params->get('type', 'enum list|form|usage|info');

        $metadata->checkStatus();

        return match ($usage) {
            'form'  => $this->showForm($metadata),
            'usage' => $this->showUsage($metadata),
            'info'  => $this->showCertificateInfo($metadata, $store),
            default => $this->renderSmarty(
                'key_metadata/inc_vw_key_metadata.tpl',
                [
                    'metadata' => $metadata,
                ]
            ),
        };
    }

    private function showForm(CKeyMetadata $metadata): Response
    {
        return $this->renderSmarty(
            'key_metadata/vw_key_metadata_form.tpl',
            [
                'metadata' => $metadata,
            ]
        );
    }

    /**
     * @throws Exception
     */
    private function showUsage(CKeyMetadata $metadata): Response
    {
        [$count_encrypted_objects_by_class, $total_encrypted_objects] = $this->getStatsForEncryptedObject(
            new CObjectEncryption(),
            $metadata
        );

        [$count_encrypted_properties_by_class, $total_encrypted_properties] = $this->getStatsForEncryptedObject(
            new CEncryptedProperty(),
            $metadata
        );

        return $this->renderSmarty(
            'key_metadata/vw_key_metadata_usage.tpl',
            [
                'metadata'                            => $metadata,
                'count_encrypted_objects_by_class'    => $count_encrypted_objects_by_class,
                'total_encrypted_objects'             => $total_encrypted_objects,
                'count_encrypted_properties_by_class' => $count_encrypted_properties_by_class,
                'total_encrypted_properties'          => $total_encrypted_properties,
            ]
        );
    }

    /**
     * @throws CouldNotUseKey
     */
    private function showCertificateInfo(CKeyMetadata $metadata, KeyStore $store): Response
    {
        if (!$metadata->isCertificate()) {
            throw CouldNotUseKey::invalidType($metadata, CKeyMetadata::TYPE_CERTIFICATE);
        }

        $cert = null;

        try {
            $cert = $store->getCertificate($metadata->name);
        } catch (Throwable) {
            // Nothing
        }

        return $this->renderSmarty(
            'key_metadata/vw_key_info.tpl',
            [
                'metadata' => $metadata,
                'cert'     => $cert,
            ]
        );
    }

    public function showDetails(CKeyMetadata $metadata): Response
    {
        return $this->renderSmarty(
            'key_metadata/vw_key_metadata_details.tpl',
            [
                'metadata' => $metadata,
            ]
        );
    }

    /**
     * @throws CouldNotUseKey
     * @throws Exception
     */
    public function generateKey(CKeyMetadata $metadata, KeyBuilder $builder, RequestParams $request_params): Response
    {
        if (!$this->isCsrfTokenValid('system_gui_key_metadata_generate_key', $request_params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if (!$metadata->isKey()) {
            throw CouldNotUseKey::invalidType($metadata, CKeyMetadata::TYPE_KEY);
        }

        try {
            if ($metadata->hasBeenPersisted()) {
                throw CouldNotPersistKey::alreadyExists($metadata->name);
            }

            $builder->generateKey($metadata);

            $this->addUiMsgOk('KeyBuilder-msg-Key have been successfully generated', true);
        } catch (Throwable $t) {
            $this->addUiMsgError($t->getMessage(), true);

            return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @param CKeyMetadata  $metadata
     * @param RequestParams $request_params
     * @param KeyBuilder    $builder
     *
     * @return Response
     * @throws CouldNotPersistKey
     * @throws CouldNotUseKey
     * @throws Exception
     */
    public function uploadCertificate(
        CKeyMetadata  $metadata,
        RequestParams $request_params,
        KeyBuilder    $builder
    ): Response {
        if (!$this->isCsrfTokenValid('system_gui_key_metadata_cert_upload', $request_params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if (!$metadata->isCertificate()) {
            throw CouldNotUseKey::invalidType($metadata, CKeyMetadata::TYPE_KEY);
        }

        $passphrase = $request_params->request('_passphrase', 'str');
        $format     = $request_params->request('_format', 'enum list|' . implode('|', Format::values()));
        $files      = $request_params->getRequest()->files->get('formfile', []);

        $passphrase = trim(stripcslashes($passphrase));

        $count = count($files);
        if (!$count) {
            throw CouldNotPersistKey::noUploadedFile($metadata);
        }

        if ($count > 1) {
            throw CouldNotPersistKey::tooManyUploadedFiles($metadata);
        }

        /** @var UploadedFile $file */
        $file = reset($files);

        try {
            // Getting the file content
            try {
                $cert = $file->getContent();
            } catch (FileException) {
                throw CouldNotPersistKey::unableToRetrieveUploadedFileContent($metadata);
            }

            // Renewing certificate
            if ($metadata->hasBeenPersisted()) {
                $format = Format::from($format);

                if (!Certificate::checkPassphrase($cert, $format, $passphrase)) {
                    throw CouldNotPersistKey::invalidFormatOrPassphrase();
                }

                $builder->renewCertificate($metadata, $cert, $format, $passphrase);
            } else {
                if (!Certificate::checkPassphrase($cert, $metadata->_format, $passphrase)) {
                    throw CouldNotPersistKey::invalidFormatOrPassphrase();
                }

                // Uploading a new certificate
                $builder->persistCertificate($metadata, $cert, $passphrase);
            }

            $this->addUiMsgOk('KeyBuilder-msg-Key have been successfully generated', true);
        } catch (Throwable $t) {
            $this->addUiMsgError($t->getMessage(), true);

            return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }/* finally {
            CMbPath::remove($file->getRealPath());
        }*/

        return $this->renderEmptyResponse();
    }

    /**
     * @param CEncryptedProperty|CObjectEncryption $encrypted
     * @param CKeyMetadata                         $metadata
     *
     * @return array
     * @throws Exception
     */
    private function getStatsForEncryptedObject(
        CEncryptedProperty|CObjectEncryption $encrypted,
        CKeyMetadata                         $metadata
    ): array {
        $request = new CRequest();
        $request->addSelect(['object_class', 'COUNT(*) AS `total`']);
        $request->addTable($encrypted->getSpec()->table);
        $request->addWhere(['key_id' => $encrypted->getDS()->prepare('= ?', $metadata->_id)]);
        $request->addGroup(['object_class']);
        $request->addOrder('total DESC');

        $count_encrypted_objects_by_class = $encrypted->getDS()->loadHashList($request->makeSelect());
        $total_encrypted_objects          = array_sum($count_encrypted_objects_by_class);

        return [
            $count_encrypted_objects_by_class,
            $total_encrypted_objects,
        ];
    }
}
