<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Cache;
use Ox\Core\CacheInfo;
use Ox\Core\CacheManager;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\CSmartyDP;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\AbstractModuleCache;
use Ox\Core\Module\Cache\CacheCleanerStrategyFactory;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class CacheController extends Controller
{
    /**
     * @throws Exception
     */
    public function index(CacheInterface $distr_cache, RequestParams $params): Response
    {
        // Get DSHM infos
        $dshm_infos = [];
        try {
            $dshm_infos = [
                'name'    => $distr_cache->getMetadata()->get(Cache::DISTR, 'engine'),
                'version' => $distr_cache->getMetadata()->get(Cache::DISTR, 'engine_version'),
            ];
        } catch (CouldNotGetCache $e) {
            // Do nothing
        }

        // Get module cache classes
        $modules_cache = [];
        try {
            $module_cache_classes = CacheManager::getModuleCacheClasses();
            foreach ($module_cache_classes as $module_cache_class) {
                /** @var AbstractModuleCache $module_cache */
                $module_cache = new $module_cache_class();

                $modules_cache[$module_cache->getModuleName()] = [
                    "class" => $module_cache_class,
                    "tr"    => $this->translator->tr('module-' . $module_cache->getModuleName() . '-court'),
                    "distr" => $module_cache->getDSHMPatterns(),
                    "outer" => $module_cache->getSHMPatterns(),
                ];
            }
        } catch (Exception $e) {
            $this->addUiMsgWarning($e->getMessage(), true);
        }

        return $this->renderSmarty(
            'cache/view_cache.tpl',
            [
                "cache_layers"  => ["outer" => "shm", "distr" => "dshm"],
                "cache_keys"    => CacheManager::$cache_values,
                "modules_cache" => $modules_cache,
                "servers_ip"    => preg_split("/\s*,\s*/", $this->conf->get("servers_ip"), -1, PREG_SPLIT_NO_EMPTY),
                "dshm_infos"    => $dshm_infos,
                "actual_ip"     => $params->getRequest()->server->get('SERVER_ADDR'),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function viewConfirm(RequestParams $params): Response
    {
        return $this->renderSmarty(
            'cache/inc_view_cache_modal.tpl',
            [
                'cache'  => $params->get('cache', 'str notNull'),
                'target' => $params->get('target', 'str notNull'),
                'layer'  => $params->get('layer', 'enum notNull list|shm|dshm|all'),
                'module' => $params->get('module', 'str'),
                'keys'   => $params->get('keys', 'str'),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function clear(RequestParams $params): Response
    {
        $cache        = $params->get('cache', 'str notNull');
        $target       = $params->get('target', 'str notNull');
        $layer        = $params->get('layer', 'enum notNull list|shm|dshm|all');
        $layer_strict = (int)$params->get('layer_strict', 'num');

        // If layer_strict is passed from a remote strategy, layer need to be the same,
        // else, convert layer (string) to layer (int).
        if ($layer_strict > 0) {
            $layer = $layer_strict;
        } else {
            switch ($layer) {
                case 'shm':
                    $layer = CacheManager::SHM_SPECIAL;
                    break;

                case 'dshm':
                    $layer = CacheManager::DSHM_SPECIAL;
                    break;

                case 'all':
                default:
                    $layer = CacheManager::ALL;
            }
        }

        $request = $params->getRequest();
        $session = $request->getSession();

        $strategy = (new CacheCleanerStrategyFactory($cache, $target, $layer))
            ->withHosts(explode(',', trim($this->conf->get("servers_ip"))))
            ->withCookie("{$session->getName()}={$session->getId()}")
            ->withActualHost($request->server->get('SERVER_ADDR'))
            ->withClearCacheUrl($this->generateUrl('system_gui_cache_clear'))
            ->create();

        $strategy->execute();

        // Outputs
        return $this->renderSmarty(
            'cache/view_cache_result.tpl',
            [
                "result" => $strategy->getHtmlResult(new CSmartyDP()),
            ]
        );
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function viewCacheDetails(
        RequestParams  $params,
        CacheInterface $outer_cache_compressed,
        CacheInterface $distr_cache_compressed
    ): Response {
        if ($prefix = $params->get("prefix", "str")) {
            return $this->showDetail($params->get("type", "enum notNull list|shm|dshm|opcode"), $prefix);
        }

        if ($key = $params->get("key", "str")) {
            $type = $params->get("type", "enum notNull list|shm|dshm");

            return $this->showKeyDetail(
                $type,
                $key,
                ($type === 'shm') ? $outer_cache_compressed : $distr_cache_compressed
            );
        }

        return $this->renderSmarty(
            'cache/vw_cache.tpl',
            [
                "shm_global_info"    => Cache::getInfo(Cache::OUTER),
                "dshm_global_info"   => Cache::getInfo(Cache::DISTR),
                "opcode_global_info" => CacheInfo::getOpcodeCacheInfo(),
                "assets_global_info" => CacheInfo::getAssetsCacheInfo(),
            ]
        );
    }

    /**
     * @throws InvalidArgumentException
     */
    private function showKeyDetail(string $type, string $key, CacheInterface $cache): Response
    {
        $key = stripslashes($key);

        return $this->renderSmarty(
            'cache/inc_vw_cache_entry_value.tpl',
            [
                "type"  => $type,
                "key"   => $key,
                "value" => CMbArray::toJSON($cache->get($key), true, JSON_PRETTY_PRINT),
            ]
        );
    }

    /**
     * @throws CMbException
     * @throws Exception
     */
    private function showDetail(string $type, string $prefix): Response
    {
        $prefix = stripslashes($prefix);

        switch ($type) {
            default:
            case "shm":
            case "dshm":
                $detail = Cache::getKeysInfo(($type === 'shm') ? Cache::OUTER : Cache::DISTR, $prefix);
                break;
            case "opcode":
                $detail = CacheInfo::getOpcodeKeysInfo($prefix);
                break;
        }

        return $this->renderSmarty(
            'cache/inc_vw_cache_detail.tpl',
            [
                "detail" => $detail,
                "type"   => $type,
            ]
        );
    }

    /**
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function removeKey(RequestParams $params, CacheInterface $outer_cache, CacheInterface $distr_cache): Response
    {
        $type = $params->post("type", "enum notNull list|shm|dshm");
        $key  = $params->post("key", "str notNull");
        $key  = str_replace('\\\\', '\\', $key);

        if (!$this->isCsrfTokenValid('system_gui_cache_delete_key', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $cache    = ($type === 'shm') ? $outer_cache : $distr_cache;
        $job_done = $cache->delete($key);

        $job_done ? $this->addUiMsgOk('System-msg-Cache entry removed', true)
            : $this->addUiMsgError('System-error-Error during suppression', true);

        return $this->renderEmptyResponse();
    }
}
