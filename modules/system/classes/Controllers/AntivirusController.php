<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Security\Antivirus\VirusCheckerInterface;
use Symfony\Component\HttpFoundation\Response;

class AntivirusController extends Controller
{
    public function show(VirusCheckerInterface $checker, Conf $conf): Response
    {
        $host      = $conf->getStatic('system ClamAv host');
        $port      = $conf->getStatic('system ClamAv port');
        $socket    = $conf->getStatic('system ClamAv socket');
        $freshness = $conf->getStatic('system antivirus freshness');
        if ($freshness > 0) {
            $freshness = CMbDT::date("-{$freshness} DAYS");
        }

        $available   = $checker->isAvailable();
        $last_update = $available ? $checker->getLastVirusDatabaseUpdate() : null;

        return $this->renderSmarty(
            'antivirus/vw_dashboard.tpl',
            [
                'host'        => $host,
                'port'        => $port,
                'socket'      => $socket,
                'available'   => $available,
                'last_update' => $last_update?->format('Y-m-d H:i:s'),
                'freshness'   => $freshness,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function refresh(VirusCheckerInterface $checker, RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('system_gui_antivirus_refresh', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        if ($checker->refreshDatabase()) {
            $this->addUiMsgOk('VirusCheckerInterface-msg-Signature database refresh');

            return $this->renderEmptyResponse();
        }

        $this->addUiMsgError('VirusCheckerInterface-error-An error occurred while trying to refresh the database');

        return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
