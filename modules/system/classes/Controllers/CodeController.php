<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\ControllerException;
use Ox\Mediboard\System\Code\CodeRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Handle the manipulation of codes
 */
class CodeController extends Controller
{
    /**
     * @api
     */
    public function list(RequestApi $request, CodeRepositoryInterface $api_repository): Response
    {
        $keywords = $request->getRequest()->get('keywords', '');

        if (strlen($keywords) < 2) {
            throw new ControllerException(
                Response::HTTP_BAD_REQUEST,
                $this->translator->tr('CodeController-Error-Keywords must be at least 2 chars long')
            );
        }

        return $this->renderApiResponse(new Collection($api_repository->find($keywords)));
    }
}
