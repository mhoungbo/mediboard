<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbArray;
use Ox\Core\CMbString;
use Ox\Core\Controller;
use Ox\Core\CSetup;
use Ox\Core\Kernel\Exception\HttpException;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Core\Module\Requirements\CRequirementsException;
use Ox\Core\Module\Requirements\CRequirementsManager;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\Modules\ModulesManager;
use Ox\Mediboard\System\Modules\ModulesRepository;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Module manipulation for API and GUI purpose
 */
class ModulesController extends Controller
{
    public const LINK_MODULE_URL      = 'module_url';
    public const LINK_MODULE_TABS_URL = 'tabs';
    public const LINK_LIST_MODULES    = 'modules';

    public const STATE_INSTALLED = 'installed';
    public const STATE_ACTIVE    = 'active';
    public const STATE_VISIBLE   = 'visible';

    public const AVAILABLE_STATES = [
        self::STATE_INSTALLED,
        self::STATE_ACTIVE,
        self::STATE_VISIBLE,
    ];

    private const MANAGE_ACTIONS         = ['toggle', 'toggleMenu', 'remove', 'install', 'upgrade'];
    private const CORE_FORBIDDEN_ACTIONS = ['toggle', 'remove', 'install'];

    /**
     * @throws ApiException
     * @throws HttpException
     * @api
     */
    public function showModule(string $mod_name, RequestApi $request_api): Response
    {
        $module = CModule::getActive($mod_name);
        if ($module === null) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'The module ' . $mod_name . ' is not active.');
        }

        $module->registerTabs();

        $item = Item::createFromRequest($request_api, $module);
        $item->addLinks(
            [
                self::LINK_MODULE_URL      => $module->getDefaultTab(),
                self::LINK_MODULE_TABS_URL => $this->generateUrl('system_modules_tabs_list', ['mod_name' => $mod_name]),
            ]
        );

        return $this->renderApiResponse($item);
    }

    /**
     * @throws ApiException
     * @api
     */
    public function listModules(RequestApi $request_api): Response
    {
        $state = $request_api->getRequest()->get('state', self::STATE_INSTALLED);

        $request_limit = $request_api->getRequestLimit();
        $offset        = $request_limit->isInQuery() ? $request_limit->getOffset() : null;
        $limit         = $request_limit->isInQuery() ? $request_limit->getLimit() : null;

        return $this->renderApiResponse($this->buildModuleListResource($state, $offset, $limit, $request_api));
    }

    private function getModuleList(string $state): array
    {
        switch ($state) {
            case self::STATE_INSTALLED:
                return CModule::getInstalled();
            case self::STATE_ACTIVE:
                return CModule::getActive();
            case self::STATE_VISIBLE:
                return CModule::getVisible();
            default:
                throw new HttpException(
                    Response::HTTP_NOT_FOUND,
                    "State '{$state}' is not in " . implode(', ', self::AVAILABLE_STATES)
                );
        }
    }

    /**
     * @throws ApiException
     */
    private function buildModuleListResource(
        string      $state,
        ?int        $offset = null,
        ?int        $limit = null,
        ?RequestApi $request_api = null
    ): Collection {
        $loaded_modules = $this->getModuleList($state);

        $modules = [];

        // In api mod modules are loaded before locales. Manual load is required
        /** @var CModule $module */
        foreach ($loaded_modules as $module) {
            if (($state === self::STATE_ACTIVE) && (!$module->getPerm(PERM_READ) || !$module->mod_ui_active)) {
                continue;
            }

            $module->updateFormFields();

            $modules[] = $module;
        }

        $total = count($modules);

        CMbArray::pluckSort($modules, SORT_FLAG_CASE | SORT_NATURAL, '_view', CMbArray::PLUCK_SORT_REMOVE_DIACRITICS);

        if ($pagination = (($offset !== null) || ($limit !== null))) {
            $modules = array_slice($modules, ($offset) ?? 0, $limit);
        }

        $collection = ($request_api)
            ? Collection::createFromRequest($request_api, $modules)
            : new Collection($modules);

        foreach ($collection as $item) {
            $item->addLinks(
                [
                    self::LINK_MODULE_TABS_URL => $this->generateUrl(
                        'system_modules_tabs_list',
                        ['mod_name' => $item->getDatas()->mod_name],
                    ),
                ]
            );
        }

        if ($pagination) {
            if (!$request_api) {
                // Using the ABSOLUTE_URL will cause the url to be http://localhost/api/modules
                // instead of http://localhost/mediboard/api/modules in non request API mode
                $collection->setRequestUrl($this->getApplicationUrl() . $this->generateUrl('system_modules_list'));
            }

            $collection->createLinksPagination($offset, $limit, $total);
            $collection->addLinks(
                [
                    self::LINK_LIST_MODULES => $this->generateUrl('system_modules_list')
                        . '?state=' . self::STATE_ACTIVE,
                ]
            );
        }

        return $collection;
    }

    /**
     * @throws ApiException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     * @todo list only authorized routes
     * @api
     */
    public function listModuleRoutes(string $mod_name): Response
    {
        $router           = $this->container->get('router');
        $route_collection = $router->getRouteCollection();
        $namespace_prefix = CClassMap::getInstance()->getNamespaceFromModule($mod_name);
        if (!$namespace_prefix) {
            throw new HttpException(404, 'Invalid module name ' . $mod_name);
        }

        $datas = [];
        /**
         * @var string $route_name
         * @var Route  $route
         */
        foreach ($route_collection as $route_name => $route) {
            $controller = $route->getDefault('_controller');
            if (!$namespace_prefix || !str_starts_with($controller, $namespace_prefix)) {
                continue;
            }

            $datas[] = [
                '_type'   => 'path_map',
                '_id'     => $route_name,
                'path'    => $route->getPath(),
                'methods' => $route->getMethods(),
            ];
        }

        $collection = new Collection($datas);
        /** @var Item $item */
        foreach ($collection as $item) {
            $array = $item->getData('methods');
            $item->addLinks(
                [
                    'schema' => $router->getGenerator()->generate(
                        'system_shemas_routes',
                        [
                            'path'   => str_replace('=', '', base64_encode($item->getData('path'))),
                            'method' => strtolower(reset($array)), // todo forge all methods links
                        ],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                ]
            );
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function index(ModulesRepository $repository): Response
    {
        return $this->renderSmarty(
            'modules/view_modules.tpl',
            [
                'mbmodules'   => $repository->initModules()->getModules(),
                'upgradable'  => $repository->isUpgradable(),
                'coreModules' => $repository->getCoreModules(),
                'php_version' => PHP_VERSION,
            ]
        );
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function manage(RequestParams $params, CacheInterface $outer_cache): Response
    {
        $mod_id   = $params->post('mod_id', 'ref class|CModule');
        $mod_name = $params->post('mod_name', 'str');
        $cmd      = $params->post('cmd', 'enum list|' . implode('|', self::MANAGE_ACTIONS));

        if (!$this->isCsrfTokenValid('system_gui_modules_manage', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $module = new CModule();
        if ($mod_id) {
            $module->load($mod_id);
            $module->checkModuleFiles();
        } else {
            $module->mod_version = '0.0';
            $module->mod_name    = $mod_name;
        }

        if (!($setup_module = CSetup::getCSetupClass($module->mod_name))) {
            if (($module->mod_type != 'core') && !$module->isFilesMissing()) {
                $this->addUiMsgError('CModule-msg-no-setup', true);

                return $this->renderEmptyResponse();
            }
        }

        if (($module->mod_type == 'core') && in_array($cmd, self::CORE_FORBIDDEN_ACTIONS)) {
            $this->addUiMsgError('CModule-msg-Core modules forbidden actions', true);

            return $this->renderEmptyResponse();
        }

        $setup = null;
        if (!$module->isFilesMissing()) {
            /** @var CSetup $setup */
            $setup = new $setup_module();
        }

        $manager = new ModulesManager($module, $cmd, $setup);

        switch ($cmd) {
            case 'toggle':
            case 'toggleMenu':
                if ($manager->execute()) {
                    $this->addUiMsgOk('CModule-msg-state-changed', true);
                } else {
                    $this->addUiMsgError('CModule-msg-state-not changed', true);
                }
                break;

            case 'remove':
                if ($manager->execute()) {
                    $this->addUiMsgOk('CModule-msg-removed', true);
                } else {
                    $this->addUiMsgError('CModule-msg-not removed', true);
                }
                break;

            case 'install':
            case 'upgrade':
                if ($manager->execute()) {
                    if ($setup->mod_version == $module->mod_version) {
                        $this->addUiMsgOk(
                            "Installation de '%s' � la version %s",
                            true,
                            $module->mod_name,
                            $setup->mod_version
                        );
                    } else {
                        $this->addUiMsgWarning(
                            "Installation de '%s' � la version %s sur %s",
                            true,
                            $module->mod_name,
                            $module->mod_version,
                            $setup->mod_version
                        );
                    }
                } else {
                    $this->addUiMsgError(
                        "Module '%s' non " . (($cmd === "install") ? 'install�' : 'mis � jour'),
                        true,
                        $module->mod_name
                    );
                }

                // In case the setup has added some user preferences
                CAppUI::buildPrefs();
                break;

            default:
                $this->addUiMsgError('Unknown Command', true);
        }

        // Delete modules cache
        $outer_cache->delete('modules');

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function show(ModulesRepository $repository, CsrfTokenManagerInterface $token_manager): Response
    {
        $module = $repository->getModule();

        if ($setup_module = CSetup::getCSetupClass($module->mod_name)) {
            $module->compareToSetup(new $setup_module());
        }

        return $this->renderSmarty(
            'modules/inc_module.tpl',
            [
                '_mb_module'        => $module,
                'installed'         => true,
                'module_id'         => $module->_id,
                'csrf_token_manage' => $token_manager->getToken('system_gui_modules_manage'),
                'manage_url'        => $this->generateUrl('system_gui_modules_manage'),
            ]
        );
    }

    /**
     * @throws ReflectionException
     * @throws CRequirementsException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function requirements(RequestParams $params, string $mod_name): Response
    {
        if (
            !($module = CModule::getInstalled($mod_name))
            || !($requirements = $module->getRequirements())
            || !($module instanceof CModule)
        ) {
            return $this->renderSmarty(
                'requirements/vw_requirements.tpl',
                [
                    "tpl_error" => $this->translator->tr(
                        "common-error-An error occurred"
                    ),
                ]
            );
        }

        $group_id = $params->get('group_id', 'ref class|CGroups');

        $requirements_class = get_class($requirements);
        $resume             = [];
        foreach (CGroups::loadGroups() as $group) {
            /** @var CRequirementsManager $requirements */
            $requirements = new $requirements_class();
            $requirements->checkRequirements($group);

            $resume[$group->text] = [
                'errors'   => $requirements->countErrors(),
                'total'    => $requirements->count(),
                'group_id' => $group->_id,
            ];
        }

        // if no group_id passed, get current
        $establishment = (new CGroups())->load(($group_id) ?: $this->getRequestContext()->getGroupId());

        // check requirements
        /** @var CRequirementsManager $requirements */
        $requirements = new $requirements_class();
        $requirements->checkRequirements($establishment);

        // meta data
        $tabs_failed   = [];
        $groups_failed = [];
        foreach ($requirements as $item) {
            if (!$item->isCheck()) {
                $tab             = $item->getTab();
                $tabs_failed[]   = $tab;
                $groups_failed[] = "{$tab}_" . $item->getGroup();
            }
        }

        $tabs        = $requirements->getTabs();
        $description = $requirements->getDescription() ?? null;
        if ($description && $description->hasDescription()) {
            array_unshift($tabs, "description");
        }

        return $this->renderSmarty(
            'requirements/vw_requirements.tpl',
            [
                "nb_requirements"   => count($requirements),
                "nb_errors"         => $requirements->countErrors(),
                "tabs_failed"       => $tabs_failed,
                "groups_failed"     => $groups_failed,
                "description"       => $description,
                "requirements_tabs" => $requirements->serialize(),
                "tabs"              => $tabs,
                "resume"            => $resume,
                "actual_group"      => $establishment->_id,
                "mod_name"          => $mod_name,
                "requirements_url"  => $this->generateUrl('system_gui_modules_requirements', ['mod_name' => $mod_name]),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function autocomplete(RequestParams $params): Response
    {
        $input_field = $params->get('input_field', 'str');
        $keywords    = $params->get($input_field, 'str');
        $profile     = $params->get('profile', 'str');
        $common      = (bool)$params->get('common', 'bool default|0');

        switch ($profile) {
            case 'active':
                $modules = CModule::getActive();
                break;

            default:
                $modules = CModule::getVisible();
        }

        if ($keywords) {
            $keywords = CMbString::lower(CMbString::removeDiacritics($keywords));

            $modules = array_filter(
                $modules,
                function (CModule $module) use ($keywords): bool {
                    $name  = CMbString::removeDiacritics($module->mod_name);
                    $short = CMbString::removeDiacritics($this->translator->tr("module-{$module->mod_name}-court"));
                    $long  = CMbString::removeDiacritics($this->translator->tr("module-{$module->mod_name}-long"));

                    return
                        (stripos($name, $keywords) !== false)
                        || (stripos($short, $keywords) !== false)
                        || (stripos($long, $keywords) !== false);
                }
            );
        }

        return $this->renderSmarty(
            'autocomplete/inc_modules_autocomplete.tpl',
            [
                'keywords' => $keywords,
                'matches'  => $modules,
                'profile'  => $profile,
                'common'   => $common,
            ]
        );
    }
}
