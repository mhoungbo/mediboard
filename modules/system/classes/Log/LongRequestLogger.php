<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Log;

use DateInterval;
use DateTimeImmutable;
use Exception;
use LogicException;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\Config\Conf;
use Ox\Core\CSQLDataSource;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\Redis\CRedisClient;
use Ox\Core\Sessions\SessionHelper;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CExchangeSourceAdvanced;
use Ox\Mediboard\System\CModuleAction;
use Ox\Mediboard\System\Sources\ExchangeSourceReport;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LongRequestLogger
{
    use RequestHelperTrait;

    private LoggerInterface $logger;
    private Response        $response;
    private Conf            $conf;
    private CApp            $app;
    private CClassMap       $map;

    public function __construct(Conf $conf, CApp $app, CClassMap $map, LoggerInterface $logger)
    {
        $this->conf   = $conf;
        $this->app    = $app;
        $this->map    = $map;
        $this->logger = $logger;
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    public function logFromGlobals(): void
    {
        $module = $this->getModuleFromGlobals();
        $action = $this->getActionFromGlobals();

        if (($module === null) || ($action === null)) {
            $this->logger->warning('Missing module/action for logging long request.');

            return;
        }

        $module_action_id = $this->getModuleActionId($this->getModuleFromGlobals(), $this->getActionFromGlobals());

        $this->log(
            $module_action_id,
            (CAppUI::$instance->_ref_user instanceof CMediusers) ? CAppUI::$instance->_ref_user->_ref_user : null,
            $_SERVER['APP_ENV'] === 'dev'
        );
    }

    public function logFromRequest(Request $request): void
    {
        $request_controller = $this->getControllerFromRequest($request);

        if ($request_controller === null) {
            // Direct response (or exception handled)
            return;
        }

        [$controller,] = explode('::', $request_controller, 2);

        $map = $this->map->getClassMap($controller);

        $module = $map->module;

        $module_action_id = $this->getModuleActionId($module, $this->getRouteFromRequest($request), true);

        $user = (CAppUI::$instance->_ref_user instanceof CMediusers) ? CAppUI::$instance->_ref_user->_ref_user : null;

        $this->log($module_action_id, $user, $this->isEnvironmentDev($request));
    }

    /**
     * @param int        $module_action_id
     * @param CUser|null $user
     * @param bool       $is_dev
     *
     * @return CLongRequestLog|null The stored log if successful
     */
    public function log(int $module_action_id, ?CUser $user, bool $is_dev): ?CLongRequestLog
    {
        if (!$this->supports()) {
            return null;
        }

        $threshold = $this->getThreshold($user);

        // Invalid configuration according to given context
        if ($threshold === null) {
            return null;
        }

        $duration = $this->app->getDuration();

        // If request is too slow
        if (($duration > $threshold) || $this->isWhitelisted($module_action_id)) {
            $log = $this->performLog($duration, $module_action_id, $user);
            if ($is_dev) {
                $this->logger->warning("CLongRequestLog stored on this request", ["_id" => $log->_id]);
            }

            return $log;
        }

        return null;
    }

    private function supports(): bool
    {
        if ($this->app->isInReadOnlyState() || !$this->conf->get('log_access') || !$this->isModuleActionReady()) {
            return false;
        }

        $human_long_request_level = $this->conf->get('human_long_request_level');
        $bot_long_request_level   = $this->conf->get('bot_long_request_level');

        if (!$human_long_request_level && !$bot_long_request_level) {
            return false;
        }

        return true;
    }

    protected function performLog(float $duration, int $module_action_id, ?CUser $user): CLongRequestLog
    {
        $log = $this->buildLog($duration, $module_action_id, $user);

        if ($msg = $log->store()) {
            throw new LogicException(sprintf('Unable to store long request log: %s', $msg));
        }

        return $log;
    }

    private function getThreshold(?CUser $user): ?int
    {
        $human_threshold = $this->conf->get('human_long_request_level');
        $bot_threshold   = $this->conf->get('bot_long_request_level');

        $bot = $this->app->isPublic() ? false : $user->isRobot();

        // Determine the log_level to apply
        $threshold = 0;
        if ($bot && $bot_threshold) {
            $threshold = $bot_threshold;
        } elseif (!$bot && $human_threshold) {
            // 'Public' is considered human
            $threshold = $human_threshold;
        }

        $threshold = (int)$threshold;

        return ($threshold > 0) ? $threshold : null;
    }

    protected function isWhitelisted(int $module_action_id): bool
    {
        $long_request_whitelist = explode("\n", (string)$this->conf->get('long_request_whitelist'));

        foreach ($long_request_whitelist as $module_action) {
            if (
                preg_match(
                    "/^(?P<module>[a-zA-Z0-9]+)\s(?P<action>([a-zA-Z0-9_]+)(::[a-zA-Z0-9_]+)?)\s(?P<probability>\d+)/",
                    trim($module_action),
                    $matches
                )
            ) {
                $_module_action_id = $this->getModuleActionId($matches['module'], $matches['action']);

                // Loosely comparison
                if ($_module_action_id == $module_action_id) {
                    return (mt_rand(1, $matches['probability']) === 1);
                }
            }
        }

        return false;
    }

    /**
     * @param float      $duration
     * @param int        $module_action_id
     * @param CUser|null $user
     *
     * @return CLongRequestLog
     * @throws Exception
     */
    private function buildLog(float $duration, int $module_action_id, ?CUser $user): CLongRequestLog
    {
        $now = new DateTimeImmutable('now');

        $log = new CLongRequestLog();

        $log->datetime_start   =
            $now->sub(new DateInterval('PT' . round($duration, 0) . 'S'))->format('Y-m-d H:i:s');
        $log->datetime_end     = $now->format('Y-m-d H:i:s');
        $log->duration         = $duration;
        $log->server_addr      = $_SERVER['SERVER_ADDR'] ?? '::1';
        $log->module_action_id = $module_action_id;

        if (!$this->app->isPublic()) {
            $log->user_id    = $user->_id;
            $log->session_id = $this->getSessionId();
        }

        // GET and POST params
        $log->_query_params_get  = $_GET;
        $log->_query_params_post = $_POST;

        // SESSION params
        $log->_session_data = SessionHelper::sanitize($this->getSessionData());

        // Performance data
        $log->_query_performance = $this->app->getPerformance()->preparePerformance($this->response)->toArray();

        // Query report
        $log->_query_report = [
            'sql'     => $this->getSqlReport(),
            'nosql'   => $this->getNoSqlReport(),
            'sources' => $this->getExchangeSourcesReports(),
        ];

        // Unique Request ID
        $log->requestUID = $this->app->getRequestIdentifier();

        return $log;
    }

    protected function isModuleActionReady(): bool
    {
        return (new CModuleAction())->isInstalled();
    }

    protected function getModuleFromGlobals(): ?string
    {
        return $GLOBALS['m'] ?? null;
    }

    protected function getActionFromGlobals(): ?string
    {
        return (($GLOBALS['dosql']) ?: (($GLOBALS['action']) ?: $GLOBALS['a'])) ?? null;
    }

    protected function getRouteFromRequest(Request $request): ?string
    {
        return $request->attributes->get('_route');
    }

    protected function getControllerFromRequest(Request $request): ?string
    {
        return $request->attributes->get('_controller');
    }

    /**
     * @param string|null $module Might be null if an error occurred too early in the FW
     * @param string|null $action Might be null if an error occurred too early in the FW
     * @param bool        $is_route
     *
     * @return int
     * @throws InvalidArgumentException
     */
    protected function getModuleActionId(?string $module, ?string $action, bool $is_route = false): int
    {
        return CModuleAction::getID($module, $action, $is_route);
    }

    protected function getSessionId(): ?string
    {
        return $this->app::getSessionHelper()->getId();
    }

    protected function getSessionData(): array
    {
        return CApp::getSessionHelper()?->getData() ?? [];
    }

    protected function getSqlReport(): array
    {
        CSQLDataSource::buildReport(20);

        return CSQLDataSource::$report_data;
    }

    protected function getNoSqlReport(): array
    {
        CRedisClient::buildReport(20);

        return CRedisClient::$report_data;
    }

    protected function getExchangeSourcesReports(): array
    {
        $report = new ExchangeSourceReport();

        foreach (CExchangeSourceAdvanced::$call_details as $type => $calls) {
            foreach ($calls as $call) {
                $report->trace($type, $call['signature'], $call['request_time']);
            }
        }

        return $report->toArray();
    }
}
