<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Log;

use Exception;
use Ox\Core\Config\Conf;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CUserLog;

/**
 * Service that can remove logs from users from other groups if the configuration is set to true.
 */
class UserLogService
{
    private Conf $conf;

    public function __construct(Conf $conf = null)
    {
        $this->conf = $conf ?? new Conf();
    }

    /**
     * If the user is not admin and the shared medical data is set to false, remove the logs from $logs that are for
     * a user from another group.
     *
     * @param CUserLog[] $logs
     * @param int        $current_group_id
     *
     * @return array
     * @throws Exception
     */
    public function removeForbiddenLogs(array $logs, int $current_group_id): array
    {
        if (
            CModule::getInstalled('system')->canAdmin()
            || $this->conf->getStatic('dPetablissement general dossiers_medicaux_shared')
        ) {
            return $logs;
        }

        $users     = CStoredObject::massLoadFwdRef($logs, 'user_id');
        $mediusers = (new CMediusers())->loadList(["user_id" => CSQLDataSource::prepareIn(array_keys($users))]);
        CStoredObject::massLoadFwdRef($mediusers, "function_id");

        $available_logs = [];
        foreach ($logs as $log) {
            $user_group_id = isset($mediusers[$log->user_id]) ?
                $mediusers[$log->user_id]->loadRefFunction()->group_id :
                $log->_ref_user->loadRefMediuser()->loadRefFunction()->group_id;

            if ($current_group_id !== (int)$user_group_id) {
                continue;
            }

            $available_logs[$log->_id] = $log;
        }

        return $available_logs;
    }
}
