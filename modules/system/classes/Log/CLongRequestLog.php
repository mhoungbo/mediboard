<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Log;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CMbSecurity;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\CValue;
use Ox\Core\ExplainQueryGenerator;
use Ox\Core\Redis\CRedisClient;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CModuleAction;
use Ox\Mediboard\System\CUserAuthentication;
use Ox\Mediboard\System\Sources\ExchangeSourceReport;

/**
 * Global request (PHP + SQL) slow queries
 */
class CLongRequestLog extends CStoredObject
{
    /** @var int Primary Key */
    public $long_request_log_id;

    // DB fields
    public $user_id;
    public $datetime_start;
    public $datetime_end;
    public $duration;
    public $server_addr;
    public $module_action_id;
    public $session_id;

    // JSON DB fields
    public $query_params_get;
    public $query_params_post;
    public $session_data;
    public $query_performance;
    public $query_report;

    // Form fields
    public $_module;
    public $_action;
    public $_link;

    // Filter fields
    public $_datetime_start_min;
    public $_datetime_start_max;
    public $_datetime_end_min;
    public $_datetime_end_max;

    // Arrays
    public $_query_params_get;
    public $_query_params_post;
    public $_session_data;
    public $_query_performance;
    public $_query_report;

    /** @var CMediusers */
    public $_ref_user;

    /** @var CModuleAction */
    public $_ref_module_action;

    /** @var CUserAuthentication */
    public $_ref_session;

    // Unique Request ID
    public $requestUID;

    /** @var string */
    public $_user_type;

    public $_php_time       = 0;
    public $_sql_time       = 0;
    public $_nosql_time     = 0;
    public $_transport_time = 0;

    public $_php_ratio       = 0;
    public $_sql_ratio       = 0;
    public $_nosql_ratio     = 0;
    public $_transport_ratio = 0;

    public bool $_enslaved = false;

    /**
     * @inheritdoc
     */
    function getSpec()
    {
        $spec           = parent::getSpec();
        $spec->table    = "long_request_log";
        $spec->key      = "long_request_log_id";
        $spec->loggable = false;

        return $spec;
    }

    /**
     * @inheritdoc
     */
    function updatePlainFields()
    {
        parent::updatePlainFields();

        // GET
        if ($this->_query_params_get) {
            $this->_query_params_get = CMbSecurity::filterInput($this->_query_params_get);
            $this->query_params_get  = CMbArray::toJSON($this->_query_params_get);
        }

        // POST
        if ($this->_query_params_post) {
            $this->_query_params_post = CMbSecurity::filterInput($this->_query_params_post);
            $this->query_params_post  = CMbArray::toJSON($this->_query_params_post);
        }

        // SESSION
        if ($this->_session_data) {
            $this->_session_data = CMbSecurity::filterInput($this->_session_data);
            $this->session_data  = CMbArray::toJSON($this->_session_data);
        }

        // Performance
        if ($this->_query_performance) {
            $this->query_performance = CMbArray::toJSON($this->_query_performance);
        }

        // SQL report
        if ($this->_query_report) {
            $this->query_report = CMbArray::toJSON($this->_query_report);
        }
    }

    /**
     * @inheritdoc
     */
    function updateFormFields()
    {
        $this->_query_params_get  = json_decode($this->query_params_get ?? '', true);
        $this->_query_params_post = json_decode($this->query_params_post ?? '', true);
        $this->_session_data      = json_decode($this->session_data ?? '', true);
        $this->_query_performance = json_decode($this->query_performance ?? '', true);
        $this->_query_report      = json_decode($this->query_report ?? '', true);

        $this->computePerformanceRatio();
    }

    /**
     * @inheritdoc
     */
    public function store(): ?string
    {
        // Purge probability
        if (!$this->_id) {
            CApp::doProbably(100, [$this, 'purgeLogs']);
        }

        return parent::store();
    }

    /**
     * Once out of 100 logs, delete the 1000 oldest logs among those that are more than 3 months old.
     *
     * @throws Exception
     */
    protected function purgeLogs(): bool
    {
        $log = new self();
        $ds  = $log->getDS();

        $request = new CRequest();
        $request->addTable($log->_spec->table)
                ->addWhere(
                    [
                        'datetime_start' => $ds->prepare(
                            '<= ?',
                            CMbDT::date("- 3 MONTHS") . ' 00:00:00'
                        ),
                    ]
                )
                ->addOrder('datetime_start ASC')
                ->setLimit(1000); // 100 logs x 10

        return $ds->exec($request->makeDelete()) !== false;
    }

    /**
     * Get module and action fields
     *
     * @return void
     */
    function getModuleAction()
    {
        if ($this->module_action_id) {
            $module_action = $this->loadRefModuleAction();

            $this->_module = $module_action->module;
            $this->_action = $module_action->action;

            return;
        }

        // @todo: Following is legacy code to be removed by 2016-01-01
        $get  = is_array($this->_query_params_get) ? $this->_query_params_get : [];
        $post = is_array($this->_query_params_post) ? $this->_query_params_post : [];

        $this->_module = CValue::first(
            CMbArray::extract($get, "m"),
            CMbArray::extract($post, "m")
        );

        $this->_action = CValue::first(
            CMbArray::extract($get, "tab"),
            CMbArray::extract($get, "a"),
            CMbArray::extract($post, "dosql")
        );
    }

    /**
     * @inheritdoc
     */
    function getProps()
    {
        $props                      = parent::getProps();
        $props["user_id"]           = "ref class|CMediusers unlink back|long_request_log";
        $props["datetime_start"]    = "dateTime notNull";
        $props["datetime_end"]      = "dateTime notNull";
        $props["duration"]          = "float notNull";
        $props["server_addr"]       = "str notNull";
        $props["module_action_id"]  = "ref class|CModuleAction back|long_request_logs";
        $props["session_id"]        = "str";
        $props["query_params_get"]  = "text show|0";
        $props["query_params_post"] = "text show|0";
        $props["session_data"]      = "text show|0";
        $props["query_performance"] = "text show|0";
        $props["query_report"]      = "text show|0";
        $props["requestUID"]        = "str";

        // Form fields
        $props["_module"]            = "str";
        $props["_action"]            = "str";
        $props["_link"]              = "str";
        $props["_query_params_get"]  = "php";
        $props["_query_params_post"] = "php";
        $props["_session_data"]      = "php";
        $props["_query_performance"] = "php";
        $props["_query_report"]      = "php";

        // Filter fields
        $props["_datetime_start_min"] = "dateTime";
        $props["_datetime_start_max"] = "dateTime";
        $props["_datetime_end_min"]   = "dateTime";
        $props["_datetime_end_max"]   = "dateTime";
        $props['_user_type']          = 'enum list|all|human|bot|public';

        $props['_enslaved'] = 'bool default|0';

        return $props;
    }

    /**
     * Generate the long request weblink
     *
     * @return void
     */
    function getLink()
    {
        if ($this->_query_params_get) {
            $this->_link = "?" . http_build_query($this->_query_params_get, null, "&");
        }
    }

    /**
     * Load the referenced user
     *
     * @return CMediusers
     */
    function loadRefUser()
    {
        return $this->_ref_user = $this->loadFwdRef("user_id", true);
    }

    /**
     * Load the referenced module/action
     *
     * @return CModuleAction
     */
    function loadRefModuleAction()
    {
        return $this->_ref_module_action = $this->loadFwdRef("module_action_id", true);
    }

    /**
     * Load the user authentication
     *
     * @return CUserAuthentication|null
     */
    function loadRefSession()
    {
        if (!$this->session_id) {
            return null;
        }

        $where = [
            "session_id" => "= '$this->session_id'",
        ];

        $user_auth = new CUserAuthentication();
        $user_auth->loadObject($where, "datetime_login DESC");

        return $this->_ref_session = $user_auth;
    }

    /**
     * Compute datasource and transport performance ratios
     *
     * @return void
     */
    public function computePerformanceRatio(): void
    {
        if (!$this->_query_performance) {
            return;
        }

        $performance = $this->_query_performance;

        if (!is_array($performance)) {
            return;
        }

        $total = ($performance['genere']) ?? 1;

        if ($total <= 0) {
            return;
        }

        if (!isset($performance['dataSources'])) {
            return;
        }

        $sql_time = 0;
        foreach ($performance['dataSources'] as $_dsn => $_datasource) {
            $sql_time += $_datasource['time'];
        }

        $nosql_time = ($performance['nosqlTime']) ?? 0;

        $this->_sql_ratio   = round(($sql_time * 100) / $total, 2);
        $this->_nosql_ratio = round(($nosql_time * 100) / $total, 2);

        $transport_time         = $performance['transportTiers']['total']['time'] ?? 0;
        $this->_transport_ratio = round(($transport_time * 100) / $total, 2);

        $php_time         = ($total - ($sql_time + $nosql_time + $transport_time));
        $this->_php_ratio = round(($php_time * 100) / $total, 2);

        $this->_php_time       = round($php_time, 3);
        $this->_sql_time       = round($sql_time, 3);
        $this->_nosql_time     = round($nosql_time, 3);
        $this->_transport_time = round($transport_time, 3);

        $this->_enslaved = $performance['enslaved'];
    }

    public function isPublic(): bool
    {
        return ($this->user_id === null);
    }

    public function displaySqlReport(): void
    {
        CSQLDataSource::displayReport($this->_query_report['sql'], true, new ExplainQueryGenerator());
    }

    public function displayNoSqlReport(): void
    {
        if (isset($this->_query_report['nosql'])) {
            CRedisClient::displayReport($this->_query_report['nosql']);
        }
    }

    public function displaySourcesReport(): void
    {
        if (isset($this->_query_report['sources'])) {
            $report = ExchangeSourceReport::fromState($this->_query_report['sources']);
            $report->report(20);
        }
    }
}
