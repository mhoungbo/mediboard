<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Log;

use Exception;
use Ox\Core\FieldSpecs\CHtmlSpec;
use Ox\Core\FieldSpecs\CTextSpec;
use Ox\Mediboard\System\CUserLog;

/**
 * API representation of the diff of a field.
 * This is used as a relation for CUserLog to display updated fields with their old and new values.
 * For CTextSpec and CHtmlSpec only the diff is valued.
 */
class FieldDiff
{
    public const RESOURCE_TYPE = 'field_diff';

    public string  $id;
    public string  $api_type = self::RESOURCE_TYPE;
    public string  $field_name;
    public ?string $old_value;
    public ?string $new_value;
    public ?string $diff;

    private function __construct(string $field_name, ?string $old_value, ?string $new_value, ?string $diff)
    {
        $this->field_name = $field_name;
        $this->old_value  = $old_value;
        $this->new_value  = $new_value;
        $this->diff       = $diff;

        $this->id = uniqid();
    }

    /**
     * Return all the fields representation for a CUserLog.
     *
     * @param CUserLog $log
     *
     * @return FieldDiff[]
     * @throws Exception
     */
    public static function fromLog(CUserLog $log): array
    {
        $log->_ref_object = $log->loadFwdRef('object_id', true);
        $old_values = $log->getOldValues();
        $log->undiff_old_Values(false);

        $diffs = [];
        foreach ($old_values as $field_name => $value) {
            $spec = $log->_ref_object->_specs[$field_name];
            if ($spec instanceof CTextSpec || $spec instanceof CHtmlSpec) {
                $diffs[] = new FieldDiff($field_name, null, null, $log->_diff_values[$field_name]);
            } else {
                $diffs[]   = new FieldDiff(
                    $field_name,
                    $value,
                    $log->_ref_object->_history[$log->_id][$field_name] ?? $log->_ref_object->{$field_name},
                    null
                );
            }
        }

        return $diffs;
    }
}
