<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Exception;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Mediusers\CMediusers;

/**
 * Read system message manager
 */
class CMessageAcquittement extends CStoredObject
{
    public $acquittement_msg_system_id;
    public $user_id;
    public $message_id;
    public $date_ack;
    public $date_display;

    public ?CMediusers $_ref_user    = null;
    public ?CMessage   $_ref_message = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec            = parent::getSpec();
        $spec->table     = "acquittement_msg_system";
        $spec->key       = "acquittement_msg_system_id";
        $spec->loggable  = false;
        $spec->anti_csrf = true;

        $spec->uniques['user_message'] = ['user_id', 'message_id'];

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props["user_id"]      = "ref class|CMediusers notNull back|user_acquittement";
        $props["message_id"]   = "ref class|CMessage notNull back|acquittals";
        $props["date_ack"]     = "dateTime";
        $props["date_display"] = "dateTime notNull";

        return $props;
    }

    /**
     * @throws Exception
     */
    public function loadRefUser(): ?CMediusers
    {
        /** @var CMediusers */
        return $this->_ref_user = $this->loadFwdRef("user_id", true);
    }

    /**
     * @throws Exception
     */
    public function loadRefMessage(): ?CMessage
    {
        /** @var CMessage */
        return $this->_ref_message = $this->loadFwdRef("message_id", true);
    }
}
