<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Cron;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Listener that check if a self::PARAM_CRON_LOG_ID is present in the GET parameters.
 * Initialize the CCronJobLog with the ID from GET.
 */
class CronJobExecutionListener implements EventSubscriberInterface
{
    private const PARAM_CRON_LOG_ID = 'execute_cron_log_id';

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => ['beforeController', 40],
        ];
    }

    public function beforeController(ControllerEvent $event)
    {
        $request = $event->getRequest();

        if ($cron_job_log_id = $request->query->getInt(self::PARAM_CRON_LOG_ID)) {
            CCronJobLog::setCronJobLogId($cron_job_log_id);
        }
    }
}
