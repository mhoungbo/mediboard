<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Cron;

use Exception;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Routing\RequestParams;

/**
 * Repository to request multiple Cronjob objects.
 * It can use RequestParams to initialize its values.
 */
class CronjobRepository
{
    private RequestParams $params;

    /** @var CCronJob|CCronJobLog */
    private CStoredObject $object;

    private CSQLDataSource $ds;

    private array   $where  = [];
    private ?string $order  = null;
    private ?int    $limit  = null;
    private ?int    $offset = null;

    public function __construct(RequestParams $params)
    {
        $this->params = $params;
    }

    /**
     * Initialize $where using the RequestParam object.
     *
     * @throws Exception
     */
    public function initFromRequestParams(): self
    {
        if (!$this->params) {
            return $this;
        }

        // Only allow CCronJob and CCronJobLog for this repository
        if (
            ($this->object instanceof CCronJob)
            || ($this->object instanceof CCronJobLog)
        ) {
            $this->ds = $this->object->getDS();

            if ($cronjob_id = $this->params->get('cronjob_id', 'ref class|CCronJob')) {
                $this->where['cronjob_id'] = $this->ds->prepare('= ?', $cronjob_id);
            }

            $active = $this->params->get('active_filter', 'enum list|0|1|all');
            if (($active !== null) && ($active !== 'all')) {
                $this->where['active'] = $this->ds->prepare('= ?', $active);
            }

            if ($status = $this->params->get('status', 'str')) {
                $this->where['status'] = $this->ds->prepare('= ?', $status);
            }

            if ($severity = $this->params->get('severity', 'enum notNull list|0|1|2|3 default|0')) {
                $this->where['severity'] = $this->ds->prepare('= ?', $severity);
            }

            if ($date_min = $this->params->get('_date_min', 'dateTime')) {
                $this->where[] = $this->ds->prepare('start_datetime >= ?', $date_min);
            }

            if ($date_max = $this->params->get('_date_max', 'dateTime')) {
                $this->where[] = $this->ds->prepare('start_datetime <= ?', $date_max);
            }
        }

        return $this;
    }

    /**
     * Find a list of CCronJob|CCronJobLog
     *
     * @return CCronjob[]
     * @throws Exception
     */
    public function find(): array
    {
        $limit = null;
        if (($this->offset !== null) && ($this->limit !== null)) {
            $limit = "{$this->offset},{$this->limit}";
        }

        return $this->object->loadList(
            $this->where ?? null,
            $this->order ?? null,
            $limit ?? null,
        );
    }

    /**
     * Count a list of CCronJob|CCronJobLog
     *
     * @throws Exception
     */
    public function count(): int
    {
        return $this->object->countList($this->where);
    }

    /**
     * @param CCronJob|CCronJobLog $object
     */
    public function forObject(CStoredObject $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function setOrder(string $order, string $way): self
    {
        $this->order = "{$order} {$way}";

        return $this;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }
}
