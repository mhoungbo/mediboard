<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System\Cron;

use Cron\CronExpression;
use DateTimeImmutable;
use Exception;
use InvalidArgumentException;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CMbString;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\CViewAccessToken;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class for manage the cronjob
 */
class CCronJob extends CMbObject
{
    public const MODE_LOCK    = 'lock';
    public const MODE_ACQUIRE = 'acquire';

    public const MODES = [self::MODE_ACQUIRE, self::MODE_LOCK];

    /** @var int Primary key */
    public $cronjob_id;
    public $name;
    public $description;
    public $active;
    public $params;
    public $route_name;
    public $execution;
    public $servers_address;
    public $mode;
    public $token_id;

    public $_frequently;
    public $_second;
    public $_minute;
    public $_hour;
    public $_day;
    public $_month;
    public $_week;
    public $_user_id;

    public array $_next_datetime     = [];
    public array $_previous_datetime = [];

    /** @var  CronExpression */
    public $_cron_expression;

    /** @var CViewAccessToken */
    public      $_token;
    public bool $_generate_token = false;

    public array $_params       = [];
    public array $_servers      = [];
    public array $_lasts_status = [
        'ok' => 0,
        'ko' => 0,
    ];

    /** @var CCronJobLog */
    public $_ref_last_log;

    /** @var bool */
    public $_is_correctly_executed;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = "cronjob";
        $spec->key   = "cronjob_id";

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props["name"]            = "str notNull";
        $props["active"]          = "bool notNull default|1";
        $props["params"]          = "text";
        $props["route_name"]      = "str";
        $props["description"]     = "text";
        $props["execution"]       = "str notNull";
        $props["servers_address"] = "str";
        $props["mode"]            = "enum list|" . implode('|', self::MODES) . " default|" . self::MODE_LOCK;
        $props["token_id"]        = "ref class|CViewAccessToken nullify autocomplete|label back|jobs";

        $props["_frequently"] = "enum list|@yearly|@monthly|@weekly|@daily|@hourly";
        $props["_second"]     = "str";
        $props["_minute"]     = "str";
        $props["_hour"]       = "str";
        $props["_day"]        = "str";
        $props["_month"]      = "str";
        $props["_week"]       = "str";
        $props["_user_id"]    = "num";

        return $props;
    }

    /**
     * @inheritdoc
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->name;

        if (strpos($this->execution, "@") === 0) {
            $this->_frequently = $this->execution;
        } else {
            [$this->_second, $this->_minute, $this->_hour, $this->_day, $this->_month, $this->_week] = explode(
                " ",
                $this->execution
            );
        }

        $this->compileParams();

        if ($this->servers_address) {
            $this->_servers = explode("|", $this->servers_address);
        }
    }

    private function compileParams(): void
    {
        $params = strtr($this->params ?? "", ["\r\n" => "&", "\n" => "&", " " => ""]);
        parse_str($params, $this->_params);
    }

    /**
     * @inheritdoc
     */
    public function check(): ?string
    {
        if ($msg = $this->checkExecutionLine()) {
            return $msg;
        }

        if ($this->route_name) {
            $this->compileParams();

            try {
                CApp::generateUrl($this->route_name, $this->_params);
            } catch (InvalidArgumentException $e) {
                return $e->getMessage();
            }
        }

        return parent::check();
    }

    /**
     * @inheritdoc
     */
    public function store(): ?string
    {
        $this->completeField(
            '_second',
            '_minute',
            '_hour',
            '_day',
            '_month',
            '_week',
            'params',
            'token_id',
            'route_name'
        );

        if (!$this->params && !$this->route_name && !$this->token_id) {
            return 'CCronJob-error-Param route or token mandatory';
        }

        $parts = [
            $this->_second !== null && $this->_second !== "" ? $this->_second : "0",
            $this->_minute !== null && $this->_minute !== "" ? $this->_minute : "*",
            $this->_hour !== null && $this->_hour !== "" ? $this->_hour : "*",
            $this->_day !== null && $this->_day !== "" ? $this->_day : "*",
            $this->_month !== null && $this->_month !== "" ? $this->_month : "*",
            $this->_week !== null && $this->_week !== "" ? $this->_week : "*",
        ];

        if ($this->_frequently) {
            $this->execution = $this->_frequently;
        } else {
            $this->execution = implode(" ", $parts);
        }

        if ($this->_generate_token) {
            $this->generateTokenFromParams();
        }

        return parent::store();
    }

    /**
     * Verification of the line cron
     */
    private function checkExecutionLine(): ?string
    {
        //Dur�e - aucune v�rification � effectuer
        if (strpos($this->execution, "@") === 0) {
            return null;
        }

        $parts = explode(" ", $this->execution);
        if (count($parts) !== 6) {
            return "Longueur de l'expression incorrecte";
        }

        foreach ($parts as $_index => $_part) {
            $virgules = explode(",", $_part);
            foreach ($virgules as $_virgule) {
                $slashs = explode("/", $_virgule);
                $count  = count($slashs);
                if ($count > 2) {
                    return "Plusieurs '/' d�tect�es : $_part";
                }
                $left  = CMbArray::get($slashs, 0);
                $right = CMbArray::get($slashs, 1);
                if ($count == 2) {
                    if (!$this->checkParts($_index, $right)) {
                        return "Apr�s '/' nombre obligatoire : $_part";
                    }
                }

                $dashs = explode("-", $left);
                $count = count($dashs);
                if (count($dashs) > 2) {
                    return "Plusieurs '-' d�tect�es : $_part";
                }
                $left  = CMbArray::get($dashs, 0);
                $right = CMbArray::get($dashs, 1);
                if ($count == 2) {
                    if (!$this->checkParts($_index, $right)) {
                        return "Apr�s '-' nombre obligatoire : $_part";
                    }
                    if ($right <= $left) {
                        return "Borne collection incorrecte: $_part";
                    }
                }

                if (($left !== "*") && !$this->checkParts($_index, $left)) {
                    return "Nombre ou '*' erron�e : $_part";
                }
            }
        }

        return null;
    }

    /**
     * Verify the part of the cron
     *
     * @param int    $position part of the cron
     * @param string $value    Value of the part
     */
    private function checkParts($position, $value): bool
    {
        $result = false;
        $regex  = "#^[0-9]{1,2}$#";
        $min    = 0;
        switch ($position) {
            //cas seconde
            case 0:
                //cas minute
            case 1:
                $max = 59;
                break;
            //cas heure
            case 2:
                $max = 23;
                break;
            //cas jour
            case 3:
                $min = 1;
                $max = 31;
                break;
            //cas mois
            case 4:
                $min = 1;
                $max = 12;
                break;
            //cas jour
            case 5:
                $max   = 7;
                $regex = "#^[0-7]{1}$#";
                break;
            default:
                return false;
        }

        if (preg_match($regex, $value)) {
            if (($value >= $min) && ($value <= $max)) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Get the n future execution
     *
     * @param int $next Number of iteration of the cron job in the future
     */
    public function getNextDate(int $next = 5): array
    {
        $next_datetime = [];
        if (!$this->_cron_expression) {
            $this->getCronExpression();
        }

        try {
            for ($i = 0; $i < $next; $i++) {
                $next_datetime[] = $this->_cron_expression->getNextRunDate("now", $i, true)->format('Y-m-d H:i:s');
            }
        } catch (Exception $e) {
            return [];
        }

        return $this->_next_datetime = $next_datetime;
    }

    /**
     * Get the n previous execution
     *
     * @param int $previous Number of iteration of the cron job in the past
     */
    public function getPreviousDate(int $previous = 5): ?array
    {
        $previous_datetime = [];
        if (!$this->_cron_expression) {
            $this->getCronExpression();
        }

        try {
            for ($i = 0; $i < $previous; $i++) {
                $previous_datetime[] = $this->_cron_expression->getPreviousRunDate("now", $i, true)->format(
                    'Y-m-d H:i:s'
                );
            }
        } catch (Exception $e) {
            return null;
        }

        return $this->_previous_datetime = $previous_datetime;
    }

    /**
     * @return CronExpression
     */
    private function getCronExpression(): CronExpression
    {
        if (strpos($this->execution, "@") === 0) {
            return $this->_cron_expression = CronExpression::factory($this->execution);
        }

        // Removing seconds expression feature which was hard-coded in precedent library
        $execution_wo_seconds = implode(' ', [$this->_minute, $this->_hour, $this->_day, $this->_month, $this->_week]);

        return $this->_cron_expression = CronExpression::factory($execution_wo_seconds);
    }

    /**
     * Make the URL
     *
     * @param string $base Base url to query
     *
     * @return string
     * @throws Exception
     */
    public function makeUrl(string $base): string
    {
        if (!$this->_params) {
            $this->compileParams();
        }

        $params = $this->_params;

        // If token ignore params and use only token
        if ($this->token_id) {
            /** @var CViewAccessToken $token */
            $token = $this->loadFwdRef('token_id', true);

            if ($token && $token->_id) {
                if ($token->_routes_names && (count($token->_routes_names) === 1)) {
                    // Generate the token URL for V2
                    $query = CApp::generateUrl(
                        'admin_token',
                        ['token' => $token->hash, 'execute_cron_log_id' => $params['execute_cron_log_id']],
                        UrlGeneratorInterface::RELATIVE_PATH
                    );
                } else {
                    // Generate the legacy token URL
                    $query = '?' . CMbString::toQuery(
                        [
                            'token'               => $token->hash,
                            'execute_cron_log_id' => $params['execute_cron_log_id'],
                        ]
                    );
                }
            }
        }

        // If no token or token is invalid
        if (!isset($query)) {
            // Check if @route is used
            if ($this->route_name) {
                $query = CApp::generateUrl($this->route_name, $params, UrlGeneratorInterface::RELATIVE_PATH);
            } else {
                // Or only apply params
                $query = '?' . CMbString::toQuery($params);
            }
        }

        return $base . $query;
    }

    /**
     * @return null|CStoredObject
     * @throws Exception
     */
    public function loadRefToken(bool $cached = true): ?CViewAccessToken
    {
        return $this->_token = $this->loadFwdRef('token_id', $cached);
    }

    /**
     * Generate a CViewAccessToken using the current user and $this->params.
     * If the token is successfully created link it in token_id and remove the params
     *
     * @throws Exception
     */
    private function generateTokenFromParams(): void
    {
        // Check if a token if already used in the params
        preg_match('/^token=(?<hash>\w+)$/', $this->params, $matches);
        if ($matches && isset($matches['hash'])) {
            $token = CViewAccessToken::getByHash($matches['hash']);

            // If token exists
            if ($token->_id) {
                $this->token_id = $token->_id;
                $this->params   = '';

                return;
            }
        }

        // Create a new token
        $token        = new CViewAccessToken();
        $token->label = $this->name;

        if ($this->route_name) {
            $token->routes_names = $this->route_name;
            $token->params       = $this->params ?: 'foo';
        } else {
            $token->params = $this->params;
        }

        $token->user_id    = $this->_user_id ?: CUser::get()->_id;
        $token->restricted = '1';
        $token->loadMatchingObjectEsc();

        $token->_hash_length = 10;

        if ($msg = $token->store()) {
            CAppUI::stepAjax($msg, UI_MSG_WARNING);

            return;
        }

        if ($token->_id) {
            $this->token_id   = $token->_id;
            $this->params     = '';
            $this->route_name = '';
        }
    }

    /**
     * @param int $limit Number of logs to load
     *
     * @throws Exception
     */
    public function loadLastsStatus(int $limit = 10): void
    {
        $logs = $this->loadBackRefs('cron_logs', 'cronjob_log_id DESC', $limit);

        /** @var CCronJobLog $_log */
        foreach ($logs as $_log) {
            if ((((int)$_log->status) && ($_log->status < 400)) || ($_log->status == 'finished')) {
                $this->_lasts_status['ok']++;
            } else {
                $this->_lasts_status['ko']++;
            }
        }
    }

    public function loadLastLog(): ?CCronJobLog
    {
        $logs = $this->loadBackRefs('cron_logs', 'start_datetime DESC', 1);

        return $this->_ref_last_log = array_pop($logs);
    }

    /**
     * @throws Exception
     */
    public function isCorrectlyExecuted(): bool
    {
        if (!$this->_ref_last_log || ($this->_ref_last_log->start_datetime === null)) {
            return $this->_is_correctly_executed = false;
        }

        return $this->_is_correctly_executed =
            new DateTimeImmutable($this->_ref_last_log->start_datetime)
            >= new DateTimeImmutable($this->_previous_datetime[0]);
    }
}
