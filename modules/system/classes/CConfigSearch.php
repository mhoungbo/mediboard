<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Ox\Core\Module\CModule;
use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

/**
 * Configuration search utility class
 */
class CConfigSearch
{
    final public const TYPE_CONFIG_INSTANCE  = 1;
    final public const TYPE_CONFIG_ETAB      = 2;
    final public const TYPE_CONFIG_PREF      = 3;
    final public const TYPE_CONFIG_FUNC_PERM = 4;
    final public const TYPE_CONFIG_SERVICE   = 5;
    final public const TYPE_CONFIG_STATIC    = 6;

    /**
     * Get the list of all configs (instance and object related configs) as a flat array
     *
     * @throws InvalidArgumentException
     */
    public static function getConfigs(): array
    {
        $config_save = $GLOBALS['dPconfig'];

        include __DIR__ . "/../../../includes/config_dist.php";

        $configs = array_fill_keys(array_keys(self::flatten('', $GLOBALS['dPconfig'])), self::TYPE_CONFIG_INSTANCE);

        $GLOBALS['dPconfig'] = $config_save;

        $model          = [];
        $configs_static = [];
        foreach (CModule::getInstalled() as $_mod) {
            try {
                $configs_static = array_merge(
                    $configs_static,
                    array_fill_keys(
                        array_keys(
                            self::flatten(
                                $_mod->mod_name . ' ',
                                ConfigurationManager::get()->getValuesForModule($_mod->mod_name)
                            )
                        ),
                        self::TYPE_CONFIG_STATIC
                    )
                );
            } catch (Throwable) {
                // Do nothing
            }

            try {
                $model = array_merge_recursive($model, ContextualConfigurationManager::_getModel($_mod->mod_name));
            } catch (Throwable) {
                // Do nothing
            }
        }

        $configs_etab = [];
        foreach ($model as $_configs) {
            $configs_etab = array_merge($configs_etab, $_configs);
        }

        $configs_etab = array_fill_keys(array_keys($configs_etab), self::TYPE_CONFIG_ETAB);

        return array_merge($configs, $configs_etab, $configs_static);
    }

    /**
     * Flatten a tree array
     *
     * @param string $prefix  Prefix to glue elements of the tree
     * @param array  $configs The tree to flatten
     */
    private static function flatten(string $prefix, array $configs): array
    {
        $result = [];
        foreach ($configs as $key => $value) {
            if (in_array($key, ['db', 'php', 'ft'])) {
                continue;
            }

            if (is_array($value)) {
                $result = array_merge($result, self::flatten("{$prefix}{$key} ", $value));
            } else {
                $result["{$prefix}{$key}"] = true;
            }
        }

        return $result;
    }
}
