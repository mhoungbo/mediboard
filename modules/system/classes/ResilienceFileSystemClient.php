<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\System;

use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\FileSystemClientInterface;
use Ox\Interop\Eai\Resilience\CircuitBreaker;
use Ox\Interop\Ftp\CircuitBreakerException;
use Ox\Interop\Ftp\CustomRequestAnalyserInterface;
use Ox\Interop\Ftp\ResponseAnalyser;
use Ox\Mediboard\System\Sources\ClientResilienceTrait;

class ResilienceFileSystemClient implements FileSystemClientInterface
{
    /** @var FileSystemClientInterface */
    public FileSystemClientInterface $client;

    /** @var CircuitBreaker */
    private CircuitBreaker $circuit;

    /** @var ResponseAnalyser */
    private ResponseAnalyser $analyser;

    /** @var CSourceFileSystem */
    private CSourceFileSystem $source;

    use ClientResilienceTrait;

    /**
     * @param FileSystemClientInterface $client
     * @param CSourceFileSystem         $source
     */
    public function __construct(FileSystemClientInterface $client, CSourceFileSystem $source)
    {
        $this->client = $client;

        $this->analyser = ($client instanceof CustomRequestAnalyserInterface)
            ? $client->getRequestAnalyser() : new ResponseAnalyser();

        $this->source  = $source;
        $this->circuit = new CircuitBreaker();
    }


    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function send(string $destination_basename): bool
    {
        $call = fn() => $this->client->send($destination_basename);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function receive(string $path_directory): array
    {
        $call = fn() => $this->client->receive($path_directory);

        return (array) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getSize(string $file_name): int
    {
        $call = fn() => $this->client->getSize($file_name);

        return (int) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function renameFile(string $file_path, string $new_name): bool
    {
        $call = fn() => $this->client->renameFile($file_path, $new_name);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function createDirectory(string $directory_name): bool
    {
        $call = fn() => $this->client->createDirectory($directory_name);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getDirectory(string $directory): string
    {
        $call = fn() => $this->client->getDirectory($directory);

        return $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getListFiles(string $directory, bool $information = false): array
    {
        $call = fn() => $this->client->getListFiles($directory, $information);

        return (array) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getListFilesDetails(string $current_directory): array
    {
        $call = fn() => $this->client->getListFilesDetails($current_directory);

        return (array) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getListDirectory(string $current_directory): array
    {
        $call = fn() => $this->client->getListDirectory($current_directory);

        return (array) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function addFile(string $source_file, string $file_name): bool
    {
        $call = fn() => $this->client->addFile($source_file, $file_name);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function delFile(string $path): bool
    {
        $call = fn() => $this->client->delFile($path);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     */
    public function getError(): ?string
    {
        return $this->client->getError();
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getData(string $path, ?string $dest = null): ?string
    {
        $call = fn() => $this->client->getData($path, $dest);

        return $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     */
    public function removeDirectory(string $dir_path, bool $purge = false): bool
    {
        $call = fn() => $this->client->removeDirectory($dir_path, $purge);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }
}
