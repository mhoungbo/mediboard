<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Mediboard\System\Sources\CSourceFile;

CCanDo::checkAdmin();

$target_directory = CView::get("new_directory", "str");
$source_guid   = CView::get("source_guid", "str");
CView::checkin();

/** @var CSourceFile $source */
$source                 = CMbObject::loadFromGuid($source_guid);
try {
    $client = $source->disableResilience();

    // first call show, directory configured
    if (empty($target_directory)) {
        $target_directory = $source->completePath()->withoutPrefix()->toDirectory();
    }

    $target_directory = $client->getDirectory($target_directory);
    $directories      = $client->getListDirectory($target_directory);
    $root             = $source->getRootDirectory($target_directory);
    $target_directory = rtrim($target_directory, '/') . '/';

    $smarty = new CSmartyDP();
    $smarty->assign("current_directory", $target_directory);
    $smarty->assign("root", $root);
    $smarty->assign("directory", $directories);
    $smarty->assign("source_guid", $source_guid);
    $smarty->display("inc_manage_directory.tpl");
} catch (CMbException $e) {
    CAppUI::stepMessage(UI_MSG_ERROR, $e->getMessage());
}
