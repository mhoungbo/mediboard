<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\Cache;
use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CMbArray;
use Ox\Core\CSmartyDP;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\CUserLog;
use Ox\Mediboard\System\Forms\CExObject;
use Ox\Mediboard\System\Log\UserLogService;

CCanDo::check();

global $dialog;

if (!CCanDo::read() && !$dialog) {
    global $can;
    $can->denied();
}

if (!CAppUI::pref('system_show_history')) {
    CAppUI::accessDenied();
}

$filter = new CUserLog();

$filter->object_class = CView::get("object_class", "str", !$dialog);
$ex_class_id          = CView::get("ex_class_id", "num");
$filter->object_id    = CView::get("object_id", "num", !$dialog); // Can be a deleted object

CView::checkin();
CView::enforceSlave();

$object = new CStoredObject();
if ($filter->object_id && $filter->object_class) {
    /** @var CStoredObject $object */
    $object = new $filter->object_class();

    $ex_object_history = false;
    if ($ex_class_id && $filter->object_class === "CExObject") {
        /** @var CExObject $object */
        $object->_ex_class_id = $ex_class_id;
        $object->setExClass();
        $filter->object_class .= "_$ex_class_id";
        $ex_object_history    = true;
    }

    $object->load($filter->object_id);

    // CExClass::inHermeticMode() handling
    if ($ex_object_history && !$object->getPerm(PERM_READ)) {
        CAppUI::accessDenied();
    }

    if (!$ex_class_id && !$object->getPerm(PERM_READ)) {
        CAppUI::accessDenied();
    }

    $object->loadHistory();
}

$filter->loadRefUser();

$list       = $object->_ref_logs;
$list_count = count($object->_ref_logs);

$dossiers_medicaux_shared = CAppUI::conf('dPetablissement general dossiers_medicaux_shared', 'static');

// Sort by id with PHP cuz dumbass MySQL won't limit rowscan before sorting
// even though `date` is explicit as first intention sorter AND obvious index in most cases
// Tends to be a known limitation
$ordered_list = CMbArray::pluck($list, "_id");
array_multisort($ordered_list, SORT_DESC, $list);

$group_id = CGroups::loadCurrent()->_id;
$users    = CStoredObject::massLoadFwdRef($list, "user_id");

$list = (new UserLogService())->removeForbiddenLogs($list, $group_id);

// The $_log->getOldValues will update $_log->_ref_object which is returned from inner cache and is a ref to $object.
// The "undiff_old_Values" function need to work on the updated object.
// Clone the $object to reset it to its actual values before displaying the view.
$tmp_obj = clone $object;
foreach ($list as $_log) {
    $_log->loadRefUser();

    $_log->loadTargetObject();
    $_log->getOldValues();
    $_log->undiff_old_Values();
}

$object = $tmp_obj;

$smarty = new CSmartyDP();
$smarty->assign("dialog", $dialog);
$smarty->assign("filter", $filter);
$smarty->assign("object", $object);
$smarty->assign("list", $list);
$smarty->assign("list_count", $list_count);
$smarty->display('view_history_object');
