<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Mediboard\System\Sources\CSourceFile;
use Ox\Mediboard\System\Sources\ObjectPath;

CCanDo::checkAdmin();

$current_directory = CView::get("current_directory", "str");
$delete            = CView::get("delete", "str", false);
$rename            = CView::get("rename", "str", false);
$new_name          = CView::get("new_name", "str");
$file              = CView::get("file", "str");
$source_guid       = CView::get("source_guid", "str");
CView::checkin();

/** @var CSourceFile $exchange_source */
$exchange_source = CMbObject::loadFromGuid($source_guid);

$files = [];
try {
    $client = $exchange_source->disableResilience();

    if (empty($current_directory)) {
        $current_directory = $exchange_source->completePath()->withoutPrefix()->toDirectory();
    } else {
        $current_directory = ObjectPath::fromPath($current_directory)->toDirectory();
    }

    if ($delete && $file) {
        $client->delFile($current_directory->withFilePath($file));
    }

    if ($rename && $new_name) {
        $client->renameFile(
            $current_directory->withFilePath($file),
            $current_directory->withFilePath($new_name)
        );
    }

    $files = $client->getListFilesDetails($current_directory);
    $count_files = count($files);

    CAppUI::stepMessage(
        UI_MSG_OK,
        "Le dossier '$current_directory' contient : $count_files fichier(s)"
    );

    if ($count_files > 1000) {
        CAppUI::stepMessage(
            UI_MSG_WARNING,
            "Le dossier $current_directory contient trop de fichiers pour �tre list�"
        );
    }

    // limit dom for only 1000 files
    $files = array_slice($files, 0, 1000);

    // append / for concatenation in template
    $current_directory = rtrim($current_directory, '/') . '/';

    $smarty = new CSmartyDP();
    $smarty->assign("files", $files);
    $smarty->assign("current_directory", $current_directory);
    $smarty->assign("source_guid", $source_guid);
    $smarty->display("inc_manage_file.tpl");
} catch (CMbException $e) {
    CAppUI::stepMessage(UI_MSG_ERROR, $e->getMessage());
}
