<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Mediboard\System\ViewSender\CViewSender;

// DO NOT DELETE, MIGHT BE USED IN TOKENS
// @deprecated Should use route system_gui_view_senders_monitor instead

CCanDo::checkRead();

CView::checkin();
CView::enforceSlave();

$sender = new CViewSender();
$sender->active = '1';
/** @var CViewSender[] $senders */
$senders = $sender->loadMatchingListEsc();

$sources = CStoredObject::massLoadBackRefs($senders, 'sources_link');
CStoredObject::massLoadFwdRef($sources, 'source_id');

foreach ($senders as $_sender) {
    $senders_source = $_sender->loadRefSendersSource();
    $_sender->getLastAge();

    foreach ($senders_source as $_sender_source) {
        $_sender_source->loadRefSender();
    }
}

$smarty = new CSmartyDP();
$smarty->assign('senders'    ,$senders);
$smarty->assign('order_date' , '');
$smarty->assign('order_col'  , '');
$smarty->assign('total'      , count($senders));
$smarty->assign('start'      , 0);
$smarty->assign('step'       , count($senders));
$smarty->display('view_senders/inc_monitor_senders.tpl');
