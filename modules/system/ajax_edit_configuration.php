<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CSmartyDP;
use Ox\Core\CValue;
use Ox\Mediboard\System\ContextualConfigurationManager;

$inherit = CValue::get("inherit");
$module  = CValue::get("module");
$mode    = CValue::get('mode');
$type    = CValue::get('type');

if (!is_array($inherit)) {
    $inherit = [$inherit];
}

try {
    $all_inherits = array_keys(ContextualConfigurationManager::_getModel($module, $inherit));
} catch (LogicException $e) {
    $all_inherits = [];
}

$mode = ($mode) ?: ContextualConfigurationManager::getConfigurationMode();

$smarty = new CSmartyDP();
$smarty->assign("module", $module);
$smarty->assign("inherit", $inherit);
$smarty->assign("all_inherits", $all_inherits);
$smarty->assign('mode', $mode);
$smarty->assign('type', $type);
$smarty->display("inc_edit_configuration.tpl");
