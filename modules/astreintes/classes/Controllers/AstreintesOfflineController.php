<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Astreintes\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Astreintes\AstreinteManager;
use Symfony\Component\HttpFoundation\Response;

class AstreintesOfflineController extends Controller
{
    /**
     * @throws Exception
     */
    public function list(RequestParams $params, AstreinteManager $manager): Response
    {
        $date        = $params->get('date', 'date default|now');
        $mode        = $params->get('mode', 'enum list|day|week|month|year default|day');
        $type_names  = $params->get('type_names', 'str default|all');
        $category_id = $params->get('category', 'ref class|CCategorieAstreinte');

        $type_names = is_array($type_names) ? $type_names : explode(',', $type_names);
        $astreintes = $manager->loadAstreintes($date, $mode, $type_names, $category_id ?: null);
        [$start, $end] = $manager->computeNextPrev($date, AstreinteManager::MODE_OFFLINE);

        return $this->renderSmarty(
            'offline_list_astreintes',
            [
                'astreintes'   => $astreintes,
                'start_period' => $start,
                'end_period'   => $end,
            ]
        );
    }
}
