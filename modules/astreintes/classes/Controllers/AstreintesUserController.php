<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Astreintes\Controllers;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Astreintes\AstreinteManager;
use Ox\Mediboard\Astreintes\CPlageAstreinte;
use Symfony\Component\HttpFoundation\Response;

class AstreintesUserController extends Controller
{
    /**
     * @throws Exception
     */
    public function listPlagesAstreintesCurrentUser(AstreinteManager $manager): Response
    {
        $user = $this->getCMediuser();
        /** @var CPlageAstreinte[] $plages */
        $plages = $manager->loadAstreintesForUser($user);

        CStoredObject::massLoadFwdRef($plages, "user_id");
        CStoredObject::massLoadFwdRef($plages, "categorie");
        foreach ($plages as $_plage) {
            $_plage->loadRefUser();
            $_plage->loadRefColor();
        }

        return $this->renderSmarty(
            "vw_idx_plages_astreinte",
            [
                "user"   => $user,
                "plages" => $plages,
                "today"  => CMbDT::dateTime(),
            ]
        );
    }
}
