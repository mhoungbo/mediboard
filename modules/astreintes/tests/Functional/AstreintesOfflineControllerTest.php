<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Astreintes\Tests\Functional;

use DOMDocument;
use DOMXPath;
use Ox\Core\Controller;
use Ox\Mediboard\Astreintes\CPlageAstreinte;
use Ox\Mediboard\Astreintes\Tests\Fixtures\AstreintesFixtures;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description
 */
class AstreintesOfflineControllerTest extends OxWebTestCase
{
    public function testListAstreintes()
    {
        $client = self::createClient();

        $crawler = $client->request(
            Request::METHOD_GET,
            '/gui/astreintes/offline/list',
            [
                'dialog' => "1",
                'period' => 'month',
            ]
        );

        /** @var CPlageAstreinte $astreinte */
        $astreinte = $this->getObjectFromFixturesReference(CPlageAstreinte::class, AstreintesFixtures::TAG_PONCT_MED);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $this->assertEquals(1, $crawler->filterXPath('//button[@class="print not-printable"]')->count());
        $this->assertEquals(1, $crawler->filterXPath("//td[text()=\"{$astreinte->loadRefCategory()->name}\"]")->count());
    }
}
