/**
 * @package Mediboard\Rpps
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

AnnuaireSante = {
  loadResource: function (form) {

    const identifier = form.elements.identifier.value;
    const date = form.elements.date.value;

    new Url("rpps", "ajaxLoadResource")
      .addParam("identifier", identifier)
      .addParam("date", date)
      .requestUpdate('annuaire_sante_results');
  }
}
