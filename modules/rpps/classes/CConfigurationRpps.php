<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Rpps;

use Ox\Mediboard\System\AbstractConfigurationRegister;
use Ox\Mediboard\System\ConfigurationManager;

/**
 * @codeCoverageIgnore
 */
class CConfigurationRpps extends AbstractConfigurationRegister
{
    /**
     * @inheritDoc
     */
    public function register()
    {
    }

    /**
     * @inheritDoc
     */
    public function registerStatic(ConfigurationManager $manager): void
    {
        $manager->registerStatic(
            [
                'general' => [
                    'populate_method' => 'enum list|zip|api|combined default|ZIP localize',
                ]
            ]
        );
    }
}
