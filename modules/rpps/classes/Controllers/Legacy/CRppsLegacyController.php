<?php

/**
 * @package Mediboard\Rpps
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Rpps\Controllers\Legacy;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbDT;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Core\Logger\LoggerLevels;
use Ox\Core\Module\CModule;
use Ox\Import\Rpps\CExternalMedecinBulkImport;
use Ox\Import\Rpps\CExternalMedecinSync;
use Ox\Import\Rpps\CMedecinExercicePlaceManager;
use Ox\Import\Rpps\CRppsFileDownloader;
use Ox\Import\Rpps\Entity\CDiplomeAutorisationExercice;
use Ox\Import\Rpps\Entity\CMssanteInfos;
use Ox\Import\Rpps\Entity\CPersonneExercice;
use Ox\Import\Rpps\Entity\CSavoirFaire;
use Ox\Import\Rpps\Exception\CImportMedecinException;
use Ox\Interop\Ans\ANSException;
use Ox\Interop\Ans\Services\AnnuaireSanteService;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\System\CConfiguration;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Description
 */
class CRppsLegacyController extends CLegacyController
{
    public function annuaire_rpps(): void
    {
        $this->checkPermAdmin();

        $this->renderSmarty(
            'vw_annuaire_sante'
        );
    }

    public function ajax_create_schema(): void
    {
        $this->checkPermAdmin();

        CView::checkin();

        $import = new CExternalMedecinBulkImport();

        if (!$import->createSchema()) {
            CAppUI::commonError();
        }

        CAppUI::stepAjax('CExternalMedecinBulkImport-msg-Tables created');

        CApp::rip();
    }

    /**
     * @throws CImportMedecinException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function ajax_populate_database(): void
    {
        $this->checkPermAdmin();

        CView::checkin();

        CApp::setTimeLimit(1200);

        $populate_method = CAppUI::conf('rpps general populate_method', 'static');

        switch ($populate_method) {
            case 'api':
                $delta = CAppUI::conf('ans general delta_date', 'static');
                $this->populateWithAPI(AnnuaireSanteService::SYNC_PRACTITIONER_ROLE, $delta, null);
                break;
            case 'zip':
                $this->populateWithZip();
                break;
            default:
                $this->populateCombined();
                break;
        }

        // Mise � jour de la date pour le delta
        CConfiguration::setConfig('ans general delta_date', CMbDT::date(), null, null, true);

        CApp::rip();
    }

    /**
     * @throws CImportMedecinException
     * @throws Exception
     */
    public function cron_synchronize_medecin(): void
    {
        $this->checkPermEdit();

        $type = CView::get('type', 'enum list|' . implode('|', CExternalMedecinSync::ALLOWED_TYPES));
        $input_codes = CView::get('codes', 'str');
        $step = CView::get('step', 'num');

        if (empty($step)) {
            $step = intval(CAppUI::conf('rpps sync_step'));
        }

        CView::checkin();

        if (!$this->checkSyncEnabled()) {
            CApp::rip();
        }

        CAppUI::$localize = false;

        $codes = [];
        if ($input_codes) {
            $codes = array_map('trim', explode(',', $input_codes));
        }

        $sync = new CExternalMedecinSync();
        $sync->synchronizeSomeMedecins($step, $type, $codes);

        CAppUI::$localize = true;

        if ($errors = $sync->getErrors()) {
            CApp::log(
                CExternalMedecinSync::class . '::Errors : ' . count($errors),
                $errors,
                LoggerLevels::LEVEL_WARNING
            );
        }

        if ($updated = $sync->getUpdated()) {
            CApp::log(CExternalMedecinSync::class . '::Updated : ' . count($updated));
        }

        $stop = '1';
        if (count($errors) === 0 && count($updated) === 0) {
            $stop = '0';
        }

        CAppUI::stepAjax("Errors : " . (count($errors) . "\nUpdated : " . count($updated)));

        CAppUI::js("nextStep($stop)");
        CApp::rip();
    }

    public function cron_disable_exercice_places(): void
    {
        $this->checkPermEdit();

        $step = CView::get('step', 'num default|100');

        CView::checkin();

        if (!$this->checkSyncEnabled()) {
            CApp::rip();
        }

        $manager = new CMedecinExercicePlaceManager();
        $manager->removeOldMedecinExercicePlaces($step);
        // Ne pas d�sactiver les medecins sans lien vers des lieux d'exercice.
        // Ceci pourra �tre r�activ� plus tard si besoin.
        //$manager->disableMedecinsWithoutExercicePlace($step);

        foreach ($manager->getInfos() as $_info) {
            CApp::log($_info);
        }

        foreach ($manager->getErrors() as $_err) {
            CApp::log($_err, null, LoggerLevels::LEVEL_WARNING);
        }

        CApp::rip();
    }

    public function vw_rpps(): void
    {
        $this->checkPermAdmin();

        $this->renderSmarty('vw_rpps');
    }

    public function vw_sync_external(): void
    {
        $this->checkPermRead();

        CView::checkin();

        $sync = new CExternalMedecinSync();
        $avancement = $sync->getAvancement();

        $this->renderSmarty(
            'vw_sync_external',
            [
                'avancements' => [
                    'CPersonneExercice' => $avancement[CPersonneExercice::class],
                    'CSavoirFaire' => $avancement[CSavoirFaire::class],
                    'CDiplomeAutorisationExercice' => $avancement[CDiplomeAutorisationExercice::class],
                    'CMssanteInfos' => $avancement[CMssanteInfos::class],
                ],
            ]
        );
    }

    public function vw_sync_medecin(): void
    {
        $this->checkPermRead();

        CView::checkin();

        $medecin = new CMedecin();
        [$versions, $total] = $medecin->getSyncAvancement();

        $this->renderSmarty(
            'vw_sync_medecin',
            [
                'versions' => $versions,
                'total' => $total,
            ]
        );
    }

    public function vw_synchronisation_state(): void
    {
        $this->checkPermRead();

        $this->renderSmarty('vw_synchronisation_state');
    }

    public function populate(): void
    {
        $this->checkPermAdmin();

        $this->renderSmarty('populate');
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function ajaxLoadResource(): void
    {
        $this->checkPermAdmin();
        $identifiers = CView::get('identifier', 'str');
        $date = CView::get('date', 'str');
        CView::checkin();

        $resource = $identifiers === '' ?
            AnnuaireSanteService::SYNC_PRACTITIONER_ROLE :
            AnnuaireSanteService::SYNC_PRACTITIONER;

        $this->populateWithAPI($resource, $date, $identifiers);
        // TODO Afficher le bon d�roul� du populate
    }

    private function checkSyncEnabled(): bool
    {
        $ds = CSQLDataSource::get('rpps_import', true);
        if (!$ds || !$ds->hasTable('personne_exercice')) {
            return false;
        }

        return true;
    }

    /**
     * Populate the RPPS data with the ANS IRIS DP API
     * @throws ANSException
     * @throws InvalidArgumentException
     * @throws Exception
     */
    private function populateWithAPI(string $type_sync, string $date, ?string $identifiers): void
    {
        $annuaire_sante_service = new AnnuaireSanteService();

        $annuaire_sante_service->populate(
            $type_sync,
            $date,
            $identifiers
        );
    }

    /**
     * Populate the RPPS database with the bulkImport way, this download a zip which contains the practitioner and the
     * mssante datas
     * @throws CImportMedecinException
     */
    private function populateWithZip(): void
    {
        $downloader = new CRppsFileDownloader();

        // Download CPersonExercice file
        if ($downloader->downloadRppsFile(CRppsFileDownloader::DOWNLOAD_RPPS_FILE_URL)) {
            CAppUI::stepAjax('CRppsFileDownloader-msg-Info-RPPS file downloaded and extracted');
        }

        // Doawnload MSSante file
        if ($downloader->downloadRppsFile(CRppsFileDownloader::DOWNLOAD_MSSANTE_URL)) {
            CAppUI::stepAjax('CRppsFileDownloader-msg-Info-MSSante file downloaded and extracted');
        }

        // Bulk import files in Database
        $import = new CExternalMedecinBulkImport();
        $messages = $import->bulkImport();

        foreach ($messages as $_msg) {
            CAppUI::stepAjax(array_shift($_msg), UI_MSG_OK, ...$_msg);
        }
    }

    /**
     * Populate the RPPS database in a combiner way, if datas have already been imported, an API call is made to the
     * ANS IRIS DP endpoint, if not, we use the bulkImport way
     * @throws ANSException
     * @throws InvalidArgumentException
     * @throws Exception
     */
    private function populateCombined(): void
    {
        if (CModule::getActive('ans')) {
            $delta = CAppUI::conf('ans general delta_date', 'static');

            if ($delta) {
                $this->populateWithAPI(AnnuaireSanteService::SYNC_PRACTITIONER_ROLE, $delta, null);

                return;
            }
        }

        $this->populateWithZip();
    }
}
