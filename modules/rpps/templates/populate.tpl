{{*
* @package Mediboard\Rpps
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=rpps script=annuaire_sante}}

<form name="form_annuaire_sante" onsubmit="return AnnuaireSante.loadResource(this)">
  <input type="hidden" name="resources"/>
  <table class="form">
    <tr>
      <th>
          {{tr}}CExternalMedecinSync-Code{{/tr}}
      </th>
      <td>
        <input type="text" name="identifier">
      </td>
      <th>
          {{tr}}RPPS-msg-retrieve_delta{{/tr}}
      <td>
        <input type="text" name="date" placeholder="2022-01-01">
      </td>
      <td>
        <button type="button" onclick="this.form.onsubmit()">{{tr}}Lookup{{/tr}}</button>
      </td>
    </tr>
  </table>
</form>

<div id="annuaire_sante_results">

</div>
