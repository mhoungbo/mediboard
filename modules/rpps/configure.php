<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Import\Rpps\CExternalMedecinBulkImport;
use Ox\Import\Rpps\CRppsFileDownloader;

CCanDo::checkAdmin();

$bulk_import = new CExternalMedecinBulkImport();
try {
    $can_load_local = $bulk_import->canLoadLocalInFile();
} catch (Exception $e) {
    $can_load_local = null;
}

$file_downloader = new CRppsFileDownloader();
$is_downloadable = $file_downloader->isRppsFileDownloadable();

$smarty = new CSmartyDP();
$smarty->assign("can_load_local", $can_load_local);
$smarty->assign("is_downloadable", $is_downloadable);
$smarty->display("configure");
