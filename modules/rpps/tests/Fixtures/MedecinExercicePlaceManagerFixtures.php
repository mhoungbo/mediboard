<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\Rpps\Tests\Fixtures;

use Exception;
use Ox\Core\CMbDT;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CMedecinExercicePlace;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Class MedecinExercicePlaceManagerFixtures
 * @package Ox\Import\Rpps\Tests\Fixtures;
 */
class MedecinExercicePlaceManagerFixtures extends Fixtures implements GroupFixturesInterface
{
    /** @var string */
    final public const REF_MEDECIN = 'medecin_exercice_place_manager_medecin';

    /**
     * @throws Exception
     */
    public function load(): void
    {
        $medecin = CMedecin::getSampleObject();
        $this->store($medecin, self::REF_MEDECIN);

        $medecin_exercice_place = CMedecinExercicePlace::getSampleObject();
        $medecin_exercice_place->rpps_file_version = CMbDT::date("-40 DAY");
        $medecin_exercice_place->medecin_id = $medecin->_id;
        $this->store($medecin_exercice_place);

        $medecin_exercice_place = CMedecinExercicePlace::getSampleObject();
        $medecin_exercice_place->rpps_file_version = "";
        $medecin_exercice_place->medecin_id = $medecin->_id;
        $this->store($medecin_exercice_place);

        $medecin_exercice_place = CMedecinExercicePlace::getSampleObject();
        $medecin_exercice_place->medecin_id = $medecin->_id;
        $this->store($medecin_exercice_place);

        $medecin_exercice_place = CMedecinExercicePlace::getSampleObject();
        $medecin_exercice_place->rpps_file_version = CMbDT::date("-10 DAY");
        $medecin_exercice_place->medecin_id = $medecin->_id;
        $this->store($medecin_exercice_place);
    }

    /**
     * @return array
     */
    public static function getGroup(): array
    {
        return ['rpps'];
    }
}
