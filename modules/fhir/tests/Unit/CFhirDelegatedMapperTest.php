<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Unit;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbPath;
use Ox\Core\Module\CModule;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Serializers\CFHIRSerializer;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\Appointment\AppointmentFhirResourcesFixtures;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\Location\LocationFhirResourcesFixtures;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\Observation\ObservationFhirResourcesFixtures;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\Organization\OrganizationFhirResourcesFixtures;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\Patient\PatientFhirResourcesFixtures;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\Practitioner\PractitionerFhirResourcesFixtures;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\PractitionerRole\PractitionerRoleFhirResourcesFixtures;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\Schedule\ScheduleFhirResourcesFixtures;
use Ox\Interop\Fhir\Tests\Fixtures\Resources\Slot\SlotFhirResourcesFixtures;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response\CurlResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class CFhirDelegatedMapperTest
 *
 * @group   schedules
 */
class CFhirDelegatedMapperTest extends OxUnitTestCase
{
    public const TIMEOUT = 300;

    /** @var null|string */
    private static ?string $output_path = null;

    /* @var array|array[] */
    private static array $issues = [];

    /** @var array */
    private static array $resources = [
        AppointmentFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
        LocationFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
        ObservationFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
        OrganizationFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
        PatientFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
        PractitionerFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
        PractitionerRoleFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
        ScheduleFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
        SlotFhirResourcesFixtures::OBJECT_RESOURCE_COUPLE,
    ];

    /**
     * @throws TestsException
     * @throws Exception
     * @throws TransportExceptionInterface
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        if (!CModule::getActive("sourceCode") && $module = CModule::getInstalled('sourceCode')) {
            self::toogleActiveModule($module);
        }

        self::assertTrue(self::hasValidator());

        self::assertNotEmpty(self::getResources());
    }

    /**
     * @return array|CFHIRResource[]
     */
    private static function getResources(): array
    {
        if (self::$resources) {
            return self::$resources;
        }

        return [];
    }

    /**
     * @return array
     */
    public function providerScenariosFHIR(): array
    {
        $resources = [];
        /** @var array $resource */
        foreach (self::getResources() as $resource) {
            $resources = array_merge($resources, $resource);
        }

        return $resources;
    }

    /**
     * @param string $fhir_resource
     * @param string $object_class
     * @param string $fixture_ref
     * @param string $mapper
     *
     * @throws InvalidArgumentException
     * @throws TestsException
     * @throws TransportExceptionInterface
     * @dataProvider providerScenariosFHIR
     */
    public function testFhirResourcesXMLFormat(
        string $fhir_resource,
        string $object_class,
        string $fixture_ref,
        string $mapper
    ): void {
        /** @var CFHIRResource $resource */
        $resource = new $fhir_resource();

        $object_to_map = $this->getObjectFromFixturesReference(
            $object_class,
            $fixture_ref
        );

        $resource->setMapper(new $mapper());

        $resource->mapFrom($object_to_map);

        $is_valid = $this->validate($resource, 'xml');

        $this->assertTrue($is_valid, $this->getFailedMessage($resource, $fhir_resource, 'xml'));
    }

    /**
     * @param string $fhir_resource
     * @param string $object_class
     * @param string $fixture_ref
     * @param string $mapper
     *
     * @throws InvalidArgumentException
     * @throws TestsException
     * @throws TransportExceptionInterface
     * @dataProvider providerScenariosFHIR
     */
    public function testFhirResourcesJSONFormat(
        string $fhir_resource,
        string $object_class,
        string $fixture_ref,
        string $mapper
    ): void {
        $resource = new $fhir_resource();

        $object_to_map = $this->getObjectFromFixturesReference(
            $object_class,
            $fixture_ref
        );

        $resource->setMapper(new $mapper());

        $resource->mapFrom($object_to_map);

        $is_valid = $this->validate($resource);

        $this->assertTrue($is_valid, $this->getFailedMessage($resource, $fhir_resource, 'json'));
    }

    /**
     * @return bool
     * @throws TransportExceptionInterface
     * @throws CFHIRException
     */
    private static function hasValidator(): bool
    {
        $response = HttpClient::create()->request(
            Request::METHOD_GET,
            'http://fhir_validator',
            ['timeout' => self::TIMEOUT]
        );

        return $response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE;
    }

    /**
     * @param CFHIRResource $resource
     * @param string        $format
     *
     * @return bool
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws TransportExceptionInterface
     */
    private function validate(CFHIRResource $resource, string $format = 'json'): bool
    {
        $counter           = 0;
        $dir               = CAppUI::getTmpPath('fhir_scenario');
        $file_name         = uniqid('fhir_resource_scenario', true);
        $resource_path     = $dir . '/' . $file_name . ".$format";
        $package_version   = 'hl7-france-fhir.administrative';
        self::$output_path = $resource_output_path = $dir . '/output_' . $file_name . ".$format";

        $resource_profile = $resource->getProfile();
        $resource_version = $resource->getResourceFHIRVersion();

        // create tmp file with data
        CMbPath::forceDir($dir);

        // Serialize resource
        $serializer = CFHIRSerializer::serialize($resource, $format);
        $data       = $serializer->getResourceSerialized();

        file_put_contents($resource_path, $data);

        /** @var CurlResponse $response */
        $response = $this->validateWithDocker($package_version, $resource_path, $resource_version, $resource_profile);

        $haystack = $response->getContent(false);
        $needle   = 'class com.google.gson.JsonNull cannot be cast to class com.google.gson.JsonArray';

        $isOk                       = $response->getStatusCode() === Response::HTTP_OK;
        $contains_whitelisted_error = str_contains($haystack, $needle);

        while (!$isOk && $contains_whitelisted_error && $counter < 3) {
            $counter++;
            $response = $this->validateWithDocker(
                $package_version,
                $resource_path,
                $resource_version,
                $resource_profile
            );
            $isOk     = $response->getStatusCode() === Response::HTTP_OK;
        }

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->markTestSkipped('Test skipped due to Validator java error');
        }

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent(false));

        file_put_contents($resource_output_path, $response->getContent());

        $result_code = $this->parseResponse();

        return $result_code === 0;
    }

    /**
     * @param string $package_version
     * @param string $resource_path
     * @param string $resource_version
     * @param string $resource_profile
     *
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    private function validateWithDocker(
        string $package_version,
        string $resource_path,
        string $resource_version,
        string $resource_profile
    ): ResponseInterface {
        $formData = new FormDataPart(
            [
                'package_version'  => $package_version,
                'resource_file'    => DataPart::fromPath($resource_path),
                'resource_version' => $resource_version,
                'resource_profile' => $resource_profile,
            ]
        );

        return HttpClient::create()->request(
            Request::METHOD_POST,
            'http://fhir_validator',
            [
                'headers' => $formData->getPreparedHeaders()->toArray(),
                'body'    => $formData->bodyToIterable(),
                'timeout' => self::TIMEOUT,
            ],
        );
    }

    /**
     * @param string $format
     *
     * @return int : number of errors
     * @throws Exception
     */
    private function parseResponse(): int
    {
        $issues = [
            'error'       => [],
            'fatal'       => [],
            'warning'     => [],
            'information' => [],
        ];

        $outcome = json_decode(file_get_contents(self::$output_path));

        foreach ($outcome->issue as $issue) {
            $issues[$issue->severity][] = $issue->details->text;
        }

        self::$issues = $issues;

        return count($issues['error']) + count($issues['fatal']);
    }

    /**
     * @param CFHIRResource $resource
     * @param string        $title
     * @param string        $format
     *
     * @return string
     */
    private function getFailedMessage(CFHIRResource $resource, string $title, string $format): string
    {
        $message = "The test '$title' for the resource '" . get_class($resource) . "' in format '$format' has failed";

        if ($errors = $this->getErrors()) {
            $message .= "\n" . $errors;
        }

        if ($warnings = $this->getWarnings()) {
            $message .= "\n" . $warnings;
        }

        return $message;
    }

    /**
     * @return string|null
     */
    private function getErrors(): ?string
    {
        if (!$errors = CMbArray::get(self::$issues, 'error')) {
            return null;
        }

        return $this->formatAnnotation($errors, 'error');
    }

    /**
     * @return string|null
     */
    private function getWarnings(): ?string
    {
        if (!$warnings = CMbArray::get(self::$issues, 'warning')) {
            return null;
        }

        return $this->formatAnnotation($warnings, 'warning');
    }

    /**
     * @param array  $messages
     * @param string $type
     *
     * @return string
     */
    private function formatAnnotation(array $messages, string $type): string
    {
        $count   = count($messages);
        $content = "$type ($count) : ";
        foreach ($messages as $index => $message) {
            $content .= "\n\t[$index] : $message";
        }

        return $content;
    }
}
