<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Unit;

use Exception;
use Ox\Core\Config\Conf;
use Ox\Components\FhirCore\Model\R4\Resources\FHIRPatient;
use Ox\Interop\Fhir\Resources\R4\Bundle\CFHIRResourceBundle;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Interop\Fhir\Serializers\CFHIRParser;
use Ox\Interop\Fhir\Tests\Fixtures\FhirApiFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

class CFHIRInteractionsControllerTest extends OxWebTestCase
{
    /**
     * @throws TestsException
     * @throws Exception
     * @throws InvalidArgumentException
     * @dataProvider searchProvider
     */
    public function testSearch(array $params): void
    {
        /** @var CMediusers $mediuser */
        $mediuser = $this->getObjectFromFixturesReference(
            CMediusers::class,
            FhirApiFixtures::REF_USER_FHIR
        );

        $user = $mediuser->loadRefUser();

        $client = $this->createClient([], [], $user);
        $client->request('GET', '/api/fhir/Patient', $params);
        $this->assertResponseStatusCodeSame(200);

        if ($content = CFHIRParser::parse($client->getResponse()->getContent())) {
            /** @var CFHIRResourceBundle $bundle */
            $bundle = $content->getResource();

            $patient    = new CPatient();
            $nb_patient = $patient->countList();

            $this->assertTrue($bundle instanceof CFHIRResourceBundle);
            $this->assertEquals('Bundle', $bundle->getResourceType());
            $this->assertEquals($nb_patient, $bundle->getTotal()->getValue());
        }
    }

    /**
     * @param array $params
     *
     * @return void
     * @throws TestsException
     * @throws Exception
     * @throws InvalidArgumentException
     * @dataProvider searchWithParamsProvider
     */
    public function testSearchWithParams(array $params): void
    {
        /** @var CMediusers $mediuser */
        $mediuser = $this->getObjectFromFixturesReference(
            CMediusers::class,
            FhirApiFixtures::REF_USER_FHIR
        );

        $user = $mediuser->loadRefUser();

        $client = $this->createClient([], [], $user);
        $client->request('GET', '/api/fhir/Patient', $params);
        $this->assertResponseStatusCodeSame(200);

        if ($content = CFHIRParser::parse($client->getResponse()->getContent())) {
            /** @var CFHIRResourceBundle $bundle */
            $bundle = $content->getResource();

            $this->assertTrue($bundle instanceof CFHIRResourceBundle);
            $this->assertEquals('Bundle', $bundle->getResourceType());
            $this->assertEquals(1, $bundle->getTotal()->getValue());

            $entries     = $bundle->getEntry();
            $first_entry = reset($entries);
            $resource    = $first_entry->getResource();

            $this->assertNotNull($resource);
            $this->assertTrue($resource instanceof FHIRPatient);

            /** @var CFHIRResourcePatient $patient */
            $patient = $resource;

            $human_names         = $patient->getName();
            $human_name_datatype = reset($human_names);
            $givens              = $human_name_datatype->getGiven();
            $given               = reset($givens);
            $this->assertEquals('FHIR API PATIENT PRENOM', $given->getValue());
        }
    }

    public function searchProvider(): array
    {
        return [
            'All patients' => [
                [
                    '_format' => 'json',
                ],
            ],
        ];
    }

    public function searchWithParamsProvider(): array
    {
        return [
            'Fixture patient' => [
                [
                    '_format' => 'json',
                    'family'  => 'fhir_api_patient_nom',
                    'given'   => 'fhir_api_patient_prenom',
                ],
            ],
        ];
    }
}
