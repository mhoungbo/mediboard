<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Fixtures;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CModelObjectException;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\CMessageSupported;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\System\CConfiguration;
use Ox\Mediboard\System\CSenderHTTP;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Class FhirApiFixtures
 * @package Ox\Interop\Fhir\Tests\Fixtures
 */
class FhirApiFixtures extends Fixtures implements GroupFixturesInterface
{
    /** @var string */
    public const REF_USER_FHIR = 'fhir_user_api';

    /** @var string */
    public const REF_PATIENT_FHIR = 'fhir_patient_api';

    private CUser $user_fhir;

    private CSenderHTTP $sender_http;

    /**
     * @throws FixturesException
     * @throws CModelObjectException
     * @throws CMbException
     */
    public function load(): void
    {
        $this->user_fhir = $this->generateUser();
        $this->generatePatient();
        $this->generateActors();
    }

    /**
     * @return CUser
     * @throws FixturesException
     */
    private function generateUser(): CUser
    {
        /** @var CMediusers $mediuser */
        $mediuser                 = $this->getUsers(1)[0];
        $mediuser->_id            = null;
        $mediuser->_user_username = 'fhir_api_user_fixture';
        $this->store($mediuser, self::REF_USER_FHIR);

        $perm_module             = new CPermModule();
        $perm_module->user_id    = $mediuser->_id;
        $perm_module->mod_id     = null;
        $perm_module->view       = 2;
        $perm_module->permission = 2;
        $this->store($perm_module);

        return $mediuser->loadRefUser();
    }

    /**
     * @return array
     */
    public static function getGroup(): array
    {
        return ['fhir_api', 100];
    }

    /**
     * @throws FixturesException
     * @throws CMbException
     */
    private function generateActors(): void
    {
        $this->sender_http = $this->generateSender();
        $list_messages     = $this->getListMessages();
        $this->generateMessage($this->sender_http, $list_messages);
    }

    /**
     * @return array[]
     */
    private function getListMessages(): array
    {
        $patient_fhir = new CFHIRResourcePatient();

        return [
            [
                'version'       => '4.0',
                'profil'        => 'CPDQm',
                'message'       => 'CFHIRInteractionSearch',
                'transaction'   => $patient_fhir->getProfile(),
                'configuration' => [
                    [
                        'path'  => "fhir delegated_objects delegated_searcher",
                        'value' => 'PatientPDQm',
                    ],
                    [
                        'path'  => "fhir delegated_objects delegated_mapper",
                        'value' => 'Patient',
                    ],
                ],
            ],
            [
                'version'     => '4.0',
                'profil'      => 'CPDQm',
                'message'     => 'CFHIRInteractionRead',
                'transaction' => $patient_fhir->getProfile(),
            ],
        ];
    }

    /**
     * @return CSenderHTTP
     * @throws FixturesException
     * @throws Exception
     */
    private function generateSender(): CSenderHTTP
    {
        $sender           = new CSenderHTTP();
        $sender->nom      = 'fhir_fixture_sender';
        $sender->user_id  = $this->user_fhir->_id;
        $sender->role     = CAppUI::conf('instance_role');
        $sender->group_id = CGroups::loadCurrent()->_id;
        $this->store($sender);

        return $sender;
    }

    /**
     * @param CInteropActor $actor
     * @param array         $list_messages
     *
     * @return array
     * @throws FixturesException
     * @throws CMbException
     * @throws Exception
     */
    private function generateMessage(CInteropActor $actor, array $list_messages): void
    {
        foreach ($list_messages as $content_message) {
            $message = new CMessageSupported();
            $message->setObject($actor);
            $message->profil      = CMbArray::get($content_message, 'profil');
            $message->transaction = CMbArray::get($content_message, 'transaction');
            $message->message     = CMbArray::get($content_message, 'message');
            $message->version     = CMbArray::get($content_message, 'version');
            $message->active      = 1;
            $this->store($message);

            if ($configs = $content_message['configuration'] ?? null) {
                foreach ($configs as $config) {
                    if ($msg = CConfiguration::setConfig($config['path'], $config['value'], $message)) {
                        throw new CMbException($msg);
                    }
                }
            }
        }
    }

    /**
     * @return void
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function generatePatient(): void
    {
        /** @var CPatient $patient */
        $patient                  = CPatient::getSampleObject();
        $patient->nom             = 'fhir_api_patient_nom';
        $patient->nom_jeune_fille = 'fhir_patient_fixtures';
        $patient->prenom          = 'fhir_api_patient_prenom';
        $patient->naissance       = CMbDT::date('2000-01-01');
        $this->store($patient, self::REF_PATIENT_FHIR);
    }

    /**
     * @return CSejour
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function generateSejour(): CSejour
    {
        $user_id = $this->getUser()->_id;

        $patient = FhirResourcesHelper::getSamplePatient();
        $this->store($patient);

        $group = FhirResourcesHelper::getSampleFhirGroups();
        $this->store($group);

        $sejour               = FhirResourcesHelper::getSampleFhirSejour();
        $sejour->patient_id   = $patient->_id;
        $sejour->praticien_id = $user_id;
        $sejour->group_id     = $group->_id;
        $this->store($sejour);

        return $sejour;
    }

    /**
     * @return void
     * @throws FixturesException
     * @throws Exception
     */
    public function purge(): void
    {
        parent::purge();

        $users                = new CUser();
        $users->user_username = 'fhir_api_user_fixture';
        if ($users->loadMatchingObject()) {
            if ($msg = $users->purge()) {
                throw new FixturesException($msg);
            }
        }
    }
}
