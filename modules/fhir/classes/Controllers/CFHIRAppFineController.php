<?php

/**
 * @package Mediboard\Fhir\Controllers
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Controllers;

use Exception;
use Ox\AppFine\Server\Controllers\Tiers\AccountTiersController;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIROperationOutcomeIssue;
use Ox\Core\CAppUI;
use Ox\Interop\Fhir\Actors\CSenderFHIR;
use Ox\Interop\Fhir\Api\Request\CRequestFHIR;
use Ox\Interop\Fhir\Api\Request\CRequestFormats;
use Ox\Interop\Fhir\Api\Response\CFHIRResponse;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Exception\CFHIRExceptionInformational;
use Ox\Interop\Fhir\Exception\CFHIRExceptionNotFound;
use Ox\Interop\Fhir\Interactions\CFHIRInteraction;
use Ox\Interop\Fhir\Resources\R4\OperationOutcome\CFHIRResourceOperationOutcome;
use Ox\Interop\Fhir\ValueSet\CFHIRIssueType;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CAppFineController
 * @package Ox\Interop\Fhir\Controllers
 * @deprecated All client must be upgrade to 07_23
 */
class CFHIRAppFineController extends CFHIRController
{
    /**
     * Create patient user for AppFine
     *
     * @param CRequestFHIR $request Request
     *
     * @return Response
     * @throws Exception
     * @deprecated to remove when all client will be migrated on 2023_07
     *
     * @api
     */
    public function patient_user_appFine(CRequestFHIR $request): Response
    {
        $sender_fhir = new CSenderFHIR();
        $sender_fhir->loadFromUser(CUser::get());

        $sender = $sender_fhir->getSender();
        if (!$sender || !$sender->_id) {
            throw new CFHIRExceptionInformational(CAppUI::tr("CSenderHTTP.none"));
        }

        // re routage vers la route AppFine
        $controller = AccountTiersController::class . '::' . 'linkPatientWithEstablishment';
        $response = $this->forward($controller);

        $data = $response->getContent();
        $data = json_decode($data ?? '', true);

        // traitement de la r�ponse JsonApi pour la transformer en r�ponse FHIR
        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $diag = $data['data']['attributes']['patient_id'] ?? null;
            if ($diag === null) {
                throw new CFHIRExceptionNotFound(CAppUI::tr("CPatientUser.none"));
            }

            $resource = new CFHIRResourceOperationOutcome();
            $resource->addIssue(
                (new FHIROperationOutcomeIssue())
                    ->setCode(CFHIRIssueType::TYPE_INFORMATIONAL)
                    ->setDiagnostics("+++{$diag}+++")
            );

            $format      = (new CRequestFormats($request->getRequest()))->getFormat();
            $interaction = new CFHIRInteraction();
            $interaction->format = $format;
            $interaction->setResource($resource);

            return $this->renderFHIRResponse(new CFHIRResponse($interaction, $format));
        } else {
            $body = $response->getContent();
            $json = json_decode($body, true);
            $message = $json['errors']['message'] ?? null;

            $exception = $message ? new CFHIRException($message) : new CFHIRException();

            return $exception->makeResponse($request->getFormat());
        }
    }
}
