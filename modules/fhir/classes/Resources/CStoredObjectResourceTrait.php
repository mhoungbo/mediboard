<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRMeta;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCanonical;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRId;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Mediboard\Sante400\CIdSante400;

/**
 * Description
 */
trait CStoredObjectResourceTrait
{
    /** @var CStoredObject */
    protected $object;

    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CStoredObject && $object->_id;
    }

    /**
     * Map property Id
     *
     * @return FHIRString|null
     * @throws Exception
     */
    public function mapId(): ?FHIRString
    {
        if ($this->object && $this->object->_id) {
            return (new FHIRString())
                ->setValue($this->getInternalId($this->object));
        }

        return null;
    }

    /**
     * Get internal id of object or use idex when receiver is present
     *
     * @param CStoredObject $object
     *
     * @return string
     * @throws Exception
     */
    private function getInternalId(CStoredObject $object): string
    {
        $receiver = $this->resource && $this->resource->getReceiver() ? $this->resource->getReceiver() : null;

        // If we send a resource (POST | PUT)
        if ($receiver) {
            $idex = CIdSante400::getMatch($object->_class, $receiver->_tag_fhir, null, $object->_id);
            if ($idex && $idex->_id) {
                return $idex->id400;
            }
        }

        return $object->getUuid();
    }

    /**
     * Map property Meta
     *
     * @return FHIRMeta|null
     * @throws Exception
     */
    public function mapMeta(): ?FHIRMeta
    {
        // meta
        $meta = new FHIRMeta();
        // meta / versionID|lastUpdated
        if (!$this->resource->isContained()) {
            if ($this->object && $this->object->_id) {
                $last_log          = $this->object->loadLastLog();
                $meta
                    ->setVersionId($last_log->_id)
                    ->setLastUpdated(CFHIR::getTimeUtc($last_log->date, false));
            } else {
                $meta->setLastUpdated((new FHIRInstant())->setValue(CMbDT::dateTime()));
            }
        }

        // meta / profile
        if (!$this->resource->isFHIRResource()) {
            $profiles          = [$this->resource->getProfile()];
            $existing_profiles = $this->resource->getMeta() ? $this->resource->getMeta()->getProfile() : [];
            $meta->setProfile(...array_merge($existing_profiles, $profiles));
        }

        return $meta;
    }

    /**
     * Map property ImplicitRules
     *
     * @return FHIRUri|null
     */
    public function mapImplicitRules(): ?FHIRUri
    {
        return null;
    }

    /**
     * Map property Language
     *
     * @return FHIRCode|null
     */
    public function mapLanguage(): ?FHIRCode
    {
        return (new FHIRCode())->setValue('fr-FR');
    }
}
