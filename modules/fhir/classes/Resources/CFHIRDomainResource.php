<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources;

use Exception;
use Ox\Core\CMbSecurity;
use Ox\Core\CStoredObject;
use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Interfaces\FHIRDomainResourceInterface;
use Ox\Components\FhirCore\Interfaces\FHIRResourceInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRNarrative;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Resources\FHIRResource;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

/**
 * FHIR generic resource
 */
abstract class CFHIRDomainResource extends CFHIRResource
{
    protected ?FHIRNarrative $text = null;

    /** @var FHIRDomainResourceInterface $fhirResource */
    protected $fhirResource;

    /** @var ResourceDomainMappingInterface */
    protected $object_mapping;

    /** @var bool */
    public $_use_contained = false;

    /**
     * @param string|CFHIRResource $resource_or_class
     * @param CStoredObject $object
     *
     * @return FHIRReference
     * @throws InvalidArgumentException|ReflectionException
     */
    public function addReference($resource_or_class, ?CStoredObject $object = null): FHIRReference
    {
        $resource_reference = parent::addReference($resource_or_class, $object);
        if ($this->isContainedActivated()) {
            if ($should_make_mapping = is_string($resource_or_class)) {
                $resource_or_class = new $resource_or_class();
            }
            $contained_resource               = $this->buildFrom($resource_or_class);
            $contained_resource->summary      = true;
            $contained_resource->is_contained = true;

            // mapping only if class is given in parameter
            if ($should_make_mapping) {
                $contained_resource->mapFrom($object);
            }

            // force id for concordance between reference and resource
            $contained_resource->setId(
                (new FHIRString())
                    ->setValue(substr($resource_reference->getReference()->getValue(), 1)));

            // add in contained resources
            $this->addContained($contained_resource->getFhirResource());
        }

        return $resource_reference;
    }

    /**
     * @param FHIRExtension ...$modifierExtension
     *
     * @return CFHIRDomainResource
     */
    public function setModifierExtension(FHIRExtension ...$modifierExtension): CFHIRDomainResource
    {
        $this->fhirResource->setModifierExtension(...$modifierExtension);

        return $this;
    }

    /**
     * @param FHIRExtension ...$modifierExtension
     *
     * @return CFHIRDomainResource
     */
    public function addModifierExtension(FHIRExtension ...$modifierExtension): CFHIRDomainResource
    {
        $this->fhirResource->addModifierExtension(...$modifierExtension);

        return $this;
    }

    /**
     * @return FHIRExtension[]
     */
    public function getModifierExtension(): array
    {
        return $this->fhirResource->getModifierExtension();
    }

    /**
     * @return FHIRExtension[]
     */
    public function mapModifierExtension(): void
    {
        $this->fhirResource->setModifierExtension(...$this->object_mapping->mapModifierExtension());
    }

    /**
     * @param FHIRNarrative|null $text
     *
     * @return CFHIRDomainResource
     */
    public function setText(?FHIRNarrative $text): CFHIRDomainResource
    {
        $this->fhirResource->setText($text);

        return $this;
    }

    /**
     * @return FHIRNarrative|null
     */
    public function getText(): ?FHIRNarrative
    {
        return $this->fhirResource->getText();
    }

    /**
     * Map property Text
     */
    public function mapText(): void
    {
        $this->text = $this->object_mapping->mapText();
    }

    public function mapContained(): void
    {
        $this->addContained(...$this->object_mapping->mapContained());
    }

    /**
     * @param FHIRResourceInterface ...$contained
     *
     * @return CFHIRDomainResource
     */
    public function setContained(FHIRResourceInterface ...$contained): CFHIRDomainResource
    {
        $this->fhirResource->setContained(...$contained);

        return $this;
    }

    /**
     * @param FHIRResourceInterface ...$contained
     *
     * @return CFHIRDomainResource
     */
    public function addContained(FHIRResourceInterface ...$contained): CFHIRDomainResource
    {
        $this->fhirResource->addContained(...$contained);

        return $this;
    }

    /**
     * @return FHIRResourceInterface[]
     */
    public function getContained(): array
    {
        return $this->fhirResource->getContained();
    }

    /**
     * @param FHIRExtension ...$extension
     *
     * @return CFHIRDomainResource
     */
    public function setExtension(FHIRExtension ...$extension): CFHIRDomainResource
    {
        $this->fhirResource->setExtension(...$extension);

        return $this;
    }

    /**
     * @param FHIRExtension ...$extension
     *
     * @return CFHIRDomainResource
     */
    public function addExtension(FHIRExtension ...$extension): CFHIRDomainResource
    {
        $this->fhirResource->addExtension(...$extension);

        return $this;
    }

    /**
     * @return FHIRExtension[]
     */
    public function getExtension(): array
    {
        return $this->fhirResource->getExtension();
    }

    /**
     * @return FHIRIdentifier[]
     */
    public function getIdentifier()
    {
        return $this->fhirResource->getIdentifier();
    }

    /**
     * @param FHIRIdentifier ...$identifier
     *
     * @return CFHIRDomainResource
     */
    public function setIdentifier(FHIRIdentifier ...$identifier): CFHIRDomainResource
    {
        $this->fhirResource->setIdentifier(...$identifier);

        return $this;
    }

    /**
     * @param FHIRIdentifier ...$identifier
     *
     * @return CFHIRDomainResource
     */
    public function addIdentifier(FHIRIdentifier ...$identifier): CFHIRDomainResource
    {
        $this->fhirResource->addIdentifier(...$identifier);

        return $this;
    }

    /**
     * @param CFHIRResource $resource
     * @param CStoredObject        $object
     *
     * @return string
     * @throws Exception
     */
    protected function getResourceIdentifier(CFHIRResource $resource, ?CStoredObject $object): string
    {
        if ($this->isContainedActivated()) {
            $identifier = ($object && $object->_id) ? $this->getInternalId($object) : CMbSecurity::generateUUID();

            return "#" . $identifier;
        }

        return parent::getResourceIdentifier($resource, $object);
    }

    /**
     * @return bool
     */
    protected function isContainedActivated(): bool
    {
        // todo utiliser une config ? un params dans l'url ?
        return $this->_use_contained;
    }

    /**
     * Map property extension
     */
    protected function mapExtension(): void
    {
        $this->fhirResource->addExtension(...$this->object_mapping->mapExtension());
    }

    protected function mapIdentifier(): void
    {
        $this->fhirResource->addIdentifier(...$this->object_mapping->mapIdentifier());
    }

    /**
     * @return bool
     */
    public function isSummary(): bool
    {
        return $this->summary;
    }

    /**
     * Search the first resource in contained field which match with the type
     *
     * @param string      $type
     * @param string|null $with_id
     *
     * @return FHIRResource|null
     */
    public function getContainedOfType(string $type, string $with_id = null): ?FHIRResource
    {
        /** @var FHIRResource $resource */
        foreach ($this->fhirResource->getContained() as $resource) {
            if ($resource instanceof $type) {
                if ($with_id) {
                    if ($resource->getId()->getValue() !== $with_id) {
                        continue;
                    }
                }

                return $resource;
            }
        }

        return null;
    }
}
