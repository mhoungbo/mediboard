<?php

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Schedule;

use Exception;
use Ox\Interop\Fhir\Contracts\Mapping\R4\ScheduleMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceScheduleInterface;
use Ox\Components\FhirCore\Interfaces\FHIRScheduleInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionHistory;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterDate;

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Class CFHIRResourceSchedule
 */
class CFHIRResourceSchedule extends CFHIRDomainResource implements ResourceScheduleInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Schedule';

    // attributes
    /** @var FHIRScheduleInterface $fhirResource */
    protected $fhirResource;

    /** @var ScheduleMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->setInteractions(
                [
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionHistory::NAME,
                ]
            )
            ->addSearchAttributes(
                [
                    new SearchParameterDate('start'),
                ]
            );
    }

    /**
     * @param FHIRBoolean|null $active
     *
     * @return CFHIRResourceSchedule
     */
    public function setActive(?FHIRBoolean $active): CFHIRResourceSchedule
    {
        $this->fhirResource->setActive($active);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getActive(): ?FHIRBoolean
    {
        return $this->fhirResource->getActive();
    }

    /**
     * @param FHIRPeriod|null $planningHorizon
     *
     * @return CFHIRResourceSchedule
     */
    public function setPlanningHorizon(?FHIRPeriod $planningHorizon): CFHIRResourceSchedule
    {
        $this->fhirResource->setPlanningHorizon($planningHorizon);

        return $this;
    }

    /**
     * @return FHIRPeriod|null
     */
    public function getPlanningHorizon(): ?FHIRPeriod
    {
        return $this->fhirResource->getPlanningHorizon();
    }

    /**
     * @param FHIRString|null $comment
     *
     * @return CFHIRResourceSchedule
     */
    public function setComment(?FHIRString $comment): CFHIRResourceSchedule
    {
        $this->fhirResource->setComment($comment);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getComment(): ?FHIRString
    {
        return $this->fhirResource->getComment();
    }

    /**
     * Map property active
     */
    protected function mapActive(): void
    {
        // not implemented
        $this->fhirResource->setActive($this->object_mapping->mapActive());
    }

    /**
     * Map property serviceCategory
     */
    protected function mapServiceCategory(): void
    {
        $this->fhirResource->setServiceCategory(...$this->object_mapping->mapServiceCategory());
    }

    /**
     * Map property serviceType
     * @throws Exception
     */
    protected function mapServiceType(): void
    {
        $this->fhirResource->setServiceType(...$this->object_mapping->mapServiceType());
    }

    /**
     * Map property specialty
     */
    protected function mapSpecialty(): void
    {
        $this->fhirResource->setSpecialty(...$this->object_mapping->mapSpecialty());
    }

    /**
     * Map property actor
     * @throws Exception
     */
    protected function mapActor(): void
    {
        $this->fhirResource->setActor(...$this->object_mapping->mapActor());
    }

    /**
     * Map property planningHorizon
     * @throws Exception
     */
    protected function mapPlanningHorizon(): void
    {
        $this->fhirResource->setPlanningHorizon($this->object_mapping->mapPlanningHorizon());
    }

    /**
     * Map property comment
     */
    protected function mapComment(): void
    {
        $this->fhirResource->setComment($this->object_mapping->mapComment());
    }

    /**
     * @param FHIRCodeableConcept ...$serviceCategory
     *
     * @return self
     */
    public function setServiceCategory(FHIRCodeableConcept ...$serviceCategory): self
    {
        $this->fhirResource->setServiceCategory(...$serviceCategory);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$serviceCategory
     *
     * @return self
     */
    public function addServiceCategory(FHIRCodeableConcept ...$serviceCategory): self
    {
        $this->fhirResource->addServiceCategory(...$serviceCategory);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceCategory(): array
    {
        return $this->fhirResource->getServiceCategory();
    }

    /**
     * @param FHIRCodeableConcept ...$serviceType
     *
     * @return self
     */
    public function setServiceType(FHIRCodeableConcept ...$serviceType): self
    {
        $this->fhirResource->setServiceType(...$serviceType);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$serviceType
     *
     * @return self
     */
    public function addServiceType(FHIRCodeableConcept ...$serviceType): self
    {
        $this->fhirResource->addServiceType(...$serviceType);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceType(): array
    {
        return $this->fhirResource->getServiceType();
    }

    /**
     * @param FHIRCodeableConcept ...$specialty
     *
     * @return self
     */
    public function setSpecialty(FHIRCodeableConcept ...$specialty): self
    {
        $this->fhirResource->setSpecialty(...$specialty);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$specialty
     *
     * @return self
     */
    public function addSpecialty(FHIRCodeableConcept ...$specialty): self
    {
        $this->fhirResource->addSpecialty(...$specialty);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->fhirResource->getSpecialty();
    }

    /**
     * @param FHIRReference ...$actor
     *
     * @return self
     */
    public function setActor(FHIRReference ...$actor): self
    {
        $this->fhirResource->setActor(...$actor);

        return $this;
    }

    /**
     * @param FHIRReference ...$actor
     *
     * @return self
     */
    public function addActor(FHIRReference ...$actor): self
    {
        $this->fhirResource->addActor(...$actor);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getActor(): array
    {
        return $this->fhirResource->getActor();
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRSchedule';
    }
}
