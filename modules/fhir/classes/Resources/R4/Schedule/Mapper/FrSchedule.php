<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Schedule\Mapper;

use Exception;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInteger;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Profiles\CFHIRInteropSante;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceTrait;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Interop\Fhir\Resources\R4\Schedule\Profiles\InteropSante\CFHIRResourceScheduleFR;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Patients\CMedecin;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Description
 */
class FrSchedule extends Schedule
{
    use CStoredObjectResourceTrait;

    /** @var CPlageconsult */
    protected $object;

    /** @var CFHIRResourceScheduleFR */
    protected CFHIRResource $resource;

    public function onlyProfiles(): array
    {
        return [CFHIRInteropSante::class];
    }

    /**
     * @throws Exception
     */
    public function mapExtension(): array
    {
        return [
            (new FHIRExtension())
                ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrScheduleAvailabiltyTime')
                ->setExtension(
                    (new FHIRExtension())
                        ->setUrl('type')
                        ->setValue(
                            (new FHIRCoding())
                                ->setSystem('http://interopsante.org/fhir/CodeSystem/fr-schedule-type')
                                ->setCode($this->object->locked ? 'busy-unavailable' : 'free')
                                ->setDisplay($this->object->locked ? 'Indisponibilit�' : 'Disponibilit�')
                        ),
                    // TODO Extension en erreur � corriger
                    /*(new FHIRExtension())
                        ->setUrl('rrule')
                        ->setExtension(...$this->loadRuleExtensions()),*/
                    (new FHIRExtension())
                        ->setUrl('start')
                        ->setValue(
                            (new FHIRDateTime())
                                ->setValue(CFHIR::getTimeUtc($this->object->debut, false))
                        ),
                    (new FHIRExtension())
                        ->setUrl('end')
                        ->setValue(
                            (new FHIRDateTime())
                                ->setValue(CFHIR::getTimeUtc($this->object->fin, false))
                        ),
                    (new FHIRExtension())
                        ->setUrl('identifier')
                        ->setValue(
                            (new FHIRDateTime())
                                ->setValue(CFHIR::getTimeUtc($this->resource->getIdentifier()[0], false))
                        ),
                )
        ];
    }

    /**
     * @throws Exception
     */
    public function mapSpecialty(): array
    {
        $specialty = [];

        $practitioner = $this->object->loadRefChir();

        if ($practitioner && $practitioner->_id) {
            $coding = $this->resource->setPractitionerSpecialty($practitioner);

            $specialty[] = (new FHIRCodeableConcept())
                ->setCoding(...$coding);
        }

        return $specialty;
    }

    /**
     * Map property actor
     * @throws Exception|InvalidArgumentException
     */
    public function mapActor(): array
    {
        // Les actors sont � dynamiser en fonction du besoin, pour le projectathon nous avons �change un practitioner
        $practitioner  = $this->object->loadRefChir();
        $medecin       = new CMedecin();
        $medecin->rpps = $practitioner->rpps;
        $medecin->loadMatchingObject();

        return [$this->resource->addReference(CFHIRResourcePractitionerFR::class, $medecin)];
    }

    /**
     * @return FHIRExtension[]
     */
    private function loadRuleExtensions(): array
    {
        $until    = $this->object->fin;
        $interval = $this->object->_freq_minutes;
        $count    = 60 / $interval;

        return [
            (new FHIRExtension())
                ->setUrl('rrule')
                ->setExtension(
                    (new FHIRExtension())
                        ->setUrl('freq')
                        ->setValue(
                            (new FHIRCoding())
                                ->setSystem('https://www.ietf.org/rfc/rfc2445')
                                ->setCode('MINUTELY')
                                ->setDisplay('Par minute')
                        ),
                    (new FHIRExtension())
                        ->setUrl('until')
                        ->setValue($until),
                    (new FHIRExtension())
                        ->setUrl('count')
                        ->setValue((new FHIRInteger())->setValue($count)),
                    (new FHIRExtension())
                        ->setUrl('interval')
                        ->setValue($interval),
                )
        ];
    }
}
