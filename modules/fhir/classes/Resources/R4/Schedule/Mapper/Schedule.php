<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Schedule\Mapper;

use Exception;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\ScheduleMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Resources\R4\Schedule\CFHIRResourceSchedule;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Patients\CMedecin;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Description
 */
class Schedule implements DelegatedObjectMapperInterface, ScheduleMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CPlageconsult */
    protected $object;

    /** @var CFHIRResourceSchedule */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    public function onlyRessources(): array
    {
        return [CFHIRResourceSchedule::class];
    }

    /**
     * @param CFHIRResource $resource
     * @param               $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CPlageconsult && $object->_id;
    }

    public function mapActive(): ?FHIRBoolean
    {
        return new FHIRBoolean(!$this->object->locked);
    }

    public function mapServiceCategory(): array
    {
        $system      = 'http://terminology.hl7.org/CodeSystem/service-category';
        $code        = '35';
        $displayName = 'Hospital';
        $text        = 'Hospital';

        return [
            (new FHIRCodeableConcept())
                ->setText($text)
                ->setCoding(
                    (new FHIRCoding())
                        ->setSystem($system)
                        ->setCode($code)
                        ->setDisplay($displayName)
                ),
        ];
    }

    public function mapServiceType(): array
    {
        // not implemented
        return [];
    }

    public function mapSpecialty(): array
    {
        return [];
    }

    /**
     * @throws Exception|InvalidArgumentException
     */
    public function mapActor(): array
    {
        // Les actors sont � dynamiser en fonction du besoin, pour le projectathon nous avons �change un practitioner
        $practitioner  = $this->object->loadRefChir();
        $medecin       = new CMedecin();
        $medecin->rpps = $practitioner->rpps;
        $medecin->loadMatchingObject();

        return [$this->resource->addReference(CFHIRResourcePractitioner::class, $medecin)];
    }

    /**
     * @throws Exception
     */
    public function mapPlanningHorizon(): ?FHIRPeriod
    {
        return (new FHIRPeriod())
            ->setStart(CFHIR::getTimeUtc($this->object->debut, false))
            ->setEnd(CFHIR::getTimeUtc($this->object->fin, false));
    }

    public function mapComment(): ?FHIRString
    {
        // not implemented
        return null;
    }
}
