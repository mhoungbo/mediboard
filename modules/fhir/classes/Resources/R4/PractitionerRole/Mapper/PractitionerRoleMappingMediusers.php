<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\PractitionerRole\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\PractitionerRoleMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\CFHIRResourcePractitionerRole;
use Ox\Mediboard\Mediusers\CMediusers;

class PractitionerRoleMappingMediusers implements DelegatedObjectMapperInterface, PractitionerRoleMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CMediusers */
    protected $object;

    /** @var CFHIRResourcePractitionerRole */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourcePractitionerRole::class];
    }

    public function mapActive(): ?FHIRBoolean
    {
        return (new FHIRBoolean())->setValue($this->object->actif);
    }

    /**
     * @throws \Exception
     */
    public function mapPeriod(): ?FHIRPeriod
    {
        $mediuser = $this->object;

        $period = null;
        if ($mediuser->deb_activite && $mediuser->fin_activite) {
            $period = (new FHIRPeriod())
                ->setStart(CFHIR::getTimeUtc($mediuser->deb_activite, false))
                ->setEnd(CFHIR::getTimeUtc($mediuser->fin_activite, false));
        }

        return $period;
    }

    public function mapPractitioner(): ?FHIRReference
    {
        return $this->resource->addReference(CFHIRResourcePractitioner::class, $this->object);
    }

    public function mapOrganization(): ?FHIRReference
    {
        // not implemented
        return null;
    }

    public function mapCode(): array
    {
        $system  = 'urn:oid:2.16.840.1.113883.4.642.3.439';
        $code    = 'doctor';
        $display = 'Doctor';
        $text    = 'A qualified/registered medical practitioner';
        $coding = (new FHIRCoding())
            ->setSystem($system)
            ->setCode($code)
            ->setDisplay($display);

        return [
            (new FHIRCodeableConcept())
                ->setCoding($coding)
                ->setText($text),
        ];
    }

    public function mapSpecialty(): array
    {
        return [];
    }

    public function mapLocation(): array
    {
        // not implemented
        return [];
    }

    public function mapHealthCareService(): array
    {
        // not implemented
        return [];
    }

    public function mapTelecom(): array
    {
        // not implemented
        return [];
    }

    public function mapAvailableTime(): array
    {
        // not implemented
        return [];
    }

    public function mapNotAvailable(): array
    {
        // not implemented
        return [];
    }

    public function mapAvailabilityExceptions(): ?FHIRString
    {
        // not implemented
        return null;
    }

    public function mapEndpoint(): array
    {
        // not implemented
        return [];
    }
}
