<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\PractitionerRole\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\PractitionerRoleMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\CFHIRResourcePractitionerRole;
use Ox\Mediboard\Patients\CMedecin;

class PractitionerRoleMappingMedecin implements DelegatedObjectMapperInterface, PractitionerRoleMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CMedecin */
    protected $object;

    /** @var CFHIRResourcePractitionerRole */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourcePractitionerRole::class];
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CMedecin && $object->_id;
    }

    public function mapActive(): ?FHIRBoolean
    {
        return (new FHIRBoolean())->setValue($this->object->actif);
    }

    public function mapPeriod(): ?FHIRPeriod
    {
        return null;
    }

    public function mapPractitioner(): ?FHIRReference
    {
        return $this->resource->addReference(CFHIRResourcePractitioner::class, $this->object);
    }

    public function mapOrganization(): ?FHIRReference
    {
        // mapping sur quel exercice_place ? groups current ?
        return null;
    }

    public function mapCode(): array
    {
        $codes   = [];
        $medecin = $this->object;

        // todo gestion des valueSet FHIR
        switch ($medecin->type) {
            case 'medecin':
                $code    = 'doctor';
                $display = $text = 'Doctor';
                break;

            case 'pharmacie':
                $code    = 'pharmacist';
                $display = $text = 'Pharmacist';
                break;

            case 'infirmier':
                $code    = 'nurse';
                $display = $text = 'Nurse';
                break;
            default:
                return $codes;
        }

        $system     = 'http://terminology.hl7.org/CodeSystem/practitioner-role';
        $profession = (new FHIRCoding())
            ->setSystem($system)
            ->setCode($code)
            ->setDisplay($display);

        $codes[] = (new FHIRCodeableConcept())
            ->setCoding($profession)
            ->setText($text);

        return $codes;
    }

    public function mapSpecialty(): array
    {
        return [];
    }

    public function mapLocation(): array
    {
        return [];
    }

    public function mapHealthCareService(): array
    {
        return [];
    }

    public function mapTelecom(): array
    {
        $telecoms = [];
        if ($this->object->tel) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel)
                ->setUse('work');
        }

        if ($this->object->fax) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('fax')
                ->setValue($this->object->fax)
                ->setUse('work');
        }

        if ($this->object->email) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('email')
                ->setValue($this->object->email)
                ->setUse('work');
        }

        return $telecoms;
    }

    public function mapAvailableTime(): array
    {
        return [];
    }

    public function mapNotAvailable(): array
    {
        return [];
    }

    public function mapAvailabilityExceptions(): ?FHIRString
    {
        return null;
    }

    public function mapEndpoint(): array
    {
        return [];
    }
}
