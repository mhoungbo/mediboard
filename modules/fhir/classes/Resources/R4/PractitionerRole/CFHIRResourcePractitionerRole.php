<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\PractitionerRole;

use Ox\Interop\Fhir\Contracts\Mapping\R4\PractitionerRoleMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourcePractitionerRoleInterface;
use Ox\Components\FhirCore\Interfaces\FHIRPractitionerRoleInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPractitionerRoleAvailableTime;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPractitionerRoleNotAvailable;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterString;

/**
 * FHIR practitionerRole resource
 */
class CFHIRResourcePractitionerRole extends CFHIRDomainResource implements ResourcePractitionerRoleInterface
{
    // constants
    /** @var string Resource type */
    public const RESOURCE_TYPE = "PractitionerRole";

    // attributes
    /** @var FHIRPractitionerRoleInterface $fhirResource */
    protected $fhirResource;

    /** @var PractitionerRoleMappingInterface */
    public $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionDelete::NAME,
                    CFHIRInteractionUpdate::NAME,
                ]
            )
            ->addSearchAttributes(
                [
                    new SearchParameterString('specialty'),
                ]
            );
    }

    /**
     * @param FHIRBoolean|null $active
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function setActive(?FHIRBoolean $active): CFHIRResourcePractitionerRole
    {
        $this->fhirResource->setActive($active);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getActive(): ?FHIRBoolean
    {
        return $this->fhirResource->getActive();
    }

    /**
     * @param FHIRPeriod|null $period
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function setPeriod(?FHIRPeriod $period): CFHIRResourcePractitionerRole
    {
        $this->fhirResource->setPeriod($period);

        return $this;
    }

    /**
     * @return FHIRPeriod|null
     */
    public function getPeriod(): ?FHIRPeriod
    {
        return $this->fhirResource->getPeriod();
    }

    /**
     * @param FHIRReference|null $practitioner
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function setPractitioner(?FHIRReference $practitioner): CFHIRResourcePractitionerRole
    {
        $this->fhirResource->setPractitioner($practitioner);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getPractitioner(): ?FHIRReference
    {
        return $this->fhirResource->getPractitioner();
    }

    /**
     * @param FHIRReference|null $organization
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function setOrganization(?FHIRReference $organization): CFHIRResourcePractitionerRole
    {
        $this->fhirResource->setOrganization($organization);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getOrganization(): ?FHIRReference
    {
        return $this->fhirResource->getOrganization();
    }

    /**
     * @param FHIRCodeableConcept ...$code
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function setCode(FHIRCodeableConcept ...$code): CFHIRResourcePractitionerRole
    {
        $this->fhirResource->setCode(...$code);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$code
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function addCode(FHIRCodeableConcept ...$code): CFHIRResourcePractitionerRole
    {
        $this->fhirResource->addCode(...$code);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCode(): array
    {
        return $this->fhirResource->getCode();
    }

    /**
     * @param FHIRCodeableConcept ...$specialty
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function setSpecialty(FHIRCodeableConcept ...$specialty): CFHIRResourcePractitionerRole
    {
        $this->fhirResource->setSpecialty(...$specialty);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$specialty
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function addSpecialty(FHIRCodeableConcept ...$specialty): CFHIRResourcePractitionerRole
    {
        $this->fhirResource->addSpecialty(...$specialty);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->fhirResource->getSpecialty();
    }

    /**
     * @param FHIRReference ...$location
     *
     * @return self
     */
    public function setLocation(FHIRReference ...$location): self
    {
        $this->fhirResource->setLocation(...$location);

        return $this;
    }

    /**
     * @param FHIRReference ...$location
     *
     * @return self
     */
    public function addLocation(FHIRReference ...$location): self
    {
        $this->fhirResource->addLocation(...$location);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getLocation(): array
    {
        return $this->fhirResource->getLocation();
    }

    /**
     * @param FHIRReference ...$healthcareService
     *
     * @return self
     */
    public function setHealthcareService(FHIRReference ...$healthcareService): self
    {
        $this->fhirResource->setHealthcareService(...$healthcareService);

        return $this;
    }

    /**
     * @param FHIRReference ...$healthcareService
     *
     * @return self
     */
    public function addHealthcareService(FHIRReference ...$healthcareService): self
    {
        $this->fhirResource->addHealthcareService(...$healthcareService);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getHealthcareService(): array
    {
        return $this->fhirResource->getHealthcareService();
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return self
     */
    public function setTelecom(FHIRContactPoint ...$telecom): self
    {
        $this->fhirResource->setTelecom(...$telecom);

        return $this;
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return self
     */
    public function addTelecom(FHIRContactPoint ...$telecom): self
    {
        $this->fhirResource->addTelecom(...$telecom);

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->fhirResource->getTelecom();
    }

    /**
     * @param FHIRPractitionerRoleAvailableTime ...$availableTime
     *
     * @return self
     */
    public function setAvailableTime(FHIRPractitionerRoleAvailableTime ...$availableTime): self
    {
        $this->fhirResource->setAvailableTime(...$availableTime);

        return $this;
    }

    /**
     * @param FHIRPractitionerRoleAvailableTime ...$availableTime
     *
     * @return self
     */
    public function addAvailableTime(FHIRPractitionerRoleAvailableTime ...$availableTime): self
    {
        $this->fhirResource->addAvailableTime(...$availableTime);

        return $this;
    }

    /**
     * @return FHIRPractitionerRoleAvailableTime[]
     */
    public function getAvailableTime(): array
    {
        return $this->fhirResource->getAvailableTime();
    }

    /**
     * @param FHIRPractitionerRoleNotAvailable ...$notAvailable
     *
     * @return self
     */
    public function setNotAvailable(FHIRPractitionerRoleNotAvailable ...$notAvailable): self
    {
        $this->fhirResource->setNotAvailable(...$notAvailable);

        return $this;
    }

    /**
     * @param FHIRPractitionerRoleNotAvailable ...$notAvailable
     *
     * @return self
     */
    public function addNotAvailable(FHIRPractitionerRoleNotAvailable ...$notAvailable): self
    {
        $this->fhirResource->addNotAvailable(...$notAvailable);

        return $this;
    }

    /**
     * @return FHIRPractitionerRoleNotAvailable[]
     */
    public function getNotAvailable(): array
    {
        return $this->fhirResource->getNotAvailable();
    }

    /**
     * @param FHIRString|null $availabilityExceptions
     *
     * @return CFHIRResourcePractitionerRole
     */
    public function setAvailabilityExceptions(
        ?FHIRString $availabilityExceptions
    ): CFHIRResourcePractitionerRole {
        $this->fhirResource->setAvailabilityExceptions($availabilityExceptions);

        return $this;
    }

    /**
     * @param FHIRReference ...$endpoint
     *
     * @return self
     */
    public function setEndpoint(FHIRReference ...$endpoint): self
    {
        $this->fhirResource->setEndpoint(...$endpoint);

        return $this;
    }

    /**
     * @param FHIRReference ...$endpoint
     *
     * @return self
     */
    public function addEndpoint(FHIRReference ...$endpoint): self
    {
        $this->fhirResource->addEndpoint(...$endpoint);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->fhirResource->getEndpoint();
    }

    /**
     * @return FHIRString|null
     */
    public function getAvailabilityExceptions(): ?FHIRString
    {
        return $this->fhirResource->getAvailabilityExceptions();
    }

    /**
     * Map property active
     */
    protected function mapActive(): void
    {
        $this->fhirResource->setActive($this->object_mapping->mapActive());
    }

    /**
     * Map property period
     */
    protected function mapPeriod(): void
    {
        $this->fhirResource->setPeriod($this->object_mapping->mapPeriod());
    }

    /**
     * Map property practitioner
     */
    protected function mapPractitioner(): void
    {
        $this->fhirResource->setPractitioner($this->object_mapping->mapPractitioner());
    }

    /**
     * Map property organization
     */
    protected function mapOrganization(): void
    {
        $this->fhirResource->setOrganization($this->object_mapping->mapOrganization());
    }

    /**
     * Map property code
     */
    protected function mapCode(): void
    {
        $this->fhirResource->setCode(...$this->object_mapping->mapCode());
    }

    /**
     * Map property speciality
     */
    protected function mapSpecialty(): void
    {
        $this->fhirResource->setSpecialty(...$this->object_mapping->mapSpecialty());
    }

    /**
     * Map property location
     */
    protected function mapLocation(): void
    {
        $this->fhirResource->setLocation(...$this->object_mapping->mapLocation());
    }

    /**
     * Map property healthcareService
     */
    protected function mapHealthCareService(): void
    {
        $this->fhirResource->setHealthcareService(...$this->object_mapping->mapHealthCareService());
    }

    /**
     * Map property telecom
     */
    protected function mapTelecom(): void
    {
        $this->fhirResource->setTelecom(...$this->object_mapping->mapTelecom());
    }

    /**
     * Map property availableTime
     */
    protected function mapAvailableTime(): void
    {
        $this->fhirResource->setAvailableTime(...$this->object_mapping->mapAvailableTime());
    }

    /**
     * Map property notAvailable
     */
    protected function mapNotAvailable(): void
    {
        $this->fhirResource->setNotAvailable(...$this->object_mapping->mapNotAvailable());
    }

    /**
     * Map property availabilityExceptions
     */
    protected function mapAvailabilityExceptions(): void
    {
        $this->fhirResource->setAvailabilityExceptions($this->object_mapping->mapAvailabilityExceptions());
    }

    /**
     * Map property endpoint
     */
    protected function mapEndpoint(): void
    {
        $this->fhirResource->setEndpoint(...$this->object_mapping->mapEndpoint());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRPractitionerRole';
    }
}
