<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\ConceptMap;

use DOMDocument;
use DOMNode;
use Ox\Core\CMbArray;
use Ox\Interop\Fhir\Actors\CReceiverFHIR;
use Ox\Interop\Fhir\CFHIRXPath;
use Ox\Interop\Fhir\Contracts\Mapping\R4\ConceptMapMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceConceptMapInterface;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Components\FhirCore\Interfaces\FHIRConceptMapInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRConceptMapGroup;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRConceptMapGroupElement;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRConceptMapGroupElementTarget;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Phast\CAideSaisieConceptMapLiaison;
use Ox\Mediboard\CompteRendu\CAideSaisie;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Sante400\CIdSante400;

/**
 * FHIR patient resource
 */
class CFHIRResourceConceptMap extends CFHIRDomainResource implements ResourceConceptMapInterface
{
    /** @var string */
    public const RESOURCE_TYPE = 'ConceptMap';

    public const URL_SNOMED      = "http://snomed.info/sct";
    public const URL_CIM10       = "https://www.atih.sante.fr/cim-10";
    public const TAG_CODE_SNOMED = "code_snomed";
    public const TAG_CODE_CIM10  = "code_cim10";

    /** @var FHIRConceptMapInterface $fhirResource */
    protected $fhirResource;

    /** @var ConceptMapMappingInterface */
    protected $object_mapping;

    /*public function build(CStoredObject $object, CFHIREvent $event): void
    {
        parent::build($object, $event);

        if (!$object instanceof CAideSaisie) {
            throw  new CFHIRException("Object is not aide saisie");
        }

        // Cas d'un ajout d'un code dans le conceptMap, on met donc l'identifiant de la resource du serveur
        if ($object->_ref_concept_map->_id) {
            $this->setId(new FHIRString($object->_ref_concept_map->identifier_concept_map));
        }

        $group = $this->getGroupAideSaisie($object);

        $resource = new CFHIRResourceConceptMap();

        $url             = "http://" . CAppUI::conf("mb_oid") . "/" . CAppUI::conf("product_name") . "/"
            . $object->class . "-" . $object->field;
        $this->url       = new FHIRString($url);
        $this->status    = "draft";
        $this->name      = new FHIRString(CAppUI::tr("$object->class-$object->field"));
        $this->publisher = new FHIRString($group->_view);
        $this->purpose   = new FHIRString("__Objet__:" . $object->class . "|__Champ__:" . $object->field);

        $codes = [];
        if ($object->_ref_concept_map->_id) {
            $aide_saisie_liaison             = new CAideSaisieConceptMapLiaison();
            $where                           = [];
            $where["identifier_concept_map"] = " = '" . $object->_ref_concept_map->_id . "' ";
            $aide_saisie_liaisons            = $aide_saisie_liaison->loadList(
                ["identifier_concept_map" => " = '" . $object->_ref_concept_map->identifier_concept_map . "' "]
            );

            $elements_snomed = $elements_cim10 = [];
            // On ajoute les codes qui sont d�j� dans le conceptMap
            foreach ($aide_saisie_liaisons as $_aide_saisie_liaison) {
                $aide_saisie = $_aide_saisie_liaison->loadRefAideSaisie();
                $display     = "__Libelle__:$aide_saisie->name|__Description__:$aide_saisie->text";

                // R�cup�ration des codes que l'on a d�j� r�cup�r�s
                $idex_cim10  = CIdSante400::getMatchFor($aide_saisie, self::TAG_CODE_CIM10);
                $idex_snomed = CIdSante400::getMatchFor($aide_saisie, self::TAG_CODE_SNOMED);

                $elements_snomed = $idex_snomed->_id
                    ? $this->addElementWithTarget($elements_snomed, $aide_saisie->_id, $display, $idex_snomed)
                    : $this->addElement($elements_snomed, $aide_saisie->_id, $display);

                $elements_cim10 = $idex_cim10->_id
                    ? $this->addElementWithTarget($elements_cim10, $aide_saisie->_id, $display, $idex_cim10)
                    : $this->addElement($elements_cim10, $aide_saisie->_id, $display);
            }

            $display         = "__Libelle__:$object->name|__Description__:$object->text";
            $elements_snomed = $this->addElement($elements_snomed, $object->_id, $display);
            $elements_cim10  = $this->addElement($elements_cim10, $object->_id, $display);

            // Ajout des elements dans le Group SNOMED
            $this->group[] = FHIRConceptMapGroup::build(
                [
                    "source"  => new FHIRString("$object->class-$object->field"),
                    "target"  => self::URL_SNOMED,
                    "element" => $elements_snomed,
                ]
            );

            // Ajout des elements dans le Group CIM10
            $this->group[] = FHIRConceptMapGroup::build(
                [
                    "source"  => new FHIRString("$object->class-$object->field"),
                    "target"  => self::URL_CIM10,
                    "element" => $elements_cim10,
                ]
            );
        } else {
            $display = "__Libelle__:$object->name|__Description__:$object->text";

            $codes[] = [
                "code"    => new FHIRCode($object->_id),
                "display" => new FHIRString($display),
            ];

            $this->group[] = FHIRConceptMapGroup::build(
                [
                    "source"  => new FHIRString("$object->class-$object->field"),
                    "target"  => self::URL_SNOMED,
                    "element" => FHIRConceptMapElement::build(
                        [
                            "code"    => new FHIRCode($object->_id),
                            "display" => new FHIRString($display),
                        ]
                    ),
                ]
            );

            $this->group[] = FHIRConceptMapGroup::build(
                [
                    "source"  => new FHIRString("$object->class-$object->field"),
                    "target"  => self::URL_CIM10,
                    "element" => FHIRConceptMapElement::build(
                        [
                            "code"    => new FHIRCode($object->_id),
                            "display" => new FHIRString($display),
                        ]
                    ),
                ]
            );
        }
    }*/

    /**
     * Add element
     *
     * @param array  $element
     * @param string $code
     * @param string $display
     *
     * @return array
     */
    public function addElement(array $element, string $code, string $display): array
    {
        $element[] = [
            "element" => (new FHIRConceptMapGroupElement())
                ->setCode($code)
                ->setDisplay($display),
        ];

        return $element;
    }

    /**
     * @param array       $element
     * @param string      $code
     * @param string      $display
     * @param CIdSante400 $idex
     *
     * @return array
     */
    public function addElementWithTarget(array $element, string $code, string $display, CIdSante400 $idex): array
    {
        $datas = explode("|", $idex->id400);

        $element[] = [
            "element" => (new FHIRConceptMapGroupElement())
                ->setCode($code)
                ->setDisplay($display)
                ->setTarget(
                    (new FHIRConceptMapGroupElementTarget())
                        ->setCode(CMbArray::get($datas, 0))
                        ->setDisplay(CMbArray::get($datas, 1))
                        ->setEquivalence(CMbArray::get($datas, 2))
                ),
        ];

        return $element;
    }

    /**
     * Get group
     *
     * @param CAideSaisie $aide_saisie aide saisie
     *
     * @return CGroups
     */
    public function getGroupAideSaisie(CAideSaisie $aide_saisie): CGroups
    {
        $group_id = null;
        // R�cup�ration de l'�tablissement directement de l'aide � la saisie
        $group_id = $aide_saisie->group_id ? $aide_saisie->group_id : null;

        // R�cup�ration de l'�tablissement � partir de la fonction de l'aide � la saisie
        if (!$group_id) {
            if ($aide_saisie->function_id) {
                $function = new CFunctions();
                $function->load($aide_saisie->function_id);
                $group_id = $function->loadRefGroup()->_id;
            }
        }

        // R�cup�ration de l'�tablissement � partir de l'utilisateur de l'aide � la saisie
        if (!$group_id) {
            if ($aide_saisie->user_id) {
                $user = new CMediusers();
                $user->load($aide_saisie->user_id);

                $group_id = $user->loadRefFunction()->loadRefGroup()->_id;
            }
        }

        // Dans le pire des cas, on prend l'�tablissement courant
        if (!$group_id) {
            $group_id = CGroups::loadCurrent()->_id;
        }

        $group = new CGroups();
        $group->load($group_id);

        return $group;
    }

    /**
     * Mapping ConceptMap resource to create CAideSaisieConceptMapLiaison
     *
     * @param CFHIRXPath    $xpath             xpath
     * @param DOMNode       $node_doc_manifest node doc manifest
     * @param CFHIRResource $resource          resource
     *
     * @return CAideSaisieConceptMapLiaison
     * @throws CFHIRException
     */
    public static function mapping(
        DOMDocument $dom,
        CAideSaisie $aide_saisie,
        CReceiverFHIR $receiver_fhir
    ): CAideSaisieConceptMapLiaison {
        $xpath = new CFHIRXPath($dom);

        $liaison_aide_saisie                 = new CAideSaisieConceptMapLiaison();
        $liaison_aide_saisie->aide_saisie_id = $aide_saisie->_id;
        $liaison_aide_saisie->class          = $aide_saisie->class;
        $liaison_aide_saisie->field          = $aide_saisie->field;
        $liaison_aide_saisie->loadMatchingObject();

        if ($liaison_aide_saisie->_id) {
            throw new CFHIRException("Link with concept map always exist");
        }

        //$liaison_aide_saisie->url                    = $receiver_fhir->_source->_location_resource;
        $node_concept_map                            = $xpath->query("fhir:ConceptMap", $dom)->item(0);
        $liaison_aide_saisie->identifier_concept_map = $xpath->getAttributeValue("fhir:id", $node_concept_map);
        $liaison_aide_saisie->store();

        return $liaison_aide_saisie;
    }

    /**
     * MappingCodes
     *
     * @param string $response
     * @param int    $concept_map_id
     *
     * @throws \Exception
     */
    public static function mappingCodes(string $response, int $concept_map_id): void
    {
        $dom = new DOMDocument();
        $dom->loadXML($response);
        $xpath = new CFHIRXPath($dom);

        // On enl�ve tous les codes existants et on remplace par les nouveaux (comme �a gestion de la modification)
        self::deleteCodes($concept_map_id);

        $nodes_group = $xpath->query("fhir:ConceptMap/fhir:group", $dom);
        foreach ($nodes_group as $_node_group) {
            $terminologie = $xpath->getAttributeValue("fhir:target", $_node_group);

            if ($terminologie != self::URL_CIM10 && $terminologie != self::URL_SNOMED) {
                continue;
            }

            $nodes_element = $xpath->query("fhir:element", $_node_group);
            if (!$nodes_element) {
                continue;
            }

            foreach ($nodes_element as $_node_element) {
                $aide_saisie_id = $xpath->getAttributeValue("fhir:code", $_node_element);
                $aide_saisie    = new CAideSaisie();
                $aide_saisie->load($aide_saisie_id);
                if (!$aide_saisie->_id) {
                    continue;
                }

                $targets_node = $xpath->query("fhir:target", $_node_element);
                if (!$targets_node) {
                    continue;
                }

                foreach ($targets_node as $_target_node) {
                    $value = $xpath->getAttributeValue(
                            "fhir:code",
                            $_target_node
                        ) . "|" .
                        $xpath->getAttributeValue("fhir:display", $_target_node) . "|" .
                        $xpath->getAttributeValue("fhir:equivalence", $_target_node);
                    $idex  = CIdSante400::getMatch(
                        $aide_saisie->_class,
                        self::getTagIdex($terminologie),
                        $value,
                        $aide_saisie->_id
                    );
                    $idex->store();
                }
            }
        }
    }

    /**
     * Delete all codes CIM10 and SNOMED for an concept map
     *
     * @param $concept_map_id
     *
     * @throws \Exception
     */
    public static function deleteCodes(int $concept_map_id): void
    {
        $aide_saisie_liaison  = new CAideSaisieConceptMapLiaison();
        $aide_saisie_liaisons = $aide_saisie_liaison->loadList(
            ["identifier_concept_map" => " = '$concept_map_id' "]
        );

        foreach ($aide_saisie_liaisons as $_aide_saisie_liaison) {
            $aide_saisie = $_aide_saisie_liaison->loadRefAideSaisie();

            $idex                  = new CIdSante400();
            $where                 = [];
            $where["object_class"] = " = '$aide_saisie->_class' ";
            $where["object_id"]    = " = '$aide_saisie->_id' ";
            $where[]               = " tag = '" . self::TAG_CODE_SNOMED . "' OR tag = '" . self::TAG_CODE_CIM10 . "' ";
            foreach ($idex->loadList($where) as $_idex) {
                $_idex->purge();
            }
        }
    }

    /**
     * Get tag idex for terminologie name
     *
     * @param string $terminologie terminologie
     *
     * @return null|string
     */
    public static function getTagIdex(string $terminologie): ?string
    {
        switch ($terminologie) {
            case self::URL_SNOMED:
                return self::TAG_CODE_SNOMED;
            case self::URL_CIM10:
                return self::TAG_CODE_CIM10;
            default:
                return null;
        }
    }

    /**
     * Map property url
     */
    protected function mapUrl(): void
    {
        $this->fhirResource->setUrl($this->object_mapping->mapUrl());
    }

    /**
     * Map property version
     */
    protected function mapVersion(): void
    {
        $this->fhirResource->setVersion($this->object_mapping->mapVersion());
    }

    /**
     * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName($this->object_mapping->mapName());
    }

    /**
     * Map property title
     */
    protected function mapTitle(): void
    {
        $this->fhirResource->setTitle($this->object_mapping->mapTitle());
    }

    /**
     * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * Map property experimental
     */
    protected function mapExperimental(): void
    {
        $this->fhirResource->setExperimental($this->object_mapping->mapExperimental());
    }

    /**
     * Map property date
     */
    protected function mapDate(): void
    {
        $this->fhirResource->setDate($this->object_mapping->mapDate());
    }

    /**
     * Map property Publisher
     */
    protected function mapPublisher(): void
    {
        $this->fhirResource->setPublisher($this->object_mapping->mapPublisher());
    }

    /**
     * Map property Contact
     */
    protected function mapContact(): void
    {
        $this->fhirResource->setContact(...$this->object_mapping->mapContact());
    }

    /**
     * Map property Description
     */
    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    /**
     * Map property UseContext
     */
    protected function mapUseContext(): void
    {
        $this->fhirResource->setUseContext(...$this->object_mapping->mapUseContext());
    }

    /**
     * Map property Jurisdiction
     */
    protected function mapJurisdiction(): void
    {
        $this->fhirResource->setJurisdiction(...$this->object_mapping->mapJurisdiction());
    }

    /**
     * Map property Purpose
     */
    protected function mapPurpose(): void
    {
        $this->fhirResource->setPurpose($this->object_mapping->mapPurpose());
    }

    /**
     * Map property CopyRight
     */
    protected function mapCopyright(): void
    {
        $this->fhirResource->setCopyright($this->object_mapping->mapCopyright());
    }

    /**
     * Map property Source
     */
    protected function mapSource(): void
    {
        $this->fhirResource->setSource($this->object_mapping->mapSource());
    }

    /**
     * Map property Target
     */
    protected function mapTarget(): void
    {
        $this->fhirResource->setTarget($this->object_mapping->mapTarget());
    }

    /**
     * Map property Group
     */
    protected function mapGroup(): void
    {
        $this->fhirResource->setGroup(...$this->object_mapping->mapGroup());
    }

    /**
     * @param FHIRUri|null $url
     *
     * @return CFHIRResourceConceptMap
     */
    public function setUrl(?FHIRUri $url): CFHIRResourceConceptMap
    {
        $this->fhirResource->setUrl($url);

        return $this;
    }

    /**
     * @return FHIRUri|null
     */
    public function getUrl(): ?FHIRUri
    {
        return $this->fhirResource->getUrl();
    }

    /**
     * @param FHIRString|null $version
     *
     * @return CFHIRResourceConceptMap
     */
    public function setVersion(?FHIRString $version): CFHIRResourceConceptMap
    {
        $this->fhirResource->setVersion($version);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getVersion(): ?FHIRString
    {
        return $this->fhirResource->getVersion();
    }

    /**
     * @param FHIRString|null $name
     *
     * @return CFHIRResourceConceptMap
     */
    public function setName(?FHIRString $name): CFHIRResourceConceptMap
    {
        $this->fhirResource->setName($name);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getName(): ?FHIRString
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRString|null $title
     *
     * @return CFHIRResourceConceptMap
     */
    public function setTitle(?FHIRString $title): CFHIRResourceConceptMap
    {
        $this->fhirResource->setTitle($title);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getTitle(): ?FHIRString
    {
        return $this->fhirResource->getTitle();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceConceptMap
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceConceptMap
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRDateTime|null $date
     *
     * @return CFHIRResourceConceptMap
     */
    public function setDate(?FHIRDateTime $date): CFHIRResourceConceptMap
    {
        $this->fhirResource->setDate($date);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getDate(): ?FHIRDateTime
    {
        return $this->fhirResource->getDate();
    }

    /**
     * @param FHIRString|null $publisher
     *
     * @return CFHIRResourceConceptMap
     */
    public function setPublisher(?FHIRString $publisher): CFHIRResourceConceptMap
    {
        $this->fhirResource->setPublisher($publisher);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getPublisher(): ?FHIRString
    {
        return $this->fhirResource->getPublisher();
    }

    /**
     * @param FHIRMarkdown|null $description
     *
     * @return CFHIRResourceConceptMap
     */
    public function setDescription(?FHIRMarkdown $description): CFHIRResourceConceptMap
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getDescription(): ?FHIRMarkdown
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return CFHIRResourceConceptMap
     */
    public function setJurisdiction(FHIRCodeableConcept ...$jurisdiction): CFHIRResourceConceptMap
    {
        $this->fhirResource->setJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return CFHIRResourceConceptMap
     */
    public function addJurisdiction(FHIRCodeableConcept ...$jurisdiction): CFHIRResourceConceptMap
    {
        $this->fhirResource->addJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @return array
     */
    public function getJurisdiction(): array
    {
        return $this->fhirResource->getJurisdiction();
    }

    /**
     * @param FHIRMarkdown|null $purpose
     *
     * @return CFHIRResourceConceptMap
     */
    public function setPurpose(?FHIRMarkdown $purpose): CFHIRResourceConceptMap
    {
        $this->fhirResource->setPurpose($purpose);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->fhirResource->getPurpose();
    }

    /**
     * @param FHIRMarkdown|null $copyright
     *
     * @return CFHIRResourceConceptMap
     */
    public function setCopyright(?FHIRMarkdown $copyright): CFHIRResourceConceptMap
    {
        $this->fhirResource->setCopyright($copyright);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->fhirResource->getCopyright();
    }

    /**
     * @param FHIRElement|null $source
     *
     * @return CFHIRResourceConceptMap
     */
    public function setSource(?FHIRElement $source): CFHIRResourceConceptMap
    {
        $this->fhirResource->setSource($source);

        return $this;
    }

    /**
     * @return FHIRElement|null
     */
    public function getSource(): ?FHIRElement
    {
        return $this->fhirResource->getSource();
    }

    /**
     * @param FHIRElement|null $target
     *
     * @return CFHIRResourceConceptMap
     */
    public function setTarget(?FHIRElement $target): CFHIRResourceConceptMap
    {
        $this->fhirResource->setTarget($target);

        return $this;
    }

    /**
     * @return FHIRElement|null
     */
    public function getTarget(): ?FHIRElement
    {
        return $this->fhirResource->getTarget();
    }

    /**
     * @param FHIRConceptMapGroup ...$group
     *
     * @return CFHIRResourceConceptMap
     */
    public function setGroup(FHIRConceptMapGroup ...$group): CFHIRResourceConceptMap
    {
        $this->fhirResource->setGroup(...$group);

        return $this;
    }

    /**
     * @param FHIRConceptMapGroup ...$group
     *
     * @return CFHIRResourceConceptMap
     */
    public function addGroup(FHIRConceptMapGroup ...$group): CFHIRResourceConceptMap
    {
        $this->fhirResource->addGroup(...$group);

        return $this;
    }

    /**
     * @return FHIRConceptMapGroup[]
     */
    public function getGroup(): array
    {
        return $this->fhirResource->getGroup();
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return CFHIRResourceConceptMap
     */
    public function setUseContext(FHIRUsageContext ...$useContext): CFHIRResourceConceptMap
    {
        $this->fhirResource->setUseContext(...$useContext);

        return $this;
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return CFHIRResourceConceptMap
     */
    public function addUseContext(FHIRUsageContext ...$useContext): CFHIRResourceConceptMap
    {
        $this->fhirResource->addUseContext(...$useContext);

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->fhirResource->getUseContext();
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return CFHIRResourceConceptMap
     */
    public function setContact(FHIRContactDetail ...$contact): CFHIRResourceConceptMap
    {
        $this->fhirResource->setContact(...$contact);

        return $this;
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return CFHIRResourceConceptMap
     */
    public function addContact(FHIRContactDetail ...$contact): CFHIRResourceConceptMap
    {
        $this->fhirResource->addContact(...$contact);

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->fhirResource->getContact();
    }

    /**
     * @param FHIRBoolean|null $experimental
     *
     * @return CFHIRResourceConceptMap
     */
    public function setExperimental(?FHIRBoolean $experimental): CFHIRResourceConceptMap
    {
        $this->fhirResource->setExperimental($experimental);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getExperimental(): ?FHIRBoolean
    {
        return $this->fhirResource->getExperimental();
    }

    /**
     * @param null $identifier
     *
     * @return CFHIRResourceConceptMap
     */
    public function setIdentifier(?FHIRIdentifier ...$identifier): self
    {
        $this->fhirResource->setIdentifier(empty($identifier) ? null : reset($identifier));

        return $this;
    }

    /**
     * @return FHIRIdentifier|null
     */
    public function getIdentifier()
    {
        $identifier = $this->fhirResource->getIdentifier();
        if (is_array($identifier)) {
            return empty($identifier) ? null : reset($identifier);
        } else {
            return $identifier;
        }
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRConceptMap';
    }
}
