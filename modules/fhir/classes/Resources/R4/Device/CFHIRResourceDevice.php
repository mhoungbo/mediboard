<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Device;

use Ox\Components\FhirCore\Interfaces\FHIRDeviceInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceDeviceName;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceProperty;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceSpecialization;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceUdiCarrier;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceVersion;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Resources\R4\Device\Mapper\Device;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * FHIR device resource
 */
class CFHIRResourceDevice extends CFHIRDomainResource
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Device';

    // attributes
    /** @var FHIRDeviceInterface $fhirResource */
    protected $fhirResource;
    
    /** @var Device */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            );
    }

    /**
     * @param FHIRReference|null $definition
     *
     * @return CFHIRResourceDevice
     */
    public function setDefinition(?FHIRReference $definition): CFHIRResourceDevice
    {
        $this->fhirResource->setDefinition($definition);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getDefinition(): ?FHIRReference
    {
        return $this->fhirResource->getDefinition();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceDevice
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceDevice
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRCodeableConcept ...$statusReason
     *
     * @return CFHIRResourceDevice
     */
    public function setStatusReason(FHIRCodeableConcept ...$statusReason): CFHIRResourceDevice
    {
        $this->fhirResource->setStatusReason(...$statusReason);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$statusReason
     *
     * @return CFHIRResourceDevice
     */
    public function addStatusReason(FHIRCodeableConcept ...$statusReason): CFHIRResourceDevice
    {
        $this->fhirResource->addStatusReason(...$statusReason);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getStatusReason(): array
    {
        return $this->fhirResource->getStatusReason();
    }

    /**
     * @param FHIRString|null $distinctIdentifier
     *
     * @return CFHIRResourceDevice
     */
    public function setDistinctIdentifier(?FHIRString $distinctIdentifier): CFHIRResourceDevice
    {
        $this->fhirResource->setDistinctIdentifier($distinctIdentifier);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getDistinctIdentifier(): ?FHIRString
    {
        return $this->fhirResource->getDistinctIdentifier();
    }

    /**
     * @param FHIRString|null $manufacturer
     *
     * @return CFHIRResourceDevice
     */
    public function setManufacturer(?FHIRString $manufacturer): CFHIRResourceDevice
    {
        $this->fhirResource->setManufacturer($manufacturer);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getManufacturer(): ?FHIRString
    {
        return $this->fhirResource->getManufacturer();
    }

    /**
     * @param FHIRDateTime|null $manufactureDate
     *
     * @return CFHIRResourceDevice
     */
    public function setManufactureDate(?FHIRDateTime $manufactureDate): CFHIRResourceDevice
    {
        $this->fhirResource->setManufactureDate($manufactureDate);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getManufactureDate(): ?FHIRDateTime
    {
        return $this->fhirResource->getManufactureDate();
    }

    /**
     * @param FHIRDateTime|null $expirationDate
     *
     * @return CFHIRResourceDevice
     */
    public function setExpirationDate(?FHIRDateTime $expirationDate): CFHIRResourceDevice
    {
        $this->fhirResource->setExpirationDate($expirationDate);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getExpirationDate(): ?FHIRDateTime
    {
        return $this->fhirResource->getExpirationDate();
    }

    /**
     * @param FHIRString|null $lotNumber
     *
     * @return CFHIRResourceDevice
     */
    public function setLotNumber(?FHIRString $lotNumber): CFHIRResourceDevice
    {
        $this->fhirResource->setLotNumber($lotNumber);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getLotNumber(): ?FHIRString
    {
        return $this->fhirResource->getLotNumber();
    }

    /**
     * @param FHIRString|null $serialNumber
     *
     * @return CFHIRResourceDevice
     */
    public function setSerialNumber(?FHIRString $serialNumber): CFHIRResourceDevice
    {
        $this->fhirResource->setSerialNumber($serialNumber);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getSerialNumber(): ?FHIRString
    {
        return $this->fhirResource->getSerialNumber();
    }

    /**
     * @param FHIRString|null $modelNumber
     *
     * @return CFHIRResourceDevice
     */
    public function setModelNumber(?FHIRString $modelNumber): CFHIRResourceDevice
    {
        $this->fhirResource->setModelNumber($modelNumber);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getModelNumber(): ?FHIRString
    {
        return $this->fhirResource->getModelNumber();
    }

    /**
     * @param FHIRString|null $partNumber
     *
     * @return CFHIRResourceDevice
     */
    public function setPartNumber(?FHIRString $partNumber): CFHIRResourceDevice
    {
        $this->fhirResource->setPartNumber($partNumber);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getPartNumber(): ?FHIRString
    {
        return $this->fhirResource->getPartNumber();
    }

    /**
     * @param FHIRCodeableConcept|null $type
     *
     * @return CFHIRResourceDevice
     */
    public function setType(?FHIRCodeableConcept $type): CFHIRResourceDevice
    {
        $this->fhirResource->setType($type);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getType(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getType();
    }

    /**
     * @param FHIRReference|null $patient
     *
     * @return CFHIRResourceDevice
     */
    public function setPatient(?FHIRReference $patient): CFHIRResourceDevice
    {
        $this->fhirResource->setPatient($patient);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getPatient(): ?FHIRReference
    {
        return $this->fhirResource->getPatient();
    }

    /**
     * @param FHIRReference|null $owner
     *
     * @return CFHIRResourceDevice
     */
    public function setOwner(?FHIRReference $owner): CFHIRResourceDevice
    {
        $this->fhirResource->setOwner($owner);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getOwner(): ?FHIRReference
    {
        return $this->fhirResource->getOwner();
    }

    /**
     * @param FHIRContactPoint ...$contact
     *
     * @return CFHIRResourceDevice
     */
    public function setContact(FHIRContactPoint ...$contact): CFHIRResourceDevice
    {
        $this->fhirResource->setContact(...$contact);

        return $this;
    }

    /**
     * @param FHIRContactPoint ...$contact
     *
     * @return CFHIRResourceDevice
     */
    public function addContact(FHIRContactPoint ...$contact): CFHIRResourceDevice
    {
        $this->fhirResource->addContact(...$contact);

        return $this;
    }

    /**
     * @return array
     */
    public function getContact(): array
    {
        return $this->fhirResource->getContact();
    }

    /**
     * @param FHIRReference|null $location
     *
     * @return CFHIRResourceDevice
     */
    public function setLocation(?FHIRReference $location): CFHIRResourceDevice
    {
        $this->fhirResource->setLocation($location);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getLocation(): ?FHIRReference
    {
        return $this->fhirResource->getLocation();
    }

    /**
     * @param FHIRUri|null $url
     *
     * @return CFHIRResourceDevice
     */
    public function setUrl(?FHIRUri $url): CFHIRResourceDevice
    {
        $this->fhirResource->setUrl($url);

        return $this;
    }

    /**
     * @return FHIRUri|null
     */
    public function getUrl(): ?FHIRUri
    {
        return $this->fhirResource->getUrl();
    }

    /**
     * @param FHIRAnnotation ...$note
     *
     * @return CFHIRResourceDevice
     */
    public function setNote(FHIRAnnotation ...$note): CFHIRResourceDevice
    {
        $this->fhirResource->setNote(...$note);

        return $this;
    }

    /**
     * @param FHIRAnnotation ...$note
     *
     * @return CFHIRResourceDevice
     */
    public function addNote(FHIRAnnotation ...$note): CFHIRResourceDevice
    {
        $this->fhirResource->addNote(...$note);

        return $this;
    }

    /**
     * @return array
     */
    public function getNote(): array
    {
        return $this->fhirResource->getNote();
    }

    /**
     * @param FHIRCodeableConcept ...$safety
     *
     * @return CFHIRResourceDevice
     */
    public function setSafety(FHIRCodeableConcept ...$safety): CFHIRResourceDevice
    {
        $this->fhirResource->setSafety(...$safety);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$safety
     *
     * @return CFHIRResourceDevice
     */
    public function addSafety(FHIRCodeableConcept ...$safety): CFHIRResourceDevice
    {
        $this->fhirResource->addSafety(...$safety);

        return $this;
    }

    /**
     * @return array
     */
    public function getSafety(): array
    {
        return $this->fhirResource->getSafety();
    }

    /**
     * @param FHIRReference|null $parent
     *
     * @return CFHIRResourceDevice
     */
    public function setParent(?FHIRReference $parent): CFHIRResourceDevice
    {
        $this->fhirResource->setParent($parent);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getParent(): ?FHIRReference
    {
        return $this->fhirResource->getParent();
    }

    /**
     * @param FHIRDeviceSpecialization ...$specialization
     *
     * @return CFHIRResourceDevice
     */
    public function setSpecialization(FHIRDeviceSpecialization ...$specialization): CFHIRResourceDevice
    {
        $this->fhirResource->setSpecialization(...$specialization);

        return $this;
    }

    /**
     * @param FHIRDeviceSpecialization ...$specialization
     *
     * @return CFHIRResourceDevice
     */
    public function addSpecialization(FHIRDeviceSpecialization ...$specialization): CFHIRResourceDevice
    {
        $this->fhirResource->addSpecialization(...$specialization);

        return $this;
    }

    /**
     * @return FHIRDeviceSpecialization[]
     */
    public function getSpecialization(): array
    {
        return $this->fhirResource->getSpecialization();
    }

    /**
     * @param FHIRDeviceVersion ...$version
     *
     * @return CFHIRResourceDevice
     */
    public function setVersion(FHIRDeviceVersion ...$version): CFHIRResourceDevice
    {
        $this->fhirResource->setVersion(...$version);

        return $this;
    }

    /**
     * @param FHIRDeviceVersion ...$version
     *
     * @return CFHIRResourceDevice
     */
    public function addVersion(FHIRDeviceVersion ...$version): CFHIRResourceDevice
    {
        $this->fhirResource->addVersion(...$version);

        return $this;
    }

    /**
     * @return array
     */
    public function getVersion(): array
    {
        return $this->fhirResource->getVersion();
    }

    /**
     * @param array $property
     *
     * @return CFHIRResourceDevice
     */
    public function setProperty(FHIRDeviceProperty ...$property): CFHIRResourceDevice
    {
        $this->fhirResource->setProperty(...$property);

        return $this;
    }

    /**
     * @param array $property
     *
     * @return CFHIRResourceDevice
     */
    public function addProperty(FHIRDeviceProperty ...$property): CFHIRResourceDevice
    {
        $this->fhirResource->addProperty(...$property);

        return $this;
    }

    /**
     * @return array
     */
    public function getProperty(): array
    {
        return $this->fhirResource->getProperty();
    }

    /**
     * @param FHIRDeviceDeviceName ...$deviceName
     *
     * @return CFHIRResourceDevice
     */
    public function setDeviceName(FHIRDeviceDeviceName ...$deviceName): CFHIRResourceDevice
    {
        $this->fhirResource->setDeviceName(...$deviceName);

        return $this;
    }

    /**
     * @param FHIRDeviceDeviceName ...$deviceName
     *
     * @return CFHIRResourceDevice
     */
    public function addDeviceName(FHIRDeviceDeviceName ...$deviceName): CFHIRResourceDevice
    {
        $this->fhirResource->addDeviceName(...$deviceName);

        return $this;
    }

    /**
     * @return array
     */
    public function getDeviceName(): array
    {
        return $this->fhirResource->getDeviceName();
    }

    /**
     * Map property definition
     */
    protected function mapDefinition(): void
    {
        $this->fhirResource->setDefinition($this->object_mapping->mapDefinition());
    }

    /**
     * Map property udiCarrier
     */
    protected function mapUdiCarrier(): void
    {
        $this->fhirResource->setUdiCarrier(...$this->object_mapping->mapUdiCarrier());
    }

    /**
     * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * Map property statusReason
     */
    protected function mapStatusReason(): void
    {
        $this->fhirResource->setStatusReason(...$this->object_mapping->mapStatusReason());
    }

    /**
     * Map property mapDistinctIdentifier
     */
    protected function mapDistinctIdentifier(): void
    {
        $this->fhirResource->setDistinctIdentifier($this->object_mapping->mapDistinctIdentifier());
    }

    /**
     * Map property manufacturer
     */
    protected function mapManufacturer(): void
    {
        $this->fhirResource->setManufacturer($this->object_mapping->mapManufacturer());
    }

    /**
     * Map property manufactureDate
     */
    protected function mapManufactureDate(): void
    {
        $this->fhirResource->setManufactureDate($this->object_mapping->mapManufactureDate());
    }

    /**
     * Map property expirationDate
     */
    protected function mapExpirationDate(): void
    {
        $this->fhirResource->setExpirationDate($this->object_mapping->mapExpirationDate());
    }

    /**
     * Map property lotNumber
     */
    protected function mapLotNumber(): void
    {
        $this->fhirResource->setLotNumber($this->object_mapping->mapLotNumber());
    }

    /**
     * Map property serialNumber
     */
    protected function mapSerialNumber(): void
    {
        $this->fhirResource->setSerialNumber($this->object_mapping->mapSerialNumber());
    }

    /**
     * Map property deviceName
     */
    protected function mapDeviceName(): void
    {
        $this->fhirResource->setDeviceName(...$this->object_mapping->mapDeviceName());
    }

    /**
     * Map property modelNumber
     */
    protected function mapModelNumber(): void
    {
        $this->fhirResource->setModelNumber($this->object_mapping->mapModelNumber());
    }

    /**
     * Map property partNumber
     */
    protected function mapPartNumber(): void
    {
        $this->fhirResource->setPartNumber($this->object_mapping->mapPartNumber());
    }

    /**
     * Map property type
     */
    protected function mapType(): void
    {
        $this->fhirResource->setType($this->object_mapping->mapType());
    }

    /**
     * Map property specialization
     */
    protected function mapSpecialization(): void
    {
        $this->fhirResource->setSpecialization(...$this->object_mapping->mapSpecialization());
    }

    /**
     * Map property version
     */
    protected function mapVersion(): void
    {
        $this->fhirResource->setVersion(...$this->object_mapping->mapVersion());
    }

    /**
     * Map property property
     */
    protected function mapProperty(): void
    {
        $this->fhirResource->setProperty(...$this->object_mapping->mapProperty());
    }

    /**
     * Map property patient
     */
    protected function mapPatient(): void
    {
        $this->fhirResource->setPatient($this->object_mapping->mapPatient());
    }

    /**
     * Map property owner
     */
    protected function mapOwner(): void
    {
        $this->fhirResource->setOwner($this->object_mapping->mapOwner());
    }

    /**
     * Map property contact
     */
    protected function mapContact(): void
    {
        $this->fhirResource->setContact(...$this->object_mapping->mapContact());
    }

    /**
     * Map property location
     */
    protected function mapLocation(): void
    {
        $this->fhirResource->setLocation($this->object_mapping->mapLocation());
    }

    /**
     * Map property url
     */
    protected function mapUrl(): void
    {
        $this->fhirResource->setUrl($this->object_mapping->mapUrl());
    }

    /**
     * Map property note
     */
    protected function mapNote(): void
    {
        $this->fhirResource->setNote(...$this->object_mapping->mapNote());
    }

    /**
     * Map property safety
     */
    protected function mapSafety(): void
    {
        $this->fhirResource->setSafety($this->object_mapping->mapSafety());
    }

    /**
     * Map property parent
     */
    protected function mapParent(): void
    {
        $this->fhirResource->setParent($this->object_mapping->mapParent());
    }

    /**
     * @param FHIRDeviceUdiCarrier ...$udiCarrier
     *
     * @return CFHIRResourceDevice
     */
    public function setUdiCarrier(FHIRDeviceUdiCarrier ...$udiCarrier): CFHIRResourceDevice
    {
        $this->fhirResource->setUdiCarrier(...$udiCarrier);

        return $this;
    }

    /**
     * @param FHIRDeviceUdiCarrier ...$udiCarrier
     *
     * @return CFHIRResourceDevice
     */
    public function addUdiCarrier(FHIRDeviceUdiCarrier ...$udiCarrier): CFHIRResourceDevice
    {
        $this->fhirResource->addUdiCarrier(...$udiCarrier);

        return $this;
    }

    /**
     * @return array
     */
    public function getUdiCarrier(): array
    {
        return $this->fhirResource->getUdiCarrier();
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRDevice';
    }
}
