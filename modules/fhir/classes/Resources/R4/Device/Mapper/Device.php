<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Device\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\DeviceMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Device\CFHIRResourceDevice;

/**
 * Description
 */
class Device implements DelegatedObjectMapperInterface, DeviceMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var \Ox\Interop\Mes\Device */
    protected $object;

    /** @var CFHIRResourceDevice */
    protected CFHIRResource $resource;

    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceDevice::class];
    }

    /**
     * @inheritDoc
     * @throws CFHIRException
     */
    public function mapIdentifier(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapDefinition(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapUdiCarrier(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapStatus(): ?FHIRCode
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapStatusReason(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapDistinctIdentifier(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapManufacturer(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapManufactureDate(): ?FHIRDateTime
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapExpirationDate(): ?FHIRDateTime
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapLotNumber(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapSerialNumber(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     * @throws CFHIRException
     */
    public function mapDeviceName(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapModelNumber(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapPartNumber(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapType(): ?FHIRCodeableConcept
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapSpecialization(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapVersion(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapProperty(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapPatient(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapOwner(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapContact(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapLocation(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapUrl(): ?FHIRUri
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapNote(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapSafety(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapParent(): ?FHIRReference
    {
        return null;
    }
}
