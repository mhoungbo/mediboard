<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Parameters;

use Ox\Interop\Fhir\Contracts\Mapping\R4\ParametersMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceParametersInterface;
use Ox\Components\FhirCore\Interfaces\FHIRParametersInterface;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRParametersParameter;
use Ox\Interop\Fhir\Resources\CFHIRResource;

/**
 * FIHR patient resource
 */
class CFHIRResourceParameters extends CFHIRResource implements ResourceParametersInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Parameters";
    
    /** @var FHIRParametersInterface $fhirResource */
    protected $fhirResource;

    /** @var ParametersMappingInterface */
    protected $object_mapping;

    /**
     * @param FHIRParametersParameter ...$parameter
     *
     * @return CFHIRResourceParameters
     */
    public function setParameter(FHIRParametersParameter ...$parameter): CFHIRResourceParameters
    {
        $this->fhirResource->setParameter(...$parameter);

        return $this;
    }

    /**
     * @param FHIRParametersParameter ...$parameter
     *
     * @return CFHIRResourceParameters
     */
    public function addParameter(FHIRParametersParameter ...$parameter): CFHIRResourceParameters
    {
        $this->fhirResource->addParameter(...$parameter);

        return $this;
    }

    /**
     * @return FHIRParametersParameter[]
     */
    public function getParameter(): array
    {
        return $this->fhirResource->getParameter();
    }

    /**
     * @return void
     */
    public function mapParameter(): void
    {
        $this->fhirResource->setParameter(...$this->object_mapping->mapParameter());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRParameters';
    }
}
