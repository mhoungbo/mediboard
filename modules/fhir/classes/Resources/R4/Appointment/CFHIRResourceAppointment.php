<?php

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Appointment;

use Exception;
use Ox\Interop\Fhir\Contracts\Mapping\R4\AppointmentMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceAppointmentInterface;
use Ox\Components\FhirCore\Interfaces\FHIRAppointmentInterface;
use Ox\Components\FhirCore\Interfaces\FHIRResourceInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRPositiveInt;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUnsignedInt;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRAppointmentParticipant;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Resources\R4\Slot\CFHIRResourceSlot;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * Description
 */
class CFHIRResourceAppointment extends CFHIRDomainResource implements ResourceAppointmentInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Appointment';

    // attributes
    /** @var FHIRAppointmentInterface $fhirResource */
    protected $fhirResource;

    /** @var AppointmentMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            );
    }

    /**
     * @return FHIRAppointmentParticipant[]
     */
    public function getParticipant(): array
    {
        return $this->fhirResource->getParticipant();
    }

    /**
     * @param FHIRAppointmentParticipant ...$participant
     *
     * @return CFHIRResourceAppointment
     */
    public function setParticipant(FHIRAppointmentParticipant ...$participant): CFHIRResourceAppointment
    {
        $this->fhirResource->setParticipant(...$participant);

        return $this;
    }

    /**
     * @param FHIRAppointmentParticipant ...$participant
     *
     * @return CFHIRResourceAppointment
     */
    public function addParticipant(FHIRAppointmentParticipant ...$participant): CFHIRResourceAppointment
    {
        $this->fhirResource->addParticipant(...$participant);

        return $this;
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceAppointment
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceAppointment
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRCodeableConcept|null $cancelationReason
     *
     * @return CFHIRResourceAppointment
     */
    public function setCancelationReason(?FHIRCodeableConcept $cancelationReason): CFHIRResourceAppointment
    {
        $this->fhirResource->setCancelationReason($cancelationReason);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getCancelationReason(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getCancelationReason();
    }

    /**
     * @param FHIRCodeableConcept ...$serviceCategory
     *
     * @return CFHIRResourceAppointment
     */
    public function setServiceCategory(FHIRCodeableConcept ...$serviceCategory): CFHIRResourceAppointment
    {
        $this->fhirResource->setServiceCategory(...$serviceCategory);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$serviceCategory
     *
     * @return CFHIRResourceAppointment
     */
    public function addServiceCategory(FHIRCodeableConcept ...$serviceCategory): CFHIRResourceAppointment
    {
        $this->fhirResource->addServiceCategory(...$serviceCategory);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceCategory(): array
    {
        return $this->fhirResource->getServiceCategory();
    }

    /**
     * @param FHIRCodeableConcept ...$serviceType
     *
     * @return CFHIRResourceAppointment
     */
    public function setServiceType(FHIRCodeableConcept ...$serviceType): CFHIRResourceAppointment
    {
        $this->fhirResource->setServiceType(...$serviceType);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$serviceType
     *
     * @return CFHIRResourceAppointment
     */
    public function addServiceType(FHIRCodeableConcept ...$serviceType): CFHIRResourceAppointment
    {
        $this->fhirResource->addServiceType(...$serviceType);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceType(): array
    {
        return $this->fhirResource->getServiceType();
    }

    /**
     * @param FHIRCodeableConcept[] $specialty
     *
     * @return CFHIRResourceAppointment
     */
    public function setSpecialty(FHIRCodeableConcept ...$specialty): CFHIRResourceAppointment
    {
        $this->fhirResource->setSpecialty(...$specialty);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept[] $specialty
     *
     * @return CFHIRResourceAppointment
     */
    public function addSpecialty(FHIRCodeableConcept ...$specialty): CFHIRResourceAppointment
    {
        $this->fhirResource->addSpecialty(...$specialty);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->fhirResource->getSpecialty();
    }

    /**
     * @param FHIRCodeableConcept|null $appointmentType
     *
     * @return CFHIRResourceAppointment
     */
    public function setAppointmentType(?FHIRCodeableConcept $appointmentType): CFHIRResourceAppointment
    {
        $this->fhirResource->setAppointmentType($appointmentType);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getAppointmentType(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getAppointmentType();
    }

    /**
     * @param FHIRCodeableConcept[] $reasonCode
     *
     * @return CFHIRResourceAppointment
     */
    public function setReasonCode(FHIRCodeableConcept ...$reasonCode): CFHIRResourceAppointment
    {
        $this->fhirResource->setReasonCode(...$reasonCode);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept[] $reasonCode
     *
     * @return CFHIRResourceAppointment
     */
    public function addReasonCode(FHIRCodeableConcept ...$reasonCode): CFHIRResourceAppointment
    {
        $this->fhirResource->addReasonCode(...$reasonCode);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getReasonCode(): array
    {
        return $this->fhirResource->getReasonCode();
    }

    /**
     * @param FHIRReference[] $reasonReference
     *
     * @return CFHIRResourceAppointment
     */
    public function setReasonReference(FHIRReference ...$reasonReference): CFHIRResourceAppointment
    {
        $this->fhirResource->setReasonReference(...$reasonReference);

        return $this;
    }

    /**
     * @param FHIRReference[] $reasonReference
     *
     * @return CFHIRResourceAppointment
     */
    public function addReasonReference(FHIRReference ...$reasonReference): CFHIRResourceAppointment
    {
        $this->fhirResource->addReasonReference(...$reasonReference);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getReasonReference(): array
    {
        return $this->fhirResource->getReasonReference();
    }

    /**
     * @param FHIRUnsignedInt|null $priority
     *
     * @return CFHIRResourceAppointment
     */
    public function setPriority(?FHIRUnsignedInt $priority): CFHIRResourceAppointment
    {
        $this->fhirResource->setPriority($priority);

        return $this;
    }

    /**
     * @return FHIRUnsignedInt|null
     */
    public function getPriority(): ?FHIRUnsignedInt
    {
        return $this->fhirResource->getPriority();
    }

    /**
     * @param FHIRString|null $description
     *
     * @return CFHIRResourceAppointment
     */
    public function setDescription(?FHIRString $description): CFHIRResourceAppointment
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getDescription(): ?FHIRString
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @param FHIRReference ...$supportingInformation
     *
     * @return CFHIRResourceAppointment
     */
    public function setSupportingInformation(FHIRReference ...$supportingInformation): CFHIRResourceAppointment
    {
        $this->fhirResource->setSupportingInformation(...$supportingInformation);

        return $this;
    }

    /**
     * @param FHIRReference ...$supportingInformation
     *
     * @return CFHIRResourceAppointment
     */
    public function addSupportingInformation(FHIRReference ...$supportingInformation): CFHIRResourceAppointment
    {
        $this->fhirResource->addSupportingInformation(...$supportingInformation);

        return $this;
    }

    /**
     * @param FHIRInstant|null $start
     *
     * @return CFHIRResourceAppointment
     */
    public function setStart(?FHIRInstant $start): CFHIRResourceAppointment
    {
        $this->fhirResource->setStart($start);

        return $this;
    }

    /**
     * @return FHIRInstant|null
     */
    public function getStart(): ?FHIRInstant
    {
        return $this->fhirResource->getStart();
    }

    /**
     * @param FHIRInstant|null $end
     *
     * @return CFHIRResourceAppointment
     */
    public function setEnd(?FHIRInstant $end): CFHIRResourceAppointment
    {
        $this->fhirResource->setEnd($end);

        return $this;
    }

    /**
     * @return FHIRInstant|null
     */
    public function getEnd(): ?FHIRInstant
    {
        return $this->fhirResource->getEnd();
    }

    /**
     * @param FHIRPositiveInt|null $minutesDuration
     *
     * @return CFHIRResourceAppointment
     */
    public function setMinutesDuration(?FHIRPositiveInt $minutesDuration): CFHIRResourceAppointment
    {
        $this->fhirResource->setMinutesDuration($minutesDuration);

        return $this;
    }

    /**
     * @return FHIRPositiveInt|null
     */
    public function getMinutesDuration(): ?FHIRPositiveInt
    {
        return $this->fhirResource->getMinutesDuration();
    }

    /**
     * @param FHIRReference ...$slot
     *
     * @return CFHIRResourceAppointment
     */
    public function setSlot(FHIRReference ...$slot): CFHIRResourceAppointment
    {
        $this->fhirResource->setSlot(...$slot);

        return $this;
    }

    /**
     * @param FHIRReference ...$slot
     *
     * @return CFHIRResourceAppointment
     */
    public function addSlot(FHIRReference ...$slot): CFHIRResourceAppointment
    {
        $this->fhirResource->addSlot(...$slot);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getSlot(): array
    {
        return $this->fhirResource->getSlot();
    }

    /**
     * @param FHIRDateTime|null $created
     *
     * @return CFHIRResourceAppointment
     */
    public function setCreated(?FHIRDateTime $created): CFHIRResourceAppointment
    {
        $this->fhirResource->setCreated($created);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getCreated(): ?FHIRDateTime
    {
        return $this->fhirResource->getCreated();
    }

    /**
     * @param FHIRString|null $comment
     *
     * @return CFHIRResourceAppointment
     */
    public function setComment(?FHIRString $comment): CFHIRResourceAppointment
    {
        $this->fhirResource->setComment($comment);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getComment(): ?FHIRString
    {
        return $this->fhirResource->getComment();
    }

    /**
     * @param FHIRString|null $patientInstruction
     *
     * @return CFHIRResourceAppointment
     */
    public function setPatientInstruction(?FHIRString $patientInstruction): CFHIRResourceAppointment
    {
        $this->fhirResource->setPatientInstruction($patientInstruction);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getPatientInstruction(): ?FHIRString
    {
        return $this->fhirResource->getPatientInstruction();
    }

    /**
     * @param FHIRReference ...$basedOn
     *
     * @return CFHIRResourceAppointment
     */
    public function setBasedOn(FHIRReference ...$basedOn): CFHIRResourceAppointment
    {
        $this->fhirResource->setBasedOn(...$basedOn);

        return $this;
    }

    /**
     * @param FHIRReference ...$basedOn
     *
     * @return CFHIRResourceAppointment
     */
    public function addBasedOn(FHIRReference ...$basedOn): CFHIRResourceAppointment
    {
        $this->fhirResource->addBasedOn(...$basedOn);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->fhirResource->getBasedOn();
    }

    /**
     * @param FHIRPeriod ...$requestedPeriod
     *
     * @return CFHIRResourceAppointment
     */
    public function setRequestedPeriod(FHIRPeriod ...$requestedPeriod): CFHIRResourceAppointment
    {
        $this->fhirResource->setRequestedPeriod(...$requestedPeriod);

        return $this;
    }

    /**
     * @param FHIRPeriod ...$requestedPeriod
     *
     * @return CFHIRResourceAppointment
     */
    public function addRequestedPeriod(FHIRPeriod ...$requestedPeriod): CFHIRResourceAppointment
    {
        $this->fhirResource->addRequestedPeriod(...$requestedPeriod);

        return $this;
    }

    /**
     * @return FHIRPeriod[]
     */
    public function getRequestedPeriod(): array
    {
        return $this->fhirResource->getRequestedPeriod();
    }

    /**
     * @return FHIRReference[]
     */
    public function getSupportingInformation(): array
    {
        return $this->fhirResource->getSupportingInformation();
    }

    /**
     * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * Map property cancelationReason
     */
    protected function mapCancelationReason(): void
    {
        $this->fhirResource->setCancelationReason($this->object_mapping->mapCancelationReason());
    }

    /**
     * Map property serviceCategory
     */
    protected function mapServiceCategory(): void
    {
        $this->fhirResource->addServiceCategory(...$this->object_mapping->mapServiceCategory());
    }

    /**
     * Map property serviceType
     * @throws Exception
     */
    protected function mapServiceType(): void
    {
        $this->fhirResource->addServiceType(...$this->object_mapping->mapServiceType());
    }

    /**
     * Map property specialty
     */
    protected function mapSpecialty(): void
    {
        $this->fhirResource->addSpecialty(...$this->object_mapping->mapSpecialty());
    }

    /**
     * Map property appointmentType
     */
    protected function mapAppointmentType(): void
    {
        // not implemented
        $this->fhirResource->setAppointmentType($this->object_mapping->mapAppointmentType());
    }

    /**
     * Map property reasonCode
     */
    protected function mapReasonCode(): void
    {
        // not implemented
        $this->fhirResource->addReasonCode(...$this->object_mapping->mapReasonCode());
    }

    /**
     * Map property reasonReference
     */
    protected function mapReasonReference(): void
    {
        $this->fhirResource->addReasonReference(...$this->object_mapping->mapReasonReference());
    }

    /**
     * Map property priority
     */
    protected function mapPriority(): void
    {
        $this->fhirResource->setPriority($this->object_mapping->mapPriority());
    }

    /**
     * Map property description
     */
    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    /**
     * Map property supportingInformation
     */
    protected function mapSupportingInformation(): void
    {
        $this->fhirResource->addSupportingInformation(...$this->object_mapping->mapSupportingInformation());
    }

    /**
     * Map property start
     */
    protected function mapStart(): void
    {
        $this->fhirResource->setStart($this->object_mapping->mapStart());
    }

    /**
     * Map property end
     */
    protected function mapEnd(): void
    {
        $this->fhirResource->setEnd($this->object_mapping->mapEnd());
    }

    /**
     * Map property minutesDuration
     */
    protected function mapMinutesDuration(): void
    {
        $this->fhirResource->setMinutesDuration($this->object_mapping->mapMinutesDuration());
    }

    /**
     * Map property slot
     */
    protected function mapSlot(string $resource_class = CFHIRResourceSlot::class): void
    {
        $this->fhirResource->addSlot(...$this->object_mapping->mapSlot($resource_class));
    }

    /**
     * Map property created
     */
    protected function mapCreated(): void
    {
        $this->fhirResource->setCreated($this->object_mapping->mapCreated());
    }

    /**
     * Map property comment
     */
    protected function mapComment(): void
    {
        $this->fhirResource->setComment($this->object_mapping->mapComment());
    }

    /**
     * Map property patientInstruction
     */
    protected function mapPatientInstruction(): void
    {
        $this->fhirResource->setPatientInstruction($this->object_mapping->mapPatientInstruction());
    }

    /**
     * Map property basedOn
     */
    protected function mapBasedOn(): void
    {
        $this->fhirResource->addBasedOn(...$this->object_mapping->mapBasedOn());
    }

    /**
     * Map property participant
     */
    protected function mapParticipant(): void
    {
        $this->fhirResource->addParticipant(...$this->object_mapping->mapParticipant());
    }

    /**
     * Map property requestedPeriod
     * @throws Exception
     */
    protected function mapRequestedPeriod(): void
    {
        $this->fhirResource->addRequestedPeriod(...$this->object_mapping->mapRequestedPeriod());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRAppointment';
    }
}
