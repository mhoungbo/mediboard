<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Appointment\Mapper;

use Exception;
use Ox\AppFine\Server\CEvenementMedical;
use Ox\Core\CMbDT;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRAppointmentParticipant;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Profiles\CFHIRMES;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\Appointment\Profiles\InteropSante\CFHIRResourceAppointmentFR;
use Ox\Interop\Fhir\Resources\R4\Patient\Profiles\InteropSante\CFHIRResourcePatientFR;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Mediboard\Patients\CMedecinExercicePlace;

/**
 * Description
 */
class ENSFrAppointment extends FrAppointment
{
    /** @var CEvenementMedical */
    protected $object;

    /** @var CFHIRResourceAppointmentFR */
    protected CFHIRResource $resource;

    /**
     * @throws Exception
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        parent::setResource($resource, $object);


        $this->object->loadRefResponsable();
        $this->object->loadRefExercicePlace();
        $this->object->loadRefPatient();
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceAppointmentFR::class];
    }

    public function onlyProfiles(): array
    {
        return [CFHIRMES::class];
    }

    public function mapStatus(): ?FHIRCode
    {
        if ($this->object->cancel) {
            return (new FHIRCode())->setValue('cancelled');
        }

        if (CMbDT::dateTime() < $this->object->date_debut) {
            return (new FHIRCode())->setValue('booked');
        } elseif (CMbDT::dateTime() > $this->object->date_debut && CMbDT::dateTime() < $this->object->date_fin) {
            return (new FHIRCode())->setValue('arrived');
        } else {
            return (new FHIRCode())->setValue('fulfilled');
        }
    }

    public function mapServiceType(): array
    {
        $system       = 'http://terminology.hl7.org/ValueSet/v3-ActEncounterCode';
        $code         = $this->object->teleconsultation ? 'VR' : 'AMB';
        $display_name = $this->object->teleconsultation ? 'virtual' : 'ambulatory';

        return [
            (new FHIRCodeableConcept())
                ->setCoding(
                    (new FHIRCoding())
                        ->setSystem($system)
                        ->setCode($code)
                        ->setDisplay($display_name)
                ),
        ];
    }

    /**
     * @throws Exception
     */
    public function mapSpecialty(): array
    {
        $practitioner = $this->object->_ref_responsable;
        $meps         = $practitioner->getMedecinExercicePlaces();
        $mep          = new CMedecinExercicePlace();

        /** @var CMedecinExercicePlace $_mep */
        foreach ($meps as $_mep) {
            if ($_mep->exercice_place_id === $this->object->_ref_exercice_place->_id) {
                $mep = $_mep;
            }
        }

        if ($mep->disciplines) {
            $coding = $this->setMedecinSpecialty($mep->disciplines);

            if (!empty($coding)) {
                return [(new FHIRCodeableConcept())->setCoding(...$coding)];
            }
        }

        return [];
    }


    /**
     * @param string $discipline
     *
     * @return array
     */
    private function setMedecinSpecialty(string $discipline): array
    {
        if (!$discipline) {
            return [];
        }

        $exploded_code = explode(' : ', $discipline);

        $system      = 'https://mos.esante.gouv.fr/NOS/TRE_R38-SpecialiteOrdinale/FHIR/TRE-R38-SpecialiteOrdinale';
        $code        = $exploded_code[0];
        $displayName = $exploded_code[1];

        return [
            (new FHIRCoding())
                ->setSystem($system)
                ->setCode($code)
                ->setDisplay($displayName),
        ];
    }

    /**
     * @throws Exception
     */
    public function mapCreated(): ?FHIRDateTime
    {
        return (new FHIRDateTime())->setValue(CFHIR::getTimeUtc($this->object->creation_datetime, false));
    }

    public function mapStart(): ?FHIRInstant
    {
        return (new FHIRInstant())->setValue(CFHIR::getTimeUtc($this->object->date_debut, false));
    }

    public function mapEnd(): ?FHIRInstant
    {
        return (new FHIRInstant())->setValue(CFHIR::getTimeUtc($this->object->date_fin, false));
    }

    public function mapComment(): ?FHIRString
    {
        return (new FHIRString())->setValue($this->object->remarques);
    }

    public function mapDescription(): ?FHIRString
    {
        return (new FHIRString())->setValue($this->object->libelle);
    }

    public function mapParticipant(): array
    {
        $participants = [];

        $patient = $this->object->_ref_patient;

        $participant_role_patient = (new FHIRAppointmentParticipant())
            ->setActor(
                $this->resource->addReference(
                    CFHIRResourcePatientFR::class,
                    $patient
                )
            )
            ->setStatus((new FHIRCode())->setValue('accepted'));

        $participants[] = $participant_role_patient;

        $practitioner = $this->object->_ref_responsable;

        $participant_role_practitioner = (new FHIRAppointmentParticipant())
            ->setActor(
                $this->resource->addReference(
                    CFHIRResourcePractitionerFR::class,
                    $practitioner
                )
            )
            ->setStatus((new FHIRCode())->setValue('accepted'));

        $participants[] = $participant_role_practitioner;

        return $participants;
    }
}
