<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Appointment\Mapper;

use Com\Tecnick\Barcode\Type\Square\QrCode\Data;
use Exception;
use Ox\Core\CAppUI;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\AppointmentMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRPositiveInt;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUnsignedInt;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRAppointmentParticipant;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Appointment\CFHIRResourceAppointment;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\CFHIRResourcePractitionerRole;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Patients\CMedecin;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

use function Ox\Interop\POCFHIR\Generated\setValue;

/**
 * Description
 */
class Appointment implements DelegatedObjectMapperInterface, AppointmentMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CConsultation */
    protected $object;

    /** @var CFHIRResourceAppointment */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CConsultation && $object->_id;
    }

    /**
     * @param CFHIRResource $resource
     * @param CConsultation|mixed $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
        $this->object->loadRefPlageConsult();
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): ?array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceAppointment::class];
    }

    public function mapStatus(): ?FHIRCode
    {
        if ($this->object->annule) {
            if ($this->object->motif_annulation && $this->object->motif_annulation == 'not_arrived') {
                return (new FHIRCode())
                    ->setValue('noshow');
            } else {
                return (new FHIRCode())
                    ->setValue('cancelled');
            }
        }

        switch ($this->object->chrono) {
            case CConsultation::DEMANDE:
                return (new FHIRCode())
                    ->setValue('proposed');
            case CConsultation::PLANIFIE:
                return (new FHIRCode())
                    ->setValue('booked');
            case CConsultation::PATIENT_ARRIVE:
            case CConsultation::EN_COURS:
                return (new FHIRCode())
                    ->setValue('arrived');
            case CConsultation::TERMINE:
                return (new FHIRCode())
                    ->setValue('fulfilled');
            default:
        }

        return null;
    }

    public function mapCancelationReason(): ?FHIRCodeableConcept
    {
        if (!$this->object->annule || !$this->object->motif_annulation) {
            return null;
        }

        $system = 'urn:oid:2.16.840.1.113883.4.642.3.1381';

        if ($this->object->motif_annulation === 'not_arrived') {
            $code    = '';
            $display = '';
        } elseif ($this->object->motif_annulation === 'by_patient') {
            $code    = 'pat';
            $display = 'Patient';
        } else {
            $code    = 'other';
            $display = 'Other';
        }

        $text = $display;

        return (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($display)
            );
    }

    public function mapServiceCategory(): array
    {
        $system      = 'http://terminology.hl7.org/CodeSystem/service-category';
        $code        = '35';
        $displayName = 'Hospital';
        $text        = 'Hospital';

        return [
            (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($displayName)
            )
        ];
    }

    /**
     * @throws Exception
     */
    public function mapServiceType(): array
    {
        if (!$this->object->categorie_id) {
            return [];
        }

        $categorie = $this->object->loadRefCategorie();

        return [
            (new FHIRCodeableConcept())
                ->setCoding(
                    (new FHIRCoding())
                        ->setSystem('urn:oid:' . CAppUI::conf('mb_oid'))
                        ->setCode($categorie->_id)
                        ->setDisplay($categorie->nom_categorie)
                )
        ];
    }

    public function mapSpecialty(): array
    {
        // not implemented
        return [];
    }

    public function mapAppointmentType(): ?FHIRCodeableConcept
    {
        // not implemented
        return null;
    }

    public function mapReasonCode(): array
    {
        // not implemented
        return [];
    }

    public function mapReasonReference(): array
    {
        // not implemented
        return [];
    }

    public function mapPriority(): ?FHIRUnsignedInt
    {
        // not implemented
        return null;
    }

    public function mapDescription(): ?FHIRString
    {
        return (new FHIRString())
            ->setValue($this->object->motif);
    }

    public function mapSupportingInformation(): array
    {
        // not implemented
        return [];
    }

    /**
     * @throws Exception
     */
    public function mapStart(): ?FHIRInstant
    {
        return (new FHIRInstant())
            ->setValue(CFHIR::getTimeUtc($this->object->_datetime, false));
    }

    /**
     * @throws Exception
     */
    public function mapEnd(): ?FHIRInstant
    {
        return (new FHIRInstant())
            ->setValue(CFHIR::getTimeUtc($this->object->_date_fin, false));
    }

    public function mapMinutesDuration(): ?FHIRPositiveInt
    {
        return (new FHIRPositiveInt())
            ->setValue($this->object->_duree);
    }

    /**
     * @param string $resource_class
     *
     * @return array
     * @throws InvalidArgumentException
     * @throws ReflectionException
     * @throws Exception
     */
    public function mapSlot(string $resource_class): array
    {
        $slots = [];

        $refs_slots = $this->object->loadRefSlots();

        foreach ($refs_slots as $_slot) {
            $slots[] = $this->resource->addReference($resource_class, $_slot);
        }

        return $slots;
    }

    /**
     * @throws Exception
     */
    public function mapCreated(): ?FHIRDateTime
    {
        return (new FHIRDateTime())
            ->setValue(CFHIR::getTimeUtc($this->object->_date, false));
    }

    public function mapComment(): ?FHIRString
    {
        return (new FHIRString())
            ->setValue($this->object->rques);
    }

    public function mapPatientInstruction(): ?FHIRString
    {
        // not implemented
        return null;
    }

    public function mapBasedOn(): array
    {
        // not implemented
        return [];
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function mapParticipant(): array
    {
        $participants = [];

        $status_patient = $this->object->annule &&
        ($this->object->motif_annulation == 'by_patient' || $this->object->motif_annulation == 'not_arrived')
            ? (new FHIRCode())->setValue('declined') : (new FHIRCode())->setValue('accepted');

        $status_practitioner = $this->object->annule && $this->object->motif_annulation == 'other'
            ? (new FHIRCode())->setValue('declined') : (new FHIRCode())->setValue('accepted');

        $practitionerRole = $this->object->loadRefPraticien();

        if ($practitionerRole->_id) {
            // PractitionerRole
            $participant_role_praticien = (new FHIRAppointmentParticipant())
                ->setActor(
                    $this->resource->addReference(
                        CFHIRResourcePractitionerRole::class,
                        $practitionerRole
                    )
                )
                ->setRequired((new FHIRCode())->setValue('required'))
                ->setStatus((new FHIRCode())->setValue('needs-action'));

            $participants[] = $participant_role_praticien;
        }

        $practitioner       = new CMedecin();
        $practitioner->rpps = $practitionerRole->rpps;
        $practitioner->loadMatchingObject();

        if ($practitioner->_id) {
            // Practitioner
            $participant_praticien = (new FHIRAppointmentParticipant())
                ->setType(
                    (new FHIRCodeableConcept())
                        ->setText('Praticien')
                        ->setCoding(
                            (new FHIRCoding())
                                ->setSystem('urn:oid:2.16.840.1.113883.4.642.3.250')
                                ->setCode('ADM')
                                ->setDisplay('admitter')
                    )
                )
                ->setActor($this->resource->addReference(
                    CFHIRResourcePractitioner::class,
                    $practitioner
                ))
                ->setRequired((new FHIRCode())->setValue('required'))
                ->setStatus($status_practitioner);

            $participants[] = $participant_praticien;
        }

        $patient = $this->object->loadRefPatient();

        if ($patient->_id) {
            // Patient
            $participant_patient = (new FHIRAppointmentParticipant())
                ->setActor($this->resource->addReference(CFHIRResourcePatient::class, $patient))
                ->setRequired((new FHIRCode())->setValue('required'))
                ->setStatus($status_patient);

            $participants[] = $participant_patient;
        }

        return $participants;
    }

    /**
     * @throws Exception
     */
    public function mapRequestedPeriod(): array
    {
        return [
            (new FHIRPeriod())
                ->setStart(CFHIR::getTimeUtc($this->object->_datetime, false))
                ->setEnd(CFHIR::getTimeUtc($this->object->_date_fin, false))
        ];
    }
}
