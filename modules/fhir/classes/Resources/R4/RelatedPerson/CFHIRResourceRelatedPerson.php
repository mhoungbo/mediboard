<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\RelatedPerson;

use Ox\Interop\Fhir\Contracts\Mapping\R4\RelatedPersonMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceRelatedPersonInterface;
use Ox\Components\FhirCore\Interfaces\FHIRRelatedPersonInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRRelatedPersonCommunication;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * FHIR RelatedPerson ressource
 */
class CFHIRResourceRelatedPerson extends CFHIRDomainResource implements ResourceRelatedPersonInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = "RelatedPerson";

    // attributes
    /** @var FHIRRelatedPersonInterface */
    protected $fhirResource;

    /** @var RelatedPersonMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            );
    }

    /**
     * @param FHIRBoolean|null $active
     *
     * @return CFHIRResourceRelatedPerson
     */
    public function setActive(?FHIRBoolean $active): CFHIRResourceRelatedPerson
    {
        $this->fhirResource->setActive($active);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getActive(): ?FHIRBoolean
    {
        return $this->fhirResource->getActive();
    }

    /**
     * @param FHIRReference|null $patient
     *
     * @return CFHIRResourceRelatedPerson
     */
    public function setPatient(?FHIRReference $patient): CFHIRResourceRelatedPerson
    {
        $this->fhirResource->setPatient($patient);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getPatient(): ?FHIRReference
    {
        return $this->fhirResource->getPatient();
    }

    /**
     * @param FHIRCode|null $gender
     *
     * @return CFHIRResourceRelatedPerson
     */
    public function setGender(?FHIRCode $gender): CFHIRResourceRelatedPerson
    {
        $this->fhirResource->setGender($gender);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getGender(): ?FHIRCode
    {
        return $this->fhirResource->getGender();
    }

    /**
     * @param FHIRDate|null $birthDate
     *
     * @return CFHIRResourceRelatedPerson
     */
    public function setBirthDate(?FHIRDate $birthDate): CFHIRResourceRelatedPerson
    {
        $this->fhirResource->setBirthDate($birthDate);

        return $this;
    }

    /**
     * @return FHIRDate|null
     */
    public function getBirthDate(): ?FHIRDate
    {
        return $this->fhirResource->getBirthDate();
    }

    /**
     * @param FHIRPeriod|null $period
     *
     * @return CFHIRResourceRelatedPerson
     */
    public function setPeriod(?FHIRPeriod $period): CFHIRResourceRelatedPerson
    {
        $this->fhirResource->setPeriod($period);

        return $this;
    }

    /**
     * @return FHIRPeriod|null
     */
    public function getPeriod(): ?FHIRPeriod
    {
        return $this->fhirResource->getPeriod();
    }

    /**
     * Map property active
     */
    protected function mapActive(): void
    {
        $this->fhirResource->setActive($this->object_mapping->mapActive());
    }

    /**
     * Map property patient
     */
    protected function mapPatient(): void
    {
        $this->fhirResource->setPatient($this->object_mapping->mapPatient());
    }

    /**
     * Map property relationship
     */
    protected function mapRelationship(): void
    {
        $this->object_mapping = $this->object_mapping->mapRelationship();
    }

    /**
     * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName(...$this->object_mapping->mapName());
    }

    /**
     * Map property telecom
     */
    protected function mapTelecom(): void
    {
        $this->fhirResource->setTelecom(...$this->object_mapping->mapTelecom());
    }

    /**
     * Map property gender
     */
    protected function mapGender(): void
    {
        $this->fhirResource->setGender($this->object_mapping->mapGender());
    }

    /**
     * Map property birthDate
     */
    protected function mapBirthDate(): void
    {
        $this->fhirResource->setBirthDate($this->object_mapping->mapBirthDate());
    }

    /**
     * Map property address
     */
    protected function mapAddress(): void
    {
        $this->fhirResource->setAddress(...$this->object_mapping->mapAddress());
    }

    /**
     * Map property photo
     */
    protected function mapPhoto(): void
    {
        $this->fhirResource->setPhoto(...$this->object_mapping->mapPhoto());
    }

    /**
     * Map property period
     */
    protected function mapPeriod(): void
    {
        $this->fhirResource->setPeriod($this->object_mapping->mapPeriod());
    }

    /**
     * Map property communication
     */
    protected function mapCommunication(): void
    {
        $this->fhirResource->setCommunication(...$this->object_mapping->mapCommunication());
    }

    /**
     * @param FHIRCodeableConcept ...$relationship
     *
     * @return self
     */
    public function setRelationship(FHIRCodeableConcept ...$relationship): self
    {
        $this->fhirResource->setRelationship(...$relationship);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$relationship
     *
     * @return self
     */
    public function addRelationship(FHIRCodeableConcept ...$relationship): self
    {
        $this->fhirResource->addRelationship(...$relationship);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getRelationship(): array
    {
        return $this->fhirResource->getRelationship();
    }

    /**
     * @param FHIRHumanName ...$name
     *
     * @return self
     */
    public function setName(FHIRHumanName ...$name): self
    {
        $this->fhirResource->setName(...$name);

        return $this;
    }

    /**
     * @param FHIRHumanName ...$name
     *
     * @return self
     */
    public function addName(FHIRHumanName ...$name): self
    {
        $this->fhirResource->addName(...$name);

        return $this;
    }

    /**
     * @return FHIRHumanName[]
     */
    public function getName(): array
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return self
     */
    public function setTelecom(FHIRContactPoint ...$telecom): self
    {
        $this->fhirResource->setTelecom(...$telecom);

        return $this;
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return self
     */
    public function addTelecom(FHIRContactPoint ...$telecom): self
    {
        $this->fhirResource->addTelecom(...$telecom);

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->fhirResource->getTelecom();
    }

    /**
     * @param FHIRAddress ...$address
     *
     * @return self
     */
    public function setAddress(FHIRAddress ...$address): self
    {
        $this->fhirResource->setAddress(...$address);

        return $this;
    }

    /**
     * @param FHIRAddress ...$address
     *
     * @return self
     */
    public function addAddress(FHIRAddress ...$address): self
    {
        $this->fhirResource->addAddress(...$address);

        return $this;
    }

    /**
     * @return FHIRAddress[]
     */
    public function getAddress(): array
    {
        return $this->fhirResource->getAddress();
    }

    /**
     * @param FHIRAttachment ...$photo
     *
     * @return self
     */
    public function setPhoto(FHIRAttachment ...$photo): self
    {
        $this->fhirResource->setPhoto(...$photo);

        return $this;
    }

    /**
     * @param FHIRAttachment ...$photo
     *
     * @return self
     */
    public function addPhoto(FHIRAttachment ...$photo): self
    {
        $this->fhirResource->addPhoto(...$photo);

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getPhoto(): array
    {
        return $this->fhirResource->getPhoto();
    }

    /**
     * @param FHIRRelatedPersonCommunication ...$communication
     *
     * @return self
     */
    public function setCommunication(FHIRRelatedPersonCommunication ...$communication): self
    {
        $this->fhirResource->setCommunication(...$communication);

        return $this;
    }

    /**
     * @param FHIRRelatedPersonCommunication ...$communication
     *
     * @return self
     */
    public function addCommunication(FHIRRelatedPersonCommunication ...$communication): self
    {
        $this->fhirResource->addCommunication(...$communication);

        return $this;
    }

    /**
     * @return FHIRRelatedPersonCommunication[]
     */
    public function getCommunication(): array
    {
        return $this->fhirResource->getCommunication();
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRRelatedPerson';
    }
}
