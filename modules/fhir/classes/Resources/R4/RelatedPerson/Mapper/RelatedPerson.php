<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\RelatedPerson\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\RelatedPersonMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientCommunication;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Interop\Fhir\Resources\R4\RelatedPerson\CFHIRResourceRelatedPerson;
use Ox\Mediboard\Patients\CCorrespondantPatient;

/**
 * Description
 */
class RelatedPerson implements DelegatedObjectMapperInterface, RelatedPersonMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CCorrespondantPatient */
    protected $object;

    /** @var CFHIRResourceRelatedPerson */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceRelatedPerson::class];
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CCorrespondantPatient && $object->_id;
    }

    /**
     * @inheritDoc
     */
    public function mapActive(): ?FHIRBoolean
    {
        return new FHIRBoolean(true);
    }

    /**
     * @inheritDoc
     */
    public function mapPatient(): ?FHIRReference
    {
        $patient = $this->object->loadRefPatient();

        return $this->resource->addReference(CFHIRResourcePatient::class, $patient);
    }

    /**
     * @inheritDoc
     */
    public function mapRelationship(): array
    {
        $relation = $this->object->parente;
        $system   = 'urn:oid:2.16.840.1.113883.4.642.3.449';

        //TODO Etayer les relations
        switch ($relation) {
            case 'mere':
                $code    = 'MTH';
                $display = 'mother';
                break;
            case 'pere':
                $code    = 'FTH';
                $display = 'father';
                break;
            default:
                $code    = 'CHILD';
                $display = 'child';
                break;
        }

        $text   = $display;
        $coding = (new FHIRCoding())
            ->setCode($code)
            ->setDisplay($display)
            ->setSystem($system);

        return [
            (new FHIRCodeableConcept())
                ->setCoding($coding)
                ->setText($text),
        ];
    }

    /**
     * @inheritDoc
     */
    public function mapName(): array
    {
        $names = [];
        // name
        $names[] = (new FHIRHumanName())
            ->setFamily($this->object->nom_jeune_fille ?: $this->object->nom)
            ->setGiven($this->object->prenom)
            ->setUse('official');

        $has_usual_familly = $this->object->nom && $this->object->nom !== $this->object->nom_jeune_fille;
        if ($has_usual_familly) {
            $names[] = (new FHIRHumanName())
                ->setFamily($this->object->nom)
                ->setGiven($this->object->prenom)
                ->setUse('usual');
        }

        return $names;
    }

    /**
     * @inheritDoc
     */
    public function mapTelecom(): array
    {
        $telecom = [];
        if ($this->object->tel) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel);
        }

        if ($this->object->tel_autre) {
            $telecom[] = (new FHIRContactPoint())
                ->setValue($this->object->tel_autre)
                ->setSystem('phone');
        }

        if ($this->object->mob) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->mob);
        }

        if ($this->object->fax) {
            $telecom[] = (new FHIRContactPoint())
                ->setValue($this->object->fax)
                ->setSystem('fax');
        }

        if ($this->object->email) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('email')
                ->setValue($this->object->email);
        }

        return $telecom;
    }

    /**
     * @inheritDoc
     */
    public function mapGender(): ?FHIRCode
    {
        return (new FHIRCode())->setValue($this->resource->formatGender($this->object->sex));
    }

    /**
     * @inheritDoc
     */
    public function mapBirthDate(): ?FHIRDate
    {
        return (new FHIRDate())->setValue($this->object->naissance);
    }

    /**
     * @inheritDoc
     */
    public function mapAddress(): array
    {
        $address = [];

        if ($this->object->adresse || $this->object->ville || $this->object->cp) {
            $address[] = (new FHIRAddress())
                ->setUse('work')
                ->setType('postal')
                ->setLine(...preg_split('/[\r\n]+/', $this->object->adresse) ?? '')
                ->setCity($this->object->ville ?? null)
                ->setPostalCode('postalCode');
        }

        return $address;
    }

    /**
     * @inheritDoc
     */
    public function mapPhoto(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapPeriod(): ?FHIRPeriod
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapCommunication(): array
    {
        $coding = (new FHIRCoding())
            ->setSystem('urn:oid:2.16.840.1.113883.4.642.3.20')
            ->setDisplay('French (France)')
            ->setCode('fr-FR');

        $codeable = (new FHIRCodeableConcept())
            ->setCoding($coding)
            ->setText('Fran�ais de France');

        $patientCommunication = (new FHIRPatientCommunication())
            ->setLanguage($codeable)
            ->setPreferred(true);

        return [$patientCommunication];
    }
}
