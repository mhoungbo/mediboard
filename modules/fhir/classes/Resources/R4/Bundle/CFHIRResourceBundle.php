<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Bundle;

use Ox\Core\CMbSecurity;
use Ox\Core\CStoredObject;
use Ox\Interop\Fhir\Contracts\Mapping\R4\BundleMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceBundleInterface;
use Ox\Interop\Fhir\Controllers\CFHIRController;
use Ox\Interop\Fhir\Exception\CFHIRExceptionNotSupported;
use Ox\Components\FhirCore\Interfaces\FHIRBundleInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRSignature;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUnsignedInt;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRBundleEntry;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRBundleLink;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * FIHR patient resource
 */
class CFHIRResourceBundle extends CFHIRResource implements ResourceBundleInterface
{
    /** @var string */
    public const TYPE_BATCH = "batch";
    /** @var string */
    public const TYPE_TRANSACTION = "transaction";
    /** @var string */
    public const TYPE_DOCUMENT = "document";
    /** @var string */
    public const TYPE_COLLECTION = "collection";
    /** @var string */
    public const TYPE_SEARCHSET = "searchset";
    /** @var string */
    public const TYPE_HISTORY = "history";
    /** @var string */
    public const TYPE_BATCH_RESPONSE = "batch-response";
    /** @var string */
    public const TYPE_TRANSACTION_RESPONSE = "transaction-response";
    /** @var string */
    public const TYPE_MESSAGE = "message";

    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Bundle';

    /** @var string */
    public const VERSION_NORMATIVE = '4.0';

    // attributes
    /** @var FHIRBundleInterface $fhirResource */
    protected $fhirResource;
    
    /** @var BundleMappingInterface */
    protected $object_mapping;

    /**
     * @return FHIRIdentifier|null
     */
    public function getIdentifier(): ?FHIRIdentifier
    {
        return $this->fhirResource->getIdentifier();
    }

    /**
     * @return FHIRCode|null
     */
    public function getType(): ?FHIRCode
    {
        return $this->fhirResource->getType();
    }

    /**
     * @return FHIRInstant|null
     */
    public function getTimestamp(): ?FHIRInstant
    {
        return $this->fhirResource->getTimestamp();
    }

    /**
     * @return FHIRUnsignedInt|null
     */
    public function getTotal(): ?FHIRUnsignedInt
    {
        return $this->fhirResource->getTotal();
    }

    /**
     * @return FHIRBundleLink[]
     */
    public function getLink(): array
    {
        return $this->fhirResource->getLink();
    }

    /**
     * @return FHIRBundleEntry[]
     */
    public function getEntry(): array
    {
        return $this->fhirResource->getEntry();
    }

    /**
     * @return FHIRSignature|null
     */
    public function getSignature(): ?FHIRSignature
    {
        return $this->fhirResource->getSignature();
    }

    /**
     * @param FHIRIdentifier|null $identifier
     *
     * @return CFHIRResourceBundle
     */
    public function setIdentifier(?FHIRIdentifier $identifier): CFHIRResourceBundle
    {
        $this->fhirResource->setIdentifier($identifier);

        return $this;
    }

    /**
     * @param FHIRCode $type
     *
     * @return CFHIRResourceBundle
     */
    public function setType(?FHIRCode $type): CFHIRResourceBundle
    {
        $available_types = [
            self::TYPE_BATCH,
            self::TYPE_BATCH_RESPONSE,
            self::TYPE_COLLECTION,
            self::TYPE_DOCUMENT,
            self::TYPE_HISTORY,
            self::TYPE_MESSAGE,
            self::TYPE_SEARCHSET,
            self::TYPE_TRANSACTION,
            self::TYPE_TRANSACTION_RESPONSE,
        ];

        if ($type) {
            $value_type = $type->getValue();
            if ($value_type && !in_array($value_type, $available_types, true)) {
                throw new CFHIRExceptionNotSupported(
                    "This type : $value_type is not supported type for Bundle resource"
                );
            }
        }

        $this->fhirResource->setType($type);

        return $this;
    }

    /**
     * @param FHIRInstant|null $timestamp
     *
     * @return CFHIRResourceBundle
     */
    public function setTimestamp(?FHIRInstant $timestamp): CFHIRResourceBundle
    {
        $this->fhirResource->setTimestamp($timestamp);

        return $this;
    }

    /**
     * @param FHIRUnsignedInt|null $total
     *
     * @return CFHIRResourceBundle
     */
    public function setTotal(?FHIRUnsignedInt $total): CFHIRResourceBundle
    {
        $this->fhirResource->setTotal($total);

        return $this;
    }

    /**
     * @param FHIRBundleLink ...$links
     *
     * @return CFHIRResourceBundle
     */
    public function setLink(FHIRBundleLink ...$links): CFHIRResourceBundle
    {
        $this->fhirResource->setLink(...$links);

        return $this;
    }

    /**
     * @param FHIRBundleLink|null $link
     *
     * @return self
     */
    public function addLink(FHIRBundleLink ...$links): self
    {
        $this->fhirResource->addLink(...$links);

        return $this;
    }

    /**
     * @param FHIRBundleEntry ...$entry
     *
     * @return self
     */
    public function addEntry(FHIRBundleEntry ...$entry): self
    {
        $this->fhirResource->addEntry(...$entry);

        return $this;
    }

    /**
     * @param FHIRBundleEntry ...$entries
     *
     * @return CFHIRResourceBundle
     */
    public function setEntry(FHIRBundleEntry ...$entries): CFHIRResourceBundle
    {
        $this->fhirResource->setEntry(...$entries);

        return $this;
    }

    /**
     * @param FHIRSignature|null $signature
     *
     * @return CFHIRResourceBundle
     */
    public function setSignature(?FHIRSignature $signature): CFHIRResourceBundle
    {
        $this->fhirResource->setSignature($signature);

        return $this;
    }

    /**
     * @return CCapabilitiesResource
     */
    protected function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionUpdate::NAME,
                ]
            );
    }

    /**
     * @param CFHIRResource $resource
     *
     * @return $this
     */
    public function addResource(CFHIRResource $resource): self
    {
        $entry = new FHIRBundleEntry();

        if ($resource_id = $resource->getResourceId()) {
            $fullUrl = CFHIRController::getUrl(
                "fhir_read",
                [
                    'resource'    => $resource->getResourceType(),
                    'resource_id' => $resource_id,
                ]
            );

            $entry->setFullUrl($fullUrl);
        }

        $entry->setResource($resource->getFhirResource());

        return $this->addEntry($entry);
    }

    /**
     * @param CStoredObject $object
     *
     * @return string
     */
    protected function getInternalId(?CStoredObject $object = null): string
    {
        return CMbSecurity::generateUUID();
    }

    /**
     * @return CFHIRInteractionSearch|null
     */
    public function getNextRequest(): ?CFHIRInteractionSearch
    {
        if ((!$link = $this->getLinkForRelation('next')) || !$link->getUrl())  {
            return null;
        }

        $url = $link->getUrl()->getValue();

        return CFHIRInteractionSearch::makeFromUrl($url);
    }

    /**
     * Get link for given relation
     *
     * @param string $type
     *
     * @return FHIRBundleLink|null
     */
    public function getLinkForRelation(string $type): ?FHIRBundleLink
    {
        foreach ($this->getLink() as $link) {
            if ($link->getRelation() && $link->getRelation()->getValue() === $type) {
                return $link;
            }
        }

        return null;
    }

    /**
     * * Map property identifier
     */
    protected function mapIdentifier(): void
    {
        $this->fhirResource->setIdentifier($this->object_mapping->mapIdentifier());
    }

    /**
     * * Map property type
     */
    protected function mapType(): void
    {
        $this->fhirResource->setType($this->object_mapping->mapType());
    }

    /**
     * * Map property timestamp
     */
    protected function mapTimestamp(): void
    {
        $this->fhirResource->setTimestamp($this->object_mapping->mapTimestamp());
    }

    /**
     * * Map property total
     */
    protected function mapTotal(): void
    {
        $this->fhirResource->setTotal($this->object_mapping->mapTotal());
    }

    /**
     * * Map property link
     */
    protected function mapLink(): void
    {
        $this->fhirResource->setLink(...$this->object_mapping->mapLink());
    }

    /**
     * * Map property entry
     */
    protected function mapEntry(): void
    {
        $this->fhirResource->setEntry(...$this->object_mapping->mapEntry());
    }

    /**
     * * Map property signature
     */
    protected function mapSignature(): void
    {
        $this->fhirResource->setSignature($this->object_mapping->mapSignature());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRBundle';
    }
}
