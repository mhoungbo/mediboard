<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\CodeSystem;

use Ox\Interop\Fhir\Contracts\Mapping\R4\CodeSystemMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceCodeSystemInterface;
use Ox\Components\FhirCore\Interfaces\FHIRCodeSystemInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCanonical;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUnsignedInt;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCodeSystemConcept;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCodeSystemFilter;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCodeSystemProperty;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;

/**
 * FHIR patient resource
 */
class CFHIRResourceCodeSystem extends CFHIRDomainResource implements ResourceCodeSystemInterface
{
    /** @var string */
    public const RESOURCE_TYPE = 'CodeSystem';

    /** @var FHIRCodeSystemInterface $fhirResource */
    protected $fhirResource;

    /** @var CodeSystemMappingInterface */
    protected $object_mapping;

    /**
     * @param FHIRUri|null $url
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setUrl(?FHIRUri $url): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setUrl($url);

        return $this;
    }

    /**
     * @return FHIRUri|null
     */
    public function getUrl(): ?FHIRUri
    {
        return $this->fhirResource->getUrl();
    }

    /**
     * @param FHIRString|null $version
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setVersion(?FHIRString $version): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setVersion($version);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getVersion(): ?FHIRString
    {
        return $this->fhirResource->getVersion();
    }

    /**
     * @param FHIRString|null $name
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setName(?FHIRString $name): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setName($name);

        return $this;
    }

    /**
     * @param FHIRString|null $title
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setTitle(?FHIRString $title): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setTitle($title);

        return $this;
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @param FHIRBoolean|null $experimental
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setExperimental(?FHIRBoolean $experimental): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setExperimental($experimental);

        return $this;
    }

    /**
     * @param FHIRDateTime|null $date
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setDate(?FHIRDateTime $date): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setDate($date);

        return $this;
    }

    /**
     * @param FHIRString|null $publisher
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setPublisher(?FHIRString $publisher): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setPublisher($publisher);

        return $this;
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setContact(FHIRContactDetail ...$contact): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setContact(...$contact);

        return $this;
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return CFHIRResourceCodeSystem
     */
    public function addContact(FHIRContactDetail ...$contact): CFHIRResourceCodeSystem
    {
        $this->fhirResource->addContact(...$contact);

        return $this;
    }


    /**
     * @param FHIRMarkdown|null $description
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setDescription(?FHIRMarkdown $description): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setUseContext(FHIRUsageContext ...$useContext): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setUseContext(...$useContext);

        return $this;
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return CFHIRResourceCodeSystem
     */
    public function addUseContext(FHIRUsageContext ...$useContext): CFHIRResourceCodeSystem
    {
        $this->fhirResource->addUseContext(...$useContext);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setJurisdiction(FHIRCodeableConcept ...$jurisdiction): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return CFHIRResourceCodeSystem
     */
    public function addJurisdiction(FHIRCodeableConcept ...$jurisdiction): CFHIRResourceCodeSystem
    {
        $this->fhirResource->addJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @param FHIRMarkdown|null $purpose
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setPurpose(?FHIRMarkdown $purpose): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setPurpose($purpose);

        return $this;
    }

    /**
     * @param FHIRMarkdown|null $copyright
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setCopyright(?FHIRMarkdown $copyright): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setCopyright($copyright);

        return $this;
    }

    /**
     * @param FHIRBoolean|null $caseSensitive
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setCaseSensitive(?FHIRBoolean $caseSensitive): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setCaseSensitive($caseSensitive);

        return $this;
    }

    /**
     * @param FHIRCanonical|null $valueSet
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setValueSet(?FHIRCanonical $valueSet): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setValueSet($valueSet);

        return $this;
    }

    /**
     * @param FHIRCode|null $hierarchyMeaning
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setHierarchyMeaning(?FHIRCode $hierarchyMeaning): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setHierarchyMeaning($hierarchyMeaning);

        return $this;
    }

    /**
     * @param FHIRBoolean|null $compositional
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setCompositional(?FHIRBoolean $compositional): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setCompositional($compositional);

        return $this;
    }

    /**
     * @param FHIRBoolean|null $versionNeeded
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setVersionNeeded(?FHIRBoolean $versionNeeded): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setVersionNeeded($versionNeeded);

        return $this;
    }

    /**
     * @param FHIRCode|null $content
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setContent(?FHIRCode $content): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setContent($content);

        return $this;
    }

    /**
     * @param FHIRCanonical|null $supplements
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setSupplements(?FHIRCanonical $supplements): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setSupplements($supplements);

        return $this;
    }

    /**
     * @param FHIRUnsignedInt|null $count
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setCount(?FHIRUnsignedInt $count): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setCount($count);

        return $this;
    }

    /**
     * @param FHIRCodeSystemFilter ...$filter
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setFilter(FHIRCodeSystemFilter ...$filter): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setFilter(...$filter);

        return $this;
    }

    /**
     * @param FHIRCodeSystemFilter ...$filter
     *
     * @return CFHIRResourceCodeSystem
     */
    public function addFilter(FHIRCodeSystemFilter ...$filter): CFHIRResourceCodeSystem
    {
        $this->fhirResource->addFilter(...$filter);

        return $this;
    }

    /**
     * @param FHIRCodeSystemProperty ...$property
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setProperty(FHIRCodeSystemProperty ...$property): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setProperty(...$property);

        return $this;
    }

    /**
     * @param FHIRCodeSystemProperty ...$property
     *
     * @return CFHIRResourceCodeSystem
     */
    public function addProperty(FHIRCodeSystemProperty ...$property): CFHIRResourceCodeSystem
    {
        $this->fhirResource->addProperty(...$property);

        return $this;
    }

    /**
     * @param FHIRCodeSystemConcept ...$concept
     *
     * @return CFHIRResourceCodeSystem
     */
    public function setConcept(FHIRCodeSystemConcept ...$concept): CFHIRResourceCodeSystem
    {
        $this->fhirResource->setConcept(...$concept);

        return $this;
    }

    /**
     * @param FHIRCodeSystemConcept ...$concept
     *
     * @return CFHIRResourceCodeSystem
     */
    public function addConcept(FHIRCodeSystemConcept ...$concept): CFHIRResourceCodeSystem
    {
        $this->fhirResource->addConcept(...$concept);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getName(): ?FHIRString
    {
        return $this->fhirResource->getName();
    }

    /**
     * @return FHIRString|null
     */
    public function getTitle(): ?FHIRString
    {
        return $this->fhirResource->getTitle();
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getExperimental(): ?FHIRBoolean
    {
        return $this->fhirResource->getExperimental();
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getDate(): ?FHIRDateTime
    {
        return $this->fhirResource->getDate();
    }

    /**
     * @return FHIRString|null
     */
    public function getPublisher(): ?FHIRString
    {
        return $this->fhirResource->getPublisher();
    }

    /**
     * @return array
     */
    public function getContact(): array
    {
        return $this->fhirResource->getContact();
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getDescription(): ?FHIRMarkdown
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @return array
     */
    public function getUseContext(): array
    {
        return $this->fhirResource->getUseContext();
    }

    /**
     * @return array
     */
    public function getJurisdiction(): array
    {
        return $this->fhirResource->getJurisdiction();
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->fhirResource->getPurpose();
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->fhirResource->getCopyright();
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getCaseSensitive(): ?FHIRBoolean
    {
        return $this->fhirResource->getCaseSensitive();
    }

    /**
     * @return FHIRCanonical|null
     */
    public function getValueSet(): ?FHIRCanonical
    {
        return $this->fhirResource->getValueSet();
    }

    /**
     * @return FHIRCode|null
     */
    public function getHierarchyMeaning(): ?FHIRCode
    {
        return $this->fhirResource->getHierarchyMeaning();
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getCompositional(): ?FHIRBoolean
    {
        return $this->fhirResource->getCompositional();
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getVersionNeeded(): ?FHIRBoolean
    {
        return $this->fhirResource->getVersionNeeded();
    }

    /**
     * @return FHIRCode|null
     */
    public function getContent(): ?FHIRCode
    {
        return $this->fhirResource->getContent();
    }

    /**
     * @return FHIRCanonical|null
     */
    public function getSupplements(): ?FHIRCanonical
    {
        return $this->fhirResource->getSupplements();
    }

    /**
     * @return FHIRUnsignedInt|null
     */
    public function getCount(): ?FHIRUnsignedInt
    {
        return $this->fhirResource->getCount();
    }

    /**
     * @return FHIRCodeSystemFilter[]
     */
    public function getFilter(): array
    {
        return $this->fhirResource->getFilter();
    }

    /**
     * @return array
     */
    public function getProperty(): array
    {
        return $this->fhirResource->getProperty();
    }

    /**
     * @return array
     */
    public function getConcept(): array
    {
        return $this->fhirResource->getConcept();
    }

    /**
     * * Map property url
     */
    protected function mapUrl(): void
    {
        $this->fhirResource->setUrl($this->object_mapping->mapUrl());
    }

    /**
     * * Map property identifier
     */
    protected function mapIdentifier(): void
    {
        $this->fhirResource->setIdentifier(...$this->object_mapping->mapIdentifier());
    }

    /**
     * * Map property version
     */
    protected function mapVersion(): void
    {
        $this->fhirResource->setVersion($this->object_mapping->mapVersion());
    }

    /**
     * * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName($this->object_mapping->mapName());
    }

    /**
     * * Map property title
     */
    protected function mapTitle(): void
    {
        $this->fhirResource->setTitle($this->object_mapping->mapTitle());
    }

    /**
     * * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * * Map property experimental
     */
    protected function mapExperimental(): void
    {
        $this->fhirResource->setExperimental($this->object_mapping->mapExperimental());
    }

    /**
     * * Map property date
     */
    protected function mapDate(): void
    {
        $this->fhirResource->setDate($this->object_mapping->mapDate());
    }

    /**
     * * Map property publisher
     */
    protected function mapPublisher(): void
    {
        $this->fhirResource->setPublisher($this->object_mapping->mapPublisher());
    }

    /**
     * * Map property contact
     */
    protected function mapContact(): void
    {
        $this->fhirResource->setContact(...$this->object_mapping->mapContact());
    }

    /**
     * * Map property description
     */
    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    /**
     * * Map property useContext
     */
    protected function mapUseContext(): void
    {
        $this->fhirResource->setUseContext(...$this->object_mapping->mapUseContext());
    }

    /**
     * * Map property jurisdiction
     */
    protected function mapJurisdiction(): void
    {
        $this->fhirResource->setJurisdiction(...$this->object_mapping->mapJurisdiction());
    }

    /**
     * * Map property purpose
     */
    protected function mapPurpose(): void
    {
        $this->fhirResource->setPurpose($this->object_mapping->mapPurpose());
    }

    /**
     * * Map property copyright
     */
    protected function mapCopyright(): void
    {
        $this->fhirResource->setCopyright($this->object_mapping->mapCopyright());
    }

    /**
     * * Map property caseSensitive
     */
    protected function mapCaseSensitive(): void
    {
        $this->fhirResource->setCaseSensitive($this->object_mapping->mapCaseSensitive());
    }

    /**
     * * Map property valueSet
     */
    protected function mapValueSet(): void
    {
        $this->fhirResource->setValueSet($this->object_mapping->mapValueSet());
    }

    /**
     * * Map property hierarchyMeaning
     */
    protected function mapHierarchyMeaning(): void
    {
        $this->fhirResource->setHierarchyMeaning($this->object_mapping->mapHierarchyMeaning());
    }

    /**
     * * Map property compositional
     */
    protected function mapCompositional(): void
    {
        $this->fhirResource->setCompositional($this->object_mapping->mapCompositional());
    }

    /**
     * * Map property versionNeeded
     */
    protected function mapVersionNeeded(): void
    {
        $this->fhirResource->setVersionNeeded($this->object_mapping->mapVersionNeeded());
    }

    /**
     * * Map property content
     */
    protected function mapContent(): void
    {
        $this->fhirResource->setContent($this->object_mapping->mapContent());
    }

    /**
     * * Map property supplements
     */
    protected function mapSupplements(): void
    {
        $this->fhirResource->setSupplements($this->object_mapping->mapSupplements());
    }

    /**
     * * Map property count
     */
    protected function mapCount(): void
    {
        $this->fhirResource->setCount($this->object_mapping->mapCount());
    }

    /**
     * * Map property filter
     */
    protected function mapFilter(): void
    {
        $this->fhirResource->setFilter(...$this->object_mapping->mapFilter());
    }

    /**
     * * Map property property
     */
    protected function mapProperty(): void
    {
        $this->fhirResource->setProperty(...$this->object_mapping->mapProperty());
    }

    /**
     * * Map property concept
     */
    protected function mapConcept(): void
    {
        $this->fhirResource->setConcept(...$this->object_mapping->mapConcept());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRCodeSystem';
    }
}
