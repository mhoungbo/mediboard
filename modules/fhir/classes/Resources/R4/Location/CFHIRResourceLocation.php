<?php

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Location;

use Ox\Interop\Fhir\Contracts\Mapping\R4\LocationMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceLocationInterface;
use Ox\Components\FhirCore\Interfaces\FHIRLocationInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRLocationHoursOfOperation;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRLocationPosition;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * Description
 */
class CFHIRResourceLocation extends CFHIRDomainResource implements ResourceLocationInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Location';

    // attributes
    /** @var FHIRLocationInterface $fhirResource */
    protected $fhirResource;
    
    /** @var LocationMappingInterface */
    protected $object_mapping;


    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            );
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceLocation
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceLocation
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRCoding|null $operationalStatus
     *
     * @return CFHIRResourceLocation
     */
    public function setOperationalStatus(?FHIRCoding $operationalStatus): CFHIRResourceLocation
    {
        $this->fhirResource->setOperationalStatus($operationalStatus);

        return $this;
    }

    /**
     * @return FHIRCoding|null
     */
    public function getOperationalStatus(): ?FHIRCoding
    {
        return $this->fhirResource->getOperationalStatus();
    }

    /**
     * @param FHIRString|null $name
     *
     * @return CFHIRResourceLocation
     */
    public function setName(?FHIRString $name): CFHIRResourceLocation
    {
        $this->fhirResource->setName($name);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getName(): ?FHIRString
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRString ...$alias
     *
     * @return CFHIRResourceLocation
     */
    public function setAlias(FHIRString ...$alias): CFHIRResourceLocation
    {
        $this->fhirResource->setAlias(...$alias);

        return $this;
    }

    /**
     * @param FHIRString ...$alias
     *
     * @return CFHIRResourceLocation
     */
    public function addAlias(FHIRString ...$alias): CFHIRResourceLocation
    {
        $this->fhirResource->addAlias(...$alias);

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getAlias(): array
    {
        return $this->fhirResource->getAlias();
    }

    /**
     * @param FHIRString|null $description
     *
     * @return CFHIRResourceLocation
     */
    public function setDescription(?FHIRString $description): CFHIRResourceLocation
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getDescription(): ?FHIRString
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @param FHIRCode|null $mode
     *
     * @return CFHIRResourceLocation
     */
    public function setMode(?FHIRCode $mode): CFHIRResourceLocation
    {
        $this->fhirResource->setMode($mode);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getMode(): ?FHIRCode
    {
        return $this->fhirResource->getMode();
    }

    /**
     * @param FHIRCodeableConcept ...$type
     *
     * @return CFHIRResourceLocation
     */
    public function setType(FHIRCodeableConcept ...$type): CFHIRResourceLocation
    {
        $this->fhirResource->setType(...$type);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$type
     *
     * @return CFHIRResourceLocation
     */
    public function addType(FHIRCodeableConcept ...$type): CFHIRResourceLocation
    {
        $this->fhirResource->addType(...$type);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->fhirResource->getType();
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return self
     */
    public function setTelecom(FHIRContactPoint ...$telecom): self
    {
        $this->fhirResource->setTelecom(...$telecom);

        return $this;
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return self
     */
    public function addTelecom(FHIRContactPoint ...$telecom): self
    {
        $this->fhirResource->addTelecom(...$telecom);

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->fhirResource->getTelecom();
    }

    /**
     * @param FHIRAddress|null $address
     *
     * @return CFHIRResourceLocation
     */
    public function setAddress(?FHIRAddress $address): CFHIRResourceLocation
    {
        $this->fhirResource->setAddress($address);

        return $this;
    }

    /**
     * @return FHIRAddress|null
     */
    public function getAddress(): ?FHIRAddress
    {
        return $this->fhirResource->getAddress();
    }

    /**
     * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * Map property operationalStatus
     */
    protected function mapOperationalStatus(): void
    {
        $this->fhirResource->setOperationalStatus($this->object_mapping->mapOperationalStatus());
    }

    /**
     * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName($this->object_mapping->mapName());
    }

    /**
     * Map property alias
     */
    protected function mapAlias(): void
    {
        $this->fhirResource->setAlias(...$this->object_mapping->mapAlias());
    }

    /**
     * Map property description
     */
    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    /**
     * Map property mode
     */
    protected function mapMode(): void
    {
        $this->fhirResource->setMode($this->object_mapping->mapMode());
    }

    /**
     * Map property type
     */
    protected function mapType(): void
    {
        $this->fhirResource->setType(...$this->object_mapping->mapType());
    }

    /**
     * Map property telecom
     */
    protected function mapTelecom(): void
    {
        $this->fhirResource->setTelecom(...$this->object_mapping->mapTelecom());
    }

    /**
     * Map property address
     */
    protected function mapAddress(): void
    {
        $this->fhirResource->setAddress($this->object_mapping->mapAddress());
    }

    /**
     * Map property physicalType
     */
    protected function mapPhysicalType(): void
    {
        $this->fhirResource->setPhysicalType($this->object_mapping->mapPhysicalType());
    }

    /**
     * Map property position
     */
    protected function mapPosition(): void
    {
        $this->fhirResource->setPosition($this->object_mapping->mapPosition());
    }

    /**
     * Map property managingOrganization
     */
    protected function mapManagingOrganization(): void
    {
        $this->fhirResource->setManagingOrganization($this->object_mapping->mapManagingOrganization());
    }

    /**
     * Map property partOf
     */
    protected function mapPartOf(): void
    {
        $this->fhirResource->setPartOf($this->object_mapping->mapPartOf());
    }

    /**
     * Map property hoursOfOperation
     */
    protected function mapHoursOfOperation(): void
    {
        $this->fhirResource->setHoursOfOperation(...$this->object_mapping->mapHoursOfOperation());
    }

    /**
     * Map property availabilityExceptions
     */
    protected function mapAvailabilityExceptions(): void
    {
        $this->fhirResource->setAvailabilityExceptions($this->object_mapping->mapAvailabilityExceptions());
    }

    /**
     * Map property endpoint
     */
    protected function mapEndpoint(): void
    {
        $this->fhirResource->setEndpoint(...$this->object_mapping->mapEndpoint());
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getPhysicalType(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getPhysicalType();
    }

    /**
     * @param FHIRCodeableConcept|null $physicalType
     *
     * @return CFHIRResourceLocation
     */
    public function setPhysicalType(?FHIRCodeableConcept $physicalType): CFHIRResourceLocation
    {
        $this->fhirResource->setPhysicalType($physicalType);

        return $this;
    }

    /**
     * @return FHIRLocationPosition|null
     */
    public function getPosition(): ?FHIRLocationPosition
    {
        return $this->fhirResource->getPosition();
    }

    /**
     * @param FHIRLocationPosition|null $position
     *
     * @return CFHIRResourceLocation
     */
    public function setPosition(?FHIRLocationPosition $position): CFHIRResourceLocation
    {
        $this->fhirResource->setPosition($position);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getManagingOrganization(): ?FHIRReference
    {
        return $this->fhirResource->getManagingOrganization();
    }

    /**
     * @param FHIRReference|null $managingOrganization
     *
     * @return CFHIRResourceLocation
     */
    public function setManagingOrganization(?FHIRReference $managingOrganization): CFHIRResourceLocation
    {
        $this->fhirResource->setManagingOrganization($managingOrganization);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getPartOf(): ?FHIRReference
    {
        return $this->fhirResource->getPartOf();
    }

    /**
     * @param FHIRReference|null $partOf
     *
     * @return CFHIRResourceLocation
     */
    public function setPartOf(?FHIRReference $partOf): CFHIRResourceLocation
    {
        $this->fhirResource->setPartOf($partOf);

        return $this;
    }

    /**
     * @return FHIRLocationHoursOfOperation[]
     */
    public function getHoursOfOperation(): array
    {
        return $this->fhirResource->getHoursOfOperation();
    }

    /**
     * @param FHIRLocationHoursOfOperation ...$hoursOfOperation
     *
     * @return CFHIRResourceLocation
     */
    public function setHoursOfOperation(
        FHIRLocationHoursOfOperation ...$hoursOfOperation
    ): CFHIRResourceLocation {
        $this->fhirResource->setHoursOfOperation(...$hoursOfOperation);

        return $this;
    }

    /**
     * @param FHIRLocationHoursOfOperation ...$hoursOfOperation
     *
     * @return CFHIRResourceLocation
     */
    public function addHoursOfOperation(
        FHIRLocationHoursOfOperation ...$hoursOfOperation
    ): CFHIRResourceLocation {
        $this->fhirResource->addHoursOfOperation(...$hoursOfOperation);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getAvailabilityExceptions(): ?FHIRString
    {
        return $this->fhirResource->getAvailabilityExceptions();
    }

    /**
     * @param FHIRString|null $availabilityExceptions
     *
     * @return CFHIRResourceLocation
     */
    public function setAvailabilityExceptions(?FHIRString $availabilityExceptions): CFHIRResourceLocation
    {
        $this->fhirResource->setAvailabilityExceptions($availabilityExceptions);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->fhirResource->getEndpoint();
    }

    /**
     * @param FHIRReference ...$endpoint
     *
     * @return CFHIRResourceLocation
     */
    public function setEndpoint(FHIRReference ...$endpoint): CFHIRResourceLocation
    {
        $this->fhirResource->setEndpoint(...$endpoint);

        return $this;
    }

    /**
     * @param FHIRReference ...$endpoint
     *
     * @return CFHIRResourceLocation
     */
    public function addEndpoint(FHIRReference ...$endpoint): CFHIRResourceLocation
    {
        $this->fhirResource->addEndpoint(...$endpoint);

        return $this;
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRLocation';
    }
}
