<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Location\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\LocationMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRLocationHoursOfOperation;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRLocationPosition;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Location\CFHIRResourceLocation;
use Ox\Mediboard\Patients\CExercicePlace;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

/**
 * Description
 */
class Location implements DelegatedObjectMapperInterface, LocationMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CExercicePlace */
    protected $object;

    /** @var CFHIRResourceLocation */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CExercicePlace && $object->_id;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceLocation::class];
    }

    /**
     * @inheritDoc
     */
    public function mapStatus(): ?FHIRCode
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapOperationalStatus(): ?FHIRCoding
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapName(): ?FHIRString
    {
        return (new FHIRString())->setValue($this->object->raison_sociale);
    }

    /**
     * @inheritDoc
     */
    public function mapAlias(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapDescription(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapMode(): ?FHIRCode
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapType(): array
    {
        return [];
    }

    public function mapTelecom(): array
    {
        $telecoms = [];
        if ($this->object->tel) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel);
        }

        if ($this->object->tel2) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel2);
        }

        if ($this->object->fax) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->fax);
        }

        if ($this->object->email) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('email')
                ->setValue($this->object->email);
        }

        return $telecoms;
    }

    /**
     * @return FHIRAddress|null
     */
    public function mapAddress(): ?FHIRAddress
    {
        return (new FHIRAddress())
            ->setType('both')
            ->setLine($this->object->adresse)
            ->setCity($this->object->commune)
            ->setPostalCode($this->object->cp)
            ->setCountry($this->object->pays);
    }

    /**
     * @inheritDoc
     */
    public function mapPhysicalType(): ?FHIRCodeableConcept
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapPosition(): ?FHIRLocationPosition
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapManagingOrganization(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapPartOf(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapHoursOfOperation(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapAvailabilityExceptions(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapEndpoint(): array
    {
        return [];
    }
}
