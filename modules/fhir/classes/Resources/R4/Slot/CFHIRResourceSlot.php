<?php

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Slot;

use Exception;
use Ox\Interop\Fhir\Contracts\Mapping\R4\SlotMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceSlotInterface;
use Ox\Components\FhirCore\Interfaces\FHIRSlotInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionHistory;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * Class CFHIRResourceSlot
 * @package Ox\Interop\Fhir\Resources
 */
class CFHIRResourceSlot extends CFHIRDomainResource implements ResourceSlotInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = "Slot";

    // attributes
    /** @var FHIRSlotInterface $fhirResource */
    protected $fhirResource;

    /** @var SlotMappingInterface */
    public $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->setInteractions(
                [
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionHistory::NAME,
                ]
            );
    }

    /**
     * @param FHIRCodeableConcept|null $appointmentType
     *
     * @return CFHIRResourceSlot
     */
    public function setAppointmentType(?FHIRCodeableConcept $appointmentType): CFHIRResourceSlot
    {
        $this->fhirResource->setAppointmentType($appointmentType);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getAppointmentType(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getAppointmentType();
    }

    /**
     * @param FHIRReference|null $schedule
     *
     * @return CFHIRResourceSlot
     */
    public function setSchedule(?FHIRReference $schedule): CFHIRResourceSlot
    {
        $this->fhirResource->setSchedule($schedule);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getSchedule(): ?FHIRReference
    {
        return $this->fhirResource->getSchedule();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceSlot
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceSlot
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRInstant|null $start
     *
     * @return CFHIRResourceSlot
     */
    public function setStart(?FHIRInstant $start): CFHIRResourceSlot
    {
        $this->fhirResource->setStart($start);

        return $this;
    }

    /**
     * @return FHIRInstant|null
     */
    public function getStart(): ?FHIRInstant
    {
        return $this->fhirResource->getStart();
    }

    /**
     * @param FHIRInstant|null $end
     *
     * @return CFHIRResourceSlot
     */
    public function setEnd(?FHIRInstant $end): CFHIRResourceSlot
    {
        $this->fhirResource->setEnd($end);

        return $this;
    }

    /**
     * @return FHIRInstant|null
     */
    public function getEnd(): ?FHIRInstant
    {
        return $this->fhirResource->getEnd();
    }

    /**
     * @param FHIRBoolean|null $overbooked
     *
     * @return CFHIRResourceSlot
     */
    public function setOverbooked(?FHIRBoolean $overbooked): CFHIRResourceSlot
    {
        $this->fhirResource->setOverbooked($overbooked);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getOverbooked(): ?FHIRBoolean
    {
        return $this->fhirResource->getOverbooked();
    }

    /**
     * @param FHIRString|null $comment
     *
     * @return CFHIRResourceSlot
     */
    public function setComment(?FHIRString $comment): CFHIRResourceSlot
    {
        $this->fhirResource->setComment($comment);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getComment(): ?FHIRString
    {
        return $this->fhirResource->getComment();
    }

    /**
     * Map property serviceCategory
     */
    protected function mapServiceCategory(): void
    {
        $this->fhirResource->setServiceCategory(...$this->object_mapping->mapServiceCategory());
    }

    /**
     * Map property serviceType
     * @throws Exception
     */
    protected function mapServiceType(): void
    {
        $this->fhirResource->setServiceType(...$this->object_mapping->mapServiceType());
    }

    /**
     * Map property specialty
     */
    protected function mapSpecialty(): void
    {
        $this->fhirResource->setSpecialty(...$this->object_mapping->mapSpecialty());
    }

    /**
     * Map property appointmentType
     */
    protected function mapAppointmentType(): void
    {
        $this->fhirResource->setAppointmentType($this->object_mapping->mapAppointmentType());
    }

    /**
     * Map property schedule
     */
    protected function mapSchedule(): void
    {
        $this->fhirResource->setSchedule($this->object_mapping->mapSchedule());
    }

    /**
     * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * Map property start
     */
    protected function mapStart(): void
    {
        $this->fhirResource->setStart($this->object_mapping->mapStart());
    }

    /**
     * Map property end
     */
    protected function mapEnd(): void
    {
        $this->fhirResource->setEnd($this->object_mapping->mapEnd());
    }

    /**
     * Map property overbooked
     */
    protected function mapOverbooked(): void
    {
        $this->fhirResource->setOverbooked($this->object_mapping->mapOverbooked());
    }

    /**
     * Map property comment
     */
    protected function mapComment(): void
    {
        $this->fhirResource->setComment($this->object_mapping->mapComment());
    }

    /**
     * @param FHIRCodeableConcept ...$specialty
     *
     * @return self
     */
    public function setSpecialty(FHIRCodeableConcept ...$specialty): self
    {
        $this->fhirResource->setSpecialty(...$specialty);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$specialty
     *
     * @return self
     */
    public function addSpecialty(FHIRCodeableConcept ...$specialty): self
    {
        $this->fhirResource->addSpecialty(...$specialty);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getSpecialty(): array
    {
        return $this->fhirResource->getSpecialty();
    }

    /**
     * @param FHIRCodeableConcept ...$serviceType
     *
     * @return self
     */
    public function setServiceType(FHIRCodeableConcept ...$serviceType): self
    {
        $this->fhirResource->setServiceType(...$serviceType);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$serviceType
     *
     * @return self
     */
    public function addServiceType(FHIRCodeableConcept ...$serviceType): self
    {
        $this->fhirResource->addServiceType(...$serviceType);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceType(): array
    {
        return $this->fhirResource->getServiceType();
    }

    /**
     * @param FHIRCodeableConcept ...$serviceCategory
     *
     * @return self
     */
    public function setServiceCategory(FHIRCodeableConcept ...$serviceCategory): self
    {
        $this->fhirResource->setServiceCategory(...$serviceCategory);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$serviceCategory
     *
     * @return self
     */
    public function addServiceCategory(FHIRCodeableConcept ...$serviceCategory): self
    {
        $this->fhirResource->addServiceCategory(...$serviceCategory);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getServiceCategory(): array
    {
        return $this->fhirResource->getServiceCategory();
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRSlot';
    }
}
