<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Slot\Mapper;

use Exception;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\SlotMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Schedule\CFHIRResourceSchedule;
use Ox\Interop\Fhir\Resources\R4\Slot\CFHIRResourceSlot;
use Ox\Mediboard\Cabinet\CSlot;

/**
 * Description
 */
class Slot implements DelegatedObjectMapperInterface, SlotMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CSlot */
    protected $object;

    /** @var CFHIRResourceSlot */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceSlot::class];
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CSlot && $object->_id;
    }

    public function mapServiceCategory(): array
    {
        $system      = 'http://terminology.hl7.org/CodeSystem/service-category';
        $code        = '35';
        $displayName = 'Hospital';
        $text        = 'Hospital';

        return [
            (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($displayName)
            )
        ];
    }

    public function mapServiceType(): array
    {
        // not implemented
        return [];
    }

    /**
     * @throws Exception
     */
    public function mapSpecialty(): array
    {
        return [];
    }

    public function mapAppointmentType(): ?FHIRCodeableConcept
    {
        // not implemented
        return null;
    }

    public function mapSchedule(): ?FHIRReference
    {
        return $this->resource->addReference(CFHIRResourceSchedule::class, $this->object);
    }

    public function mapStatus(): ?FHIRCode
    {
        return (new FHIRCode())->setValue($this->object->status);
    }

    /**
     * @throws Exception
     */
    public function mapStart(): ?FHIRInstant
    {
        return (new FHIRInstant())->setValue(CFHIR::getTimeUtc($this->object->start, false));
    }

    /**
     * @throws Exception
     */
    public function mapEnd(): ?FHIRInstant
    {
        return (new FHIRInstant())->setValue(CFHIR::getTimeUtc($this->object->end, false));
    }

    /**
     * @throws Exception
     */
    public function mapOverbooked(): ?FHIRBoolean
    {
        return (new FHIRBoolean())->setValue($this->object->overbooked);
    }

    public function mapComment(): ?FHIRString
    {
        // not implemented
        return null;
    }
}
