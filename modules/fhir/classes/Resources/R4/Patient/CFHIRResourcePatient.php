<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Patient;

use Exception;
use Ox\Core\CMbArray;
use Ox\Core\CMbObject;
use Ox\Core\CStoredObject;
use Ox\Interop\Eai\CDomain;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\PatientMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourcePatientInterface;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Exception\CFHIRExceptionBadRequest;
use Ox\Interop\Fhir\Exception\CFHIRExceptionEmptyResultSet;
use Ox\Components\FhirCore\Interfaces\FHIRPatientInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientCommunication;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientContact;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientLink;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionHistory;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Operations\CFHIROperationIhePix;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Resources\R4\Patient\Mapper\Patient;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterDate;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterString;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterToken;
use Ox\Mediboard\Sante400\CIdSante400;

/**
 * FHIR patient resource
 */
class CFHIRResourcePatient extends CFHIRDomainResource implements ResourcePatientInterface
{
    /** @var string */
    public const VERSION_NORMATIVE = '4.0';

    /** @var string */
    public const RESOURCE_TYPE = "Patient";

    // attributes
    /** @var FHIRPatientInterface $fhirResource */
    protected $fhirResource;

    /** @var PatientMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                    CFHIRInteractionHistory::NAME,
                    CFHIROperationIhePix::NAME,
                ]
            )
            ->addSearchAttributes(
                [
                    new SearchParameterToken('identifier'),
                    new SearchParameterString('family'),
                    new SearchParameterString('given'),
                    new SearchParameterDate('birthdate'),
                    new SearchParameterString('email'),
                    new SearchParameterString('address'),
                    new SearchParameterString('address-city'),
                    new SearchParameterString('address-postalcode'),
                    new SearchParameterToken('gender'),
                ]
            );
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return CFHIRResourcePatient
     */
    public function setTelecom(FHIRContactPoint ...$telecom): CFHIRResourcePatient
    {
        $this->fhirResource->setTelecom(...$telecom);

        return $this;
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return CFHIRResourcePatient
     */
    public function addTelecom(FHIRContactPoint ...$telecom): CFHIRResourcePatient
    {
        $this->fhirResource->addTelecom(...$telecom);

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->fhirResource->getTelecom();
    }

    /**
     * @param FHIRCode|null $gender
     *
     * @return CFHIRResourcePatient
     */
    public function setGender(?FHIRCode $gender): CFHIRResourcePatient
    {
        $this->fhirResource->setGender($gender);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getGender(): ?FHIRCode
    {
        return $this->fhirResource->getGender();
    }

    /**
     * @param FHIRDate|null $birthDate
     *
     * @return CFHIRResourcePatient
     */
    public function setBirthDate(?FHIRDate $birthDate): CFHIRResourcePatient
    {
        $this->fhirResource->setBirthDate($birthDate);

        return $this;
    }

    /**
     * @return FHIRDate|null
     */
    public function getBirthDate(): ?FHIRDate
    {
        return $this->fhirResource->getBirthDate();
    }

    /**
     * @param FHIRElement|null $deceased
     *
     * @return CFHIRResourcePatient
     */
    public function setDeceased(?FHIRElement $deceased): CFHIRResourcePatient
    {
        $this->fhirResource->setDeceased($deceased);

        return $this;
    }

    /**
     * @return FHIRElement|null
     */
    public function getDeceased(): ?FHIRElement
    {
        return $this->fhirResource->getDeceased();
    }

    /**
     * @param FHIRAddress ...$address
     *
     * @return CFHIRResourcePatient
     */
    public function setAddress(FHIRAddress ...$address): CFHIRResourcePatient
    {
        $this->fhirResource->setAddress(...$address);

        return $this;
    }

    /**
     * @param FHIRAddress ...$address
     *
     * @return CFHIRResourcePatient
     */
    public function addAddress(FHIRAddress ...$address): CFHIRResourcePatient
    {
        $this->fhirResource->addAddress(...$address);

        return $this;
    }

    /**
     * @return FHIRAddress[]
     */
    public function getAddress(): array
    {
        return $this->fhirResource->getAddress();
    }

    /**
     * @param FHIRCodeableConcept|null $maritalStatus
     *
     * @return CFHIRResourcePatient
     */
    public function setMaritalStatus(?FHIRCodeableConcept $maritalStatus): CFHIRResourcePatient
    {
        $this->fhirResource->setMaritalStatus($maritalStatus);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getMaritalStatus(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getMaritalStatus();
    }

    /**
     * @param FHIRElement|null $multipleBirth
     *
     * @return CFHIRResourcePatient
     */
    public function setMultipleBirth(?FHIRElement $multipleBirth): CFHIRResourcePatient
    {
        $this->fhirResource->setMultipleBirth($multipleBirth);

        return $this;
    }

    /**
     * @return FHIRElement|null
     */
    public function getMultipleBirth(): ?FHIRElement
    {
        return $this->fhirResource->getMultipleBirth();
    }

    /**
     * @param FHIRAttachment ...$photo
     *
     * @return CFHIRResourcePatient
     */
    public function setPhoto(FHIRAttachment ...$photo): CFHIRResourcePatient
    {
        $this->fhirResource->setPhoto(...$photo);

        return $this;
    }

    /**
     * @param FHIRAttachment ...$photo
     *
     * @return CFHIRResourcePatient
     */
    public function addPhoto(FHIRAttachment ...$photo): CFHIRResourcePatient
    {
        $this->fhirResource->addPhoto(...$photo);

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getPhoto(): array
    {
        return $this->fhirResource->getPhoto();
    }

    /**
     * @param FHIRPatientContact ...$contact
     *
     * @return CFHIRResourcePatient
     */
    public function setContact(FHIRPatientContact ...$contact): CFHIRResourcePatient
    {
        $this->fhirResource->setContact(...$contact);

        return $this;
    }

    /**
     * @param FHIRPatientContact ...$contact
     *
     * @return CFHIRResourcePatient
     */
    public function addContact(FHIRPatientContact ...$contact): CFHIRResourcePatient
    {
        $this->fhirResource->addContact(...$contact);

        return $this;
    }

    /**
     * @return array
     */
    public function getContact(): array
    {
        return $this->fhirResource->getContact();
    }

    /**
     * @param FHIRPatientCommunication ...$communication
     *
     * @return CFHIRResourcePatient
     */
    public function setCommunication(FHIRPatientCommunication ...$communication): CFHIRResourcePatient
    {
        $this->fhirResource->setCommunication(...$communication);

        return $this;
    }

    /**
     * @param FHIRPatientCommunication ...$communication
     *
     * @return CFHIRResourcePatient
     */
    public function addCommunication(FHIRPatientCommunication ...$communication): CFHIRResourcePatient
    {
        $this->fhirResource->addCommunication(...$communication);

        return $this;
    }

    /**
     * @return FHIRPatientCommunication[]
     */
    public function getCommunication(): array
    {
        return $this->fhirResource->getCommunication();
    }

    /**
     * @param FHIRReference ...$generalPractitioner
     *
     * @return CFHIRResourcePatient
     */
    public function setGeneralPractitioner(FHIRReference ...$generalPractitioner): CFHIRResourcePatient
    {
        $this->fhirResource->setGeneralPractitioner(...$generalPractitioner);

        return $this;
    }

    /**
     * @param FHIRReference ...$generalPractitioner
     *
     * @return CFHIRResourcePatient
     */
    public function addGeneralPractitioner(FHIRReference ...$generalPractitioner): CFHIRResourcePatient
    {
        $this->fhirResource->addGeneralPractitioner(...$generalPractitioner);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getGeneralPractitioner(): array
    {
        return $this->fhirResource->getGeneralPractitioner();
    }

    /**
     * @param FHIRReference|null $managingOrganization
     *
     * @return CFHIRResourcePatient
     */
    public function setManagingOrganization(?FHIRReference $managingOrganization): CFHIRResourcePatient
    {
        $this->fhirResource->setManagingOrganization($managingOrganization);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getManagingOrganization(): ?FHIRReference
    {
        return $this->fhirResource->getManagingOrganization();
    }

    /**
     * @param FHIRPatientLink ...$link
     *
     * @return CFHIRResourcePatient
     */
    public function setLink(FHIRPatientLink ...$link): CFHIRResourcePatient
    {
        $this->fhirResource->setLink(...$link);

        return $this;
    }

    /**
     * @param FHIRPatientLink ...$link
     *
     * @return CFHIRResourcePatient
     */
    public function addLink(FHIRPatientLink ...$link): CFHIRResourcePatient
    {
        $this->fhirResource->addLink(...$link);

        return $this;
    }

    /**
     * @return FHIRPatientLink[]
     */
    public function getLink(): array
    {
        return $this->fhirResource->getLink();
    }

    /**
     * @param array $name
     *
     * @return CFHIRResourcePatient
     */
    public function setName(FHIRHumanName ...$name): CFHIRResourcePatient
    {
        $this->fhirResource->setName(...$name);

        return $this;
    }

    /**
     * @param array $name
     *
     * @return CFHIRResourcePatient
     */
    public function addName(FHIRHumanName ...$name): CFHIRResourcePatient
    {
        $this->fhirResource->addName(...$name);

        return $this;
    }

    /**
     * @return FHIRHumanName[]
     */
    public function getName(): array
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRBoolean|null $active
     *
     * @return CFHIRResourcePatient
     */
    public function setActive(?FHIRBoolean $active): CFHIRResourcePatient
    {
        $this->fhirResource->setActive($active);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getActive(): ?FHIRBoolean
    {
        return $this->fhirResource->getActive();
    }

    /**
     * @param CStoredObject $object
     *
     * @return DelegatedObjectMapperInterface
     */
    protected function setMapperOld(CStoredObject $object): DelegatedObjectMapperInterface
    {
        $mapping_object = Patient::class;

        return new $mapping_object();
    }

    /**
     * Map property active
     */
    protected function mapActive(): void
    {
        $this->fhirResource->setActive($this->object_mapping->mapActive());
    }

    /**
     * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName(...$this->object_mapping->mapName());
    }

    /**
     * Map property telecom
     */
    protected function mapTelecom(): void
    {
        $this->fhirResource->setTelecom(...$this->object_mapping->mapTelecom());
    }

    /**
     * Map property gender
     */
    protected function mapGender(): void
    {
        $this->fhirResource->setGender($this->object_mapping->mapGender());
    }

    /**
     * Map property birthDate
     */
    protected function mapBirthDate(): void
    {
        $this->fhirResource->setBirthDate($this->object_mapping->mapBirthDate());
    }

    /**
     * Map property deceased
     */
    protected function mapDeceased(): void
    {
        $this->fhirResource->setDeceased($this->object_mapping->mapDeceased());
    }

    /**
     * Map property address
     */
    protected function mapAddress(): void
    {
        $this->fhirResource->addAddress(...$this->object_mapping->mapAddress());
    }

    /**
     * Map property maritalStatus
     */
    protected function mapMaritalStatus(): void
    {
        $this->fhirResource->setMaritalStatus($this->object_mapping->mapMaritalStatus());
    }

    /**
     * Map property multipleBirth
     */
    protected function mapMultipleBirth(): void
    {
        $this->fhirResource->setMultipleBirth($this->object_mapping->mapMultipleBirth());
    }

    /**
     * Map property photo
     */
    protected function mapPhoto(): void
    {
        $this->fhirResource->setPhoto(...$this->object_mapping->mapPhoto());
    }

    /**
     * Map property contact
     */
    protected function mapContact(): void
    {
        $this->fhirResource->setContact(...$this->object_mapping->mapContact());
    }

    /**
     * Map property communication
     */
    protected function mapCommunication(): void
    {
        $this->fhirResource->setCommunication(...$this->object_mapping->mapCommunication());
    }

    /**
     * Map property generalPractitioner
     *
     * @param string $resource_class
     */
    protected function mapGeneralPractitioner(): void
    {
        $this->fhirResource->setGeneralPractitioner(...$this->object_mapping->mapGeneralPractitioner(CFHIRResourcePractitioner::class));
    }

    /**
     * Map property managingOrganization
     */
    protected function mapManagingOrganization(): void
    {
        $this->fhirResource->setManagingOrganization($this->object_mapping->mapManagingOrganization());
    }

    /**
     * Map property link
     * @throws Exception
     */
    protected function mapLink(): void
    {
        $this->fhirResource->setLink(...$this->object_mapping->mapLink());
    }

    /**
     * Perform a search query based on the current object data
     *
     * @param array $data Data to handle
     *
     * @return CStoredObject
     * @throws CFHIRException|Exception
     */
    public function operation_ihe_pix($data)
    {
        if (!$sourceIdentifier = $this->getParameterSearch('sourceIdentifier')) {
            throw new CFHIRException("Invalid number of source identifiers");
        }

        if (strpos($sourceIdentifier, "urn:oid:") === 0) {
            [$system, $value] = explode("|", substr($sourceIdentifier, 8));

            $patient = $this->findPatientByIdentifier($system, $value);

            if ($targetSystem = $this->allParameters->get('targetSystem')) {
                $targetSystem = explode(',', $targetSystem);

                $identifiers = [];
                foreach ($targetSystem as $_system) {
                    if (strpos($_system, "urn:oid:") !== 0) {
                        continue;
                    }

                    $array  = explode("|", str_replace("urn:oid:", "", $_system));
                    $system = CMbArray::get($array, 0);

                    $identifiers[] = $system;
                }

                $patient->_returned_oids = $identifiers;
            }

            return $patient;
        }

        throw new CFHIRExceptionEmptyResultSet();
    }

    /**
     * // todo [Fhir - recherche]
     * Finds a patient by his identifier
     *
     * @param string $system Identifying system (OID)
     * @param string $value  Identifier value
     *
     * @return CMbObject
     * @throws CFHIRExceptionBadRequest
     * @throws CFHIRExceptionEmptyResultSet
     * @throws Exception
     */
    public function findPatientByIdentifier(string $system, string $value): CMbObject
    {
        $domain      = new CDomain();
        $domain->OID = $system;
        $domain->loadMatchingObject();

        if (!$domain->_id) {
            throw new CFHIRExceptionBadRequest("sourceIdentifier Assigning Authority not found");
        }

        $idex = CIdSante400::getMatch("CPatient", $domain->tag, $value);

        if (!$idex->_id) {
            throw new CFHIRExceptionEmptyResultSet("Unknown Patient identified by '$system|$value'");
        }

        return $idex->loadTargetObject();
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRPatient';
    }
}
