<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Patient\Mapper;

use Exception;
use Ox\Interop\Eai\CDomain;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientContact;
use Ox\Interop\Fhir\Profiles\CFHIRInteropSante;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\Organization\Profiles\InteropSante\CFHIRResourceOrganizationFR;
use Ox\Interop\Fhir\Resources\R4\Patient\Profiles\InteropSante\CFHIRResourcePatientFR;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Interop\Fhir\Resources\R4\RelatedPerson\Profiles\InteropSante\CFHIRResourceRelatedPersonFR;
use Ox\Interop\Fhir\Utilities\Helper\PatientHelper;
use Ox\Mediboard\Patients\CCorrespondantPatient;
use Ox\Mediboard\Patients\CIdentityProofType;
use Ox\Mediboard\Patients\CINSPatient;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CPatientINSNIR;
use Ox\Mediboard\Patients\CSourceIdentite;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

/**
 * Description
 */
class FrPatient extends Patient
{
    /** @var CPatient */
    protected $object;

    /** @var CFHIRResourcePatientFR */
    protected CFHIRResource $resource;

    public function onlyProfiles(): ?array
    {
        return [CFHIRInteropSante::class];
    }

    public function mapExtension(): array
    {
        $extensions = [];

        $extensions[] = (new FHIRExtension())
            ->setUrl('http://hl7.org/fhir/StructureDefinition/patient-nationality');

        $extensions[] = (new FHIRExtension())
            ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrPatientIdentReliability');

        $extensions[] = (new FHIRExtension())
            ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrPatientDeathPlace');

        $extensions[] = (new FHIRExtension())
            ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrPatientIdentityMethodCollection');

        $extensions[] = (new FHIRExtension())
            ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrPatientBirthdateUpdateIndicator');

        if ($this->object->cp_naissance) {
            $extensions[] = (new FHIRExtension())
                ->setUrl('http://hl7.org/fhir/StructureDefinition/patient-birthPlace')
                ->setValue(
                    (new FHIRAddress())
                        ->setDistrict($this->object->cp_naissance)
                );
        }

        // todo gestion des extension null

        $source_identite_active = $this->object->loadRefSourceIdentite();
        if ($source_identite_active && $source_identite_active->_id) {
            $sub_extensions = [];
            if ($code_identity = $this->getCodeSourceIdentity()) {
                $sub_extensions[] = (new FHIRExtension())
                    ->setUrl('identityReliability')
                    ->setValue($code_identity);

                $sub_extensions[] = (new FHIRExtension())
                    ->setUrl('validationDate')
                    ->setValue(
                        (new FHIRDate())
                            ->setValue($source_identite_active->debut)
                    );
            }

            if ($mode_identity = $this->getModeSourceIdentity($source_identite_active)) {
                $sub_extensions[] = (new FHIRExtension())
                    ->setUrl('validationMode')
                    ->setValue($mode_identity);
            }

            if ($sub_extensions) {
                $extensions[] = (new FHIRExtension())
                    ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrPatientIdentReliability')
                    ->setExtension(...$sub_extensions);
            }
        }

        return $extensions;
    }

    /**
     * @throws Exception
     */
    public function mapIdentifier(): array
    {
        $identifiers = parent::mapIdentifier();

        if ($patientINSNIR = $this->object->loadRefPatientINSNIR()) {
            if ($patientINSNIR->_is_ins_nir) {
                $type_ins = (new FHIRCodeableConcept())
                    ->setCoding(PatientHelper::getTypeCodingINS());

                $INS_NIR = (new FHIRIdentifier())
                    ->setValue($patientINSNIR->ins_nir)
                    ->setSystem(CPatientINSNIR::OID_INS_NIR)
                    ->setType($type_ins)
                    ->setUse('official');

                $identifiers[] = $INS_NIR;
            }

            if ($patientINSNIR->_is_ins_nia) {
                $typeSystem = 'http://interopsante.org/CodeSystem/fr-v2-0203';
                $coding     = (new FHIRCoding())
                    ->setCode('INS-NIA')
                    ->setSystem(CPatientINSNIR::OID_INS_NIA)
                    ->setDisplay('NIA');
                $type_nia   = (new FHIRCodeableConcept())->setCoding($coding);

                $identifiers[] = (new FHIRIdentifier())
                    ->setValue($patientINSNIR->is_nia)
                    ->setSystem($typeSystem)
                    ->setType($type_nia)
                    ->setUse('temp');
            }
        }

        $patientINS             = new CINSPatient();
        $patientINS->patient_id = $this->object->_id;
        $patientINS->type       = 'C';
        if ($patientINS->loadMatchingObject()) {
            $system = 'urn:oid:1.2.250.1.213.1.4.2';
            $coding = (new FHIRCoding())
                ->setSystem('http://interopsante.org/CodeSystem/fr-v2-0203')
                ->setCode('INS-C')
                ->setDisplay('INS calcul�');
            $type   = (new FHIRCodeableConcept())->setCoding($coding);

            $identifiers[] = (new FHIRIdentifier())
                ->setValue($patientINS->type)
                ->setSystem($system)
                ->setType($type)
                ->setUse('secondary');
        }

        if ($IPP = $this->object->loadIPP()) {
            $master_domain = CDomain::getMasterDomainPatient();
            if ($oid = $master_domain->OID) {
                $type_identifier = (new FHIRCodeableConcept())
                    ->setCoding(PatientHelper::getTypeCodingIPP());

                $identifiers[] = (new FHIRIdentifier())
                    ->setValue($IPP)
                    ->setSystem($oid)
                    ->setType($type_identifier)
                    ->setUse('usual');
            }
        }

        return $identifiers;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function mapName(): array
    {
        $assemblyOrder = [
            'url'   => 'http://terminology.hl7.org/CodeSystem/v2-0444',
            'value' => (new FHIRCode())->setValue('F'),
        ];

        $extension = (new FHIRExtension())
            ->setUrl('http://terminology.hl7.org/CodeSystem/v2-0444')
            ->setValue((new FHIRCoding())->setCode('F'));

        $names = [
            (new FHIRHumanName())
                ->setFamily($this->object->nom_jeune_fille)
                ->setGiven($this->object->prenom)
                ->setUse('official')
                ->setText($this->object->prenoms)
        ];

        if ($this->object->prenom_usuel) {
            $names[] = (new FHIRHumanName())
                ->setFamily($this->object->nom)
                ->setGiven($this->object->prenom_usuel)
                ->setUse('usual');
        }

        return $names;
    }

    public function mapTelecom(): array
    {
        return [];
    }

    /**
     */
    public function mapAddress(): array
    {
        return [];
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapContact(): array
    {
        $contacts = [];

        /** @var CCorrespondantPatient[] */
        $correspondantsPatient = $this->object->loadRefsCorrespondantsPatient();

        foreach ($correspondantsPatient as $_correspondant) {
            $relationship = $this->getContactRelationship($_correspondant);

            $name = $this->getContactName($_correspondant);

            $telecom = $this->getContactTelecom($_correspondant);

            $address = $this->getContactAddress($_correspondant);

            $gender = (new FHIRCode())->setValue($this->resource->formatGender($_correspondant->sex));

            $organization = null;

            $period = null;

            $extension = $this->getContactExtension($_correspondant);

            $contact = (new FHIRPatientContact())
                ->setRelationship(...$relationship)
                ->setName($name)
                ->setTelecom(...$telecom)
                ->setAddress($address)
                ->setGender($gender)
                ->setOrganization($organization)
                ->setPeriod($period)
                ->setExtension(...$extension);

            $contacts[] = $contact;
        }

        return $contacts;
    }

    public function mapGeneralPractitioner(string $resource_class = CFHIRResourcePractitionerFR::class): array
    {
        return parent::mapGeneralPractitioner($resource_class);
    }

    public function mapManagingOrganization(string $resource_class = CFHIRResourceOrganizationFR::class
    ): ?FHIRReference {
        return parent::mapManagingOrganization($resource_class);
    }

    /**
     * @return FHIRExtension[]
     * @throws ReflectionException
     * @throws Exception
     */
    protected function getContactExtension(CCorrespondantPatient $correspondant_patient): array
    {
        return [
            (new FHIRExtension())
                ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrPatientContactIdentifier')
                ->setValue(
                    (new FHIRIdentifier())
                        ->setValue($correspondant_patient->getUuid())
                )
        ];
    }

    /**
     * @param CCorrespondantPatient $_correspondant_patient
     *
     * @return FHIRCodeableConcept[]
     */
    protected function getContactRelationship(CCorrespondantPatient $_correspondant_patient): array
    {
        $rolePersonSystem  = 'urn:oid:2.16.840.1.113883.5.110';
        $rolePersonCode    = 'ECON';
        $rolePersonDisplay = 'Entit  contacter en cas d\'urgence';

        $relatedPersonSystem = 'urn:oid:2.16.840.1.113883.5.111';

        $relationships = [];

        if ($related_person = CFHIRResourceRelatedPersonFR::getRelatedPerson($_correspondant_patient)) {
            return [$related_person];
        }

        switch ($_correspondant_patient->parente) {
            case 'mere':
                $relatedPersonCode    = 'MTH';
                $relatedPersonDisplay = 'Mre';
                break;

            case 'pere':
                $relatedPersonCode    = 'FTH';
                $relatedPersonDisplay = 'Pre';
                break;

            case 'grand_parent':
                $relatedPersonCode    = 'GRPRN';
                $relatedPersonDisplay = 'Grand-parent';
                break;

            case 'frere':
                $relatedPersonCode    = 'BRO';
                $relatedPersonDisplay = 'Frre';
                break;

            case 'soeur':
                $relatedPersonCode    = 'SIS';
                $relatedPersonDisplay = 'Soeur';
                break;

            case 'petits_enfants':
                $relatedPersonCode    = 'GRNDCHILD';
                $relatedPersonDisplay = 'Petit-enfant';
                break;

            //TODO Vers quel code mapper ? HUSB Epoux, WIFE Epouse, SPS Epoux ou pouse
            case 'epoux':
                $relatedPersonCode    = 'SPS';
                $relatedPersonDisplay = 'Epoux ou pouse';
                break;

            case 'enfant':
                $relatedPersonCode    = 'CHILD';
                $relatedPersonDisplay = 'Enfant';
                break;

            case 'enfant_adoptif':
                $relatedPersonCode    = 'CHLDADOPT';
                $relatedPersonDisplay = 'Enfant adopt';
                break;

            //TODO Vers quel code mapper ? CHLDINLAW Gendre ou belle-fille
            case 'beau_fils':
                $relatedPersonCode    = '';
                $relatedPersonDisplay = '';
                break;

            case 'conjoint':
                $relatedPersonCode    = 'SIGOTHR';
                $relatedPersonDisplay = 'Conjoint';
                break;

            default:
                $relatedPersonCode    = 'FRND';
                $relatedPersonDisplay = 'Autre proche';
                break;
        }

        $relatedPersonCoding = (new FHIRCoding())
            ->setSystem($relatedPersonSystem)
            ->setCode($relatedPersonCode)
            ->setDisplay($relatedPersonDisplay);

        $text = $relatedPersonDisplay;

        $relationships[] = (new FHIRCodeableConcept())
            ->setCoding($relatedPersonCoding)
            ->setText($text);

        if (!$relationships) {
            $rolePersonCoding = (new FHIRCoding())
                ->setSystem($relatedPersonSystem)
                ->setCode($relatedPersonCode)
                ->setDisplay($relatedPersonDisplay);

            $text = $rolePersonDisplay;

            $relationships[] = (new FHIRCodeableConcept())
                ->setCoding($rolePersonCoding)
                ->setText($text);
        }

        return $relationships;
    }

    protected function getCodeSourceIdentity(): FHIRCoding
    {
        if ($this->object->status === "QUAL") {
            $code    = 'VALI';
            $display = "Identit� valid�e";
        } else {
            $code    = 'PROV';
            $display = "Identit� provisoire";
        }

        return (new FHIRCoding())
            ->setCode($code)
            ->setSystem("http://interopsante.org/fhir/CodeSystem/fr-v2-0445")
            ->setDisplay($display);
    }

    protected function getModeSourceIdentity(CSourceIdentite $source_identite_active): ?FHIRCoding
    {
        $type   = $source_identite_active->loadRefIdentityProofType();
        $system = '';
        if ($type->code === CIdentityProofType::PROOF_ACTE_NAISSANCE) {
            $code    = 'AN';
            $display = "Extrait d'acte de naissance";
        } elseif ($type->code === CIdentityProofType::PROOF_CARTE_ID) {
            $code    = 'CN';
            $display = "Carte nationale d'identit�";
        } elseif ($type->code === CIdentityProofType::PROOF_LIVRET_FAMILLE) {
            $code    = 'LE';
            $display = "Livret de famille";
        } elseif ($type->code === CIdentityProofType::PROOF_PASSEPORT) {
            $code    = 'PA';
            $display = "Passeport";
        } else {
            return null;
        }

        return (new FHIRCoding())
            ->setCode($code)
            ->setSystem($system)
            ->setDisplay($display);
    }
}
