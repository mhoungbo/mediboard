<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Patient\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\PatientMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientCommunication;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientContact;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Organization\CFHIRResourceOrganization;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Ihe\CPDQm;
use Ox\Interop\Ihe\CPIXm;
use Ox\Mediboard\Patients\CCorrespondantPatient;
use Ox\Mediboard\Patients\CPatient;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

/**
 * Description
 */
class Patient implements DelegatedObjectMapperInterface, PatientMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CPatient */
    protected $object;

    /** @var CFHIRResourcePatient */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @param CFHIRResource $resource
     * @param               $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CPatient && $object->_id;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): ?array
    {
        return [CFHIR::class, CPDQm::class, CPIXm::class];
    }

    public function onlyRessources(): ?array
    {
        return [CFHIRResourcePatient::RESOURCE_TYPE];
    }

    public function mapActive(): ?FHIRBoolean
    {
        return (new FHIRBoolean())
            ->setValue(true);
    }

    public function mapName(): array
    {
        // name
        $names = [];
        $names[] = (new FHIRHumanName())
            ->setFamily($this->object->nom_jeune_fille)
            ->addGiven($this->object->prenom)
            ->setUse('official');

        $has_usual_familly = $this->object->nom && $this->object->nom !== $this->object->nom_jeune_fille;
        if ($has_usual_familly || $this->object->prenom_usuel) {
            $names[] = (new FHIRHumanName())
                ->setFamily($this->object->nom)
                ->addGiven($this->object->prenom_usuel ?: $this->object->prenoms)
                ->setUse('usual');
        }

        return $names;
    }

    public function mapTelecom(): array
    {
        $telecom = [];

        if ($this->object->tel) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel);
        }

        if ($this->object->tel2) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel2);
        }

        if ($this->object->tel_autre) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel_autre);
        }

        if ($this->object->tel_autre_mobile) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel_autre_mobile);
        }

        if ($this->object->tel_pro) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel_pro);
        }

        if ($this->object->email) {
            $telecom[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->email);
        }

        return $telecom;
    }

    public function mapGender(): ?FHIRCode
    {
        return (new FHIRCode())
            ->setValue($this->resource->formatGender($this->object->sexe));
    }

    public function mapBirthDate(): ?FHIRDate
    {
        return (new FHIRDate())
            ->setValue($this->object->naissance);
    }

    /**
     * @throws \Exception
     */
    public function mapDeceased(): ?FHIRElement
    {
        if (!$this->object->deces) {
            return null;
        }

        return (new FHIRDateTime())
            ->setValue(CFHIR::getTimeUtc($this->object->deces, false));
    }

    public function mapAddress(): array
    {
        if (!$this->object->adresse && !$this->object->ville && !$this->object->cp) {
            return [];
        }

        return [
            (new FHIRAddress())
                ->setUse('home')
                ->setType('postal')
                ->setLine(...preg_split('/[\r\n]+/', $this->object->adresse) ?? null)
                ->setCity($this->object->ville ?? null)
                ->setPostalCode($this->object->cp ?? null)
        ];
    }

    public function mapMaritalStatus(): ?FHIRCodeableConcept
    {
        $system = 'http://terminology.hl7.org/CodeSystem/v3-MaritalStatus';

        switch ($this->object->situation_famille) {
            case 'M':
                $code    = 'M';
                $display = 'Married';
                $text    = 'A current marriage contract is active';
                break;

            case 'G':
                $code    = 'T';
                $display = 'Domestic partner';
                $text    = 'Person declares that a domestic partner relationship exists.';
                break;

            case 'D':
                $code    = 'D';
                $display = 'Divorced';
                $text    = 'Marriage contract has been declared dissolved and inactive';
                break;

            case 'W':
                $code    = 'W';
                $display = 'Widowed';
                $text    = 'The spouse has died';
                break;

            case 'A':
                $code    = 'L';
                $display = 'Legally Separated';
                $text    = 'Legally Separated';
                break;

            case 'S':
                $code    = 'U';
                $display = 'unmarried';
                $text    = 'Currently not in a marriage contract.';
                break;

            default:
                $system  = 'http://terminology.hl7.org/CodeSystem/v3-NullFlavor';
                $code    = 'UNK';
                $display = 'unknown';
                $text    = '';
                break;
        }

        return (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($display)
            );
    }

    public function mapMultipleBirth(): ?FHIRElement
    {
        // not implemented
        return null;
    }

    public function mapPhoto(): array
    {
        // not implemented
        return [];
    }

    public function mapContact(): array
    {
        $contacts = [];

        /** @var CCorrespondantPatient[] */
        $correspondantsPatient = $this->object->loadRefsCorrespondantsPatient();

        foreach ($correspondantsPatient as $_correspondant) {
            $relationship = $this->getContactRelationship($_correspondant);

            $name = $this->getContactName($_correspondant);

            $telecom = $this->getContactTelecom($_correspondant);

            $address = $this->getContactAddress($_correspondant);

            $gender = (new FHIRCode())
                ->setValue($this->resource->formatGender($_correspondant->sex));

            $organization = null;

            $period = null;

            $contact = (new FHIRPatientContact())
                ->setName($name)
                ->setAddress($address)
                ->setGender($gender)
                ->setOrganization($organization)
                ->setPeriod($period);

            foreach ($relationship as $item) {
                $contact->addRelationship($item);
            }

            foreach ($telecom as $item) {
                $contact->addTelecom($item);
            }

            $contacts[] = $contact;
        }

        return $contacts;
    }

    public function mapCommunication(): array
    {
        $system  = 'urn:oid:2.16.840.1.113883.4.642.3.20';
        $code    = 'fr-FR';
        $display = 'French (France)';

        $text   = 'Fran�ais de France';

        $language = (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($display)
            );

        $preferred = (new FHIRBoolean())
            ->setValue(true);

        return [
            (new FHIRPatientCommunication())
            ->setLanguage($language)
            ->setPreferred($preferred)
        ];
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapGeneralPractitioner(string $resource_class = CFHIRResourcePractitioner::class): array
    {
        $resource_class = CFHIRResourcePractitioner::class;

        $medecin = $this->object->loadRefMedecinTraitant();

        if (!$medecin->_id) {
            return [];
        }

        return [$this->resource->addReference($resource_class, $medecin)];
    }

    public function mapManagingOrganization(
        string $resource_class = CFHIRResourceOrganization::class
    ): ?FHIRReference {
        $group = $this->object->loadRefGroup();

        if (!$group->_id) {
            return null;
        }

        return $this->resource->addReference($resource_class, $group);
    }

    public function mapLink(): array
    {
        return [];
    }

    /**
     * @param CCorrespondantPatient $_correspondant_patient
     *
     * @return FHIRCodeableConcept[]
     */
    private function getContactRelationship(CCorrespondantPatient $_correspondant_patient): array
    {
        $system = 'urn:oid:2.16.840.1.113883.4.642.3.1130';
        $text   = 'The nature of the relationship between the patient and the contact person.';

        switch ($_correspondant_patient->relation) {
            case 'assurance':
                $code    = 'I';
                $display = 'Insurance Company';
                break;

            case 'employeur':
                $code    = 'E';
                $display = 'Employer';
                break;

            case 'parent_proche':
                $code    = 'N';
                $display = 'Next-of-Kin';
                break;

            default:
                $code    = 'U';
                $display = 'Unknown';
                break;
        }

        return [
            (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($display)
            )
        ];
    }

    protected function getContactName(CCorrespondantPatient $_correspondant_patient): ?FHIRHumanName
    {
        if ($_correspondant_patient->nom_jeune_fille) {
            $name = (new FHIRHumanName())
                ->setFamily($_correspondant_patient->nom_jeune_fille)
                ->setGiven($_correspondant_patient->prenom)
                ->setUse('official');
        } elseif ($_correspondant_patient->nom) {
            $name = (new FHIRHumanName())
                ->setFamily($_correspondant_patient->nom)
                ->setGiven($_correspondant_patient->prenom)
                ->setUse('usual');
        }

        return $name ?? null;
    }

    /**
     * @param CCorrespondantPatient $_correspondant_patient
     *
     * @return FHIRContactPoint[]
     */
    protected function getContactTelecom(CCorrespondantPatient $_correspondant_patient): array
    {
        $contactPoints = [];

        if ($_correspondant_patient->tel) {
            $contactPoints[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($_correspondant_patient->tel);
        }

        if ($_correspondant_patient->tel_autre) {
            $contactPoints[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($_correspondant_patient->tel_autre);
        }

        if ($_correspondant_patient->mob) {
            $contactPoints[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($_correspondant_patient->mob);
        }

        if ($_correspondant_patient->fax) {
            $contactPoints[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($_correspondant_patient->fax);
        }

        if ($_correspondant_patient->email) {
            $contactPoints[] = (new FHIRContactPoint())
                ->setSystem('email')
                ->setValue($_correspondant_patient->email);
        }

        return $contactPoints;
    }

    protected function getContactAddress(CCorrespondantPatient $_correspondant_patient): ?FHIRAddress
    {
        if ($_correspondant_patient->adresse || $_correspondant_patient->ville || $_correspondant_patient->cp) {
            return (new FHIRAddress())
                ->setUse('work')
                ->setType('both')
                ->setLine($_correspondant_patient->adresse ?? null)
                ->setCity($_correspondant_patient->ville ?? null)
                ->setPostalCode($_correspondant_patient->cp ?? null);
        }

        return null;
    }
}
