<?php

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Binary;

use Ox\Interop\Fhir\Contracts\Mapping\R4\BinaryMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceBinaryInterface;
use Ox\Components\FhirCore\Interfaces\FHIRBinaryInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBase64Binary;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * Description
 */
class CFHIRResourceBinary extends CFHIRResource implements ResourceBinaryInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Binary';

    /** @var string */
    public const VERSION_NORMATIVE = '4.0';

    /** @var FHIRBinaryInterface */
    protected $fhirResource;

    /** @var BinaryMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionCreate::NAME,
                ]
            );
    }

    /**
     * @param FHIRCode|null $contentType
     *
     * @return CFHIRResourceBinary
     */
    public function setContentType(?FHIRCode $contentType): CFHIRResourceBinary
    {
        $this->fhirResource->setContentType($contentType);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getContentType(): ?FHIRCode
    {
        return $this->fhirResource->getContentType();
    }

    /**
     * @param FHIRReference|null $securityContext
     *
     * @return CFHIRResourceBinary
     */
    public function setSecurityContext(?FHIRReference $securityContext): CFHIRResourceBinary
    {
        $this->fhirResource->setSecurityContext($securityContext);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getSecurityContext(): ?FHIRReference
    {
        return $this->fhirResource->getSecurityContext();
    }

    /**
     * @return FHIRBase64Binary|null
     */
    public function getData(): ?FHIRBase64Binary
    {
        return $this->fhirResource->getData();
    }

    /**
     * @param FHIRBase64Binary|null $data
     *
     * @return CFHIRResourceBinary
     */
    public function setData(?FHIRBase64Binary $data): CFHIRResourceBinary
    {
        $this->fhirResource->setData($data);

        return $this;
    }

    protected function mapContentType(): void
    {
        $this->fhirResource->setContentType($this->object_mapping->mapContentType());
    }

    protected function mapData(): void
    {
        $this->fhirResource->setData($this->object_mapping->mapData());
    }

    protected function mapSecurityContext(): void
    {
        $this->fhirResource->setSecurityContext($this->object_mapping->mapSecurityContext());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRBinary';
    }
}
