<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Organization;

use Ox\Interop\Fhir\Contracts\Mapping\R4\OrganizationMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceOrganizationInterface;
use Ox\Components\FhirCore\Interfaces\FHIROrganizationInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIROrganizationContact;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * FHIR organization resource
 */
class CFHIRResourceOrganization extends CFHIRDomainResource implements ResourceOrganizationInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Organization';

    /** @var FHIROrganizationInterface $fhirResource */
    protected $fhirResource;
    
    /** @var OrganizationMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            );
    }

    /**
     * @param FHIRBoolean|null $active
     *
     * @return CFHIRResourceOrganization
     */
    public function setActive(?FHIRBoolean $active): CFHIRResourceOrganization
    {
        $this->fhirResource->setActive($active);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getActive(): ?FHIRBoolean
    {
        return $this->fhirResource->getActive();
    }

    /**
     * @param FHIRString|null $name
     *
     * @return CFHIRResourceOrganization
     */
    public function setName(?FHIRString $name): CFHIRResourceOrganization
    {
        $this->fhirResource->setName($name);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getName(): ?FHIRString
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRReference|null $partOf
     *
     * @return CFHIRResourceOrganization
     */
    public function setPartOf(?FHIRReference $partOf): CFHIRResourceOrganization
    {
        $this->fhirResource->setPartOf($partOf);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getPartOf(): ?FHIRReference
    {
        return $this->fhirResource->getPartOf();
    }

    /**
     * @param FHIRReference ...$endpoint
     *
     * @return CFHIRResourceOrganization
     */
    public function setEndpoint(FHIRReference ...$endpoint): CFHIRResourceOrganization
    {
        $this->fhirResource->setEndpoint(...$endpoint);

        return $this;
    }

    /**
     * @param FHIRReference ...$endpoint
     *
     * @return CFHIRResourceOrganization
     */
    public function addEndpoint(FHIRReference ...$endpoint): CFHIRResourceOrganization
    {
        $this->fhirResource->addEndpoint(...$endpoint);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEndpoint(): array
    {
        return $this->fhirResource->getEndpoint();
    }

    /**
     * Map property active
     */
    protected function mapActive(): void
    {
        $this->fhirResource->setActive($this->object_mapping->mapActive());
    }

    /**
     * Map property type
     */
    protected function mapType(): void
    {
        $this->fhirResource->setType(...$this->object_mapping->mapType());
    }

    /**
     * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName($this->object_mapping->mapName());
    }

    /**
     * Map property alias
     */
    protected function mapAlias(): void
    {
        $this->fhirResource->setAlias(...$this->object_mapping->mapAlias());
    }

    /**
     * Map property telecom
     */
    protected function mapTelecom(): void
    {
        $this->fhirResource->setTelecom(...$this->object_mapping->mapTelecom());
    }

    /**
     * Map property address
     */
    protected function mapAddress(): void
    {
        $this->fhirResource->setAddress(...$this->object_mapping->mapAddress());
    }

    /**
     * Map property partOf
     */
    protected function mapPartOf(): void
    {
        $this->fhirResource->setPartOf($this->object_mapping->mapPartOf());
    }

    /**
     * Map property contact
     */
    protected function mapContact(): void
    {
        $this->fhirResource->setContact(...$this->object_mapping->mapContact());
    }

    /**
     * Map property endpoint
     */
    protected function mapEndpoint(): void
    {
        $this->fhirResource->setEndpoint(...$this->object_mapping->mapEndpoint());
    }

    /**
     * @param FHIRCodeableConcept ...$type
     *
     * @return self
     */
    public function setType(FHIRCodeableConcept ...$type): self
    {
        $this->fhirResource->setType(...$type);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$type
     *
     * @return self
     */
    public function addType(FHIRCodeableConcept ...$type): self
    {
        $this->fhirResource->addType(...$type);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getType(): array
    {
        return $this->fhirResource->getType();
    }

    /**
     * @param FHIRString ...$alias
     *
     * @return self
     */
    public function setAlias(FHIRString ...$alias): self
    {
        $this->fhirResource->setAlias(...$alias);

        return $this;
    }

    /**
     * @param FHIRString ...$alias
     *
     * @return self
     */
    public function addAlias(FHIRString ...$alias): self
    {
        $this->fhirResource->addAlias(...$alias);

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getAlias(): array
    {
        return $this->fhirResource->getAlias();
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return self
     */
    public function setTelecom(FHIRContactPoint ...$telecom): self
    {
        $this->fhirResource->setTelecom(...$telecom);

        return $this;
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return self
     */
    public function addTelecom(FHIRContactPoint ...$telecom): self
    {
        $this->fhirResource->addTelecom(...$telecom);

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->fhirResource->getTelecom();
    }

    /**
     * @param FHIRAddress ...$address
     *
     * @return self
     */
    public function setAddress(FHIRAddress ...$address): self
    {
        $this->fhirResource->setAddress(...$address);

        return $this;
    }

    /**
     * @param FHIRAddress ...$address
     *
     * @return self
     */
    public function addAddress(FHIRAddress ...$address): self
    {
        $this->fhirResource->addAddress(...$address);

        return $this;
    }

    /**
     * @return FHIRAddress[]
     */
    public function getAddress(): array
    {
        return $this->fhirResource->getAddress();
    }

    /**
     * @param FHIROrganizationContact ...$contact
     *
     * @return self
     */
    public function setContact(FHIROrganizationContact ...$contact): self
    {
        $this->fhirResource->setContact(...$contact);

        return $this;
    }

    /**
     * @param FHIROrganizationContact ...$contacts
     *
     * @return self
     */
    public function addContact(FHIROrganizationContact ...$contacts): self
    {
        $this->fhirResource->addContact(...$contacts);

        return $this;
    }

    /**
     * @return FHIROrganizationContact[]
     */
    public function getContact(): array
    {
        return $this->fhirResource->getContact();
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIROrganization';
    }
}
