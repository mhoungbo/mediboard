<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Organization\Mapper;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Profiles\CFHIRInteropSante;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\Organization\Profiles\InteropSante\CFHIRResourceOrganizationFR;
use Ox\Mediboard\Patients\CExercicePlace;
use ReflectionException;

/**
 * Description
 */
class FrOrganization extends Organization
{
    /** @var CExercicePlace */
    protected $object;

    /** @var CFHIRResourceOrganizationFR */
    protected CFHIRResource $resource;

    public function mapExtension(): array
    {
        return [
            (new FHIRExtension())
                ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrOrganizationShortName')
                ->setValue((new FHIRString())->setValue($this->object->raison_sociale))
        ];
    }

    public function onlyProfiles(): array
    {
        return [CFHIRInteropSante::class];
    }

    /**
     * @throws ReflectionException|\Exception
     */
    public function mapIdentifier(): array
    {
        $identifiers = parent::mapIdentifier();
        // SYSTEM
        $system = 'http://interopsante.org/CodeSystem/fr-v2-0203';

        // USE
        $use = 'usual';

        if ($this->object->finess_juridique) {
            // FINEJ
            $finejValue = $this->object->finess_juridique;
            $coding = (new FHIRCoding())
                ->setCode('FINEJ')
                ->setDisplay('FINESS d\'entit� juridique')
                ->setSystem($system);
            $type = (new FHIRCodeableConcept())->setCoding($coding);

            // add FINEJ as identifier
            $identifiers[] = (new FHIRIdentifier())
                ->setValue($finejValue)
                ->setSystem($system)
                ->setUse($use)
                ->setType($type);
        }

        if ($this->object->finess) {
            // FINEG
            $finegValue = $this->object->finess;
            $coding     = (new FHIRCoding())
                ->setCode('FINEG')
                ->setDisplay('FINESS d\'entit� g�ographique')
                ->setSystem($system);
            $type       = (new FHIRCodeableConcept())->setCoding($coding);

            // add FINEG as identifier
            $identifiers[] = (new FHIRIdentifier())
                ->setValue($finegValue)
                ->setSystem($system)
                ->setType($type)
                ->setUse($use);
        }

        if ($this->object->siren) {
            // SIREN
            $sirenValue = $this->object->siren;
            $coding     = (new FHIRCoding())
                ->setCode('SIREN')
                ->setDisplay('Identification de l\'organisation au SIREN')
                ->setSystem($system);
            $type       = (new FHIRCodeableConcept())->setCoding($coding);

            // add SIREN as identifier
            $identifiers[] = (new FHIRIdentifier())
                ->setValue($sirenValue)
                ->setSystem($system)
                ->setUse($use)
                ->setType($type);
        }

        if ($this->object->siret) {
            // SIRET
            $siretValue = $this->object->siret;

            $coding     = (new FHIRCoding())
                ->setCode('SIRET')
                ->setDisplay('Identification de l\'organisation au SIRET')
                ->setSystem($system);
            $type       = (new FHIRCodeableConcept())->setCoding($coding);

            // add SIRET as identifier
            $identifiers[] = (new FHIRIdentifier())
                ->setValue($siretValue)
                ->setSystem($system)
                ->setType($type)
                ->setUse($use);
        }

        return $identifiers;
    }

    public function mapType(): array
    {
        $types = [];

        // organizationType
        $system  = "https://simplifier.net/frenchprofiledfhirar/v2-3307";
        $code    = "GROUP";
        $display = "Groupe priv�/hospitalier";
        $coding  = (new FHIRCoding())
            ->setSystem($system)
            ->setCode($code)
            ->setDisplay($display);
        $organizationType = $coding;

        // secteurActiviteRASS
        $system  = "https://mos.esante.gouv.fr/NOS/TRE_R02-SecteurActivite/FHIR/TRE-R02-SecteurActivite";
        $code    = "SA04";
        $display = "Etablissement priv� non PSPH";
        //$version             = 20201030120000;
        //$user_selected             = true;
        $coding  = (new FHIRCoding())
            ->setSystem($system)
            ->setCode($code)
            ->setDisplay($display);
        $secteurActiviteRASS = $coding;

        // categorieEtablissementRASS
        $system  = "https://mos.esante.gouv.fr/NOS/TRE_R66-CategorieEtablissement/FHIR/TRE-R66-CategorieEtablissement";
        $code    = "106";
        $display = "Centre hospitalier, ex H�pital local";
        //$version             = 20210528120000;
        //$user_selected             = true;
        $coding  = (new FHIRCoding())
            ->setSystem($system)
            ->setCode($code)
            ->setDisplay($display);
        $categorieEtablissementRASS = $coding;

        $types[] = (new FHIRCodeableConcept())
            ->setCoding($organizationType)
            ->setText('');
        $types[] = (new FHIRCodeableConcept())
            ->setCoding($secteurActiviteRASS)
            ->setText('');
        $types[] = (new FHIRCodeableConcept())
            ->setCoding($categorieEtablissementRASS)
            ->setText('');

        return $types;
    }
}
