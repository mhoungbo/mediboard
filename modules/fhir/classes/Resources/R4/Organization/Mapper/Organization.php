<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Organization\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\OrganizationMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Organization\CFHIRResourceOrganization;
use Ox\Mediboard\Patients\CExercicePlace;

/**
 * Description
 */
class Organization implements DelegatedObjectMapperInterface, OrganizationMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CExercicePlace */
    protected $object;

    /** @var CFHIRResourceOrganization */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CExercicePlace && $object->_id;
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource,  $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceOrganization::class];
    }

    public function mapActive(): ?FHIRBoolean
    {
        return (new FHIRBoolean())->setValue(true);
    }

    public function mapType(): array
    {
        $system      = 'urn:oid:2.16.840.1.113883.4.642.1.1128';
        $code        = 'prov';
        $displayName = 'Healthcare Provider';
        $text        = 'An organization that provides healthcare services.';

        return [
            (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($displayName)
            )
        ];
    }

    public function mapName(): ?FHIRString
    {
        return (new FHIRString())
            ->setValue($this->object->raison_sociale);
    }

    public function mapAlias(): array
    {
        return [(new FHIRString())
            ->setValue($this->object->raison_sociale)];
    }

    public function mapTelecom(): array
    {
        $telecoms = [];

        if ($this->object->tel) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel);
        }

        if ($this->object->tel2) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel2);
        }

        if ($this->object->fax) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->fax);
        }

        if ($this->object->email) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('email')
                ->setValue($this->object->email);
        }

        return $telecoms;
    }

    public function mapAddress(): array
    {
        if ($this->object->adresse || $this->object->commune || $this->object->cp || $this->object->pays) {
            return [
                (new FHIRAddress())
                    ->setUse('work')
                    ->setType('both')
                    ->setLine($this->object->adresse)
                    ->setCity($this->object->commune)
                    ->setPostalCode($this->object->cp)
                    ->setCountry($this->object->pays)
            ];
        }

        return [];
    }

    public function mapPartOf(): ?FHIRReference
    {
        // not implemented
        return null;
    }

    public function mapContact(): array
    {
        // not implemented
        return [];
    }

    public function mapEndpoint(): array
    {
        // not implemented
        return [];
    }
}
