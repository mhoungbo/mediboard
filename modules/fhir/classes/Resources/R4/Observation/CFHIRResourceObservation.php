<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Observation;

use Exception;
use Ox\Interop\Fhir\Contracts\Mapping\R4\ObservationMappingInterface;
use Ox\Components\FhirCore\Interfaces\FHIRObservationInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRObservationComponent;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * FHIR observation resource
 */
class CFHIRResourceObservation extends CFHIRDomainResource
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Observation';

    // attributes
    /** @var FHIRObservationInterface $fhirResource */
    protected $fhirResource;

    /** @var ObservationMappingInterface */
    protected $object_mapping;

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->fhirResource->getBasedOn();
    }

    /**
     * @param FHIRReference ...$basedOn
     */
    public function setBasedOn(FHIRReference ...$basedOn): self
    {
        $this->fhirResource->setBasedOn(...$basedOn);

        return $this;
    }

    /**
     * @param FHIRReference ...$basedOn
     */
    public function addBasedOn(FHIRReference ...$basedOn): self
    {
        $this->fhirResource->addBasedOn(...$basedOn);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPartOf(): array
    {
        return $this->fhirResource->getPartOf();
    }

    /**
     * @param FHIRReference ...$partOf
     */
    public function setPartOf(FHIRReference ...$partOf): self
    {
        $this->fhirResource->setPartOf(...$partOf);

        return $this;
    }

    /**
     * @param FHIRReference ...$partOf
     */
    public function addPartOf(FHIRReference ...$partOf): self
    {
        $this->fhirResource->addPartOf(...$partOf);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceObservation
     */
    public function setStatus(?FHIRCode $status): self
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCategory(): array
    {
        return $this->fhirResource->getCategory();
    }

    /**
     * @param FHIRCodeableConcept ...$category
     */
    public function setCategory(FHIRCodeableConcept ...$category): self
    {
        $this->fhirResource->setCategory(...$category);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$category
     */
    public function addCategory(FHIRCodeableConcept ...$category): self
    {
        $this->fhirResource->addCategory(...$category);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getCode();
    }

    /**
     * @param FHIRCodeableConcept|null $code
     *
     * @return CFHIRResourceObservation
     */
    public function setCode(?FHIRCodeableConcept $code): self
    {
        $this->fhirResource->setCode($code);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getSubject(): ?FHIRReference
    {
        return $this->fhirResource->getSubject();
    }

    /**
     * @param FHIRReference|null $subject
     *
     * @return CFHIRResourceObservation
     */
    public function setSubject(?FHIRReference $subject): self
    {
        $this->fhirResource->setSubject($subject);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getFocus(): array
    {
        return $this->fhirResource->getFocus();
    }

    /**
     * @param FHIRReference ...$focus
     */
    public function setFocus(FHIRReference ...$focus): self
    {
        $this->fhirResource->setFocus(...$focus);

        return $this;
    }

    /**
     * @param FHIRReference ...$focus
     */
    public function addFocus(FHIRReference ...$focus): self
    {
        $this->fhirResource->addFocus(...$focus);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getEncounter(): ?FHIRReference
    {
        return $this->fhirResource->getEncounter();
    }

    /**
     * @param FHIRReference|null $encounter
     *
     * @return CFHIRResourceObservation
     */
    public function setEncounter(?FHIRReference $encounter): self
    {
        $this->fhirResource->setEncounter($encounter);

        return $this;
    }

    /**
     * @return FHIRElement|null
     */
    public function getEffective(): ?FHIRElement
    {
        return $this->fhirResource->getEffective();
    }

    /**
     * @param FHIRElement|null $effective
     *
     * @return CFHIRResourceObservation
     */
    public function setEffective(?FHIRElement $effective): self
    {
        $this->fhirResource->setEffective($effective);

        return $this;
    }

    /**
     * @return FHIRInstant|null
     */
    public function getIssued(): ?FHIRInstant
    {
        return $this->fhirResource->getIssued();
    }

    /**
     * @param FHIRInstant|null $issued
     *
     * @return CFHIRResourceObservation
     */
    public function setIssued(?FHIRInstant $issued): self
    {
        $this->fhirResource->setIssued($issued);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getPerformer(): array
    {
        return $this->fhirResource->getPerformer();
    }

    /**
     * @param FHIRReference ...$performer
     */
    public function setPerformer(FHIRReference ...$performer): self
    {
        $this->fhirResource->setPerformer(...$performer);

        return $this;
    }

    /**
     * @param FHIRReference ...$performer
     */
    public function addPerformer(FHIRReference ...$performer): self
    {
        $this->fhirResource->addPerformer(...$performer);

        return $this;
    }

    /**
     * @return FHIRElement|null
     */
    public function getValue(): ?FHIRElement
    {
        return $this->fhirResource->getValue();
    }

    /**
     * @param FHIRElement $value
     */
    public function setValue(?FHIRElement $value): self
    {
        $this->fhirResource->setValue($value);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getDataAbsentReason(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getDataAbsentReason();
    }

    /**
     * @param FHIRCodeableConcept $dataAbsentReason
     */
    public function setDataAbsentReason(?FHIRCodeableConcept $dataAbsentReason): self
    {
        $this->fhirResource->setDataAbsentReason($dataAbsentReason);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getInterpretation(): array
    {
        return $this->fhirResource->getInterpretation();
    }

    /**
     * @param FHIRCodeableConcept ...$interpretation
     *
     * @return CFHIRResourceObservation
     */
    public function setInterpretation(FHIRCodeableConcept ...$interpretation): self
    {
        $this->fhirResource->setInterpretation(...$interpretation);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$interpretation
     *
     * @return CFHIRResourceObservation
     */
    public function addInterpretation(FHIRCodeableConcept ...$interpretation): self
    {
        $this->fhirResource->addInterpretation(...$interpretation);

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->fhirResource->getNote();
    }

    /**
     * @param FHIRAnnotation ...$note
     *
     * @return CFHIRResourceObservation
     */
    public function setNote(FHIRAnnotation ...$note): self
    {
        $this->fhirResource->setNote(...$note);

        return $this;
    }

    /**
     * @param FHIRAnnotation ...$note
     *
     * @return CFHIRResourceObservation
     */
    public function addNote(FHIRAnnotation ...$note): self
    {
        $this->fhirResource->addNote(...$note);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getBodySite(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getBodySite();
    }

    /**
     * @param FHIRCodeableConcept|null $bodySite
     *
     * @return CFHIRResourceObservation
     */
    public function setBodySite(?FHIRCodeableConcept $bodySite): self
    {
        $this->fhirResource->setBodySite($bodySite);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getMethod(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getMethod();
    }

    /**
     * @param FHIRCodeableConcept|null $method
     *
     * @return CFHIRResourceObservation
     */
    public function setMethod(?FHIRCodeableConcept $method): self
    {
        $this->fhirResource->setMethod($method);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getSpecimen(): ?FHIRReference
    {
        return $this->fhirResource->getSpecimen();
    }

    /**
     * @param FHIRReference $specimen
     */
    public function setSpecimen(?FHIRReference $specimen): self
    {
        $this->fhirResource->setSpecimen($specimen);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getDevice(): ?FHIRReference
    {
        return $this->fhirResource->getDevice();
    }

    /**
     * @param FHIRReference $device
     */
    public function setDevice(?FHIRReference $device): self
    {
        $this->fhirResource->setDevice($device);

        return $this;
    }

    /**
     * @return FHIRBackboneElement[]
     */
    public function getReferenceRange(): array
    {
        return $this->fhirResource->getReferenceRange();
    }

    /**
     * @param FHIRBackboneElement ...$referenceRange
     *
     * @return CFHIRResourceObservation
     */
    public function setReferenceRange(FHIRBackboneElement ...$referenceRange): self
    {
        $this->fhirResource->setReferenceRange(...$referenceRange);

        return $this;
    }

    /**
     * @param FHIRBackboneElement ...$referenceRange
     *
     * @return CFHIRResourceObservation
     */
    public function addReferenceRange(FHIRBackboneElement ...$referenceRange): self
    {
        $this->fhirResource->addReferenceRange(...$referenceRange);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getHasMember(): array
    {
        return $this->fhirResource->getHasMember();
    }

    /**
     * @param FHIRReference ...$hasMember
     *
     * @return CFHIRResourceObservation
     */
    public function setHasMember(FHIRReference ...$hasMember): self
    {
        $this->fhirResource->setHasMember(...$hasMember);

        return $this;
    }

    /**
     * @param FHIRReference ...$hasMember
     *
     * @return CFHIRResourceObservation
     */
    public function addHasMember(FHIRReference ...$hasMember): self
    {
        $this->fhirResource->addHasMember(...$hasMember);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getDerivedFrom(): array
    {
        return $this->fhirResource->getDerivedFrom();
    }

    /**
     * @param FHIRReference ...$derivedFrom
     *
     * @return CFHIRResourceObservation
     */
    public function setDerivedFrom(FHIRReference ...$derivedFrom): self
    {
        $this->fhirResource->setDerivedFrom(...$derivedFrom);

        return $this;
    }

    /**
     * @param FHIRReference ...$derivedFrom
     *
     * @return CFHIRResourceObservation
     */
    public function addDerivedFrom(FHIRReference ...$derivedFrom): self
    {
        $this->fhirResource->addDerivedFrom(...$derivedFrom);

        return $this;
    }

    /**
     * @return FHIRObservationComponent[]
     */
    public function getComponent(): array
    {
        return $this->fhirResource->getComponent();
    }

    /**
     * @param FHIRObservationComponent ...$component
     *
     * @return CFHIRResourceObservation
     */
    public function setComponent(FHIRObservationComponent ...$component): self
    {
        $this->fhirResource->setComponent(...$component);

        return $this;
    }

    /**
     * @param FHIRObservationComponent ...$component
     *
     * @return CFHIRResourceObservation
     */
    public function addComponent(FHIRObservationComponent ...$component): self
    {
        $this->fhirResource->addComponent(...$component);

        return $this;
    }

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            );
    }

    /**
     * Map property basedOn
     */
    protected function mapBasedOn(): void
    {
        $this->fhirResource->setBasedOn(...$this->object_mapping->mapBasedOn());
    }

    /**
     * Map property partOf
     */
    protected function mapPartOf(): void
    {
        $this->fhirResource->setPartOf(...$this->object_mapping->mapPartOf());
    }

    /**
     * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * Map property category
     */
    protected function mapCategory(): void
    {
        $this->fhirResource->setCategory(...$this->object_mapping->mapCategory());
    }

    /**
     * Map property code
     */
    protected function mapCode(): void
    {
        $this->fhirResource->setCode($this->object_mapping->mapCode());
    }

    /**
     * Map property subject
     * @throws Exception
     */
    protected function mapSubject(): void
    {
        $this->fhirResource->setSubject($this->object_mapping->mapSubject());
    }

    /**
     * Map property focus
     */
    protected function mapFocus(): void
    {
        $this->fhirResource->setFocus(...$this->object_mapping->mapFocus());
    }

    /**
     * Map property encounter
     */
    protected function mapEncounter(): void
    {
        $this->fhirResource->setEncounter($this->object_mapping->mapEncounter());
    }

    /**
     * Map property effective
     */
    protected function mapEffective(): void
    {
        $this->fhirResource->setEffective($this->object_mapping->mapEffective());
    }

    /**
     * Map property issued
     */
    protected function mapIssued(): void
    {
        $this->fhirResource->setIssued($this->object_mapping->mapIssued());
    }

    /**
     * Map property performer
     * @throws Exception
     */
    protected function mapPerformer(): void
    {
        $this->fhirResource->setPerformer(...$this->object_mapping->mapPerformer());
    }

    /**
     * Map property value
     */
    protected function mapValue(): void
    {
        $this->fhirResource->setValue($this->object_mapping->mapValue());
    }

    /**
     * Map property dataAbsentReason
     */
    protected function mapDataAbsentReason(): void
    {
        $this->fhirResource->setDataAbsentReason($this->object_mapping->mapDataAbsentReason());
    }

    /**
     * Map property interpretation
     */
    protected function mapInterpretation(): void
    {
        $this->fhirResource->setInterpretation(...$this->object_mapping->mapInterpretation());
    }

    /**
     * Map property note
     */
    protected function mapNote(): void
    {
        $this->fhirResource->setNote(...$this->object_mapping->mapNote());
    }

    /**
     * Map property bodySite
     */
    protected function mapBodySite(): void
    {
        $this->fhirResource->setBodySite($this->object_mapping->mapBodySite());
    }

    /**
     * Map property method
     */
    protected function mapMethod(): void
    {
        $this->fhirResource->setMethod($this->object_mapping->mapMethod());
    }

    /**
     * Map property specimen
     */
    protected function mapSpecimen(): void
    {
        $this->fhirResource->setSpecimen($this->object_mapping->mapSpecimen());
    }

    /**
     * Map property device
     */
    protected function mapDevice(): void
    {
        $this->fhirResource->setDevice($this->object_mapping->mapDevice());
    }

    /**
     * Map property referenceRange
     */
    protected function mapReferenceRange(): void
    {
        $this->fhirResource->setReferenceRange(...$this->object_mapping->mapReferenceRange());
    }

    /**
     * Map property hasMember
     */
    protected function mapHasMember(): void
    {
        $this->fhirResource->setHasMember(...$this->object_mapping->mapHasMember());
    }

    /**
     * Map property derivedFrom
     */
    protected function mapDerivedFrom(): void
    {
        $this->fhirResource->setDerivedFrom(...$this->object_mapping->mapDerivedFrom());
    }

    /**
     * Map property component
     */
    protected function mapComponent(): void
    {
        $this->fhirResource->setComponent(...$this->object_mapping->mapComponent());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRObservation';
    }

}
