<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Observation\Mapper\ANS;

use Exception;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRObservationReferenceRange;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBMIENS;
use Ox\Mediboard\Patients\Constants\CAbstractConstant;
use Ox\Mediboard\Patients\Constants\CConstantException;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Description
 */
class ENSObservationBMI extends ENSObservation
{
    /** @var CAbstractConstant */
    protected $object;

    /** @var CFHIRResourceObservationBMIENS */
    protected CFHIRResource $resource;

    public function onlyRessources(): array
    {
        return [CFHIRResourceObservationBMIENS::class];
    }

    /**
     * @param CFHIRResource     $resource
     * @param CAbstractConstant $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        if (!parent::isSupported($resource, $object)) {
            return false;
        }

        $spec = $object->getRefSpec();

        return $spec->code === 'imc';
    }

    public function mapCode(): ?FHIRCodeableConcept
    {
        $system  = "http://loinc.org";
        $code    = "39156-5";
        $display = "BMICode";

        $text   = 'Indice de Masse Corporelle';

        return (new FHIRCodeableConcept())
                ->setText($text)
                ->setCoding(
                    (new FHIRCoding())
                        ->setSystem($system)
                        ->setCode($code)
                        ->setDisplay($display)
                );
    }

    /**
     * @throws CConstantException
     */
    public function mapValue(): ?FHIRElement
    {
        // L'unit� de l'IMC est g�n�ralement en kg/m2
        // Ici on a l'unit� g/mm�
        return (new FHIRQuantity())
            ->setValue($this->object->getValue())
            ->setUnit("kg/m2") // todo Attention mauvaise unit�
            ->setSystem("http://unitsofmeasure.org")
            ->setCode("kg/m2");
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function mapReferenceRange(): array
    {
        $system = "https://mos.esante.gouv.fr/NOS/TRE_R303-HL7v3AdministrativeGender/FHIR/TRE-R303-HL7v3AdministrativeGender";

        $patient = $this->object->loadRefPatient();
        $sexe    = $patient->sexe;

        $grossesse = $patient->loadLastGrossesse();

        if (!$grossesse) {
            return [];
        }

        $isPregnant = $grossesse->active;

        $code    = $sexe;
        $display = $sexe === 'M' ? 'Masculin' : 'F�minin';

        if ($isPregnant) {
            $system  = "https://mos.esante.gouv.fr/NOS/TRE_A04-Loinc/FHIR/TRE-A04-Loinc";
            $code    = "LA15173-0";
            $display = "Femme enceinte";
        }

        $text   = $display;

        return [
            (new FHIRObservationReferenceRange())
            ->setAppliesTo(
                (new FHIRCodeableConcept())
                    ->setText($text)
                    ->setCoding(
                        (new  FHIRCoding())
                            ->setSystem($system)
                            ->setCode($code)
                            ->setDisplay($display)
                    )
            )
        ];
    }
}
