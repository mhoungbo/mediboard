<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Observation\Mapper\ANS;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRObservationComponent;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceTrait;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBPENS;
use Ox\Mediboard\Patients\Constants\CAbstractConstant;
use Ox\Mediboard\Patients\Constants\CConstantException;
use ReflectionException;

/**
 * Description
 */
class ENSObservationBP extends ENSObservation
{
    use CStoredObjectResourceTrait;

    /** @var CAbstractConstant */
    protected $object;

    /** @var CFHIRResourceObservationBPENS */
    protected CFHIRResource $resource;

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceObservationBPENS::class];
    }

    /**
     * @param CFHIRResource     $resource
     * @param CAbstractConstant $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return false; // il faut que le object soit un tableau de deux constants [diastole, systole]

        if (!parent::isSupported($resource, $object)) {
            return false;
        }

        $spec = $object->getRefSpec();

        return $spec->code === '';
    }

    public function mapExtension(): array
    {
        return [
            (new FHIRExtension())
                ->setUrl("http://esante.gouv.fr/ci-sis/fhir/StructureDefinition/ENS_ReasonForMeasurement")
                ->setValue((new FHIRString())->setValue("Ma nouvelle pression artérielle !"))
        ];
    }

    public function mapCategory(): array
    {
        $system  = "http://terminology.hl7.org/CodeSystem/observation-category";
        $code    = "vital-signs";
        $display = "Signes vitaux";

        $text   = "Signes vitaux";

        return [
            (new FHIRCodeableConcept())
                ->setText($text)
                ->setCoding(
                    (new FHIRCoding())
                        ->setSystem($system)
                        ->setCode($code)
                        ->setDisplay($display)
                ),
        ];
    }

    public function mapCode(): ?FHIRCodeableConcept
    {
        $system  = "http://loinc.org";
        $code    = "85354-9";
        $display = "BPCode";

        $text   = 'Pression artérielle';

        return (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($display)
            );
    }

    /**
     * @throws CConstantException
     * @throws ReflectionException|\Psr\SimpleCache\InvalidArgumentException
     */
    public function mapComponent(): array
    {
        // todo soucis ici, il faut qu'on passe plusieurs AbstractConstant !!
        $system = 'http://loinc.org';

        $systole_coding = (new FHIRCoding())
            ->setSystem($system)
            ->setCode('8480-6')
            ->setDisplay('Systolic blood pressure');

        $systole_value = (new FHIRQuantity())
            ->setValue($this->object->getValue())
            ->setUnit($this->object->getViewUnit())
            ->setSystem("http://unitsofmeasure.org")
            ->setCode("mm[Hg]");

        $systole_code = (new FHIRCodeableConcept())
            ->setCoding($systole_coding)
            ->setText('');

        $systole_backbone = (new FHIRObservationComponent())
            ->setCode($systole_code)
            ->setValue($systole_value);


        $diastole_coding = (new FHIRCoding())
            ->setSystem($system)
            ->setCode('8462-4')
            ->setDisplay('Diastolic blood pressure');

        $diastole_value = (new FHIRQuantity())
            ->setValue($this->object->getValue())
            ->setUnit($this->object->getViewUnit())
            ->setSystem("http://unitsofmeasure.org")
            ->setCode("mm[Hg]");

        $diastole_code = (new FHIRCodeableConcept())
            ->setCoding($diastole_coding)
            ->setText('');

        $diastole_backbone = (new FHIRObservationComponent())
            ->setCode($diastole_code)
            ->setValue($diastole_value);

        return [$systole_backbone, $diastole_backbone];
    }
}
