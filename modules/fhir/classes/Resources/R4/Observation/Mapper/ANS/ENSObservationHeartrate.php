<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Observation\Mapper\ANS;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceTrait;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationHeartrateENS;
use Ox\Mediboard\Patients\Constants\CAbstractConstant;
use Ox\Mediboard\Patients\Constants\CConstantException;

/**
 * Description
 */
class ENSObservationHeartrate extends ENSObservation
{
    use CStoredObjectResourceTrait;

    /** @var CAbstractConstant */
    protected $object;

    /** @var CFHIRResourceObservationHeartrateENS */
    protected CFHIRResource $resource;

    public function onlyRessources(): array
    {
        return [CFHIRResourceObservationHeartrateENS::class];
    }

    /**
     * @param CFHIRResource     $resource
     * @param CAbstractConstant $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        if (!parent::isSupported($resource, $object)) {
            return false;
        }

        $spec = $object->getRefSpec();

        return $spec->code === 'heartrate';
    }

    public function mapExtension(): array
    {
        return [
            (new FHIRExtension())
                ->setUrl("http://esante.gouv.fr/ci-sis/fhir/StructureDefinition/ENS_ReasonForMeasurement")
                ->setValue((new FHIRString())->setValue("Ma nouvelle fr�quence cardiaque !"))
        ];
    }

    public function mapCode(): ?FHIRCodeableConcept
    {
        $system  = "http://loinc.org";
        $code    = "8867-4";
        $display = "heartrate";

        $text   = "Fr�quence cardiaque";

        return (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($display)
            );
    }

    /**
     * @throws CConstantException
     */
    public function mapValue(): ?FHIRElement
    {
        return (new FHIRQuantity())
            ->setValue($this->object->getValue())
            ->setUnit($this->object->getViewUnit()) // todo attention aux unit�s
            ->setSystem("http://unitsofmeasure.org")
            ->setCode("/min");
    }
}
