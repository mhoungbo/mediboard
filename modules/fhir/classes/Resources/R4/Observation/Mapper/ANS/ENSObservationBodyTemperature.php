<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Observation\Mapper\ANS;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceTrait;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBodyTemperatureENS;
use Ox\Mediboard\Patients\Constants\CAbstractConstant;
use Ox\Mediboard\Patients\Constants\CConstantException;

/**
 * Description
 */
class ENSObservationBodyTemperature extends ENSObservation
{
    use CStoredObjectResourceTrait;

    /** @var CAbstractConstant */
    protected $object;

    /** @var CFHIRResourceObservationBodyTemperatureENS */
    protected CFHIRResource $resource;

    public function onlyRessources(): array
    {
        return [CFHIRResourceObservationBodyTemperatureENS::class];
    }

    /**
     * @param CFHIRResource     $resource
     * @param CAbstractConstant $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        if (!parent::isSupported($resource, $object)) {
            return false;
        }

        $spec = $object->getRefSpec();

        return $spec->code === 'temperature';
    }

    public function mapExtension(): array
    {
        //TODO Change the codeableconcept once the levelOfExertion is up
        $system  = "https://mos.esante.gouv.fr/NOS/TRE_R306-CLADIMED/FHIR/TRE-R306-CLADIMED";
        $code    = "K50BI02";
        $display = "Balance";

        $coding = (new FHIRCoding())
            ->setSystem($system)
            ->setCode($code)
            ->setDisplay($display);
        $text   = "Balance";

        $extension = [
            (new FHIRExtension())
                ->setUrl('http://interopsante.org/fhir/StructureDefinition/FrObservationLevelOfExertion')
                ->setValue(
                    (new FHIRCodeableConcept())
                        ->setCoding($coding)
                        ->setText($text)
                )
        ];

        $extension[] = (new FHIRExtension())
            ->setUrl('http://esante.gouv.fr/ci-sis/fhir/StructureDefinition/ENS_ReasonForMeasurement')
            ->setValue((new FHIRString())->setValue('Ma nouvelle température !'));

        return $extension;
    }

    public function mapCode(): ?FHIRCodeableConcept
    {
        $system  = "http://loinc.org";
        $code    = "8310-5";
        $display = "BodyTempCode";

        $text   = 'Température corporelle';

        return (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($display)
            );
    }

    /**
     * @throws CConstantException
     */
    public function mapValue(): ?FHIRElement
    {
        return (new FHIRQuantity())
            ->setValue($this->object->getValue())
            ->setUnit($this->object->getViewUnit())
            ->setSystem("http://unitsofmeasure.org")
            ->setCode("Cel");
    }
}
