<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Observation\Mapper\ANS;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRQuantity;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceTrait;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationStepsByDayENS;
use Ox\Mediboard\Patients\Constants\CAbstractConstant;
use Ox\Mediboard\Patients\Constants\CConstantException;

/**
 * Description
 */
class ENSObservationStepByDay extends ENSObservation
{
    use CStoredObjectResourceTrait;

    /** @var CAbstractConstant */
    protected $object;

    /** @var CFHIRResourceObservationStepsByDayENS */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource     $resource
     * @param CAbstractConstant $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        if (!parent::isSupported($resource, $object)) {
            return false;
        }

        $spec = $object->getRefSpec();

        return $spec->code === 'dailyactivity';
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceObservationStepsByDayENS::class];
    }

    public function mapExtension(): array
    {
        return [
            (new FHIRExtension())
                ->setUrl("http://esante.gouv.fr/ci-sis/fhir/StructureDefinition/ENS_ReasonForMeasurement")
                ->setValue((new FHIRString())->setValue("Ma nouvelle activit� quotidienne !"))
        ];
    }

    public function mapCode(): ?FHIRCodeableConcept
    {
        $system  = "http://loinc.org";
        $code    = "41950-7";
        $display = "Number of steps in 24 hour Measured";

        $text   = "Nombre de pas mesur�(s) ces derni�res 24h";

        return (new FHIRCodeableConcept())
            ->setText($text)
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem($system)
                    ->setCode($code)
                    ->setDisplay($display)
            );
    }

    /**
     * @throws CConstantException
     */
    public function mapValue(): ?FHIRElement
    {
        return (new FHIRQuantity())
            ->setValue($this->object->getValue())
            ->setUnit('steps/day')
            ->setSystem("http://unitsofmeasure.org")
            ->setCode("1/(24.h)");
    }
}
