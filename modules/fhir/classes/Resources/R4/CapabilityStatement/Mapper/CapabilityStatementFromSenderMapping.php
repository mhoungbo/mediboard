<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\CapabilityStatement\Mapper;

use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Interop\Fhir\Actors\CSenderFHIR;
use Ox\Interop\Fhir\Contracts\Mapping\R4\CapabilityStatementMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRMeta;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRNarrative;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCanonical;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementImplementation;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementRest;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementRestResource;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementRestResourceInteraction;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementSoftware;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\CapabilityStatement\CFHIRResourceCapabilityStatement;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\System\CSenderFileSystem;
use Ox\Mediboard\System\CSenderHTTP;

/**
 * FIHR CapabilityStatement resource
 */
class CapabilityStatementFromSenderMapping implements CapabilityStatementMappingInterface
{
    private CFHIRResourceCapabilityStatement $capability;
    private CSenderFHIR $sender_FHIR;

    private CSenderHTTP $sender_HTTP;

    public function __construct(CFHIRResourceCapabilityStatement $capability, CSenderFHIR $sender_FHIR)
    {
        $this->capability  = $capability;
        $this->sender_FHIR = $sender_FHIR;
        $this->sender_HTTP = $sender_FHIR->getSender();
    }

    /**
     * @inheritDoc
     */
    public function mapUrl(): ?FHIRUri
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapVersion(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapName(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapTitle(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapStatus(): ?FHIRCode
    {
        return (new FHIRCode())
            ->setValue("active");
    }

    /**
     * @inheritDoc
     */
    public function mapExperimental(): ?FHIRBoolean
    {
        return null;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function mapDate(): ?FHIRDateTime
    {
        return (new FHIRDateTime())
            ->setValue(CFHIR::getTimeUtc(CApp::getVersion()->getReleaseDate(), false));
    }

    /**
     * @inheritDoc
     */
    public function mapPublisher(): ?FHIRString
    {
        $publisher = null;
        if ($product_name = CAppUI::conf('product_name')) {
            $publisher = (new FHIRString())
                ->setValue($product_name);
        }

        return $publisher;
    }

    /**
     * @inheritDoc
     */
    public function mapContact(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapDescription(): ?FHIRMarkdown
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapUseContext(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapJurisdiction(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapPurpose(): ?FHIRMarkdown
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapCopyright(): ?FHIRMarkdown
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapKind(): ?FHIRCode
    {
        return (new FHIRCode())
            ->setValue("instance");
    }

    /**
     * @inheritDoc
     */
    public function mapInstantiates(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapImports(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapSoftware(): ?FHIRCapabilityStatementSoftware
    {
        $product_name = CAppUI::conf("product_name") ?: null;
        $version      = (string)CApp::getVersion() ?: null;
        $release_date = CApp::getVersion()->getReleaseDate() ?: null;

        if (!$product_name && !$version && !$release_date) {
            return null;
        }

        return (new FHIRCapabilityStatementSoftware())
            ->setName((new FHIRString())->setValue($product_name))
            ->setVersion((new FHIRString())->setValue($version))
            ->setReleaseDate((new FHIRDateTime())->setValue(CFHIR::getTimeUtc($release_date, false)));
    }

    /**
     * @inheritDoc
     */
    public function mapFhirVersion(): ?FHIRCode
    {
        return (new FHIRCode())
            ->setValue($this->capability->getResourceFHIRVersion());
    }

    /**
     * @inheritDoc
     */
    public function mapFormat(): array
    {
        return [
            (new FHIRCode())->setValue("application/fhir+xml"),
            (new FHIRCode())->setValue("application/fhir+json"),
        ];
    }

    /**
     * @inheritDoc
     */
    public function mapPatchFormat(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapImplementationGuide(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapRest(): array
    {
        $available_resources = $this->sender_FHIR->getAvailableResources();

        $resources = [];
        foreach ($available_resources as $resource) {
            if ($resource::RESOURCE_TYPE === CFHIRResourceCapabilityStatement::RESOURCE_TYPE) {
                continue;
            }

            $interactions = $this->sender_FHIR->getAvailableInteractions($resource);

            $interactions = array_map(
                function ($interaction) {
                    return (new FHIRCapabilityStatementRestResourceInteraction())
                        ->setCode((new FHIRCode())->setValue($interaction));
                },
                $interactions
            );

            $profile = (new FHIRCanonical())
                ->setValue($resource->getProfile());

            /** @var CFHIRResource[] $extension_classes */
            $supportedProfile = $this->sender_FHIR->getAvailableProfiles($resource);
            $resources[] = (new FHIRCapabilityStatementRestResource())
                ->setType($resource::RESOURCE_TYPE)
                ->setProfile($profile)
                ->setSupportedProfile(...$supportedProfile)
                ->setInteraction(...$interactions);
        }

        return [
            (new FHIRCapabilityStatementRest())
                ->setMode('server')
                ->setResource(...$resources)
        ];
    }

    /**
     * @inheritDoc
     */
    public function mapText(): ?FHIRNarrative
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapContained(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapExtension(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapModifierExtension(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapId(): ?FHIRString
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapMeta(): ?FHIRMeta
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapImplicitRules(): ?FHIRUri
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapLanguage(): ?FHIRCode
    {
        return null;
    }

    public function mapImplementation(): ?FHIRCapabilityStatementImplementation
    {
        return null;
    }

    public function mapMessaging(): array
    {
        return [];
    }

    public function mapDocument(): array
    {
        return [];
    }
}
