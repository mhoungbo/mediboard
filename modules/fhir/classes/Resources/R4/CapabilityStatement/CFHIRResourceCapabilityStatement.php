<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\CapabilityStatement;

use DOMDocument;
use DOMNode;
use Exception;
use Ox\Interop\Fhir\CFHIRXPath;
use Ox\Interop\Fhir\Contracts\Mapping\R4\CapabilityStatementMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceCapabilityStatementInterface;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Components\FhirCore\Interfaces\FHIRCapabilityStatementInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCanonical;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementDocument;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementImplementation;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementMessaging;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementRest;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementRestInteraction;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementRestResource;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementSoftware;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCapabilities;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Resources\R4\CapabilityStatement\Mapper\CapabilityStatementFromSender;
use Ox\Interop\Fhir\Utilities\CCapabilities;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * FHIR CapabilityStatement resource
 */
class CFHIRResourceCapabilityStatement extends CFHIRDomainResource implements ResourceCapabilityStatementInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'CapabilityStatement';

    /** @var string */
    public const VERSION_NORMATIVE = '4.0';

    // attributes
    public ?FHIRString $base_fhir_version;
    public ?FHIRCode   $acceptUnknown;
    /** @var FHIRCapabilityStatementInterface */
    protected $fhirResource;

    /** @var CapabilityStatementMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return parent::generateCapabilities()
            ->addInteractions([CFHIRInteractionCapabilities::NAME]);
    }

    /**
     * @param string $data
     *
     * @return CCapabilities|null
     * @throws Exception
     */
    public function deserialize(string $data): ?CCapabilities
    {
        $dom = new DOMDocument();
        $dom->loadXML($data);
        $dom->formatOutput = true;

        $xpath = new CFHIRXPath($dom);

        $resources      = [];
        $resource_nodes = $xpath->query('/fhir:' . $this::RESOURCE_TYPE . "/fhir:rest/fhir:resource");
        /** @var DOMNode $node */
        foreach ($resource_nodes as $node) {
            $resource_type = $xpath->queryAttributNode('fhir:type', $node, 'value');
            $profile       = $xpath->queryAttributNode('fhir:profile', $node, 'value');

            $interaction_nodes = $xpath->query('fhir:interaction', $node);
            $interactions      = [];
            foreach ($interaction_nodes as $interaction_node) {
                $interactions[] = $xpath->queryAttributNode('fhir:code', $interaction_node, 'value');
            }

            $supported_profiles       = [];
            $supported_profiles_nodes = $xpath->query('fhir:supportedProfile', $node);
            /** @var DOMNode $profiles_node */
            foreach ($supported_profiles_nodes as $profiles_node) {
                if ($value = $profiles_node->attributes->getNamedItem('value')->nodeValue) {
                    $supported_profiles[] = $value;
                }
            }

            $resources[] = [
                'type'              => $resource_type,
                'profile'           => $profile,
                'supportedProfiles' => $supported_profiles,
                'interactions'      => $interactions,
                'updateCreate'      => false // allow client to define its own id on update request (PUT)
            ];
        }

        return (new CCapabilities())
            ->addResources($resources);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function interactionCapabilities(): void
    {
        if (!$this->_sender) {
            throw new CFHIRException('Impossible to find sender');
        }

        $this->setMapper(new CapabilityStatementFromSender());
        $this->mapFrom($this->_sender);
        $this->interaction->setResource($this);
    }

    /**
     * @param FHIRString|null $base_fhir_version
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setBaseFHIRVersion(?FHIRString $base_fhir_version): self
    {
        $this->base_fhir_version = $base_fhir_version;

        return $this;
    }

    /**
     * @param FHIRString|null $version
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setVersion(?FHIRString $version): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setVersion($version);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getVersion(): ?FHIRString
    {
        return $this->fhirResource->getVersion();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRDateTime|null $date
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setDate(?FHIRDateTime $date): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setDate($date);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getDate(): ?FHIRDateTime
    {
        return $this->fhirResource->getDate();
    }

    /**
     * @param FHIRString|null $publisher
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setPublisher(?FHIRString $publisher): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setPublisher($publisher);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getPublisher(): ?FHIRString
    {
        return $this->fhirResource->getPublisher();
    }

    /**
     * @param FHIRCode|null $kind
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setKind(?FHIRCode $kind): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setKind($kind);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getKind(): ?FHIRCode
    {
        return $this->fhirResource->getKind();
    }

    /**
     * @param FHIRCapabilityStatementSoftware|null $software
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setSoftware(?FHIRCapabilityStatementSoftware $software): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setSoftware($software);

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementSoftware|null
     */
    public function getSoftware(): ?FHIRCapabilityStatementSoftware
    {
        return $this->fhirResource->getSoftware();
    }

    /**
     * @param FHIRCode|null $fhirVersion
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setFhirVersion(?FHIRCode $fhirVersion): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setFhirVersion($fhirVersion);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getFhirVersion(): ?FHIRCode
    {
        return $this->fhirResource->getFhirVersion();
    }

    /**
     * @param FHIRCode|null $acceptUnknown
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setAcceptUnknown(?FHIRCode $acceptUnknown): CFHIRResourceCapabilityStatement
    {
        $this->acceptUnknown = $acceptUnknown;

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getAcceptUnknown(): ?FHIRCode
    {
        return $this->acceptUnknown;
    }

    /**
     * @param FHIRCode ...$format
     *
     * @return self
     */
    public function setFormat(FHIRCode ...$format): self
    {
        $this->fhirResource->setFormat(...$format);

        return $this;
    }

    /**
     * @param FHIRCode ...$format
     *
     * @return self
     */
    public function addFormat(FHIRCode ...$format): self
    {
        $this->fhirResource->addFormat(...$format);

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getFormat(): array
    {
        return $this->fhirResource->getFormat();
    }

    /**
     * @param FHIRCapabilityStatementRest ...$rest
     *
     * @return self
     */
    public function setRest(FHIRCapabilityStatementRest ...$rest): self
    {
        $this->fhirResource->setRest(...$rest);

        return $this;
    }

    /**
     * @param FHIRCapabilityStatementRest ...$rest
     *
     * @return self
     */
    public function addRest(FHIRCapabilityStatementRest ...$rest): self
    {
        $this->fhirResource->addRest(...$rest);
        return $this;
    }

    /**
     * @return FHIRCapabilityStatementRest[]
     */
    public function getRest(): array
    {
        return $this->fhirResource->getRest();
    }

    /**
     * @return FHIRString|null
     */
    public function getBaseFhirVersion(): ?FHIRString
    {
        return $this->base_fhir_version;
    }

    /**
     * @param FHIRCanonical ...$implementationGuide
     *
     * @return self
     */
    public function setImplementationGuide(FHIRCanonical ...$implementationGuide): self
    {
        $this->fhirResource->setImplementationGuide(...$implementationGuide);

        return $this;
    }

    /**
     * @param FHIRCanonical ...$implementationGuide
     *
     * @return self
     */
    public function addImplementationGuide(FHIRCanonical ...$implementationGuide): self
    {
        $this->fhirResource->addImplementationGuide(...$implementationGuide);

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getImplementationGuide(): array
    {
        return $this->fhirResource->getImplementationGuide();
    }

    /**
     * @param FHIRMarkdown|null $copyright
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setCopyright(?FHIRMarkdown $copyright): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setCopyright($copyright);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->fhirResource->getCopyright();
    }

    /**
     * @param FHIRMarkdown|null $purpose
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setPurpose(?FHIRMarkdown $purpose): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setPurpose($purpose);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->fhirResource->getPurpose();
    }

    /**
     * @param FHIRMarkdown|null $description
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setDescription(?FHIRMarkdown $description): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getDescription(): ?FHIRMarkdown
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @param FHIRBoolean|null $experimental
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setExperimental(?FHIRBoolean $experimental): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setExperimental($experimental);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getExperimental(): ?FHIRBoolean
    {
        return $this->fhirResource->getExperimental();
    }

    /**
     * @param FHIRString|null $title
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setTitle(?FHIRString $title): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setTitle($title);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getTitle(): ?FHIRString
    {
        return $this->fhirResource->getTitle();
    }

    /**
     * @param FHIRString|null $name
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setName(?FHIRString $name): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setName($name);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getName(): ?FHIRString
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRUri|null $url
     *
     * @return CFHIRResourceCapabilityStatement
     */
    public function setUrl(?FHIRUri $url): CFHIRResourceCapabilityStatement
    {
        $this->fhirResource->setUrl($url);

        return $this;
    }

    /**
     * @return FHIRUri|null
     */
    public function getUrl(): ?FHIRUri
    {
        return $this->fhirResource->getUrl();
    }

    /**
     * * Map property url
     */
    protected function mapUrl(): void
    {
        $this->fhirResource->setUrl($this->object_mapping->mapUrl());
    }

    /**
     * * Map property version
     */
    protected function mapVersion(): void
    {
        $this->fhirResource->setVersion($this->object_mapping->mapVersion());
    }

    /**
     * * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName($this->object_mapping->mapName());
    }

    /**
     * * Map property title
     */
    protected function mapTitle(): void
    {
        $this->fhirResource->setTitle($this->object_mapping->mapTitle());
    }

    /**
     * * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * * Map property experimental
     */
    protected function mapExperimental(): void
    {
        $this->fhirResource->setExperimental($this->object_mapping->mapExperimental());
    }

    /**
     * * Map property date
     */
    protected function mapDate(): void
    {
        $this->fhirResource->setDate($this->object_mapping->mapDate());
    }

    /**
     * * Map property publisher
     */
    protected function mapPublisher(): void
    {
        $this->fhirResource->setPublisher($this->object_mapping->mapPublisher());
    }

    /**
     * * Map property contact
     */
    protected function mapContact(): void
    {
        $this->fhirResource->setContact(...$this->object_mapping->mapContact());
    }

    /**
     * * Map property description
     */
    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    /**
     * * Map property useContext
     */
    protected function mapUseContext(): void
    {
        $this->fhirResource->setUseContext(...$this->object_mapping->mapUseContext());
    }

    /**
     * * Map property jurisdiction
     */
    protected function mapJurisdiction(): void
    {
        $this->fhirResource->setJurisdiction(...$this->object_mapping->mapJurisdiction());
    }

    /**
     * * Map property purpose
     */
    protected function mapPurpose(): void
    {
        $this->fhirResource->setPurpose($this->object_mapping->mapPurpose());
    }


    /**
     * * Map property copyright
     */
    protected function mapCopyright(): void
    {
        $this->fhirResource->setCopyright($this->object_mapping->mapCopyright());
    }

    /**
     * * Map property kind
     */
    protected function mapKind(): void
    {
        $this->fhirResource->setKind($this->object_mapping->mapKind());
    }

    /**
     * * Map property instantiates
     */
    protected function mapInstantiates(): void
    {
        $this->fhirResource->setInstantiates(...$this->object_mapping->mapInstantiates());
    }

    /**
     * * Map property imports
     */
    protected function mapImports(): void
    {
        $this->fhirResource->setImports(...$this->object_mapping->mapImports());
    }

    /**
     * * Map property software
     */
    protected function mapSoftware(): void
    {
        $this->fhirResource->setSoftware($this->object_mapping->mapSoftware());
    }

    /**
     * * Map property fhirVersion
     */
    protected function mapFhirVersion(): void
    {
        $this->fhirResource->setFhirVersion($this->object_mapping->mapFhirVersion());
    }

    /**
     * * Map property format
     */
    protected function mapFormat(): void
    {
        $this->fhirResource->setFormat(...$this->object_mapping->mapFormat());
    }

    /**
     * * Map property patchFormat
     */
    protected function mapPatchFormat(): void
    {
        $this->fhirResource->setPatchFormat(...$this->object_mapping->mapPatchFormat());
    }

    /**
     * * Map property implementationGuide
     */
    protected function mapImplementationGuide(): void
    {
        $this->fhirResource->setImplementationGuide(...$this->object_mapping->mapImplementationGuide());
    }

    /**
     * * Map property rest
     */
    protected function mapRest(): void
    {
        $this->fhirResource->setRest(...$this->object_mapping->mapRest());
    }

    /**
     * @param FHIRCode ...$patchFormat
     *
     * @return self
     */
    public function setPatchFormat(FHIRCode ...$patchFormat): self
    {
        $this->fhirResource->setPatchFormat(...$patchFormat);

        return $this;
    }

    /**
     * @param FHIRCode ...$patchFormat
     *
     * @return self
     */
    public function addPatchFormat(FHIRCode ...$patchFormat): self
    {
        $this->fhirResource->addPatchFormat(...$patchFormat);

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getPatchFormat(): array
    {
        return $this->fhirResource->getPatchFormat();
    }

    /**
     * @param FHIRCanonical ...$instantiates
     *
     * @return self
     */
    public function setInstantiates(FHIRCanonical ...$instantiates): self
    {
        $this->fhirResource->setInstantiates(...$instantiates);

        return $this;
    }

    /**
     * @param FHIRCanonical ...$instantiates
     *
     * @return self
     */
    public function addInstantiates(FHIRCanonical ...$instantiates): self
    {
        $this->fhirResource->addInstantiates(...$instantiates);

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getInstantiates(): array
    {
        return $this->fhirResource->getInstantiates();
    }

    /**
     * @param FHIRCanonical ...$imports
     *
     * @return self
     */
    public function setImports(FHIRCanonical ...$imports): self
    {
        $this->fhirResource->setImports(...$imports);

        return $this;
    }

    /**
     * @param FHIRCanonical ...$imports
     *
     * @return self
     */
    public function addImports(FHIRCanonical ...$imports): self
    {
        $this->fhirResource->addImports(...$imports);

        return $this;
    }

    /**
     * @return FHIRCanonical[]
     */
    public function getImports(): array
    {
        return $this->fhirResource->getImports();
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return self
     */
    public function setUseContext(FHIRUsageContext ...$useContext): self
    {
        $this->fhirResource->setUseContext(...$useContext);

        return $this;
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return self
     */
    public function addUseContext(FHIRUsageContext ...$useContext): self
    {
        $this->fhirResource->addUseContext(...$useContext);

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->fhirResource->getUseContext();
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return self
     */
    public function setJurisdiction(FHIRCodeableConcept ...$jurisdiction): self
    {
        $this->fhirResource->setJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return self
     */
    public function addJurisdiction(FHIRCodeableConcept ...$jurisdiction): self
    {
        $this->fhirResource->addJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->fhirResource->getJurisdiction();
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return self
     */
    public function setContact(FHIRContactDetail ...$contact): self
    {
        $this->fhirResource->setContact(...$contact);

        return $this;
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return self
     */
    public function addContact(FHIRContactDetail ...$contact): self
    {
        $this->fhirResource->addContact(...$contact);

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->fhirResource->getContact();
    }

    public function setImplementation(?FHIRCapabilityStatementImplementation $implementation): self
    {
        $this->fhirResource->setImplementation($implementation);

        return $this;
    }

    public function getImplementation(): ?FHIRCapabilityStatementImplementation
    {
        return $this->fhirResource->getImplementation();
    }

    public function mapImplementation(): void
    {
        $this->setImplementation($this->object_mapping->mapImplementation());
    }

    public function setMessaging(FHIRCapabilityStatementMessaging ...$messaging): self
    {
        $this->fhirResource->setMessaging(...$messaging);

        return $this;
    }

    public function addMessaging(FHIRCapabilityStatementMessaging ...$messaging): self
    {
        $this->fhirResource->addMessaging(...$messaging);

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementMessaging[]
     */
    public function getMessaging(): array
    {
        return $this->fhirResource->getMessaging();
    }

    public function mapMessaging(): void
    {
        $this->setMessaging(...$this->object_mapping->mapMessaging());
    }

    public function setDocument(?FHIRCapabilityStatementDocument ...$document): self
    {
        $this->fhirResource->setDocument(...$document);

        return $this;
    }

    public function addDocument(?FHIRCapabilityStatementDocument ...$document): self
    {
        $this->fhirResource->addDocument(...$document);

        return $this;
    }

    /**
     * @return FHIRCapabilityStatementDocument[]
     */
    public function getDocument(): array
    {
        return $this->fhirResource->getDocument();
    }

    public function mapDocument(): void
    {
        $this->setDocument(...$this->object_mapping->mapDocument());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRCapabilityStatement';
    }
}
