<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\OperationOutcome;

use Ox\Interop\Fhir\Contracts\Mapping\R4\OperationOutcomeMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceOperationOutcomeInterface;
use Ox\Components\FhirCore\Interfaces\FHIROperationOutcomeInterface;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIROperationOutcomeIssue;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;

/**
 * FIHR patient resource
 */
class CFHIRResourceOperationOutcome extends CFHIRDomainResource implements ResourceOperationOutcomeInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = "OperationOutcome";

    // attributes
    /** @var FHIROperationOutcomeInterface $fhirResource */
    protected $fhirResource;

    /** @var OperationOutcomeMappingInterface */
    protected $object_mapping;

    /**
     * @param FHIROperationOutcomeIssue ...$issue
     *
     * @return $this
     */
    public function addIssue(FHIROperationOutcomeIssue ...$issue): self
    {
        $this->fhirResource->addIssue(...$issue);

        return $this;
    }

    /**
     * @param FHIROperationOutcomeIssue[] $issue
     *
     * @return CFHIRResourceOperationOutcome
     */
    public function setIssue(FHIROperationOutcomeIssue ...$issue): CFHIRResourceOperationOutcome
    {
        $this->fhirResource->setIssue(...$issue);

        return $this;
    }

    /**
     * @return FHIROperationOutcomeIssue[]
     */
    public function getIssue(): array
    {
        return $this->fhirResource->getIssue();
    }

    protected function mapIssue(): void
    {
        $this->fhirResource->setIssue(...$this->object_mapping->mapIssue());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIROperationOutcome';
    }
}
