<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\ValueSet;

use Ox\Interop\Fhir\Contracts\Mapping\R4\ValueSetMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceValueSetInterface;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRValueSetCompose;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRValueSetExpansion;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Interfaces\FHIRValueSetInterface;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;

/**
 * FIHR patient resource
 */
class CFHIRResourceValueSet extends CFHIRDomainResource implements ResourceValueSetInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "ValueSet";

    /** @var FHIRValueSetInterface $fhirResource */
    protected $fhirResource;

    /** @var ValueSetMappingInterface */
    protected $object_mapping;

    /**
     * Search a coding in the value set among code systems
     *
     * @param string $code_system
     * @param string $code
     *
     * @return FHIRCoding|null
     */
    public function searchCoding(string $code_system, string $code): ?FHIRCoding
    {
        if (!$this->getCompose() || !$this->getCompose()->getInclude()) {
            return null;
        }

        foreach ($this->getCompose()->getInclude() as $include_code_system) {
            if (!$include_code_system->getSystem()->isMatch($code_system) || !$include_code_system->getConcept()) {
                continue;
            }

            foreach ($include_code_system->getConcept() as $concept) {
                if ($concept->getCode() && $concept->getCode()->getValue() === $code) {
                    return (new FHIRCoding())
                        ->setSystem($include_code_system->getSystem()->getValue())
                        ->setCode($concept->getCode()->getValue())
                        ->setDisplay($concept->getDisplay() ? $concept->getDisplay()->getValue() : null)
                        ->setVersion($include_code_system->getVersion() ? $include_code_system->getVersion()->getValue() : null);
                }
            }
        }

        return null;
    }

    /**
     * @param FHIRUri|null $url
     *
     * @return CFHIRResourceValueSet
     */
    public function setUrl(?FHIRUri $url): CFHIRResourceValueSet
    {
        $this->fhirResource->setUrl($url);

        return $this;
    }

    /**
     * @return FHIRUri|null
     */
    public function getUrl(): ?FHIRUri
    {
        return $this->fhirResource->getUrl();
    }

    /**
     * @param FHIRString|null $version
     *
     * @return CFHIRResourceValueSet
     */
    public function setVersion(?FHIRString $version): CFHIRResourceValueSet
    {
        $this->fhirResource->setVersion($version);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getVersion(): ?FHIRString
    {
        return $this->fhirResource->getVersion();
    }

    /**
     * @param FHIRString|null $name
     *
     * @return CFHIRResourceValueSet
     */
    public function setName(?FHIRString $name): CFHIRResourceValueSet
    {
        $this->fhirResource->setName($name);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getName(): ?FHIRString
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRString|null $title
     *
     * @return CFHIRResourceValueSet
     */
    public function setTitle(?FHIRString $title): CFHIRResourceValueSet
    {
        $this->fhirResource->setTitle($title);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getTitle(): ?FHIRString
    {
        return $this->fhirResource->getTitle();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceValueSet
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceValueSet
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRBoolean|null $experimental
     *
     * @return CFHIRResourceValueSet
     */
    public function setExperimental(?FHIRBoolean $experimental): CFHIRResourceValueSet
    {
        $this->fhirResource->setExperimental($experimental);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getExperimental(): ?FHIRBoolean
    {
        return $this->fhirResource->getExperimental();
    }

    /**
     * @param FHIRDateTime|null $date
     *
     * @return CFHIRResourceValueSet
     */
    public function setDate(?FHIRDateTime $date): CFHIRResourceValueSet
    {
        $this->fhirResource->setDate($date);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getDate(): ?FHIRDateTime
    {
        return $this->fhirResource->getDate();
    }

    /**
     * @param FHIRString|null $publisher
     *
     * @return CFHIRResourceValueSet
     */
    public function setPublisher(?FHIRString $publisher): CFHIRResourceValueSet
    {
        $this->fhirResource->setPublisher($publisher);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getPublisher(): ?FHIRString
    {
        return $this->fhirResource->getPublisher();
    }

    /**
     * @param FHIRMarkdown|null $description
     *
     * @return CFHIRResourceValueSet
     */
    public function setDescription(?FHIRMarkdown $description): CFHIRResourceValueSet
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getDescription(): ?FHIRMarkdown
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @param FHIRBoolean|null $immutable
     *
     * @return CFHIRResourceValueSet
     */
    public function setImmutable(?FHIRBoolean $immutable): CFHIRResourceValueSet
    {
        $this->fhirResource->setImmutable($immutable);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getImmutable(): ?FHIRBoolean
    {
        return $this->fhirResource->getImmutable();
    }

    /**
     * @param FHIRMarkdown|null $purpose
     *
     * @return CFHIRResourceValueSet
     */
    public function setPurpose(?FHIRMarkdown $purpose): CFHIRResourceValueSet
    {
        $this->fhirResource->setPurpose($purpose);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->fhirResource->getPurpose();
    }

    /**
     * @param FHIRMarkdown|null $copyright
     *
     * @return CFHIRResourceValueSet
     */
    public function setCopyright(?FHIRMarkdown $copyright): CFHIRResourceValueSet
    {
        $this->fhirResource->setCopyright($copyright);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->fhirResource->getCopyright();
    }

    /**
     * @param FHIRValueSetCompose|null $compose
     *
     * @return CFHIRResourceValueSet
     */
    public function setCompose(?FHIRValueSetCompose $compose): CFHIRResourceValueSet
    {
        $this->fhirResource->setCompose($compose);

        return $this;
    }

    /**
     * @return FHIRValueSetCompose|null
     */
    public function getCompose(): ?FHIRValueSetCompose
    {
        return $this->fhirResource->getCompose();
    }

    /**
     * @param FHIRValueSetExpansion|null $expansion
     *
     * @return CFHIRResourceValueSet
     */
    public function setExpansion(?FHIRValueSetExpansion $expansion): CFHIRResourceValueSet
    {
        $this->fhirResource->setExpansion($expansion);

        return $this;
    }

    /**
     * @return FHIRValueSetExpansion|null
     */
    public function getExpansion(): ?FHIRValueSetExpansion
    {
        return $this->fhirResource->getExpansion();
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return self
     */
    public function setUseContext(FHIRUsageContext ...$useContext): self
    {
        $this->fhirResource->setUseContext(...$useContext);

        return $this;
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return self
     */
    public function addUseContext(FHIRUsageContext ...$useContext): self
    {
        $this->fhirResource->addUseContext(...$useContext);

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->fhirResource->getUseContext();
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return self
     */
    public function setJurisdiction(FHIRCodeableConcept ...$jurisdiction): self
    {
        $this->fhirResource->setJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return self
     */
    public function addJurisdiction(FHIRCodeableConcept ...$jurisdiction): self
    {
        $this->fhirResource->addJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->fhirResource->getJurisdiction();
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return self
     */
    public function setContact(FHIRContactDetail ...$contact): self
    {
        $this->fhirResource->setContact(...$contact);

        return $this;
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return self
     */
    public function addContact(FHIRContactDetail ...$contact): self
    {
        $this->fhirResource->addContact(...$contact);

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->fhirResource->getContact();
    }

    /**
     * * Map property url
     */
    protected function mapUrl(): void
    {
        $this->fhirResource->setUrl($this->object_mapping->mapUrl());
    }

    /**
     * * Map property version
     */
    protected function mapVersion(): void
    {
        $this->fhirResource->setVersion($this->object_mapping->mapVersion());
    }

    /**
     * * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName($this->object_mapping->mapName());
    }

    /**
     * * Map property title
     */
    protected function mapTitle(): void
    {
        $this->fhirResource->setTitle($this->object_mapping->mapTitle());
    }

    /**
     * * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * * Map property experimental
     */
    protected function mapExperimental(): void
    {
        $this->fhirResource->setExperimental($this->object_mapping->mapExperimental());
    }

    /**
     * * Map property date
     */
    protected function mapDate(): void
    {
        $this->fhirResource->setDate($this->object_mapping->mapDate());
    }

    /**
     * * Map property publisher
     */
    protected function mapPublisher(): void
    {
        $this->fhirResource->setPublisher($this->object_mapping->mapPublisher());
    }

    /**
     * * Map property contact
     */
    protected function mapContact(): void
    {
        $this->fhirResource->setContact(...$this->object_mapping->mapContact());
    }

    /**
     * * Map property description
     */
    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    /**
     * * Map property useContext
     */
    protected function mapUseContext(): void
    {
        $this->fhirResource->setUseContext(...$this->object_mapping->mapUseContext());
    }

    /**
     * * Map property jurisdiction
     */
    protected function mapJurisdiction(): void
    {
        $this->fhirResource->setJurisdiction(...$this->object_mapping->mapJurisdiction());
    }

    /**
     * * Map property immutable
     */
    protected function mapImmutable(): void
    {
        $this->fhirResource->setImmutable($this->object_mapping->mapImmutable());
    }

    /**
     * * Map property purpose
     */
    protected function mapPurpose(): void
    {
        $this->fhirResource->setPurpose($this->object_mapping->mapPurpose());
    }

    /**
     * * Map property copyright
     */
    protected function mapCopyright(): void
    {
        $this->fhirResource->setCopyright($this->object_mapping->mapCopyright());
    }

    /**
     * * Map property compose
     */
    protected function mapCompose(): void
    {
        $this->fhirResource->setCompose($this->object_mapping->mapCompose());
    }

    /**
     * * Map property expansion
     */
    protected function mapExpansion(): void
    {
        $this->fhirResource->setExpansion($this->object_mapping->mapExpansion());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRValueSet';
    }
}
