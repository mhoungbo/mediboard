<?php

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\AllergyIntolerance;

use Ox\Interop\Fhir\Contracts\Mapping\R4\AllergyIntoleranceMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceAllergieIntoleranceInterface;
use Ox\Components\FhirCore\Interfaces\Backbone\FHIRAllergyIntoleranceReactionInterface;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Interfaces\FHIRAllergyIntoleranceInterface;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * Description
 *
 * @see http://hl7.org/fhir/allergyintolerance.html
 */
class CFHIRResourceAllergyIntolerance extends CFHIRDomainResource implements ResourceAllergieIntoleranceInterface
{
    /** @var string */
    public const CRITICALITY_LOW = 'low';
    /** @var string */
    public const CRITICALITY_HIGH = 'high';

    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'AllergyIntolerance';

    /** @var FHIRAllergyIntoleranceInterface $fhirResource */
    protected $fhirResource;

    /** @var AllergyIntoleranceMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            );
    }

    /**
     * @param FHIRCodeableConcept|null $clinicalStatus
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setClinicalStatus(?FHIRCodeableConcept $clinicalStatus): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setClinicalStatus($clinicalStatus);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getClinicalStatus(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getClinicalStatus();
    }

    /**
     * @param FHIRCodeableConcept|null $verificationStatus
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setVerificationStatus(
        ?FHIRCodeableConcept $verificationStatus
    ): CFHIRResourceAllergyIntolerance {
        $this->fhirResource->setVerificationStatus($verificationStatus);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getVerificationStatus(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getVerificationStatus();
    }

    /**
     * @param FHIRCode|null $type
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setType(?FHIRCode $type): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setType($type);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getType(): ?FHIRCode
    {
        return $this->fhirResource->getType();
    }

    /**
     * @param FHIRCode ...$category
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setCategory(FHIRCode ...$category): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setCategory(...$category);

        return $this;
    }

    /**
     * @param FHIRCode ...$category
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function addCategory(FHIRCode ...$category): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->addCategory(...$category);

        return $this;
    }

    /**
     * @return FHIRCode[]
     */
    public function getCategory(): array
    {
        return $this->fhirResource->getCategory();
    }

    /**
     * @param FHIRCode|null $criticality
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setCriticality(?FHIRCode $criticality): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setCriticality($criticality);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getCriticality(): ?FHIRCode
    {
        return $this->fhirResource->getCriticality();
    }

    /**
     * @param FHIRCodeableConcept|null $code
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setCode(?FHIRCodeableConcept $code): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setCode($code);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getCode(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getCode();
    }

    /**
     * @param FHIRReference|null $patient
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setPatient(?FHIRReference $patient): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setPatient($patient);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getPatient(): ?FHIRReference
    {
        return $this->fhirResource->getPatient();
    }

    /**
     * @param FHIRReference|null $encounter
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setEncounter(?FHIRReference $encounter): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setEncounter($encounter);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getEncounter(): ?FHIRReference
    {
        return $this->fhirResource->getEncounter();
    }

    /**
     * @param FHIRElement|null $onset
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setOnset(?FHIRElement $onset): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setOnSet($onset);

        return $this;
    }

    /**
     * @return FHIRElement|null
     */
    public function getOnset(): ?FHIRElement
    {
        return $this->fhirResource->getOnSet();
    }

    /**
     * @param FHIRDateTime|null $recordedDate
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setRecordedDate(?FHIRDateTime $recordedDate): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setRecordeddate($recordedDate);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getRecordedDate(): ?FHIRDateTime
    {
        return $this->fhirResource->getRecordedDate();
    }

    /**
     * @param FHIRReference|null $recorder
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setRecorder(?FHIRReference $recorder): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setRecorder($recorder);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getRecorder(): ?FHIRReference
    {
        return $this->fhirResource->getRecorder();
    }

    /**
     * @param FHIRReference|null $asserter
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setAsserter(?FHIRReference $asserter): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setAsserter($asserter);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getAsserter(): ?FHIRReference
    {
        return $this->fhirResource->getAsserter();
    }

    /**
     * @param FHIRDateTime|null $lastOccurrence
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setLastOccurrence(?FHIRDateTime $lastOccurrence): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setLastOccurrence($lastOccurrence);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getLastOccurrence(): ?FHIRDateTime
    {
        return $this->fhirResource->getLastOccurrence();
    }

    /**
     * @param FHIRAnnotation[] $note
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setNote(FHIRAnnotation ...$note): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setNote(...$note);

        return $this;
    }

    /**
     * @param FHIRAnnotation[] $note
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function addNote(FHIRAnnotation ...$note): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->addNote(...$note);

        return $this;
    }

    /**
     * @return FHIRAnnotation[]
     */
    public function getNote(): array
    {
        return $this->fhirResource->getNote();
    }

    /**
     * @param FHIRAllergyIntoleranceReactionInterface[] $reaction
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function setReaction(FHIRAllergyIntoleranceReactionInterface ...$reaction): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->setReaction(...$reaction);

        return $this;
    }

    /**
     * @param FHIRAllergyIntoleranceReactionInterface[] $reaction
     *
     * @return CFHIRResourceAllergyIntolerance
     */
    public function addReaction(FHIRAllergyIntoleranceReactionInterface ...$reaction): CFHIRResourceAllergyIntolerance
    {
        $this->fhirResource->addReaction(...$reaction);

        return $this;
    }

    /**
     * @return FHIRAllergyIntoleranceReactionInterface[]
     */
    public function getReaction(): array
    {
        return $this->fhirResource->getReaction();
    }


    /**
     * Map property clinicalStatus
     */
    protected function mapClinicalStatus(): void
    {
        $this->fhirResource->setClinicalStatus($this->object_mapping->mapClinicalStatus());
    }

    /**
     * Map property verificationStatus
     */
    protected function mapVerificationStatus(): void
    {
        // only confirmed allergy is added
        $this->fhirResource->setVerificationStatus($this->object_mapping->mapVerificationStatus());
    }

    /**
     * Map property type (only 'allergy' is supported)
     */
    protected function mapType(): void
    {
        $this->fhirResource->setType($this->object_mapping->mapType());
    }

    /**
     * Map property
     */
    protected function mapCategory(): void
    {
        $this->fhirResource->addCategory(...$this->object_mapping->mapCategory());
    }

    /**
     * Map property criticality
     */
    protected function mapCriticality(): void
    {
        $this->fhirResource->setCriticality($this->object_mapping->mapCriticality());
    }

    /**
     * Map property code
     */
    protected function mapCode(): void
    {
        $this->fhirResource->setCode($this->object_mapping->mapCode());
    }

    /**
     * Map property patient
     */
    protected function mapPatient(): void
    {
        $this->fhirResource->setPatient($this->object_mapping->mapPatient());
    }

    /**
     * Map property encounter
     */
    protected function mapEncounter(): void
    {
        $this->fhirResource->setEncounter($this->object_mapping->mapEncounter());
    }

    /**
     * Map property onset
     */
    protected function mapOnset(): void
    {
        $this->fhirResource->setOnset($this->object_mapping->mapOnset());
    }

    /**
     * Map property recordedDate
     */
    protected function mapRecordedDate(): void
    {
        $this->fhirResource->setRecordedDate($this->object_mapping->mapRecordedDate());
    }

    /**
     * Map property recorder
     */
    protected function mapRecorder(): void
    {
        $this->fhirResource->setRecorder($this->object_mapping->mapRecorder());
    }

    /**
     * Map property asserter
     */
    protected function mapAsserter(): void
    {
        $this->fhirResource->setAsserter($this->object_mapping->mapAsserter());
    }

    /**
     * Map property lastOccurrence
     */
    protected function mapLastOccurrence(): void
    {
        $this->fhirResource->setLastOccurrence($this->object_mapping->mapLastOccurrence());
    }

    /**
     * Map property note
     */
    protected function mapNote(): void
    {
        $this->fhirResource->setNote(...$this->object_mapping->mapNote());
    }

    /**
     * Map property reaction
     */
    protected function mapReaction(): void
    {
        $this->fhirResource->setReaction($this->object_mapping->mapReaction());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRAllergyIntolerance';
    }
}
