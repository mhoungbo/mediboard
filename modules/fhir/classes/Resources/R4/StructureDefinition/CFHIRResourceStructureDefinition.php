<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\StructureDefinition;

use Ox\Interop\Fhir\Contracts\Mapping\R4\StructureDefinitionMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceStructureDefinitionInterface;
use Ox\Components\FhirCore\Interfaces\FHIRStructureDefinitionInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCanonical;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRStructureDefinitionContext;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRStructureDefinitionDifferential;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRStructureDefinitionMapping;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRStructureDefinitionSnapshot;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;

/**
 * FIHR patient resource
 */
class CFHIRResourceStructureDefinition extends CFHIRDomainResource implements ResourceStructureDefinitionInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = "StructureDefinition";

    /** @var FHIRStructureDefinitionInterface $fhirResource */
    protected $fhirResource;

    /** @var StructureDefinitionMappingInterface */
    protected $object_mapping;

    /**
     * @param FHIRUri|null $url
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setUrl(?FHIRUri $url): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setUrl($url);

        return $this;
    }

    /**
     * @return FHIRUri|null
     */
    public function getUrl(): ?FHIRUri
    {
        return $this->fhirResource->getUrl();
    }

    /**
     * @param FHIRString|null $version
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setVersion(?FHIRString $version): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setVersion($version);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getVersion(): ?FHIRString
    {
        return $this->fhirResource->getVersion();
    }

    /**
     * @param FHIRString|null $name
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setName(?FHIRString $name): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setName($name);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getName(): ?FHIRString
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRString|null $title
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setTitle(?FHIRString $title): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setTitle($title);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getTitle(): ?FHIRString
    {
        return $this->fhirResource->getTitle();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRBoolean|null $experimental
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setExperimental(?FHIRBoolean $experimental): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setExperimental($experimental);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getExperimental(): ?FHIRBoolean
    {
        return $this->fhirResource->getExperimental();
    }

    /**
     * @param FHIRDateTime|null $date
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setDate(?FHIRDateTime $date): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setDate($date);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getDate(): ?FHIRDateTime
    {
        return $this->fhirResource->getDate();
    }

    /**
     * @param FHIRString|null $publisher
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setPublisher(?FHIRString $publisher): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setPublisher($publisher);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getPublisher(): ?FHIRString
    {
        return $this->fhirResource->getPublisher();
    }

    /**
     * @param FHIRMarkdown|null $description
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setDescription(?FHIRMarkdown $description): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getDescription(): ?FHIRMarkdown
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @param FHIRMarkdown|null $purpose
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setPurpose(?FHIRMarkdown $purpose): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setPurpose($purpose);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getPurpose(): ?FHIRMarkdown
    {
        return $this->fhirResource->getPurpose();
    }

    /**
     * @param FHIRMarkdown|null $copyright
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setCopyright(?FHIRMarkdown $copyright): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setCopyright($copyright);

        return $this;
    }

    /**
     * @return FHIRMarkdown|null
     */
    public function getCopyright(): ?FHIRMarkdown
    {
        return $this->fhirResource->getCopyright();
    }

    /**
     * @param FHIRCode|null $fhirVersion
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setFhirVersion(?FHIRCode $fhirVersion): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setFhirVersion($fhirVersion);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getFHIRVersion(): ?FHIRCode
    {
        return $this->fhirResource->getFhirVersion();
    }

    /**
     * @param FHIRCode|null $kind
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setKind(?FHIRCode $kind): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setKind($kind);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getKind(): ?FHIRCode
    {
        return $this->fhirResource->getKind();
    }

    /**
     * @param FHIRBoolean|null $abstract
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setAbstract(?FHIRBoolean $abstract): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setAbstract($abstract);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getAbstract(): ?FHIRBoolean
    {
        return $this->fhirResource->getAbstract();
    }

    /**
     * @param FHIRUri|null $type
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setType(?FHIRUri $type): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setType($type);

        return $this;
    }

    /**
     * @return FHIRUri|null
     */
    public function getType(): ?FHIRUri
    {
        return $this->fhirResource->getType();
    }

    /**
     * @param FHIRCanonical|null $baseDefinition
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setBaseDefinition(?FHIRCanonical $baseDefinition): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setBaseDefinition($baseDefinition);

        return $this;
    }

    /**
     * @return FHIRCanonical|null
     */
    public function getBaseDefinition(): ?FHIRCanonical
    {
        return $this->fhirResource->getBaseDefinition();
    }

    /**
     * @param FHIRCode|null $derivation
     *
     * @return CFHIRResourceStructureDefinition
     */
    public function setDerivation(?FHIRCode $derivation): CFHIRResourceStructureDefinition
    {
        $this->fhirResource->setDerivation($derivation);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getDerivation(): ?FHIRCode
    {
        return $this->fhirResource->getDerivation();
    }

    /**
     * * Map property url
     */
    protected function mapUrl(): void
    {
        $this->fhirResource->setUrl($this->object_mapping->mapUrl());
    }

    /**
     * * Map property version
     */
    protected function mapVersion(): void
    {
        $this->fhirResource->setVersion($this->object_mapping->mapVersion());
    }

    /**
     * * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName($this->object_mapping->mapName());
    }

    /**
     * * Map property title
     */
    protected function mapTitle(): void
    {
        $this->fhirResource->setTitle($this->object_mapping->mapTitle());
    }

    /**
     * * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * * Map property experimental
     */
    protected function mapExperimental(): void
    {
        $this->fhirResource->setExperimental($this->object_mapping->mapExperimental());
    }

    /**
     * * Map property date
     */
    protected function mapDate(): void
    {
        $this->fhirResource->setDate($this->object_mapping->mapDate());
    }

    /**
     * * Map property publisher
     */
    protected function mapPublisher(): void
    {
        $this->fhirResource->setPublisher($this->object_mapping->mapPublisher());
    }

    /**
     * * Map property contact
     */
    protected function mapContact(): void
    {
        $this->fhirResource->setContact(...$this->object_mapping->mapContact());
    }

    /**
     * * Map property description
     */
    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    /**
     * * Map property useContext
     */
    protected function mapUseContext(): void
    {
        $this->fhirResource->setUseContext(...$this->object_mapping->mapUseContext());
    }

    /**
     * * Map property jurisdiction
     */
    protected function mapJurisdiction(): void
    {
        $this->fhirResource->setJurisdiction(...$this->object_mapping->mapJurisdiction());
    }

    /**
     * * Map property purpose
     */
    protected function mapPurpose(): void
    {
        $this->fhirResource->setPurpose($this->object_mapping->mapPurpose());
    }

    /**
     * * Map property copyright
     */
    protected function mapCopyright(): void
    {
        $this->fhirResource->setCopyright($this->object_mapping->mapCopyright());
    }

    /**
     * * Map property keyword
     */
    protected function mapKeyword(): void
    {
        $this->fhirResource->setKeyword(...$this->object_mapping->mapKeyword());
    }

    /**
     * * Map property fhirVersion
     */
    protected function mapFhirVersion(): void
    {
        $this->fhirResource->setFhirVersion($this->object_mapping->mapFhirVersion());
    }

    /**
     * * Map property kind
     */
    protected function mapKind(): void
    {
        $this->fhirResource->setKind($this->object_mapping->mapKind());
    }

    /**
     * * Map property abstract
     */
    protected function mapAbstract(): void
    {
        $this->fhirResource->setAbstract($this->object_mapping->mapAbstract());
    }

    /**
     * * Map property contextInvariant
     */
    protected function mapContextInvariant(): void
    {
        $this->fhirResource->setContextInvariant(...$this->object_mapping->mapContextInvariant());
    }

    /**
     * * Map property type
     */
    protected function mapType(): void
    {
        $this->fhirResource->setType($this->object_mapping->mapType());
    }

    /**
     * * Map property baseDefinition
     */
    protected function mapBaseDefinition(): void
    {
        $this->fhirResource->setBaseDefinition($this->object_mapping->mapBaseDefinition());
    }

    /**
     * * Map property derivation
     */
    protected function mapDerivation(): void
    {
        $this->fhirResource->setDerivation($this->object_mapping->mapDerivation());
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return self
     */
    public function setContact(FHIRContactDetail ...$contact): self
    {
        $this->fhirResource->setContact(...$contact);

        return $this;
    }

    /**
     * @param FHIRContactDetail ...$contact
     *
     * @return self
     */
    public function addContact(FHIRContactDetail ...$contact): self
    {
        $this->fhirResource->addContact(...$contact);

        return $this;
    }

    /**
     * @return FHIRContactDetail[]
     */
    public function getContact(): array
    {
        return $this->fhirResource->getContact();
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return self
     */
    public function setUseContext(FHIRUsageContext ...$useContext): self
    {
        $this->fhirResource->setUseContext(...$useContext);

        return $this;
    }

    /**
     * @param FHIRUsageContext ...$useContext
     *
     * @return self
     */
    public function addUseContext(FHIRUsageContext ...$useContext): self
    {
        $this->fhirResource->addUseContext(...$useContext);

        return $this;
    }

    /**
     * @return FHIRUsageContext[]
     */
    public function getUseContext(): array
    {
        return $this->fhirResource->getUseContext();
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return self
     */
    public function setJurisdiction(FHIRCodeableConcept ...$jurisdiction): self
    {
        $this->fhirResource->setJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$jurisdiction
     *
     * @return self
     */
    public function addJurisdiction(FHIRCodeableConcept ...$jurisdiction): self
    {
        $this->fhirResource->addJurisdiction(...$jurisdiction);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getJurisdiction(): array
    {
        return $this->fhirResource->getJurisdiction();
    }

    /**
     * @param FHIRCoding ...$keyword
     *
     * @return self
     */
    public function setKeyword(FHIRCoding ...$keyword): self
    {
        $this->fhirResource->setKeyword(...$keyword);

        return $this;
    }

    /**
     * @param FHIRCoding ...$keyword
     *
     * @return self
     */
    public function addKeyword(FHIRCoding ...$keyword): self
    {
        $this->fhirResource->addKeyword(...$keyword);

        return $this;
    }

    /**
     * @return FHIRCoding[]
     */
    public function getKeyword(): array
    {
        return $this->fhirResource->getKeyword();
    }

    /**
     * @param FHIRString ...$contextInvariant
     *
     * @return self
     */
    public function setContextInvariant(FHIRString ...$contextInvariant): self
    {
        $this->fhirResource->setContextInvariant(...$contextInvariant);

        return $this;
    }

    /**
     * @param FHIRString ...$contextInvariant
     *
     * @return self
     */
    public function addContextInvariant(FHIRString ...$contextInvariant): self
    {
        $this->fhirResource->addContextInvariant(...$contextInvariant);

        return $this;
    }

    /**
     * @return FHIRString[]
     */
    public function getContextInvariant(): array
    {
        return $this->fhirResource->getContextInvariant();
    }

    public function setContext(FHIRStructureDefinitionContext ...$context): self
    {
        $this->fhirResource->setContext(...$context);

        return $this;
    }

    public function addContext(FHIRStructureDefinitionContext ...$context): self
    {
        $this->fhirResource->addContext(...$context);

        return $this;
    }

    /**
     * @return FHIRStructureDefinitionContext[]
     */
    public function getContext(): array
    {
        return $this->fhirResource->getContext();
    }

    public function mapContext(): void
    {
        $this->setContext(...$this->object_mapping->mapContext());
    }

    public function setMapping(FHIRStructureDefinitionMapping ...$mapping): self
    {
        $this->fhirResource->setMapping(...$mapping);

        return $this;
    }

    public function addMapping(FHIRStructureDefinitionMapping ...$mapping): self
    {
        $this->fhirResource->addMapping(...$mapping);

        return $this;
    }

    /**
     * @return FHIRStructureDefinitionMapping[]
     */
    public function getMapping(): array
    {
        return $this->fhirResource->getMapping();
    }

    public function mapMapping(): void
    {
        $this->setMapping(...$this->object_mapping->mapMapping());
    }

    public function setSnapshot(?FHIRStructureDefinitionSnapshot $snapshot): self
    {
        $this->fhirResource->setSnapshot($snapshot);

        return $this;
    }

    public function getSnapshot(): ?FHIRStructureDefinitionSnapshot
    {
        return $this->fhirResource->getSnapshot();
    }

    public function mapSnapshot(): void
    {
        $this->setSnapshot($this->object_mapping->mapSnapshot());
    }

    public function setDifferential(?FHIRStructureDefinitionDifferential $differential): self
    {
        $this->fhirResource->setDifferential($differential);

        return $this;
    }

    public function getDifferential(): ?FHIRStructureDefinitionDifferential
    {
        return $this->fhirResource->getDifferential();
    }

    public function mapDifferential(): void
    {
        $this->setDifferential($this->object_mapping->mapDifferential());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRStructureDefinition';
    }
}
