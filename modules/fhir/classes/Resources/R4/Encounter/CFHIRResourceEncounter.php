<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Encounter;

use Ox\Interop\Fhir\Contracts\Mapping\R4\EncounterMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceEncounterInterface;
use Ox\Components\FhirCore\Interfaces\FHIREncounterInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRDuration;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterClassHistory;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterDiagnosis;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterHospitalization;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterLocation;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterParticipant;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterStatusHistory;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * FHIR encounter resource
 */
class CFHIRResourceEncounter extends CFHIRDomainResource implements ResourceEncounterInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Encounter';

    /** @var FHIREncounterInterface $fhirResource */
    protected $fhirResource;

    /** @var EncounterMappingInterface */
    protected $object_mapping;


    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionCreate::NAME,
                ]
            );
    }

    /**
     * @return FHIRPeriod|null
     */
    public function getPeriod(): ?FHIRPeriod
    {
        return $this->fhirResource->getPeriod();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceEncounter
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceEncounter
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIREncounterStatusHistory ...$statusHistory
     *
     * @return CFHIRResourceEncounter
     */
    public function setStatusHistory(FHIREncounterStatusHistory ...$statusHistory): CFHIRResourceEncounter
    {
        $this->fhirResource->setStatusHistory(...$statusHistory);

        return $this;
    }

    /**
     * @param FHIREncounterStatusHistory ...$statusHistory
     *
     * @return CFHIRResourceEncounter
     */
    public function addStatusHistory(FHIREncounterStatusHistory ...$statusHistory): CFHIRResourceEncounter
    {
        $this->fhirResource->addStatusHistory(...$statusHistory);

        return $this;
    }

    /**
     * @return FHIREncounterStatusHistory[]
     */
    public function getStatusHistory(): array
    {
        return $this->fhirResource->getStatusHistory();
    }

    /**
     * @param FHIRCoding|null $class
     *
     * @return CFHIRResourceEncounter
     */
    public function setClass(?FHIRCoding $class): CFHIRResourceEncounter
    {
        $this->fhirResource->setClass($class);

        return $this;
    }

    /**
     * @return FHIRCoding|null
     */
    public function getClass(): ?FHIRCoding
    {
        return $this->fhirResource->getClass();
    }

    /**
     * @param FHIRCodeableConcept ...$type
     *
     * @return CFHIRResourceEncounter
     */
    public function setType(FHIRCodeableConcept ...$type): CFHIRResourceEncounter
    {
        $this->fhirResource->setType(...$type);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$type
     *
     * @return CFHIRResourceEncounter
     */
    public function addType(FHIRCodeableConcept ...$type): CFHIRResourceEncounter
    {
        $this->fhirResource->addType(...$type);

        return $this;
    }

    /**
     * @return array
     */
    public function getType(): array
    {
        return $this->fhirResource->getType();
    }

    /**
     * @param FHIRCodeableConcept|null $serviceType
     *
     * @return CFHIRResourceEncounter
     */
    public function setServiceType(?FHIRCodeableConcept $serviceType): CFHIRResourceEncounter
    {
        $this->fhirResource->setServiceType($serviceType);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getServiceType(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getServiceType();
    }

    /**
     * @param FHIRCodeableConcept|null $priority
     *
     * @return CFHIRResourceEncounter
     */
    public function setPriority(?FHIRCodeableConcept $priority): CFHIRResourceEncounter
    {
        $this->fhirResource->setPriority($priority);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getPriority(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getPriority();
    }

    /**
     * @param FHIRReference|null $subject
     *
     * @return CFHIRResourceEncounter
     */
    public function setSubject(?FHIRReference $subject): CFHIRResourceEncounter
    {
        $this->fhirResource->setSubject($subject);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getSubject(): ?FHIRReference
    {
        return $this->fhirResource->getSubject();
    }

    /**
     * @param FHIRReference ...$episodeOfCare
     *
     * @return CFHIRResourceEncounter
     */
    public function setEpisodeOfCare(FHIRReference ...$episodeOfCare): CFHIRResourceEncounter
    {
        $this->fhirResource->setEpisodeOfCare(...$episodeOfCare);

        return $this;
    }

    /**
     * @param FHIRReference ...$episodeOfCare
     *
     * @return CFHIRResourceEncounter
     */
    public function addEpisodeOfCare(FHIRReference ...$episodeOfCare): CFHIRResourceEncounter
    {
        $this->fhirResource->addEpisodeOfCare(...$episodeOfCare);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getEpisodeOfCare(): array
    {
        return $this->fhirResource->getEpisodeOfCare();
    }

    /**
     * @param FHIRReference ...$basedOn
     *
     * @return CFHIRResourceEncounter
     */
    public function setBasedOn(FHIRReference ...$basedOn): CFHIRResourceEncounter
    {
        $this->fhirResource->setBasedOn(...$basedOn);

        return $this;
    }

    /**
     * @param FHIRReference ...$basedOn
     *
     * @return CFHIRResourceEncounter
     */
    public function addBasedOn(FHIRReference ...$basedOn): CFHIRResourceEncounter
    {
        $this->fhirResource->addBasedOn(...$basedOn);

        return $this;
    }

    /**
     * @return FHIRReference[]
     */
    public function getBasedOn(): array
    {
        return $this->fhirResource->getBasedOn();
    }

    /**
     * @param FHIREncounterParticipant ...$participant
     *
     * @return CFHIRResourceEncounter
     */
    public function setParticipant(FHIREncounterParticipant ...$participant): CFHIRResourceEncounter
    {
        $this->fhirResource->setParticipant(...$participant);

        return $this;
    }

    /**
     * @param FHIREncounterParticipant ...$participant
     *
     * @return CFHIRResourceEncounter
     */
    public function addParticipant(FHIREncounterParticipant ...$participant): CFHIRResourceEncounter
    {
        $this->fhirResource->addParticipant(...$participant);

        return $this;
    }

    /**
     * @return array
     */
    public function getParticipant(): array
    {
        return $this->fhirResource->getParticipant();
    }

    /**
     * @param FHIRReference ...$appointment
     *
     * @return CFHIRResourceEncounter
     */
    public function setAppointment(FHIRReference ...$appointment): CFHIRResourceEncounter
    {
        $this->fhirResource->setAppointment(...$appointment);

        return $this;
    }

    /**
     * @param FHIRReference ...$appointment
     *
     * @return CFHIRResourceEncounter
     */
    public function addAppointment(FHIRReference ...$appointment): CFHIRResourceEncounter
    {
        $this->fhirResource->addAppointment(...$appointment);

        return $this;
    }

    /**
     * @return array
     */
    public function getAppointment(): array
    {
        return $this->fhirResource->getAppointment();
    }

    /**
     * @param FHIRPeriod|null $period
     *
     * @return CFHIRResourceEncounter
     */
    public function setPeriod(?FHIRPeriod $period): CFHIRResourceEncounter
    {
        $this->fhirResource->setPeriod($period);

        return $this;
    }

    /**
     * @param FHIRDuration|null $length
     *
     * @return CFHIRResourceEncounter
     */
    public function setLength(?FHIRDuration $length): CFHIRResourceEncounter
    {
        $this->fhirResource->setLength($length);

        return $this;
    }

    /**
     * @return FHIRDuration|null
     */
    public function getLength(): ?FHIRDuration
    {
        return $this->fhirResource->getLength();
    }

    /**
     * @param FHIRCodeableConcept ...$reasonCode
     *
     * @return CFHIRResourceEncounter
     */
    public function setReasonCode(FHIRCodeableConcept ...$reasonCode): CFHIRResourceEncounter
    {
        $this->fhirResource->setReasonCode(...$reasonCode);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$reasonCode
     *
     * @return CFHIRResourceEncounter
     */
    public function addReasonCode(FHIRCodeableConcept ...$reasonCode): CFHIRResourceEncounter
    {
        $this->fhirResource->addReasonCode(...$reasonCode);

        return $this;
    }

    /**
     * @return array
     */
    public function getReasonCode(): array
    {
        return $this->fhirResource->getReasonCode();
    }

    /**
     * @param FHIRReference ...$reasonReference
     *
     * @return CFHIRResourceEncounter
     */
    public function setReasonReference(FHIRReference ...$reasonReference): CFHIRResourceEncounter
    {
        $this->fhirResource->setReasonReference(...$reasonReference);

        return $this;
    }

    /**
     * @param FHIRReference ...$reasonReference
     *
     * @return CFHIRResourceEncounter
     */
    public function addReasonReference(FHIRReference ...$reasonReference): CFHIRResourceEncounter
    {
        $this->fhirResource->addReasonReference(...$reasonReference);

        return $this;
    }

    /**
     * @return array
     */
    public function getReasonReference(): array
    {
        return $this->fhirResource->getReasonReference();
    }

    /**
     * @param FHIREncounterDiagnosis ...$diagnosis
     *
     * @return CFHIRResourceEncounter
     */
    public function setDiagnosis(FHIREncounterDiagnosis ...$diagnosis): CFHIRResourceEncounter
    {
        $this->fhirResource->setDiagnosis(...$diagnosis);

        return $this;
    }

    /**
     * @param FHIREncounterDiagnosis ...$diagnosis
     *
     * @return CFHIRResourceEncounter
     */
    public function addDiagnosis(FHIREncounterDiagnosis ...$diagnosis): CFHIRResourceEncounter
    {
        $this->fhirResource->addDiagnosis(...$diagnosis);

        return $this;
    }

    /**
     * @return array
     */
    public function getDiagnosis(): array
    {
        return $this->fhirResource->getDiagnosis();
    }


    /**
     * @return array
     */
    protected function mapDiagnosis(): void
    {
        $this->fhirResource->setDiagnosis(...$this->object_mapping->mapDiagnosis());
    }


    /**
     * @param FHIRReference ...$account
     *
     * @return CFHIRResourceEncounter
     */
    public function setAccount(FHIRReference ...$account): CFHIRResourceEncounter
    {
        $this->fhirResource->setAccount(...$account);

        return $this;
    }

    /**
     * @param FHIRReference ...$account
     *
     * @return CFHIRResourceEncounter
     */
    public function addAccount(FHIRReference ...$account): CFHIRResourceEncounter
    {
        $this->fhirResource->addAccount(...$account);

        return $this;
    }

    /**
     * @return array
     */
    public function getAccount(): array
    {
        return $this->fhirResource->getAccount();
    }

    /**
     * @param FHIREncounterHospitalization|null $hospitalization
     *
     * @return CFHIRResourceEncounter
     */
    public function setHospitalization(?FHIREncounterHospitalization $hospitalization): CFHIRResourceEncounter
    {
        $this->fhirResource->setHospitalization($hospitalization);

        return $this;
    }

    /**
     * @return FHIREncounterHospitalization|null
     */
    public function getHospitalization(): ?FHIREncounterHospitalization
    {
        return $this->fhirResource->getHospitalization();
    }

    /**
     * Map property Hospitalization
     */
    protected function mapHospitalization(): void
    {
        $this->fhirResource->setHospitalization($this->object_mapping->mapHospitalization());
    }

    /**
     * @param FHIREncounterLocation ...$location
     *
     * @return CFHIRResourceEncounter
     */
    public function setLocation(FHIREncounterLocation ...$location): CFHIRResourceEncounter
    {
        $this->fhirResource->setLocation(...$location);

        return $this;
    }

    /**
     * @param FHIREncounterLocation ...$location
     *
     * @return CFHIRResourceEncounter
     */
    public function addLocation(FHIREncounterLocation ...$location): CFHIRResourceEncounter
    {
        $this->fhirResource->addLocation(...$location);

        return $this;
    }

    /**
     * @param FHIRReference|null $serviceProvider
     *
     * @return CFHIRResourceEncounter
     */
    public function setServiceProvider(?FHIRReference $serviceProvider): CFHIRResourceEncounter
    {
        $this->fhirResource->setServiceProvider($serviceProvider);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getServiceProvider(): ?FHIRReference
    {
        return $this->fhirResource->getServiceProvider();
    }

    /**
     * @param FHIRReference|null $partOf
     *
     * @return CFHIRResourceEncounter
     */
    public function setPartOf(?FHIRReference $partOf): CFHIRResourceEncounter
    {
        $this->fhirResource->setPartOf($partOf);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getPartOf(): ?FHIRReference
    {
        return $this->fhirResource->getPartOf();
    }

    /**
     * @param FHIREncounterClassHistory ...$classHistory
     *
     * @return CFHIRResourceEncounter
     */
    public function setClassHistory(FHIREncounterClassHistory ...$classHistory): CFHIRResourceEncounter
    {
        $this->fhirResource->setClassHistory(...$classHistory);

        return $this;
    }

    /**
     * @param FHIREncounterClassHistory ...$classHistory
     *
     * @return CFHIRResourceEncounter
     */
    public function addClassHistory(FHIREncounterClassHistory ...$classHistory): CFHIRResourceEncounter
    {
        $this->fhirResource->addClassHistory(...$classHistory);

        return $this;
    }

    /**
     * @return array
     */
    public function getClassHistory(): array
    {
        return $this->fhirResource->getClassHistory();
    }

    /**
     * @return array
     */
    protected function mapClassHistory(): void
    {
        $this->fhirResource->setClassHistory(...$this->object_mapping->mapClassHistory());
    }

    /**
     * @return array
     */
    public function getLocation(): array
    {
        return $this->fhirResource->getLocation();
    }

    /**
     * Map property Location
     */
    protected function mapLocation(): void
    {
        $this->fhirResource->setLocation(...$this->object_mapping->mapLocation());
    }

    /**
     * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * Map property status
     */
    protected function mapStatusHistory(): void
    {
        $this->fhirResource->setStatusHistory(...$this->object_mapping->mapStatusHistory());
    }

    /**
     * Map property class
     */
    protected function mapClass(): void
    {
        $this->fhirResource->setClass($this->object_mapping->mapClass());
    }

    /**
     * Map property type
     */
    protected function mapType(): void
    {
        $this->fhirResource->setType(...$this->object_mapping->mapType());
    }

    /**
     * Map property serviceType
     */
    protected function mapServiceType(): void
    {
        $this->fhirResource->setServiceType($this->object_mapping->mapServiceType());
    }

    /**
     * Map property priority
     */
    protected function mapPriority(): void
    {
        $this->fhirResource->setPriority($this->object_mapping->mapPriority());
    }

    /**
     * Map property subject
     */
    protected function mapSubject(): void
    {
        $this->fhirResource->setSubject($this->object_mapping->mapSubject());
    }

    /**
     * Map property episodeOfCare
     */
    protected function mapEpisodeOfCare(): void
    {
        $this->fhirResource->setEpisodeOfCare(...$this->object_mapping->mapEpisodeOfCare());
    }

    /**
     * Map property basedOn
     */
    protected function mapBasedOn(): void
    {
        $this->fhirResource->setBasedOn(...$this->object_mapping->mapBasedOn());
    }

    /**
     * Map property participant
     */
    protected function mapParticipant(): void
    {
        $this->fhirResource->setParticipant(...$this->object_mapping->mapParticipant());
    }

    /**
     * Map property appointment
     */
    protected function mapAppointment(): void
    {
        $this->fhirResource->setAppointment(...$this->object_mapping->mapAppointment());
    }

    /**
     * Map property period
     */
    protected function mapPeriod(): void
    {
        $this->fhirResource->setPeriod($this->object_mapping->mapPeriod());
    }

    /**
     * Map property length
     */
    protected function mapLength(): void
    {
        $this->fhirResource->setLength($this->object_mapping->mapLength());
    }

    /**
     * Map property reasonCode
     */
    protected function mapReasonCode(): void
    {
        $this->fhirResource->setReasonCode(...$this->object_mapping->mapReasonCode());
    }

    /**
     * Map property reasonReference
     */
    protected function mapReasonReference(): void
    {
        $this->fhirResource->setReasonReference(...$this->object_mapping->mapReasonReference());
    }

    /**
     * Map property account
     */
    protected function mapAccount(): void
    {
        $this->fhirResource->setAccount(...$this->object_mapping->mapAccount());
    }

    /**
     * Map property serviceProvider
     */
    protected function mapServiceProvider(): void
    {
        $this->fhirResource->setServiceProvider($this->object_mapping->mapServiceProvider());
    }

    /**
     * Map property partOf
     */
    protected function mapPartOf(): void
    {
        $this->fhirResource->setPartOf($this->object_mapping->mapPartOf());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIREncounter';
    }
}
