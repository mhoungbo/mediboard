<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Encounter\Mapper;

use Exception;
use Ox\Interop\Eai\CDomain;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\EncounterMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRDuration;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterHospitalization;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterParticipant;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Encounter\CFHIRResourceEncounter;
use Ox\Interop\Fhir\Utilities\Helper\SejourHelper;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

/**
 * Description
 */
class Encounter implements DelegatedObjectMapperInterface, EncounterMappingInterface
{
    use CStoredObjectResourceDomainTrait {
        mapIdentifier as protected mapIdentifierTrait;
    }

    /** @var CSejour */
    protected $object;

    /** @var CFHIRResourceEncounter */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceEncounter::class];
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CSejour && $object->_id;
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function mapIdentifier(): array
    {
        $identifiers = $this->mapIdentifierTrait();
        $domains     = CDomain::loadDomainIdentifiers($this->object);

        /** @var CDomain $_domain */
        foreach ($domains as $_domain) {
            if (empty($sejour->_returned_oids) || in_array($_domain->OID, $sejour->_returned_oids)) {
                $identifier = (new FHIRIdentifier())
                    ->setSystem("urn:oid:$_domain->OID")
                    ->setValue($_domain->_identifier->id400);

                if ($_domain->isMaster()) {
                    $identifier->setType((new FHIRCodeableConcept())->setCoding(SejourHelper::getTypeCodingNDA()), 'oui');
                }

                $identifiers[] = $identifier;
            }
        }

        return $identifiers;
    }

    public function mapStatus(): ?FHIRCode
    {
        if ($this->object->annule) {
            return (new FHIRCode())->setValue('cancelled');
        }

        switch ($this->object->_etat) {
            case "preadmission":
                return (new FHIRCode())->setValue('planned');
            case "encours":
                return (new FHIRCode())->setValue('in-progress');
            case "cloture":
                return (new FHIRCode())->setValue('finished');
            default:
                return null;
        }
    }

    public function mapType(): array
    {
        return [];
    }

    public function mapSubject(): ?FHIRReference
    {
        return $this->resource->addReference(get_class(new CPatient()), $this->object->loadRefPatient());
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapParticipant(): array
    {
        return [
            (new FHIREncounterParticipant())
            ->setType(
                (new FHIRCodeableConcept())
                    ->setCoding(
                        (new FHIRCoding())
                            ->setCode('ADM')))
            ->setIndividual(
                $this->resource->addReference(
                    get_class(new CMedecin()),
                    $this->object->loadRefMedecinTraitant()
                )
            )
        ];
    }

    /**
     * @throws Exception
     */
    public function mapPeriod(): ?FHIRPeriod
    {
        return (new FHIRPeriod())
            ->setStart(CFHIR::getTimeUtc($this->object->entree, false))
            ->setEnd(CFHIR::getTimeUtc($this->object->sortie, false));
    }

    /**
     * @inheritDoc
     */
    public function mapClass(): ?FHIRCoding
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapServiceType(): ?FHIRCodeableConcept
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapPriority(): ?FHIRCodeableConcept
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapEpisodeOfCare(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapBasedOn(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapAppointment(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapLength(): ?FHIRDuration
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapReasonCode(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapReasonReference(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapAccount(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapServiceProvider(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapPartOf(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapStatusHistory(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapClassHistory(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapDiagnosis(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapHospitalization(): ?FHIREncounterHospitalization
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapLocation(): array
    {
        return [];
    }
}
