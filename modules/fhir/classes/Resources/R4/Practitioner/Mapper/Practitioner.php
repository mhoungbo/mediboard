<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Practitioner\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\PractitionerMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPractitionerQualification;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Mediboard\Patients\CMedecin;

/**
 * Description
 */
class Practitioner implements DelegatedObjectMapperInterface, PractitionerMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CMedecin */
    protected $object;

    /** @var CFHIRResourcePractitioner */
    protected CFHIRResource $resource;

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return void
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    public function onlyProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourcePractitioner::class];
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CMedecin && $object->_id;
    }

    public function mapActive(): ?FHIRBoolean
    {
        return (new FHIRBoolean())->setValue($this->object->actif);
    }

    public function mapName(): array
    {
        return [
            (new FHIRHumanName())
                ->setFamily($this->object->nom)
                ->setGiven($this->object->prenom)
                ->setUse('usual')
                ->setText($this->object->_view),
        ];
    }

    public function mapTelecom(): array
    {
        $telecoms = [];
        if ($this->object->tel) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel);
        }

        if ($this->object->tel_autre) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel_autre);
        }

        if ($this->object->fax) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('fax')
                ->setValue($this->object->fax);
        }

        if ($this->object->portable) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->portable);
        }

        if ($this->object->email) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('email')
                ->setValue($this->object->email);
        }

        return $telecoms;
    }

    public function mapAddress(): array
    {
        if ($this->object->adresse || $this->object->ville || $this->object->cp) {
            return [
                (new FHIRAddress())
                    ->setUse('work')
                    ->setType('postal')
                    ->setLine(...preg_split('/[\r\n]+/', $this->object->adresse) ?? null)
                    ->setCity($this->object->ville ?? null)
                    ->setPostalCode($this->object->cp ?? null)
            ];
        }

        return [];
    }

    public function mapGender(): ?FHIRCode
    {
        return (new FHIRCode())->setValue($this->resource->formatGender($this->object->sexe));
    }

    public function mapBirthDate(): ?FHIRDate
    {
        return null;
    }

    public function mapPhoto(): array
    {
        return [];
    }

    public function mapQualification(): array
    {
        $system  = 'urn:oid:2.16.840.1.113883.18.220';
        $code    = 'MD';
        $display = 'Doctor of Medicine';
        $text    = 'Doctor of Medicine';

        $coding = (new FHIRCoding())
            ->setSystem($system)
            ->setCode($code)
            ->setDisplay($display);

        return [
            (new FHIRPractitionerQualification())
                ->setIdentifier(null)
                ->setPeriod(null)
                ->setIssuer(null)
                ->setCode(
                    (new FHIRCodeableConcept())
                        ->setCoding($coding)
                        ->setText($text)
                )
        ];
    }

    public function mapCommunication(): array
    {
        $system  = 'urn:ietf:bcp:47';
        $code    = 'fr-FR';
        $display = 'French (France)';
        $text    = 'Fran�ais de France';

        return [
            (new FHIRCodeableConcept())
                ->setText($text)
                ->setCoding(
                    (new FHIRCoding())
                        ->setSystem($system)
                        ->setCode($code)
                        ->setDisplay($display)
                )
        ];
    }
}
