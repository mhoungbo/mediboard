<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Practitioner\Mapper;

use Ox\Core\CMbArray;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;

use Ox\Interop\Fhir\Profiles\CFHIRInteropSante;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CMedecinExercicePlace;

/**
 * Description
 */
class FrPractitioner extends Practitioner
{
    /** @var CMedecin */
    protected $object;

    /** @var CFHIRResourcePractitionerFR */
    protected CFHIRResource $resource;

    public function onlyProfiles(): array
    {
        return [CFHIRInteropSante::class];
    }

    public function mapIdentifier(): array
    {
        $identifiers = parent::mapIdentifier();

        /** @var CMedecinExercicePlace[] $medecin_exercice_places */
        $medecin_exercice_places = $this->object->getMedecinExercicePlaces();
        $adeli                   = array_unique(CMbArray::pluck($medecin_exercice_places, "adeli"))[0];

        // ADELI
        if ($adeli || $this->object->adeli) {
            $coding = (new FHIRCoding())
                ->setCode('ADELI')
                ->setDisplay('N� ADELI')
                ->setSystem('http://interopsante.org/fhir/CodeSystem/fr-v2-0203');
            $type   = (new FHIRCodeableConcept())
                ->setCoding($coding)
                ->setText('N� ADELI');


            $identifiers[] = (new FHIRIdentifier())
                ->setValue($adeli ?? $this->object->adeli)
                ->setSystem('urn:oid:1.2.250.1.71.4.2.1')
                ->setUse('official')
                ->setType($type);
        }

        // RPPS
        if ($this->object->rpps) {
            $coding = (new FHIRCoding())
                ->setCode('RPPS')
                ->setSystem('http://interopsante.org/fhir/CodeSystem/fr-v2-0203')
                ->setDisplay('N� RPPS');
            $type   = (new FHIRCodeableConcept())
                ->setCoding($coding)
                ->setText('N� RPPS');

            $identifiers[] = (new FHIRIdentifier())
                ->setValue($this->object->rpps)
                ->setSystem('urn:oid:1.2.250.1.71.4.2.1')
                ->setType($type)
                ->setUse('official');
        }

        return $identifiers;
    }

    public function mapName(): array
    {
        // TODO PASSER EN FrHumanName

        return [
            (new FHIRHumanName())
            ->setFamily($this->object->nom)
            ->setGiven($this->object->prenom)
            ->setUse('usual')
            ->setText($this->object->_view)
        ];
    }

    public function mapTelecom(): array
    {
        // TODO PASSER EN FrContactPoint
        $telecoms = [];
        if ($this->object->tel) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel);
        }

        if ($this->object->tel_autre) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('phone')
                ->setValue($this->object->tel_autre);
        }

        if ($this->object->fax) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('fax')
                ->setValue($this->object->fax);
        }

        if ($this->object->portable) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('portable')
                ->setValue($this->object->portable);
        }

        if ($this->object->email) {
            $telecoms[] = (new FHIRContactPoint())
                ->setSystem('email')
                ->setValue($this->object->email);
        }

        return $telecoms;
    }

    public function mapAddress(): array
    {
        // TODO PASSER EN FrAddress
        if ($this->object->adresse || $this->object->ville || $this->object->cp) {
            return [
                (new FHIRAddress())
                    ->setUse('work')
                    ->setType('postal')
                    ->setLine(...preg_split('/[\r\n]+/', $this->object->adresse) ?? null)
                    ->setCity($this->object->ville ?? null)
                    ->setPostalCode($this->object->cp ?? null),
            ];
        }

        return [];
    }

    public function mapQualification(): array
    {
        // TODO Mapper avec les sp�cialit�s fran�aise
        return parent::mapQualification();
    }
}
