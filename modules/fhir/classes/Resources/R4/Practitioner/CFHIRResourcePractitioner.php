<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Practitioner;

use Ox\Interop\Fhir\Contracts\Mapping\R4\PractitionerMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourcePractitionerInterface;
use Ox\Components\FhirCore\Interfaces\FHIRPractitionerInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPractitionerQualification;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterString;

/**
 * FHIR practitioner resource
 */
class CFHIRResourcePractitioner extends CFHIRDomainResource implements ResourcePractitionerInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Practitioner';

    // attributes
    /** @var FHIRPractitionerInterface $fhirResource */
    protected $fhirResource;
    
    /** @var PractitionerMappingInterface */
    protected $object_mapping;

    /**
     * @param FHIRBoolean|null $active
     *
     * @return CFHIRResourcePractitioner
     */
    public function setActive(?FHIRBoolean $active): CFHIRResourcePractitioner
    {
        $this->fhirResource->setActive($active);

        return $this;
    }

    /**
     * @return FHIRBoolean|null
     */
    public function getActive(): ?FHIRBoolean
    {
        return $this->fhirResource->getActive();
    }

    /**
     * @param FHIRHumanName ...$name
     *
     * @return CFHIRResourcePractitioner
     */
    public function setName(FHIRHumanName ...$name): CFHIRResourcePractitioner
    {
        $this->fhirResource->setName(...$name);

        return $this;
    }

    /**
     * @param FHIRHumanName ...$name
     *
     * @return CFHIRResourcePractitioner
     */
    public function addName(FHIRHumanName ...$name): CFHIRResourcePractitioner
    {
        $this->fhirResource->addName(...$name);

        return $this;
    }

    /**
     * @return FHIRHumanName[]
     */
    public function getName(): array
    {
        return $this->fhirResource->getName();
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return CFHIRResourcePractitioner
     */
    public function setTelecom(FHIRContactPoint ...$telecom): CFHIRResourcePractitioner
    {
        $this->fhirResource->setTelecom(...$telecom);

        return $this;
    }

    /**
     * @param FHIRContactPoint ...$telecom
     *
     * @return CFHIRResourcePractitioner
     */
    public function addTelecom(FHIRContactPoint ...$telecom): CFHIRResourcePractitioner
    {
        $this->fhirResource->addTelecom(...$telecom);

        return $this;
    }

    /**
     * @return FHIRContactPoint[]
     */
    public function getTelecom(): array
    {
        return $this->fhirResource->getTelecom();
    }

    /**
     * @param FHIRAddress ...$address
     *
     * @return CFHIRResourcePractitioner
     */
    public function setAddress(FHIRAddress ...$address): CFHIRResourcePractitioner
    {
        $this->fhirResource->setAddress(...$address);

        return $this;
    }


    /**
     * @param FHIRAddress ...$address
     *
     * @return CFHIRResourcePractitioner
     */
    public function addAddress(FHIRAddress ...$address): CFHIRResourcePractitioner
    {
        $this->fhirResource->addAddress(...$address);

        return $this;
    }

    /**
     * @return FHIRAddress[]
     */
    public function getAddress(): array
    {
        return $this->fhirResource->getAddress();
    }

    /**
     * @param FHIRCode|null $gender
     *
     * @return CFHIRResourcePractitioner
     */
    public function setGender(?FHIRCode $gender): CFHIRResourcePractitioner
    {
        $this->fhirResource->setGender($gender);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getGender(): ?FHIRCode
    {
        return $this->fhirResource->getGender();
    }

    /**
     * @param FHIRDate|null $birthDate
     *
     * @return CFHIRResourcePractitioner
     */
    public function setBirthDate(?FHIRDate $birthDate): CFHIRResourcePractitioner
    {
        $this->fhirResource->setBirthDate($birthDate);

        return $this;
    }

    /**
     * @return FHIRDate|null
     */
    public function getBirthDate(): ?FHIRDate
    {
        return $this->fhirResource->getBirthDate();
    }

    /**
     * @param FHIRAttachment ...$photo
     *
     * @return CFHIRResourcePractitioner
     */
    public function setPhoto(FHIRAttachment ...$photo): CFHIRResourcePractitioner
    {
        $this->fhirResource->setPhoto(...$photo);

        return $this;
    }

    /**
     * @param FHIRAttachment ...$photo
     *
     * @return CFHIRResourcePractitioner
     */
    public function addPhoto(FHIRAttachment ...$photo): CFHIRResourcePractitioner
    {
        $this->fhirResource->addPhoto(...$photo);

        return $this;
    }

    /**
     * @return FHIRAttachment[]
     */
    public function getPhoto(): array
    {
        return $this->fhirResource->getPhoto();
    }

    /**
     * @param FHIRPractitionerQualification ...$qualification
     *
     * @return CFHIRResourcePractitioner
     */
    public function setQualification(FHIRPractitionerQualification ...$qualification): CFHIRResourcePractitioner
    {
        $this->fhirResource->setQualification(...$qualification);

        return $this;
    }

    /**
     * @param FHIRPractitionerQualification ...$qualification
     *
     * @return CFHIRResourcePractitioner
     */
    public function addQualification(FHIRPractitionerQualification ...$qualification): CFHIRResourcePractitioner
    {
        $this->fhirResource->addQualification(...$qualification);

        return $this;
    }

    /**
     * @return FHIRPractitionerQualification[]
     */
    public function getQualification(): array
    {
        return $this->fhirResource->getQualification();
    }

    /**
     * @param FHIRCodeableConcept ...$communication
     *
     * @return CFHIRResourcePractitioner
     */
    public function setCommunication(FHIRCodeableConcept ...$communication): CFHIRResourcePractitioner
    {
        $this->fhirResource->setCommunication(...$communication);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$communication
     *
     * @return CFHIRResourcePractitioner
     */
    public function addCommunication(FHIRCodeableConcept ...$communication): CFHIRResourcePractitioner
    {
        $this->fhirResource->addCommunication(...$communication);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept[]
     */
    public function getCommunication(): array
    {
        return $this->fhirResource->getCommunication();
    }

    /**
     * @return CCapabilitiesResource
     */
    protected function generateCapabilities(): CCapabilitiesResource
    {
        return parent::generateCapabilities()
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            )
            ->addSearchAttributes(
                [
                    new SearchParameterString('family'),
                ]
            );
    }

    /**
     * Map property telecom
     */
    protected function mapTelecom(): void
    {
        $this->fhirResource->setTelecom(...$this->object_mapping->mapTelecom());
    }

    /**
     * Map property address
     */
    protected function mapAddress(): void
    {
        $this->fhirResource->setAddress(...$this->object_mapping->mapAddress());
    }

    /**
     * Map property gender
     */
    protected function mapGender(): void
    {
        $this->fhirResource->setGender($this->object_mapping->mapGender());
    }

    /**
     * Map property birthDate
     */
    protected function mapBirthDate(): void
    {
        $this->fhirResource->setBirthDate($this->object_mapping->mapBirthDate());
    }

    /**
     * Map property photo
     */
    protected function mapPhoto(): void
    {
        $this->fhirResource->setPhoto(...$this->object_mapping->mapPhoto());
    }

    /**
     * Map property qualification
     */
    protected function mapQualification(): void
    {
        $this->fhirResource->setQualification(...$this->object_mapping->mapQualification());
    }

    /**
     * Map property communication
     */
    protected function mapCommunication(): void
    {
        $this->fhirResource->setCommunication(...$this->object_mapping->mapCommunication());
    }

    /**
     * Map property active
     */
    protected function mapActive(): void
    {
        $this->fhirResource->setActive($this->object_mapping->mapActive());
    }

    /**
     * Map property name
     */
    protected function mapName(): void
    {
        $this->fhirResource->setName(...$this->object_mapping->mapName());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRPractitioner';
    }
}
