<?php

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\DocumentManifest;

use Ox\Interop\Fhir\Contracts\Mapping\R4\DocumentManifestMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceDocumentManifestInterface;
use Ox\Components\FhirCore\Interfaces\FHIRDocumentManifestInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentManifestRelated;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;

/**
 * Description
 */
class CFHIRResourceDocumentManifest extends CFHIRDomainResource implements ResourceDocumentManifestInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'DocumentManifest';

    /** @var string */
    public const SYSTEM = 'urn:ietf:rfc:3986';

    /** @var FHIRDocumentManifestInterface $fhirResource */
    protected $fhirResource;

    /** @var DocumentManifestMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                ]
            );
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->fhirResource->getContent();
    }

    /**
     * @param FHIRDocumentManifestRelated ...$related
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setRelated(FHIRDocumentManifestRelated ...$related): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setRelated(...$related);

        return $this;
    }

    /**
     * @param FHIRDocumentManifestRelated ...$related
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function addRelated(FHIRDocumentManifestRelated ...$related): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->addRelated(...$related);

        return $this;
    }

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->fhirResource->getRelated();
    }

    /**
     * @param FHIRIdentifier|null $masterIdentifier
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setMasterIdentifier(?FHIRIdentifier $masterIdentifier): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setMasterIdentifier($masterIdentifier);

        return $this;
    }

    /**
     * @return FHIRIdentifier|null
     */
    public function getMasterIdentifier(): ?FHIRIdentifier
    {
        return $this->fhirResource->getMasterIdentifier();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRCodeableConcept|null $type
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setType(?FHIRCodeableConcept $type): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setType($type);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getType(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getType();
    }

    /**
     * @param FHIRReference|null $subject
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setSubject(?FHIRReference $subject): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setSubject($subject);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getSubject(): ?FHIRReference
    {
        return $this->fhirResource->getSubject();
    }

    /**
     * @param FHIRDateTime|null $created
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setCreated(?FHIRDateTime $created): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setCreated($created);

        return $this;
    }

    /**
     * @return FHIRDateTime|null
     */
    public function getCreated(): ?FHIRDateTime
    {
        return $this->fhirResource->getCreated();
    }

    /**
     * @param FHIRReference ...$author
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setAuthor(FHIRReference ...$author): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setAuthor(...$author);

        return $this;
    }

    /**
     * @param FHIRReference ...$author
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function addAuthor(FHIRReference ...$author): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->addAuthor(...$author);

        return $this;
    }

    /**
     * @return array
     */
    public function getAuthor(): array
    {
        return $this->fhirResource->getAuthor();
    }

    /**
     * @param FHIRReference ...$recipient
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setRecipient(FHIRReference ...$recipient): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setRecipient(...$recipient);

        return $this;
    }

    /**
     * @param FHIRReference ...$recipient
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function addRecipient(FHIRReference ...$recipient): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->addRecipient(...$recipient);

        return $this;
    }

    /**
     * @return array
     */
    public function getRecipient(): array
    {
        return $this->fhirResource->getRecipient();
    }

    /**
     * @param FHIRUri|null $source
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setSource(?FHIRUri $source): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setSource($source);

        return $this;
    }

    /**
     * @return FHIRUri|null
     */
    public function getSource(): ?FHIRUri
    {
        return $this->fhirResource->getSource();
    }

    /**
     * @param FHIRString|null $description
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setDescription(?FHIRString $description): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getDescription(): ?FHIRString
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @param FHIRReference ...$content
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function setContent(FHIRReference ...$content): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->setContent(...$content);

        return $this;
    }

    /**
     * @param FHIRReference ...$content
     *
     * @return CFHIRResourceDocumentManifest
     */
    public function addContent(FHIRReference ...$content): CFHIRResourceDocumentManifest
    {
        $this->fhirResource->addContent(...$content);

        return $this;
    }

    /**
     * * Map property masterIdentifier
     */
    protected function mapMasterIdentifier(): void
    {
        $this->fhirResource->setMasterIdentifier($this->object_mapping->mapMasterIdentifier());
    }

    /**
     * * Map property status
     */
    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    /**
     * * Map property type
     */
    protected function mapType(): void
    {
        $this->fhirResource->setType($this->object_mapping->mapType());
    }

    /**
     * * Map property subject
     */
    protected function mapSubject(): void
    {
        $this->fhirResource->setSubject($this->object_mapping->mapSubject());
    }

    /**
     * * Map property created
     */
    protected function mapCreated(): void
    {
        $this->fhirResource->setCreated($this->object_mapping->mapCreated());
    }


    /**
     * * Map property author
     */
    protected function mapAuthor(): void
    {
        $this->fhirResource->setAuthor(...$this->object_mapping->mapAuthor());
    }

    /**
     * * Map property recipient
     */
    protected function mapRecipient(): void
    {
        $this->fhirResource->setRecipient(...$this->object_mapping->mapRecipient());
    }

    /**
     * * Map property source
     */
    protected function mapSource(): void
    {
        $this->fhirResource->setSource($this->object_mapping->mapSource());
    }

    /**
     * * Map property description
     */
    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    /**
     * * Map property content
     */
    protected function mapContent(): void
    {
        $this->fhirResource->setContent(...$this->object_mapping->mapContent());
    }

    /**
     * * Map property related
     */
    protected function mapRelated(): void
    {
        $this->fhirResource->setRelated(...$this->object_mapping->mapRelated());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRDocumentManifest';
    }
}
