<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\DocumentReference\Mapper;

use Exception;
use Ox\Core\CMbArray;
use Ox\Core\CStoredObject;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\DocumentReferenceMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBase64Binary;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceContent;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceContext;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\DocumentReference\CFHIRResourceDocumentReference;
use Ox\Interop\Fhir\Resources\R4\Encounter\CFHIRResourceEncounter;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Interop\Fhir\Resources\R4\Patient\Profiles\InteropSante\CFHIRResourcePatientFR;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Utilities\CFHIRTools;
use Ox\Interop\InteropResources\valueset\CANSValueSet;
use Ox\Mediboard\Files\CDocumentItem;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Patients\CCorrespondantPatient;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\System\CNote;

/**
 * Description
 */
class DocumentReference implements DocumentReferenceMappingInterface, DelegatedObjectMapperInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CFHIRDomainResource */
    protected CFHIRResource $resource;

    /** @var CDocumentItem */
    protected $object;

    /** @var CStoredObject */
    protected $target;

    /**
     * @inheritDoc
     */
    public function onlyProfiles(): array
    {
        return [CFhir::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceDocumentReference::class];
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CDocumentItem && $object->_id;
    }

    /**
     * @inheritDoc
     */
    public function setResource(CFHIRResource $resource, $object): void
    {
        $resource->_use_contained = true; // TODO : supprimer
        $this->resource = $resource;
        $this->object   = $object;
        $this->target   = $this->object->loadTargetObject();
    }

    public function mapMasterIdentifier(): ?FHIRIdentifier
    {
        return (new FHIRIdentifier())->setValue($this->object->getUuid());
    }

    /**
     * @inheritDoc
     */
    public function mapStatus(): ?FHIRCode
    {
        return (new FHIRCode())->setValue('current');
    }

    /**
     * @inheritDoc
     */
    public function mapDocStatus(): ?FHIRCode
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapType(): ?FHIRCodeableConcept
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapCategory(): array
    {
        $categories = [];

        if ($this->object->type_doc_dmp) {
            [$codeSystem, $code] = explode('^', $this->object->type_doc_dmp);

            if ($codes = CANSValueSet::getTypeCode($code)) {
                $categories[] = CFHIRTools::codeableConceptFromValues($codes);
            }
        }

        return $categories;
    }

    /**
     * @inheritDoc
     */
    public function mapSubject(): ?FHIRReference
    {
        if (!$patient = $this->object->getFromStore(CPatient::class)) {
            $patient = $this->object->getIndexablePatient();
            if (!$patient->_id) {
                return null;
            }
        }

        return $this->resource->addReference(CFHIRResourcePatientFR::class, $patient);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function mapDate(): ?FHIRInstant
    {
        return (new FHIRInstant())
            ->setValue(CFHIR::getTimeUtc($this->object->_file_date, false));
    }

    /**
     * @inheritDoc
     */
    public function mapAuthor(): array
    {
        return [];
        $available_author = [
            CCorrespondantPatient::class,
            CMedecin::class,
        ];

        $authors = [];
        foreach ($available_author as $author_class) {
            if ($author = $this->object->getFromStore($author_class)) {
                 $authors[] = $this->resource->addReference(CFHIRResourcePractitioner::class, $author);
            }
        }

        if ($author = $this->object->loadRefAuthor()) {
            $authors[] = $this->resource->addReference(CFHIRResourcePractitioner::class, $author);
        }

        return $authors;
    }

    /**
     * @inheritDoc
     */
    public function mapAuthenticator(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapCustodian(): ?FHIRReference
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapRelatesTo(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapDescription(): ?FHIRString
    {
        /** @var CNote $note */
        $note = $this->object->loadLastBackRef('notes');
        if ($note && $note->_id) {
            return (new FHIRString())->setValue($note->text);
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function mapSecurityLabel(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function mapContent(): array
    {
        try {
            if (!$content = $this->object->getBinaryContent(true, false)) {
                return [];
            }
        } catch (Exception $e) {
            return [];
        }

        $content_type = $this->object instanceof CFile ? $this->object->file_type : "application/pdf";
        $title = $this->object instanceof CFile ? $this->object->file_name : $this->object->nom;
        $reference_content = (new FHIRDocumentReferenceContent())
            ->setAttachment(
                (new FHIRAttachment())
                    ->setData((new FHIRBase64Binary())->setValue($content, false))
                    ->setSize(strlen($content))
                    ->setHash((new FHIRBase64Binary())->setValue(sha1($content), false))
                    ->setTitle($title)
                    ->setCreation($this->object->_file_date)
                    ->setContentType($content_type)
            );
        return [$reference_content];
    }

    /**
     * @inheritDoc
     */
    public function mapContext(): ?FHIRDocumentReferenceContext
    {
        $context = new FHIRDocumentReferenceContext();
        $target  = $this->target;

        if ($target instanceof COperation) {
            $target = $target->loadRefSejour();
        }

        if ($target instanceof CSejour) {
            $context->setEncounter($this->resource->addReference(CFHIRResourceEncounter::class, $target));
            $context->setPeriod(
                (new FHIRPeriod())
                    ->setStart(CFHIR::getTimeUtc($target->entree, false))
                    ->setEnd(CFHIR::getTimeUtc($target->sortie, false))
            );
        }

        return !$context->isNull() ? $context : null;
    }
}
