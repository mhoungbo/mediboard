<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\DocumentReference\Mapper\ANS;

use Exception;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\DocumentReferenceMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceContent;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceContext;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceRelatesTo;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Profiles\CFHIRCDL;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\CStoredObjectResourceDomainTrait;
use Ox\Interop\Fhir\Resources\R4\DocumentReference\Profiles\ANS\CFHIRResourceDocumentReferenceCdL;
use Ox\Interop\Fhir\Resources\R4\Patient\Profiles\InteropSante\CFHIRResourcePatientFR;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Interop\Fhir\Resources\R4\RelatedPerson\CFHIRResourceRelatedPerson;
use Ox\Interop\Fhir\Utilities\CFHIRTools;
use Ox\Interop\InteropResources\valueset\CANSValueSet;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Patients\CCorrespondantPatient;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\System\CNote;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

class DocumentReferenceCdLNote implements DelegatedObjectMapperInterface, DocumentReferenceMappingInterface
{
    use CStoredObjectResourceDomainTrait;

    /** @var CNote */
    protected $object;

    /** @var CFHIRResourceDocumentReferenceCdL */
    protected CFHIRResource $resource;

    public function setResource(CFHIRResource $resource, $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    /**
     * @param CFHIRResource $resource
     * @param mixed         $object
     *
     * @return bool
     */
    public function isSupported(CFHIRResource $resource, $object): bool
    {
        return $object instanceof CNote && $object->_id;
    }

    /**
     * @return string[]
     */
    public function onlyProfiles(): array
    {
        return [CFHIRCDL::class];
    }

    /**
     * @return string[]
     */
    public function onlyRessources(): array
    {
        return [CFHIRResourceDocumentReferenceCdL::class];
    }

    /**
     * @throws Exception
     */
    public function mapMasterIdentifier(): ?FHIRIdentifier
    {
        return (new FHIRIdentifier())->setValue($this->object->getUuid());
    }

    public function mapStatus(): ?FHIRCode
    {
        return (new FHIRCode())->setValue('current');
    }

    public function mapDocStatus(): ?FHIRCode
    {
        return null;
    }

    public function mapType(): ?FHIRCodeableConcept
    {
        $values               = CANSValueSet::loadEntries('typeNoteCdL', 'OBS');
        $values['codeSystem'] = 'urn:oid:1.2.250.1.213.1.1.4.334';

        return CFHIRTools::codeableConceptFromValues($values);
    }

    public function mapCategory(): array
    {
        return [];
    }

    public function mapSubject(): ?FHIRReference
    {
        $target = $this->object->loadTargetObject();
        if (($target instanceof CFile || $target instanceof CNote) && $target->_id) {
            $target = $target->loadTargetObject();
        }
        if (!$target->_id) {
            return null;
        }

        return $target instanceof CPatient
            ? $this->resource->addReference(CFHIRResourcePatientFR::class, $target)
            : null;
    }

    /**
     * @throws Exception
     */
    public function mapDate(): ?FHIRInstant
    {
        return (new FHIRInstant())->setValue(CFHIR::getTimeUtc($this->object->date));
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapAuthor(): array
    {
        if ($author = $this->object->getFromStore(CCorrespondantPatient::class)) {
            return [$this->resource->addReference(CFHIRResourceRelatedPerson::class, $author)];
        }

        if ($author = $this->object->getFromStore(CMedecin::class)) {
            return [$this->resource->addReference(CFHIRResourcePractitionerRoleProfessionalRass::class, $author)];
        }

        $user = $this->object->loadRefUser();
        if ($user->_id) {
            return [$this->resource->addReference(CFHIRResourcePractitionerFR::class, $user)];
        }

        return [];
    }

    public function mapAuthenticator(): ?FHIRReference
    {
        // Forbidden in this profile
        return null;
    }

    public function mapCustodian(): ?FHIRReference
    {
        // Forbidden in this profile
        return null;
    }

    /**
     * @return FHIRDocumentReferenceRelatesTo[]
     * @throws \Exception
     */
    public function mapRelatesTo(): array
    {
        $relates = [];
        $target = $this->object->loadTargetObject();
        if (($target instanceof CFile || $target instanceof CNote) && $target->_id) {
            $relates[] = (new FHIRDocumentReferenceRelatesTo())
                ->setCode('appends')
                ->setTarget($this->resource->addReference(CFHIRResourceDocumentReferenceCdL::class, $target));
        }

        return $relates;
    }

    public function mapDescription(): ?FHIRString
    {
        return $this->object->text ? (new FHIRString())->setValue($this->object->text) : null;
    }

    public function mapSecurityLabel(): array
    {
        return [];
    }

    public function mapContent(): array
    {
        $title   = explode("\n", $this->object->text)[0] ?? $this->object->text;
        $content = $this->object->text;

        $reference_content = (new FHIRDocumentReferenceContent())
            ->setAttachment(
                (new FHIRAttachment())
                    ->setData($content)
                    ->setSize(strlen($content))
                    ->setHash(sha1($content))
                    ->setTitle($title)
                    ->setCreation($this->resource->getDate()->getValue())
                    ->setContentType('text/plain')
            );

        return [$reference_content];
    }

    public function mapContext(): ?FHIRDocumentReferenceContext
    {
        return null;
    }

    public function mapExtension(): array
    {
        $extensions = [];
        if ($this->object->degre === 'high') {
            $extensions[] = (new FHIRExtension())
                ->setUrl('http://esante.gouv.fr/ci-sis/fhir/StructureDefinition/isUrgent')
                ->setValue((new FHIRBoolean())->setValue(true));
        }

        return $extensions;
    }
}
