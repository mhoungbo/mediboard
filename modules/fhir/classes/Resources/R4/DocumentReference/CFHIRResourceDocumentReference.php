<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\DocumentReference;

use Ox\Interop\Fhir\Contracts\Mapping\R4\DocumentReferenceMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceDocumentReferenceInterface;
use Ox\Components\FhirCore\Interfaces\FHIRDocumentReferenceInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceContent;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceContext;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceRelatesTo;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterReference;

/**
 * FIHR document reference resource
 */
class CFHIRResourceDocumentReference extends CFHIRDomainResource implements ResourceDocumentReferenceInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'DocumentReference';

    /** @var string */
    public const SYSTEM = 'urn:ietf:rfc:3986';

    // attributes

    /** @var FHIRDocumentReferenceInterface */
    protected $fhirResource;

    /** @var DocumentReferenceMappingInterface */
    protected $object_mapping;

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionDelete::NAME,
                    CFHIRInteractionUpdate::NAME,
                ]
            )
            ->addSearchAttributes(
                [
                    new SearchParameterReference('encounter'),
                    new SearchParameterReference('patient'),
                ]
            );
    }

    /**
     * @param FHIRIdentifier|null $masterIdentifier
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setMasterIdentifier(?FHIRIdentifier $masterIdentifier): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setMasterIdentifier($masterIdentifier);

        return $this;
    }

    /**
     * @return FHIRIdentifier|null
     */
    public function getMasterIdentifier(): ?FHIRIdentifier
    {
        return $this->fhirResource->getMasterIdentifier();
    }

    /**
     * @param FHIRCode|null $status
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setStatus(?FHIRCode $status): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setStatus($status);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getStatus(): ?FHIRCode
    {
        return $this->fhirResource->getStatus();
    }

    /**
     * @param FHIRCode|null $docStatus
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setDocStatus(?FHIRCode $docStatus): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setDocStatus($docStatus);

        return $this;
    }

    /**
     * @return FHIRCode|null
     */
    public function getDocStatus(): ?FHIRCode
    {
        return $this->fhirResource->getDocStatus();
    }

    /**
     * @param FHIRCodeableConcept|null $type
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setType(?FHIRCodeableConcept $type): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setType($type);

        return $this;
    }

    /**
     * @return FHIRCodeableConcept|null
     */
    public function getType(): ?FHIRCodeableConcept
    {
        return $this->fhirResource->getType();
    }

    /**
     * @param FHIRCodeableConcept ...$category
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setCategory(FHIRCodeableConcept ...$category): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setCategory(...$category);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$category
     *
     * @return CFHIRResourceDocumentReference
     */
    public function addCategory(FHIRCodeableConcept ...$category): CFHIRResourceDocumentReference
    {
        $this->fhirResource->addCategory(...$category);

        return $this;
    }

    /**
     * @return array
     */
    public function getCategory(): array
    {
        return $this->fhirResource->getCategory();
    }

    /**
     * @param FHIRReference|null $subject
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setSubject(?FHIRReference $subject): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setSubject($subject);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getSubject(): ?FHIRReference
    {
        return $this->fhirResource->getSubject();
    }

    /**
     * @param FHIRInstant|null $date
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setDate(?FHIRInstant $date): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setDate($date);

        return $this;
    }

    /**
     * @return FHIRInstant|null
     */
    public function getDate(): ?FHIRInstant
    {
        return $this->fhirResource->getDate();
    }

    /**
     * @param FHIRReference ...$author
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setAuthor(FHIRReference ...$author): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setAuthor(...$author);

        return $this;
    }

    /**
     * @param FHIRReference ...$author
     *
     * @return CFHIRResourceDocumentReference
     */
    public function addAuthor(FHIRReference ...$author): CFHIRResourceDocumentReference
    {
        $this->fhirResource->addAuthor(...$author);

        return $this;
    }

    /**
     * @return array
     */
    public function getAuthor(): array
    {
        return $this->fhirResource->getAuthor();
    }

    /**
     * @param FHIRReference|null $authenticator
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setAuthenticator(?FHIRReference $authenticator): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setAuthenticator($authenticator);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getAuthenticator(): ?FHIRReference
    {
        return $this->fhirResource->getAuthenticator();
    }

    /**
     * @param FHIRReference|null $custodian
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setCustodian(?FHIRReference $custodian): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setCustodian($custodian);

        return $this;
    }

    /**
     * @return FHIRReference|null
     */
    public function getCustodian(): ?FHIRReference
    {
        return $this->fhirResource->getCustodian();
    }

    /**
     * @param FHIRDocumentReferenceRelatesTo ...$relatesTo
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setRelatesTo(FHIRDocumentReferenceRelatesTo ...$relatesTo): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setRelatesTo(...$relatesTo);

        return $this;
    }

    /**
     * @param FHIRDocumentReferenceRelatesTo ...$relatesTo
     *
     * @return CFHIRResourceDocumentReference
     */
    public function addRelatesTo(FHIRDocumentReferenceRelatesTo ...$relatesTo): CFHIRResourceDocumentReference
    {
        $this->fhirResource->addRelatesTo(...$relatesTo);

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceRelatesTo[]
     */
    public function getRelatesTo(): array
    {
        return $this->fhirResource->getRelatesTo();
    }

    /**
     * @param FHIRString|null $description
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setDescription(?FHIRString $description): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setDescription($description);

        return $this;
    }

    /**
     * @return FHIRString|null
     */
    public function getDescription(): ?FHIRString
    {
        return $this->fhirResource->getDescription();
    }

    /**
     * @param FHIRCodeableConcept ...$securityLabel
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setSecurityLabel(FHIRCodeableConcept ...$securityLabel): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setSecurityLabel(...$securityLabel);

        return $this;
    }

    /**
     * @param FHIRCodeableConcept ...$securityLabel
     *
     * @return CFHIRResourceDocumentReference
     */
    public function addSecurityLabel(FHIRCodeableConcept ...$securityLabel): CFHIRResourceDocumentReference
    {
        $this->fhirResource->addSecurityLabel(...$securityLabel);

        return $this;
    }

    /**
     * @return array
     */
    public function getSecurityLabel(): array
    {
        return $this->fhirResource->getSecurityLabel();
    }

    /**
     * @param FHIRDocumentReferenceContent ...$content
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setContent(FHIRDocumentReferenceContent ...$content): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setContent(...$content);

        return $this;
    }

    /**
     * @param FHIRDocumentReferenceContent ...$content
     *
     * @return CFHIRResourceDocumentReference
     */
    public function addContent(FHIRDocumentReferenceContent ...$content): CFHIRResourceDocumentReference
    {
        $this->fhirResource->addContent(...$content);

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceContent[]
     */
    public function getContent(): array
    {
        return $this->fhirResource->getContent();
    }

    /**
     * @param FHIRDocumentReferenceContext|null $context
     *
     * @return CFHIRResourceDocumentReference
     */
    public function setContext(?FHIRDocumentReferenceContext $context): CFHIRResourceDocumentReference
    {
        $this->fhirResource->setContext($context);

        return $this;
    }

    /**
     * @return FHIRDocumentReferenceContext|null
     */
    public function getContext(): ?FHIRDocumentReferenceContext
    {
        return $this->fhirResource->getContext();
    }

    protected function mapMasterIdentifier(): void
    {
        $this->fhirResource->setMasterIdentifier($this->object_mapping->mapMasterIdentifier());
    }

    protected function mapStatus(): void
    {
        $this->fhirResource->setStatus($this->object_mapping->mapStatus());
    }

    protected function mapType(): void
    {
        $this->fhirResource->setType($this->object_mapping->mapType());
    }

    protected function mapCategory(): void
    {
        $this->fhirResource->setCategory(...$this->object_mapping->mapCategory());
    }

    protected function mapSubject(): void
    {
        $this->fhirResource->setSubject($this->object_mapping->mapSubject());
    }

    protected function mapDate(): void
    {
        $this->fhirResource->setDate($this->object_mapping->mapDate());
    }

    protected function mapAuthor(): void
    {
        $this->fhirResource->setAuthor(...$this->object_mapping->mapAuthor());
    }

    protected function mapRelatesTo(): void
    {
        $this->fhirResource->setRelatesTo(...$this->object_mapping->mapRelatesTo());
    }

    protected function mapDescription(): void
    {
        $this->fhirResource->setDescription($this->object_mapping->mapDescription());
    }

    protected function mapSecurityLabel(): void
    {
        $this->fhirResource->setSecurityLabel(...$this->object_mapping->mapSecurityLabel());
    }

    protected function mapContent(): void
    {
        $this->fhirResource->setContent(...$this->object_mapping->mapContent());
    }

    protected function mapContext(): void
    {
        $this->fhirResource->setContext($this->object_mapping->mapContext());
    }

    protected function mapAuthenticator(): void
    {
        $this->fhirResource->setAuthenticator($this->object_mapping->mapAuthenticator());
    }

    protected function mapCustodian(): void
    {
        $this->fhirResource->setCustodian($this->object_mapping->mapCustodian());
    }

    protected function mapDocStatus(): void
    {
        $this->fhirResource->setDocStatus($this->object_mapping->mapDocStatus());
    }

    public function getResourceNamespace(): string
    {
        return 'Ox\Components\FhirCore\Model\R4\Resources\FHIRDocumentReference';
    }
}
