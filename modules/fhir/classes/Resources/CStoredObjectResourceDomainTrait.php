<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRNarrative;
use Ox\Components\FhirCore\Model\R4\Resources\FHIRResource;

/**
 * Description
 */
trait CStoredObjectResourceDomainTrait
{
    use CStoredObjectResourceTrait;
    use CStoredObjectResourceIdentifierTrait;

    /**
     * Map property Extension
     *
     * @return array
     */
    public function mapExtension(): array
    {
        return [];
    }

    /**
     * Map property ModifierExtension
     *
     * @return array
     */
    public function mapModifierExtension(): array
    {
        return [];
    }

    /**
     * Map property Text
     *
     * @return FHIRNarrative|null
     */
    public function mapText(): ?FHIRNarrative
    {
        return null;
    }

    /**
     * Map property Contained
     *
     * @return FHIRResource[]
     */
    public function mapContained(): array
    {
        return [];
    }

    /**
     * @return ResourceMappingInterface
     */
    public function getMapping(): ResourceMappingInterface
    {
        return $this;
    }
}
