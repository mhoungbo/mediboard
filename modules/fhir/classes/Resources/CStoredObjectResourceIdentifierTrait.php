<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbString;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;

/**
 * Description
 */
trait CStoredObjectResourceIdentifierTrait
{
    use CStoredObjectResourceTrait;

    /**
     * Map property identifier
     *
     * @return array
     * @throws Exception
     */
    public function mapIdentifier(): array
    {
        if (!$this->object || !$this->object->_id) {
            return [];
        }

        $type = (new FHIRCodeableConcept())
            ->setCoding(
                (new FHIRCoding())
                    ->setSystem('http://terminology.hl7.org/CodeSystem/v2-0203')
                    ->setCode('RI')
                    ->setDisplay('Resource identifier'),
                (new FHIRCoding())
                    ->setSystem('http://interopsante.org/fhir/CodeSystem/fr-v2-0203')
                    ->setCode('INTRN')
                    ->setDisplay('Identifiant interne')
            );

        $system = CAppUI::conf('mb_oid');
        if (CMbString::isUUID($system)) {
            $system = "urn:uuid:$system";
        } elseif (CMbString::isOID($system)) {
            $system = "urn:oid:$system";
        }

        return [
            (new FHIRIdentifier())
                ->setSystem($system)
                ->setType($type)
                ->setValue($this->object->_id)
        ];
    }
}
