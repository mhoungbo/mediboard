<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Serializers;

use DOMElement;
use DOMNode;
use Ox\Components\FhirCore\Parser\Parser;
use Ox\Core\CMbXMLDocument;
use Ox\Interop\Fhir\Api\Request\CRequestFormats;
use Ox\Interop\Fhir\CFHIRXPath;
use Ox\Interop\Fhir\ClassMap\FHIRClassMap;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Exception\CFHIRExceptionInformational;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Psr\SimpleCache\InvalidArgumentException;

class CFHIRParser
{
    /** @var mixed */
    private $data;

    /** @var string */
    private $format;

    /** @var CFHIRResource */
    private $resource;

    /** @var CFHIRXPath */
    private $xpath;

    /** @var CMbXMLDocument */
    private $dom;

    /**
     * CFHIRParser constructor.
     *
     * @param string      $data
     * @param string|null $format
     */
    public function __construct(string $data, ?string $format)
    {
        $this->data   = $data;
        $this->format = $format ? $this->setFormat($format) : $this->determineFormat();
    }

    /**
     * @param string $format
     *
     * @return string
     * @throws CFHIRException
     */
    private function setFormat(string $format): string
    {
        if (!$format = CRequestFormats::getFormatSupported($format)) {
            throw new CFHIRException("The format '$format' for Parser is not supported");
        }

        return $this->format = $format;
    }

    /**
     * @return string
     * @throws CFHIRException
     */
    private function determineFormat(): string
    {
        if (is_string($this->data)) {
            // is json
            if (strpos($this->data, '{') === 0) {
                return CRequestFormats::CONTENT_TYPE_JSON;
            }

            // is xml
            if (strpos($this->data, '<?xml') === 0 || strpos($this->data, '<') === 0) {
                return CRequestFormats::CONTENT_TYPE_XML;
            }
        }

        throw new CFHIRException("Impossible to detect format for parsing data");
    }

    /**
     * @return bool
     */
    private function isJsonFormat(): bool
    {
        return $this->format === CRequestFormats::CONTENT_TYPE_JSON;
    }

    private function initialize(): void
    {
        $data = $this->data;

        // is json
        if ($this->isJsonFormat()) {
            $dom = $this->transformJsonInXml($data);
        } else {
            $dom = new CMbXMLDocument();
            $dom->loadXML($data);
        }

        $this->xpath = new CFHIRXPath($dom);
        $this->dom   = $dom;
    }

    /**
     * @param string|array $data
     * @param string|null $format
     *
     * @return CFHIRResource|null
     */
    public static function tryToDetermineResource($data, ?string $format = null): ?CFHIRResource
    {
        $serializer = new self($data, $format);

        // initialize parsing
        $serializer->initialize();

        if (!$serializer->dom->documentElement) {
            return null;
        }

        // determine resource
        return $serializer->determineResource($serializer->dom->documentElement);
    }

    private function determineResource(DOMNode $node): ?CFHIRResource
    {
        $resource_type = $node->nodeName;
        $profiles      = $this->xpath->query('fhir:meta/fhir:profile');

        $canonical = CFHIR::BASE_PROFILE . $resource_type;
        if (count($profiles) === 1) {
            $node_profile       = $profiles->item(0);
            $canonical_profiled = $node_profile->attributes->getNamedItem('value')->textContent;
        }

        $map = new FHIRClassMap();
        // try to retrieve class for profiled resource
        if (isset($canonical_profiled) && $canonical_profiled) {
            try {
                return $map->resource->getResource($canonical_profiled);
            } catch (CFHIRException $exception) {
            }
        }

        // try to retrieve class for international resource
        try {
            return $resource ?? $map->resource->getResource($canonical);
        } catch (CFHIRException $exception) {
            // resource not supported
            return null;
        }
    }

    /**
     * @param string $data
     *
     * @return string
     */
    private function transformJsonInXml(string $json): CMbXMLDocument
    {
        $data = json_decode($json, false);

        $this->dom = new CMbXMLDocument('UTF-8');
        if ($resource = $this->handleJsonResource($data)) {
            $this->dom->appendChild($resource);
        }

        return $this->dom;
    }

    /**
     * @param object|null $object
     *
     * @return DOMNode|null
     */
    private function handleJsonResource(?object $object): ?DOMNode
    {
        if (!$object || !$resource_type = $object->resourceType) {
            return null;
        }

        unset($object->resourceType);

        return $this->handleJsonObject($resource_type, $object);
    }

    /**
     * @param string             $field
     * @param mixed|array|object $value
     *
     * @return array|DOMElement|DOMNode|null
     */
    private function handleJsonElements(string $field, $value)
    {
        if (is_object($value)) {
            return $this->handleJsonObject($field, $value);
        }

        if (is_array($value)) {
            return $this->handleJsonArray($field, $value);
        }

        return $this->handleJsonPrimitive($field, $value);
    }

    /**
     * @param string $field
     * @param array  $value
     *
     * @return array
     */
    private function handleJsonArray(string $field, array $value): array
    {
        $resources = [];
        foreach ($value as $value_child) {
            $element     = $this->handleJsonElements($field, $value_child);
            $resources[] = $element;
        }

        return $resources;
    }

    /**
     * @param string $field
     * @param object $object
     *
     * @return DOMNode|null
     */
    private function handleJsonObject(string $field, object $object): ?DOMNode
    {
        $resource = $this->dom->createElementNS(CFHIRXPath::FHIR_NAMESPACE, $field);
        if (isset($object->resourceType)) {
            $element = $this->handleJsonResource($object);
            $resource->appendChild($element);

            return $resource;
        }

        foreach ($object as $object_field => $value) {
            if (strpos($object_field, '_') === 0) {
                continue;
            }

            if ($field === 'extension' && $object_field === 'url') {
                $resource->setAttribute('url', $value);

                continue;
            } else {
                $elements = $this->handleJsonElements($object_field, $value);
            }

            if (!is_array($elements)) {
                if (isset($object->{"_$object_field"})) {
                    // retrieve data
                    $extension_data = $object->{"_$object_field"};

                    // clean object
                    unset($object->{"_$object_field"});

                    // handle id on primitive datatype
                    if (isset($extension_data->id) && $extension_data->id) {
                        $elements->setAttribute('id', $extension_data->id);
                    }

                    // handle extension on primitive datatype
                    if (isset($extension_data->extension) && count($extension_data->extension) > 0) {
                        $extension_elements = $this->handleJsonElements('extension', $extension_data->extension);
                        if (!is_array($extension_elements)) {
                            $extension_elements = [$extension_elements];
                        }

                        // append extension element
                        foreach ($extension_elements as $extension) {
                            $elements->appendChild($extension);
                        }
                    }
                }

                $elements = [$elements];
            }

            $elements = array_filter($elements);
            foreach ($elements as $element) {
                $resource->appendChild($element);
            }
        }

        return $resource;
    }

    /**
     * @param string          $field
     * @param string|bool|int $value
     *
     * @return DOMElement
     */
    private function handleJsonPrimitive(string $field, $value): DOMElement
    {
        // sanitize value
        if ($value === true) {
            $value = 'true';
        } elseif ($value === false) {
            $value = 'false';
        }

        $element = $this->dom->createElementNS(CFHIRXPath::FHIR_NAMESPACE, $field);
        $element->setAttribute('value', $value);

        return $element;
    }

    /**
     * @param string|array $data
     * @param string|null  $format
     *
     * @return self
     * @throws InvalidArgumentException
     */
    public static function parse($data, ?string $format = null): ?self
    {
        $serializer = new self($data, $format);

        $fhirResource = Parser::parse($data)->getResource();
        if ($fhirResource === null) {
            return $serializer;
        }

        // initialize parsing
        $serializer->initialize();

        if (!$serializer->dom->documentElement) {
            return $serializer;
        }

        $resource_fhir = (new FHIRClassMap())->resource->getResource($fhirResource::RESOURCE_NAME);
        $resources_profiled = [];
        $profiles = $fhirResource->getMeta() ? $fhirResource->getMeta()->getProfile() : [];
        foreach ($profiles as $profile) {
            try {
                  $resources_profiled[] = (new FHIRClassMap())->resource->getResource($profile->getValue());
            } catch (CFHIRExceptionInformational $e) {
            }
        }

        $resource = (count($resources_profiled) === 1) ? reset($resources_profiled) : $resource_fhir;

        $serializer->resource = new $resource($fhirResource);

        return $serializer;
    }

    public function getResource(): ?CFHIRResource
    {
        return $this->resource;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return CMbXMLDocument
     */
    public function getDom(): ?CMbXMLDocument
    {
        return $this->dom;
    }

    /**
     * @return CFHIRXPath
     */
    public function getXpath(): ?CFHIRXPath
    {
        return $this->xpath;
    }
}
