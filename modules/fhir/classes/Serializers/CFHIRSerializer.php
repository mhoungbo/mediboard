<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Serializers;

use Ox\Core\CMbXMLDocument;
use Ox\Interop\Fhir\Api\Request\CRequestFormats;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Psr\SimpleCache\InvalidArgumentException;
use Ox\Components\FhirCore\Serializer\Serializer;

class CFHIRSerializer
{
    /** @var string */
    public const OPTION_OUTPUT_PRETTY = 'pretty';

    /** @var CFHIRResource */
    private $resource;

    /** @var string */
    private $format;

    /** @var array */
    private $options;

    /** @var string */
    private $resource_serialized;

    /** @var CMbXMLDocument */
    private $dom;

    /** @var array|null */
    private $json_data;

    public function __construct(CFHIRResource $resource, string $format, array $options = [])
    {
        $this->setFormat($format);
        $this->setOptions($options);
        $this->resource = $resource;
    }

    /**
     * @param string $format
     */
    public function setFormat(string $format): void
    {
        if (!$format = CRequestFormats::getFormatSupported($format)) {
            throw new CFHIRException("The format '$format' for Serializer is not supported");
        }

        $this->format = $format;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options): void
    {
        $default_options = $this->getDefaultOptions();

        foreach ($options as $key => $option) {
            if (!array_key_exists($key, $default_options)) {
                continue;
            }

            $default_options[$key] = $option;
        }

        $this->options = $default_options;
    }

    protected function getDefaultOptions(): array
    {
        return [
            self::OPTION_OUTPUT_PRETTY => false,
        ];
    }

    /**
     * @return bool
     */
    private function isXml(): bool
    {
        return $this->format === CRequestFormats::CONTENT_TYPE_XML;
    }

    /**
     * @param CFHIRResource $resource
     * @param string        $format
     * @param array         $options
     *
     * @return static
     * @throws InvalidArgumentException
     */
    public static function serialize(
        CFHIRResource $resource,
        string $format = CRequestFormats::CONTENT_TYPE_XML,
        array $options = []
    ): self {
        $serializer = new self($resource, $format, $options);

        if ($serializer->isXml()) {
            $content = $serializer->serializeXML();

        } else {
            $content = $serializer->serializeJSON();
        }

        $serializer->resource_serialized = $content;

        return $serializer;
    }

    /**
     * @return string
     */
    public function getResourceSerialized(): string
    {
        return $this->resource_serialized;
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    private function serializeXML(): string
    {
        $serializer = Serializer::serialize(
            $this->resource->getFhirResource(),
            Serializer::FORMAT_XML,
            ['formatOutput' => $this->options[self::OPTION_OUTPUT_PRETTY]]
        );
        $this->dom = $serializer->getDocument();

        return (string) $serializer;
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    private function serializeJSON(): string
    {
        $serializer = Serializer::serialize(
            $this->resource->getFhirResource(),
            Serializer::FORMAT_JSON,
            ['formatOutput' => $this->options[self::OPTION_OUTPUT_PRETTY]]
        );

        $this->json_data = json_decode((string) $serializer); // TODO : � rajouter dans le serializer fhircore ?

        return (string) $serializer;
    }

    /**
     * @return CMbXMLDocument|null
     */
    public function getDom(): ?CMbXMLDocument
    {
        return $this->dom;
    }

    /**
     * @return array|null
     */
    public function getJsonData(): ?array
    {
        return $this->json_data;
    }
}
