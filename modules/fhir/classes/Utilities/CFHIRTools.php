<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Core\CMbArray;
use Ox\Core\CMbString;

/**
 * Description
 */
class CFHIRTools
{
    /**
     * @param array $values
     *
     * @return FHIRCodeableConcept
     */
    public static function codeableConceptFromValues(array $values)
    {
        $codeableConcept = (new FHIRCodeableConcept())
            ->setCoding(self::codingFromValues($values));

        if ($text = CMbArray::get($values, 'text')) {
            $codeableConcept->setText((new FHIRString())->setValue($text));
        }

        return $codeableConcept;
    }

    public static function codingFromValues(array $values)
    {
        $coding = new FHIRCoding();

        if ($code = CMbArray::get($values, 'code')) {
            $coding->setCode((new FHIRCode())->setValue($code));
        }

        if ($code_system = CMbArray::get($values, 'codeSystem')) {
            if (CMbString::isUUID($code_system)) {
                $code_system = "urn:uuid:$code_system";
            } elseif (CMbString::isOID($code_system)) {
                $code_system = "urn:oid:$code_system";
            }

            $coding->setSystem((new FHIRUri())->setValue($code_system));
        }

        if ($display_name = CMbArray::get($values, 'displayName')) {
            $coding->setDisplay((new FHIRString())->setValue($display_name));
        }

        return $coding;
    }
}
