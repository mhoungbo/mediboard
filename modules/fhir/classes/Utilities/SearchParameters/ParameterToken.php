<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\SearchParameters;

use Ox\Core\CMbString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;

class ParameterToken
{
    private ?string $code = null;

    private ?FHIRUri $system = null;

    private bool $system_should_be_null = false;

    /**
     * SearchParameter constructor.
     *
     * @param AbstractSearchParameter $type
     * @param mixed                   $value
     * @param string|null             $modifier
     * @param string|null             $prefix
     */
    public function __construct(string $value)
    {
        $pipe_position = strpos($value, '|');

        // [parameter]=[code] / only code given
        if ($pipe_position === false) {
            $this->code = $value;
        } elseif ($pipe_position === 0) {
            // [parameter]=|[code] / only code given but system is null
            $this->code                  = $this->withoutPipe($value);
            $this->system_should_be_null = true;
        } elseif ($pipe_position === strlen($value)) {
            // [parameter]=[system] / only system given

            $system = $this->withoutPipe($value);
            if (CMbString::isUUID($system)) {
                $system = "urn:uuid:$system";
            } elseif (CMbString::isOID($system)) {
                $system = "urn:oid:$system";
            }

            $this->system = (new FHIRUri())->setValue($system);
        } else {
            // [parameter]=[system]|[code] / System and code given
            [$system, $code] = explode('|', $value, 2);

            if (CMbString::isUUID($system)) {
                $system = "urn:uuid:$system";
            } elseif (CMbString::isOID($system)) {
                $system = "urn:oid:$system";
            }

            $this->system = (new FHIRUri())->setValue($system);
            $this->code   = $code;
        }
    }

    /**
     * @param string $value
     *
     * @return array|string|string[]
     */
    protected function withoutPipe(string $value)
    {
        return str_replace('|', '', $value);
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getSystem(): ?FHIRUri
    {
        return $this->system;
    }

    /**
     * @return bool
     */
    public function isSystemShouldBeNull(): bool
    {
        return $this->system_should_be_null;
    }
}
