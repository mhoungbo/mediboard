<?php

/**
 * @package Mediboard\Fhir\Objects
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\Helper;

use Ox\Components\FhirCore\Interfaces\FHIRResourceInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;

class SejourHelper
{
    /**
     * @return FHIRCoding
     */
    public static function getTypeCodingNDA(): FHIRCoding
    {
        return (new FHIRCoding())
            ->setSystem('http://interopsante.org/fhir/ValueSet/fr-encounter-identifier-type')
            ->setCode('VN')
            ->setDisplay('Visit Number');
    }

    /**
     * Search NDA identifier in resource encounter
     *
     * @param FHIRResourceInterface $encounter
     * @param string                 $nda_oid
     *
     * @return string|null
     */
    public static function getNDA(FHIRResourceInterface $encounter, string $nda_oid): ?string
    {
        $coding_nda = self::getTypeCodingNDA();

        foreach ($encounter->getIdentifier() as $identifier) {
            // search coding for NDA (system_oid and code)
            if (
                $identifier->getType() &&
                $coding_nda->getSystem() &&
                $coding_nda->getCode() &&
                !$identifier->getType()->searchCoding($coding_nda->getSystem()->getValue(), $coding_nda->getCode()->getValue())
            ) {
                continue;
            }

            if (!$identifier->getSystem() || $identifier->getSystem()->isNull() || $identifier->getSystem()->isMatch($nda_oid)) {
                continue;
            }

            return $identifier->getValue() ? $identifier->getValue()->getValue() : null;
        }

        return null;
    }
}
