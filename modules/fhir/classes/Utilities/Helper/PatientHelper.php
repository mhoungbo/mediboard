<?php

/**
 * @package Mediboard\Fhir\Objects
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\Helper;

use Exception;
use Ox\Interop\Eai\Resolver\Identifiers\PIIdentifierResolver;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\R4\Resources\FHIRPatient;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CPatientINSNIR;

class PatientHelper
{
    /** @var string  */
    public const INS_NIR = CPatientINSNIR::OID_INS_NIR;
    /** @var string  */
    public const INS_NIA = CPatientINSNIR::OID_INS_NIA;

    /** @var string */
    private const TYPE_INS_SYSTEM = 'http://interopsante.org/CodeSystem/fr-v2-0203';

    /** @var string[] */
    private const MAPPING_INS_TYPE_CODE = [
        self::INS_NIR => 'INS-NIR',
        self::INS_NIA => 'INS-NIA',
    ];

    /**
     * Search INS (type) of patient in resource fhir
     *
     * @param FHIRPatient $resource_patient
     * @param string               $ins_type
     *
     * @return string|null
     * @throw CFHIRException|Exception
     */
    public static function getINS(FHIRPatient $resource_patient, string $ins_type = self::INS_NIR): ?string
    {
        if (!in_array($ins_type, [self::INS_NIR, self::INS_NIA])) {
            throw CFHIRException::tr('PatientHelper-msg-type INS invalid', $ins_type);
        }

        foreach ($resource_patient->getIdentifier() ?? [] as $identifier) {
            if (!$identifier->getSystem()->isMatch($ins_type) || !$identifier->getType()) {
                continue;
            }

            foreach ($identifier->getType()->getCoding() ?? [] as $type_identifier) {
                if (!$type_identifier->getSystem() || !$type_identifier->getSystem()->isMatch(self::TYPE_INS_SYSTEM) || !$type_identifier->getCode()) {
                    continue;
                }

                $type_code = $type_identifier->getCode()->getValue();
                if ($identifier->getValue() && ($type_code === self::MAPPING_INS_TYPE_CODE[$ins_type])) {
                    return $identifier->getValue()->getValue();
                }
            }
        }

        return null;
    }

    /**
     * Search IPP of patient in resource fhir
     *
     * @param FHIRPatient $resource_patient
     * @param string|null          $group_id
     *
     * @return string|null
     * @throws Exception
     */
    public static function getIPP(FHIRPatient $resource_patient, ?string $group_id): ?string
    {
        $identifier_resolver = (new PIIdentifierResolver())
            ->setGroup(CGroups::get($group_id))
            ->setModeOID();

        foreach ($resource_patient->getIdentifier() ?? [] as $identifier) {
            $system    = $identifier->getSystem() ? $identifier->getSystem()->getValue() : null;
            $id_number = $identifier->getValue() ? $identifier->getValue()->getValue() : null;

            // for now $type_code is required
            foreach ($identifier->getType()->getCoding() ?? [] as $type_coding) {
                $type_code = $type_coding->getCode() ? $type_coding->getCode()->getValue() : null;

                if ($ipp = $identifier_resolver->resolve($id_number, $system, $type_code)) {
                    return $ipp;
                }
            }
        }

        return null;
    }

    public static function getTypeCodingIPP(): FHIRCoding
    {
        return (new FHIRCoding())
            ->setSystem('http://interopsante.org/CodeSystem/fr-v2-0203')
            ->setCode('PI')
            ->setDisplay('Patient internal identifier');
    }

    public static function getTypeCodingINS(): FHIRCoding
    {
        return (new FHIRCoding())
            ->setSystem('http://interopsante.org/CodeSystem/fr-v2-0203')
            ->setCode('INS-NIR')
            ->setDisplay('NIR');
    }

    /**
     * Search patient infos in resource and map on object CPatient
     *
     * @param FHIRPatient $resource_patient
     *
     * @return CPatient|null
     * @throws Exception
     */
    public static function primaryMapping(FHIRPatient $resource_patient): ?CPatient
    {
        $patient = new CPatient();

        // birthdate
        $patient->naissance = $resource_patient->getBirthDate() ? $resource_patient->getBirthDate()->getValue() : null;

        if ($resource_name = self::getOfficialName($resource_patient)) {
            // First name
            if ($resource_name->getFamily() && !$resource_name->getFamily()->isNull()) {
                $patient->nom = $resource_name->getFamily()->getValue();
            }

            // Given name
            if ($resource_name->getGiven()) {
                foreach ($resource_name->getGiven() as $key => $given) {
                    if ($key > 3) {
                        break;
                    }

                    $key = $key === 0 ? 'prenom' : ('_prenom_' . ($key + 1));

                    $patient->{$key} = $given->getValue();
                }
            }
        }

        $patient->deces = $resource_patient->getDeceased() instanceof FHIRDateTime
            ? $resource_patient->getDeceased()->getValue() : null;

        $patient->sexe = self::mapGender($resource_patient->getGender());

        return $patient;
    }

    /**
     * Search official name in the resource
     *
     * @param FHIRPatient $resource_patient
     *
     * @return FHIRHumanName|null
     */
    public static function getOfficialName(FHIRPatient $resource_patient): ?FHIRHumanName
    {
        foreach ($resource_patient->getName() ?? [] as $name) {
            if ($name->getUse() && $name->getUse()->getValue() === 'official') {
                return $name;
            }
        }

        return null;
    }

    /**
     * Map gender of patient
     *
     * @param FHIRCode|null $code
     *
     * @return string|null
     * @throws Exception
     */
    public static function mapGender(?FHIRCode $code): ?string
    {
        if (!$code || !$code->getValue()) {
            return null;
        }

        switch ($code->getValue()) {
            case 'male':
                return 'm';
            case 'female':
                return 'f';
            case 'unknown':
            default:
                return 'i';
        }
    }
}
