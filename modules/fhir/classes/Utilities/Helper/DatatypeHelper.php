<?php

/**
 * @package Mediboard\Fhir\Objects
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\Helper;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\StructureDefinition;
use Ox\Core\CMbArray;
use Ox\Interop\Fhir\ClassMap\FHIRClassMap;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Mediboard\Loinc\CLoinc;

class DatatypeHelper
{
    /** @var string */
    public const REFERENCE_TYPE_CONTAINED = 'contained';
    /** @var string */
    public const REFERENCE_TYPE_ABSOLUTE = 'absolute';
    /** @var string */
    public const REFERENCE_TYPE_RELATIVE = 'relative';

    /**
     * @param FHIRReference $reference
     * @return string|null
     */
    public static function getReferenceID(FHIRReference $reference): ?string
    {
        $reference_value = $reference->getReference() ? $reference->getReference()->getValue() : null;
        if (is_null($reference_value)) {
            return null;
        }

        $reference_type  = self::resolveTypeReference($reference);
        if ($reference_type === self::REFERENCE_TYPE_RELATIVE) {
            $parts = explode('/', $reference_value);

            return end($parts);
        }

        if ($reference_type === self::REFERENCE_TYPE_CONTAINED) {
            return str_starts_with($reference_value, '#') ? substr($reference_value, 1) : null;
        }

        if ($reference_type === self::REFERENCE_TYPE_ABSOLUTE) {
            if (($position_id = strrpos($reference_value, '/')) === false) {
                return null;
            }

            return substr($reference_value, $position_id + 1);
        }

        return null;
    }

    /**
     * @param FHIRReference $reference
     * @return CFHIRResource|null
     */
    public static function resolveResourceTarget(FHIRReference $reference): ?CFHIRResource
    {
        $reference_value = $reference->getReference() ? $reference->getReference()->getValue() : null;
        if (is_null($reference_value)) {
            return null;
        }

        $map = new FHIRClassMap();
        switch (self::resolveTypeReference()) {
            case self::REFERENCE_TYPE_RELATIVE:
                $type = $reference->getType() ? $reference->getType()->getValue() : null;
                if (!$type) {
                    preg_match("#(?'type'^\w+)(?:\/)#", $reference_value, $matches);
                    $type = CMbArray::get($matches, 'type');
                }

                if (!$type) {
                    return null;
                }

                return $map->resource->getResource($type);

            case self::REFERENCE_TYPE_CONTAINED:
                // todo aller chercher dans la resource le contained
                return null;
                break;

            case self::REFERENCE_TYPE_ABSOLUTE:
                return null;
                break;

            default:
                return null;
        }
    }

    /**
     * @param FHIRReference $reference
     * @return string|null
     */
    public static function resolveTypeReference(FHIRReference $reference): ?string
    {
        $reference_value = $reference->getReference() ? $reference->getReference()->getValue() : null;
        if (is_null($reference)) {
            return null;
        }

        // contained
        if (str_starts_with($reference_value, '#')) {
            return self::REFERENCE_TYPE_CONTAINED;
        }

        // absolute
        if (str_starts_with($reference_value, 'http://') || str_starts_with($reference_value, 'https://')) {
            return self::REFERENCE_TYPE_ABSOLUTE;
        }

        // relative
        $type = $reference->getType() ? $reference->getType()->getValue() : null;
        if ($type && str_starts_with($reference_value, "$type/")) {
            return self::REFERENCE_TYPE_RELATIVE;
        } elseif (preg_match("#(?'type'^\w+)(?:\/)#", $reference_value, $matches)) {
            if (!($type = CMbArray::get($matches, 'type'))) {
                return null;
            }

            // try to resolve resource type
            $resource = StructureDefinition::getResourceDefinition($type);
            if (is_null($resource)) {
                return null;
            }

            return self::REFERENCE_TYPE_RELATIVE;
        }

        return null;
    }
}
