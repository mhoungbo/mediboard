<?php

/**
 * @package Mediboard\Fhir\Objects
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\Helper;

use Ox\Components\FhirCore\Interfaces\FHIRResourceInterface;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Mediboard\Loinc\CLoinc;

class ResourceHelper
{
    /**
     * Search Resource identifier (RI)
     *
     * @param FHIRResourceInterface $resource
     *
     * @return string|null
     * @throw CFHIRException
     */
    public static function getResourceIdentifier(FHIRResourceInterface $resource): ?string
    {
        $system_RI = 'http://terminology.hl7.org/CodeSystem/v2-0203';
        $code_RI   = 'RI';

        foreach ($resource->getIdentifier() as $identifier) {
            if ($identifier->getType() && $identifier->getType()->searchCoding(
                    $system_RI,
                    $code_RI
                ) && $identifier->getValue()) {
                return $identifier->getValue()->getValue();
            }
        }

        return null;
    }

    /**
     * Search identifier for system
     *
     * @param FHIRResourceInterface $resource
     * @param string                $system
     *
     * @return string|null
     * @throw CFHIRException
     */
    public static function getIdentifierForSystem(FHIRResourceInterface $resource, string $system): ?string
    {
        foreach ($resource->getIdentifier() as $identifier) {
            $id = $identifier->getValue() ? $identifier->getValue()->getValue() : null;
            if ($identifier->getSystem() && $identifier->getSystem()->isMatch($system) && $id) {
                return $id;
            }
        }

        return null;
    }

    /**
     * @param string $system
     *
     * @return bool
     */
    public static function isLoincSystem(FHIRUri $datatype_uri): bool
    {
        if (!$system = $datatype_uri->getValue()) {
            return false;
        }

        return $system === CLoinc::$system_loinc || $datatype_uri->isMatch(CLoinc::$oid_loinc);
    }
}
