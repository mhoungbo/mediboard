<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\BundleBuilder;

use Ox\Interop\Fhir\Controllers\CFHIRController;
use Ox\Components\FhirCore\Backbone\Interfaces\FHIRBundleEntryResponseInterface;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRBundleEntry;
use Ox\Interop\Fhir\Resources\CFHIRResource;

/**
 * Description
 */
class BuilderBatchTransactionResponse extends BuilderCollection
{
    /**
     * @param FHIRBundleEntryResponseInterface $response
     * @param CFHIRResource|null          $resource
     *
     * @return $this
     */
    public function addResponse(FHIRBundleEntryResponseInterface $response, CFHIRResource $resource = null): self
    {
        $entry = new FHIRBundleEntry();
        if ($resource) {
            $route_params = [
                'resource'    => $resource->getResourceType(),
                'resource_id' => $resource->getResourceId(),
            ];
            $full_url = CFHIRController::getUrl('fhir_read', $route_params);

            $entry->setFullUrl($full_url)
                ->setResource($resource->getFhirResource());
        }

        $entry->setResponse($response);

        $this->bundle->addEntry($entry);

        return $this;
    }
}
