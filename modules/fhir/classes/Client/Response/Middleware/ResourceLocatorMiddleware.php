<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Client\Response\Middleware;

use Ox\Components\FhirCore\Definition\Field;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\R4\StructureDefinition;
use Ox\Core\CMbArray;
use Ox\Interop\Fhir\ClassMap\FHIRClassMap;
use Ox\Interop\Fhir\Client\CFHIRClient;
use Ox\Interop\Fhir\Client\Response\Envelope;
use Ox\Interop\Fhir\Client\Response\Middleware\Stack\StackInterface;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Utilities\Helper\DatatypeHelper;
use Psr\SimpleCache\InvalidArgumentException;

class ResourceLocatorMiddleware implements MiddlewareInterface
{
    /** @var CFHIRClient */
    private $client;

    /** @var string|string[] */
    private $resource_retriever;
    /** @var string */
    private $type_retriever;

    /**
     * @param Envelope $envelope
     * @param StackInterface $stack
     *
     * @return Envelope
     * @throws InvalidArgumentException
     */
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        // Add reference for header Location
        /** @var CFHIRClient $client */
        $this->client = $envelope->last(CFHIRClient::class);
        $interaction = $this->client->getInteraction();
        $option_location = $this->getOption('retrieve_location_header');
        if ($option_location && ($interaction instanceof CFHIRInteractionUpdate || $interaction instanceof CFHIRInteractionCreate)) {
            $response = $envelope->getResponse();
            if ($location_header = $response->getHeader('Location')) {
                $reference = (new FHIRReference())->setReference($location_header);
                $envelope = $envelope->with($reference);
            }
        }

        // search references in resource
        if ($this->supportRetrieveResources($envelope) && $envelope->getResource()) {
            $references = $this->searchReferences($envelope);

            foreach ($references as $reference) {
                $envelope = $envelope->with($reference);
            }
        }

        return $stack->next()->handle($envelope, $stack);
    }

    /**
     * @param string $name
     *
     * @return array|mixed
     */
    private function getOption(string $name)
    {
        $options = $this->client->getOptions();

        return CMbArray::getRecursive($options, $name);
    }

    private function resolveTypeRetrieve(Envelope $envelope): void
    {
        $type = $this->getOption('retrieve_type');
        $resource_retriever = $this->getOption('retrieve_resources') ?: 'none';
        $authorize_types = ['fields', 'class'];
        $is_type_incorrect = !$type || !is_string($type) || !in_array($type, $authorize_types);
        if (!($resource = $envelope->getResource()) || $is_type_incorrect || ($resource_retriever === 'none')) {
            $this->resource_retriever = 'none';
            $this->type_retriever = 'class';

            return;
        }

        $this->type_retriever = $type;
        $this->resource_retriever = $resource_retriever;
        if (is_string($resource_retriever) && ($this->resource_retriever === 'all')) {
            return;
        }

        if (!is_array($this->resource_retriever)) {
            $this->resource_retriever = [$this->resource_retriever];
        }

        if ($type === 'class') {
            $function_filter = fn($class) => is_subclass_of($class, CFHIRResource::class);
        } else {
            $labels = $resource->getResourceFields();
            $function_filter = function (Field $field) use ($labels, $type, $resource) {
                if (in_array($field, $labels)) {
                    $method_get = 'get' . ucfirst($field->getLabel());
                    $datatype = $resource->$method_get();
                    if (($datatype instanceof FHIRReference) && !$datatype->isNull()) {
                        return true;
                    }
                }

                return false;
            };
        }

        $resource_retriever = array_filter($this->resource_retriever, $function_filter);

        $this->resource_retriever = (count($resource_retriever) > 0) ? $resource_retriever : 'none';
    }

    /**
     * @param Envelope $envelope
     *
     * @return bool
     */
    private function supportRetrieveResources(Envelope $envelope): bool
    {
        $this->resolveTypeRetrieve($envelope);

        return $this->resource_retriever && ($this->resource_retriever !== 'none');
    }

    /**
     * @param Envelope $envelope
     *
     * @return array
     * @throws InvalidArgumentException
     */
    private function searchReferences(Envelope $envelope): array
    {
        $resource = $envelope->getResource();
        $map = new FHIRClassMap();

        $references = [];
        foreach ($this->getReferences($resource) as $reference) {
            if ($reference->isNull()) {
                continue;
            }

            // find target resource
            $target_resource = DatatypeHelper::resolveResourceTarget($reference);
            if (is_null($target_resource)) {
                continue;
            }

            if ($this->resource_retriever !== 'all') {
                // filter per type
                $target_resources_class = array_map(
                    'get_class',
                    $map->resource->listResources($target_resource::RESOURCE_TYPE)
                );
                if (count(array_diff($target_resources_class, $this->resource_retriever)) !== 1) {
                    continue;
                }
            }

            if (DatatypeHelper::resolveTypeReference($reference) === DatatypeHelper::REFERENCE_TYPE_RELATIVE) {
                $references[] = $reference;
            }
        }

        return $references;
    }


    /**
     * @param CFHIRResource $resource
     *
     * @return FHIRReference[]
     */
    private function getReferences(CFHIRResource $resource): array
    {

        $search_fields = $resource->getResourceDefinition()->getFields();
        if ($this->type_retriever === 'fields') {
            foreach ($search_fields as $key => $field) {
                if (!in_array($field->getLabel(), $this->resource_retriever)) {
                    unset($search_fields[$key]);
                }
            }
        }

        $fields = [];
        foreach ($search_fields as $field) {
            foreach ($field->getTypes() as $type => $final) {
                $datatype = StructureDefinition::getResourceDefinition($type)->getObjectName();
                if (!($datatype instanceof FHIRReference)) {
                    continue;
                }
            }

            $fields[] = $field;
        }

        return $fields;
    }
}
