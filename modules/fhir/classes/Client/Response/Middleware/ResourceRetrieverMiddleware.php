<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Client\Response\Middleware;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Interop\Fhir\Client\CFHIRClient;
use Ox\Interop\Fhir\Client\Response\Envelope;
use Ox\Interop\Fhir\Client\Response\Middleware\Stack\StackInterface;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Utilities\Helper\DatatypeHelper;
use Psr\SimpleCache\InvalidArgumentException;

class ResourceRetrieverMiddleware implements MiddlewareInterface
{
    /** @var string */
    public const OPTION_RETRIEVE_RESOURCES = 'retrieve_resources';

    /**
     * @param Envelope       $envelope
     * @param StackInterface $stack
     *
     * @return CFHIRResource
     * @throws InvalidArgumentException
     */
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        /** @var CFHIRClient $client */
        $client     = $envelope->last(CFHIRClient::class);
        $references = $envelope->all(FHIRReference::class);
        if (!$client || !$references) {
            return $stack->next()->handle($envelope, $stack);
        }

        $envelope = $envelope->withoutAll(FHIRReference::class);

        // creation call
        $calls = [];
        /** @var FHIRReference[] $reference */
        foreach ($references as $reference) {
            $id              = DatatypeHelper::getReferenceID($reference);
            $target_resource = DatatypeHelper::resolveResourceTarget($reference);
            if (!$id || !$target_resource) {
                continue;
            }
            $supported_interactions = $client->getReceiver()->getAvailableInteractions($target_resource);
            if (!in_array(CFHIRInteractionRead::NAME, $supported_interactions)) {
                continue;
            }
            // todo lier le format au format pass� en option dans la first request
            $interaction = new CFHIRInteractionRead($target_resource);
            $interaction->setResourceId($id);

            $calls[] = [$interaction, $reference];
        }

        // call && mapping
        foreach ($calls as $call) {
            // todo gestion du call en bundle
            /** @var CFHIRInteractionRead $interaction */
            /** @var FHIRReference $reference */
            [$interaction, $reference] = $call;
            $options  = $client->getOptions(['retrieve_resources' => 'none']);
            $response = $client->request($interaction, $options);
            if (!$response->hasError() && ($target_resource = $response->getResource())) {
                // todo FHIR-core
                //$reference->setTargetResource($target_resource);
            }
        }

        return $stack->next()->handle($envelope, $stack);
    }

}
