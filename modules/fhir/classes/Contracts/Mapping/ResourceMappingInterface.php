<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRMeta;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;

/**
 * Description
 */
interface ResourceMappingInterface
{
    /**
     * Map property Id
     *
     * @return FHIRString|null
     */
    public function mapId(): ?FHIRString;

    /**
     * Map property Meta
     *
     * @return FHIRMeta|null
     */
    public function mapMeta(): ?FHIRMeta;

    /**
     * Map property ImplicitRules
     *
     * @return FHIRUri|null
     */
    public function mapImplicitRules(): ?FHIRUri;

    /**
     * Map property Language
     *
     * @return FHIRCode|null
     */
    public function mapLanguage(): ?FHIRCode;
}
