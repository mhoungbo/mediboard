<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUnsignedInt;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRBundleEntry;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRBundleLink;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRSignature;

/**
 * Description
 */
interface BundleMappingInterface extends ResourceMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Bundle";

    /**
     * Map property type
     *
     * @return FHIRCode
     */
    public function mapType(): ?FHIRCode;

    /**
     * Map porperty Identifier
     *
     * @return FHIRIdentifier|null
     */
    public function mapIdentifier(): ?FHIRIdentifier;

    /**
     * Map property timestamp
     *
     * @return FHIRInstant|null
     */
    public function mapTimestamp(): ?FHIRInstant;

    /**
     * Map property total
     *
     * @return FHIRUnsignedInt|null
     */
    public function mapTotal(): ?FHIRUnsignedInt;

    /**
     * Map property link
     *
     * @return FHIRBundleLink[]
     */
    public function mapLink(): array;

    /**
     * Map property entry
     *
     * @return FHIRBundleEntry[]
     */
    public function mapEntry(): array;

    /**
     * Map property signature
     *
     * @return FHIRSignature|null
     */
    public function mapSignature(): ?FHIRSignature;
}
