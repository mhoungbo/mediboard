<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceContent;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceContext;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentReferenceRelatesTo;

/**
 * Description
 */
interface DocumentReferenceMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = 'DocumentReference';

    /**
     * @return FHIRIdentifier|null
     */
    public function mapMasterIdentifier(): ?FHIRIdentifier;

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * @return FHIRCode|null
     */
    public function mapDocStatus(): ?FHIRCode;

    /**
     * @return FHIRCodeableConcept|null
     */
    public function mapType(): ?FHIRCodeableConcept;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function mapCategory(): array;

    /**
     * @return FHIRReference|null
     */
    public function mapSubject(): ?FHIRReference;

    /**
     * @return FHIRInstant|null
     */
    public function mapDate(): ?FHIRInstant;

    /**
     * @return FHIRReference[]
     */
    public function mapAuthor(): array;

    /**
     * @return FHIRReference|null
     */
    public function mapAuthenticator(): ?FHIRReference;

    /**
     * @return FHIRReference|null
     */
    public function mapCustodian(): ?FHIRReference;

    /**
     * @return FHIRDocumentReferenceRelatesTo[]
     */
    public function mapRelatesTo(): array;

    /**
     * @return FHIRString|null
     */
    public function mapDescription(): ?FHIRString;

    /**
     * @return FHIRCodeableConcept[]
     */
    public function mapSecurityLabel(): array;

    /**
     * @return FHIRDocumentReferenceContent[]
     */
    public function mapContent(): array;

    /**
     * @return FHIRDocumentReferenceContext|null
     */
    public function mapContext(): ?FHIRDocumentReferenceContext;
}
