<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCanonical;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementDocument;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementImplementation;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementMessaging;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementRest;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCapabilityStatementSoftware;

/**
 * Description
 */
interface CapabilityStatementMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "CapabilityStatement";

    /**
     * Map property url
     *
     * @return FHIRUri|null
     */
    public function mapUrl(): ?FHIRUri;

    /**
     * Map property version
     *
     * @return FHIRString|null
     */
    public function mapVersion(): ?FHIRString;

    /**
     * Map property name
     *
     * @return FHIRString|null
     */
    public function mapName(): ?FHIRString;

    /**
     * Map property title
     *
     * @return FHIRString|null
     */
    public function mapTitle(): ?FHIRString;

    /**
     * Map property status
     *
     * @return FHIRCode
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property experimental
     *
     * @return FHIRBoolean|null
     */
    public function mapExperimental(): ?FHIRBoolean;

    /**
     * Map property date
     *
     * @return FHIRDateTime|null
     */
    public function mapDate(): ?FHIRDateTime;

    /**
     * Map property publisher
     *
     * @return FHIRString|null
     */
    public function mapPublisher(): ?FHIRString;

    /**
     * Map property contact
     *
     * @return FHIRContactDetail[]
     */
    public function mapContact(): array;

    /**
     * Map property description
     *
     * @return FHIRMarkdown|null
     */
    public function mapDescription(): ?FHIRMarkdown;

    /**
     * Map property useContext
     *
     * @return FHIRUsageContext[]
     */
    public function mapUseContext(): array;

    /**
     * Map property jurisdiction
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapJurisdiction(): array;

    /**
     * Map property purpose
     *
     * @return FHIRMarkdown|null
     */
    public function mapPurpose(): ?FHIRMarkdown;

    /**
     * Map property copyright
     *
     * @return FHIRMarkdown|null
     */
    public function mapCopyright(): ?FHIRMarkdown;

    /**
     * Map property kind
     *
     * @return FHIRCode|null
     */
    public function mapKind(): ?FHIRCode;

    /**
     * Map property instantiates
     *
     * @return FHIRCanonical[]
     */
    public function mapInstantiates(): array;

    /**
     * Map property imports
     *
     * @return FHIRCanonical[]
     */
    public function mapImports(): array;

    /**
     * Map property software
     *
     * @return FHIRCapabilityStatementSoftware|null
     */
    public function mapSoftware(): ?FHIRCapabilityStatementSoftware;

    /**
     * Map property fhirVersion
     *
     * @return FHIRCode
     */
    public function mapFhirVersion(): ?FHIRCode;

    /**
     * Map property format
     *
     * @return FHIRCode[]
     */
    public function mapFormat(): array;

    /**
     * Map property patchFormat
     *
     * @return FHIRCode[]
     */
    public function mapPatchFormat(): array;

    /**
     * Map property implementationGuide
     *
     * @return FHIRCanonical[]
     */
    public function mapImplementationGuide(): array;

    /**
     * Map property rest
     *
     * @return FHIRCapabilityStatementRest[]
     */
    public function mapRest(): array;

    public function mapImplementation(): ?FHIRCapabilityStatementImplementation;

    /**
     * @return FHIRCapabilityStatementMessaging[]
     */
    public function mapMessaging(): array;

    /**
     * @return FHIRCapabilityStatementDocument
     */
    public function mapDocument(): array;
}
