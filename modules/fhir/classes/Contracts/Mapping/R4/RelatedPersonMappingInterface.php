<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRRelatedPersonCommunication;

/**
 * Description
 */
interface RelatedPersonMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "RelatedPerson";

    /**
     * Map property identifier
     *
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property active
     *
     * @return FHIRBoolean|null
     */
    public function mapActive(): ?FHIRBoolean;

    /**
     * Map property patient
     *
     * @return FHIRReference|null
     */
    public function mapPatient(): ?FHIRReference;

    /**
     * Map property Relationship
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapRelationship(): array;

    /**
     * Map property Name
     *
     * @return FHIRHumanName[]
     */
    public function mapName(): array;

    /**
     * Map property Telecom
     *
     * @return FHIRContactPoint[]
     */
    public function mapTelecom(): array;

    /**
     * Map property Gender
     *
     * @return FHIRCode|null
     */
    public function mapGender(): ?FHIRCode;

    /**
     * Map property BirthDate
     *
     * @return FHIRDate|null
     */
    public function mapBirthDate(): ?FHIRDate;

    /**
     * Map property Address
     *
     * @return FHIRAddress[]
     */
    public function mapAddress(): array;

    /**
     * Map property Photo
     *
     * @return FHIRAttachment[]
     */
    public function mapPhoto(): array;

    /**
     * Map property Period
     *
     * @return FHIRPeriod|null
     */
    public function mapPeriod(): ?FHIRPeriod;

    /**
     * Map property Communication
     *
     * @return FHIRRelatedPersonCommunication[]
     */
    public function mapCommunication(): array;
}
