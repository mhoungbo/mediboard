<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRPositiveInt;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUnsignedInt;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRAppointmentParticipant;

/**
 * Description
 */
interface AppointmentMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Appointment";

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property status
     *
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property cancelationReason
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapCancelationReason(): ?FHIRCodeableConcept;

    /**
     * Map property serviceCategory
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapServiceCategory(): array;

    /**
     * Map property serviceType
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapServiceType(): array;

    /**
     * Map property specialty
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapSpecialty(): array;

    /**
     * Map property appointmentType
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapAppointmentType(): ?FHIRCodeableConcept;

    /**
     * Map property reasonCode
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapReasonCode(): array;

    /**
     * Map property reasonReference
     *
     * @return FHIRReference[]
     */
    public function mapReasonReference(): array;

    /**
     * Map property priority
     *
     * @return FHIRUnsignedInt|null
     */
    public function mapPriority(): ?FHIRUnsignedInt;

    /**
     * Map property description
     *
     * @return FHIRString|null
     */
    public function mapDescription(): ?FHIRString;

    /**
     * Map property supportingInformation
     *
     * @return FHIRReference[]
     */
    public function mapSupportingInformation(): array;

    /**
     * Map property start
     *
     * @return FHIRInstant|null
     */
    public function mapStart(): ?FHIRInstant;

    /**
     * Map property end
     *
     * @return FHIRInstant|null
     */
    public function mapEnd(): ?FHIRInstant;

    /**
     * Map property minutesDuration
     *
     * @return FHIRPositiveInt|null
     */
    public function mapMinutesDuration(): ?FHIRPositiveInt;

    /**
     * Map property slot
     *
     * @return FHIRReference[]
     */
    public function mapSlot(string $resource_class): array;

    /**
     * Map property created
     *
     * @return FHIRDateTime|null
     */
    public function mapCreated(): ?FHIRDateTime;

    /**
     * Map property comment
     *
     * @return FHIRString|null
     */
    public function mapComment(): ?FHIRString;

    /**
     * Map property patientInstruction
     *
     * @return FHIRString|null
     */
    public function mapPatientInstruction(): ?FHIRString;

    /**
     * Map property baseOn
     *
     * @return FHIRReference[]
     */
    public function mapBasedOn(): array;

    /**
     * Map property participant
     *
     * @return FHIRAppointmentParticipant[]
     */
    public function mapParticipant(): array;

    /**
     * Map property requestedPeriod
     *
     * @return FHIRPeriod[]
     */
    public function mapRequestedPeriod(): array;
}
