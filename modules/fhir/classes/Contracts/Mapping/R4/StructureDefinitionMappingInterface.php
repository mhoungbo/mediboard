<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCanonical;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRStructureDefinitionContext;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRStructureDefinitionDifferential;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRStructureDefinitionMapping;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRStructureDefinitionSnapshot;

/**
 * Description
 */
interface StructureDefinitionMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "StructureDefinition";

    /**
     * Map property url
     *
     * @return FHIRUri
     */
    public function mapUrl(): ?FHIRUri;

    /**
     * Map property identifier
     *
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property version
     *
     * @return FHIRString|null
     */
    public function mapVersion(): ?FHIRString;

    /**
     * Map property name
     *
     * @return FHIRString|null
     */
    public function mapName(): ?FHIRString;

    /**
     * Map property title
     *
     * @return FHIRString|null
     */
    public function mapTitle(): ?FHIRString;

    /**
     * Map property status
     *
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property experimental
     *
     * @return FHIRBoolean|null
     */
    public function mapExperimental(): ?FHIRBoolean;

    /**
     * Map property date
     *
     * @return FHIRDateTime|null
     */
    public function mapDate(): ?FHIRDateTime;

    /**
     * Map property publisher
     *
     * @return FHIRString|null
     */
    public function mapPublisher(): ?FHIRString;

    /**
     * Map property contact
     *
     * @return FHIRContactDetail[]
     */
    public function mapContact(): array;

    /**
     * Map property description
     *
     * @return FHIRMarkdown|null
     */
    public function mapDescription(): ?FHIRMarkdown;

    /**
     * Map property useContext
     *
     * @return FHIRUsageContext[]
     */
    public function mapUseContext(): array;

    /**
     * Map property jurisdiction
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapJurisdiction(): array;

    /**
     * Map property purpose
     *
     * @return FHIRMarkdown|null
     */
    public function mapPurpose(): ?FHIRMarkdown;

    /**
     * Map property copyright
     *
     * @return FHIRMarkdown|null
     */
    public function mapCopyright(): ?FHIRMarkdown;

    /**
     * Map property keyword
     *
     * @return FHIRCoding[]
     */
    public function mapKeyword(): array;

    /**
     * Map property fhirVersion
     *
     * @return FHIRCode|null
     */
    public function mapFhirVersion(): ?FHIRCode;

    /**
     * Map property kind
     *
     * @return FHIRCode|null
     */
    public function mapKind(): ?FHIRCode;

    /**
     * Map property abstract
     *
     * @return FHIRBoolean|null
     */
    public function mapAbstract(): ?FHIRBoolean;

    /**
     * Map property contextInvariant
     *
     * @return FHIRString[]
     */
    public function mapContextInvariant(): array;

    /**
     * Map property type
     *
     * @return FHIRUri|null
     */
    public function mapType(): ?FHIRUri;

    /**
     * Map property baseDefinition
     *
     * @return FHIRCanonical|null
     */
    public function mapBaseDefinition(): ?FHIRCanonical;

    /**
     * Map property derivation
     *
     * @return FHIRCode|null
     */
    public function mapDerivation(): ?FHIRCode;

    /**
     * @return FHIRStructureDefinitionContext[]
     */
    public function mapContext(): array;

    /**
     * @return FHIRStructureDefinitionMapping[]
     */
    public function mapMapping(): array;

    public function mapSnapshot(): FHIRStructureDefinitionSnapshot;

    public function mapDifferential(): FHIRStructureDefinitionDifferential;
}
