<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPractitionerRoleAvailableTime;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPractitionerRoleNotAvailable;

/**
 * Description
 */
interface PractitionerRoleMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = 'PractitionerRole';

    /**
     * Map property identifier
     *
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property active
     */
    public function mapActive(): ?FHIRBoolean;

    /**
     * Map property period
     */
    public function mapPeriod(): ?FHIRPeriod;

    /**
     * Map property practitioner
     */
    public function mapPractitioner(): ?FHIRReference;

    /**
     * Map property organization
     */
    public function mapOrganization(): ?FHIRReference;

    /**
     * Map property code
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapCode(): array;

    /**
     * Map property speciality
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapSpecialty(): array;

    /**
     * Map property location
     *
     * @return FHIRReference[]
     */
    public function mapLocation(): array;

    /**
     * Map property healthcareService
     *
     * @return FHIRReference[]
     */
    public function mapHealthCareService(): array;

    /**
     * Map property telecom
     *
     * @return FHIRContactPoint[]
     */
    public function mapTelecom(): array;

    /**
     * Map property availableTime
     *
     * @return FHIRPractitionerRoleAvailableTime[]
     */
    public function mapAvailableTime(): array;

    /**
     * Map property notAvailable
     *
     * @return FHIRPractitionerRoleNotAvailable[]
     */
    public function mapNotAvailable(): array;

    /**
     * Map property availabilityExceptions
     */
    public function mapAvailabilityExceptions(): ?FHIRString;

    /**
     * Map property endpoint
     *
     * @return FHIRReference[]
     */
    public function mapEndpoint(): array;
}
