<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIROperationOutcomeIssue;

/**
 * Description
 */
interface OperationOutcomeMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "OperationOutcome";

    /**
     * @return FHIROperationOutcomeIssue[]
     */
    public function mapIssue(): array;
}
