<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRConceptMapGroup;

/**
 * Description
 */
interface ConceptMapMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "ConceptMap";

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property url
     *
     * @return FHIRUri|null
     */
    public function mapUrl(): ?FHIRUri;

    /**
     * Map property version
     *
     * @return FHIRString|null
     */
    public function mapVersion(): ?FHIRString;

    /**
     * Map property name
     *
     * @return FHIRString|null
     */
    public function mapName(): ?FHIRString;

    /**
     * Map property title
     *
     * @return FHIRString|null
     */
    public function mapTitle(): ?FHIRString;

    /**
     * Map property status
     *
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property experimental
     *
     * @return FHIRBoolean|null
     */
    public function mapExperimental(): ?FHIRBoolean;

    /**
     * Map property date
     *
     * @return FHIRDateTime|null
     */
    public function mapDate(): ?FHIRDateTime;

    /**
     * Map property Publisher
     *
     * @return FHIRString|null
     */
    public function mapPublisher(): ?FHIRString;

    /**
     * Map property Contact
     *
     * @return FHIRContactDetail[]
     */
    public function mapContact(): array;

    /**
     * Map property Description
     *
     * @return FHIRMarkdown|null
     */
    public function mapDescription(): ?FHIRMarkdown;

    /**
     * Map property UseContext
     *
     * @return FHIRUsageContext[]
     */
    public function mapUseContext(): array;

    /**
     * Map property Jurisdiction
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapJurisdiction(): array;

    /**
     * Map property Purpose
     *
     * @return FHIRMarkdown|null
     */
    public function mapPurpose(): ?FHIRMarkdown;

    /**
     * Map property CopyRight
     *
     * @return FHIRMarkdown|null
     */
    public function mapCopyright(): ?FHIRMarkdown;

    /**
     * Map property Source
     *
     * @return FHIRElement|null
     */
    public function mapSource(): ?FHIRElement;

    /**
     * Map property Target
     *
     * @return FHIRElement|null
     */
    public function mapTarget(): ?FHIRElement;

    /**
     * Map property Group
     *
     * @return FHIRConceptMapGroup[]
     */
    public function mapGroup(): array;
}
