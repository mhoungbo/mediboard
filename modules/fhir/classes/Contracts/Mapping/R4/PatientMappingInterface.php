<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAttachment;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRHumanName;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDate;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientCommunication;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientContact;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRPatientLink;

/**
 * Description
 */
interface PatientMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Patient";

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property active
     *
     * @return FHIRBoolean
     */
    public function mapActive(): ?FHIRBoolean;

    /**
     * Map property name
     *
     * @return FHIRHumanName[]
     */
    public function mapName(): array;

    /**
     * Map property telecom
     *
     * @return FHIRContactPoint[]
     */
    public function mapTelecom(): array;

    /**
     * Map property gender
     *
     * @return FHIRCode
     */
    public function mapGender(): ?FHIRCode;

    /**
     * Map property birthDate
     *
     * @return FHIRDate
     */
    public function mapBirthDate(): ?FHIRDate;

    /**
     * Map property deceased
     *
     * @return FHIRELement
     */
    public function mapDeceased(): ?FHIRELement;

    /**
     * Map property address
     *
     * @return FHIRAddress[]
     */
    public function mapAddress(): array;

    /**
     * Map property maritalStatus
     *
     * @return FHIRCodeableConcept
     */
    public function mapMaritalStatus(): ?FHIRCodeableConcept;

    /**
     * Map property multipleBirth
     *
     * @return FHIRELement
     */
    public function mapMultipleBirth(): ?FHIRELement;

    /**
     * Map property photo
     *
     * @return FHIRAttachment[]
     */
    public function mapPhoto(): array;

    /**
     * Map property contact
     *
     * @return FHIRPatientContact[]
     */
    public function mapContact(): array;

    /**
     * Map property communication
     *
     * @return FHIRPatientCommunication[]
     */
    public function mapCommunication(): array;

    /**
     * Map property generalPractitioner
     *
     * @return FHIRReference[]
     */
    public function mapGeneralPractitioner(string $resource_class): array;

    /**
     * Map property managingOrganization
     *
     * @return FHIRReference
     */
    public function mapManagingOrganization(): ?FHIRReference;

    /**
     * Map property link
     *
     * @return FHIRPatientLink[]
     */
    public function mapLink(): array;
}
