<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceDeviceName;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceProperty;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceSpecialization;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceUdiCarrier;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDeviceVersion;

/**
 * Description
 */
interface DeviceMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Device";

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property definition
     *
     * @return FHIRReference|null
     */
    public function mapDefinition(): ?FHIRReference;

    /**
     * Map property mapUdiCarrier
     *
     * @return FHIRDeviceUdiCarrier[]
     */
    public function mapUdiCarrier(): array;

    /**
     * Map property status
     *
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property statusReason
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapStatusReason(): array;

    /**
     * Map property distinctIdentifier
     *
     * @return FHIRString|null
     */
    public function mapDistinctIdentifier(): ?FHIRString;

    /**
     * Map property manufacturer
     *
     * @return FHIRString|null
     */
    public function mapManufacturer(): ?FHIRString;

    /**
     * Map property manufactureDate
     *
     * @return FHIRDateTime|null
     */
    public function mapManufactureDate(): ?FHIRDateTime;

    /**
     * Map property expirationDate
     *
     * @return FHIRDateTime|null
     */
    public function mapExpirationDate(): ?FHIRDateTime;

    /**
     * Map property lotNumber
     *
     * @return FHIRString|null
     */
    public function mapLotNumber(): ?FHIRString;

    /**
     * Map property serialNumber
     *
     * @return FHIRString|null
     */
    public function mapSerialNumber(): ?FHIRString;

    /**
     * Map property deviceName
     *
     * @return FHIRDeviceDeviceName[]
     */
    public function mapDeviceName(): array;

    /**
     * Map property modelNumber
     *
     * @return FHIRString|null
     */
    public function mapModelNumber(): ?FHIRString;

    /**
     * Map property partNumber
     *
     * @return FHIRString|null
     */
    public function mapPartNumber(): ?FHIRString;

    /**
     * Map property type
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapType(): ?FHIRCodeableConcept;

    /**
     * Map property specialization
     *
     * @return FHIRDeviceSpecialization[]
     */
    public function mapSpecialization(): array;

    /**
     * Map property version
     *
     * @return FHIRDeviceVersion[]
     */
    public function mapVersion(): array;

    /**
     * Map property property
     *
     * @return FHIRDeviceProperty[]
     */
    public function mapProperty(): array;

    /**
     * Map property patient
     *
     * @return FHIRReference|null
     */
    public function mapPatient(): ?FHIRReference;

    /**
     * Map property owner
     *
     * @return FHIRReference|null
     */
    public function mapOwner(): ?FHIRReference;

    /**
     * Map property contact
     *
     * @return FHIRContactPoint[]
     */
    public function mapContact(): array;

    /**
     * Map property location
     *
     * @return FHIRReference|null
     */
    public function mapLocation(): ?FHIRReference;

    /**
     * Map property url
     *
     * @return FHIRUri|null
     */
    public function mapUrl(): ?FHIRUri;

    /**
     * Map property note
     *
     * @return FHIRAnnotation[]
     */
    public function mapNote(): array;

    /**
     * Map property safety
     *
     * @return FHIRCodeableConcept[]|null
     */
    public function mapSafety(): array;

    /**
     * Map property parent
     *
     * @return FHIRReference|null
     */
    public function mapParent(): ?FHIRReference;
}
