<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRDocumentManifestRelated;

/**
 * Description
 */
interface DocumentManifestMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "DocumentManifest";

    /**
     * Map property masterIdentifier
     *
     * @return FHIRIdentifier|null
     */
    public function mapMasterIdentifier(): ?FHIRIdentifier;

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property status
     *
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property type
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapType(): ?FHIRCodeableConcept;

    /**
     * Map property subject
     *
     * @return FHIRReference|null
     */
    public function mapSubject(): ?FHIRReference;

    /**
     * Map property created
     *
     * @return FHIRDateTime|null
     */
    public function mapCreated(): ?FHIRDateTime;

    /**
     * Map property author
     *
     * @return FHIRReference[]
     */
    public function mapAuthor(): array;

    /**
     * Map property recipient
     *
     * @return FHIRReference[]
     */
    public function mapRecipient(): array;

    /**
     * Map property source
     *
     * @return FHIRUri|null
     */
    public function mapSource(): ?FHIRUri;

    /**
     * Map property description
     *
     * @return FHIRString|null
     */
    public function mapDescription(): ?FHIRString;

    /**
     * Map property content
     *
     * @return FHIRReference[]
     */
    public function mapContent(): array;

    /**
     * Map property related
     *
     * @return FHIRDocumentManifestRelated[]
     */
    public function mapRelated(): array;
}
