<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRBackboneElement;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRObservationComponent;

/**
 * Description
 */
interface ObservationMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Observation";

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property basedOn
     *
     * @return FHIRReference[]
     */
    public function mapBasedOn(): array;

    /**
     * Map property partOf
     *
     * @return FHIRReference[]
     */
    public function mapPartOf(): array;

    /**
     * Map property status
     *
     * @return FHIRCode
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property category
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapCategory(): array;

    /**
     * Map property code
     *
     * @return FHIRCodeableConcept
     */
    public function mapCode(): ?FHIRCodeableConcept;

    /**
     * Map property subject
     *
     * @return FHIRReference
     */
    public function mapSubject(): ?FHIRReference;

    /**
     * Map property focus
     *
     * @return FHIRReference[]
     */
    public function mapFocus(): array;

    /**
     * Map property encounter
     *
     * @return FHIRReference
     */
    public function mapEncounter(): ?FHIRReference;

    /**
     * Map property effective
     *
     * @return FHIRElement
     */
    public function mapEffective(): ?FHIRElement;

    /**
     * Map property issued
     *
     * @return FHIRInstant
     */
    public function mapIssued(): ?FHIRInstant;

    /**
     * Map property performer
     *
     * @return FHIRReference
     */
    public function mapPerformer(): array;

    /**
     * Map property value
     *
     * @return FHIRElement
     */
    public function mapValue(): ?FHIRElement;

    /**
     * Map property dataAbsentReason
     *
     * @return FHIRCodeableConcept
     */
    public function mapDataAbsentReason(): ?FHIRCodeableConcept;

    /**
     * Map property interpretation
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapInterpretation(): array;

    /**
     * Map property note
     *
     * @return FHIRAnnotation[]
     */
    public function mapNote(): array;

    /**
     * Map property bodySite
     *
     * @return FHIRCodeableConcept
     */
    public function mapBodySite(): ?FHIRCodeableConcept;

    /**
     * Map property method
     *
     * @return FHIRCodeableConcept
     */
    public function mapMethod(): ?FHIRCodeableConcept;

    /**
     * Map property specimen
     *
     * @return FHIRReference
     */
    public function mapSpecimen(): ?FHIRReference;

    /**
     * Map property device
     *
     * @return FHIRReference
     */
    public function mapDevice(): ?FHIRReference;

    /**
     * Map property referenceRange
     *
     * @return FHIRBackboneElement[]
     */
    public function mapReferenceRange(): array;

    /**
     * Map property hasMember
     *
     * @return FHIRReference[]
     */
    public function mapHasMember(): array;

    /**
     * Map property derivedFrom
     *
     * @return FHIRReference[]
     */
    public function mapDerivedFrom(): array;

    /**
     * Map property component
     *
     * @return FHIRObservationComponent[]
     */
    public function mapComponent(): array;
}
