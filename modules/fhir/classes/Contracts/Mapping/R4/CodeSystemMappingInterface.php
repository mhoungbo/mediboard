<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactDetail;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRUsageContext;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCanonical;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRMarkdown;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUnsignedInt;
use Ox\Components\FhirCore\Model\Datatypes\FHIRUri;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCodeSystemConcept;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCodeSystemFilter;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRCodeSystemProperty;

/**
 * Description
 */
interface CodeSystemMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "CodeSystem";

    /**
     * Map property url
     *
     *      @return FHIRUri|null
     */
    public function mapUrl(): ?FHIRUri;

    /**
     * Map property identifier
     *
     *      @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property version
     *
     *      @return FHIRString|null
     */
    public function mapVersion(): ?FHIRString;

    /**
     * Map property name
     *
     *      @return FHIRString|null
     */
    public function mapName(): ?FHIRString;

    /**
     * Map property title
     *
     *      @return FHIRString|null
     */
    public function mapTitle(): ?FHIRString;

    /**
     * Map property status
     *
     *      @return FHIRCode
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property experimental
     *
     *      @return FHIRBoolean|null
     */
    public function mapExperimental(): ?FHIRBoolean;

    /**
     * Map property date
     *
     *      @return FHIRDateTime|null
     */
    public function mapDate(): ?FHIRDateTime;

    /**
     * Map property publisher
     *
     *      @return FHIRString|null
     */
    public function mapPublisher(): ?FHIRString;

    /**
     * Map property contact
     *
     *      @return FHIRContactDetail[]
     */
    public function mapContact(): array;

    /**
     * Map property description
     *
     *      @return FHIRMarkdown|null
     */
    public function mapDescription(): ?FHIRMarkdown;

    /**
     * Map property useContext
     *
     *      @return FHIRUsageContext[]
     */
    public function mapUseContext(): array;

    /**
     * Map property jurisdiction
     *
     *      @return FHIRCodeableConcept[]
     */
    public function mapJurisdiction(): array;

    /**
     * Map property purpose
     *
     *      @return FHIRMarkdown|null
     */
    public function mapPurpose(): ?FHIRMarkdown;

    /**
     * Map property copyright
     *
     *      @return FHIRMarkdown
     */
    public function mapCopyright(): FHIRMarkdown;


    /**
     * Map property caseSensitive
     *
     *      @return FHIRBoolean|null
     */
    public function mapCaseSensitive(): ?FHIRBoolean;

    /**
     * Map property valueSet
     *
     *      @return FHIRCanonical|null
     */
    public function mapValueSet(): ?FHIRCanonical;

    /**
     * Map property hierarchyMeaning
     *
     *      @return FHIRCode|null
     */
    public function mapHierarchyMeaning(): ?FHIRCode;

    /**
     * Map property compositional
     *
     *      @return FHIRBoolean|null
     */
    public function mapCompositional(): ?FHIRBoolean;

    /**
     * Map property versionNeeded
     *
     *      @return FHIRBoolean|null
     */
    public function mapVersionNeeded(): ?FHIRBoolean;

    /**
     * Map property content
     *
     *      @return FHIRCode|null
     */
    public function mapContent(): ?FHIRCode;

    /**
     * Map property supplements
     *
     *      @return FHIRCanonical|null
     */
    public function mapSupplements(): ?FHIRCanonical;

    /**
     * Map property count
     *
     *      @return FHIRUnsignedInt|null
     */
    public function mapCount(): ?FHIRUnsignedInt;

    /**
     * Map property filter
     *
     *      @return FHIRCodeSystemFilter[]
     */
    public function mapFilter(): array;

    /**
     * Map property property
     *
     *      @return FHIRCodeSystemProperty[]
     */
    public function mapProperty(): array;

    /**
     * Map property concept
     *
     *      @return FHIRCodeSystemConcept[]
     */
    public function mapConcept(): array;
}
