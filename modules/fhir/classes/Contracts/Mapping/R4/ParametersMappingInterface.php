<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceMappingInterface;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRParametersParameter;

/**
 * Description
 */
interface ParametersMappingInterface extends ResourceMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Parameters";

    /**
     * @return FHIRParametersParameter[]
     */
    public function mapParameter(): array;
}
