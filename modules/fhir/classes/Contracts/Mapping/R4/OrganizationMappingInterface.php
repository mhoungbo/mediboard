<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIROrganizationContact;

/**
 * Description
 */
interface OrganizationMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Organization";

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property active
     *
     * @return FHIRBoolean
     */
    public function mapActive(): ?FHIRBoolean;

    /**
     * Map property type
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapType(): array;

    /**
     * Map property name
     *
     * @return FHIRString|null
     */
    public function mapName(): ?FHIRString;

    /**
     * Map property alias
     *
     * @return FHIRString[]
     */
    public function mapAlias(): array;

    /**
     * Map property telecom
     *
     * @return FHIRContactPoint[]
     */
    public function mapTelecom(): array;

    /**
     * Map property address
     *
     * @return FHIRAddress[]
     */
    public function mapAddress(): array;

    /**
     * Map property partOf
     *
     * @return FHIRReference|null
     */
    public function mapPartOf(): ?FHIRReference;

    /**
     * Map property contact
     *
     * @return FHIROrganizationContact[]
     */
    public function mapContact(): array;

    /**
     * Map property endpoint
     *
     * @return FHIRReference[]
     */
    public function mapEndpoint(): array;
}
