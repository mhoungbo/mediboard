<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAddress;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRContactPoint;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRLocationHoursOfOperation;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRLocationPosition;

/**
 * Description
 */
interface LocationMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Location";

    /**
     * Map property status
     *
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property operationStatus
     *
     * @return FHIRCoding|null
     */
    public function mapOperationalStatus(): ?FHIRCoding;

    /**
     * Map property name
     *
     * @return FHIRString|null
     */
    public function mapName(): ?FHIRString;

    /**
     * Map property alias
     *
     * @return FHIRString[]
     */
    public function mapAlias(): array;

    /**
     * Map property description
     *
     * @return FHIRString|null
     */
    public function mapDescription(): ?FHIRString;

    /**
     * Map property code
     *
     * @return FHIRCode|null
     */
    public function mapMode(): ?FHIRCode;

    /**
     * Map property type
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapType(): array;

    /**
     * Map property telecom
     *
     * @return FHIRContactPoint[]
     */
    public function mapTelecom(): array;

    /**
     * Map property address
     *
     * @return FHIRAddress|null
     */
    public function mapAddress(): ?FHIRAddress;

    /**
     * Map property physicalType
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapPhysicalType(): ?FHIRCodeableConcept;

    /**
     * Map property position
     *
     * @return FHIRLocationPosition|null
     */
    public function mapPosition(): ?FHIRLocationPosition;

    /**
     * Map property managingOrganization
     *
     * @return FHIRReference|null
     */
    public function mapManagingOrganization(): ?FHIRReference;

    /**
     * Map property partOf
     *
     * @return FHIRReference|null
     */
    public function mapPartOf(): ?FHIRReference;

    /**
     * Map property hoursOfOperation
     *
     * @return FHIRLocationHoursOfOperation[]
     */
    public function mapHoursOfOperation(): array;

    /**
     * Map property availabilityExceptions
     *
     * @return FHIRString|null
     */
    public function mapAvailabilityExceptions(): ?FHIRString;

    /**
     * Map property endpoint
     *
     * @return FHIRReference[]
     */
    public function mapEndpoint(): array;
}
