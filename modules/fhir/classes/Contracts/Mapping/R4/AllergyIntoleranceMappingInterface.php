<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRAnnotation;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRDateTime;
use Ox\Components\FhirCore\Model\Datatypes\FHIRElement;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRAllergyIntoleranceReaction;

/**
 * Description
 */
interface AllergyIntoleranceMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "AllergyIntolerance";

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property ClinicalStatus
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapClinicalStatus(): ?FHIRCodeableConcept;

    /**
     * Map property VerificationStatus
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapVerificationStatus(): ?FHIRCodeableConcept;

    /**
     * Map property Type
     *
     * @return FHIRCode
     */
    public function mapType(): ?FHIRCode;

    /**
     * Map property Category
     *
     * @return FHIRCode[]
     */
    public function mapCategory(): array;

    /**
     * Map property Criticality
     *
     * @return FHIRCode
     */
    public function mapCriticality(): ?FHIRCode;

    /**
     * Map property Code
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapCode(): ?FHIRCodeableConcept;

    /**
     * Map property Patient
     *
     * @return FHIRReference|null
     */
    public function mapPatient(): ?FHIRReference;

    /**
     * Map property Encounter
     *
     * @return FHIRReference|null
     */
    public function mapEncounter(): ?FHIRReference;

    /**
     * Map property Onset
     *
     * @return ?FHIRElement
     */
    public function mapOnset(): ?FHIRElement;

    /**
     * Map property RecordedDate
     *
     * @return FHIRDateTime|null
     */
    public function mapRecordedDate(): ?FHIRDateTime;

    /**
     * Map property Recorder
     *
     * @return FHIRReference|null
     */
    public function mapRecorder(): ?FHIRReference;

    /**
     * Map property Asserter
     *
     * @return FHIRReference|null
     */
    public function mapAsserter(): ?FHIRReference;

    /**
     * Map property LastOccurrence
     *
     * @return FHIRDateTime|null
     */
    public function mapLastOccurrence(): ?FHIRDateTime;

    /**
     * Map property end
     *
     * @return FHIRAnnotation[]
     */
    public function mapNote(): array;

    /**
     * Map property minutesDuration
     *
     * @return FHIRAllergyIntoleranceReaction|null
     */
    public function mapReaction(): ?FHIRAllergyIntoleranceReaction;
}
