<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\Datatypes\FHIRInstant;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;

/**
 * Description
 */
interface SlotMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Slot";

    /**
     * Map property identifier
     *
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property serviceCategory
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapServiceCategory(): array;

    /**
     * Map property serviceType
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapServiceType(): array;

    /**
     * Map property specialty
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapSpecialty(): array;

    /**
     * Map property appointment
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapAppointmentType(): ?FHIRCodeableConcept;

    /**
     * Map property schedule
     *
     * @return FHIRReference
     */
    public function mapSchedule(): ?FHIRReference;

    /**
     * Map property status
     *
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property start
     *
     * @return FHIRInstant|null
     */
    public function mapStart(): ?FHIRInstant;

    /**
     * Map property end
     *
     * @return FHIRInstant|null
     */
    public function mapEnd(): ?FHIRInstant;

    /**
     * Map property overbooked
     *
     * @return FHIRBoolean|null
     */
    public function mapOverbooked(): ?FHIRBoolean;

    /**
     * Map property comment
     *
     * @return FHIRString|null
     */
    public function mapComment(): ?FHIRString;
}
