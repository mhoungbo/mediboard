<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCoding;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRDuration;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterClassHistory;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterDiagnosis;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterHospitalization;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterLocation;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIREncounterParticipant;

/**
 * Description
 */
interface EncounterMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Encounter";

    /**
     * Map property identifier
     *
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property status
     *
     * @return FHIRCode|null
     */
    public function mapStatus(): ?FHIRCode;

    /**
     * Map property status
     *
     * @return FHIRCode[]
     */
    public function mapStatusHistory(): array;

    /**
     * Map property class
     *
     * @return FHIRCoding|null
     */
    public function mapClass(): ?FHIRCoding;

    /**
     * Map property class
     *
     * @return FHIREncounterClassHistory[]
     */
    public function mapClassHistory(): array;

    /**
     * Map property type
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapType(): array;

    /**
     * Map property serviceType
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapServiceType(): ?FHIRCodeableConcept;

    /**
     * Map property priority
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapPriority(): ?FHIRCodeableConcept;

    /**
     * Map property subject
     *
     * @return FHIRReference|null
     */
    public function mapSubject(): ?FHIRReference;

    /**
     * Map property episodeOfCare
     *
     * @return FHIRReference[]
     */
    public function mapEpisodeOfCare(): array;

    /**
     * Map property basedOn
     *
     * @return FHIRReference[]
     */
    public function mapBasedOn(): array;

    /**
     * Map property participant
     *
     * @return FHIREncounterParticipant[]
     */
    public function mapParticipant(): array;

    /**
     * Map property appointment
     *
     * @return FHIRReference[]
     */
    public function mapAppointment(): array;

    /**
     * Map property period
     *
     * @return FHIRPeriod|null
     */
    public function mapPeriod(): ?FHIRPeriod;

    /**
     * Map property length
     *
     * @return FHIRDuration|null
     */
    public function mapLength(): ?FHIRDuration;

    /**
     * Map property reasonCode
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapReasonCode(): array;

    /**
     * Map property reasonReference
     *
     * @return FHIRReference[]
     */
    public function mapReasonReference(): array;

    /**
     * Map property account
     *
     * @return FHIRReference[]
     */
    public function mapAccount(): array;

    /**
     * Map property serviceProvider
     *
     * @return FHIRReference|null
     */
    public function mapServiceProvider(): ?FHIRReference;

    /**
     * Map property partOf
     *
     * @return FHIRReference|null
     */
    public function mapPartOf(): ?FHIRReference;

    /**
     * Map property Diagnosis
     *
     * @return FHIREncounterDiagnosis[]
     */
    public function mapDiagnosis(): array;

    /**
     * Map property Hospitalization
     *
     * @return FHIREncounterHospitalization|null
     */
    public function mapHospitalization(): ?FHIREncounterHospitalization;

    /**
     * Map property Location
     *
     * @return FHIREncounterLocation[]
     */
    public function mapLocation(): array;
}
