<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceDomainMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRPeriod;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBoolean;
use Ox\Components\FhirCore\Model\Datatypes\FHIRString;

/**
 * Description
 */
interface ScheduleMappingInterface extends ResourceDomainMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Schedule";

    /**
     * Map property identifier
     *
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property active
     *
     * @return FHIRBoolean|null
     */
    public function mapActive(): ?FHIRBoolean;

    /**
     * Map property serviceCategory
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapServiceCategory(): array;

    /**
     * Map property serviceType
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapServiceType(): array;

    /**
     * Map property specialty
     *
     * @return FHIRCodeableConcept[]
     */
    public function mapSpecialty(): array;

    /**
     * Map property actor
     *
     * @return FHIRReference[]
     */
    public function mapActor(): array;

    /**
     * Map property planningHorizon
     *
     * @return FHIRPeriod|null
     */
    public function mapPlanningHorizon(): ?FHIRPeriod;

    /**
     * Map property comment
     *
     * @return FHIRString|null
     */
    public function mapComment(): ?FHIRString;
}
