<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping\R4;

use Ox\Interop\Fhir\Contracts\Mapping\ResourceMappingInterface;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRCodeableConcept;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\Datatypes\FHIRBase64Binary;
use Ox\Components\FhirCore\Model\Datatypes\FHIRCode;

/**
 * Description
 */
interface BinaryMappingInterface extends ResourceMappingInterface
{
    /** @var string */
    public const RESOURCE_TYPE = "Binary";

    /**
     * @return FHIRIdentifier[]
     */
    public function mapIdentifier(): array;

    /**
     * Map property contentType
     *
     * @return FHIRCode|null
     */
    public function mapContentType(): ?FHIRCode;

    /**
     * Map property data
     *
     * @return FHIRCodeableConcept|null
     */
    public function mapData(): ?FHIRBase64Binary;

    /**
     * Map property securityContext
     *
     * @return FHIRReference|null
     */
    public function mapSecurityContext(): ?FHIRReference;
}
