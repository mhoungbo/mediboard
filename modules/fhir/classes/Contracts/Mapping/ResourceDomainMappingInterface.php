<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Contracts\Mapping;

use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRExtension;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRNarrative;
use Ox\Components\FhirCore\Model\R4\Resources\FHIRResource;

/**
 * Description
 */
interface ResourceDomainMappingInterface extends ResourceMappingInterface
{
    /**
     * Map property Text
     *
     * @return FHIRNarrative|null
     */
    public function mapText(): ?FHIRNarrative;

    /**
     * @return FHIRResource[]
     */
    public function mapContained(): array;

    /**
     * Map property Extension
     *
     * @return FHIRExtension[]
     */
    public function mapExtension(): array;

    /**
     * Map property ModifierExtension
     *
     * @return FHIRExtension[]
     */
    public function mapModifierExtension(): array;
}
