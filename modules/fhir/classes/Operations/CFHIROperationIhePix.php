<?php
/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Operations;

use Ox\Interop\Eai\CDomain;
use Ox\Interop\Fhir\Api\Response\CFHIRResponse;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRIdentifier;
use Ox\Components\FhirCore\Model\Datatypes\Complex\FHIRReference;
use Ox\Components\FhirCore\Model\R4\Backbone\FHIRParametersParameter;
use Ox\Interop\Fhir\Interactions\CFHIRInteraction;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\Parameters\CFHIRResourceParameters;
use Ox\Mediboard\Patients\CPatient;

/**
 * Description
 */
class CFHIROperationIhePix extends CFHIRInteraction
{
    /** @var string */
    public const NAME = 'ihe-pix';

    /**
     * Get the resource method name
     *
     * @return string
     */
    public function getResourceMethodName(): string
    {
        $interaction = preg_replace('/[^\w]/', '_', self::NAME);

        return "operation_$interaction";
    }

    public function getBasePath(): ?string
    {
        return parent::getBasePath() . '/$ihe-pix';
    }

    /**
     * @inheritdoc
     *
     * @param CPatient $result
     */
    public function handleResult(CFHIRResource $resource, $result): CFHIRResponse
    {
        $root = new CFHIRResourceParameters();
        if ($result) {
            $domains = CDomain::loadDomainIdentifiers($result);
            foreach ($domains as $_domain) {
                if (empty($result->_returned_oids) || in_array($_domain->OID, $result->_returned_oids)) {
                    $identiifier = (new FHIRIdentifier())
                        ->setValue($_domain->_identifier->id400)
                        ->setSystem($_domain->OID)
                        ->setUse('official');
                    $parameter   = (new FHIRParametersParameter())
                        ->setName("targetIdentifier")
                        ->setValue($identiifier);

                    $root->addParameter($parameter);
                }
            }

            $reference = (new FHIRReference())
                ->setReference($resource->getResourceType() . "/$result->patient_id");

            $parameter = (new FHIRParametersParameter())
                ->setName("targetId")
                ->setValue($reference);

            $root->addParameter($parameter);
        }

        $this->setResource($root);

        return new CFHIRResponse($this, $this->format);
    }
}
