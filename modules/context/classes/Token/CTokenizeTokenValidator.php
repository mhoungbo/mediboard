<?php
/**
 * @package Mediboard\Context
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Context\Token;

use Ox\Mediboard\Admin\CTokenValidator;

/**
 * Valid handle_commit params
 */
class CTokenizeTokenValidator extends CTokenValidator
{
    protected $patterns           = [
        'post' => [],
        'get'  => [
            'm'              => ['/context/', false],
            'raw'            => ['/tokenize/', false],
            'name'           => ['/.*/', false],
            'firstname'      => ['/.*/', false],
            'birthdate'      => ['/^([0-9]{4})-((?!00)[0-9]{1,2})-((?!00)[0-9]{1,2})$/', false],
            'rpps'           => ['/[a-zA-Z0-9]+/', false],
            'view'           => ['/.*/', false],
            'g'              => ['/[0-9]+/', false],
            'cabinet_id'     => ['/[0-9]+/', false],
            'ext_patient_id' => ['/[0-9]+/', false],
            'context_guid'   => ['/.*/', false],
            'token_username' => ['/.*/', false],

            'ipp'             => ['/.*/', false],
            'nda'             => ['/.*/', false],
            'admit_date'      => ['/.*/', false],
            'group_tag'       => ['/.*/', false],
            'group_idex'      => ['/.*/', false],
            'sejour_tag'      => ['/.*/', false],
            'sejour_idex'     => ['/.*/', false],
            'show_menu'       => ['/.*/', false],
            'RetourURL'       => ['/.*/', false],
            'consultation_id' => ['/[0-9]+/', false],
            'patient_id'      => ['/[0-9]+/', false],
            'rpps_praticien'  => ['/[a-zA-Z0-9]+/', false],
            'numero_finess'   => ['/.*/', false],
            'tabs'            => ['/.*/', false],
        ],
    ];
    protected $authorized_methods = [
        'get',
    ];
}
