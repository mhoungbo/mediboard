<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Invoicing;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the possible values for invoice's treatment type
 */
enum TreatmentTypeEnum: int
{
    use JfseEnumTrait;

    /** @var int */
    case UNDEFINED = 0;

    /** @var int */
    case ISOLATED_ACTS = 1;

    /** @var int */
    case ACTS_SERIE = 2;
}
