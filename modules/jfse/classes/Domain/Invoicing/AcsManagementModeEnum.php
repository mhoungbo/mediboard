<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Invoicing;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the different values for the Acs management mode
 */
enum AcsManagementModeEnum: int
{
    use JfseEnumTrait;

    /** @var int */
    case NO_THIRD_PARTY_AMC = 0;

    /** @var int */
    case COORDINATED_THIRD_PARTY = 1;

    /** @var int */
    case UNIQUE_MANAGEMENT = 2;

    /** @var int */
    case SEPARATED_MANAGEMENT = 3;
}
