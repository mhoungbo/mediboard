<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Invoicing;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * An enumeration for the statuses of an Invoice
 */
enum InvoiceStatusEnum: string
{
    use JfseEnumTrait;

    /** @var string The invoice is being created */
    case PENDING = 'pending';

    /** @var string The invoice has been validated by Jfse*/
    case VALIDATED = 'validated';

    /** @var string The invoice has been sent to the insurance */
    case SENT = 'sent';

    /** @var string The insurance has received a positive acknowledgement by the insurance */
    case ACCEPTED = 'accepted';

    /** @var string The insurance has been rejected by the insurance */
    case REJECTED = 'rejected';

    /** @var string The practitioner has received a payment by the insurance for this invoice */
    case PAID = 'paid';

    /** @var string The payment by the insurance for this invoice has been rejected */
    case PAYMENT_REJECTED = 'payment_rejected';

    /** @var string The invoice will not receive any acknowledgement by the insurance, usually for the degraded mode */
    case NO_ACK_NEEDED = 'no_ack_needed';
}
