<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Invoicing;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the different values for the anonymization causes for an invoice
 */
enum AnonymizationEnum: int
{
    use JfseEnumTrait;

    /** @var int */
    case INAPPLICABLE = 0;

    /** @var int */
    case URGENT_CONTRACEPTIVE_DELIVERY = 1;

    /** @var int */
    case CONTRACEPTIVE_DELIVERY = 2;

    /** @var int */
    case CONTRACEPTION_CONSULTATION = 3;
}
