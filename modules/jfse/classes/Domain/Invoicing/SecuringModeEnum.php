<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Invoicing;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the different values for invoices securing mode
 */
enum SecuringModeEnum: int
{
    use JfseEnumTrait;

    /** @var int */
    case UNSECURED = 0;

    /** @var int */
    case CARDLESS = 1;

    /** @var int */
    case VISIT = 2;

    /** @var int */
    case SECURED = 3;

    /** @var int */
    case DESYNCHRONIZED = 4;

    /** @var int */
    case DEGRADED = 5;
}
