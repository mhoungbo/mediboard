<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Invoicing;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the possible values for the Acs contract types
 */
enum AcsContractTypeEnum: string
{
    use JfseEnumTrait;

    /** @var string */
    case THIRD_PARTY_AMO = 'AMO';

    /** @var string */
    case ACS_A_CONTRACT = 'A';

    /** @var string */
    case ACS_B_CONTRACT = 'B';

    /** @var string */
    case ACS_C_CONTRACT = 'C';
}
