<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Vital;

use Ox\Mediboard\Jfse\JfseEnumTrait;

enum ApCvAcquisitionModeEnum: int
{
    use JfseEnumTrait;

    case NFC = 1;
    case QRCODE = 2;
}
