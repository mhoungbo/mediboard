<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Vital;

use JsonSerializable;
use Ox\Mediboard\Jfse\Domain\AbstractEntity;

class Insured extends AbstractEntity implements JsonSerializable
{
    /** @var string */
    protected $nir;

    /** @var string */
    protected $nir_key;

    /** @var string */
    protected $last_name;

    /** @var string */
    protected $first_name;

    /** @var string */
    protected $birth_name;

    /** @var string */
    protected $regime_code;

    /** @var string */
    protected $regime_label;

    /** @var string */
    protected $managing_fund;

    /** @var string */
    protected $managing_center;

    /** @var string */
    protected $managing_code;

    /** @var string */
    protected $situation_code;

    /** @var string */
    protected $address;

    /** @var string */
    protected $zip_code;

    /** @var string */
    protected $city;

    public function getNir(): ?string
    {
        return $this->nir;
    }

    public function getNirKey(): ?string
    {
        return $this->nir_key;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function getBirthName(): ?string
    {
        return $this->birth_name;
    }

    public function getRegimeCode(): ?string
    {
        return $this->regime_code;
    }

    public function getRegimeLabel(): ?string
    {
        return $this->regime_label ?? "";
    }

    public function getManagingFund(): ?string
    {
        return $this->managing_fund;
    }

    public function getManagingCenter(): ?string
    {
        return $this->managing_center;
    }

    public function getManagingCode(): ?string
    {
        return $this->managing_code;
    }

    /**
     * Sets the managing code, and set it to a default value if the given value is null
     *
     * @param string|null $managing_code
     *
     * @return $this
     */
    public function setManagingCode(?string $managing_code): self
    {
        $managing_code = $managing_code ?: '10';

        $this->managing_code = $managing_code;

        return $this;
    }

    public function getSituationCode(): ?string
    {
        return $this->situation_code;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    public function jsonSerialize(): array
    {
        $data = [
            'nir'             => $this->nir,
            'nir_key'         => $this->nir_key,
            'last_name'       => mb_convert_encoding($this->last_name ?? '', 'UTF-8', 'ISO-8859-1'),
            'first_name'      => mb_convert_encoding($this->first_name ?? '', 'UTF-8', 'ISO-8859-1'),
            'birth_name'      => mb_convert_encoding($this->birth_name ?? '', 'UTF-8', 'ISO-8859-1'),
            'regime_code'     => $this->regime_code,
            'regime_label'    => mb_convert_encoding($this->regime_label ?? '', 'UTF-8', 'ISO-8859-1'),
            'managing_fund'   => $this->managing_fund,
            'managing_center' => $this->managing_center,
            'managing_code'   => $this->managing_code,
            'situation_code'  => $this->situation_code,
        ];

        if ($this->address && trim($this->address) !== '') {
            $data['address'] = mb_convert_encoding(trim($this->address), 'UTF-8', 'ISO-8859-1');
        }

        if ($this->zip_code && '' !== $this->zip_code) {
            $data['zip_code'] = $this->zip_code;
        }

        if ($this->city && '' !== $this->city) {
            $data['city'] = mb_convert_encoding($this->city, 'UTF-8', 'ISO-8859-1');
        }

        return $data;
    }
}
