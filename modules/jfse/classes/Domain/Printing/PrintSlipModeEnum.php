<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Printing;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Represents the different print modes
 */
enum PrintSlipModeEnum: int
{
    use JfseEnumTrait;
    case MODE_ONE_PRINT            = 1;
    case MODE_MULTIPLE_PRINT       = 2;
    case MODE_PRINT_DATE_BOUNDS    = 3;
    case MODE_ONE_OR_SEVERAL_FILES = 4;
}
