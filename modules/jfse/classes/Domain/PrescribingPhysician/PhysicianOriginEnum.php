<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\PrescribingPhysician;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the possible values for the origin of a physician
 */
enum PhysicianOriginEnum: string
{
    use JfseEnumTrait;

    case CORRESPONDANT_PHYSICIAN    = 'O';
    case OTHER_CARE_PATH_SITUATIONS = 'P';
    case OUT_OF_CARE_PATH           = 'S';
    case REFERRING_PHYSICIAN        = 'T';
}
