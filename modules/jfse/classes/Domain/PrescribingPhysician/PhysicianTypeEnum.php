<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\PrescribingPhysician;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the different types of physicians
 */
enum PhysicianTypeEnum: int
{
    use JfseEnumTrait;

    case LIBERAL   = 0;
    case EMPLOYED  = 1;
    case VOLUNTEER = 2;
}
