<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\History;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the different data group type
 */
enum DataGroupTypeEnum: int
{
    use JfseEnumTrait;

    /** @var int */
    case SSV = 0;

    /** @var int */
    case INPUT_STS = 1;

    /** @var int */
    case OUTPUT_STS = 2;

    /** @var int */
    case B2_FSE = 3;

    /** @var int */
    case B2_DRE = 4;
}
