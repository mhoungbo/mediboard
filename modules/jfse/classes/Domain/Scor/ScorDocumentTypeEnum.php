<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Scor;

use Ox\Core\CAppUI;
use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the values for the types of Scor documents
 */
enum ScorDocumentTypeEnum: int
{
    use JfseEnumTrait;

    case PRESCRIPTION = 0;
    case CERFA = 1;
    case DSIP = 2;
    case OTHER = 3;
    case BIZONE_PRESCRIPTION = 4;
    case EXCEPTION_PRESCRIPTION = 5;
    case PREVENTION_COUPON = 6;
    case FLU_COUPON = 7;
    case VITALE_TICKET = 8;
    case EXCEPTION_CERFA = 9;

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return CAppUI::tr("ScorDocumentTypeEnum.{$this->value}");
    }

    /**
     * @param string $label
     *
     * @return static|null
     */
    public static function getFromString(string $label): ?self
    {
        return match (strtolower($label)) {
            'ordonnance' => self::PRESCRIPTION,
            'cerfa' => self::CERFA,
            'dsip' => self::DSIP,
            'autre' => self::OTHER,
            'ordonnance bizone' => self::BIZONE_PRESCRIPTION,
            'ordonnance exception' => self::EXCEPTION_PRESCRIPTION,
            'coupon prévention', 'coupon prevention' => self::PREVENTION_COUPON,
            'coupon grippe' => self::FLU_COUPON,
            'ticket vitale' => self::VITALE_TICKET,
            'cerfa exception' => self::EXCEPTION_CERFA,
            default => null,
        };
    }
}
