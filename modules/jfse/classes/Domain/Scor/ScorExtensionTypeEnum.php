<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Scor;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * An enumeration class that list all the authorized extensions for the SCOR documents
 */
enum ScorExtensionTypeEnum: string
{
    use JfseEnumTrait;

    case PDFA1 = 'PDFA1';

    case TIFF = 'TIFF';

    case JPEG = 'JPEG';

    case PNG = 'PNG';

    case PDF = 'PDF';
}
