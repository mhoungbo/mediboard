<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Scor;

use Exception;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\ApiClients\ScorClient;
use Ox\Mediboard\Jfse\DataModels\CJfseInvoice;
use Ox\Mediboard\Jfse\DataModels\CJfseScorDocument;
use Ox\Mediboard\Jfse\Domain\AbstractService;
use Ox\Mediboard\Jfse\Domain\Invoicing\Invoice;
use Ox\Mediboard\Jfse\Domain\Invoicing\InvoiceStatusEnum;
use Ox\Mediboard\Jfse\Exceptions\DataModelException;
use Ox\Mediboard\Jfse\Mappers\ScorMapper;

/**
 * A service class for the Scor feature
 */
class ScorService extends AbstractService
{
    /** @var ScorClient The ApiClient */
    protected $client;

    /** @var int[] The list of specialities who can use Scor */
    public static array $allowed_specialities = [
        21, 24, 26, 27, 28, 29, 30, 39, 40, 50, 51, 60, 61, 62, 63, 64, 65, 66, 67, 68, 86
    ];

    /**
     * @param ScorClient|null $client
     */
    public function __construct(ScorClient $client = null)
    {
        parent::__construct($client ?? new ScorClient());
    }

    /**
     * Checks if the given speciality is allowed to use Scor
     *
     * @param int $speciality
     *
     * @return bool
     */
    protected static function isSpecialityAllowed(int $speciality): bool
    {
        return in_array($speciality, self::$allowed_specialities);
    }

    /**
     * Checks if the given invoice status is compatible with the Scor feature
     * Jfse do not allow users to link documents if the invoice is not validated
     *
     * @param InvoiceStatusEnum $status
     *
     * @return bool
     */
    protected static function isInvoiceStatusAllowed(?InvoiceStatusEnum $status): bool
    {
        return $status !== InvoiceStatusEnum::PENDING;
    }

    /**
     * Returns the Scor data for the given invoice.
     *
     * Returns null if the speciality of the Jfse user is not allowed to use Scor, or if the invoice is not validated.
     *
     * If the invoice was manually excluded from the scor process, will return an InvoiceScor object, with the excluded
     * flag set to true.
     *
     * Otherwise, will get the linked documents data from the Jfse API (and will create data models for them if they
     * were added in Jfse's UI), and will get the list of missing documents types for the invoice.
     *
     * @param Invoice $invoice
     *
     * @return InvoiceScor|null
     * @throws Exception
     */
    public function getScorInvoiceInformations(Invoice $invoice): ?InvoiceScor
    {
        $user = $invoice->getPractitioner();

        /* Checks that the user speciality is allowed */
        if (!self::isSpecialityAllowed(intval($user->getSituation()->getSpecialityCode()))) {
            return null;
        }

        /* Checks that the status of the invoice is allowed. The Invoice's data model must be set and stored */
        if (!self::isInvoiceValid($invoice)) {
            return null;
        }

        /* Returns the list of missing documents, and the types of linked document */
        $invoice_scor = ScorMapper::getScorInvoiceFromResponse($this->client->listDocumentsForInvoice($invoice));

        if (!$invoice_scor->getExcluded() && $invoice->getDataModel()->scor === '0') {
            $invoice->getDataModel()->scor = '1';
            $invoice->getDataModel()->store();
        }

        foreach ($invoice_scor->getDocuments() as $document) {
            if (!$document->loadDataModel()) {
                /* If the data model of the document does not exists in the database, we get the document full data, and store it */
                ScorMapper::updateDocumentFromResponse($document, $this->client->getDocument($document));

                $consultation = $invoice->getConsultation();

                /* Checks if a similar file exists linked on the consultation */
                $file               = $document->getFile();
                $file->object_class = $consultation->_class;
                $file->loadMatchingObject();

                /* If a similar file is not found on the consultation, we check if it exists on the patient */
                if (!$file->_id) {
                    $consultation->loadRefPatient();
                    $file->object_class = $consultation->_ref_patient->_class;
                    $file->object_id    = $consultation->_ref_patient->_id;
                    $file->loadMatchingObject();

                    if (!$file->_id) {
                        /* If the file still do not exists, we create it (linked on the consultation) */
                        $file->object_class = $consultation->_class;
                        $file->object_id    = $consultation->_id;
                        $file->fillFields();
                        $file->updateFormFields();
                        $file->store();
                    }
                }

                if ($file->_id) {
                    /* If the CFile exists or has been created, we create the data model and link it to the invoice */
                    $document->createDataModel();
                    $document->createLinkToInvoice();
                }
            } elseif (!$document->getLinkDataModel()) {
                /* If the data model of the link between the document and the invoice does not exist, we create it */
                $document->createLinkToInvoice();
            } elseif ($document->getLinkDataModel()->status !== $document->getStatus()->value) {
                /* If the status of the document has changed, we update it */
                $document->updateLinkStatus();
            }

            $file = $document->getFile();
            $file->loadRefCategory();
            $file->_ref_object = $file->loadFwdRef('object_id', true);
        }

        return $invoice_scor;
    }

    /**
     * Checks if the given invoice object has all the conditions and data needed to start the Scor process
     *
     * @param Invoice $invoice
     *
     * @return bool
     */
    public static function isInvoiceValid(Invoice $invoice): bool
    {
        return $invoice->getId() && $invoice->getDataModel()
            && $invoice->getDataModel()->_id && !empty($invoice->getDataModel()->status)
            && self::isInvoiceStatusAllowed(InvoiceStatusEnum::tryFrom($invoice->getDataModel()->status));
    }

    /**
     * Returns the list of missing documents types for the given invoice
     *
     * @param Invoice $invoice
     *
     * @return array
     */
    public function getMissingDocumentTypesForInvoice(Invoice $invoice): array
    {
        $invoice_scor = ScorMapper::getScorInvoiceFromResponse($this->client->listDocumentsForInvoice($invoice));

        return $invoice_scor->getMissingDocumentTypes();
    }

    /**
     * Add the given file to the given invoice.
     *
     * If the document has already been linked to another invoice, will only link the document.
     * Otherwise, the content of the document will be sent to Jfse, and it's data model will be created.
     *
     * @param CFile                $file
     * @param CJfseInvoice         $invoice
     * @param ScorDocumentTypeEnum $type
     *
     * @return bool
     * @throws Exception
     */
    public function addDocument(CFile $file, CJfseInvoice $invoice, ScorDocumentTypeEnum $type): bool
    {
        $user = $invoice->loadJfseUser();

        $data_model = CJfseScorDocument::getFor($file, $user, $type);

        $document_data = [
            'jfse_user_id' => $user->jfse_id,
            'invoice_id'   => $invoice->jfse_id,
            'type'         => $type
        ];

        if ($data_model->_id) {
            $document_data['id'] = $data_model->jfse_id;
            $document_data['file'] = $file;
        } else {
            $document_data['name'] = $file->file_name;
            $document_data['file'] = $file;
        }

        $document = Document::hydrate($document_data);
        $response = $this->client->setDocumentForInvoice($document);

        /* If the API call is successful, Jfse will return the id of the document */
        if (array_key_exists('idDocument', $response->getContent())) {
            $document->setId($response->getContent()['idDocument']);

            /* If the document did not previously exists, we store the data model */
            if (!$data_model->_id) {
                $document->createDataModel();
            } else {
                $document->loadDataModel();
            }

            /* The document is linked to the invoice */
            $document->setStatus(ScorDocumentStatusEnum::LINKED);
            $document->createLinkToInvoice();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the document
     *
     * If the document is linked to multiple invoices, only the link with the given will be deleted.
     * Otherwise, the document will be completely deleted.
     *
     * @param string $document_id
     * @param string $invoice_id
     *
     * @return bool
     * @throws Exception
     * @throws DataModelException
     */
    public function deleteDocument(string $document_id, string $invoice_id): bool
    {
        $document = Document::hydrate([
            'id'         => $document_id,
            'invoice_id' => $invoice_id,
        ]);

        if (!$document->loadDataModel()) {
            throw DataModelException::persistenceError('CScorDocumentView-error-not_found');
        }

        $data_model = $document->getDataModel();
        if (
            $data_model->countInvoiceLinks() > 1 && $document->getLinkDataModel()
            && $document->getLinkDataModel()->status === ScorDocumentStatusEnum::LINKED->value
        ) {
            /* If the Document is linked to multiple invoices, and the link to given invoice has not been sent or bundled, we remove the link only */
            $link = $document->getLinkDataModel();
            $link->loadInvoiceDataModel();

            $this->client->unlinkDocumentToInvoice($document, Invoice::hydrate(['invoice_number' => $link->_invoice_data_model->invoice_number]));
            $document->deleteLinkToInvoice();
        } elseif (
            $document->getLinkDataModel()
            && $document->getLinkDataModel()->status === ScorDocumentStatusEnum::LINKED->value
        ) {
            /* If the document has not been sent or bundled, we remove the document (and therefore the link) */
            $this->client->deleteDocument($document);
            $document->deleteDataModel();
        } else {
            /* We can not delete the document otherwise (the document is bundled or sent), not linked to the current invoice */
            throw DataModelException::persistenceError('CScorDocumentView-error-document_is_sent');
        }

        return true;
    }

    /**
     * Exclude the given invoice from the Scor process
     *
     * @param Invoice $invoice
     *
     * @return bool
     * @throws Exception
     */
    public function excludeInvoice(Invoice $invoice): bool
    {
        $this->client->excludeInvoiceFromScorProcess($invoice);
        $data_model = $invoice->loadDataModel();
        $data_model->scor = '0';
        $data_model->store();

        return true;
    }

    /**
     * Include the given invoice in the Scor process
     *
     * @param Invoice $invoice
     *
     * @return bool
     * @throws Exception
     */
    public function includeInvoice(Invoice $invoice): bool
    {
        $this->client->includeInvoiceInScorProcess($invoice);
        $data_model = $invoice->loadDataModel();
        $data_model->scor = '1';
        $data_model->store();

        return true;
    }

    /**
     * Returns the list of document types that can be added by the current Jfse user
     *
     * @return ScorDocumentTypeEnum[]
     */
    public function getDocumentTypes(): array
    {
        return ScorMapper::getDocumentTypesFromResponse($this->client->getDocumentTypes());
    }
}
