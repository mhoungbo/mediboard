<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Scor;

use Ox\Mediboard\Jfse\Domain\AbstractEntity;

/**
 * Contains the information regarding Scor for an Invoice
 */
class InvoiceScor extends AbstractEntity
{
    /** Document[] Contains the list of documents linked to the invoice */
    protected array $documents = [];

    /** @var ScorDocumentTypeEnum[] Contains the list of documents types that are required and missing */
    protected array $missing_document_types = [];

    /** @var bool Indicate that the invoice is excluded from the Scor process */
    protected bool $excluded = false;

    /**
     * @return Document[]
     */
    public function getDocuments(): array
    {
        return $this->documents;
    }

    /**
     * @return ScorDocumentTypeEnum[]
     */
    public function getMissingDocumentTypes(): array
    {
        return $this->missing_document_types;
    }

    /**
     * @return bool
     */
    public function getExcluded(): bool
    {
        return $this->excluded;
    }
}
