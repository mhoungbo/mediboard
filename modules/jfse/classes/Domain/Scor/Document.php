<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Scor;

use Exception;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\DataModels\CJfseInvoice;
use Ox\Mediboard\Jfse\DataModels\CJfseInvoiceDocumentLink;
use Ox\Mediboard\Jfse\DataModels\CJfseScorDocument;
use Ox\Mediboard\Jfse\DataModels\CJfseUser;
use Ox\Mediboard\Jfse\Domain\AbstractEntity;
use Ox\Mediboard\Jfse\Domain\Invoicing\Invoice;

/**
 * Represents a document in the SCOR process
 */
class Document extends AbstractEntity
{
    /** @var ?string The id of the document given by Jfse */
    protected ?string $id = null;

    /** @var ?int The id of the jfse user  */
    protected ?int $jfse_user_id = null;

    /** @var ?string The id of the linked invoice */
    protected ?string $invoice_id = null;

    /** @var ?ScorDocumentTypeEnum The type of document  */
    protected ?ScorDocumentTypeEnum $type = null;

    /** @var ?ScorDocumentStatusEnum The status of the document towards the invoice */
    protected ?ScorDocumentStatusEnum $status = null;

    /** @var ?string The name of the document  */
    protected ?string $name = null;

    /** @var ?CFile The file object */
    protected ?CFile $file = null;

    /** @var bool A flag that indicate if the document is in paper mode or not */
    protected bool $paper_mode = false;

    /** @var bool Flag that indicate if a CERFA must be automatically generated or not */
    protected bool $generate_document = false;

    /** @var ?Invoice The linked invoice */
    protected ?Invoice $invoice = null;

    /** @var ?CJfseScorDocument The data model */
    protected ?CJfseScorDocument $data_model = null;

    /** @var ?CJfseInvoiceDocumentLink The link to the invoice data model */
    protected ?CJfseInvoiceDocumentLink $link_data_model = null;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getJfseUserId(): ?int
    {
        return $this->jfse_user_id;
    }

    /**
     * @param int|null $jfse_user_id
     */
    public function setJfseUserId(?int $jfse_user_id): void
    {
        $this->jfse_user_id = $jfse_user_id;
    }

    /**
     * @return string|null
     */
    public function getInvoiceId(): ?string
    {
        return $this->invoice_id;
    }

    /**
     * @param string|null $invoice_id
     */
    public function setInvoiceId(?string $invoice_id): void
    {
        $this->invoice_id = $invoice_id;
    }

    /**
     * @return ScorDocumentTypeEnum
     */
    public function getType(): ScorDocumentTypeEnum
    {
        return $this->type;
    }

    /**
     * @param ScorDocumentTypeEnum $type
     */
    public function setType(ScorDocumentTypeEnum $type): void
    {
        $this->type = $type;
    }

    /**
     * @return ScorDocumentStatusEnum|null
     */
    public function getStatus(): ?ScorDocumentStatusEnum
    {
        return $this->status;
    }

    /**
     * @param ScorDocumentStatusEnum|null $status
     */
    public function setStatus(?ScorDocumentStatusEnum $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return CFile|null
     */
    public function getFile(): ?CFile
    {
        return $this->file;
    }

    /**
     * Returns the content of the CFile, encoded in base64
     *
     * @return string|null
     */
    public function getBinaryData(): ?string
    {
        $data = '';
        if ($this->file) {
            $data = base64_encode($this->file->getBinaryContent());
        }

        return $data;
    }

    /**
     * @param CFile|null $file
     */
    public function setFile(?CFile $file): void
    {
        $this->file = $file;
    }

    /**
     * Returns the type of the file
     *
     * @return ScorExtensionTypeEnum|null
     */
    public function getFileType(): ?ScorExtensionTypeEnum
    {
        $type = null;

        if ($this->file) {
            $type = match ($this->file->file_type) {
                'application/pdf' => ScorExtensionTypeEnum::PDF,
                'image/jpg', 'image/jpeg' => ScorExtensionTypeEnum::JPEG,
                'image/tif', 'image/tiff' => ScorExtensionTypeEnum::TIFF,
                'image/png' => ScorExtensionTypeEnum::PNG,
                default => null,
            };
        }

        return $type;
    }

    /**
     * Checks if the given CFile file_type value is valid or not
     *
     * @param string $file_type
     *
     * @return bool
     */
    public static function isFileTypeValid(string $file_type): bool
    {
        return in_array(
            $file_type,
            ['application/pdf', 'image/jpg', 'image/jpeg', 'image/tif', 'image/tiff', 'image/png']
        );
    }

    /**
     * @return bool
     */
    public function isPaperMode(): bool
    {
        return $this->paper_mode;
    }

    /**
     * @param bool $paper_mode
     */
    public function setPaperMode(bool $paper_mode): void
    {
        $this->paper_mode = $paper_mode;
    }

    /**
     * @return bool
     */
    public function getGenerateDocument(): bool
    {
        return $this->generate_document;
    }
    /**
     * @return CJfseScorDocument|null
     */
    public function getDataModel(): ?CJfseScorDocument
    {
        return $this->data_model;
    }

    /**
     * Load the data model, and the link data model (only if the invoice's id is set)
     *
     * @return bool
     * @throws Exception
     */
    public function loadDataModel(): bool
    {
        $result = false;
        if ($this->id) {
            $data_model          = new CJfseScorDocument();
            $data_model->jfse_id = $this->id;

            if ($this->jfse_user_id) {
                $jfse_user = CJfseUser::getFromJfseId($this->jfse_user_id);
                if ($jfse_user) {
                    $data_model->jfse_user_id = $jfse_user->_id;
                }
            }

            if ($this->type) {
                $data_model->type         = $this->type->value;
            }

            $data_model->loadMatchingObject();

            if ($data_model->_id) {
                $this->data_model = $data_model;
                $this->file       = $this->data_model->loadFile();
                $this->name       = $this->data_model->_file->file_name;

                $result = true;

                if ($this->invoice_id) {
                    $invoice_data_model = CJfseInvoice::getFromJfseId($this->invoice_id);
                    if ($invoice_data_model->_id) {
                        $link              = new CJfseInvoiceDocumentLink();
                        $link->document_id = $data_model->_id;
                        $link->invoice_id  = $invoice_data_model->_id;
                        $link->loadMatchingObject();

                        if ($link->_id) {
                            $this->link_data_model = $link;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Creates the data model
     *
     * @return string|null
     * @throws Exception
     */
    public function createDataModel(): ?string
    {
        $jfse_user = CJfseUser::getFromJfseId($this->jfse_user_id);

        if ($jfse_user) {
            $this->data_model               = new CJfseScorDocument();
            $this->data_model->jfse_id      = $this->id;
            $this->data_model->type         = $this->type->value;
            $this->data_model->jfse_user_id = $jfse_user->_id;
            $this->data_model->file_id      = $this->file->_id;

            if ($msg = $this->data_model->store()) {
                return $msg;
            }
        }

        return null;
    }

    /**
     * Deletes the data model
     *
     * @return string|null
     * @throws Exception
     */
    public function deleteDataModel(): ?string
    {
        if ($this->data_model && $this->data_model->_id) {
            return $this->data_model->delete();
        }

        return null;
    }

    /**
     * @return CJfseInvoiceDocumentLink|null
     */
    public function getLinkDataModel(): ?CJfseInvoiceDocumentLink
    {
        return $this->link_data_model;
    }

    /**
     * Creates the link data model between the document and the invoice
     *
     * @return string|null
     * @throws Exception
     */
    public function createLinkToInvoice(): ?string
    {
        if ($this->data_model && $this->data_model->_id && $this->invoice_id) {
            $invoice_data_model = CJfseInvoice::getFromJfseId($this->invoice_id);
            if ($invoice_data_model->_id) {
                $this->link_data_model              = new CJfseInvoiceDocumentLink();
                $this->link_data_model->document_id = $this->data_model->_id;
                $this->link_data_model->invoice_id  = $invoice_data_model->_id;
                $this->link_data_model->status      = $this->status->value;

                return $this->link_data_model->store();
            }
        }

        return null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function deleteLinkToInvoice(): ?string
    {
        if ($this->link_data_model && $this->link_data_model->_id) {
            return $this->link_data_model->delete();
        }

        return null;
    }

    /**
     * Updates the status of the link between the document and the invoice
     *
     * @return string|null
     * @throws Exception
     */
    public function updateLinkStatus(): ?string
    {
        if ($this->link_data_model && $this->link_data_model->_id) {
            $this->link_data_model->status = $this->status->value;
            return $this->link_data_model->store();
        }

        return null;
    }
}
