<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Scor;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the possible status for a Scor Document
 */
enum ScorDocumentStatusEnum: string
{
    use JfseEnumTrait;

    case LINKED = 'linked';
    case BUNDLED = 'bundled';
    case SENT = 'sent';
    case ACCEPTED = 'accepted';
    case REJECTED = 'rejected';
    case MANUAL = 'manual';

    /**
     * @param int $value
     *
     * @return static|null
     */
    public static function getFromStatusInt(int $value): ?self
    {
        return match ($value) {
            -1 => self::LINKED,
            0 => self::SENT,
            3 => self::REJECTED,
            4 => self::ACCEPTED,
            12 => self::MANUAL,
            default => null,
        };
    }
}
