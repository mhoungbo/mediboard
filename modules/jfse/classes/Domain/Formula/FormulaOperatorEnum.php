<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Formula;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Class FormulaOperatorEnum
 *
 * @package Ox\Mediboard\Jfse\Domain\Formula
 */
enum FormulaOperatorEnum: string
{
    use JfseEnumTrait;
    case NO_OPERATOR = '0';
    case SUBSTRACT   = '1';
    case ADD         = '2';
    case MULTIPLY    = '3';
    case DIVIDE      = '4';
}
