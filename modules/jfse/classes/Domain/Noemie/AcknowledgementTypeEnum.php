<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Noemie;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * An enum listing the different types of acknowledgements
 */
enum AcknowledgementTypeEnum: string
{
    use JfseEnumTrait;
    case POSITIVE = 'positive';
    case NEGATIVE = 'negative';
}
