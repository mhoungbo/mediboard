<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\Noemie;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the different possible statuses for the invoice third party payment
 */
enum InvoiceThirdPartyPaymentStatusEnum: string
{
    use JfseEnumTrait;
    case PAID = 'P';

    case PENDING = 'C';

    case REJECTED = 'R';

    /** @var string Used when a payment is made for an invoice, but the AMO and AMC amount do not match the ones computed in the invoice */
    case ANOMALY = 'A';
}
