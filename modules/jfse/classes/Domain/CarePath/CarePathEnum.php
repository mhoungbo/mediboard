<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\CarePath;

use Ox\Mediboard\Jfse\JfseEnumTrait;

enum CarePathEnum: string
{
    use JfseEnumTrait;

    /** @var string Urgence */
    case EMERGENCY = 'U';

    /** @var string Medecin traitant (aka 'RP') */
    case REFERRING_PHYSICIAN = 'T';

    /** @var string Nouveau medecin traitant */
    case NEW_RP = 'N';

    /** @var string Medecin traitant de substitution */
    case RP_SUBSTITUTE = 'R';

    /** @var string Oriente par un medecin traitant */
    case ORIENTED_BY_RP = 'O';

    /** @var string Oriente par un medecin autre que le medecin traitant (Not Reffering Physician aka NRP) */
    case ORIENTED_BY_NRP = 'M';

    /** @var string Generaliste recemment installe */
    case RECENTLY_INSTALLED_RP = 'J';

    /** @var string Medecin installe en zone sous medicalisee */
    case POOR_MEDICALIZED_ZONE = 'B';

    /** @var string Acces direct spécifique */
    case SPECIFIC_DIRECT_ACCESS = 'D';

    /** @var string Hors residence habituelle */
    case OUT_OF_RESIDENCY = 'H';

    /** @var string Hors acces spécifique */
    case NOT_SPECIFIC_ACCESS = 'S1';

    /** @var string Non-respect du parcours / autre medecin */
    case NON_COMPLIANCE_CARE_PATH = 'S2';
}
