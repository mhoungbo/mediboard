<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\CarePath;

use Ox\Mediboard\Jfse\JfseEnumTrait;

enum CarePathStatusEnum: int
{
    use JfseEnumTrait;

    /** @var int  */
    case NOT_CONCERNED = 0;
    /** @var int  */
    case VALID_CARE_PATH = 1;
    /** @var int  */
    case OUTSIDE_CARE_PATH = 2;
}
