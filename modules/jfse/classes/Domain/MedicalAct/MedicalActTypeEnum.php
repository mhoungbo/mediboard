<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\MedicalAct;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the different values for the type of medical act
 */
enum MedicalActTypeEnum: int
{
    use JfseEnumTrait;

    case NGAP = 0;
    case CCAM = 1;
    case ID = 2;
    case IK = 3;
}
