<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\MedicalAct;

use Ox\Mediboard\Jfse\JfseEnumTrait;

/**
 * Contains the different values for the type of LPP acts
 */
enum LppTypeEnum: string
{
    use JfseEnumTrait;

    case BUY = 'A';
    case MAINTENANCE = 'E';
    case RENT = 'L';
    case SHIPPING_FEE = 'P';
    case REPARATION = 'R';
    case SERVICE = 'S';
    case SHIPPING = 'V';
}
