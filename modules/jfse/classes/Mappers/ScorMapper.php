<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Mappers;

use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\Api\Response;
use Ox\Mediboard\Jfse\Domain\Scor\Document;
use Ox\Mediboard\Jfse\Domain\Scor\InvoiceScor;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentStatusEnum;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentTypeEnum;

/**
 * A mapper for the Scor section of the Jfse API
 */
class ScorMapper extends AbstractMapper
{
    /**
     * Returns the data from the given document in the format needed for the Api method SCOR-setDocument
     *
     * @param Document $document
     *
     * @return array
     */
    public static function setDocumentDataFromDocument(Document $document): array
    {
        $data = [
            'idJfse'          => $document->getJfseUserId(),
            'idFacture'       => $document->getInvoiceId(),
            'typeDocument'    => $document->getType()->value,
            'collectePapier'  => intval($document->isPaperMode()),
            'genererDocument' => intval($document->getGenerateDocument()),
        ];

        if ($document->getId()) {
            /* The document already exists in Jfse */
            $data['idDocument'] = $document->getId();
        } else {
            /* The document doesn't exists in Jfse, we need to send it's content */
            $data['nomDocument'] = $document->getName();
            $data['lstBinaires']  = [
                [
                    'typeDocBinaire' => $document->getFileType()?->value,
                    'binaire'        => $document->getBinaryData(),
                ],
            ];
        }

        return $data;
    }

    /**
     * Parse the response of the API method SCOR-getListeFacturesScor and returns an InvoiceScor object
     *
     * @param Response $response
     *
     * @return InvoiceScor
     */
    public static function getScorInvoiceFromResponse(Response $response): InvoiceScor
    {
        $content = $response->getContent();

        $excluded = true;
        $documents         = [];
        $missing_documents = [];
        if (array_key_exists('lstFactures', $content) && count($content['lstFactures']) === 1) {
            $content = reset($content['lstFactures']);
            $excluded = false;

            if (array_key_exists('lstDocumentScors', $content) && count($content['lstDocumentScors'])) {
                foreach ($content['lstDocumentScors'] as $data) {
                    $documents[] = Document::hydrate([
                        'id'           => $data['idScorDoc'],
                        'type'         => ScorDocumentTypeEnum::tryFrom(intval($data['typeDoc'])),
                        'paper_mode'   => boolval($data['collectePapier']),
                        'jfse_user_id' => intval($content['idJfse']),
                        'invoice_id'   => $content['idFacture'],
                        'status'       => ScorDocumentStatusEnum::getFromStatusInt(intval($data['statut'])),
                    ]);
                }
            }

            if (
                array_key_exists('lstDocumentsScorsManquants', $content) && count($content['lstDocumentsScorsManquants'])
            ) {
                foreach ($content['lstDocumentsScorsManquants'] as $data) {
                    if (array_key_exists('code', $data) && ScorDocumentTypeEnum::tryFrom(intval($data['code']))) {
                        $missing_documents[] = ScorDocumentTypeEnum::from(intval($data['code']));
                    }
                }
            }
        }

        return InvoiceScor::hydrate([
            'documents'              => $documents,
            'missing_document_types' => $missing_documents,
            'excluded'                => $excluded,
        ]);;
    }

    /**
     * Update the given document from the response of the SCOR-getDocument API method
     *
     * @param Document $document
     * @param Response $response
     *
     * @return void
     */
    public static function updateDocumentFromResponse(Document $document, Response $response): void
    {
        $content = $response->getContent();

        if (
            array_key_exists('document', $content) && array_key_exists('nomFichier', $content['document'])
            && array_key_exists('binaire', $content['document'])
        ) {
            $document->setName($content['document']['nomFichier']);
            $file            = new CFile();
            $file->file_name = $document->getName();
            $file->setContent(base64_decode($content['document']['binaire']));
            $document->setFile($file);
        }
    }

    /**
     * Parse the response of the API method SCOR-getListeTypesDocumentsPourAjout
     *
     * @param Response $response
     *
     * @return ScorDocumentTypeEnum[]
     */
    public static function getDocumentTypesFromResponse(Response $response): array
    {
        $content = $response->getContent();

        $types = [];

        if (array_key_exists('lstTypeDocumentsPrescriptions', $content)) {
            $types = array_merge($types, self::getTypesFromArray($content['lstTypeDocumentsPrescriptions']));
        }

        if (array_key_exists('lstTypeDocumentsTicketsVitales', $content)) {
            $types = array_merge($types, self::getTypesFromArray($content['lstTypeDocumentsTicketsVitales']));
        }

        if (array_key_exists('lstTypeDocumentsCerfa', $content)) {
            $types = array_merge($types, self::getTypesFromArray($content['lstTypeDocumentsCerfa']));
        }

        if (array_key_exists('lstTypeDocumentsAutres', $content)) {
            $types = array_merge($types, self::getTypesFromArray($content['lstTypeDocumentsAutres']));
        }

        if (array_key_exists('lstTypeDocumentsDevis', $content)) {
            $types = array_merge($types, self::getTypesFromArray($content['lstTypeDocumentsDevis']));
        }

        if (array_key_exists('lstTypeDocumentsDetailFactures', $content)) {
            $types = array_merge($types, self::getTypesFromArray($content['lstTypeDocumentsDetailFactures']));
        }

        return $types;
    }

    /**
     * @param array $data
     *
     * @return ScorDocumentTypeEnum[]
     */
    protected static function getTypesFromArray(array $data): array
    {
        $types = [];
        foreach ($data as $type) {
            if (ScorDocumentTypeEnum::tryFrom(intval($type['code']))) {
                $types[] = ScorDocumentTypeEnum::from(intval($type['code']));
            }
        }

        return $types;
    }
}
