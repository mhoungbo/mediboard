<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Controllers;

use Exception;
use Ox\Core\CCanDo;
use Ox\Core\CView;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Files\CDocumentItem;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\DataModels\CJfseInvoice;
use Ox\Mediboard\Jfse\Domain\Invoicing\Invoice;
use Ox\Mediboard\Jfse\Domain\Scor\Document;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentTypeEnum;
use Ox\Mediboard\Jfse\Domain\Scor\ScorService;
use Ox\Mediboard\Jfse\Responses\SmartyResponse;
use Ox\Mediboard\Jfse\Utils;
use Ox\Mediboard\Patients\CPatient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Jfse controller for the Scor feature
 */
class ScorController extends AbstractController
{
    /** @var string[][] */
    public static $routes = [
        'viewAddDocument' => [
            'method' => 'viewAddDocument',
        ],
        'addDocument' => [
            'method' => 'addDocument',
        ],
        'listDocuments' => [
            'method' => 'showPatientDocuments',
        ],
        'deleteDocument' => [
            'method' => 'deleteDocument',
        ],
        'excludeInvoice' => [
            'method' => 'excludeInvoiceFromScor'
        ],
        'includeInvoice' => [
            'method' => 'includeInvoiceInScor'
        ]
    ];

    /**
     * @return string
     */
    public static function getRoutePrefix(): string
    {
        return 'scor';
    }

    /**
     * Displays the view for adding a document to an invoice
     *
     * @param Request $request
     *
     * @return SmartyResponse
     * @throws Exception
     */
    public function viewAddDocument(Request $request): SmartyResponse
    {
        Utils::setJfseUserIdFromInvoiceId($request->get('invoice_id'));
        $invoice      = CJfseInvoice::getFromJfseId($request->get('invoice_id'));
        $consultation = $invoice->loadConsultation();
        $patient      = $consultation->loadRefPatient();

        $documents = $this->getPatientDocuments($patient);

        return new SmartyResponse('scor/document_add', [
            'invoice_id'   => $request->get('invoice_id'),
            'type'         => $request->get('type'),
            'types'        => (new ScorService())->getDocumentTypes(),
            'consultation' => $consultation,
            'patient'      => $patient,
            'documents'    => $documents,
        ]);
    }

    /**
     * @return Request
     * @throws Exception
     */
    public function viewAddDocumentRequest(): Request
    {
        CCanDo::checkEdit();

        $data = ['invoice_id' => CView::post('invoice_id', 'str notNull'), 'type' => null];
        $type = CView::post('type', 'num');

        if ($type !== null && $type !== '') {
            $data['type'] = ScorDocumentTypeEnum::tryFrom(intval($type));
        }

        return new Request($data);
    }

    /**
     * Displays the list of CDocumentItems linked to the CPatient
     *
     * @param Request $request
     *
     * @return SmartyResponse
     * @throws Exception
     */
    public function showPatientDocuments(Request $request): SmartyResponse
    {
        $consultation = CConsultation::findOrNew($request->get('consultation_id'));
        $patient      = $consultation->loadRefPatient();

        $documents = $this->getPatientDocuments($patient);

        return new SmartyResponse('scor/document_list', [
            'invoice_id'   => $request->get('invoice_id'),
            'consultation' => $consultation,
            'patient'      => $patient,
            'documents'    => $documents,
        ]);
    }

    /**
     * @return Request
     * @throws Exception
     */
    public function showPatientDocumentsRequest(): Request
    {
        return new Request(['consultation_id' => CView::post('consultation_id', 'ref class|CConsultation notNull')]);
    }

    /**
     * Send the document to JFSE
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function addDocument(Request $request): JsonResponse
    {
        Utils::setJfseUserIdFromInvoiceId($request->get('invoice_id'));
        $invoice      = CJfseInvoice::getFromJfseId($request->get('invoice_id'));
        $document = CDocumentItem::loadFromGuid($request->get('document_class') . '-' . $request->get('document_id'));

        if ($document instanceof CCompteRendu) {
            $file = $document->loadFile();
        } else {
            $file = $document;
        }

        $result = (new ScorService())->addDocument(
            $file,
            $invoice,
            ScorDocumentTypeEnum::tryFrom(intval($request->get('type')))
        );

        return new JsonResponse(['success' => $result]);
    }

    /**
     * @return Request
     * @throws Exception
     */
    public function addDocumentRequest(): Request
    {
        CCanDo::checkEdit();

        return new Request([
            'invoice_id'     => CView::post('invoice_id', 'str notNull'),
            'type'           => CView::post('type', ScorDocumentTypeEnum::getProp(false) ),
            'document_class' => CView::post('document_class', 'str notNull'),
            'document_id'    => CView::post('document_id', 'ref meta|document_class notNull'),
        ]);
    }

    /**
     * Deletes the given document
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function deleteDocument(Request $request): JsonResponse
    {
        Utils::setJfseUserIdFromInvoiceId($request->get('invoice_id'));

        $result = (new ScorService())->deleteDocument($request->get('document_id'), $request->get('invoice_id'));

        return new JsonResponse(['success' => $result]);
    }

    /**
     * @return Request
     * @throws Exception
     */
    public function deleteDocumentRequest(): Request
    {
        CCanDo::checkEdit();

        return new Request([], [
            'invoice_id'  => CView::post('invoice_id', 'str notNull'),
            'document_id' => CView::post('document_id', 'str notNull'),
        ]);
    }

    /**
     * Excludes the given invoice from the Scor process
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function excludeInvoiceFromScor(Request $request): JsonResponse
    {
        Utils::setJfseUserIdFromInvoiceId($request->get('invoice_id'));

        $result = (new ScorService())->excludeInvoice(Invoice::hydrate(['id' => $request->get('invoice_id')]));

        return new JsonResponse(['success' => $result]);
    }

    /**
     * @return Request
     * @throws Exception
     */
    public function excludeInvoiceFromScorRequest(): Request
    {
        CCanDo::checkEdit();

        return new Request([], [
            'invoice_id'  => CView::post('invoice_id', 'str notNull'),
        ]);
    }

    /**
     * Includes the given invoice in the Scor process
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function includeInvoiceInScor(Request $request): JsonResponse
    {
        Utils::setJfseUserIdFromInvoiceId($request->get('invoice_id'));

        $result = (new ScorService())->includeInvoice(Invoice::hydrate(['id' => $request->get('invoice_id')]));

        return new JsonResponse(['success' => $result]);
    }

    /**
     * @return Request
     * @throws Exception
     */
    public function includeInvoiceInScorRequest(): Request
    {
        CCanDo::checkEdit();

        return new Request([], [
            'invoice_id'  => CView::post('invoice_id', 'str notNull'),
        ]);
    }


    /**
     * Load the documents (CFile or CCompteRendu) linked to the patient
     *
     * @param CPatient $patient
     *
     * @return CDocumentItem[]
     */
    protected function getPatientDocuments(CPatient $patient): array
    {
        $patient->loadAllDocs(['with_forms' => false]);

        $documents = [];
        foreach ($patient->_all_docs['docitems'] as $_documents) {
            foreach ($_documents as $_key => $document) {
                if ($document instanceof CFile && !Document::isFileTypeValid($document->file_type)) {
                    continue;
                }

                $document->canDo();
                /* Set to false for not displaying the buttons in the file widget */
                $document->_can->edit        = false;
                $documents[$document->_guid] = $document;
            }
        }

        return $documents;
    }
}
