<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\ApiClients;

use Ox\Mediboard\Jfse\Api\Request;
use Ox\Mediboard\Jfse\Api\Response;
use Ox\Mediboard\Jfse\Domain\Invoicing\Invoice;
use Ox\Mediboard\Jfse\Domain\Scor\Document;
use Ox\Mediboard\Jfse\Mappers\ScorMapper;
use Ox\Mediboard\Jfse\Utils;

/**
 * The Scor JFSE Api Client
 */
class ScorClient extends AbstractApiClient
{
    /**
     * Returns the list of documents that are linked or are missing for the given invoice
     *
     * @param Invoice $invoice
     *
     * @return Response
     */
    public function listDocumentsForInvoice(Invoice $invoice): Response
    {
        return self::sendRequest(Request::forge('SCOR-getListeFacturesScor', [
            'getListeFacturesScor' => [
                'noFacture' => $invoice->getInvoiceNumber(),
                'filtreLot' => 0,
            ]
        ]));
    }

    /**
     * Includes the given Invoice in the Scor process
     *
     * @param Invoice $invoice
     *
     * @return Response
     */
    public function includeInvoiceInScorProcess(Invoice $invoice): Response
    {
        return self::sendRequest(Request::forge('SCOR-inclureExclureFactureDeScor', [
            'inclureExclureFactureDeScor' => [
                'idFacture' => $invoice->getId(),
                'idJfse'    => Utils::getJfseUserId(),
                'exclure'   => 0
            ]
        ]));
    }

    /**
     * Excludes the given Invoice from the Scor process
     *
     * @param Invoice $invoice
     *
     * @return Response
     */
    public function excludeInvoiceFromScorProcess(Invoice $invoice): Response
    {
        return self::sendRequest(Request::forge('SCOR-inclureExclureFactureDeScor', [
            'inclureExclureFactureDeScor' => [
                'idFacture' => $invoice->getId(),
                'exclure'   => 1
            ]
        ]));
    }

    /**
     * Returns the list of document types that can be added on invoices
     *
     * @return Response
     */
    public function getDocumentTypes(): Response
    {
        return self::sendRequest(Request::forge('SCOR-getListeTypesDocumentsPourAjout', []));
    }

    /**
     * Adds the given document to an invoice
     *
     * @param Document $document
     *
     * @return Response
     */
    public function setDocumentForInvoice(Document $document): Response
    {
        return self::sendRequest(Request::forge('SCOR-setDocument', [
            'setDocument' => ScorMapper::setDocumentDataFromDocument($document)
        ])->setForceObject(false));
    }

    /**
     * Get the name and the content of the given document
     *
     * @param Document $document
     *
     * @return Response
     */
    public function getDocument(Document $document): Response
    {
        return self::sendRequest(Request::forge('SCOR-getDocument', [
            'getDocument' => [
                'idDocument' => $document->getId()
            ]
        ]));
    }

    /**
     * Deletes the link between a document and an invoice
     *
     * @param Document $document
     * @param Invoice  $invoice
     *
     * @return Response
     */
    public function unlinkDocumentToInvoice(Document $document, Invoice $invoice): Response
    {
        return self::sendRequest(Request::forge('SCOR-deleteLienFseDocument', [
            'deleteLienFseDocument' => [
                'noFacture' => $invoice->getInvoiceNumber(),
                'idDocument' => $document->getId(),
            ]
        ]));
    }

    /**
     * Deletes a document, and all it's links to invoices
     *
     * @param Document $document
     *
     * @return Response
     */
    public function deleteDocument(Document $document): Response
    {
        return self::sendRequest(Request::forge('SCOR-deleteDocument', [
            'deleteDocument' => [
                'idDocument' => $document->getId(),
            ]
        ]));
    }
}
