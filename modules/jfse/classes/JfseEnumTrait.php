<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse;

/**
 * A trait containing useful methods for backed enums.
 *
 * Important, this trait can only be used on backed enums
 */
trait JfseEnumTrait
{
    /**
     * Returns the list of values of the enum
     *
     * @return array
     */
    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    /**
     * Returns the props value for an enum
     *
     * @param bool               $nullable If false, the notNull flag will be added to the prop
     * @param JfseEnumTrait|null $default  If set, will add a default value flag to the prop
     *
     * @return string
     */
    public static function getProp(bool $nullable = true, ?self $default = null): string
    {
        return 'enum list|' . implode('|', self::values())
            . (!$nullable ? ' notNull' : '') . ($default ? " default|$default->value" : '');
    }
}
