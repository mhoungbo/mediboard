<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\ViewModels\Scor;

use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\DataModels\CJfseScorDocument;
use Ox\Mediboard\Jfse\Domain\AbstractEntity;
use Ox\Mediboard\Jfse\Domain\Scor\Document;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentStatusEnum;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentTypeEnum;
use Ox\Mediboard\Jfse\ViewModels\CJfseViewModel;

/**
 * The view model for a Scor document
 */
class CScorDocumentView extends CJfseViewModel
{
    /** @var ?string The id of the document in Jfse */
    public ?string $id = null;

    /** @var string The type of SCOR document */
    public string $type = '';

    /** @var ?string The name of the document */
    public ?string $name = '';

    /** @var string */
    public string $paper_mode = '0';

    /** @var string */
    public string $generate_document = '0';

    /** @var ?string */
    public ?string $status = null;

    /** @var ?CJfseScorDocument */
    public ?CJfseScorDocument $_data_model = null;

    /** @var ?CFile */
    public ?CFile $_file;

    /**
     * @inheritDoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['id']                = 'str notNull';
        $props['type']              = ScorDocumentTypeEnum::getProp();
        $props['name']              = 'str';
        $props['paper_mode']        = 'bool default|0';
        $props['generate_document'] = 'bool default|0';
        $props['status']            = ScorDocumentStatusEnum::getProp();

        return $props;
    }

    /**
     * Create a new view model and sets its properties from the given entity
     *
     * @param AbstractEntity $entity
     *
     * @return static|null
     */
    public static function getFromEntity(AbstractEntity $entity): ?CJfseViewModel
    {
        /** @var Document $entity */
        $view_model = parent::getFromEntity($entity);

        $view_model->type        = $entity->getType()->value;
        $view_model->status      = $entity->getStatus()->value;
        $view_model->_data_model = $entity->getDataModel();
        $view_model->_file       = $entity->getFile();

        return $view_model;
    }
}
