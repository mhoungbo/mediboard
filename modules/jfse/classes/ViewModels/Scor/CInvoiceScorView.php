<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\ViewModels\Scor;

use Ox\Mediboard\Jfse\Domain\AbstractEntity;
use Ox\Mediboard\Jfse\Domain\Scor\InvoiceScor;
use Ox\Mediboard\Jfse\ViewModels\CJfseViewModel;

/**
 * A model object for the InvoiceScor
 */
class CInvoiceScorView extends CJfseViewModel
{
    /** @var CScorDocumentView[] The list of linked documents */
    public array $documents = [];

    /** @var array A list of missing documents types */
    public array $missing_documents_types = [];

    /** @var bool A flag that indicate if the invoice is excluded of the Scor process or not */
    public bool $excluded = false;

    /**
     * Create a new view model and sets its properties from the given entity
     *
     * @param AbstractEntity $entity
     *
     * @return static|null
     */
    public static function getFromEntity(AbstractEntity $entity): ?CJfseViewModel
    {
        /** @var InvoiceScor $entity */
        $view_model = parent::getFromEntity($entity);
        foreach ($entity->getDocuments() as $document) {
            $view_model->documents[] = CScorDocumentView::getFromEntity($document);
        }

        foreach ($entity->getMissingDocumentTypes() as $missing_document_type) {
            $view_model->missing_documents_types[] = $missing_document_type->value;
        }

        $view_model->excluded = $entity->getExcluded();

        return $view_model;
    }
}
