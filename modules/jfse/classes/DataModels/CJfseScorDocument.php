<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\DataModels;

use Exception;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentTypeEnum;

/**
 * Represents a Scor Document sent to JFSE
 */
class CJfseScorDocument extends CJfseDataModel
{
    /** @var ?int Primary key */
    public ?int $jfse_scor_document_id;

    /** @var int The id of the CJfseUser */
    public ?int $jfse_user_id = null;

    /** @var ?string The id given by Jfse to the document */
    public ?string $jfse_id;

    /** @var ?int The id of the CFile */
    public ?int $file_id;

    /** @var ?string The type of SCOR document */
    public ?string $type;

    /** @var ?CFile The file object */
    public ?CFile $_file;

    /** @var ?CJfseUser The CJfseUser object */
    public ?CJfseUser $_user;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'jfse_scor_documents';
        $spec->key   = 'jfse_scor_document_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['jfse_user_id'] = 'ref class|CJfseUser notNull back|jfse_scor_documents';
        $props['jfse_id'] = 'str';
        $props['file_id'] = 'ref class|CFile notNull back|jfse_scor_documents';
        $props['type'] = ScorDocumentTypeEnum::getProp() . ' notNull';

        return $props;
    }

    /**
     * @return CFile|null
     * @throws Exception
     */
    public function loadFile(): ?CFile
    {
        if (!$this->_file) {
            $this->_file = $this->loadFwdRef('file_id', true);
        }

        return $this->_file;
    }

    /**
     * Returns the number of link between the document and invoices
     *
     * @return int
     * @throws Exception
     */
    public function countInvoiceLinks(): int
    {
        return intval($this->countBackRefs('invoice_links'));
    }

    /**
     * @param CFile                $file
     * @param CJfseUser            $user
     * @param ScorDocumentTypeEnum $type
     *
     * @return static
     * @throws Exception
     */
    public static function getFor(CFile $file, CJfseUser $user, ScorDocumentTypeEnum $type): self
    {
        $document = new self();
        $document->jfse_user_id = $user->_id;
        $document->file_id = $file->_id;
        $document->type = $type->value;
        $document->loadMatchingObject();

        return $document;
    }

    /**
     * @param string $jfse_id
     *
     * @return CJfseScorDocument
     * @throws Exception
     */
    public static function getFromJfseId(string $jfse_id): self
    {
        $document          = new self();
        $document->jfse_id = $jfse_id;
        $document->loadMatchingObject();

        return $document;
    }
}
