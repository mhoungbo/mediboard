<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\DataModels;

use Exception;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentStatusEnum;

/**
 * Represents the link between a Scor Document and an Invoice
 */
class CJfseInvoiceDocumentLink extends CJfseDataModel
{
    /** @var ?int Primary key */
    public ?int $jfse_invoice_document_links_id = null;

    /** @var ?int The id of the linked CJfseInvoice */
    public ?int $invoice_id = null;

    /** @var ?int The id of the CJfseScorDocument */
    public ?int $document_id = null;

    /** @var ?int The status of the document towards the FSE */
    public ?string $status = null;

    /** @var ?CJfseInvoice The linked invoice */
    public ?CJfseInvoice $_invoice_data_model = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'jfse_invoice_document_links';
        $spec->key   = 'jfse_invoice_document_link_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['invoice_id'] = 'ref class|CJfseInvoice back|scor_document_links';
        $props['document_id'] = 'ref class|CJfseScorDocument back|invoice_links cascade';
        $props['status'] = ScorDocumentStatusEnum::getProp() . ' default|linked';

        return $props;
    }

    /**
     * @return CJfseInvoice|null
     * @throws Exception
     */
    public function loadInvoiceDataModel(): ?CJfseInvoice
    {
        if (!$this->_invoice_data_model) {
            $this->_invoice_data_model = $this->loadFwdRef('invoice_id', true);
        }

        return $this->_invoice_data_model;
    }
}
