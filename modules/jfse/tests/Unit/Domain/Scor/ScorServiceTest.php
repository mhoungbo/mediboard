<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Tests\Unit\Domain\Scor;

use Exception;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\ApiClients\ScorClient;
use Ox\Mediboard\Jfse\DataModels\CJfseInvoice;
use Ox\Mediboard\Jfse\DataModels\CJfseInvoiceDocumentLink;
use Ox\Mediboard\Jfse\DataModels\CJfsePatient;
use Ox\Mediboard\Jfse\DataModels\CJfseScorDocument;
use Ox\Mediboard\Jfse\DataModels\CJfseUser;
use Ox\Mediboard\Jfse\Domain\Cps\Situation;
use Ox\Mediboard\Jfse\Domain\Invoicing\Invoice;
use Ox\Mediboard\Jfse\Domain\Invoicing\InvoiceStatusEnum;
use Ox\Mediboard\Jfse\Domain\Scor\Document;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentStatusEnum;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentTypeEnum;
use Ox\Mediboard\Jfse\Domain\Scor\ScorService;
use Ox\Mediboard\Jfse\Domain\UserManagement\User;
use Ox\Mediboard\Jfse\Tests\Fixtures\JfseFixtures;
use Ox\Mediboard\Jfse\Tests\Unit\UnitTestJfse;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\TestsException;

/**
 * Tests the ScorService class
 */
class ScorServiceTest extends UnitTestJfse
{
    protected static array $invoice_ids = [];
    protected static array $document_ids = [];
    protected static array $file_names = [];

    /**
     * @throws Exception
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        /* Generate invoices and document ids for the test session.
           This allows the tests to be run multiples times without purging the database */
        if (!count(self::$invoice_ids)) {
            self::$invoice_ids[] = $this->getRandomJfseId();
            self::$invoice_ids[] = $this->getRandomJfseId();
            self::$invoice_ids[] = $this->getRandomJfseId();
        }

        if (!count(self::$document_ids)) {
            self::$document_ids[] = $this->getRandomJfseId();
            self::$document_ids[] = $this->getRandomJfseId();
        }

        if (!count(self::$file_names)) {
            self::$file_names[] = $this->getRandomJfseId();
            self::$file_names[] = $this->getRandomJfseId();
        }

        parent::__construct($name, $data, $dataName);
    }

    /**
     * @dataProvider isInvoiceValidProvider
     *
     * @param Invoice $invoice
     * @param bool    $result
     *
     * @return void
     */
    public function testIsInvoiceValid(Invoice $invoice, bool $result): void
    {
        $this->assertEquals($result, ScorService::isInvoiceValid($invoice));
    }

    /**
     * @return array[]
     */
    public function isInvoiceValidProvider(): array
    {
        $invoice_1 = new Invoice();
        $invoice_2 = Invoice::hydrate(['id' => 'e3564131ze']);
        $invoice_3 = Invoice::hydrate(['id' => 'e3564131ze', 'data_model' => new CJfseInvoice()]);

        $data_model_1 = new CJfseInvoice();
        $data_model_1->_id = 12;

        $invoice_4 = Invoice::hydrate(['id' => 'e3564131ze', 'data_model' => $data_model_1]);

        $data_model_2 = new CJfseInvoice();
        $data_model_2->_id = 12;
        $data_model_2->status = 'pending';

        $invoice_5 = Invoice::hydrate(['id' => 'e3564131ze', 'data_model' => $data_model_2]);

        $data_model_3 = new CJfseInvoice();
        $data_model_3->_id = 12;
        $data_model_3->status = 'validated';


        $invoice_6 = Invoice::hydrate(['id' => 'e3564131ze', 'data_model' => $data_model_3]);

        return [
            [$invoice_1, false],
            [$invoice_2, false],
            [$invoice_3, false],
            [$invoice_4, false],
            [$invoice_5, false],
            [$invoice_6, true]
        ];
    }

    /**
     * @dataProvider getScorInvoiceInformationsFailProvider
     *
     * @param Invoice $invoice
     *
     * @return void
     * @throws Exception
     */
    public function testGetScorInvoiceInformationsFail(Invoice $invoice): void
    {
        $this->assertNull((new ScorService())->getScorInvoiceInformations($invoice));
    }

    /**
     * @return array[]
     */
    public function getScorInvoiceInformationsFailProvider(): array
    {
        $invoice_1 = Invoice::hydrate([
            'practitioner' => User::hydrate([
                'situation' => Situation::hydrate(['speciality_code' => '01'])
            ])
        ]);

        $invoice_2 = Invoice::hydrate([
            'practitioner' => User::hydrate([
                'situation' => Situation::hydrate(['speciality_code' => '24'])
            ])
        ]);

        return [
            /* Unauthorized speciality */
            [$invoice_1],
            /* Invoice without id */
            [$invoice_2]
        ];
    }

    /**
     * Test the case when a Scor document has been added to the invoice in the Jfse GUI
     *
     * @return void
     * @throws TestsException
     */
    public function testGetScorInvoiceInformations(): void
    {
        /** @var CPlageconsult $plage */
        $plage = $this->getObjectFromFixturesReference(CPlageconsult::class, JfseFixtures::PLAGE_NURSE_TAG);
        $patient = $this->getObjectFromFixturesReference(CPatient::class, JfseFixtures::PATIENT_TEST_ALAIN_TAG);
        $jfse_user = $this->getObjectFromFixturesReference(CJfseUser::class, JfseFixtures::JFSE_USER_NURSE_TAG);
        $jfse_patient = $this->getObjectFromFixturesReference(CJfsePatient::class, JfseFixtures::JFSE_PATIENT_TEST_ALAIN_TAG);

        $invoice_id = self::$invoice_ids[0];
        $document_id = self::$document_ids[0];
        $file_name = self::$file_names[0];

        $invoices_response = <<<JSON
{
    "method": {
        "name": "SCOR-getListeFacturesScor",
        "service": "1",
        "parameters": {
            "getListeFacturesScor": {
                "noFacture": "110",
                "filtreLot": "0"
            }
        },
        "output": {
            "lstFactures": [
                {
                    "idJfse": "$jfse_user->jfse_id",
                    "idFacture": "$invoice_id",
                    "noFacture": "000000110",
                    "dateFacture": "20230111",
                    "securisation": "5",
                    "nomBenef": "TEST",
                    "prenomBenef": "Alain",
                    "nir": "172192B99900224",
                    "noLotFse": "",
                    "idLotFse": "0",
                    "lstDocumentScors": [
                        {
                            "idScorDoc": "$document_id",
                            "idScorLot": "0",
                            "typeDoc": "0",
                            "collectePapier": "0",
                            "dateTransScor": "",
                            "statut": "-1",
                            "idSehLot": "",
                            "noDossier": "",
                            "date": ""
                        }
                    ],
                    "lstDocumentsScorsManquants": [
                        {
                            "code": "1",
                            "libelle": "Cerfa"
                        }
                    ]
                }
            ]
        },
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $document_response = <<<JSON
{
    "method": {
        "name": "SCOR-getDocument",
        "service": "1",
        "parameters": {
            "getDocument": {
                "idDocument": "$document_id"
            }
        },
        "output": {
            "document": {
                "nomFichier": "$file_name.pdf",
                "binaire": "a256ZWxuZWxibmx2ZWJucnRibm5ibnJibnJ0a2JucnRsYm5sa2J0cg=="
            }
        },
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses([
            $this->makeJsonGuzzleResponse(200, mb_convert_encoding($invoices_response, 'UTF-8', 'ISO-8859-1')),
            $this->makeJsonGuzzleResponse(200, mb_convert_encoding($document_response, 'UTF-8', 'ISO-8859-1')),
        ]);

        $slots = $plage->getEmptySlots();
        $slot = reset($slots);

        $consultation = new CConsultation();
        $consultation->plageconsult_id = $plage->_id;
        $consultation->chrono = 8;
        $consultation->heure = $slot['hour'];
        $consultation->patient_id = $patient->_id;
        $consultation->store();

        $data_model = new CJfseInvoice();
        $data_model->jfse_id = $invoice_id;
        $data_model->consultation_id = $consultation->_id;
        $data_model->jfse_user_id = $jfse_user->_id;
        $data_model->jfse_patient_id = $jfse_patient->_id;
        $data_model->status = InvoiceStatusEnum::VALIDATED->value;
        $data_model->scor = '0';
        $data_model->store();

        $invoice = Invoice::hydrate([
            'practitioner' => User::hydrate([
                'situation' => Situation::hydrate(['speciality_code' => '24'])
            ]),
            'id' => $data_model->jfse_id,
            'data_model' => $data_model,
            'consultation' => $consultation
        ]);

        $service = new ScorService(new ScorClient($client));
        $scor = $service->getScorInvoiceInformations($invoice);

        $documents = $scor->getDocuments();
        /** @var Document $document */
        $document = reset($documents);

        $this->assertEquals('1', $invoice->getDataModel()->scor);
        $this->assertNotNull($document->getFile()->_id);
        $this->assertNotNull($document->getDataModel()->_id);
        $this->assertNotNull($document->getLinkDataModel()->_id);
    }

    /**
     * Test the case when a Scor document has been linked to a second invoice
     *
     * @return void
     * @throws TestsException
     */
    public function testGetScorInvoiceInformationsExistingDocument(): void
    {
        /** @var CPlageconsult $plage */
        $plage = $this->getObjectFromFixturesReference(CPlageconsult::class, JfseFixtures::PLAGE_NURSE_TAG);
        $patient = $this->getObjectFromFixturesReference(CPatient::class, JfseFixtures::PATIENT_TEST_ALAIN_TAG);
        $jfse_user = $this->getObjectFromFixturesReference(CJfseUser::class, JfseFixtures::JFSE_USER_NURSE_TAG);
        $jfse_patient = $this->getObjectFromFixturesReference(CJfsePatient::class, JfseFixtures::JFSE_PATIENT_TEST_ALAIN_TAG);

        $invoice_id = self::$invoice_ids[1];
        $document_id = self::$document_ids[0];

        $invoices_response = <<<JSON
{
    "method": {
        "name": "SCOR-getListeFacturesScor",
        "service": "1",
        "parameters": {
            "getListeFacturesScor": {
                "noFacture": "110",
                "filtreLot": "0"
            }
        },
        "output": {
            "lstFactures": [
                {
                    "idJfse": "$jfse_user->jfse_id",
                    "idFacture": "$invoice_id",
                    "noFacture": "000000110",
                    "dateFacture": "20230111",
                    "securisation": "5",
                    "nomBenef": "TEST",
                    "prenomBenef": "Alain",
                    "nir": "172192B99900224",
                    "noLotFse": "",
                    "idLotFse": "0",
                    "lstDocumentScors": [
                        {
                            "idScorDoc": "$document_id",
                            "idScorLot": "0",
                            "typeDoc": "0",
                            "collectePapier": "0",
                            "dateTransScor": "",
                            "statut": "-1",
                            "idSehLot": "",
                            "noDossier": "",
                            "date": ""
                        }
                    ],
                    "lstDocumentsScorsManquants": [
                        {
                            "code": "1",
                            "libelle": "Cerfa"
                        }
                    ]
                }
            ]
        },
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses([
            $this->makeJsonGuzzleResponse(200, mb_convert_encoding($invoices_response, 'UTF-8', 'ISO-8859-1')),
        ]);

        $slots = $plage->getEmptySlots();
        $slot = reset($slots);

        $consultation = new CConsultation();
        $consultation->plageconsult_id = $plage->_id;
        $consultation->chrono = 8;
        $consultation->heure = $slot['hour'];
        $consultation->patient_id = $patient->_id;
        $consultation->store();

        $data_model = new CJfseInvoice();
        $data_model->jfse_id = $invoice_id;
        $data_model->consultation_id = $consultation->_id;
        $data_model->jfse_user_id = $jfse_user->_id;
        $data_model->jfse_patient_id = $jfse_patient->_id;
        $data_model->status = InvoiceStatusEnum::VALIDATED->value;
        $data_model->scor = '0';
        $data_model->store();

        $invoice = Invoice::hydrate([
            'practitioner' => User::hydrate([
                'situation' => Situation::hydrate(['speciality_code' => '24'])
            ]),
            'id' => $data_model->jfse_id,
            'data_model' => $data_model,
            'consultation' => $consultation
        ]);

        $document_data_model = new CJfseScorDocument();
        $document_data_model->jfse_id = $document_id;
        $document_data_model->loadMatchingObject();
        $document_data_model->loadFile();

        $service = new ScorService(new ScorClient($client));
        $scor = $service->getScorInvoiceInformations($invoice);

        $documents = $scor->getDocuments();
        /** @var Document $document */
        $document = reset($documents);

        $this->assertEquals('1', $invoice->getDataModel()->scor);
        $this->assertEquals($document_data_model->_id, $document->getDataModel()->_id);
        $this->assertEquals($document_data_model->_file->_id, $document->getFile()->_id);
        $this->assertNotNull($document->getLinkDataModel()->_id);
    }

    /**
     * Test the case when a Scor document status has changed
     *
     * @return void
     * @throws TestsException
     */
    public function testGetScorInvoiceInformationsModifiedStatus(): void
    {
        $jfse_user = $this->getObjectFromFixturesReference(CJfseUser::class, JfseFixtures::JFSE_USER_NURSE_TAG);

        $invoice_id = self::$invoice_ids[0];
        $document_id = self::$document_ids[0];

        $invoices_response = <<<JSON
{
    "method": {
        "name": "SCOR-getListeFacturesScor",
        "service": "1",
        "parameters": {
            "getListeFacturesScor": {
                "noFacture": "110",
                "filtreLot": "0"
            }
        },
        "output": {
            "lstFactures": [
                {
                    "idJfse": "$jfse_user->jfse_id",
                    "idFacture": "$invoice_id",
                    "noFacture": "000000110",
                    "dateFacture": "20230111",
                    "securisation": "5",
                    "nomBenef": "TEST",
                    "prenomBenef": "Alain",
                    "nir": "172192B99900224",
                    "noLotFse": "",
                    "idLotFse": "0",
                    "lstDocumentScors": [
                        {
                            "idScorDoc": "$document_id",
                            "idScorLot": "0",
                            "typeDoc": "0",
                            "collectePapier": "0",
                            "dateTransScor": "",
                            "statut": "0",
                            "idSehLot": "",
                            "noDossier": "",
                            "date": ""
                        }
                    ],
                    "lstDocumentsScorsManquants": [
                        {
                            "code": "1",
                            "libelle": "Cerfa"
                        }
                    ]
                }
            ]
        },
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses([
            $this->makeJsonGuzzleResponse(200, mb_convert_encoding($invoices_response, 'UTF-8', 'ISO-8859-1')),
        ]);

        $data_model = new CJfseInvoice();
        $data_model->jfse_id = $invoice_id;
        $data_model->loadMatchingObject();

        $consultation = $data_model->loadConsultation();

        $invoice = Invoice::hydrate([
            'practitioner' => User::hydrate([
                'situation' => Situation::hydrate(['speciality_code' => '24'])
            ]),
            'id' => $data_model->jfse_id,
            'data_model' => $data_model,
            'consultation' => $consultation
        ]);

        $document_data_model = new CJfseScorDocument();
        $document_data_model->jfse_id = $document_id;
        $document_data_model->loadMatchingObject();
        $document_data_model->loadFile();

        $link_data_model = new CJfseInvoiceDocumentLink();
        $link_data_model->invoice_id = $data_model->_id;
        $link_data_model->document_id = $document_data_model->_id;
        $link_data_model->loadMatchingObject();

        $service = new ScorService(new ScorClient($client));
        $scor = $service->getScorInvoiceInformations($invoice);

        $documents = $scor->getDocuments();
        /** @var Document $document */
        $document = reset($documents);

        $this->assertEquals('1', $invoice->getDataModel()->scor);
        $this->assertEquals($document_data_model->_id, $document->getDataModel()->_id);
        $this->assertEquals($document_data_model->_file->_id, $document->getFile()->_id);
        $this->assertEquals($link_data_model->_id, $document->getLinkDataModel()->_id);
        $this->assertEquals(ScorDocumentStatusEnum::SENT->value, $document->getLinkDataModel()->status);
    }

    /**
     * @return void
     */
    public function testGetMissingDocumentTypesForInvoice(): void
    {
        $response = <<<JSON
{
    "method": {
        "name": "SCOR-getListeFacturesScor",
        "service": "1",
        "parameters": {
            "getListeFacturesScor": {
                "noFacture": "110",
                "filtreLot": "0"
            }
        },
        "output": {
            "lstFactures": [
                {
                    "idJfse": "16",
                    "idFacture": "1673431038357877001",
                    "noFacture": "000000110",
                    "dateFacture": "20230111",
                    "securisation": "5",
                    "nomBenef": "TEST",
                    "prenomBenef": "Alain",
                    "nir": "172192B99900224",
                    "noLotFse": "",
                    "idLotFse": "0",
                    "lstDocumentScors": [
                        {
                            "idScorDoc": "1673517388192862301",
                            "idScorLot": "0",
                            "typeDoc": "0",
                            "collectePapier": "0",
                            "dateTransScor": "",
                            "statut": "-1",
                            "idSehLot": "",
                            "noDossier": "",
                            "date": ""
                        }
                    ],
                    "lstDocumentsScorsManquants": [
                        {
                            "code": "1",
                            "libelle": "Cerfa"
                        },
                        {
                            "code": "2",
                            "libelle": "DSIP"
                        },
                        {
                            "code": 58,
                            "libelle": "Invalid document type"
                        }
                    ]
                }
            ]
        },
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );

        $expected = [
            ScorDocumentTypeEnum::CERFA,
            ScorDocumentTypeEnum::DSIP
        ];

        $actual = (new ScorService(new ScorClient($client)))->getMissingDocumentTypesForInvoice(
            Invoice::hydrate(['invoice_number' => 12])
        );

        $this->assertEquals($expected, $actual);
    }

    /**
     * Tests the case when the document data model does not exist
     *
     * @return void
     * @throws TestsException
     */
    public function testAddDocumentDataModelCreation(): void
    {
        $jfse_user = $this->getObjectFromFixturesReference(CJfseUser::class, JfseFixtures::JFSE_USER_NURSE_TAG);
        $invoice_id  = self::$invoice_ids[1];
        $document_id = self::$document_ids[1];
        $file_name   = self::$file_names[1];
        $type = ScorDocumentTypeEnum::PRESCRIPTION;

        $invoice_dm = CJfseInvoice::getFromJfseId($invoice_id);
        $consultation = $invoice_dm->loadConsultation();

        $file = new CFile();
        $file->object_class = $consultation->_class;
        $file->object_id = $consultation->_id;
        $file->file_name = "$file_name.pdf";
        $file->file_type = 'application/pdf';
        $file->setContent(bin2hex(random_bytes(64)));
        $file->fillFields();
        $file->updateFormFields();
        $file->store();

        $response = <<<JSON
{
    "method": {
        "name": "SCOR-setDocument",
        "service": "1",
        "parameters": {
            "setDocument": {
            }
        },
        "output": {
            "idDocument": "$document_id"
        },
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );

        $result = (new ScorService(new ScorClient($client)))->addDocument($file, $invoice_dm, $type);

        $document_dm = CJfseScorDocument::getFor($file, $jfse_user, $type);

        $this->assertTrue($result);
        $this->assertNotNull($document_dm->_id);
    }

    /**
     * Test the case when the data model already exists
     *
     * @return void
     * @throws TestsException
     */
    public function testAddDocumentDataModelExists(): void
    {
        /** @var CPlageconsult $plage */
        $plage = $this->getObjectFromFixturesReference(CPlageconsult::class, JfseFixtures::PLAGE_NURSE_TAG);
        $patient = $this->getObjectFromFixturesReference(CPatient::class, JfseFixtures::PATIENT_TEST_ALAIN_TAG);
        $jfse_user = $this->getObjectFromFixturesReference(CJfseUser::class, JfseFixtures::JFSE_USER_NURSE_TAG);
        $jfse_patient = $this->getObjectFromFixturesReference(CJfsePatient::class, JfseFixtures::JFSE_PATIENT_TEST_ALAIN_TAG);

        $invoice_id  = self::$invoice_ids[2];
        $document_id = self::$document_ids[0];
        $type = ScorDocumentTypeEnum::PRESCRIPTION;


        $slots = $plage->getEmptySlots();
        $slot = reset($slots);

        $consultation = new CConsultation();
        $consultation->plageconsult_id = $plage->_id;
        $consultation->chrono = 8;
        $consultation->heure = $slot['hour'];
        $consultation->patient_id = $patient->_id;
        $consultation->store();

        $invoice_dm = new CJfseInvoice();
        $invoice_dm->jfse_id = $invoice_id;
        $invoice_dm->consultation_id = $consultation->_id;
        $invoice_dm->jfse_user_id = $jfse_user->_id;
        $invoice_dm->jfse_patient_id = $jfse_patient->_id;
        $invoice_dm->status = InvoiceStatusEnum::VALIDATED->value;
        $invoice_dm->scor = '0';
        $invoice_dm->store();

        $document_dm = new CJfseScorDocument();
        $document_dm->jfse_id = $document_id;
        $document_dm->loadMatchingObject();

        $file = $document_dm->loadFile();

        $response = <<<JSON
{
    "method": {
        "name": "SCOR-setDocument",
        "service": "1",
        "parameters": {
            "setDocument": {
            }
        },
        "output": {
            "idDocument": "$document_id"
        },
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );

        $result = (new ScorService(new ScorClient($client)))->addDocument($file, $invoice_dm, $type);

        $document_dm_actual = CJfseScorDocument::getFor($file, $jfse_user, $type);

        $this->assertTrue($result);
        $this->assertEquals($document_dm->_id, $document_dm_actual->_id);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testAddDocumentFailure(): void
    {
        $invoice_id  = self::$invoice_ids[1];
        $document_id = self::$document_ids[0];
        $type = ScorDocumentTypeEnum::PRESCRIPTION;

        $invoice_dm = CJfseInvoice::getFromJfseId($invoice_id);
        $document_dm = new CJfseScorDocument();
        $document_dm->jfse_id = $document_id;
        $document_dm->loadMatchingObject();
        $file = $document_dm->loadFile();

        $response = <<<JSON
{
    "method": {
        "name": "SCOR-setDocument",
        "service": "1",
        "parameters": {
            "setDocument": {
            }
        },
        "output": {},
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );

        $this->assertFalse((new ScorService(new ScorClient($client)))->addDocument($file, $invoice_dm, $type));
    }

    /**
     * Test the deletion of an unknown document
     *
     * @return void
     * @throws Exception
     */
    public function testDeleteDocumentDataModelNotFound(): void
    {
        $this->expectExceptionMessage('PersistenceError');

        (new ScorService())->deleteDocument('aaaaaaa', 'bbbbbbbb');
    }

    /**
     * Test the deletion of a document linked to multiple invoices
     *
     * @return void
     * @throws Exception
     */
    public function testDeleteDocumentDataModelLinkedToMultipleInvoices(): void
    {
        $document_id = self::$document_ids[0];
        $invoice_id = self::$invoice_ids[1];

        $response = <<<JSON
{
    "method": {
        "name": "SCOR-deleteLienFseDocument",
        "service": "1",
        "parameters": {
            "deleteLienFseDocument": {
            }
        },
        "output": {},
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;
        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );
        (new ScorService(new ScorClient($client)))->deleteDocument($document_id, $invoice_id);

        $document_dm = CJfseScorDocument::getFromJfseId($document_id);
        $invoice_dm = CJfseInvoice::getFromJfseId($invoice_id);

        $link = new CJfseInvoiceDocumentLink();
        $link->document_id = $document_dm->_id;
        $link->invoice_id = $invoice_dm->_id;
        $link->loadMatchingObject();

        $this->assertNotNull($document_dm->_id);
        $this->assertNull($link->_id);
    }

    /**
     * Test the deletion of a document only linked to one invoice
     *
     * @return void
     * @throws Exception
     */
    public function testDeleteDocumentDataModelToOneInvoice(): void
    {
        $document_id = self::$document_ids[1];
        $invoice_id = self::$invoice_ids[1];

        $response = <<<JSON
{
    "method": {
        "name": "SCOR-deleteLienFseDocument",
        "service": "1",
        "parameters": {
            "deleteLienFseDocument": {
            }
        },
        "output": {},
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;
        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );
        (new ScorService(new ScorClient($client)))->deleteDocument($document_id, $invoice_id);

        $document_dm = CJfseScorDocument::getFromJfseId($document_id);

        $this->assertNull($document_dm->_id);
    }

    /**
     * Test the deletion of the document that is marked as sent
     *
     * @return void
     * @throws Exception
     */
    public function testDeleteDocumentInvalidDocumentStatus(): void
    {
        $this->expectExceptionMessage('PersistenceError');

        $invoice_id = self::$invoice_ids[0];
        $document_id = self::$document_ids[0];

        (new ScorService())->deleteDocument($document_id, $invoice_id);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testExcludeInvoice(): void
    {
        $invoice_id = self::$invoice_ids[0];

        $response = <<<JSON
{
    "method": {
        "name": "SCOR-inclureExclureFactureDeScor",
        "service": "1",
        "parameters": {
            "deleteLienFseDocument": {
            }
        },
        "output": {},
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );
        (new ScorService(new ScorClient($client)))->excludeInvoice(Invoice::hydrate(['id' => $invoice_id]));

        $invoice_dm = CJfseInvoice::getFromJfseId($invoice_id);

        $this->assertEquals('0', $invoice_dm->scor);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testIncludeInvoice(): void
    {
        $invoice_id = self::$invoice_ids[0];

        $response = <<<JSON
{
    "method": {
        "name": "SCOR-inclureExclureFactureDeScor",
        "service": "1",
        "parameters": {
            "deleteLienFseDocument": {
            }
        },
        "output": {},
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;

        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );
        (new ScorService(new ScorClient($client)))->includeInvoice(Invoice::hydrate(['id' => $invoice_id]));

        $invoice_dm = CJfseInvoice::getFromJfseId($invoice_id);

        $this->assertEquals('1', $invoice_dm->scor);
    }

    /**
     * @return void
     */
    public function testGetDocumentTypes(): void
    {
        $response = <<<JSON
{
    "method": {
        "name": "SCOR-getListeTypesDocumentsPourAjout",
        "service": "1",
        "parameters": [],
        "output": {
            "lstTypeDocumentsPrescriptions": [
                {
                    "code": "0",
                    "libelle": "Ordonnance"
                },
                {
                    "code": "2",
                    "libelle": "DSIP"
                }
            ],
            "lstTypeDocumentsTicketsVitales": [
                {
                    "code": "8",
                    "libelle": "Ticket Vitale"
                }
            ],
            "lstTypeDocumentsCerfa": [
                {
                    "code": "1",
                    "libelle": "CERFA"
                }
            ],
            "lstTypeDocumentsAutres": [
                {
                    "code": "3",
                    "libelle": "Autre"
                }
            ],
            "lstTypeDocumentsDevis": [
                {
                    "code": "7",
                    "libelle": "Coupon Grippe"
                },
                {
                    "code": "12",
                    "libelle": "Document invalide"
                }
            ],
            "lstTypeDocumentsDetailFactures": [
                {
                    "code": "6",
                    "libelle": "Coupon prevention"
                }
            ]
        },
        "lstException": [],
        "cancel": "",
        "asynchronous": ""
    }
}
JSON;
        $client = $this->makeClientFromGuzzleResponses(
            [$this->makeJsonGuzzleResponse(200, mb_convert_encoding($response, 'UTF-8', 'ISO-8859-1'))]
        );

        $expected = [
            ScorDocumentTypeEnum::PRESCRIPTION,
            ScorDocumentTypeEnum::DSIP,
            ScorDocumentTypeEnum::VITALE_TICKET,
            ScorDocumentTypeEnum::CERFA,
            ScorDocumentTypeEnum::OTHER,
            ScorDocumentTypeEnum::FLU_COUPON,
            ScorDocumentTypeEnum::PREVENTION_COUPON,
        ];

        $actual = (new ScorService(new ScorClient($client)))->getDocumentTypes();

        $this->assertEquals($expected, $actual);

    }

    /**
     * Returns a random jfse id used for testing purpose
     *
     * @return string
     * @throws Exception
     */
    protected static function getRandomJfseId(): string
    {
        return bin2hex(random_bytes(16));
    }
}
