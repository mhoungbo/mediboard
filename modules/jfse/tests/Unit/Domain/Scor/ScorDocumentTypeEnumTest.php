<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Tests\Unit\Domain\Scor;

use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentTypeEnum;
use Ox\Tests\OxUnitTestCase;

/**
 * Test the class ScorDocumentTypeEnum
 */
class ScorDocumentTypeEnumTest extends OxUnitTestCase
{
    /**
     * @dataProvider getFromStringProvider
     *
     * @param string                    $label
     * @param ScorDocumentTypeEnum|null $expected
     *
     * @return void
     */
    public function testGetFromString(string $label, ?ScorDocumentTypeEnum $expected): void
    {
        $this->assertEquals($expected, ScorDocumentTypeEnum::getFromString($label));
    }

    /**
     * @return array[]
     */
    public function getFromStringProvider(): array
    {
        return [
            ['ordonnance', ScorDocumentTypeEnum::PRESCRIPTION],
            ['cerfa', ScorDocumentTypeEnum::CERFA],
            ['dsip', ScorDocumentTypeEnum::DSIP],
            ['autre', ScorDocumentTypeEnum::OTHER],
            ['ordonnance bizone', ScorDocumentTypeEnum::BIZONE_PRESCRIPTION],
            ['ordonnance exception', ScorDocumentTypeEnum::EXCEPTION_PRESCRIPTION],
            ['coupon prévention', ScorDocumentTypeEnum::PREVENTION_COUPON],
            ['coupon prevention', ScorDocumentTypeEnum::PREVENTION_COUPON],
            ['coupon grippe', ScorDocumentTypeEnum::FLU_COUPON],
            ['ticket vitale', ScorDocumentTypeEnum::VITALE_TICKET],
            ['cerfa exception', ScorDocumentTypeEnum::EXCEPTION_CERFA],
            ['prescription', null],
        ];
    }
}
