<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Tests\Unit\Domain\Scor;

use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentStatusEnum;
use Ox\Tests\OxUnitTestCase;

/**
 * Test the class ScorDocumentStatusEnum
 */
class ScorDocumentStatusEnumTest extends OxUnitTestCase
{
    /**
     * @dataProvider getFromStatusIntProvider
     *
     * @param int                         $value
     * @param ScorDocumentStatusEnum|null $expected
     *
     * @return void
     */
    public function testGetFronStatusInt(int $value, ?ScorDocumentStatusEnum $expected): void
    {
        $this->assertEquals($expected, ScorDocumentStatusEnum::getFromStatusInt($value));
    }

    /**
     * @return array[]
     */
    public function getFromStatusIntProvider(): array
    {
        return [
            [-1, ScorDocumentStatusEnum::LINKED],
            [0, ScorDocumentStatusEnum::SENT],
            [3, ScorDocumentStatusEnum::REJECTED],
            [4, ScorDocumentStatusEnum::ACCEPTED],
            [12, ScorDocumentStatusEnum::MANUAL],
            [5, null],
        ];
    }
}
