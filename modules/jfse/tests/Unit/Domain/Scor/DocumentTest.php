<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Tests\Unit\Domain\Scor;

use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\Domain\Scor\Document;
use Ox\Mediboard\Jfse\Domain\Scor\ScorExtensionTypeEnum;
use Ox\Tests\OxUnitTestCase;

class DocumentTest extends OxUnitTestCase
{
    /**
     * @dataProvider getFileTypeProvider
     *
     * @param CFile                 $file
     * @param ScorExtensionTypeEnum $expected_type
     *
     * @return void
     */
    public function testGetFileType(CFile $file, ?ScorExtensionTypeEnum $expected_type): void
    {
        $document = new Document();
        $document->setFile($file);

        $this->assertEquals($expected_type, $document->getFileType());
    }

    /**
     * @return array[]
     */
    public function getFileTypeProvider(): array
    {
        $file_pdf = new CFile();
        $file_pdf->file_type = 'application/pdf';
        $file_jpeg = new CFile();
        $file_jpeg->file_type = 'image/jpeg';
        $file_jpg = new CFile();
        $file_jpg->file_type = 'image/jpg';
        $file_tif = new CFile();
        $file_tif->file_type = 'image/tif';
        $file_tiff = new CFile();
        $file_tiff->file_type = 'image/tiff';
        $file_png = new CFile();
        $file_png->file_type = 'image/png';
        $file_zip = new CFile();
        $file_zip->file_type = 'application/zip';

        return [
            [$file_pdf, ScorExtensionTypeEnum::PDF],
            [$file_jpg, ScorExtensionTypeEnum::JPEG],
            [$file_jpeg, ScorExtensionTypeEnum::JPEG],
            [$file_tif, ScorExtensionTypeEnum::TIFF],
            [$file_tiff, ScorExtensionTypeEnum::TIFF],
            [$file_png, ScorExtensionTypeEnum::PNG],
            [$file_zip, null],
        ];
    }

    /**
     * @dataProvider isFileTypeValidProvider
     *
     * @param string $file_type
     * @param bool   $expected
     *
     * @return void
     */
    public function testIsFileTypeValid(string $file_type, bool $expected): void
    {
        $this->assertEquals($expected, Document::isFileTypeValid($file_type));
    }

    /**
     * @return array[]
     */
    public function isFileTypeValidProvider(): array
    {
        return [
            ['application/pdf', true],
            ['image/jpg', true],
            ['image/jpeg', true],
            ['image/tif', true],
            ['image/tiff', true],
            ['image/png', true],
            ['application/zip', false],
        ];
    }
}
