<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Tests\Unit\ViewModels;

use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Jfse\DataModels\CJfseScorDocument;
use Ox\Mediboard\Jfse\Domain\Scor\Document;
use Ox\Mediboard\Jfse\Domain\Scor\InvoiceScor;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentStatusEnum;
use Ox\Mediboard\Jfse\Domain\Scor\ScorDocumentTypeEnum;
use Ox\Mediboard\Jfse\ViewModels\Scor\CInvoiceScorView;
use Ox\Mediboard\Jfse\ViewModels\Scor\CScorDocumentView;
use Ox\Tests\OxUnitTestCase;

/**
 * Test the CInvoiceScoView class
 */
class CInvoiceScorViewTest extends OxUnitTestCase
{
    /**
     * @return void
     */
    public function testGetFromEntity(): void
    {
        $document_dm = new CJfseScorDocument();
        $document_dm->_id = 12;
        $document_dm->jfse_id = '78d218e88641d2ced1';
        $document_dm->jfse_user_id = 177;
        $document_dm->file_id = 24;

        $file = new CFile();
        $file->file_name = 'cerfa_05-11.pdf';
        $file->_id = 24;

        $invoice_scor = InvoiceScor::hydrate([
            'documents' => [
                Document::hydrate([
                    'id' => '78d218e88641d2ced1',
                    'jfse_user_id' => 177,
                    'invoice_id' => '1548764312021541',
                    'type' => ScorDocumentTypeEnum::CERFA,
                    'status' => ScorDocumentStatusEnum::LINKED,
                    'name' => 'cerfa_05-11.pdf',
                    'data_model' => $document_dm,
                    'file' => $file,
                ])
            ],
            'missing_document_types' => [
                ScorDocumentTypeEnum::PRESCRIPTION,
                ScorDocumentTypeEnum::DSIP
            ],
            'excluded' => false,
        ]);

        $document_view = new CScorDocumentView();
        $document_view->id = '78d218e88641d2ced1';
        $document_view->type = 1;
        $document_view->name = 'cerfa_05-11.pdf';
        $document_view->paper_mode = '0';
        $document_view->generate_document = '0';
        $document_view->status = 'linked';
        $document_view->_data_model = $document_dm;
        $document_view->_file = $file;

        $expected = new CInvoiceScorView();
        $expected->documents = [$document_view];
        $expected->missing_documents_types = [0, 2];
        $expected->excluded = false;

        $this->assertEquals($expected,  CInvoiceScorView::getFromEntity($invoice_scor));
    }
}
