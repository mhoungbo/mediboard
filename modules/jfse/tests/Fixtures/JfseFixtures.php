<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Tests\Fixtures;

use DateTimeImmutable;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Jfse\DataModels\CJfsePatient;
use Ox\Mediboard\Jfse\DataModels\CJfseUser;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Users and patient fixtures for the Jfse module tests
 */
class JfseFixtures extends Fixtures implements GroupFixturesInterface
{
    public const GROUP_NAME                  = 'jfse_fixtures';
    public const GROUP_TAG                   = 'jfse_group';
    public const FUNCTION_TAG                = 'jfse_function';
    public const MEDIUSER_GENERALIST_TAG     = 'jfse_mediuser_generalist';
    public const JFSE_USER_GENERALIST_TAG    = 'jfse_user_generalist';
    public const PLAGE_GENERALIST_TAG        = 'jfse_plage_generalist';
    public const MEDIUSER_NURSE_TAG          = 'jfse_mediuser_nurse';
    public const JFSE_USER_NURSE_TAG         = 'jfse_user_nurse';
    public const PLAGE_NURSE_TAG             = 'jfse_plage_nurse';
    public const PATIENT_TEST_ALAIN_TAG      = 'jfse_patient_test_alain';
    public const JFSE_PATIENT_TEST_ALAIN_TAG = 'jfse_jfse_patient_test_alain';

    protected CGroups       $group;
    protected CFunctions    $function;
    protected CMediusers    $generalist_user;
    protected CJfseUser     $generalist_jfse_user;
    protected CPlageconsult $generalist_plage;
    protected CMediusers    $nurse_user;
    protected CJfseUser     $nurse_jfse_user;
    protected CPlageconsult $nurse_plage;

    protected string $date_string;

    /**
     * @inheritDoc
     * @throws FixturesException
     */
    public function load()
    {
        $this->date_string = (new DateTimeImmutable())->format('Y-m-d');

        $this->generateGroup();
        $this->generateFunction();
        $this->generateGeneralist();
        $this->generateNurse();
        $this->generatePatientTestAlain();
    }

    /**
     * @return void
     * @throws FixturesException
     */
    protected function generateGroup(): void
    {
        $this->group        = new CGroups();
        $this->group->_name = $this->group->raison_sociale = 'Etablissement JFSE Test';
        $this->group->code  = 'etab_jfse_test';

        $this->store($this->group, self::GROUP_TAG);
    }

    /**
     * @return void
     * @throws FixturesException
     */
    protected function generateFunction(): void
    {
        $this->function           = new CFunctions();
        $this->function->group_id = $this->group->_id;
        $this->function->text     = 'Cabinet Jfse test';
        $this->function->type     = 'cabinet';
        $this->function->color    = 'FFFFFF';

        $this->store($this->function, self::FUNCTION_TAG);
    }

    /**
     * @return void
     * @throws FixturesException
     */
    protected function generateGeneralist(): void
    {
        $this->generalist_user                  = new CMediusers();
        $this->generalist_user->_user_username  = 'jfse_generalist_fixtures';
        $this->generalist_user->_user_last_name = 'GENE';
        $this->generalist_user->_user_last_name = 'Alain';
        $this->generalist_user->function_id     = $this->function->_id;
        $this->generalist_user->_user_type      = 13;
        $this->generalist_user->adeli           = '991142381';
        $this->generalist_user->rpps            = '99900093578';
        $this->generalist_user->spec_cpam_id    = 1;
        $this->generalist_user->secteur         = '1';
        $this->store($this->generalist_user, self::MEDIUSER_GENERALIST_TAG);

        $this->generalist_jfse_user                = new CJfseUser();
        $this->generalist_jfse_user->jfse_id       = 177;
        $this->generalist_jfse_user->mediuser_id   = $this->generalist_user->_id;
        $this->generalist_jfse_user->securing_mode = 4;
        $this->store($this->generalist_jfse_user, self::JFSE_USER_GENERALIST_TAG);

        $this->generalist_plage          = new CPlageconsult();
        $this->generalist_plage->chir_id = $this->generalist_user->_id;
        $this->generalist_plage->date    = $this->date_string;
        $this->generalist_plage->debut   = '08:00:00';
        $this->generalist_plage->fin     = '18:00:00';
        $this->generalist_plage->freq    = '00:05:00';
        $this->store($this->generalist_plage, self::PLAGE_GENERALIST_TAG);
    }

    /**
     * @return void
     * @throws FixturesException
     */
    protected function generateNurse(): void
    {
        $this->nurse_user                  = new CMediusers();
        $this->nurse_user->_user_username  = 'jfse_nurse_fixtures';
        $this->nurse_user->_user_last_name = 'FIRMIER';
        $this->nurse_user->_user_last_name = 'Alain';
        $this->nurse_user->function_id     = $this->function->_id;
        $this->nurse_user->_user_type      = 7;
        $this->nurse_user->adeli           = '996101853';
        $this->nurse_user->spec_cpam_id    = 24;
        $this->nurse_user->secteur         = '1';
        $this->store($this->nurse_user, self::MEDIUSER_NURSE_TAG);

        $this->nurse_jfse_user                = new CJfseUser();
        $this->nurse_jfse_user->jfse_id       = 180;
        $this->nurse_jfse_user->mediuser_id   = $this->nurse_user->_id;
        $this->nurse_jfse_user->securing_mode = 4;
        $this->store($this->nurse_jfse_user, self::JFSE_USER_NURSE_TAG);

        $this->nurse_plage          = new CPlageconsult();
        $this->nurse_plage->chir_id = $this->nurse_user->_id;
        $this->nurse_plage->date    = $this->date_string;
        $this->nurse_plage->debut   = '08:00:00';
        $this->nurse_plage->fin     = '18:00:00';
        $this->nurse_plage->freq    = '00:05:00';
        $this->store($this->nurse_plage, self::PLAGE_NURSE_TAG);
    }

    /**
     * @return void
     * @throws FixturesException
     */
    protected function generatePatientTestAlain(): void
    {
        $patient                    = new CPatient();
        $patient->group_id          = $this->group->_id;
        $patient->nom               = 'TEST';
        $patient->prenom            = 'Alain';
        $patient->matricule         = '172192B99900224';
        $patient->naissance         = '1972-12-01';
        $patient->sexe              = 'm';
        $patient->rang_naissance    = '1';
        $patient->qual_beneficiaire = '0';
        $patient->code_regime       = '01';
        $patient->caisse_gest       = '349';
        $patient->centre_gest       = '9881';
        $patient->code_gestion      = '18';
        $patient->cp_naissance      = '34000';
        $this->store($patient, self::PATIENT_TEST_ALAIN_TAG);

        $jfse_patient                      = new CJfsePatient();
        $jfse_patient->patient_id          = $patient->_id;
        $jfse_patient->nir                 = $patient->matricule;
        $jfse_patient->birth_date          = $patient->naissance;
        $jfse_patient->birth_rank          = $patient->rang_naissance;
        $jfse_patient->quality             = $patient->qual_beneficiaire;
        $jfse_patient->last_name           = $patient->nom;
        $jfse_patient->first_name          = $patient->prenom;
        $jfse_patient->amo_regime_code     = $patient->code_regime;
        $jfse_patient->amo_managing_fund   = $patient->caisse_gest;
        $jfse_patient->amo_managing_center = $patient->centre_gest;
        $jfse_patient->amo_managing_code   = $patient->code_gestion;

        $this->store($jfse_patient, self::JFSE_PATIENT_TEST_ALAIN_TAG);
    }

    /**
     * @inheritDoc
     */
    public static function getGroup(): array
    {
        return [self::GROUP_NAME, 1];
    }
}
