/**
 * @package Mediboard\Jfse
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Scor = {
    uid: null,
    form: null,

    /**
     * Displays the view for adding a document in a modal
     *
     * @param invoice_id
     * @param type
     */
    showAddDocumentView: function (invoice_id, type) {
        Jfse.displayViewModal('scor/viewAddDocument', 350, null, {
            invoice_id: invoice_id,
            type: type
        }, {
            title: $T('CScorDocumentView-action-add_document'),
            onClose: this.reloadScorInvoiceView.bind(this, invoice_id)
        });
    },

    /**
     * Initialize the addDocument view and sets the input as not nullable
     *
     * @param uid
     */
    initializeAddDocumentView: function (uid) {
        this.uid = uid;
        this.form = getForm('AddScorDocument-' + this.uid);
        Jfse.setInputNotNull(this.form.elements['type']);
        Jfse.setInputNotNull(this.form.elements['document_id']);
    },

    /**
     * Displays the list of documents and files linked to a CPatient
     */
    displayDocumentListModal: function () {
        Modal.open('patient_documents_list-' + this.uid, {
            title: $T('CScorDocumentView-action-select_document'),
            showClose: true
        });
    },

    /**
     * Reloads the list of documents and files linked to a CPatient
     *
     * @param consultation_id
     */
    reloadPatientDocumentList: function (consultation_id) {
        Jfse.displayView('scor/listDocuments', 'patient_documents_list-' + this.uid, {consultation_id: consultation_id});
    },

    /**
     * Select the given document or file
     *
     * @param document_row
     */
    selectDocument: function (document_row) {
        $V(this.form.elements['document_class'], document_row.get('class'));
        $V(this.form.elements['document_id'], document_row.get('id'));
        $('selected_document_view-' + this.uid).innerText = document_row.get('view');
        Control.Modal.close();
    },

    /**
     * @param invoice_id
     */
    reloadScorInvoiceView: function (invoice_id) {
        Jfse.displayView('scor/showInvoiceDocuments', '', {
            invoice_id: invoice_id,
        });
    },

    /**
     * Send the document to Jfse
     *
     * @returns {Promise<void>}
     */
    addDocument: async function () {
        if ($V(this.form.elements['type']) === '') {
            Modal.alert($T('CScorDocumentView-msg-type_not_set'));
            return;
        } else if ($V(this.form.elements['document_id']) === '') {
            Modal.alert($T('CScorDocumentView-msg-file_not_selected'));
            return;
        }

        const invoice_id = $V(this.form.elements['invoice_id']);
        const response = await Jfse.requestJson('scor/addDocument', {
            invoice_id: invoice_id,
            type: $V(this.form.elements['type']),
            document_id: $V(this.form.elements['document_id']),
            document_class: $V(this.form.elements['document_class']),
        });

        if (response.success) {
            Jfse.notifySuccessMessage('CScorDocumentView-msg-document_added');
            Control.Modal.close();
            Invoicing.reload(Invoicing.getConsultationId(invoice_id), invoice_id);
        } else if (response.messages) {
            Jfse.displayMessagesModal(response.messages);
        } else if (response.error) {
            Jfse.displayErrorMessageModal(response.error);
        }
    },

    /**
     * Deletes the given Scor document
     *
     * @param document_id
     * @param invoice_id
     * @returns {Promise<void>}
     */
    deleteDocument: async function (document_id, invoice_id) {
        const response = await Jfse.requestJson('scor/deleteDocument', {
            invoice_id: invoice_id,
            document_id: document_id
        });

        if (response.success) {
            Jfse.notifySuccessMessage('CScorDocumentView-msg-document_deleted');
            Invoicing.reload(Invoicing.getConsultationId(invoice_id), invoice_id);
        } else if (response.messages) {
            Jfse.displayMessagesModal(response.messages);
        } else if (response.error) {
            Jfse.displayErrorMessageModal(response.error);
        }
    },

    /**
     * Excludes the given invoice from the Scor process
     *
     * @param invoice_id
     * @returns {Promise<void>}
     */
    excludeInvoice: async function (invoice_id) {
        const response = await Jfse.requestJson('scor/excludeInvoice', {
            invoice_id: invoice_id,
        });

        if (response.success) {
            Jfse.notifySuccessMessage('CInvoiceScorView-msg-invoice_excluded');
            Invoicing.reload(Invoicing.getConsultationId(invoice_id), invoice_id);
        } else if (response.messages) {
            Jfse.displayMessagesModal(response.messages);
        } else if (response.error) {
            Jfse.displayErrorMessageModal(response.error);
        }
    },


    /**
     * Includes the given invoice in the Scor process
     *
     * @param invoice_id
     * @returns {Promise<void>}
     */
    includeInvoice: async function (invoice_id) {
        const response = await Jfse.requestJson('scor/includeInvoice', {
            invoice_id: invoice_id,
        });

        if (response.success) {
            Jfse.notifySuccessMessage('CInvoiceScorView-msg-invoice_included');
            Invoicing.reload(Invoicing.getConsultationId(invoice_id), invoice_id);
        } else if (response.messages) {
            Jfse.displayMessagesModal(response.messages);
        } else if (response.error) {
            Jfse.displayErrorMessageModal(response.error);
        }
    }
};
