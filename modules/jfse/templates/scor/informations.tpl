{{*
 * @package Mediboard\Jfse
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=jfse script=Scor ajax=true}}

{{assign var=invoice_id value=$invoice->id}}

<div class="me-display-flex" style="align-items: center;">
    <div style="flex-grow: 7;">
        {{if $invoice->scor->excluded}}
            <div class="small-warning me-line-height-26">
                {{tr}}CInvoiceScorView-msg-invoice_excluded{{/tr}}
            </div>
        {{else}}
            {{foreach from=$invoice->scor->missing_documents_types item=_type}}
                <div class="small-warning me-line-height-26">
                    {{tr var1='Ox\Core\CAppUI::tr'|static_call:"ScorDocumentTypeEnum.$_type"}}CInvoiceScorView-message-missing_document{{/tr}}
                    <button class="me-add notext me-primary" type="button" onclick="Scor.showAddDocumentView('{{$invoice->id}}', '{{$_type}}');">{{tr}}CInvoiceScorView-action-add_document{{/tr}}</button>
                </div>
            {{/foreach}}

            {{foreach from=$invoice->scor->documents item=_document}}
                <div class="me-line-height-26">
                    <span onmouseover="ObjectTooltip.createDOM(this, 'CScorDocumentView-{{$_document->id}}');">{{$_document->name}}</span>
                    <button class="trash notext me-secondary" type="button" onclick="Scor.deleteDocument('{{$_document->id}}', '{{$invoice->id}}');">{{tr}}CInvoiceScorView-action-delete_document{{/tr}}</button>
                </div>
                <div id="CScorDocumentView-{{$_document->id}}" style="display: none;">
                    {{mb_include module=jfse template=scor/document_tooltip document=$_document}}
                </div>
            {{/foreach}}
        {{/if}}
    </div>
    <div id="scor-actions" class="me-flex-grow-1 me-text-align-right">
        {{if $invoice->scor->excluded}}
            {{me_button label='CInvoiceScorView-action-include' icon=tick onclick="Scor.includeInvoice('$invoice_id');"}}
        {{else}}
            {{me_button label="CScorDocumentView-action-add_document" icon=fa-plus onclick="Scor.showAddDocumentView('$invoice_id');"}}
            {{if !$invoice->scor->documents|@count}}
                {{me_button label='CInvoiceScorView-action-exclude' icon=cancel onclick="Scor.excludeInvoice('$invoice_id');"}}
            {{/if}}
        {{/if}}
        {{me_dropdown_button button_label=Options button_icon=opt button_class="notext me-tertiary" container_class="me-dropdown-button-right"}}
    </div>
</div>
