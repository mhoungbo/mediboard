{{*
 * @package Mediboard\Jfse
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl">
    <tr>
        <th class="title" colspan="5">
                <span style="float: right;">
                    {{mb_include module=patients template=inc_button_add_doc
                        context_guid=$consultation->_guid patient_id=$patient->_id show_text=false
                        callback="function(){Scor.reloadPatientDocumentList('`$consultation->_id`');}"
                    }}
                </span>
            {{tr}}CCompteRendu|pl{{/tr}}
        </th>
    </tr>
    <tr>
        <th class="category">{{tr}}CFile-file_type-court{{/tr}}</th>
        <th class="category">{{tr}}CFile-file_name{{/tr}}</th>
        <th class="category">{{tr}}CFile-_file_size{{/tr}}</th>
        <th class="category">{{tr}}CFile-object_id{{/tr}}</th>
        <th class="category narrow"></th>
    </tr>
    {{foreach from=$documents item=_document}}
        {{if $_document->_class != 'CExLink'}}
            <tr data-id="{{$_document->_id}}" data-class="{{$_document->_class}}" data-view="{{$_document->_view}}"
                data-size="{{$_document->doc_size}}">
                <td>
                    {{tr}}{{$_document->_class}}{{/tr}}
                </td>
                <td>
                    {{if $_document->_class == 'CFile'}}
                        {{assign var=_file value=$_document}}
                        {{mb_include module=files template=inc_widget_line_file object_id=$_file->object_id object_class=$_file->object_class}}
                    {{else}}
                        <span onmouseover="ObjectTooltip.createEx(this, '{{$_document->_guid}}', 'objectView')">
                            {{$_document}}
                        </span>
                    {{/if}}
                </td>
                <td>
                    {{mb_value object=$_document field=_file_size}}
                </td>
                <td>
                    <span onmouseover="ObjectTooltip.createEx(this, '{{$_document->object_class}}-{{$_document->object_id}}', 'objectView');">
                        {{tr}}{{$_document->object_class}}{{/tr}}
                    </span>
                </td>
                <td>
                    <button type="button" class="tick notext" onclick="Scor.selectDocument(this.up().up());">
                        {{tr}}Select{{/tr}}
                    </button>
                </td>
            </tr>
        {{/if}}
    {{foreachelse}}
        <tr>
            <td class="empty" style="text-align: center;" colspan="5">{{tr}}CFile.none{{/tr}}</td>
        </tr>
    {{/foreach}}
</table>
