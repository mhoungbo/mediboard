{{*
 * @package Mediboard\Jfse
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl">
    <tr>
        <th class="title me-text-align-center" colspan="2">{{$document->name}}</th>
    </tr>
    <tr>
        <td>
            <strong>{{mb_label object=$document field=type}} :</strong>{{tr}}ScorDocumentTypeEnum.{{$document->type}}{{/tr}}
        </td>
        <td style="text-align: center; width: 66px;" rowspan="7">
            <div style="width: 66px; height: 92px; background: white; cursor: pointer;"
                 onclick="new Url().ViewFilePopup('{{$document->_file->object_class}}', '{{$document->_file->object_id}}', 'CFile', '{{$document->_file->_id}}')">
                {{thumbnail document=$document->_file profile=medium style="max-width: 64px; max-height: 92px; border: 1px solid black; vertical-align: middle;"}}
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <strong>{{mb_label object=$document field=status}} :</strong> {{tr}}ScorDocumentStatusEnum.{{$document->status}}{{/tr}}
        </td>
    </tr>
    <tr>
        <th class="category me-text-align-center">{{tr}}CFile{{/tr}}</th>
    </tr>
    {{if $document->_file->file_category_id}}
        <tr>
            <td>
                <strong>{{mb_label object=$document->_file field=file_category_id}} :</strong>{{$document->_file->_ref_category}}
            </td>
        </tr>
    {{/if}}
    <tr>
        <td>
            <strong>{{mb_label object=$document->_file field=file_date}} :</strong>{{mb_value object=$document->_file field=file_date}}
        </td>
    </tr>
    <tr>
        <td>
            <strong>{{mb_label object=$document->_file field=_file_size}} :</strong>{{mb_value object=$document->_file field=_file_size}}
        </td>
    </tr>
    <tr>
        <td>
            <strong>{{mb_label object=$document->_file field=object_id}} :</strong>{{$document->_file->_ref_object}}
        </td>
    </tr>
        <td class="button" colspan="2">
            <button type="button" class="trash" onclick="Scor.deleteDocument('{{$document->id}}', '{{$invoice->id}}');">{{tr}}Delete{{/tr}}</button>
        </td>
    </tr>
</table>
