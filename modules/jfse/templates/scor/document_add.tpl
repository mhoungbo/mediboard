{{*
 * @package Mediboard\Jfse
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{unique_id var=uid}}

{{mb_script module=files script=file ajax=true}}

<script type="text/javascript">
    Main.add(function() {
        Scor.initializeAddDocumentView('{{$uid}}');
    });
</script>

<div id="add_scor_document_form" class="me-margin-bottom-5">
    <form action="?" name="AddScorDocument-{{$uid}}" method="post" onsubmit="return false;">
        <input type="hidden" name="invoice_id" value="{{$invoice_id}}">
        <input type="hidden" name="consultation_id" value="{{$consultation->_id}}">

        <table class="keep" style="width: 100%;">
            <tr>
                {{me_form_field nb_cells=2 label=Type class="me-padding-5"}}
                    <select name="type" onchange="">
                        <option value="">&mdash; {{tr}}Select{{/tr}}</option>
                        {{foreach from=$types item=_type}}
                            <option
                              value="{{$_type->value}}" {{if $type && $type === $_type}} selected="selected"{{/if}}>
                                {{$_type->getLocale()}}
                            </option>
                        {{/foreach}}
                    </select>
                {{/me_form_field}}
            </tr>
            <tr>
                {{me_form_field nb_cells=1 label=Document layout=true class="me-padding-5"}}
                    <input type="hidden" name="document_id" value="">
                    <input type="hidden" name="document_class" value="">
                    <span id="selected_document_view-{{$uid}}" class="empty">
                        Aucun document sélectionné
                    </span>
                {{/me_form_field}}
                <td class="narrow">
                    <button type="button" class="search notext" onclick="Scor.displayDocumentListModal();">{{tr}}Sélectionner un document{{/tr}}</button>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="button">
                    <button type="button" class="add me-primary" onclick="Scor.addDocument();">{{tr}}Add{{/tr}}</button>
                    <button type="button" class="cancel me-secondary" onclick="Control.Modal.close();">{{tr}}Cancel{{/tr}}</button>
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="patient_documents_list-{{$uid}}" style="display: none;">
    {{mb_include module=jfse template=scor/document_list}}
</div>
