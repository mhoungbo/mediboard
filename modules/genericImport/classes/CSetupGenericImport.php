<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\GenericImport;

use Ox\Core\CSetup;

/**
 * @codeCoverageIgnore
 */
class CSetupGenericImport extends CSetup
{
    /**
     * @inheritDoc
     */
    function __construct()
    {
        parent::__construct();

        $this->mod_name = "genericImport";
        $this->makeRevision("0.0");
        $this->setModuleCategory("import", "ox");

        $this->addDependency('import', '0.03');

        $this->makeRevision('0.01');

        $query = "CREATE TABLE `import_file` (
                `import_file_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `import_campaign_id` INT (11) UNSIGNED NOT NULL,
                `file_name` VARCHAR (255) NOT NULL,
                `entity_type` VARCHAR(80),
                INDEX `campaign_type` (`import_campaign_id`, `entity_type`)
              )/*! ENGINE=MyISAM */";
        $this->addQuery($query);

        $this->makeRevision('0.02');

        // Delete all useless import modules
        $this->deleteModule('ami');
        $this->deleteModule('calystene');
        $this->deleteModule('crossway');
        $this->deleteModule('diane');
        $this->deleteModule('dxcare');
        $this->deleteModule('endomax');
        $this->deleteModule('fisimed');
        $this->deleteModule('hellodoc');
        $this->deleteModule('hierarchicalFiles');
        $this->deleteModule('hypercare');

        $this->makeRevision('0.03');

        $this->deleteModule('iDocteur');
        $this->deleteModule('import4D');
        $this->deleteModule('importHM');
        $this->deleteModule('lifelineImport');
        $this->deleteModule('maidis');
        $this->deleteModule('medicalNet');
        $this->deleteModule('medicawin');
        $this->deleteModule('medimust');
        $this->deleteModule('medistory');
        $this->deleteModule('nova');

        $this->makeRevision('0.04');

        $this->deleteModule('novaxel');
        $this->deleteModule('odyssee');
        $this->deleteModule('opesim');
        $this->deleteModule('pckent');
        $this->deleteModule('phenix');
        $this->deleteModule('primaPatient');
        $this->deleteModule('resurgences');
        $this->deleteModule('soga');
        $this->deleteModule('specilog');
        $this->deleteModule('studiovision');

        $this->mod_version = "0.05";
    }
}
