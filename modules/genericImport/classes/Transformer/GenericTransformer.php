<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\GenericImport\Transformer;

use Ox\Import\Framework\Transformer\DefaultTransformer;

class GenericTransformer extends DefaultTransformer
{
    /**
     * @param mixed $field
     */
    protected function getFieldValue(string $field_name, $field)
    {
        $update = $this->configuration['update'];

        if (
            !$update
            || (
                ($update && isset($this->configuration['fields_to_update'][$field_name]))
                && ($field !== null)
            )
        ) {
            return $field;
        }

        return null;
    }
}
