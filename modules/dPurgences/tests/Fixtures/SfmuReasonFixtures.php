<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Fixtures;

use Exception;
use Ox\Mediboard\Urgences\CMotifSFMU;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

/**
 * @description Fixture SfmuReason with random data.
 */
class SfmuReasonFixtures extends Fixtures
{
    private const COUNT_SFMU_REASONS = 5;

    /**
     * @inheritDoc
     * @throws FixturesException
     */
    public function load(): void
    {
        $this->createSfmuReasons();
    }

    /**
     * Create SFMU reasons.
     *
     * @throws FixturesException
     */
    private function createSfmuReasons(): void
    {
        for ($index = 0; $index < self::COUNT_SFMU_REASONS; $index++) {
            $sfmu_reason            = new CMotifSFMU();
            $sfmu_reason->code      = "FI0.$index";
            $sfmu_reason->libelle   = "Fixture-$index";
            $sfmu_reason->categorie = "FixtureCategory-$index";

            $this->store($sfmu_reason);
        }
    }
}
