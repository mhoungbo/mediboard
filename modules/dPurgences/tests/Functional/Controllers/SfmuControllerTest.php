<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Urgences\Tests\Functional\Controllers;

use Exception;
use Ox\Mediboard\Urgences\CMotifSFMU;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test class for SfmuController.
 */
class SfmuControllerTest extends OxWebTestCase
{
    /**
     * Test: List SFMU reasons.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testList(): CMotifSFMU
    {
        // Create client.
        $client = self::createClient();
        $client->request('GET', '/api/urgences/sfmu/reasons', ['limit' => 3]);

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Collection (data).
        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(3, $collection->count());

        foreach ($collection as $item) {
            $this->assertEquals(CMotifSFMU::RESOURCE_TYPE, $item->getType());
            $this->assertNotNull($item->getId());
        }

        return CMotifSFMU::findOrFail($collection->getFirstItem()->getId());
    }

    /**
     * Test: Show a report by 'motif_sfmu_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     * @depends testList
     */
    public function testShowWithPermission(CMotifSFMU $sfmu_reason): void
    {
        // Create client.
        $client = self::createClient();
        $client->request('GET', "/api/urgences/sfmu/reasons/{$sfmu_reason->_id}");

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($sfmu_reason::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($sfmu_reason->_id, $item->getId());
    }
}
