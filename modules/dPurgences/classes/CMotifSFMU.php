<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Urgences;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Symfony\Component\Routing\RouterInterface;

/**
 * Representation of a SFMU reason.
 */
class CMotifSFMU extends CMbObject
{
    /** @var string Resource type. */
    public const RESOURCE_TYPE = 'sfmu_reason';

    /**
     * DB Table key.
     * @var int
     */
    public $motif_sfmu_id;

    public ?string $code      = null;
    public ?string $libelle   = null;
    public ?string $categorie = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = "motif_sfmu";
        $spec->key   = "motif_sfmu_id";

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props["code"]      = "str fieldset|" . self::FIELDSET_DEFAULT;
        $props["libelle"]   = "str fieldset|" . self::FIELDSET_DEFAULT;
        $props["categorie"] = "str fieldset|" . self::FIELDSET_DEFAULT;

        return $props;
    }

    /**
     * @inheritdoc
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->libelle;
    }

    /**
     * @inheritDoc
     */
    public function getApiLink(RouterInterface $router): ?string
    {
        return $router->generate('urgences_sfmu_reasons_show', ['motif_sfmu_id' => $this->_id]);
    }
}
