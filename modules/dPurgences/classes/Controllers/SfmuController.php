<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Urgences\Controllers;

use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Exceptions\ApiRequestException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Controller;
use Ox\Mediboard\Urgences\CMotifSFMU;
use Symfony\Component\HttpFoundation\Response;

/**
 * CRUD API SFMU.
 * Controller that answer the api calls on SFMU.
 */
class SfmuController extends Controller
{
    /**
     * List a SFMU reasons using the request parameters.
     *
     * Security:
     * => READ perm is required.
     *
     * @throws ApiException
     * @throws ApiRequestException
     * @api
     */
    public function list(RequestApi $request_api): Response
    {
        $sfmu = new CMotifSFMU();

        $sfmu_reasons = $sfmu->loadListFromRequestApi($request_api);
        $total        = $sfmu->countListFromRequestApi($request_api);

        $collection = Collection::createFromRequest($request_api, $sfmu_reasons);
        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            $total
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * Show a sfmu reason identified by 'motif_sfmu_id' parameter.
     *
     * Security:
     * => READ Perm needed.
     *
     * @throws ApiException
     * @api
     */
    public function show(CMotifSFMU $sfmu_reason, RequestApi $request_api): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request_api, $sfmu_reason));
    }
}
