{{*
 * @package Mediboard\Cim10
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
    importCim10OMS = function () {
        new Url()
            .setRoute('{{url name=cim_import_cim_oms action=import}}')
            .requestUpdate('cim10_oms', {
                onComplete: displayDataSourceVersions.curry('{{url name=cim_datasource_version datasource=cim}}', 'cim_datasources_versions')
            });
    };

    updateCim10OMS = function () {
        new Url()
            .setRoute('{{url name=cim_import_cim_oms action=update}}')
            .requestUpdate('cim10_oms_update', {
                onComplete: displayDataSourceVersions.curry('{{url name=cim_datasource_version datasource=cim}}', 'cim_datasources_versions')
            });
    };

    importCim10ATIH = function () {
        new Url()
            .setRoute('{{url name=cim_import_cim_atih}}')
            .requestUpdate('cim10_atih', {
                onComplete: displayDataSourceVersions.curry('{{url name=cim_datasource_version datasource=cim}}', 'cim_datasources_versions')
            });
    };

    importCim10GM = function () {
        new Url()
            .setRoute('{{url name=cim_import_cim_gm}}')
            .requestUpdate('cim10_gm', {
                onComplete: displayDataSourceVersions.curry('{{url name=cim_datasource_version datasource=cim}}', 'cim_datasources_versions')
            });
    };

    importDRC = function () {
        new Url()
            .setRoute('{{url name=cim_import_drc}}')
            .requestUpdate('drc_import', {
                onComplete: displayDataSourceVersions.curry('{{url name=cim_datasource_version datasource=drc}}', 'drc_datasources_versions')
            });
    };

    importCISP = function () {
        new Url()
            .setRoute('{{url name=cim_import_cisp}}')
            .requestUpdate('cisp_import', {
                onComplete: displayDataSourceVersions.curry('{{url name=cim_datasource_version datasource=cisp}}', 'cisp_datasources_versions')
            });
    };

    displayDataSourceVersions = function(route, target) {
        new Url()
            .setRoute(route)
            .requestUpdate(target);
    };

    Main.add(function () {
        Control.Tabs.create('tabs-configure', true);
        Configuration.edit('dPcim10', 'CGroups', 'Configs');
        displayDataSourceVersions('{{url name=cim_datasource_version datasource=cim}}', 'cim_datasources_versions');
        displayDataSourceVersions('{{url name=cim_datasource_version datasource=drc}}', 'drc_datasources_versions');
        displayDataSourceVersions('{{url name=cim_datasource_version datasource=cisp}}', 'cisp_datasources_versions');
    });
</script>

<ul id="tabs-configure" class="control_tabs">
    <li><a href="#CIM">CIM10</a></li>
    <li><a href="#drc">DRC</a></li>
    <li><a href="#cisp">CISP</a></li>
    <li><a href="#favoris">{{tr}}CFavoriCIM10{{/tr}}</a></li>
    <li><a href="#Configs">{{tr}}CConfiguration{{/tr}}</a></li>
    <li><a href="#outils">{{tr}}Tools{{/tr}}</a></li>
</ul>

<div id="CIM" style="display: none;">
    <form name="editConfig-ccam" method="post" onsubmit="return onSubmitFormAjax(this);">
        {{mb_configure module=$m}}

        <table class="form">
            <tr>
                <th class="category" colspan="2">{{tr}}CConfiguration{{/tr}}</th>
            </tr>
            {{mb_include module=system template=inc_config_enum var=cim10_version values='oms|atih|gm'}}

            <tr>
                <td class="button" colspan="2">
                    <button class="modify">{{tr}}Save{{/tr}}</button>
                </td>
            </tr>
        </table>
    </form>

    {{mb_include module=system template=datasources/configure_dsn dsn=cim10}}

    <h2 class="me-text-align-center me-margin-5">{{tr}}CCodeCIM10.import_base.tile{{/tr}}</h2>

    <div class="me-display-flex" style="align-items: center;">
        <div id="cim_datasources_versions"></div>
        <div class="me-flex-grow-1">
            <table class="tbl">
                <tr>
                    <th>{{tr}}Action{{/tr}}</th>
                    <th>{{tr}}Status{{/tr}}</th>
                </tr>

                <tr>
                    <td class="narrow">
                        <button id="import_cim10_oms" class="tick"
                                onclick="importCim10OMS()">{{tr}}CCodeCIM10.import_base_oms{{/tr}}</button>
                    </td>
                    <td id="cim10_oms"></td>
                </tr>

                <tr>
                    <td class="narrow">
                        <button id="import_cim10_oms_update" class="tick"
                                onclick="updateCim10OMS()">{{tr}}CCodeCIM10.update_base_oms{{/tr}}</button>
                    </td>
                    <td id="cim10_oms_update"></td>
                </tr>

                <tr>
                    <td class="narrow">
                        <button id="import_cim10_atih" class="tick"
                                onclick="importCim10ATIH()">{{tr}}CCodeCIM10.import_base_atih{{/tr}}</button>
                    </td>
                    <td id="cim10_atih"></td>
                </tr>

                <tr>
                    <td class="narrow">
                        <button id="import_cim10_gm" class="tick"
                                onclick="importCim10GM()">{{tr}}CCodeCIM10.import_base_gm{{/tr}}</button>
                    </td>
                    <td id="cim10_gm"></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div id="drc" style="display: none;">
    {{mb_include module=system template=datasources/configure_dsn dsn=drc}}

    <h2 class="me-text-align-center me-margin-5">Import de la base DRC</h2>

    <div class="me-display-flex" style="align-items: center;">
        <div id="drc_datasources_versions"></div>
        <div class="me-flex-grow-1">
            <table class="tbl">
                <tr>
                    <th>{{tr}}Action{{/tr}}</th>
                    <th>{{tr}}Status{{/tr}}</th>
                </tr>

                <tr>
                    <td class="narrow">
                        <button class="tick" onclick="importDRC()">Import de la base</button>
                    </td>
                    <td id="drc_import"></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div id="cisp" style="display: none;">
    {{mb_include module=system template=datasources/configure_dsn dsn=cisp}}

    <h2 class="me-text-align-center me-margin-5">Import de la base CISP</h2>

    <div class="me-display-flex" style="align-items: center;">
        <div id="cisp_datasources_versions"></div>
        <div class="me-flex-grow-1">
            <table class="tbl">
                <tr>
                    <th>{{tr}}Action{{/tr}}</th>
                    <th>{{tr}}Status{{/tr}}</th>
                </tr>

                <tr>
                    <td class="narrow">
                        <button class="tick" onclick="importCISP()">Import de la base</button>
                    </td>
                    <td id="cisp_import"></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div id="favoris" style="display: none;">
    {{mb_include template=inc_config_favoris}}
</div>

<div id="Configs" style="display: none;"></div>

<div id="outils" style="display: none">
    {{mb_include template=inc_outils_cim10}}
</div>
