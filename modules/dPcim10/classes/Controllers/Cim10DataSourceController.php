<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cim10\Controllers;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\Controller;
use Ox\Core\Import\VersionedExternalDataSourceImport;
use Ox\Mediboard\Cim10\Atih\CCIM10AtihImport;
use Ox\Mediboard\Cim10\Cisp\CCIM10CispImport;
use Ox\Mediboard\Cim10\Drc\CCIM10DrcImport;
use Ox\Mediboard\Cim10\Gm\CCIM10GmImport;
use Ox\Mediboard\Cim10\Oms\CCIM10OmsImport;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 */
class Cim10DataSourceController extends Controller
{
    /**
     * @param string $datasource
     *
     * @return Response
     * @throws Exception
     */
    public function getDataSourceVersions(string $datasource): Response
    {
        switch ($datasource) {
            case 'cim':
                $datasources = [
                    'cim_oms' => [
                        'name' => 'cim10-version-oms',
                        'import' => new CCIM10OmsImport()
                    ],
                    'cim_gm' => [
                        'name' => 'cim10-version-gm',
                        'import' => new CCIM10GmImport()
                    ],
                    'cim_atih' => [
                        'name' => 'cim10-version-atih',
                        'import' => new CCIM10AtihImport()
                    ],
                ];
                break;
            case 'cisp':
                $datasources = [
                    'cisp' => [
                        'name' => 'CCISP',
                        'import' => new CCIM10CispImport()
                    ],
                ];
                break;
            case 'drc':
                $datasources = [
                    'drc' => [
                        'name' => 'mod-dPcim10-tab-drc',
                        'import' => new CCIM10DrcImport()
                    ],
                ];
                break;
            default:
                throw new Exception('Unknown datasource');
        }

        $display_messages = false;
        foreach ($datasources as $key => $datasource) {
            $import = $datasource['import'];

            $installed_version = $import->getInstalledVersion();
            if (!$installed_version && $import->hasMessages()) {
                foreach ($import->getMessages() as $message) {
                    CAppUI::setMsg(...$message);
                }

                $display_messages = true;
                break;
            }

            $datasources[$key]['available_version'] = $import->getLastAvailableVersion();
            $datasources[$key]['installed_version'] = $installed_version;
            $datasources[$key]['is_up_to_date']     = $import->isDataSourceUpToDate();
            unset($datasources['import']);
        }

        if ($display_messages) {
            return new Response(CAppUI::getMsg());
        } else {
            return $this->renderSmarty('datasources_versions', [
                'datasources' => $datasources,
            ], 'system');
        }
    }

    /**
     * @param string $action
     *
     * @return Response
     * @throws Exception
     */
    public function importCim10Oms(string $action): Response
    {
        $import = new CCIM10OmsImport();
        $import->setAction($action);

        return $this->import($import);
    }

    /**
     *
     * @return Response
     * @throws Exception
     */
    public function importCim10Atih(): Response
    {
        $import = new CCIM10AtihImport();

        return $this->import($import);
    }

    /**
     *
     * @return Response
     * @throws Exception
     */
    public function importCim10Gm(): Response
    {
        $import = new CCIM10GmImport();

        return $this->import($import);
    }

    /**
     *
     * @return Response
     * @throws Exception
     */
    public function importDrc(): Response
    {
        $import = new CCIM10DrcImport();

        return $this->import($import);
    }

    /**
     *
     * @return Response
     * @throws Exception
     */
    public function importCisp(): Response
    {
        $import = new CCIM10CispImport();

        return $this->import($import);
    }

    /**
     * Execute the given import, and returns a Response object
     *
     * @param VersionedExternalDataSourceImport $import
     *
     * @return Response
     * @throws Exception
     */
    protected function import(VersionedExternalDataSourceImport $import): Response
    {
        $import->import();

        foreach ($import->getMessages() as $message) {
            CAppUI::setMsg(...$message);
        }

        return new Response(CAppUI::getMsg());
    }
}
