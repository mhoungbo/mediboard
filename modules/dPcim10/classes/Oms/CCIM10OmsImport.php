<?php

/**
 * @package Mediboard\Cim10
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cim10\Oms;

use Exception;
use Ox\Core\Import\VersionedExternalDataSourceImport;
use Ox\Mediboard\Cim10\CImportCim10;

class CCIM10OmsImport extends VersionedExternalDataSourceImport
{
    public const SOURCE_NAME = 'cim10';
    public const DATA_DIR    = '../../base';

    public const OMS_IMPORT_FILES = ['cim10.tar.gz', 'cim10.sql'];

    public const FILES = [
        'cim10_oms' => self::OMS_IMPORT_FILES,
    ];

    protected string $action = 'import';

    public const OMS_UPDATE_ARCHIVE  = 'cim10_modifs.tar.gz';
    public const OMS_UPDATE_CSV_FILE = 'cim10_modifs.csv';

    protected const VERSION_TABLE = 'versions_cim_oms';

    public function __construct()
    {
        parent::__construct(
            self::SOURCE_NAME,
            self::DATA_DIR,
            self::FILES
        );
    }

    /**
     * @param string|null $type
     *
     * @return string
     */
    protected function getVersionsTable(?string $type = null): string
    {
        return self::VERSION_TABLE;
    }

    /**
     * @param string|null $type
     *
     * @return string
     */
    public function getLastAvailableVersion(?string $type = null): string
    {
        return array_key_last(CCodeCIM10OMS::getDataBaseVersions());
    }

    /**
     * Sets the action to perform (import, update, or all)
     *
     * @param string $action
     *
     * @return void
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @param array|null $types
     *
     * @return bool
     * @throws Exception
     */
    protected function importDatabase(?array $types = []): bool
    {
        switch ($this->action) {
            case 'import':
                return parent::importDatabase($types);
            case 'update':
                return $this->importDataFromCsv();
            case 'all':
            default:
                $result = parent::importDatabase($types);
                if (false === $result) {
                    return false;
                }
                return $this->importDataFromCsv();
        }
    }

    /**
     * @return bool
     */
    protected function importDataFromCsv(): bool
    {
        $this->setSource();
        $this->extractData(self::OMS_UPDATE_ARCHIVE);

        (new CImportCim10(
            $this->getTmpDir() . self::OMS_UPDATE_CSV_FILE,
            $this->getSource(),
            $this
        ))->run();

        return true;
    }
}
