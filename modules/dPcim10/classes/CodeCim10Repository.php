<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cim10;

use Ox\Mediboard\System\Code\Code;
use Ox\Mediboard\System\Code\CodeRepositoryInterface;

/**
 * Search and return a list of CCodeCIM10 using the type of cim configured.
 */
class CodeCim10Repository implements CodeRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getType(): string
    {
        return CCodeCIM10::CODE_TYPE;
    }

    /**
     * @inheritdoc
     */
    public function find(string $keywords, int $offset = 0, int $limit = 10): array
    {
        $codes = [];
        foreach (array_slice(CCodeCIM10::findCodes($keywords, $keywords), $offset, $limit) as $code) {
            $codes[] = Code::fromCodeInterface($code);
        }

        return $codes;
    }

    /**
     * @inheritdoc
     */
    public function getModuleName(): string
    {
        return 'dPcim10';
    }
}
