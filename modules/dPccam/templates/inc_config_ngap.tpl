{{*
 * @package Mediboard\Ccam
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<h2 class="me-text-align-center me-margin-5">Import de la base de codes NGAP</h2>

<div class="me-display-flex" style="align-items: center;">
    <div id="ngap_datasources_versions"></div>
    <div class="me-flex-grow-1">
        <table class="tbl">
            <tr>
                <th>{{tr}}Action{{/tr}}</th>
                <th>{{tr}}Status{{/tr}}</th>
            </tr>

            <tr>
                <td>
                    <button id="update_ngap" class="tick" onclick="startNGAP()">Importer la base de codes NGAP</button>
                </td>
                <td id="ngap"></td>
            </tr>
        </table>
    </div>
</div>
