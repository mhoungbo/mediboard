<?php

/**
 * @package Mediboard\Ccam
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ccam;

use Exception;
use Ox\Core\Import\VersionedExternalDataSourceImport;

class CCAMImport extends VersionedExternalDataSourceImport
{
    public const SOURCE_NAME = 'ccamV2';
    public const DATA_DIR    = '../base';

    public const FILES = [
        'ccam' => [
            'ccam.tar.gz',
            'tables.sql',
            'basedata.sql',
            'base.sql',
            'pmsi.sql',
        ],
        'ccam_forfaits' => [
            'forfaits_ccam.tar.gz',
            'forfaits_ccam.sql',
        ],
        'ngap' => [
            'ngap.tar.gz',
            'ngap.sql',
        ]
    ];

    public const VERSIONS_TABLES = [
        'ccam'          => 'versions_ccam',
        'ngap'          => 'versions_ngap',
        'ccam_forfaits' => 'versions_forfaits',
    ];

    public function __construct()
    {
        parent::__construct(
            self::SOURCE_NAME,
            self::DATA_DIR,
            self::FILES
        );
    }

    /**
     * @param string|null $type
     *
     * @return string
     * @throws Exception
     */
    protected function getVersionsTable(?string $type = null): string
    {
        if ($type && array_key_exists($type, self::VERSIONS_TABLES)) {
            return self::VERSIONS_TABLES[$type];
        } else {
            throw new Exception("Unknown data type $type for datasource " . self::SOURCE_NAME);
        }
    }

    /**
     * @param string|null $type
     *
     * @return string
     * @throws Exception
     */
    public function getLastAvailableVersion(?string $type = null): string
    {
        switch ($type) {
            case 'ccam':
                $version = array_key_last(CCCAM::getDatabaseVersions());
                break;
            case 'ngap':
                $version = array_key_last(CNGAP::getDatabaseVersions());
                break;
            case 'ccam_forfaits':
                $version = array_key_last(CCCAM::getForfaitCcamDataBaseVersions());
                break;
            default:
                throw new Exception("Unknown data type $type for datasource " . self::SOURCE_NAME);
        }

        return $version;
    }
}
