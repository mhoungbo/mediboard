<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ccam;

use Ox\Mediboard\System\Code\Code;
use Ox\Mediboard\System\Code\CodeRepositoryInterface;

/**
 * Search and return a list of CCodeNGAP
 */
class CodeNgapRepository implements CodeRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getType(): string
    {
        return CCodeNGAP::CODE_TYPE;
    }

    /**
     * @inheritdoc
     */
    public function find(string $keywords, int $offset = 0, int $limit = 10): array
    {
        $codes = [];
        foreach (array_slice(CCodeNGAP::search($keywords), $offset, $limit) as $code) {
            $codes[] = Code::fromCodeInterface($code);
        }

        return $codes;
    }

    /**
     * @inheritdoc
     */
    public function getModuleName(): string
    {
        return 'dPccam';
    }
}
