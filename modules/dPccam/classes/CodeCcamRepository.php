<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ccam;

use Ox\Mediboard\System\Code\Code;
use Ox\Mediboard\System\Code\CodeRepositoryInterface;

/**
 * Search and return a list of CCodeCCAM.
 */
class CodeCcamRepository implements CodeRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getType(): string
    {
        return CCodeCCAM::CODE_TYPE;
    }

    /**
     * @inheritdoc
     */
    public function find(string $keywords, int $offset = 0, int $limit = 10): array
    {
        $codes_array = CCodeCCAM::findCodes(
            $keywords,
            $keywords,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            "LIMIT $offset,$limit"
        );

        $codes = [];
        foreach ($codes_array as $item) {
            $codes[] = Code::fromCodeInterface(CCodeCCAM::get($item["CODE"]));
        }

        return $codes;
    }

    /**
     * @inheritdoc
     */
    public function getModuleName(): string
    {
        return 'dPccam';
    }
}
