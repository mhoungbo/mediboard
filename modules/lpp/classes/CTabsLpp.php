<?php

/**
 * @package Mediboard\Lpp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Lpp;

use Ox\Core\Module\AbstractTabsRegister;

/**
 * @codeCoverageIgnore
 */
class CTabsLpp extends AbstractTabsRegister
{
    public function registerAll(): void
    {
        $this->registerRoute('lpp_gui_index', TAB_READ);
        $this->registerRoute('lpp_gui_configure', TAB_ADMIN, self::TAB_CONFIGURE);
    }
}
