<?php

/**
 * @package Mediboard\Lpp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Lpp;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbPath;
use Ox\Core\Database\CDBF;
use Ox\Core\Import\VersionedExternalDataSourceImport;

/**
 * Import the LPP database
 */
class CLPPImport extends VersionedExternalDataSourceImport
{
    protected const SOURCE_NAME = 'lpp';
    protected const DATA_DIR    = '../base';

    protected const LPP_IMPORT_FILES = ['lpp_chapters.tar.gz', 'lpp_chapters.sql'];

    protected const FILES = [
        'lpp' => self::LPP_IMPORT_FILES,
    ];

    /** @var string The base url for */
    protected const LPP_BASE_URL = 'http://www.codage.ext.cnamts.fr';

    protected const LPP_VERSION_PATH          = '/codif/tips/telecharge/index_tele.php';
    protected const LPP_REMOTE_DATA_PATH      = '/codif/tips/download_file.php';
    protected const LPP_REMOTE_DOWNLOAD_PARAMETER = 'filename';
    protected const LPP_REMOTE_FILE_NAME      = 'tips/LPP';
    protected const LPP_REMOTE_FILE_EXTENSION = 'zip';
    protected const LPP_REMOTE_OUTPUT         = 'LPP.zip';

    protected const VERSION_TABLE = 'versions';

    protected Client $client;

    /**
     *
     */
    public function __construct()
    {
        $this->client = new Client(['base_uri' => self::LPP_BASE_URL]);

        parent::__construct(
            self::SOURCE_NAME,
            self::DATA_DIR,
            self::FILES
        );
    }

    /**
     * @param array|null $types
     * @param string     $action
     *
     * @return bool
     * @throws Exception
     */
    protected function importDatabase(?array $types = [], string $action = 'all'): bool
    {
        $result = parent::importDatabase($types);
        if (false === $result) {
            return false;
        }

        return $this->importRemoteData();
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function importRemoteData(): bool
    {
        if (!$this->downloadArchive()) {
            $this->addMessage([$this->getClassName() . '-error-unable_to_download_archive', CAppUI::UI_MSG_ERROR]);
            return false;
        }

        $archive_path = self::LPP_REMOTE_OUTPUT;

        if (!$this->extractData($archive_path)) {
            return false;
        }

        $this->setSource();

        $tables = [
            'fiche'  => 'lpp_fiche_tot*.dbf',
            'comp'   => 'lpp_comp_tot*.dbf',
            'incomp' => 'lpp_incomp_tot*.dbf',
            'histo'  => 'lpp_histo_tot*.dbf',
        ];

        foreach ($tables as $table => $filename) {
            $this->importDbfFile($table, $filename);
        }

        return true;
    }

    /**
     * Download the archive from the Assurance Maladie page
     *
     * @return bool
     */
    private function downloadArchive(): bool
    {
        $version = $this->getLastAvailableVersion();
        if ($version === '') {
            return false;
        }

        $file = self::LPP_REMOTE_FILE_NAME . "$version." . self::LPP_REMOTE_FILE_EXTENSION;

        try {
            $response = $this->client->request('GET', self::LPP_REMOTE_DATA_PATH, [
                'query' => [self::LPP_REMOTE_DOWNLOAD_PARAMETER => $file]
            ]);

            if ($response->getStatusCode() !== 200) {
                return false;
            }

            $this->source_dir = $this->getTmpDir();
            CMbPath::forceDir($this->source_dir);
            if (!file_put_contents($this->source_dir . self::LPP_REMOTE_OUTPUT, $response->getBody())) {
                return false;
            }
        } catch (GuzzleException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $table
     * @param string $filename
     *
     * @return void
     * @throws Exception
     */
    private function importDbfFile(string $table, string $filename): void
    {
        $files     = glob("$this->tmp_dir/$filename");
        $file      = reset($files);

        $dbf       = new CDBF($file);

        $this->dropTable($table);
        if ($this->createTable($table, $dbf)) {
            $this->insertData($table, $dbf);
        }
    }

    /**
     * Drop the given table and add a message if the table was dropped
     *
     * @param string $table
     *
     * @return bool
     * @throws Exception
     */
    private function dropTable(string $table): bool
    {
        if ($this->ds->exec("DROP TABLE IF EXISTS $table;")) {
            $this->addMessage([$this->getClassName() . '-msg-table_deleted', CAppUI::UI_MSG_OK, $table]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $table
     * @param CDBF   $file
     *
     * @return bool
     * @throws Exception
     */
    private function createTable(string $table, CDBF $file): bool
    {
        $query = "CREATE TABLE $table (";

        $cols = [];
        foreach ($file->dbf_names as $i => $col) {
            switch ($col['type']) {
                case 'C':
                    $cols[] = "{$col['name']} VARCHAR({$col['len']})";
                    break;
                case 'D':
                    $cols[] = "{$col['name']} DATE";
                    break;
                case 'N':
                    $cols[] = "{$col['name']} FLOAT";
                    break;
                default:
            }
        }

        $query .= implode(', ', $cols);
        $query .= ')/*! DEFAULT CHARSET=latin1 */';

        if ($this->ds->exec($query)) {
            $this->addMessage([$this->getClassName() . '-msg-table_created', CAppUI::UI_MSG_OK, $table]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $table
     * @param CDBF   $file
     *
     * @return bool
     * @throws Exception
     */
    private function insertData(string $table, CDBF $file): bool
    {
        $num_rec   = $file->dbf_num_rec;

        // Table insertion
        $query_start = "INSERT INTO $table (";
        $query_start .= implode(', ', CMbArray::pluck($file->dbf_names, 'name'));
        $query_start .= ') VALUES';

        for ($i = 0; $i < $num_rec; $i++) {
            $query  = $query_start;
            $values = [];

            if ($row = $file->getRow($i)) {
                foreach ($file->dbf_names as $j => $col) {
                    switch ($col['type']) {
                        case 'C':
                            $values[] = '"' . addslashes($row[$j]) . '"';
                            break;

                        case 'N':
                            $values[] = (($row[$j] === '') ? 'NULL' : $row[$j]);
                            break;

                        case 'D':
                            $date = 'NULL';
                            if (preg_match('/(\d{4})(\d{2})(\d{2})/', $row[$j], $parts)) {
                                $date = "\"$parts[1]-$parts[2]-$parts[3]\"";
                            }
                            $values[] = $date;
                            break;
                        default:
                    }
                }
            }

            $query .= '(' . implode(', ', $values) . ')';

            if (!$this->ds->exec($query)) {
                return false;
            }
        }

        $this->addMessage([$this->getClassName() . '-msg-%d entries added', CAppUI::UI_MSG_OK, $num_rec, $table]);

        return true;
    }

    /**
     * @param string|null $type
     *
     * @return string
     */
    protected function getVersionsTable(?string $type = null): string
    {
        return self::VERSION_TABLE;
    }

    /**
     * @param string|null $type
     *
     * @return string
     */
    public function getLastAvailableVersion(?string $type = null): string
    {
        try {
            $response = $this->client->request('GET', self::LPP_VERSION_PATH);

            if (preg_match('/LPP(\d{3,})\.zip/', $response->getBody(), $matches)) {
                $version = $matches[1];
            } else {
                throw new Exception('Unable to get LPP version');
            }
        } catch (GuzzleException|Exception $e) {
            $version = '';
            $this->addMessage([$this->getClassName() . '-error-unable_to_get_version', CAppUI::UI_MSG_ERROR]);
        }

        return $version;
    }
}
