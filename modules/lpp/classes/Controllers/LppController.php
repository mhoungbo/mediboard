<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Lpp\Controllers;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbObject;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Ccam\CCodable;
use Ox\Mediboard\Lpp\CActeLPP;
use Ox\Mediboard\Lpp\CLPPImport;
use Ox\Mediboard\Lpp\Exceptions\LppDatabaseException;
use Ox\Mediboard\Lpp\Repositories\LppChapterRepository;
use Ox\Mediboard\Lpp\Repositories\LppCodeRepository;
use Ox\Mediboard\Lpp\Repositories\LppPricingRepository;
use Ox\Mediboard\Mediusers\CMediusers;
use Symfony\Component\HttpFoundation\Response;

class LppController extends Controller
{
    /**
     * Display the index view for searching LppCodes
     *
     * @throws Exception
     */
    public function index(): Response
    {
        try {
            $repository = new LppChapterRepository();
            $chapters = $repository->loadChaptersFromParent('0');
        } catch (LppDatabaseException $e) {
            return $this->renderError($e->getMessage());
        }

        return $this->renderSmarty('vw_search', [
            'chapters' => $chapters,
            'codes'    => [],
            'start'    => 0,
            'total'    => 0,
        ]);
    }

    /**
     * Returns the list of chapters that are descendant of the given chapter
     *
     * @params string $parent_id The id of the parent (is a string because the id can have leading zeros)
     * @throws Exception
     */
    public function getDescendantChapters(RequestParams $params): Response
    {
        $parent_id = $params->get('parent_id', 'str notNull');

        try {
            $repository = new LppChapterRepository();
            $chapters = $repository->loadChaptersFromParent($parent_id);
        } catch (LppDatabaseException $e) {
            return $this->renderJsonResponse(CMbArray::toJSON(['msg' => $this->translator->tr($e->getMessage())]));
        }

        $data = ['level' => strlen($parent_id), 'chapters' => []];

        foreach ($chapters as $_chapter) {
            $data['chapters'][] = [
                'id'   => $_chapter->id,
                'view' => "$_chapter->rank - $_chapter->name",
            ];
        }

        return $this->renderJsonResponse(CMbArray::toJSON($data));
    }

    /**
     * Display the view for the given Lpp code
     *
     * @throws Exception
     */
    public function viewCode(RequestParams $params): Response
    {
        $code = $params->get('code', 'str notNull');

        try {
            $repository = new LppCodeRepository();
            $code = $repository->load($code);
            $code->loadLastPricing();
            $code->loadPricings();
            $code->loadParent();
            $code->loadCompatibilities();
            $code->loadIncompatibilities();
        } catch (LppDatabaseException $e) {
            return $this->renderError($e->getMessage());
        }

        return $this->renderSmarty('inc_code', ['code' => $code]);
    }

    /**
     * Display the view of the Lpp pricing for the given code at the given date
     *
     * @param string               $code
     * @param RequestParams        $params
     *
     * @return Response
     * @throws Exception
     */
    public function viewCodePricing(string $code, RequestParams $params): Response
    {
        $date = $params->get('date', 'date notNull');

        try {
            $repository = new LppPricingRepository();
            $pricing = $repository->loadFromDate($code, $date);
        } catch (LppDatabaseException $e) {
            return $this->renderError($e->getMessage());
        }

        return $this->renderSmarty('inc_pricing', ['pricing' => $pricing]);
    }

    /**
     * Search the Lpp codes matching the given parameters
     *
     * @throws Exception
     */
    public function searchCodes(RequestParams $params): Response
    {
        $code       = $params->get('code', 'str');
        $text       = $params->get('text', 'str');
        $chapter_id = $params->get('chapter_id', 'str');
        $start      = $params->get('start', 'num default|0');

        try {
            $repository = new LppCodeRepository();
            $codes      = $repository->search($code, $text, $chapter_id, null, $start, 100);
            $total      = $repository->count($code, $text, $chapter_id);
        } catch (LppDatabaseException $e) {
            return $this->renderError($e->getMessage());
        }

        return $this->renderSmarty('inc_search_results', [
            'codes' => $codes,
            'start' => $start,
            'total' => $total,
        ]);
    }

    /**
     * Autocomplete view for Lpp codes
     *
     * @throws Exception
     */
    public function codeAutocomplete(RequestParams $params): Response
    {
        $text         = $params->post('code', 'str');
        $executant_id = $params->post('executant_id', 'ref class|CMediusers');
        $date         = $params->post('date', 'date');

        if (!$this->isCsrfTokenValid('lpp_gui_code_autocomplete', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        try {
            $repository = new LppCodeRepository();
            $codes      = $repository->search($text, $text, null, $date, 0, 100);

            if ($executant_id) {
                $user             = CMediusers::get($executant_id);
                $prestation_codes = [];
                if ($user->spec_cpam_id) {
                    $prestation_codes = $repository->getAllowedPrestationCodesForSpeciality($user->spec_cpam_id);
                }
            }

            foreach ($codes as $_key => $_code) {
                $_code->loadLastPricing($date);
                $_code->getQualificatifsDepense();

                if (!$_code->_last_pricing->code) {
                    unset($codes[$_key]);
                }

                if ($executant_id && !in_array($_code->_last_pricing->prestation_code, $prestation_codes)) {
                    unset($codes[$_key]);
                }
            }
        } catch (LppDatabaseException $e) {
            return $this->renderError($e->getMessage());
        }

        return $this->renderSmarty('inc_code_autocomplete', ['codes' => $codes]);
    }

    /**
     * Displays the codage lpp view
     *
     * @throws Exception
     */
    public function coding(RequestParams $params): Response
    {
        $object_class = $params->get('object_class', 'str');
        $object_id    = $params->get('object_id', 'ref meta|object_class');

        /** @var CCodable $codable */
        $codable = CMbObject::loadFromGuid("$object_class-$object_id");

        $codable->loadRefsActesLPP();
        foreach ($codable->_ref_actes_lpp as $_acte) {
            $_acte->loadRefExecutant();
            $_acte->_ref_executant->loadRefFunction();
        }

        $acte_lpp = CActeLPP::createFor($codable);

        return $this->renderSmarty('inc_codage_lpp', [
            'codable'  => $codable,
            'acte_lpp' => $acte_lpp,
        ]);
    }

    /**
     * Displays the state of the LPP datasource
     *
     * @return Response
     * @throws Exception
     */
    public function getDataSourceVersion(): Response
    {
        $import = new CLPPImport();

        $available_version = $import->getLastAvailableVersion();
        $installed_version = $import->getInstalledVersion();
        $is_up_to_date = $import->isDataSourceUpToDate();

        if ((!$installed_version || !$available_version) && $import->hasMessages()) {
            foreach ($import->getMessages() as $message) {
                CAppUI::setMsg(...$message);
            }

            return $this->renderResponse(CAppUI::getMsg());
        } else {
            return $this->renderSmarty('datasources_versions', [
                'datasources' => [
                    'lpp' => [
                        'name' => 'module-lpp-court',
                        'available_version' => $available_version,
                        'installed_version' => $installed_version,
                        'is_up_to_date'     => $is_up_to_date
                    ]
                ],
            ], 'system');
        }
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function importDatasource(): Response
    {
        $import = new CLPPImport();
        $import->import();

        foreach ($import->getMessages() as $message) {
            CAppUI::setMsg(...$message);
        }

        return new Response(CAppUI::getMsg());
    }

    /**
     * @return Response
     */
    public function configure(): Response
    {
        return $this->renderSmarty('configure');
    }

    /**
     * @param string $error
     *
     * @return Response
     */
    protected function renderError(string $error): Response
    {
        return $this->renderSmarty('error.tpl', ['error' => $error]);
    }
}
