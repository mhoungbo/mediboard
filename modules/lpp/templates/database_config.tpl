{{*
 * @package Mediboard\Lpp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script type="text/javascript">
    displayDataSourceVersion = function() {
        new Url()
            .setRoute('{{url name=lpp_gui_datasource_version}}')
            .requestUpdate('lpp_datasources_version');
    };

    importDataSource = function () {
        new Url()
            .setRoute('{{url name=lpp_gui_datasource_import}}')
            .requestUpdate('lpp_import', {
                onComplete: displayDataSourceVersion.curry()
            });
    };

    Main.add(function() {
        displayDataSourceVersion();
    });
</script>

{{mb_include module=system template=datasources/configure_dsn dsn=lpp}}

<h2>Import de la base de donn�es LPP</h2>

<div class="me-display-flex" style="align-items: center;">
    <div id="lpp_datasources_version"></div>
    <div class="me-flex-grow-1">
        <table class="tbl" style="table-layout: fixed;">
            <tr>
                <th>{{tr}}Action{{/tr}}</th>
                <th>{{tr}}Status{{/tr}}</th>
            </tr>
            <tr>
                <td>
                    <button class="tick" onclick="importDataSource()">Importer la base de donn�es LPP</button>
                </td>
                <td id="lpp_import"></td>
            </tr>
        </table>
    </div>
</div>
