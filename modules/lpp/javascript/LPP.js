/**
 * @package Mediboard\Lpp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

LPP = {
    view_code_url: null,

    viewCode: function (code) {
        if (this.view_code_url) {
            new Url()
                .setRoute(this.view_code_url, 'lpp_gui_code', 'lpp')
                .addParam('code', code)
                .requestModal();
        }
    }
};
