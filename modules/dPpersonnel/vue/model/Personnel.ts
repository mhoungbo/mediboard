/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import { isModuleActive } from "@/core/utils/OxModulesManager"

let BrancardageBrancardier = OxObject
let brancardageBrancardier = new OxObject()

if (isModuleActive("brancardage")) {
    // @ts-ignore
    import("@modules/brancardage/vue/models/BrancardageBrancardier").then((mod) => {
        BrancardageBrancardier = mod.default
        brancardageBrancardier = new BrancardageBrancardier()
    })
}

export default class Personnel extends OxObject {
    protected _relationsTypes = {
        brancardage_brancardier: BrancardageBrancardier,
        mediuser: Mediuser
    }

    constructor () {
        super()
        this.type = "personnel"
    }

    get emplacement (): OxAttr<string> {
        return super.get<string>("emplacement")
    }

    get actif (): OxAttr<boolean> {
        return super.get<boolean>("actif")
    }

    get brancardiers (): typeof brancardageBrancardier[] {
        return super.loadBackwardRelation("brancardiers")
    }

    get user (): Mediuser|undefined {
        return super.loadForwardRelation<Mediuser>("user")
    }
}
