<?php

/**
 * @package Mediboard\Personnel
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Personnel\Repositories;

use Exception;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Personnel\CPersonnel;

/**
 * Repository for CPersonnel.
 */
class PersonnelRepository extends AbstractRequestApiRepository
{
    /** @var array */
    protected array $ljoin = [];

    /**
     * @inheritDoc
     */
    protected function getObjectInstance(): CStoredObject
    {
        return new CPersonnel();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    protected function massLoadRelation(array $objects, string $relation): void
    {
        switch ($relation) {
            case RequestRelations::QUERY_KEYWORD_ALL:
                $this->massLoadUser($objects);
                $this->massLoadBrancardiers($objects);
                break;
            case CPersonnel::RELATION_USER:
                $this->massLoadUser($objects);
                break;
            case CPersonnel::RELATION_BRANCARDIERS:
                $this->massLoadBrancardiers($objects);
                break;
            default:
                break;
        }
    }

    /**
     * Mass load user relationship.
     *
     * @param array $objects
     *
     * @throws Exception
     */
    private function massLoadUser(array $objects): void
    {
        CStoredObject::massLoadFwdRef($objects, "user_id");
    }

    /**
     * Return a list of personnels.
     *
     * @return array
     * @throws Exception
     */
    public function loadPersonnels(): array
    {
        return $this->find();
    }

    /**
     * Mass load brancardiers relationship
     *
     * @param array $objects
     *
     * @throws Exception
     */
    private function massLoadBrancardiers(array $objects): void
    {
        CStoredObject::massLoadBackRefs($objects, "personnel_ref_brancardiers");
    }

    /**
     * @throws Exception
     */
    private function prepareByGroup (array $where, int $group_id = null): void {
        $ds = $this->object->getDS();
        $group_id = $group_id ?? CGroups::loadCurrent()->_id;

        $this->ljoin = [];
        $this->ljoin["users_mediboard"]     = "users_mediboard.user_id = personnel.user_id";
        if (str_contains($this->order, "user_first_name") || str_contains($this->order, "user_last_name") ) {
            $this->ljoin["users"]     = "users.user_id = users_mediboard.user_id";
        }
        $this->ljoin["functions_mediboard"] = "functions_mediboard.function_id = users_mediboard.function_id";
        $this->ljoin["secondary_function"]  = "secondary_function.user_id = users_mediboard.user_id";
        $this->ljoin[]                      = "functions_mediboard secondary_function_B 
        ON secondary_function_B.function_id = secondary_function.function_id";

        $this->where = [];
        foreach ($where as $value) {
            if (str_contains($value, "actif")) {
                $this->where[] = str_replace("`actif`", "`personnel`.`actif`", $value);
            } else {
                $this->where[] = $value;
            }
        }

        $this->where[] = $ds->prepare(
            "functions_mediboard.group_id = % OR secondary_function_B.group_id = %",
            $group_id
        );
    }


    /**
     * @param array       $where
     * @param string      $limit
     * @param string|null $order
     * @param int|null    $group_id
     *
     * @return array
     * @throws Exception
     */
    public function loadPersonnelsByGroup(
        array $where,
        string $limit,
        string $order = null,
        int $group_id = null
    ): array {
        $this->order = $order;
        $this->limit = $limit;
        $this->prepareByGroup($where, $group_id);

        return $this->object->loadList(
            $this->where,
            $this->order,
            $this->limit,
            "personnel.personnel_id",
            $this->ljoin
        );
    }

    /**
     * @throws Exception
     */
    public function countByGroup (
        array $where,
        int $group_id = null
    ): ?int {
        $this->prepareByGroup($where, $group_id);

        return $this->object->countList(
            $this->where,
            "personnel.personnel_id",
            $this->ljoin
        );
    }
}
