<?php

/**
 * @package Mediboard\Personnel
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Personnel\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Personnel\CPersonnel;
use Ox\Mediboard\Personnel\Repositories\PersonnelRepository;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller API for CPersonnel.
 */
class CPersonnelController extends Controller
{
    /**
     * List of personnels
     *
     * @param RequestApi          $request_api
     * @param PersonnelRepository $personnel_repository
     *
     * @return Response
     * @throws ApiException
     * @throws Exception|InvalidArgumentException
     * @api
     */
    public function listPersonnels(RequestApi $request_api, PersonnelRepository $personnel_repository): Response
    {
        $this->enforceSlave();

        $request_filter = $request_api->getRequestFilter();
        $count          = null;

        // Group_id not exist in personnel table
        // So we recreate SQL request
        if (
            !$request_filter->isEmpty()
            && ($group_id_filter = $request_filter->getFilter("group_id")) !== null
        ) {
            $request_filter->removeFilter($request_filter->getFilterPosition("group_id"));
            $where      = $request_api->getFilterAsSQL(CSQLDataSource::get("std"));
            $group_id   = $group_id_filter->getValue(0);
            $personnels = $personnel_repository->loadPersonnelsByGroup(
                $where,
                $request_api->getLimitAsSql(),
                $request_api->getSortAsSql(),
                $group_id
            );
            $count      = $personnel_repository->countByGroup($where, $group_id);
        } else {
            $personnel_repository->initFromRequest($request_api);
            $personnels = $personnel_repository->loadPersonnels();
            $count = $personnel_repository->count();
        }

        $personnel_repository->massLoadRelations($personnels, $request_api->getRelations());

        CStoredObject::filterByPerm($personnels);

        $collection = Collection::createFromRequest($request_api, $personnels);

        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            $count
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * Return one personnel.
     *
     * @param RequestApi $request_api
     * @param CPersonnel $personnel
     *
     * @return Response
     * @throws ApiException
     * @api
     */
    public function showPersonnel(RequestApi $request_api, CPersonnel $personnel): Response
    {
        $this->enforceSlave();
        return $this->renderApiResponse(Item::createFromRequest($request_api, $personnel));
    }
}
