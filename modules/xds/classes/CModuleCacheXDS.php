<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Xds;

use Ox\Core\Module\AbstractModuleCache;

/**
 * Class CModuleCacheXDS
 * @package Ox\Interop\Xds
 */
class CModuleCacheXDS extends AbstractModuleCache
{
    public function getModuleName(): string
    {
        return 'xds';
    }

    /**
     * @inheritdoc
     */
    public function clearSpecialActions(): void
    {
        parent::clearSpecialActions();
    }
}
