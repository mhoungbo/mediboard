<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Xds\Structure;

class CXDSEbXMLCustomSlot implements XDSElementInterface
{
    /** @var string */
    private string $name = '';

    /** @var string[] */
    private array $data = [];

    /**
     * @param string $name
     *
     * @return CXDSEbXMLCustomSlot
     */
    public function setName(string $name): CXDSEbXMLCustomSlot
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string[] $data
     *
     * @return CXDSEbXMLCustomSlot
     */
    public function setData(array $data): CXDSEbXMLCustomSlot
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getData(): array
    {
        return $this->data;
    }
}
