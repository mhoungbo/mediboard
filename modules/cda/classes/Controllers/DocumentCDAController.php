<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Cda\Controllers;

use Ox\Core\CClassMap;
use Ox\Core\CMbException;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Interop\Cda\CCDAFactory;
use Ox\Interop\Cda\CCdaTools;
use Ox\Interop\Dmp\Context\DMPContextStructured;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Xds\Structure\SubmissionSet\CXDSSubmissionSet;
use Ox\Interop\Xds\XDM\CXDMRepository;
use Ox\Mediboard\Admin\CAccessMedicalData;
use Ox\Mediboard\Cabinet\CConsultAnesth;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\Vaccination\CInjection;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Prescription\CPrescription;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class DocumentCDAController extends Controller
{
    public function index(): Response
    {
        return $this->renderSmarty(
            'vw_tools_cda',
            [
                'cda' => [
                    'vsm'  => [
                        'object_class' => DMPContextStructured::SUPPORTED_OBJECT,
                        'type_cda'     => CCDAFactory::TYPE_VSM,
                        'type_xds'     => CXDSSubmissionSet::TYPE_ANS
                    ],
                    'ldl'  => [
                        'object_class' => DMPContextStructured::SUPPORTED_OBJECT,
                        'type_cda'     => [CCDAFactory::TYPE_LDL_EES, CCDAFactory::TYPE_LDL_SES],
                        'type_xds'     => CXDSSubmissionSet::TYPE_ANS
                    ],
                    'vac'  => [
                        'object_class' => DMPContextStructured::SUPPORTED_OBJECT,
                        'type_cda'     => CCDAFactory::TYPE_VACCINATION_NOTE,
                        'type_xds'     => CXDSSubmissionSet::TYPE_ANS
                    ],
                    'lvl1' => [
                        'object_class' => [CFile::class, CCompteRendu::class],
                        'types'     => [
                            ['ANS', CCDAFactory::TYPE_ANS_L1, CXDSSubmissionSet::TYPE_ANS],
                            ['DMP', CCDAFactory::TYPE_DMP, CXDSSubmissionSet::TYPE_DMP],
                            ['ZEPRA', CCDAFactory::TYPE_ZEPRA, CXDSSubmissionSet::TYPE_ZEPRA],
                        ]
                    ]
                ]
            ],
            'connectathon'
        );
    }

    public function generateDocument(RequestParams $request_params): Response
    {
        if (!$this->isCsrfTokenValid('cda_generate_document', $request_params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $type     = $request_params->post(
            'type_cda',
            'enum list|' . implode('|', CCDAFactory::getManagedTypes()) . ' notNull'
        );
        $type_xds = $request_params->post(
            'type_xds',
            'enum list|' . implode('|', CXDSSubmissionSet::TYPES_AUTHORIZED) . ' notNull'
        );

        /** @var CSejour|CConsultation|COperation|CConsultAnesth|CPrescription|CFile|CCompteRendu $object */
        $object = $this->loadObject($request_params);
        CAccessMedicalData::logAccess($object);

        $context = new DMPContextStructured($object);

        // Cr�ation du dossier m�dical car indispensable pour la cr�ation du CDA
        $patient = $context->getPatient();
        CDossierMedical::dossierMedicalId($patient->_id, $patient->_class);

        // generate XDM
        $repo_xdm = new CXDMRepository($context, $type, $type_xds);

        $options_cda = [];
        if ($file_category_id = $request_params->post('file_category_id', 'ref class|CFilesCategory')) {
            $options_cda['file_category_id'] = $file_category_id;
        }

        // generate XDM
        try {
            $repo_xdm->generateXDM($options_cda);
        } catch (CMbException $e) {
            $repo_xdm->getReport()->addData($e->getMessage(), CItemReport::SEVERITY_ERROR);
        }

        if ($type === CCDAFactory::TYPE_VSM) {
            // G�n�ration du PDF de la VSM
            $cda_file = $repo_xdm->getFileCDA();
            CCdaTools::generatePdfVSM($object, $cda_file);
        }

        $report = $repo_xdm->getReport();
        if ($repo_xdm->hasErrors() === false) {
            $factory           = $repo_xdm->getRepoCda()->getFactory();
            $factory_short_name = CClassMap::getInstance()->getShortName($factory);
            $report->addData(
                $this->translator->tr('CFile-msg-create') . ' : ' . $this->translator->tr($factory_short_name),
                CItemReport::SEVERITY_SUCCESS
            );
        }

        return $this->renderSmarty('inc_create_cda_vsm.tpl', ['report' => $report]);
    }

    /**
     * @param RequestParams $request_params
     * @return CStoredObject
     * @throws CMbModelNotFoundException
     */
    private function loadObject(RequestParams $request_params): CStoredObject
    {
        $classes      = ['CSejour', 'CConsultation', 'CPrescription', 'COperation', 'CConsultAnesth', 'CFile', 'CCompteRendu'];
        $object_class = $request_params->post('object_class', "enum list|" . implode('|', $classes) . ' notNull');
        $object_id    = $request_params->post('object_id', "ref class|$object_class notNull");

        $object = (new $object_class())::findOrFail($object_id);

        if ($injection_id = $request_params->post("injection_id", "ref class|CInjection")) {
            $injection = CInjection::findOrFail($injection_id);

            // on link l'injection avec la consultation
            $object->addToStore(CInjection::class, $injection);
        }

        return $object;
    }
}
