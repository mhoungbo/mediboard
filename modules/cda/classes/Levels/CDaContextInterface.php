<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Cda\Levels;

use Ox\Core\CStoredObject;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;

interface CDaContextInterface
{
    /**
     * Get patient for the context
     *
     * @return CPatient
     */
    public function getPatient(): CPatient;

    /**
     * Get initial object
     *
     * @return CStoredObject
     */
    public function getObject(): CStoredObject;

    /**
     * Get the object for context global
     *
     * @return COperation|CSejour|CConsultation
     */
    public function getTargetContextObject(): ?CStoredObject;

    /**
     * Get the Group link to the target context
     *
     * @return CGroups
     */
    public function getGroups(): CGroups;

    /**
     * Get pratcien responsable of context
     *
     * @return CMediusers|null
     */
    public function getPracticien(): ?CMediusers;
}
