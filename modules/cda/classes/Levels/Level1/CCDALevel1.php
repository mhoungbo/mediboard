<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Cda\Levels\Level1;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbPath;
use Ox\Interop\Cda\CCDAFactory;
use Ox\Interop\Cda\CCdaTools;
use Ox\Interop\Cda\Handle\CCDAHandle;
use Ox\Interop\Cda\Handle\CCDAHandleLevel1;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;

class CCDALevel1 extends CCDAFactory
{
    /** @var int */
    public const LEVEL = 1;

    /** @var CFile|CCompteRendu */
    protected $document_item;

    /** @var string */
    public $file_content;

    /**
     * @return CCDAHandleLevel1
     */
    public function getHandle(): ?CCDAHandle
    {
        return new CCDAHandleLevel1();
    }

    /**
     * @return CMediusers
     * @throws Exception
     */
    protected function determineAuthor(): CMediusers
    {
        /*
            1 - On r�cup�re le signataire du CR si RPPS
            2 - On r�cup�re l'auteur du document si RPPS
            3 - On vient r�cup�rer le praticien de la target
        */
        if ($this->document_item instanceof CCompteRendu) {
            $signatory = $this->document_item->loadRefSignatory();
            if ($signatory && $signatory->rpps) {
                return $signatory;
            }
        }
        $author = $this->document_item->loadRefAuthor();
        // Si on est bien sur un praticien (cas secr�taire)
        if (!$author->isPraticien() || !$author->rpps) {
            // On vient r�cup�rer le praticien de la target (CCodable)
            $author = $this->practicien;
        }

        return $author;
    }

    /**
     * @return null[]
     */
    protected function prepareServiceEvent(): array
    {
        $target_object    = $this->targetObject;
        $event_range_time = $this->retrieveRangeTimeForServiceEvent($target_object);

        return [
            "nullflavor" => null,
            "executant"  => $target_object->loadRefPraticien(),
            'time_start' => $event_range_time[0] ?? null,
            'time_stop'  => $event_range_time[1] ?? null,
        ];
    }

    /**
     * @throws CMbException
     */
    public function extractData()
    {
        // elements should be declared before
        $doc_item      = $this->document_item = $this->context->getObject();
        $this->version = $doc_item->_version;
        $this->langage = $doc_item->language;
        //R�cup�ration du dernier log qui correspond � la date de cr�ation de cette version
        $last_log            = $doc_item->loadLastLog();
        $this->date_creation = $last_log->date;
        $this->date_author   = $last_log->date;

        // parent call
        parent::extractData();

        // elements should be declared after

        //Confirmit� IHE XSD-SD => contenu non structur�
        $this->templateId[] = $this->createTemplateID("1.3.6.1.4.1.19376.1.2.20", "IHE XDS-SD");

        //G�n�ration du PDF
        $this->generatePdf();
    }

    /**
     * @throws CMbException
     */
    protected function generatePdf(): void
    {
        if ($this->document_item instanceof CFile) {
            [$content, $mediaType] = $this->generatePdfFromFile();
        } else {
            [$content, $mediaType] = $this->generatePdfFromCompteRendu();
        }

        $this->file_content = $content;
        $this->mediaType    = $mediaType;
    }

    /**
     * @throws CMbException
     */
    private function generatePdfFromFile(): array
    {
        /** @var CFile $docItem */
        $docItem   = $this->document_item;
        $mediaType = $docItem->file_type;
        $content   = null;
        switch ($docItem->file_type) {
            case "application/pdf":
                $content = CCdaTools::generatePDFA($docItem->_file_path);
                break;
            case "image/jpeg":
            case "image/jpg":
                $mediaType = "image/jpeg";
                break;
            case "application/rtf":
                $mediaType = "text/rtf";
                break;
            case "text/plain":
                $mediaType = "text/plain";
                break;
            default:
                if ($this::TYPE === self::TYPE_DMP || $this::TYPE === self::TYPE_ZEPRA) {
                    $type = $this::TYPE;
                    throw new CMbException("$type-msg-Document type authorized in $type|pl");
                } else {
                    throw new CMbException("XDS-msg-Document type authorized in XDS|pl");
                }
        }

        if ($content === null) {
            $content = $docItem->getBinaryContent();
        }

        return [$content, $mediaType];
    }

    /**
     * @return array
     * @throws CMbException
     */
    private function generatePdfFromCompteRendu(): array
    {
        /** @var CCompteRendu $compte_rendu */
        $compte_rendu = $this->document_item;
        if ($msg = $compte_rendu->makePDFpreview(1, 0)) {
            throw new CMbException($msg);
        }
        $file    = $compte_rendu->_ref_file;
        $content = CCdaTools::generatePDFA($file->_file_path);

        return [$content, 'application/pdf'];
    }

    /**
     * @return string
     */
    protected function prepareNom(): string
    {
        $docItem = $this->document_item;
        if ($docItem instanceof CCompteRendu) {
            return $docItem->nom;
        }

        /** @var CFile $docItem */
        if (isset($docItem->_file_name_cda) && $docItem->_file_name_cda) {
            return $docItem->_file_name_cda;
        }

        return CMbPath::getFilename($docItem->file_name);
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function prepareCode(): array
    {
        $docItem = $this->document_item;
        $type    = [];
        if ($docItem->type_doc_dmp) {
            $type = explode("^", $docItem->type_doc_dmp);
        }

        // Par d�faut, on prend les jeux de valeurs ASIP, DMP ou XDS
        return $this->valueset_factory::getTypeCode(CMbArray::get($type, 1));
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function getConfidentiality(): string
    {
        return $this->document_item->private ? "R" : "N";
    }

    /**
     * @param string $content_cda
     * @param int    $file_category_id
     *
     * @return CFile
     * @throws Exception
     */
    protected function getFile(string $content_cda): CFile
    {
        $file                                = parent::getFile($content_cda);
        $filename                            = $this->document_item instanceof CFile
            ? CMbPath::getFilename($this->document_item->file_name)
            : $this->document_item->nom;
        $file->file_name                     = "$filename.xml";
        $file->_file_name_cda                = CAppUI::tr(get_class($this));
        $file->author_id                     = CAppUI::$instance->user_id;
        $file->type_doc_dmp                  = $this->document_item->type_doc_dmp;
        $file->masquage_patient              = $this->document_item->masquage_patient;
        $file->masquage_praticien            = $this->document_item->masquage_praticien;
        $file->masquage_representants_legaux = $this->document_item->masquage_representants_legaux;
        $file->fillFields();

        return $file;
    }
}
