<?php

/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CSmartyDP;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Patients\CInclusionProgramme;
use Ox\Mediboard\Patients\CPatient;

$patient_id = CView::getRefCheckRead("patient_id", "ref class|CPatient");
CView::checkin();

$patient = new CPatient();
$patient->load($patient_id);
$patient->canDo();

//list des programmes
$where_inclusion               = [];
$where_inclusion["patient_id"] = " = '$patient->_id'";

$inclusion_programme = new CInclusionProgramme();
$inclusions_patient  = $inclusion_programme->loadList($where_inclusion);

$programs = CStoredObject::massLoadFwdRef($inclusions_patient, "programme_clinique_id");
$coordinateurs = CStoredObject::massLoadFwdRef($programs, "coordinateur_id");
$secondary_functions = CStoredObject::massLoadBackRefs($coordinateurs, 'secondary_functions');
CStoredObject::massLoadFwdRef($secondary_functions, 'function_id');
CStoredObject::massLoadFwdRef($coordinateurs, 'function_id');
foreach ($inclusions_patient as $key => $_inclusion_patient) {
    $_inclusion_patient->loadRefProgrammeClinique()->loadRefMedecin();
    if (!$_inclusion_patient->_ref_programme_clinique->_ref_medecin->getPerm(CPermObject::READ)) {
        unset($inclusions_patient[$key]);
    }
}

$smarty = new CSmartyDP();
$smarty->assign("inclusion_programme", $inclusion_programme);
$smarty->assign("inclusions_patient", $inclusions_patient);
$smarty->assign("patient", $patient);
$smarty->display("vw_inclusion_programme.tpl");
