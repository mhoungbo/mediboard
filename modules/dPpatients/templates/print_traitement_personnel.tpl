{{*
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=patients script=traitements}}

<script>
  document.title = $T("CPatient-tableau_traitement_personnel");
</script>

<table class="main" id="traitement_personnel" style="font-size: 14px;">
  <tr>
    <th style="width: 50%; text-align: left; vertical-align: top;">
        {{$group}} <br/>
        {{tr}}Date{{/tr}} : {{$dnow|date_format:$conf.date}} <br/>
        {{tr}}responsible_doctor{{/tr}} : {{$sejour->_ref_praticien}} <br/>
        {{tr}}common-Print by{{/tr}} : {{$app->_ref_user}} ({{$app->_ref_user->_user_type_view}})
    </th>
    <th style="width: 50%; text-align: left; vertical-align: top;">
        {{tr}}CPatient-nom-desc_court{{/tr}} : {{$patient}}
    </th>
  </tr>
  <tr>
    <td colspan="2">
      <table class="tbl" style=" text-align: center;">
        <tr>
          <th style="width: 5%;">
            <button type="button" class="add not-printable notext" style="float: left;"
                    onclick="Traitement.addLine(this.up('table'))">
                {{tr}}Add{{/tr}}
            </button>
            <button type="button" class="print not-printable notext" style="float: left;" onclick="window.print();">
                {{tr}}Print{{/tr}}
            </button>
              {{tr}}CTraitement(|pl){{/tr}}
          </th>
          <th style="width: 5%;" class="text">{{tr}}common-Start{{/tr}}</th>
          <th style="width: 5%;" class="text">{{tr}}end{{/tr}}</th>
          <th style="width: 5%;" class="text">{{tr}}CTraitement-posologie{{/tr}}</th>
          <th style="width: 5%;" class="text">{{tr}}CAntecedent-comment{{/tr}}</th>
        </tr>
          {{foreach from=$traitements key=type item=_traitements}}
              {{foreach from=$_traitements item=_traitement}}
                  {{mb_include module=patients template=print_line_traitement_personnel}}
              {{/foreach}}
          {{/foreach}}
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="6">
      <span style="text-decoration: underline;">{{tr}}CPrescription-remarque|pl{{/tr}} :</span>
      <div contenteditable=""></div>
    </td>
  </tr>
</table>
