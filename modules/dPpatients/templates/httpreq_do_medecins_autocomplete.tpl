{{*
* @package Mediboard\Patients
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<ul>
    {{foreach from=$matches item=match}}
  <li id="match-{{$match->_id}}" data-id="{{$match->_id}}">
    <div style="display: flex">
      <div style="flex: 90%">
        <strong class="view">{{$match->_view|emphasize:$keywords}}</strong>
      </div>
        {{if $match->rpps && $match->import_file_version}}
      <div>
        <i class="fas fa-address-card" style="color: #7986CB; font-size: 1.25em;"
           title="{{tr}}CMedecin-Rpps-synchronized{{/tr}}"></i>
      </div>
      {{/if}}
    </div>
    <br/>
    <br/>

      {{if $match->adresse || !$match->_count.medecins_exercices_places}}
    <small>{{$match->cp}} {{$match->ville|emphasize:$keywords}} - {{$match->adresse|emphasize:$keywords}}
      - {{$match->disciplines|@truncate:25}}</small>
    {{/if}}
  </li>

    {{foreachelse}}
  <li>
      {{tr}}CMedecin-Info-Search-empty-use-chose-button{{/tr}}
  </li>
  {{/foreach}}
</ul>
