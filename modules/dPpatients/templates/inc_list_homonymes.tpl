{{*
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl">
    <tr>
        <th class="title">{{tr var1=$patient->_view}}CPatient-List of homonyms{{/tr}}</th>
    </tr>

    {{foreach from=$sejours item=sejour}}
        <tr>
            <td>
                <span onmouseover="ObjectTooltip.createEx(this, '{{$sejour->_guid}}')">
                    {{$sejour->_view}}
                </span>
            </td>
        </tr>
    {{/foreach}}
</table>
