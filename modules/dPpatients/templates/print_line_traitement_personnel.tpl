{{*
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<tr>
  <td>
    <button type="button" class="edit not-printable notext" style="float: left;"
            onclick="Traitement.modifyLine(this)">{{tr}}Modify{{/tr}}ddd
    </button>
    <button type="button" class="up line_action not-printable notext" style="float: left; display: none;"
            onclick="Traitement.moveLine('up', this);"></button>
    <button type="button" class="down line_action not-printable notext" style="float: left; display: none;"
            onclick="Traitement.moveLine('down', this);"></button>
    <button type="button" class="trash line_action not-printable notext" style="float: left; display: none;"
            onclick="Traitement.delLine(this.up('tr'))">{{tr}}Delete{{/tr}}</button>
    <p>&nbsp;{{$_traitement}}</p>
  </td>
  <td><p>&nbsp;{{$_traitement->debut}}</p></td>
  <td><p>&nbsp;{{$_traitement->fin}}</p></td>
  <td>
    <p>
        {{if $_traitement->_class == "CTraitement" || !$_traitement->_ref_prises|@count}}
          &nbsp;
        {{/if}}
        {{if $_traitement->_class != "CTraitement"}}
            {{foreach from=$_traitement->_ref_prises item=_prise}}
                {{$_prise}} <br/>
            {{/foreach}}
        {{/if}}
    </p>
  </td>
  <td>
    <p>
      &nbsp;
        {{if $_traitement->_class != "CTraitement"}}
            {{$_traitement->commentaire}}
        {{/if}}
    </p>
  </td>
</tr>
