{{*
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=edit_for_admin value="dPpatients CMedecin edit_for_admin"|gconf}}
{{assign var=function_distinct value=$conf.dPpatients.CPatient.function_distinct}}

<script>
    setClose = function (id, view, view_update) {
        Medecin.set(id, view, view_update);
        Control.Modal.close();
    };

    var formVisible = false;

    function showAddCorres() {
        if (!formVisible) {
            $('addCorres').show();
            getForm('editFrm').focusFirstElement();
            formVisible = true;
        } else {
            hideAddCorres();
        }
    }

    function hideAddCorres() {
        $('addCorres').hide();
        formVisible = false;
    }

    function onSubmitCorrespondant(form) {
        return onSubmitFormAjax(form, {
            onComplete: function () {
                hideAddCorres();
                var formFind = getForm('find_medecin');
                formFind.elements.medecin_nom.value = form.elements.nom.value;
                formFind.elements.medecin_prenom.value = form.elements.prenom.value;
                formFind.elements.medecin_cp.value = form.elements.cp.value;
                formFind.submit();
            }
        });
    }

    Main.add(() => {
        {{if !$medecins|@count && 'rpps'|module_active}}
        $V(getForm(Medecin.current_form_search).type_search, 'annuaire');
        Medecin.search(1);
        {{/if}}
    });
</script>

<form name="fusion_medecin" action="?" method="get">
    <input type="hidden" name="m" value="system"/>
    <input type="hidden" name="a" value="object_merger"/>
    <input type="hidden" name="objects_class" value="CMedecin"/>
    <input type="hidden" name="readonly_class" value="true"/>

    {{mb_include module=system template=inc_pagination current=$start_med step=$step_med total=$count_medecins change_page="Medecin.refreshPageMedecin"}}

    <table class="tbl">
        <tr>
        {{if $can->admin || !$edit_for_admin}}
            <th class="narrow">
                <button type="button" onclick="Medecin.doMerge('fusion_medecin');"
                        class="merge notext compact me-tertiary" title="{{tr}}Merge{{/tr}}">
                    {{tr}}Merge{{/tr}}
                </button>
            </th>
            <th class="category narrow"></th>
            {{if $is_admin && $function_distinct}}
                <th style="width:10%">
                    <label title="{{tr}}CMedecin-contexte-desc{{/tr}}">{{tr}}CMedecin-contexte{{/tr}}</label>
                </th>
            {{/if}}
        {{/if}}
            <th style="width:10%">{{mb_title class=CMedecin field=nom}}</th>
            <th class="narrow">{{mb_title class=CMedecin field=sexe}}</th>
            {{if "rpps"|module_active}}
                <th class="narrow">{{mb_title class=CMedecin field=rpps}}</th>
            {{/if}}
            <th class="narrow">{{mb_title class=CExercicePlace field=raison_sociale}}</th>
            <th style="width:25%">{{mb_title class=CMedecin field=adresse}}</th>
            <th class="narrow">{{mb_title class=CMedecin field=type}}</th>
            <th style="width:10%">{{mb_title class=CMedecin field=disciplines}}</th>
            <th style="width:15%">
                <label title="{{tr}}CMedecin-contact-desc{{/tr}}">{{tr}}CMedecin-contact{{/tr}}</label>
            </th>
        {{if $dialog}}
            <th class="narrow" id="vw_medecins_th_select">{{tr}}Select{{/tr}}</th>
        {{/if}}
        </tr>

        {{foreach from=$medecins item=_medecin}}
            {{assign var=nb_line_ex_places value=$_medecin->_ref_exercice_places|@count}}
            {{assign var=exercice_places value=$_medecin->_ref_exercice_places}}
            {{assign var=medecin_exercice_places value=$_medecin->_ref_medecin_exercice_places}}
            {{if $nb_line_ex_places > 0}}
                {{assign var=first_ex_places value=$exercice_places|@first}}
                {{assign var=first_med_ex_places value=$medecin_exercice_places|@first}}
            {{/if}}
            
            <tr id="line-medecin-{{$_medecin->_id}}" {{if !$_medecin->actif}}class="hatching"{{/if}}
                onmouseover="Correspondant.addRemoveClassHoverOnOtherTrMedecin(this, 'add')" onmouseout="Correspondant.addRemoveClassHoverOnOtherTrMedecin(this, 'remove')">
                {{assign var=medecin_id value=$_medecin->_id}}
                {{mb_ternary var=href test=$dialog value="#choose" other="?m=$m&tab=vw_correspondants&medecin_id=$medecin_id"}}

                {{if $can->admin || !$edit_for_admin}}
                    <td style="text-align: center" {{if $nb_line_ex_places > 1}}rowspan="{{$nb_line_ex_places}}"{{/if}}>
                        <input type="checkbox" name="objects_id[]" value="{{$_medecin->_id}}"/>
                    </td>
                    <td {{if $nb_line_ex_places > 1}}rowspan="{{$nb_line_ex_places}}"{{/if}}>
                    {{if "rpps"|module_active}}
                        {{if $_medecin->_updatable}}
                            <button type="button" class="change notext"
                                    onclick="Correspondant.updateCorrespondant('{{$_medecin->rpps}}');">
                                {{tr}}CMedecin-action-update{{/tr}}
                            </button>
                        {{else}}
                            <span class="texticon texticon-stroke"
                                  title="{{tr}}CMedecin-search-not synchronized rpps{{/tr}}">RPPS</span>
                        {{/if}}
                    {{/if}}

                        <button type="button" class="edit notext me-tertiary"
                                onclick="Medecin.editMedecin('{{$_medecin->_id}}', Medecin.refreshPageMedecin.curry('{{$start_med}}'))">
                        </button>
                    </td>
                {{/if}}

                <!-- context -->
                {{if $is_admin && $function_distinct}}
                    <td class="text" {{if $nb_line_ex_places > 1}}rowspan="{{$nb_line_ex_places}}"{{/if}}>
                    {{if $function_distinct == 1 || $function_distinct == 2}}
                        {{if $_medecin->function_id}}
                            <span onmouseover="ObjectTooltip.createEx(this, 'CFunction-{{$_medecin->function_id}}')">
                                {{mb_value object=$_medecin field=function_id}}
                            </span>
                        {{elseif $_medecin->group_id}}
                            <span onmouseover="ObjectTooltip.createEx(this, 'CGroups-{{$_medecin->group_id}}')">
                                {{mb_value object=$_medecin field=group_id}}
                            </span>
                        {{else}}
                            <div class="empty">N/A</div>
                        {{/if}}
                    {{/if}}
                    </td>
                {{/if}}

                <!-- Nom et pr�nom -->
                <td class="text" {{if $nb_line_ex_places > 1}}rowspan="{{$nb_line_ex_places}}"{{/if}}>
                {{if $_medecin->nom || $_medecin->prenom}}
                    <p>{{$_medecin->nom}} {{$_medecin->prenom|strtolower|ucfirst}}</p>
                {{else}}
                    <div class="empty">N/A</div>
                {{/if}}
                </td>

                <!-- Sexe -->
                <td style="text-align: center" {{if $nb_line_ex_places > 1}}rowspan="{{$nb_line_ex_places}}"{{/if}}>
                {{if $_medecin->sexe == "f"}}
                    <i class="fas fa-venus" style="color: deeppink;"></i>
                {{elseif $_medecin->sexe == "m"}}
                    <i class="fas fa-mars" style="color: blue;"></i>
                {{else}}
                    <i class="fas fa-genderless" style="color: grey;"></i>
                {{/if}}
                </td>

                <!-- RPPS -->
                {{if "rpps"|module_active}}
                <td class="text" {{if $nb_line_ex_places > 1}}rowspan="{{$nb_line_ex_places}}"{{/if}}>
                    {{if $_medecin->rpps}}
                        <p>{{$_medecin->rpps}}</p>
                    {{else}}
                        <div class="empty">N/A</div>
                    {{/if}}
                </td>
                {{/if}}

                <!-- Raison sociale -->
                <td class="text">
                {{if $nb_line_ex_places > 0}}
                    {{if $first_ex_places->raison_sociale}}
                        <p>{{mb_value object=$first_ex_places field=raison_sociale}}</p>
                    {{else}}
                        <div class="empty">N/A</div>
                    {{/if}}
                {{else}}
                    <div class="empty">N/A</div>
                {{/if}}
                </td>

                <!-- Adresse -->
                <td class="text">
                {{if $nb_line_ex_places > 0}}
                    {{if $first_ex_places->adresse || $first_ex_places->cp || $first_ex_places->commune}}
                        <p>
                            {{mb_value object=$first_ex_places field=adresse}}<br/>
                            {{mb_value object=$first_ex_places field=cp}}
                            {{mb_value object=$first_ex_places field=commune}}<br/>
                        </p>
                    {{else}}
                        <div class="empty">N/A</div>
                    {{/if}}
                {{else}}
                    {{if $_medecin->adresse || $_medecin->cp || $_medecin->ville}}
                        <p>
                            {{mb_value object=$_medecin field=adresse}}
                            {{mb_value object=$_medecin field=cp}}
                            {{mb_value object=$_medecin field=ville}}
                        </p>
                    {{else}}
                        <div class="empty">N/A</div>
                    {{/if}}
                {{/if}}
                </td>

                <!-- Type -->
                <td class="text">
                {{if $nb_line_ex_places > 0 && $first_med_ex_places}}
                    {{if $first_med_ex_places->type}}
                        <p>{{mb_ditto name=type_$_medecin value=$first_med_ex_places->getFormattedValue('type')}}</p>
                    {{else}}
                        <div class="empty">N/A</div>
                    {{/if}}
                {{else}}
                    {{if $_medecin->type}}
                        <p>{{mb_value object=$_medecin field=type}}</p>
                    {{else}}
                        <div class="empty">N/A</div>
                    {{/if}}
                {{/if}}
                </td>

                <!-- Disciplines -->
                <td class="text">
                {{if $nb_line_ex_places > 0 && $first_med_ex_places}}
                    {{if $first_med_ex_places->disciplines}}
                        <p>{{mb_ditto name=discipline_$_medecin value=$first_med_ex_places->disciplines}}</p>
                    {{else}}
                        <div class="empty">N/A</div>
                    {{/if}}
                {{else}}
                    {{if $_medecin->disciplines}}
                        <p>{{mb_value object=$_medecin field=disciplines}}</p>
                    {{else}}
                        <div class="empty">N/A</div>
                    {{/if}}
                {{/if}}
                </td>

                <!-- Contact -->
                <td class="text">
                {{if $nb_line_ex_places > 0}}
                    {{if $first_ex_places->tel}}
                        <p><i class="fas fa-phone"></i> {{mb_value object=$first_ex_places field=tel}}</p>
                    {{else}}
                        <div class="empty"><i class="fas fa-phone"></i> N/A</div>
                    {{/if}}
                    {{if $first_ex_places->tel2}}
                        <p><i class="fas fa-mobile"></i> {{mb_value object=$first_ex_places field=tel2}}</p>
                    {{else}}
                        <div class="empty"><i class="fas fa-mobile"></i> N/A</div>
                    {{/if}}
                    {{if $first_ex_places->fax}}
                        <p><i class="fas fa-fax"></i> {{mb_value object=$first_ex_places field=fax}}</p>
                    {{else}}
                        <div class="empty"><i class="fas fa-fax"></i> N/A</div>
                    {{/if}}
                    {{if $first_ex_places->email}}
                        <p><i class="fas fa-envelope"></i> {{mb_value object=$first_ex_places field=email}}</p>
                    {{else}}
                        <div class="empty"><i class="fas fa-envelope"></i> N/A</div>
                    {{/if}}
                    {{if $first_med_ex_places}}
                        {{if $first_med_ex_places->mssante_address}}
                            <p><i class="fas fa-envelope"></i>{{$first_med_ex_places->mssante_address}}</p>
                        {{/if}}
                    {{/if}}
                {{else}}
                    {{if $_medecin->tel}}
                        <p><i class="fas fa-phone"></i> {{mb_value object=$_medecin field=tel}}</p>
                    {{else}}
                        <div class="empty"><i class="fas fa-phone"></i> N/A</div>
                    {{/if}}
                    {{if $_medecin->portable}}
                        <p><i class="fas fa-mobile"></i> {{mb_value object=$_medecin field=portable}}</p>
                    {{else}}
                        <div class="empty"><i class="fas fa-mobile"></i> N/A</div>
                    {{/if}}
                    {{if $_medecin->fax}}
                        <p><i class="fas fa-fax"></i> {{mb_value object=$_medecin field=fax}}</p>
                    {{else}}
                        <div class="empty"><i class="fas fa-fax"></i> N/A</div>
                    {{/if}}
                    {{if $_medecin->email}}
                        <p><i class="fas fa-envelope"></i> {{mb_value object=$_medecin field=email}}</p>
                    {{elseif $_medecin->email_apicrypt}}
                        <p><i class="fas fa-envelope"></i> {{mb_value object=$_medecin field=email_apicrypt}}</p>
                    {{else}}
                        <div class="empty"><i class="fas fa-envelope"></i> N/A</div>
                    {{/if}}
                    {{if $_medecin->mssante_address}}
                        <p><i class="fas fa-envelope"></i> {{$_medecin->mssante_address}}</p>
                    {{/if}}
                {{/if}}
                </td>

                {{if $dialog}}
                <td {{if $nb_line_ex_places > 1}}rowspan="{{$nb_line_ex_places}}"{{/if}}>
                    <button type="button" class="tick me-secondary"
                            onclick="setClose({{$_medecin->_id}}, '{{$_medecin->_view|smarty:nodefaults|JSAttribute}}', '{{$view_update}}' )">
                        {{tr}}Select{{/tr}}
                    </button>
                </td>
                {{/if}}
            </tr>
                {{foreach from=$exercice_places item=_exercice_place name="exo_place"}}
                    {{if !$smarty.foreach.exo_place.first }}
                    <tr id="other-{{$smarty.foreach.exo_place.index}}-line-medecin-{{$_medecin->_id}}" {{if !$_medecin->actif}}class="hatching"{{/if}}>
                        <!-- Raison sociale -->
                        <td class="text">
                        {{if $_exercice_place->raison_sociale}}
                            <p>{{mb_value object=$_exercice_place field=raison_sociale}}</p>
                        {{else}}
                            <div class="empty">N/A</div>
                        {{/if}}
                        </td>

                        <!-- Adresse -->
                        <td class="text">
                        {{if $_exercice_place->adresse || $_exercice_place->cp || $_exercice_place->commune}}
                            <p>
                                {{mb_value object=$_exercice_place field=adresse}}<br/>
                                {{mb_value object=$_exercice_place field=cp}}
                                {{mb_value object=$_exercice_place field=commune}}<br/>
                            </p>
                        {{else}}
                            <div class="empty">N/A</div>
                        {{/if}}
                        </td>

                        <!-- Type -->
                        <td class="text">
                        {{foreach from=$medecin_exercice_places item=_med_exercice_place}}
                            {{if $_med_exercice_place->exercice_place_id === $_exercice_place->_id}}
                                {{if $_med_exercice_place->type}}
                                    <p>{{mb_ditto name=type_$_medecin value=$_med_exercice_place->getFormattedValue('type')}}</p>
                                {{else}}
                                    <div class="empty">N/A</div>
                                {{/if}}
                            {{/if}}
                        {{foreachelse}}
                            {{if $_medecin->type}}
                                <p>{{mb_value object=$_medecin field=type}}</p>
                            {{else}}
                                <div class="empty">N/A</div>
                            {{/if}}
                        {{/foreach}}
                        </td>

                        <!-- Disciplines -->
                        <td class="text">
                        {{foreach from=$medecin_exercice_places item=_med_exercice_place}}
                            {{if $_med_exercice_place->exercice_place_id === $_exercice_place->_id}}
                                {{if $_med_exercice_place->disciplines}}
                                    <p>{{mb_ditto name=discipline_$_medecin value=$_med_exercice_place->disciplines}}</p>
                                {{else}}
                                    <div class="empty">N/A</div>
                                {{/if}}
                            {{/if}}
                        {{foreachelse}}
                            {{if $_medecin->disciplines}}
                                <p>{{mb_value object=$_medecin field=disciplines}}</p>
                            {{else}}
                                <div class="empty">N/A</div>
                            {{/if}}
                        {{/foreach}}
                        </td>

                        <!-- Contact -->
                        <td class="text">
                            {{if $_exercice_place->tel}}
                                <p><i class="fas fa-phone"></i> {{mb_value object=$_exercice_place field=tel}}</p>
                            {{else}}
                                <div class="empty"><i class="fas fa-phone"></i> N/A</div>
                            {{/if}}
                            {{if $_exercice_place->tel2}}
                                <p><i class="fas fa-mobile"></i> {{mb_value object=$_exercice_place field=mobile}}</p>
                            {{else}}
                                <div class="empty"><i class="fas fa-mobile"></i> N/A</div>
                            {{/if}}
                            {{if $_exercice_place->fax}}
                                <p><i class="fas fa-fax"></i> {{mb_value object=$_exercice_place field=fax}}</p>
                            {{else}}
                                <div class="empty"><i class="fas fa-fax"></i> N/A</div>
                            {{/if}}
                            {{if $_exercice_place->email}}
                                <p><i class="fas fa-envelope"></i> {{mb_value object=$_exercice_place field=email}}</p>
                            {{else}}
                                <div class="empty"><i class="fas fa-envelope"></i> N/A</div>
                            {{/if}}
                        </td>
                    </tr>
                    {{/if}}
                {{/foreach}}
            {{foreachelse}}
            <tr>
                <td colspan="20" class="empty">{{tr}}CMedecin.none{{/tr}}</td>
            </tr>
        {{/foreach}}
    </table>
</form>
