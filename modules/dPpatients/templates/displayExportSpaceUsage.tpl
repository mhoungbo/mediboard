{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main tbl">
  <tr>
    <th class="narrow">{{tr}}dPpatients-export-Target directory{{/tr}}</th>
    <td>{{$directory}}</td>
  </tr>
  <tr>
    <th class="narrow">{{tr}}CXMLPatientExport-Info-Patient count in directory{{/tr}}</th>
    <td>{{$patient_count}}</td>
  </tr>
  <tr>
    <th class="narrow">{{tr}}CXMLPatientExport-Info-Used space{{/tr}}</th>
    <td>{{$used_space}}</td>
  </tr>
  <tr>
    <th class="narrow">{{tr}}CXMLPatientExport-Info-Free space{{/tr}}</th>
    <td>{{$free_space}}</td>
  </tr>
</table>
