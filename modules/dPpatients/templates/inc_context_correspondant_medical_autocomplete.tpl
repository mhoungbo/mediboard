{{*
 * @package Mediboard\OxERP
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<ul>
    {{foreach from=$results item=_result}}
        <li>
            <div>
                <span class="data_autocomplete" data-class="{{$_result->_class}}" data-guid="{{$_result->_guid}}"
                      data-id="{{$_result->_id}}"><strong>{{$_result->_view}}</strong>
                </span>

                <br/>

                <small>
                    <span class="compact">{{tr}}{{$_result->_class}}{{/tr}}</span>
                </small>
            </div>
        </li>
    {{foreachelse}}
        <li>{{tr}}common-msg-No result{{/tr}}</li>
    {{/foreach}}
</ul>
