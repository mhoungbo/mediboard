{{*
 * @package Mediboard\Soins
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=patient value=""}}
{{mb_default var=date value=""}}

{{if !$patient && !$date}}
    {{mb_return}}
{{/if}}

{{if !$patient->_homonyme}}
    {{mb_return}}
{{/if}}

<script>
    showHomonymes = (patient_id, date) => {
        new Url('patients', 'showHomonymes')
            .addParam('patient_id', patient_id)
            .addParam('date', date)
            .requestModal('500');
    }
</script>

<span class="me-texticon texticon-homonyme" title="{{tr}}CPatientSignature-homonyme{{/tr}}" style="cursor: pointer;"
      onclick="showHomonymes('{{$patient->_id}}', '{{$date}}');">
    {{tr}}CPatientSignature-homonyme{{/tr}}
</span>
