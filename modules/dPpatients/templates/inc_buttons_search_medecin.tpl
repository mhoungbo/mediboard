{{*
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=show_advanced_search value=false}}

{{if "rpps"|module_active}}
    <label>
        <input type="radio" name="type_search" value="correspondant" checked />
        {{tr}}CMedecin-search-by internal corresponding{{/tr}}
    </label>
    <label>
        <input type="radio" name="type_search" value="annuaire" />
        {{tr}}CMedecin-search-by rpps directory{{/tr}}
    </label>
{{/if}}

{{if !$dialog}}
    <button class="search me-primary" type="submit">{{tr}}Search{{/tr}}</button>
{{else}}
    <button id="vw_medecins_button_dialog_search" class="search me-primary" type="submit"
            onclick="formVisible=false;">
        {{tr}}Search{{/tr}}
    </button>

    {{if $show_advanced_search}}
        <button type="button" class="search me-tertiary me-noicon"
                title="{{tr}}CPatient.other_fields{{/tr}}" onclick="Medecin.showAdvancedForm(); this.remove();">
            {{tr}}CPatient.other_fields{{/tr}}
        </button>
    {{/if}}
{{/if}}

<button type="button" name="add_out_of_repository_button" class="new" onclick="Medecin.editMedecin('0', Medecin.afterCreate);">
    {{tr}}CMedecin-action-Create outside RPPS directory{{/tr}}
</button>

<button class="erase me-tertiary" type="button" onclick="Correspondant.eraseForm(this.form)">{{tr}}Empty{{/tr}}</button>

{{if $is_admin}}
    <button type="button" class="download me-tertiary"
            onclick="popupExportCorrespondants();">{{tr}}Export-CSV{{/tr}}</button>
    <button type="button" class="import me-tertiary" onclick="popupImportCorrespondants();"
            title="Importer des correspondants m�dicaux">{{tr}}Import{{/tr}}</button>
{{/if}}
