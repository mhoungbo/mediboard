/**
 * @package Openxtrem\dentaire
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

export type BHReStatus = {
    "BMR+" ?: string;
    "BHReP" ?: string;
    "BHReR" ?:string;
    "BHReC" ?:string;
}
