/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import Patient from "@modules/dPpatients/vue/models/Patient"

export default class PatientRecordLock extends OxObject {
    constructor () {
        super()
        this.type = "patient_record_lock"
    }

    public static RELATION_CANCELLED_BY = "cancelledBy"
    public static RELATION_LOCKED_BY = "lockedBy"
    public static RELATION_PATIENT = "patient"

    protected _relationsTypes = {
        mediuser: Mediuser,
        patient: Patient
    }

    /**
     * Get if the lock is cancelled.
     *
     * @return Returns if the lock is cancelled.
     */
    get cancelled (): OxAttr<boolean> {
        return super.get("annule")
    }

    /**
     * Get the cancelled date.
     *
     * @return Returns the cancelled date.
     */
    get cancelledDate (): OxAttr<string> {
        return super.get("annule_date")
    }

    /**
     * Get the cancelled reason.
     *
     * @return Returns the cancelled reason.
     */
    get cancelledReason (): OxAttr<string> {
        return super.get("annule_motif")
    }

    /**
     * Get if the lock is for administrative reasons.
     *
     * @return Returns if the lock is for administrative reasons.
     */
    get isAdministrative (): OxAttr<boolean> {
        return super.get("administratif")
    }

    /**
     * Get if the lock is for medical reasons.
     *
     * @return Returns if the lock is for medical reasons.
     */
    get isMedical (): OxAttr<boolean> {
        return super.get("medical")
    }

    /**
     * Get the coordinates given for the lock.
     *
     * @return Returns the coordinates given for the lock.
     */
    get lockedContact (): OxAttr<string> {
        return super.get("coordonnees")
    }

    /**
     * Get the locked date.
     *
     * @return Returns the lock date.
     */
    get lockedDate (): OxAttr<string> {
        return super.get("date")
    }

    /**
     * Get the locked reason.
     *
     * @return Returns the reason for the lock.
     */
    get lockedReason (): OxAttr<string> {
        return super.get("motif")
    }

    // ============ Relation ============

    /**
     * Get the user who cancelled the lock.
     *
     * @return Returns the user who cancelled the lock.
     */
    get cancelledBy (): Mediuser | undefined {
        return this.loadForwardRelation<Mediuser>(PatientRecordLock.RELATION_CANCELLED_BY)
    }

    /**
     * Get the user who set the lock.
     *
     * @return Returns the user who set the lock.
     */
    get lockedBy (): Mediuser | undefined {
        return this.loadForwardRelation<Mediuser>(PatientRecordLock.RELATION_LOCKED_BY)
    }

    /**
     * Get the patient concerned by the lock.
     *
     * @return Returns the patient concerned by the lock.
     */
    get patient (): Patient | undefined {
        return this.loadForwardRelation<Patient>(PatientRecordLock.RELATION_PATIENT)
    }
}
