/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class InclusionProgram extends OxObject {
    protected _relationsTypes = {}

    constructor () {
        super()
        this.type = "inclusion_program"
    }

    set start (start: OxAttr<string>) {
        super.set("date_debut", start)
    }

    get start (): OxAttr<string> {
        return super.get("date_debut")
    }

    get name (): OxAttr<string> {
        return super.get("program_name")
    }
}
