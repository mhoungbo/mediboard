/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Patient from "@modules/dPpatients/vue/models/Patient"

export default class PatientDisability extends OxObject {
    constructor () {
        super()
        this.type = "patient_disability"
    }

    public static RELATION_PATIENT = "patient"

    protected _relationsTypes = {
        patient: Patient
    }

    /**
     * Type of disability.
     *
     * @return Returns the patient's disability type
     */
    get disability (): OxAttr<string> {
        return super.get("handicap")
    }

    // ============ Relation ============

    /**
     * Patient linked to disability.
     *
     * @return Returns the patient's linked to disability
     */
    get patient (): Patient | undefined {
        return this.loadForwardRelation<Patient>(PatientDisability.RELATION_PATIENT)
    }
}
