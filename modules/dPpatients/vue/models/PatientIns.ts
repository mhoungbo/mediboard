/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Patient from "@modules/dPpatients/vue/models/Patient"

export default class PatientIns extends OxObject {
    constructor () {
        super()
        this.type = "patient_ins"
    }

    public static RELATION_PATIENT = "patient"

    protected _relationsTypes = {
        patient: Patient
    }

    /**
     * Get the patient's NIR.
     *
     * @return Returns the patient's NIR.
     */
    get nir (): OxAttr<string> {
        return super.get("ins_nir")
    }

    // ============ Relation ============

    /**
     * Get the patient concerned by the INS.
     *
     * @return Returns the patient concerned by the INS.
     */
    get patient (): Patient | undefined {
        return this.loadForwardRelation<Patient>(PatientIns.RELATION_PATIENT)
    }
}
