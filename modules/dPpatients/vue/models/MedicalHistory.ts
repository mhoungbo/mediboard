/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class MedicalHistory extends OxObject {
    public static CLASS = "CAntecedent"
    public static TYPE_ALLERGY = "alle"

    constructor () {
        super()
        this.type = "antecedent"
    }

    get remark (): OxAttr<string> {
        return super.get("rques")
    }

    set remark (remark: OxAttr<string>) {
        super.set("rques", remark)
    }

    get medicalType (): OxAttr<string> {
        return super.get("type")
    }

    set medicalType (type: OxAttr<string>) {
        super.set("type", type)
    }

    get date (): OxAttr<string> {
        return super.get("date")
    }

    set date (start: OxAttr<string>) {
        super.set("date", start)
    }

    get guid (): string {
        return MedicalHistory.CLASS + "-" + super.id
    }

    get major (): OxAttr<boolean> {
        return super.get("majeur")
    }

    get important (): OxAttr<boolean> {
        return super.get("important")
    }
}
