/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import InclusionProgram from "@modules/dPpatients/vue/models/InclusionProgram"
import Injection from "@modules/dPcabinet/vue/models/Injection"
import { isModuleActive } from "@/core/utils/OxModulesManager"
import MedicalRecord from "@modules/dPpatients/vue/models/MedicalRecord"
import { BHReStatus } from "@modules/dPpatients/vue/types/PatientTypes"
import PatientIns from "@modules/dPpatients/vue/models/PatientIns"
import PatientDisability from "@modules/dPpatients/vue/models/PatientDisability"
import PatientRecordLock from "@modules/dPpatients/vue/models/PatientRecordLock"

export function isMaterniteModuleActive (): boolean {
    return isModuleActive("maternite")
}

let Pregnancy = OxObject
let pregnancy = new OxObject()
if (isMaterniteModuleActive()) {
    import("@modules/maternite/vue/models/Pregnancy").then((mod) => {
        Pregnancy = mod.default
        pregnancy = new Pregnancy()
    })
}

export default class Patient extends OxObject {
    public static RELATION_PREGNANCY = "pregnancy"
    public static RELATION_MEDICAL_RECORD = "medicalRecord"
    public static RELATION_VACCINES = "vaccines"
    public static RELATION_INCLUSION_PROGRAMS = "inclusionPrograms"
    public static RELATION_DISABILITY = "disabilities"
    public static RELATION_INS = "ins"
    public static RELATION_RECORD_LOCK = "lock"

    public static SEXES = {
        Man: "m",
        Woman: "f",
        Indefinite: "i"
    }

    public static STATE_INS = {
        Provisional: "PROV",
        Validated: "VALI",
        Recovered: "RECUP",
        Qualified: "QUAL"
    }

    protected _relationsTypes = {
        inclusion_program: InclusionProgram,
        injection: Injection,
        pregnancy: Pregnancy,
        dossierMedical: MedicalRecord,
        patient_disability: PatientDisability,
        patient_ins: PatientIns,
        patient_record_lock: PatientRecordLock
    }

    constructor () {
        super()
        this.type = "patient"
    }

    get age (): OxAttr<string> {
        return super.get("_age")
    }

    get lastName (): OxAttr<string> {
        return super.get("nom")
    }

    set lastName (name: OxAttr<string>) {
        super.set("nom", name)
    }

    get firstName (): OxAttr<string> {
        return super.get("prenom")
    }

    set firstName (firstName: OxAttr<string>) {
        super.set("prenom", firstName)
    }

    get shortView (): string {
        let prenom = ""
        if (this.firstName) {
            prenom = " " + this.firstName.charAt(0).toUpperCase() + this.firstName.slice(1).toLowerCase()
        }
        return this.lastName + prenom
    }

    get civility (): OxAttr<string> {
        return super.get("civilite")
    }

    set civility (civility: OxAttr<string>) {
        super.set("civilite", civility)
    }

    get genitals (): OxAttr<string> {
        return super.get("sexe")
    }

    get birthDate (): OxAttr<string> {
        return super.get("naissance")
    }

    set birthDate (birthDate: OxAttr<string>) {
        super.set("naissance", birthDate)
    }

    get status (): OxAttr<string> {
        return super.get("status")
    }

    set status (status: OxAttr<string>) {
        super.set("status", status)
    }

    get address (): OxAttr<string> {
        return super.get("adresse")
    }

    get tel (): OxAttr<string> {
        return super.get("tel")
    }

    get postcode (): OxAttr<string> {
        return super.get("cp")
    }

    get town (): OxAttr<string> {
        return super.get("ville")
    }

    get birthCp (): OxAttr<string> {
        return super.get("cp_naissance")
    }

    get birthPlace (): OxAttr<string> {
        return super.get("lieu_naissance")
    }

    get inclusionPrograms (): InclusionProgram[] {
        return this.loadBackwardRelation<InclusionProgram>(Patient.RELATION_INCLUSION_PROGRAMS)
    }

    get vaccines (): Injection[] {
        return this.loadBackwardRelation<Injection>(Patient.RELATION_VACCINES)
    }

    get IPP (): string {
        return super.get("_IPP")
    }

    get bhreStatus (): BHReStatus {
        return super.get("_bmr_bhre_status")
    }

    // ============ Relation ============

    /**
     * Get the patient's different disabilities.
     *
     * @return Returns the patient's disabilities.
     */
    get disabilities (): PatientDisability[] {
        return this.loadBackwardRelation<PatientDisability>(Patient.RELATION_DISABILITY)
    }

    /**
     * Get the patient's INS information.
     *
     * @return Returns the patient's INS information.
     */
    get ins (): PatientIns | undefined {
        return this.loadForwardRelation<PatientIns>(Patient.RELATION_INS)
    }

    /**
     * Get the active record lock.
     *
     * @return Returns the active record lock.
     */
    get recordLock (): PatientRecordLock | undefined {
        return this.loadForwardRelation<PatientRecordLock>(Patient.RELATION_RECORD_LOCK)
    }

    get pregnancy (): typeof pregnancy | undefined {
        return this.loadForwardRelation<typeof pregnancy>(Patient.RELATION_PREGNANCY)
    }

    set pregnancy (pregnancyObject: typeof pregnancy| undefined) {
        this.setForwardRelation<typeof pregnancy>(Patient.RELATION_PREGNANCY, pregnancyObject)
    }

    get medicalRecord (): MedicalRecord | undefined {
        return this.loadForwardRelation<MedicalRecord>(Patient.RELATION_MEDICAL_RECORD)
    }

    set medicalRecord (record: MedicalRecord | undefined) {
        this.setForwardRelation<MedicalRecord>(Patient.RELATION_MEDICAL_RECORD, record)
    }

    // ============== Link ==============

    /**
     * Get the patient's avatar.
     *
     * @return Returns the avatar link.
     */
    get avatar (): string | undefined {
        return this.links.picture
    }

    // ============= Legacy =============

    /**
     * Get the GUID.
     *
     * @deprecated Use only for legacy.
     * @return Returns the patient's guid.
     */
    get guid (): string {
        return `CPatient-${this.id}`
    }
}
