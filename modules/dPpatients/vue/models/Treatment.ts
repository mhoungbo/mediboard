/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class Treatment extends OxObject {
    public static CLASS = "CTraitement"

    constructor () {
        super()
        this.type = "traitement"
    }

    get description (): OxAttr<string> {
        return super.get("traitement")
    }

    set description (treatment: OxAttr<string>) {
        super.set("traitement", treatment)
    }

    get start (): OxAttr<string> {
        return super.get("debut")
    }

    set start (start: OxAttr<string>) {
        super.set("debut", start)
    }

    get guid (): string {
        return Treatment.CLASS + "-" + super.id
    }
}
