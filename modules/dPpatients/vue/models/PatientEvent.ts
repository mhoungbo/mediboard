/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr, OxAttrNullable } from "@/core/types/OxObjectTypes"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import File from "@modules/dPfiles/vue/models/File"
import CompteRendu from "@modules/dPcompteRendu/vue/models/CompteRendu"

export default class PatientEvent extends OxObject {
    public static RELATION_AUTHOR = "author"
    public static RELATION_FILES = "files"
    public static RELATION_COMPTE_RENDUS = "medicalReports"

    protected _relationsTypes = {
        mediuser: Mediuser,
        file: File,
        medicalReports: CompteRendu
    }

    constructor () {
        super()
        this.type = "patient_event"
    }

    get wording (): OxAttrNullable<string> {
        return super.get("libelle")
    }

    set wording (value: OxAttrNullable<string>) {
        super.set("libelle", value)
    }

    get dateTime (): OxAttr<string> {
        return super.get("date")
    }

    set dateTime (value: OxAttr<string>) {
        super.set("date", value)
    }

    get operationDateTimeEnd (): OxAttr<string> {
        return super.get("date_fin_operation")
    }

    set operationDateTimeEnd (value: OxAttr<string>) {
        super.set("date_fin_operation", value)
    }

    get libelle (): OxAttrNullable<string> {
        return super.get("description")
    }

    set libelle (value: OxAttrNullable<string>) {
        super.set("description", value)
    }

    get guid (): string {
        return "CEvenementPatient-" + super.id
    }

    get practitioner (): Mediuser | undefined {
        return this.loadForwardRelation<Mediuser>(PatientEvent.RELATION_AUTHOR)
    }

    get files (): File[] {
        return this.loadBackwardRelation<File>(PatientEvent.RELATION_FILES)
    }

    get medicalReports (): CompteRendu[] {
        return this.loadBackwardRelation<CompteRendu>(PatientEvent.RELATION_COMPTE_RENDUS)
    }
}
