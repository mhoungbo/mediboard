/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Pathology from "@modules/dPpatients/vue/models/Pathology"
import Treatment from "@modules/dPpatients/vue/models/Treatment"
import MedicalHistory from "@modules/dPpatients/vue/models/MedicalHistory"
import { isModuleActive } from "@/core/utils/OxModulesManager"

// Dynamic import
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
let MedicamentLine: any
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
let medicamentLine: any

/**
 * Prescription import
 */
if (isModuleActive("dPprescription")) {
    // @ts-ignore
    import("@modules/dPprescription/vue/models/MedicamentLine").then((mod) => {
        MedicamentLine = mod.default

        /** @type { MedicamentLine } */
        medicamentLine = new MedicamentLine()
    })
}

export enum BloodGroups {
    NONE = "?",
    O = "O",
    A = "A",
    B = "B",
    AB = "AB"
}

export enum Rhesus {
    NONE = "?",
    NEG = "NEG",
    POS = "POS"
}

export enum RhesusSign {
    NONE = "?",
    NEG = "-",
    POS = "+",
    "?" = "?"
}

export default class MedicalRecord extends OxObject {
    // Relations
    public static RELATION_MEDICAL_HISTORY = "medicalHistory"
    public static RELATION_MEDICAL_TREATMENTS = "medicalTreatments"
    public static RELATION_MEDICAL_PRESCRIPTION_LINES = "medicalPrescriptionLines"
    public static RELATION_MEDICAL_PATHOLOGIES = "medicalPathologies"
    public static RELATION_MEDICAL_EVENTS = "medicalEvents"

    protected _relationsTypes = {
        antecedent: MedicalHistory,
        traitement: Treatment,
        pathology: Pathology,
        lineMedicament: MedicamentLine
    }

    constructor () {
        super()
        this.type = "dossierMedical"
    }

    get bloodGroup (): OxAttr<string> {
        return super.get("groupe_sanguin")
    }

    get rhesus (): OxAttr<string> {
        return super.get("rhesus")
    }

    get pathologies (): Pathology[] {
        return this.loadBackwardRelation(MedicalRecord.RELATION_MEDICAL_PATHOLOGIES)
    }

    get medicalHistories (): MedicalHistory[] {
        return this.loadBackwardRelation(MedicalRecord.RELATION_MEDICAL_HISTORY)
    }

    get treatments (): Treatment[] {
        return this.loadBackwardRelation(MedicalRecord.RELATION_MEDICAL_TREATMENTS)
    }

    get medicamentLines (): typeof medicamentLine[] {
        return this.loadBackwardRelation(MedicalRecord.RELATION_MEDICAL_PRESCRIPTION_LINES)
    }

    get guid (): string {
        return "CDossierMedical-" + super.id
    }
}
