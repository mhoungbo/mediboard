/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"

export default class MedicalConstant extends OxObject {
    get dateTime (): string {
        return super.get("datetime")
    }

    set dateTime (ald: string) {
        super.set("datetime", ald)
    }

    get name (): string {
        return super.get("name")
    }

    set name (ald: string) {
        super.set("name", ald)
    }

    get unit (): string {
        return super.get("unit")
    }

    set unit (unit: string) {
        super.set("unit", unit)
    }

    get value (): string {
        return super.get("value")
    }

    set value (value: string) {
        super.set("value", value)
    }
}
