/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class Pathology extends OxObject {
    public static CLASS = "CPathologie"

    constructor () {
        super()
        this.type = "pathology"
    }

    get description (): OxAttr<string> {
        return super.get("pathologie")
    }

    set description (desc: OxAttr<string>) {
        super.set("pathologie", desc)
    }

    get start (): OxAttr<string> {
        return super.get("debut")
    }

    set start (start: OxAttr<string>) {
        super.set("debut", start)
    }

    get guid (): string {
        return Pathology.CLASS + "-" + super.id
    }
}
