/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import { setActivePinia } from "pinia"
import { storeObject } from "@/core/utils/OxStorage"
import pinia from "@/core/plugins/OxPiniaCore"
import OxTranslator from "@/core/plugins/OxTranslator"
import PatientCard from "@modules/dPpatients/vue/components/PatientCard/PatientCard.vue"
import Patient from "@modules/dPpatients/vue/models/Patient"
import MedicalRecord from "@modules/dPpatients/vue/models/MedicalRecord"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

/**
 * Test pour SampleActorLine
 */
export default class PatientCardTest extends OxTest {
    protected component = PatientCard

    private patient = new Patient()

    private medicalRecord = new MedicalRecord()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.patient.id = "1"
        this.patient.type = "consultation"
        this.patient.attributes = {
            sexe: "m",
            civilite: "enf",
            _age: "12",
            naissance: "10/05/2010",
            prenom: "TEST",
            nom: "REUSSI"
        }
        this.patient.relationships = {
            medicalRecord: {
                data: {
                    type: "dossierMedical",
                    id: "2"
                }
            }
        }
        this.medicalRecord.id = "2"
        this.medicalRecord.type = "dossierMedical"
        this.medicalRecord.attributes = {
            groupe_sanguin: "AB",
            rhesus: "NEG"
        }
        setActivePinia(pinia)
        storeObject(this.medicalRecord)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testViewPatient () {
        const patientCardComponent = this.vueComponent({
            patient: this.patient
        })
        this.assertEqual(this.privateCall(patientCardComponent, "viewPatient"), "CPatient.civilite.enf REUSSI Test")
    }

    public testIsPregnant () {
        const patientCardComponent = this.vueComponent({
            patient: this.patient
        })
        this.assertFalse(this.privateCall(patientCardComponent, "isPregnant"))
    }

    public testBloogGroup () {
        const patientCardComponent = this.vueComponent({
            patient: this.patient
        })
        this.assertEqual(this.privateCall(patientCardComponent, "bloodGroup"), "AB-")
    }

    public testOpenSIHSelectDHEPratTrue () {
        const patientCardComponent = this.vueComponent({
            patient: this.patient,
            selectDhePrat: true
        })

        patientCardComponent["listPrats"] = () => jest.fn()
        // @ts-ignore
        const listPrats = jest.spyOn(patientCardComponent, "listPrats")

        patientCardComponent["openSIH"]()
        expect(listPrats).toHaveBeenCalledTimes(1)
    }

    public testOpenSIHSelectDHEPratFalse () {
        const patientCardComponent = this.vueComponent({
            patient: this.patient,
            selectDhePrat: false
        })
        patientCardComponent["listEtablishments"] = () => jest.fn()
        // @ts-ignore
        const listEtablishments = jest.spyOn(patientCardComponent, "listEtablishments")
        patientCardComponent["openSIH"]()
        expect(listEtablishments).toHaveBeenCalledTimes(1)
    }

    public testCreationPatientCardUseDHEFalse () {
        const patientCardComponent = this.vueComponent({
            patient: this.patient,
            useDhe: false
        })
        this.assertEqual(patientCardComponent["dropdownActions"].length, 6)
    }

    public testCreationPatientCardUseDHETrue () {
        const patientCardComponent = this.vueComponent({
            patient: this.patient,
            useDhe: true
        })
        this.assertEqual(patientCardComponent["dropdownActions"].length, 7)
    }
}

(new PatientCardTest()).launchTests()
