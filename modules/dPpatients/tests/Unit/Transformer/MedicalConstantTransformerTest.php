<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Transformer;

use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\Entities\Constants\MedicalConstantUnit;
use Ox\Mediboard\Patients\Tests\Fixtures\MedicalConstantsFixtures;
use Ox\Mediboard\Patients\Transformer\Constants\MedicalConstantTransformer;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

/**
 * @deprecated
 */
class MedicalConstantTransformerTest extends OxUnitTestCase
{
    /**
     * @dataProvider medicalConstantsProvider
     */
    public function testTransform(array $data): void
    {
        $constants = (new MedicalConstantTransformer())->transform(
            $data['constants'],
            $data['selected'],
            CConstantesMedicales::getRanksFor()
        );

        // Check: $constants is an array
        $this->assertIsArray($constants);

        // Check: Count
        $this->assertEquals(count($data['expected']), count($constants));

        /** @var MedicalConstantUnit $constant */
        foreach ($constants as $constant) {
            $this->assertEquals($data['expected'][$constant->name]['name'], $constant->name);
            $this->assertEquals($data['expected'][$constant->name]['unit'], $constant->unit);
            $this->assertEquals($data['expected'][$constant->name]['value'], $constant->value);
        }
    }

    /**
     * DataProvider for testTransform
     *
     * @throws TestsException
     */
    public function medicalConstantsProvider(): array
    {
        return [
            'medicalConstantsFullAvailable'  => [
                [
                    'constants' => $this->getObjectFromFixturesReference(
                        CConstantesMedicales::class,
                        MedicalConstantsFixtures::MEDICAL_CONSTANT_TAG . 1
                    ),
                    'selected'  => array_intersect_key(
                        CConstantesMedicales::$list_constantes,
                        array_flip(['poids', 'taille', 'glycemie'])
                    ),
                    'expected'  => [
                        'poids'    => [
                            'name'  => 'poids',
                            'unit'  => 'kg',
                            'value' => '70',
                        ],
                        'taille'   => [
                            'name'  => 'taille',
                            'unit'  => 'cm',
                            'value' => '180',
                        ],
                        'glycemie' => [
                            'name'  => 'glycemie',
                            'unit'  => 'g/l',
                            'value' => '0.8',
                        ],
                    ],
                ],
            ],
            'medicalConstantsFullAvailable2' => [
                [
                    'constants' => $this->getObjectFromFixturesReference(
                        CConstantesMedicales::class,
                        MedicalConstantsFixtures::MEDICAL_CONSTANT_TAG . 2
                    ),
                    'selected'  => array_intersect_key(
                        CConstantesMedicales::$list_constantes,
                        array_flip(['taille', 'ta'])
                    ),
                    'expected'  => [
                        'taille' => [
                            'name'  => 'taille',
                            'unit'  => 'cm',
                            'value' => '184',
                        ],
                        'ta'     => [
                            'name'  => 'ta',
                            'unit'  => 'cmHg',
                            'value' => '12/8',
                        ],
                    ],
                ],
            ],
            'medicalConstantsOnlyPoids'      => [
                [
                    'constants' => $this->getObjectFromFixturesReference(
                        CConstantesMedicales::class,
                        MedicalConstantsFixtures::MEDICAL_CONSTANT_TAG . 1
                    ),
                    'selected'  => array_intersect_key(
                        CConstantesMedicales::$list_constantes,
                        array_flip(['poids'])
                    ),
                    'expected'  => [
                        'poids' => [
                            'name'  => 'poids',
                            'unit'  => 'kg',
                            'value' => '70',
                        ],
                    ],
                ],
            ],
            'medicalConstantsEmpty'          => [
                [
                    'constants' => $this->getObjectFromFixturesReference(
                        CConstantesMedicales::class,
                        MedicalConstantsFixtures::MEDICAL_CONSTANT_TAG . 2
                    ),
                    'selected'  => array_intersect_key(
                        CConstantesMedicales::$list_constantes,
                        array_flip(['poids'])
                    ),
                    'expected'  => [],
                ],
            ],
        ];
    }
}
