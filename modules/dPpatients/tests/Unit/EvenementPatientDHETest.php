<?php

/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Unit;

use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Core\Kernel\Exception\RouteException;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CEvenementPatient;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\Services\EvenementPatientDHEService;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use ReflectionException;

class EvenementPatientDHETest extends OxUnitTestCase
{
    /**
     * @dataProvider prepareResourcesProvider
     *
     * @param string $dhe_class
     * @param int    $dhe_id
     * @param string $expected
     *
     * @throws CModelObjectException
     * @throws RouteException
     * @throws TestsException
     * @throws ReflectionException
     */
    public function testPrepareResourcesRelations(string $dhe_class, int $dhe_id, string $expected): void
    {
        $ep                          = $this->generateEvenemenPatient();
        $ep->_ref_context_id400      = new CIdSante400();
        $ep->_ref_sih_id400          = new CIdSante400();
        $ep->_ref_context_id400->_id = 0;
        $ep->_ref_sih_id400->_id     = 0;
        $service                     = new EvenementPatientDHEService($ep);
        $actual                      = $this->invokePrivateMethod($service, 'constructRoute', $dhe_class, $dhe_id);
        $this->assertTrue(str_contains($actual, $expected));
    }

    /**
     * @throws CModelObjectException
     */
    private function generateEvenemenPatient(): CEvenementPatient
    {
        $patient = CPatient::getSampleObject();
        $this->storeOrFailed($patient);

        $ep                     = new CEvenementPatient();
        $ep->dossier_medical_id = CDossierMedical::dossierMedicalId($patient->_id, $patient->_class);
        $ep->date               = CMbDT::dateTime();
        $ep->libelle            = uniqid('libelle');
        $ep->owner_id           = CMediusers::get()->_id;
        $this->storeOrFailed($ep);

        return $ep;
    }

    public function prepareResourcesProvider(): array
    {
        return [
            'sejour'    => [
                'CSejour',
                1,
                '/1?relations=praticien,patient,service&fieldsets=default,admission,sortie,annulation,urgences,placement,repas,cotation',
            ],
            'operation' => [
                'COperation',
                1,
                '/1?relations=praticien,patient,anesth&fieldsets=default,examen,timing,tarif,extra',
            ],
        ];
    }
}
