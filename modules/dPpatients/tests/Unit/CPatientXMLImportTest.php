<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Unit;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Mediboard\Patients\CPatientXMLImport;
use Ox\Tests\OxUnitTestCase;

class CPatientXMLImportTest extends OxUnitTestCase
{
    protected CPatientXMLImport $import;

    /**
     * @inheritDoc
     * @throws CMbException
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $route        = CAppUI::conf('root_dir');
        $file         = $route . '/modules/dPpatients/tests/Resources/import_xml_test.xml';
        $this->import = new CPatientXMLImport($file);
    }

    /**
     * @config [CConfiguration] importTools import import_tag 0
     * @config dPpatients import_tag 0
     * @throws Exception
     */
    public function testGetImportTagDefault(): void
    {
        $this->assertEquals('migration', $this->import->getImportTag());
    }

    /**
     * @config [CConfiguration] importTools import import_tag 0
     * @throws Exception
     */
    public function testGetImportTagConfigInstance(): void
    {
        $this->assertEquals('migration', $this->import->getImportTag());
    }

    /**
     * @throws CMbException
     */
    public function testConstructFileNotExists(): void
    {
        $this->expectException(CMbException::class);
        $file_name = __DIR__ . "/../../../tmp/test_file.txt";

        new CPatientXMLImport($file_name);
    }

    /**
     * @dataProvider isDateCorrectProvider
     */
    public function testIsDateCorrect(?string $date, ?string $date_min, ?string $date_max, bool $expected): void
    {
        $this->invokePrivateMethod($this->import, 'setOptions', ['date_min' => $date_min, 'date_max' => $date_max]);

        $this->assertEquals($expected, $this->invokePrivateMethod($this->import, 'isDateCorrect', $date));
    }

    public function isDateCorrectProvider(): array
    {
        return [
            'date_null'           => [null, null, null, true],
            'date_max_ok'         => ['2023-01-01', null, '2023-01-02', true],
            'date_max_equal_date' => ['2023-01-01', null, '2023-01-01', true],
            'date_max_ko'         => ['2023-01-02', null, '2023-01-01', false],
            'date_min_ok'         => ['2023-01-01', '2022-01-01', null, true],
            'date_min_equal_date' => ['2023-01-01', '2023-01-01', null, true],
            'date_min_ko'         => ['2023-01-01', '2023-01-02', null, false],
            'date_min_ok_max_ok'  => ['2023-01-02', '2023-01-01', '2023-01-03', true],
            'date_min_ko_max_ok'  => ['2023-01-02', '2023-01-03', '2023-01-03', false],
            'date_min_ok_max_ko'  => ['2023-01-02', '2023-01-01', '2023-01-01', false],
            'date_min_ko_max_ko'  => ['2023-01-02', '2023-01-03', '2023-01-01', false],
        ];
    }
}
