<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Unit\Entities;

use Ox\Mediboard\Patients\Entities\CConstantTypeBookmark;
use Ox\Mediboard\Patients\Exceptions\MedicalConstant\ValidatorMedicalConstantBookmark;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use ReflectionException;

/**
 * Test class for CConstantTypeBookmark.
 */
class CConstantTypeBookmarkTest extends OxUnitTestCase
{
    /**
     * Assert: Check type is not available to be bookmarked.
     *
     * @dataProvider constantTypeNotAvailable
     *
     * @throws TestsException
     * @throws ReflectionException
     */
    public function testTypeNotAvailable(string $name): void
    {
        $bookmark_constant                = new CConstantTypeBookmark();
        $bookmark_constant->constant_type = $name;

        $this->expectExceptionObject(
            ValidatorMedicalConstantBookmark::typeIsNotAvailable($bookmark_constant->constant_type)
        );
        $this->invokePrivateMethod($bookmark_constant, 'checkTypeAvailable');
    }

    /**
     * DataProvider for 'testTypeNotAvailable'
     *
     * @return array[]
     */
    public function constantTypeNotAvailable(): array
    {
        return [
            'fixtures'   => ['fixtures'],
            '_imc_'      => ['_imc_'],
            'temperatur' => ['temperatur'],
        ];
    }

    /**
     * Assert: Check type is available to be bookmarked.
     *
     * @dataProvider constantTypeAvailable
     *
     * @throws ReflectionException
     * @throws TestsException
     */
    public function testTypeAvailable(string $name): void
    {
        $bookmark_constant                = new CConstantTypeBookmark();
        $bookmark_constant->constant_type = $name;

        $this->expectNotToPerformAssertions();
        $this->invokePrivateMethod($bookmark_constant, 'checkTypeAvailable');
    }

    /**
     * DataProvider for 'testTypeAvailable'
     *
     * @return array[]
     */
    public function constantTypeAvailable(): array
    {
        return [
            'poids'       => ['poids'],
            '_imc'        => ['_imc'],
            'temperature' => ['temperature'],
            '_vst'        => ['_vst'],
        ];
    }
}
