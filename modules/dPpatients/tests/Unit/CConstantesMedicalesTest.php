<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Unit;

use DateTimeImmutable;
use Ox\Core\CMbDT;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\Exceptions\MedicalConstant\ValidatorMedicalConstantException;
use Ox\Mediboard\Patients\Tests\Fixtures\MedicalConstantsFixtures;
use Ox\Mediboard\Patients\Tests\Fixtures\SimplePatientFixtures;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use ReflectionException;

class CConstantesMedicalesTest extends OxUnitTestCase
{
    /**
     * @dataProvider getForProvider
     */
    public function testGetFor(CPatient $patient, string $field, string $order, string $expected): void
    {
        [$constante, $list_datetimes, $list_contexts] =
            CConstantesMedicales::getFor($patient, null, null, null, null, null, $order);

        $this->assertEquals($expected, $constante->$field);
    }

    public function getForProvider(): array
    {
        $patient = $this->getObjectFromFixturesReference(CPatient::class, SimplePatientFixtures::SAMPLE_PATIENT);
        $user    = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_CHIR);

        $constante                = new CConstantesMedicales();
        $constante->patient_id    = $patient->_id;
        $constante->context_class = $patient->_class;
        $constante->context_id    = $patient->_id;
        $constante->user_id       = $user->_id;
        $constante->datetime      = CMbDT::dateTime('-1 minute');
        $constante->poids         = 50;
        $this->storeOrFailed($constante);

        $constante->_id      = '';
        $constante->datetime = CMbDT::dateTime();
        $constante->poids    = 60;
        $constante->_poids_g = '';
        $this->storeOrFailed($constante);

        return [
            'asc' => [
                $patient,
                'poids',
                'ASC',
                '50',
            ],

            'desc' => [
                $patient,
                'poids',
                'DESC',
                '60',
            ],
        ];
    }

    /**
     * Test medical constant is lock.
     *
     * @dataProvider providerMedicalConstantLock
     *
     * @throws TestsException|ReflectionException
     */
    public function testMedicalConstantIsLock(array $data): void
    {
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            MedicalConstantsFixtures::MEDICAL_CONSTANT_PATIENT_TAG
        );

        $medical_constant                = new CConstantesMedicales();
        $medical_constant->creation_date = (new DateTimeImmutable('now'))
            ->modify($data['modifier_date'])
            ->format('Y-m-d H:i:s');
        $medical_constant->patient_id    = $patient->_id;

        $this->expectExceptionObject(ValidatorMedicalConstantException::constantIsLock($data['lock_time']));
        $this->invokePrivateMethod($medical_constant, 'checkIsLock', $data['lock_time']);
    }

    /**
     * Provider for testMedicalConstantIsLock.
     *
     * @return array[]
     */
    public function providerMedicalConstantLock(): array
    {
        return [
            'constantLockSinceOneHour'  => [
                [
                    'modifier_date' => '-2 hours',
                    'lock_time'     => '1',
                ],
            ],
            'constantLockSinceSixHours' => [
                [
                    'modifier_date' => '-12 hours',
                    'lock_time'     => '6',
                ],
            ],
            'constantLockSinceOneDay'   => [
                [
                    'modifier_date' => '-48 hours',
                    'lock_time'     => '24',
                ],
            ],
        ];
    }

    /**
     * Test medical constant is not lock.
     *
     * @dataProvider providerMedicalConstantNotLock
     *
     * @throws TestsException
     * @throws ReflectionException
     */
    public function testConstantIsNotLock(array $data): void
    {
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            MedicalConstantsFixtures::MEDICAL_CONSTANT_PATIENT_TAG
        );

        $medical_constant                = new CConstantesMedicales();
        $medical_constant->creation_date = (new DateTimeImmutable('now'))
            ->modify($data['modifier_date'])
            ->format('Y-m-d H:i:s');
        $medical_constant->patient_id    = $patient->_id;

        $this->expectNotToPerformAssertions();
        $this->invokePrivateMethod($medical_constant, 'checkIsLock', $data['lock_time']);
    }

    /**
     * Provider for testConstantIsNotLock.
     *
     * @return array[]
     */
    public function providerMedicalConstantNotLock(): array
    {
        return [
            'constantLockInOneHour'  => [
                [
                    'modifier_date' => '-2 hours',
                    'lock_time'     => '3',
                ],
            ],
            'constantLockInSixHours' => [
                [
                    'modifier_date' => '-12 hours',
                    'lock_time'     => '18',
                ],
            ],
            'constantLockInOneDay'   => [
                [
                    'modifier_date' => '0 hours',
                    'lock_time'     => '24',
                ],
            ],
            'constantNotLock'        => [
                [
                    'modifier_date' => '0 hours',
                    'lock_time'     => '0',
                ],
            ],
        ];
    }

    /**
     * Test medical constant is modify by the author or a user with right perm.
     *
     * @dataProvider providerConstantWithAuthorOrRightPerm
     *
     * @throws TestsException|ReflectionException
     */
    public function testConstantWithAuthorOrRightPerm(array $data): void
    {
        $author = $this->getObjectFromFixturesReference(
            CMediusers::class,
            MedicalConstantsFixtures::MEDICAL_CONSTANT_USER_TAG
        );

        $medical_constant          = new CConstantesMedicales();
        $medical_constant->user_id = $author->_id;

        $this->expectNotToPerformAssertions();
        $this->invokePrivateMethod($medical_constant, 'checkIsAuthor', $data['author_id'], $data['allowed_modify']);
    }

    /**
     * Provider for testConstantWithAuthorOrRightPerm.
     *
     * @return array[]
     * @throws TestsException
     */
    public function providerConstantWithAuthorOrRightPerm(): array
    {
        return [
            'rightAuthor' => [
                [
                    'author_id'      => $this->getObjectFromFixturesReference(
                        CMediusers::class,
                        MedicalConstantsFixtures::MEDICAL_CONSTANT_USER_TAG
                    )->_id,
                    'allowed_modify' => '0',
                ],
            ],
            'rightPerm'   => [
                [
                    'author_id'      => $this->getObjectFromFixturesReference(
                        CMediusers::class,
                        UsersFixtures::REF_USER_CHIR
                    )->_id,
                    'allowed_modify' => '1',
                ],
            ],
        ];
    }

    /**
     * Test medical constant cannot modify by a user who is not the author and has not the right perm.
     *
     * @dataProvider providerConstantWithNotAuthorAndNotRightPerm
     *
     * @throws TestsException|ReflectionException
     */
    public function testConstantWithNotAuthorAndNotRightPerm(array $data): void
    {
        $author = $this->getObjectFromFixturesReference(
            CMediusers::class,
            MedicalConstantsFixtures::MEDICAL_CONSTANT_USER_TAG
        );

        $medical_constant          = new CConstantesMedicales();
        $medical_constant->user_id = $author->_id;

        $this->expectExceptionObject(ValidatorMedicalConstantException::notAuthorOfConstant());
        $this->invokePrivateMethod($medical_constant, 'checkIsAuthor', $data['author_id'], $data['allowed_modify']);
    }

    /**
     * Provider for testConstantWithAuthorOrRightPerm.
     *
     * @return array[]
     * @throws TestsException
     */
    public function providerConstantWithNotAuthorAndNotRightPerm(): array
    {
        return [
            'noRightPerm' => [
                [
                    'author_id'      => $this->getObjectFromFixturesReference(
                        CMediusers::class,
                        UsersFixtures::REF_USER_CHIR
                    )->_id,
                    'allowed_modify' => '0',
                ],
            ],
        ];
    }
}
