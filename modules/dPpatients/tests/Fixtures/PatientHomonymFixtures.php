<?php

/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Fixtures;

use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

/**
 * @description Generate patients with sejour to check the homonym feature
 */
class PatientHomonymFixtures extends Fixtures
{
    public const PATIENT_WITH_HOMONYM = 'patient_with_homonym';

    public const PATIENT_WITHOUT_HOMONYM = 'patient_without_homonym';

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function load(): void
    {
        $patient = CPatient::getSampleObject();

        $patient->nom    = 'DOUBLON';
        $patient->prenom = 'MARCEL';

        $this->store($patient);

        $this->generateSejour($patient);

        $patient->_id = '';
        $patient->prenom = 'MARCELLE';

        $this->store($patient, self::PATIENT_WITH_HOMONYM);

        $this->generateSejour($patient);

        $patient = CPatient::getSampleObject();

        $patient->nom = 'PASDOUBLON';
        $patient->prenom = 'MARCEL';

        $this->store($patient, self::PATIENT_WITHOUT_HOMONYM);

        $this->generateSejour($patient);
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function generateSejour(CPatient $patient): void
    {
        $user = self::getUser();

        /** @var CSejour $sejour */
        $sejour = CSejour::getSampleObject();

        $sejour->patient_id   = $patient->_id;
        $sejour->entree_prevue = CMbDT::dateTime();
        $sejour->sortie_prevue = CMbDT::dateTime('+2 days');
        $sejour->praticien_id = $user->_id;
        $sejour->group_id     = $user->loadRefFunction()->group_id;

        $this->store($sejour);
    }
}
