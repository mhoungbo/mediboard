<?php

/**
 * @author  SAS XtremSante <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Fixtures;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Fixture for sejour
 */
class SejourFixtures extends Fixtures implements GroupFixturesInterface
{
    public const TAG_PATIENT_DEFAULT = 'patient_sejour_fixtures-patient_default';

    public const TAG_SEJOUR_DEFAULT   = 'patient_sejour_fixtures-sejour_default';
    public const TAG_SEJOUR_CANCELLED = 'patient_sejour_fixtures-sejour_cancelled';

    private CMediusers $user;

    public static function getGroup(): array
    {
        return ['patient_sejour_fixtures'];
    }

    /**
     * @inheritDoc
     * @throws FixturesException
     * @throws CModelObjectException
     * @throws InvalidArgumentException
     */
    public function load(): void
    {
        $this->user = $this->generateUser();
        $patient    = $this->generatePatient(self::TAG_PATIENT_DEFAULT);
        $this->generateSejour($patient, false, self::TAG_SEJOUR_DEFAULT);
        $this->generateSejour($patient, true, self::TAG_SEJOUR_CANCELLED);
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function generateSejour(CPatient $patient, bool $cancelled, ?string $tag = null): void
    {
        /** @var CSejour $sejour */
        $sejour = CSejour::getSampleObject();

        $sejour->patient_id    = $patient->_id;
        $sejour->entree_prevue = CMbDT::dateTime();
        $sejour->sortie_prevue = CMbDT::dateTime('+10 days');
        $sejour->praticien_id  = $this->user->_id;
        $sejour->group_id      = $this->user->loadRefFunction()->group_id;
        $sejour->annule        = $cancelled ? '1' : '0';

        $this->store($sejour, $tag);
    }

    /**
     * @throws FixturesException
     * @throws CModelObjectException
     */
    private function generatePatient(?string $tag = null): CPatient
    {
        /** @var CPatient $patient */
        $patient = CPatient::getSampleObject();
        $this->store($patient, $tag);

        return $patient;
    }

    /**
     * @throws CModelObjectException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    private function generateUser(?string $tag = null): CMediusers
    {
        /** @var CMediusers $user */
        $user              = CMediusers::getSampleObject();
        $user->function_id = CFunctions::getCurrent()->_id;
        $user->_user_type  = array_search('Chirurgien', CUser::$types);
        $this->store($user, $tag);

        return $user;
    }
}
