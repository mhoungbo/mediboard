<?php

/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Fixtures;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CEvenementPatient;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Create CEvenementPatients fixtures
 */
class PatientEventsFixtures extends Fixtures implements GroupFixturesInterface
{
    public const TAG_MEDICAL_RECORD_DEFAULT     = 'patient_event-medical_record_default';
    public const TAG_PATIENT_EVENT_DEFAULT      = "patient_event-patient_event_default";
    public const TAG_PATIENT_EVENT_INTERVENTION = "patient_event-patient_event_intervention";

    public const TAG_PRACTITIONER = "patient_event-practitioner";

    private CMediusers $user;

    /**
     * @throws FixturesException
     * @throws CModelObjectException
     * @throws InvalidArgumentException
     */
    public function load(): void
    {
        $this->user = $this->createPractitioner();

        /** @var CPatient $patient */
        $patient = CPatient::getSampleObject();
        $this->store($patient);
        $dossier = $this->createDossierMedical($patient, self::TAG_MEDICAL_RECORD_DEFAULT);
        $this->createPatientEvent($dossier, false, self::TAG_PATIENT_EVENT_DEFAULT);
        $this->createPatientEvent($dossier, false, self::TAG_PATIENT_EVENT_INTERVENTION, 'intervention');
    }

    /**
     * @param CPatient $patient
     * @param string   $tag
     *
     * @return CDossierMedical
     * @throws FixturesException
     */
    public function createDossierMedical(CPatient $patient, string $tag): CDossierMedical
    {
        $dossier               = new CDossierMedical();
        $dossier->object_id    = $patient->_id;
        $dossier->object_class = "CPatient";
        $this->store($dossier, $tag);

        return $dossier;
    }

    /**
     * @throws FixturesException
     */
    public function createPatientEvent(CDossierMedical $dossier, bool $remind, string $tag, ?string $type = null): void
    {
        $event                     = new CEvenementPatient();
        $event->dossier_medical_id = $dossier->_id;
        $event->libelle            = 'dossier_medical_fixtures';
        $event->owner_id           = $this->user->_id;
        $event->praticien_id       = $this->user->_id;
        $event->date               = CMbDT::dateTime();
        $event->rappel             = $remind ? '1' : '0';
        $event->type               = $type;
        $event->cancel             = '0';

        if ($type === "intervention") {
            $event->date_fin_operation = CMbDT::dateTime('+1 hour');
        }

        $this->store($event, $tag);
    }

    /**
     * @throws CModelObjectException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    private function createPractitioner(): CMediusers
    {
        /** @var CMediusers $user */
        $user              = CMediusers::getSampleObject();
        $user->function_id = CFunctions::getCurrent()->_id;
        $user->_user_type  = array_search('M�decin', CUser::$types);
        $this->store($user, self::TAG_PRACTITIONER);

        return $user;
    }

    public static function getGroup(): array
    {
        return ['patient_event'];
    }
}
