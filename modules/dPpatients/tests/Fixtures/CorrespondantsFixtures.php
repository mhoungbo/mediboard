<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Fixtures;

use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Patients\CExercicePlace;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CMedecinExercicePlace;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

class CorrespondantsFixtures extends Fixtures implements GroupFixturesInterface
{
    final public const REF_MEDECIN_WITH_RPPS_AND_IMPORT_FILE_VERSION     = 'ref_medecin_with_rpps_and_import_file_version';
    final public const REF_MEDECIN_WITHOUT_RPPS_AND_IMPORT_FILE_VERSION  = 'ref_medecin_without_rpps_and_import_file_version';
    final public const REF_MEDECIN_WITHOUT_RPPS_AND_IMPORT_FILE_VERSION2 = 'ref_medecin_without_rpps_and_import_file_version2';
    final public const EXISTING_RPPS                                     = '10005219190';
    final public const NON_EXISTING_RPPS                                 = '10005219182';

    public static function getGroup(): array
    {
        return ['correspondants', 100];
    }

    /**
     * @return void
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function load(): void
    {
        $medecin = $this->createMedecinWithRppsAndImportFileVersion();
        $this->store($medecin, self::REF_MEDECIN_WITH_RPPS_AND_IMPORT_FILE_VERSION);

        $this->createExercicePlace($medecin);

        $medecin         = $this->createMedecinWithoutRppsAndImportFileVersion();
        $medecin->nom    = 'nom_test_correspondant';
        $medecin->prenom = 'prenom_test_correspondant';
        $medecin->cp     = '01000';
        $medecin->ville  = 'ville_test_correspondant';
        $this->store($medecin, self::REF_MEDECIN_WITHOUT_RPPS_AND_IMPORT_FILE_VERSION);

        $medecin         = $this->createMedecinWithoutRppsAndImportFileVersion();
        $medecin->nom    = 'nom_test_correspondant2';
        $medecin->prenom = 'prenom_test_correspondant2';
        $medecin->cp     = '01000';
        $medecin->ville  = 'ville_test_correspondant2';
        $this->store($medecin, self::REF_MEDECIN_WITHOUT_RPPS_AND_IMPORT_FILE_VERSION2);
    }

    /**
     * @return CMedecin
     * @throws CModelObjectException
     */
    private function createMedecinWithRppsAndImportFileVersion(): CMedecin
    {
        /** @var CMedecin $medecin */
        $medecin                      = CMedecin::getSampleObject();
        $medecin->rpps                = self::EXISTING_RPPS;
        $medecin->import_file_version = CMbDT::date();

        return $medecin;
    }

    /**
     * @param CMedecin $medecin
     *
     * @return void
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createExercicePlace(CMedecin $medecin): void
    {
        $exercice_place = CExercicePlace::getSampleObject();
        $this->store($exercice_place);

        $medecin_exercice_place                    = CMedecinExercicePlace::getSampleObject();
        $medecin_exercice_place->medecin_id        = $medecin->_id;
        $medecin_exercice_place->exercice_place_id = $exercice_place->_id;
        $this->store($medecin_exercice_place);
    }

    /**
     * @return CMedecin
     * @throws CModelObjectException
     */
    private function createMedecinWithoutRppsAndImportFileVersion(): CMedecin
    {
        /** @var CMedecin $medecin */
        $medecin                      = CMedecin::getSampleObject();
        $medecin->rpps                = null;
        $medecin->import_file_version = null;

        return $medecin;
    }
}
