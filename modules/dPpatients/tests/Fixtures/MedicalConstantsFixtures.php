<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Fixtures;

use DateTimeImmutable;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

class MedicalConstantsFixtures extends Fixtures implements GroupFixturesInterface
{
    public const MEDICAL_CONSTANT_USER_TAG    = 'medical_constant_user';
    public const MEDICAL_CONSTANT_PATIENT_TAG = 'medical_constant_patient';
    public const MEDICAL_CONSTANT_TAG         = 'medical_constant_';

    private CMediusers $user;
    private CPatient   $patient;

    /**
     * @inheritDoc
     *
     * @throws FixturesException
     * @throws CModelObjectException
     */
    public function load(): void
    {
        $this->createUser();
        $this->createPatient();
        $this->createMedicalConstants();
    }

    /**
     * Create user.
     *
     * @throws FixturesException
     */
    private function createUser(): void
    {
        $user        = $this->getUser(false);
        $user->actif = 1;

        $this->store($user, self::MEDICAL_CONSTANT_USER_TAG);
        $this->user = $user;
    }

    /**
     * Create patient.
     *
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createPatient(): void
    {
        $patient        = CPatient::getSampleObject();
        $patient->deces = null;

        $this->store($patient, self::MEDICAL_CONSTANT_PATIENT_TAG);
        $this->patient = $patient;
    }

    /**
     * Create medical constants.
     *
     * @throws FixturesException
     */
    private function createMedicalConstants(): void
    {
        $datetime = (new DateTimeImmutable('now'))->modify('-6 MONTH');

        // Medical constant (1)
        $medical_constant                = new CConstantesMedicales();
        $medical_constant->patient_id    = $this->patient->_id;
        $medical_constant->user_id       = $this->user->_id;
        $medical_constant->creation_date = $datetime->format('Y-m-d H:i:s');
        $medical_constant->datetime      = $datetime->format('Y-m-d H:i:s');
        $medical_constant->poids         = 70;
        $medical_constant->taille        = 180;
        $medical_constant->_glycemie     = 0.8;

        $this->store($medical_constant, self::MEDICAL_CONSTANT_TAG . 1);

        // Medical constant (2)
        $datetime = (new DateTimeImmutable('now'));

        $medical_constant                = new CConstantesMedicales();
        $medical_constant->patient_id    = $this->patient->_id;
        $medical_constant->user_id       = $this->user->_id;
        $medical_constant->creation_date = $datetime->format('Y-m-d H:i:s');
        $medical_constant->datetime      = $datetime->format('Y-m-d H:i:s');
        $medical_constant->taille        = 184;
        $medical_constant->_ta_systole   = 12;
        $medical_constant->_ta_diastole  = 8;

        $this->store($medical_constant, self::MEDICAL_CONSTANT_TAG . 2);
    }

    /**
     * @inheritDoc
     */
    public static function getGroup(): array
    {
        return ['medical_constants_fixtures', 100];
    }
}
