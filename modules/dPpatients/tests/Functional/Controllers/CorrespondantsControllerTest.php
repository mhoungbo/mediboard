<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Import\Rpps\Entity\CPersonneExercice;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\Repositories\CorrespondantRepository;
use Ox\Mediboard\Patients\Tests\Fixtures\CorrespondantsFixtures;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class CorrespondantsControllerTest extends OxWebTestCase
{
    /**
     * @param CMedecin $medecin
     * @param string $rpps
     * @param bool $expected
     * @return void
     * @throws Exception
     * @dataProvider addRppsToCorrespondantProvider
     */
    public function testAddRppsToCorrespondant(CMedecin $medecin, string $rpps, bool $expected): void
    {
        $client = $this->createClient();

        $medecin_mock = $this->createMock(CMedecin::class);
        $medecin_mock->expects($this->any())->method('store')->willReturn(null);
        $container = $this->getContainer();
        $container->set(CMedecin::class, $medecin_mock);

        $client->request(
            'POST',
            '/gui/dossierpatient/medecin/' . $medecin->_id . '/rpps',
            ['rpps' => $rpps]
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals($expected, empty($client->getResponse()->getContent()));
    }

    /**
     * @param CMedecin $medecin
     * @param array $personnes_exercice
     * @param bool $expected
     * @return void
     * @throws TestsException
     * @throws Exception
     * @dataProvider checkMatchingCorrespondantProvider
     */
    public function testCheckMatchingCorrespondant(CMedecin $medecin, array $personnes_exercice, bool $expected): void
    {
        $client = $this->createClient();

        $correspondant_repository_mock = $this->createMock(CorrespondantRepository::class);
        $correspondant_repository_mock->expects($this->any())->method('findMatchingPersonneExercice')->willReturn($personnes_exercice);
        $container = $this->getContainer();
        $container->set(CorrespondantRepository::class, $correspondant_repository_mock);

        $client->request(
            'GET',
            '/gui/dossierpatient/medecin/' . $medecin->_id . '/match'
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertIsArray($this->getResponseContent($client));
        $this->assertIsArray($this->getResponseContent($client)[0]);

        $this->assertEquals($expected, $this->getResponseContent($client)[0]['is_unique']);
        $this->assertEquals($expected, isset($this->getResponseContent($client)[0]['correspondant']) && count($this->getResponseContent($client)[0]['correspondant']));
    }

    /**
     * @throws TestsException
     */
    public function addRppsToCorrespondantProvider(): array
    {
        return [
            'Medecin with RPPS and Import file' => [
                $this->getObjectFromFixturesReference(
                    CMedecin::class,
                    CorrespondantsFixtures::REF_MEDECIN_WITH_RPPS_AND_IMPORT_FILE_VERSION
                ),
                CorrespondantsFixtures::EXISTING_RPPS,
                true
            ],
            'Medecin without RPPS and Import file' => [
                $this->getObjectFromFixturesReference(
                    CMedecin::class,
                    CorrespondantsFixtures::REF_MEDECIN_WITHOUT_RPPS_AND_IMPORT_FILE_VERSION
                ),
                CorrespondantsFixtures::EXISTING_RPPS,
                false
            ],
            'Medecin neither Existing Medecin with RPPS and Import file' => [
                $this->getObjectFromFixturesReference(
                    CMedecin::class,
                    CorrespondantsFixtures::REF_MEDECIN_WITHOUT_RPPS_AND_IMPORT_FILE_VERSION
                ),
                CorrespondantsFixtures::NON_EXISTING_RPPS,
                true
            ],
        ];
    }

    /**
     * @throws TestsException
     */
    public function checkMatchingCorrespondantProvider(): array
    {
        return [
            'Medecin with RPPS and Import file' => [
                $this->getObjectFromFixturesReference(
                    CMedecin::class,
                    CorrespondantsFixtures::REF_MEDECIN_WITH_RPPS_AND_IMPORT_FILE_VERSION
                ),
                [new CPersonneExercice()],
                false
            ],
            'Medecin and one PersonneExercice without RPPS and Import file' => [
                $this->getObjectFromFixturesReference(
                    CMedecin::class,
                    CorrespondantsFixtures::REF_MEDECIN_WITHOUT_RPPS_AND_IMPORT_FILE_VERSION
                ),
                [new CPersonneExercice()],
                true
            ],
            'Medecin and two PersonneExercice with RPPS and Import file' => [
                $this->getObjectFromFixturesReference(
                    CMedecin::class,
                    CorrespondantsFixtures::REF_MEDECIN_WITHOUT_RPPS_AND_IMPORT_FILE_VERSION2
                ),
                [new CPersonneExercice(), new CPersonneExercice()],
                false
            ],
        ];
    }
}
