<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Functional\Controllers;

use DateTimeImmutable;
use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\Tests\Fixtures\ConsultationFixtures;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Response;

class PatientsControllerTest extends OxWebTestCase
{
    /**
     * Test: Create a patient for TAMM / HM
     *
     * @throws TestsException
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function testCreateWithSIH(): CPatient
    {
        $data = [
            'nom_jeune_fille'         => 'FIXTURE',
            'nom'                     => 'FIXTURE',
            'prenom'                  => 'FIXTURE',
            'prenoms'                 => 'FIXTURE FIXTUREMODIFY',
            'prenom_usuel'            => 'FIXTURE',
            'sexe'                    => 'f',
            'civilite'                => 'mme',
            'naissance'               => '01/01/2000',
            'lieu_naissance'          => 'Saintes',
            'cp_naissance'            => '17100',
            'commune_naissance_insee' => '17415',
            'pays_naissance_insee'    => '250',
        ];

        // Create client.
        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('POST', '/api/dossierpatient/patients', [], [], [], json_encode($data));

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals(CPatient::RESOURCE_TYPE, $item->getType());
        $this->assertNotNull($item->getId());

        // Asserting: Item (attributes (some)).
        $this->assertEquals($data['nom_jeune_fille'], $item->getAttribute('nom_jeune_fille'));
        $this->assertEquals($data['nom'], $item->getAttribute('nom'));
        $this->assertEquals($data['prenom'], $item->getAttribute('prenom'));
        $this->assertEquals($data['prenoms'], $item->getAttribute('prenoms'));
        $this->assertEquals($data['sexe'], $item->getAttribute('sexe'));
        $this->assertEquals(
            (new DateTimeImmutable($data['naissance']))->format('Y-m-d'),
            $item->getAttribute('naissance')
        );

        // Get patient and create an Id400Sih
        $patient = CPatient::findOrFail($item->getId());
        $this->createId400SihForPatient($patient);

        return $patient;
    }

    /**
     * Test: Update a patient for TAMM / HM
     *
     * @throws CMbModelNotFoundException
     * @throws TestsException
     * @throws Exception
     *
     * @depends testCreateWithSIH
     */
    public function testUpdateWithSIH(CPatient $patient): CPatient
    {
        $parameters = [
            'name'      => 'FIXTUREMODIFY',
            'firstname' => 'FIXTURE',
            'birth'     => '01/01/1999',
        ];

        // Create client.
        $client = self::createClient();
        $client->request('PUT', "/api/dossierpatient/patients/$patient->_id", $parameters);

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($patient::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($patient->_id, $item->getId());

        // Asserting: Item (attributes not modified)
        $this->assertEquals($patient->nom_jeune_fille, $item->getAttribute('nom_jeune_fille'));
        $this->assertEquals($patient->prenoms, $item->getAttribute('prenoms'));
        $this->assertEquals($patient->sexe, $item->getAttribute('sexe'));

        // Asserting: Item (attributes modified).
        $this->assertEquals($parameters['name'], $item->getAttribute('nom'));
        $this->assertEquals($parameters['firstname'], $item->getAttribute('prenom'));
        $this->assertEquals(
            (new DateTimeImmutable($parameters['birth']))->format('Y-m-d'),
            $item->getAttribute('naissance')
        );

        return CPatient::findOrFail($item->getId());
    }

    /**
     *
     * @throws TestsException
     * @throws Exception
     *
     * @depends testUpdateWithSIH
     */
    public function testList(): void
    {
        $parameters = [
            'nom'                     => 'FIXTUREMODIFY',
            'prenom'                  => 'FIXTURE',
            'sexe'                    => 'f',
            'naissance'               => '1999-01-01',
            'lieu_naissance'          => 'Saintes',
            'cp_naissance'            => '17100',
            'commune_naissance_insee' => '17415',
            'pays_naissance_insee'    => '250',
        ];

        // Create client.
        $client = self::createClient();
        $client->request('GET', "/api/dossierpatient/patients", $parameters);

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Collection.
        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(1, $collection->getMeta('count'));

        /** @var Item $item */
        foreach ($collection as $item) {
            // Asserting: Item (data).
            $this->assertEquals(CPatient::RESOURCE_TYPE, $item->getType());
            $this->assertNotNull($item->getId());
        }
    }

    /**
     * Test: Show a patient by 'patient_id' identifier.
     *
     * @throws TestsException
     * @throws Exception
     *
     * @depends testUpdateWithSIH
     */
    public function testShow(CPatient $patient): CPatient
    {
        // Create client.
        $client = self::createClient();
        $client->request('GET', "/api/dossierpatient/patients/$patient->_id");

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($patient::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($patient->_id, $item->getId());

        // Asserting: Item (attributes (some)).
        $this->assertEquals($patient->nom_jeune_fille, $item->getAttribute('nom_jeune_fille'));
        $this->assertEquals($patient->nom, $item->getAttribute('nom'));
        $this->assertEquals($patient->prenom, $item->getAttribute('prenom'));
        $this->assertEquals($patient->prenoms, $item->getAttribute('prenoms'));
        $this->assertEquals($patient->sexe, $item->getAttribute('sexe'));
        $this->assertEquals($patient->naissance, $item->getAttribute('naissance'));

        return $patient;
    }

    /**
     * Test: Show patient by Id400
     *
     * @throws TestsException
     * @throws Exception
     *
     * @depends testUpdateWithSIH
     */
    public function testShowById400WithSIH(CPatient $patient): void
    {
        $parameters = [
            'patient_id' => $patient->_id,
            'cabinet_id' => '1',
        ];

        // Create client.
        $client = self::createClient();
        $client->request('GET', '/api/dossierpatient/patientbyid400sih', $parameters);

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($patient::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($patient->_id, $item->getId());
    }

    /**
     * Test: Delete a patient by 'patient_id' identifier.
     *
     * @throws Exception
     *
     * @depends testShow
     */
    public function testDelete(CPatient $patient): void
    {
        // Create client.
        $client = self::createClient();
        $client->request('DELETE', "/api/dossierpatient/patients/$patient->_id");

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_NO_CONTENT);
        $this->assertEmpty($client->getResponse()->getContent());

        // Asserting: Patient deleted.
        $this->assertFalse(CPatient::find($patient->_id));
    }

    /**
     * Test: List consultation by 'patient_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListConsultationsWithPermission(): void
    {
        $patient = $this->getPatient();

        // Create client.
        $client = self::createClient();
        $client->request('GET', "/api/dossierpatient/patients/$patient->_id/consultations");

        // Asserting: Collection (data).
        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(10, $collection->count());

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        foreach ($collection as $item) {
            $this->assertEquals(CConsultation::RESOURCE_TYPE, $item->getType());
            $this->assertNotNull($item->getId());
        }
    }

    /**
     * Test: List consultations by 'patient_id' without permission on context.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListConsultationWithoutPermissionOnContext(): void
    {
        $patient      = $this->getPatient();
        $current_user = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id]
                ?? null;
            CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "/api/dossierpatient/patients/$patient->_id/consultations");

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id]
                    = $actual_perm;
            }
        }
    }

    /**
     * Test: List consultations by 'patient_id' without permission on one consultation.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListConsultationWithoutPermissionOnOneConsultation(): void
    {
        $patient      = $this->getPatient();
        $consultation = $this->getObjectFromFixturesReference(
            CConsultation::class,
            ConsultationFixtures::TAG_NEXT_CONSULTATION_ . 1
        );
        $current_user = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$consultation->_class][$consultation->_id]
                ?? null;
            CPermObject::$users_cache[$current_user->_id][$consultation->_class][$consultation->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "/api/dossierpatient/patients/$patient->_id/consultations");

            // Asserting: Collection (data).
            $collection = $this->getJsonApiCollection($client);
            $this->assertEquals(9, $collection->count());
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$consultation->_class][$consultation->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$consultation->_class][$consultation->_id] = $actual_perm;
            }
        }
    }

    /**
     * Create an ID400 for patient.
     *
     * @throws TestsException
     * @throws Exception
     */
    private function createId400SihForPatient(CPatient $patient): void
    {
        $idx = CIdSante400::getMatch($patient->_class, 'ext_patient_id-1', $patient->_id, $patient->_id);

        if ($msg = $idx->store()) {
            throw new TestsException($msg);
        }
    }

    /**
     * Get consultation.
     *
     * @throws TestsException
     */
    private function getPatient(): CPatient
    {
        /** @var CPatient */
        return $this->getObjectFromFixturesReference(CPatient::class, ConsultationFixtures::TAG_CONSULTATION_PATIENT);
    }
}
