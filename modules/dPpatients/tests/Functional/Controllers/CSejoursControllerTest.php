<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Patients\Tests\Fixtures\SejourFixtures;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Response;

class CSejoursControllerTest extends OxWebTestCase
{
    /**
     * Test mediuser not found
     *
     * @throws Exception
     */
    public function testListSejoursFromPatient(): void
    {
        $client = self::createClient();

        /** @var CSejour $sejour_default */
        $sejour_default = $this->getSejour(SejourFixtures::TAG_SEJOUR_DEFAULT);
        /** @var CSejour $sejour_canclled */
        $sejour_cancelled = $this->getSejour(SejourFixtures::TAG_SEJOUR_CANCELLED);

        /** @var CPatient $patient */
        $patient = $this->getPatient();
        /** @var CMediusers $chir */

        $client->request('GET', "/api/dossierpatient/patients/{$patient->_id}/sejours", ['filter' => 'annule.equal.0']);

        $this->assertResponseIsSuccessful();

        $collection = $this->getJsonApiCollection($client);

        $ids = [];
        /** @var Item $item */
        foreach ($collection as $item) {
            $ids[] = $item->getId();
            $this->assertEquals(CSejour::RESOURCE_TYPE, $item->getType());
        }

        $this->assertEquals([$sejour_default->_id], $ids);
        $this->assertNotContains($sejour_cancelled, $ids);
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testListSejoursFromPatientWithoutPermOnSejour(): void
    {
        $current_user = CMediusers::get();

        /** @var CSejour $sejour_default */
        $sejour_default = $this->getSejour(SejourFixtures::TAG_SEJOUR_DEFAULT);
        /** @var CSejour $sejour_canclled */
        $sejour_cancelled = $this->getSejour(SejourFixtures::TAG_SEJOUR_CANCELLED);

        /** @var CPatient $patient */
        $patient = $this->getPatient();

        try {
            $actual_perm                                                                                 = CPermObject::$users_cache[$current_user->_id][$sejour_default->_class][$sejour_default->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$sejour_default->_class][$sejour_default->_id] = PERM_DENY;

            $client = self::createClient();
            $client->request('GET', "/api/dossierpatient/patients/{$patient->_id}/sejours");


            $this->assertResponseIsSuccessful();

            $collection = $this->getJsonApiCollection($client);

            $ids = [];
            /** @var Item $item */
            foreach ($collection as $item) {
                $ids[] = $item->getId();
                $this->assertEquals(CSejour::RESOURCE_TYPE, $item->getType());
            }

            $this->assertEquals([$sejour_cancelled->_id], $ids);
            $this->assertNotContains($sejour_default, $ids);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$sejour_default->_class][$sejour_default->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$sejour_default->_class][$sejour_default->_id] = $actual_perm;
            }
        }
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testListSejoursFromPatientWithoutPermOnPatient(): void
    {
        $current_user = CMediusers::get();

        /** @var CPatient $patient */
        $patient = $this->getPatient();

        try {
            $actual_perm                                                                                 = CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id] = PERM_DENY;

            $client = self::createClient();
            $client->request('GET', "/api/dossierpatient/patients/{$patient->_id}/sejours");


            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id] = $actual_perm;
            }
        }
    }

    /**
     * @throws TestsException
     */
    private function getSejour(string $tag): CStoredObject
    {
        return $this->getObjectFromFixturesReference(CSejour::class, $tag);
    }

    /**
     * @throws TestsException
     */
    private function getPatient(): CStoredObject
    {
        return $this->getObjectFromFixturesReference(CPatient::class, SejourFixtures::TAG_PATIENT_DEFAULT);
    }
}
