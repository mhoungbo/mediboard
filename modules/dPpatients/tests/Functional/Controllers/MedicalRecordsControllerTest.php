<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbObject;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mpm\CPrescriptionLineMedicament;
use Ox\Mediboard\Patients\CAntecedent;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CEvenementPatient;
use Ox\Mediboard\Patients\CPathologie;
use Ox\Mediboard\Patients\CTraitement;
use Ox\Mediboard\Patients\Tests\Fixtures\DossierMedicalFixtures;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test class for MedicalRecordsController.
 */
class MedicalRecordsControllerTest extends OxWebTestCase
{
    /**
     * Test: Show a report by 'dossier_medical_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testShowWithPermission(): void
    {
        $medical_record = $this->getMedicalRecord();

        // Create client.
        $client = self::createClient();
        $client->request('GET', "/api/dossierpatient/medical_records/{$medical_record->_id}", ['relations' => 'all']);

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($medical_record::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($medical_record->_id, $item->getId());

        // Asserting: Relation MedicalHistory.
        $this->assertTrue($item->hasRelationship(CDossierMedical::RELATION_MEDICAL_HISTORY));
        $this->assertEquals(
            CAntecedent::RESOURCE_TYPE,
            $item->getRelationship(CDossierMedical::RELATION_MEDICAL_HISTORY)->getFirstItem()->getType()
        );

        // Asserting: Relation MedicalTreatments.
        $this->assertTrue($item->hasRelationship(CDossierMedical::RELATION_MEDICAL_TREATMENTS));
        $this->assertEquals(
            CTraitement::RESOURCE_TYPE,
            $item->getRelationship(CDossierMedical::RELATION_MEDICAL_TREATMENTS)->getFirstItem()->getType()
        );

        // Asserting: Relation MedicalPrescriptionLines.
        $this->assertTrue($item->hasRelationship(CDossierMedical::RELATION_MEDICAL_PRESCRIPTION_LINES));
        $this->assertEquals(
            CPrescriptionLineMedicament::RESOURCE_TYPE,
            $item->getRelationship(CDossierMedical::RELATION_MEDICAL_PRESCRIPTION_LINES)->getFirstItem()->getType()
        );

        // Asserting: Relation MedicalPathologies.
        $this->assertTrue($item->hasRelationship(CDossierMedical::RELATION_MEDICAL_PATHOLOGIES));
        $this->assertEquals(
            CPathologie::RESOURCE_TYPE,
            $item->getRelationship(CDossierMedical::RELATION_MEDICAL_PATHOLOGIES)->getFirstItem()->getType()
        );

        // Asserting: Relation MedicalEvents.
        $this->assertTrue($item->hasRelationship(CDossierMedical::RELATION_MEDICAL_EVENTS));
        $this->assertEquals(
            CEvenementPatient::RESOURCE_TYPE,
            $item->getRelationship(CDossierMedical::RELATION_MEDICAL_EVENTS)->getFirstItem()->getType()
        );
    }

    /**
     * Test: Show a report by 'dossier_medical_id' without permission.
     *
     * @throws TestsException
     * @throws Exception
     * @dataProvider dataProviderShowWithoutPermission
     */
    public function testShowWithoutPermission(CMbObject $object): void
    {
        $medical_record = $this->getMedicalRecord();
        $current_user   = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "/api/dossierpatient/medical_records/{$medical_record->_id}");

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] = $actual_perm;
            }
        }
    }

    /**
     * DataProvider for testShowWithoutPermission.
     *
     * @throws TestsException
     */
    public function dataProviderShowWithoutPermission(): array
    {
        return [
            'dataObject'  => [$this->getMedicalRecord()],
            'dataContext' => [$this->getMedicalRecord()->loadRefObject()],
        ];
    }

    /**
     * Test: List events by 'dossier_medical_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListEventsWithPermission(): void
    {
        $medical_record = $this->getMedicalRecord();

        // Create client.
        $client = self::createClient();
        $client->request('GET', "/api/dossierpatient/medical_records/{$medical_record->_id}/events");

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Collection (data).
        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(3, $collection->count());

        foreach ($collection as $item) {
            $this->assertEquals(CEvenementPatient::RESOURCE_TYPE, $item->getType());
            $this->assertNotNull($item->getId());
        }
    }

    /**
     * Test: List events by 'dossier_medical_id' without permission on context.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListEventsWithoutPermissionOnContext(): void
    {
        $medical_record = $this->getMedicalRecord();
        $current_user   = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$medical_record->_class][$medical_record->_id]
                ?? null;
            CPermObject::$users_cache[$current_user->_id][$medical_record->_class][$medical_record->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "/api/dossierpatient/medical_records/{$medical_record->_id}/events");

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$medical_record->_class][$medical_record->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$medical_record->_class][$medical_record->_id]
                    = $actual_perm;
            }
        }
    }

    /**
     * Test: List events by 'dossier_medical_id' without permission on one event.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListEventsWithoutPermissionOnOneEvent(): void
    {
        $medical_record = $this->getMedicalRecord();
        $event          = $this->getObjectFromFixturesReference(
            CEvenementPatient::class,
            DossierMedicalFixtures::DOSSIER_MEDICAL_EVENT_TAG . 1
        );

        $current_user = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$event->_class][$event->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$event->_class][$event->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "/api/dossierpatient/medical_records/{$medical_record->_id}/events");

            // Asserting: Collection (data).
            $collection = $this->getJsonApiCollection($client);
            $this->assertEquals(2, $collection->count());
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$event->_class][$event->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$event->_class][$event->_id] = $actual_perm;
            }
        }
    }

    /**
     * Get medical record.
     *
     * @throws TestsException
     */
    private function getMedicalRecord(): CDossierMedical
    {
        /** @var CDossierMedical */
        return $this->getObjectFromFixturesReference(
            CDossierMedical::class,
            DossierMedicalFixtures::DOSSIER_MEDICAL
        );
    }
}
