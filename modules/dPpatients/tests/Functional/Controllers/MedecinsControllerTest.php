<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Functional\Controllers;

use Exception;
use Ox\Mediboard\Patients\CExercicePlace;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\Repositories\MedecinRepository;
use Ox\Mediboard\Patients\Tests\Fixtures\CorrespondantsFixtures;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class MedecinsControllerTest extends OxWebTestCase
{
    /**
     * @return void
     * @throws TestsException
     * @throws Exception
     */
    public function testList(): void
    {
        $client = $this->createClient();

        /** @var CMedecin $medecin */
        $medecin = $this->getObjectFromFixturesReference(
            CMedecin::class,
            CorrespondantsFixtures::REF_MEDECIN_WITH_RPPS_AND_IMPORT_FILE_VERSION
        );

        $medecin_repository_mock = $this->createMock(MedecinRepository::class);
        $medecin_repository_mock->expects($this->any())->method('listMedecinWithRpps')->willReturn([$medecin]);
        $container = $this->getContainer();
        $container->set(MedecinRepository::class, $medecin_repository_mock);

        $client->request(
            'GET',
            '/api/dossierpatient/doctors/',
            [
                'rpps' => 1,
                'relations' => 'exercicePlace,medecinExercicePlace',
                'fieldsets' => 'default,identifier,extra',
            ]
        );

        $this->assertResponseStatusCodeSame(200);
        $content = $this->getResponseContent($client);
        $this->assertIsArray($content['included']);
        $this->assertTrue($content['included'][0]['type'] == CExercicePlace::RESOURCE_TYPE);
    }

    /**
     * @return void
     * @throws TestsException
     * @throws Exception
     */
    public function testGetWithRppsIdentifier(): void
    {
        $client = $this->createClient();

        /** @var CMedecin $medecin */
        $medecin = $this->getObjectFromFixturesReference(
            CMedecin::class,
            CorrespondantsFixtures::REF_MEDECIN_WITH_RPPS_AND_IMPORT_FILE_VERSION
        );

        $medecin_repository_mock = $this->createMock(MedecinRepository::class);
        $medecin_repository_mock->expects($this->any())->method('getMedecinByRpps')->willReturn($medecin);
        $container = $this->getContainer();
        $container->set(MedecinRepository::class, $medecin_repository_mock);

        $client->request(
            'GET',
            '/api/dossierpatient/doctors/rpps',
            [
                'identifier' => $medecin->rpps,
                'relations'  => 'exercicePlace,medecinExercicePlace',
                'fieldsets'  => 'default,identifier,extra',
            ]
        );

        $this->assertResponseStatusCodeSame(200);
        $content = $this->getResponseContent($client);
        $this->assertIsArray($content['included']);
        $this->assertTrue($content['included'][0]['type'] == CExercicePlace::RESOURCE_TYPE);
    }
}
