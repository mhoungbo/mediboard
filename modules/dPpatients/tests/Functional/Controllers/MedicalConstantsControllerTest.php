<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Functional\Controllers;

use Exception;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\Tests\Fixtures\MedicalConstantsFixtures;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class MedicalConstantsControllerTest extends OxWebTestCase
{
    /**
     * Test: Verification that we get a non-empty array.
     *
     * Note: Can't go deeper in the tests because too sensitive to break.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testList(): void
    {
        // Get patient
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            MedicalConstantsFixtures::MEDICAL_CONSTANT_PATIENT_TAG
        );

        // Create client
        $client = self::createClient();
        $client->request('GET', "/api/dossierpatient/patients/$patient->_id/constants");

        // Asserting successful
        $this->assertResponseIsSuccessful();

        // Collection
        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(15, $collection->getMeta('count'));
    }

    /**
     * Test: Verification that we get a non-empty array.
     *
     * Note: Can't go deeper in the tests because too sensitive to break.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testLast(): void
    {
        // Get patient
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            MedicalConstantsFixtures::MEDICAL_CONSTANT_PATIENT_TAG
        );

        // Create client
        $client = self::createClient();
        $client->request('GET', "/api/dossierpatient/patients/$patient->_id/constants", ['last' => '1']);

        // Asserting successful
        $this->assertResponseIsSuccessful();

        // Collection
        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(11, $collection->getMeta('count'));

        /** @var Item $item */
        foreach ($collection as $item) {
            $this->assertEquals(
                "/api/dossierpatient/patients/$patient->_id/constants?selected=" . $item->getAttribute('name'),
                $item->getLink('list')
            );
        }
    }
}
