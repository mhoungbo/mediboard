<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CEvenementPatient;
use Ox\Mediboard\Patients\Tests\Fixtures\DossierMedicalFixtures;
use Ox\Mediboard\Patients\Tests\Fixtures\PatientEventsFixtures;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Response;


/**
 * Test class for PatientEventsController.
 */
class PatientEventsControllerTest extends OxWebTestCase
{
    /**
     * Test: Show a patient event by 'evenement_patient_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testShowWithPermission(): void
    {
        $event = $this->getEvent();

        // Create client.
        $client = self::createClient();
        $client->request('GET', "/api/dossierpatient/patient_events/{$event->_id}", ['relations' => 'all']);

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($event::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($event->_id, $item->getId());

        // Asserting: Relation author.
        $this->assertTrue($item->hasRelationship(CEvenementPatient::RELATION_AUTHOR));
        $this->assertEquals(
            CMediusers::RESOURCE_TYPE,
            $item->getRelationship(CEvenementPatient::RELATION_AUTHOR)->getType()
        );
        $this->assertEquals($event->owner_id, $item->getRelationship(CEvenementPatient::RELATION_AUTHOR)->getId());
    }

    /**
     * Test: Show a patient event by 'evenement_patient_id' without permission.
     *
     * @throws TestsException
     * @throws Exception
     * @dataProvider dataProviderShowWithoutPermission
     */
    public function testShowWithoutPermission(CMbObject $object): void
    {
        $event        = $this->getEvent();
        $current_user = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "/api/dossierpatient/patient_events/{$event->_id}");

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] = $actual_perm;
            }
        }
    }

    /**
     * DataProvider for testShowWithoutPermission.
     *
     * @throws TestsException
     */
    public function dataProviderShowWithoutPermission(): array
    {
        return [
            'dataObject'  => [$this->getEvent()],
            'dataContext' => [$this->getEvent()->loadRefDossierMedical()],
        ];
    }

    /**
     * Get event.
     *
     * @throws TestsException
     */
    private function getEvent(): CEvenementPatient
    {
        /** @var CEvenementPatient */
        return $this->getObjectFromFixturesReference(
            CEvenementPatient::class,
            DossierMedicalFixtures::DOSSIER_MEDICAL_EVENT_TAG . 1
        );
    }

    /**
     * Test: Print document with wrong parameter
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testPrintWrongDocument(): void
    {
        $event = $this->getObjectFromFixturesReference(
            CEvenementPatient::class,
            PatientEventsFixtures::TAG_PATIENT_EVENT_DEFAULT);

        // Create client.
        $client = self::createClient();
        $client->request('GET', "/gui/dossierpatient/patientevents/{$event->_id}/print", ["document" => "wrongDocument"]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertEmpty($client->getResponse()->getContent());
    }

    /**
     * Test: Print DHE with unsynchronized event
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testPrintDHENotSynchronized(): void
    {
        $event = $this->getObjectFromFixturesReference(
            CEvenementPatient::class,
            PatientEventsFixtures::TAG_PATIENT_EVENT_DEFAULT);

        // Create client.
        $client = self::createClient();
        $client->request('GET', "/gui/dossierpatient/patientevents/{$event->_id}/print", ["document" => "DHE"]);

        $this->assertResponseStatusCodeSame(400);
    }

    /**
     * Test: Print Convocation without template assigned on the user
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testPrintConvocationWithoutTemplate(): void
    {
        $event = $this->getObjectFromFixturesReference(
            CEvenementPatient::class,
            PatientEventsFixtures::TAG_PATIENT_EVENT_DEFAULT);

        // Create client.
        $client = self::createClient();
        $client->request('GET', "/gui/dossierpatient/patientevents/{$event->_id}/print", ["document" => "convocation"]);

        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * Test: Default for route dossierpatient_gui_patient_events_print_for_practitioner
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testPrintListForPractitioner(): void
    {
        /** @var CEvenementPatient $event */
        $event = $this->getObjectFromFixturesReference(
            CEvenementPatient::class,
            PatientEventsFixtures::TAG_PATIENT_EVENT_INTERVENTION
        );

        $user = $this->getObjectFromFixturesReference(
            CMediusers::class,
            PatientEventsFixtures::TAG_PRACTITIONER
        );

        $client = self::createClient();
        $crawler = $client->request(
            'GET',
            "/gui/dossierpatient/mediusers/{$user->_id}/patientevents/print",
            ["period" => "day", "date" => CMbDT::date()]
        );

        $this->assertResponseIsSuccessful();

        if (!CModule::getActive('oxCabinet')) {
            return;
        }

        $trs = $crawler->filterXPath('//tr[@class="vw_planning_interv_line"]');
        $this->assertEquals(1, $trs->count());
        $this->assertStringContainsString($event->_guid, $trs->html());
    }
}
