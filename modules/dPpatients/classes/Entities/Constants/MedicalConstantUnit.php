<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Entities\Constants;

/**
 * @deprecated
 * TODO: This class will have to be deleted when the medical constants refactor will be done.
 *
 * Representation of the CConstantMedical class for the API.
 * /!\ This class should not be used in any other case.
 */
final class MedicalConstantUnit
{
    public string $name;
    public string $unit;
    public string $value;
    public string $order;
    public string $datetime;

    public function __construct(string $name, string $unit, string $value, string $datetime, string $order)
    {
        $this->name     = $name;
        $this->unit     = $unit;
        $this->value    = $value;
        $this->datetime = $datetime;
        $this->order    = $order;
    }
}
