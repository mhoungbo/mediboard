<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Entities;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\Exceptions\MedicalConstant\ValidatorMedicalConstantBookmark;

/**
 * Representation of a constant type bookmark.
 */
class CConstantTypeBookmark extends CMbObject
{
    // Resource.
    public const RESOURCE_TYPE = 'constant_type_bookmark';

    // Relations.
    public const RELATION_USER    = 'user';
    public const RELATION_PATIENT = 'patient';

    // DB Field.
    /** @var int|null $constant_type_bookmark_id Constant bookmark identifier (PK). */
    public ?int $constant_type_bookmark_id = null;

    /** @var int|null $user_id User identifier. */
    public ?int $user_id = null;

    /** @var int|null $patient_id Patient identifier. */
    public ?int $patient_id = null;

    /** @var string|null $constant_type Constant type name. */
    public ?string $constant_type = null;

    /**
     * @inheritDoc
     *
     * Ensure that the same constant type cannot be bookmarked twice for the same
     * patient and connected user.
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = "constant_type_bookmark";
        $spec->key   = "constant_type_bookmark_id";

        $spec->uniques['user_patient_type'] = ['patient_id', 'user_id', 'constant_type'];

        return $spec;
    }

    /**
     * @inheritDoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['user_id']       = 'ref class|CMediusers notNull back|bookmarked_constants';
        $props['patient_id']    = 'ref class|CPatient notNull back|bookmarked_constants_by';
        $props['constant_type'] = 'str notNull fieldset|' . self::FIELDSET_DEFAULT;

        return $props;
    }

    /**
     * @inheritDoc
     *
     * Check if constant type is available or not in mediboard.
     * Return a message if type is not available.
     */
    public function check(): ?string
    {
        $msg = parent::check();

        try {
            $this->checkTypeAvailable();
        } catch (ValidatorMedicalConstantBookmark $e) {
            return $e->getMessage();
        }

        return $msg;
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function store(): ?string
    {
        // If the object has no 'id'
        if (!$this->_id) {
            // If the object has no 'user_id'
            if (!$this->user_id) {
                $this->user_id = CMediusers::get()->_id;
            }
        }

        return parent::store();
    }

    /**
     * Return the user of the constant type bookmark has an Item.
     *
     * @throws ApiException
     * @throws Exception
     */
    public function getResourceUser(): Item
    {
        return new Item($this->loadFwdRef('user_id', true));
    }

    /**
     * Return the patient of the constant type bookmark has an Item.
     *
     * @throws ApiException
     * @throws Exception
     */
    public function getResourcePatient(): Item
    {
        return new Item($this->loadFwdRef('patient_id', true));
    }

    /**
     * Check if constant type is available or not in Mediboard/TAMM.
     * => Throw an error if check fail else do nothing.
     *
     * @return void
     * @throws ValidatorMedicalConstantBookmark
     */
    private function checkTypeAvailable(): void
    {
        if ($this->constant_type) {
            if (!in_array($this->constant_type, array_keys(CConstantesMedicales::$list_constantes))) {
                throw ValidatorMedicalConstantBookmark::typeIsNotAvailable($this->constant_type);
            }
        }
    }
}
