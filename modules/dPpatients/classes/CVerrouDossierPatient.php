<?php

/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Mediusers\CMediusers;

/**
 * Archive le contenu d'un dossier patient, le rendant non modifiable sur l'ensemble du contenu
 */
class CVerrouDossierPatient extends CMbObject
{
    public const RESOURCE_TYPE = 'patient_record_lock';

    final public const RELATION_CANCELLED_BY = 'cancelledBy';
    final public const RELATION_LOCKED_BY    = 'lockedBy';
    final public const RELATION_PATIENT      = 'patient';

    /** @var int|null DB Primary key */
    public ?int $verrou_dossier_patient_id = null;

    // DB fields
    public $patient_id;
    public $user_id;
    public $date;
    public $motif;
    public $coordonnees;
    public $medical;
    public $administratif;
    public $doc_hash;
    public $annule;
    public $annule_user_id;
    public $annule_motif;
    public $annule_date;

    /** @var CPatient */
    public $_ref_patient;
    /** @var CMediusers */
    public $_ref_user;
    /** @var CMediusers */
    public $_ref_user_unarchive;
    /** @var CFile */
    public $_ref_archive_file;

    public $_archive_view;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = 'verrou_dossier_patient';
        $spec->key   = 'verrou_dossier_patient_id';

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props                   = parent::getProps();

        $props["patient_id"]     = "ref class|CPatient notNull back|verrou_dossier_patient";
        $props["user_id"]        = "ref class|CMediusers notNull back|verrou_user";
        $props["date"]           = "dateTime fieldset|" . self::FIELDSET_DEFAULT;
        $props["motif"]          = "text fieldset|" . self::FIELDSET_DEFAULT;
        $props["coordonnees"]    = "text fieldset|" . self::FIELDSET_DEFAULT;
        $props["medical"]        = "bool default|0 fieldset|" . self::FIELDSET_DEFAULT;
        $props["administratif"]  = "bool default|0 fieldset|" . self::FIELDSET_DEFAULT;
        $props["doc_hash"]       = "str";
        $props["annule"]         = "bool default|0 fieldset|" . self::FIELDSET_DEFAULT;
        $props["annule_user_id"] = "ref class|CMediusers back|verrou_annule_user";
        $props["annule_motif"]   = "text fieldset|" . self::FIELDSET_DEFAULT;
        $props["annule_date"]    = "dateTime fieldset|" . self::FIELDSET_DEFAULT;

        return $props;
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $date = CMbDT::transform($this->date, null, CAppUI::conf("date"));
        $time = CMbDT::transform($this->date, null, CAppUI::conf("time"));
        $type = null;

        if ($this->medical && $this->administratif) {
            $type = CAppUI::tr("CVerrouDossierPatient-Medical and administrative");
        } elseif ($this->medical) {
            $type = CAppUI::tr("CVerrouDossierPatient-medical");
        } elseif ($this->administratif) {
            $type = CAppUI::tr("CVerrouDossierPatient-administratif");
        }

        $this->_archive_view = CAppUI::tr("CVerrouDossierPatient-Folder archive on %s at %s", $date, $time)
            . " ($type)";
    }

    /**
     * Load patient concerned by the lock.
     *
     * @return CPatient
     * @throws Exception
     */
    public function loadRefPatient(): CPatient
    {
        /** @var CPatient _ref_patient */
        return $this->_ref_patient = $this->loadFwdRef("patient_id", true);
    }

    /**
     * Load the user who set the lock.
     *
     * @return CMediusers
     * @throws Exception
     */
    public function loadRefUser(): CMediusers
    {
        /** @var CMediusers _ref_user */
        return $this->_ref_user = $this->loadFwdRef("user_id", true);
    }

    /**
     * Load the user who cancelled the lock.
     *
     * @return CMediusers
     * @throws Exception
     */
    public function loadRefUserUnarchive(): CMediusers
    {
        /** @var CMediusers _ref_user_unarchive */
        return $this->_ref_user_unarchive = $this->loadFwdRef("annule_user_id", true);
    }

    /**
     * Load file concerned by the lock.
     *
     * @return CFile
     * @throws Exception
     */
    public function loadRefArchiveFile(): CFile
    {
        $archive = new CFile();
        $archive->setObject($this);
        $archive->loadMatchingObject();

        return $this->_ref_archive_file = $archive;
    }

    /**
     * @inheritdoc
     */
    public function store(): ?string
    {
        $pdf = null;

        if ($msg = parent::store()) {
            return $msg;
        }

        // Archiver le dossier patient en pdf
        if ($this->_id && !$this->annule) {
            $pdf = CApp::fetch("oxCabinet", "ajax_print_global", ["patient_id" => $this->patient_id, "type" => "tous"]);
        }

        if ($pdf) {
            $file = new CFile();
            $file->setObject($this);
            $file->file_name = CAppUI::tr("Archive_dossier_patient-")
                . CMbDT::format(CMbDT::dateTime(), "%d_%m_%Y_%H_%M_%S")
                . ".pdf";
            $file->file_type = "application/pdf";
            $file->author_id = CMediusers::get()->_id;
            $file->fillFields();
            $file->setContent($pdf);
            $file->store();

            if ($file) {
                //doc hash
                $this->doc_hash = md5($pdf);
            }

            if ($msg = parent::store()) {
                return $msg;
            }
        }

        return null;
    }

    /**
     * Returns the user who cancelled the lock.
     *
     * @return Item
     * @throws ApiException
     * @throws Exception
     */
    public function getResourceCancelledBy(): Item
    {
        $user = $this->loadRefUserUnarchive();

        return new Item($user);
    }

    /**
     * Returns the user who set the lock.
     *
     * @return Item
     * @throws ApiException
     * @throws Exception
     */
    public function getResourceLockedBy(): Item
    {
        $user = $this->loadRefUser();

        return new Item($user);
    }

    /**
     * Returns the patient concerned by the lock.
     *
     * @return Item
     * @throws ApiException
     * @throws Exception
     */
    public function getResourcePatient(): Item
    {
        $patient = $this->loadRefPatient();

        return new Item($patient);
    }
}
