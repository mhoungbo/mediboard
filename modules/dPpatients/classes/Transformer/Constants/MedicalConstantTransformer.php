<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Transformer\Constants;

use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\Entities\Constants\MedicalConstantUnit;

/**
 * @deprecated
 * TODO: This class will have to be deleted when the medical constants refactor will be done.
 *
 * Transformer of CConstantesMedicales to MedicalConstantUnit.
 */
final class MedicalConstantTransformer
{
    /**
     * Create a front-readable version of medical constants.
     */
    public function transform(
        CConstantesMedicales $data,
        array                $selected_constants,
        array                $rank_constants
    ): array {
        // Initialization of the array which will contain the constants.
        $constants = [];

        // Create medical constants for the selected constants.
        foreach ($selected_constants as $name => $spec) {
            // Check: Property exist.
            if (!property_exists($data, $name)) {
                continue;
            }

            // Check: Value is not null.
            if ($data->$name === null) {
                continue;
            }

            // Check: Unit exist or not.
            if (property_exists($data, "_unite_$name")) {
                $unit  = $data->{"_unite_$name"};

                // Check: formfields to get formfields value if exist.
                if (isset($spec['formfields'])) {
                    $value = [];

                    foreach ($spec['formfields'] as $form_field) {
                        $value[] = $data->$form_field;
                    }

                    $value = implode('/', $value);
                } else {
                    $value = $data->$name;
                }
            } else {
                $unit  = $spec['unit'];
                $value = $data->$name;
            }

            $constants[] = new MedicalConstantUnit($name, $unit, $value, $data->datetime, $rank_constants[$name]);
        }

        // Sort array.
        usort($constants, function ($a, $b): int {
            if (!$a->order) {
                return 1;
            }
            if (!$b->order) {
                return -1;
            }

            return $a->order - $b->order;
        });

        return $constants;
    }
}
