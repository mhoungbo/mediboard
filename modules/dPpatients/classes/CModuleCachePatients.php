<?php

/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients;

use Ox\Core\Module\AbstractModuleCache;

class CModuleCachePatients extends AbstractModuleCache
{
    protected array $dshm_patterns = [
        'CPatient',
    ];

    public function getModuleName(): string
    {
        return 'dPpatients';
    }
}
