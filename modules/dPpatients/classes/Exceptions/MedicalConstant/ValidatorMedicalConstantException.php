<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Exceptions\MedicalConstant;

use Ox\Core\CMbException;

class ValidatorMedicalConstantException extends CMbException
{
    public static function constantIsLock(string $lock_time): self
    {
        return new self('CConstantes-Medicales-msg-modif-timeout-%s', $lock_time);
    }

    public static function notAuthorOfConstant(): self
    {
        return new self('CConstantes-Medicales-msg-edit_not_creator');
    }
}
