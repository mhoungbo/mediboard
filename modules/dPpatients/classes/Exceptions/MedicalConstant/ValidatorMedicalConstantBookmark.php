<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Exceptions\MedicalConstant;

use Ox\Core\CMbException;

class ValidatorMedicalConstantBookmark extends CMbException
{
    public static function typeIsNotAvailable(string $type): self
    {
        return new self('CConstantTypeBookmark-Error-This-constant-type-is-not-available: "%s"', $type);
    }
}
