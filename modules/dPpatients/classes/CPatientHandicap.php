<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;

/**
 * Represents a patient disability
 */
class CPatientHandicap extends CStoredObject
{
    public const RESOURCE_TYPE = 'patient_disability';

    final public const RELATION_PATIENT = 'patient';

    /** @var int|null DB Primary key */
    public ?int $patient_handicap_id = null;

    /** @var string|null Type of disability */
    public ?string $handicap = null;

    /** @var int|null Patient linked to disability */
    public ?int $patient_id = null;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table = "patient_handicap";
        $spec->key   = "patient_handicap_id";

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props["handicap"]   = 'enum list|moteur|psychique|autonome|fauteuil|besoin_aidant|mal_entendant|mal_voyant fieldset|' . self::FIELDSET_DEFAULT;
        $props["patient_id"] = "ref class|CPatient back|patient_handicaps";

        return $props;
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();
        self::loadView();
    }

    public function loadView(): void
    {
        parent::loadView();

        $this->_view = CAppUI::tr(CClassMap::getSN($this) . '.handicap.' . $this->handicap);
    }

    /**
     * Check if a handicap is in a a list of handicaps
     *
     * @param array  $handicaps - list of handicaps
     * @param string $handicap  - the handicap
     *
     * @return bool
     */
    public static function hasHandicap(array $handicaps, string $handicap): bool
    {
        $filtered = array_filter(
            $handicaps,
            fn(CPatientHandicap $patient_handicap) => $patient_handicap->handicap === $handicap
        );

        return count($filtered) > 0;
    }

    /**
     * Returns the patient's linked to disability.
     *
     * @return Item
     * @throws ApiException
     * @throws Exception
     */
    public function getResourcePatient(): Item
    {
        $patient = $this->loadFwdRef("patient_id");

        return new Item($patient);
    }
}
