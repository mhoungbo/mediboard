<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Services;

use DateTimeImmutable;
use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CPDODataSource;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Patients\CEvenementPatient;

/**
 * Description
 */
class EvenementPatientDHEplanningService
{
    public const MONTH_PERIOD = 'month';
    public const WEEK_PERIOD  = 'week';
    public const DAY_PERIOD   = 'day';

    private DateTimeImmutable             $date_min;
    private DateTimeImmutable             $date_max;
    private CSQLDataSource|CPDODataSource $ds;
    private array                         $prat_ids;
    private string                        $period;

    /**
     * Prepare the date filter depending on the period given
     *
     * @throws Exception
     */
    public function __construct(array $prat_ids, DateTimeImmutable $date, string $period = self::DAY_PERIOD)
    {
        $evenement = new CEvenementPatient();
        $this->ds  = $evenement->getDS();

        $this->period = $period;

        if ($period === self::DAY_PERIOD) {
            $date_max = $date_min = $date->format('Y-m-d');
        } elseif ($period === self::WEEK_PERIOD) {
            $date_min = CMbDT::date("last sunday", $date->format('Y-m-d'));
            $date_max = CMbDT::date("next sunday", $date->format('Y-m-d'));
            $date_min = CMbDT::date("+1 day", $date_min);
        } else {
            $date_min = CMbDT::date("first day of +0 month", $date->format('Y-m-d'));
            $date_max = CMbDT::date("last day of +0 month", $date->format('Y-m-d'));
        }

        $this->date_min = DateTimeImmutable::createFromFormat("Y-m-d", $date_min);
        $this->date_max = DateTimeImmutable::createFromFormat("Y-m-d", $date_max);

        $this->prat_ids = $prat_ids;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function listEvents(): array
    {
        $where_event = [
            "praticien_id"       => CSQLDataSource::prepareIn($this->prat_ids),
            "type"               => $this->ds->prepare("= 'intervention'"),
            "date"               => $this->ds->prepareBetween(
                $this->date_min->format('Y-m-d 00:00:00'),
                $this->date_max->format('Y-m-d 23:59:59')
            ),
            "date_fin_operation" => $this->ds->prepare("IS NOT NULL"),
            "cancel"             => $this->ds->prepare("= ?", '0'),
        ];

        $events = (new CEvenementPatient())->loadList($where_event);

        $medical_records = CStoredObject::massLoadFwdRef($events, "dossier_medical_id");
        CStoredObject::massLoadFwdRef($medical_records, "object_id");
        CStoredObject::massLoadFwdRef($events, "praticien_id");
        CStoredObject::filterByPerm($events);

        foreach ($events as $event) {
            $event->loadRefPraticien();
            $event->loadRefPatient()->loadRefPhotoIdentite();
        }

        return $events;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function countEvents(): int
    {
        $where_event = [
            "praticien_id"       => CSQLDataSource::prepareIn($this->prat_ids),
            "type"               => $this->ds->prepare("= 'intervention'"),
            "date"               => $this->ds->prepareBetween(
                $this->date_min->format('Y-m-d 00:00:00'),
                $this->date_max->format('Y-m-d 23:59:59')
            ),
            "date_fin_operation" => $this->ds->prepare("IS NOT NULL"),
            "cancel"             => $this->ds->prepare("= ?", '0'),
        ];

        return (new CEvenementPatient())->countList($where_event);
    }

    /**
     * Array with dates as key and array of CEvenement patient for each date
     *
     * @param CEvenementPatient[] $events
     *
     * @return array
     */
    public function constructPlanning(array $events): array
    {
        $interv_planning = [];

        if ($this->date_min <= $this->date_max && count($events)) {
            $date_iterator = $this->date_min;

            //Case a single day filter
            if ($this->period === self::DAY_PERIOD) {
                $interv_planning[$this->date_min->format('Y-m-d')] = $events;
            } else {
                //Create a key for each date and set all the events matching that date
                while ($date_iterator <= $this->date_max) {
                    $interv_planning[$date_iterator->format('Y-m-d')] = array_filter(
                        $events,
                        fn($event) => CMbDT::date($event->date) === $date_iterator->format('Y-m-d')
                    );
                    $date_iterator                                    = $date_iterator->modify('+1 day');
                }
            }
        }

        return $interv_planning;
    }

    /**
     * date min getter
     * @return string
     */
    public function getDateMinStr(): string
    {
        return $this->date_min->format('Y-m-d');
    }

    /**
     * date max gette
     * @return string
     */
    public function getDateMaxStr(): string
    {
        return $this->date_max->format('Y-m-d');
    }
}
