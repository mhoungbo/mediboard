<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Controller;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\Entities\CConstantTypeBookmark;
use Ox\Mediboard\Patients\Entities\Constants\MedicalConstantUnit;
use Ox\Mediboard\Patients\Transformer\Constants\MedicalConstantTransformer;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that answer the api calls on MedicalConstant (CConstantesMedicales).
 */
class MedicalConstantsController extends Controller
{
    private const SELECTED_PARAMETER   = 'selected';
    private const BOOKMARKED_PARAMETER = 'bookmarked';
    private const LAST_PARAMETER       = 'last';

    /**
     * @api
     *
     * Load a list of medical constants using the request parameters.
     *
     * @throws ApiException
     * @throws Exception
     */
    public function list(CPatient $patient, RequestApi $request_api): Response
    {
        // Selected & Bookmark constants.
        $selected_constants = $this->selectedConstantsByRequestApi($patient, $request_api);

        /**
         * PARAM: Last
         * => Redirect to another function.
         */
        if ($request_api->getRequest()->get(self::LAST_PARAMETER)) {
            return $this->last($patient, $request_api, $selected_constants);
        }

        $medical_constants  = $patient->loadRefsConstantesMedicales();

        $constants = [];
        foreach ($medical_constants as $medical_constant) {
            $constant = (new MedicalConstantTransformer())->transform(
                $medical_constant,
                $selected_constants,
                CConstantesMedicales::getRanksFor()
            );

            if (count($constant)) {
                foreach ($constant as $_constant) {
                    $constants[] = $_constant;
                }
            }
        }

        // Collection.
        $collection = Collection::createFromRequest($request_api, $constants);
        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            count($constants)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * Get last vitals of patient by 'patient_id' parameter.
     *
     * @throws Exception
     */
    private function last(CPatient $patient, RequestApi $request_api, array $selected_constants): Response
    {
        $last_medical_constants = CConstantesMedicales::getLatestFor($patient);

        // MedicalConstantUnit
        $constants = (new MedicalConstantTransformer())->transform(
            $last_medical_constants[0],
            $selected_constants,
            CConstantesMedicales::getRanksFor()
        );

        // Collection.
        $collection = Collection::createFromRequest($request_api, $constants);

        /**
         * Adding additional information.
         * @var Item $item
         */
        foreach ($collection as $item) {
            /** @var MedicalConstantUnit $constant */
            $constant = $item->getDatas();

            $item->addAdditionalDatas(['datetime' => $last_medical_constants[1][$constant->name]]);
            $item->addLinks(
                [
                    'list' => $this->generateUrl(
                        'dossierpatient_constants_list',
                        [
                            'patient_id' => $patient->_id,
                            'selected'   => $constant->name,
                        ]
                    ),
                ]
            );
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * Retrieves the constants selected by the Request Api.
     *
     * @throws Exception
     */
    private function selectedConstantsByRequestApi(CPatient $patient, RequestApi $request_api): array
    {
        $selected = $request_api->getRequest()->get(self::SELECTED_PARAMETER);

        // If there are selected constants, retrieve only the specifics of the constants that have been selected.
        $selected_constants = ($selected)
            ? array_intersect_key(CConstantesMedicales::$list_constantes, array_flip(explode(',', $selected)))
            : CConstantesMedicales::$list_constantes;

        // If you want to retrieve the favorite constants,
        // retrieve only the specifics of the constants that have been selected.
        if ($request_api->getRequest()->get(self::BOOKMARKED_PARAMETER)) {
            $bookmark_constant  = new CConstantTypeBookmark();
            $bookmark_constants = $bookmark_constant->loadColumn(
                'constant_type',
                [
                    'patient_id' => $bookmark_constant->getDS()->prepare('= ?', $patient->_id),
                    'user_id'    => $bookmark_constant->getDS()->prepare('= ?', $this->getCMediuser()->_id),
                ]
            );

            $selected_constants = array_intersect_key($selected_constants, array_flip($bookmark_constants));
        }

        return $selected_constants;
    }
}
