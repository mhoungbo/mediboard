<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Import\Rpps\Entity\CPersonneExercice;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\Repositories\CorrespondantRepository;
use Symfony\Component\HttpFoundation\Response;

class CorrespondantsController extends Controller
{
    /**
     * @param CMedecin $medecin
     * @param RequestParams $request_params
     *
     * @return Response
     * @throws Exception
     */
    public function addRppsToCorrespondant(CMedecin $medecin, RequestParams $request_params): Response
    {
        // If medecin already have a RPPS, we can skip the process
        if ($medecin->rpps && $medecin->import_file_version) {
            return $this->renderEmptyResponse();
        }

        if (!$this->isCsrfTokenValid('dossierpatient_gui_medecin_add_rpps', $request_params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $rpps = $request_params->post('rpps', 'str notNull');

        // Check if a medecin with the provided rpps already exists, is so, then return the medecin_id
        $where                        = [];
        $ds                           = $medecin->getDS();
        $where['rpps']                = $ds->prepare('= ?', $rpps);
        $where['import_file_version'] = $ds->prepare('IS NOT NULL');

        $medecin_existing = new CMedecin();
        $medecin_existing->loadObject($where);

        if ($medecin_existing->_id) {
            return $this->renderJsonResponse(json_encode(['medecin_id' => $medecin_existing->_id]));
        }

        // Update the provided medecin with the rpps in request parameters
        $medecin->rpps = $rpps;
        if ($msg = $medecin->store()) {
            return $this->renderJsonResponse(json_encode(['msg' => $msg]), 500);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @param CMedecin $medecin
     * @param CorrespondantRepository $correspondantRepository
     * @return Response
     * @throws Exception
     */
    public function checkMatchingCorrespondant(CMedecin $medecin, CorrespondantRepository $correspondantRepository): Response
    {
        $datas = [];

        if ($medecin->rpps && $medecin->import_file_version) {
            $datas['is_unique'] = false;

            return $this->renderJsonResponse(json_encode([$datas]));
        }

        $personnes = $correspondantRepository->findMatchingPersonneExercice($medecin);

        $is_unique = count($personnes) === 1;

        $datas['is_unique'] = $is_unique;

        if ($is_unique) {
            /** @var CPersonneExercice $personne */
            $personne = reset($personnes);

            $datas['correspondant'] = [
                'correspondant_rpps' => $personne->identifiant,
                'correspondant_nom' => $personne->nom,
                'correspondant_prenom' => $personne->prenom,
                'correspondant_profession' => $personne->libelle_profession,
                'correspondant_cp' => $personne->cp,
                'correspondant_ville' => $personne->libelle_commune,
            ];
        }

        return $this->renderJsonResponse(json_encode([$datas]));
    }
}
