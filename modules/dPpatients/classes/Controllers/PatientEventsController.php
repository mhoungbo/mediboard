<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers;

use DateTimeImmutable;
use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\HttpException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\CompteRendu\CHtmlToPDF;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CEvenementPatient;
use Ox\Mediboard\Patients\Services\EvenementPatientDHEplanningService;
use Ox\Mediboard\Patients\Services\EvenementPatientDHEService;
use Symfony\Component\HttpFoundation\Response;

/**
 * CRUD API CEvenementPatient.
 * Controller that answer the api calls on CEvenementPatient.
 */
class PatientEventsController extends Controller
{
    /**
     * Show a patient event identified by 'evenement_patient_id' parameter.
     *
     * Security:
     * => READ perm is required.
     *
     * @throws ApiException
     * @api
     */
    public function show(CEvenementPatient $event, RequestApi $request_api): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request_api, $event));
    }

    /**
     * @param RequestParams     $params
     * @param CEvenementPatient $patient_event
     *
     * @return Response
     * @throws Exception
     */
    public function print(RequestParams $params, CEvenementPatient $patient_event): Response
    {
        $document = $params->get("document", "str");

        if ($document === "convocation") {
            $patient_event->loadRefsId400SIH();
            $patient_event->loadRefParent();

            // Si on est dans le cas d'une DHE on veut imprimer depuis le s�jour et pas l'interv
            if (isset($patient_event->_ref_sih_id400->_id) && isset($patient_event->_ref_parent->_id)) {
                $patient_event = $patient_event->_ref_parent;
            }

            $contexts = [
                $patient_event->loadRefPraticien(),
                $patient_event->_ref_praticien->loadRefFunction(),
                $patient_event->_ref_praticien->_ref_function->loadRefGroup(),
            ];

            foreach ($contexts as $context) {
                $model = CCompteRendu::getSpecialModel($context, "CEvenementPatient", "[CONVOCATION]");
                if (isset($model->_id)) {
                    break;
                }
            }

            if (isset($model->_id)) {
                $pdf = CCompteRendu::getDocForObject($model, $patient_event, null, true, true);
            } else {
                throw new HttpException(
                    Response::HTTP_NOT_FOUND,
                    $this->translator->tr("CEvenementPatient-msg-No convocation model found")
                );
            }

            return $this->renderFileResponse($pdf, "export_convocation-" . CMbDT::date() . ".pdf", "application/pdf");
        } elseif ($document === "DHE") {
            $evenement_patient_dhe = new EvenementPatientDHEService($patient_event);
            $content               = $evenement_patient_dhe->getDHEPDFContent();

            $file               = new CFile();
            $file->object_class = $patient_event->_class;
            $file->object_id    = $patient_event->_id;
            $file->file_name    = 'DHE-' . $patient_event->type . '-' .
                CMbDT::date() . '.pdf';
            $file->file_type    = 'application/pdf';
            $file->fillFields();
            $file->updateFormFields();

            $htmltopdf = new CHtmlToPDF();

            $pdf = $htmltopdf->generatePDF($content, true, new CCompteRendu(), $file);

            if ($msg = $file->store()) {
                CAppUI::setMsg($msg, UI_MSG_WARNING);
            }

            return $this->renderFileResponse($pdf, $file->file_name, $file->file_type);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * Print a list of intervention events of a given practitioner
     * @param RequestParams $params
     * @param CMediusers    $user
     *
     * @return Response
     * @throws Exception
     */
    public function printListForPractitioner(RequestParams $params, CMediusers $user): Response
    {
        $date   = $params->get("date", "str");
        $period = $params->get("period", "enum list|day|week|month default|day");

        if (!CModule::getActive('oxCabinet')) {
            return $this->renderEmptyResponse();
        }

        if (!$date) {
            $date = CMbDT::date();
        }

        $planning_service = new EvenementPatientDHEplanningService(
            [$user->_id],
            DateTimeImmutable::createFromFormat("Y-m-d", $date),
            $period
        );
        $events           = $planning_service->listEvents();
        $interv_planning  = $planning_service->constructPlanning($events);

        return $this->renderSmarty(
            "vw_planning_interv",
            [
                "interv_planning" => array_filter($interv_planning),
                "print"           => true,
                'user'            => $user,
                'start'           => $planning_service->getDateMinStr(),
                'end'             => $planning_service->getDateMaxStr(),
                'period'          => $period,
            ],
            "oxCabinet"
        );
    }
}
