<?php

/**
 * @package Mediboard\Mediusers
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers\Legacy;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbString;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Import\Rpps\CMedecinExercicePlaceManager;
use Ox\Import\Rpps\Entity\CAbstractExternalRppsObject;
use Ox\Import\Rpps\Entity\CMssanteInfos;
use Ox\Import\Rpps\Entity\CPersonneExercice;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CExercicePlace;
use Ox\Mediboard\Patients\CMedecin;

class CCorrespondantController extends CLegacyController
{
    /**
     * @throws Exception
     */
    public function seekPersonnesExercices(): void
    {
        $this->checkPermRead();

        $nom   = CView::get('nom', 'str');
        $cps   = CView::get('medecin_cp', 'str');
        $start = CView::get('start_med', 'num default|0');
        $step  = CView::get('step_med', 'num default|10');

        $show_no_internal_results = CView::get('show_no_internal_results', 'bool');

        CView::checkin();
        CView::enforceSlave();

        $personne_exercice = new CPersonneExercice();

        $where = [
            'type_identifiant' => $personne_exercice->getDS()->prepare(
                '= ?',
                CAbstractExternalRppsObject::TYPE_IDENTIFIANT_RPPS
            )
        ];

        $cps = self::getCps($cps);

        if (count($cps)) {
            $where_cp = [];
            foreach ($cps as $cp) {
                $where_cp[] = "cp LIKE '" . $cp . "%'";
            }

            $where[] = implode(" OR ", $where_cp);
        }

        $personnes = $personne_exercice->seek($nom, $where, "{$start},{$step}", true);

        $correspondants = self::computeCorrespondants($personnes);

        $this->renderSmarty(
            'inc_list_rpps_correspondant',
            [
                "correspondants"           => $correspondants,
                "nb_correspondants"        => $personne_exercice->_totalSeek,
                "start"                    => $start,
                "step"                     => $step,
                'show_no_internal_results' => $show_no_internal_results,
            ]
        );
    }

    /**
     * Retrieve the list of correspondant matching the fields
     *
     * @throws Exception
     */
    public function searchPersonnesExercices(): void
    {
        $this->checkPermRead();

        $correspondant_nom         = CView::get("medecin_nom", "str");
        $correspondant_prenom      = CView::get("medecin_prenom", "str");
        $correspondant_type        = CView::get("type", "str");
        $correspondant_cp          = CView::get("medecin_cp", "str");
        $correspondant_ville       = CView::get("medecin_ville", "str");
        $correspondant_disciplines = CView::get("disciplines", "str");
        $rpps                      = CView::get("rpps", "numchar");
        $show_no_internal_results  = CView::get('show_no_internal_results', 'bool');

        // pagination
        $start = CView::get("start_med", 'num default|0');
        $step  = CView::get("step_med", 'num default|20');

        CView::checkin();
        CView::enforceSlave();

        $correspondant = new CMedecin();
        $ds            = $correspondant->getDS();

        $where = [];

        if ($rpps && (strlen($rpps) === 11)) {
            $where["identifiant"] = $ds->prepare('= ?', $rpps);
        } else {
            if ($correspondant_nom) {
                /* We use stripslashes because if the string contains quotes, they can already be escaped,
                 * and if they are, the call to prepare() will fucked it up by adding more slashes */
                $where["nom"] = CMbString::removeDiacritics($ds->prepareLike(stripslashes("$correspondant_nom%")));
            }

            if ($correspondant_prenom) {
                $where["prenom"] = CMbString::removeDiacritics(
                    $ds->prepareLike(stripslashes("$correspondant_prenom%"))
                );
            }

            if ($correspondant_type) {
                $where["code_profession"] = $ds->prepare('= ?', array_search($correspondant_type, CMedecin::$types));
            }

            if ($correspondant_cp) {
                $cps = self::getCps($correspondant_cp);

                $where_cp = [];
                foreach ($cps as $cp) {
                    $where_cp[] = "cp LIKE '" . $cp . "%'";
                }

                $where[] = implode(" OR ", $where_cp);
            }

            if ($correspondant_ville) {
                $where["libelle_commune"] = CMbString::removeDiacritics(
                    stripslashes($ds->prepareLike("$correspondant_ville%"))
                );
            }

            if ($correspondant_disciplines) {
                $where[] = $ds->prepareLikeMulti($correspondant_disciplines, 'libelle_savoir_faire');
            }
        }

        if (empty($where)) {
            CAppUI::stepMessage(UI_MSG_WARNING, "CCorrespondant-warning fill at least one field");

            return;
        }

        $where["type_identifiant"] = $ds->prepare('= ?', CAbstractExternalRppsObject::TYPE_IDENTIFIANT_RPPS);

        $personne                = new CPersonneExercice();
        $nb_personne             = (!is_null($personne->getDS()) ? $personne->countList($where) : 0);
        $order                   = "nom, prenom";
        $count_personne_exercice = min($nb_personne, 1000);
        $personnes               = $personne->loadList($where, $order, "$start, $step");

        $correspondants = self::computeCorrespondants($personnes);

        $this->renderSmarty(
            'inc_list_rpps_correspondant',
            [
                "correspondants"           => $correspondants,
                "nb_correspondants"        => $count_personne_exercice,
                "start"                    => $start,
                "step"                     => $step,
                'show_no_internal_results' => $show_no_internal_results,
            ]
        );
    }

    /**
     * Create a CMedecin from a CPersonneExercice, create the associated CExercicePlace if needed and sync the
     * CMedecinExercicePlace
     *
     * @throws CMbException
     * @throws Exception
     */
    public function addCorrespondantFromRPPS(): void
    {
        $this->checkPermRead();

        $rpps = CView::get('rpps', 'str');

        CView::checkin();

        $this->synchronizeMedecinAndExercicePlace($rpps);

        CAppUI::stepAjax(
            '1 ' . CAppUI::tr(
                'mod-dPpatients-tab-openCorrespondantImportFromRPPSModal-msg-add correspondant success.one'
            )
        );
    }

    /**
     * Update a CMedecin from a CPersonneExercice, update the associated CExercicePlace if needed and sync the
     * CMedecinExercicePlace
     *
     * @throws CMbException
     * @throws Exception
     */
    public function updateCorrespondant(): void
    {
        $this->checkPermRead();

        $rpps = CView::get("rpps", "str");

        CView::checkin();

        $this->synchronizeMedecinAndExercicePlace($rpps);

        CAppUI::stepAjax(
            CAppUI::tr("mod-dPpatients-tab-openCorrespondantImportFromRPPSModal-msg-Update correspondant success")
        );
        CAppUI::stepAjax(
            CAppUI::tr("mod-dPpatients-tab-openCorrespondantImportFromRPPSModal-msg-Synchronize exercicePlace success")
        );
    }

    /**
     * Return the exercice place of the correspondant
     *
     * @param CExercicePlace $correspondant Correspondant
     *
     * @return CExercicePlace|string
     * @throws Exception
     */
    private function loadExercicePlace(CPersonneExercice $correspondant)
    {
        $existingExercicePlace = new CExercicePlace();
        $exercicePlace         = new CExercicePlace();
        $ds                    = $exercicePlace->getDS();
        $where                 = [];

        if ($correspondant->hashIdentifier()) {
            $where["exercice_place_identifier"] = $ds->prepare('= ?', $correspondant->hashIdentifier());
            if ($where) {
                $existingExercicePlace->loadObject($where);
            }
        }

        $where = [];

        if ($correspondant->siret_site) {
            $where["siret"] = $ds->prepare('= ?', $correspondant->siret_site);
        }

        if ($correspondant->siren_site) {
            $where["siren"] = $ds->prepare('= ?', $correspondant->siren_site);
        }

        if ($correspondant->finess_site) {
            $where["finess"] = $ds->prepare('= ?', $correspondant->finess_site);
        }

        if ($correspondant->finess_etab_juridique) {
            $where["finess_juridique"] = $ds->prepare('= ?', $correspondant->finess_etab_juridique);
        }

        if ($correspondant->id_technique_structure) {
            $where["id_technique"] = $ds->prepare('= ?', $correspondant->id_technique_structure);
        }

        if ($correspondant->raison_sociale_site) {
            $where["raison_sociale"] = $ds->prepare('= ?', $correspondant->raison_sociale_site);
        }

        if ($where) {
            $exercicePlace->loadObject($where);

            if ($existingExercicePlace->_id && ($existingExercicePlace->_id === $exercicePlace->_id)) {
                return $existingExercicePlace;
            } else {
                return $correspondant->updateOrCreatePlace($exercicePlace);
            }
        }

        return null;
    }

    /**
     * Synchronize the medecin and the associated exercice places
     *
     * @param int $rpps
     *
     * @return CExercicePlace|string
     * @throws Exception
     */
    public function synchronizeMedecinAndExercicePlace(int $rpps): void
    {
        $correspondant = new CPersonneExercice();
        $medecin       = new CMedecin();

        $where = [];

        // Gestion du cloisonnement utilisateur
        $current_user = CMediusers::get();
        $function_id  = null;
        $group_id     = null;

        if (CAppUI::isCabinet()) {
            $function_id = $current_user->function_id;
        } elseif (CAppUI::isGroup()) {
            $group_id = CGroups::loadCurrent()->_id;
        }

        $medecin->loadFromRPPS($rpps, $function_id, $group_id);

        $ds = $correspondant->getDS();

        $where['identifiant'] = $ds->prepare('= ?', $rpps);
        $correspondants       = $correspondant->loadList($where);

        $mssante_adress              = new CMssanteInfos();
        $mssante_adress->identifiant = $rpps;
        $mssante_adresses            = $mssante_adress->loadMatchingList();

        $manager = new CMedecinExercicePlaceManager();

        foreach ($correspondants as $_correspondant) {
            $medecin              = $_correspondant->synchronize($medecin);
            $medecin->function_id = $function_id ?? null;
            $medecin->group_id    = $group_id ?? null;

            // Gestion du cas des s�parations par cabinets o� on ajoute un m�decin en tant qu'administrateur
            if (!$medecin->_id && $medecin->group_id) {
                $medecin->enableImporting();
            }

            $res = $medecin->store();
            if (!is_null($res)) {
                CAppUI::stepAjax("$res", UI_MSG_ERROR);
            }

            $exercicePlace = $this->loadExercicePlace($_correspondant);

            if (is_string($exercicePlace)) {
                throw new Exception($exercicePlace);
            }

            $medecin_exercice_place = $_correspondant->synchronizeExercicePlace($medecin, $exercicePlace);

            if (!empty($mssante_adresses)) {
                $_correspondant->addMSSanteAddress(
                    $medecin_exercice_place,
                    $medecin,
                    $mssante_adresses
                );
            }

            $manager->removeBadMatchingMedecinExercicePlace($medecin);
        }

        $manager->purgeObsoleteMedecinExercicePlace($medecin);
    }

    /**
     * @throws Exception
     */
    public function checkIfMedecinIsUpdatable(int $existing_medecin_id, CMedecin $medecin): bool
    {
        $existing_medecin = new CMedecin();
        $existing_medecin->load($existing_medecin_id);

        return !$existing_medecin->equals($medecin);
    }

    /**
     * @throws Exception
     */
    public function checkIfExercicePlaceIsUpdatable(int $existingExercicePlaceId, CExercicePlace $exercicePlace): bool
    {
        $existing_exercice_place = new CExercicePlace();
        $existing_exercice_place->load($existingExercicePlaceId);

        return !$existing_exercice_place->equals($exercicePlace);
    }

    /**
     * @throws Exception
     */
    public function checkIfMSSanteInfoIsUpdatable(int $existingMSSanteInfoId, CMssanteInfos $mssante_infos): bool
    {
        $existing_mssante_infos = new CMssanteInfos();
        $existing_mssante_infos->load($existingMSSanteInfoId);

        return !$existing_mssante_infos->equals($mssante_infos);
    }

    private static function getCps(?string $cps): array
    {
        $cps_result = preg_split("/\s*[\s\|,]\s*/", $cps);
        CMbArray::removeValue("", $cps_result);

        return $cps_result;
    }

    /**
     * @param CPersonneExercice[] $personnes
     *
     * @return array
     * @throws Exception
     */
    private function computeCorrespondants(array $personnes): array
    {
        // Gestion du cloisonnement utilisateur
        $current_user = CMediusers::get();
        $function_id  = null;
        $group_id     = null;

        if (CAppUI::isCabinet()) {
            $function_id = $current_user->function_id;
        } elseif (CAppUI::isGroup()) {
            $group_id = CGroups::loadCurrent()->_id;
        }

        $correspondants = [];

        foreach ($personnes as $_personne_exercice) {
            /** @var CMedecin $medecin */
            $medecin = $_personne_exercice->synchronize();

            $medecin->type = $_personne_exercice->code_profession ?
                CMedecin::$types[$_personne_exercice->code_profession] : null;

            $medecin->_id = null;

            $loaded_medecin = $medecin->loadFromRPPS($_personne_exercice->identifiant, $function_id, $group_id);
            $is_up_to_date  = $loaded_medecin->import_file_version === $_personne_exercice->version;

            if ($loaded_medecin->_id && $loaded_medecin->actif && $is_up_to_date) {
                $medecin->_already_imported = true;
            }

            $exercicePlace                 = new CExercicePlace();
            $exercicePlace->adresse        = $_personne_exercice->buildAdresse();
            $exercicePlace->cp             = $_personne_exercice->cp;
            $exercicePlace->commune        = $_personne_exercice->libelle_commune;
            $exercicePlace->raison_sociale = $_personne_exercice->raison_sociale_site;

            $correspondants[$medecin->rpps]['medecin'][]        = $medecin;
            $correspondants[$medecin->rpps]['disciplines'][]    = $_personne_exercice->code_savoir_faire ?
                ($_personne_exercice->code_savoir_faire . ' : '
                    . $_personne_exercice->libelle_savoir_faire) : null;
            $correspondants[$medecin->rpps]['exercicePlaces'][] = $exercicePlace;
        }

        return $correspondants;
    }
}
