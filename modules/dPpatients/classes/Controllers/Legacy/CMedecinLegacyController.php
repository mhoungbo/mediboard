<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers\Legacy;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CMbString;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\CValue;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Cabinet\CConsultAnesth;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Mediusers\CSpecCPAM;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CMedecinExercicePlace;
use Ox\Mediboard\Patients\MedecinFieldService;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;

class CMedecinLegacyController extends CLegacyController
{
    /**
     * @throws Exception
     */
    public function listMedecins(): void
    {
        $this->checkPermRead();

        $dialog      = CValue::get("dialog");
        $view_update = CValue::get("view_update");

        // pagination
        $start_med = CValue::get("start_med", 0);
        $step_med  = CValue::get("step_med", 20);

        $medecin = new CMedecin();
        $ds      = $medecin->getDS();

        // R�cuperation des m�decins recherch�s
        if ($dialog) {
            $medecin_nom            = CView::get("medecin_nom", "str");
            $medecin_prenom         = CView::get("medecin_prenom", "str");
            $medecin_context_class  = CView::get("context_class", "enum list|CFunctions|CGroups");
            $medecin_context_id     = CView::get("context_id", "ref class|$medecin_context_class");
            $medecin_cp             = CView::get("medecin_cp", "numchar");
            $medecin_ville          = CView::get("medecin_ville", "str");
            $medecin_type           = CView::get(
                "type",
                "enum list|"
                . implode('|', CMedecin::$types)
                . "|pharmacie|maison_medicale|autre default|medecin"
            );
            $medecin_disciplines    = CView::get("disciplines", "text");
            $actif                  = CView::get("actif", "enum list|0|1|2 default|1");
            $rpps                   = CView::get("rpps", "numchar", true);
        } else {
            $medecin_nom            = CView::get("medecin_nom", "str", true);
            $medecin_prenom         = CView::get("medecin_prenom", "str", true);
            $medecin_context_class  = CView::get("context_class", "enum list|CFunctions|CGroups", true);
            $medecin_context_id     = CView::get("context_id", "ref class|$medecin_context_class", true);
            $medecin_cp             = CView::get("medecin_cp", "numchar", true);
            $medecin_ville          = CView::get("medecin_ville", "str", true);
            $medecin_type           = CView::get(
                "type",
                "enum list|"
                . implode('|', CMedecin::$types)
                . "|pharmacie|maison_medicale|autre default|medecin",
                true
            );
            $medecin_disciplines = CView::get("disciplines", "text", true);
            $actif               = CView::get("actif", "enum list|0|1|2 default|1", true);
            $rpps                = CView::get("rpps", "numchar", true);
        }

        CView::checkin();
        CView::enforceSlave();

        if (!$medecin_nom && !$medecin_prenom && !$medecin_cp && !$medecin_ville && !$medecin_disciplines && !$rpps) {
            CAppUI::stepMessage(UI_MSG_WARNING, "CCorrespondant-warning fill at least one field");

            return;
        }

        $where = [];

        $current_user = CMediusers::get();
        $is_admin     = $current_user->isAdmin();

        if ($medecin_context_id && $is_admin) {
            // Cas de la consultation en administrateur, filtr� sur une fonction
            if ($medecin_context_class === "CFunctions") {
                $where["function_id"] = "= '$medecin_context_id'";
            } elseif ($medecin_context_class === "CGroups") {
                $where["group_id"] = "= '$medecin_context_id'";
            }
        } elseif (CAppUI::isCabinet() && !$is_admin) {
            // Cas du cloisonnement en non administrateur
            $where["function_id"] = "= '$current_user->function_id'";
        } elseif (CAppUI::isGroup() && !$is_admin) {
            $where["group_id"] = "= '" . $current_user->loadRefFunction()->group_id . "'";
        }

        if ($rpps && strlen($rpps) === 11) {
            $where["rpps"] = $ds->prepareLike($rpps);
        } else {
            if ($medecin_nom) {
                $medecin_nom  = stripslashes($medecin_nom);
                $where["nom"] = $ds->prepareLike("$medecin_nom%");
            }

            if ($medecin_prenom) {
                $where["prenom"] = $ds->prepareLike("%$medecin_prenom%");
            }

            if ($medecin_disciplines) {
                $where_disciplines   = [];
                $where_disciplines[] = "medecin.disciplines LIKE '%" . $medecin_disciplines . "%'";
                $where_disciplines[] = "medecin_exercice_place.disciplines LIKE '%" . $medecin_disciplines . "%'";
                $where[]             = implode(" OR ", $where_disciplines);
            }

            if ($actif !== '2') {
                $where["actif"] = "= '$actif'";
            }

            if ($medecin_cp && ($medecin_cp != "00")) {
                $cps = self::getCps($medecin_cp);

                $where_cp = [];
                foreach ($cps as $cp) {
                    $where_cp[] = "exercice_place.cp LIKE '" . $cp . "%'";

                    // La recherche sur les codes postaux implique un full scan de la table
                    // on ne les ajoute que si on recherche au moins sur un nom ou un pr�nom
                    if ($medecin_nom || $medecin_prenom || $medecin_type) {
                        $where_cp[] = "medecin.cp LIKE '" . $cp . "%'";
                    }
                }

                $where[] = implode(" OR ", $where_cp);
            }

            if ($medecin_ville) {
                $where_ville   = [];
                $where_ville[] = "commune LIKE '%" . $medecin_ville . "%'";
                $where_ville[] = "ville LIKE '%" . $medecin_ville . "%'";
                $where[]       = implode(" OR ", $where_ville);
            }

            if ($medecin_type) {
                $where_type   = [];
                $where_type[] = "medecin.type = '" . $medecin_type . "'";
                $where_type[] = "medecin_exercice_place.type = '" . $medecin_type . "'";
                $where[]      = implode(" OR ", $where_type);
            }
        }

        // On cr�e une jointure car les donn�es peuvent �tre stock�es sur un medecin, un lieu d'exercice ou un medecin exercice place
        $ljoin = [
            'medecin_exercice_place' => 'medecin_exercice_place.medecin_id = medecin.medecin_id',
            'exercice_place'         => 'exercice_place.exercice_place_id = medecin_exercice_place.exercice_place_id',
        ];

        $medecin        = new CMedecin();
        $count_medecins = count($medecin->countMultipleList($where, null, "medecin.medecin_id", $ljoin));
        $order          = "nom, prenom";
        /** @var CMedecin[] $medecins */
        $medecins = $medecin->loadList($where, $order, "$start_med, $step_med", "medecin.medecin_id", $ljoin);

        CStoredObject::massLoadFwdRef($medecins, "function_id");
        CStoredObject::massLoadFwdRef($medecins, "group_id");

        foreach ($medecins as $key => $_medecin) {
            if (!$rpps && ($medecin_cp || $medecin_ville || $medecin_type || $medecin_disciplines)) {
                $_medecin->getExercicePlacesByFilters(
                    $medecin_cp,
                    $medecin_ville,
                    $medecin_type,
                    $medecin_disciplines
                );

                // Si il n'y a pas d'exercice place, on vient alors filtrer sur les champs du m�decin
                if (empty($_medecin->_ref_exercice_places)) {
                    if (!str_contains(CMbString::lower($_medecin->ville), CMbString::lower($medecin_ville))) {
                        unset($medecins[$key]);
                        break;
                    }

                    if (!str_contains(CMbString::lower($_medecin->type), CMbString::lower($medecin_type))) {
                        unset($medecins[$key]);
                        break;
                    }

                    if (
                        !str_contains(
                            CMbString::lower($_medecin->disciplines),
                            CMbString::lower($medecin_disciplines)
                        )
                    ) {
                        unset($medecins[$key]);
                        break;
                    }
                }
            } else {
                $_medecin->getExercicePlaces();
            }

            foreach ($_medecin->_ref_exercice_places as $key_place => $_excercice_place) {
                if ($_excercice_place->annule) {
                    unset($_medecin->_ref_exercice_places[$key_place]);
                }
            }
            foreach ($_medecin->_ref_medecin_exercice_places as $key_exercice => $_medecin_exercice) {
                if ($_medecin_exercice->annule) {
                    unset($_medecin->_ref_medecin_exercice_places[$key_exercice]);
                }
            }
        }

        $list_types = $medecin->_specs['type']->_locales;

        $this->renderSmarty(
            'inc_list_medecins',
            [
                "is_admin"       => $is_admin,
                "dialog"         => $dialog,
                "nom"            => $medecin_nom,
                "prenom"         => $medecin_prenom,
                "cp"             => $medecin_cp,
                "type"           => $medecin_type,
                "medecins"       => $medecins,
                "medecin"        => $medecin,
                "list_types"     => $list_types,
                "count_medecins" => $count_medecins,
                "start_med"      => $start_med,
                "step_med"       => $step_med,
                "view_update"    => $view_update,
            ]
        );
    }

    public function editMedecin(): void
    {
        $this->checkPermEdit();

        $medecin_id      = CView::get('medecin_id', 'ref class|CMedecin');
        $duplicate       = CView::get('duplicate', 'bool default|0');
        $medecin_type    = CView::get('medecin_type', 'str');
        $compte_rendu_id = CView::get('compte_rendu_id', 'ref class|CCompteRendu');

        CView::checkin();

        $medecin = CMedecin::findOrNew($medecin_id);

        if ($duplicate) {
            $medecin->_id = null;
        }

        if ($medecin->_id) {
            $current_user = CMediusers::get();
            $is_admin     = $current_user->isAdmin();
            if (CAppUI::isCabinet()) {
                $same_function = $current_user->function_id == $medecin->function_id;
                if (!$is_admin && !$same_function) {
                    CAppUI::accessDenied();
                }
            } elseif (CAppUI::isGroup()) {
                $same_group = $current_user->loadRefFunction()->group_id == $medecin->group_id;
                if (!$is_admin && !$same_group) {
                    CAppUI::accessDenied();
                }
            }
        }
        if (!$medecin->_id && !$medecin->type) {
            $medecin->type = $medecin_type;
        }

        $compte_rendu = CCompteRendu::findOrNew($compte_rendu_id);
        // Chargement des formules de politesse selon le praticien du contexte
        $user_id = CMediusers::get()->_id;
        $compte_rendu->loadTargetObject();

        switch (true) {
            case $compte_rendu->_ref_object instanceof CConsultation:
                $user_id = CConsultation::find($compte_rendu->object_id)->loadRefPraticien()->_id;
                break;
            case $compte_rendu->_ref_object instanceof CConsultAnesth:
                $user_id = CConsultAnesth::find($compte_rendu->object_id)->chir_id;
                break;
            case $compte_rendu->_ref_object instanceof COperation:
                $user_id = COperation::find($compte_rendu->object_id)->chir_id;
                break;
            case $compte_rendu->_ref_object instanceof CSejour:
                $user_id = CSejour::find($compte_rendu->object_id)->praticien_id;
                break;

            default:
        }

        $medecin->loadSalutations($user_id);
        $medecin->loadRefsNotes();
        $medecin->loadRefUser();
        $medecin->getExercicePlaces();

        $this->renderSmarty(
            'inc_edit_medecin',
            [
                'object'    => $medecin,
                'spec_cpam' => CSpecCPAM::getList(),
            ]
        );
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function listMedecinExercicePlaces(): void
    {
        $this->checkPermRead();

        $medecin_id = CView::get('medecin_id', 'ref class|CMedecin');

        CView::checkin();

        $medecin = CMedecin::findOrFail($medecin_id);

        /** @var CMedecinExercicePlace $_medecin_exercice_place */
        foreach ($medecin->getMedecinExercicePlaces() as $_medecin_exercice_place) {
            $_medecin_exercice_place->loadRefExercicePlace();
        }
        $this->renderSmarty('inc_list_medecin_exercice_places', ['medecin' => $medecin]);
    }

    public function chooseExercicePlace(): void
    {
        $medecin_id   = CView::get('medecin_id', 'ref class|CMedecin');
        $object_class = CView::get('object_class', 'str notNull');
        $object_id    = CView::get('object_id', 'ref class|CStoredObject meta|object_class');
        $field        = CView::get('field', 'str');

        CView::checkin();

        $medecin = CMedecin::findOrFail($medecin_id);
        $medecin->getExercicePlaces();

        $object = $object_class::findOrNew($object_id);

        $this->renderSmarty(
            'inc_choose_medecin_exercice_place',
            [
                'medecin'          => $medecin,
                'object'           => $object,
                'field'            => $field,
                'submit_on_change' => 0,
            ]
        );
    }

    /**
     * @return void
     */
    public function tooltipMedecin(): void
    {
        $this->checkPerm();

        $medecin_id                = CView::get('medecin_id', 'ref class|CMedecin');
        $medecin_exercice_place_id = CView::get('medecin_exercice_place_id', 'ref class|CMedecinExercicePlace');

        CView::checkin();

        $medecin                = CMedecin::findOrFail($medecin_id);
        $medecin_exercice_place = CMedecinExercicePlace::findOrFail($medecin_exercice_place_id);

        $medecin->canDo();
        $medecin->loadView();

        $medecin_service      = new MedecinFieldService($medecin, $medecin_exercice_place);
        $medecin->disciplines = $medecin_service->getDisciplines();
        $medecin->tel         = $medecin_service->getTel();
        $medecin->adresse     = $medecin_service->getAdresse();
        $medecin->fax         = $medecin_service->getFax();
        $medecin->cp          = $medecin_service->getCP();
        $medecin->ville       = $medecin_service->getVille();
        $medecin->portable    = $medecin_service->getPortable();
        $medecin->email       = $medecin_service->getEmail();

        $this->renderSmarty('CMedecin_view', ['object' => $medecin]);
    }

    public function listPraticiens(): void
    {
        $this->checkPermAdmin();
        $praticien_id    = CView::get("praticien_id", "str", true);
        $function_select = CView::get("function_select", "str");
        CView::checkin();

        // load all the users from the group
        $praticiens = (new CMediusers())->loadListFromType(
            null,
            PERM_READ,
            ($function_select !== 'all') ? $function_select : null,
            null,
            false
        );

        $this->renderSmarty(
            'inc_vw_export_patients_praticiens',
            [
                'praticiens'         => $praticiens,
                'array_praticien_id' => is_array($praticien_id) ? $praticien_id : (($praticien_id) ? explode(
                    ',',
                    $praticien_id
                ) : []),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function seekMedecins(): void
    {
        $this->checkPermRead();

        $nom         = CView::get('nom', 'str');
        $cp          = CView::get('medecin_cp', 'str');
        $start_med   = CView::get('start_med', 'num default|0');
        $step_med    = CView::get('step_med', 'num default|' . (CModule::getActive('rpps') ? '5' : '10'));
        $view_update = CView::get('view_update', 'str');

        CView::checkin();
        CView::enforceSlave();

        $curr_user = CMediusers::get();

        $seek_results = self::seekAutocompleteMedecins(
            $nom,
            $cp,
            true,
            $curr_user->function_id,
            "$start_med,$step_med"
        );

        /** @var CMedecin[] $medecins */
        $medecins = $seek_results['list'];

        CStoredObject::massLoadBackRefs($medecins, 'medecins_exercices_places');

        foreach ($medecins as $medecin) {
            $medecin->getExercicePlacesByFilters($cp, null, null, null);
        }

        $this->renderSmarty(
            'inc_list_medecins',
            [
                'medecins'       => $medecins,
                'is_admin'       => $curr_user->isAdmin(),
                'start_med'      => $start_med,
                'step_med'       => $step_med,
                'count_medecins' => $seek_results['countTotal'],
                'view_update'    => $view_update,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public static function seekAutocompleteMedecins(
        string $keywords,
        ?string $cps,
        bool $all_departements,
        int $function_id,
        string $limit = '50',
        string $type = null
    ): array {
        $medecin     = new CMedecin();
        $order       = 'nom';
        $group       = CGroups::loadCurrent();
        $rpps_active = CModule::getActive('rpps');
        $key_cp      = $rpps_active ? 'exercice_place.cp' : 'medecin.cp';

        $where = [];
        $ljoin = [];

        if (($type !== "pharmacie") && $rpps_active) {
            $ljoin['medecin_exercice_place'] = 'medecin_exercice_place.medecin_id = medecin.medecin_id';
            $ljoin['exercice_place']         =
                'exercice_place.exercice_place_id = medecin_exercice_place.exercice_place_id';
        }

        $where["actif"] = "= '1'";

        if ($type == "pharmacie") {
            $where['type'] = " = 'pharmacie'";
        } elseif ($cps != "") {
            $cps = self::getCps($cps);

            if (count($cps)) {
                $where_cp = [];
                foreach ($cps as $cp) {
                    $where_cp[] = "{$key_cp} LIKE '" . $cp . "%'";
                    if ($rpps_active && $keywords) {
                        $where_cp[] = "medecin.cp LIKE '" . $cp . "%'";
                    }
                }
                $where[] = "(" . implode(" OR ", $where_cp) . ")";
            }
        } elseif ($group->_cp_court && !$all_departements) {
            $where[] = "$key_cp LIKE '{$group->_cp_court}%'" .
                (($rpps_active && $keywords) ? " OR medecin.cp LIKE '{$group->_cp_court}%'" : '');
        }

        if (CAppUI::isCabinet()) {
            $where["function_id"] = "= '$function_id'";
        } elseif (CAppUI::isGroup()) {
            $user_group_id     = CMediusers::get()->loadRefFunction()->group_id;
            $where["group_id"] = "= '$user_group_id'";
        }

        $medecins = $medecin->seek($keywords, $where, $limit, true, $ljoin, $order, 'medecin.medecin_id', false);

        CStoredObject::massCountBackRefs($medecins, 'medecins_exercices_places');

        return [
            'list' => $medecins,
            'countTotal' => $medecin->_totalSeek
        ];
    }

    private static function getCps(?string $cps): array
    {
        $cps_result = preg_split("/\s*[\s\|,]\s*/", $cps);
        CMbArray::removeValue("", $cps_result);

        return $cps_result;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function contextCorrespondantMedicalAutocomplete(): void
    {
        $this->checkPerm();

        $classes       = CView::get('classes', 'str');
        $input_field   = CView::get('input_field', 'str');
        $str           = trim(CView::get($input_field, 'str'));

        CView::checkin();
        CView::enableSlave();

        if (!$classes) {
            $classes = 'CGroups|CFunctions';
        }

        $classes = explode('|', $classes);

        /** @var CGroups|CFunctions $results */
        $results = [];

        $ds = CSQLDataSource::get('std');
        $ljoin = [];

        if (in_array('CGroups', $classes)) {
            $group = new CGroups();

            $ljoin['medecin'] = 'groups_mediboard.group_id = medecin.group_id';
            $where_group['text'] = $ds->prepareLike("%$str%");

            $groups = $group->loadListWithPerms(PERM_READ, $where_group, 'text ASC', null, "group_id", $ljoin);

            foreach ($groups as $grp) {
                $results[] = $grp;
            }
        }

        if (in_array('CFunctions', $classes)) {
            $function = new CFunctions();

            $ljoin['medecin'] = 'functions_mediboard.function_id = medecin.function_id';
            $where_function['text'] = $ds->prepareLike("%$str%");

            $functions = $function->loadListWithPerms(PERM_READ, $where_function, 'text ASC', null, "function_id", $ljoin);

            foreach ($functions as $fnc) {
                $results[] = $fnc;
            }
        }

        $this->renderSmarty(
            'inc_context_correspondant_medical_autocomplete.tpl',
            [
                'results' => $results,
            ]
        );
    }
}
