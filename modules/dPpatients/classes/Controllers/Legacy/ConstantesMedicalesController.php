<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers\Legacy;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Core\Logger\LoggerLevels;
use Ox\Mediboard\ObservationResult\CObservationValueToConstant;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\PlanningOp\CSejour;

class ConstantesMedicalesController extends CLegacyController
{
    /**
     * Stores CConstantesMedicales from the CObservationResults linked to the source
     *
     * @return mixed
     * @throws Exception
     */
    public function importObservationResultsDataToConstants()
    {
        $this->checkPermEdit();

        $source_name  = CView::get("source_name", "str");
        $current_date = CView::get("date", "date default|now");

        CView::checkin();

        if (!$source_name) {
            CAppUI::stepMessage(UI_MSG_ERROR, "CExchangeSource-error-noSourceName");

            return;
        }

        $source = CMbObject::loadFromGuid($source_name);

        if (!$source->_id) {
            CAppUI::stepMessage(UI_MSG_ERROR, CAppUI::tr("CExchangeSource-no-source", $source->name));

            return;
        }

        $counter = 0;

        $frequency_integration_constants = CAppUI::gconf(
            "monitoringPatient CConstantesMedicales frequency_integration_constants"
        );

        $values      = [];
        $ds          = CSQLDataSource::get('std');
        $conversions = CObservationValueToConstant::loadForGroup();

        if (empty($conversions)) {
            CAppUI::setMsg('CObservationValueToConstant-error-conversion-not_configured', UI_MSG_ERROR);

            return;
        }

        /* @var CObservationValueToConstant[] $_conversions */
        foreach ($conversions as $_conversions) {
            /* @var CObservationValueToConstant $conversion */
            foreach ($_conversions as $conversion) {
                $query = new CRequest();
                $query->addColumn(
                    "TRUNCATE(rv.value {$conversion->conversion_operation} {$conversion->conversion_ratio}, 2)",
                    'value'
                );
                $query->addColumn('s.datetime', 'datetime');
                $query->addColumn('s.context_id', 'sejour_id');
                $query->addTable('observation_result AS r');
                $query->addLJoin(
                    'observation_result_value AS rv ON rv.observation_result_id = r.observation_result_id'
                );
                $query->addLJoin(
                    'observation_result_set AS s ON r.observation_result_set_id = s.observation_result_set_id'
                );
                $query->addLJoin('observation_value_type AS t ON rv.value_type_id = t.observation_value_type_id');
                $query->addLJoin('observation_value_unit AS u ON rv.unit_id = u.observation_value_unit_id');
                $query->addLJoin('sejour ON (sejour.sejour_id = s.context_id AND s.context_class = "CSejour")');
                $query->addWhere("s.sender_class = '{$source->_class}'");
                $query->addWhere("s.sender_id = {$source->_id}");
                $query->addWhere(
                    "s.datetime <= DATE_ADD(s.datetime, INTERVAL {$frequency_integration_constants} MINUTE)"
                );
                $query->addWhere("rv.value_type_id = {$conversion->value_type_id}");
                $query->addWhere("rv.unit_id = {$conversion->value_unit_id}");
                $query->addWhere("DATE(s.datetime) = '{$current_date}'");
                $query->addOrder('s.datetime DESC');
                $query->setLimit('20');

                $results = $ds->loadList($query->makeSelect());

                foreach ($results as $_result) {
                    if ($_result['value']) {
                        $datetime = $_result['datetime'];

                        $values[$_result['sejour_id']][$datetime][$conversion->constant_name] = $_result['value'];
                    }
                }
            }
        }

        if (count($values)) {
            $sejours = (new CSejour())->loadAll(array_keys($values));

            $ds = CSQLDataSource::get('std');

            foreach ($values as $_sejour_id => $constants) {
                $sejour = $sejours[$_sejour_id] ?? null;

                if (!$sejour) {
                    continue;
                }

                foreach ($constants as $_datetime => $_constants) {
                    $constant = new CConstantesMedicales();

                    $where = [
                        'patient_id'    => $ds->prepare('= ?', $sejour->patient_id),
                        'context_class' => $ds->prepare('= ?', $sejour->_class),
                        'context_id'    => $ds->prepare('= ?', $sejour->_id),
                        'datetime'      => $ds->prepare('= ?', $_datetime),
                        'origin'        => $ds->prepare('= ?', 'Mindray'),
                    ];

                    $constant->loadObject($where);

                    $constant->patient_id    = $sejour->patient_id;
                    $constant->context_class = $sejour->_class;
                    $constant->context_id    = $sejour->_id;
                    $constant->datetime      = $_datetime;
                    $constant->origin        = 'Mindray';

                    $constant->_convert_value = false;

                    foreach ($_constants as $_constant_name => $_constant_value) {
                        $constant->$_constant_name = $_constant_value;
                    }

                    $msg = $constant->store();

                    if ($msg) {
                        CApp::log(
                            "$msg ({$sejour->loadRefPatient()->_view}) - {$_constant_name} : {$_constant_value}",
                            [],
                            LoggerLevels::LEVEL_CRITICAL
                        );
                    }

                    if ($constant->_id) {
                        $counter++;
                    }
                }
            }
        }

        CAppUI::stepMessage(UI_MSG_OK, CAppUI::tr("CConstantesMedicales-msg-added") . " x $counter");
    }
}
