<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers;

use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Mpm\CPrescriptionLineMedicament;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Prescription\CPrescriptionLineElement;
use Symfony\Component\HttpFoundation\Response;

class DossierMedicalController extends Controller
{
    public function showTraitementsPersonnels(
        RequestParams $params,
        CPatient      $patient
    ): Response {
        $sejour_id = $params->get("sejour_id", "ref class|CSejour");
        $print     = $params->get("print", "bool default|0");

        $dossier_medical = $patient->loadRefDossierMedical();
        $dossier_medical->loadTraitementsInProgress();
        CStoredObject::massLoadBackRefs($dossier_medical->_traitements_in_progress["medicament"], "prise_posologie");
        /** @var CPrescriptionLineMedicament $_medicament */
        foreach ($dossier_medical->_traitements_in_progress["medicament"] as $_medicament) {
            $_medicament->loadRefsPrises();
        }
        CStoredObject::massLoadBackRefs($dossier_medical->_traitements_in_progress["element"], "prise_posologie");
        /** @var CPrescriptionLineElement $_element */
        foreach ($dossier_medical->_traitements_in_progress["element"] as $_element) {
            $_element->loadRefsPrises();
        }

        $sejour = CSejour::findOrNew($sejour_id);
        $sejour->loadRefPraticien();

        //Enlever les commentaires lorsque l'affichage des traitements personnels sera fait par cette route
        //if ($print) {
        return $this->renderSmarty(
            "print_traitement_personnel",
            [
                "traitements" => $dossier_medical->_traitements_in_progress,
                "patient"     => $patient,
                "sejour"      => $sejour,
                "user"        => CMediusers::get(),
                "group"       => CGroups::get(),
            ]
        );
        //}
    }
}
