<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Exceptions\ApiRequestException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CModelObject;
use Ox\Core\Controller;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CMedecinExercicePlace;
use Ox\Mediboard\Patients\Repositories\MedecinRepository;
use Symfony\Component\HttpFoundation\Response;

class MedecinsController extends Controller
{
    final public const SEARCH_RPPS                 = 'rpps';
    final public const SEARCH_WITH_RPPS_IDENTIFIER = 'identifier';

    /**
     * @param RequestApi        $request_api
     * @param MedecinRepository $medecin_repository
     *
     * @return Response
     * @throws ApiException
     * @throws ApiRequestException
     * @throws Exception
     * @api
     */
    public function list(RequestApi $request_api, MedecinRepository $medecin_repository): Response
    {
        if ($request_api->getRequest()->query->getBoolean(self::SEARCH_RPPS)) {
            $medecins = $medecin_repository->listMedecinWithRpps();
            $total    = count($medecins);
        } else {
            $medecin = new CMedecin();

            $medecins = $medecin->loadListFromRequestApi($request_api);
            $total    = $medecin->countListFromRequestApi($request_api);
        }

        // Collection.
        $collection = Collection::createFromRequest($request_api, $medecins);
        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            $total
        );

        if ($request_api->hasRelation(CMedecinExercicePlace::RESOURCE_TYPE)) {
            $collection->addFieldsetsOnRelation(
                CMedecinExercicePlace::RESOURCE_TYPE,
                [CMedecinExercicePlace::FIELDSET_SPECIALITY, CModelObject::FIELDSET_EXTRA]
            );
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * @param RequestApi        $request_api
     * @param MedecinRepository $medecin_repository
     *
     * @return Response
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function getWithRppsIdentifier(RequestApi $request_api, MedecinRepository $medecin_repository): Response
    {
        $identifier = $request_api->getRequest()->query->get(self::SEARCH_WITH_RPPS_IDENTIFIER);
        $medecin    = $medecin_repository->getMedecinByRpps($identifier);
        $item       = Item::createFromRequest($request_api, $medecin);

        if ($request_api->hasRelation(CMedecinExercicePlace::RESOURCE_TYPE)) {
            $item->addFieldsetsOnRelation(
                CMedecinExercicePlace::RESOURCE_TYPE,
                [CMedecinExercicePlace::FIELDSET_SPECIALITY, CModelObject::FIELDSET_EXTRA]
            );
        }

        return $this->renderApiResponse($item);
    }
}
