<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers;

use DateTimeImmutable;
use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\CMbString;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Exception\ControllerException;
use Ox\Mediboard\Cabinet\Repositories\Api\ConsultationRepository;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CPatientINSNIR;
use Ox\Mediboard\Patients\CPaysInsee;
use Ox\Mediboard\Patients\Services\PatientSearchService;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\PlanningOp\Repositories\OperationRepository;
use Ox\Mediboard\PlanningOp\Repositories\SejourRepository;
use Ox\Mediboard\Sante400\CIdSante400;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * CRUD API Patients.
 * Controller that answer the api calls on patients.
 */
class PatientsController extends Controller
{
    public const REQUEST_CONSULT_PARAM_PERIOD = 'period';

    public const PARAM_CONSULT_PERIOD_NEXT     = 'next';
    public const PARAM_CONSULT_PERIOD_PREVIOUS = 'previous';

    protected static array $patient_fields = [
        "nom",
        "prenom",
        "sexe",
        "naissance",
        "prenoms",
        "prenom_usuel",
        "nom_jeune_fille",
        "deces",
        "civilite",
        "rang_naissance",
        "cp_naissance",
        "lieu_naissance",
        "vip",
        "adresse",
        "ville",
        "cp",
        "pays",
        "phone_area_code",
        "tel",
        "tel2",
        "allow_sms_notification",
        "tel_pro",
        "tel_autre",
        "tel_autre_mobile",
        "email",
        "allow_email",
        "situation_famille",
        "mdv_familiale",
        "condition_hebergement",
        "niveau_etudes",
        "activite_pro",
        "profession",
        "csp",
        "fatigue_travail",
        "travail_hebdo",
        "transport_jour",
        "matricule",
        "qual_beneficiaire",
        "don_organes",
        "directives_anticipees",
        "rques",
        "tutelle",
        "commune_naissance_insee",
        "pays_naissance_insee",
    ];

    /**
     * List patients using the request parameters.
     *
     * @throws Exception
     * @throws InvalidArgumentException
     * @api
     */
    public function listPatients(RequestApi $request_api): Response
    {
        $nom                     = CMbString::utf8Decode($request_api->getRequest()->get("nom"));
        $prenom                  = CMbString::utf8Decode($request_api->getRequest()->get("prenom"));
        $sexe                    = $request_api->getRequest()->get("sexe");
        $naissance               = $request_api->getRequest()->get("naissance");
        $commune_naissance_insee = $request_api->getRequest()->get("communenaissanceinsee");
        $pays_naissance_insee    = $request_api->getRequest()->get("paysnaissanceinsee");
        $cp_naissance            = $request_api->getRequest()->get("cpnaissance");
        $commune_naissance       = CMbString::utf8Decode($request_api->getRequest()->get("communenaissance"));
        $pays_naissance          = CMbString::utf8Decode($request_api->getRequest()->get("paysnaissance"));
        $INS                     = $request_api->getRequest()->get("INS");
        $OID                     = $request_api->getRequest()->get("OID");
        $IPP                     = $request_api->getRequest()->get("IPP");
        $proche                  = $request_api->getRequest()->get("proche");
        $keywords                = $request_api->getRequest()->get("keywords");

        $mediuser              = CMediusers::get();
        $use_function_distinct = CAppUI::isCabinet() && !$mediuser->isAdmin();
        $use_group_distinct    = CAppUI::isGroup() && !$mediuser->isAdmin();
        $function_id           = $use_function_distinct ? CFunctions::getCurrent()->_id : null;
        $curr_group_id         = $mediuser->loadRefFunction()->group_id;

        $patient  = new CPatient();
        $ds       = $patient->getDS();
        $patients = [];
        $offset   = $request_api->getOffset();
        $limit    = $request_api->getLimit();
        $total    = 0;

        if ($INS) {
            $ins_patient          = new CPatientINSNIR();
            $ins_patient->ins_nir = $INS;
            $ins_patient->oid     = $OID;
            if ($ins_patient->loadMatchingObject()) {
                $patient->load($ins_patient->patient_id);
            }
        }

        if ($IPP) {
            $patient->_IPP = $IPP;
            $patient->loadFromIPP();
        }

        if ($patient->_id) {
            $patients = [$patient];
            $total    = 1;
        } elseif ($nom || $prenom) {
            $patient_search_service = new PatientSearchService();

            $prenom_search = $patient_search_service->reformatResearchValue($prenom);
            $nom_search    = $patient_search_service->reformatResearchValue($nom);

            if ($nom_search) {
                $patient_search_service->addLastNameFilter(
                    $nom_search,
                    $nom_search,
                    $nom
                );
                $patient_search_service->setOrder("LOCATE('$nom_search', nom) DESC, nom, prenom, naissance");
            } else {
                $patient_search_service->setOrder('nom, prenom, naissance');
            }

            if ($prenom_search) {
                $patient_search_service->addFirstNameFilter($prenom_search, $prenom_search);
            }


            if ($sexe && $sexe !== 'i') {
                $patient_search_service->addSexFilter($sexe);
            }

            if ($naissance) {
                $patient_search_service->addBirthFilter($naissance);
            }

            if ($pays_naissance_insee) {
                $pays_num = (new CPaysInsee())->loadByInsee($pays_naissance_insee);
                if (isset($pays_num->numerique)) {
                    $patient_search_service->addPaysNaissanceInseeFilter($pays_num);
                }
            }

            if ($commune_naissance_insee) {
                $patient_search_service->addCommuneNaissanceInseeFilter($commune_naissance_insee);
            }

            if ($cp_naissance) {
                $patient_search_service->addCPNaissanceFilter($cp_naissance);
            }

            if ($commune_naissance) {
                $patient_search_service->addLieuNaissanceFilter($commune_naissance);
            }

            if ($pays_naissance) {
                $pays_num = CPaysInsee::getPaysNumByNomFR($pays_naissance);
                if ($pays_num && $pays_num != '000') {
                    $patient_search_service->addPaysNaissanceInseeFilter($pays_num);
                }
            }

            $patient_search_service->setLimit(intval($request_api->getLimit()));

            $patient_search_service->queryPatients(
                $use_function_distinct,
                $use_group_distinct,
                $function_id,
                $curr_group_id
            );
            $patients = $patient_search_service->getPatients();
            $total    = $patient_search_service->getTotal();

            // Résultats proches
            if ($proche) {
                $patient_search_service->queryPatientsSoundex(
                    $use_function_distinct,
                    $use_group_distinct,
                    $function_id,
                    $curr_group_id
                );
                $patients = $patient_search_service->getPatientsSoundex();
                $total    = $patient_search_service->getTotalSoundex();
                if (count($patients) === 0 && $commune_naissance_insee) {
                    $patient_search_service->removeFilter('cp_naissance');
                    $patient_search_service->removeFilter('commune_naissance_insee');
                    $patient_search_service->removeFilter('pays_naissance_insee');
                    $patient_search_service->removeFilter('lieu_naissance');
                    $patient_search_service->queryPatientsSoundex(
                        $use_function_distinct,
                        $use_group_distinct,
                        $function_id,
                        $curr_group_id
                    );
                    $patients = $patient_search_service->getPatientsSoundex();
                    $total    = $patient_search_service->getTotalSoundex();
                }
            }
        } elseif ($keywords) {
            $where = [];
            if ($use_function_distinct) {
                $where["patients.function_id"] = $ds->prepare("= ?", $function_id);
            } elseif ($use_group_distinct) {
                $where["patients.group_id"] = $ds->prepare("= ?", $curr_group_id);
            }

            $patients = $patient->getAutocompleteList(
                $keywords,
                $where,
                $request_api->getLimit(),
                null,
                $request_api->getSort() ?: 'nom, prenom, naissance'
            );
        }

        $resource = Collection::createFromRequest($request_api, $patients);
        $resource->createLinksPagination($offset, $limit, $total);

        return $this->renderApiResponse($resource);
    }

    /**
     * Show a patient identified by 'patient_id' parameter.
     *
     * Security:
     * => READ Perm needed.
     *
     * @throws Exception
     * @api
     */
    public function showPatient(CPatient $patient, RequestApi $request_api): Response
    {
        $patient->updateBMRBHReStatus();
        $patient->loadRefPatientState();

        if ($patient->pays_naissance_insee) {
            $patient->_pays_naissance_insee = CPaysInsee::getPaysByNumerique($patient->pays_naissance_insee)->nom_fr;
        }

        if ($request_api->hasFieldset(CPatient::FIELDSET_IPP)) {
            $patient->loadIPP();
        }

        $item = Item::createFromRequest($request_api, $patient);
        $item->addLinks(
            $patient->buildAvatarLink()
        );

        return $this->renderApiResponse($item);
    }

    /**
     * Show a patient identified by id400 ('patient_id' and 'cabinet_id' parameter).
     *
     * @throws Exception
     * @api
     */
    public function showPatientById400SIH(RequestApi $request_api): Response
    {
        $patient_id = $request_api->getRequest()->get("patient_id");
        $cabinet_id = $request_api->getRequest()->get("cabinet_id");

        $id400 = CIdSante400::getMatch("CPatient", "ext_patient_id-{$cabinet_id}", $patient_id);

        $patient = new CPatient();
        $patient->load($id400->object_id);

        return $this->showPatient($patient, $request_api);
    }

    /**
     * Create a patient from the body content.
     *
     * @throws Exception
     * @api
     */
    public function addPatient(RequestApi $request_api): ?Response
    {
        $fields = $request_api->getContent(true, "windows-1252");

        $patient = new CPatient();

        foreach (self::$patient_fields as $_patient_field) {
            $patient->$_patient_field = isset($fields[$_patient_field]) ? $fields[$_patient_field] : "";
        }

        if ($patient->commune_naissance_insee) {
            $patient->pays_naissance_insee = CPaysInsee::NUMERIC_FRANCE;
        } elseif (isset($fields["pays_naissance_insee"])) {
            if ($fields["pays_naissance_insee"] === "99999") {
                $patient->_code_insee = $patient->cp_naissance = "99999";
            } else {
                $patient->pays_naissance_insee = (new CPaysInsee())->loadByInsee(
                    $fields["pays_naissance_insee"]
                )->numerique;
            }
        }

        if (isset($fields["fictif"])) {
            $patient->_fictif = $fields["fictif"];
        }

        if (isset($fields["douteux"])) {
            $patient->_douteux = $fields["douteux"];
        }

        if ($msg = $patient->store()) {
            (new ControllerException(Response::HTTP_BAD_REQUEST, $msg))->throw();
        }

        $response = $this->showPatient($patient, $request_api);
        $response->setStatusCode(Response::HTTP_CREATED);

        return $response;
    }

    /**
     * Update a patient by request parameters.
     *
     * @throws Exception
     * @api
     */
    public function modifyPatient(CPatient $patient, RequestApi $request_api): ?Response
    {
        $patient->nom       = $request_api->getRequest()->get("name") ?: $patient->nom;
        $patient->prenom    = $request_api->getRequest()->get("firstname") ?: $patient->prenom;
        $patient->naissance = $request_api->getRequest()->get("birth") ?: $patient->naissance;

        if ($msg = $patient->store()) {
            (new ControllerException(Response::HTTP_BAD_REQUEST, $msg))->throw();
        }

        return $this->showPatient($patient, $request_api);
    }

    /**
     * Delete a patient identified by 'patient_id' parameter.
     *
     * Security:
     * => EDIT Perm needed.
     *
     * @throws Exception
     * @api
     */
    public function deletePatient(CPatient $patient): Response
    {
        $this->deleteObject($patient);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * List consultations identified by 'patient_id' parameter and using the request parameters.
     *
     * Security:
     * => READ Perm needed.
     *
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function listConsultations(
        CPatient               $patient,
        ConsultationRepository $consultation_repository,
        RequestApi             $request_api
    ): Response {
        if ($period = $request_api->getRequest()->get(self::REQUEST_CONSULT_PARAM_PERIOD)) {
            $datetime_now = new DateTimeImmutable();

            $consultations = ($period === self::PARAM_CONSULT_PERIOD_NEXT)
                ? $consultation_repository->withPatient($patient)
                                          ->findWithGreaterThanOrEqualDate($datetime_now)
                : $consultation_repository->withPatient($patient)
                                          ->findWithLessThanOrEqualDate($datetime_now);
        } else {
            $consultations = $consultation_repository->withPatient($patient)
                                                     ->find();
        }

        $consultations = array_filter($consultations, fn($consultation) => $this->checkPermRead($consultation));

        $consultation_repository->massLoadRelations($consultations, $request_api->getRelations());

        $collection = Collection::createFromRequest($request_api, $consultations);
        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            count($consultations)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * List stays identified by 'patient_id' parameter and using the request parameters.
     * @throws Exception
     * @api
     */
    public function listSejours(
        CPatient         $patient,
        SejourRepository $sejour_repository,
        RequestApi       $request_api
    ): Response {
        $sejours = $sejour_repository->withPatient($patient)->find();

        CStoredObject::filterByPerm($sejours);

        $sejour_repository->massLoadRelations($sejours, $request_api->getRelations());

        $collection = Collection::createFromRequest($request_api, $sejours);

        /** @var Item $item */
        foreach ($collection as $item) {
            /** @var CSejour $sejour */
            $sejour = $item->getDatas();
            [$libelle_sejour] = CSejour::getLibelles($sejour);
            $item->addAdditionalDatas(['dhe_event_wording' => $libelle_sejour]);
        }

        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            count($sejours)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * List operations identified by 'patient_id' parameter and using the request parameters.
     * @throws Exception
     * @api
     */
    public function listOperations(
        CPatient            $patient,
        OperationRepository $operation_repository,
        RequestApi          $request_api
    ): Response {
        $operations = $operation_repository->withPatient($patient)->find();

        CStoredObject::filterByPerm($operations);

        $operation_repository->massLoadRelations($operations, $request_api->getRelations());

        $collection = Collection::createFromRequest($request_api, $operations);

        /** @var Item $item */
        foreach ($collection as $item) {
            /** @var COperation $op */
            $op = $item->getDatas();
            [$libelle_interv] = CSejour::getLibelles($op);
            $item->addAdditionalDatas(['determined_start_time' => $op->determineStartTime(),]);
        }

        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            count($operations)
        );

        return $this->renderApiResponse($collection);
    }
}
