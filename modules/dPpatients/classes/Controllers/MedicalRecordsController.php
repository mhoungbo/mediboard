<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Controller;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\Repositories\PatientEventRepository;
use Symfony\Component\HttpFoundation\Response;

/**
 * CRUD API CDossierMedical.
 * Controller that answer the api calls on CDossierMedical.
 */
class MedicalRecordsController extends Controller
{
    /**
     * Show a report identified by 'dossier_medical_id' parameter.
     *
     * Security:
     * => READ Perm needed.
     *
     * @throws ApiException
     * @api
     */
    public function show(CDossierMedical $medical_record, RequestApi $request_api): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request_api, $medical_record));
    }

    /**
     * List events identified by 'dossier_medical_id' parameter and using the request parameters.
     *
     * Security:
     * => READ Perm needed.
     *
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function listEvents(
        CDossierMedical        $medical_record,
        PatientEventRepository $event_repository,
        RequestApi             $request_api
    ): Response {
        $events = $event_repository->findByMedicalRecord($medical_record);
        $events = array_filter($events, fn($event) => $this->checkPermRead($event));

        $event_repository->massLoadRelations($events, $request_api->getRelations());

        $collection = Collection::createFromRequest($request_api, $events);
        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            count($events)
        );

        return $this->renderApiResponse($collection);
    }
}
