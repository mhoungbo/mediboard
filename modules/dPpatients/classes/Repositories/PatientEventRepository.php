<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Repositories;

use DateTimeImmutable;
use Exception;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\CPDODataSource;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CEvenementPatient;

/**
 * Repository to fetch CEvenementPatient objects.
 */
class PatientEventRepository extends AbstractRequestApiRepository
{
    /** @var CPDODataSource|CSQLDataSource */
    private $ds;

    public function __construct()
    {
        parent::__construct();
        $this->ds = $this->object->getDS();
    }

    /**
     * Find PatientEvents by a MedicalRecord.
     *
     * @param CDossierMedical $medical_record
     *
     * @return CEvenementPatient[]
     * @throws Exception
     */
    public function findByMedicalRecord(CDossierMedical $medical_record): array
    {
        $where = ['dossier_medical_id' => $this->ds->prepare('= ?', $medical_record->_id)];

        return $this->find($where);
    }

    /**
     * Count reminder
     *
     * @param CMediusers[]           $users
     * @param DateTimeImmutable|null $date_min
     * @param DateTimeImmutable|null $date_max
     *
     * @return int
     * @throws Exception
     */
    public function countReminder(
        array             $users = [],
        DateTimeImmutable $date_min = null,
        DateTimeImmutable $date_max = null
    ): int {
        $where = [
            'evenement_patient.rappel'       => $this->ds->prepare('= ?', '1'),
            'evenement_patient.praticien_id' => $this->ds->prepareIn(array_column($users, "_id")),
            'evenement_patient.date'         => $this->ds->prepareBetween(
                $date_min->format('Y-m-d H:i:s'),
                $date_max->format('Y-m-d H:i:s')
            ),
        ];

        return $this->count($where);
    }

    /**
     * @param array $where
     * @param array $ljoin
     *
     * @return CEvenementPatient[]
     * @throws Exception
     */
    public function find(array $where = [], array $ljoin = []): array
    {
        return $this->object->loadList(
            array_merge($this->where, $where),
            $this->order,
            $this->limit,
            null,
            $ljoin
        );
    }

    /**
     * @param array $where
     * @param array $ljoin
     *
     * @return int
     * @throws Exception
     */
    public function count(array $where = [], array $ljoin = []): int
    {
        return $this->object->countList(
            array_merge($this->where, $where),
            null,
            $ljoin
        );
    }

    protected function getObjectInstance(): CStoredObject
    {
        return new CEvenementPatient();
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    protected function massLoadRelation(array $objects, string $relation): void
    {
        switch ($relation) {
            case RequestRelations::QUERY_KEYWORD_ALL:
                $this->massLoadFiles($objects);
                $this->massLoadMedicalReports($objects);
                $this->massLoadAuthor($objects);
                break;
            case CEvenementPatient::RELATION_FILES:
                $this->massLoadFiles($objects);
                break;
            case CEvenementPatient::RELATION_MEDICAL_REPORTS:
                $this->massLoadMedicalReports($objects);
                break;
            case CEvenementPatient::RELATION_AUTHOR:
                $this->massLoadAuthor($objects);
                break;
            default:
                // Do nothing
        }
    }

    /**
     * MassLoad the author of patient event.
     *
     * @throws Exception
     */
    private function massLoadAuthor(array $objects): void
    {
        CStoredObject::massLoadFwdRef($objects, 'owner_id');
    }

    /**
     * MassLoad the files related to patient event.
     *
     * @throws Exception
     */
    private function massLoadFiles(array $objects): void
    {
        CStoredObject::massLoadBackRefs($objects, 'files', 'file_date DESC');
    }

    /**
     * MassLoad the files related to patient event.
     *
     * @throws Exception
     */
    private function massLoadMedicalReports(array $objets): void
    {
        CStoredObject::massLoadBackRefs($objets, 'documents', 'creation_date DESC');
    }
}
