<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Repositories;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Mediboard\Patients\CMedecin;

/**
 * Repository to manage medecins
 */
class MedecinRepository extends AbstractRequestApiRepository
{
    /**
     * @return array
     * @throws Exception
     */
    public function listMedecinWithRpps(): array
    {
        $medecin                      = new CMedecin();
        $where                        = [];
        $where['rpps']                = $medecin->getDS()->prepare('IS NOT NULL');
        $where['import_file_version'] = $medecin->getDS()->prepare('IS NOT NULL');

        return $medecin->loadList($where, $this->order, $this->limit);
    }

    /**
     * @param string $identifier
     *
     * @return CMedecin
     * @throws Exception
     */
    public function getMedecinByRpps(string $identifier): CMedecin
    {
        $medecin       = new CMedecin();
        $medecin->rpps = $identifier;
        $medecin->loadMatchingObject();

        return $medecin;
    }

    protected function getObjectInstance(): CStoredObject
    {
        return new CMedecin();
    }

    protected function massLoadRelation(array $objects, string $relation): void
    {
        // TODO: Implement massLoadRelation() method.
    }
}
