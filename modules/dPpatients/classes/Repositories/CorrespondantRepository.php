<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Patients\Repositories;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Import\Rpps\Entity\CPersonneExercice;
use Ox\Mediboard\Patients\CMedecin;

/**
 * Repository to manage correspondants
 */
class CorrespondantRepository extends AbstractRequestApiRepository
{
    /**
     * @param CMedecin $medecin
     * @return array
     * @throws Exception
     */
    public function findMatchingPersonneExercice(CMedecin $medecin): array
    {
        $personne_exercice                  = new CPersonneExercice();
        $personne_exercice->nom             = $medecin->nom;
        $personne_exercice->prenom          = $medecin->prenom;
        $personne_exercice->cp              = $medecin->cp;
        $personne_exercice->libelle_commune = $medecin->ville;

        return $personne_exercice->loadMatchingList();
    }

    protected function getObjectInstance(): CStoredObject
    {
        return new CMedecin();
    }

    protected function massLoadRelation(array $objects, string $relation): void
    {
        // TODO: Implement massLoadRelation() method.
    }
}
