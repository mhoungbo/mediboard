/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Traitement = {
  prescription_sejour_id: null,
  url_show_traitements_personnels : "",

  remove:                 function (oForm, onComplete) {
    var oOptions = {
      typeName: 'ce traitement',
      ajax:     1,
      target:   'systemMsg'
    };

    var oOptionsAjax = {
      onComplete: function () {
        if (onComplete) {
          onComplete();
        }
        if (window.refreshWidget) {
          TdBTamm.refreshDistinctsWidget('tp', 'tp_dossier', 'list_tp');
        }
      }
    };

    confirmDeletion(oForm, oOptions, oOptionsAjax);
  },
  cancel:                 function (oForm, onComplete) {
    $V(oForm.annule, 1);
    onSubmitFormAjax(oForm, {
      onComplete: function () {
        if (onComplete) {
          onComplete();
        }
        if (window.refreshWidget) {
          TdBTamm.refreshDistinctsWidget('tp', 'tp_dossier', 'list_tp');
        }
      }
    });
    $V(oForm.annule, '');
  },
  restore:                function (oForm, onComplete) {
    $V(oForm.annule, '0');
    onSubmitFormAjax(oForm, {
      onComplete: function () {
        if (onComplete) {
          onComplete();
        }
        if (window.refreshWidget) {
          TdBTamm.refreshDistinctsWidget('tp', 'tp_dossier', 'list_tp');
        }
      }
    });
    $V(oForm.annule, '');
  },
  toggleCancelled:        function (list) {
    $(list).select('.cancelled').invoke('toggle');
  },

  stopAll: function (form, callback) {
    if (!confirm($T('CPrescription-Ask stop all TP'))) {
      return;
    }

    onSubmitFormAjax(form, callback ? callback : null);
  },

  printTraitementPersonnel: function (sejour_id) {
    new Url()
      .setRoute(Traitement.url_show_traitements_personnels)
      .addParam("sejour_id", sejour_id)
      .addParam("print", 1)
      .addParam("dialog", 1)
      .open('');
  },

  modifyLine: function (button) {
    let contenteditable = true;
    let backgroundColor = "#ffd700";
    let buttons = button.up('td').select('.line_action');

    if (!button.hasClassName("edit")) {
      contenteditable = false;
      backgroundColor = "";
      buttons.invoke('hide');
    } else {
      buttons.invoke('show');
    }

    button.toggleClassName("edit");
    button.toggleClassName("tick");

    button.up("tr").select("td").each(function (td) {
      td.down("p").writeAttribute("contenteditable", contenteditable ? "" : false);

      td.setStyle({backgroundColor: backgroundColor});
    });
  },

  addLine: function (table) {
    table.down("tbody").insert(
      DOM.tr().insert(
        DOM.td().insert(
          DOM.p({className: "tableau_traitement_personnel"}).insert(
            DOM.button({
              className: "edit not-printable notext",
              style:     "float: left;",
              onClick:   "Traitement.modifyLine(this)"
            }))
            .insert(DOM.button({
              className: "up line_action not-printable notext",
              style:     "float: left; display: none;",
              onClick:   "Traitement.moveLine('up', this)"
            })).insert("&nbsp;")
            .insert(DOM.button({
              className: "down line_action not-printable notext",
              style:     "float: left; display: none;",
              onClick:   "Traitement.moveLine('down', this);"
            })).insert("&nbsp;")
            .insert(DOM.button({
              className: "trash line_action not-printable notext",
              style:     "float: left; display: none;",
              onClick:   "Traitement.delLine(this.up('tr'))"
            })).insert("&nbsp;")
        ))
        .insert(DOM.td().insert(DOM.p().insert("&nbsp;")))
        .insert(DOM.td().insert(DOM.p().insert("&nbsp;")))
        .insert(DOM.td().insert(DOM.p().insert("&nbsp;")))
        .insert(DOM.td().insert(DOM.p().insert("&nbsp;")))
    );
  },

  delLine: function (tr) {
    if (confirm($T('CPrescriptionLine-confirm_del'))) {
      tr.remove();
    }
  },

  moveLine: function (sens, button) {
    let line = button.up('tr');
    let other_tr = sens === 'up' ? line.previous() : line.next();

    if (!other_tr) {
      return;
    }

    line = line.remove();

    let options = {};
    options[sens === 'up' ? 'before' : 'after'] = line;

    other_tr.insert(options);
  },
};
