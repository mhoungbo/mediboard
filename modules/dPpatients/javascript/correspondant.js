/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Correspondant = {
  show_no_internal_results: false,
  current_form:             null,
  patient_form:             null,

  edit: function (correspondant_id, patient_id, callback, duplicate) {
    var url = new Url('dPpatients', 'ajax_form_correspondant');
    url.addParam('correspondant_id', correspondant_id);
    url.addParam("patient_id", patient_id);
    url.addNotNullParam("duplicate", duplicate);
    url.requestModal("380px", "95%");
    if (!Object.isUndefined(callback)) {
      url.modalObject.observe("afterClose", function () {
        callback();
        Correspondant.applyTutelle();
      });
    }
  },

  duplicate: function (correspondant_id, patient_id, callback) {
    var url = new Url('dPpatients', 'ajax_form_correspondant');
    url.addParam('correspondant_id', correspondant_id);
    url.addParam("patient_id", patient_id);
    url.addParam("duplicate", true);
    url.requestModal(600, "95%");
    if (!Object.isUndefined(callback)) {
      url.modalObject.observe("afterClose", function () {
        callback();
        Correspondant.applyTutelle();
      });
    }
  },

  onSubmit: function (form) {
    Correspondant.current_form = form;

    return onSubmitFormAjax(form, {
      onComplete: function () {
        Control.Modal.close();
      }
    });
  },

  confirmDeletion: function (form) {
    var options = {
      typeName: 'correspondant',
      objName:  $V(form.nom),
      ajax:     1
    };

    var ajax = {
      onComplete: function () {
        Control.Modal.close();
      }
    };

    confirmDeletion(form, options, ajax);
  },

  refreshList: function (patient_id) {
    var url = new Url('dPpatients', 'ajax_list_correspondants');
    url.addParam("patient_id", patient_id);
    url.requestUpdate('list-correspondants');

    var form = getForm('editFrm');
    if (form && window.Patient && Patient.refreshInfoTutelle) {
      Patient.refreshInfoTutelle($V(form.tutelle));
    }
  },

  checkCorrespondantMedical: (form, object_class, object_id, use_meff) => {
    if ($V(form.patient_id) != '') {
      new Url('patients', 'checkCorrespondantMedical')
        .addParam('patient_id', $V(form.patient_id))
        .addParam('object_id', object_id)
        .addParam('object_class', object_class)
        .addParam('use_meff', Object.isUndefined(use_meff) ? 1 : use_meff)
        .requestUpdate('correspondant_medical');

      // Au changement de patient, on vide la liste des lieux d'exercice
      $('medecin_exercice_place').update();
    }
  },

  reloadExercicePlaces: (medecin_id, object_class, object_id, field) => {
    // Si pas de m�decin, on vide la div qui contient les lieux d'exercices
    if (!medecin_id || medecin_id === '') {
      $('medecin_exercice_place').update();
      return;
    }

    new Url('patients', 'chooseExercicePlace')
      .addParam('medecin_id', medecin_id)
      .addParam('object_class', object_class)
      .addParam('object_id', object_id)
      .addParam('field', field)
      .requestUpdate('medecin_exercice_place');
  },

  refreshPageCorrespondant: function (page) {
    let form = getForm(Medecin.current_form_search);
    $V(form.start_med, page);

    if (Medecin.current_form_search === 'shortFindMedecin') {
      Correspondant.seekCorrespondants(form);
    } else {
      Correspondant.searchCorrespondants(form);
    }
  },

  search: (show_no_internal_results = 0) => {
    let form = getForm(Medecin.current_form_search);

    $V(form.start_med, 0);

    Correspondant.show_no_internal_results = show_no_internal_results;

    if (form.name === 'shortFindMedecin') {
      Correspondant.seekCorrespondants(form);
    } else {
      Correspondant.searchCorrespondants(form);
    }
  },

  seekCorrespondants: (form) => {
    new Url('patients', 'seekPersonnesExercices')
      .addFormData(form)
      .addParam('show_no_internal_results', Correspondant.show_no_internal_results)
      .requestUpdate('medicaux_result');
  },

  searchCorrespondants: (form) => {
    new Url('patients', 'searchPersonnesExercices')
      .addFormData(form)
      .addParam('show_no_internal_results', Correspondant.show_no_internal_results)
      .requestUpdate('medicaux_result');
  },

  /**
   * Ajout de correspondant
   */
  addCorrespondant: function (rpps) {
    new Url('patients', 'addCorrespondantFromRPPS')
      .addParam('rpps', rpps)
      .requestUpdate('systemMsg', {
        onComplete: () => {
          $V(getForm(Medecin.current_form_search).type_search, 'correspondant');
          Medecin.search();
        }
      });
  },

  /**
   * Mise � jour du correspondant (informations et lieux d'exercice)
   */
  updateCorrespondant: function (rpps) {
    new Url('patients', 'updateCorrespondant')
      .addParam('rpps', rpps)
      .requestUpdate('systemMsg');
  },

  /**
   * Suppression de correspondant patient
   *
   * @param correspondant_id
   * @param view
   * @param callback
   */
  delete: (correspondant_id, view, callback) => {
    if (!confirm($T('CCorrespondantPatient-Ask delete', view))) {
      return;
    }

    new Url()
      .addParam('@class', 'CCorrespondantPatient')
      .addParam('correspondant_patient_id', correspondant_id)
      .addParam('del', 1)
      .requestUpdate(
        'systemMsg',
        {
          onComplete: callback,
          method:     'POST'
        }
      );
  },

  applyTutelle: () => {
    if (!Correspondant.current_form && !Correspondant.patient_form) {
      return;
    }

    if ($V(Correspondant.current_form.relation) !== 'representant_legal') {
      return;
    }

    const parente = $V(Correspondant.current_form.parente);

    if (!['curateur', 'tuteur'].includes(parente)) {
      return;
    }

    const date_debut = $V(Correspondant.current_form.date_debut);
    const date_fin = $V(Correspondant.current_form.date_fin);
    const now = (new Date()).toDATE();

    if (date_debut && (date_debut > now)) {
      return;
    }

    if (date_fin && (date_fin < now)) {
      return;
    }

    $V(Correspondant.patient_form.tutelle, parente === 'curateur' ? 'curatelle' : 'tutelle');
  },

  makeContextCorrespondantMedicalAucocomplete: function (form, inputField, classes, classField, idField, callback) {
    if (classField !== false) {
      classField = classField || form.elements.object_class
    }

    idField = idField || form.elements.object_id

    const contextAutocomplete = new Url("dPpatients", "contextCorrespondantMedicalAutocomplete")
    contextAutocomplete.addParam("input_field", inputField.name)
    contextAutocomplete.addParam("classes", classes)
    contextAutocomplete.autoComplete(inputField, null, {
      minChars:      2,
      dropdown:      true,
      method:        "get",
      updateElement: function (selected) {
        const data = selected.down(".data_autocomplete")
        const _class = data.get("class")
        const _id = data.get("id")

        if (_class && _id) {
          if (classField !== false) {
            $V(classField, _class)
          }

          $V(idField, _id);
          $V(inputField, selected.down("strong").getText())

          if (callback) {
            callback()
          }
        }
      }
    })
  },

  addRemoveClassHoverOnOtherTrMedecin(el, action) {
    document.querySelectorAll("tr[id$=-" + el.getAttribute("id") + "]").forEach(function (other) {
      if (action === "add") {
        other.classList.add("hover")
      } else {
        other.classList.remove("hover")
      }
    })
  },

  eraseForm(form) {
    $V(form.medecin_nom, "");
    $V(form.nom, "");
    $V(form.medecin_prenom, "");
    $V(form.rpps, "");
    $V(form.medecin_cp, "");
    $V(form.medecin_ville, "");
    $V(form.type, "");
    $V(form.disciplines, "");
    $V(form._context_autocomplete, "");
    $V(form.function_id, "");
    $V(form.surnom, "");
    $V(form.prenom, "");
    $V(form.cp, "");
    $V(form.ville, "");
    $V(form.relation, "");
  },

  /**
   *
   * @param medecin_id
   * @param callback
   * @returns {Promise<void>}
   */
  checkMatchingCorrespondant: async function checkMatchingCorrespondant(medecin_id, url, callback) {
    new Url()
      .setRoute(url.replace('999999', medecin_id))
      .requestJSON(function (response) {
        if (response[0].is_unique) {
          const nom = response[0].correspondant['correspondant_nom'];
          const prenom = response[0].correspondant['correspondant_prenom'];
          const cp = response[0].correspondant['correspondant_cp'];
          const ville = response[0].correspondant['correspondant_ville'];
          const rpps = response[0].correspondant['correspondant_rpps'];
          const profession = response[0].correspondant['correspondant_profession'];

          const user_action = confirm('Confirmer la s�lection du correspondant m�dical ' + nom +
            ' ' + prenom +
            ' exercant en tant que ' + profession +
            ' � ' + cp +
            ' ' + ville +
            ' ?');

          return callback([user_action, rpps]);
        }

        return callback([false]);
      });
  },

  /**
   *
   * @param medecin_id
   * @param rpps
   * @param token
   * @param url
   * @param callback
   * @returns {Promise<void>}
   */
  addRppsToCorrespondant: async function addRppsToCorrespondant(medecin_id, rpps, token, url, callback) {
    new Url()
      .setRoute(url.replace('999999', medecin_id))
      .addParam('rpps', rpps)
      .addParam('token', token)
      .requestJSON(function (response) {
        return callback(response)
      }, {method: 'post'});
  }
};
