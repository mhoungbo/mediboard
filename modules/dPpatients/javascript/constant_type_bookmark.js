/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ConstantTypeBookmark = window.ConstantTypeBookmark || {
  /**
   * Add a constant name to bookmark.
   *
   * @param {string}   constant_name
   * @param {string}   patient_id
   * @param {function} callback
   */
  create: function (constant_name, patient_id, callback = null) {
    new Url()
      .addParam('@class', 'CConstantTypeBookmark')
      .addParam('patient_id', patient_id)
      .addParam('constant_type', constant_name)
      .requestUpdate('systemMsg', {
        method: 'POST',
        onComplete: callback
      });
  },

  /**
   * Delete a constant name to bookmark.
   *
   * @param {string}   constant_type_bookmark_id
   * @param {function} callback
   */
  delete: function (constant_type_bookmark_id, callback = null) {
    new Url()
      .addParam('@class', 'CConstantTypeBookmark')
      .addParam('constant_type_bookmark_id', constant_type_bookmark_id)
      .addParam('del', '1')
      .requestUpdate('systemMsg', {
        method: 'DELETE',
        onComplete: callback
      });
  }
}
