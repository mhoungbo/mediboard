<?php

/**
 * @package Mediboard\Patients
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CValue;
use Ox\Core\CView;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\Controllers\Legacy\CMedecinLegacyController;

CCanDo::checkRead();

$current_user = CMediusers::get();

$keywords         = CValue::post("_view");
$all_departements = CValue::post("all_departements", 0);
$function_id      = CValue::get("function_id", $current_user->function_id);
$type             = CValue::get("type", "medecin");

$input_field = CView::get("input_field", "str");
if ($keywords == "") {
    $keywords = CView::post($input_field, "str");
}
CView::enforceSlave(false);
CView::checkin();

if ($keywords == "") {
    $keywords = "%%";
}

$matches = CMedecinLegacyController::seekAutocompleteMedecins(
    $keywords,
    CAppUI::pref("medecin_cps_pref"),
    boolval($all_departements),
    intval($function_id),
    50,
    $type
)['list'];

// Cr�ation du template
$smarty = new CSmartyDP();

$smarty->assign("keywords", $keywords);
$smarty->assign("matches", $matches);
$smarty->assign("nodebug", true);

$smarty->display("httpreq_do_medecins_autocomplete");
