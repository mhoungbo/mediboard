<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sante400\Tests\Helpers;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Cache;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Tests\TestsException;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class CRecordSante400Test
 */
trait OverrideTagIPPTrait
{
    private static string $ipp_tag = 'tag_ipp';
    private static ?string $ipp_tag_old = null;
    private static ?string $group_id = null;

    /**
     * @throws TestsException
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        if (method_exists(self::class, 'configureTagIPP')) {
            $data = self::configureTagIPP();
            self::$ipp_tag = $data[0] ?? null;
            self::$group_id = $data[1] ?? null;
        }

        if (self::$group_id === null) {
            self::$group_id = CGroups::loadCurrent()->_id;
        }

        $cache = Cache::getCache(Cache::INNER);
        $cache_key = 'CPatient.getTagIPP-' . self::$group_id;
        self::$ipp_tag_old = $cache->get($cache_key);

        $cache->set($cache_key, self::$ipp_tag);
    }

    /**
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        $cache = Cache::getCache(Cache::INNER);
        $cache_key = 'CPatient.getTagIPP-' . self::$group_id;
        if (self::$ipp_tag_old) {
            $cache->set($cache_key, self::$ipp_tag_old);
        } else {
            $cache->delete($cache_key);
        }
    }
}
