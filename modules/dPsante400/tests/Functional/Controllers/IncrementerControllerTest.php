<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sante400\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Mediboard\Sante400\CIncrementer;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class IncrementerControllerTest extends OxWebTestCase
{
    /**
     * @param array $content
     *
     * @return CIncrementer
     * @throws CMbModelNotFoundException
     * @throws TestsException
     * @throws Exception
     * @dataProvider createProvider
     */
    public function testCreate(array $content): CIncrementer
    {
        $content = Item::createFromArray([AbstractObject::DATA => $content]);

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
            ->request('POST', '/api/identifiers/incrementers', [], [], [], json_encode($content));

        $this->assertResponseStatusCodeSame(201);

        $collection = $this->getJsonApiCollection($client);
        $this->assertCount(1, $collection);

        $item = $collection->getFirstItem();

        $this->assertEquals('incrementer', $item->getType());
        $this->assertNotNull($item->getId());
        $this->assertEquals('CPatient', $item->getAttribute('_object_class'));
        $this->assertEquals('1934', $item->getAttribute('value'));
        $this->assertEquals('%06d', $item->getAttribute('pattern'));
        $this->assertNotNull($item->getAttribute('last_update'));

        return CIncrementer::findOrFail($item->getId());
    }

    /**
     * @return \array[][]
     */
    public function createProvider(): array
    {
        return [
            'Create a patient incrementer' => [
                [
                    Item::ID         => '',
                    Item::TYPE       => 'incrementer',
                    Item::ATTRIBUTES => [
                        '_object_class' => 'CPatient',
                        'value'         => '1934',
                        'pattern'       => '%06d',
                        'last_update'   => 'now',
                    ],
                ],
            ],
        ];
    }
}
