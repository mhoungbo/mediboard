<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sante400\Tests\Functional\Controllers;

use Ox\Core\CMbDT;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Mediboard\System\Tests\Fixtures\NoteFixtures;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;

class IdexControllerTest extends OxWebTestCase
{
    public function testCreate(): CIdSante400
    {
        $user = CUser::get();

        $content = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID            => '',
                    Item::TYPE          => 'idex',
                    Item::ATTRIBUTES    => [
                        'id400' => 'identifier',
                        'tag'   => 'idex tag',
                    ],
                    Item::RELATIONSHIPS => [
                        'context' => [
                            AbstractObject::DATA => [
                                Item::ID   => $user->_id,
                                Item::TYPE => $user::RESOURCE_TYPE,
                            ],
                        ],
                    ],
                ],
            ]
        );

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('POST', '/api/identifiers/idex?relations=all', [], [], [], json_encode($content));

        $this->assertResponseStatusCodeSame(201);

        $coll = $this->getJsonApiCollection($client);
        $this->assertCount(1, $coll);

        $item = $coll->getFirstItem();

        $this->assertEquals('idex', $item->getType());
        $this->assertNotNull($item->getId());
        $this->assertEquals('identifier', $item->getAttribute('id400'));
        $this->assertEquals('idex tag', $item->getAttribute('tag'));
        $this->assertNotNull($item->getAttribute('datetime_create'));
        $this->assertNotNull($item->getAttribute('last_update'));

        $context = $item->getRelationship('context');
        $this->assertNotNull($context);
        $this->assertEquals($user->_id, $context->getId());
        $this->assertEquals($user::RESOURCE_TYPE, $context->getType());


        return CIdSante400::findOrFail($item->getId());
    }

    /**
     * @depends testCreate
     */
    public function testShow(CIdSante400 $idex): CIdSante400
    {
        $client = self::createClient();
        $client->request('GET', "/api/identifiers/idex/$idex->_id");

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals('idex', $item->getType());
        $this->assertEquals($idex->_id, $item->getId());

        return $idex;
    }

    /**
     * @depends testShow
     */
    public function testUpdate(CIdSante400 $idex): CIdSante400
    {
        $update = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID         => $idex->_id,
                    Item::TYPE       => $idex::RESOURCE_TYPE,
                    Item::ATTRIBUTES => [
                        'tag' => 'Some tag',
                    ],
                ],
            ]
        );

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('PATCH', "/api/identifiers/idex/$idex->_id", [], [], [], json_encode($update));

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals('idex', $item->getType());
        $this->assertEquals($idex->_id, $item->getId());
        $this->assertEquals('identifier', $item->getAttribute('id400'));
        $this->assertEquals('Some tag', $item->getAttribute('tag'));

        return CIdSante400::findOrFail($idex->_id);
    }

    /**
     * @depends testUpdate
     */
    public function testDelete(CIdSante400 $idex): void
    {
        $client = self::createClient();
        $client->request('DELETE', "/api/identifiers/idex/$idex->_id");

        $this->assertResponseStatusCodeSame(204);
        $this->assertEquals('', $client->getResponse()->getContent());

        $this->assertFalse(CIdSante400::find($idex->_id));
    }

    public function testListWithoutReadOnContext(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, NoteFixtures::USER_NOTE_FIXTURES);

        $current_user = CUser::get();
        try {
            $actual_perm                                                            = CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] = PERM_DENY;

            $client = self::createClient();
            $client->request('GET', '/api/identifiers/idex/' . $user::RESOURCE_TYPE . '/' . $user->_id);

            $this->assertResponseStatusCodeSame(403);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id]['CMediusers'][$user->_id] = $actual_perm;
            }
        }
    }

    public function testList(): void
    {
        $user              = CUser::get();
        $idx               = new CIdSante400();
        $idx->object_class = $user->_class;
        $idx->object_id    = $user->_id;
        $idx->id400        = 'test';
        $idx->tag          = 'tag_test';
        $this->storeOrFailed($idx);

        $idx2               = new CIdSante400();
        $idx2->object_class = $user->_class;
        $idx2->object_id    = $user->_id;
        $idx2->id400        = 'Another_idex';
        $idx2->tag          = 'tag_test';
        $this->storeOrFailed($idx2);

        $client = self::createClient();
        $client->request(
            'GET',
            '/api/identifiers/idex/' . $user::RESOURCE_TYPE . '/' . $user->_id,
            [
                'relations' => 'context',
                'sort'      => '-datetime_create',
                'filter'    => 'id400.equal.test',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);
        $this->assertGreaterThanOrEqual(1, $collection->count());

        $new_idex = $collection->getFirstItem();
        $this->assertEquals('idex', $new_idex->getType());
        $this->assertEquals($idx->_id, $new_idex->getId());
        $this->assertEquals($user->_id, $new_idex->getRelationship('context')->getId());

        $idx->delete();
    }
}
