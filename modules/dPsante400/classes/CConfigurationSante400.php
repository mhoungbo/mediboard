<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sante400;

use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\System\AbstractConfigurationRegister;
use Ox\Mediboard\System\CConfiguration;

/**
 * @codeCoverageIgnore
 */
class CConfigurationSante400 extends AbstractConfigurationRegister
{
    public function register()
    {
        $types = [];
        foreach (CSejour::$types as $_type) {
            $types[$_type] = "str default|$_type";
        }

        CConfiguration::register(
            [
                "CGroups" => [
                    "dPsante400" => [
                        "CIdSante400"  => [
                            "add_ipp_nda_manually"     => "bool default|0",
                            "admit_ipp_nda_obligatory" => "bool default|0",
                        ],
                        "CIncrementer" => [
                            "type_sejour" => $types,
                            "CSejour"     => [
                                "increment_NDA_date_min" => "date",
                            ],
                        ],
                        "CDomain"      => [
                            "group_id_pour_sejour_facturable" => "num",
                        ],
                    ],
                ],
            ]
        );
    }
}
