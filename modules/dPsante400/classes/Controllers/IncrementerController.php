<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sante400\Controllers;

use Exception;
use Ox\Core\Api\Request\Content\RequestContentException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFieldsets;
use Ox\Core\CModelObject;
use Ox\Core\Controller;
use Ox\Mediboard\Sante400\CIncrementer;
use Symfony\Component\HttpFoundation\Response;

class IncrementerController extends Controller
{
    /**
     * @throws RequestContentException
     * @throws Exception
     * @api
     *
     * Create one or multiple incrementer.
     */
    public function create(RequestApi $request_api): Response
    {
        $incrementers = $request_api->getModelObjectCollection(
            CIncrementer::class,
            [CModelObject::FIELDSET_DEFAULT],
        );

        $collection = $this->storeCollection($incrementers);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }
}
