<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Sante400\Controllers;

use Exception;
use Ox\Core\Api\Request\Content\RequestContentException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbObject;
use Ox\Core\Controller;
use Ox\Core\CRequest;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Sante400\CIdSante400;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * CRUD for CIdSante400 items
 */
class IdexController extends Controller
{
    /**
     * @throws InvalidArgumentException
     * @throws Exception
     * @api
     *
     * List the external identifiers for the chosen context.
     * Permissions are checked on context and each external identifier.
     */
    public function list(string $resource_type, string $resource_id, RequestApi $request): Response
    {
        $context = $this->getObjectFromResourceTypeAndId($resource_type, $resource_id);
        $context->needsRead();

        $filters = $request->getFilterAsSQL($context->getDS());

        /** @var CIdSante400[] $identifiers */
        $identifiers = $context->loadBackRefs(
                   'identifiants',
                   $request->getSortAsSql(),
                   $request->getLimitAsSql(),
            where: $filters
        );

        foreach ($identifiers as $idex) {
            $idex->_fwd['object_id'] = $context;
            $idex->getSpecialType();
        }

        $collection = Collection::createFromRequest($request, $identifiers);
        /** @var Item $item */
        foreach ($collection as $item) {
            $item->addAdditionalDatas(['idex_type' => $item->getDatas()->_type]);
        }

        $collection->createLinksPagination(
            $request->getOffset(),
            $request->getLimit(),
            $context->countBackRefs('identifiants', $filters)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * @throws Exception
     * @api
     *
     * Display a single external identifier.
     */
    public function show(CIdSante400 $idex, RequestApi $request): Response
    {
        $idex->loadFwdRef('object_id')->needsRead();
        $item = Item::createFromRequest($request, $idex);
        $item->addAdditionalDatas(['idex_type' => $idex->getSpecialType()]);

        return $this->renderApiResponse($item);
    }

    /**
     * @throws RequestContentException
     * @throws Exception
     * @api
     *
     * Create one or multiple external identifier.
     */
    public function create(RequestApi $request): Response
    {
        $check_existence = $request->getRequest()->query->getBoolean('check_existence');
        $idexs           = $request->getModelObjectCollection(CIdSante400::class);

        if ($check_existence) {
            /** @var CIdSante400 $idex */
            foreach ($idexs as $key => $idex) {
                $idex->datetime_create = $idex->last_update = null;
                $idex->loadMatchingObjectEsc();
                if ($idex->_id) {
                    unset($idexs[$key]);
                }
            }
        }

        $identifiers = $this->storeCollection(
            $idexs,
            false
        );
        $this->massLoadRelations($identifiers, $request->getRelations());

        return $this->renderApiResponse(Collection::createFromRequest($request, $identifiers), Response::HTTP_CREATED);
    }

    /**
     * @throws Exception
     * @api
     *
     * Update an external identifier.
     */
    public function update(CIdSante400 $idex, RequestApi $request): Response
    {
        $idex->loadFwdRef('object_id')->needsRead();

        return $this->renderApiResponse(
            Item::createFromRequest(
                $request,
                $this->storeObject($request->getModelObject($idex), false)
            )
        );
    }

    /**
     * @throws Exception
     * @api
     *
     * Delete the external identifier.
     */
    public function delete(CIdSante400 $idex): Response
    {
        $idex->loadFwdRef('object_id')->needsRead();

        $this->deleteObject($idex);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Massload the relation object_id from the external identifiers.
     *
     * @throws Exception
     */
    private function massLoadRelations(array $identifiers, array $relations): void
    {
        if (
            in_array(RequestRelations::QUERY_KEYWORD_ALL, $relations)
            || in_array(CIdSante400::RELATION_CONTEXT, $relations)
        ) {
            CStoredObject::massLoadFwdRef($identifiers, 'object_id');
        }
    }

    /**
     * @throws Exception
     */
    public function listIdentifiants(RequestParams $params): Response
    {
        $idex_id      = $params->get("idex_id", "ref class|CIdSante400");
        $page         = $params->get("page", "num default|0");
        $object_class = $params->get("object_class", "str");
        $object_id    = $params->get("object_id", ($object_class ? "ref class|$object_class" : "num"));
        $tag          = $params->get("tag", "str");
        $id400        = $params->get("id400", "str");
        $duplicate    = $params->get("duplicate", "bool default|0");

        $this->enforceSlave();

        if (!$object_id && !$object_class && !$tag && !$id400) {
            $this->addUiMsgWarning('No filter');

            return $this->renderEmptyResponse();
        }

        $idSante400 = new CIdSante400();
        $ds         = $idSante400->getDS();

        // Chargement de la liste des id4Sante400 pour le filtre
        $filter               = new CIdSante400();
        $filter->object_id    = $object_id;
        $filter->object_class = $object_class;
        $filter->nullifyEmptyFields();

        // Chargement de la cible si objet unique
        $target = null;
        if ($filter->object_id && $filter->object_class) {
            $target = CMbObject::getInstance($filter->object_class);
            $target->load($filter->object_id);
        }

        $where = [];
        if ($object_id) {
            $where[] = $ds->prepare('object_id = ?', $object_id);
        }

        if ($object_class) {
            $where[] = $ds->prepare('object_class = ?', $object_class);
        }
        if ($id400) {
            $where[] = $ds->prepare('id400 = ?', $id400);
        }

        if ($tag) {
            $where[] = $ds->prepare('tag = ?', $tag);
        }

        $step = 25;

        if ($duplicate) {
            $request = new CRequest();
            $request->addSelect(
                "id_sante400_id,
             object_class,
              object_id,
              tag,
              GROUP_CONCAT(last_update SEPARATOR '|') AS last_update,
              GROUP_CONCAT(datetime_create SEPARATOR '|') AS datetime_create,
              GROUP_CONCAT(id400 SEPARATOR '|') AS id400,
              COUNT(*) as nb_idex"
            );
            $request->addTable($idSante400->getSpec()->table);
            $request->addWhere($where);
            $request->addGroup('object_class, object_id, tag');
            $request->setLimit("$page, $step");
            $request->addOrder('last_update DESC');
            $request->addHaving('nb_idex > 1');

            $results = $ds->loadList($request->makeSelect());

            $idexs = [];
            foreach ($results as $_idex) {
                $new_id400                   = new CIdSante400();
                $new_id400->id_sante400_id   = $_idex['id_sante400_id'];
                $new_id400->object_class     = $_idex['object_class'];
                $new_id400->object_id        = $_idex['object_id'];
                $new_id400->tag              = $_idex['tag'];
                $new_id400->_datetime_create = explode('|', $_idex['datetime_create']);
                $new_id400->_id400           = explode('|', $_idex['id400']);
                $new_id400->_last_update     = explode('|', $_idex['last_update']);
                $new_id400->_nb_idex         = $_idex['nb_idex'];
                $idexs[]                     = $new_id400;
            }

            $step        = 1;
            $total_idexs = count($idexs);
        } else {
            $idexs       = $idSante400->loadList($where, null, "{$page}, {$step}");
            $total_idexs = $idSante400->countList($where);
        }

        CStoredObject::massLoadFwdRef($idexs, "object_id");
        foreach ($idexs as $_idex) {
            $_idex->getSpecialType();
        }

        return $this->renderSmarty('inc_list_identifiants.tpl', [
            'idexs'                 => $idexs,
            'total_idexs'           => $total_idexs,
            'filter'                => $filter,
            'idex_id'               => $idex_id,
            'page'                  => $page,
            'step'                  => $step,
            'target'                => $target,
            'looking_for_duplicate' => $duplicate ? true : false,
        ]);
    }

    /**
     * @throws Exception
     */
    public function countTags(RequestParams $params): Response
    {
        $object_class = $params->get("object_class", "str");
        $tag          = $params->get("tag", "str");
        $values       = $params->get("values", "str");

        if (!$object_class || !$tag || !$values) {
            return $this->renderJsonResponse(json_encode(['error_tag' => true]));
        }

        $values = array_filter(explode(',', $values));
        $values = array_map('trim', $values);

        $tags = $this->loadFilteredIdSante400($object_class, $tag, $values);

        return $this->renderJsonResponse(json_encode(['nb_tags' => count($tags)]));
    }

    /**
     * @throws Exception
     */
    public function editTags(RequestParams $params): Response
    {
        $object_class = $params->get("object_class", "str");
        $tag          = $params->get("tag", "str");
        $values       = $params->get("values", "str");
        $new_tag      = $params->get("new_tag", "str");

        if (!$object_class || !$tag || !$values || !$new_tag) {
            return $this->renderJsonResponse(json_encode(['error_tag' => true]));
        }

        $success = [];
        $error   = [];

        $values = array_filter(explode(',', $values));
        $values = array_map('trim', $values);

        $tags = $this->loadFilteredIdSante400($object_class, $tag, $values);

        foreach ($tags as $_tag) {
            $_tag->tag = $new_tag;
            if ($_tag->store()) {
                $error[] = $_tag;
                continue;
            }
            $success[] = $_tag;
        }

        return $this->renderJsonResponse(
            json_encode(['nb_tags' => count($tags), 'nb_success' => count($success), 'nb_error' => count($error)])
        );
    }

    /**
     * @throws Exception
     */
    private function loadFilteredIdSante400(string $object_class, string $tag, array $values): array
    {
        $id_sante400 = new CIdSante400();
        $ds          = $id_sante400->getDS();

        $where = [];

        if ($object_class) {
            $where[] = $ds->prepare('object_class = ?', $object_class);
        }

        if ($tag) {
            $where[] = $ds->prepare('tag = ?', $tag);
        }

        if (!empty($values)) {
            $where['id400'] = $ds->prepareIn($values);
        }

        return $id_sante400->loadList($where);
    }
}
