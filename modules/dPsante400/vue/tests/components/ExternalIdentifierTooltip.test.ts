/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import ExternalIdentifier from "@modules/dPsante400/vue/models/ExternalIdentifier"
import OxTranslator from "@/core/plugins/OxTranslator"
import ExternalIdentifierTooltip
    from "@modules/dPsante400/vue/components/ExternalIdentifierTooltip/ExternalIdentifierTooltip.vue"
import OxTest from "@oxify/utils/OxTest"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * Test for ExternalIdentifierLine
 */
export default class ExternalIdentifierTooltipTest extends OxTest {
    protected component = ExternalIdentifierTooltip
    private externalIdentifiers = [] as ExternalIdentifier[]

    protected beforeTest () {
        super.beforeTest()

        this.externalIdentifiers = []
        const externalIdentifier = new ExternalIdentifier()
        externalIdentifier.id = "1"
        externalIdentifier.id400 = "identifier"
        externalIdentifier.dateTimeCreate = "2023-04-18 16:44:26"
        this.externalIdentifiers.push(externalIdentifier)
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testCreateShouldEmitCreate () {
        const externalIdentifierTooltip = this.mountComponent({
            externalIdentifiers: this.externalIdentifiers
        })

        this.privateCall(externalIdentifierTooltip.vm, "create")

        expect(externalIdentifierTooltip.emitted("create")).toBeDefined()
    }

    public testModifyShouldEmitModify () {
        const externalIdentifierTooltip = this.mountComponent({
            externalIdentifiers: this.externalIdentifiers
        })

        this.privateCall(externalIdentifierTooltip.vm, "modify", this.externalIdentifiers[0])

        expect(externalIdentifierTooltip.emitted("modify")).toEqual([[this.externalIdentifiers[0]]])
    }

    public testDeleteShouldEmitDelete () {
        const externalIdentifierTooltip = this.mountComponent({
            externalIdentifiers: this.externalIdentifiers
        })

        this.privateCall(externalIdentifierTooltip.vm, "askDelete", this.externalIdentifiers[0])

        expect(externalIdentifierTooltip.emitted("delete")).toEqual([[this.externalIdentifiers[0]]])
    }

    public testExternalIdentifiersAreNotEmpty () {
        const externalIdentifierTooltip = this.mountComponent({
            externalIdentifiers: this.externalIdentifiers
        })
        expect(externalIdentifierTooltip.vm["isExternalIdentifiersEmpty"]).toBe(false)
    }

    public testExternalIdentifiersAreEmpty () {
        const externalIdentifierTooltip = this.mountComponent({
            externalIdentifiers: []
        })
        expect(externalIdentifierTooltip.vm["isExternalIdentifiersEmpty"]).toBe(true)
    }
}

(new ExternalIdentifierTooltipTest()).launchTests()
