/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import ExternalIdentifierLine
    from "@modules/dPsante400/vue/components/ExternalIdentifierLine/ExternalIdentifierLine.vue"
import ExternalIdentifier from "@modules/dPsante400/vue/models/ExternalIdentifier"
import OxTranslator from "@/core/plugins/OxTranslator"
import OxDropdownButton from "@oxify/components/OxDropdownButton/OxDropdownButton.vue"
import { cloneObject } from "@/core/utils/OxObjectTools"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

let externalIdentifier: ExternalIdentifier

describe("ExternalIdentifierLine", () => {
    beforeEach(() => {
        externalIdentifier = new ExternalIdentifier()
        externalIdentifier.id400 = "identifier"
        externalIdentifier.dateTimeCreate = "2023-04-18 16:44:26"
        externalIdentifier.idexType = "IPP"
    })
    afterEach(() => {
        jest.clearAllMocks()
    })

    test("emit modify when is modified", () => {
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier
            },
            localVue
        })
        line.vm["modify"]()
        expect(line.emitted("modify")).toEqual([[externalIdentifier]])
    })
    test("emit delete when deletion is asked", () => {
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier
            },
            localVue
        })
        line.vm["askDelete"]()
        expect(line.emitted("delete")).toEqual([[externalIdentifier]])
    })
    test("display more button when specified", () => {
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier,
                showMoreButton: true
            },
            localVue
        })
        const dropdown = line.findComponent(OxDropdownButton)

        expect(dropdown.exists()).toBe(true)
    })
    test("more button is hide by default", () => {
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier
            },
            localVue
        })
        const dropdown = line.findComponent(OxDropdownButton)

        expect(dropdown.exists()).toBe(false)
    })
    test("IPP type chip should be green", () => {
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier
            },
            localVue
        })
        expect(line.vm["chipColor"]).toBe("#6cb94d")
    })
    test("action is hide by default", () => {
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier
            },
            localVue
        })
        expect(line.vm["moreButtonClass"]).toBe("")
    })
    test("action is shown after actionChange", () => {
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier
            },
            localVue
        })
        line.vm["actionChange"](true)
        expect(line.vm["moreButtonClass"]).toBe("shown")
    })
    test("identifier without lastUpdate is not modified", () => {
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier
            },
            localVue
        })
        expect(line.vm["isModified"]).toBe(false)
    })
    test("identifier with same creation date and last update date is not modified", () => {
        const idex = cloneObject(externalIdentifier)
        idex.lastUpdate = "2023-04-18 16:44:26"
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier: idex
            },
            localVue
        })
        expect(line.vm["isModified"]).toBe(false)
    })
    test("identifier with different creation date and last update date is modified", () => {
        const idex = cloneObject(externalIdentifier)
        idex.lastUpdate = "2023-05-18 16:44:26"
        const line = shallowMount(ExternalIdentifierLine, {
            propsData: {
                externalIdentifier: idex
            },
            localVue
        })
        expect(line.vm["isModified"]).toBe(true)
    })
})
