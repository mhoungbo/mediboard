/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import { cloneObject } from "@/core/utils/OxObjectTools"
import { setActivePinia } from "pinia"
import {
    ExternalIdentifiersApiMock,
    ExternalIdentifiersSchemaApiMock
} from "@modules/dPsante400/vue/tests/mocks/ExternalIdentifierMocks"
import OxTranslator from "@/core/plugins/OxTranslator"
import ExternalIdentifierDialog
    from "@modules/dPsante400/vue/components/ExternalIdentifierDialog/ExternalIdentifierDialog.vue"
import oxApiService from "@/core/utils/OxApiService"
import OxObject from "@/core/models/OxObject"
import pinia from "@/core/plugins/OxPiniaCore"
import ExternalIdentifier from "@modules/dPsante400/vue/models/ExternalIdentifier"
import { flushPromises } from "@oxify/utils/OxTest"
import * as OxApiManager from "@/core/utils/OxApiManager"
import * as OxNotifyManager from "@/core/utils/OxNotifyManager"
import { useSchemaStore } from "@/core/stores/schema"

const localVue = createLocalVue()
localVue.use(OxTranslator)

let schemaStore
let object
let spyApi

/* eslint-disable dot-notation */
describe("ExternalIdentifierDialog", function () {
    beforeAll(() => {
        setActivePinia(pinia)
        schemaStore = useSchemaStore()
    })
    beforeEach(() => {
        object = new OxObject()
        object.id = "1"
        object.type = "test_object"

        spyApi = jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: ExternalIdentifiersSchemaApiMock })
            .mockResolvedValue({ data: ExternalIdentifiersApiMock })
    })
    afterEach(() => {
        schemaStore.$reset()
        jest.resetAllMocks()
        jest.clearAllMocks()
    })
    test("load idex on mounted", async () => {
        const dialog = shallowMount(
            ExternalIdentifierDialog, {
                propsData: { object },
                localVue,
                pinia
            }
        )
        await flushPromises()

        expect(dialog.vm["externalIdentifiers"].length).toEqual(2)
    })
    test("no default identifier is selected", async () => {
        const dialog = shallowMount(
            ExternalIdentifierDialog, {
                propsData: { object },
                localVue,
                pinia
            }
        )
        await flushPromises()

        expect(dialog.vm["selectedExternalIdentifier"]).toBeUndefined()
    })
    test("given identifier is selected by default", async () => {
        const selected = new ExternalIdentifier()
        selected.id = "1"
        selected.id400 = "Lorem"
        selected.tag = null
        selected.dateTimeCreate = "2023-04-19 09:57:45"
        selected.lastUpdate = "2023-04-19 09:58:30"
        selected.idexType = "IPP"
        const dialog = shallowMount(
            ExternalIdentifierDialog, {
                propsData: {
                    object,
                    defaultSelectedExternalIdentifier: selected
                },
                localVue,
                pinia
            }
        )
        await flushPromises()

        expect(dialog.vm["selectedExternalIdentifier"]).toMatchObject(selected)
        expect(dialog.vm["selectedExternalIdentifierMutated"]).toMatchObject(selected)
    })
    test("detect the current selected identifier", async () => {
        const selected = new ExternalIdentifier()
        selected.id = "1"
        const notSelected = new ExternalIdentifier()
        notSelected.id = "2"
        const dialog = shallowMount(
            ExternalIdentifierDialog, {
                propsData: {
                    object,
                    defaultSelectedExternalIdentifier: selected
                },
                localVue,
                pinia
            }
        )
        await flushPromises()

        expect(dialog.vm["externalIdentifierSelected"](notSelected)).toEqual({ active: false })
        expect(dialog.vm["externalIdentifierSelected"](selected)).toEqual({ active: true })
    })
    test("emit event for deletion asking", async () => {
        const dialog = shallowMount(
            ExternalIdentifierDialog, {
                propsData: { object },
                localVue,
                pinia
            }
        )
        await flushPromises()
        dialog.vm["askDelete"]()
        expect(dialog.emitted("askDelete")).toBeDefined()
    })
    test("create new identifier", async () => {
        const dialog = shallowMount(
            ExternalIdentifierDialog, {
                propsData: { object },
                localVue,
                pinia
            }
        )
        const identifier = new ExternalIdentifier()
        identifier.id400 = "lorem"
        identifier.tag = "ipsum"
        identifier.context = object

        // @ts-ignore, ref "form" is defined
        dialog.vm.$refs.form["validate"] = () => new Promise<boolean>(() => true)
        dialog.vm["selectedExternalIdentifier"] = undefined
        dialog.vm["selectedExternalIdentifierMutated"] = identifier

        const spyUpdate = jest.spyOn(OxApiManager, "updateJsonApiObject").mockImplementation()
        const spyCreate = jest.spyOn(OxApiManager, "createJsonApiObjects").mockImplementation()
        const spyAddInfo = jest.spyOn(OxNotifyManager, "addInfo")

        await dialog.vm["save"]()

        expect(spyUpdate).not.toHaveBeenCalled()
        expect(spyCreate).toHaveBeenCalledWith(identifier, "api/identifiers/idex")
        expect(spyAddInfo).toHaveBeenCalledTimes(1)
        expect(dialog.emitted("dataSaved")).toBeDefined()
    })
    test("update identifier", async () => {
        const dialog = shallowMount(
            ExternalIdentifierDialog, {
                propsData: { object },
                localVue,
                pinia
            }
        )

        const identifier = new ExternalIdentifier()
        identifier.id = "3"

        const identifierMutated = cloneObject(identifier)
        identifierMutated.id400 = "lorem"
        identifierMutated.tag = "ipsum"
        identifierMutated.context = object

        // @ts-ignore
        dialog.vm.$refs.form["validate"] = () => new Promise<boolean>(() => true)
        dialog.vm["selectedExternalIdentifier"] = identifier
        dialog.vm["selectedExternalIdentifierMutated"] = identifierMutated

        const spyUpdate = jest.spyOn(OxApiManager, "updateJsonApiObject").mockImplementation()
        const spyCreate = jest.spyOn(OxApiManager, "createJsonApiObjects").mockImplementation()
        const spyAddInfo = jest.spyOn(OxNotifyManager, "addInfo")

        await dialog.vm["save"]()

        expect(spyUpdate).toHaveBeenCalledWith(identifier, identifierMutated)
        expect(spyCreate).not.toHaveBeenCalled()
        expect(spyAddInfo).toHaveBeenCalledTimes(1)
        expect(dialog.emitted("dataSaved")).toBeDefined()
    })
})
