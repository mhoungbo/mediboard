/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import OxObject from "@/core/models/OxObject"
import ExternalIdentifier from "@modules/dPsante400/vue/models/ExternalIdentifier"
import * as OxNotifyManager from "@/core/utils/OxNotifyManager"
import * as OxApiManager from "@/core/utils/OxApiManager"
import AddonIdex from "@modules/dPsante400/vue/components/AddonIdex/AddonIdex.vue"
import pinia from "@/core/plugins/OxPiniaCore"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */
let object = new OxObject()

describe("AddonIdex", () => {
    beforeEach(() => {
        object = new OxObject()
        object.id = "1"
        object.type = "test_object"
    })
    afterEach(() => {
        jest.clearAllMocks()
    })

    test("open dialog works", () => {
        const idex = shallowMount(AddonIdex, {
            propsData: { object },
            stubs: {
                OxAlert: true,
                ExternalIdentifierDialog: true
            },
            localVue,
            pinia
        })

        idex.vm["openDialog"]()

        expect(idex.vm["showDialog"]).toBe(true)
        expect(idex.vm["selectedIdentifier"]).toBeUndefined()
    })
    test("open modify dialog works", () => {
        const idex = shallowMount(AddonIdex, {
            propsData: { object },
            stubs: {
                OxAlert: true,
                ExternalIdentifierDialog: true
            },
            localVue,
            pinia
        })
        const identifier = new ExternalIdentifier()
        identifier.id = "2"

        idex.vm["modify"](identifier)

        expect(idex.vm["showDialog"]).toBe(true)
        expect(idex.vm["selectedIdentifier"]).toEqual(identifier)
    })
    test("show tooltip confirmation on delete", () => {
        const idex = shallowMount(AddonIdex, {
            propsData: { object },
            stubs: {
                OxAlert: true,
                ExternalIdentifierDialog: true
            },
            localVue,
            pinia
        })
        const identifier = new ExternalIdentifier()
        identifier.id = "3"

        idex.vm["askDelete"](identifier)

        expect(idex.vm["showModal"]).toBe(true)
        expect(idex.vm["selectedIdentifier"]).toEqual(identifier)
    })
    test("reset async tooltip", () => {
        const idex = shallowMount(AddonIdex, {
            propsData: { object },
            stubs: {
                OxAlert: true,
                ExternalIdentifierDialog: true
            },
            localVue,
            pinia
        })
        idex.vm["loadedAsyncTooltip"] = true

        idex.vm["resetAsyncTooltip"]()

        expect(idex.vm["loadedAsyncTooltip"]).toBe(false)
    })
    test("detects if dialog or modal is open", () => {
        const idex = shallowMount(AddonIdex, {
            propsData: { object },
            stubs: {
                OxAlert: true,
                ExternalIdentifierDialog: true
            },
            localVue,
            pinia
        })

        const expectDefaultFalse = idex.vm["isDialogOrModalOpened"]
        idex.vm["showModal"] = true
        const expectModalTrue = idex.vm["isDialogOrModalOpened"]
        idex.vm["showDialog"] = true
        const expectDialogModalTrue = idex.vm["isDialogOrModalOpened"]
        idex.vm["showModal"] = false
        const expectDialogTrue = idex.vm["isDialogOrModalOpened"]
        idex.vm["showDialog"] = false
        const expectFalse = idex.vm["isDialogOrModalOpened"]

        expect(expectDefaultFalse).toBe(false)
        expect(expectModalTrue).toBe(true)
        expect(expectDialogModalTrue).toBe(true)
        expect(expectDialogTrue).toBe(true)
        expect(expectFalse).toBe(false)
    })
    test("delete idex works", async () => {
        const idex = shallowMount(AddonIdex, {
            propsData: { object },
            localVue,
            stubs: {
                OxAlert: true,
                ExternalIdentifierDialog: true
            },
            pinia
        })
        const identifier = new ExternalIdentifier()
        identifier.id = "4"
        idex.vm["selectedIdentifier"] = identifier
        const spyDelete = jest.spyOn(OxApiManager, "deleteJsonApiObject").mockImplementation()
        const spyAddInfo = jest.spyOn(OxNotifyManager, "addInfo")
        // @ts-ignore
        const spyResetAsyncTooltip = jest.spyOn(idex.vm, "resetAsyncTooltip")

        await idex.vm["deleteIdentifier"]()

        expect(spyDelete).toHaveBeenCalledWith(identifier)
        expect(spyAddInfo).toHaveBeenCalledTimes(1)
        expect(spyResetAsyncTooltip).toHaveBeenCalledTimes(1)
    })
    test("url is empty string if not define", () => {
        const idex = shallowMount(AddonIdex, {
            propsData: { object },
            localVue,
            stubs: {
                OxAlert: true,
                ExternalIdentifierDialog: true
            },
            pinia
        })
        expect(idex.vm["externalIdentifierUrl"]).toBe("")
    })
    test("url is get from links", () => {
        object.links.identifiers = "url"
        const idex = shallowMount(AddonIdex, {
            propsData: { object },
            localVue,
            stubs: {
                OxAlert: true,
                ExternalIdentifierDialog: true
            },
            pinia
        })
        expect(idex.vm["externalIdentifierUrl"]).toBe("url")
    })
})
