export const ExternalIdentifiersApiMock = {
    data: [
        {
            type: "idex",
            id: "2",
            attributes: {
                id400: "ipsum",
                tag: "dolor",
                datetime_create: "2023-04-19 09:57:15",
                last_update: "2023-04-19 09:57:15",
                idex_type: null
            },
            links: {
                self: "/api/identifiers/idex/1777",
                schema: "/api/schemas/idex",
                history: "/api/history/idex/1777"
            }
        },
        {
            type: "idex",
            id: "1",
            attributes: {
                id400: "Lorem",
                tag: null,
                datetime_create: "2023-04-19 09:57:45",
                last_update: "2023-04-19 09:58:30",
                idex_type: "IPP"
            },
            links: {
                self: "/api/identifiers/idex/1776",
                schema: "/api/schemas/idex",
                history: "/api/history/idex/1776"
            }
        }
    ],
    meta: {
        date: "2023-04-19 09:57:37+02:00",
        copyright: "OpenXtrem-2023",
        authors: "dev@openxtrem.com",
        count: 2
    }
}
export const ExternalIdentifiersSchemaApiMock = {
    data: [
        {
            type: "schema",
            id: "b576b04f612e4941163ac78a3393311c",
            attributes: {
                owner: "idex",
                field: "id400",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                helped: null,
                length: null,
                minLength: null,
                maxLength: "80",
                libelle: "Identifiant",
                label: "Identifiant",
                description: "Identifiant Externe de l'objet"
            }
        },
        {
            type: "schema",
            id: "90bdf6b6a4b5eca8edb9496ae986df35",
            attributes: {
                owner: "idex",
                field: "tag",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                helped: null,
                length: null,
                minLength: null,
                maxLength: "80",
                libelle: "Etiquette",
                label: "Tag",
                description: "Etiquette (s�mantique) de l'identifiant"
            }
        },
        {
            type: "schema",
            id: "178a988f5da7f13d011da67e8a016eec",
            attributes: {
                owner: "idex",
                field: "datetime_create",
                type: "dateTime",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                helped: null,
                hide_date: null,
                libelle: "Cr�ation",
                label: "Cr�ation",
                description: "Date de cr�ation"
            }
        },
        {
            type: "schema",
            id: "86d44774070d6c5d2abfce06e1c4ef01",
            attributes: {
                owner: "idex",
                field: "last_update",
                type: "dateTime",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                helped: null,
                hide_date: null,
                libelle: "Mise � jour",
                label: "Maj",
                description: "Date et heure de la derni�re mise � jour par synchronisation"
            }
        }
    ],
    meta: {
        date: "2023-04-19 09:59:59+02:00",
        copyright: "OpenXtrem-2023",
        authors: "dev@openxtrem.com",
        count: 4
    }
}
