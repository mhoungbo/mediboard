/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import ExternalIdentifier from "@modules/dPsante400/vue/models/ExternalIdentifier"

describe("ExternalIdentifier createdDate", () => {
    test("is in good format", () => {
        const idex = new ExternalIdentifier()
        idex.dateTimeCreate = "2023-05-15 11:25:00"
        expect(idex.createdDate).toBe("15/05/2023 11:25")
    })
    test("return undefined if no data", () => {
        const idex = new ExternalIdentifier()
        expect(idex.createdDate).toBeUndefined()
    })
})

describe("ExternalIdentifier lastUpdatedDate", () => {
    test("is in good format", () => {
        const idex = new ExternalIdentifier()
        idex.dateTimeCreate = "2023-05-16 11:25:00"
        expect(idex.createdDate).toBe("16/05/2023 11:25")
    })
    test("return undefined if no data", () => {
        const idex = new ExternalIdentifier()
        expect(idex.createdDate).toBeUndefined()
    })
})
