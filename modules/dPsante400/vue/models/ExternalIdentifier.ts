/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr, OxAttrNullable } from "@/core/types/OxObjectTypes"
import OxMoment from "@/core/utils/OxMoment"

export default class ExternalIdentifier extends OxObject {
    constructor () {
        super()
        this.type = "idex"
    }

    protected _relationsTypes = {
        [ExternalIdentifier.RELATIONS_TYPE_OTHERS]: OxObject
    }

    get id400 (): OxAttr<string> {
        return super.get("id400")
    }

    set id400 (value: OxAttr<string>) {
        super.set("id400", value)
    }

    get tag (): OxAttrNullable<string> {
        return super.get("tag")
    }

    set tag (value: OxAttrNullable<string>) {
        super.set("tag", value)
    }

    get idexType (): OxAttrNullable<string> {
        return super.get("idex_type")
    }

    set idexType (value: OxAttrNullable<string>) {
        super.set("idex_type", value)
    }

    get dateTimeCreate (): OxAttr<string> {
        return super.get("datetime_create")
    }

    set dateTimeCreate (value: OxAttr<string>) {
        super.set("datetime_create", value)
    }

    get lastUpdate (): OxAttrNullable<string> {
        return super.get("last_update")
    }

    set lastUpdate (value: OxAttrNullable<string>) {
        super.set("last_update", value)
    }

    get context (): OxObject | undefined {
        return this.loadForwardRelation<OxObject>("context")
    }

    set context (value: OxObject | undefined) {
        this.setForwardRelation("context", value)
    }

    /**
     * Returns idex creation date in DD/MM/YYYY HH:mm format
     */
    get createdDate (): OxAttr<string> {
        return this.dateTimeCreate === undefined ? this.dateTimeCreate : new OxMoment(this.dateTimeCreate).format("DD/MM/YYYY HH:mm")
    }

    /**
     * Returns idex last update date in DD/MM/YYYY HH:mm format
     */
    get lastUpdatedDate (): OxAttr<string> {
        return this.lastUpdate === undefined ? this.lastUpdate : new OxMoment(this.lastUpdate).format("DD/MM/YYYY HH:mm")
    }
}
