<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Exceptions\Repositories;

use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Mediboard\PlanningOp\Repositories\SejourRepository;

/**
 * Exception class for SejourRepository
 * @see SejourRepository
 */
class SejourRepositoryException extends CMbException
{
    public static function idCantBeNull(CMbObject $object): self
    {
        return new self('SejourRepositoryException-msg-Id cant be null for object: %s.', $object->_class);
    }
}
