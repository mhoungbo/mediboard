<?php

/**
 * @package Mediboard\PlanningOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Controllers;

use Exception;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Controller;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\PlanningOp\CModeSortieSejour;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CSejoursController
 */
class CModeSortieSejourController extends Controller
{
    /**
     * @param RequestApi $request_api
     *
     * @return Response
     * @throws Exception
     * @api
     */
    public function list(RequestApi $request_api): Response
    {
        $mode = new CModeSortieSejour();

        $ds = $mode->getDS();

        $where = [
            'mode_sortie_sejour.actif'    => "= '1'",
            'mode_sortie_sejour.group_id' => $ds->prepare("= ?", CGroups::loadCurrent()->_id),
        ];

        if ($group_filter = $request_api->getRequestFilter()->getFilter("group_id")) {
            $where["mode_sortie_sejour.group_id"] = $ds->prepare("= ?", $group_filter->getValue());
        }

        if ($active_filter = $request_api->getRequestFilter()->getFilter("actif")) {
            $where["mode_sortie_sejour.actif"] = $ds->prepare("= ?", $active_filter->getValue());
        }

        $modes = $mode->loadList(
            $where,
            $request_api->getSortAsSql("mode_sortie_sejour.libelle ASC"),
            $request_api->getLimitAsSql()
        );

        $collection = Collection::createFromRequest($request_api, $modes);

        return $this->renderApiResponse($collection);
    }
}
