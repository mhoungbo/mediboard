<?php

/**
 * @package Mediboard\PlanningOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Controllers;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Exception\HttpException;
use Ox\Core\Kernel\Exception\PermissionException;
use Ox\Mediboard\Bloc\CPlageOp;
use Ox\Mediboard\Ccam\CCodageCCAM;
use Ox\Mediboard\CompteRendu\CTemplateManager;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\FieldsSIH;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class COperationsController
 */
class COperationsController extends Controller
{
    use FieldsSIH;

    /**
     * @param RequestApi $request_api
     * @param CPlageOp    $plage
     *
     * @return Response
     * @throws \Ox\Core\Api\Exceptions\ApiException
     * @api
     */
    public function listOperationsForPlage(RequestApi $request_api, CPlageOp $plage): Response
    {
        $operations = $plage->loadRefsOperations(false);

        $resource = Collection::createFromRequest($request_api, $operations);

        return $this->renderApiResponse($resource);
    }

    /**
     * @param RequestApi $request_api
     *
     * @return Response
     * @api
     */
    public function getFields(RequestApi $request_api): Response
    {
        $operation = COperation::findOrNew($request_api->getRequest()->get('operation_id'));

        $template = new CTemplateManager();

        if (!$operation->_id) {
            $template->valueMode = false;
        }

        $operation->fillTemplate($template);

        $fields = $this->computeFields($template->sections);

        return $this->renderJsonResponse(json_encode($fields));
    }


    /**
     * List codes (CCAM, NGAP, LPP) from operation with specified pract executionner
     *
     * @param RequestApi $request_api
     * @param COperation $operation
     * @param string     $rpps
     *
     * @return Response
     * @throws ApiException
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     * @throws Exception
     * @api
     */
    public function listOperationCcamsForPract(RequestApi $request_api, COperation $operation, string $rpps): Response
    {
        /** @var CMediusers $user */
        $user       = CMediusers::loadFromRPPS($rpps);
        $acts       = [];
        $all_locked = null;

        // Check perms
        if (!$user->_id) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                $this->translator->tr("common-error-Object %s not found", $user::class)
            );
        } elseif (!$user->getPerm(PERM_READ)) {
            throw new PermissionException(Response::HTTP_FORBIDDEN, $this->translator->tr('access-forbidden'));
        }

        // Load acts from ccam codings
        $operation->loadRefsCodagesCCAM();

        if (isset($operation->_ref_codages_ccam[$user->_id])) {
            /** @var CCodageCCAM[] $ccams_coding */
            $ccams_coding = $operation->_ref_codages_ccam[$user->_id];

            CStoredObject::massLoadFwdRef($ccams_coding, "codable_id");

            $all_locked = true;
            foreach ($ccams_coding as $coding) {
                if (!$coding->locked) {
                    $all_locked = false;
                }
                $coding->loadActesCCAM();
            }

            $acts = array_merge([], ...array_column($ccams_coding, '_ref_actes_ccam'));
            CStoredObject::filterByPerm($acts);
        }

        $collection = Collection::createFromRequest($request_api, $acts);

        // The meta makes sense only if there is an act
        if (count($acts)) {
            $collection->addMeta('all_codings_locked', $all_locked);
        }

        return $this->renderApiResponse($collection);
    }
}
