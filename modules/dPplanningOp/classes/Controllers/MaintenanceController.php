<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Controllers;

use Ox\Core\CApp;
use Ox\Core\Controller;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CUniteFonctionnelle;
use Ox\Mediboard\PlanningOp\Services\FunctionalUnitAllocator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class MaintenanceController extends Controller
{
    public function viewFunctionalUnitAllocation(): Response
    {
        $functional_units = CUniteFonctionnelle::loadForGroup(CGroups::loadCurrent(), 'medicale');

        return $this->renderSmarty('maintenance/functional_units_allocation', [
            'functional_units' => $functional_units,
        ]);
    }

    public function allocateFunctionalUnit(RequestParams $params): Response
    {
        CApp::setTimeLimit(600);

        if (!$this->isCsrfTokenValid('planning_op_maintenance_allocate_function_unit', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $uf_id = $params->post('functional_unit_id', 'ref class|CUniteFonctionnelle notNull');
        $force_allocation = boolval($params->post('force_allocation', 'bool default|0'));
        $deactivate_handlers = boolval($params->post('deactivate_handlers', 'bool default|0'));
        $raw_store = boolval($params->post('raw_store', 'bool default|0'));

        $unit = CUniteFonctionnelle::findOrNew($uf_id);
        $group = CGroups::loadCurrent();

        if (!($files = $params->getRequest()->files->get('formfile'))) {
            $this->addUiMsgWarning($this->translator->tr('common-error-No file found.'));

            return $this->renderEmptyResponse();
        }

        if (!($file = $files[0])) {
            return $this->renderEmptyResponse();
        }

        $handle = fopen($file->getPathname(), 'r');
        if (!$handle) {
            $this->addUiMsgError('common-error-File is not readable: %s', false, $file->getPathname());
            return $this->renderEmptyResponse();
        }

        $csv = new CCSVFile($handle);
        $sejour_ids = [];

        while ($line = $csv->readLine()) {
            if (is_array($line) && (is_numeric($line[0]))) {
                $sejour_ids[] = $line[0];
            }
        }

        $allocator = new FunctionalUnitAllocator($group, $unit);
        $allocator->setSejourIds($sejour_ids);

        if ($force_allocation) {
            $allocator->forceUfAllocation();
        }

        if ($deactivate_handlers) {
            $allocator->deactivateHandlers();
        }

        if ($raw_store) {
            $allocator->forceRawStore();
        }

        $report = $allocator->allocate();

        return $this->renderSmarty('maintenance/functional_units_allocation_result', [
            'report' => $report
        ]);
    }
}
