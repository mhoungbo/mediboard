<?php
/**
 * @package Mediboard\PlanningOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp;

use Exception;
use Ox\AppFine\Client\CAppFineClientOrder;
use Ox\Core\CAppUI;
use Ox\Core\CMbObject;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\Forms\CExObject;

/**
 * Classe CAppelSejour
 *
 * Appels de s�jour
 */
class CAppelSejour extends CMbObject
{
    public const RESOURCE_TYPE = "stay_call";

    /** @var string */
    public const TYPE_ADMISSION = 'admission';
    public const TYPE_SORTIE    = 'sortie';
    public const ETAT_REALISE   = 'realise';
    public const ETAT_ECHEC     = 'echec';

    // DB Table key
    public $appel_id;

    // DB Table key
    public $sejour_id;
    public $user_id;
    public $datetime;
    public $type;
    public $etat;
    public $commentaire;

    public $_open_form = 0;
    /** @var bool */
    public $_form_sent_appfine_client = false;
    /** @var bool */
    public $_form_sent_appfine_waiting = false;

    public $_formula_field_result = 0;
    public $_form_alert = [];

    //Distant field
    public $_ref_user;
    /** @var CSejour */
    public $_ref_sejour;

    /**
     * @see parent::getSpec()
     */
    function getSpec()
    {
        $spec        = parent::getSpec();
        $spec->table = 'sejour_appel';
        $spec->key   = 'appel_id';

        $spec->events = [
            'appel'                => [
                'reference1' => ['CSejour', 'sejour_id'],
                'reference2' => ['CPatient', 'sejour_id.patient_id'],
            ],
            'appel_j_moins_1_auto' => [
                'auto'       => true,
                'reference1' => ['CSejour', 'sejour_id'],
                'reference2' => ['CPatient', 'sejour_id.patient_id'],
            ],
            'appel_j_plus_1_auto'  => [
                'auto'       => true,
                'reference1' => ['CSejour', 'sejour_id'],
                'reference2' => ['CPatient', 'sejour_id.patient_id'],
            ],
        ];

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    function getProps()
    {
        $props                = parent::getProps();
        $props["sejour_id"]   = "ref notNull class|CSejour back|appels";
        $props["user_id"]     = "ref notNull class|CMediusers back|appels_user";
        $props["datetime"]    = "dateTime notNull";
        $props["type"]        = "enum notNull list|admission|sortie default|admission";
        $props["etat"]        = "enum notNull list|realise|echec default|realise";
        $props["commentaire"] = "text helped";

        return $props;
    }

    /**
     * @inheritDoc
     */
    function store()
    {
        if ($msg = parent::store()) {
            return $msg;
        }

        // Lancement des formulaires automatiques lors de l'appel J-1 et J+1
        if ($this->_id && $this->_open_form && CModule::getActive("forms")) {
            $event_name = $this->type == 'admission' ? "appel_j_moins_1_auto" : "appel_j_plus_1_auto";

            CAppUI::callbackAjax("Appel.afterTriggerFormsAppel", $this->_guid, $event_name);
        }

        return null;
    }

    /**
     * Load the user
     *
     * @return CMediusers The user object
     */
    function loadRefUser()
    {
        return $this->_ref_user = $this->loadFwdRef("user_id");
    }

    /**
     * Load sejour
     *
     * @return CSejour
     */
    function loadRefSejour()
    {
        return $this->_ref_sejour = $this->loadFwdRef("sejour_id", true);
    }

    /**
     * Check if the form sent by appFine
     *
     * @return bool|null
     * @throws Exception
     */
    public function checkFormSentByAppFine(): ?bool
    {
        if (!CModule::getActive("appFineClient")) {
            return null;
        }

        $sejour = $this->loadRefSejour();

        if (!$sejour->_id) {
            return null;
        }

        switch ($this->type) {
            case self::TYPE_ADMISSION:
                $form_type = CAppFineClientOrder::FORM_TYPE__DAY_BEFORE_CALL;
                break;
            case self::TYPE_SORTIE:
                $form_type = CAppFineClientOrder::FORM_TYPE__DAY_AFTER_CALL;
                break;
            default:
                $form_type = null;
                break;
        }

        $order_items = $sejour->_ref_orders_item ?: $sejour->loadRefsOrdersItem();

        foreach ($order_items as $_order_item) {
            // Permet de savoir si on a une autre question en attente
            if ( $_order_item->sent_datetime && $_order_item->sent_datetime != $this->datetime && !$_order_item->received_datetime) {
                $this->_form_sent_appfine_waiting = true;
            }
        }

        foreach ($order_items as $_order_item_id => $_order_item) {
            if ($_order_item->sent_datetime != $this->datetime) {
                unset($order_items[$_order_item_id]);
                continue;
            }
            if ($form_type) {
                $order = $_order_item->loadRefOrder();

                if ($order->form_type != $form_type) {
                    unset($order_items[$_order_item_id]);
                }
            }
        }

        return $this->_form_sent_appfine_client = (count($order_items) > 0) ? true : false;
    }

    /**
     * Get the alerts form
     *
     * @return array|null
     * @throws Exception
     */
    public function getAlertsForm(): ?array
    {
        if (!CModule::getActive("appFineClient") || !$this->_form_sent_appfine_client) {
            return null;
        }

        $sejour = $this->loadRefSejour();

        if (!$sejour->_id) {
            return null;
        }

        if (!$this->_ref_forms) {
            $this->loadRefsForms();
        }

        $last_alert = [];

        // last form
        $last_ex_link                = end($this->_ref_forms);

        if ($last_ex_link && $last_ex_link->_id) {
            $_ex                         = $last_ex_link->loadRefExObject();
            $ex_class                    = $_ex->loadRefExClass();
            $_formula_field              = $ex_class->getFormulaField();

            if ($_formula_field) {
                $this->_formula_field_result = $_ex->$_formula_field;
            }

            $alerts     = CExObject::getThresholdAlerts([$sejour]);

            if ($alerts && isset($alerts[$sejour->_id][$ex_class->_id])) {
                $last_alert = end($alerts[$sejour->_id][$ex_class->_id]);
            }
        }

        return $this->_form_alert = $last_alert;
    }
}
