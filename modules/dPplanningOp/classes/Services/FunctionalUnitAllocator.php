<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Services;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\Handlers\Facades\HandlerManager;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\Hospi\CUniteFonctionnelle;
use Ox\Mediboard\PlanningOp\CSejour;

class FunctionalUnitAllocator
{
    protected array $sejour_ids = [];

    protected FunctionalUnitAllocationReport $report;

    /* If true, the given uf will be allocated even to sejour that already have an uf */
    protected bool $force_uf_allocation = false;

    protected bool $raw_store = false;

    /* If true, all the object handlers and the interop modification will be deactivated during the execution */
    protected bool $deactivate_handlers = false;

    public function __construct(
        protected readonly CGroups $group,
        protected readonly CUniteFonctionnelle $ufm,
    ) {
    }

    /**
     * @param array $ids
     *
     * @return void
     */
    public function setSejourIds(array $ids): void
    {
        $this->sejour_ids = $ids;
    }

    /**
     * Sets a flag that will force the uf allocation on sejours that already have an uf
     *
     * @return void
     */
    public function forceUfAllocation(): void
    {
        $this->force_uf_allocation = true;
    }

    /**
     * Sets a flag that will deactivate the objects handers during the allocation
     *
     * @return void
     */
    public function deactivateHandlers(): void
    {
        $this->deactivate_handlers = true;
    }

    public function forceRawStore(): void
    {
        $this->raw_store = true;
    }

    /**
     * The main process for allocating the functional units to the given sejours
     *
     * @return FunctionalUnitAllocationReport
     */
    public function allocate(): FunctionalUnitAllocationReport
    {
        $this->report = new FunctionalUnitAllocationReport();
        /* Checks if the functional unit is linked to the given group, is of the right type */
        if (!$this->doesFunctionalUnitBelongsToGroup()) {
            $this->report->setFatalError('FunctionalUnitAllocator-error-uf_does_not_belong_in_current_group');

            return $this->report;
        }

        if (!$this->isFunctionalUnitMedical()) {
            $this->report->setFatalError('FunctionalUnitAllocator-error-wong_functional_unit_type');

            return $this->report;
        }

        if ($this->deactivate_handlers) {
            HandlerManager::disableObjectHandlers();
        }

        CSQLDataSource::$log = false;
        /* Divide the sejour ids into chunks of 500, for performance purpose */
        try {
            foreach (array_chunk($this->sejour_ids, 250) as $chunk) {
                $sejours = $this->loadSejours($chunk);
                $this->prepareSejoursCollection($sejours);

                foreach ($sejours as $sejour) {
                    $this->allocateUfToSejour($sejour);
                }
            }
        } catch (Exception $e) {
            $this->report->setFatalError($e->getMessage());
        }

        CSQLDataSource::$log = true;

        return $this->report;
    }

    /**
     * Allocate the functional unit to the given CSejour, and report the result of the allocation
     *
     * @param CSejour $sejour
     *
     * @return void
     */
    protected function allocateUfToSejour(CSejour $sejour): void
    {
        if (!$this->doesSejourBelongsToGroup($sejour)) {
            $this->report->addSejourOutsideGroup($sejour);
            return;
        }

        try {
            if (
                $sejour->type !== $this->ufm->type_sejour
                 && !is_null($this->ufm->type_sejour) && $this->ufm->type_sejour === ''
            ) {
                $this->report->addIncompatibleSejourTypeError($sejour);
                return;
            } elseif ($sejour->uf_medicale_id && !$this->force_uf_allocation) {
                $this->report->addUfAlreadySetError($sejour);
                return;
            } else {
                $sejour->uf_medicale_id = $this->ufm->_id;
                switch ($this->raw_store) {
                    case true:
                        if (!$sejour->rawStore()) {
                            $this->report->addError($sejour);
                            return;
                        }
                        break;
                    case false:
                    default:
                        if ($error = $sejour->store()) {
                            $this->report->addError($sejour, $error);
                            return;
                        }
                }
            }

            foreach ($sejour->loadRefsAffectations() as $affectation) {
                $this->allocateUfToAffectation($affectation);
            }

            $this->report->increaseModifiedCounter();
        } catch (Exception $e) {
            $this->report->addError($sejour, $e->getMessage());
            return;
        }
    }

    /**
     * Allocate the functional unit to the given affectation
     *
     * @param CAffectation $affectation
     *
     * @return void
     */
    protected function allocateUfToAffectation(CAffectation $affectation): void
    {
        if (!$affectation->uf_medicale_id) {
            $affectation->uf_medicale_id = $this->ufm->_id;
            $this->raw_store ? $affectation->rawStore() : $affectation->store();
        }
    }

    /**
     * @param CSejour[] $ids
     *
     * @return CSejour[]
     */
    protected function loadSejours(array $ids): array
    {
        try {
            return (new CSejour())->loadList(['sejour_id' => CSQLDataSource::prepareIn($ids)]);
        } catch (Exception) {
            return [];
        }
    }

    /**
     * Make the necessary massloads for the collection of sejours
     *
     * @param CSejour[] $sejours
     *
     * @return void
     * @throws Exception
     */
    protected function prepareSejoursCollection(array $sejours): void
    {
        CSejour::massLoadBackRefs($sejours, 'affectations');
        CSejour::massLoadFwdRef($sejours, 'patient_id');
    }

    /**
     * Checks if the CUniteFonctionnel is linked to the CGroups object
     *
     * @return bool
     */
    protected function doesFunctionalUnitBelongsToGroup(): bool
    {
        return $this->ufm->group_id === $this->group->_id;
    }

    /**
     * Checks that the functional unit is of type "medical"
     *
     * @return bool
     */
    protected function isFunctionalUnitMedical(): bool
    {
        return $this->ufm->type === 'medicale';
    }

    /**
     * Checks if the given CSejour is linked to the CGroups object
     *
     * @return bool
     */
    protected function doesSejourBelongsToGroup(CSejour $sejour): bool
    {
        return $sejour->group_id === $this->group->_id;
    }
}
