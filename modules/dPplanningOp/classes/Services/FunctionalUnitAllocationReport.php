<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Services;

use Ox\Mediboard\PlanningOp\CSejour;

class FunctionalUnitAllocationReport
{
    /** @var bool Indicate that the process has encouter an error that prevented its execution */
    public bool $fatal_error = false;
    /** @var string|null Contains an optional message for the fatal error */
    public ?string $fatal_error_message = null;
    /** @var int A counter for the number of successfully modified sejours */
    public int $modified = 0;
    /** @var array A list of unmodified sejours (mostly because the medical functional unit was already set) */
    public array $uf_already_set = [];
    /** @var array A list of unmodified sejours (mostly because the medical functional unit was already set) */
    public array $incompatible_sejour_type = [];
    /** @var array A list of sejours that are outside of the current group */
    public array $outside_group = [];
    /** @var array A list of sejours for which there was an error (a store error or an exception). */
    public array $errors = [];

    /**
     * @return void
     */
    public function increaseModifiedCounter(): void
    {
        $this->modified++;
    }

    /**
     * @param CSejour $sejour
     *
     * @return void
     */
    public function addSejourOutsideGroup(CSejour $sejour): void
    {
        $this->outside_group[] = $sejour->_id;
    }

    /**
     * @param CSejour $sejour
     *
     * @return void
     */
    public function addUfAlreadySetError(CSejour $sejour): void
    {
        $this->uf_already_set[] = $sejour->_id;
    }

    /**
     * @param CSejour $sejour
     *
     * @return void
     */
    public function addIncompatibleSejourTypeError(CSejour $sejour): void
    {
        $this->incompatible_sejour_type[] = $sejour->_id;
    }

    /**
     * @param CSejour $sejour
     * @param string  $error
     *
     * @return void
     */
    public function addError(CSejour $sejour, string $error = ''): void
    {
        $this->errors[$sejour->_id] = $error;
    }

    public function setFatalError(string $message): void
    {
        $this->fatal_error = true;
        $this->fatal_error_message = $message;
    }
}
