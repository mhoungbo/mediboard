<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Repositories;

use Exception;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\PlanningOp\Exceptions\Repositories\SejourRepositoryException;

/**
 * Repository of CSejour.
**/
class SejourRepository extends AbstractRequestApiRepository
{
    /**
     * Add condition 'patient_id' for request.
     *
     * @throws SejourRepositoryException
     */
    public function withPatient(CPatient $patient): self
    {
        if (!$patient->_id) {
            throw SejourRepositoryException::idCantBeNull($patient);
        }

        $this->where['sejour.patient_id'] = $this->object->getDS()->prepare('= ?', $patient->_id);

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function getObjectInstance(): CStoredObject
    {
        return new CSejour();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    protected function massLoadRelation(array $objects, string $relation): void
    {
        switch ($relation) {
            case RequestRelations::QUERY_KEYWORD_ALL:
                $this->massLoadPract($objects);
                break;
            case CSejour::RELATION_PRATICIEN:
                $this->massLoadPract($objects);
                break;
            default:
                // Do nothing.
        }
    }

    /**
     * MassLoad patient.
     *
     * @throws Exception
     */
    private function massLoadPract(array $objects): void
    {
        CStoredObject::massLoadFwdRef($objects, 'praticien_id');
    }

}
