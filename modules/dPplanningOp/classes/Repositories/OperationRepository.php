<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Repositories;

use Exception;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\Exceptions\Repositories\OperationRepositoryException;

/**
 * Repository of COperation.
 **/
class OperationRepository extends AbstractRequestApiRepository
{
    /** @var array */
    private $ljoin = [];

    /**
     * Add condition 'patient_id' for request.
     *
     * @throws OperationRepositoryException
     */
    public function withPatient(CPatient $patient): self
    {
        if (!$patient->_id) {
            throw OperationRepositoryException::idCantBeNull($patient);
        }

        $this->ljoin['sejour']            = 'operations.sejour_id = sejour.sejour_id';
        $this->where['sejour.patient_id'] = $this->object->getDS()->prepare('= ?', $patient->_id);

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function getObjectInstance(): CStoredObject
    {
        return new COperation();
    }

    /**
     * Load a list of $object using the repository parameters.
     *
     * @throws Exception
     */
    public function find(): array
    {
        return $this->seek_keywords
            ? $this->object->seek($this->seek_keywords, $this->where, $this->limit, true, null, $this->order)
            : $this->object->loadList($this->where, $this->order, $this->limit, null, $this->ljoin);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    protected function massLoadRelation(array $objects, string $relation): void
    {
        switch ($relation) {
            case RequestRelations::QUERY_KEYWORD_ALL:
                $this->massLoadPract($objects);
                break;
            case COperation::RELATION_PRATICIEN:
                $this->massLoadPract($objects);
                break;
            default:
                // Do nothing.
        }
    }

    /**
     * MassLoad patient.
     *
     * @throws Exception
     */
    private function massLoadPract(array $objects): void
    {
        CStoredObject::massLoadFwdRef($objects, 'chir_id');
    }

}
