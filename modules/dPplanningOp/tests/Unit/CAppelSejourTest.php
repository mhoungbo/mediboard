<?php

/**
 * @package Mediboard\Patient\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Tests\Unit;

use Exception;
use Ox\Core\Module\CModule;
use Ox\Mediboard\PlanningOp\CAppelSejour;
use Ox\Mediboard\PlanningOp\Tests\Fixtures\AppelSejourFixtures;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

class CAppelSejourTest extends OxUnitTestCase
{
    /**
     * Test to check if the form sent by appFine (without order item)
     * @throws TestsException
     */
    public function testCheckFormSentByAppFineWithoutOrderItem(): void
    {
        $appel = $this->getAppelSejour(AppelSejourFixtures::APPEL_SEJOUR_ADMISSION_NO_ORDER_ITEM);

        $this->assertFalse($appel->checkFormSentByAppFine());
    }

    /**
     * Test to check if the form sent by appFine
     * @throws TestsException
     */
    public function testCheckFormSentByAppFine(): void
    {
        $appel = $this->getAppelSejour(AppelSejourFixtures::APPEL_SEJOUR_ADMISSION);

        $this->assertTrue($appel->checkFormSentByAppFine());
    }

    /**
     * Test to load CSejour object without sejour ID
     * @throws TestsException
     */
    public function testCheckFormSentByAppFineWithoutSejourID(): void
    {
        $appel = $this->getAppelSejour(AppelSejourFixtures::APPEL_SEJOUR_ADMISSION);
        $appel->sejour_id = null;

        $this->assertNull($appel->checkFormSentByAppFine());
    }


    /**
     * Test to get the alerts form
     * @throws TestsException
     * @throws Exception
     */
    public function testGetAlertsFormWithForm(): void
    {
        $appel = $this->getAppelSejour(AppelSejourFixtures::APPEL_SEJOUR_ADMISSION);
        $appel->_form_sent_appfine_client = true;
        $appel->getAlertsForm();

        $this->assertEmpty($appel->_form_alert);
    }

    /**
     * Test to check if the form sent by appFine with the appFineClient module inactive
     * @throws TestsException
     */
    public function testCheckFormSentByAppFineWithAppFineClientInactive(): void
    {
        $mod_appfine_client                = CModule::$active['appFineClient'];
        CModule::$active['appFineClient'] = null;

        $appel = $this->getAppelSejour(AppelSejourFixtures::APPEL_SEJOUR_ADMISSION);
        $appel->sejour_id = null;

        $this->assertNull($appel->checkFormSentByAppFine());

        CModule::$active['appFineClient'] = $mod_appfine_client;
    }

    /**
     * Test to get the alerts form (without form)
     * @throws TestsException
     * @throws Exception
     */
    public function testGetAlertsFormWithoutForm(): void
    {
        $appel = $this->getAppelSejour(AppelSejourFixtures::APPEL_SEJOUR_SORTIE);

        $this->assertNull($appel->getAlertsForm());
    }

    /**
     * Get CAppelSejour object
     * @throws TestsException
     */
    private function getAppelSejour(string $tag): CAppelSejour
    {
       return $this->getObjectFromFixturesReference(
            CAppelSejour::class,
            $tag
        );
    }
}
