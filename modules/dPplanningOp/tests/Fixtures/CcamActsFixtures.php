<?php

/**
 * @author  SAS XtremSante <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Tests\Fixtures;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Ccam\CCodageCCAM;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\SalleOp\CActeCCAM;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Fixture for ccam acts
 */
class CcamActsFixtures extends Fixtures implements GroupFixturesInterface
{
    public const TAG_OPERATION   = 'ccam_acts_fixtures-operation';
    public const TAG_CHIR        = 'ccam_acts_fixtures-chir';
    public const TAG_CCAM_CODAGE = 'ccam_acts_fixtures-ccam_codage';
    public const TAG_CCAM_ACT    = 'ccam_acts_fixtures-ccam_act';

    public const RPPS = 10100569002;

    private CMediusers $user;
    private CPatient   $patient;

    public static function getGroup(): array
    {
        return ['ccam_acts_fixtures'];
    }

    /**
     * @inheritDoc
     * @throws CModelObjectException
     * @throws InvalidArgumentException
     * @throws FixturesException
     */
    public function load(): void
    {
        $this->user    = $this->generateUser(self::TAG_CHIR);
        $this->patient = $this->generatePatient();
        $sejour        = $this->generateSejour();
        $operation     = $this->generateOperation($sejour, self::TAG_OPERATION);
        $this->generateCcamCoding($operation, self::TAG_CCAM_CODAGE);
        $this->generateCcamAct($operation, self::TAG_CCAM_ACT);
    }

    /**
     * @throws CModelObjectException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    private function generateUser(?string $tag = null): CMediusers
    {
        /** @var CMediusers $user */
        $user              = CMediusers::getSampleObject();
        $user->function_id = CFunctions::getCurrent()->_id;
        $user->_user_type  = array_search('Chirurgien', CUser::$types);
        $user->rpps        = self::RPPS;
        $this->store($user, $tag);

        return $user;
    }

    /**
     * @param string|null $tag
     *
     * @return CSejour
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function generateSejour(?string $tag = null): CSejour
    {
        /** @var CSejour $sejour */
        $sejour               = CSejour::getSampleObject();
        $sejour->patient_id   = $this->patient->_id;
        $sejour->praticien_id = $this->user->_id;
        $sejour->group_id     = CGroups::loadCurrent()->_id;
        $sejour->entree       = $sejour->entree_reelle = CMbDT::dateTime('-7 DAYS');
        $sejour->sortie       = $sejour->sortie_reelle = CMbDT::dateTime('+7 DAYS');
        $this->store($sejour, $tag);

        return $sejour;
    }

    /**
     * @param CSejour     $sejour
     * @param string|null $tag
     *
     * @return COperation
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function generateOperation(CSejour $sejour, ?string $tag = null): COperation
    {
        $operation                 = COperation::getSampleObject();
        $operation->sejour_id      = $sejour->_id;
        $operation->chir_id        = $this->user->_id;
        $operation->time_operation = CMbDT::time();
        $operation->date           = CMbDT::date();
        $operation->codes_ccam     = "AHPC001";

        $this->store($operation, $tag);

        return $operation;
    }

    /**
     * @param string|null $tag
     *
     * @return CPatient
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function generatePatient(?string $tag = null): CPatient
    {
        /** @var CPatient $patient */
        $patient = CPatient::getSampleObject();

        $this->store($patient, $tag);

        return $patient;
    }

    /**
     * @param COperation  $operation
     * @param string|null $tag
     *
     * @return void
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function generateCcamAct(COperation $operation, ?string $tag = null): void
    {
        $act                = CActeCCAM::getSampleObject();
        $act->object_id     = $operation->_id;
        $act->object_class  = $operation->_class;
        $act->executant_id  = $operation->chir_id;
        $act->execution     = CMbDT::dateTime($operation->date);
        $act->_permissive   = true;
        $act->code_activite = "1";
        $act->code_phase    = "0";
        $act->facturable    = 1;
        $act->ald           = 0;
        $act->lieu          = "C";
        $act->num_facture   = 1;
        $act->gratuit       = 0;
        $act->code_acte     = $operation->codes_ccam;

        $this->store($act, $tag);
    }

    /**
     * @param COperation  $operation
     * @param string|null $tag
     *
     * @return void
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function generateCcamCoding(COperation $operation, ?string $tag = null): void
    {
        $coding                = CCodageCCAM::getSampleObject();
        $coding->codable_class = $operation->_class;
        $coding->codable_id    = $operation->_id;
        $coding->date          = $operation->date;
        $coding->praticien_id  = $operation->chir_id;
        $coding->locked        = '0';

        $this->store($coding, $tag);
    }
}
