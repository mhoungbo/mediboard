<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Tests\Fixtures;

use Exception;
use Ox\AppFine\Client\CAppFineClientOrder;
use Ox\AppFine\Client\CAppFineClientOrderItem;
use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CAppelSejour;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\System\Forms\CExClass;
use Ox\Mediboard\System\Forms\CExLink;
use Ox\Mediboard\System\Forms\CExObject;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

/**
 * @description Datas used for appel sejour
 */
class AppelSejourFixtures extends Fixtures
{
    public const APPEL_SEJOUR                         = 'appel_sejour';
    public const APPEL_SEJOUR_OTHER                   = 'appel_sejour_other';
    public const APPEL_SEJOUR_ADMISSION_NO_ORDER_ITEM = 'appel_sejour_admission_no_order_item';
    public const APPEL_SEJOUR_ADMISSION               = 'appel_sejour_admission';
    public const APPEL_SEJOUR_SORTIE                  = 'appel_sejour_sortie';

    public const APPEL_SEJOUR_FORM_EX_CLASS  = 'appel_sejour_form_ex_class';
    public const APPEL_SEJOUR_FORM_EX_OBJECT = 'appel_sejour_form_ex_object';
    public const APPEL_SEJOUR_FORM_EX_LINK   = 'appel_sejour_form_ex_link';

    public const APPEL_SEJOUR_APPFINE_ORDER_ADMISSION      = 'appel_sejour_appfine_order_admission';
    public const APPEL_SEJOUR_APPFINE_ORDER_SORTIE         = 'appel_sejour_appfine_order_sortie';
    public const APPEL_SEJOUR_APPFINE_ORDER_ITEM_ADMISSION = 'appel_sejour_appfine_order_item_admission';
    public const APPEL_SEJOUR_APPFINE_ORDER_ITEM_SORTIE    = 'appel_sejour_appfine_order_item_sortie';

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function load()
    {
        $sejour_other = $this->createSejour(self::APPEL_SEJOUR_OTHER);
        $this->createAppel(
            $sejour_other,
            CAppelSejour::TYPE_ADMISSION,
            CAppelSejour::ETAT_REALISE,
            self::APPEL_SEJOUR_ADMISSION_NO_ORDER_ITEM
        );

        $sejour          = $this->createSejour(self::APPEL_SEJOUR);
        $appel_admission = $this->createAppel(
            $sejour,
            CAppelSejour::TYPE_ADMISSION,
            CAppelSejour::ETAT_REALISE,
            self::APPEL_SEJOUR_ADMISSION
        );
        $this->createAppel(
            $sejour,
            CAppelSejour::TYPE_SORTIE,
            CAppelSejour::ETAT_ECHEC,
            self::APPEL_SEJOUR_SORTIE
        );

        // Form
        $ex_class = $this->createFormExClass();
        $this->createFormLinks($ex_class, $appel_admission, $sejour->group_id);

        // AppFineClient
        $order_admission = $this->createAppFineOrder(
            $sejour,
            CAppFineClientOrder::FORM_TYPE__DAY_BEFORE_CALL,
            self::APPEL_SEJOUR_APPFINE_ORDER_ADMISSION
        );
        $order_sortie    = $this->createAppFineOrder(
            $sejour,
            CAppFineClientOrder::FORM_TYPE__DAY_AFTER_CALL,
            self::APPEL_SEJOUR_APPFINE_ORDER_SORTIE
        );
        $this->createAppFineOrderItem(
            $appel_admission,
            $order_admission,
            self::APPEL_SEJOUR_APPFINE_ORDER_ITEM_ADMISSION
        );
        $this->createAppFineOrderItem($appel_admission, $order_sortie, self::APPEL_SEJOUR_APPFINE_ORDER_ITEM_SORTIE);
    }

    /**
     * @throws FixturesException
     * @throws CModelObjectException
     */
    private function createSejour(string $tag): CSejour
    {
        $patient = CPatient::getSampleObject();
        $this->store($patient);

        $prat = $this->getUser();

        $sejour                = new CSejour();
        $sejour->patient_id    = $patient->_id;
        $sejour->praticien_id  = $prat->_id;
        $sejour->group_id      = $prat->loadRefFunction()->group_id;
        $sejour->entree_prevue = CMbDT::dateTime();
        $sejour->sortie_prevue = CMbDT::dateTime('+6 HOURS');
        $sejour->libelle       = uniqid();
        $this->store($sejour, $tag);

        return $sejour;
    }

    /**
     * @throws FixturesException
     * @throws CModelObjectException
     */
    private function createAppel(CSejour $sejour, string $type, string $etat, ?string $tag = null): CAppelSejour
    {
        $datetime = CMbDT::dateTime("+1 DAYS", $sejour->entree);

        if ($type === CAppelSejour::TYPE_ADMISSION) {
            $datetime = CMbDT::dateTime("-1 DAYS", $sejour->entree);
        }

        $appel            = CAppelSejour::getSampleObject();
        $appel->sejour_id = $sejour->_id;
        $appel->user_id   = $sejour->praticien_id;
        $appel->type      = $type;
        $appel->datetime  = $datetime;
        $appel->etat      = $etat;
        $this->store($appel, $tag);

        return $appel;
    }

    /**
     * @throws FixturesException
     * @throws CModelObjectException
     */
    private function createAppFineOrder(CSejour $sejour, string $form_type, string $tag): CAppFineClientOrder
    {
        $group = $sejour->loadRefEtablissement();

        $order                 = CAppFineClientOrder::getSampleObject();
        $order->form_type      = $form_type;
        $order->group_id       = $group->_id;
        $order->object_class   = $sejour->_class;
        $order->context_class  = "CExClass";
        $order->_order_pack_id = null;
        $this->store($order, $tag);

        return $order;
    }

    /**
     * @throws FixturesException
     */
    private function createAppFineOrderItem(
        CAppelSejour $appel_sejour,
        CAppFineClientOrder $order,
        string $tag
    ): CAppFineClientOrderItem {
        $sejour = $appel_sejour->loadRefSejour();

        $order_item                          = new CAppFineClientOrderItem();
        $order_item->context_class           = $sejour->_class;
        $order_item->context_id              = $sejour->_id;
        $order_item->appfine_client_order_id = $order->_id;
        $order_item->created_datetime        = $appel_sejour->datetime;
        $order_item->sent_datetime           = $appel_sejour->datetime;
        $this->store($order_item, $tag);

        return $order_item;
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    private function createFormExClass(): CExClass
    {
        $ex_class       = CExClass::getSampleObject();
        $ex_class->name = 'ex_class_test';
        $this->store($ex_class, self::APPEL_SEJOUR_FORM_EX_CLASS);

        return $ex_class;
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException|Exception
     */
    private function createFormLinks(CExClass $ex_class, CAppelSejour $appel_sejour, int $group_id): CExLink
    {
        $ex_object               = new CExObject($ex_class->_id);
        $ex_object->object_class = $appel_sejour->_class;
        $ex_object->object_id    = $appel_sejour->_id;
        $ex_object->group_id     = $group_id;
        $this->store($ex_object, self::APPEL_SEJOUR_FORM_EX_OBJECT);

        $ex_link               = new CExLink();
        $ex_link->object_id    = $appel_sejour->_id;
        $ex_link->object_class = $appel_sejour->_class;
        $ex_link->ex_class_id  = $ex_class->_id;
        $ex_link->ex_object_id = $ex_object->_id;
        $ex_link->group_id     = $group_id;
        $this->store($ex_link, self::APPEL_SEJOUR_FORM_EX_LINK);

        return $ex_link;
    }
}
