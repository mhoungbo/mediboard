<?php

/**
 * @author  SAS XtremSante <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Tests\Fixtures;

use Exception;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\PlanningOp\CModeSortieSejour;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Fixture for mode sortie
 */
class CModeSortieSejourFixtures extends Fixtures implements GroupFixturesInterface
{
    public const TAG_MODE_SORTIE_A          = 'mode_sortie_sejour_fixtures-mode-a';
    public const TAG_MODE_SORTIE_B_INACTIVE = 'mode_sortie_sejour_fixtures-mode-b';

    public static function getGroup(): array
    {
        return ['mode_sortie_sejour_fixtures'];
    }

    /**
     * @inheritDoc
     * @throws FixturesException
     */
    public function load(): void
    {
        $this->generateMode("A", self::TAG_MODE_SORTIE_A);
        $this->generateMode("B", self::TAG_MODE_SORTIE_B_INACTIVE, false);
    }

    /**
     * @throws FixturesException
     * @throws Exception
     */
    private function generateMode(string $libelle, string $tag, bool $active = true)
    {
        /** @var CModeSortieSejour $mode_sortie */
        $mode_sortie           = CModeSortieSejour::getSampleObject();
        $mode_sortie->libelle  = $libelle;
        $mode_sortie->actif    = $active ? '1' : '0';
        $mode_sortie->group_id = CGroups::getCurrent()->_id;
        $this->store($mode_sortie, $tag);
    }
}
