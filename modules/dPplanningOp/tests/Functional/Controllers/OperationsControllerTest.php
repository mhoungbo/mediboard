<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Ccam\CCodageCCAM;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\Tests\Fixtures\CcamActsFixtures;
use Ox\Mediboard\SalleOp\CActeCCAM;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Response;

class OperationsControllerTest extends OxWebTestCase
{
    /**
     * Test list ccams act list from operation and pract
     *
     * @throws Exception
     */
    public function testDefaultListOperationCcamsForPract(): void
    {
        $client = self::createClient();

        /** @var COperation $operation */
        $operation = $this->getOperation();
        /** @var CMediusers $chir */
        $chir = $this->getChir();
        /** @var CActeCCAM $ccam_act */
        $ccam_act = $this->getCcamAct();
        // This coding is locked
        /** @var CCodageCCAM $ccam_coding */
        $ccam_coding = $this->getCcamCoding();

        $client->request('GET', "/api/planning/operations/{$operation->_id}/mediusers/{$chir->rpps}/acte_ccams");

        $this->assertResponseIsSuccessful();

        $collection = $this->getJsonApiCollection($client);

        $this->assertEquals(!!$ccam_coding->locked, $collection->getMeta("all_codings_locked"));

        $ccam_ids = [];
        /** @var Item $item */
        foreach ($collection as $item) {
            $ccam_ids[] = $item->getId();
            $this->assertEquals(CActeCCAM::RESOURCE_TYPE, $item->getType());
        }

        $this->assertEquals([$ccam_act->_id], $ccam_ids);
    }

    /**
     * Test mediuser perm denied
     *
     * @throws Exception
     */
    public function testListOperationCcamsForPractUserPermDenied(): void
    {
        $current_user = CMediusers::get();
        /** @var COperation $operation */
        $operation = $this->getOperation();
        /** @var CMediusers $chir */
        $chir = $this->getChir();

        try {
            $actual_perm                                                            = CPermObject::$users_cache[$current_user->_id][$chir->_class][$chir->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$chir->_class][$chir->_id] = PERM_DENY;

            $client = self::createClient();
            $client->request('GET', "/api/planning/operations/{$operation->_id}/mediusers/{$chir->rpps}/acte_ccams");

            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$chir->_class][$chir->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$chir->_class][$chir->_id] = $actual_perm;
            }
        }
    }

    /**
     * Test mediuser not found
     *
     * @throws Exception
     */
    public function testListOperationCcamsForPractNotFound(): void
    {
        $client = self::createClient();

        /** @var COperation $operation */
        $operation = $this->getOperation();
        /** @var CMediusers $chir */

        $client->request('GET', "/api/planning/operations/{$operation->_id}/mediusers/wrong_rpps/acte_ccams");

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
        $error = $this->getJsonApiError($client);
        $this->assertEquals(
            $this->translator->tr("common-error-Object %s not found", CMediusers::class),
            $error->getMessage()
        );
    }

    /**
     * @throws TestsException
     */
    private function getOperation(): CStoredObject
    {
        return $this->getObjectFromFixturesReference(COperation::class, CcamActsFixtures::TAG_OPERATION);
    }

    /**
     * @throws TestsException
     */
    private function getChir(): CStoredObject
    {
        return $this->getObjectFromFixturesReference(CMediusers::class, CcamActsFixtures::TAG_CHIR);
    }

    /**
     * @throws TestsException
     */
    private function getCcamAct(): CStoredObject
    {
        return $this->getObjectFromFixturesReference(CActeCCAM::class, CcamActsFixtures::TAG_CCAM_ACT);
    }

    /**
     * @throws TestsException
     */
    private function getCcamCoding(): CStoredObject
    {
        return $this->getObjectFromFixturesReference(CCodageCCAM::class, CcamActsFixtures::TAG_CCAM_CODAGE);
    }
}
