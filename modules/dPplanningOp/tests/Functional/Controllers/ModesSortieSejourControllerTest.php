<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\PlanningOp\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Mediboard\PlanningOp\CModeSortieSejour;
use Ox\Mediboard\PlanningOp\Tests\Fixtures\CModeSortieSejourFixtures;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class ModesSortieSejourControllerTest extends OxWebTestCase
{
    /**
     * Test default use on list
     *
     * @throws Exception
     */
    public function testListDefault(): void
    {
        $client = self::createClient();

        $mode_a = $this->getMode(CModeSortieSejourFixtures::TAG_MODE_SORTIE_A);
        $mode_b = $this->getMode(CModeSortieSejourFixtures::TAG_MODE_SORTIE_B_INACTIVE);

        $client->request('GET', '/api/planning/stay_discharge_modes');

        $this->assertResponseIsSuccessful();

        $collection = $this->getJsonApiCollection($client);
        $mode_ids   = [];

        /** @var Item $item */
        foreach ($collection as $item) {
            $mode_ids[] = $item->getId();
        }

        // Test if we receive by default only active modes
        $this->assertNotContains($mode_b->_id, $mode_ids);
        $this->assertContains($mode_a->_id, $mode_ids);
    }

    /**
     * @throws TestsException
     */
    private function getMode(string $tag): CStoredObject
    {
        return $this->getObjectFromFixturesReference(CModeSortieSejour::class, $tag);
    }
}
