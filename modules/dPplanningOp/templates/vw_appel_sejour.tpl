{{*
 * @package Mediboard\PlanningOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=mode_interv      value=0}}
{{mb_default var=appel_modal_ambu value=0}}

{{assign var=appel value=null}}
{{if array_key_exists($type, $sejour->_ref_appels_by_type)}}
  {{assign var=appel value=$sejour->_ref_appels_by_type.$type}}
{{/if}}

{{assign var=color value="gray"}}

{{if $appel}}
  {{if $appel->_form_sent_appfine_client}}
    {{assign var=color value="#10BA2C"}}
  {{elseif $appel->etat == "realise"}}
    {{assign var=color value="#10BA2C"}}
  {{elseif $appel->etat == "echec"}}
    {{assign var=color value="orange"}}
  {{/if}}
{{/if}}

{{assign var=etat value="none"}}

{{if $appel && $appel->_id}}
    {{assign var=etat value=$appel->etat}}
{{/if}}

{{* AppFine client *}}
{{assign var=color_form value="gray"}}
{{assign var=etat_form  value="none"}}

{{if "appFineClient"|module_active && $appel && $appel->_form_sent_appfine_client}}
  {{if !$appel->_form_sent_appfine_waiting && $appel->_formula_field_result && !$appel->_form_alert}}
    {{assign var=color_form value="#10BA2C"}}
    {{assign var=etat_form value="realise"}}
  {{elseif !$appel->_form_sent_appfine_waiting && !$appel->_formula_field_result || ($appel->_form_alert && $appel->_form_alert.alert)}}
    {{assign var=color_form value="orange"}}
    {{assign var=etat_form value="echec"}}
  {{/if}}
{{elseif "appFineClient"|module_active && array_key_exists($type, $sejour->_call_sent_appfine_client) && $sejour->_call_sent_appfine_client.$type}}
  {{assign var=etat value="realise_appfine"}}
  {{assign var=color value="#10BA2C"}}
{{/if}}

{{* Dans le cas des appels de la veille et du lendemain, si l appel n a pas �t� fait � la bonne date, il n est pas prit en compte pour l affichage de l �tat *}}
{{if (($appel && $appel->_id) && ($type == 'admission') && ($etat != 'none') && ('Ox\Core\CMbDT::daysRelative'|static_call:$appel->datetime:$sejour->entree:true > 2 || $sejour->entree < $appel->datetime))
  || (($appel && $appel->_id) && ($type == 'sortie') && ($etat != 'none') && ('Ox\Core\CMbDT::daysRelative'|static_call:$sejour->sortie:$appel->datetime:true > 2 || $sejour->sortie > $appel->datetime))}}
  {{assign var=color value="gray"}}
  {{assign var=etat value="none"}}
{{/if}}

<div style="position: relative;">
  {{if "appFineClient"|module_active && array_key_exists($type, $sejour->_call_sent_appfine_client) && $sejour->_call_sent_appfine_client.$type}}
        <i class="fa fa-list event-icon icon_form_appel_sejour{{if $appel_modal_ambu}}_big{{/if}} etat_form_{{$etat_form}}"
    style="background-color: {{$color_form}};"
    title="{{tr}}CAppelSejour.etat_form.{{$etat_form}}{{/tr}}"></i>
  {{/if}}

  {{if $mode_interv}}
    <i class="fa fa-phone event-icon {{if $appel_modal_ambu}}big_pointer{{/if}}" style="background-color: {{$color}}; {{if !$appel_modal_ambu}}font-size: 100%;{{/if}} cursor: pointer;"
       title="{{if !$appel_modal_ambu}}{{tr}}CAppelSejour.etat.{{$etat}}{{/tr}}{{if $appel && $appel->commentaire}} - {{$appel->commentaire}}{{/if}}{{/if}}"
       onclick="Appel.edit(0, '{{$type}}', '{{$sejour->_id}}', '{{if $interv}}{{$interv->_id}}{{/if}}', '{{$appel_modal_ambu}}');"></i>
  {{else}}
    <button type="button" class="fa fa-phone notext" style="color:{{$color}} !important; border: {{$color}} 2px solid;"
            title="{{if !$appel_modal_ambu}}{{tr}}CAppelSejour.etat.{{$etat}}{{/tr}}{{if $appel && $appel->commentaire}} - {{$appel->commentaire}}{{/if}}{{/if}}"
            onclick="Appel.edit(0, '{{$type}}', '{{$sejour->_id}}', '{{$appel_modal_ambu}}');"></button>
  {{/if}}
</div>

{{if $appel_modal_ambu}}
  {{if $etat == "realise_appfine"}}
    {{assign var=etat value="realise"}}
  {{/if}}
  <div style="background-color: {{$color}}; color: white; text-align: center; margin-top: 6px;">
    {{tr}}CAppelSejour.etat.{{$etat}}{{/tr}}
  </div>
{{/if}}
