{{*
 * @package Mediboard\PlanningOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script type="text/javascript">
    toggleDetails = function (element, target) {
        element.classList.toggle('fa-chevron-left');
        element.classList.toggle('fas-chevron-down');
        $(target).toggle();
    }
</script>

<div class="me-display-flex me-flex-column">
    {{if $report->fatal_error}}
        <div class="small-error">
            {{tr}}{{$report->fatal_error_message}}{{/tr}}
        </div>
    {{else}}
        <div class="small-success">
            {{tr var1=$report->modified}}FunctionalUnitAllocationReport-msg-sejours_modified{{/tr}}
        </div>
        {{if count($report->uf_already_set)}}
            <div class="small-warning">
                <i class="fas fa-chevron-right" style="cursor: pointer;" onclick="toggleDetails(this, 'report-unmodified-details')"></i>
                {{tr var1=$report->uf_already_set|@count}}FunctionalUnitAllocationReport-msg-uf_already_set{{/tr}}
                <ul id="report-unmodified-details" style="display: none">
                    {{foreach from=$report->uf_already_set item=sejour_id}}
                        <li><span onmouseover="ObjectTooltip.createEx(this, 'CSejour-{{$sejour_id}}')">{{$sejour_id}}</span></li>
                    {{/foreach}}
                </ul>
            </div>
        {{/if}}
        {{if count($report->incompatible_sejour_type)}}
            <div class="small-warning">
                <i class="fas fa-chevron-right" style="cursor: pointer;" onclick="toggleDetails(this, 'report-unmodified-details')"></i>
                {{tr var1=$report->incompatible_sejour_type|@count}}FunctionalUnitAllocationReport-msg-incompatible_sejour_type{{/tr}}
                <ul id="report-unmodified-details" style="display: none">
                    {{foreach from=$report->incompatible_sejour_type item=sejour_id}}
                        <li><span onmouseover="ObjectTooltip.createEx(this, 'CSejour-{{$sejour_id}}')">{{$sejour_id}}</span></li>
                    {{/foreach}}
                </ul>
            </div>
        {{/if}}
        {{if count($report->outside_group)}}
            <div class="small-warning">
                <i class="fas fa-chevron-right" style="cursor: pointer;" onclick="toggleDetails(this, 'report-outside_group-details')"></i>
                {{tr var1=$report->outside_group|@count}}FunctionalUnitAllocationReport-msg-sejours_outside_group{{/tr}}
                <ul id="report-outside_group-details" style="display: none">
                    {{foreach from=$report->outside_group item=sejour_id}}
                        <li><span onmouseover="ObjectTooltip.createEx(this, 'CSejour-{{$sejour_id}}')">{{$sejour_id}}</span></li>
                    {{/foreach}}
                </ul>
            </div>
        {{/if}}
        {{if count($report->errors)}}
            <div class="small-error">
                <i class="fas fa-chevron-right" style="cursor: pointer;" onclick="toggleDetails(this, 'report-errors-details')"></i>
                {{tr var1=$report->errors|@count}}FunctionalUnitAllocationReport-msg-errors{{/tr}}
                <ul id="report-errors-details" style="display: none">
                    {{foreach from=$report->errors key=sejour_id item=message}}
                        <li><span onmouseover="ObjectTooltip.createEx(this, 'CSejour-{{$sejour_id}}')">{{$sejour_id}}</span>: {{$message}}</li>
                    {{/foreach}}
                </ul>
            </div>
        {{/if}}
    {{/if}}
</div>
