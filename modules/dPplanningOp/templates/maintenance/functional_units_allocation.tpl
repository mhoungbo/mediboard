{{*
 * @package Mediboard\PlanningOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script type="text/javascript">
  toggleEaiHandlers = function(form)
  {
    if($V(form.raw_store)) {
      form.deactivate_handlers.checked = false
    }
    form.deactivate_handlers.disabled = $V(form.raw_store)
  }
</script>

<form name="allocateFunctionalUnit" method="post" action="?" enctype="multipart/form-data"
      onsubmit="return onSubmitFormAjax(this, null, 'allocation_results');">
        <input type="hidden" name="MAX_FILE_SIZE" value="67108864"/><!-- 64MB -->
    {{mb_route name=planning_op_maintenance_allocate_function_unit}}
    {{csrf_token id=planning_op_maintenance_allocate_function_unit}}

  <table class="form">
        <tr>
            <td colspan="2">
                <div class="small-info">{{tr}}FunctionalUnitAllocationReport-msg-file_format{{/tr}}</div>
            </td>
        </tr>
        <tr>
            <th>
                <label for="sejours">{{tr}}File{{/tr}}</label>
            </th>
            <td>
                {{mb_include module=system template=inc_inline_upload lite=true extensions='csv' multi=false paste=false}}
            </td>
        </tr>
        <tr>
            <th>
                <label for="functional_unit_id">{{tr}}CSejour-uf_medicale_id{{/tr}}</label>
            </th>
            <td>
                <select name="functional_unit_id">
                    <option value="">&mdash; {{tr}}Select{{/tr}}</option>
                    {{foreach from=$functional_units item=unit}}
                        <option value="{{$unit->_id}}">
                            {{mb_value object=$unit field=code}} - {{mb_value object=$unit field=libelle}}
                        </option>
                    {{/foreach}}
                </select>
            </td>
        </tr>
        <tr>
            <th>
                <label for="force_allocation" title="{{tr}}FunctionalUnitAllocationReport-force_allocation-desc{{/tr}}">
                    {{tr}}FunctionalUnitAllocationReport-force_allocation{{/tr}}
                </label>
            </th>
            <td>
                <input type="checkbox" name="force_allocation" value="1">
            </td>
        </tr>
        <tr>
            <th>
                <label for="raw_store" title="{{tr}}FunctionalUnitAllocationReport-raw_store-desc{{/tr}}">
                    {{tr}}FunctionalUnitAllocationReport-raw_store{{/tr}}
                </label>
            </th>
            <td>
                <input type="checkbox" name="raw_store" value="1" onchange="toggleEaiHandlers(this.form)">
            </td>
        </tr>
        <tr>
            <th>
                <label for="deactivate_handlers" title="{{tr}}FunctionalUnitAllocationReport-deactivate_handlers-desc{{/tr}}">
                    {{tr}}FunctionalUnitAllocationReport-deactivate_handlers{{/tr}}
                </label>
            </th>
            <td>
                <input type="checkbox" name="deactivate_handlers" value="1">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="small-info">{{tr}}FunctionalUnitAllocationReport-msg-behavior{{/tr}}</div>
            </td>
        </tr>
        <tr>
            <td class="button" colspan="2">
                <button type="submit">
                    {{tr}}FunctionalUnitAllocator-action{{/tr}}
                </button>
            </td>
        </tr>
    </table>
</form>

<div id="allocation_results" class="me-overflow-hidden"></div>
