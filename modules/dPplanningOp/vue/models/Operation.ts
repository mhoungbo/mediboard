/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class Operation extends OxObject {
    constructor () {
        super()
        this.type = "intervention"
    }

    get stayId (): OxAttr<string> {
        return super.get("sejour_id")
    }

    set stayId (stayId: OxAttr<string>) {
        super.set("sejour_id", stayId)
    }

    get date (): OxAttr<string> {
        return super.get("date")
    }

    set date (date: OxAttr<string>) {
        super.set("date", date)
    }

    get surgeonRPPS (): OxAttr<string> {
        return super.get("chir_rpps")
    }

    set surgeonRPPS (surgeonRPPS: OxAttr<string>) {
        super.set("chir_rpps", surgeonRPPS)
    }

    get wording (): OxAttr<string> {
        return super.get("libelle")
    }

    set wording (wording: OxAttr<string>) {
        super.set("libelle", wording)
    }

    get determinedStartTime (): OxAttr<string> {
        return super.get("determined_start_time")
    }

    set determinedStartTime (determinedStartTime: OxAttr<string>) {
        super.set("determined_start_time", determinedStartTime)
    }

    get duration (): OxAttr<string> {
        return super.get("temp_operation")
    }

    set duration (duration: OxAttr<string>) {
        super.set("temp_operation", duration)
    }
}
