/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Patient from "@modules/dPpatients/vue/models/Patient"

export default class Sejour extends OxObject {
    public static RELATION_PATIENT = "patient"

    protected _relationsTypes = {
        patient: Patient
    }

    constructor () {
        super()
        this.type = "sejour"
    }

    get admission (): OxAttr<string> {
        return super.get("entree")
    }

    set admission (admission: OxAttr<string>) {
        super.set("entree", admission)
    }

    get practitionerRPPS (): OxAttr<string> {
        return super.get("praticien_rpps")
    }

    set practitionerRPPS (practitionerRPPS: OxAttr<string>) {
        super.set("praticien_rpps", practitionerRPPS)
    }

    get dheEventWording (): OxAttr<string> {
        return super.get("dhe_event_wording")
    }

    set dheEventWording (dheEventWording: OxAttr<string>) {
        super.set("dhe_event_wording", dheEventWording)
    }
}
