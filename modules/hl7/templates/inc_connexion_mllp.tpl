{{*
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_include module="eai" template="report/inc_report"}}

{{if $ack}}
  <table class="table">
      {{foreach from=$ack item=_ack name="mllp_ack"}}
          {{if $_ack}}
            <tr>
              <td>
                  {{$smarty.foreach.mllp_ack.index}}
                <pre>{{$_ack}}</pre>
              </td>
            </tr>
          {{/if}}
      {{/foreach}}
  </table>
{{/if}}
