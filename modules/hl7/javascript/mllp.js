/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

MLLP = {
  connexion: function (element, exchange_source_name) {
    const route = element.getAttribute('data-route-connexion')
    if (!route) {
      return;
    }

    new Url()
      .setRoute(route, 'hl7_gui_sources_connexion', 'hl7')
      .addParam("exchange_source_name", exchange_source_name)
      .requestModal(600);
  },

  sendTest: function (element, exchange_source_name) {
    const route = element.getAttribute('data-route-send')
    if (!route) {
      return;
    }

    new Url()
      .setRoute(route, 'hl7_gui_sources_send', 'hl7')
      .addParam("exchange_source_name", exchange_source_name)
      .addParam("test", 1)
      .requestModal(600);
  },

  toggleDisabled : function (input_name, source_name) {
    var form = getForm("editSourceMLLP-"+source_name);
    var input = form.elements[input_name];
    input.disabled ? input.disabled = '' : input.disabled = 'disabled';
  }
}
