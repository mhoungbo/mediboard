<?php

/**
 * @package Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Tests\Fixtures\V2\Handle\Appointments;

use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\CModelObjectException;
use Ox\Interop\Eai\Tests\Fixtures\SejourFixturesTrait;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\System\CSenderFileSystem;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

class HandleAppointmentsFixtures extends Fixtures
{
    use SejourFixturesTrait;

    public const DEFAULT_APPOINTMENTS_DATA = 'HandleAppointmentsFixtures_default';
    public const PATIENT_IPP               = "10000001";
    public const SEJOUR_NDA                = "10000001";
    const        MEDIUSER_INAMI            = "12341234123";

    /**
     * @return void
     * @throws CMbException
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function load(): void
    {
        $data = $this->createFastSejour(self::DEFAULT_APPOINTMENTS_DATA, true);

        $patient = $data[CPatient::class];
        $this->addIPPToPatient($patient, null, self::PATIENT_IPP);

        $sejour = $data[CSejour::class];
        $this->addNdaToSejour($sejour, null, self::SEJOUR_NDA);

        /** @var CMediusers $mediuser */
        $mediuser = $data[CMediusers::class];
        $mediuser->inami = self::MEDIUSER_INAMI;
        $this->store($mediuser, self::DEFAULT_APPOINTMENTS_DATA);

        // actor
        $sender           = new CSenderFileSystem();
        $sender->group_id = CGroups::loadCurrent()->_id;
        $sender->nom      = uniqid('fixtures_record_appointment');
        $sender->actif    = 1;
        $sender->role     = CAppUI::conf('instance_role');
        $this->store($sender, self::DEFAULT_APPOINTMENTS_DATA);
    }
}
