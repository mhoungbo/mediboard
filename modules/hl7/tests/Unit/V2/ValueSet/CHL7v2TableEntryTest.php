<?php
/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Tests\Unit\V2\ValueSet;

use Ox\Core\CMbObjectSpec;
use Ox\Interop\Hl7\CHL7v2;
use Ox\Interop\Hl7\V2\ValueSet\CHL7v2TableEntry;
use Ox\Tests\OxUnitTestCase;

/**
 * Class CHL7v2TableEntry
 * HL7 Table Entry
 */
class CHL7v2TableEntryTest extends OxUnitTestCase
{
    public function testGetEntryValuesForTable001(): void
    {
        $this->assertEquals('m', CHL7v2TableEntry::mapFrom('1', 'M'));
        $this->assertEquals('f', CHL7v2TableEntry::mapFrom('1', 'F'));

        $this->assertEquals('M', CHL7v2TableEntry::mapTo('1', 'm'));
        $this->assertEquals('F', CHL7v2TableEntry::mapTo('1', 'f'));
    }

    public function testEntryDefaultValue(): void
    {
        $this->assertEquals('M', CHL7v2TableEntry::mapTo('1', 'undefined_value', 'M'));
    }

    public function testGetEntryDescriptionForTable001(): void
    {
        $this->assertNotNull(CHL7v2TableEntry::getDescription('1', 'M'));
        $this->assertNull(CHL7v2TableEntry::getDescription('1', 'm'));
    }

}
