<?php
/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Tests\Unit\V2\Handle\Appointments;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\Config\Conf;
use Ox\Interop\Eai\Repository\Exceptions\SejourRepositoryException;
use Ox\Interop\Hl7\CExchangeHL7v2;
use Ox\Interop\Hl7\Events\SWF\CHL7v2EventSIUS12;
use Ox\Interop\Hl7\Exceptions\V2\CHL7v2ExceptionError;
use Ox\Interop\Hl7\Tests\Fixtures\V2\Handle\Appointments\HandleAppointmentsFixtures;
use Ox\Interop\Hl7\Tests\Helper\Component;
use Ox\Interop\Hl7\Tests\Helper\MessageBuilder;
use Ox\Interop\Hl7\V2\Handle\Appointments\AbstractHandleAppointments;
use Ox\Interop\Hl7\V2\Handle\Appointments\HandleElementPrescription;
use Ox\Interop\Hl7\V2\Handle\RecordAppointment;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Prescription\CPrescription;
use Ox\Mediboard\Prescription\CPrescriptionLineElement;
use Ox\Mediboard\System\CSenderFileSystem;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

/**
 * Class CHL7v2TableEntry
 * HL7 Table Entry
 */
class HandleElementPrescriptionTest extends OxUnitTestCase
{
    private const DEFAULT_MESSAGE_SIU = <<<EOT
MSH|^~\&|CTU|QSYNC|||20190904123006||SIU^S12|20190904123006044|P|2.5
SCH|||||894727|RSPP|||||^^^20230525123000^20230525143000|inami||||empty||||me
PID|||ipp^^^tag_ipp^PI||Test^Siu||195007050000|M|||AVENUE DES VILLAS 102^^LINKEBEEK^^1630||||||||50070518783
PV1||I|URGENCES^^SAUV2^MEDIPOLE||||10100591501||||||||||||nda^^^tag_nda^AN
EOT;

    /**
     * @param string    $msg_hl7
     * @param Conf|null $conf
     *
     * @return RecordAppointment
     * @throws TestsException
     */
    public function getRecordAppointmentFromMsg(string $msg_hl7, Conf $conf = null): RecordAppointment
    {
        /** @var CSenderFileSystem $sender */
        $sender = $this->getObjectFromFixturesReference(
            CSenderFileSystem::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );

        if ($conf) {
            $sender->setConf($conf);
        }

        $sender->_tags['_tag_patient'] = 'tag_ipp';
        $sender->_tags['_tag_sejour'] = 'tag_nda';

        $exchange_hl7v2 = new CExchangeHL7v2();
        $exchange_hl7v2->_ref_sender = $sender;

        $event = new CHL7v2EventSIUS12();
        $event->_sender = $sender;
        $event->_exchange_hl7v2 = $exchange_hl7v2;

        /** @var RecordAppointment $record_app */
        $record_app = $event->handle($msg_hl7);
        $record_app->_ref_exchange_hl7v2 = $exchange_hl7v2;
        $record_app->_ref_sender         = $sender;

        $ack                             = $record_app->getEventACK($event);
        $ack->_ref_exchange_hl7v2        = $exchange_hl7v2;

        $record_app->setAck($ack);
        $exchange_hl7v2->code = "S12";

        return $record_app;
    }

    public function providerWhenPatientIsNotFound(): array
    {
        return [
            'without pid and ipp' => [[]],
            'with ipp which not exists' => [['personIdentifiers' => ['PI' => 'not_exist_ipp']]]
        ];
    }

    /**
     * @dataProvider providerWhenPatientIsNotFound
     * @param array $data
     *
     * @return void
     * @throws CHL7v2ExceptionError
     * @throws TestsException
     */
    public function testWhenPatientIsNotFound(array $data): void
    {
        $record_app = $this->getRecordAppointmentFromMsg(self::DEFAULT_MESSAGE_SIU);
        $this->expectException(CHL7v2ExceptionError::class);

        $handle = new HandleElementPrescription($record_app);
        $handle->handlePatient($data);
    }

    public function providerWhenSejourIsNotFound(): array
    {
        return [
            'without pid and nda' => [[]],
            'with nda which not exists' => [['personIdentifiers' => ['AN' => 'not_exist_nda']]]
        ];
    }

    /**
     * @dataProvider providerWhenSejourIsNotFound
     * @param array $data
     *
     * @return void
     * @throws CHL7v2ExceptionError
     * @throws TestsException
     * @throws SejourRepositoryException
     */
    public function testWhenSejourIsNotFound(array $data): void
    {
        $record_app = $this->getRecordAppointmentFromMsg(self::DEFAULT_MESSAGE_SIU);
        /** @var CPatient $patient */
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );

        $handle = new HandleElementPrescription($record_app);
        $sejour = $handle->handleSejour($patient, $data);

        $this->assertNull($sejour);
    }

    /**
     * @return void
     * @throws CHL7v2ExceptionError
     * @throws TestsException
     */
    public function testWhenSejourIsNotGivenToTheHandle(): void
    {
        $record_app = $this->getRecordAppointmentFromMsg(self::DEFAULT_MESSAGE_SIU);

        /** @var CPatient $patient */
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );

        $this->expectException(CHL7v2ExceptionError::class);

        $handle = new HandleElementPrescription($record_app);
        $handle->handleAppointments($patient, []);
    }

    /**
     * @return void
     * @throws CHL7v2ExceptionError
     * @throws TestsException
     * @throws Exception
     */
    public function testWhenPraticienIsNotFound(): void
    {
        $record_app = $this->getRecordAppointmentFromMsg(self::DEFAULT_MESSAGE_SIU);

        /** @var CPatient $patient */
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );

        $sejour = $this->getObjectFromFixturesReference(
            CSejour::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );

        $data = $record_app->getContentNodes();
        $data[CSejour::class] = $sejour;

        $this->expectException(CHL7v2ExceptionError::class);

        $handle = new HandleElementPrescription($record_app);
        $handle->handleAppointments($patient, $data);
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testWhenMessageNotContainsAIGSegments(): void
    {
        /** @var CPatient $patient */
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );

        $sejour = $this->getObjectFromFixturesReference(
            CSejour::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );

        $msg_hl7 = MessageBuilder::from(self::DEFAULT_MESSAGE_SIU);
        $msg_hl7->getOrCreateSegment('SCH')
                ->setOnlyComponent(Component::from(HandleAppointmentsFixtures::MEDIUSER_INAMI), 12);

        $record_app = $this->getRecordAppointmentFromMsg($msg_hl7);
        $handle     = new HandleElementPrescription($record_app);

        $data = $record_app->getContentNodes();
        $data[CSejour::class] = $sejour;

        $this->expectException(CHL7v2ExceptionError::class);
        $handle->handleAppointments($patient, $data);
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testHandleElementPrescriptionForSejour(): void
    {
        /** @var CMediusers $mediusers */
        $mediusers = $this->getObjectFromFixturesReference(
            CMediusers::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );

        $conf = $this->getMockBuilder(Conf::class)
                     ->onlyMethods(['getContextual'])
                     ->getMock();
        $conf->method('getContextual')->willReturnCallback(function (string $name, $context) {
            if ($name === "eai CExchangeHL7v2-handle-siu handle_SIU_object") {
                return AbstractHandleAppointments::HANDLE_ELEMENT_PRESCRIPTION;
            }

            return CAppUI::conf($name, $context);
        });

        $nda = HandleAppointmentsFixtures::SEJOUR_NDA;
        $ipp = HandleAppointmentsFixtures::PATIENT_IPP;

        // prepare Message HL7
        $msg_hl7 = MessageBuilder::from(self::DEFAULT_MESSAGE_SIU);
        $msg_hl7->getOrCreateSegment('SCH')
                ->setOnlyComponent(Component::from(HandleAppointmentsFixtures::MEDIUSER_INAMI), 12);
        $msg_hl7->getOrCreateSegment('PID')
                ->setOnlyComponent(Component::from("$ipp^^^mb^PI"), 3)
                ->setOnlyComponent(Component::from("$nda^^^mb^AN"), 18);

        $msg_hl7->getOrCreateSegment('AIG')
            ->setOnlyComponent(Component::from('1'), 1)
            ->setOnlyComponent(Component::from('Rdv pansement bras gauche'), 3)
            ->setOnlyComponent(Component::from('Pansements'), 4)
            ->setOnlyComponent(Component::from('biologie'), 5);

        $msg_hl7->getOrCreateSegment(Component::from('AIG'), 2)
            ->setOnlyComponent(Component::from('2'), 1)
                ->setOnlyComponent(Component::from('Rdv pansement bras droit'), 3)
                ->setOnlyComponent(Component::from('Pansements'), 4)
                ->setOnlyComponent(Component::from('biologie'), 5);

        $record_app = $this->getRecordAppointmentFromMsg($msg_hl7, $conf);

        $data = $record_app->getContentNodes();
        $record_app->handle($record_app->getAck(), new CPatient(), $data);

        /** @var CSejour $sejour_expected */
        $sejour_expected = $this->getObjectFromFixturesReference(
            CSejour::class,
            HandleAppointmentsFixtures::DEFAULT_APPOINTMENTS_DATA
        );
        $this->assertEquals($sejour_expected->_id, $record_app->getSejour()->_id);
        $this->assertPrescriptionHasTwoLignes($sejour_expected);
        $this->assertPrescribedByMediusers($sejour_expected, $mediusers);
    }

    private function assertPrescriptionHasTwoLignes(CSejour $sejour)
    {
        /** @var CPrescription $prescription */
        $prescription = $sejour->loadRefPrescriptionSejour();
        $this->assertNotNull($prescription->_id);

        $lines = $prescription->loadRefsLinesElement();
        $this->assertGreaterThan(2, $lines);
    }

    private function assertPrescribedByMediusers(CSejour $sejour, CMediusers $mediusers)
    {
        /** @var CPrescription $prescription */
        $prescription = $sejour->loadRefPrescriptionSejour();

        /** @var CPrescriptionLineElement[] $lines */
        $lines = $prescription->loadRefsLinesElement();
        $this->assertGreaterThanOrEqual(2, $lines);

        foreach ($lines as $line) {
            $this->assertEquals($mediusers->_id, $line->creator_id);
            $this->assertEquals($mediusers->_id, $line->praticien_id);
        }
    }
}
