<?php

/**
 * @package Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Tests\Helper;

class Component
{
    private $sub_components = [];

    public function getSubComponent(int $index): SubComponent
    {
        if ($index === 0) {
            $index = 1;
        }

        return $this->sub_components[$index];
    }

    public function setSubComponent(int $index, SubComponent $sub_component): self
    {
        if ($index === 0) {
            $index = 1;
        }

        $this->sub_components[$index] = $sub_component;

        return $this;
    }

    public function __toString(): string
    {
        $str = '';

        $max_item = max(array_keys($this->sub_components));
        for ($i = 1; $i <= $max_item; $i++) {
            if (!isset($this->sub_components[$i])) {
                $this->sub_components[$i] = SubComponent::from('');
            }

            $component = $this->sub_components[$i];

            $str .= (($i === 1) ? '' : '^') . ((string) $component);
        }

        return $str;
    }

    public static function from(string $message): self
    {
        $component = new self();

        foreach (explode('^', $message) as $index => $sub_component) {
            $index += 1;
            $component->setSubComponent($index, SubComponent::from($sub_component));
        }

        return $component;
    }
}
