<?php

/**
 * @package Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Tests\Helper;

class MessageBuilder
{
    /** @var Segment[] */
    private array $segments = [];

    /**
     * @param string $name
     * @param int    $index
     *
     * @return Segment
     */
    public function getSegment(string $name, int $index = 1): ?Segment
    {
        if ($index === 0) {
            $index = 1;
        }

        return $this->segments[$name][$index] ?? null;
    }

    public function getOrCreateSegment(string $name, int $index = 1): Segment
    {
        $segment = $this->getSegment($name, $index);
        if (is_null($segment)) {
            $segment = new Segment($name);
            $this->setSegment($segment, $index);
        }

        return $segment;
    }

    /**
     * @param Segment $segment
     *
     * @return void
     */
    public function addSegment(Segment $segment): void
    {
        $index = empty($this->segments[$segment->getName()])
            ? 1
            : (array_key_last($this->segments[$segment->getName()]) + 1);

        $this->segments[$segment->getName()][$index] = $segment;
    }

    public function setSegment(?Segment $segment, int $index = 1): void
    {
        if ($index === 0) {
            $index = 1;
        }

        $this->segments[$segment->getName()][$index] = $segment;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $str = '';
        foreach ($this->segments as $name => $segments) {
            foreach ($segments as $segment) {
                if ($segment === null) {
                    continue;
                }

                $str .= ((string)$segment) . "\n";
            }
        }

        return $str;
    }

    public static function from(string $message): self
    {
        $builder = new self();
        foreach (explode("\n", $message) as $seg) {
            $builder->addSegment(Segment::from($seg));
        }

        return $builder;
    }
}
