<?php

/**
 * @package Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Tests\Helper;

class Segment
{
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    private string $name;
    private array $components = [];

    public function getComponent(int $index, int $index_repetition = 0): ?Component
    {
        $index++;

        return $this->components[$index][$index_repetition] ?? null;
    }

    public function addComponent(?Component $component, int $index): self
    {
        if ($component === null) {
            $component = Component::from('');
        }

        $this->components[$index][] = $component;

        return $this;
    }

    public function setComponent(?Component $component, int $index, int $index_rep = 0): self
    {
        if ($component === null) {
            $component = Component::from('');
        }

        $this->components[$index][$index_rep] = $component;

        return $this;
    }

    public function setOnlyComponent(?Component $component, int $index): self
    {
        if ($component === null) {
            $component = Component::from('');
        }

        $this->components[$index] = [$component];

        return $this;
    }

    public function __toString(): string
    {
        $str = $this->name;

        $max_item = max(array_keys($this->components));
        for ($i = 1; $i <= $max_item; $i++) {
            if (!isset($this->components[$i])) {
                $this->setComponent(Component::from(''), $i);
            }

            $components = $this->components[$i];
            foreach ($components as $component) {
                $str .= '|' . ($component ? ((string) $component) : '');
            }
        }

        return $str;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public static function from(string $message): self
    {
        $exploded_message = explode('|', $message);
        $name = array_shift($exploded_message);

        $segment = new self($name);
        foreach ($exploded_message as $index => $component) {
            $index++;
            if ($component === "^~\&") {
                $segment->addComponent(Component::from($component), $index);
            } else {
                $exploded_components = explode('~', $component);
                foreach ($exploded_components as $_comp) {
                    $segment->addComponent(Component::from($_comp), $index);
                }
            }
        }

        return $segment;
    }
}
