<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\MLLPClientInterface;
use Ox\Interop\Eai\ExchangeSources\Repository\ExchangeSourceRepository;
use Ox\Interop\Hl7\CSourceMLLP;
use Ox\Tests\OxWebTestCase;

class MLLPControllerTest extends OxWebTestCase
{
    public function providerConnexionKO(): array
    {
        $mock_client = $this->createMock(MLLPClientInterface::class);
        $mock_client->method('isReachableSource')->willThrowException(new CMbException(""));

        $mock_client_connexion = $this->createMock(MLLPClientInterface::class);
        $mock_client_connexion->method('isReachableSource')->willReturn(false);

        return [
            'with exception'      => [$mock_client],
            'with bad connnexion' => [$mock_client_connexion],
        ];
    }

    /**
     * @dataProvider providerConnexionKO
     *
     *
     * @param MLLPClientInterface $mock_client
     *
     * @return void
     * @throws Exception
     */
    public function testConnexionKO(MLLPClientInterface $mock_client): void
    {
        $mock_source = $this->getMockBuilder(CSourceMLLP::class)
                            ->disableOriginalConstructor()
                            ->onlyMethods(['getClient'])
                            ->getMock();
        $mock_source->method('getClient')->willReturn($mock_client);

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
                                 ->onlyMethods(['getTestableSource'])
                                 ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($mock_source);

        $client    = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/hl7/sources/connexion",
            [
                "exchange_source_name" => "source_name",
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//table/tr[@data-severity='1' and @data-item='1']");
        $this->assertEquals(1, $nodes->count());
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testConnexion(): void
    {
        $mock_client = $this->createMock(MLLPClientInterface::class);
        $mock_client->method('isReachableSource')->willReturn(true);

        $mock_source = $this->getMockBuilder(CSourceMLLP::class)
                            ->disableOriginalConstructor()
                            ->onlyMethods(['getClient'])
                            ->getMock();
        $mock_source->method('getClient')->willReturn($mock_client);

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
                                 ->onlyMethods(['getTestableSource'])
                                 ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($mock_source);

        $client    = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/hl7/sources/connexion",
            [
                "exchange_source_name" => "source_name",
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//table/tr[@data-severity='1' and @data-item='1']");
        $this->assertEquals(0, $nodes->count());
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testSendWithoutParameterTestShouldFailed(): void
    {
        $client = $this->createClient();
        $client->request(
            'GET',
            "/gui/hl7/sources/send",
            [
                "exchange_source_name" => "source_name",
            ]
        );

        $this->assertResponseStatusCodeSame(500);
    }

    /**
     * @throws Exception
     */
    public function testSendKo(): void
    {
        $mock_client = $this->createMock(MLLPClientInterface::class);
        $mock_client->method('send')->willThrowException(new CMbException(''));

        $mock_source = $this->getMockBuilder(CSourceMLLP::class)
                            ->disableOriginalConstructor()
                            ->onlyMethods(['getClient'])
                            ->getMock();
        $mock_source->method('getClient')->willReturn($mock_client);

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
                                 ->onlyMethods(['getTestableSource'])
                                 ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($mock_source);

        $client    = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/hl7/sources/send",
            [
                "exchange_source_name" => "source_name",
                'test'                 => 1,
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//table/tr[@data-severity='1' and @data-item='1']");
        $this->assertGreaterThanOrEqual(1, $nodes->count());
    }

    /**
     * @throws Exception
     */
    public function testSend(): void
    {
        $mock_client = $this->createMock(MLLPClientInterface::class);
        $mock_client->method('isReachableSource')->willReturn(true);
        $mock_client->method('send')->willReturn('');

        $mock_source = $this->getMockBuilder(CSourceMLLP::class)
                            ->disableOriginalConstructor()
                            ->onlyMethods(['getClient'])
                            ->getMock();
        $mock_source->method('getClient')->willReturn($mock_client);

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
                                 ->onlyMethods(['getTestableSource'])
                                 ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($mock_source);

        $client    = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/hl7/sources/send",
            [
                "exchange_source_name" => "source_name",
                "test"                 => 1,
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//table/tr[@data-severity='1' and @data-item='1']");
        $this->assertEquals(0, $nodes->count());
    }
}
