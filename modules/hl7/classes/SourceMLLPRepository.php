<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7;

use Ox\Interop\Eai\ExchangeSources\Exceptions\InvalidSourceException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNonActiveException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNotConfiguredException;
use Ox\Interop\Eai\ExchangeSources\Repository\ExchangeSourceRepository;
use Ox\Interop\Eai\ExchangeSources\Repository\SourceRepositoryInterface;

class SourceMLLPRepository implements SourceRepositoryInterface
{
    protected const DEFAULT_TYPES = CSourceMLLP::TYPE;

    public function __construct(private ExchangeSourceRepository $repository)
    {
    }

    /**
     * @inheritDoc
     *
     * @param string $name
     * @param array|string|null $types
     *
     * @return CSourceMLLP
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSource(string $name, array|string|null $types = ''): CSourceMLLP
    {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceMLLP $source */
        $source = $this->repository->getSource($name, $types);

        return $source;
    }

    /**
     * @inheritDoc
     *
     * @param string $name
     * @param array|string|null $types
     *
     * @return CSourceMLLP
     * @throws SourceNotConfiguredException
     */
    public function getTestableSource(string $name, array|string|null $types = ''): CSourceMLLP
    {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceMLLP $source */
        $source = $this->repository->getTestableSource($name, $types);

        return $source;
    }


    /**
     * @inheritDoc
     */
    public function getConfigurableSource(
        string            $name,
        array|string|null $types = '',
        string            $exchange_type = null
    ): CSourceMLLP {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceMLLP $source */
        $source = $this->repository->getConfigurableSource($name, $types, $exchange_type);

        return $source;
    }

    /**
     * @inheritDoc
     *
     * @param string $id
     * @param string $exchange_class
     * @param bool $only_active
     * @return CSourceMLLP
     * @throws InvalidSourceException
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSourceFromId(
        string $id,
        string $exchange_class = 'CSourceMLLP',
        bool $only_active = true
    ): CSourceMLLP {
        return $this->getSourceFromGuid("$exchange_class-$id", $only_active);
    }

    /**
     * @inheritDoc
     * @param string $guid
     * @param bool $only_active
     * @return CSourceMLLP
     * @throws InvalidSourceException
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSourceFromGuid(string $guid, bool $only_active = true): CSourceMLLP
    {
        [$class, $id] = explode('-', $guid, 2);
        if ($class !== "CSourceMLLP") {
            throw new InvalidSourceException($guid);
        }

        /** @var CSourceMLLP $source */
        $source = $this->repository->getSourceFromGuid($guid, $only_active);

        return $source;
    }
}
