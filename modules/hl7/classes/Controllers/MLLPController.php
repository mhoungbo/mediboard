<?php


/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Eai\CReport;
use Ox\Interop\Hl7\CSourceMLLP;
use Ox\Interop\Hl7\SourceMLLPRepository;
use Symfony\Component\HttpFoundation\Response;

class MLLPController extends Controller
{
    /**
     * @param RequestParams        $request_params
     * @param SourceMLLPRepository $source_repository
     *
     * @return Response
     * @throws Exception
     */
    public function connexion(RequestParams $request_params, SourceMLLPRepository $source_repository): Response
    {
        $exchange_source_name = $request_params->get("exchange_source_name", "str notNull");
        $report = new CReport('Rapport connexion');
        $ack    = [];
        try {
            $source = $source_repository->getTestableSource($exchange_source_name);
            $source->disableResilience();

            $ack[] = $this->testAccessibility($source, $report);
        } catch (Exception $exception) {
            $report->addData($exception->getMessage(), CItemReport::SEVERITY_ERROR);
        } finally {
            return $this->renderSmarty('inc_connexion_mllp', ['report' => $report, 'ack' => $ack]);
        }
    }

    /**
     * @param CSourceMLLP $source
     * @param CReport     $report
     *
     * @return string|null
     * @throws CMbException
     */
    private function testAccessibility(CSourceMLLP $source, CReport $report): ?string
    {
        $client = $source->getClient();
        if ($client->isReachableSource()) {
            $report->addData($this->translator->tr('CSourceMLLP-reachable-source'), CItemReport::SEVERITY_SUCCESS);

            return $client->receive();
        } else {
            $report->addData(
                $this->translator->tr(
                    'CSourceMLLP-unreachable-source',
                    [$source->name, $source->port, $source->host]
                ),
                CItemReport::SEVERITY_ERROR
            );
        }

        return null;
    }

    /**
     * @param RequestParams        $request_params
     * @param SourceMLLPRepository $source_repository
     *
     * @return Response
     * @throws Exception
     */
    public function send(RequestParams $request_params, SourceMLLPRepository $source_repository): Response
    {
        if (!$request_params->request('test', 'bool default|0')) {
            throw new CMbException('not supported');
        }

        $name   = $request_params->get("exchange_source_name", "str notNull");
        $report = new CReport('Mllp envoi de donn�es');
        $ack    = [];
        try {
            $source = $source_repository->getTestableSource($name);
            $client = $source->disableResilience();
            $ack[]  = $this->testAccessibility($source, $report);

            $ack[] = $client->send("Hello world !\n");

            $report->addData($this->translator->tr('CSourceMLLP-msg-data sent'), CItemReport::SEVERITY_SUCCESS);
        } catch (Exception $exception) {
            $report->addData($exception->getMessage(), CItemReport::SEVERITY_ERROR);
        } finally {
            return $this->renderSmarty('inc_connexion_mllp', ['report' => $report, 'ack' => $ack]);
        }
    }
}
