<?php

/**
 * @package Mediboard\Ftp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7;

use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\MLLPClientInterface;
use Ox\Interop\Eai\Resilience\CircuitBreaker;
use Ox\Interop\Ftp\CircuitBreakerException;
use Ox\Interop\Ftp\CustomRequestAnalyserInterface;
use Ox\Interop\Ftp\ResponseAnalyser;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Mediboard\System\Sources\ClientResilienceTrait;
use phpDocumentor\Reflection\Types\Mixed_;

class ResilienceMLLPClient implements MLLPClientInterface
{
    /** @var MLLPClientInterface */
    public MLLPClientInterface $client;

    /** @var CircuitBreaker */
    private CircuitBreaker $circuit;

    /** @var ResponseAnalyser */
    private ResponseAnalyser $analyser;

    /** @var CSourceMLLP */
    private CSourceMLLP $source;

    use ClientResilienceTrait;

    /**
     * @param MLLPClientInterface $client
     * @param CExchangeSource     $source
     */
    public function __construct(MLLPClientInterface $client, CSourceMLLP $source)
    {
        $this->client = $client;

        $this->analyser = ($client instanceof CustomRequestAnalyserInterface)
            ? $client->getRequestAnalyser() : new ResponseAnalyser();

        $this->source  = $source;
        $this->circuit = new CircuitBreaker();
    }

    /**
     * @return int
     * @throws CircuitBreakerException
     * @throws CMbException
     */
    public function getResponseTime(): int
    {
        return $this->client->getResponseTime();
    }

    /**
     * @param string|null $data
     *
     * @return string
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function send(string $data = null): string
    {
        $call = fn() => $this->client->send($data);

        return $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @return string
     * @throws CircuitBreakerException
     * @throws CMbException
     */
    public function receive(): string
    {
        $call = fn() => $this->client->receive();

        return $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @return Mixed_|null
     * @throws CircuitBreakerException
     * @throws CMbException
     */
    public function getSocketClient()
    {
        $call = fn() => $this->client->getSocketClient();

        return $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @return Mixed_|null
     * @throws CircuitBreakerException
     * @throws CMbException
     */
    public function getData()
    {
        $call = fn() => $this->client->getData();

        return $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @return Mixed_|null
     * @throws CircuitBreakerException
     * @throws CMbException
     */
    public function getError()
    {
        return $this->client->getError();
    }
}
