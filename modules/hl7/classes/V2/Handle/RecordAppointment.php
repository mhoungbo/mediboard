<?php

/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\V2\Handle;

use DOMNode;
use Exception;
use Ox\AppFine\Client\CAppFineClient;
use Ox\AppFine\Server\CAppFineServer;
use Ox\Core\CMbArray;
use Ox\Core\CMbObject;
use Ox\Core\CMbString;
use Ox\Core\Module\CModule;
use Ox\Erp\CabinetSIH\CCabinetSIHRecordData;
use Ox\Interop\Eai\Tools\CDoctorTrait;
use Ox\Interop\Hl7\CExchangeHL7v2;
use Ox\Interop\Hl7\CHL7Acknowledgment;
use Ox\Interop\Hl7\CHL7v2Acknowledgment;
use Ox\Interop\Hl7\CHL7v2MessageXML;
use Ox\Interop\Hl7\Exceptions\V2\CHL7v2ExceptionError;
use Ox\Interop\Hl7\V2\Handle\Appointments\AbstractHandleAppointments;
use Ox\Interop\Hl7\V2\Handle\Appointments\HandleAppointmentsInterface;
use Ox\Interop\Hl7\V2\Handle\Appointments\HandleConsultation;
use Ox\Interop\Hl7\V2\Handle\Appointments\HandleElementPrescription;
use Ox\Mediboard\Doctolib\CSenderHL7v2Doctolib;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * Class RecordAppointment
 * Record appointment, message XML
 */
class RecordAppointment extends CHL7v2MessageXML
{
    use CDoctorTrait;

    /** @var string[] Event codes */
    public static $event_codes = ["S12", "S13", "S14", "S15", "S16", "S17", "S26"];

    private ?CHL7Acknowledgment $ack = null;

    private ?CPatient $patient = null;

    private ?CSejour $sejour = null;


    /**
     * @return array
     * @throws Exception
     * @see parent::getContentNodes
     */
    public function getContentNodes()
    {
        $data = [];

        $exchange_hl7v2 = $this->_ref_exchange_hl7v2;
        $sender         = $exchange_hl7v2->_ref_sender;
        $sender->loadConfigValues();

        $this->queryNode("SCH", null, $data, true);

        $this->queryNodes("NTE", null, $data, true);

        $PID                       = $this->queryNode("PID", null, $data, true);
        $data["personIdentifiers"] = $this->getPersonIdentifiers("PID.3", $PID, $sender);

        $this->queryNode("PD1", null, $data, true);

        $PV1 = $this->queryNode("PV1", null, $data, true);
        if ($PV1) {
            $data["admitIdentifiers"] = $this->getAdmitIdentifiers($PV1, $sender);
        }

        $resources         = $this->queryNodes("SIU_$exchange_hl7v2->code.RESOURCES", null, $varnull, true);
        $data["resources"] = [];
        foreach ($resources as $_resource) {
            $tmp = [];

            $this->queryNodes("RGS", $_resource, $tmp, true);

            // $AISs
            $AISs = $this->queryNodes("SIU_$exchange_hl7v2->code.SERVICE", $_resource, $varnull);
            foreach ($AISs as $_AIS) {
                $this->queryNodes("AIS", $_AIS, $tmp, true);
            }

            // AIGs
            $AIGs = $this->queryNodes("SIU_$exchange_hl7v2->code.GENERAL_RESOURCE", $_resource, $varnull);
            foreach ($AIGs as $_AIG) {
                $this->queryNodes("AIG", $_AIG, $tmp, true);
            }

            // $AILs
            $AILs = $this->queryNodes("SIU_$exchange_hl7v2->code.LOCATION_RESOURCE", $_resource, $varnull);
            foreach ($AILs as $_AIL) {
                $this->queryNodes("AIL", $_AIL, $tmp, true);
            }

            // AIPs
            $AIPs = $this->queryNodes("SIU_$exchange_hl7v2->code.PERSONNEL_RESOURCE", $_resource, $varnull);
            foreach ($AIPs as $_AIP) {
                $this->queryNodes("AIP", $_AIP, $tmp, true);
            }

            if ($tmp) {
                $data["resources"][] = $tmp;
            }
        }

        $this->queryNode("ZTG", null, $data, true);

        return $data;
    }

    /**
     * Handle event
     *
     * @param CHL7V2Acknowledgment $ack    Acknowledgement
     * @param CPatient             $object Person
     * @param array                $data   Nodes data
     *
     * @return null|string
     * @throws Exception
     */
    public function handle(CHL7Acknowledgment $ack = null, CMbObject $object = null, $data = [])
    {
        $this->ack = $ack;

        /** @var CExchangeHL7v2 $exchange_hl7v2 */
        $exchange_hl7v2 = $this->_ref_exchange_hl7v2;
        $sender         = $this->_ref_sender = $exchange_hl7v2->_ref_sender;

        // Pas d'observations
        $first_result = reset($data["resources"]);

        // Traitement sp�cifique pour Doctolib
        if ($sender->confHL7v2("doctolib handle_doctolib") && CModule::getActive("doctolib")) {
            CSenderHL7v2Doctolib::storeLastEvent($exchange_hl7v2);
        }

        // Traitement sp�cifique pour AppFine Client
        if (CModule::getActive("appFineClient") && $sender->confHL7v2("appFine handle_portail_patient")
            && in_array($exchange_hl7v2->code, RecordAppointment::$event_codes)
        ) {
            return CAppFineClient::handleConsultationPatient($ack, $data, $sender, $exchange_hl7v2);
        }

        // Traitement sp�cifique pour AppFine
        if (CModule::getActive("appFine") && $sender->confHL7v2("appFine handle_portail_patient")
            && in_array($exchange_hl7v2->code, RecordAppointment::$event_codes)
        ) {
            return CAppFineServer::handleConsultationPatient($ack, $data, $sender, $exchange_hl7v2);
        }

        // Traitement sp�cifique pour TAMM-SIH
        if (CModule::getActive("oxCabinetSIH") && $sender->confHL7v2("TAMM-SIH handle_tamm_sih")
            && in_array($exchange_hl7v2->code, RecordAppointment::$event_codes)) {
            return CCabinetSIHRecordData::handleOperation($ack, $data, $sender, $exchange_hl7v2);
        }

        if (!$first_result) {
            return $exchange_hl7v2->setAckAR($ack, "E1000", null, $object);
        }

        try {
            $handle = $this->getHandleAppointments();

            // traitement patient
            $this->patient = $patient = $handle->handlePatient($data);

            // traitement sejour
            $this->sejour = $handle->handleSejour($patient, $data);

            if ($this->sejour) {
                $data[CSejour::class] = $this->sejour;
            }

            return $handle->handleAppointments($patient, $data);
        } catch (CHL7v2ExceptionError $error) {
            if ($error_ack = $error->getAck()) {
                return $error_ack;
            }

            throw $error;
        }
    }

    /**
     * Return Placer Contact Person
     *
     * @param DOMNode $SCH SCH node
     *
     * @return String
     * @throws Exception
     */
    public function getPlacerContactPerson(DOMNode $SCH)
    {
        // Recherche par d�faut dans SCH.12
        $XCNs = $this->queryNodes('SCH.12', $SCH);
        if (!$XCNs || ($XCNs->length === 0)) {
            // Recherche alternative dans le SCH.16
            $XCNs = $this->queryNodes('SCH.16', $SCH);
        }

        if (!$XCNs || ($XCNs->length === 0)) {
            return null;
        }

        return $this->getDoctor($XCNs, new CMediusers(), false);
    }

    /**
     * Get appointment duration
     *
     * @param DOMNode $node SCH Node
     *
     * @return string|null
     * @throws Exception
     */
    public function getAppointmentDuration(DOMNode $node): ?string
    {
        $duration = null;
        $unit     = "m";

        // Duration + Unit
        $SCH_11_3 = $this->queryTextNode("SCH.11/TQ.3", $node);
        if ($SCH_11_3) {
            $duration = $SCH_11_3;
            if (!is_numeric($SCH_11_3)) {
                $unit     = substr($SCH_11_3, 0, 1);
                $duration = substr($SCH_11_3, 1);
            }
        }

        // Duration
        if ($SCH_9 = $this->queryTextNode("SCH.9", $node)) {
            $duration = $SCH_9;
        }

        // No duration
        if (!$duration) {
            return null;
        }

        // Unit
        if ($SCH_10 = $this->queryTextNode("SCH.10/CE.1", $node)) {
            $unit = $SCH_10;
        }

        switch (strtolower($unit)) {
            case 's':
                $duration = $duration / 60;
                break;
            case 'h':
                $duration = $duration * 60;

                break;
            default:
        }

        return $duration;
    }

    /**
     * @throws Exception
     */
    public function getAppointmentResourceGroup(array $resources): ?int
    {
        $resource = CMbArray::get($resources, 0);
        if (!CMbArray::get($resource, 'AIG')) {
            return null;
        }

        $function_id = null;
        foreach (CMbArray::get($resource, 'AIG') as $_AIG) {
            $identifier = $this->queryTextNode("AIG.5/CE.1", $_AIG);
            $text = CMbString::lower($this->queryTextNode("AIG.5/CE.2", $_AIG));
            $name_of_coding = CMbString::lower($this->queryTextNode("AIG.5/CE.3", $_AIG));

            if (!$identifier) {
                continue;
            }

            if (($text !== 'finess') && ($name_of_coding !== 'finess')) {
                continue;
            }

            $function         = new CFunctions();
            $function->finess = $identifier;
            $function->loadMatchingObjectEsc();

            if ($function->_id) {
                return $function->_id;
            }
        }

        return $function_id;
    }

    /**
     * Get specific handle appointments objects
     *
     * @return HandleAppointmentsInterface
     * @throws CHL7v2ExceptionError
     */
    protected function getHandleAppointments(): HandleAppointmentsInterface
    {
        $siu_mode_handle = $this->_ref_sender->confHL7v2("handle-siu handle_SIU_object");
        if ($siu_mode_handle === AbstractHandleAppointments::HANDLE_ELEMENT_PRESCRIPTION) {
            return new HandleElementPrescription($this);
        }

        if ($siu_mode_handle === AbstractHandleAppointments::HANDLE_CONSULTATION) {
            return new HandleConsultation($this);
        }

        throw CHL7v2ExceptionError::ackAR($this->_ref_exchange_hl7v2, $this->ack, "E1011");
    }

    /**
     * @return CHL7Acknowledgment|null
     */
    public function getAck(): ?CHL7Acknowledgment
    {
        return $this->ack;
    }

    /**
     * @param CHL7Acknowledgment|null $ack
     */
    public function setAck(?CHL7Acknowledgment $ack): void
    {
        $this->ack = $ack;
    }

    /**
     * @return CSejour|null
     */
    public function getSejour(): ?CSejour
    {
        return $this->sejour;
    }
}
