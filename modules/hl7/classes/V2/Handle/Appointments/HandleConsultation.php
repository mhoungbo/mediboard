<?php

/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\V2\Handle\Appointments;

use DOMNode;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\Module\CModule;
use Ox\Interop\Eai\CEAIMbObject;
use Ox\Interop\Hl7\Exceptions\V2\CHL7v2ExceptionError;
use Ox\Mediboard\Cabinet\CAgendaPraticien;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Doctolib\CDoctolib;
use Ox\Mediboard\Doctolib\CSenderHL7v2Doctolib;
use Ox\Mediboard\Galaxie\CGalaxie;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Sante400\CIdSante400;

class HandleConsultation extends AbstractHandleAppointments implements HandleAppointmentsInterface
{
    /**
     * @param CPatient $patient
     * @param array    $data
     *
     * @return CSejour|null
     * @throws CHL7v2ExceptionError
     * @throws Exception
     */
    public function handleSejour(CPatient $patient, array $data): ?CSejour
    {
        $sender = $this->sender;
        $venueAN = $this->message->getVenueAN($sender, $data);
        if ($venueAN) {
            $NDA    = CIdSante400::getMatch("CSejour", $sender->_tag_sejour, $venueAN);
            /** @var CSejour $sejour */
            $sejour = $NDA->loadTargetObject();
            if (!$sejour->_id) {
                throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1003", $patient);
            }

            if ($sejour->patient_id !== $patient->_id) {
                throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1004", $patient);
            }

            // nothing is done with sejour in consultation
            return $sejour;
        }

        return null;
    }

    /**
     * @inheritDoc
     * @throws CHL7v2ExceptionError
     * @throws Exception
     */
    public function handleAppointments(CPatient $patient, array $data): string
    {
        $modif_appointment = false;
        $sender = $this->sender;
        $group_id = $this->sender->group_id;

        // Gestion du rendez-vous
        $SCH                         = CMbArray::get($data, "SCH");
        $own_consultation_identifier = $sender_consultation_identifier = null;
        $EI_1                        = $this->message->queryTextNode("SCH.2/EI.1", $SCH);
        $EI_2                        = $this->message->queryTextNode("SCH.2/EI.2", $SCH);
        $EI_3                        = $this->message->queryTextNode("SCH.2/EI.3", $SCH);

        // Notre propre identifiant de consult
        if (($EI_2 == CAppUI::gconf("hl7 CHL7 assigning_authority_namespace_id", $group_id))
            || ($EI_3 == CAppUI::gconf("hl7 CHL7 assigning_authority_universal_id", $group_id))
        ) {
            $own_consultation_identifier = $EI_1;
        }

        // L'identifiant de consult du sender
        if (($EI_3 == $sender->confHL7v2("application assigning_authority_universal_id"))
            || ($EI_2 == $sender->confHL7v2("application assigning_authority_universal_id"))
        ) {
            $sender_consultation_identifier = $EI_1;
        }

        if (!$own_consultation_identifier && !$sender_consultation_identifier) {
            throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1005", $patient);
        }

        // Notre propre ID de consult
        $appointment = new CConsultation();
        if ($own_consultation_identifier) {
            $appointment->load($own_consultation_identifier);
            $appointment->loadRefPlageConsult();
        }

        $idex = new CIdSante400();
        // ID de consult du partenaire
        if ($sender_consultation_identifier) {
            $tag_consultation =
                ($sender->confHL7v2("doctolib handle_doctolib") && CModule::getActive("doctolib"))
                    ? CDoctolib::getObjectTag($group_id)
                    : $sender->_tag_consultation;

            $idex = CIdSante400::getMatch(
                $appointment->_class,
                $tag_consultation,
                $sender_consultation_identifier
            );
        }
        // Chargement de la consultation par l'ID du tiers
        if (!$appointment->_id && $idex->_id) {
            $appointment->load($idex->object_id);
            $appointment->loadRefPlageConsult();
        }

        // Si on ne retrouve pas la consultation et que l'on n'est pas en cr�ation, alors on retourne une erreur
        if (!$appointment->_id && ($this->exchange_hl7v2->code != "S12")) {
            throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1006", $patient);
        }

        if ($appointment->_id && ($appointment->patient_id !== $patient->_id)) {
            throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1007", $patient);
        }

        if ($appointment->_id) {
            $modif_appointment = true;
        }

        // Mapping de la consultation
        $this->mapAndStoreAppointment($data, $appointment, $patient);

        // Idex de l'id de la consult du sender
        if ($sender_consultation_identifier) {
            CEAIMbObject::storeIdex($idex, $appointment, $sender);
        }

        $codes   = [$modif_appointment ? "I1002" : "I1001"];
        $comment = CEAIMbObject::getComment($appointment);

        // Segment TAMM-Galaxie
        if (CMbArray::get($data, "ZTG") && CModule::getActive("galaxie")) {
            $comment .= CGalaxie::mapAppointment($this->message, $data["ZTG"], $appointment, $group_id);
        }

        return $this->exchange_hl7v2->setAckAA($this->ack, $codes, $comment, $appointment);
    }

    /**
     * Mapping and store appointment
     *
     * @param array         $data        Datas
     * @param CConsultation $appointment Appointment
     * @param CPatient      $patient     Patient
     *
     * @return CConsultation
     * @throws Exception
     */
    protected function mapAndStoreAppointment($data, CConsultation $appointment, CPatient $patient): CConsultation
    {
        $exchange_hl7v2 = $this->exchange_hl7v2;
        $sender         = $this->sender;
        $agenda         = new CAgendaPraticien();

        // praticien_id
        if (CModule::getActive("doctolib") && $sender->confHL7v2("doctolib handle_doctolib")) {
            $return = CSenderHL7v2Doctolib::getAgenda($this->message, $data);
            if (!($return instanceof CAgendaPraticien)) {
                throw CHL7v2ExceptionError::ackAR(
                    $this->exchange_hl7v2,
                    $this->ack,
                    ["E1008" => $return],
                    $patient
                );
            }

            $agenda       = $return;
            $praticien_id = $agenda->praticien_id;
        } else {
            $praticien_id = $this->message->getPlacerContactPerson($data["SCH"]);
        }

        if (!$praticien_id) {
            throw CHL7v2ExceptionError::ackAR(
                $this->exchange_hl7v2,
                $this->ack,
                "E1009",
                $patient
            );
        }

        $duration = $this->message->getAppointmentDuration($data["SCH"]);
        $this->getFillerStatutsCode($data["SCH"], $appointment);

        $appointment->_no_synchro_eai = true;
        $dateTime                     = $this->message->queryTextNode("SCH.11/TQ.4/TS.1", $data["SCH"]);

        // On r�cup�re la fonction
        $function_id = $this->message->getAppointmentResourceGroup($data["resources"]);
        try {
            // Si on n'a pas la consultation on va la cr�er
            if (!$appointment->_id) {
                // Cr�ation de la consultation
                $appointment->createByDatetime(
                    $dateTime,
                    $praticien_id,
                    $patient->_id,
                    $duration,
                    $appointment->chrono,
                    1,
                    null,
                    $agenda->_id,
                    $duration,
                    $function_id
                );
            } else { // Modification de la date/heure de la consult. ou de la dur�e
                if (($appointment->_datetime !== $dateTime) || ($appointment->_duree !== $duration)) {
                    $appointment->changeDateTime(
                        $dateTime,
                        $appointment->duree,
                        $appointment->chrono,
                        null,
                        $agenda->_id,
                        $duration,
                        $function_id
                    );
                }

                // Le praticien a chang�
                if ($appointment->_praticien_id !== $praticien_id) {
                    $appointment->changePraticien($praticien_id, $agenda->_id, $function_id);
                }
            }
        } catch (CMbException $e) {
            throw CHL7v2ExceptionError::ackAR(
                $this->exchange_hl7v2,
                $this->ack,
                ["E1008" => $e->getMessage()],
                $patient
            );
        }

        if (($exchange_hl7v2->code == "S15") || ($exchange_hl7v2->code == "S17")) {
            $appointment->annule = 1;
        }

        if ($exchange_hl7v2->code == "S26") {
            $appointment->annule = 1;
            $appointment->motif_annulation = "not_arrived";
        }

        $this->getReason($data["SCH"], $appointment);

        // R�cup�ration des notes sur la consultation
        $this->secondaryMappingAppointment($data, $appointment);

        if (CModule::getActive("galaxie")) {
            $appointment->_link_galaxie = true;
        }

        $appointment->_no_synchro_eai = true;
        if ($msg = $appointment->store()) {
            throw CHL7v2ExceptionError::ackAR(
                $this->exchange_hl7v2,
                $this->ack,
                ["E1008" => $msg],
                $patient
            );
        }

        return $appointment;
    }

    /**
     * Get filler statuts code
     *
     * @param DOMNode       $node        SCH Node
     * @param CConsultation $appointment Appointment
     *
     * @return void
     * @throws Exception
     */
    private function getFillerStatutsCode(DOMNode $node, CConsultation $appointment): void
    {
        // Table - 0278
        // Pending   - Appointment has not yet been confirmed
        // Waitlist  - Appointment has been placed on a waiting list for a particular slot, or set of slots
        // Booked    - The indicated appointment is booked
        // Started   - The indicated appointment has begun and is currently in progress
        // Complete  - The indicated appointment has completed normally (was not discontinued, canceled, or deleted)
        // Cancelled - The indicated appointment was stopped from occurring (canceled prior to starting)
        // Dc        - The indicated appointment was discontinued (DC'ed while in progress, discontinued parent appointment,
        //             or discontinued child appointment)
        // Deleted   - The indicated appointment was deleted from the filler application
        // Blocked   - The indicated time slot(s) is(are) blocked
        // Overbook  - The appointment has been confirmed; however it is confirmed in an overbooked state
        // Noshow    - The patient did not show up for the appointment

        switch ($this->message->queryTextNode("SCH.25/CE.1", $node)) {
            case 'Started':
            case 'In progress':
                $appointment->annule = 0;
                $appointment->chrono = "48";

                return;

            case 'Waiting':
                $appointment->annule = 0;
                $appointment->chrono = "32";

                return;

            case 'Complete':
                $appointment->annule = 0;
                $appointment->chrono = "64";

                return;

            case 'Deleted':
            case 'Cancelled':
                $appointment->annule = 1;

                return;

            case 'Noshow':
                $appointment->annule           = 1;
                $appointment->motif_annulation = "not_arrived";

                return;

            default:
                $appointment->annule = 0;
                $appointment->chrono = "16";
        }
    }

    /**
     * Get appointment's reason
     *
     * @param DOMNode       $node        SCH Node
     * @param CConsultation $appointment Appointment
     *
     * @return void
     * @throws Exception
     */
    private function getReason(DOMNode $node, CConsultation $appointment): void
    {
        // Dans le cas de Doctolib on synchronise le motif de Doctolib uniquement sur la cr�ation (S12)
        if (CModule::getActive("doctolib") &&
            $this->sender->confHL7v2("doctolib handle_doctolib") &&
            ($this->exchange_hl7v2->code != "S12")
        ) {
            return;
        }
        if ($appointment_reason = $this->message->queryTextNode("SCH.7/CE.2", $node)) {
            if (!str_contains($appointment->motif, $appointment_reason)) {
                $appointment->motif .= (($appointment->motif) ? " \n" : null) . $appointment_reason;
            }
        }

        if (!($event_reason = $this->message->queryTextNode("SCH.6/CE.2", $node))) {
            return;
        }

        if (!str_contains($appointment->motif, $event_reason)) {
            $appointment->motif .= (($appointment->motif) ? " \n" : null) . $event_reason;
        }
    }

    /**
     * Secondary mapping appointment
     *
     * @param array         $data
     * @param CConsultation $appointment
     *
     * @return void
     * @throws Exception
     */
    private function secondaryMappingAppointment(array $data, CConsultation $appointment): void
    {
        // R�cup�ration des notes
        if (array_key_exists("NTE", $data)) {
            $this->getAppointmentNotes($data["NTE"], $appointment);
        }
    }

    /**
     * Get notes
     *
     * @param array         $NTEs       NTE nodes
     * @param CConsultation $appointment Appointment
     *
     * @return void
     * @throws Exception
     */
    private function getAppointmentNotes(array $NTEs, CConsultation $appointment): void
    {
        foreach ($NTEs as $_NTE) {
            $comment = $this->message->queryTextNode("NTE.3", $_NTE);
            if (!$comment) {
                continue;
            }
            $comment = str_replace("\\.br\\", "\r\n", $comment);

            $note_type = $this->message->queryTextNode("NTE.4/CE.1", $_NTE);
            switch ($note_type) {
                // Information du patient
                case 'PI':
                    if (!str_contains($appointment->rques, $comment)) {
                        $appointment->rques .= (($appointment->rques) ? " \n" : null) .
                            'Information du patient : ' . $comment;
                    }
                    break;

                // Motif de la consultation
                case '1R':
                    if (!str_contains($appointment->motif, $comment)) {
                        $appointment->motif .= (($appointment->motif) ? " \n" : null) . $comment;
                    }
                    break;

                // Remarque
                case 'RE':
                    if (!str_contains($appointment->rques, $comment)) {
                        $appointment->rques .= (($appointment->rques) ? " \n" : null) . $comment;
                    }
                    break;

                // Histoire de la maladie
                case 'HD':
                    if (!str_contains($appointment->histoire_maladie, $comment)) {
                        $appointment->histoire_maladie .= (($appointment->histoire_maladie) ? " \n" : null) . $comment;
                    }
                    break;

                // Examen clinique
                case 'CE':
                    if (!str_contains($appointment->examen, $comment)) {
                        $appointment->examen .= (($appointment->examen) ? " \n" : null) . $comment;
                    }
                    break;

                // Au total
                case 'GR':
                    if (!str_contains($appointment->conclusion, $comment)) {
                        $appointment->conclusion .= (($appointment->conclusion) ? " \n" : null) . $comment;
                    }
                    break;

                default:
                    if (!str_contains($appointment->rques, $comment)) {
                        $appointment->rques .= (($appointment->rques) ? " \n" : null) . $comment;
                    }
            }
        }
    }
}
