<?php

/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\V2\Handle\Appointments;

use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;

interface HandleAppointmentsInterface
{
    /**
     * Find or create patient
     * Have a patient is required
     *
     * @param array $data
     *
     * @return CPatient
     */
    public function handlePatient(array $data): CPatient;

    /**
     * Search an encouter context
     * Have an encounter is optional
     *
     * @param CPatient $patient
     * @param array    $data
     *
     * @return CSejour|null
     */
    public function handleSejour(CPatient $patient, array $data): ?CSejour;

    /**
     * Handle the specific data for the siu
     *
     * @return string It's should be a stringify Acknowledgment (CExchangeHL7v2->setAck**::())
     */
    public function handleAppointments(CPatient $patient, array $data): string;
}
