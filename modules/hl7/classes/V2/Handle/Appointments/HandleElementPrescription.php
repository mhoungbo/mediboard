<?php

/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\V2\Handle\Appointments;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Interop\Eai\Repository\Exceptions\SejourRepositoryException;
use Ox\Interop\Eai\Repository\SejourRepository;
use Ox\Interop\Hl7\Exceptions\V2\CHL7v2ExceptionError;
use Ox\Interop\Hl7\Exceptions\V2\CHL7v2ExceptionWarning;
use Ox\Mediboard\Medicament\CMedicamentProduit;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Prescription\CCategoryPrescription;
use Ox\Mediboard\Prescription\CElementPrescription;
use Ox\Mediboard\Prescription\CPrescription;
use Ox\Mediboard\Prescription\CPrescriptionLineElement;

class HandleElementPrescription extends AbstractHandleAppointments implements HandleAppointmentsInterface
{
    protected CSejour $sejour;
    protected CPatient $patient;

    /**
     * @inheritDoc
     *
     * @param CPatient $patient
     * @param array    $data
     *
     * @return CSejour|null
     * @throws CHL7v2ExceptionError
     * @throws SejourRepositoryException
     */
    public function handleSejour(CPatient $patient, array $data): ?CSejour
    {
        $nda = $this->message->getVenueAN($this->sender, $data);

        try {
            return (new SejourRepository(SejourRepository::STRATEGY_ONLY_NDA))
                ->setPatient($patient)
                ->setNDA($nda, $this->sender->_tag_sejour)
                ->find();
        } catch (Exception $exception) {
            $code = 'E1003';
            if ($exception instanceof SejourRepositoryException) {
                $code = ($exception->getId() === SejourRepositoryException::PATIENT_DIVERGENCE_FOUND) ? "E1004" : $code;
            }

            throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, $code, $patient);
        }
    }

    /**
     * @param CPatient $patient
     * @param array    $data
     *
     * @return string
     * @throws CHL7v2ExceptionError
     * @throws Exception
     */
    public function handleAppointments(CPatient $patient, array $data): string
    {
        if (!isset($this->sejour)) {
            $sejour = $data[CSejour::class] ?? null;
        }

        if ($sejour === null) {
            throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1003", $patient);
        }

        $this->sejour = $sejour;
        $this->patient = $patient;

        $mediuser = $this->searchPraticien($patient, $data);

        $prescription = $this->searchOrCreatePrescription($sejour);

        $first_data = reset($data['resources']);
        $AIG_nodes = $first_data['AIG'] ?? null;
        if (empty($AIG_nodes)) {
            throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1018", $patient);
        }

        $codes_warnings = [];
        $prescription_lines = [];
        foreach ($AIG_nodes as $index => $_AIG) {
            try {
                $category           = new CCategoryPrescription();
                $category->chapitre = $this->message->queryTextNode("AIG.5/CE.1", $_AIG);
                $category->nom      = $this->message->queryTextNode("AIG.4/CE.1", $_AIG);
                $category->loadMatchingObject();
                if (!$category->_id) {
                    if ($msg = $category->store()) {
                        throw (new CHL7v2ExceptionWarning('E1014'))
                            ->setComments($msg)
                            ->setPosition("AIG[$index]");
                    }
                }

                // element prescription
                $elt_presc                           = new CElementPrescription();
                $elt_presc->category_prescription_id = $category->_id;
                $elt_presc->libelle                  = $this->message->queryTextNode("AIG.3/CE.1", $_AIG);
                $elt_presc->bdm                      = CMedicamentProduit::getBase();
                $category->loadMatchingObject();
                if (!$elt_presc->_id) {
                    if ($msg = $elt_presc->store()) {
                        throw (new CHL7v2ExceptionWarning('E1015'))
                            ->setComments($msg)
                            ->setPosition("AIG[$index]");
                    }
                }

                $debut = explode(' ', $this->message->queryTextNode("SCH.11/TQ.4/TS.1", $data["SCH"]));
                $fin   = explode(' ', $this->message->queryTextNode("SCH.11/TQ.5/TS.1", $data["SCH"]));

                $presc_element_line                          = new CPrescriptionLineElement();
                $presc_element_line->prescription_id         = $prescription->_id;
                $presc_element_line->creator_id              = $mediuser->_id;
                $presc_element_line->element_prescription_id = $elt_presc->_id;
                $presc_element_line->praticien_id            = $mediuser->_id;
                $presc_element_line->debut                   = $debut[0] ?? '';
                $presc_element_line->time_debut              = $debut[1] ?? '';
                $presc_element_line->fin                     = $fin[0] ?? '';
                $presc_element_line->time_fin                = $fin[1] ?? '';
                $presc_element_line->signee                  = 1;

                if ($msg = $presc_element_line->store()) {
                    throw (new CHL7v2ExceptionWarning('E1016'))
                        ->setComments($msg)
                        ->setPosition("AIG[$index]");
                }

                $prescription_lines[] = $presc_element_line;
            } catch (CHL7v2ExceptionWarning $warning) {
                $codes_warnings[] = $warning->getWarning();
            }
        }

        // generate msg
        if (count($prescription_lines) > 0) {
            $codes = array_merge(
                ['I1005' => CAppUI::tr('CHL7Event-I1005', count($prescription_lines))],
                $codes_warnings
            );

            return $this->exchange_hl7v2->setAckAA($this->ack, $codes);
        } else {
            $codes = array_merge(["E1017"], $codes_warnings);

            return $this->exchange_hl7v2->setAckAA($this->ack, $codes);
        }
    }

    /**
     * Search praticien
     *
     * @param CPatient $patient
     * @param array    $data
     *
     * @return CMediusers
     * @throws CHL7v2ExceptionError
     * @throws Exception
     */
    protected function searchPraticien(CPatient $patient, array $data): CMediusers
    {
        $mediuser        = new CMediusers();
        $mediuser->inami = $this->message->queryTextNode("SCH.12/XCN.1", CMbArray::get($data, "SCH"));
        if ($mediuser->inami) {
            $mediuser->loadMatchingObject();
        }

        if (!$mediuser->_id) {
            throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1012", $patient);
        }

        return $mediuser;
    }

    /**
     * Search prescription for sejour
     *
     * @param CSejour $sejour
     *
     * @return CPrescription
     * @throws Exception
     */
    protected function searchOrCreatePrescription(CSejour $sejour): CPrescription
    {
        // Recherche de la prescription
        $prescription               = new CPrescription();
        $prescription->group_id     = $this->sender->group_id;
        $prescription->object_id    = $sejour->_id;
        $prescription->object_class = $sejour->_class;
        $prescription->type         = "sejour";
        $prescription->actif        = "1";
        $prescription->loadMatchingObject();

        if (!$prescription->_id) {
            if ($msg = $prescription->store()) {
                throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, ["E1013" => $msg], $this->patient);
            }
        }

        return $prescription;
    }
}
