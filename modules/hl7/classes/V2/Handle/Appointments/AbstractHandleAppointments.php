<?php

/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\V2\Handle\Appointments;

use Exception;
use Ox\Interop\Eai\CInteropSender;
use Ox\Interop\Hl7\CExchangeHL7v2;
use Ox\Interop\Hl7\CHL7Acknowledgment;
use Ox\Interop\Hl7\Exceptions\V2\CHL7v2ExceptionError;
use Ox\Interop\Hl7\V2\Handle\RecordAppointment;
use Ox\Interop\Hl7\V2\Handle\RecordPerson;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Sante400\CIdSante400;

abstract class AbstractHandleAppointments implements HandleAppointmentsInterface
{
    public const HANDLE_CONSULTATION         = 'consultation';
    public const HANDLE_ELEMENT_PRESCRIPTION = 'element_prescription';
    public const HANDLE_INTERVENTION         = 'intervention';


    protected CInteropSender    $sender;
    protected CExchangeHL7v2    $exchange_hl7v2;
    protected CHL7Acknowledgment $ack;
    protected RecordAppointment $message;

    public function __construct(RecordAppointment $message)
    {
        $this->message        = $message;
        $this->sender         = $message->_ref_sender;
        $this->exchange_hl7v2 = $message->_ref_exchange_hl7v2;
        $this->ack            = $message->getAck();
    }

    /**
     * @inheritDoc
     *
     * @param array $data
     *
     * @return CPatient
     * @throws CHL7v2ExceptionError
     * @throws Exception
     */
    public function handlePatient(array $data): CPatient
    {
        $patient = new CPatient();
        // Traitement du patient
        if ($this->sender->confHL7v2("handle-siu handle_patient_SIU")) {
            $hl7v2_record_person                      = new RecordPerson();
            $hl7v2_record_person->_ref_exchange_hl7v2 = $this->exchange_hl7v2;
            $msg_ack                                  = $hl7v2_record_person->handle($this->ack, $patient, $data);

            // Retour de l'acquittement si erreur sur le traitement du patient
            if ($this->exchange_hl7v2->statut_acquittement == "AR") {
                throw CHL7v2ExceptionError::setAckAR($msg_ack);
            }
        } else {
            $patientPI = $data['personIdentifiers']["PI"] ?? null;
            // Patient
            if (!$patientPI) {
                throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1001", $patient);
            }

            $IPP = CIdSante400::getMatch("CPatient", $this->sender->_tag_patient, $patientPI);
            // Patient non retrouv� par son IPP
            if (!$IPP->_id) {
                throw CHL7v2ExceptionError::ackAR($this->exchange_hl7v2, $this->ack, "E1002", $patient);
            }

            $patient->load($IPP->object_id);
        }

        // find patient is required !

        return $patient;
    }
}
