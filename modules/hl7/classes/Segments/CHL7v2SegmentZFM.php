<?php
/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Segments;

use Ox\Interop\Hl7\CHEvent;
use Ox\Interop\Hl7\CHL7v2Exception;
use Ox\Interop\Hl7\CHL7v2Segment;
use Ox\Interop\Hl7\Events\CHL7v2Event;
use Ox\Mediboard\Hospi\CAffectation;
use Ox\Mediboard\PlanningOp\CSejour;

/**
 * Class CHL7v2SegmentZFM
 * ZFM - Represents an HL7 ZFM message segment (Mouvement PMSI)
 */

class CHL7v2SegmentZFM extends CHL7v2Segment {

  /** @var string */
  public $name   = "ZFM";
  

  /** @var CSejour */
  public $sejour;

  /** @var CAffectation */
  public $curr_affectation;

    /**
     * Build ZFM segement
     *
     * @param CHL7v2Event $event Event
     * @param string|null $name Segment name
     *
     * @return void
     * @throws CHL7v2Exception
     */
  public function build(CHEvent $event, string $name = null): void
  {
      parent::build($event);

      $sejour = $this->sejour;
      $affectation = $this->curr_affectation;

      // ZFM-1: Mode d'entr�e PMSI
      $data[] = ($affectation && $affectation->_id && $affectation->mode_entree != null) ? $affectation->mode_entree : $sejour->mode_entree;

      // ZFM-2: Mode de sortie PMSI
      // normal - transfert - mutation - deces
      $data[] = ($affectation && $affectation->_id && $affectation->mode_sortie != null) ? $affectation->mode_sortie : $this->getModeSortie($sejour);

      // ZFM-3: Mode de provenance PMSI
      $data[] = ($affectation && $affectation->_id && $affectation->provenance != null) ? $affectation->provenance : $this->getModeProvenance($sejour);

      // ZFM-4: Mode de destination PMSI
      $destination = ($affectation && $affectation->_id && $affectation->destination != null) ? $affectation->destination : $sejour->destination;
      $data[] = $destination == "0" ? null : $destination;

      $this->fill($data);
  }
}
