<?php

/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Segments;

use Ox\Interop\Hl7\DelegatedObjectInterface;
use Ox\Interop\Hl7\Events\CHL7v2Event;

/**
 * Interface DelegatedObjectMapperInterface
 */
interface DelegatedObjectMapperInterface extends DelegatedObjectInterface
{
    /**
     * Build segment
     *
     * @param CHL7v2Event $event
     * @param string|null $name
     * @return void
     */
    public function build(CHL7v2Event $event, string $name = null): void;
}
