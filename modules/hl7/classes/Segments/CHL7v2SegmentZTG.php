<?php
/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Segments;

use Ox\Core\Module\CModule;
use Ox\Interop\Hl7\CHEvent;
use Ox\Interop\Hl7\CHL7v2Exception;
use Ox\Interop\Hl7\CHL7v2Segment;
use Ox\Interop\Hl7\CReceiverHL7v2;
use Ox\Interop\Hl7\Events\CHL7v2Event;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Galaxie\CGalaxie;
use Ox\Mediboard\Patients\CPatient;

/**
 * Class CHL7v2SegmentZTG
 * ZFV - Represents an HL7 ZTG message segment (TAMM-GALAXIE)
 */
class CHL7v2SegmentZTG extends CHL7v2Segment
{
    /** @var string */
    public $name = "ZTG";

    /** @var CConsultation|CPatient */
    public $object;

    /**
     * Build ZTG segment
     *
     * @param CHL7v2Event $event Event
     * @param string|null $name Segment name
     *
     * @return void
     * @throws CHL7v2Exception
     */
    public function build(CHEvent $event, string $name = null): void
    {
        parent::build($event);

        /** @var CReceiverHL7v2 $receiver */
        $receiver = $event->_receiver;

        if (CModule::getActive("galaxie")) {
            $galaxie = new CGalaxie();

            $this->fill($galaxie->generateSegmentZTG($this->object, $receiver->group_id));
            return;
        }
    }
}
