<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7;

use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Module\AbstractModuleCache;
use Ox\Interop\Hl7\ClassMap\HL7v2ClassMap;
use Psr\SimpleCache\InvalidArgumentException;

class CModuleCacheHL7 extends AbstractModuleCache
{
    public function getModuleName(): string
    {
        return 'hl7';
    }

    /**
     * @inheritdoc
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function clearSpecialActions(): void
    {
        parent::clearSpecialActions();

        // Delete cache resource map
        HL7v2ClassMap::clearCache();
    }
}
