<?php
/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Events\ADT;

use Ox\Core\CMbObject;
use Ox\Interop\Hl7\Events\HL7EventInterface;
use Ox\Mediboard\Patients\CPatient;

/**
 * Interface CHL7EventADT
 * Admit Discharge Transfer
 */
interface CHL7EventADT extends HL7EventInterface
{
    /**
     * Construct
     *
     * @return CHL7EventADT
     */
    function __construct();

    /**
     * Build event
     *
     * @param CMbObject $object Object
     *
     * @return void
     * @see parent::build()
     *
     */
    function build($object);

    /**
     * Build i18n segements
     *
     * @param CMbObject $object Object
     *
     * @return void
     * @see parent::buildI18nSegments()
     *
     */
    function buildI18nSegments($object);

    /**
     * Build specifics XtremSanté segments
     *
     * @param CPatient $patient
     * @return void
     */
    public function buildXtremSegments(CPatient $patient): void;
}
