<?php
/**
 * @package Mediboard\Hl7
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7;

use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CRequest;
use Ox\Core\CSetup;
use Ox\Core\CSQLDataSource;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\CInteropSender;
use Ox\Interop\Hl7\V2\ValueSet\CHL7v2TableEntry;
use Ox\Mediboard\System\CExchangeSourceAdvanced;

/**
 * @codeCoverageIgnore
 */
class CSetupHl7 extends CSetup
{
    function __construct()
    {
        parent::__construct();

        $this->mod_name = "hl7";
        $this->makeRevision("0.0");

        // create Table entry / description
        $this->addMethod('createTableHL7Entries');

        $this->makeRevision("0.01");

        $this->addMethod("populateFromSQLFile");

        $this->makeRevision("0.02");

        $query = "CREATE TABLE `hl7_config` (
                `hl7_config_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `assigning_authority_namespace_id` VARCHAR (255),
                `assigning_authority_universal_id` VARCHAR (255),
                `assigning_authority_universal_type_id` VARCHAR (255),
                `sender_id` INT (11) UNSIGNED,
                `sender_class` ENUM ('CSenderFTP','CSenderSOAP')
              ) /*! ENGINE=MyISAM */;";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config` 
              ADD INDEX (`sender_id`);";
        $this->addQuery($query);

        $this->makeRevision("0.06");
        $query = "CREATE TABLE `source_mllp` (
              `source_mllp_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
              `port` INT (11) DEFAULT '7001',
              `name` VARCHAR (255) NOT NULL,
              `role` ENUM ('prod','qualif') NOT NULL DEFAULT 'qualif',
              `host` TEXT NOT NULL,
              `user` VARCHAR (255),
              `password` VARCHAR (50),
              `type_echange` VARCHAR (255),
              `active` ENUM ('0','1') NOT NULL DEFAULT '1',
              `loggable` ENUM ('0','1') NOT NULL DEFAULT '1'
    ) /*! ENGINE=MyISAM */;";
        $this->addQuery($query);

        $this->makeRevision("0.13");

        $query = "CREATE TABLE `sender_mllp` (
                `sender_mllp_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `user_id` INT (11) UNSIGNED,
                `nom` VARCHAR (255) NOT NULL,
                `libelle` VARCHAR (255),
                `group_id` INT (11) UNSIGNED NOT NULL,
                `actif` ENUM ('0','1') NOT NULL DEFAULT '0'
              ) /*! ENGINE=MyISAM */;";
        $this->addQuery($query);

        $query = "ALTER TABLE `sender_mllp` 
                ADD INDEX (`user_id`),
                ADD INDEX (`group_id`);";
        $this->addQuery($query);

        $this->makeRevision("0.14");

        $query = "ALTER TABLE `hl7_config` 
                CHANGE `sender_class` `sender_class` ENUM ('CSenderFTP','CSenderSOAP','CSenderMLLP');";
        $this->addQuery($query);

        $this->makeRevision("0.19");

        $query = "ALTER TABLE `hl7_config` 
                ADD `handle_mode` ENUM ('normal','simple') DEFAULT 'normal';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config` 
                CHANGE `sender_class` `sender_class` VARCHAR (80);";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config` 
                ADD `get_NDA` ENUM ('PID_18','PV1_19') DEFAULT 'PID_18';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config` 
              ADD `handle_PV1_10` ENUM ('discipline','service') DEFAULT 'discipline',
              CHANGE `get_NDA` `handle_NDA` ENUM ('PID_18','PV1_19') DEFAULT 'PID_18';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config` 
                ADD `encoding` ENUM ('UTF-8','ISO-8859-1') DEFAULT 'UTF-8';";
        $this->addQuery($query);

        $this->makeRevision("0.23");

        $query = "ALTER TABLE `sender_mllp` 
                ADD `save_unsupported_message` ENUM ('0','1') DEFAULT '1',
                ADD `create_ack_file` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.27");

        $query = "ALTER TABLE `hl7_config` 
                ADD `handle_NSS` ENUM ('PID_3','PID_19') DEFAULT 'PID_3';";
        $this->addQuery($query);

        $this->makeRevision("0.28");

        $query = "ALTER TABLE `hl7_config` 
                ADD `iti30_option_merge` ENUM ('0','1') DEFAULT '1',
                ADD `iti30_option_link_unlink` ENUM ('0','1') DEFAULT '0',
                ADD `iti31_in_outpatient_emanagement` ENUM ('0','1') DEFAULT '1',
                ADD `iti31_pending_event_management` ENUM ('0','1') DEFAULT '0',
                ADD `iti31_advanced_encounter_management` ENUM ('0','1') DEFAULT '1',
                ADD `iti31_temporary_patient_transfer_tracking` ENUM ('0','1') DEFAULT '0',
                ADD `iti31_historic_movement` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.29");
        $query = "ALTER TABLE `source_mllp` 
              ADD `ssl_enabled` ENUM ('0','1') NOT NULL DEFAULT '0',
              ADD `ssl_certificate` VARCHAR (255),
              ADD `ssl_passphrase` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("0.30");
        $query = "ALTER TABLE `hl7_config` 
              ADD `strict_segment_terminator` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("0.31");
        $query = "ALTER TABLE `sender_mllp` 
                ADD `delete_file` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.32");
        $query = "ALTER TABLE `hl7_config` 
                ADD `handle_PV1_14` ENUM ('admit_source','ZFM') DEFAULT 'admit_source',
                ADD `handle_PV1_36` ENUM ('discharge_disposition','ZFM') DEFAULT 'discharge_disposition';";
        $this->addQuery($query);

        $this->makeRevision("0.33");
        $query = "ALTER TABLE `hl7_config` 
                ADD `purge_idex_movements` ENUM ('0','1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config` 
                ADD `repair_patient` ENUM ('0','1') DEFAULT '1',
                ADD `control_date` ENUM ('permissif','strict') DEFAULT 'strict';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config` 
              ADD `handle_PV1_3` ENUM ('name','config_value','idex') DEFAULT 'name';";
        $this->addQuery($query);

        $this->addDependency("ihe", "0.26");

        $this->makeRevision("0.37");
        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `RAD48_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5') DEFAULT '2.5';";
        $this->addQuery($query);

        $this->makeRevision("0.38");

        $query = "ALTER TABLE `exchange_ihe`
                CHANGE `object_class` `object_class` VARCHAR (80);";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_ihe` 
                ADD `reprocess` TINYINT (4) UNSIGNED DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("0.40");
        $query = "ALTER TABLE `receiver_ihe_config` 
                ADD `build_telephone_number` ENUM ('XTN_1','XTN_12') DEFAULT 'XTN_12';";
        $this->addQuery($query);

        $this->makeRevision("0.41");
        $query = "ALTER TABLE `hl7_config` 
                ADD `handle_telephone_number` ENUM ('XTN_1','XTN_12') DEFAULT 'XTN_12';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config` 
                ADD `handle_PID_31` ENUM ('avs','none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("0.43");

        $query = "ALTER TABLE `receiver_ihe_config` 
                ADD `build_PID_31` ENUM ('avs','none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("0.44");
        $query = "ALTER TABLE `hl7_config`
                ADD `segment_terminator` ENUM ('CR','LF','CRLF')";
        $this->addQuery($query);

        $this->makeRevision("0.45");
        $query = "ALTER TABLE `receiver_ihe_config` 
                ADD `build_PID_34` ENUM ('finess','actor') DEFAULT 'finess';";
        $this->addQuery($query);

        $this->makeRevision("0.46");
        $query = "ALTER TABLE `receiver_ihe_config` 
                ADD `build_PV2_45` ENUM ('operation','none') DEFAULT 'none';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_ihe_config` 
                ADD `build_cellular_phone` ENUM ('PRN','ORN') DEFAULT 'PRN';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_ihe_config` 
                ADD `send_first_affectation` ENUM ('A02','Z99') DEFAULT 'Z99';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `ITI21_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5') DEFAULT '2.5',
                ADD `ITI22_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5') DEFAULT '2.5';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `build_PV1_26` ENUM ('movement_id','none') DEFAULT 'none';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `send_assigning_authority` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.52");
        $query = "ALTER TABLE `hl7_config`
                ADD `receiving_application` VARCHAR (255),
                ADD `receiving_facility` VARCHAR (255);";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_PV2_12` ENUM ('libelle','none') DEFAULT 'libelle';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config`
                ADD `send_assigning_authority` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config`
                ADD `send_self_identifier` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `send_self_identifier` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config`
                ADD `send_area_local_number` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("0.57");
        $query = "ALTER TABLE `source_mllp`
                CHANGE `password` `password` VARCHAR (255),
                ADD `iv` VARCHAR (16) AFTER `password`,
                ADD `iv_passphrase` VARCHAR (16) AFTER `ssl_passphrase`;";
        $this->addQuery($query);

        $this->makeRevision("0.58");
        $query = "ALTER TABLE `hl7_config`
                ADD `handle_PV1_7` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.59");
        $query = "ALTER TABLE `hl7_config`
                ADD `check_receiving_application_facility` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_PV1_20` ENUM ('old_presta','none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("0.61");
        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `send_update_patient_information` ENUM ('A08','A31') DEFAULT 'A31';";
        $this->addQuery($query);

        $this->makeRevision("0.62");
        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `modification_admit_code` ENUM ('A08','Z99') DEFAULT 'Z99';";
        $this->addQuery($query);

        $this->makeRevision("0.63");
        $query = "ALTER TABLE `receiver_ihe_config`
                CHANGE `build_PID_34` `build_PID_34` ENUM ('finess','actor','domain') DEFAULT 'finess';";
        $this->addQuery($query);

        $this->makeRevision("0.64");

        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `country_code` ENUM ('FRA');";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config`
                ADD `country_code` ENUM ('FRA');";
        $this->addQuery($query);

        $this->makeRevision("0.65");

        $query = "ALTER TABLE `receiver_ihe_config`
                CHANGE `country_code` `country_code` ENUM ('FRA','INT');";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config`
                CHANGE `country_code` `country_code` ENUM ('FRA','INT');";
        $this->addQuery($query);

        $this->makeRevision("0.66");

        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `build_other_residence_number` ENUM ('ORN','WPN') DEFAULT 'ORN';";
        $this->addQuery($query);

        $this->makeRevision("0.67");

        $query = "CREATE TABLE `receiver_hl7_v3` (
                `receiver_hl7_v3_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `nom` VARCHAR (255) NOT NULL,
                `libelle` VARCHAR (255),
                `group_id` INT (11) UNSIGNED NOT NULL DEFAULT '0',
                `actif` ENUM ('0','1') NOT NULL DEFAULT '0'
              )/*! ENGINE=MyISAM */;";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7_v3`
                ADD INDEX (`group_id`);";
        $this->addQuery($query);

        $this->makeRevision("0.68");

        $query = "CREATE TABLE `exchange_hl7v3` (
                `exchange_hl7v3_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `identifiant_emetteur` VARCHAR (255),
                `initiateur_id` INT (11) UNSIGNED,
                `group_id` INT (11) UNSIGNED NOT NULL DEFAULT '0',
                `date_production` DATETIME NOT NULL,
                `sender_id` INT (11) UNSIGNED,
                `sender_class` ENUM ('CSenderFTP','CSenderSOAP','CSenderFileSystem'),
                `receiver_id` INT (11) UNSIGNED,
                `type` VARCHAR (255),
                `sous_type` VARCHAR (255),
                `date_echange` DATETIME,
                `message_content_id` INT (11) UNSIGNED,
                `acquittement_content_id` INT (11) UNSIGNED,
                `statut_acquittement` VARCHAR (255),
                `message_valide` ENUM ('0','1') DEFAULT '0',
                `acquittement_valide` ENUM ('0','1') DEFAULT '0',
                `id_permanent` VARCHAR (255),
                `object_id` INT (11) UNSIGNED,
                `object_class` ENUM ('CPatient','CSejour','COperation','CAffectation','CConsultation'),
                `reprocess` TINYINT (4) UNSIGNED DEFAULT '0'
              )/*! ENGINE=MyISAM */;";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v3`
                ADD INDEX (`initiateur_id`),
                ADD INDEX (`group_id`),
                ADD INDEX (`date_production`),
                ADD INDEX (`sender_id`),
                ADD INDEX (`receiver_id`),
                ADD INDEX (`date_echange`),
                ADD INDEX (`message_content_id`),
                ADD INDEX (`acquittement_content_id`),
                ADD INDEX (`object_id`);";
        $this->addQuery($query);

        $this->makeRevision("0.69");

        $query = "ALTER TABLE `receiver_ihe_config`
                ADD `send_change_after_admit` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.70");
        $this->addDependency("ihe", "0.28");

        $query = "UPDATE user_log
                SET user_log.object_class = 'CReceiverHL7v2'
                WHERE user_log.object_class = 'CReceiverIHE';";
        $this->addQuery($query);

        $query = "UPDATE user_log
                SET user_log.object_class = 'CReceiverHL7v2Config'
                WHERE user_log.object_class = 'CReceiverIHEConfig';";
        $this->addQuery($query);

        $this->makeRevision("0.71");
        $this->addDependency("eai", "0.02");

        $query = "UPDATE message_supported
              SET message_supported.object_class = 'CReceiverHL7v2'
              WHERE message_supported.object_class = 'CReceiverIHE';";
        $this->addQuery($query);


        $this->makeRevision("0.72");

        $query = "ALTER TABLE `receiver_hl7v2`
                ADD `OID` VARCHAR (255);";
        $this->addQuery($query);

        $query = "RENAME TABLE `receiver_hl7_v3`
                TO `receiver_hl7v3`;";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v3`
                CHANGE `receiver_hl7_v3_id` `receiver_hl7v3_id` INT (11) UNSIGNED NOT NULL AUTO_INCREMENT;";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v3`
                ADD `OID` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("0.73");

        $query = "ALTER TABLE `hl7_config`
                ADD `ignore_fields` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("0.74");

        $query = "ALTER TABLE `exchange_hl7v3`
                CHANGE `object_class` `object_class` ENUM ('CPatient','CSejour','COperation','CAffectation','CConsultation','CFile','CCompteRendu');";
        $this->addQuery($query);

        $this->makeRevision("0.75");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_PID_19` ENUM ('matricule','none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("0.76");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_actor_identifier` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("0.77");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_PV1_11` ENUM ('uf_medicale','none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("0.78");
        $query = "ALTER TABLE `receiver_hl7v2`
                ADD `synchronous` ENUM ('0','1') NOT NULL DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.79");
        $query = "ALTER TABLE `receiver_hl7v3`
                ADD `synchronous` ENUM ('0','1') NOT NULL DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.80");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `iti31_in_outpatient_emanagement` `iti31_in_outpatient_management` ENUM ('0','1') DEFAULT '1',
                ADD `create_grossesse` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.81");
        $query = "ALTER TABLE `hl7_config`
                CHANGE `iti31_in_outpatient_emanagement` `iti31_in_outpatient_management` ENUM ('0','1') DEFAULT '1',
                ADD `create_grossesse` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("0.82");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_PID_6` ENUM ('nom_naissance','none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("0.83");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `RAD3_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5') DEFAULT '2.5'";
        $this->addQuery($query);

        $this->makeRevision("0.84");
        $query = "ALTER TABLE `exchange_hl7v2`
                CHANGE `object_class` `object_class` ENUM ('CPatient','CSejour','COperation','CAffectation','COperation','CConsultation','CPrescriptionLineElement');";
        $this->addQuery($query);

        $this->makeRevision("0.85");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_PID_11` ENUM ('simple','multiple') DEFAULT 'multiple',
                ADD `build_PID_13` ENUM ('simple','multiple') DEFAULT 'multiple',
                ADD `build_PV1_5` ENUM ('NPA','None') DEFAULT 'NPA',
                ADD `build_PV1_17` ENUM ('praticien','None') DEFAULT 'praticien',
                ADD `build_PV1_19` ENUM ('normal','simple') DEFAULT 'normal';";
        $this->addQuery($query);

        $this->makeRevision("0.86");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `modification_before_admit` ENUM ('0','1') DEFAULT '1'";
        $this->addQuery($query);

        $this->makeRevision("0.87");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_PID_18` ENUM ('normal','simple') DEFAULT 'normal',
                ADD `send_patient_with_visit` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("0.88");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_identifier_authority` ENUM ('normal','PI_AN') DEFAULT 'normal',
                CHANGE `build_PV1_5` `build_PV1_5` ENUM ('NPA','none') DEFAULT 'NPA',
                CHANGE `build_PV1_17` `build_PV1_17` ENUM ('praticien','none') DEFAULT 'praticien';";
        $this->addQuery($query);

        $this->makeRevision("0.89");

        $query = "ALTER TABLE `hl7_config`
                ADD `search_master_IPP` ENUM ('0','1') DEFAULT '0',
                ADD `search_master_NDA` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("0.90");

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_PV1_50` ENUM ('sejour_id','none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("0.91");

        $query = "ALTER TABLE `receiver_hl7v2_config`
               ADD `send_patient_with_current_admit` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("0.92");

        $query = "ALTER TABLE `hl7_config`
                ADD `bypass_validating` ENUM ('0','1') DEFAULT '0'";
        $this->addQuery($query);

        $this->makeRevision("0.93");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `mode_identito_vigilance` ENUM ('light','medium','strict') DEFAULT 'light'";
        $this->addQuery($query);

        $this->makeRevision("0.94");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_no_facturable` ENUM ('0','1') DEFAULT '1'";
        $this->addQuery($query);

        $this->makeRevision("0.95");

        $query = "CREATE TABLE `receiver_hl7v3_config` (
                `receiver_hl7v3_config_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `object_id` INT (11) UNSIGNED,
                `use_receiver_oid` ENUM ('0','1') DEFAULT '0'
              )/*! ENGINE=MyISAM */;";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v3_config`
                ADD INDEX (`object_id`);";

        $this->addQuery($query);

        $this->makeRevision("0.96");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_a42_onmerge` ENUM ('0','1') DEFAULT '0';";

        $this->addQuery($query);

        $this->makeRevision("0.97");

        $query = "ALTER TABLE `source_mllp`
                ADD `libelle` VARCHAR (255);";

        $this->addQuery($query);

        $this->makeRevision("0.98");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_ZBE_7` ENUM ('medicale','soins') DEFAULT 'medicale',
                ADD `build_ZBE_8` ENUM ('medicale','soins') DEFAULT 'soins';";
        $this->addQuery($query);

        $this->makeRevision("0.99");

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_ZBE_7` ENUM ('medicale','soins') DEFAULT 'medicale',
                ADD `handle_ZBE_8` ENUM ('medicale','soins') DEFAULT 'soins';";

        $this->addQuery($query);

        $this->makeRevision("1.00");

        $query = "ALTER TABLE `hl7_config`
                ADD `ins_integrated` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.01");

        $query = "ALTER TABLE `receiver_hl7v2`
                ADD `monitor_sources` ENUM ('0','1') NOT NULL DEFAULT '1';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v3`
                ADD `monitor_sources` ENUM ('0','1') NOT NULL DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.02");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `build_PID_34` `build_PID_3_4` ENUM ('finess','actor','domain') DEFAULT 'finess';";
        $this->addQuery($query);

        $this->makeRevision("1.03");
        $query = "ALTER TABLE `hl7_config`
                ADD `manage_npa` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_PV1_19_identifier_authority` ENUM ('AN','RI','VN') DEFAULT 'RI';";
        $this->addQuery($query);

        $this->makeRevision("1.04");
        $query = "ALTER TABLE `hl7_config`
                ADD `handle_PV1_3_null` VARCHAR (255);";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_PV1_3_1_default` VARCHAR (255),
                ADD `build_PV1_3_1` ENUM ('UF','service') DEFAULT 'UF';";
        $this->addQuery($query);

        $this->makeRevision("1.05");

        $query = "ALTER TABLE `hl7_config`
                ADD `change_filler_placer` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.06");
        $query = "CREATE TABLE `hl7_transformation` (
                `hl7_transformation_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `actor_id` INT (11) UNSIGNED,
                `actor_class` VARCHAR (80),
                `profil` VARCHAR (255),
                `message` VARCHAR (255),
                `version` VARCHAR (255),
                `extension` VARCHAR (255),
                `component` VARCHAR (255),
                `action` ENUM ('add','modify','move','delete')
              )/*! ENGINE=MyISAM */;";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_transformation`
                ADD INDEX (`actor_id`);";
        $this->addQuery($query);

        $this->makeRevision("1.07");

        $query = "DROP TABLE `hl7_transformation`";
        $this->addQuery($query);

        $this->makeRevision("1.08");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_expected_discharge_with_affectation` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.09");

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_OBR_identity_identifier` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("1.10");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `build_PV1_10` `build_PV1_10` ENUM ('discipline','service','finess') DEFAULT 'discipline';";
        $this->addQuery($query);

        $query = "ALTER TABLE `hl7_config`
                CHANGE `handle_PV1_10` `handle_PV1_10` ENUM ('discipline','service','finess') DEFAULT 'discipline';";
        $this->addQuery($query);

        $this->makeRevision("1.11");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_child_admit` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.12");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `ITI9_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5') DEFAULT '2.5';";
        $this->addQuery($query);

        $this->makeRevision("1.13");

        $query = "ALTER TABLE `hl7_config`
                ADD `control_identifier_type_code` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.14");
        $query = "ALTER TABLE `exchange_hl7v2`
                ADD `master_idex_missing` ENUM ('0','1') DEFAULT '0',
                ADD INDEX (`master_idex_missing`);";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v3`
                ADD `master_idex_missing` ENUM ('0','1') DEFAULT '0',
                ADD INDEX (`master_idex_missing`);";
        $this->addQuery($query);

        $this->makeRevision("1.15");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_not_master_IPP` ENUM ('0','1') DEFAULT '1',
                ADD `send_not_master_NDA` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.16");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `build_PID_18` `build_PID_18` ENUM ('normal','simple', 'none') DEFAULT 'normal';";
        $this->addQuery($query);

        $this->makeRevision("1.17");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `send_no_facturable` `send_no_facturable` ENUM ('0','1','2') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.18");

        $query = "ALTER TABLE `hl7_config`
                ADD `associate_category_to_a_file` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.19");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `build_PID_18` `build_PID_18` ENUM ('normal','simple', 'sejour_id', 'none') DEFAULT 'normal';";
        $this->addQuery($query);

        $this->makeRevision("1.20");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_insurance` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.22");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `ITI30_HL7_version` `ITI30_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','FR_2.3','FR_2.4','FR_2.5','FR_2.6') DEFAULT '2.5',
                CHANGE `ITI31_HL7_version` `ITI31_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','FR_2.3','FR_2.4','FR_2.5','FR_2.6') DEFAULT '2.5';";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI30_HL7_version` = 'FR_2.5'
                WHERE `ITI30_HL7_version` = 'FR_2.1' OR `ITI30_HL7_version` = 'FR_2.2' OR `ITI30_HL7_version` = 'FR_2.3';";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI31_HL7_version` = 'FR_2.5'
                WHERE `ITI31_HL7_version` = 'FR_2.1' OR `ITI31_HL7_version` = 'FR_2.2' OR `ITI31_HL7_version` = 'FR_2.3';";
        $this->addQuery($query);

        $this->makeRevision("1.23");

        $query = "ALTER TABLE `exchange_hl7v2`
                CHANGE `object_class` `object_class` VARCHAR (80) NOT NULL;";

        $this->addQuery($query);

        $this->makeRevision("1.24");

        $query = "ALTER TABLE `exchange_hl7v2`
                CHANGE `object_class` `object_class` VARCHAR (80);";

        $this->addQuery($query);

        $this->makeRevision("1.25");
        $query = "ALTER TABLE `hl7_config`
                ADD `create_user_to_patient` ENUM ('0','1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.26");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `build_PV1_5` `build_PV1_5` ENUM ('NPA', 'sejour_id', 'none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("1.28");
        $query = "ALTER TABLE `hl7_config`
                ADD `handle_portail_patient` ENUM ('0','1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.29");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_sejour_to_mbdmp` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.30");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `send_sejour_to_mbdmp` `send_evenement_to_mbdmp` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.31");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_not_price_indicator_for_birth` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.32");
        $this->addDefaultConfig("hl7 CHL7 sending_application", "hl7 sending_application");
        $this->addDefaultConfig("hl7 CHL7 sending_facility", "hl7 sending_facility");
        $this->addDefaultConfig("hl7 CHL7 assigning_authority_namespace_id", "hl7 assigning_authority_namespace_id");
        $this->addDefaultConfig("hl7 CHL7 assigning_authority_universal_id", "hl7 assigning_authority_universal_id");
        $this->addDefaultConfig(
            "hl7 CHL7 assigning_authority_universal_type_id",
            "hl7 assigning_authority_universal_type_id"
        );

        $this->makeRevision("1.33");
        $query = "ALTER TABLE `receiver_hl7v2`
                CHANGE `actif` `actif` ENUM ('0','1') NOT NULL DEFAULT '1',
                ADD `role` ENUM ('prod','qualif') NOT NULL DEFAULT 'prod';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v3`
                CHANGE `actif` `actif` ENUM ('0','1') NOT NULL DEFAULT '1',
                ADD `role` ENUM ('prod','qualif') NOT NULL DEFAULT 'prod';";
        $this->addQuery($query);

        $this->makeRevision("1.34");
        $query = "ALTER TABLE `sender_mllp`
                CHANGE `actif` `actif` ENUM ('0','1') NOT NULL DEFAULT '1',
                ADD `role` ENUM ('prod','qualif') NOT NULL DEFAULT 'prod';";
        $this->addQuery($query);

        $this->makeRevision("1.35");

        $query = "ALTER TABLE `receiver_hl7v2`
                ADD `exchange_format_delayed` SMALLINT (4) UNSIGNED DEFAULT '60';";
        $this->addQuery($query);
        $query = "ALTER TABLE `receiver_hl7v3`
                ADD `exchange_format_delayed` SMALLINT (4) UNSIGNED DEFAULT '60';";
        $this->addQuery($query);
        $query = "ALTER TABLE `sender_mllp`
                ADD `exchange_format_delayed` SMALLINT (4) UNSIGNED DEFAULT '60';";
        $this->addQuery($query);

        $this->makeRevision("1.36");
        $query = "ALTER TABLE `exchange_hl7v2`
                ADD INDEX( `receiver_id`, `date_echange`),
                ADD INDEX( `sender_id`, `sender_class`, `date_echange`);";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v3`
                ADD INDEX( `receiver_id`, `date_echange`),
                ADD INDEX( `sender_id`, `sender_class`, `date_echange`);";
        $this->addQuery($query);

        $this->makeRevision("1.37");
        $query = "ALTER TABLE `hl7_config`
                ADD `ack_severity_mode` ENUM ('IWE','W','E','I') NOT NULL DEFAULT 'IWE';";
        $this->addQuery($query);

        $this->makeRevision("1.38");
        $query = "ALTER TABLE `hl7_config`
                ADD `handle_patient_ITI_31` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.39");
        $query = "ALTER TABLE `hl7_config`
                ADD `check_similar_patient` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.40");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `ITI30_HL7_version` `ITI30_HL7_version`
                ENUM( '2.1', '2.2', '2.3', '2.3.1', '2.4', '2.5', 'FR_2.3', 'FR_2.4', 'FR_2.5', 'FR_2.6', 'FRA_2.3', 'FRA_2.4', 'FRA_2.5', 'FRA_2.6' )";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI30_HL7_version` = 'FRA_2.3'
                WHERE `ITI30_HL7_version` = 'FR_2.3'";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI30_HL7_version` = 'FRA_2.4'
                WHERE `ITI30_HL7_version` = 'FR_2.4'";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI30_HL7_version` = 'FRA_2.5'
                WHERE `ITI30_HL7_version` = 'FR_2.5'";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI30_HL7_version` = 'FRA_2.6'
                WHERE `ITI30_HL7_version` = 'FR_2.6'";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `ITI30_HL7_version` `ITI30_HL7_version`
                ENUM( '2.1', '2.2', '2.3', '2.3.1', '2.4', '2.5', 'FRA_2.3', 'FRA_2.4', 'FRA_2.5', 'FRA_2.6' )";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `ITI31_HL7_version` `ITI31_HL7_version`
                ENUM( '2.1', '2.2', '2.3', '2.3.1', '2.4', '2.5', 'FR_2.3', 'FR_2.4', 'FR_2.5', 'FR_2.6', 'FRA_2.3', 'FRA_2.4', 'FRA_2.5', 'FRA_2.6' )";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI31_HL7_version` = 'FRA_2.3'
                WHERE `ITI31_HL7_version` = 'FR_2.3'";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI31_HL7_version` = 'FRA_2.4'
                WHERE `ITI31_HL7_version` = 'FR_2.4'";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI31_HL7_version` = 'FRA_2.5'
                WHERE `ITI31_HL7_version` = 'FR_2.5'";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v2_config`
                SET `ITI31_HL7_version` = 'FRA_2.6'
                WHERE `ITI31_HL7_version` = 'FR_2.6'";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v2_config`
                CHANGE `ITI31_HL7_version` `ITI31_HL7_version`
                ENUM( '2.1', '2.2', '2.3', '2.3.1', '2.4', '2.5', 'FRA_2.3', 'FRA_2.4', 'FRA_2.5', 'FRA_2.6' )";
        $this->addQuery($query);

        $this->makeRevision("1.41");

        $query = "UPDATE `message_supported`
                SET `message` = CONCAT( `message` , 'A' )
                WHERE `message` LIKE '%\\_FR'";
        $this->addQuery($query);

        $this->makeRevision("1.43");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `change_idex_for_admit` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.44");

        $query = "ALTER TABLE `hl7_config`
                ADD `create_duplicate_patient` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.45");

        $query = "ALTER TABLE `receiver_hl7v2_config`
               ADD `build_OBX_2` ENUM ('AD','CE','CF','CK','CN','CP','CX','DT','ED','FT','MO','NM','PN','RP','SN',
              'ST','TM','TN','TS','TX','XAD','XCN','XON','XPN','XTN') DEFAULT 'ED';";
        $this->addQuery($query);

        $this->makeRevision("1.46");

        $query = "ALTER TABLE `exchange_hl7v2`
                ADD `emptied` ENUM('0', '1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $query = "UPDATE `exchange_hl7v2`
                SET `emptied` = '1'
                WHERE `message_content_id` IS NULL
                AND  `acquittement_content_id` IS NULL";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v2`
                ADD INDEX `emptied_production` (`emptied`, `date_production`);";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v3`
                ADD `emptied` ENUM('0', '1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $query = "UPDATE `exchange_hl7v3`
                SET `emptied` = '1'
                WHERE `message_content_id` IS NULL
                AND  `acquittement_content_id` IS NULL";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v3`
                ADD INDEX `emptied_production` (`emptied`, `date_production`);";
        $this->addQuery($query);

        $this->makeRevision("1.47");

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_context_url` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("1.48");

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_PV1_8` ENUM ('adressant','traitant') DEFAULT 'adressant',
                ADD `handle_PV1_9` ENUM ('null','famille') DEFAULT 'null';";
        $this->addQuery($query);

        $this->makeRevision("1.49");

        $query = "ALTER TABLE `exchange_hl7v2`
                ADD `response_datetime` DATETIME;";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v3`
                ADD `response_datetime` DATETIME;";
        $this->addQuery($query);

        $this->makeRevision("1.50");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                CHANGE `ITI30_HL7_version` `ITI30_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','FRA_2.3','FRA_2.4','FRA_2.5','FRA_2.6') DEFAULT '2.5',
                CHANGE `ITI31_HL7_version` `ITI31_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','FRA_2.3','FRA_2.4','FRA_2.5','FRA_2.6') DEFAULT '2.5',
                CHANGE `RAD3_HL7_version` `RAD3_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1') DEFAULT '2.5',
                CHANGE `RAD48_HL7_version` `RAD48_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1') DEFAULT '2.5',
                CHANGE `ITI21_HL7_version` `ITI21_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1') DEFAULT '2.5',
                CHANGE `ITI22_HL7_version` `ITI22_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1') DEFAULT '2.5',
                CHANGE `ITI9_HL7_version` `ITI9_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1') DEFAULT '2.5',
                CHANGE `modification_admit_code` `modification_admit_code` ENUM ('A08','Z99') DEFAULT 'Z99',
                CHANGE `build_PID_6` `build_PID_6` ENUM ('nom_naissance','none') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("1.51");

        $query = "ALTER TABLE `exchange_hl7v3`
                CHANGE `date_echange` `send_datetime` DATETIME;";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v2`
                CHANGE `date_echange` `send_datetime` DATETIME;";
        $this->addQuery($query);

        $this->makeRevision("1.52");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                ADD `build_PV1_19_idex_tag` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("1.53");
        $this->addDependency('eai', '0.21');

        $query = "UPDATE `message_supported`
                SET `profil` = CONCAT( `profil` , 'A' )
                WHERE `profil` LIKE '%\\FR'";
        $this->addQuery($query);

        $this->makeRevision("1.54");

        $query = "ALTER TABLE `hl7_config`
                ADD `define_date_category_name` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.55");

        $query = "ALTER TABLE `hl7_config`
                ADD `control_nda_target_document` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.56");

        $query = "CREATE TABLE `log_modification_exchange` (
                `log_modification_exchange_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `content_id` INT (11) UNSIGNED NOT NULL,
                `content_class` ENUM ('CContentTabular') NOT NULL,
                `user_id` INT (11) UNSIGNED NOT NULL,
                `datetime_update` DATETIME NOT NULL,
                `data_update` TEXT NOT NULL
              )/*! ENGINE=MyISAM */;
              ALTER TABLE `log_modification_exchange` 
                ADD INDEX (`content_id`),
                ADD INDEX (`user_id`),
                ADD INDEX (`datetime_update`);";
        $this->addQuery($query);

        $this->makeRevision("1.57");

        $query = "ALTER TABLE `exchange_hl7v2`
                ADD INDEX `ots` (`object_class`, `type`, `sous_type`);";
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v3`
                ADD INDEX `ots` (`object_class`, `type`, `sous_type`);";
        $this->addQuery($query);

        $this->makeRevision("1.58");

        $query = "ALTER TABLE `source_mllp` 
                ADD `delete_file` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.59");
        $query = "ALTER TABLE `source_mllp` 
                DROP `delete_file`;";
        $this->addQuery($query);

        $this->makeRevision("1.60");
        $query = "ALTER TABLE `hl7_config`
                ADD `handle_OBX_photo_patient` ENUM ('0','1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.61");
        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `build_fields_format` ENUM ('normal','uppercase') DEFAULT 'normal';";
        $this->addQuery($query);

        $this->makeRevision("1.62");
        $query = "ALTER TABLE `hl7_config`
                ADD `exclude_not_collide_exte` ENUM ('0','1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.63");
        $query = "ALTER TABLE `hl7_config`
                ADD `force_attach_doc_admit` ENUM ('0','1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.64");
        $query = "ALTER TABLE `hl7_config`
                ADD `force_attach_doc_patient` ENUM ('0','1') NOT NULL DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.65");
        $query = "ALTER TABLE `hl7_config`
                ADD `id_category_patient` INT (11);";
        $this->addQuery($query);

        $this->makeRevision("1.66");
        $query = "ALTER TABLE `hl7_config` 
                ADD `object_attach_OBX` ENUM ('CPatient','CSejour','CMbObject') DEFAULT 'CMbObject';";
        $this->addQuery($query);

        $this->makeRevision("1.67");

        $this->addMethod("changeAttachObjectOBX");

        $query = "ALTER TABLE `hl7_config`
                DROP `force_attach_doc_admit`,
                DROP `force_attach_doc_patient`;";
        $this->addQuery($query);

        $this->makeRevision("1.69");

        $query = "ALTER TABLE `hl7_config` 
                CHANGE `object_attach_OBX` `object_attach_OBX` ENUM ('CPatient','CSejour','COperation','CMbObject') DEFAULT 'CMbObject';";
        $this->addQuery($query);

        $this->makeRevision("1.70");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                CHANGE `ITI30_HL7_version` `ITI30_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','FRA_2.3','FRA_2.4','FRA_2.5','FRA_2.6','FRA_2.7','FRA_2.8','FRA_2.9') DEFAULT '2.5',
                CHANGE `ITI31_HL7_version` `ITI31_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','FRA_2.3','FRA_2.4','FRA_2.5','FRA_2.6','FRA_2.7','FRA_2.8','FRA_2.9') DEFAULT '2.5',
                CHANGE `RAD3_HL7_version` `RAD3_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7') DEFAULT '2.5',
                CHANGE `RAD48_HL7_version` `RAD48_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7') DEFAULT '2.5',
                CHANGE `ITI21_HL7_version` `ITI21_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7') DEFAULT '2.5',
                CHANGE `ITI22_HL7_version` `ITI22_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7') DEFAULT '2.5',
                CHANGE `ITI9_HL7_version` `ITI9_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7') DEFAULT '2.5';";
        $this->addQuery($query);

        $this->makeRevision("1.76");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                ADD `HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7') DEFAULT '2.5'";
        $this->addQuery($query);

        $this->makeRevision("1.77");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `exclude_admit_type` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("1.78");

        $query = "ALTER TABLE `hl7_config`
                ADD `check_identifiers` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.79");

        $query = "ALTER TABLE `hl7_config`
                ADD `oru_use_sas` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.80");

        $query = "ALTER TABLE `hl7_config` 
                CHANGE `object_attach_OBX` `object_attach_OBX` ENUM ('CPatient','CSejour','COperation','CMbObject', 'CFilesCategory') DEFAULT 'CMbObject';";
        $this->addQuery($query);

        $this->makeRevision("1.81");

        $query = "ALTER TABLE `hl7_config`
                ADD `retrieve_all_PI_identifiers` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.82");

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `check_field_length` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("1.83");

        $query = "ALTER TABLE `receiver_hl7v3` 
                ADD `type` ENUM ('none','DMP','Zepra','ASIP') DEFAULT 'none';";
        $this->addQuery($query);

        $this->makeRevision("1.84");

        $query = "ALTER TABLE `hl7_config` 
                ADD `type_sas` ENUM ('lifen', 'zepra');";
        $this->addQuery($query);

        $this->addMethod("changeTypeSASORULifen");

        $this->makeRevision("1.85");

        $query = "ALTER TABLE `hl7_config` 
                DROP `oru_use_sas`;";
        $this->addQuery($query);

        $this->makeRevision("1.86");

        $query = "ALTER TABLE `hl7_config` 
                CHANGE `type_sas` `type_sas` ENUM ('lifen', 'sisra');";
        $this->addQuery($query);

        $this->makeRevision("1.87");

        $query = "ALTER TABLE `hl7_config`
                ADD `ignore_admit_with_field` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("1.88");

        $query = "ALTER TABLE `receiver_hl7v3`
                CHANGE `type` `type` ENUM ('DMP','ZEPRA','ASIP');";
        $this->addQuery($query);

        $query = "UPDATE `receiver_hl7v3` SET `type` = NULL WHERE `type`= 'none';
              UPDATE `receiver_hl7v3` SET `type` = 'ZEPRA' WHERE `type`= 'Zepra';";
        $this->addQuery($query);

        $this->makeRevision("1.89");

        $query = "ALTER TABLE `hl7_config`
                ADD `unqualified_identity` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.90");

        $query = "ALTER TABLE `hl7_config`
                ADD `change_OBX_5` VARCHAR (50);";
        $this->addQuery($query);

        $this->makeRevision("1.91");

        $query = "ALTER TABLE `receiver_hl7v2`
                ADD `type` VARCHAR (255);";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v3`
                CHANGE `type` `type` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("1.92");

        $query = "ALTER TABLE `receiver_hl7v2`
                ADD `use_specific_handler` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v3`
                ADD `use_specific_handler` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.93");

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_doctolib` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.94");

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_patient_SIU` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("1.95");
        $this->setModuleCategory("interoperabilite", "echange");

        $this->makeRevision("1.96");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                ADD `build_empty_fields` ENUM ('yes','no','restricted') DEFAULT 'no',
                ADD `fields_allowed_to_empty` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision("1.97");

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_tamm_sih` VARCHAR (255);";
        $this->addQuery($query);
        
        $this->makeRevision("1.99");

        $query = "ALTER TABLE `hl7_config` 
                CHANGE `define_date_category_name` `define_name` VARCHAR (50) default 'name' ;";
        $this->addQuery($query);

        $this->addMethod("updateDefaultDefineName");

        $this->makeRevision("2.02");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                ADD `build_PV1_4` ENUM ('normal','charge_price_indicator') DEFAULT 'normal';";
        $this->addQuery($query);

        $this->makeRevision("2.03");

        $query = "ALTER TABLE `hl7_config` 
                ADD `handle_PV1_4` ENUM ('normal','charge_price_indicator') DEFAULT 'normal';";
        $this->addQuery($query);

        $this->makeRevision("2.04");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                ADD `sih_cabinet_id` INT (11);";
        $this->addQuery($query);

        $this->makeRevision("2.05");

        $query = "ALTER TABLE `sender_mllp` 
                ADD `response` ENUM ('none','auto_generate_before','postprocessor') DEFAULT 'none';";
        $this->addQuery($query);

        $query = "UPDATE `sender_mllp`
                SET `response` = 'postprocessor'
                WHERE `create_ack_file` = '1'";
        $this->addQuery($query);

        $query = "ALTER TABLE `sender_mllp` 
                DROP `create_ack_file`,
                DROP `delete_file`;";
        $this->addQuery($query);

        $this->makeRevision("2.06");

        $query = "ALTER TABLE `hl7_config` 
                ADD `generate_IPP_unqualified_identity` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("2.07");

        $query = "ALTER TABLE `hl7_config` 
                ADD `handle_IIM_6` ENUM ('group','service','sejour');";
        $this->addQuery($query);

        $this->makeRevision("2.08");

        $query = "ALTER TABLE `hl7_config` 
                ADD `search_patient` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("2.09");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                ADD `RAD28_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5') DEFAULT '2.5';";
        $this->addQuery($query);

        $this->makeRevision("2.10");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                ADD `files_mode_sas` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);

        $this->makeRevision("2.11");

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                ADD `cabinet_sih_id` INT (11);";
        $this->addQuery($query);

        $this->makeRevision("2.12");

        $query = "ALTER TABLE `hl7_config`
                ADD `creation_date_file_like_treatment` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision('2.14');

        $query = 'ALTER TABLE `exchange_hl7v2` ADD `altered_content_id` INT (11) UNSIGNED;';
        $this->addQuery($query);

        $query = "ALTER TABLE `exchange_hl7v2` 
                ADD INDEX (`altered_content_id`);";
        $this->addQuery($query);

        $this->makeRevision('2.15');

        $query = "ALTER TABLE `receiver_hl7v2_config`
               ADD `build_OBX_3` ENUM ('DMP','INTERNE') DEFAULT 'INTERNE';";
        $this->addQuery($query);

        $this->makeRevision('2.16');

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_SIU_object` ENUM ('consultation','intervention') DEFAULT 'consultation';";
        $this->addQuery($query);

        $this->makeRevision('2.18');

        $query = "ALTER TABLE `source_mllp`
                  ADD `timeout_socket` INT (3) DEFAULT '5',
                  ADD `timeout_period_stream` INT (3);";
        $this->addQuery($query);

        $this->makeRevision('2.19');

        $query = "ALTER TABLE `source_mllp`
                  ADD `set_blocking` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision('2.20');

        $query = "CREATE TABLE `exchange_mllp` (
                `exchange_mllp_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `emetteur` VARCHAR (255),
                `destinataire` VARCHAR (255),
                `date_echange` DATETIME NOT NULL,
                `response_datetime` DATETIME,
                `function_name` VARCHAR (255) NOT NULL,
                `input` LONGTEXT,
                `output` LONGTEXT,
                `purge` ENUM ('0','1') DEFAULT '0',
                `response_time` FLOAT,
                `source_id` INT (11) UNSIGNED,
                `source_class` ENUM ('CSourceMLLP')
              )/*! ENGINE=MyISAM */;
              ALTER TABLE `exchange_mllp` 
                ADD INDEX (`date_echange`),
                ADD INDEX (`response_datetime`),
                ADD INDEX source (source_class, source_id);";
        $this->addQuery($query);

        $this->makeRevision('2.21');

        $query = "ALTER TABLE `hl7_config` 
                CHANGE `type_sas` `type_sas` ENUM ('lifen', 'sisra', 'sih_cabinet');";
        $this->addQuery($query);
        $this->makeRevision('2.22');

        $query = "ALTER TABLE `sender_mllp` 
                ADD `type` VARCHAR (255);";
        $this->addQuery($query);

        $this->makeRevision('2.25');

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                CHANGE `ITI30_HL7_version` `ITI30_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','FRA_2.3','FRA_2.4','FRA_2.5','FRA_2.6','FRA_2.7','FRA_2.8','FRA_2.9','FRA_2.10') DEFAULT '2.5',
                CHANGE `ITI31_HL7_version` `ITI31_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','FRA_2.3','FRA_2.4','FRA_2.5','FRA_2.6','FRA_2.7','FRA_2.8','FRA_2.9','FRA_2.10') DEFAULT '2.5';";
        $this->addQuery($query);

        $this->makeRevision('2.26');

        $query = "ALTER TABLE `exchange_hl7v2`
                    DROP INDEX group_id,
                    ADD INDEX group_date (group_id, date_production),
                    ADD INDEX sender (sender_class, sender_id),
                    ADD INDEX object (object_class, object_id),
                    ADD INDEX sender_date (sender_id, date_production),
                    ADD INDEX receiver_date (receiver_id, date_production);";
        $this->addQuery($query);

        $this->makeRevision('2.27');

        $query = "ALTER TABLE `hl7_config`
                    ADD `mode_sas` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->addMethod("updateModeSAS");

        $this->makeRevision('2.28');

        $query = "ALTER TABLE `receiver_hl7v2_config`
                    ADD `build_PID_5_2` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision('2.29');

        $query = "ALTER TABLE `receiver_hl7v2_config` 
                    CHANGE `HL7_version` `HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2') DEFAULT '2.5',
                    CHANGE `ITI30_HL7_version` `ITI30_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2','FRA_2.3','FRA_2.4','FRA_2.5','FRA_2.6','FRA_2.7','FRA_2.8','FRA_2.9','FRA_2.10') DEFAULT '2.5',
                    CHANGE `ITI31_HL7_version` `ITI31_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2','FRA_2.3','FRA_2.4','FRA_2.5','FRA_2.6','FRA_2.7','FRA_2.8','FRA_2.9','FRA_2.10') DEFAULT '2.5',
                    CHANGE `RAD3_HL7_version` `RAD3_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2') DEFAULT '2.5',
                    CHANGE `RAD28_HL7_version` `RAD28_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2') DEFAULT '2.5',
                    CHANGE `RAD48_HL7_version` `RAD48_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2') DEFAULT '2.5',
                    CHANGE `ITI21_HL7_version` `ITI21_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2') DEFAULT '2.5',
                    CHANGE `ITI22_HL7_version` `ITI22_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2') DEFAULT '2.5',
                    CHANGE `ITI9_HL7_version` `ITI9_HL7_version` ENUM ('2.1','2.2','2.3','2.3.1','2.4','2.5','2.5.1','2.6','2.7','2.7.1','2.8','2.8.1','2.8.2') DEFAULT '2.5';";
        $this->addQuery($query);

        $this->makeRevision('2.30');

        $query = "ALTER TABLE `hl7_config`
                ADD `handle_patient_ORU` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision('2.31');

        $query = "ALTER TABLE `receiver_hl7v2_config`
                ADD `send_NTE` ENUM ('0','1') DEFAULT '1';";
        $this->addQuery($query);


        $this->makeRevision('2.33');
        $query = "ALTER TABLE `source_mllp` ADD `client_name`  VARCHAR(255);";
        $this->addQuery($query);

        $this->makeRevision('2.34');
        $query = "ALTER TABLE `hl7_config` 
                ADD `search_patient_strategy` VARCHAR(255) DEFAULT 'best';";
        $this->addQuery($query);

        $this->makeRevision('2.35');
        $query = "ALTER TABLE `source_mllp` 
        ADD `retry_strategy` VARCHAR(255), 
        ADD `first_call_date` DATETIME;";
        $this->addQuery($query);

        $query = "UPDATE source_mllp SET retry_strategy = '" . CExchangeSourceAdvanced::DEFAULT_RETRY_STRATEGY . "' WHERE retry_strategy IS NULL;";
        $this->addQuery($query);

        $this->makeRevision("2.36");

        $query = "ALTER TABLE `hl7_config`
                ADD `ignore_the_patient_with_an_unauthorized_IPP` ENUM ('0','1') DEFAULT '0';";
        $this->addQuery($query);

        $this->makeRevision("2.37");

        $query = "ALTER TABLE `hl7_config`
                CHANGE `handle_SIU_object` `handle_SIU_object` ENUM ('consultation','intervention', 'element_prescription') DEFAULT 'consultation';";
        $this->addQuery($query);

        $this->makeRevision("2.38");
        $this->addMethod('createTableHL7Entries');
        $this->addMethod("migrateExternTableHL7InLocal");

        $this->makeRevision("2.39");
        $this->addQuery("ALTER TABLE source_mllp MODIFY COLUMN loggable INT(11) DEFAULT 0 NOT NULL;");
        $this->addQuery("ALTER TABLE exchange_mllp ADD purged_date Datetime;");

        $this->makeRevision("2.40");

        $query = "ALTER TABLE `receiver_hl7v2` ADD `debug_session` DATETIME;";
        $this->addQuery($query);

        $query = "ALTER TABLE `receiver_hl7v3` ADD `debug_session` DATETIME;";
        $this->addQuery($query);

        $query = "ALTER TABLE `sender_mllp` ADD `debug_session` DATETIME;";
        $this->addQuery($query);

        $this->mod_version = "2.41";
    }

    /**
     * Check HL7v2 tables presence
     *
     * @return bool
     */
    protected function migrateExternTableHL7InLocal(): bool
    {
        $dshl7 = CSQLDataSource::get("hl7v2", true);

        $request = new CRequest();
        $count_entries = $this->ds->loadResult($request->makeSelectCount(new CHL7v2TableEntry()));
        // no migration needed
        if (!$dshl7 || !$dshl7->loadTable("table_entry") || (intval($count_entries) !== 0)) {
            return true;
        }

        // migrate entry
        $req = new CRequest();
        $req->addSelect("*");
        $req->addTable("table_entry");
        $result = $dshl7->loadList($req->makeSelect());
        $result = array_map(function ($res) {
            $res['hl7v2_table_entry_id'] = $res['table_entry_id'];

            return $res;
        }, $result);
        $fields = ["hl7v2_table_entry_id", "number", "code_hl7_from", "code_hl7_to", "code_mb_from", "code_mb_to", "description", "user", "codesystem_id"];
        $query = $this->buildQueryInsertObjects($dshl7, 'hl7v2_table_entry', $fields, $result);
        $this->ds->exec($query);

        // migrate description
        $req = new CRequest();
        $req->addSelect("*");
        $req->addTable("table_description");
        $result = $dshl7->loadList($req->makeSelect());
        $result = array_map(function ($res) {
            $res['hl7v2_table_description_id'] = $res['table_description_id'];

            return $res;
        }, $result);

        $fields = ["hl7v2_table_description_id", "number", "description", "user", "valueset_id"];
        $query = $this->buildQueryInsertObjects($dshl7, 'hl7v2_table_description', $fields, $result);
        $this->ds->exec($query);

        return true;
    }

    protected function buildQueryInsertObjects(CSQLDataSource $ds, string $table, array $fields, array $objects): ?string
    {
        if (empty($objects)) {
            return null;
        }

        $f_quote = function ($field) {
            return "`$field`";
        };

        $query = "INSERT INTO `$table` (" . implode(',', array_map($f_quote, $fields)) . ") VALUES ";

        foreach ($objects as $values) {
            $query_object = "";
            foreach ($fields as $field) {
                $value = $values[$field] ?? null;

                if ($value !== null) {
                    $value = $ds->escape($value);
                    $value = "\"$value\"";
                } else {
                    $value = "NULL";
                }

                $query_object .= $query_object ? ",$value" : "$value";
            }

            $query .= "($query_object),";
        }

        return substr($query, 0, -1) . ';';
    }

    /**
     * Add constantes ranks
     *
     * @return bool
     */
    protected function changeAttachObjectOBX()
    {
        $ds = $this->ds;

        if ($ds->hasTable("hl7_config")) {
            $results = $ds->exec("SELECT * FROM `hl7_config`");

            if ($results) {
                while ($row = $ds->fetchAssoc($results)) {
                    $hl7_config_id = CMbArray::get($row, "hl7_config_id");

                    $object_attach_OBX = null;
                    if (CMbArray::get($row, "force_attach_doc_admit")) {
                        $object_attach_OBX = "CSejour";
                    }
                    if (CMbArray::get($row, "force_attach_doc_patient")) {
                        $object_attach_OBX = "CPatient";
                    }

                    if ($object_attach_OBX) {
                        $query = "UPDATE `hl7_config`
                        SET `object_attach_OBX` = '$object_attach_OBX'
                        WHERE `hl7_config_id` = '$hl7_config_id';";

                        $ds->exec($query);
                    }
                }
            }
        }

        return true;
    }

    /**
     * Set default config define_name
     *
     * @return bool
     */
    protected function updateDefaultDefineName()
    {
        $ds = $this->ds;

        if ($ds->hasTable("hl7_config")) {
            $results = $ds->exec("SELECT * FROM `hl7_config`");

            if ($results) {
                while ($row = $ds->fetchAssoc($results)) {
                    $hl7_config_id = CMbArray::get($row, "hl7_config_id");

                    $define_name = 'name';
                    if (CMbArray::get($row, "id_category_patient") || CMbArray::get($row, "define_name")) {
                        $define_name = "datefile_category";
                    }

                    $query = "UPDATE `hl7_config`
                      SET `define_name` = '$define_name'
                      WHERE `hl7_config_id` = '$hl7_config_id';";

                    $ds->exec($query);
                }
            }
        }

        return true;
    }

    /**
     * Change type SAS
     *
     * @return bool
     */
    protected function changeTypeSASORULifen()
    {
        $ds = $this->ds;

        if ($ds->hasTable("hl7_config")) {
            $results = $ds->exec("SELECT * FROM `hl7_config`");

            if ($results) {
                while ($row = $ds->fetchAssoc($results)) {
                    $query = "UPDATE `hl7_config`
                      SET `type_sas` = 'lifen'
                      WHERE `oru_use_sas` = '1'";

                    $ds->exec($query);
                }
            }
        }

        return true;
    }

    /**
     * Update mode SAS
     *
     * @return bool
     */
    protected function updateModeSAS()
    {
        $ds = $this->ds;

        if ($ds->hasTable("hl7_config")) {
            $results = $ds->exec(
                "SELECT * FROM `hl7_config` WHERE `type_sas` = 'lifen' OR `type_sas` = 'sisra' OR `type_sas` = 'sih_cabinet'"
            );

            if ($results) {
                while ($row = $ds->fetchAssoc($results)) {
                    $hl7_config_id = CMbArray::get($row, "hl7_config_id");

                    // On active le mode SAS
                    $query = "UPDATE `hl7_config` SET `mode_sas` = '1' WHERE `hl7_config_id` = '$hl7_config_id';";
                    $ds->exec($query);

                    $sender_class = CMbArray::get($row, "sender_class");
                    $sender_id    = CMbArray::get($row, "sender_id");
                    /** @var CInteropSender $sender */
                    $sender = CMbObject::loadFromGuid("$sender_class-$sender_id");
                    if ($sender->_id) {
                        switch (CMbArray::get($row, "type_sas")) {
                            case 'sisra':
                                $sender->type = CInteropActor::ACTOR_ZEPRA;
                                $sender->store();
                                break;
                            case 'sih_cabinet':
                                $sender->type = CInteropActor::ACTOR_TAMM;
                                $sender->store();
                                break;

                            default:
                        }
                    }
                }
            }
        }

        return true;
    }

    protected function createTableHL7Entries(): bool
    {
        $query = "CREATE TABLE IF NOT EXISTS `hl7v2_table_entry` (
                `hl7v2_table_entry_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                `number` INT (11) NOT NULL,
                `code_hl7_from` VARCHAR (30),
                `code_hl7_to` VARCHAR (30),
                `code_mb_from` VARCHAR (30),
                `code_mb_to` VARCHAR (30),
                `description` VARCHAR (255),
                `user` ENUM ('0','1') NOT NULL DEFAULT '0',
                `codesystem_id` VARCHAR (80)
                )/*! ENGINE=MyISAM */;";
        $this->ds->exec($query);

        if (!$this->isIndexPresent($this->ds, "hl7v2_table_entry", "number")) {
            $query = "ALTER TABLE `hl7v2_table_entry` 
                ADD INDEX (`number`);";
            $this->ds->exec($query);
        }

        $query = "CREATE TABLE IF NOT EXISTS `hl7v2_table_description` (
                  `hl7v2_table_description_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                  `number` int(11) NOT NULL,
                  `description` varchar(80) NOT NULL,
                  `user` enum('0','1') NOT NULL DEFAULT '0',
                  `valueset_id` varchar(80))/*! ENGINE=MyISAM */;";
        $this->ds->exec($query);

        return true;
    }

    protected function populateFromSQLFile(): bool
    {
        $filename = __DIR__ . '/../resources/base/dump_hl7.sql';
        if (!file_exists($filename)) {
            throw new CMbException('CSetupHL7-msg-invalid dump entries HL7');
        }

        $this->ds->queryDump($filename);

        return true;
    }
}
