<?php

/**
 * @package Mediboard\Hl7\Resources
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\ClassMap;

use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Components\Cache\LayeredCache;
use Ox\Core\Cache;
use Psr\SimpleCache\InvalidArgumentException;

class HL7v2ClassMap
{
    /** @var string */
    private const CACHE_KEY = 'HL7v2-map';
    /** @var int */
    private const CACHE_TTL = 600;

    private array $map = [];

    private LayeredCache $cache;

    public readonly DelegatedMap $delegated;

    /**
     * HL7v2ClassMap constructor.
     *
     * @param string[] $resources
     *
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     */
    public function __construct()
    {
        $this->cache = self::getCache();
        $this->map   = $this->getOrBuildClassMap();
    }

    /**
     * Get cache
     *
     * @return LayeredCache
     * @throws CouldNotGetCache
     */
    private static function getCache(): LayeredCache
    {
        return Cache::getCache(Cache::INNER_OUTER);
    }

    /**
     * @return array
     * @throws InvalidArgumentException
     */
    public function getOrBuildClassMap(): array
    {
        if ($this->map) {
            return $this->map;
        }

        $class_map = (new HL7ClassMapBuilder())->build();

        $this->map       = $class_map;
        $this->delegated = new DelegatedMap($this);

        $this->cache->set(self::CACHE_KEY, $class_map, self::CACHE_TTL);

        return $class_map;
    }

    /**
     * Clear cache
     *
     * @return bool
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public static function clearCache(): bool
    {
        return self::getCache()->delete(self::CACHE_KEY);
    }
}
