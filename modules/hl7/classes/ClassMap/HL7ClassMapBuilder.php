<?php

/**
 * @package Mediboard\Hl7\Resources
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\ClassMap;

use Exception;
use Ox\Core\CClassMap;
use Ox\Interop\Hl7\DelegatedObjectInterface;
use Ox\Interop\Hl7\Segments\DelegatedObjectMapperInterface;

class HL7ClassMapBuilder
{
    private array $map = [];

    private ?CClassMap $class_map = null;

    /** @var DelegatedObjectInterface[] */
    private array $delegated_objects = [];
    private array $profiles = [];

    /**
     * @return array
     * @throws Exception
     */
    public function build(): array
    {
        $this->class_map = CClassMap::getInstance();

        if (!$this->delegated_objects) {
            $this->delegated_objects = $this->findDelegated();
        }

        $this->map = [];

        $this->buildObjectsDelegated();

        return $this->map;
    }


    /**
     * @return array
     * @throws Exception
     */
    private function findDelegated(): array
    {
        $delegated_objects = [];

        // mapper
        $delegated_objects['mapper'] = $this->findSpecificDelegated(DelegatedObjectMapperInterface::class);

        return $delegated_objects;
    }

    /**
     * @param string    $interface
     * @param CClassMap $classMap
     *
     * @return array
     * @throws Exception
     */
    private function findSpecificDelegated(string $interface): array
    {
        $result = [];
        $class_map = $this->class_map;

        foreach ($class_map->getClassChildren($interface) as $class) {
            if (!$class_map->getClassMap($class)) {
                continue;
            }

                // all
            if (!in_array($class, $result['all'] ?? [])) {
                $result['all'][] = $class;
            }
        }

        return $result;
    }
    /**
     * @return void
     */
    private function buildObjectsDelegated(): void
    {
        // delegated mapper
        $this->map['delegated']['mapper']['all'] = $this->delegated_objects['mapper']['all'];
    }
}
