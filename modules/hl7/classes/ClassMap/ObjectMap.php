<?php

/**
 * @package Mediboard\Hl7\Resources
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\ClassMap;

use Psr\SimpleCache\InvalidArgumentException;

abstract class ObjectMap
{
    /** @var string */
    protected const KEY_MAP = '';

    /** @var array */
    protected $map;

    /** @var bool */
    protected $return_class = false;

    /** @var array */
    protected $class_map;

    /**
     * @param HL7v2ClassMap $class_map
     * @throws InvalidArgumentException
     */
    public function __construct(HL7v2ClassMap $class_map)
    {
        $this->class_map = $class_map->getOrBuildClassMap();
        $this->map       = $this->class_map[$this::KEY_MAP];
    }


    /**
     * @param bool $return_class
     *
     * @return ObjectMap
     */
    public function setReturnClass(bool $return_class): ObjectMap
    {
        $this->return_class = $return_class;

        return $this;
    }

    /**
     * @param string|string[] $data
     *
     * @return array|mixed
     */
    protected function out(string|array $data): mixed
    {
        if (!$data) {
            return $data;
        }

        if (!is_array($data)) {
            return $this->return_class ? $data : new $data();
        }

        // sanitize
        $data = array_filter($data);

        return array_map(fn ($element) => $this->return_class ? $element : new $element(), $data);
    }
}
