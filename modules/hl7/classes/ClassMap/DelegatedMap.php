<?php

/**
 * @package Mediboard\Hl7\Resources
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\ClassMap;

use Ox\Interop\Hl7\DelegatedObjectInterface;

class DelegatedMap extends ObjectMap
{
    /** @var string */
    public const TYPE_DELEGATED_MAPPER = "mapper";

    /** @var string */
    protected const KEY_MAP = 'delegated';

    /**
     * @param string $type
     * @param string $canonical_or_type
     *
     * @return DelegatedObjectInterface[]|string[]
     */
    public function listDelegated(string $type, string $canonical_or_type = null): array
    {
        return match ($type) {
            self::TYPE_DELEGATED_MAPPER => $this->listDelegatedMapper($canonical_or_type),
            default => [],
        };
    }

    /**
     * @param string $canonical_or_type
     *
     * @return DelegatedObjectInterface[]|string[]
     */
    public function listDelegatedMapper(string $canonical_or_type = null): array
    {
        return $this->listSpecificDelegated('mapper', $canonical_or_type);
    }

    /**
     * @param string $key
     * @param string $canonical_or_type
     *
     * @return DelegatedObjectInterface[]|string[]
     */
    private function listSpecificDelegated(string $key, ?string $event_or_profile): array
    {
        return $this->map;
    }

    /**
     * @param string $short_name
     *
     * @return DelegatedObjectInterface|string|null
     */
    public function getDelegatedMapperFromShortName(string $short_name): DelegatedObjectInterface|string|null
    {
        return $this->getDelegatedFromShortName('mapper', $short_name);
    }

    /**
     * @param string $key
     * @param string $short_name
     *
     * @return DelegatedObjectInterface|string
     */
    public function getDelegatedFromShortName(
        string $key,
        string $short_name
    ): DelegatedObjectInterface|string|null {
        $all_delegated = $this->map[$key]['all'] ?? [];

        $filtered_delegated = array_filter(
            $all_delegated,
            fn($class_name) => str_ends_with($class_name, "\\$short_name")
        );

        if (count($filtered_delegated) !== 1) {
            return null;
        }

        return $this->out(reset($filtered_delegated));
    }
}
