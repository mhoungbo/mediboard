<?php

/**
 * @package Mediboard\BloodSalvage
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\BloodSalvage;

use Exception;
use Ox\Core\Module\CModule;
use Ox\Mediboard\System\AbstractConfigurationRegister;
use Ox\Mediboard\System\CConfiguration;

/**
 * @codeCoverageIgnore
 */
class CConfigurationBloodSalvage extends AbstractConfigurationRegister
{
    /**
     * @return mixed
     * @throws Exception
     */
    public function register()
    {
        $config = [
            "CGroups" => [
                "bloodSalvage" => [
                    "General" => [],
                    "Timing_list" => [
                        "max_add_minutes" => "num min|0 default|10",
                        "max_sub_minutes" => "num min|0 default|30",
                    ],
                ],
            ],
        ];

        if (CModule::getActive('dPmedicament')) {
            $config['CGroups']['bloodSalvage']['General']['inLivretTherapeutique'] = "bool default|0";
        } else {
            $config['CGroups']['bloodSalvage']['General']['AntiCoagulantList'] = "str";
        }

        CConfiguration::register($config);
    }
}
