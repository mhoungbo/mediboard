<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Ftp\Tests\Functional;

use Exception;
use Ox\Interop\Eai\ExchangeSources\Accessibility\ExchangeSourceFileAccessibility;
use Ox\Interop\Eai\ExchangeSources\Repository\ExchangeSourceRepository;
use Ox\Interop\Eai\Tests\Classes\SourceFileTestTrait;
use Ox\Interop\Ftp\CSourceFTP;
use Ox\Tests\OxWebTestCase;

class FTPControllerTest extends OxWebTestCase
{
    use SourceFileTestTrait;

    public function providerConnexionKO(): array
    {
        return [
            'without connexion'              => [[], 4],
            'without authentication'         => [
                [
                    ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE => ['status' => true],
                ],
                3,
            ],
            'with directory which not exits' => [
                [
                    ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE   => ['status' => true],
                    ExchangeSourceFileAccessibility::REPORT_SOURCE_AUTHENTICATE => ['status' => true],
                ],
                2,
            ],
            'without writable directory'     => [
                [
                    ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE   => ['status' => true],
                    ExchangeSourceFileAccessibility::REPORT_SOURCE_AUTHENTICATE => ['status' => true],
                    ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE      => ['status' => true],
                ],
                1,
            ],
        ];
    }

    /**
     * @dataProvider providerConnexionKO
     *
     * @param array $report_data
     * @param int   $expected_errors
     *
     * @return void
     * @throws Exception
     */
    public function testConnexionKO(array $report_data, int $expected_errors): void
    {
        $mock_access = $this->getMockBuilder(ExchangeSourceFileAccessibility::class)
                            ->onlyMethods(['fullTest'])
                            ->getMock();
        $mock_access->method('fullTest')->willReturn($report_data);

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
                                 ->onlyMethods(['getTestableSource'])
                                 ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($this->createMock(CSourceFTP::class));

        $client    = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceFileAccessibility::class, $mock_access);
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/ftp/sources/connexion",
            [
                "exchange_source_name" => "source_name",
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//table/tr[@data-severity='1' and @data-item='1']");
        $this->assertEquals($expected_errors, $nodes->count());
    }

    public function testConnexion(): void
    {
        $mock_access = $this->getMockBuilder(ExchangeSourceFileAccessibility::class)
                            ->onlyMethods(['fullTest'])
                            ->getMock();
        $mock_access->method('fullTest')->willReturn(
            [
                ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE   => ['status' => true],
                ExchangeSourceFileAccessibility::REPORT_SOURCE_AUTHENTICATE => ['status' => true],
                ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE      => ['status' => true],
                ExchangeSourceFileAccessibility::REPORT_DIR_WRITABLE        => ['status' => true],
                ExchangeSourceFileAccessibility::REPORT_FILE_ADD            => ['status' => true],
                ExchangeSourceFileAccessibility::REPORT_FILE_GET            => ['status' => true],
                ExchangeSourceFileAccessibility::REPORT_FILE_DELETE         => ['status' => true],
            ]
        );

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
                                 ->onlyMethods(['getTestableSource'])
                                 ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($this->createMock(CSourceFTP::class));

        $client    = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceFileAccessibility::class, $mock_access);
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/ftp/sources/connexion",
            [
                "exchange_source_name" => "source_name",
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//table/tr[@data-severity='3' and @data-item='1']");
        $this->assertEquals(4, $nodes->count());
    }

    public function testGetFiles(): void
    {
        $files = [
            ['name' => 'file_1.txt', 'size' => 1],
            ['name' => 'file_2.txt', 'size' => 1],
            ['name' => 'file_3.txt', 'size' => 1],
        ];

        $source = $this->mockSourceFile(
            ['func' => 'getListFilesDetails', 'val'  => $files],
            $this->createMock(CSourceFTP::class)
        );

        $source_repo = $this->createMock(ExchangeSourceRepository::class);
        $source_repo->method('getTestableSource')->willReturn($source);

        $client    = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $source_repo);

        $crawler = $client->request(
            'GET',
            "/gui/ftp/sources/files",
            [
                "exchange_source_name" => "source_name",
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        foreach ($files as $file) {
            $filename = $file['name'];
            $query = "//table/tr/td/text()[contains(., '$filename')]";
            $this->assertCount(1, $crawler->filterXPath($query));
        }
    }
}
