/**
 * @package Mediboard\Ftp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

FTP = {
  connexion: function (element, exchange_source_name) {
    const route = element.getAttribute('data-route-connexion')
    if (!route) {
      return;
    }

    new Url()
      .setRoute(route, "ftp_gui_sources_connexion", "ftp")
      .addParam("exchange_source_name", exchange_source_name)
      .addParam('type', 'ftp')
      .requestModal(500, 400);
  },

  getFiles: function (element, exchange_source_name) {
    const route = element.getAttribute('data-route-list_files')
    if (!route) {
      return;
    }

    new Url()
      .setRoute(route , "ftp_gui_sources_list_files", "ftp")
      .addParam("exchange_source_name", exchange_source_name)
      .addParam('type', 'ftp')
      .requestModal(500, 400);
  },

  toggleDisabled : function (input_name, source_name) {
    var form = getForm("editSourceFTP-"+source_name);
    var input = form.elements[input_name];
    input.disabled ? input.disabled = '' : input.disabled = 'disabled';
  }
};
