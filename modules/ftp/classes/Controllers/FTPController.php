<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Ftp\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Eai\ExchangeSources\Accessibility\AccessibilityReportMaker;
use Ox\Interop\Eai\ExchangeSources\Accessibility\ExchangeSourceFileAccessibility;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNotConfiguredException;
use Ox\Interop\Eai\ExchangeSources\Repository\SourceFileRepository;
use Ox\Interop\Ftp\CSourceFTP;
use Ox\Interop\Ftp\CSourceSFTP;
use Symfony\Component\HttpFoundation\Response;

class FTPController extends Controller
{
    /**
     * @param RequestParams                   $request_params
     * @param ExchangeSourceFileAccessibility $accessibility
     * @param SourceFileRepository            $source_repository
     *
     * @return Response
     * @throws Exception
     */
    public function connexion(
        RequestParams                   $request_params,
        ExchangeSourceFileAccessibility $accessibility,
        SourceFileRepository            $source_repository
    ): Response {
        // Check params
        $exchange_source_name = $request_params->get("exchange_source_name", "str notNull");
        $type = $request_params->get("type", "enum list|ftp|sftp");
        if (!$type) {
            $type = [CSourceFTP::TYPE, CSourceSFTP::TYPE];
        }

        $report_maker = new AccessibilityReportMaker($this->translator);
        try {
            $source = $source_repository->getTestableSource($exchange_source_name, $type);
            $source->disableResilience();

            $data = $accessibility->fullTest($source);
        } catch (SourceException $exception) {
        } finally {
            $report = $report_maker->makeReport($data ?? []);

            if (isset($exception)) {
                $report->addData($exception->getMessage(), CItemReport::SEVERITY_ERROR);
            }

            return $this->renderSmarty('report/inc_report', ['report' => $report], 'eai');
        }
    }

    /**
     * @throws SourceNotConfiguredException
     * @throws Exception
     */
    public function listFiles(
        RequestParams                   $request_params,
        ExchangeSourceFileAccessibility $accessibility,
        SourceFileRepository            $source_repository
    ): Response {
        $source_name = $request_params->get("exchange_source_name", "str notNull");
        $directory   = $request_params->get("directory", "str");
        $type = $request_params->get("type", "enum list|ftp|sftp");
        if (!$type) {
            $type = [CSourceFTP::TYPE, CSourceSFTP::TYPE];
        }

        $files = [];
        /** @var CSourceFTP|CSourceSFTP $source */
        $source = $source_repository->getTestableSource($source_name, $type);
        $client = $source->disableResilience();

        $data_report = [];
        $accessibility->isAccessible($source, $data_report);
        $accessibility->isAuthenticate($source, $data_report);

        if (($source instanceof CSourceFTP) && $source->pasv) {
            $this->addUiMsgOk("CFTP-msg-passive_mode", false);
        }

        try {
            if (empty($data_report)) {
                $directory = $directory ?: $source->completePath()->toDirectory();
                $directory = $client->getDirectory($directory);
                $files     = $client->getListFilesDetails($directory);
            } else {
                $report = (new AccessibilityReportMaker($this->translator))->makeReport($data_report);
            }
        } catch (CMbException $e) {
            if (isset($report)) {
                $report->addData($e->getMessage(), CItemReport::SEVERITY_ERROR);
            } else {
                $this->addUiMsgError($e->getMessage(), true);
            }
        }

        return $this->renderSmarty(
            "inc_ftp_files.tpl",
            ["exchange_source" => $source, "files" => $files, 'error_report' => $report ?? null]
        );
    }
}
