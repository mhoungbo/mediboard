<?php

/**
 * @package Mediboard\Ftp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Ftp;

use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\SFTPClientInterface;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Interop\Eai\Resilience\CircuitBreaker;
use Ox\Mediboard\System\Sources\ClientResilienceTrait;

class ResilienceSFTPClient implements SFTPClientInterface
{
    /** @var SFTPClientInterface */
    public sFTPClientInterface $client;

    /** @var CircuitBreaker */
    private CircuitBreaker $circuit;

    /** @var ResponseAnalyser */
    private ResponseAnalyser $analyser;

    /** @var CSourceSFTP */
    private CSourceSFTP $source;

    use ClientResilienceTrait;

    /**
     * @param SFTPClientInterface $client
     * @param CExchangeSource     $source
     */
    public function __construct(SFTPClientInterface $client, CSourceSFTP $source)
    {
        $this->client = $client;

        $this->analyser = ($client instanceof CustomRequestAnalyserInterface)
            ? $client->getRequestAnalyser() : new ResponseAnalyser();

        $this->source  = $source;
        $this->circuit = new CircuitBreaker();
    }

    /**
     * @inheritdoc
     */
    public function getResponseTime(): int
    {
        return $this->client->getResponseTime();
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function send(string $destination_basename): bool
    {
        $call = fn() => $this->client->send($destination_basename);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function receive(string $path_directory): array
    {
        $call = fn() => $this->client->receive($path_directory);

        return (array) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getSize(string $file_name): int
    {
        $call = fn() => $this->client->getSize($file_name);

        return (int) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function renameFile(string $file_path, string $new_name): bool
    {
        $call = fn() => $this->client->renameFile($file_path, $new_name);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function createDirectory(string $directory_name): bool
    {
        $call = fn() => $this->client->createDirectory($directory_name);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getDirectory(string $directory): string
    {
        $call = fn() => $this->client->getDirectory($directory);

        return $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getListFiles(string $directory, bool $information = false): array
    {
        $call = fn() => $this->client->getListFiles($directory, $information);

        return (array) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getListFilesDetails(string $current_directory): array
    {
        $call = fn() => $this->client->getListFilesDetails($current_directory);

        return (array) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function getListDirectory(string $current_directory): array
    {
        $call = fn() => $this->client->getListDirectory($current_directory);

        return (array) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function addFile(string $source_file, string $file_name): bool
    {
        $call = fn() => $this->client->addFile($source_file, $file_name);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     * @throws CMbException
     * @throws CircuitBreakerException
     */
    public function delFile(string $path): bool
    {
        $call = fn() => $this->client->delFile($path);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     */
    public function getError(): ?string
    {
        return $this->client->getError();
    }

    /**
     * @inheritdoc
     * @throw CircuitBreakerException
     * @throws CMbException
     */
    public function getData(string $path, ?string $dest = null): ?string
    {
        $call = fn() => $this->client->getData($path, $dest);

        return $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }

    /**
     * @inheritdoc
     */
    public function removeDirectory(string $dir_path, bool $purge = false): bool
    {
        $call = fn() => $this->client->removeDirectory($dir_path, $purge);

        return (bool) $this->circuit->execute($this->source, $this->client, $call, $this->analyser);
    }
}
