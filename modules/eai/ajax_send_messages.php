<?php

/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\Chronometer;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Ox\Core\CSmartyDP;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Core\Logger\LoggerLevels;
use Ox\Interop\Eai\Actors\Repositories\ReceiverRepository;
use Ox\Interop\Eai\CInteropActorFactory;
use Ox\Interop\Eai\CInteropReceiver;
use Ox\Interop\Eai\CInteropReceiverFactory;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Eai\CReport;
use Ox\Interop\Eai\EAIMessageDispatcher;
use Ox\Interop\Eai\ExchangeDataFormatFactory;
use Ox\Interop\Eai\Logger\Wrapper\InteropLoggerWrapper;

/**
 * Send message
 */
CCanDo::checkRead();
$exchange_classes = CView::get("exchange_classes", 'str');
$receiver_guid    = CView::get("receiver_guid", 'guid class|CInteropReceiver');
$group_id         = CView::get("group_id", "ref class|CGroups");
$count            = CView::get("count", 'num');
$datetime_min     = CView::get('date_min', 'date');
$datetime_max     = CView::get('date_max', 'date');
CView::checkin();

$smarty = new CSmartyDP();
$report = new CReport("Rapport de traitement");
$smarty->assign('report', $report);

// control datetime_max
if ($datetime_max) {
    $datetime_max .= ' 00:00:00';
}

// control datetime_min
if ($datetime_min) {
    $datetime_min .= ' 00:00:00';

    // stop
    if ($datetime_min < CMbDT::dateTime("- 2 MONTHS")) {
        $report->addData(
            'La date_min ne peut pas �tre inf�rieure � 2 mois : ' . $datetime_min,
            CItemReport::SEVERITY_ERROR
        );
        $smarty->display("report/inc_report");

        return;
    }

    // pour rattrapage jusqu'a aujourd'hui
    if (!$datetime_max) {
        $datetime_max = CMbDT::dateTime("+1 day");
    }
}

// control conf is initialize
if (!$limit = CAppUI::conf('eai max_files_to_process')) {
    $report->addData(
        'La configuration "' . CAppUI::tr('config-eai-max_files_to_process') . '" n\'est pas initilialis�e',
        CItemReport::SEVERITY_ERROR
    );
    $smarty->display("report/inc_report");

    return;
}

// control exchange_classes
$authorize_classes = ['CEchangeHprim', 'CExchangeHL7v2'];
$exchange_classes  = array_filter(
    explode("|", $exchange_classes ?: ''),
    function ($exchange_class) use ($authorize_classes) {
        return in_array($exchange_class, $authorize_classes);
    }
);

if (empty($exchange_classes)) {
    $report->addData(
        'La valeure pour "exchange_classes" n\'est pas correcte, authoris�e : ' . implode(', ', $authorize_classes),
        CItemReport::SEVERITY_ERROR
    );
    $smarty->display("report/inc_report");

    return;
}

$chrono              = new Chronometer();
$logger              = new InteropLoggerWrapper();
$repository_receiver = new ReceiverRepository(
    $logger,
    new CInteropReceiverFactory(),
    new Conf(),
    CSQLDataSource::get(('std'))
);
$message_dispatcher  = new EAIMessageDispatcher(new ExchangeDataFormatFactory());

$configuration_report = new CItemReport('Configurations du script', CItemReport::SEVERITY_SUCCESS);
$configuration_report->addSubData("Nombre max de fichiers � traiter : $limit");
$report->addItem($configuration_report);


// determine available receivers
if ($receiver_guid) {
    $receiver            = CInteropReceiver::loadFromGuid($receiver_guid);
    $available_receivers = $receiver ? [$receiver] : [];
} else {
    $receiver_factory    = (new CInteropActorFactory())->receiver();
    $available_receivers = [];
    foreach ($exchange_classes as $exchange_class) {
        $chrono->start();
        // CEchangeHprim | CExchangeHL7v2
        $receiver = $receiver_factory->makeReceiverFromExchange(new $exchange_class());

        $receivers_for_format = $repository_receiver->getReceivers($receiver, [
            ReceiverRepository::ONLY_ACCESSIBLE => true,
            ReceiverRepository::ONLY_GROUPS     => $group_id ?: null,
        ]);

        $time_loading_receivers = number_format($chrono->stop(), 3);

        if ($receivers_for_format) {
            $trad_receiver = CAppUI::tr($receiver->_class);
            $configuration_report->addSubData(
                "[$trad_receiver] Chargement de " . count($receivers_for_format) . " destinaitaires (" . $time_loading_receivers . ' s)'
            );
        }

        $available_receivers = array_merge($available_receivers, $receivers_for_format);
    }
}

if (!$available_receivers) {
    $report->addData('Aucun destinataire disponible ou accessible', CItemReport::SEVERITY_WARNING);
    $smarty->display("report/inc_report");

    return;
}

$limit_message_receiver = (int)($limit / count($available_receivers));
$options                = [
    'datetime_min' => $datetime_min ?: null,
    'datetime_max' => $datetime_max ?: null,
];

/** @var CInteropReceiver $receiver */
foreach ($available_receivers as $receiver) {
    $options['limit'] = $receiver_limit = $count ?: $limit_message_receiver;
    $report_receiver  = new CItemReport(
        'Rapport destinataire : ' . $receiver->nom . " [$receiver->_guid]",
        CItemReport::SEVERITY_SUCCESS
    );
    $report->addItem($report_receiver);

    $report_receiver->addSubData("Limite du nombre de fichier trait� : $receiver_limit");
    $report_receiver->addSubData(
        "Date du dernier traitement (cache) : " . $message_dispatcher->getLastDateMessageProcessed($receiver)
    );

    try {
        // send messages
        $chrono->start();
        $count_messages_send = $message_dispatcher->sendMessagesFor($receiver, $options);
        $time_treat_messages = number_format($chrono->stop(), 3);
        $limit               -= $count_messages_send;

        $report_receiver->addSubData("$count_messages_send messages trait�s ($time_treat_messages s)");
    } catch (Exception $exception) {
        $logger->log(LoggerLevels::LEVEL_ERROR, $exception, ['receiver' => $receiver->_guid]);

        $report->addData(
            "$receiver->nom [$receiver->_guid] : " . $exception->getMessage(),
            CItemReport::SEVERITY_ERROR
        );
    }

    $report_receiver->addSubData(
        "Date du prochain traitement (cache) : " . $message_dispatcher->getLastDateMessageProcessed($receiver)
    );

    // redistribution des messages non trait�s par les premiers receiver
    $limit_message_receiver = max((int)($limit / count($available_receivers)), 10);
}

$smarty = new CSmartyDP();
$smarty->assign('report', $report);
$smarty->display("report/inc_report");
