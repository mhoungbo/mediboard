<?php
/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CMbObject;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Interop\Eai\CInteropReceiver;
use Ox\Interop\Ftp\CSourceFTP;
use Ox\Interop\Ftp\CSourceSFTP;
use Ox\Interop\Hl7\CSourceMLLP;
use Ox\Interop\Webservices\CSourceSOAP;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Mediboard\System\CExchangeSourceAdvanced;
use Ox\Mediboard\System\CSourceFileSystem;
use Ox\Mediboard\System\CSourceHTTP;

CCanDo::checkAdmin();

$sources = [
  CSourceSOAP::class,
  CSourceFTP::class,
  CSourceSFTP::class,
  CSourceFileSystem::class,
  CSourceHTTP::class,
  CSourceMLLP::class,
];

$source_errors = [];
$source_updated = [];
$limit = 200;
foreach ($sources as $source_class) {
    /** @var CExchangeSource $source */
    $source = new $source_class();

    $where = ['retry_strategy' => " IS NULL"];
    $sources_to_update = $source->loadList($where, null, $limit);
    foreach ($sources_to_update as $source) {
        $source->retry_strategy = CExchangeSourceAdvanced::DEFAULT_RETRY_STRATEGY;
        if ($msg = $source->store()) {
            $source_errors[$source->_guid] = $msg;
        } else {
            $source_updated[] = $source->_guid;
        }
    }
}


echo count($source_updated) . " source modifi�s";

foreach ($source_errors as $source_error_guid => $source_error) {
    echo "source $source_error_guid : $source_error";
}
