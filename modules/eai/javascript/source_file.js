/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * JS function Domain EAI
 */
SourceFile = {
  createDir: function (element, directory, callback) {
    const route = element.getAttribute('data-route');
    if (!route) {
      return;
    }

    oOptions = {
      postBody: JSON.stringify({'path': directory}),
      method:   "POST",
      onComplete: (res) => {callback(res)},
      insertion: (element) => {return false /* do nothing */}
    }

    new Url().setRoute(route).requestUpdate("systemMsg", oOptions)
  },

  purgeDir: function (element, directory, callback) {
    const route = element.getAttribute('data-route');
    if (!route) {
      return;
    }

    if (!confirm($T("CSourceFile-action-ask to delete directory %s", directory))) {
      return;
    }

    onComplete = function (args) {
      if (typeof callback === "function") {
        callback(args)
      }

      const tr = $T('CSourceFile-msg-directory deleted')
      SystemMessage.notify("<div class=\"small-info\">" + tr + " </div>")
    }

    const json_data = {
      'path': directory
    };

    fetch(route + "?purge=1", {
      'method':  'DELETE',
      'headers': {
        'Content-type': 'application/json'
      },
      'body': JSON.stringify(json_data),
    })
      .then((response) => {
        if (response.status === 200) {
          onComplete(response)
        } else {
          console.log(response)
          const tr = $T('common-error-An error occurred')
          SystemMessage.notify("<div class=\"error\">" + tr + " </div>")
        }
      })
  }
};
