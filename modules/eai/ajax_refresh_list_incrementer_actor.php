<?php
/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Interop\Eai\CDomain;
use Ox\Interop\Eai\CInteropActor;
use Ox\Mediboard\Etablissement\CGroups;

/**
 * Refresh incrementer/actor EAI
 */
CCanDo::checkAdmin();

$domain_id = CView::get("domain_id", "ref class|CDomain", true);
CView::checkin();

// Liste des domaines
$domain = new CDomain();
$domain->load($domain_id);
$domain->loadRefsGroupDomains();
$domain->loadRefActor();
$domain->loadRefIncrementer()->loadView();

// Liste des acteurs
$actors = [];
if (!$domain->_ref_incrementer->_id) {
    $actors = CInteropActor::getObjects(true, CGroups::loadCurrent()->_id, false, false);
}

$groups = CGroups::loadGroups();

// Cr�ation du template
$smarty = new CSmartyDP();
$smarty->assign("domain", $domain);
$smarty->assign("actors", $actors);
$smarty->assign("groups", $groups);
$smarty->display("inc_vw_domain_actor.tpl");
