{{*
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=system script=exchange_source}}

<script type="text/javascript">
    Source = {
        tab: null,

        edit: function (source_guid) {
            new Url("eai", "ajax_edit_source")
                .addParam("source_guid", source_guid)
                .requestModal(600, null, {onClose: Source.refresh.curry(source_guid)});

            return false;
        },

        changePage: function (page) {
            $V(getForm('filterSource').page, page);
        },

        refresh: function (source_guid) {
            new Url("eai", "ajax_refresh_exchange_source")
                .addParam("source_guid", source_guid)
                .requestUpdate("line_" + source_guid);
        },

        showTrace: function (source_guid) {
            new Url("eai", "ajaxShowTrace")
                .addParam("source_guid", source_guid)
                .requestModal('80%', '80%');
        },

        createSource: function () {
            new Url("eai", "ajaxSelectSourceType")
                .requestModal('80%', '100%');
        },

        getSourceClassSelected: function () {
            var sources_tab = document.getElementById("tabs-sources");
            var source_class = sources_tab.querySelector("a[class*='active']").key;
            return source_class;
        },

        viewAllFilter: function (form) {
            var source_class = Source.getSourceClassSelected();

            new Url("eai", "ajaxViewAllSourcesFilter")
                .addParam("source_class", source_class)
                .addParam("name", $V(form.name))
                .addParam("role", $V(form.role))
                .addParam("active", $V(form.active))
                .addParam("loggable", $V(form.loggable))
                .addParam("blocked", $V(form._blocked))
                .addParam("page", $V(form.page))
                .requestUpdate(source_class);

            return false;
        }

    };

    Main.add(function () {
        var form = getForm("filterSource");

        Source.tab = Control.Tabs.create("tabs-sources", false, {
            afterChange: function () {
                Source.viewAllFilter(form);
            }
        });
    });
</script>

<button type="button" class="add" onclick="Source.createSource();">{{tr}}CExchangeSource-msg-New source{{/tr}}</button>

{{mb_include module=eai template=inc_sources_filter}}

<table class="form">
    <tr>
        <td style="vertical-align: top; width: 10%;">
            <ul id="tabs-sources" class="control_tabs_vertical small me-margin-top-6">
                {{foreach from=$all_sources key=name item=_sources}}
                    <li>
                        <a href="#{{$name}}" {{if $count_exchange.$name == 0}}class="empty"{{/if}}>
                            {{tr}}{{$name}}{{/tr}} ({{$count_exchange.$name}})
                        </a>
                    </li>
                {{/foreach}}
            </ul>
        </td>
        <td style="vertical-align: top;">
            {{foreach from=$all_sources key=name item=_sources}}
                <div id="{{$name}}" style="display: none;">
                </div>
            {{/foreach}}
        </td>
    </tr>
</table>
