{{*
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<tr {{if !$_source->active}}class="opacity-30"{{/if}}>
    <td class="narrow">
        <button type="button" title="Modifier {{$_source->_view}}" class="edit notext button me-tertiary"
                onclick="Source.edit('{{$_source->_guid}}')"
                style="float: left">
            {{tr}}edit{{/tr}} {{$_source->name}}
        </button>
    </td>
    <td class="narrow">
        <button class="lookup button me-tertiary notext" type="button"
                onclick="Source.showTrace('{{$_source->_guid}}')"></button>
    </td>
    <td class="narrow">
        {{if $_source|instanceof:'Ox\Mediboard\System\CExchangeSourceAdvanced' && $_source->_blocked}}
            <button type="button" title="D�bloquer {{$_source->_view}}" class="unlock notext button me-tertiary"
                    onclick="ExchangeSource.unlock('{{$_source->name}}','{{$_source->_class}}');">
                {{tr}}{{$_source->_class}}-unlock{{/tr}}
            </button>
        {{/if}}
    </td>
    <td class="narrow">
        {{if $_source->role != $conf.instance_role}}
            <i class="fas fa-exclamation-triangle" style="color: goldenrod;"
               title="{{tr var1=$_source->role var2=$conf.instance_role}}CExchangeSource-msg-Source incompatible %s with the instance role %s{{/tr}}"></i>
        {{/if}}

        {{if $_source->role == "prod"}}
            <strong style="color: red" title="{{tr}}CExchangeSource.role.prod{{/tr}}">
                {{tr}}CExchangeSource.role.prod-court{{/tr}}
            </strong>
        {{else}}
            <span style="color: green" title="{{tr}}CExchangeSource.role.qualif{{/tr}}">
        {{tr}}CExchangeSource.role.qualif-court{{/tr}}
      </span>
        {{/if}}
    </td>
    <td class="narrow">
        {{assign var=source_guid value=$_source->_guid}}

        {{mb_include module="system" template="inc_form_button_active" field_name="active" object=$_source onComplete="Source.refresh('$source_guid')"}}
    </td>
    <td>
        <form name="edit-loggable-{{$_source->_guid}}" method="post"
              onsubmit="return onSubmitFormAjax(this, function () { Source.refresh('{{$source_guid}}') });">
            {{mb_key object=$_source}}
            {{mb_class object=$_source}}
            {{mb_field object=$_source field="loggable" hidden=true}}

            <a href="#1" onclick="toggleUpdate(this.up('form').elements.loggable);"
               style="display: inline-block; vertical-align: middle;">
                {{if $_source->loggable}}
                    <i class="fa fa-archive" style="color: #449944; font-size: large;"></i>
                {{else}}
                    <i class="fa fa-archive" style="color: #CCC; font-size: large;"></i>
                {{/if}}
            </a>
        </form>
    </td>
    <td class="text compact narrow">
        {{if $_source->libelle}}
            {{$_source->libelle}}
            <br/>
            <em style="font-size: 0.9em"> {{$_source->name}}</em>
        {{else}}
            {{$_source->name}}
        {{/if}}

        {{if $_source|instanceof:'Ox\Mediboard\System\CSourcePOP'}}
            <br/>
            <br/>
            <strong>{{mb_label object=$_source field="object_id"}} :</strong>
            {{if $_source->_ref_mediuser}}
                {{mb_include module=mediusers template=inc_vw_mediuser mediuser=$_source->_ref_mediuser}}
            {{else}}
                <input type="text" readonly="readonly" name="_object_view"
                       value="{{$_source->_ref_metaobject->_view}}" size="50"/>
            {{/if}}
        {{/if}}
    </td>
    {{assign var=source_status value=""}}
    {{if $_source|instanceof:'Ox\Mediboard\System\CExchangeSourceAdvanced'}}
        {{assign var=last_statistics value=$_source->_ref_last_statistics}}
        {{if $_source->_blocked}}
            {{assign var=source_status value="ko"}}
        {{elseif $last_statistics && $last_statistics->_id}}
            {{if !$last_statistics->failures}}
                {{assign var=source_status value="ok"}}
            {{else}}
                {{assign var=source_status value="warning"}}
            {{/if}}
        {{/if}}
    {{/if}}
    <td class="narrow button">
        {{unique_id var=uid}}
        <button {{if !$_source->active}}disabled="disabled"{{/if}} type="button"
                class="fas fa-network-wired notext me-tertiary singleclick"
                onclick="ExchangeSource.SourceReachable(this.parentNode);"
                title="{{tr}}CInteropActor-msg-Test accessibility sources{{/tr}}">
        </button>

        <i class="fa fa-circle"
           style="color:{{if $source_status == "ko"}}red{{elseif $source_status == "warning"}}orange{{elseif $source_status == "ok"}}limegreen{{else}}grey{{/if}}"
           id="{{$uid}}" data-id="{{$_source->_id}}" data-guid="{{$_source->_guid}}" guid="{{$_source->_guid}}"
           title="{{$_source->name}}"></i>
    </td>
    <td class="text">
        {{if $_source|instanceof:'Ox\Mediboard\System\CExchangeSourceAdvanced'}}
            {{assign var=last_statistics value=$_source->_ref_last_statistics}}
            {{if $_source->_blocked}}
                <strong>{{tr}}CExchangeSourceAdvanced-msg-Source blocked{{/tr}}</strong>
            {{elseif $last_statistics && $last_statistics->_id}}
                <strong>{{tr var1=$last_statistics->failures}}CExchangeSourceAdvanced-msg-Source {{if !$last_statistics->failures}}available{{else}}unavailable{{/if}}{{/tr}}</strong>
                <br/>
                <em> {{$_source->_ref_last_statistics->_view}} </em>
            {{else}}
                <strong>{{tr}}CExchangeSourceAdvanced-msg-Status of the source to be checked{{/tr}}</strong>
            {{/if}}
        {{else}}
            <strong>{{tr}}CExchangeSourceAdvanced-msg-Status of the source to be checked{{/tr}}</strong>
        {{/if}}
    </td>
</tr>
