{{*
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=delegated_conf value=0}}
{{if $_message_supported->_id !== null}}
    {{assign var=delegated_conf value="$data_format_module delegated_objects $delegated_type"|conf:$_message_supported}}
{{/if}}

{{assign var=callable value=$data_format|get_class|cat:'::getDelegatedObjects'}}
{{assign var=delegated_objects value=$callable|static_call:$_message_supported:"$delegated_type"}}

{{csrf_token id=eai_update_delegated_object var=csrf_token_edit_delegated_object}}

{{if $delegated_objects}}
  {{me_form_field nb_cells="0" label="CMessageSupported-delegated.$delegated_type" field_class="me-margin-left-6"}}
  <select onchange="InteropActor.updateDelegated(this, '{{$csrf_token_edit_delegated_object}}')"
          form="{{$form_name}}"
          name="{{$delegated_type}}"
          data-module="{{$data_format_module}}"
          data-route-delegated="{{url name='eai_message_supported_update_delegated_object'}}"
          data-delegated_type="{{$delegated_type}}">
    <option value="">---</option>
      {{foreach from=$delegated_objects item=delegated_object}}
        {{assign var=delegated value=$delegated_object|getShortName}}

        <option {{if $delegated_conf == "$delegated"}}selected="selected"{{/if}} value="{{$delegated}}">{{$delegated}}</option>
      {{/foreach}}
  </select>
    {{/me_form_field}}
{{/if}}
