{{*
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main tbl">
  <tr>
    <td>
      <div class="small-warning">{{tr}}CInteropActor-msg-migration is needed{{/tr}}</div>
    </td>
    <td>
      <button type="button" onclick="InteropActor.migrateActorConfigurations('{{$actor->_guid}}')" class="fas fa-sync">
          {{tr}}CInteropActor-msg-migrate{{/tr}}
      </button>
    </td>
</table>
