{{*
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}
{{assign var=_message_supported value=0}}
{{foreach from=$_messages_supported item=_message}}
    {{if $_message->_id}}
        {{assign var=_message_supported value=$_message}}
    {{/if}}
{{/foreach}}

{{if !$_message_supported}}
    {{assign var=_message_supported value=$_messages_supported|@first}}
{{/if}}

<tr>
    <th style="text-align: left;" class="section" colspan="4">
        {{* checkAll *}}
        <button class="fa fa-check notext" onclick="checkAll('{{$_family_name}}', '{{$category_uid}}')"></button>

        {{* Category name *}}
        {{if $_category_name != "none"}}
            {{if $data_format_module === 'fhir'}}
                {{assign var=resource value='Ox\Interop\Fhir\CExchangeFHIR::getResourceFromCanonical'|static_call:$_category_name}}
                {{assign var=resource_type_profiled value=$resource|const:'PROFILE_TYPE'}}
                {{$resource|const:'RESOURCE_TYPE'}}
                {{if $resource_type_profiled}}
                    (<em>{{$resource_type_profiled}}</em>)
                {{/if}}
            {{else}}
                {{tr}}{{$_category_name}}{{/tr}}
                (<em>{{$_category_name}}</em>)
            {{/if}}
        {{else}}
            {{tr}}All{{/tr}}
        {{/if}}

        <form name="{{$form_name}}"
              method="post"
              data-category_uid="{{$category_uid}}"
              data-family="{{$_family_name}}"
              data-category="{{$_category_name}}"
              onsubmit="InteropActor.updateMessageSupported(this)">
            <input type="hidden" name="object_id" value="{{$_message_supported->object_id}}"/>
            <input type="hidden" name="object_class" value="{{$_message_supported->object_class}}"/>
            <input type="hidden" name="message" value="{{$_message_supported->message}}"/>
            <input type="hidden" name="transaction" value="{{$_message_supported->transaction}}"/>
            <input type="hidden" name="old_transaction" value="{{$_category_name}}"/>
            <input type="hidden" name="profil" value="{{$_family_name}}"/>
            <input type="hidden" name="category_uid" value="{{$category_uid}}"/>

            {{* version *}}
            <span id="container_version_{{$_family_name}}_{{$category_uid}}">
            {{if $_families->_versions_category}}
                {{mb_include module=eai template=inc_message_supported_section_version}}
            {{/if}}
        </span>
        </form>
    </th>
</tr>

<tr>
    <th colspan="4">
        <div style="display: inline-flex; justify-content: space-around" class="me-margin-6">
            {{* Delegated mapper object *}}
            {{mb_include module="eai" template="inc_choice_object_delegated" delegated_type='delegated_mapper'}}

            {{* Delegated searcher object *}}
            {{mb_include module="eai" template="inc_choice_object_delegated" delegated_type='delegated_searcher'}}

            {{* Delegated handle object *}}
            {{mb_include module="eai" template="inc_choice_object_delegated" delegated_type='delegated_handle'}}
        </div>
    </th>
</tr>
