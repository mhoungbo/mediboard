{{*
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table>
    {{foreach from=$log.events key=event_name item=_data}}
        {{foreach from=$_data item=value}}
            {{if "logs/events/$event_name"|@tpl_exist:'eai'}}
                {{mb_include template="logs/events/$event_name" module="eai"}}
            {{else}}
                <tr>
                    <th class="me-text-align-left">{{$event_name}}</th>
                </tr>
                <tr>
                    <td>{{$value.json|highlight:json}}</td>
                </tr>
            {{/if}}
        {{/foreach}}
    {{/foreach}}

    {{if $log.context}}
        <tr>
            <th class="me-text-align-left">Context</th>
        </tr>
        <tr>
            <td>{{$log.context|highlight:json}}</td>
        </tr>
    {{/if}}

    {{if $log.extra}}
        <tr>
            <th class="me-text-align-left">Extra</th>
        </tr>
        <tr>
            <td>{{$log.extra|highlight:json}}</td>
        </tr>
    {{/if}}
</table>

