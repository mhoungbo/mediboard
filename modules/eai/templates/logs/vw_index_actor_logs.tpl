{{*
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=eai script=interop_actor ajax=true}}

<script>
    Main.add(function() {
        document.getElementById('button_search_log_filter').onclick();
        var form = getForm('list_actor_logs_filters')
        Calendar.regField(form.elements.date_start)
        Calendar.regField(form.elements.date_end)
    } );
</script>

<form name="list_actor_logs_filters" data-route-list-logs="{{url name='eai_actors_view_logs'}}">
    <input type="hidden" name="actor_guid" value="{{$actor->_guid}}"/>
    <input type="hidden" name="log_start" value="0"/>
    <input type="hidden" name="show_more_logs" value="0"/>
    <table class="form">
        <tr>
            <th>{{tr}}common-Date{{/tr}}</th>
            <td>
                <input type="hidden" name="date_start" class="dateTime"/>
                &raquo;
                <input type="hidden" name="date_end" class="dateTime"/>
            </td>

            <td>
                {{me_form_field nb_cells=2 label='Session ID'}}
                    <input type="text" name="session_uuid"/>
                {{/me_form_field}}
            </td>

            <td>
                {{me_form_field nb_cells=2 label='Message ID'}}
                    <input type="text" name="message_uuid"/>
                {{/me_form_field}}
            </td>

            <td>
                {{me_form_field nb_cells=2 label='Mode'}}
                <select id="elasticsearch-or-file"
                        name="elasticsearch_or_file">
                    <option value="file" {{if !$application_log_datasource || !$elastic_up_application}}selected{{/if}}>
                        Filesystem
                    </option>
                    {{if $application_log_datasource}}
                        <option value="elasticsearch" {{if $elastic_up_application}} selected {{/if}}>
                            {{tr}}common-Elasticsearch{{/tr}}
                        </option>
                    {{/if}}
                </select>
                {{/me_form_field}}
            </td>

            <td>
                <button type="button" id="button_search_log_filter" class="search me-primary" onclick="InteropActor.listActorLogs(this.form, 'actor_container_logs')">
                    {{tr}}Filter{{/tr}}
                </button>
            </td>
        </tr>
    </table>
</form>

<div id="divShowMoreLog"
     style="display: none"
     onclick="InteropActor.showMoreLogs(this, getForm('list_actor_logs_filters'))">
    <i class="fas"></i>
    Afficher plus de logs ...
</div>
<div id="divAllLogsLoaded" style="display: none" >Tous les logs ont �t� charg�s</div>


<div id="list_actor_logs_container">
    <div class="me-display-flex">
        <div id="actor_container_logs" style="max-height: 700px; overflow-y: scroll; width: 40%;"></div>


        <div id="actor_container_logs_details" style="width: 60%; overflow-x: scroll"></div>
    </div>
</div>
