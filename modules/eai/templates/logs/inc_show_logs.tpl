{{*
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
    Main.add(() => {
        {{if $search_more}}
        document.getElementById('divShowMoreLog').style.display = 'block';
        document.getElementById('divAllLogsLoaded').style.display = 'none';
        {{else}}
        document.getElementById('divShowMoreLog').style.display = 'none';
        document.getElementById('divAllLogsLoaded').style.display = 'block';
        {{/if}}

        toggleItems = function (button, session_uid) {
            var elements = Array.from(document.getElementsByClassName(session_uid));
            if (button.classList.contains('fa-chevron-circle-up')) {
                elements.forEach(function (element) {
                    element.style.display = "none";
                });
                button.classList.remove('fa-chevron-circle-up');
                button.classList.add('fa-chevron-circle-down')
            } else {
                elements.forEach(function (element) {
                    element.style.display = "table-row";
                });
                button.classList.remove('fa-chevron-circle-down');
                button.classList.add('fa-chevron-circle-up')
            }
        }
    })
</script>

{{if $nb_logs > 0 }}
    <table class="tbl">
        {{foreach from=$logs_display item=_data_logs key=session_uuid name=for_log_display}}
            <tr class="tr_sessions" data-session_uuid="{{$session_uuid}}">
                <th>Session de log [#{{$session_uuid}}]
                    {{if 'start'|array_key_exists:$_data_logs}}{{$_data_logs.start}}{{else}}--{{/if}}
                    >>
                    {{if 'stop'|array_key_exists:$_data_logs}}{{$_data_logs.stop}} {{else}}--{{/if}}

                    {{if 'logs'|array_key_exists:$_data_logs}}
                        ({{$_data_logs.logs|@count}}{{if $search_more}}+{{/if}})
                    {{else}}
                        (0)
                    {{/if}}
                </th>
                <th>
                    <button id="button-{{$session_uuid}}" type="button"
                            class="fas fa-chevron-circle-down  notext me-tertiary"
                            onclick="toggleItems(this, '{{$session_uuid}}')"></button>
                </th>
            </tr>
            {{if 'logs'|array_key_exists:$_data_logs}}
                {{* Event Object Logs *}}
                {{foreach from=$_data_logs.logs item=_log key=message_uuid name=for_log_display}}
                    <tr style="cursor:pointer; display: none"
                        class="tr_log {{$session_uuid}}"
                        data-session_uuid="{{$session_uuid}}"
                            {{if $_log.type === 'messages'}}
                        data-message_uuid="{{$message_uuid}}"
                        onclick="InteropActor.loadJsonLog(getForm('list_actor_logs_filters'),'{{$session_uuid}}', '{{$message_uuid}}')"
                            {{else}}
                        data-extra="{{$_log.extra_json|htmlentities}}"
                        data-context="{{$_log.context_json|htmlentities}}"
                        onclick="InteropActor.jsonViewer(this)"
                            {{/if}}>
                        {{math equation=x+y x=$smarty.foreach.for_log_display.index y=$log_start assign=index_log}}
                        <td class="section">
                            #{{$index_log}} {{$_log.date}}] [#{{$message_uuid}}]
                        </td>
                    </tr>
                {{/foreach}}
            {{else}}
                <tr>
                    <td>Pas de logs pour cette session</td>
                </tr>
            {{/if}}
        {{/foreach}}
    </table>
{{/if}}
