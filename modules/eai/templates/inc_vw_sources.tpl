{{*
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_include module=system template=inc_pagination total=$total_sources current=$page
change_page='Source.changePage' jumper='10' step=25}}

<table class="tbl me-striped">
    <tr>
        <th class="section button" colspan="3"></th>
        <th class="section narrow">
            {{tr}}CExchangeSource-role-court{{/tr}}
        </th>
        <th class="section narrow" title="{{tr}}CExchangeSource-active{{/tr}}">
            {{tr}}CExchangeSource-active-court{{/tr}}
        </th>
        <th class="section narrow" title="{{tr}}CExchangeSource-loggable{{/tr}}">
            {{tr}}CExchangeSource-loggable-court{{/tr}}
        </th>
        <th class="section" style="width: 30%">
            {{tr}}CExchangeSource-name{{/tr}}
        </th>
        <th class="section" colspan="2">
            {{tr}}Time-response{{/tr}} / {{tr}}Status{{/tr}}
        </th>
    </tr>
    {{foreach from=$_sources name=boucle_source item=_source}}
        <tbody id="line_{{$_source->_guid}}">
        {{mb_include module=eai template=inc_vw_source}}
        </tbody>
        {{foreachelse}}
        <tr>
            <td colspan="6" class="empty">
                {{tr}}{{$name}}.none{{/tr}}
            </td>
        </tr>
    {{/foreach}}
</table>


