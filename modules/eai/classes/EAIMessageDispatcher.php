<?php

/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Components\Cache\LayeredCache;
use Ox\Core\Cache;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Psr\SimpleCache\InvalidArgumentException;

class EAIMessageDispatcher
{
    private const KEY_CACHE = "EAIMessageDispatcher-last-treated-datetime";
    private ExchangeDataFormatFactory $data_format_factory;
    private LayeredCache              $cache;

    /**
     * @param ExchangeDataFormatFactory $data_format_factory
     *
     * @throws CouldNotGetCache
     */
    public function __construct(ExchangeDataFormatFactory $data_format_factory)
    {
        $this->data_format_factory = $data_format_factory;
        $this->cache = Cache::getCache(Cache::DISTR);
    }

    private const TREAT_MAX_MESSAGES = 50;

    /**
     * @param CInteropReceiver $receiver
     * @param array            $options
     *
     * @return int
     * @throws CMbException
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function sendMessagesFor(CInteropReceiver $receiver, array $options = []): int
    {
        // resolve date_min
        $datetime_min  = $options['datetime_min'] ?? $this->getLastDateMessageProcessed($receiver);
        if (!$datetime_min) {
            $datetime_min = CMbDT::dateTime("-1 day");
        }

        $options['datetime_min'] = $datetime_min;

        // load messages
        $messages = $this->getMessagesForReceiver($receiver, $options);

        if (empty($messages)) {
             $this->updateLastTreatedDate($receiver, CMbDT::dateTime());

             return 0;
        }

        // Envoi des messages
        try {
            foreach ($messages as $message) {
                $message->send();
            }
        } catch (CMbException $e) {
            $message->send_datetime = "";
            $message->store();

            throw $e;
        } finally {
            // update when exception or all treated with the last date of message
            if (isset($message) && $message->date_production) {
                $this->updateLastTreatedDate($receiver, $message->date_production);
            }
        }

        return count($messages);
    }

    /**
     * @param CInteropReceiver $receiver
     * @param array            $data
     *
     * @return CExchangeDataFormat[]
     * @throws Exception
     */
    protected function getMessagesForReceiver(CInteropReceiver $receiver, array $data): array
    {
        if (!$receiver->_id) {
            return [];
        }

        $datetime_min = $data['datetime_min'];
        $datetime_max = $data['datetime_max'] ?? CMbDT::dateTime("+1 day");
        $limit    = $data['limit'] ?? self::TREAT_MAX_MESSAGES;

        $exchange = $this->data_format_factory->makeExchangeFromReceiver($receiver);

        $where = [
            'sender_id'               => "IS NULL",
            'receiver_id'             => "= '$receiver->_id'",
            'date_production'         => "BETWEEN '$datetime_min' AND '$datetime_max'",
            'message_valide'          => "= '1'",
            'acquittement_content_id' => "IS NULL",
            'send_datetime'           => "IS NULL",
            'statut_acquittement'     => "IS NULL",
        ];
        $where[]                          = "master_idex_missing = '0' OR master_idex_missing IS NULL";
        $where[]                          = "acquittement_valide IS NULL OR acquittement_valide != '1'";

        $order      = "date_production ASC";
        $forceindex = ["date_production"];

        return $exchange->loadList($where, $order, $limit, null, null, $forceindex);
    }

    /**
     * Get last date treatment for the receiver
     *
     * @param CInteropReceiver $receiver
     *
     * @return string|null
     * @throws InvalidArgumentException
     */
    public function getLastDateMessageProcessed(CInteropReceiver $receiver): ?string
    {
        return $this->cache->get($this->getKeyCache($receiver));
    }

    /**
     * Get key for last date treatment for the receiver
     *
     * @param CInteropReceiver $receiver
     *
     * @return string
     */
    private function getKeyCache(CInteropReceiver $receiver): string
    {
        return self::KEY_CACHE . '-' . $receiver->_guid;
    }

    /**
     * @param CInteropReceiver $receiver
     * @param string           $dateTime
     *
     * @return void
     * @throws InvalidArgumentException
     */
    private function updateLastTreatedDate(CInteropReceiver $receiver, ?string $dateTime): void
    {
        $this->cache->set($this->getKeyCache($receiver), $dateTime, 86400);
    }
}
