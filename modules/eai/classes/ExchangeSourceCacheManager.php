<?php

/**
 * @package Interop\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Ox\Components\Cache\LayeredCache;
use Ox\Core\Cache;
use Ox\Core\Mutex\CMbMutex;
use Ox\Mediboard\System\CExchangeSource;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Manage the cache for exchange sources
 */
class ExchangeSourceCacheManager
{
    private const CACHE_PREFIX       = 'CExchangeSource-cache-';
    private const CACHE_MANAGER_KEYS = self::CACHE_PREFIX . 'all_keys';

    private LayeredCache $cache;

    public function __construct()
    {
        $this->cache = Cache::getCache(LayeredCache::INNER_DISTR);
    }

    /**
     * Set a source on cache
     *
     * @param CExchangeSource $source
     * @param string          $key
     *
     * @return CExchangeSource
     * @throws InvalidArgumentException
     */
    public function setSource(CExchangeSource $source, string $key): CExchangeSource
    {
        // name of source should be required !
        if (!$source->name) {
            return $source;
        }

        // lock
        $lock = new CMbMutex("cache-source-manager-add_key");
        $lock->acquire(260);

        $key = self::CACHE_PREFIX . $key;

        $this->registerCacheKey($source, $key);

        $this->cache->set($key, $source, 86400);

        // unlock
        $lock->release();

        return $source;
    }

    /**
     * Get a source from cache
     *
     * @param string $key
     *
     * @return CExchangeSource|null
     * @throws InvalidArgumentException
     */
    public function getSource(string $key): ?CExchangeSource
    {
        $key = self::CACHE_PREFIX . $key;

        $source = $this->cache->get($key);

        return $source instanceof CExchangeSource ? $source : null;
    }

    /**
     * When a source is cached with a key, we store this key and associate with source
     *
     * @param CExchangeSource $source
     * @param string          $key
     *
     * @return void
     * @throws InvalidArgumentException
     */
    private function registerCacheKey(CExchangeSource $source, string $key): void
    {
        $cache_manager_keys = $this->getCacheKeysManager();
        $cache_manager_keys[$source->name][] = $key;

        $this->cache->set(self::CACHE_MANAGER_KEYS, $cache_manager_keys);
    }

    /**
     * Invalidate cache for exchange sources
     *
     * Invalidate cache for all sources or you can given some sources explicitly with argument $sources
     *
     * @param CExchangeSource ...$sources
     *
     * @return void
     * @throws InvalidArgumentException
     */
    public function invalidateCache(CExchangeSource ...$sources): void
    {
        if (!$cache_manager_keys = $this->getCacheKeysManager()) {
            return;
        }

        if (empty($sources)) { // invalidate all
            $source_names = array_keys($cache_manager_keys);
        } else { // invalidate only sources
            $source_names = array_unique(array_column($sources, 'name'));
        }

        foreach ($source_names as $source_name) {
            $keys = $cache_manager_keys[$source_name] ?? [];
            if (empty($keys)) {
                continue;
            }

            // delete all keys for source_guid
            foreach ($keys as $key) {
                $this->cache->delete($key);
            }

            unset($cache_manager_keys[$source_name]);
        }

        $this->cache->set(self::CACHE_MANAGER_KEYS, $cache_manager_keys);
    }

    private function getCacheKeysManager(): array
    {
        return $this->cache->get(self::CACHE_MANAGER_KEYS) ?: [];
    }
}
