<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Ox\Core\Module\AbstractModuleCache;
use Psr\SimpleCache\InvalidArgumentException;

class ModuleCacheEAI extends AbstractModuleCache
{
    public function getModuleName(): string
    {
        return 'eai';
    }

    /**
     * @inheritdoc
     * @throws InvalidArgumentException
     */
    public function clearSpecialActions(): void
    {
        parent::clearSpecialActions();

        // invalidate cache for all sources
        (new ExchangeSourceCacheManager())->invalidateCache();
    }
}
