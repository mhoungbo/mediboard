<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Countable;
use Iterator;
use JsonSerializable;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CSmartyDP;
use Ox\Mediboard\CompteRendu\CWkHtmlToPDFConverter;
use ReturnTypeWillChange;

/**
 * To allow to generate report after execute an action
 */
class CReport implements Countable, Iterator, JsonSerializable
{
    /** @var string */
    private $title;

    /** @var CItemReport[] */
    protected $items = [];

    /** @var int */
    private $position;

    public function __construct(string $title)
    {
        $this->position         = 0;
        $this->title            = $title;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Add item to report
     *
     * @param string $data
     * @param int    $severity
     *
     * @return CReport
     */
    public function addData(string $data, int $severity): self
    {
        $this->items[] = new CItemReport($data, $severity);

        return $this;
    }

    /**
     * @param CItemReport $item
     *
     * @return CReport
     */
    public function addItem(CItemReport $item): self
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Add item to report
     *
     * @param int $severity
     *
     * @return CItemReport[]
     */
    public function getItems(int $severity = null): array
    {
        if ($severity === null) {
            return $this->items;
        }

        return array_filter($this->items, fn(CItemReport $item) => $item->getSeverity() === $severity);
    }

    /**
     * Transforme report in JSON string
     *
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this);
    }

    /**
     * Transform json report to a CReport
     *
     * @param string $json
     *
     * @return CReport
     */
    public static function toObject(string $json): CReport
    {
        $data  = json_decode($json, true);
        $title = mb_convert_encoding(CMbArray::get($data, 'title', 'Report'), 'ISO-8859-1', 'UTF-8');
        $report = new CReport($title);

        foreach (CMbArray::get($data, 'items', []) as $json_item) {
            $report->addItem((new CItemReport('', 0))->toObject($json_item));
        }

        return $report;
    }


    /**
     * @return mixed
     */
    #[ReturnTypeWillChange]
    public function current()
    {
        return $this->items[$this->position];
    }

    /**
     * @return void
     */
    public function next(): void
    {
        ++$this->position;
    }

    /**
     * @return mixed
     */
    #[ReturnTypeWillChange]
    public function key()
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->items[$this->position]);
    }


    /**
     * @return void
     */
    public function rewind(): void
    {
        $this->position = 0;
        $this->items    = array_values($this->items);
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }


    /**
     * @return mixed
     */
    #[ReturnTypeWillChange]
    public function jsonSerialize()
    {
        $result = [
            'title' => mb_convert_encoding($this->title, 'UTF-8', 'ISO-8859-1'),
            'items' => []
        ];
        foreach ($this->getItems() as $item) {
            $result['items'][] = json_encode($item);
        }

        return $result;
    }

    /**
     * @return string
     * @throws CMbException
     */
    public function getContentMail(): string
    {
        $step = intval($this->position) + 1;

        $smarty = new CSmartyDP('modules/eai');
        $smarty->assign('report', $this);
        $smarty->assign('title', CAppUI::tr('CDeployStep') . "({$step})");
        $content = $smarty->fetch('report/inc_report_mail');

        CWkHtmlToPDFConverter::init('CWkHtmlToPDFConverter');

        return CWkHtmlToPDFConverter::convert($content, 'a4', 'portrait');
    }

    /**
     * @param array $data
     * @param int   $severity
     */
    public function makeListItems(array $data, int $severity): void
    {
        foreach ($data as $class => $values) {
            $main_item = new CItemReport(CAppUI::tr($class), $severity);
            foreach ($values as $value) {
                $item = new CItemReport($value, $severity);
                $main_item->addSubItem($item);
            }

            $this->addItem($main_item);
        }
    }
}
