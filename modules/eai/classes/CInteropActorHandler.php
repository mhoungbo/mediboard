<?php

/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Exception;
use Ox\Core\CClassMap;
use Ox\Core\CStoredObject;
use Ox\Core\Handlers\ObjectHandler;
use Ox\Interop\Eai\Logger\InteropLoggerManager;

/**
 * Class CInteropActorHandler
 * Interop actor handler
 */
class CInteropActorHandler extends ObjectHandler
{
    /** @var array */
    public static $handled = ["CInteropActor"];

    /**
     * @inheritdoc
     * @param CStoredObject $object
     * @return bool
     * @throws Exception
     */
    public static function isHandled(CStoredObject $object): bool
    {
        foreach (self::$handled as $_handled_class) {
            if (is_subclass_of($object, CClassMap::getInstance()->getAliasByShortName($_handled_class))) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     * @param CStoredObject $object
     * @return bool
     * @throws Exception
     */
    public function onBeforeStore(CStoredObject $object): bool
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        /** @var CInteropActor $actor */
        $actor = $object;

        $this->getLoggerManager($actor)->beforeActorStore();

        return true;
    }

    /**
     * @inheritdoc
     * @param CStoredObject $object
     * @return bool
     * @throws Exception
     */
    public function onAfterStore(CStoredObject $object): bool
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        // Cas d'une fusion
        if ($object->_merging) {
            return false;
        }

        if ($object->_forwardRefMerging) {
            return false;
        }

        /** @var CInteropActor $actor */
        $actor = $object;

        $this->getLoggerManager($actor)->afterActorStore();

        // Send e-mail if actor create
        CEAITools::notifyNewActor($actor);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function onBeforeMerge(CStoredObject $object)
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        if (!$object->_merging) {
            return false;
        }


        return true;
    }

    /**
     * @inheritdoc
     */
    public function onMergeFailure(CStoredObject $object)
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function onAfterMerge(CStoredObject $object)
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        if (!$object->_merging) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function onBeforeDelete(CStoredObject $object)
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function onAfterDelete(CStoredObject $object)
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        return true;
    }

    /**
     * @param CInteropActor $actor
     * @return InteropLoggerManager
     */
    private function getLoggerManager(CInteropActor $actor): InteropLoggerManager
    {
        static $actors = [];

        $name = $actor->_class . '-' . $actor->_id . "." . $actor->nom;
        if (null === ($actors[$name] ?? null)) {
            $actors[$name] = new InteropLoggerManager($actor);
        }

        return $actors[$name];
    }
}
