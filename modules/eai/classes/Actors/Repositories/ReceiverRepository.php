<?php

/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Actors\Repositories;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Config\Conf;
use Ox\Core\CSQLDataSource;
use Ox\Core\Logger\LoggerLevels;
use Ox\Core\Module\CModule;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\CInteropReceiver;
use Ox\Interop\Eai\CInteropReceiverFactory;
use Ox\Interop\Eai\CMessageSupported;
use Ox\Interop\Eai\ExchangeSources\Repository\ExchangeSourceRepository;
use Psr\Log\LoggerInterface;

class ReceiverRepository
{
    final public const ONLY_ACCESSIBLE = 'opt_only_receiver_accessible';
    final public const ONLY_ACTIVE = 'opt_only_active';
    final public const ONLY_GROUPS = 'opt_only_groups';
    final public const WITH_CONFIGURATIONS = 'opt_with_configurations';
    final public const WITH_MESSAGE_SUPPORTED = 'opt_with_message_supported';

    public function __construct(
        private LoggerInterface          $logger,
        private CInteropReceiverFactory  $receiver_factory,
        private Conf                     $conf,
        private ExchangeSourceRepository $source_repository,
        private CSQLDataSource           $ds
    ) {
    }

    /**
     * Search receivers compatible with options given
     *
     * @param class-string<CInteropReceiver>|CInteropReceiver $receiver
     * @param array                                           $options
     *
     * @return array
     * @throws Exception
     */
    public function getReceivers(string|CInteropReceiver $receiver, array $options = []): array
    {
        $options = $this->mergeOptions($options);
        if (is_string($receiver)) {
            $receiver = $this->receiver_factory->make($receiver);
        }

        $where          = [];
        $ljoin          = [];
        $group          = null;
        $receiver_table = $receiver->_spec->table;

        // actif
        if ($options[self::ONLY_ACTIVE] || $options[self::ONLY_ACCESSIBLE]) {
            $where["$receiver_table.actif"] = $this->ds->prepare(' = ?', 1);
        }

        // role
        $where["$receiver_table.role"] = $this->ds->prepare(' = ?', $this->conf->get("instance_role"));

        // groups
        if ($group_id = $options[self::ONLY_GROUPS]) {
            if (is_array($group_id)) {
                $where["$receiver_table.group_id"] = $this->ds->prepareIn($group_id);
            } else {
                $where["$receiver_table.group_id"] = $this->ds->prepare(' = ?', $group_id);
            }
        }

        // With configurations
        if ($options[self::WITH_CONFIGURATIONS]) {
            $this->filterForConfiguration($receiver, $options, $where, $ljoin, $group);
        }

        // With message supported
        if ($options[self::WITH_MESSAGE_SUPPORTED]) {
            $this->filterForMessageSupported($receiver, $options, $where, $ljoin, $group);
        }

        $receivers = $receiver->loadList($where, null, null, $group, $ljoin);

        if ($options[self::ONLY_ACCESSIBLE]) {
            $receivers = $this->filterOnlyAccessible(...$receivers);
        }

        // propagate Conf object
        foreach ($receivers as $receiver) {
            $receiver->setConf($this->conf)
                ->setSourceRepository($this->source_repository);
        }

        return $receivers;
    }

    /**
     * Merge given options with default options
     *
     * @param array $options
     *
     * @return array
     */
    private function mergeOptions(array $options): array
    {
        return array_replace_recursive($this->getDefaultOptions(), $options);
    }

    /**
     * Default values foreach options
     *
     * @return array
     */
    private function getDefaultOptions(): array
    {
        return [
            self::ONLY_ACCESSIBLE        => false,
            self::ONLY_GROUPS            => null, // null|string|string[] : group_id
            self::ONLY_ACTIVE            => true,
            self::WITH_CONFIGURATIONS    => [], // array<[feature, value]>
            self::WITH_MESSAGE_SUPPORTED => [], // array<CMessageSupported|
            //['profil' => v, 'transaction' => v, 'message' => v]>
        ];
    }

    /**
     * Filter receiver for match with configuration
     *
     * @param CInteropReceiver $receiver
     * @param array            $options
     * @param array            $where
     * @param array            $ljoin
     * @param string|null      $group
     * @return void
     * @throws Exception
     */
    private function filterForConfiguration(
        CInteropReceiver $receiver,
        array            $options,
        array            &$where,
        array            &$ljoin,
        ?string          &$group
    ): void {
        $configurations = $options[self::WITH_CONFIGURATIONS];

        $role_instance = $this->conf->get("instance_role");
        $key_value     = ($role_instance === 'prod') ? 'value' : 'alt_value';
        $index = 0;
        foreach ($configurations as $feature => $value) {
            $alias = "conf$index";
            $where[] = $this->ds->prepare(
                " `$alias`.`feature` = ?1 AND `$alias`.`$key_value` = ?2",
                $feature,
                $value
            );
            $index++;
        }

        $receiver_primary_key   = $receiver->getPrimaryKey();
        $receiver_table         = $receiver->_spec->table;
        $receiver_class         = $receiver->_class;

        for ($i =0; $i < $index; $i++) {
            $alias = "conf$i";
            $ljoin[] = "`configuration` as $alias ON `$alias`.`object_id` = `$receiver_table`.`$receiver_primary_key`
                                            AND `$alias`.`object_class` = '$receiver_class'";
        }

        $group = "`$receiver_table`.`$receiver_primary_key`";
    }

    /**
     * Filter receiver for match with configured messages supported
     *
     * @param CInteropReceiver $receiver
     * @param array            $options
     * @param array            $where
     * @param array            $ljoin
     * @param string|null      $group
     * @return void
     */
    private function filterForMessageSupported(
        CInteropReceiver $receiver,
        array            $options,
        array            &$where,
        array            &$ljoin,
        ?string          &$group
    ): void {
        $messages_supported = $options[self::WITH_MESSAGE_SUPPORTED];

        // allow to pass one or multiple config
        $key_first   = array_key_first($messages_supported);
        $first_value = reset($messages_supported);
        if (($first_value instanceof CMessageSupported) || is_string($key_first)) {
            $messages_supported = [$messages_supported];
        }

        $receiver_primary_key       = $receiver->getPrimaryKey();
        $receiver_class             = $receiver->_class;
        $receiver_table             = $receiver->_spec->table;

        /** @var CMessageSupported|array $message_supported */
        foreach ($messages_supported as $index => $message_supported) {
            $alias = "msg_$index";
            $is_object = is_object($message_supported);

            $values = [
                "$alias.transaction" =>
                    $is_object ? ($message_supported->transaction ?: null) : ($message_supported['transaction'] ?? null),
                "$alias.message"     =>
                    $is_object ? ($message_supported->message ?: null) : ($message_supported['message'] ?? null),
                "$alias.profil"      =>
                    $is_object ? ($message_supported->profil ?: null) : ($message_supported['profil'] ?? null)
            ];
            $values = array_filter($values);
            if (empty($values)) {
                continue;
            }

            $values["$alias.active"] = 1; // only active message supported

            $constraint = '';
            foreach ($values as $key => $value) {
                $constraint = $constraint
                    . (($constraint === '') ? '' : ' AND ') . $this->ds->prepare(" $key = ?", $value);
            }

            $where[] = "( $constraint )";
            $ljoin[] = "`message_supported` as $alias ON `$alias`.`object_id` = `$receiver_table`.`$receiver_primary_key`
                                            AND `$alias`.`object_class` = '$receiver_class'";
        }

        $group   = "`$receiver_table`.`$receiver_primary_key`";
    }

    /**
     * @param CInteropReceiver ...$receivers
     *
     * @return CInteropReceiver[]
     * @throws Exception
     */
    private function filterOnlyAccessible(CInteropReceiver ...$receivers): array
    {
        $accessible_receivers = [];
        foreach ($receivers as $receiver) {
            if ($this->hasSourceAccessible($receiver)) {
                $accessible_receivers[$receiver->_id] = $receiver;
            }
        }

        return $accessible_receivers;
    }

    /**
     * Test if receiver has a source accessible
     *
     * @param CInteropReceiver $receiver
     *
     * @return bool
     * @throws Exception
     */
    private function hasSourceAccessible(CInteropReceiver $receiver): bool
    {
        foreach ($receiver->loadRefsExchangesSources() as $_source) {
            // only active sources
            if (!$_source->_id || !$_source->active) {
                continue;
            }

            // Only receiver with sources reachable
            try {
                $force_accessible = CModule::getActive('doctolib')
                    && ($receiver->type === CInteropActor::ACTOR_DOCTOLIB);

                if ($force_accessible || $_source->getClient()->isReachableSource()) {
                    return true;
                }
            } catch (CMbException $e) {
                $this->logger->log(
                    LoggerLevels::LEVEL_WARNING,
                    $e,
                    ['action' => 'ReceiverRepository', 'receiver' => $receiver]
                );
            }
        }

        return false;
    }
}
