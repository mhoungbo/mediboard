<?php

/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Ox\Interop\Dmp\CExchangeDMP;
use Ox\Interop\Dmp\CReceiverDMP;
use Ox\Interop\Fhir\Actors\CReceiverFHIR;
use Ox\Interop\Fhir\CExchangeFHIR;
use Ox\Interop\Hl7\CExchangeHL7v2;
use Ox\Interop\Hl7\CExchangeHL7v3;
use Ox\Interop\Hl7\CReceiverHL7v2;
use Ox\Interop\Hl7\CReceiverHL7v3;
use Ox\Interop\Hprimsante\CExchangeHprimSante;
use Ox\Interop\Hprimsante\CReceiverHprimSante;
use Ox\Interop\Hprimxml\CDestinataireHprim;
use Ox\Interop\Hprimxml\CEchangeHprim;

/**
 * Class CExchangeDataFormat
 * Exchange Data Format
 */
class ExchangeDataFormatFactory
{
    /**
     * Get DMP exchange
     *
     * @return CExchangeDMP
     */
    public function makeExchangeDMP(): CExchangeDMP
    {
        return new CExchangeDMP();
    }

    /**
     * Get HL7v2 exchange
     *
     * @return CExchangeHL7v2
     */
    public function makeExchangeHL7v2(): CExchangeHL7v2
    {
        return new CExchangeHL7v2();
    }

    /**
     * Get HL7v3 exchange
     *
     * @return CExchangeHL7v3
     */
    public function makeExchangeHL7v3(): CExchangeHL7v3
    {
        return new CExchangeHL7v3();
    }

    /**
     * Get FHIR exchange
     *
     * @return CExchangeFHIR
     */
    public function makeExchangeFHIR(): CExchangeFHIR
    {
        return new CExchangeFHIR();
    }

    /**
     * Get H'XML exchange
     *
     * @return CEchangeHprim
     */
    public function makeExchangeHprimXML(): CEchangeHprim
    {
        return new CEchangeHprim();
    }

    /**
     * Get H'Sante exchange
     *
     * @return CExchangeHprimSante
     */
    public function makeExchangeHprimSante(): CExchangeHprimSante
    {
        return new CExchangeHprimSante();
    }

    /**
     * Get exchange from a receiver type
     *
     * @param CInteropReceiver $receiver
     *
     * @return CExchangeDataFormat
     * @throws ExchangeDataFormatException
     */
    public function makeExchangeFromReceiver(CInteropReceiver $receiver): CExchangeDataFormat
    {
        switch (get_class($receiver)) {
            case CReceiverHprimSante::class:
                return $this->makeExchangeHprimsante();
            case CReceiverHL7v2::class:
                return $this->makeExchangeHL7v2();
            case CReceiverHL7v3::class:
                return $this->makeExchangeHL7v3();
            case CReceiverDMP::class:
                return $this->makeExchangeDMP();
            case CReceiverFHIR::class:
                return $this->makeExchangeFHIR();
            case CDestinataireHprim::class:
                return $this->makeExchangeHprimXML();
            default:
                throw new ExchangeDataFormatException(
                    ExchangeDataFormatException::NOT_SUPPORTED_RECEIVER,
                    500,
                    get_class($receiver)
                );
        }
    }
}
