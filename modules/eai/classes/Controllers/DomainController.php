<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Controllers;

use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestFieldsets;
use Ox\Core\CMbException;
use Ox\Core\CModelObject;
use Ox\Core\Controller;
use Ox\Interop\Eai\CDomain;
use Symfony\Component\HttpFoundation\Response;

class DomainController extends Controller
{
    /**
     * @throws ApiException|CMbException
     * @api
     */
    public function create(RequestApi $request_api): Response
    {
        $domains = $request_api->getModelObjectCollection(
            CDomain::class,
            [CModelObject::FIELDSET_DEFAULT],
        );

        $collection = $this->storeCollection($domains);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }
}
