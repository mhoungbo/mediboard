<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Controllers;

use Exception;
use InvalidArgumentException;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\CMbException;
use Ox\Core\CModelObject;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Interop\Eai\CExchangeDataFormat;
use Ox\Interop\Eai\CMessageSupported;
use Ox\Mediboard\System\CConfiguration;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class MessageSupportedController extends Controller
{
    /**
     * @throws ApiException|CMbException
     * @api
     */
    public function create(RequestApi $request_api): Response
    {
        $messages_supported = $request_api->getModelObjectCollection(
            CMessageSupported::class,
            [CModelObject::FIELDSET_DEFAULT],
        );

        $collection = $this->storeCollection($messages_supported);

        return $this->renderApiResponse($collection, Response::HTTP_CREATED);
    }


    /**
     * @param RequestParams $request_params
     * @return Response
     * @throws Exception
     *
     * @api
     */
    public function updateDelegatedObject(RequestParams $request_params): Response
    {
        if (!$this->isCsrfTokenValid('eai_update_delegated_object', $request_params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $message_supported_ids = $request_params->post('message_supported_ids', 'str notNull');
        if (empty($message_supported_ids)) {
            throw new InvalidArgumentException('invalid parameter message_supported_ids');
        }

        $messages_supported = (new CMessageSupported())->loadAll($message_supported_ids);
        $delegated_type     = $request_params->post(
            'delegated_type',
            'enum list|' . implode('|', CExchangeDataFormat::DELEGATED_OBJECTS) . " notNull"
        );
        $delegated_value    = $request_params->post('delegated_value', 'str') ?: '';
        $module             = $request_params->post('module', 'str notNull');

        $error = false;
        foreach ($messages_supported as $message_supported) {
            $msg = CConfiguration::setConfig(
                "$module delegated_objects $delegated_type",
                $delegated_value,
                $message_supported,
            );

            if ($msg) {
                $error = true;
                $this->addUiMsgError($msg, true);
            }
        }

        if ($error) {
            $this->addUiMsgError('CMessageSupported-msg-fail configure message supported', true);
        } else {
            $this->addUiMsgOk('CMessageSupported-msg-configure message supported', true);
        }

        return $this->renderEmptyResponse($error ? Response::HTTP_INTERNAL_SERVER_ERROR : Response::HTTP_CREATED);
    }
}
