<?php

/**
 * @package Mediboard\EAI
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Controllers\Actors;

use Exception;
use InvalidArgumentException;
use Ox\Core\CMbDT;
use Ox\Core\Controller;
use Ox\Core\Elastic\Exceptions\ElasticClientException;
use Ox\Core\Elastic\Exceptions\ElasticException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Interop\Eai\CInteropActor;
use Ox\Mediboard\Developpement\Logger\ApplicationLogRepository;
use Ox\Mediboard\System\Elastic\ErrorLogRepository;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class CEAIController
 */
class ActorsLogsController extends Controller
{
    /**
     * @param RequestParams $params
     * @return Response
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function showIndex(RequestParams $params): Response
    {
        $actor_guid = $params->get('actor_guid', 'guid class|CInteropActor');
        if (!$actor_guid) {
            throw new InvalidArgumentException(
                $this->translator->tr('common-error-Missing parameter: %s', 'actor_guid')
            );
        }

        $elastic_up_application = false;
        if ($this->conf->get("application_log_using_nosql")) {
            try {
                $elastic_up_application = true;
                $repo                   = new ErrorLogRepository();
                $repo->count();
            } catch (Throwable $e) {
                $elastic_up_application = false;
            }
        }

        return $this->renderSmarty(
            'logs/vw_index_actor_logs',
            [
                'actor'                      => CInteropActor::loadFromGuid($actor_guid),
                'elastic_up_application'     => $elastic_up_application,
                'application_log_datasource' => empty($this->conf->get('application_log_datasource')),
            ]
        );
    }

    /**
     * @param RequestParams            $params
     * @param ApplicationLogRepository $repository
     * @return Response
     * @throws ElasticClientException
     * @throws ElasticException
     * @throws Exception
     */
    public function listLogs(RequestParams $params, ApplicationLogRepository $repository): Response
    {
        $log_start    = $params->get("log_start", "num default|0");
        $mode         = $params->get('mode', 'str default|file');
        $message_uuid = $params->get("message_uuid", "str");

        if ($message_uuid) {
            return $this->showLogInfos($params, $repository);
        }

        $start = microtime(true);
        $logs  = $repository->search(
            $mode,
            [
                'log_start'   => $log_start,
                'grep_search' => $this->prepareSearchRegexForActor($params),
                'grep_regex'  => 1
            ]
        );

        $exec_time    = microtime(true) - $start;
        $nb_logs      = count($logs);
        $display_logs = $this->prepareLogs($logs);

        return $this->renderSmarty(
            'logs/inc_show_logs',
            [
                'actor'        => CInteropActor::loadFromGuid(
                    $params->get('actor_guid', 'guid class|CInteropActor')
                ),
                'logs_display' => $display_logs,
                'nb_logs'      => $nb_logs,
                'search_more'  => $nb_logs === 1000,
                'exec_time'    => $exec_time,
                'log_start'    => $log_start,
            ]
        );
    }

    /**
     * @param RequestParams            $params
     * @param ApplicationLogRepository $repository
     * @return Response
     * @throws ElasticClientException
     * @throws ElasticException
     */
    private function showLogInfos(RequestParams $params, ApplicationLogRepository $repository): Response
    {
        $log_start = $params->get("log_start", "num default|0");
        $mode      = $params->get('mode', 'str default|file');
        $logs      = $repository->search(
            $mode,
            [
                'log_start'   => $log_start,
                'grep_search' => $this->prepareSearchRegexForActor($params),
                'grep_regex'  => 1
            ]
        );

        if ($logs) {
            $log   = reset($logs);
            $infos = $this->extractInfosFromLog($log);

            $data = [
                'message_uuid' => $infos['message_uuid'] ?? 'none',
                'session_uuid' => $infos['session_uuid'] ?? 'none',
                'message'      => $log['message'],
                'events'       => null,
                'context'      => null,
                'extra'        => null
            ];

            // extra
            $extra = $log['extra_json'] ? json_decode($log['extra_json'], true) : $log['extra_json'];
            if ($extra) {
                $data['extra'] = json_encode($extra, JSON_PRETTY_PRINT);
            }

            // context
            $context = json_decode($log['context_json'], true);
            if (array_key_exists('extraContext', $context)) {
                $data['context'] = json_encode($context['extraContext'], JSON_PRETTY_PRINT);
                unset($context['extraContext']);
            }

            // events
            foreach ($context as $event => $values) {
                if (!is_array($values)) {
                    continue;
                }

                foreach ($values as $value) {
                    $data['events'][$event][] = [
                        'json' => json_encode($value, JSON_PRETTY_PRINT),
                        'data' => $value
                    ];
                }
            }
        }

        return $this->renderSmarty('logs/inc_show_log', ['log' => $data ?? []]);
    }

    /**
     * @param array $log
     * @return array
     */
    private function extractInfosFromLog(array $log): array
    {
        preg_match(
            "#\[EAI\] \[(?'actor_guid'[\w\-]+)\]\[(?'session_uuid'\w+)\](\[(?'message_uuid'\w+)\])? ?(?'log_bounds'(?:start|stop)+ log)?#",
            $log['message'],
            $matches
        );

        return $matches;
    }

    private function prepareLogs(array $logs): array
    {
        $display_logs = [];
        foreach ($logs as $log) {
            $matches = $this->extractInfosFromLog($log);
            // prevent logs which don't has session_uuid
            if (!array_key_exists('session_uuid', $matches)) {
                continue;
            }

            // catch logs for start and stop session dates
            if (array_key_exists('log_bounds', $matches)) {
                $is_start                                     = str_starts_with($matches['log_bounds'], 'start');
                $key                                          = $is_start ? 'start' : 'stop';
                $display_logs[$matches['session_uuid']][$key] = str_replace(['[', ']'], '', $log['date']);

                continue;
            }

            $log['type'] = array_key_exists('message_uuid', $matches) ? 'messages' : 'raw';
            if (array_key_exists('message_uuid', $matches)) {
                $display_logs[$matches['session_uuid']]['logs'][$matches['message_uuid']] = $log;
            } else {
                $log['type']         = 'raw';
                $context             = json_decode($log['context_json'], true);
                $extra               = $log['extra_json'] ? json_decode($log['extra_json'], true) : $log['extra_json'];
                $log['extra_json']   = json_encode($extra, JSON_PRETTY_PRINT);
                $log['context_json'] = json_encode($context, JSON_PRETTY_PRINT);

                $display_logs[$matches['session_uuid']]['logs'][] = $log;
            }
        }

        return $display_logs;
    }

    /**
     * @param RequestParams $params
     * @return string
     * @throws Exception
     * @throws InvalidArgumentException
     */
    private function prepareSearchRegexForActor(RequestParams $params): string
    {
        $actor_guid = $params->get('actor_guid', 'guid class|CInteropActor');
        if (!$actor_guid) {
            throw new InvalidArgumentException(
                $this->translator->tr('common-error-Missing parameter: %s', 'actor_guid')
            );
        }

        $path       = '';
        $date_start = $params->get('date_start', 'dateTime');
        $date_end   = $params->get('date_end', 'dateTime');
        if ($date_start && $date_end) {
            $nb_days = CMbDT::daysRelative($date_start, $date_end);

            $dates         = [];
            $date_relative = CMbDT::date($date_start);
            for ($i = 0; $i < $nb_days; $i++) {
                $dates[] = '(' . preg_quote(CMbDT::date("+$i DAYS", $date_relative)) . ')';
            }

            $date_formated = implode('|', $dates);
            $path          = "\[({$date_formated}) .*\]";
        } elseif ($date_start || $date_end) {
            $date = preg_quote(CMbDT::date(null, $date_end ?: $date_start));

            $path .= "\[$date .*\]";
        }

        $path .= ($path ? ' ' : '') . preg_quote("[EAI] [{$actor_guid}]");

        $session_uuid = $params->get('session_uuid', 'str');
        if ($session_uuid) {
            $path .= preg_quote("[$session_uuid]");
        }

        $message_uuid = $params->get("message_uuid", "str");
        if ($message_uuid) {
            $path .= preg_quote("[$message_uuid]");
        }

        // fix because search files use grep
        $path = str_replace('(', '\(', $path);
        $path = str_replace(')', '\)', $path);
        $path = str_replace('|', '\|', $path);

        // [date]? [EAI] [$actor_guid][$session_uuid]?

        return "$path";
    }
}
