<?php
/**
 * @package Mediboard\EAI
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Controllers;

use Exception;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\FileClientInterface;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Exception\ControllerException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\System\Sources\CSourceFile;
use Symfony\Component\HttpFoundation\Response;

class SourceFileController extends Controller
{
    private function getSource(RequestParams $request_params): CSourceFile
    {
        $source_type = $request_params->request('source_type', 'enum list|FTP|FS|SFTP');
        $source      = CSourceFile::getSourceFromType($source_type ?? '');
        if (!$source) {
            throw new AccessDeniedException($this->translator->tr("common-error-No permission"));
        }

        $source_id = $request_params->request('source_id', "ref class|$source->_class");

        return $source::findOrFail($source_id);
    }

    /**
     * @param RequestParams $params
     *
     * @return Response
     * @throws CMbException
     * @api
     */
    public function createDirectories(RequestApi $request_api, RequestParams $params): Response
    {
        $source                   = $this->getSource($params);

        // check permission administration for the source
        if (!$this->checkPermAdmin($source->_ref_module)) {
            throw new AccessDeniedException($this->translator->tr("common-error-No permission"));
        }

        $content = $request_api->getContent();
        if (!($path = $content['path'] ?? null)) {
            throw new ControllerException(
                Response::HTTP_PRECONDITION_FAILED,
                $this->translator->tr('common-error-Missing parameter: %s', 'path')
            );
        }

        // controll path
        $source_path = $source->completePath();

        if (!str_starts_with($path, $source_path)) {
            throw new ControllerException(
                Response::HTTP_PRECONDITION_FAILED,
                $this->translator->tr(
                    'CSourceFile-msg-invalid path "%s", it should be start with the source path "%s"',
                    $path,
                    $source_path
                )
            );
        }

        $client = $source->getClient();

        // control directory exist
        $directory_exists = $this->isDirectoryExists($client, $path);
        if ($directory_exists) {
            throw new ControllerException(
                Response::HTTP_PRECONDITION_FAILED,
                $this->translator->tr('CSourceFile-msg-directory already exist', 'path')
            );
        }

        // create directories
        $client->createDirectory($path);

        // test if directory exist
        $client->getListDirectory($path);

        return $this->renderJsonResponse(json_encode(['path' => $path]));
    }

    /**
     * Check if directory exists
     *
     * @param FileClientInterface $client
     * @param string              $path
     *
     * @return bool
     */
    private function isDirectoryExists(FileClientInterface $client, string $path): bool
    {
        try {
            $client->getListDirectory($path);

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }


    /**
     * @param RequestApi    $request_api
     * @param RequestParams $params
     *
     * @return Response
     * @throws CMbException
     * @api
     */
    public function deleteDirectories(RequestApi $request_api, RequestParams $params): Response
    {
        $source                   = $this->getSource($params);

        // check permission administration for the source
        if (!$this->checkPermAdmin($source->_ref_module)) {
            throw new AccessDeniedException($this->translator->tr("common-error-No permission"));
        }

        $content = $request_api->getContent();
        if (!($path = $content['path'] ?? null)) {
            throw new ControllerException(
                Response::HTTP_PRECONDITION_FAILED,
                $this->translator->tr('common-error-Missing parameter: %s', 'path')
            );
        }

        $purge_directory = $params->get('purge', 'bool default|0');

        // controll path
        $source_path = $source->completePath();
        if (!str_starts_with($path, $source_path)) {
            throw new ControllerException(
                Response::HTTP_PRECONDITION_FAILED,
                $this->translator->tr(
                    'CSourceFile-msg-invalid path "%s", it should be start with the source path "%s"',
                    $path,
                    $source_path
                )
            );
        }

        $client = $source->getClient();

        // control directory exist
        $directory_exists = $this->isDirectoryExists($client, $path);
        if (!$directory_exists) {
            throw new ControllerException(
                Response::HTTP_PRECONDITION_FAILED,
                $this->translator->tr('CSourceFile-msg-directory not exist', $path)
            );
        }

        // create directories
        $client->removeDirectory($path, $purge_directory);

        return $this->renderJsonResponse(json_encode(['path' => $path]));
    }
}
