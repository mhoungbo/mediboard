<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Controllers\Legacy;

use Exception;
use Ox\Core\Api\Request\RequestApiBuilder;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Core\CMbObject;
use Ox\Core\CStoredObject;
use Ox\Core\CValue;
use Ox\Core\CView;
use Ox\Interop\Eai\CConfigurationEai;
use Ox\Interop\Eai\CEchangeXML;
use Ox\Interop\Eai\CExchangeBinary;
use Ox\Interop\Eai\CExchangeDataFormat;
use Ox\Interop\Eai\CExchangeTabular;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\CInteropReceiver;
use Ox\Interop\Eai\CInteropSender;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\CConfiguration;
use Ox\Mediboard\System\ContextualConfigurationManager;
use Ox\Mediboard\System\Controllers\ConfigurationController;

class CActorControllerLegacy extends CLegacyController
{
    /**
     * Show configuration template for actor config
     */
    public function showConfigObjectValues(): void
    {
        CCanDo::checkEdit();
        $actor_guid = CView::get('object_guid', 'str') ?: CView::get('actor_guid', 'str');
        $mode       = CView::get('mode', 'str') ?: ContextualConfigurationManager::getConfigurationMode();
        CView::checkin();

        [$actor_class, $actor_uid] = explode('-', $actor_guid);

        // retrieve all configuration actors
        $configs       = (new CConfigurationEai())->getAllConfigActors();
        $configs_actor = CMbArray::getRecursive($configs, "$actor_class eai", []);

        // compute all format availlable
        $config_sections = array_keys($configs_actor);
        $formats         = array_unique(
            array_map(function ($section_name) {
                return substr($section_name, 0, strpos($section_name, '-'));
            }, $config_sections)
        );

        $this->renderSmarty(
            'inc_config_object_values',
            [
                'mode'         => $mode,
                'module'       => 'eai',
                'object_class' => $actor_class,
                'actor_guid'   => $actor_guid,
                'formats'      => array_values($formats),
            ]
        );
    }

    /**
     * Export all configurations for an actor in the $format given
     */
    public function exportConfigs(): void
    {
        CCanDo::checkEdit();
        $actor_guid = CView::get('object_guid', 'str');
        $format     = CView::get('format', 'str');
        CView::checkin();

        [$actor_class, $actor_id] = explode('-', $actor_guid, 2);

        // retrieve all key of configurations actor
        $all_path = array_keys(
            CMbArray::flattenArrayKeys((new CConfigurationEai())->getAllConfigActors()[$actor_class])
        );

        $uri            = CApp::generateUrl('system_get_configs');
        $configurations = [];
        // build all key of config actor in object CConfiguration
        foreach ($all_path as $path) {
            $path    = ltrim($path, ' ');
            $section = explode(' ', $path)[1];
            if (!str_starts_with($section, $format)) {
                continue;
            }

            $configurations[]       = $configuration = new CConfiguration();
            $configuration->feature = $path;
        }

        // build RequestAPI
        $request = (new RequestApiBuilder())
            ->setUri($uri)
            ->setContent(json_encode((new Collection($configurations))->serialize()))
            ->buildRequestApi();
        $request->getRequest()->query->set('context', $actor_guid);

        // request route configurations
        $controller = new ConfigurationController();
        $response   = $controller->getConfigurations($request);

        if ($response->getStatusCode() !== 200) {
            CAppUI::stepAjax('CActorController-msg-fail export', UI_MSG_ERROR);
        }

        // parse content to transform in CConfiguration object (return array json api)
        $content        = $response->getContent();
        $data           = json_decode($content, true)['data'];
        $configurations = [];
        foreach ($data as $item) {
            $attributes             = $item['attributes'];
            $configurations[]       = $configuration = new CConfiguration();
            $configuration->feature = array_key_first($attributes);
            $configuration->value   = reset($attributes);
        }

        header("Content-Disposition: attachment; filename=\"config-actor-$format.json\"");

        // transform CConfiguration in json api (and remove meta)
        $values = (new Collection($configurations))->serialize();
        unset($values['meta']);

        $this->renderJson($values);
    }

    /**
     * Import all configuration from a file for an actor
     *
     * @return void
     * @throws Exception
     */
    public function importConfigs(): void
    {
        CCanDo::checkEdit();
        $actor_guid = CView::post('actor_guid', 'str');
        $file       = isset($_FILES['import']) ? $_FILES['import'] : null;
        if (!empty($file)) {
            $contents = file_get_contents($file['tmp_name']);
        }
        CView::checkin();

        $actor = CStoredObject::loadFromGuid($actor_guid);
        if (!$actor || !$actor instanceof CInteropActor || !$actor->_id || !($contents ?? null)) {
            CAppUI::stepAjax('CActorController-msg-fail import', UI_MSG_ERROR);
        }

        $uri     = CApp::generateUrl('system_set_configs');
        $request = (new RequestApiBuilder())
            ->setUri($uri)
            ->setMethod('PUT')
            ->setContent($contents)
            ->buildRequestApi();
        $request->getRequest()->query->set('context', $actor_guid);

        // request configurations
        $controller = new ConfigurationController();
        $response   = $controller->setConfigurations($request);

        if ($response->getStatusCode() !== 200) {
            echo CAppUI::tr('CActorController-msg-fail import');
        }

        echo CAppUI::tr('CConfiguration-msg-modify');
    }

    /**
     * Show template for import file configuration
     */
    public function showUploadFileConfig(): void
    {
        CCanDo::checkEdit();
        $actor_guid = CView::get('object_guid', 'str');
        CView::checkin();

        $this->renderSmarty(
            'import_config',
            [
                'actionType' => 'a',
                'action'     => 'importConfigs',
                'm'          => 'eai',
                'actor_guid' => $actor_guid,
            ]
        );
    }

    public function ajax_vw_configs_format(): void
    {
        $this->checkPermRead();

        $actor_guid  = CValue::getOrSession("actor_guid");
        $config_guid = CValue::getOrSession("config_guid");

        /** @var CInteropActor $actor */
        $actor         = CMbObject::loadFromGuid($actor_guid);
        $format_config = CMbObject::loadFromGuid($config_guid);
        $format_config->getConfigFields();

        $fields = $format_config->getPlainFields();
        unset($fields[$format_config->_spec->key]);
        if (!isset($format_config->_categories)) {
            $categories = ["" => array_keys($fields)];
        } else {
            $categories = $format_config->_categories;
        }

        $this->renderSmarty(
            'inc_configs_format',
            [
                "format_config" => $format_config,
                "fields"        => $fields,
                "categories"    => $categories,
                "actor"         => $actor,
                "actor_guid"    => $actor->_guid,
            ]
        );
    }

    public function ajax_refresh_configs_formats(): void
    {
        $this->checkPermRead();
        $actor_guid = CValue::getOrSession("actor_guid");

        $formats_xml = CExchangeDataFormat::getAll(CEchangeXML::class);
        foreach ($formats_xml as &$_format_xml) {
            $_format_xml = new $_format_xml();
            $_format_xml->getConfigs($actor_guid);
        }

        $formats_tabular = CExchangeDataFormat::getAll(CExchangeTabular::class);
        foreach ($formats_tabular as &$_format_tabular) {
            $_format_tabular = new $_format_tabular();
            $_format_tabular->getConfigs($actor_guid);
        }

        $formats_binary = CExchangeDataFormat::getAll(CExchangeBinary::class);
        foreach ($formats_binary as &$_format_binary) {
            $_format_binary = new $_format_binary();
            $_format_binary->getConfigs($actor_guid);
        }

        $this->renderSmarty(
            'inc_configs_formats.tpl',
            [
                'actor_guid'      => $actor_guid,
                'formats_xml'     => $formats_xml,
                'formats_tabular' => $formats_tabular,
                'formats_binary'  => $formats_binary,
            ]
        );
    }

    public function ajaxRefreshActor(): void
    {
        /**
         * Details interop receiver EAI
         */
        CCanDo::checkRead();

        $actor_guid  = CView::get('actor_guid', 'str');
        $actor_class = CView::get('actor_class', 'str');
        CView::checkin();

        // Chargement de l'acteur d'interopérabilité
        if ($actor_class) {
            $actor = new $actor_class();
            $actor->updateFormFields();
            $actor->loadRefGroup();
            $actor->lastMessage();
            $actor->isINSCompatible();
        } elseif ($actor_guid) {
            /** @var CInteropActor $actor */
            $actor = CMbObject::loadFromGuid($actor_guid);
            if ($actor->_id) {
                $actor->loadRefGroup();
                $actor->loadRefUser();
                $actor->isReachable();
                $actor->lastMessage();
                $actor->isINSCompatible();
            }
        }

        // Création du template
        $this->renderSmarty('inc_actor.tpl', ['_actor' => $actor]);
    }

    public function ajaxViewActor(): void
    {
        CCanDo::checkRead();
        $actor_guid = CView::get("actor_guid", "str");

        CView::checkin();

        /** @var CInteropActor $actor */
        $actor = CMbObject::loadFromGuid($actor_guid);
        if ($actor->_id) {
            $actor->loadRefGroup();
            $actor->loadRefUser();

            if ($actor instanceof CInteropSender) {
                $actor->countBackRefs("routes_sender");
            }
        }

        // Création du template
        $this->renderSmarty('inc_view_actor.tpl', ['actor' => $actor]);
    }

    public function doDuplicateReceiver(): void
    {
        /**
         * Duplicate receiver
         */
        CCanDo::checkAdmin();

        $receiver_guid = CValue::get("receiver_guid");

        if (!$receiver_guid) {
            return;
        }

        /** @var CInteropReceiver $receiver */
        $receiver = CMbObject::loadFromGuid($receiver_guid);

        $backrefs = [
            "messages_supported",
            "actor_transformations",
        ];

        if ($msg = $receiver->duplicateObject("libelle", $backrefs, "object_id")) {
            CAppUI::stepAjax("CInteropReceiver-action-Duplicate error", UI_MSG_ERROR, $msg);
        }

        // retrieve receiver duplicated
        /** @var CInteropReceiver $receiver */
        $receiver_duplicated = new $receiver->_class();
        $receiver_duplicated->cloneFrom($receiver);
        $receiver_duplicated->libelle = $receiver->libelle . ' (Copy)';
        $receiver_duplicated->loadMatchingObject($receiver->getPrimaryKey() . " DESC");

        if (!$receiver_duplicated->_id || $receiver_duplicated->_id == $receiver->_id) {
            CAppUI::stepAjax("CInteropReceiver-action-Duplicate error", UI_MSG_ERROR);
        }

        if ($msg_errors = $this->duplicateActorConfigurations($receiver, $receiver_duplicated)) {
            $msg = implode("\n", $msg_errors);
            CAppUI::stepAjax("CInteropReceiver-action-Duplicate error", UI_MSG_ERROR, $msg);
        }

        CAppUI::stepAjax("CInteropReceiver-action-Duplicate");
    }

    public function doDuplicateSender(): void
    {
        /**
         * Duplicate sender
         */
        CCanDo::checkAdmin();

        $sender_guid = CValue::get("sender_guid");
        if (!$sender_guid) {
            return;
        }

        /** @var CInteropSender $sender */
        $sender = CMbObject::loadFromGuid($sender_guid);

        $backrefs = [
            "messages_supported",
            "object_links",
        ];

        // remove user_id for duplicate sender
        if ($sender_user_id = $sender->user_id) {
            $sender->user_id = '';
            if ($msg = $sender->store()) {
                CAppUI::stepAjax("CInteropSender-action-Duplicate error", UI_MSG_ERROR, $msg);
            }
        }

        $msg_error_duplication = $sender->duplicateObject("libelle", $backrefs);

        // restore user with user_id
        if ($sender_user_id) {
            $sender->user_id = $sender_user_id;
            if ($msg = $sender->store()) {
                CAppUI::stepAjax("CInteropSender-action-Duplicate error", UI_MSG_ERROR, $msg);
            }
        }

        if ($msg_error_duplication) {
            CAppUI::stepAjax("CInteropSender-action-Duplicate error", UI_MSG_ERROR, $msg_error_duplication);
        }

        // retrieve sender duplicated
        /** @var CInteropSender $sender_duplicate */
        $sender_duplicate = new $sender->_class();
        $sender_duplicate->cloneFrom($sender);
        $sender_duplicate->user_id = null;
        $sender_duplicate->libelle = $sender->libelle . ' (Copy)';
        $sender_duplicate->loadMatchingObject($sender->getPrimaryKey() . " DESC");

        if (!$sender_duplicate->_id || $sender_duplicate->_id == $sender->_id) {
            CAppUI::stepAjax("CInteropSender-action-Duplicate error", UI_MSG_ERROR);
        }

        if ($msg_errors = $this->duplicateActorConfigurations($sender, $sender_duplicate)) {
            $msg = implode("\n", $msg_errors);
            CAppUI::stepAjax("CInteropSender-action-Duplicate error", UI_MSG_ERROR, $msg);
        }

        CAppUI::stepAjax("CInteropSender-action-Duplicate");
    }

    /**
     * Duplicate actor configurations
     *
     * @param CInteropActor $actor
     * @param CInteropActor $duplicated_actor
     *
     * @return array
     * @throws Exception
     */
    private function duplicateActorConfigurations(CInteropActor $actor, CInteropActor $duplicated_actor): array
    {
        if (!$actor->_id) {
            return [];
        }

        $configurations = (new CConfiguration())->loadList(
            [
                'object_class' => "= '$actor->_class'",
                'object_id'    => "= '$actor->_id'",
            ]
        );

        if (empty($configurations)) {
            return [];
        }

        $configs       = [];
        $strategy_mode = $actor->role === 'prod' ? 'std' : 'alt';
        $strategy      = ContextualConfigurationManager::getStrategy($strategy_mode);

        foreach ($configurations as $configuration) {
            $configs[$configuration->feature] = $actor->role === 'prod'
                ? $configuration->value
                : $configuration->alt_value;
        }

        return CConfiguration::setConfigs($configs, $duplicated_actor, $strategy);
    }

    public function ajaxCreateReceiverEasy(): void
    {
        CCanDo::checkRead();
        $actor_class = "CInteropReceiver";

        /** @var CInteropReceiver $actor */
        $actor  = new $actor_class();
        $actors = $actor->getChildReceivers();

        $actor->group_id = CGroups::loadCurrent()->_id;
        $actor->actif    = "1";
        $actor->role     = CAppUI::conf("instance_role");

        // Création du template
        $this->renderSmarty(
            'inc_create_receiver_easy.tpl',
            [
                'actor'     => $actor,
                'actors'    => $actors,
                'tabs_menu' => "receiver_type",
            ]
        );
    }

    public function ajaxRefreshActorsType(): void
    {
        CCanDo::checkRead();
        $actor_class = CView::get('actor_class', 'str');
        $all_actors  = CView::get('all_actors', 'str');
        CView::checkin();

        /** @var CInteropActor $actor */
        $actor = new $actor_class();
        $group = CGroups::loadCurrent()->_id;

        $actors = $all_actors
            ? $actor->getObjectsByClass($actor_class, false, false)
            : $actor->getObjectsByClass($actor_class, true, true, $group);

        $this->renderSmarty(
            'inc_refresh_actors_type.tpl',
            [
                'actors'        => $actors,
                'type_actor'    => $actor_class,
                'role_instance' => CAppUI::conf('instance_role'),
            ]
        );
    }

    public function doEnableActors(): void
    {
        /**
         * Link transformations
         */
        CCanDo::checkAdmin();

        $actor_role  = CValue::get("actor_role");
        $actor_class = CValue::get("actor_class");
        $enable      = CValue::get("enable");

        /** @var CInteropActor $actor */
        $actor  = new $actor_class();
        $actors = $actor->getObjects();

        $count_actor_desactivate = 0;
        $count_actor_activate    = 0;
        foreach ($actors as $_actors) {
            if (!$_actors) {
                continue;
            }

            /** @var CInteropActor[] $_actors */
            foreach ($_actors as $_actor) {
                if ($enable && $_actor->actif || !$enable && !$_actor->actif) {
                    continue;
                }

                if ($actor_role == "prod" && $_actor->role == "qualif") {
                    continue;
                }

                if ($actor_role == "qualif" && $_actor->role == "prod") {
                    continue;
                }

                if (!$enable && $_actor->actif) {
                    $count_actor_desactivate++;
                }

                if ($enable && !$_actor->actif) {
                    $count_actor_activate++;
                }

                $_actor->actif = $enable;

                $_actor->store();
            }
        }

        CAppUI::setMsg("$actor_class-action-Activate %s", UI_MSG_OK, $count_actor_activate);
        CAppUI::setMsg("$actor_class-action-Desactivate %s", UI_MSG_WARNING, $count_actor_desactivate);

        echo CAppUI::getMsg();
        CApp::rip();
    }

    public function ajaxEditActor(): void
    {
        CCanDo::checkEdit();

        $actor_guid  = CValue::getOrSession("actor_guid");
        $actor_class = CValue::getOrSession("actor_class");

        // Chargement de l'acteur d'interoprabilit
        if ($actor_class) {
            $actor = new $actor_class();
            $actor->updateFormFields();
        } elseif ($actor_guid) {
            /** @var CInteropActor $actor */
            $actor = CMbObject::loadFromGuid($actor_guid);
            if ($actor->_id) {
                $actor->loadRefGroup();
                $actor->loadRefUser();
            }
        }

        // Cration du template
        $this->renderSmarty('inc_edit_actor.tpl', ['actor' => $actor]);
    }
}
