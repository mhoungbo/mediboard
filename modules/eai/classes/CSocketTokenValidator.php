<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Ox\Mediboard\Admin\CTokenValidator;

/**
 * Valid handle_commit params
 */
class CSocketTokenValidator extends CTokenValidator
{
    protected $patterns = [
        'get'  => [
            'm'               => ['/eai/', false],
            'a'               => ['/do_receive_mllp/', false],
            'suppressHeaders' => ['/(\d)*/', false],
        ],
        'post' => [
            'client_addr' => ['/.*/', false],
            'port'        => ['/.*/', false],
            'client_port' => ['/.*/', false],
            'message'     => ['/.*/', false],
        ],
    ];

    protected $authorized_methods = [
        'get',
        'post',
    ];
}
