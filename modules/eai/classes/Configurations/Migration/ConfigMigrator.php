<?php

/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Configurations\Migration;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\CMbObjectConfig;
use Ox\Interop\Cda\CCDAConfig;
use Ox\Interop\Dicom\CDicomConfig;
use Ox\Interop\Eai\CConfigurationEai;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Fhir\Actors\CFHIRConfig;
use Ox\Interop\Fhir\Actors\CReceiverFHIRConfig;
use Ox\Interop\Hl7\CHL7Config;
use Ox\Interop\Hl7\CReceiverHL7v2Config;
use Ox\Interop\Hl7\CReceiverHL7v3Config;
use Ox\Interop\Hprimsante\CHPrimSanteConfig;
use Ox\Interop\Hprimsante\CReceiverHPrimSanteConfig;
use Ox\Interop\Hprimxml\CDestinataireHprimConfig;
use Ox\Interop\Hprimxml\CHprimXMLConfig;
use Ox\Interop\Phast\CPhastConfig;
use Ox\Interop\Phast\CPhastDestinataireConfig;
use Ox\Mediboard\System\CConfiguration;
use Psr\Log\LoggerInterface;

class ConfigMigrator
{
    /** @var array */
    private static array    $eai_configurations = [];
    private LoggerInterface $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param CInteropActor   $actor
     * @param CMbObjectConfig $config_object
     *
     * @return bool
     * @throws CMbException
     * @throws Exception
     */
    public function migrate(CInteropActor $actor, CMbObjectConfig $config_object): int
    {
        $exchange_name = CAppUI::tr($this->prefixMapping($config_object));
        $context       = ['actor' => $actor->_guid, 'object_config' => $config_object->_guid];

        // retrieve configs for actor
        $actor_configs = $this->getConfigs($actor, $config_object);

        $stat_migration = [
            'empty_config'   => [],
            'missing_config' => [],
            'default_value'  => [],
            'to_migrate'     => [],
        ];
        $configs        = [];
        foreach ($actor_configs as $section_name => $config_sections) {
            foreach ($config_sections as $config_name => $config_prop) {
                if (!property_exists($config_object, $config_name)) {
                    $stat_migration['missing_config'][] = $config_name;

                    continue;
                }

                $default_value = $this->defaultConfig($config_prop);

                $value = $config_object->{$config_name};
                if ($value === null) {
                    $stat_migration['empty_config'][] = $config_name;

                    continue;
                }

                if ($value === $default_value) {
                    $stat_migration['default_value'][] = $config_name;

                    continue;
                }

                $stat_migration['to_migrate'][] = $config_name;
                $path                           = "eai $section_name $config_name";
                $configs[$path]                 = $value;
            }
        }

        if (!empty($stat_migration['missing_config'])) {
            $this->logger->critical('ConfigMigrator-msg-missing configuration', $stat_migration['missing_config']);
        }

        $this->logger->debug(
            CAppUI::tr('ConfigMigrator-msg-skipped migration'),
            array_merge($context, $stat_migration)
        );

        // migration
        $messages = $this->migrateConfigs($actor, $configs);

        if (!empty($messages)) {
            $this->logger->error(
                CAppUI::tr('ConfigMigrator-msg-failed process'),
                array_merge($context, ['error_messages' => $messages])
            );
        }

        $this->logger->info(
            CAppUI::tr('ConfigMigrator-msg-end', [$exchange_name]),
            array_merge($context, ['config_migrated' => count($configs)])
        );

        return count($configs) - count($messages);
    }

    /**
     * @param CInteropActor   $actor
     * @param CMbObjectConfig $config_object
     *
     * @return array
     * @throws CMbException
     */
    private function getConfigs(CInteropActor $actor, CMbObjectConfig $config_object): array
    {
        $all_configs = $this->getEAIConfigurations();

        if (!$actor_configs = ($all_configs[$actor->_class]['eai'] ?? null)) {
            throw new CMbException('invalid config actor');
        }
        $prefix = $this->prefixMapping($config_object);

        return array_filter(
            $actor_configs,
            function ($config) use ($prefix) {
                return str_starts_with($config, $prefix);
            },
            ARRAY_FILTER_USE_KEY
        );
    }

    /**
     * @return array
     */
    private function getEAIConfigurations(): array
    {
        if (empty(self::$eai_configurations)) {
            self::$eai_configurations = (new CConfigurationEai())->getAllConfigActors();
        }

        return self::$eai_configurations;
    }

    /**
     * @throws CMbException
     */
    private function prefixMapping(CMbObjectConfig $config): string
    {
        $config_class = get_class($config);
        switch ($config_class) {
            case CHL7Config::class:
            case CReceiverHL7v2Config::class:
                return 'CExchangeHL7v2';
            case CReceiverHL7v3Config::class:
                return 'CExchangeHL7v3';
            case CCDAConfig::class:
                return 'CExchangeCDA';
            case CDicomConfig::class:
                return 'CExchangeDicom';
            case CReceiverHPrimSanteConfig::class:
            case CHPrimSanteConfig::class:
                return 'CExchangeHprimSante';
            case CHprimXMLConfig::class:
            case CDestinataireHprimConfig::class:
                return 'CEchangeHprim';
            case CPhastConfig::class:
            case CPhastDestinataireConfig::class:
                return 'CExchangePhast';
            case CReceiverFHIRConfig::class:
            case CFHIRConfig::class:
                return 'CExchangeFHIR';

            default:
                throw new CMbException("not supported class $config_class");
        }
    }

    /**
     * @param CInteropActor $actor
     * @param array         $configs
     *
     * @return void
     * @throws Exception
     */
    private function migrateConfigs(CInteropActor $actor, array $configs): array
    {
        return CConfiguration::setConfigs($configs, $actor);
    }

    /**
     * @param string $config_prop
     *
     * @return mixed|null
     */
    private function defaultConfig(string $config_prop)
    {
        $regex = "/default\|(?'default'\w*)/";

        preg_match($regex, $config_prop, $matches);

        return $matches['default'] ?? null;
    }
}
