<?php

/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Configurations\Migration;

use Exception;
use Ox\Core\CMbObjectConfig;
use Ox\Core\CStoredObject;
use Ox\Interop\Cda\CReceiverCDA;
use Ox\Interop\Dicom\CDicomSender;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\CInteropReceiver;
use Ox\Interop\Eai\CInteropSender;
use Ox\Interop\Ftp\CSenderFTP;
use Ox\Interop\Ftp\CSenderSFTP;
use Ox\Interop\Hl7\CSenderMLLP;
use Ox\Interop\Webservices\CSenderSOAP;
use Ox\Mediboard\System\CSenderFileSystem;
use Ox\Mediboard\System\CSenderHTTP;

class ActorConfigRepository
{
    /**
     * @param CInteropActor $actor
     *
     * @return CMbObjectConfig[]
     */
    public function findConfigs(CInteropActor $actor): array
    {
        if (!$actor->_id) {
            return [];
        }

        if ($actor instanceof CInteropReceiver) {
            return $this->searchFromReceiver($actor);
        } elseif ($actor instanceof CInteropSender) {
            return $this->searchFromSender($actor);
        }

        return [];
    }

    public function findActors(array $actor_classes = [], bool $only_active = false): array
    {
        if (!$actor_classes) {
            $actor_classes = array_merge(
                CInteropReceiver::getChildReceivers(true),
                CInteropSender::getChildSenders()
            );
        }

        $all_actors = [];
        foreach ($actor_classes as $actor_class) {
            $actor = new $actor_class();
            if (!($actor instanceof CInteropActor) || ($actor->_ref_module === null)) {
                continue;
            }

            // to avoid errors during fresh install
            if ($actor->getDS()->hasTable($actor->getSpec()->table) === false) {
                continue;
            }

            $where = [];
            $ljoin = [];
            if ($only_active) {
                $where['actif'] = " = '1'";
            }

            try {
                $actors = $actor->loadList($where, null, null, null, $ljoin);
            } catch (Exception $exception) {
                // module not actif / table not already installed
                continue;
            }

            // module actor not actived
            if ($actors === null) {
                continue;
            }

            $all_actors = array_merge($all_actors, $actors);
        }

        return $all_actors;
    }

    public function massLoadObjectConfigForActors(array $actors): void
    {
        $receivers = [];
        $senders   = [];

        foreach ($actors as $actor) {
            if ($actor instanceof CReceiverCDA) {
                continue;
            }

            if ($actor instanceof CInteropReceiver) {
                $receivers[get_class($actor)][$actor->_id] = $actor;
            } elseif ($actor instanceof CInteropSender) {
                $senders[get_class($actor)][$actor->_id] = $actor;
            }
        }

        foreach ($receivers as $_receivers) {
            CStoredObject::massLoadBackRefs($_receivers, 'object_configs');
        }

        foreach ($senders as $sender_class => $_senders) {
            switch ($sender_class) {
                case CSenderHTTP::class:
                    CStoredObject::massLoadBackRefs($_senders, 'config_hl7');
                    CStoredObject::massLoadBackRefs($_senders, 'config_cda');
                    CStoredObject::massLoadBackRefs($_senders, 'config_fhir');
                    break;
                case CSenderMLLP::class:
                    CStoredObject::massLoadBackRefs($_senders, 'config_hl7');
                    break;
                case CSenderSFTP::class:
                case CSenderFTP::class:
                case CSenderFileSystem::class:
                case CSenderSOAP::class:
                    CStoredObject::massLoadBackRefs($_senders, 'config_hl7');
                    CStoredObject::massLoadBackRefs($_senders, 'config_hprimxml');
                    CStoredObject::massLoadBackRefs($_senders, 'config_phast');
                    CStoredObject::massLoadBackRefs($_senders, 'config_cda');
                    CStoredObject::massLoadBackRefs($_senders, 'config_hprimsante');
                    break;
                case CDicomSender::class:
                    CStoredObject::massLoadBackRefs($_senders, 'config_dicom');
                    break;
                default:
            }
        }
    }

    /**
     * @param CInteropReceiver $actor
     *
     * @return array
     */
    private function searchFromReceiver(CInteropReceiver $actor): array
    {
        /** @var CMbObjectConfig $object_config */
        $actor->loadRefObjectConfigs();
        $object_config = $actor->_ref_object_configs;

        return (!$object_config || !$object_config->_id) ? [] : [$object_config];
    }

    /**
     * @param CInteropSender $actor
     *
     * @return array
     * @throws Exception
     */
    private function searchFromSender(CInteropSender $actor): array
    {
        $object_configs = [];
        switch (get_class($actor)) {
            case CSenderHTTP::class:
                $object_configs = [
                    $actor->loadBackRefConfigHL7(),
                    $actor->loadBackRefConfigCDA(),
                    $actor->loadBackRefConfigFHIR(),
                ];
                break;
            case CSenderMLLP::class:
                $object_configs = [
                    $actor->loadBackRefConfigHL7(),
                ];
                break;
            case CSenderSFTP::class:
            case CSenderFTP::class:
            case CSenderFileSystem::class:
            case CSenderSOAP::class:
                $object_configs = [
                    $actor->loadBackRefConfigHL7(),
                    $actor->loadBackRefConfigHprimXML(),
                    $actor->loadBackRefConfigHprimSante(),
                    $actor->loadBackRefConfigCDA(),
                    $actor->loadBackRefConfigPhast(),
                ];
                break;
            case CDicomSender::class:
                $object_configs = [
                    $actor->loadBackRefConfigDicom(),
                ];
                break;
            default:
        }

        return array_filter($object_configs, function (?CMbObjectConfig $config) {
            return $config && $config->_id;
        });
    }
}
