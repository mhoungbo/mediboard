<?php

/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Configurations\Migration;

use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\CMbObjectConfig;
use Ox\Interop\Eai\CInteropActor;
use Psr\Log\LoggerInterface;

class ActorConfigMigration
{
    /** @var CMbObjectConfig[] */
    private ConfigMigrator        $config_migrator;
    private ActorConfigRepository $repository;
    private LoggerInterface       $logger;

    public function __construct(
        ActorConfigRepository $repository,
        ConfigMigrator $config_migrator,
        LoggerInterface $logger
    ) {
        $this->repository      = $repository;
        $this->logger          = $logger;
        $this->config_migrator = $config_migrator;
    }

    /**
     * Migrate configuration from CMbObjectConfig (interop actor) to CConfiguration
     *
     * @param CInteropActor $actor
     *
     * @return void
     * @throws CMbException
     */
    public function migrate(CInteropActor $actor): int
    {
        $object_configs = $this->repository->findConfigs($actor);
        $context        = ["actor" => $actor->_guid, "object_configs" => count($object_configs)];

        if (!$object_configs) {
            return 0;
        }

        $config_migrated = 0;
        $object_migrated = [];
        foreach ($object_configs as $config) {
            $nb_conf_migrated  = $this->config_migrator->migrate($actor, $config);
            $config_migrated   += $nb_conf_migrated;
            $object_migrated[] = ['config' => get_class($config), 'migrated' => $nb_conf_migrated];
        }

        $this->logger->info(
            CAppUI::tr(
                'ActorConfigMigration-msg-migration completed',
                [$actor->nom]
            ),
            array_merge($context, ['configurations_migrated' => $object_migrated, 'all_migrated' => $config_migrated])
        );

        return $config_migrated;
    }
}
