<?php
/**
 * @package Mediboard\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbRange;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Mediboard\Sante400\CIncrementer;

/**
 * Class CEAISejour
 * Patient utilities EAI
 */
class CEAISejour extends CEAIMbObject
{
    /**
     * Recording NDA
     *
     * @param CIdSante400    $NDA    Object id400
     * @param CSejour        $sejour Admit
     * @param CInteropSender $sender Sender
     *
     * @return null|string null if successful otherwise returns and error message
     */
    public static function storeNDA(
        CIdSante400    $NDA,
        CSejour        $sejour,
        CInteropSender $sender,
        bool           $is_purge = false
    ): ?string {
        /* Gestion du num�roteur */
        $group = new CGroups();
        $group->load($sender->group_id);
        $group->loadConfigValues();

        // Purge du NDA existant sur le s�jour et on le remplace par le nouveau
        if ($is_purge) {
            // On charge le NDA courant du s�jour
            $sejour->loadNDA($sender->group_id);

            $ref_NDA = $sejour->_ref_NDA;

            if ($ref_NDA) {
                // Si le NDA actuel est identique � celui qu'on re�oit on ne fait rien
                if ($ref_NDA->id400 == $NDA->id400) {
                    return null;
                }

                // On passe le NDA courant en trash
                $ref_NDA->tag              = CAppUI::conf("dPplanningOp CSejour tag_dossier_trash") . $ref_NDA->tag;
                $ref_NDA->_eai_sender_guid = $sender->_guid;
                $ref_NDA->store();
            }

            // On sauvegarde le nouveau
            $NDA->tag              = $sender->_tag_sejour;
            $NDA->object_class     = "CSejour";
            $NDA->object_id        = $sejour->_id;
            $NDA->_eai_sender_guid = $sender->_guid;

            return $NDA->store();
        }

        // G�n�ration du NDA ?
        // Non
        if (!$group->_configs["smp_idex_generator"]) {
            if (!$NDA->id400) {
                return null;
            }

            if ($sejour) {
                $NDA->object_id = $sejour->_id;
            }

            $NDA->_eai_sender_guid = $sender->_guid;

            return $NDA->store();
        } else {
            $NDA_temp = CIdSante400::getMatch("CSejour", $sender->_tag_sejour, null, $sejour->_id);
            if ($NDA_temp->_id) {
                return null;
            }

            // Pas de NDA pass�
            if (!$NDA->id400) {
                if (!CIncrementer::generateIdex($sejour, $sender->_tag_sejour, $sender->group_id)) {
                    return CAppUI::tr("CEAISejour-error-generate-idex");
                }

                return null;
            } else {
                $incrementer = $sender->loadRefGroup()->loadDomainSupplier("CSejour");
                if (
                    $incrementer &&
                    $incrementer->manage_range &&
                    !CMbRange::in(
                        $NDA->id400,
                        $incrementer->range_min,
                        $incrementer->range_max
                    )
                ) {
                    return CAppUI::tr("CEAISejour-idex-not-in-the-range");
                }

                $NDA->object_id        = $sejour->_id;
                $NDA->_eai_sender_guid = $sender->_guid;

                return $NDA->store();
            }
        }
    }

    /**
     * Recording admit
     *
     * @param CSejour        $newSejour   Admit
     * @param CInteropSender $sender      Sender
     * @param bool           $generateNDA Generate NDA ?
     *
     * @return null|string null if successful otherwise returns and error message
     */
    public static function storeSejour(CSejour $newSejour, CInteropSender $sender, bool $generateNDA = false): ?string
    {
        // Notifier les autres destinataires autre que le sender
        $newSejour->_eai_sender_guid = $sender->_guid;
        $newSejour->_generate_NDA    = $generateNDA;

        if ($newSejour->store()) {
            $newSejour->repair();

            // Notifier les autres destinataires autre que le sender
            $newSejour->_eai_sender_guid = $sender->_guid;
            $newSejour->_generate_NDA    = $generateNDA;

            return $newSejour->store();
        }

        return null;
    }
}
