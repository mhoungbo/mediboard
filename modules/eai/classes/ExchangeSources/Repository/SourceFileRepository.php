<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\ExchangeSources\Repository;

use Ox\Interop\Eai\ExchangeSources\Exceptions\InvalidSourceException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNonActiveException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNotConfiguredException;
use Ox\Mediboard\System\Sources\CSourceFile;

class SourceFileRepository implements SourceRepositoryInterface
{
    protected const DEFAULT_TYPES = CSourceFile::TYPE;

    public function __construct(private ExchangeSourceRepository $repository)
    {
    }

    /**
     * @inheritDoc
     *
     * @param string            $name
     * @param array|string|null $types
     *
     * @return CSourceFile
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSource(string $name, array|string|null $types = ''): CSourceFile
    {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceFile $source */
        $source = $this->repository->getSource($name, $types);

        return $source;
    }

    /**
     * @inheritDoc
     *
     * @param string            $name
     * @param array|string|null $types
     *
     * @return CSourceFile
     * @throws SourceNotConfiguredException
     */
    public function getTestableSource(string $name, array|string|null $types = ''): CSourceFile
    {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceFile $source */
        $source = $this->repository->getTestableSource($name, $types);

        return $source;
    }


    /**
     * @inheritDoc
     */
    public function getConfigurableSource(
        string            $name,
        array|string|null $types = '',
        string            $exchange_type = null
    ): CSourceFile {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceFile $source */
        $source = $this->repository->getConfigurableSource($name, $types, $exchange_type);

        return $source;
    }

    /**
     * @inheritDoc
     * @param string $id
     * @param string $exchange_class
     * @param bool $only_active
     * @return CSourceFile
     * @throws InvalidSourceException
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSourceFromId(string $id, string $exchange_class, bool $only_active = true): CSourceFile
    {
        return $this->getSourceFromGuid("$exchange_class-$id", $only_active);
    }

    /**
     * @param string $guid
     * @param bool $only_active
     * @return CSourceFile
     * @throws InvalidSourceException
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSourceFromGuid(string $guid, bool $only_active = true): CSourceFile
    {
        [$class, $id] = explode('-', $guid, 2);
        if (!in_array($class, ['CSourceFTP', 'CSourceSFTP', 'CSourceFileSystem'])) {
            throw new InvalidSourceException($guid);
        }

        /** @var CSourceFile $source */
        $source = $this->repository->getSourceFromGuid($guid, $only_active);

        return $source;
    }
}
