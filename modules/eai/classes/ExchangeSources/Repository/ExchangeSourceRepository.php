<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\ExchangeSources\Repository;

use Exception;
use Ox\Interop\Eai\ExchangeSources\Exceptions\InvalidSourceException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNonActiveException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNotConfiguredException;
use Ox\Mediboard\System\CExchangeSource;

class ExchangeSourceRepository implements SourceRepositoryInterface
{
    protected bool $use_cache;

    public function __construct(bool $use_cache = true)
    {
        $this->use_cache = $use_cache;
    }

    /**
     * Return only a source active
     *
     * @param string            $name  Name of the source
     * @param string|array|null $types Allowed type for source, if null $default_types will be used, else all sources
     *                                 will be eligible
     *
     * @return CExchangeSource
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSource(string $name, null|string|array $types = null): CExchangeSource
    {
        $only_active = is_array($types) && (count($types) > 1);
        $source      = CExchangeSource::get($name, $types, false, null, $only_active, false, $this->use_cache);
        if (!$source->_id) {
            throw new SourceNotConfiguredException($source);
        }

        if (!$source->active) {
            throw new SourceNonActiveException($source);
        }

        return $source;
    }

    /**
     * Return only a source active
     *
     * @param string            $name  Name of the source
     * @param string|array|null $types Allowed type for source, if null $default_types will be used, else all sources
     *                                 will be eligible
     *
     * @return CExchangeSource
     * @throws SourceNotConfiguredException
     */
    public function getTestableSource(string $name, null|string|array $types = null): CExchangeSource
    {
        $only_active = is_array($types) && (count($types) > 1);
        $source      = CExchangeSource::get($name, $types, false, null, $only_active, false, $this->use_cache);
        if (!$source->_id) {
            throw new SourceNotConfiguredException($source);
        }

        return $source;
    }

    /**
     * Return a source for made this configuration.
     * So the source should be non active or non stored
     *
     * @param string            $name          The name of source
     * @param string|array|null $types         Allowed type for source,
     *                                         if null $default_types will be used,
     *                                         else all sources will be eligible
     * @param string|null       $exchange_type Put name for exchange type for all exchange made with this source
     *
     * @return CExchangeSource
     */
    public function getConfigurableSource(
        string            $name,
        null|string|array $types = null,
        string            $exchange_type = null
    ): CExchangeSource {
        return CExchangeSource::get($name, $types, false, $exchange_type, false, false, $this->use_cache);
    }

    /**
     * @inheritDoc
     * @param string $guid
     * @param bool $only_active
     *
     * @return CExchangeSource
     * @throws InvalidSourceException
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     * @throws Exception
     */
    public function getSourceFromGuid(string $guid, bool $only_active = true): CExchangeSource
    {
        [$class, $id] = explode('-', $guid, 2);
        if (!in_array($class, CExchangeSource::getExchangeClasses())) {
            throw new InvalidSourceException($guid);
        }

        /** @var CExchangeSource $source */
        $source = CExchangeSource::loadFromGuid($guid, $this->use_cache);

        if (!$source->_id) {
            throw new SourceNotConfiguredException($source);
        }

        if ($only_active && !$source->active) {
            throw new SourceNonActiveException($source);
        }

        return $source;
    }

    /**
     * @inheritDoc
     * @param string $id
     * @param string $exchange_class
     *
     * @return CExchangeSource
     * @throws InvalidSourceException
     * @throws SourceNotConfiguredException
     */
    public function getSourceFromId(string $id, string $exchange_class, bool $only_active = true): CExchangeSource
    {
        return $this->getSourceFromGuid("$exchange_class-$id", $only_active);
    }
}
