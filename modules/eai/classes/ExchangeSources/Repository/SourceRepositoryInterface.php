<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\ExchangeSources\Repository;

use Ox\Mediboard\System\CExchangeSource;

interface SourceRepositoryInterface
{
    /**
     * Return only a source active
     *
     * @param string            $name  Name of the source
     * @param string|array|null $types Allowed type for source, if null $default_types will be used, else all sources
     *                                 will be eligible
     *
     * @return CExchangeSource
     */
    public function getSource(string $name, null|string|array $types = ''): CExchangeSource;

    /**
     * Return a loaded source from his guid
     *
     * @param string $guid
     * @param bool $only_active
     *
     * @return CExchangeSource
     */
    public function getSourceFromGuid(string $guid, bool $only_active = true): CExchangeSource;

    /**
     * Return a loaded source from his id
     *
     * @param string $id
     *
     * @param string $exchange_class
     * @param bool $only_active
     *
     * @return CExchangeSource
     */
    public function getSourceFromId(string $id, string $exchange_class, bool $only_active = true): CExchangeSource;

    /**
     * Return only a source active
     *
     * @param string            $name  Name of the source
     * @param string|array|null $types Allowed type for source, if null $default_types will be used, else all sources
     *                                 will be eligible
     *
     * @return CExchangeSource
     */
    public function getTestableSource(string $name, null|string|array $types = ''): CExchangeSource;

    /**
     * Return a source for made this configuration.
     * So the source should be non active or non stored
     *
     * @param string            $name          The name of source
     * @param string|array|null $types         Allowed type for source,
     *                                         if null $default_types will be used,
     *                                         else all sources will be eligible
     * @param string|null       $exchange_type Put name for exchange type for all exchange made with this source
     *
     * @return CExchangeSource
     */
    public function getConfigurableSource(
        string            $name,
        null|string|array $types = '',
        string            $exchange_type = null
    ): CExchangeSource;
}
