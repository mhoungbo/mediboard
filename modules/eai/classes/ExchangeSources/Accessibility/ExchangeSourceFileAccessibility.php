<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\ExchangeSources\Accessibility;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbPath;
use Ox\Mediboard\System\Sources\CSourceFile;
use Ox\Mediboard\System\Sources\ObjectPath;

class ExchangeSourceFileAccessibility
{
    public const REPORT_SOURCE_ACCESSIBLE = 'source.accessible';
    public const REPORT_SOURCE_AUTHENTICATE = 'source.authentication';
    public const REPORT_DIR_ACCESSIBLE = 'source.dir.accessible';
    public const REPORT_DIR_WRITABLE = 'source.dir.writable';
    public const REPORT_FILE_ADD = 'source.file.add';
    public const REPORT_FILE_GET = 'source.file.get';
    public const REPORT_FILE_DELETE = 'source.file.delete';

    /**
     * Test all elements for the source
     * Test accessibility
     * Test authentification
     * Test accessibility for directory
     * Test Add, get and delete file in directory target
     *
     * @param CSourceFile     $source
     * @param ObjectPath|null $path
     *
     * @return array|array[]
     * @throws Exception
     */
    public function fullTest(CSourceFile $source, ObjectPath $path = null): array
    {
        $report = [
            self::REPORT_SOURCE_ACCESSIBLE   => ['status' => false, 'exception' => null],
            self::REPORT_SOURCE_AUTHENTICATE => ['status' => false, 'exception' => null],
            self::REPORT_DIR_ACCESSIBLE      => ['status' => false, 'dir_path' => null, 'exception' => null],
            self::REPORT_DIR_WRITABLE        => ['status' => false, 'dir_path' => null],
            self::REPORT_FILE_ADD            => ['status' => false, 'file_path' => null, 'exception' => null],
            self::REPORT_FILE_GET            => ['status' => false, 'file_path' => null, 'exception' => null],
            self::REPORT_FILE_DELETE         => ['status' => false, 'file_path' => null, 'exception' => null],
        ];

        // accessible
        $is_accessible = $this->isAccessible($source, $report);
        if ($is_accessible === false) {
            return $report;
        }
        $report[self::REPORT_SOURCE_ACCESSIBLE]['status'] = $is_accessible;

        // authentication
        $is_authenticate = $this->isAuthenticate($source, $report);
        if ($is_authenticate === false) {
            return $report;
        }
        $report[self::REPORT_SOURCE_AUTHENTICATE]['status'] = $is_authenticate;

        // dir accessible
        $path              = $path ?: $source->completePath();
        $directory         = $path->toDirectory();
        $is_dir_accessible = $this->isDirAccessible($source, $directory, $report);
        if ($is_dir_accessible === false) {
            return $report;
        }
        $report[self::REPORT_DIR_ACCESSIBLE]['status'] = $is_dir_accessible;


        // dir writable
        $path_file_test = $path->withFilePath('test.txt');
        $report[self::REPORT_DIR_WRITABLE]['status'] = $this->isWritable($source, $path_file_test, $report);

        return $report;
    }


    /**
     * Test if the source is authentificate
     *
     * @param CSourceFile $source
     * @param array       $report
     *
     * @return bool
     */
    public function isAuthenticate(CSourceFile $source, array &$report = []): bool
    {
        try {
            return $source->getClient()->isAuthentificate();
        } catch (Exception $exception) {
            $report[self::REPORT_SOURCE_AUTHENTICATE]['exception'] = $exception;

            return false;
        }
    }


    /**
     * Test if source is accessible
     *
     * @param CSourceFile $source
     * @param array       $report
     *
     * @return bool
     */
    public function isAccessible(CSourceFile $source, array &$report = []): bool
    {
        try {
            return $source->getClient()->isReachableSource();
        } catch (Exception $exception) {
            $report[self::REPORT_SOURCE_ACCESSIBLE]['exception'] = $exception;

            return false;
        }
    }

    /**
     * Generate tmp file with $content
     *
     * @param string $content
     *
     * @return string
     * @throws Exception
     */
    protected function getTmpFile(string $content): string
    {
        $tmp_dir = CAppUI::getTmpPath('test_sources');
        CMbPath::forceDir($tmp_dir);
        $file_temp = tempnam($tmp_dir, 'source_file_accessibility');
        if (file_put_contents($file_temp, $content) === false) {
            throw new Exception();
        }

        return $file_temp;
    }

    /**
     * Clear the tmp file created
     *
     * @param string $file_tmp
     *
     * @return void
     * @throws Exception
     */
    protected function clearTmpFile(string $file_tmp): void
    {
        if (str_starts_with($file_tmp, CAppUI::getTmpPath('test_sources'))) {
            CMbPath::remove($file_tmp);
        }
    }

    /**
     * Test if directory is accessible
     *
     * @param CSourceFile $source
     * @param string      $path
     * @param array       $report
     *
     * @return bool
     */
    public function isDirAccessible(CSourceFile $source, string $path, array &$report = []): bool
    {
        try {
            $report[self::REPORT_DIR_ACCESSIBLE]['dir_path'] = $path;
            $source->getClient()->getListDirectory($path);

            return true;
        } catch (Exception $exception) {
            $report[self::REPORT_DIR_ACCESSIBLE]['exception'] = $exception;

            return false;
        }
    }

    /**
     * Test source write, get and delete file is available
     *
     * @param CSourceFile $source
     * @param ObjectPath  $path
     * @param array       $report
     *
     * @return bool
     * @throws Exception
     */
    private function isWritable(CSourceFile $source, ObjectPath $path, array &$report = []): bool
    {
        $content       = "ok";
        $tmp_path_file = $this->getTmpFile($content);

        // added
        $report[self::REPORT_FILE_ADD]['status'] = $this->addTestFile($source, $path, $tmp_path_file, $report);
        if ($report[self::REPORT_FILE_ADD]['status']) {
            // get
            $report[self::REPORT_FILE_GET]['status'] = $this->getTestFile($source, $path, $content, $report);
            if ($report[self::REPORT_FILE_GET]['status']) {
                // delete
                $report[self::REPORT_FILE_DELETE]['status'] = $this->deleteTestFile($source, $path, $report);
            }
        }

        $this->clearTmpFile($tmp_path_file);

        return $report[self::REPORT_FILE_ADD]['status']
            && $report[self::REPORT_FILE_GET]['status']
            && $report[self::REPORT_FILE_DELETE]['status'];
    }

    /**
     * Test add file on source is available
     *
     * @param CSourceFile $source
     * @param string      $path_to_send
     * @param string      $path_to_content
     * @param array       $report
     *
     * @return bool
     */
    protected function addTestFile(
        CSourceFile $source,
        string $path_to_send,
        string $path_to_content,
        array &$report
    ): bool {
        try {
            $report[self::REPORT_FILE_ADD]['file_path'] = $path_to_send;

            return $source->getClient()->addFile($path_to_content, $path_to_send);
        } catch (Exception $exception) {
            $report[self::REPORT_FILE_ADD]['exception'] = $exception;

            return false;
        }
    }

    /**
     * Test delete file on source is available
     *
     * @param CSourceFile $source
     * @param string      $path
     * @param array       $report
     *
     * @return bool
     */
    protected function deleteTestFile(CSourceFile $source, string $path, array &$report): bool
    {
        try {
            $report[self::REPORT_FILE_DELETE]['file_path'] = $path;

            return $source->getClient()->delFile($path);
        } catch (Exception $exception) {
            $report[self::REPORT_FILE_DELETE]['exception'] = $exception;

            return false;
        }
    }

    /**
     * Test get file on source is available
     *
     * @param CSourceFile $source
     * @param string      $path
     * @param string      $content_expected
     * @param array       $report
     *
     * @return bool
     */
    private function getTestFile(CSourceFile $source, string $path, string $content_expected, array &$report): bool
    {
        try {
            $report[self::REPORT_FILE_GET]['file_path'] = $path;
            $content = $source->getClient()->getData($path);

            return $content_expected === $content;
        } catch (Exception $exception) {
            $report[self::REPORT_FILE_GET]['exception'] = $exception;

            return false;
        }
    }
}
