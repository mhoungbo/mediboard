<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\ExchangeSources\Accessibility;

use Ox\Core\Locales\Translator;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Eai\CReport;

class AccessibilityReportMaker
{
    protected Translator $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Make a report for accessibility
     *
     * @param array $data
     *
     * @return CReport
     */
    public function makeReport(array $data): CReport
    {
        $report = new CReport(
            $this->translator->tr("AccessibilityReportMaker-title-test accessibility source")
        );

        $this->makeIsAccessible($data, $report);

        $this->makeIsAuthenticate($data, $report);

        $this->makeIsDirAccessible($data, $report);

        $this->makeIsWritable($data, $report);

        return $report;
    }

    /**
     * Report accessibility
     *
     * @param array   $data
     * @param CReport $report
     *
     * @return void
     */
    protected function makeIsAccessible(array $data, CReport $report): void
    {
        $is_accessible = $data[ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE]['status'] ?? false;
        $exception     = $data[ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE]['exception'] ?? null;

        $item = new CItemReport(
            $this->translator->tr(
                'AccessibilityReportMaker-msg-test accessibility'
            ),
            $is_accessible ? CItemReport::SEVERITY_SUCCESS : CItemReport::SEVERITY_ERROR
        );

        if (!$is_accessible && isset($exception)) {
            $item->addSubData($exception->getMessage());
        }

        $report->addItem($item);
    }

    /**
     * Report test for writing
     *
     * @param array   $data
     * @param CReport $report
     */
    protected function makeIsWritable(array $data, CReport &$report): void
    {
        $is_writable = $data[ExchangeSourceFileAccessibility::REPORT_DIR_WRITABLE]['status'] ?? false;
        $file_path   = $data[ExchangeSourceFileAccessibility::REPORT_FILE_ADD]['file_path'] ?? '';

        $item = new CItemReport(
            $this->translator->tr('AccessibilityReportMaker-msg-test manage file'),
            $is_writable ? CItemReport::SEVERITY_SUCCESS : CItemReport::SEVERITY_ERROR
        );

        $report->addItem($item);

        // path
        $item->addSubData(
            $this->translator->tr('AccessibilityReportMaker-msg-reference file path', $file_path)
        );

        // add
        $data_add = $data[ExchangeSourceFileAccessibility::REPORT_FILE_ADD] ?? null;
        $item->addSubData(
            $this->translator->tr('AccessibilityReportMaker-msg-test add file'),
            ($data_add['status'] ?? false) ? CItemReport::SEVERITY_SUCCESS : CItemReport::SEVERITY_ERROR
        );
        if ($exception = ($data_add['exception'] ?? null)) {
            $item->addSubData($exception->getMessage(), CItemReport::SEVERITY_ERROR);
        }

        // get
        $data_get = $data[ExchangeSourceFileAccessibility::REPORT_FILE_GET] ?? null;
        $item->addSubData(
            $this->translator->tr('AccessibilityReportMaker-msg-test get file'),
            ($data_get['status'] ?? false) ? CItemReport::SEVERITY_SUCCESS : CItemReport::SEVERITY_ERROR
        );
        if ($exception = ($data_get['exception'] ?? null)) {
            $item->addSubData($exception->getMessage(), CItemReport::SEVERITY_ERROR);
        }

        // delete
        $data_delete = $data[ExchangeSourceFileAccessibility::REPORT_FILE_DELETE] ?? null;
        $item->addSubData(
            $this->translator->tr('AccessibilityReportMaker-msg-test delete file'),
            ($data_delete['status'] ?? false) ? CItemReport::SEVERITY_SUCCESS : CItemReport::SEVERITY_ERROR
        );
        if ($exception = ($data_delete['exception'] ?? null)) {
            $item->addSubData($exception->getMessage(), CItemReport::SEVERITY_ERROR);
        }
    }

    /**
     * Report test authentication
     *
     * @param array   $data
     * @param CReport $report
     *
     * @return bool
     */
    protected function makeIsAuthenticate(array $data, CReport $report): void
    {
        $is_authorized = $data[ExchangeSourceFileAccessibility::REPORT_SOURCE_AUTHENTICATE]['status'] ?? false;
        $exception     = $data[ExchangeSourceFileAccessibility::REPORT_SOURCE_AUTHENTICATE]['exception'] ?? null;

        $item = new CItemReport(
            $this->translator->tr(
                'AccessibilityReportMaker-msg-test authentification'
            ),
            $is_authorized ? CItemReport::SEVERITY_SUCCESS : CItemReport::SEVERITY_ERROR
        );

        if (!$is_authorized && isset($exception)) {
            $item->addSubData($exception->getMessage());
        }

        $report->addItem($item);
    }

    private function makeIsDirAccessible(array $data, CReport $report): void
    {
        $is_accessible = $data[ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE]['status'] ?? false;
        $exception     = $data[ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE]['exception'] ?? null;
        $directory     = $data[ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE]['dir_path'] ?? '';

        $item = new CItemReport(
            $this->translator->tr(
                'AccessibilityReportMaker-msg-test directory accessible'
            ),
            $is_accessible ? CItemReport::SEVERITY_SUCCESS : CItemReport::SEVERITY_ERROR
        );

        $item->addSubData(
            $this->translator->tr('AccessibilityReportMaker-msg-test directory path usage', $directory)
        );

        if (!$is_accessible && isset($exception)) {
            $item->addSubData($exception->getMessage());
        }

        $report->addItem($item);
    }
}
