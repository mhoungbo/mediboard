<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\ExchangeSources\Exceptions;

use Ox\Mediboard\System\CExchangeSource;

class InvalidSourceException extends SourceException
{
    public function __construct(string|CExchangeSource $source, $text = null)
    {
        if ($source instanceof CExchangeSource) {
            $source = $source->name ?: $source->_guid;
        }

        if (!$text) {
            $text = "CExchangeSource-msg-invalid search source for %s";
        }

        parent::__construct($text, $source);
    }
}
