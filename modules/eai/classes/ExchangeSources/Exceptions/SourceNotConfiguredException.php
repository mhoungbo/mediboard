<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\ExchangeSources\Exceptions;

use Ox\Mediboard\System\CExchangeSource;

class SourceNotConfiguredException extends SourceException
{
    public function __construct(string|CExchangeSource $source, $text = null)
    {
        if ($source instanceof CExchangeSource) {
            $source = $source->name ?: $source->_guid;
        }

        if (!$text) {
            $text = "CExchangeSource-error-Source %s missing settings";
        }

        parent::__construct($text, $source);
    }
}
