<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Logger;

use Exception;
use Ox\Core\CMbDT;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\Logger\Wrapper\DebugInteropLoggerWrapper;

class InteropLoggerManager
{
    private CInteropActor $actor;
    private ?CInteropActor $old_actor;
    private bool $debug_modified = false;

    public function __construct(CInteropActor $actor)
    {
        $this->actor     = $actor;
        $this->old_actor = $actor->loadOldObject()->_id ? $actor->_old : null;
    }

    /**
     * Do some checks before the store of actor
     *
     * @return void
     * @throws Exception
     */
    public function beforeActorStore(): void
    {
        // retrieve state of sender
        $this->debug_modified = $is_debug_mode_modified = $this->actor->fieldModified('debug_session');

        $this->actor->debug_session = $this->limitDebugDuration();

        $old_mode_debug = $this->old_actor?->debug_session;

        if ($is_debug_mode_modified && $old_mode_debug) {
            $this->stopLogEvent();
        }
    }

    /**
     * Put a specific log for this actor when the session is stopped
     *
     * @return void
     * @throws Exception
     */
    private function stopLogEvent(): void
    {
        if (!$this->actor->_id) {
            return;
        }

        $actor  = $this->actor;
        $logger = $actor->getLogger();
        if (!($logger instanceof DebugInteropLoggerWrapper)) {
            $logger = new DebugInteropLoggerWrapper($actor);
        }

        if ($logger->isStarted()) {
            $logger->stopSession();
        }

        // the logger session should be not started here
        $uid_session = $this->old_actor ? $this->old_actor->getSessionLogUUID() : $this->actor->getSessionLogUUID();
        $logger->debug("stop log", ['actor_log_session_uuid' => $uid_session]);
    }

    /**
     * Put a specific log for this actor when the session is started
     *
     * @return void
     * @throws Exception
     */
    private function startLogEvent(): void
    {
        $actor = $this->actor;
        if (!$actor->_id) {
            return;
        }

        $now = CMbDT::dateTime();
        if (!$actor->debug_session || ($actor->debug_session < $now)) {
            return;
        }

        $logger = $actor->getLogger();
        if ($logger->isStarted()) {
            $logger->stopSession();
        }

        // the logger session should be not started here
        $logger->debug("start log");
    }

    /**
     * Handle specific actions for logger after actor was modified
     *
     * @return void
     * @throws Exception
     */
    public function afterActorStore(): void
    {
        if (!$this->actor->_id) {
            return;
        }

        // stop or / and start Session log
        if ($this->debug_modified && $this->actor->debug_session) {
            $this->startLogEvent();
        }
    }

    /**
     * Feature to debug an actor is limited in days
     *
     * @return string|null
     * @throws Exception
     */
    private function limitDebugDuration(): ?string
    {
        $debug_session = $this->actor->debug_session;
        if (($debug_session === null) || ($debug_session === '')) {
            return $this->actor->debug_session;
        }

        $max_days     = $this->actor->conf('eai CInteropActor limit_days_debug_session', 'static') ?? 14;
        $max_datetime = CMbDT::dateTime("+$max_days DAYS", CMbDT::dateTime());

        return ($debug_session > $max_datetime) ? $max_datetime : $debug_session;
    }
}
