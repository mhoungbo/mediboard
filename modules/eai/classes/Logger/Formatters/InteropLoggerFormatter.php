<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Logger\Formatters;

use Exception;
use Ox\Core\CClassMap;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\Logger\Events\EAILoggerEventInterface;
use Stringable;

class InteropLoggerFormatter
{
    public function __construct(private CInteropActor $actor)
    {
    }

    /**
     * @param Stringable[]|EAILoggerEventInterface[] $records
     * @return array<string,array>
     * @throws Exception
     */
    public function format(array $records): array
    {
        return [
            $this->formatMessage($records),
            $this->formatContext($records)
        ];
    }

    /**
     * @param Stringable[]|EAILoggerEventInterface[] $records
     * @return string
     * @throws Exception
     */
    private function formatMessage(array $records): string
    {
        return "";
    }

    /**
     * @param Stringable[]|EAILoggerEventInterface[] $records
     * @return array
     * @throws Exception
     */
    private function formatContext(array $records): array
    {
        $context   = [];
        $class_map = CClassMap::getInstance();

        foreach ($records as $record) {
            [$level, $message, $data] = $record;

            // event object
            if ($message instanceof EAILoggerEventInterface) {
                $context[$class_map->getShortName($message::class)][] = $message->getContext();
            } elseif (is_string($message) || ($message instanceof Stringable)) { // other data
                if (!$data) {
                    $context['extraContext'][] = (string) $message;
                } else {
                    $context['extraContext'][$message] = $data;
                }
            }
        }

        $context['actor_log_message_uuid'] = uniqid();

        return $context;
    }
}
