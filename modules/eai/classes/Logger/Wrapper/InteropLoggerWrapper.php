<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Logger\Wrapper;

use Exception;
use Ox\Core\CApp;
use Ox\Core\Logger\LoggerLevels;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;
use Psr\Log\LogLevel;

class InteropLoggerWrapper implements LoggerInterface
{
    use LoggerAwareTrait;
    use LoggerTrait;

    private const LEVEL_MAPPING = [
        LogLevel::EMERGENCY => LoggerLevels::LEVEL_EMERGENCY,
        LogLevel::ALERT     => LoggerLevels::LEVEL_ALERT,
        LogLevel::CRITICAL  => LoggerLevels::LEVEL_CRITICAL,
        LogLevel::ERROR     => LoggerLevels::LEVEL_ERROR,
        LogLevel::WARNING   => LoggerLevels::LEVEL_WARNING,
        LogLevel::NOTICE    => LoggerLevels::LEVEL_NOTICE,
        LogLevel::INFO      => LoggerLevels::LEVEL_INFO,
        LogLevel::DEBUG     => LoggerLevels::LEVEL_DEBUG,
    ];

    /**
     * @param LoggerInterface|null $logger
     */
    public function __construct(?LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     *
     * @param       $level
     * @param       $message
     * @param array $context
     *
     * @throws Exception
     */
    public function log($level, $message, array $context = [])
    {
        $message = "[EAI] $message";

        $level = is_string($level) ? (self::LEVEL_MAPPING[$level] ?? LoggerLevels::LEVEL_INFO) : $level;

        if ($this->logger) {
            $this->logger->log($level, $message, $context);
        } else {
            CApp::log($message, $context, $level);
        }
    }
}
