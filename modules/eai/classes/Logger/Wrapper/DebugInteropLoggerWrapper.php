<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Logger\Wrapper;

use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\Logger\Formatters\InteropLoggerFormatter;
use Ox\Interop\Eai\Logger\LoggerAggregatorInterface;
use Psr\Log\LoggerInterface;

class DebugInteropLoggerWrapper extends InteropLoggerWrapper implements LoggerAggregatorInterface
{
    private array $records = [];

    private bool $is_started = false;

    private InteropLoggerFormatter $formatter;
    private CInteropActor $actor;

    public function __construct(CInteropActor $actor, ?LoggerInterface $logger = null)
    {
        parent::__construct($logger);

        $this->actor     = $actor;
        $this->formatter = new InteropLoggerFormatter($actor);
    }

    /**
     * @inheritDoc
     */
    public function startSession(): void
    {
        $this->records    = [];
        $this->is_started = true;
    }

    /**
     * @inheritDoc
     */
    public function isStarted(): bool
    {
        return $this->is_started;
    }

    /**
     * @inheritDoc
     */
    public function stopSession(): void
    {
        $this->is_started = false;

        if ($this->records) {
            [$message, $context] = $this->formatter->format($this->records);

            $this->debug($message, $context);
            $this->records = [];
        }

        $this->actor->addToStore('getEventForLogger', null);
    }

    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = [])
    {
        if ($this->is_started) {
            $this->records[] = [$level, $message, $context];
        } else {
            $prefix_message = "[{actor_log_guid}][{actor_log_session_uuid}]";
            if (!array_key_exists('actor_log_guid', $context)) {
                $context['actor_log_guid'] = $this->actor->_class . '-' . $this->actor->_id;
            }

            if (!array_key_exists('actor_log_session_uuid', $context)) {
                $context['actor_log_session_uuid'] = $this->actor->getSessionLogUUID();
            }

            if (array_key_exists('actor_log_message_uuid', $context)) {
                $prefix_message .= '[{actor_log_message_uuid}]';
            }

            $message = $prefix_message . ($message ? ' ' : '') . $message;

            parent::log($level, $message, $context);
        }
    }
}
