<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Logger;

use Psr\Log\LoggerTrait;

class EmptyLogger implements LoggerAggregatorInterface
{
    use LoggerTrait;

    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = [])
    {
        return;
    }

    public function startSession(): void
    {
        return;
    }

    public function stopSession(): void
    {
        return;
    }

    public function isStarted(): bool
    {
        return false;
    }
}
