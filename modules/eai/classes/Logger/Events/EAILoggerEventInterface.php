<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Logger\Events;

use Ox\Interop\Eai\CInteropActor;
use Stringable;

/**
 * EAILoggerEventInterface represent a structured message in destination of handler log Interop
 *
 * Each children of this interface send to the logger Interop will be automatically handled
 * and the function getContext will be called. Each data of this call will be log in the format
 */
interface EAILoggerEventInterface extends Stringable
{
    public function __construct(CInteropActor $actor);

    /**
     * Each data return by this function will be added to a log associate with this event type class.
     * It's preferable to add a guid or only certain fields of an object rather than a full object
     *
     * @return array
     */
    public function getContext(): array;
}
