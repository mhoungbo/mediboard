<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Logger\Events;

use Ox\Core\Locales\Translator;
use Ox\Interop\Eai\CExchangeDataFormat;
use Ox\Interop\Eai\CInteropActor;

class EAIMessageHandleEvent implements EAILoggerEventInterface
{
    private Translator $translator;
    private bool $message_decoded = false;
    private ?CExchangeDataFormat $exchange = null;
    private CInteropActor $actor;

    public function __construct(CInteropActor $actor)
    {
        $this->actor      = $actor;
        $this->translator = new Translator();
    }

    public function __toString(): string
    {
        $uid_session_log = $this->actor->getSessionLogUUID();

        return "[$uid_session_log] eai handle message event";
    }

    public function getContext(): array
    {
        $context = [
            'message_decoded' => $this->message_decoded,
        ];

        if ($this->exchange && $this->exchange->_guid) {
            $context['exchange_class'] = $this->exchange->_class;
            $context['exchange_id']    = $this->exchange->_id;
        }

        return $context;
    }

    /**
     * Set when message was utf8 decoded
     *
     * @param bool $message_decoded
     * @return EAIMessageHandleEvent
     */
    public function setMessageDecoded(bool $message_decoded): self
    {
        $this->message_decoded = $message_decoded;

        return $this;
    }

    /**
     * Set exchange
     *
     * @param CExchangeDataFormat|null $exchange
     */
    public function setExchange(CExchangeDataFormat $exchange): void
    {
        $this->exchange = $exchange;
    }
}
