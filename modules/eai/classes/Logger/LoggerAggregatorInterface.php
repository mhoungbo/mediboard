<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Logger;

use Psr\Log\LoggerInterface;

/**
 * Add to a logger the functionality to stack logs and then to reformat logs for create only one log.
 */
interface LoggerAggregatorInterface extends LoggerInterface
{
    /**
     * Start the session of logging
     * All log message adding after this usage of function will be stacked and reformatted to create only one log.
     *
     * @return void
     */
    public function startSession(): void;

    /**
     * Stop the session of logging
     * All logs collected since startSession was activated
     * will be reformatted and only one log will be send to the system of collecting logs
     *
     * @return void
     */
    public function stopSession(): void;

    /**
     * Know if a session is already started
     *
     * @return bool
     */
    public function isStarted(): bool;
}
