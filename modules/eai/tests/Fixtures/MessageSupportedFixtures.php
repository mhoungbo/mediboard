<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures;

use Ox\Core\CModelObjectException;
use Ox\Interop\Hl7\CReceiverHL7v2;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Class MessageSupportedFixtures
 * @package Ox\Interop\Eai\Tests\Fixtures
 */
class MessageSupportedFixtures extends Fixtures implements GroupFixturesInterface
{
    public const MESSAGE_SUPPORTED_RECEIVER = 'message_supported_receiver';

    /**
     * @return void
     * @throws FixturesException
     */
    public function load(): void
    {
        $this->createReceiver();
    }

    /**
     * @return array
     */
    public static function getGroup(): array
    {
        return ['message_supported', 100];
    }

    /**
     * @return void
     * @throws FixturesException
     */
    private function createReceiver(): void
    {
        $receiver           = new CReceiverHL7v2();
        $receiver->nom      = 'MessageSupported_receiverhl7v2';
        $receiver->libelle  = 'MessageSupported ReceiverHL7v2';
        $receiver->group_id = $this->getUser()->_group_id;
        $this->store($receiver, self::MESSAGE_SUPPORTED_RECEIVER);
    }
}
