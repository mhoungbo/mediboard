<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbSecurity;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CPatientINSNIR;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\Utilities\PersonInfoProvider;

/**
 * Use this trait only in Fixtures class
 */
trait PatientFixturesTrait
{
    /**
     * @param CPatient    $patient
     * @param string|null $tag
     * @param string|null $ipp
     * @param string      $tag_ipp
     *
     * @return CIdSante400
     * @throws CMbException
     */
    public function addIPPToPatient(
        CPatient $patient,
        ?string  $tag = null,
        ?string  $ipp = null,
        string   $tag_ipp = "tag_ipp"
    ): CIdSante400 {
        if (!$patient->_id) {
            throw new CMbException('Impossible de r�cup�rer le patient_id de l\'objet fournit');
        }

        if (!$ipp) {
            $ipp = CMbSecurity::generateUUID();
        }

        $id_sante400 = CIdSante400::getMatch($patient->_class, $tag_ipp, $ipp, $patient->_id);
        $this->store($id_sante400, $tag);

        return $id_sante400;
    }

    /**
     * @param CPatient    $patient
     * @param string|null $ins
     * @param string      $ins_type
     *
     * @return void
     * @throws Exception
     */
    public function addINSToPatient(
        CPatient $patient,
        ?string  $ins = null,
        string   $ins_type = CPatientINSNIR::OID_INS_NIR
    ): void {
        $source_identity = $patient->loadRefSourceIdentite();

        if ($ins === null) {
            $ins = CMbSecurity::generateUUID();
        }

        CPatientINSNIR::createUpdate(
            $patient->_id,
            $patient->nom,
            $patient->prenom,
            $patient->naissance,
            $ins,
            'from_fixtures',
            $ins_type,
            null,
            $source_identity->_id
        );
    }

    /**
     * Create AppFine Patient
     *
     * @param CUser|null  $user
     * @param string|null $tag
     *
     * @return CPatient
     * @throws CModelObjectException
     * @throws FixturesException
     * @throws Exception
     */
    public function createFastPatient(?string $tag = null): CPatient
    {
        $name  = PersonInfoProvider::getLastName();
        $given = PersonInfoProvider::getFirstName();

        $sex   = PersonInfoProvider::getSexe();
        $email = $name . "." . $given . "@openxtrem.com";

        /** @var CPatient $patient */
        $patient            = CPatient::getSampleObject();
        $patient->nom       = $name;
        $patient->prenom    = $given;
        $patient->sexe      = $sex;
        $patient->naissance = CMbDT::getRandomDate("1950-01-01", "2000-12-31", str_replace('%', '', CMbDT::ISO_DATE));
        $patient->email     = $email;

        if ($tag) {
            $this->store($patient, $tag);
        }

        return $patient;
    }
}
