<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures\ExchangeSources;

use Ox\Core\CAppUI;
use Ox\Core\CMbPath;
use Ox\Core\CModelObjectException;
use Ox\Core\Module\CModule;
use Ox\Interop\Ftp\CSourceFTP;
use Ox\Interop\Ftp\CSourceSFTP;
use Ox\Mediboard\System\CSourceFileSystem;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

/**
 * @description Create sources (FS, FTP, SFTP) to make integrations test on CI.
 */
class ClientFileFixtures extends Fixtures
{
    public const SOURCE_FTP  = 'client_file_fixtures_source_ftp';
    public const SOURCE_SFTP = 'client_file_fixtures_source_sftp';
    public const SOURCE_FS   = 'client_file_fixtures_source_fs';

    /**
     * @inheritDoc
     * @throws FixturesException|CModelObjectException
     */
    public function load()
    {
        if (CModule::getActive('ftp')) {
            $source_ftp = $this->createFTP();
            $this->store($source_ftp, self::SOURCE_FTP);

            $source_sftp = $this->createSFTP();
            $this->store($source_sftp, self::SOURCE_SFTP);
        }

        $source_fs = $this->createFS();
        $this->store($source_fs, self::SOURCE_FS);
    }

    private function createFTP(): CSourceFTP
    {
        $source           = new CSourceFTP();
        $source->name     = uniqid('fixtures_source_ftp_');
        $source->host     = 'ftpd_server';
        $source->role     = CAppUI::conf("instance_role");
        $source->port     = 21;
        $source->user     = 'dev';
        $source->password = 'oxdev17!';

        return $source;
    }

    private function createSFTP(): CSourceSFTP
    {
        $source             = new CSourceSFTP();
        $source->name       = uniqid('fixtures_source_ftp_');
        $source->host       = 'sftp_server';
        $source->role       = CAppUI::conf("instance_role");
        $source->port       = 22;
        $source->user       = 'dev';
        $source->password   = 'oxdev17!';
        $source->fileprefix = 'upload/';

        return $source;
    }

    private function createFS(): CSourceFileSystem
    {
        $tmp_dir = CAppUI::getTmpPath('exchange_sources_fs');
        CMbPath::forceDir($tmp_dir);

        $source       = new CSourceFileSystem();
        $source->name = uniqid('fixtures_source_fs_');
        $source->host = $tmp_dir;
        $source->role = CAppUI::conf("instance_role");

        return $source;
    }
}
