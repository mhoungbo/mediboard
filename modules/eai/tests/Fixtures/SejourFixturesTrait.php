<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures;

use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CMbSecurity;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Sante400\CIdSante400;

/**
 * Use this trait only in Fixtures class
 */
trait SejourFixturesTrait
{
    use PatientFixturesTrait;

    /**
     * @param string|null $tag
     * @param CPatient    $patient
     * @param CMediusers  $mediusers
     * @param string|null $datetime
     *
     * @return CSejour
     * @throws CMbException
     * @throws CModelObjectException
     */
    public function createSejour(
        ?string    $tag,
        CPatient   $patient,
        CMediusers $mediusers,
        ?string    $datetime = null
    ): CSejour {
        if (!$patient->_id) {
            throw new CMbException('Impossible de r�cup�rer le patient_id de l\'objet fournit');
        }

        if (!$mediusers->_id) {
            throw new CMbException('Impossible de r�cup�rer le user_id de l\'objet fournit');
        }

        if (!$datetime) {
            $datetime = CMbDT::dateTime();
        }

        /** @var CSejour $sejour */
        $sejour               = CSejour::getSampleObject();
        $sejour->patient_id   = $patient->_id;
        $sejour->group_id     = CGroups::loadCurrent()->_id;
        $sejour->praticien_id = $mediusers->_id;
        $sejour->entree       = $sejour->entree_prevue = $datetime;
        $sejour->sortie       = $sejour->sortie_prevue = CMbDT::dateTime('+2 DAYS', $datetime);
        $sejour->type         = 'comp';

        if ($tag) {
            $this->store($sejour, $tag);
        }

        return $sejour;
    }

    /**
     * @param string|null $tag
     *
     * @return array
     * @throws CMbException
     * @throws CModelObjectException
     * @throws \Ox\Tests\Fixtures\FixturesException
     */
    public function createFastSejour(string $tag, bool $new_mediusers = false): array
    {
        return [
            CPatient::class   => ($patient = $this->createFastPatient($tag)),
            CMediusers::class => ($mediusers = $this->getUser($new_mediusers)),
            CSejour::class    => ($this->createSejour($tag, $patient, $mediusers)),
        ];
    }

    /**
     * @param CPatient    $patient
     * @param string|null $tag
     * @param string|null $ipp
     * @param string      $tag_ipp
     *
     * @return CIdSante400
     * @throws CMbException
     */
    public function addNdaToSejour(
        CSejour $sejour,
        ?string $tag = null,
        ?string $nda = null,
        string  $tag_nda = "tag_nda"
    ): CIdSante400 {
        if (!$sejour->_id) {
            throw new CMbException('Impossible de r�cup�rer le sejour_id de l\'objet fournit');
        }

        if (!$nda) {
            $nda = CMbSecurity::generateUUID();
        }

        $idex = CIdSante400::getMatch($sejour->_class, $tag_nda, $nda, $sejour->_id);
        $this->store($idex, $tag);

        return $idex;
    }
}
