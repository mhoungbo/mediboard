<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures;

use Ox\Core\CModelObjectException;
use Ox\Interop\Eai\CDomain;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Class GroupDomainFixtures
 * @package Ox\Interop\Eai\Tests\Fixtures
 */
class GroupDomainFixtures extends Fixtures implements GroupFixturesInterface
{
    public const GROUP_DOMAIN_GROUP  = 'group_domain_group';
    public const GROUP_DOMAIN_DOMAIN = 'group_domain_domain';

    /**
     * @return void
     * @throws FixturesException
     * @throws CModelObjectException
     */
    public function load(): void
    {
        $this->createGroup();
        $this->createDomain();
    }

    /**
     * @return array
     */
    public static function getGroup(): array
    {
        return ['group_domain', 100];
    }

    /**
     * @return void
     * @throws FixturesException
     * @throws CModelObjectException
     */
    private function createGroup(): void
    {
        /** @var CGroups $group */
        $group       = CGroups::getSampleObject();
        $group->text = 'GroupDomain_text';
        $group->code = 'GroupDomain_code';
        $this->store($group, self::GROUP_DOMAIN_GROUP);
    }

    /**
     * @return void
     * @throws FixturesException
     */
    private function createDomain(): void
    {
        $domain      = new CDomain();
        $domain->tag = 'ipp_fixtures';
        $this->store($domain, self::GROUP_DOMAIN_DOMAIN);
    }
}
