<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures\Actors;

use Ox\Core\CMbException;
use Ox\Core\CStoredObject;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\CMessageSupported;
use Ox\Mediboard\System\CConfiguration;

trait ActorFixturesTrait
{
    public function addConfigurationForActor(CInteropActor $actor, string $path, mixed $value): void
    {
        $this->addConfiguration($actor, $path, $value);
    }

    public function addConfiguration(CStoredObject $object, string $path, mixed $value): void
    {
        $msg = CConfiguration::setConfig($path, $value, $object);
        if ($msg) {
            throw new CMbException($msg);
        }
    }

    public function addMessageSupportedForActor(
        CInteropActor $actor,
        array|CMessageSupported $message_supported
    ): CMessageSupported {
        $is_object = is_object($message_supported);

        $new_message_supported              = new CMessageSupported();
        $new_message_supported->transaction = $is_object ? ($message_supported->transaction) : ($message_supported['transaction'] ?? null);
        $new_message_supported->message     = $is_object ? ($message_supported->message) : ($message_supported['message'] ?? null);
        $new_message_supported->profil      = $is_object ? ($message_supported->profil) : ($message_supported['profil'] ?? null);
        $new_message_supported->active      = 1;
        $new_message_supported->setObject($actor);

        $this->store($new_message_supported);

        return $new_message_supported;
    }

    /**
     * @param CMessageSupported $message_supported
     * @param string            $module
     * @param string            $mapper
     * @return void
     * @throws CMbException
     */
    public function setMapperForMessageSupported(
        CMessageSupported $message_supported,
        string            $module,
        string            $mapper
    ): void {
        $this->addConfiguration($message_supported, "$module delegated_objects delegated_mapper", $mapper);
    }
}
