<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures\Actors;

use Ox\Interop\Hl7\CReceiverHL7v2;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Tests\Fixtures\Fixtures;

/**
 * @description Use for test algorithms used in interop to record the Patient
 */
class ReceiverFixtures extends Fixtures
{
    use ReceiverFixturesTrait;

    final public const RECEIVER_HL7V2_WITH_CONFIGURATION = 'receiver_hl7v2_with_configuration';
    final public const RECEIVER_HL7V2_WITH_MULTIPLE_CONFIGURATIONS = 'receiver_hl7v2_with_configurations';
    final public const RECEIVER_HL7V2_WITH_MESSAGE_SUPPORTED = 'receiver_hl7v2_with_message_supported';
    final public const RECEIVER_HL7V2_WITH_MULTIPLE_MESSAGES_SUPPORTED = 'receiver_hl7v2_with_messages_supported';


    public function load(): void
    {
        $current_groups = CGroups::loadCurrent();

        // one configuration
        $receiver = $this->createReceiver(
            CReceiverHL7v2::class,
            $current_groups,
            self::RECEIVER_HL7V2_WITH_CONFIGURATION
        );
        $this->addConfigurationForActor($receiver, "doctolib handle_doctolib", '1');

        // multiple configurations
        $receiver = $this->createReceiver(
            CReceiverHL7v2::class,
            $current_groups,
            self::RECEIVER_HL7V2_WITH_MULTIPLE_CONFIGURATIONS
        );
        $this->addConfigurationForActor($receiver, "doctolib handle_doctolib", '1');
        $this->addConfigurationForActor($receiver, "appFine handle_portail_patient", '1');

        // one message supported
        $receiver = $this->createReceiver(
            CReceiverHL7v2::class,
            $current_groups,
            self::RECEIVER_HL7V2_WITH_MESSAGE_SUPPORTED
        );
        $this->addMessageSupportedForActor(
            $receiver,
            ['message' => 'CHL7EventORUR01', 'transaction' => 'PDC-01', 'profil' => 'CDEC']
        );

        // multiple message supported
        $receiver = $this->createReceiver(
            CReceiverHL7v2::class,
            $current_groups,
            self::RECEIVER_HL7V2_WITH_MULTIPLE_MESSAGES_SUPPORTED
        );
        $this->addMessageSupportedForActor(
            $receiver,
            ['message' => 'CHL7EventORUR01', 'transaction' => 'PDC-01', 'profil' => 'CDEC']
        );
        $this->addMessageSupportedForActor($receiver, ['message' => 'CHL7EventORUR01', 'profil' => 'CHL7ORU']);
    }
}
