<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures\Actors;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Interop\Eai\CInteropReceiver;
use Ox\Interop\Eai\CInteropReceiverFactory;
use Ox\Interop\Eai\ExchangeDataFormatFactory;
use Ox\Mediboard\Etablissement\CGroups;

/**
 * @description Use for test algorithms used in interop to record the Patient
 */
trait ReceiverFixturesTrait
{
    use ActorFixturesTrait;

    /**
     * @param class-string<CInteropReceiver> $class
     * @param CGroups                        $groups
     * @param string|null                    $tag
     * @param string|null                    $type
     * @return CInteropReceiver
     * @throws Exception
     */
    public function createReceiver(
        string $class,
        CGroups $groups,
        string $tag = null,
        string $type = null
    ): CInteropReceiver {
        $receiver           = (new CInteropReceiverFactory())->make($class, $type);
        $receiver->nom      = "Fixtues-" . uniqid();
        $receiver->actif    = 1;
        $receiver->group_id = $groups->_id;
        $receiver->role     = CAppUI::conf('instance_role');
        $this->store($receiver, $tag);

        return $receiver;
    }

    /**
     * @throws CMbException
     */
    public function addConfigurationForActor(CInteropReceiver $actor, string $path, mixed $value): void
    {
        if (!str_starts_with('eai ', $path)) {
            $data_format = (new ExchangeDataFormatFactory())->makeExchangeFromReceiver($actor);

            $path = "eai {$data_format->_class}-$path";
        }

        $this->addConfiguration($actor, $path, $value);
    }
}
