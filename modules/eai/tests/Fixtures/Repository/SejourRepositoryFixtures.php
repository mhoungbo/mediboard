<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures\Repository;

use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Interop\Eai\Tests\Fixtures\SejourFixturesTrait;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * @description Use for test algorithms used in interop to record the Patient
 */
class SejourRepositoryFixtures extends Fixtures implements GroupFixturesInterface
{
    use SejourFixturesTrait;

    /** @var string */
    public const REF_SEJOUR = 'ref_primary_sejour_repository';
    /** @var string */
    public const REF_MULTIPLE_SEJOUR = 'ref_multiple_sejour_repository';

    /** @var string */
    public const PRIMARY_SEJOUR_DATE_ENTREE = "2022-01-20 10:00:00";

    /** @var string */
    public const TAG_NDA = 'tag_nda';

    /** @var CMediusers */
    public static $mediusers;

    /**
     * @inheritDoc
     * @throws FixturesException|CModelObjectException
     */
    public function load()
    {
        // Patient
        $patient = $this->createFastPatient();
        $this->store($patient);

        $primary_sejour   = $this->createSejour(
            self::REF_SEJOUR,
            $patient,
            $this->getUser(),
            self::PRIMARY_SEJOUR_DATE_ENTREE
        );

        $this->addNdaToSejour($primary_sejour);

        // multiple sejour in same date
        $patient = $this->createFastPatient();
        $this->store($patient);

        // sejour 1
        $specific_mediuser = $this->getUser(false);
        $sejour_1          = $this->createSejour(
            self::REF_MULTIPLE_SEJOUR,
            $patient,
            $specific_mediuser,
            self::PRIMARY_SEJOUR_DATE_ENTREE
        );
        $this->addNdaToSejour($sejour_1);

        // sejour 2
        $sejour_2 = $this->createSejour(
            null,
            $patient,
            $specific_mediuser,
            self::PRIMARY_SEJOUR_DATE_ENTREE
        );
        $sejour_2->entree = $sejour_2->entree_prevue = CMbDT::dateTime('-1 DAYS', $sejour_1->entree);
        $sejour_2->sortie = $sejour_2->sortie_prevue = CMbDT::dateTime('+6 HOURS', $sejour_2->entree);
        $this->store($sejour_2);
    }

    /**
     * @inheritDoc
     */
    public static function getGroup(): array
    {
        return ['eai-repository'];
    }

    /**
     * @param bool $new
     *
     * @return CMediusers
     * @throws FixturesException
     */
    public static function getMediusers(bool $new = false): CMediusers
    {
        if (!self::$mediusers || $new) {
            self::$mediusers = (new self())->getUser(false);
        }

        return self::$mediusers;
    }
}
