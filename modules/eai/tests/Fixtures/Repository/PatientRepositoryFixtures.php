<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures\Repository;

use Ox\Core\CMbException;
use Ox\Core\CModelObjectException;
use Ox\Interop\Eai\Tests\Fixtures\PatientFixturesTrait;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * @description Use for test algorithms used in interop to record the Patient
 */
class PatientRepositoryFixtures extends Fixtures implements GroupFixturesInterface
{
    use PatientFixturesTrait;

    /** @var string */
    public const PATIENT_WITH_INS = 'patient_locator_ins';
    /** @var string */
    public const PATIENT_WITH_IPP = 'patient_locator_ipp';
    /** @var string */
    public const PATIENT_WITH_NO_IDENTIFIERS = 'patient_locator_without_identifiers';

    /** @var string */
    public const TRAIT_FAMILY = 'TEST_FIXTURE_NAME';
    /** @var string */
    public const TRAIT_GIVEN = 'TEST_FIXTURE_GIVEN';
    /** @var string */
    public const TRAIT_BIRTH_DATE = '1990-01-01';

    /**
     * @return void
     * @throws FixturesException
     * @throws CModelObjectException
     * @throws CMbException
     */
    public function load(): void
    {
        // ins
        /** @var CPatient $patient */
        $patient = $this->createFastPatient(self::PATIENT_WITH_INS);
        $this->addINSToPatient($patient);

        // ipp
        /** @var CPatient $patient */
        $patient = $this->createFastPatient(self::PATIENT_WITH_IPP);
        $this->addIPPToPatient($patient);

        // trait - resource identifier
        $patient = $this->createPatientWithoutIdentifiers();
        $this->store($patient, self::PATIENT_WITH_NO_IDENTIFIERS);
    }

    /**
     * @return CPatient
     * @throws CModelObjectException
     */
    private function createPatientWithoutIdentifiers(): CPatient
    {
        /** @var CPatient $patient */
        $patient            = CPatient::getSampleObject();
        $patient->nom       = self::TRAIT_FAMILY;
        $patient->prenom    = self::TRAIT_GIVEN;
        $patient->naissance = self::TRAIT_BIRTH_DATE;

        return $patient;
    }

    /**
     * @return array
     */
    public static function getGroup(): array
    {
        return ['eai-repository'];
    }
}
