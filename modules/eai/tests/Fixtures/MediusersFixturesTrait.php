<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Fixtures;

use Ox\Interop\Eai\Tests\Exception;

/**
 * Use this trait only in Fixtures class
 */
trait MediusersFixturesTrait
{
    use PatientFixturesTrait;


}
