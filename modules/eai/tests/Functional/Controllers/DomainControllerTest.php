<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Interop\Eai\CDomain;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class DomainControllerTest extends OxWebTestCase
{
    /**
     * @param array $content
     *
     * @return CDomain
     * @throws CMbModelNotFoundException
     * @throws TestsException
     * @throws Exception
     * @dataProvider createProvider
     */
    public function testCreate(array $content): CDomain
    {
        $content = Item::createFromArray([AbstractObject::DATA => $content]);

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
            ->request('POST', '/api/eai/domain', [], [], [], json_encode($content));

        $this->assertResponseStatusCodeSame(201);

        $collection = $this->getJsonApiCollection($client);
        $this->assertCount(1, $collection);

        $item = $collection->getFirstItem();

        $this->assertEquals('domain', $item->getType());
        $this->assertNotNull($item->getId());
        $this->assertEquals('ipp', $item->getAttribute('tag'));

        return CDomain::findOrFail($item->getId());
    }

    /**
     * @return \array[][]
     */
    public function createProvider(): array
    {
        return [
            'Create an ipp domain' => [
                [
                    Item::ID         => '',
                    Item::TYPE       => 'domain',
                    Item::ATTRIBUTES => [
                        'tag' => 'ipp',
                    ],
                ],
            ],
        ];
    }
}
