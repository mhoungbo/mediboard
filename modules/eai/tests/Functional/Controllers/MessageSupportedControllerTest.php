<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Interop\Eai\CMessageSupported;
use Ox\Interop\Eai\Tests\Fixtures\MessageSupportedFixtures;
use Ox\Interop\Hl7\CReceiverHL7v2;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class MessageSupportedControllerTest extends OxWebTestCase
{
    /**
     * @param array $content
     *
     * @return CMessageSupported
     * @throws CMbModelNotFoundException
     * @throws TestsException
     * @throws Exception
     * @dataProvider createProvider
     */
    public function testCreate(array $content): CMessageSupported
    {
        $content = Item::createFromArray([AbstractObject::DATA => $content]);

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
            ->request('POST', '/api/eai/message_supported', [], [], [], json_encode($content));

        $this->assertResponseStatusCodeSame(201);

        $collection = $this->getJsonApiCollection($client);
        $this->assertCount(1, $collection);

        $item = $collection->getFirstItem();

        $this->assertEquals('message_supported', $item->getType());
        $this->assertNotNull($item->getId());
        $this->assertEquals($this->getReceiverFixturesReference()->_id, $item->getAttribute('object_id'));
        $this->assertEquals($this->getReceiverFixturesReference()->_class, $item->getAttribute('object_class'));
        $this->assertEquals('CHL7EventADTA28_FRA', $item->getAttribute('message'));

        return CMessageSupported::findOrFail($item->getId());
    }

    /**
     * @return \array[][]
     * @throws TestsException
     */
    public function createProvider(): array
    {
        return [
            'Create a message supported' => [
                [
                    Item::ID         => '',
                    Item::TYPE       => 'message_supported',
                    Item::ATTRIBUTES => [
                        'object_id'    => $this->getReceiverFixturesReference()->_id,
                        'object_class' => $this->getReceiverFixturesReference()->_class,
                        'message'      => 'CHL7EventADTA28_FRA',
                    ],
                ],
            ],
        ];
    }

    /**
     * @return CReceiverHL7v2
     * @throws TestsException
     */
    private function getReceiverFixturesReference(): CReceiverHL7v2
    {
        /** @var CReceiverHL7v2 $receiver */
        $receiver = $this->getObjectFromFixturesReference(
            CReceiverHL7v2::class,
            MessageSupportedFixtures::MESSAGE_SUPPORTED_RECEIVER
        );

        return $receiver;
    }
}
