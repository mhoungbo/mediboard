<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Interop\Eai\CDomain;
use Ox\Interop\Eai\CGroupDomain;
use Ox\Interop\Eai\Tests\Fixtures\GroupDomainFixtures;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class GroupDomainControllerTest extends OxWebTestCase
{
    /**
     * @param array $content
     *
     * @return CGroupDomain
     * @throws CMbModelNotFoundException
     * @throws TestsException
     * @throws Exception
     * @dataProvider createProvider
     */
    public function testCreate(array $content): CGroupDomain
    {
        $content = Item::createFromArray([AbstractObject::DATA => $content]);

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
            ->request('POST', '/api/eai/group_domain', [], [], [], json_encode($content));

        $this->assertResponseStatusCodeSame(201);

        $collection = $this->getJsonApiCollection($client);
        $this->assertCount(1, $collection);

        $item = $collection->getFirstItem();

        $this->assertEquals('group_domain', $item->getType());
        $this->assertNotNull($item->getId());
        $this->assertEquals('CPatient', $item->getAttribute('object_class'));
        $this->assertEquals(false, $item->getAttribute('master'));

        return CGroupDomain::findOrFail($item->getId());
    }

    /**
     * @return \array[][]
     * @throws TestsException
     */
    public function createProvider(): array
    {
        return [
            'Create a message supported' => [
                [
                    Item::ID         => '',
                    Item::TYPE       => 'group_domain',
                    Item::ATTRIBUTES => [
                        'group_id'     => $this->getGroupFixturesReference()->_id,
                        'domain_id'    => $this->getDomainFixturesReference()->_id,
                        'object_class' => 'CPatient',
                        'master'       => 0,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return CGroups
     * @throws TestsException
     */
    private function getGroupFixturesReference(): CGroups
    {
        /** @var CGroups $group */
        $group = $this->getObjectFromFixturesReference(
            CGroups::class,
            GroupDomainFixtures::GROUP_DOMAIN_GROUP
        );

        return $group;
    }

    /**
     * @return CDomain
     * @throws TestsException
     */
    private function getDomainFixturesReference(): CDomain
    {
        /** @var CDomain $domain */
        $domain = $this->getObjectFromFixturesReference(
            CDomain::class,
            GroupDomainFixtures::GROUP_DOMAIN_DOMAIN
        );

        return $domain;
    }
}
