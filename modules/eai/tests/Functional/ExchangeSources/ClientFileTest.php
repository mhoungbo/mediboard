<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Functional\ExchangesSources;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\Module\CModule;
use Ox\Interop\Eai\CExchangeDataFormat;
use Ox\Interop\Eai\Tests\Fixtures\ExchangeSources\ClientFileFixtures;
use Ox\Interop\Ftp\CSourceFTP;
use Ox\Interop\Ftp\CSourceSFTP;
use Ox\Mediboard\System\CSourceFileSystem;
use Ox\Mediboard\System\Sources\CSourceFile;
use Ox\Mediboard\System\Sources\ObjectPath;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

/**
 * @group schedules
 */
class ClientFileTest extends OxWebTestCase
{
    /**
     * @return array[]
     * @throws TestsException
     */
    public function providerSources(): array
    {
        $source_fs = $this->getObjectFromFixturesReference(CSourceFileSystem::class, ClientFileFixtures::SOURCE_FS);
        $data      = [
            'FileSystem' => [$source_fs],
        ];

        if (CModule::getActive('ftp')) {
            $source_ftp  = $this->getObjectFromFixturesReference(CSourceFTP::class, ClientFileFixtures::SOURCE_FTP);
            $source_sftp = $this->getObjectFromFixturesReference(CSourceSFTP::class, ClientFileFixtures::SOURCE_SFTP);

            $data['FTP']  = [$source_ftp];
            $data['SFTP'] = [$source_sftp];
        }

        return $data;
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testConnection(CSourceFile $source_file): void
    {
        $client = $source_file->getClient();

        $this->assertTrue($client->isReachableSource());
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testAuthentification(CSourceFile $source_file): void
    {
        $client = $source_file->getClient();

        $this->assertTrue($client->isReachableSource());
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testCreateDirectory(CSourceFile $source_file): void
    {
        $dir_name  = uniqid('create_dir_');
        $directory = $source_file->completePath($dir_name)->toDirectory();

        $client = $source_file->getClient();

        try {
            $client->getDirectory($directory);

            $this->fail("An exception should be throw when directory not exits");
        } catch (Exception $exception) {
        }

        $this->assertTrue($client->createDirectory($directory));

        $this->assertStringEndsWith($dir_name, rtrim($client->getDirectory($directory), '/'));
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testExceptionWhenDirectoryAlreadyExist(CSourceFile $source_file): void
    {
        $dir_name  = uniqid('create_dir_');
        $directory = $source_file->completePath($dir_name)->toDirectory();

        $client = $source_file->getClient();
        $this->assertTrue($client->createDirectory($directory));

        $this->expectException(CMbException::class);
        $client->createDirectory($directory);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testSendContent(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $this->createDir($source_file, uniqid('send_'));

        $content = 'ok';
        $source_file->setData($content);
        $file_path = $directory->withFilePath("test.txt");
        $this->assertTrue($client->send($file_path));

        $this->assertEquals($content, $client->getData($file_path));
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testSendContentOk(CSourceFile $source_file): void
    {
        $file_path = $this->setupSendContentOk($source_file);
        $client    = $source_file->getClient();

        $this->assertNotEmpty($client->getData($file_path));
        $this->assertEquals('', $client->getData($file_path->withExtension('ok', true)));
    }

    /**
     * @param CSourceFile $source_file
     *
     * @return ObjectPath
     * @throws CMbException
     */
    private function setupSendContentOk(CSourceFile $source_file): ObjectPath
    {
        $source_file->fileextension_write_end = 'ok';
        $source_file->_client                 = null;
        $client                               = $source_file->getClient();
        $directory                            = $this->createDir($source_file, uniqid('send_ok_'));

        $source_file->setData('ok');
        $file_path = $directory->withFilePath("test.txt");
        $client->send($file_path);

        $source_file->fileextension_write_end = null;
        $source_file->_client                 = null;

        return $file_path;
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testListDirectory(CSourceFile $source_file): void
    {
        $directory = $this->setupEnvListDir($source_file);
        $client    = $source_file->getClient();

        $directories = $client->getListDirectory($directory);

        $this->assertCount(2, $directories);
        foreach ($directories as $dir) {
            $this->assertStringContainsString('sub_dir', $dir);
        }
    }

    /**
     * Setup a directory with 2 directories empty and 6 files
     *
     * @param CSourceFile $source_file
     *
     * @return ObjectPath
     * @throws CMbException
     */
    private function setupEnvListDir(CSourceFile $source_file): ObjectPath
    {
        $client = $source_file->getClient();

        $dir_name  = uniqid("list_dir_");
        $directory = $this->createDir($source_file, $dir_name);

        $this->createDir($source_file, "$dir_name/sub_dir_1");
        $this->createDir($source_file, "$dir_name/sub_dir_2");

        $source_file->setData('ok');
        for ($i = 0; $i <= 5; $i++) {
            $path_file = $directory->withFilePath(uniqid($source_file->generateFileName() . "_"))
                ->withExtension("txt");
            $client->send($path_file);
        }

        return $directory;
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testSize(CSourceFile $source_file): void
    {
        $data   = $this->setupFileSize($source_file);
        $client = $source_file->getClient();

        $this->assertEquals($data['file_empty']['length'], $client->getSize($data['file_empty']['path']));

        $this->assertEquals($data['file_one_char']['length'], $client->getSize($data['file_one_char']['path']));

        $this->assertEquals($data['file_content']['length'], $client->getSize($data['file_content']['path']));
    }

    /**
     * @param CSourceFile $source_file
     *
     * @return array[]
     * @throws CMbException
     */
    private function setupFileSize(CSourceFile $source_file): array
    {
        $client    = $source_file->getClient();
        $directory = $this->createDir($source_file, uniqid("file_size"));

        $path_file_empty = $directory->withFilePath('file_empty.txt');
        $source_file->setData("");
        $client->send($path_file_empty);

        $path_file_1_char = $directory->withFilePath('file_one_char.txt');
        $source_file->setData("a");
        $client->send($path_file_1_char);

        $path_file_with_content = $directory->withFilePath('file_content.txt');
        $content                = sha1(CMbDT::dateTime());
        $source_file->setData($content);
        $client->send($path_file_with_content);

        return [
            'file_content'  => [
                'path'   => $path_file_with_content,
                'length' => strlen($content),
            ],
            'file_one_char' => [
                'path'   => $path_file_1_char,
                'length' => 1,
            ],
            'file_empty'    => [
                'path'   => $path_file_empty,
                'length' => 0,
            ],
        ];
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testGetData(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $this->createDir($source_file, uniqid('get_data_'));

        $content           = 'not empty content';
        $file_path_content = $directory->withFilePath('test_content.txt');
        $source_file->setData($content);
        $client->send($file_path_content);

        $this->assertEquals($content, $client->getData($file_path_content));

        $file_path_empty = $directory->withFilePath('test_content.txt');
        $source_file->setData('');
        $client->send($file_path_content);

        $this->assertEquals('', $client->getData($file_path_empty));
    }

    /**
     * @param CSourceFile $source_file
     * @param string      $dir_name
     *
     * @return ObjectPath
     * @throws CMbException
     */
    private function createDir(CSourceFile $source_file, string $dir_name): ObjectPath
    {
        $client    = $source_file->getClient();
        $directory = $source_file->completePath($dir_name)->toDirectory();
        if (false === $client->createDirectory($directory)) {
            throw new CMbException("Invalid create directory failed ($directory)");
        }

        return $directory;
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testGetDataWithFileNotExists(CSourceFile $source_file): void
    {
        $client = $source_file->getClient();

        $this->expectException(CMbException::class);
        $client->getData($source_file->completePath("file_which_does_not_exists.txt"));
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testListFiles(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $this->setupEnvListDir($source_file);

        $files = $client->getListFiles($directory);
        $this->assertCount(6, $files);

        $this->assertNotContains('.', $files);
        $this->assertNotContains('..', $files);
        $this->assertNotContains("$directory/.", $files);
        $this->assertNotContains("$directory/..", $files);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testReceive(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $this->setupEnvListDir($source_file);

        $files = $client->receive($directory);
        $this->assertCount(6, $files);
        $this->assertNotContains('.', $files);
        $this->assertNotContains('..', $files);
        $this->assertNotContains("$directory.", $files);
        $this->assertNotContains("$directory..", $files);

        foreach ($files as $file) {
            $this->assertStringContainsString("$directory", $file);
        }
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testListFilesDetails(CSourceFile $source_file): void
    {
        $client = $source_file->getClient();
        $directory = $this->setupEnvListDir($source_file);
        $files = $client->getListFilesDetails($directory);

        $count_dir = 0;
        $count_files = 0;
        foreach ($files as $file_infos) {
            $this->assertArrayHasKey("path", $file_infos);
            $this->assertArrayHasKey("type", $file_infos);
            $this->assertArrayHasKey("size", $file_infos);
            $this->assertArrayHasKey("date", $file_infos);
            $this->assertArrayHasKey("name", $file_infos);
            $this->assertArrayHasKey("relativeDate", $file_infos);

            // type
            if ($file_infos['type'] === 'd') {
                $count_dir++;
            } elseif ($file_infos['type'] === 'f') {
                $count_files++;
            }

            // date
            $this->assertMatchesRegularExpression("/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/", $file_infos['date']);

            // path
            $this->assertEquals($directory->withFilePath($file_infos['name']), $file_infos['path']);
        }

        $this->assertEquals(0, $count_dir);
        $this->assertEquals(6, $count_files);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testGetResponseTime(CSourceFile $source_file): void
    {
        $this->assertGreaterThanOrEqual(0, $source_file->getClient()->getResponseTime());
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testGetDirectoryExists(CSourceFile $source_file): void
    {
        $client = $source_file->getClient();

        $this->assertNotEmpty($client->getDirectory($source_file->completePath(".")));
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testGetDirectoryNotExists(CSourceFile $source_file): void
    {
        $client = $source_file->getClient();

        $this->expectException(CMbException::class);
        $client->getDirectory($source_file->completePath("directory_which_not_exits"));
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testAddFile(CSourceFile $source_file): void
    {
        $directory = $this->createDir($source_file, uniqid('add_file_'));
        $client    = $source_file->getClient();

        $path_source_file = tempnam(CAppUI::getTmpPath('.'), "test_add_file_");
        file_put_contents($path_source_file, 'ok');

        $path_file_dest = $directory->withFilePath("copy.txt");
        $this->assertTrue($client->addFile($path_source_file, $path_file_dest));
        $this->assertEquals('ok', $client->getData($path_file_dest));
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testAddFileWhichNotExits(CSourceFile $source_file): void
    {
        $client         = $source_file->getClient();
        $path_file_dest = $source_file->completePath("copy.txt");

        $this->expectException(CMbException::class);
        $client->addFile(CAppUI::getTmpPath('.') . '/file_which_not_exists.txt', $path_file_dest);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testDelFile(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $this->createDir($source_file, uniqid('delete_file_'));

        $source_file->setData('ok');
        $file_path = $directory->withFilePath("test.txt");
        $client->send($file_path);

        $this->assertNotEmpty($client->getData($file_path));

        $this->assertTrue($client->delFile($file_path));

        $this->expectException(CMbException::class);
        $client->getData($file_path);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testDelFileNotExists(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $file_path = $source_file->completePath("file_which_not_exists.txt");

        $this->expectException(CMbException::class);
        $client->delFile($file_path);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testRemoveDirectory(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $this->createDir($source_file, uniqid('remove_dir_'));

        $this->assertNotEmpty($client->getDirectory($directory));

        $this->assertTrue($client->removeDirectory($directory));

        $this->expectException(CMbException::class);
        $client->getDirectory($directory);
    }

    /**
     * @param CSourceFile $source_file
     * @param string      $dir_name
     *
     * @return ObjectPath
     * @throws CMbException
     */
    private function setupEnvRemoveDirWithSubDir(CSourceFile $source_file, string $dir_name): ObjectPath
    {
        $directory = $this->createDir($source_file, $dir_name);
        $this->createDir($source_file, "$dir_name/sub_dir");

        return $directory;
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testRemoveDirectoryNotEmpty(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $this->setupEnvRemoveDirWithSubDir($source_file, uniqid('remove_dir_not_empty'));

        $this->expectException(CMbException::class);
        $client->removeDirectory($directory);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testRemoveDirectoryNotExists(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $source_file->completePath('dir_which_not_exists')->toDirectory();

        $this->expectException(CMbException::class);
        $client->removeDirectory($directory);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testPurgeDirectory(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $this->setupEnvRemoveDirWithSubDir($source_file, uniqid('purge_dir'));

        $this->assertTrue($client->removeDirectory($directory, true));

        $this->expectException(CMbException::class);
        $client->getDirectory($directory);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     * @throws CMbException
     */
    public function testPurgeDirectoryWhichNotExists(CSourceFile $source_file): void
    {
        $client    = $source_file->getClient();
        $directory = $source_file->completePath(uniqid('purge_dir_not_exits'))->toDirectory();

        $this->expectException(CMbException::class);
        $client->removeDirectory($directory, true);
    }

    /**
     * @dataProvider providerSources
     *
     * @param CSourceFile $source_file
     *
     * @return void
     */
    public function testSendWithGenerateNameExchangesource(CSourceFile $source_file): void
    {
        $exchange      = new CExchangeDataFormat();
        $exchange->_id = 999;
        $source_file->setData("ok", false, $exchange);

        $this->assertStringEndsWith("-{$exchange->_id}", $source_file->generateFileName());
    }
}
