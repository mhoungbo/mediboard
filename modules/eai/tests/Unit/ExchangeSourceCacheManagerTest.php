<?php

/**
 * @package Interop\Eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai;

use Ox\Interop\Webservices\CSourceSOAP;
use Ox\Mediboard\System\CSourceFileSystem;
use Ox\Mediboard\System\CSourceHTTP;
use Ox\Tests\OxUnitTestCase;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Manage the cache for exchange sources
 */
class ExchangeSourceCacheManagerTest extends OxUnitTestCase
{
    public function testSetSource(): void
    {
        $source = new CSourceSOAP();
        $source->name = uniqid();
        $source->host = $expected_host = 'test host soap';

        $key = uniqid();
        $cache_manager = new ExchangeSourceCacheManager();
        $cache_manager->setSource($source, $key);

        $source_actual = $cache_manager->getSource($key);

        $this->assertEquals($expected_host, $source_actual->host);
    }

    public function testSetSourceWithoutName(): void
    {
        $source = new CSourceSOAP();
        $source->host = 'test host soap';

        $key = uniqid();
        $cache_manager = new ExchangeSourceCacheManager();
        $cache_manager->setSource($source, $key);

        $this->assertNull($cache_manager->getSource($key));
    }

    /**
     * @return void
     * @throws InvalidArgumentException
     */
    public function testInvalidateCacheForDifferentSources(): void
    {
        $same_name_source = uniqid();

        $source_soap = new CSourceSOAP();
        $source_soap->name = $same_name_source;
        $source_soap->host = 'test host soap';

        $source_http = new CSourceHTTP();
        $source_http->name = $same_name_source;
        $source_http->host = 'test host http';

        $key_http = uniqid();
        $key_soap = uniqid();
        $cache_manager = new ExchangeSourceCacheManager();
        $cache_manager->setSource($source_http, $key_http);
        $cache_manager->setSource($source_soap, $key_soap);

        $this->assertNotNull($cache_manager->getSource($key_http));
        $this->assertNotNull($cache_manager->getSource($key_soap));

        $cache_manager->invalidateCache($source_http);

        // same name ==> cache invalidate by name of source
        $this->assertNull($cache_manager->getSource($key_http));
        $this->assertNull($cache_manager->getSource($key_soap));
    }

    public function testInvalidateAllSources(): void
    {
        $key_1 = uniqid('soap');
        $key_2 = uniqid('soap_empty');
        $cache_manager = new ExchangeSourceCacheManager();
        $cache_manager->setSource(new CSourceSOAP(), $key_1);
        $cache_manager->setSource(new CSourceSOAP(), $key_2);

        $key_3 = uniqid('files');
        $source_files = new CSourceFileSystem();
        $source_files->_guid = 'CSourceFileSystem-4';
        $cache_manager->setSource($source_files, $key_3);

        $cache_manager->invalidateCache();

        $this->assertNull($cache_manager->getSource($key_1));
        $this->assertNull($cache_manager->getSource($key_2));
        $this->assertNull($cache_manager->getSource($key_3));
    }

    public function testInvalidateOnlySourceConcernedByName(): void
    {
        $cache_manager = new ExchangeSourceCacheManager();

        // one source with two keys
        $source_soap = new CSourceSOAP();
        $source_soap->name = uniqid();
        $key_1 = uniqid('soap');
        $key_2 = uniqid('soap_2');
        $cache_manager->setSource($source_soap, $key_1);
        $cache_manager->setSource($source_soap, $key_2);

        // source without guid
        $source_soap_empty = new CSourceSOAP();
        $source_soap_empty->name = uniqid();
        $key_3 = uniqid('empty_source');
        $cache_manager->setSource($source_soap_empty, $key_3);

        // other source
        $key_4 = uniqid('files');
        $source_files = new CSourceFileSystem();
        $source_files->name = uniqid();
        $cache_manager->setSource($source_files, $key_4);


        $cache_manager->invalidateCache($source_soap);

        // all key of $source_soap (and empty sources)
        $this->assertNull($cache_manager->getSource($key_1));
        $this->assertNull($cache_manager->getSource($key_2));

        $this->assertNotNull($cache_manager->getSource($key_3));
        $this->assertNotNull($cache_manager->getSource($key_4));
    }
}
