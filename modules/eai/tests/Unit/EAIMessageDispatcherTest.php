<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Unit;

use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Interop\Eai\CExchangeDataFormat;
use Ox\Interop\Eai\CInteropReceiver;
use Ox\Interop\Eai\EAIMessageDispatcher;
use Ox\Interop\Eai\ExchangeDataFormatFactory;
use Ox\Interop\Hl7\CReceiverHL7v2;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

class EAIMessageDispatcherTest extends OxUnitTestCase
{
    private static EAIMessageDispatcher $message_dispatcher;
    private static CInteropReceiver     $receiver;

    private const PRODUCTION_DATETIME_WHEN_EXCEPTION = '2022-05-01 00:00:00';

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$message_dispatcher = new EAIMessageDispatcher(new ExchangeDataFormatFactory());
        self::$receiver           = new CReceiverHL7v2();
        self::$receiver->_guid    = 'CReceiverHL7v2-1';
    }

    /**
     * @return void
     * @throws CMbException
     * @throws InvalidArgumentException
     * @throws ReflectionException
     * @throws TestsException
     */
    public function testUsageOfCacheWhenNoMessageTreated(): void
    {
        $receiver   = self::$receiver;
        $dispatcher = $this->getMockBuilder(EAIMessageDispatcher::class)
            ->setConstructorArgs([new ExchangeDataFormatFactory()])
            ->onlyMethods(['getMessagesForReceiver'])
            ->getMock();
        $dispatcher->method('getMessagesForReceiver')->willReturn([]);

        $this->resetCacheDatetime($receiver);
        $this->assertNull($dispatcher->getLastDateMessageProcessed($receiver));

        $dispatcher->sendMessagesFor($receiver, []);

        $date_in_cache = $dispatcher->getLastDateMessageProcessed($receiver);
        $this->assertGreaterThanOrEqual(CMbDT::dateTime('-2 MINUTES'), $date_in_cache);
        $this->assertLessThanOrEqual(CMbDT::dateTime('+2 MINUTES'), $date_in_cache);

        $this->resetCacheDatetime($receiver);
    }

    /**
     * @return void
     * @throws CMbException
     * @throws TestsException
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function testUpdateDateInCacheWhenNoMessageToTreatAndDateGiven(): void
    {
        $receiver = self::$receiver;

        $dispatcher = $this->getMockBuilder(EAIMessageDispatcher::class)
            ->setConstructorArgs([new ExchangeDataFormatFactory()])
            ->onlyMethods(['getMessagesForReceiver'])
            ->getMock();
        $dispatcher->method('getMessagesForReceiver')->willReturn([]);

        $this->resetCacheDatetime($receiver);
        $this->assertNull($dispatcher->getLastDateMessageProcessed($receiver));

        $dispatcher->sendMessagesFor($receiver, ['date_min' => CMbDT::dateTime()]);

        $this->assertNotNull($dispatcher->getLastDateMessageProcessed($receiver));
    }

    /**
     * @return void
     * @throws CMbException
     * @throws TestsException
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function testSendMessages(): void
    {
        $receiver        = new CReceiverHL7v2();
        $receiver->_guid = 'CReceiverHL7v2-1';

        $message_builder = $this->getMockBuilder(CExchangeDataFormat::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['send']);

        $message                  = $message_builder->getMock();
        $message->date_production = '2022-01-01 00:00:00';

        $last_message                  = $message_builder->getMock();
        $last_message->date_production = '2022-03-01 00:00:00';

        $dispatcher = $this->getMockBuilder(EAIMessageDispatcher::class)
            ->setConstructorArgs([new ExchangeDataFormatFactory()])
            ->onlyMethods(['getMessagesForReceiver'])
            ->getMock();
        $dispatcher->method('getMessagesForReceiver')->willReturn([$message, $last_message]);

        // treated elements
        $count_message_treated = $dispatcher->sendMessagesFor($receiver);
        $this->assertEquals(2, $count_message_treated);

        // setup cache
        $this->assertEquals($last_message->date_production, $dispatcher->getLastDateMessageProcessed($receiver));
    }

    /**
     * @throws InvalidArgumentException
     */
    public function testSendMessagesWithException(): void
    {
        $receiver = self::$receiver;

        $message_builder = $this->getMockBuilder(CExchangeDataFormat::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['send', 'store']);

        $message                  = $message_builder->getMock();
        $message->date_production = '2022-04-01 00:00:00';
        $message->method('send');

        $message_exception                  = $message_builder->getMock();
        $message_exception->date_production = self::PRODUCTION_DATETIME_WHEN_EXCEPTION;
        $message_exception->method('send')->willThrowException(new CMbException(''));

        $dispatcher = $this->getMockBuilder(EAIMessageDispatcher::class)
            ->setConstructorArgs([new ExchangeDataFormatFactory()])
            ->onlyMethods(['getMessagesForReceiver'])
            ->getMock();
        $dispatcher->method('getMessagesForReceiver')->willReturn([$message, $message_exception]);

        $message_exception->expects($this->once())->method('store');

        $this->expectException(CMbException::class);
        $dispatcher->sendMessagesFor($receiver);
    }

    /**
     * @depends testSendMessagesWithException
     * @return void
     * @throws InvalidArgumentException
     */
    public function testIsCorrectlySetWhenExceptionAppend(): void
    {
        $this->assertEquals(
            self::PRODUCTION_DATETIME_WHEN_EXCEPTION,
            self::$message_dispatcher->getLastDateMessageProcessed(
                self::$receiver
            )
        );
    }

    /**
     * Reset cache for EAIMessageDispatcher
     *
     * @param CInteropReceiver $receiver
     *
     * @return void
     * @throws ReflectionException
     * @throws TestsException
     */
    private function resetCacheDatetime(CInteropReceiver $receiver): void
    {
        $this->invokePrivateMethod(self::$message_dispatcher, 'updateLastTreatedDate', $receiver, null);
    }
}
