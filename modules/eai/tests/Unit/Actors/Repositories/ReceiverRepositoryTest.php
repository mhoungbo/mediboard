<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Unit\Actors\Repository;

use Ox\Core\Config\Conf;
use Ox\Core\Contracts\Client\ClientInterface;
use Ox\Core\CSQLDataSource;
use Ox\Interop\Eai\Actors\Repositories\ReceiverRepository;
use Ox\Interop\Eai\CInteropReceiver;
use Ox\Interop\Eai\CInteropReceiverFactory;
use Ox\Interop\Eai\ExchangeSources\Repository\ExchangeSourceRepository;
use Ox\Interop\Eai\Logger\Wrapper\InteropLoggerWrapper;
use Ox\Interop\Eai\Tests\Fixtures\Actors\ReceiverFixtures;
use Ox\Interop\Hl7\CReceiverHL7v2;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Tests\OxUnitTestCase;
use Psr\Log\LoggerInterface;

class ReceiverRepositoryTest extends OxUnitTestCase
{
    private static function getRepository(
        LoggerInterface          $logger = null,
        CInteropReceiverFactory  $receiver_factory = null,
        Conf                     $conf = null,
        ExchangeSourceRepository $exchange_source_repository = null,
        CSQLDataSource           $ds = null
    ): ReceiverRepository {
        return new ReceiverRepository(
            $logger ?: new InteropLoggerWrapper(),
            $receiver_factory ?: new CInteropReceiverFactory(),
            $conf ?: new Conf(),
            $exchange_source_repository ?: new ExchangeSourceRepository(),
            $ds ?: CSQLDataSource::get(('std'))
        );
    }

    public function testReceiverNotAccessible(): void
    {
        $receiver_repository = self::getRepository();
        $receiver_found      = $this->getReceiverWithSource(false);

        $receiver_repo = $this->getMockBuilder(CReceiverHL7v2::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadList'])
            ->getMock();
        $receiver_repo->method('loadList')->willReturn([$receiver_found]);
        $receiver_repo->_spec = (new CReceiverHL7v2())->_spec;

        $options   = [ReceiverRepository::ONLY_ACCESSIBLE => true];
        $receivers = $receiver_repository->getReceivers($receiver_repo, $options);

        $this->assertEmpty($receivers);
    }

    public function testReceiverAccessible(): void
    {
        $receiver_repository = self::getRepository();
        $receiver_found      = $this->getReceiverWithSource(true);

        $receiver_repo = $this->getMockBuilder(CReceiverHL7v2::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadList'])
            ->getMock();
        $receiver_repo->method('loadList')->willReturn([$receiver_found]);
        $receiver_repo->_spec = (new CReceiverHL7v2())->_spec;

        $options   = [ReceiverRepository::ONLY_ACCESSIBLE => true];
        $receivers = $receiver_repository->getReceivers($receiver_repo, $options);

        $this->assertNotEmpty($receivers);
    }

    private function getReceiverWithSource(bool $source_accessible): CInteropReceiver
    {
        $client_mocked = $this->getMockBuilder(ClientInterface::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isReachableSource', 'init', 'isAuthentificate', 'getResponseTime'])
            ->getMock();
        $client_mocked->method('isReachableSource')->willReturn($source_accessible);

        $source_mocked = $this->getMockBuilder(CExchangeSource::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getClient'])
            ->getMock();
        $source_mocked->method('getClient')->willReturn($client_mocked);
        $source_mocked->_id    = 1;
        $source_mocked->active = 1;

        $receiver = $this->getMockBuilder(CReceiverHL7v2::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['loadRefsExchangesSources'])
            ->getMock();
        $receiver->method('loadRefsExchangesSources')->willReturn([$source_mocked]);

        return $receiver;
    }

    public function testRetrieveReceiverWithSingleConfiguration(): void
    {
        $repository = self::getRepository();

        $expected_receiver = $this->getObjectFromFixturesReference(
            CReceiverHL7v2::class,
            ReceiverFixtures::RECEIVER_HL7V2_WITH_CONFIGURATION
        );

        $receivers = $repository->getReceivers(CReceiverHL7v2::class, [
            ReceiverRepository::ONLY_ACTIVE         => 1,
            ReceiverRepository::WITH_CONFIGURATIONS => ['eai CExchangeHL7v2-doctolib handle_doctolib' => '1']
        ]);

        self::assertArrayHasKey($expected_receiver->_id, $receivers);

        foreach ($receivers as $receiver) {
            $this->assertEquals(1, $receiver->confHL7v2('doctolib handle_doctolib'));
        }
    }

    public function testRetrieveReceiverForSpecificGroups(): void
    {
        $repository = self::getRepository();

        $expected_receiver = $this->getObjectFromFixturesReference(
            CReceiverHL7v2::class,
            ReceiverFixtures::RECEIVER_HL7V2_WITH_MULTIPLE_CONFIGURATIONS
        );

        $non_expected_receiver = $this->getObjectFromFixturesReference(
            CReceiverHL7v2::class,
            ReceiverFixtures::RECEIVER_HL7V2_WITH_CONFIGURATION
        );

        $receivers = $repository->getReceivers(CReceiverHL7v2::class, [
            ReceiverRepository::ONLY_ACTIVE         => 1,
            ReceiverRepository::WITH_CONFIGURATIONS => [
                'eai CExchangeHL7v2-doctolib handle_doctolib'       => '1',
                'eai CExchangeHL7v2-appFine handle_portail_patient' => '1'
            ]
        ]);

        self::assertArrayHasKey($expected_receiver->_id, $receivers);
        self::assertArrayNotHasKey($non_expected_receiver->_id, $receivers);

        foreach ($receivers as $receiver) {
            $this->assertEquals(1, $receiver->confHL7v2('doctolib handle_doctolib'));
            $this->assertEquals(1, $receiver->confHL7v2('appFine handle_portail_patient'));
        }
    }

    public function providerRetrieveReceiverWithMessageSupported(): array
    {
        return [
            'with message and transaction' => [['message' => 'CHL7EventORUR01', 'transaction' => 'PDC-01']],
            'with message and profil'      => [['message' => 'CHL7EventORUR01', 'profil' => 'CDEC']],
            'with full spec'               => [['message' => 'CHL7EventORUR01', 'transaction' => 'PDC-01', 'profil' => 'CDEC']]
        ];
    }

    /**
     * @dataProvider providerRetrieveReceiverWithMessageSupported
     * @param array $message_supported
     * @return void
     * @throws \Ox\Tests\TestsException
     */
    public function testRetrieveReceiverWithMessageSupported(array $message_supported): void
    {
        $repository = self::getRepository();

        $expected_receiver = $this->getObjectFromFixturesReference(
            CReceiverHL7v2::class,
            ReceiverFixtures::RECEIVER_HL7V2_WITH_MESSAGE_SUPPORTED
        );

        $receivers = $repository->getReceivers(CReceiverHL7v2::class, [
            ReceiverRepository::ONLY_ACTIVE            => 1,
            ReceiverRepository::WITH_MESSAGE_SUPPORTED => [$message_supported]
        ]);

        self::assertArrayHasKey($expected_receiver->_id, $receivers);

        foreach ($receivers as $receiver) {
            $has_message_supported = $receiver->isMessageSupported(
                $message_supported['message'] ?? null,
                $message_supported['profil'] ?? null,
                $message_supported['transaction'] ?? null
            );
            $this->assertTrue($has_message_supported);
        }
    }

    public function testRetrieveReceiverWithMultipleMessagesSupported(): void
    {
        $repository = self::getRepository();

        $expected_receiver = $this->getObjectFromFixturesReference(
            CReceiverHL7v2::class,
            ReceiverFixtures::RECEIVER_HL7V2_WITH_MULTIPLE_MESSAGES_SUPPORTED
        );

        $non_expected_receiver = $this->getObjectFromFixturesReference(
            CReceiverHL7v2::class,
            ReceiverFixtures::RECEIVER_HL7V2_WITH_MESSAGE_SUPPORTED
        );

        $receivers = $repository->getReceivers(CReceiverHL7v2::class, [
            ReceiverRepository::ONLY_ACTIVE         => 1,
            ReceiverRepository::WITH_MESSAGE_SUPPORTED => [
                ['message' => 'CHL7EventORUR01', 'transaction' => 'PDC-01', 'profil' => 'CDEC'],
                ['message' => 'CHL7EventORUR01', 'profil' => 'CHL7ORU']
            ]
        ]);

        self::assertArrayHasKey($expected_receiver->_id, $receivers);
        self::assertArrayNotHasKey($non_expected_receiver->_id, $receivers);

        foreach ($receivers as $receiver) {
            $this->assertTrue(
                $receiver->isMessageSupported(
                    'CHL7EventORUR01',
                    'CDEC',
                    'PDC-01'
                )
            );
            $this->assertTrue(
                $receiver->isMessageSupported(
                    'CHL7EventORUR01',
                    'CHL7ORU'
                )
            );
        }
    }
}
