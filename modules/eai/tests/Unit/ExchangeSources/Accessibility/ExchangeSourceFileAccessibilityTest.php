<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Unit\ExchangeSources\Accessibility;

use Exception;
use Ox\Core\Contracts\Client\FileClientInterface;
use Ox\Interop\Eai\ExchangeSources\Accessibility\ExchangeSourceFileAccessibility;
use Ox\Mediboard\System\Sources\CSourceFile;
use Ox\Mediboard\System\Sources\ObjectPath;
use Ox\Tests\OxUnitTestCase;

class ExchangeSourceFileAccessibilityTest extends OxUnitTestCase
{
    private const DIR_PATH = "tmp/accessibility";

    /**
     * @param FileClientInterface|null $client
     *
     * @return CSourceFile
     */
    private function getSourceMocked(FileClientInterface $client = null): CSourceFile
    {
        $source = $this->getMockBuilder(CSourceFile::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getClient', 'completePath'])
            ->getMock();

        $source->method('completePath')->willReturn(ObjectPath::fromPath(self::DIR_PATH)->toDirectory());

        if ($client === null) {
            $client = $this->createMock(FileClientInterface::class);
        }

        $source->method('getClient')->willReturn($client);

        return $source;
    }

    public function providerUnreachableSource(): array
    {
        $client_exception = $this->createMock(FileClientInterface::class);
        $client_exception->method('isReachableSource')->willThrowException(new Exception());

        return [
            "isReachable return false"    => [null],
            "isReachable throw exception" => [$client_exception],
        ];
    }

    /**
     * @dataProvider providerUnreachableSource
     *
     * @param FileClientInterface|null $client
     *
     * @return void
     * @throws Exception
     */
    public function testUnreachableSource(?FileClientInterface $client): void
    {
        $source = $this->getSourceMocked($client);

        $accessibility = new ExchangeSourceFileAccessibility();
        $result        = $accessibility->fullTest($source);

        $this->assertFalse($result[ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE]['status']);
    }

    public function providerNoAuthenticateSource(): array
    {
        $client = $this->createMock(FileClientInterface::class);
        $client->method('isReachableSource')->willReturn(true);

        $client_exception = clone $client;
        $client_exception->method('isAuthentificate')->willThrowException(new Exception());

        return [
            'isAuthenticate return false'    => [$client],
            'isAuthenticate throw exception' => [$client_exception],
        ];
    }

    /**
     * @dataProvider providerNoAuthenticateSource
     *
     * @param FileClientInterface $client
     *
     * @return void
     * @throws Exception
     */
    public function testNoAuthenticateSource(FileClientInterface $client): void
    {
        $source = $this->getSourceMocked($client);

        $accessibility = new ExchangeSourceFileAccessibility();
        $result        = $accessibility->fullTest($source);

        $this->assertTrue($result[ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE]['status']);
        $this->assertFalse($result[ExchangeSourceFileAccessibility::REPORT_SOURCE_AUTHENTICATE]['status']);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testUsageOfGivenPath(): void
    {
        $client = $this->createMock(FileClientInterface::class);
        $client->method('isReachableSource')->willReturn(true);
        $client->method('isAuthentificate')->willReturn(true);

        $source        = $this->getSourceMocked($client);
        $dir           = 'tmp/other_path';
        $accessibility = $this->getMockBuilder(ExchangeSourceFileAccessibility::class)
            ->onlyMethods(['isDirAccessible'])
            ->getMock();

        $accessibility->method('isDirAccessible')->willReturnCallback(function ($source, $path) use ($dir) {
            $this->assertEquals($dir, rtrim($path, '/'));

            return true;
        });

        $accessibility->fullTest($source, ObjectPath::fromPath($dir));
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testDireAccessible(): void
    {
        $client = $this->createMock(FileClientInterface::class);
        $client->method('isReachableSource')->willReturn(true);
        $client->method('isAuthentificate')->willReturn(true);
        $client->method('getListDirectory')->willReturn([]);
        $source = $this->getSourceMocked($client);

        $accessibility = new ExchangeSourceFileAccessibility();
        $result        = $accessibility->fullTest($source);

        $this->assertTrue($result[ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE]['status']);
        $this->assertFalse($result[ExchangeSourceFileAccessibility::REPORT_DIR_WRITABLE]['status']);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testDireNotAccessible(): void
    {
        $client = $this->createMock(FileClientInterface::class);
        $client->method('isReachableSource')->willReturn(true);
        $client->method('isAuthentificate')->willReturn(true);
        $client->method('getListDirectory')->willThrowException(new Exception());

        $source = $this->getSourceMocked($client);

        $accessibility = new ExchangeSourceFileAccessibility();
        $result        = $accessibility->fullTest($source);

        $this->assertFalse($result[ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE]['status']);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testWritableOk(): void
    {
        $client = $this->createMock(FileClientInterface::class);
        $client->method('isReachableSource')->willReturn(true);
        $client->method('isAuthentificate')->willReturn(true);
        $client->method('addFile')->willReturn(true);
        $client->method('delFile')->willReturn(true);
        $client->method('getData')->willReturn('ok');
        $source = $this->getSourceMocked($client);

        $accessibility = new ExchangeSourceFileAccessibility();
        $result        = $accessibility->fullTest($source);

        $this->assertTRUE($result[ExchangeSourceFileAccessibility::REPORT_DIR_WRITABLE]['status']);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testWritableNotOkWhenTestFailed(): void
    {
        $client = $this->createMock(FileClientInterface::class);
        $client->method('isReachableSource')->willReturn(true);
        $client->method('isAuthentificate')->willReturn(true);
        $client->method('addFile')->willReturn(true);
        $client->method('delFile')->willReturn(false);
        $client->method('getData')->willReturn('ok');
        $source = $this->getSourceMocked($client);

        $accessibility = new ExchangeSourceFileAccessibility();
        $result        = $accessibility->fullTest($source);

        $this->assertFalse($result[ExchangeSourceFileAccessibility::REPORT_DIR_WRITABLE]['status']);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testWritableFailedIfDifferent(): void
    {
        $client = $this->createMock(FileClientInterface::class);
        $client->method('isReachableSource')->willReturn(true);
        $client->method('isAuthentificate')->willReturn(true);
        $client->method('addFile')->willReturn(true);
        $client->method('delFile')->willReturn(false);
        $client->method('getData')->willReturn('not the good content');
        $source = $this->getSourceMocked($client);

        $accessibility = new ExchangeSourceFileAccessibility();
        $result        = $accessibility->fullTest($source);

        $this->assertFalse($result[ExchangeSourceFileAccessibility::REPORT_DIR_WRITABLE]['status']);
    }

    public function providerWritableFailedWhenException(): array
    {
        $client = $this->createMock(FileClientInterface::class);
        $client->method('isReachableSource')->willReturn(true);
        $client->method('isAuthentificate')->willReturn(true);

        $client_add_file_false = clone $client;
        $client_add_file_false->method('addFile')->willReturn(false);

        $client_exception_add_file = clone $client;
        $client_exception_add_file->method('addFile')->willThrowException(new Exception());

        $client->method('addFile')->willReturn(true);
        $client_get_data_null = clone $client;
        $client_get_data_null->method('getData')->willReturn(null);

        $client_get_data_exception = clone $client;
        $client_get_data_exception->method('getData')->willThrowException(new Exception());

        $client->method('getData')->willReturn('ok');
        $client_del_file_false = clone $client;
        $client_del_file_false->method('delFile')->willReturn(false);

        $client_del_file_exception = clone $client;
        $client_del_file_exception->method('delFile')->willThrowException(new Exception());

        return [
            "addFile return false"    => [$client_add_file_false],
            "addFile throw exception" => [$client_exception_add_file],
            "getData return null"     => [$client_get_data_null],
            "getData throw exception" => [$client_get_data_exception],
            "delFile return false"    => [$client_del_file_false],
            "delFile throw exception" => [$client_del_file_exception],
        ];
    }

    /**
     * @dataProvider providerWritableFailedWhenException
     *
     * @param FileClientInterface $client
     *
     * @return void
     * @throws Exception
     */
    public function testWritableFailedWhenException(FileClientInterface $client): void
    {
        $source = $this->getSourceMocked($client);

        $accessibility = new ExchangeSourceFileAccessibility();
        $result        = $accessibility->fullTest($source);

        $this->assertFalse($result[ExchangeSourceFileAccessibility::REPORT_DIR_WRITABLE]['status']);
    }
}
