<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Unit\ExchangeSources\Accessibility;

use Ox\Core\Locales\Translator;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Eai\ExchangeSources\Accessibility\AccessibilityReportMaker;
use Ox\Interop\Eai\ExchangeSources\Accessibility\ExchangeSourceFileAccessibility;
use Ox\Tests\OxUnitTestCase;

class AccessibilityReportMakerTest extends OxUnitTestCase
{
    public function testMakeReportFromEmptyContent(): void
    {
        $report_maker = new AccessibilityReportMaker(new Translator());
        $report       = $report_maker->makeReport([]);

        $this->assertCount(count($report->getItems()), $report->getItems(CItemReport::SEVERITY_ERROR));
    }

    public function testMakeReportWithAllItemsOk(): void
    {
        $data = [
            ExchangeSourceFileAccessibility::REPORT_SOURCE_ACCESSIBLE   => ['status' => true],
            ExchangeSourceFileAccessibility::REPORT_SOURCE_AUTHENTICATE => ['status' => true],
            ExchangeSourceFileAccessibility::REPORT_DIR_ACCESSIBLE      => ['status' => true],
            ExchangeSourceFileAccessibility::REPORT_DIR_WRITABLE        => ['status' => true],
            ExchangeSourceFileAccessibility::REPORT_FILE_ADD            => ['status' => true],
            ExchangeSourceFileAccessibility::REPORT_FILE_DELETE         => ['status' => true],
            ExchangeSourceFileAccessibility::REPORT_FILE_GET            => ['status' => true],
        ];

        $report_maker = new AccessibilityReportMaker(new Translator());
        $report       = $report_maker->makeReport($data);

        $this->assertCount(count($report->getItems()), $report->getItems(CItemReport::SEVERITY_SUCCESS));
    }
}
