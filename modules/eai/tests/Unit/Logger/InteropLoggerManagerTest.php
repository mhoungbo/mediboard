<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Unit\Logger;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\Logger\InteropLoggerManager;
use Ox\Interop\Eai\Logger\Wrapper\DebugInteropLoggerWrapper;
use Ox\Tests\OxUnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

class InteropLoggerManagerTest extends OxUnitTestCase
{
    /**
     * @return CInteropActor&MockObject
     */
    public function makeMockActor(array $functions): CInteropActor
    {
        $actor        = $this->createPartialMock(CInteropActor::class, $functions);
        $actor->_guid = 'CInteropActor-1';
        $actor->_id   = '1';
        $actor->setConf(new Conf());

        return $actor;
    }

    public function providerDontMakeAStopLogWhenOlDebugSessionNotStartedOrNotModified(): array
    {
        $functions            = ['getLogger', 'loadOldObject', 'fieldModified'];
        $actor                = $this->makeMockActor($functions);
        $actor->debug_session = CMbDT::dateTime("+1 DAYS");
        $actor->method('loadOldObject')->willReturn(new CInteropActor());
        $actor->method('fieldModified')->willReturn(true);

        $old_actor_with_debug                = new CInteropActor();
        $old_actor_with_debug->_id           = 1;
        $old_actor_with_debug->debug_session = CMbDT::dateTime();

        $second_actor                = $this->makeMockActor($functions);
        $second_actor->debug_session = CMbDT::dateTime("+1 DAYS");
        $second_actor->method('loadOldObject')->willReturn($old_actor_with_debug);
        $second_actor->method('fieldModified')->willReturn(false);

        return [
            'actor with a field modified, but it is  the first store'                  => [$actor],
            'actor with old debug session, but the current debug session not modified' => [$second_actor]
        ];
    }

    /**
     * @dataProvider providerDontMakeAStopLogWhenOlDebugSessionNotStartedOrNotModified
     * @param CInteropActor|MockObject $actor
     * @return void
     * @throws Exception
     */
    public function testDontMakeAStopLogWhenOlDebugSessionNotStartedOrNotModified(CInteropActor $actor): void
    {
        $logger = $this->createMock(DebugInteropLoggerWrapper::class);
        $actor->method('getLogger')->willReturn($logger);

        $logger->expects($this->never())->method('log');
        $manager = new InteropLoggerManager($actor);
        $manager->beforeActorStore();
    }

    public function testMakeAStopLogWhenNewSessionAndHasOldDebugSession(): void
    {
        $old_actor                = new CInteropActor();
        $old_actor->_id           = 1;
        $old_actor->debug_session = CMbDT::dateTime();

        $actor                = $this->makeMockActor(['getLogger', 'fieldModified']);
        $actor->debug_session = CMbDT::dateTime("+1 DAYS");
        $actor->_old          = $old_actor;
        $actor->method('fieldModified')->willReturn(true);
        $logger = $this->createMock(LoggerInterface::class);

        $uuid = $old_actor->getSessionLogUUID();
        $logger->method('log')->willReturnCallback(function ($level, $message, array $context = []) use ($uuid): void {
            $this->assertStringEndsWith("] stop log", $message);
            $this->assertArrayHasKey('actor_log_session_uuid', $context);
            $this->assertArrayHasKey('actor_log_guid', $context);
            $this->assertEquals($uuid, $context['actor_log_session_uuid']);
        });

        $debug_wrapper = new DebugInteropLoggerWrapper($actor, $logger);
        $actor->method('getLogger')->willReturn($debug_wrapper);

        $manager = new InteropLoggerManager($actor);
        $manager->beforeActorStore();
    }

    public function testMakeAStartLogWhenDebugSessionChanged(): void
    {
        $actor                = $this->makeMockActor(['getLogger', 'fieldModified']);
        $actor->debug_session = CMbDT::dateTime("+1 DAYS");
        $actor->_old          = new CInteropActor();
        $actor->method('fieldModified')->willReturn(true);

        $logger = $this->createMock(LoggerInterface::class);
        $uuid = $actor->getSessionLogUUID();
        $logger->method('log')->willReturnCallback(function ($level, $message, array $context = []) use ($uuid): void {
            $this->assertStringEndsWith("] start log", $message);
            $this->assertArrayHasKey('actor_log_session_uuid', $context);
            $this->assertArrayHasKey('actor_log_guid', $context);
            $this->assertEquals($uuid, $context['actor_log_session_uuid']);
        });

        $debug_wrapper = new DebugInteropLoggerWrapper($actor, $logger);
        $actor->method('getLogger')->willReturn($debug_wrapper);

        $manager = new InteropLoggerManager($actor);
        $manager->beforeActorStore();
        $manager->afterActorStore();
    }

    public function providerDoNotMakeStartLogWhenDebugIsModifiedOrNull(): array
    {
        $actor                = $this->makeMockActor(['getLogger', 'fieldModified']);
        $actor->debug_session = CMbDT::dateTime("+1 DAYS");
        $actor->_old          = new CInteropActor();
        $actor->method('fieldModified')->willReturn(false);

        $second_actor                = $this->makeMockActor(['getLogger', 'fieldModified']);
        $second_actor->debug_session = "";
        $second_actor->_old          = new CInteropActor();
        $second_actor->method('fieldModified')->willReturn(true);

        return [
            'actor with field not changing'        => [$actor],
            'actor with field empty debug session' => [$second_actor],
        ];
    }

    /**
     * @dataProvider providerDoNotMakeStartLogWhenDebugIsModifiedOrNull
     * @param CInteropActor&MockObject $actor
     * @return void
     * @throws Exception
     */
    public function testDoNotMakeStartLogWhenDebugIsModifiedOrNull(CInteropActor $actor): void
    {
        $logger = $this->createPartialMock(DebugInteropLoggerWrapper::class, ['log']);
        $actor->method('getLogger')->willReturn($logger);

        $logger->expects($this->never())->method('log');

        $manager = new InteropLoggerManager($actor);
        $manager->beforeActorStore();
        $manager->afterActorStore();
    }

    public function testStopLoggerIfSessionIsStarted(): void
    {
        $old_actor = new CInteropActor();
        $old_actor->_id = 1;
        $old_actor->debug_session = CMbDT::dateTime();

        $actor                = $this->makeMockActor(['getLogger', 'fieldModified']);
        $actor->debug_session = CMbDT::dateTime("+1 DAYS");
        $actor->_old          = $old_actor;
        $actor->method('fieldModified')->willReturn(true);

        $logger = $this->createMock(DebugInteropLoggerWrapper::class);
        $logger->method('isStarted')->willReturn(true);
        $actor->method('getLogger')->willReturn($logger);

        $logger->expects($this->once())->method('stopSession');

        $manager = new InteropLoggerManager($actor);
        $manager->beforeActorStore();
    }
}
