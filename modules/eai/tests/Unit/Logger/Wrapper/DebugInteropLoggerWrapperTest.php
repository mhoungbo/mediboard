<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Unit\Logger\Wrapper;

use Ox\Core\CMbDT;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\Logger\Wrapper\DebugInteropLoggerWrapper;
use Ox\Tests\OxUnitTestCase;
use Psr\Log\LoggerInterface;

class DebugInteropLoggerWrapperTest extends OxUnitTestCase
{
    public function testLifeCycleOfLoggerSession(): void
    {
        $psr_logger = $this->createMock(LoggerInterface::class);

        $actor = new CInteropActor();
        $actor->debug_session = CMbDT::dateTime();
        $actor->_guid = 'CInteropActor-1';

        $psr_logger->expects($this->once())->method('log');

        $logger = new DebugInteropLoggerWrapper($actor, $psr_logger);
        $logger->startSession();
        $logger->debug("1");
        $logger->debug("2");
        $logger->debug("3");
        $logger->debug("4");
        $logger->stopSession();
    }

    public function testLogWhenDebugSessionNotStarted(): void
    {
        $psr_logger = $this->createMock(LoggerInterface::class);

        $actor = new CInteropActor();
        $actor->debug_session = CMbDT::dateTime();
        $actor->_guid = 'CInteropActor-1';

        $psr_logger->expects($this->exactly(4))->method('log');

        $logger = new DebugInteropLoggerWrapper($actor, $psr_logger);
        $logger->debug("1");
        $logger->debug("2");
        $logger->debug("3");
        $logger->debug("4");
    }
}
