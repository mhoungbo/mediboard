<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Unit\Logger\Formatters;

use Ox\Core\CMbDT;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Eai\Logger\Events\EAIMessageHandleEvent;
use Ox\Interop\Eai\Logger\Formatters\InteropLoggerFormatter;
use Ox\Tests\OxUnitTestCase;
use stdClass;

class InteropLoggerFormatterTest extends OxUnitTestCase
{
    private function getActor(): CInteropActor
    {
        $actor                = new CInteropActor();
        $actor->_id           = 1;
        $actor->debug_session = CMbDT::dateTime();

        return $actor;
    }

    public function testWithObjectNonStringable(): void
    {
        $formatter = new InteropLoggerFormatter($this->getActor());
        [$msg, $context] = $formatter->format([[null, new stdClass(), null]]);
        $this->assertArrayHasKey('actor_log_message_uuid', $context);
        $this->assertCount(1, $context);
    }

    public function testWithString(): void
    {
        $actor     = $this->getActor();
        $formatter = new InteropLoggerFormatter($actor);
        [$msg, $context] = $formatter->format([[null, "with a content string", null]]);

        $this->assertArrayHasKey('extraContext', $context);
        $this->assertCount(1, $context['extraContext']);
    }

    public function testWithObjectEAIEventLogger(): void
    {
        $actor     = $this->getActor();
        $formatter = new InteropLoggerFormatter($actor);
        [$msg, $context] = $formatter->format(
            [[null, (new EAIMessageHandleEvent($actor))->setMessageDecoded(true), null]]
        );
        $this->assertArrayHasKey('EAIMessageHandleEvent', $context);

        $first_event_data = reset($context['EAIMessageHandleEvent']);
        $this->assertArrayHasKey('message_decoded', $first_event_data);
    }

    public function testWithStringAndData(): void
    {
        $formatter  = new InteropLoggerFormatter($this->getActor());
        $expect_key = 'a content key';
        [$msg, $context] = $formatter->format([[null, $expect_key, ['foo' => 'bar']]]);

        $this->assertArrayHasKey('extraContext', $context);
        $this->assertArrayHasKey($expect_key, $context['extraContext']);
        $this->assertArrayHasKey('foo', $context['extraContext'][$expect_key]);
    }
}
