<?php
/**
 * @package Core\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Unit\Configurations;

use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbException;
use Ox\Interop\Eai\CConfigurationEai;
use Ox\Interop\Eai\CInteropSender;
use Ox\Interop\Eai\ConfigurationActorInterface;
use Ox\Interop\Eai\Controllers\Legacy\CActorControllerLegacy;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\CConfiguration;
use Ox\Tests\OxUnitTestCase;

/**
 * Class CAppTest
 * @package Ox\Core\Tests\Unit
 */
class CEAIConfigurationTest extends OxUnitTestCase
{
    /**
     * @return array
     * @throws \Exception
     */
    public function providerConfigurationActorStartWithPrefix(): array
    {
        /** @var ConfigurationActorInterface[] $config_classes */
        $config_classes = CClassMap::getInstance()->getClassChildren(ConfigurationActorInterface::class, true, true);

        $data = [];
        foreach ($config_classes as $config_class) {
            $prefix = $config_class->getPrefixSectionActor();

            $configs = $config_class->getConfigurationsActor();
            foreach ($configs as $actor_class => $section_configs) {
                foreach ($section_configs as $section_name => $values) {
                    $data[get_class($config_class) . ' ' . $actor_class . ' ' . $section_name] = [
                        'config' => $section_name,
                        'prefixes' => $prefix,
                    ];
                }
            }
        }

        return $data;
    }

    /**
     * Each section for configurations actor should be prefixed by his format.
     *
     * @dataProvider providerConfigurationActorStartWithPrefix
     *
     * @param string $config
     * @param string $suffix
     *
     * @return void
     */
    public function testConfigurationActorEndWithSuffix(string $config, array $prefixes): void
    {
        $prefix = implode('|', $prefixes);
        $pattern = sprintf(ConfigurationActorInterface::REGEX_MATCH_PREFIX, $prefix);

        $this->assertTrue(
            preg_match("/$pattern/", $config) === 1,
            "The section of configuration [$config] for eai actor should start with the prefix : $prefix"
        );
    }
}
