<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Eai\Tests\Classes;

use Ox\Core\Contracts\Client\FileClientInterface;
use Ox\Core\Contracts\Client\FileSystemClientInterface;
use Ox\Core\Contracts\Client\FTPClientInterface;
use Ox\Core\Contracts\Client\SFTPClientInterface;
use Ox\Interop\Ftp\CSourceFTP;
use Ox\Interop\Ftp\CSourceSFTP;
use Ox\Mediboard\System\CSourceFileSystem;
use Ox\Mediboard\System\Sources\CSourceFile;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker as BuilderInvocationMocker;

trait SourceFileTestTrait
{
    /**
     * Source will return Client configured to simulate successful calls
     *
     * @param CSourceFile|null $source_file
     * @param string|null      $function
     * @param                  $response
     * @method BuilderInvocationMocker method($constraint)
     *
     * @return CSourceFile
     */
    public function mockSourceFile(
        array        $override_mapping = [],
        ?CSourceFile $source_file = null
    ): CSourceFile {
        $client_interface = FileClientInterface::class;
        if ($source_file instanceof CSourceFTP) {
            $client_interface = FTPClientInterface::class;
        } elseif ($source_file instanceof CSourceSFTP) {
            $client_interface = SFTPClientInterface::class;
        } elseif ($source_file instanceof CSourceFileSystem) {
            $client_interface = FileSystemClientInterface::class;
        }

        $mapping = [
            'willReturn'         => [
                'isReachableSource'   => true,
                'isAuthentificate'    => true,
                'getListFiles'        => [],
                'getListDirectory'    => [],
                'getListFilesDetails' => [],
                'getData'             => '',
                'removeDirectory'     => true,
                'addFile'             => true,
                'delFile'             => true,
                'send'                => true,
                'renameFile'          => true,
                'createDirectory'     => true,
                'getSize'             => 0,
                'receive'             => [],
                'getResponseTime'     => 1,
            ],
            'willReturnArgument' => [
                'getDirectory' => 0,
            ],
        ];

        $client = $this->createMock($client_interface);

        if ($override_mapping) {
            if (!is_int(array_key_first($override_mapping))) {
                $override_mapping = [$override_mapping];
            }

            foreach ($override_mapping as $data) {
                $func_mock = $data['func_mock'] ?? 'willReturn';
                $func      = $data['func'] ?? null;
                if (!$func) {
                    continue;
                }

                $mapping[$func_mock][$func] = $data['val'] ?? null;
            }
        }

        foreach ($mapping as $func_mock => $data) {
            foreach ($data as $function => $value) {
                $client->method($function)->$func_mock($value);
            }
        }

        if (!$source_file) {
            $source_file = $this->createMock(CSourceFile::class);
        }

        $source_file->method('getClient')->willReturn($client);
        $source_file->method('disableResilience')->willReturn($client);
        $source_file->method('reactivateResilience')->willReturn($client);

        return $source_file;
    }
}
