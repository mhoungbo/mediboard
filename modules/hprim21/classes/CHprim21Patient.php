<?php
/**
 * @package Mediboard\Hprim21
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hprim21;

use Ox\Core\CMbArray;
use Ox\Core\CSoundex2;
use Ox\Mediboard\Patients\CPatient;

/**
 * The HPRIM 2.1 patient class
 */
class CHprim21Patient extends CHprim21Object {
  // DB Table key
  public $hprim21_patient_id;
  
  // DB references
  public $patient_id;
  
  // Patient DB Fields
  public $nom;
  public $prenom;
  public $civilite;
  public $nom_jeune_fille;
  public $nom_soundex2;
  public $prenom_soundex2;
  public $nomjf_soundex2;
  public $naissance;
  public $sexe;
  public $adresse1;
  public $ville;
  public $cp;
  public $telephone1;
  public $traitement_local1;
  public $taille;
  public $poids;
  public $traitement;
  public $situation_maritale;
  public $date_derniere_modif;
  public $date_deces;
  
  // Assur� primaire DB Fields
  public $nature_assurance;
  public $rang_beneficiaire;
  public $rang_naissance;
  public $code_regime;
  public $caisse_gest;
  public $centre_gest;
  public $nom_assure;
  public $prenom_assure;
  public $nom_jeune_fille_assure;
  public $_ref_patient;
  public $_ref_hprim21_sejours;

  /**
   * @see parent::getSpec()
   */
  function getSpec() {
    $spec = parent::getSpec();
    $spec->table = 'hprim21_patient';
    $spec->key   = 'hprim21_patient_id';
    return $spec;
  }

  /**
   * @see parent::getProps()
   */
  function getProps() {
    $specs = parent::getProps();

    $specs["patient_id"]                = "ref class|CPatient back|hprim21_patients";
    $specs["nom"]                       = "str notNull seekable";
    $specs["prenom"]                    = "str seekable";
    $specs["civilite"]                  = "enum list|M|Mme|Mlle";
    $specs["nom_jeune_fille"]           = "str";
    $specs["nom_soundex2"]              = "str";
    $specs["prenom_soundex2"]           = "str";
    $specs["nomjf_soundex2"]            = "str";
    $specs["naissance"]                 = "birthDate";
    $specs["sexe"]                      = "enum list|M|F|U";
    $specs["adresse1"]                  = "str";
    $specs["ville"]                     = "str";
    $specs["cp"]                        = "str";
    $specs["telephone1"]                = "str";
    $specs["traitement_local1"]         = "str";
    $specs["taille"]                    = "num";
    $specs["poids"]                     = "num";
    $specs["traitement"]                = "str";
    $specs["situation_maritale"]        = "enum list|M|S|D|W|A|U";
    $specs["date_derniere_modif"]       = "dateTime";
    $specs["date_deces"]                = "date";
    // Assur� primaire
    $specs["nature_assurance"]       = "str";
    $specs["rang_beneficiaire"]      = "enum list|01|02|09|11|12|13|14|15|16|31";
    $specs["rang_naissance"]         = "enum list|1|2|3|4|5|6 default|1";
    $specs["code_regime"]            = "numchar maxLength|3";
    $specs["caisse_gest"]            = "numchar length|3";
    $specs["centre_gest"]            = "numchar length|4";
    $specs["nom_assure"]             = "str";
    $specs["prenom_assure"]          = "str";
    $specs["nom_jeune_fille_assure"] = "str";
    $specs["echange_hprim21_id"]    .= " back|patients_hprim21";

    return $specs;
  }

  /**
   * @see parent::updatePlainFields()
   */
  function updatePlainFields() {
    
    parent::updatePlainFields();
     
    $soundex2 = new CSoundex2;
    if ($this->nom) {
      $this->nom = strtoupper($this->nom);
      $this->nom_soundex2 = $soundex2->build($this->nom);
    }
    
    if ($this->nom_jeune_fille) {
      $this->nom_jeune_fille = strtoupper($this->nom_jeune_fille);
      $this->nomjf_soundex2 = $soundex2->build($this->nom_jeune_fille);
    }

    if ($this->prenom) {
      $this->prenom = ucwords(strtolower($this->prenom));
      $this->prenom_soundex2 = $soundex2->build($this->prenom);
    }
  }

  /**
   * @inheritdoc
   */
  function bindToLine($line, CHPrim21Reader &$reader, CHprim21Object $object = null, CHprim21Medecin $medecin = null) {
    $this->setHprim21ReaderVars($reader);
    
    $elements = explode($reader->separateur_champ, $line);
  
    if (count($elements) < 26) {
      $reader->error_log[] = "Champs manquant dans le segment patient : ".count($elements)." champs trouv�s";
      return false;
    }

    $identifiant = explode($reader->separateur_sous_champ, $elements[2]);
    if (!$identifiant[0]) {
      $reader->error_log[] = "Identifiant externe manquant dans le segment patient";
      return false;
    }
    $this->external_id               = $identifiant[0];
    $this->loadMatchingObject();
    $identite                        = explode($reader->separateur_sous_champ, $elements[5]);
    $this->nom                       = CMbArray::get($identite, 0);
    $this->prenom                    = CMbArray::get($identite, 1);
    $this->civilite                  = CMbArray::get($identite, 4);
    $this->nom_jeune_fille           = $elements[6];
    $this->naissance                 = $this->getDateFromHprim($elements[7]);
    $this->sexe                      = $elements[8];
    $adresse                         = explode($reader->separateur_sous_champ, $elements[10]);
    $this->adresse1                  = $adresse[0];
    $this->adresse1                  = $adresse[1];
    $this->ville                     = $adresse[2];
    $this->cp                        = $adresse[4];
    $telephone                       = explode($reader->repetiteur, $elements[12]);
    $this->telephone1                = $telephone[0];
    $this->traitement_local1         = $elements[14];
    $this->taille                    = $elements[16];
    $this->poids                     = $elements[17];
    $this->traitement                = $elements[19];
    $this->situation_maritale        = CMbArray::get($elements, 28);
    $this->date_derniere_modif       = $this->getDateTimeFromHprim(CMbArray::get($elements, 32));
    $this->date_deces                = $this->getDateFromHprim(CMbArray::get($elements, 33));
    $reader->nb_patients++;
    return true;
  }
  
  function bindAssurePrimaireToLine($line, &$reader) {
    $elements = explode($reader->separateur_champ, $line);
  
    if (count($elements) < 22) {
      $this->error_log[] = "Champs manquant dans le segment assur� primaire";
      return false;
    }
    
    $this->nature_assurance       = $elements[2];
    $this->rang_beneficiaire      = $elements[5] == "00" ? "01" : $elements[5];
    $this->rang_naissance         = $elements[5] == "0"  ? "1"  : $elements[5];
    $this->code_regime            = $elements[8];
    $this->caisse_gest            = $elements[9];
    $this->centre_gest            = $elements[10];
    $identite                     = explode($reader->separateur_sous_champ, $elements[13]);
    $this->nom_assure             = CMbArray::get($identite, 0);
    $this->prenom_assure          = CMbArray::get($identite, 1);
    $this->nom_jeune_fille_assure = $elements[14];
    $adresse_employeur            = explode($reader->separateur_sous_champ, $elements[21]);
    // Attention, champs manquants chez Simemens (4 au lieu de 6)
    
    return true;
  }

  /**
   * @see parent::updateFormFields()
   */
  function updateFormFields() {
    parent::updateFormFields();
    $this->_view = "$this->civilite $this->nom $this->prenom [$this->external_id]";
  }

  /**
   * @see parent::loadRefsFwd()
   */
  function loadRefsFwd(){
    // Chargement du patient correspondant
    $this->_ref_patient = new CPatient();
    $this->_ref_patient->load($this->patient_id);
  }
  
  function loadRefHprim21Sejours(){
    $sejour = new CHprim21Sejour();
    $where["hprim21_patient_id"] = "= '$this->_id'";
    $order = "date_mouvement";
    $this->_ref_hprim21_sejours = $sejour->loadList($where, $order);
  }

  /**
   * @see parent::loadRefsBack()
   */
  function loadRefsBack() {
    parent::loadRefsBack();
    $this->loadRefHprim21Sejours();
  }
}
