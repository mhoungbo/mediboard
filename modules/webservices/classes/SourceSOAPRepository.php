<?php

/**
 * @package Mediboard\eai
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Webservices;

use Ox\Interop\Eai\ExchangeSources\Exceptions\InvalidSourceException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNonActiveException;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceNotConfiguredException;
use Ox\Interop\Eai\ExchangeSources\Repository\ExchangeSourceRepository;
use Ox\Interop\Eai\ExchangeSources\Repository\SourceRepositoryInterface;

class SourceSOAPRepository implements SourceRepositoryInterface
{
    protected const DEFAULT_TYPES = CSourceSOAP::TYPE;

    public function __construct(private ExchangeSourceRepository $repository)
    {
    }

    /**
     * @inheritDoc
     *
     * @param string $name
     * @param array|string|null $types
     *
     * @return CSourceSOAP
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSource(string $name, array|string|null $types = ''): CSourceSOAP
    {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceSOAP $source */
        $source = $this->repository->getSource($name, $types);

        return $source;
    }

    /**
     * @inheritDoc
     *
     * @param string $name
     * @param array|string|null $types
     *
     * @return CSourceSOAP
     * @throws SourceNotConfiguredException
     */
    public function getTestableSource(string $name, array|string|null $types = ''): CSourceSOAP
    {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceSOAP $source */
        $source = $this->repository->getTestableSource($name, $types);

        return $source;
    }


    /**
     * @inheritDoc
     */
    public function getConfigurableSource(
        string            $name,
        array|string|null $types = '',
        string            $exchange_type = null
    ): CSourceSOAP
    {
        if ($types === '') {
            $types = self::DEFAULT_TYPES;
        }

        /** @var CSourceSOAP $source */
        $source = $this->repository->getConfigurableSource($name, $types, $exchange_type);

        return $source;
    }


    /**
     * @inheritDoc
     *
     * @param string $id
     * @param string $exchange_class
     * @param bool $only_active
     * @return CSourceSOAP
     * @throws InvalidSourceException
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSourceFromId(
        string $id,
        string $exchange_class = 'CSourceSOAP',
        bool   $only_active = true
    ): CSourceSOAP
    {
        return $this->getSourceFromGuid("$exchange_class-$id", $only_active);
    }

    /**
     * @inheritDoc
     *
     * @param string $guid
     * @param bool $only_active
     * @return CSourceSOAP
     * @throws InvalidSourceException
     * @throws SourceNonActiveException
     * @throws SourceNotConfiguredException
     */
    public function getSourceFromGuid(string $guid, bool $only_active = true): CSourceSOAP
    {
        [$class, $id] = explode('-', $guid, 2);
        if ($class !== "CSourceSOAP") {
            throw new InvalidSourceException($guid);
        }

        /** @var CSourceSOAP $source */
        $source = $this->repository->getSourceFromGuid($guid, $only_active);

        return $source;
    }
}
