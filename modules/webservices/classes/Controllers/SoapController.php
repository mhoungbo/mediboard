<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Webservices\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Eai\CReport;
use Ox\Interop\Eai\ExchangeSources\Exceptions\SourceException;
use Ox\Interop\Webservices\CSourceSOAP;
use Ox\Interop\Webservices\SourceSOAPRepository;
use Symfony\Component\HttpFoundation\Response;

class SoapController extends Controller
{
    /**
     * Try to connect to SOAP Source
     *
     * @param RequestParams        $request_params
     * @param SourceSOAPRepository $source_repository
     *
     * @return Response
     * @throws Exception
     */
    public function connexion(RequestParams $request_params, SourceSOAPRepository $source_repository): Response
    {
        // Check params
        $exchange_source_name = $request_params->get("exchange_source_name", "str notNull");

        $report = new CReport('Connexion source SOAP');
        try {
            $source = $source_repository->getTestableSource($exchange_source_name);
            $client = $source->disableResilience();

            // check reachable source
            if ($client->isReachableSource()) {
                $report->addData(
                    $this->translator->tr('CSourceSOAP-msg-reachable source'),
                    CItemReport::SEVERITY_SUCCESS
                );
            } else {
                $report->addData($this->translator->tr('CSourceSOAP-unreachable-source'), CItemReport::SEVERITY_ERROR);
            }

            // auth check service availability
            if ($client->isAuthentificate()) {
                $report->addData(
                    $this->translator->tr('CSourceSOAP-msg-authenticate source'),
                    CItemReport::SEVERITY_SUCCESS
                );
            } else {
                $report->addData(
                    $this->translator->tr('CSourceSOAP-msg-failed authenticate source'),
                    CItemReport::SEVERITY_ERROR
                );
            }
        } catch (SourceException|CMbException $exception) {
            $report->addData($exception->getMessage(), CItemReport::SEVERITY_ERROR);
        } finally {
            return $this->renderSmarty('report/inc_report', ['report' => $report], 'eai');
        }
    }

    /**
     * List all function for SOAP Source
     *
     * @param RequestParams        $request_params
     * @param SourceSOAPRepository $source_repository
     *
     * @return Response
     * @throws Exception
     */
    public function listFunctions(RequestParams $request_params, SourceSOAPRepository $source_repository): Response
    {
        // Check params
        $exchange_source_name = $request_params->get("exchange_source_name", "str notNull");

        try {
            $source = $source_repository->getTestableSource($exchange_source_name);
            $client = $source->disableResilience();

            // check reachable source
            if ($client->isReachableSource() === false) {
                $this->addUiMsgError('CSourceSOAP-unreachable-source');
            }

            // auth check service availability
            if ($client->isAuthentificate() === false) {
                $this->addUiMsgError('CSourceSOAP-msg-failed authenticate source');
            }
        } catch (SourceException|CMbException $exception) {
            $this->addUiMsgError($exception->getMessage());
        } finally {
            if (!isset($source)) {
                $source = new CSourceSOAP();
                $source->_reachable = 0;
            }

            return $this->renderSmarty(
                'inc_soap_functions',
                [
                    'exchange_source' => $source,
                    'functions'       => ($source->_reachable && isset($client)) ? $client->getFunctions() : [],
                    'types'           => ($source->_reachable && isset($client)) ? $client->getTypes() : [],
                    'form_name'       => $request_params->get('form_name', 'str notNull'),
                ]
            );
        }
    }

    /**
     * @param RequestParams        $request_params
     * @param SourceSOAPRepository $source_repository
     *
     * @return Response
     * @throws Exception
     */
    public function executeFunction(RequestParams $request_params, SourceSOAPRepository $source_repository): Response
    {
        $method               = $request_params->post("func", 'str');
        $exchange_source_guid = $request_params->post("exchange_source_guid", 'str');
        $parameters           = $request_params->post("parameters", 'str');

        if (
            !$this->isCsrfTokenValid(
                'webservices_gui_sources_function_execute',
                $request_params->post('token', 'str')
            )
        ) {
            throw new InvalidCsrfTokenException();
        }

        try {
            $source = $source_repository->getSourceFromGuid($exchange_source_guid, false);
            $source->setData($parameters);

            if (is_array($parameters) && (count($parameters) > 1) && $source->single_parameter) {
                $source->single_parameter = null;
            }

            $client = $source->disableResilience();
            $client->send($method);

            return $this->renderResponse($source->getACQ());
        } catch (SourceException|Exception $exception) {
            $this->addUiMsgError($exception->getMessage());

            return $this->renderEmptyResponse();
        }
    }
}
