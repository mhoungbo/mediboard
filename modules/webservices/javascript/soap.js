/**
 * @package Mediboard\Webservices
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

SOAP = {
    connexion: function (element, exchange_source_name) {
        const route = element.getAttribute('data-route-connexion')
        if (!route) {
            return;
        }

        new Url()
            .setRoute(route, 'webservices_gui_sources_connexion', 'webservices')
            .addParam("exchange_source_name", exchange_source_name)
            .requestModal(600);
    },

    getFunctions: function (element, exchange_source_name) {
        const route = element.getAttribute('data-route-functions')
        if (!route) {
            return;
        }

        new Url()
            .setRoute(route, 'webservices_gui_sources_list_functions', 'webservices')
            .addParam("form_name", element.form.getAttribute("name"))
            .addParam("exchange_source_name", exchange_source_name)
            .requestModal(600);
    },

    toggleDisabled: function (input_name, source_name) {
        var form = getForm("editSourceSOAP-" + source_name);
        var input = form.elements[input_name];
        input.disabled ? input.disabled = '' : input.disabled = 'disabled';
    }
};
