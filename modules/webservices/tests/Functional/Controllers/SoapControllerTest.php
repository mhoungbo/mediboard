<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hl7\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\SOAPClientInterface;
use Ox\Interop\Eai\ExchangeSources\Repository\ExchangeSourceRepository;
use Ox\Interop\Webservices\CSourceSOAP;
use Ox\Tests\OxWebTestCase;

class SoapControllerTest extends OxWebTestCase
{
    public function providerConnexionKO(): array
    {
        $mock_client = $this->createMock(SOAPClientInterface::class);
        $mock_client->method('isReachableSource')->willThrowException(new CMbException(""));

        $mock_client_connexion = $this->createMock(SOAPClientInterface::class);
        $mock_client_connexion->method('isReachableSource')->willReturn(false);
        $mock_client_connexion->method('isAuthentificate')->willReturn(false);

        $mock_client_auth = $this->createMock(SOAPClientInterface::class);
        $mock_client_auth->method('isReachableSource')->willReturn(true);
        $mock_client_auth->method('isAuthentificate')->willReturn(false);

        return [
            'with exception' => [$mock_client, 1],
            'with bad connnexion' => [$mock_client_connexion, 2],
            'with bad auth' => [$mock_client_auth, 1],
        ];
    }

    /**
     * @dataProvider providerConnexionKO
     *
     *
     * @param SOAPClientInterface $mock_client
     * @param int $expected_errors
     * @return void
     * @throws Exception
     */
    public function testConnexionKO(SOAPClientInterface $mock_client, int $expected_errors): void
    {
        $mock_source = $this->getMockBuilder(CSourceSOAP::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getClient'])
            ->getMock();
        $mock_source->method('getClient')->willReturn($mock_client);

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
            ->onlyMethods(['getTestableSource'])
            ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($mock_source);

        $client = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/webservices/sources/connexion",
            [
                "exchange_source_name" => "source_name",
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//table/tr[@data-severity='1' and @data-item='1']");
        $this->assertEquals($expected_errors, $nodes->count());
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testConnexion(): void
    {
        $mock_client = $this->createMock(SOAPClientInterface::class);
        $mock_client->method('isReachableSource')->willReturn(true);
        $mock_client->method('isAuthentificate')->willReturn(true);

        $mock_source = $this->getMockBuilder(CSourceSOAP::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getClient'])
            ->getMock();
        $mock_source->method('getClient')->willReturn($mock_client);

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
            ->onlyMethods(['getTestableSource'])
            ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($mock_source);

        $client = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/webservices/sources/connexion",
            [
                "exchange_source_name" => "source_name",
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//table/tr[@data-severity='1' and @data-item='1']");
        $this->assertEquals(0, $nodes->count());
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testListFunctionsFailed(): void
    {
        $mock_client = $this->createMock(SOAPClientInterface::class);
        $mock_client->method('isReachableSource')->willThrowException(new CMbException(''));

        $mock_source = $this->getMockBuilder(CSourceSOAP::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['disableResilience'])
            ->getMock();
        $mock_source->method('disableResilience')->willReturn($mock_client);
        $mock_source->_reachable = 1;

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
            ->onlyMethods(['getTestableSource'])
            ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($mock_source);

        $client = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/webservices/sources/functions",
            [
                "exchange_source_name" => "source_name",
                "form_name" => "source_name",
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//div[@class='error']");
        $this->assertEquals(1, $nodes->count());
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testListFunctions(): void
    {
        $mock_client = $this->createMock(SOAPClientInterface::class);
        $mock_client->method('isReachableSource')->willReturn(true);
        $mock_client->method('isAuthentificate')->willReturn(true);
        $mock_client->method('getFunctions')->willReturn(['add', 'message']);

        $mock_source = $this->getMockBuilder(CSourceSOAP::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['disableResilience'])
            ->getMock();
        $mock_source->method('disableResilience')->willReturn($mock_client);
        $mock_source->_reachable = 1;

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
            ->onlyMethods(['getTestableSource'])
            ->getMock();
        $mock_source_repo->method('getTestableSource')->willReturn($mock_source);

        $client = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'GET',
            "/gui/webservices/sources/functions",
            [
                "exchange_source_name" => "source_name",
                "form_name" => "source_name",
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//td[@class='text']/text()[contains(., 'message')]");
        $this->assertEquals(1, $nodes->count());
        $nodes = $crawler->filterXPath("//td[@class='text']/text()[contains(., 'add')]");
        $this->assertEquals(1, $nodes->count());
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testExecuteFunctionFailed(): void
    {
        $client = $this->createClient();
        $crawler = $client->request(
            'POST',
            "/gui/webservices/sources/functions",
            [
                "exchange_source_guid" => "",
                "form_name" => "source_name",
                "parameters" => ['add', 1, 1]
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//div[@class='error']");
        $this->assertEquals(1, $nodes->count());
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testExecuteFunction(): void
    {
        $mock_client = $this->createMock(SOAPClientInterface::class);
        $mock_client->method('send')->willReturn(true);

        $mock_source = $this->getMockBuilder(CSourceSOAP::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['disableResilience'])
            ->getMock();
        $mock_source->method('disableResilience')->willReturn($mock_client);
        $mock_source->_reachable = 1;

        $mock_source_repo = $this->getMockBuilder(ExchangeSourceRepository::class)
            ->onlyMethods(['getSourceFromGuid'])
            ->getMock();
        $mock_source_repo->method('getSourceFromGuid')->willReturn($mock_source);

        $client = $this->createClient();
        $container = $this->getContainer();
        $container->set(ExchangeSourceRepository::class, $mock_source_repo);
        $crawler = $client->request(
            'POST',
            "/gui/webservices/sources/functions",
            [
                "exchange_source_guid" => "CSourceSOAP-1",
                "form_name" => "source_name",
                "parameters" => ['add', 1, 1]
            ]
        );
        $this->assertResponseStatusCodeSame('200');

        $nodes = $crawler->filterXPath("//div[@class='error']");
        $this->assertEquals(0, $nodes->count());
    }
}
