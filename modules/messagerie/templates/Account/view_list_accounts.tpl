{{*
 * @package Mediboard\Messagerie
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{* Start/Script *}}
{{mb_script module=system     script=exchange_source}}
{{mb_script module=messagerie script=Messagerie}}
{{mb_script module=mediusers  script=CMediusers}}

{{if $module_mailiz_active}}
    {{mb_script module=mssante script=Account}}
{{/if}}

{{if $module_medimail_active}}
    {{mb_script module=medimail script=medimail_account}}
{{/if}}

<script>
  Main.add(function () {
      CMediusers.standardAutocomplete('filterFrm', 'user_id', '_user_view');

      {{if $module_medimail_active}}
          MedimailAccount.url_edit = '{{url name=medimail_gui_accounts_edit}}';
      {{/if}}
  });
</script>
{{* End/Script *}}

{{mb_include module=messagerie template=Account/inc_filter_accounts}}

<table class="me-w100">
  <tr>
    <td id="account_list">
        {{mb_include module=messagerie template=Account/inc_list_accounts}}
    </td>
  </tr>
</table>
