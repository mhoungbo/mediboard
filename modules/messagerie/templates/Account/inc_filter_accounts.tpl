{{*
 * @package Mediboard\Messagerie
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<fieldset class="me-margin-top-8 me-margin-left-10 me-margin-right-10">
  <legend>
    <i class="fas fa-filter"></i> {{tr}}filters{{/tr}}
  </legend>
  <form name="filterFrm" method="get" onsubmit="return onSubmitFormAjax(this, null, 'account_list')">
    <input type="hidden" name="m" value="messagerie" />
    <input type="hidden" name="a" value="vw_list_accounts" />
    <input type="hidden" name="ajax" value="0" />
    <input type="hidden" name="only_list" value="1" />
    <table class="form me-no-box-shadow">
      <tr>
        <th>
            {{tr}}User{{/tr}}
        </th>
        <td class="text">
          <input type="hidden" name="user_id" value="{{$user->_id}}" />
          <input type="text" name="_user_view" class="autocomplete" value="{{$user->_view}}" />
        </td>
      </tr>
      <tr>
        <td class="button" colspan="6">
          <button type="submit" class="search me-primary">
            {{tr}}Search{{/tr}}
          </button>
        </td>
      </tr>
    </table>
  </form>
</fieldset>
