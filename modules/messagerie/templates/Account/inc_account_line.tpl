{{*
 * @package Mediboard\Messagerie
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<tr>
  <td>
    {{$account->getType()}}
  </td>
  <td>
    {{$account->getEmail()}}
  </td>
  <td>
    <button class="edit notext"
            title="{{tr}}Edit{{/tr}}"
            onclick="
            {{if $account->getType() === 'POP'}}
              ExchangeSource.editSource('CSourcePOP-{{$account->getIdentifier()}}', true);
            {{elseif $account->getType() === 'Apicrypt'}}
              new Url('apicrypt', 'ajax_apicrypt_account')
                .addParam('user_id', '{{$user->_id}}')
                .requestModal(500, 600, { method: 'POST', getParameters: {m: 'apicrypt', a: 'ajax_apicrypt_account'} });
            {{elseif $account->getType() === 'Mailiz'}}
              Account.edit(null, '{{$user->_id}}');
            {{elseif $account->getType() === 'Medimail'}}
              MedimailAccount.edit('{{$user->_id}}');
            {{/if}}
            ">
    </button>
  </td>
</tr>
