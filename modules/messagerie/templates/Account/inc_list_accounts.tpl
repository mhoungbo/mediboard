{{*
 * @package Mediboard\Messagerie
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main tbl me-w100">
  <tr>
    <th class="title" colspan="3">
      {{tr}}Account{{if $accounts|@count > 1}}|pl{{/if}}{{/tr}}
      <button class="new me-primary me-float-right" onclick="Messagerie.addAccount('{{$user->_id}}');">
        {{tr}}Account-action-Create an account{{/tr}}
      </button>
    </th>
  </tr>
  <tr>
    <th>{{tr}}Account-type{{/tr}}</th>
    <th>{{tr}}Account-email{{/tr}}</th>
    <th class="narrow"></th>
  </tr>
    {{foreach from=$accounts item=account}}
        {{mb_include module=messagerie template=Account/inc_account_line account=$account}}
    {{foreachelse}}
      <tr>
        <td colspan="6" class="empty">
            {{tr}}Account.none{{/tr}}
        </td>
      </tr>
    {{/foreach}}
</table>
