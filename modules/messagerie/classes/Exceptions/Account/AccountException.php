<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Messagerie\Exceptions\Account;

use Ox\Core\CMbException;

class AccountException extends CMbException
{
    public static function notSupported(): self
    {
        return new self('Account-error-Account not supported');
    }
}
