<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Messagerie\Domain\Account;

use Ox\Core\CMbObject;
use Ox\Mediboard\Medimail\CMedimailAccount;
use Ox\Mediboard\Messagerie\Exceptions\Account\AccountException;
use Ox\Mediboard\Mssante\CMSSanteUserAccount;
use Ox\Mediboard\System\CSourcePOP;

class Account
{
    /**
     * @var CSourcePOP|CMedimailAccount|CMSSanteUserAccount Origin account.
     */
    private CMbObject $account;

    public function __construct(CMbObject $account)
    {
        $this->account = $account;
    }

    /**
     * @throws AccountException
     */
    public function getType(): ?string
    {
        switch (true) {
            case $this->account instanceof CSourcePOP:
                return (str_contains($this->account->name, 'apicrypt')) ? 'Apicrypt' : 'POP';
            case $this->account instanceof CMedimailAccount:
                return 'Medimail';
            case $this->account instanceof CMSSanteUserAccount:
                return 'Mailiz';
            default:
                throw AccountException::notSupported();
        }
    }

    public function getEmail(): ?string
    {
        switch (true) {
            case $this->account instanceof CSourcePOP:
                return $this->account->user;
            case $this->account instanceof CMedimailAccount:
                return $this->account->medimail_login;
            case $this->account instanceof CMSSanteUserAccount:
                return $this->account->address;
            default:
                throw AccountException::notSupported();
        }
    }

    public function getIdentifier(): ?int
    {
        return $this->account->_id;
    }
}
