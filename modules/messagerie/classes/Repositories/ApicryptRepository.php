<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Messagerie\Repositories;

use Exception;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Messagerie\CUserMail;
use Ox\Mediboard\System\CSourcePOP;

/**
 * Repository to fetch Mail and MailCounter objects.
 */
class ApicryptRepository extends AbstractMailRepository
{
    /**
     * Count the unread user's mails using the repository parameters.
     *
     * @throws Exception
     */
    public function countUnreadMails(CMediusers $user): int
    {
        $account = CSourcePOP::getApicryptAccountFor($user);

        return CUserMail::countUnread($account->_id);
    }
}
