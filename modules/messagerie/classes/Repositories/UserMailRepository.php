<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Messagerie\Repositories;

use Exception;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Messagerie\CUserMail;
use Ox\Mediboard\System\CSourcePOP;

/**
 * Repository to fetch Mail and MailCounter objects.
 */
class UserMailRepository extends AbstractMailRepository
{
    /**
     * Count the unread user's mails using the repository parameters.
     *
     * @throws Exception
     */
    public function countUnreadMails(CMediusers $user): int
    {
        $count = 0;

        $accounts = CSourcePOP::getAccountsFor($user);
        foreach ($accounts as $account) {
            $count += CUserMail::countUnread($account->_id);
        }

        return $count;
    }
}
