<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Messagerie\Repositories;

use Exception;
use Ox\Mediboard\Medimail\CMedimailAccount;
use Ox\Mediboard\Messagerie\Domain\Account\Account;
use Ox\Mediboard\Mssante\CMSSanteUserAccount;
use Ox\Mediboard\System\CSourcePOP;

class AccountRepository
{
    /**
     * Get the user's medimail account.
     *
     * @param int $user_id User ID to be searched
     *
     * @return Account|null
     * @throws Exception
     */
    public function getMedimailAccount(int $user_id): ?Account
    {
        $medimail_account = new CMedimailAccount();
        $medimail_account->loadObject(
            [
                'user_id' => $medimail_account->getDS()->prepare('= ?', $user_id),
            ]
        );

        return ($medimail_account->_id) ? new Account($medimail_account) : null;
    }

    /**
     * Get the user's mailiz account.
     *
     * @param int $user_id User ID to be searched
     *
     * @return Account|null
     * @throws Exception
     */
    public function getMailizAccount(int $user_id): ?Account
    {
        $mailiz_account = new CMSSanteUserAccount();
        $mailiz_account->loadObject(
            [
                'user_id' => $mailiz_account->getDS()->prepare('= ?', $user_id),
            ]
        );

        return ($mailiz_account->_id) ? new Account($mailiz_account) : null;
    }

    /**
     * Get the user's pop accounts.
     *
     * @param int $user_id User ID to be searched
     *
     * @return Account[]
     * @throws Exception
     */
    public function getPopAccounts(int $user_id): array
    {
        $pop_source   = new CSourcePOP();
        $pop_accounts = $pop_source->loadList(
            [
                'object_class' => $pop_source->getDS()->prepare('= ?', 'CMediusers'),
                'object_id'    => $pop_source->getDS()->prepare('= ?', $user_id),
            ]
        );

        return array_map(fn($pop_account) => new Account($pop_account), $pop_accounts);
    }
}
