<?php
/**
 * @package Mediboard\Messagerie
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Messagerie;

use Ox\Core\Autoload\IShortNameAutoloadable;
use Ox\Core\CAppUI;
use Ox\Core\CMbObject;
use Ox\Core\CMbString;
use Ox\Core\CModelObject;
use Ox\Mediboard\System\CSourcePOP;
use stdClass;

/**
 * A utility class for all the POP/IMAP interactions
 */
class CPop implements IShortNameAutoloadable
{
    public CSourcePOP $source;

    /** @var mixed Is an instance of IMAP\Connection as of PHP 8.1.0, and a resource before, so do not type this property */
    public $_mailbox; //ressource id
    public string $_server;
    public ?stdClass $_mailbox_info = null;
    public array $_mailbox_overview = [];
    public array $_mailbox_filtered = [];
    public array $_mailbox_notfound = [];
    public array $_mailbox_archived = [];

    public bool $_is_open = false;

    public array $_parts = [];

    public array $content = [
        'text'        => [
            'plain'       => null,
            'html'        => null,
            'is_apicrypt' => null,
        ],
        'attachments' => [],
    ];


    /**
     * constructor
     *
     * @param CSourcePOP $source Source POP
     */
    public function __construct(CSourcePOP $source)
    {
        //stock the source
        $this->source = $source;

        if (!function_exists('imap_open')) {
            CModelObject::error('FE-IMAP-support-not-available');
        }

        //initialise open TIMEOUT
        imap_timeout(1, $this->source->timeout);


        //lets create the string for stream
        $type      = $this->source->type === 'pop3' ? "/{$this->source->type}" : '';
        $ssl       = $this->source->auth_ssl === 'SSL/TLS' ? '/ssl/novalidate-cert' : '/notls';
        $port      = $this->source->port ? ":{$this->source->port}" : '';
        $extension = $this->source->extension ?? '';

        $this->_server = '{' . $this->source->host . $port . $type . $ssl . '}' . $extension;
    }

    /**
     * Cleanup temporary elements
     *
     * @return void
     */
    public function cleanTemp(): void
    {
        $this->content = [
            'text'        => [
                'plain'       => null,
                'html'        => null,
                'is_apicrypt' => null,
            ],
            'attachments' => [],
        ];
    }

    /**
     * Start the timer that trace network time
     *
     * @return void
     */
    protected function startNetworkTimer(): void
    {
        $this->source->startCallTrace();
    }

    /**
     * Stop the timer that trace network time
     *
     * @return void
     */
    protected function stopNetworkTimer(): void
    {
        $this->source->stopCallTrace();
    }

    /**
     * Open the remote mailbox
     *
     * @param string|null $extension Extension of the serveur to open (/Inbox, ...)
     *
     * @return int|bool
     */
    public function open($extension = null)
    {
        $port      = ($this->source->port) ? ":{$this->source->port}" : '';
        $protocole = ($this->source->auth_ssl) ? 'https://' : 'http://';
        $url       = $protocole . $this->source->host . $port;

        if (!isset($this->_server)) {
            CAppUI::stepAjax('CPop-error-notInitiated', UI_MSG_ERROR);

            return false;
        }

        $password       = $this->source->getPassword();
        $server         = $this->_server . $extension;

        $this->startNetworkTimer();
        $this->_mailbox = @imap_open($server, $this->source->user, $password, 0, 0);
        $this->stopNetworkTimer();
        //avoid errors reporting
        imap_errors();
        imap_alerts();
        if ($this->_mailbox === false) {
            //CModelObject::warning('IMAP-warning-configuration-unreashable', $this->source->object_class, $this->source->object_id);
            return false;
        }

        $this->_is_open = true;

        return $this->_mailbox;
    }

    /**
     * @return array
     */
    public function getOverview(): array
    {
        if (empty($this->_mailbox_info) || !property_exists(
                $this->_mailbox_info,
                'Nmsgs'
            ) || $this->_mailbox_info->Nmsgs === 0) {
            return [];
        }

        $this->startNetworkTimer();
        $overview = imap_fetch_overview(
            $this->_mailbox,
            "1:{$this->_mailbox_info->Nmsgs}",
            0
        );
        $this->stopNetworkTimer();

        /* imap_fetch_overview can return an array or false */
        return $this->_mailbox_overview = $overview ?: [];
    }

    /**
     * return the mailbox check : new msgs
     *
     * @return ?stdClass
     */
    public function check(): ?stdClass
    {
        $this->startNetworkTimer();
        /* imap_check returns an stdClass if successful, and false otherwise */
        $info = imap_check($this->_mailbox);
        $this->stopNetworkTimer();

        $this->getOverview();

        return $this->_mailbox_info = $info ?: null;
    }

    /**
     * search for a mail using specific string
     *
     * @param string $string  search string
     * @param bool   $uid     use uid of mails
     *
     * @return array
     */
    public function search(string $string, bool $uid = true): array
    {
        $this->startNetworkTimer();
        if ($uid) {
            $results = imap_search($this->_mailbox, $string, SE_UID);
        } else {
            $results = imap_search($this->_mailbox, $string);
        }
        $this->stopNetworkTimer();

        /* The function imap_search can return false in case of a failure */
        return $results ?: [];
    }

    /**
     * @param string $subject
     * @param bool   $uid
     * @param string $from
     * @param string $to
     *
     * @description Function that filters emails from a mailbox depending on entry parameters
     *
     * @return array|bool
     */
    public function filter(string $subject = '', bool $uid = true, string $from = '', string $to = ''): array
    {
        if (empty($to)) {
            $to = $this->source->user;
        }

        $request = '';
        if (!empty($subject) && is_string($subject)) {
            $request .= ' SUBJECT "' . $subject . '"';
        }
        if (!empty($from) && filter_var($from, FILTER_VALIDATE_EMAIL)) {
            $request .= ' FROM "' . $from . '"';
        }
        if (!empty($to) && filter_var($to, FILTER_VALIDATE_EMAIL)) {
            $request .= ' TO "' . $to . '"';
        }

        if (!empty($request)) {
            $this->startNetworkTimer();
            $filtered_ids = imap_search(
                $this->_mailbox,
                $request,
                $uid ? SE_UID : false
            );
            $this->stopNetworkTimer();

            $filtered_emails = [];
            $archived_emails = [];

            if (!empty($filtered_ids)) {
                foreach ($this->_mailbox_overview as $email) {
                    if (in_array($email->uid, $filtered_ids)) {
                        $filtered_emails[] = $email;
                    } else {
                        $archived_emails[] = $email;
                    }
                }
            }

            $this->_mailbox_archived = $archived_emails;

            return $this->_mailbox_filtered = $filtered_emails;
        }

        return [];
    }

    /**
     * @param string $pattern
     *
     * @return array
     */
    public function getMailboxesList(string $pattern = '*'): array
    {
        $this->startNetworkTimer();
        $result = imap_list($this->_mailbox, $this->_server, $pattern) ?: [];
        $this->stopNetworkTimer();

        return $result;
    }

    /**
     * @param string $name
     * @param string $extension
     * @param bool   $server
     *
     * @return string
     */
    public function getMailboxName(string $name, string $extension = 'INBOX', bool $server = true): string
    {
        return ($server ? $this->_server : '') . $extension . '.' . $name;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function createMailbox(string $name): bool
    {
        $this->startNetworkTimer();
        $result = @imap_createmailbox($this->_mailbox, imap_utf7_encode($this->getMailboxName($name)));
        $this->stopNetworkTimer();
        if ($result) {
            return true;
        } else {
            echo('Impossible de cr�er une nouvelle bo�te aux lettres : ' . implode(
                    "<br />\n",
                    imap_errors()
                ) . "<br />\n");

            return false;
        }
    }

    /**
     * Return the content type of the given mail
     *
     * @param int $id The id of the mail
     *
     * @return bool
     */
    public function checkSMIME(int $id): bool
    {
        $structure = $this->structure($id);

        return $structure->ifsubtype && isset($structure->subtype) && $structure->subtype == 'PKCS7-MIME';
    }

    /**
     * Get the raw content of the mail
     *
     * @param int $id The mail id
     *
     * @return string
     */
    public function getEncryptedMail(int $id): string
    {
        $this->startNetworkTimer();
        $mime = imap_fetchheader($this->_mailbox, $id, FT_UID);
        $body = imap_body($this->_mailbox, $id, FT_UID);
        $this->stopNetworkTimer();

        return $mime . "\r\n" . $body;
    }

    /**
     * Return the MIME headers of the given mail
     *
     * @param int $id The mail id
     *
     * @return string
     */
    public function mimeHeaders(int $id): string
    {
        $this->startNetworkTimer();
        $headers = imap_fetchmime($this->_mailbox, $id, 0, FT_UID) ?: '';
        $this->stopNetworkTimer();

        return $headers;
    }

    /**
     * get the header of the mail
     *
     * @param int $id mail id
     *
     * @return null|stdClass
     */
    public function header(int $id): ?stdClass
    {
        $this->startNetworkTimer();
        $header = imap_headerinfo($this->_mailbox, imap_msgno($this->_mailbox, $id));
        $this->stopNetworkTimer();
        if ($header instanceof stdClass) {
            $header->uid = $id;
        }

        return $header;
    }

    /**
     * get informations of an email (UID)
     *
     * @param int $id message number (msgno ! not uid)
     *
     * @return object
     */
    public function infos(int $id): ?stdClass
    {
        $this->startNetworkTimer();
        $info = imap_headerinfo($this->_mailbox, $id, FT_UID);
        $this->stopNetworkTimer();

        return $info;
    }

    /**
     * get the structure of the mail (parts)
     *
     * @param int $id uid of the mail
     *
     * @return object
     */
    public function structure(int $id): ?stdClass
    {
        $this->startNetworkTimer();
        /* The imap_fetch_structure function can return false */
        $structure = imap_fetchstructure($this->_mailbox, $id, FT_UID) ?: null;
        $this->stopNetworkTimer();

        return $structure;
    }

    /**
     * Send a flag to the server
     *
     * @param string $id   A message uid, or a sequence of id (format "x,y" or "x:y" for an interval)
     * @param string $flag The flag to set
     *
     * @return bool
     */
    public function setFlag(string $id, string $flag): bool
    {
        $this->startNetworkTimer();
        $result = imap_setflag_full($this->_mailbox, $id, $flag, ST_UID);
        $this->stopNetworkTimer();

        return $result;
    }


    /**
     * Open a part of an email
     *
     * @param int    $msgId
     * @param string $partId
     * @param bool   $uid
     *
     * @return string
     */
    public function openPart(int $msgId, string $partId, bool $uid = true): string
    {
        $flags = FT_PEEK;
        if ($uid) {
            $flags = FT_UID | FT_PEEK;
        }

        $this->startNetworkTimer();
        /* imap_fetchbody can return false in case of error */
        $part = imap_fetchbody($this->_mailbox, $msgId, $partId, $flags) ?: '';
        $this->stopNetworkTimer();

        return $part;
    }

    /**
     * Return the raw body content
     *
     * @param int $mail_id The mail id
     *
     * @return string
     */
    public function body(int $mail_id): string
    {
        $this->startNetworkTimer();
        $body = imap_body($this->_mailbox, $mail_id, FT_UID) ?: '';
        $this->stopNetworkTimer();

        return $body;
    }

    /**
     * Get the body of the mail : HTML & plain text if available!
     *
     * @param int   $mail_id
     * @param mixed $structure   Can be an array, or a stdClass (I think)
     * @param mixed $part_number Can be a string or an int
     * @param bool  $only_text   test
     *
     * @return array
     */
    public function getFullBody(
        int $mail_id,
        ?stdClass $structure = null,
        ?string $part_number = null,
        bool $only_text = false
    ): array {
        if (!$structure) {
            $structure = $this->structure($mail_id);
        }

        if ($structure) {
            if (!isset($structure->parts) && !$part_number) {  //plain text only, no recursive
                $part_number = '1';
            }

            if (!$part_number) {
                $part_number = '0';
            }

            switch ($structure->type) {
                /* text/plain or text/html */
                case 0:
                    /* Checks if the part or subpart has a filename to detect if it is an attachment or not */
                    if ($structure->subtype == 'PLAIN') {
                        $is_attachment = false;
                        if (isset($structure->parameters) && is_array($structure->parameters)) {
                            foreach ($structure->parameters as $parameter) {
                                if (in_array(strtolower($parameter->attribute), ['name', 'filename'])) {
                                    $is_attachment = true;
                                }
                            }
                        }

                        if (!$is_attachment) {
                            $this->content['text']['plain'] = self::decodeMail(
                                $structure->encoding,
                                $this->openPart($mail_id, $part_number),
                                $structure
                            );

                            if (self::isContentApicrypt($this->content['text']['plain'])) {
                                $this->content['text']['is_apicrypt'] = 'plain';
                            }
                        }
                    } elseif ($structure->subtype == 'HTML') {
                        $this->content['text']['html'] = self::decodeMail(
                            $structure->encoding,
                            $this->openPart($mail_id, $part_number),
                            $structure
                        );

                        if (self::isContentApicrypt($this->content['text']['html'])) {
                            $this->content['text']['is_apicrypt'] = 'html';
                        }
                    }
                    break;
                /* multipart or alternatived */
                case 1:
                    foreach ($structure->parts as $index => $sub_structure) {
                        $prefix = null;
                        if ($part_number) {
                            $prefix = $part_number . '.';
                        }

                        $this->getFullBody($mail_id, $sub_structure, $prefix . ($index + 1));
                    }
                    break;
                /* message */
                case 2:
                /* application */
                case 3:
                /* audio */
                case 4:
                /* images */
                case 5:
                /* video */
                case 6:
                default:
                    if ($only_text) {
                        $attach = new CMailAttachments();
                        $attach->loadFromHeader($structure);
                        if ($attach->name) {
                            $attach->loadContentFromPop($this->openPart($mail_id, $part_number));

                            /* inline attachments */
                            if ($attach->id && $attach->subtype != 'SVG+XML') {
                                $id                            = 'cid:' . str_replace(['<', '>'],
                                                                                      ['', ''],
                                                                                      $attach->id);
                                $url                           = "data:image/$attach->subtype|strtolower;base64," . $attach->_content;
                                $this->content['text']['html'] = str_replace($id, $url, $this->content['text']['html']);
                            } else {  //attachments below
                                $this->content['attachments'][] = $attach;
                            }
                        }
                    }
            }
        }

        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return bool
     */
    protected static function isContentApicrypt(string $content): bool
    {
        return stripos($content, '$APICRYPT') !== false || stripos($content, '****FINFICHIER****') !== false;
    }

    /**
     * Get the attachments of a mail_id
     *
     * @param int           $mail_id     id of the mail (warning, UID and not ID)
     * @param stdClass|null $structure   structure
     * @param string|null   $part_number part number
     * @param string|null   $part_temp   part for get the part later
     *
     * @return CMailAttachments[]
     */
    public function getListAttachments(
        int $mail_id,
        ?stdClass $structure = null,
        ?string $part_number = null,
        ?string $part_temp = null
    ): array {
        if (!$structure) {
            $structure = $this->structure($mail_id);
        }

        if ($structure) {
            if (!isset($structure->parts) && !$part_number) {  //plain text only, no recursive
                $part_number = '1';
            }

            if (!$part_number) {
                $part_number = '0';
            }

            switch ($structure->type) {
                /* multipart or alternatived */
                case 1:
                    foreach ($structure->parts as $index => $sub_structure) {
                        $prefix = '';
                        if ($part_number !== false) {
                            $prefix = $part_number . '.';
                        }

                        $prefix_temp = '';
                        if ($part_temp) {
                            $prefix_temp = $part_temp . '.';
                        }

                        $this->getListAttachments(
                            $mail_id,
                            $sub_structure,
                            $prefix . ($index + 1),
                            $prefix_temp . $index
                        );
                    }
                    break;
                /* text/plain or text/html */
                case 0:
                    /* message */
                case 2:
                    /* application */
                case 3:
                    /* audio */
                case 4:
                    /* images */
                case 5:
                    /* video */
                case 6:
                    $attach = new CMailAttachments();
                    $attach->loadFromHeader($structure);
                    if ($attach->name) {
                        $attach->part = $part_temp;
                        //inline attachments
                        $this->content['attachments'][] = $attach;
                    }
                    break;
                default:
            }
        }

        return $this->content['attachments'];
    }

    /** TOOLS **/

    /**
     * get the right decoding string from mail structure
     *
     * @param int         $encoding  encoding number (from structure)
     * @param string      $text      the text to decode
     * @param object|null $structure an mail structure for additional decoding
     *
     * @return string
     */
    public static function decodeMail(?int $encoding, string $text, ?stdClass $structure = null): ?string
    {
        $retour = null;
        switch ($encoding) {
            /* 0 : 7 bit / 1 : 8 bit / 2 ; binary / 5 : other  => default  */
            case(3):  //base64
                $retour = imap_base64($text);
                break;
            case(4):
                $retour = imap_qprint($text);
                break;
            default:
                $retour = $text;
                break;
        }

        //Hack for bad defined encoding
        if (!empty($structure->parameters) && is_array($structure->parameters)) {
            for ($k = 0, $l = count($structure->parameters); $k < $l; $k++) {
                $attribute = $structure->parameters[$k];
                if ($attribute->attribute == 'CHARSET' && strtoupper($attribute->value) == 'UTF-8') {
                    return CMbString::utf8Decode($retour);
                }
            }
        }

        return $retour;
    }

    /**
     * Decode an encoded string (in base64 or quoted printable)
     *
     * @param string $string The string to decode
     *
     * @return string
     */
    public static function decodeString(string $string): string
    {
        $str = '';

        if (strpos($string, '?=')) {
            foreach (explode('?= =?', $string) as $_part) {
                $_part = str_replace(['=?', '?='], '', $_part);//substr($_part, strpos($_part, '=?') + 2);

                $_parts = explode('?', $_part);

                if (in_array($_parts[0], mb_list_encodings())) {
                    $charset  = $_parts[0];
                    $encoding = $_parts[1];

                    switch ($encoding) {
                        case 'B':
                            $_str = base64_decode($_parts[2]);
                            break;
                        case 'Q':
                            $_str = quoted_printable_decode($_parts[2]);
                            break;
                        default:
                            $_str = $_parts[2];
                    }

                    $str .= iconv($charset, 'ISO-8859-1//TRANSLIT', CMbString::normalizeUtf8($_str));
                }
            }
        } else {
            $str = $string;
        }

        return $str;
    }

    /**
     * Mark the mail with the given uid to be deleted from the current mailbox
     *
     * @param int $uid
     *
     * @return bool
     */
    public function deleteMail(int $uid): bool
    {
        $this->startNetworkTimer();
        $result = imap_delete($this->_mailbox, $uid, FT_UID);
        $this->stopNetworkTimer();

        return $result;
    }

    /**
     * Deletes all the messages marked for deletion
     *
     * @return bool
     */
    public function expunge(): bool
    {
        $this->startNetworkTimer();
        $result = imap_expunge($this->_mailbox);
        $this->stopNetworkTimer();

        return $result;
    }

    /**
     * check if imap_lib exist
     *
     * @return bool
     */
    public static function checkImapLib(): bool
    {
        if (!function_exists('imap_open')) {
            CMbObject::error('no-imap-lib');
            CAppUI::stepAjax('no-imap-lib', UI_MSG_ERROR);

            return false;
        }

        return true;
    }

    /**
     * Close the stream
     *
     * @return bool
     */
    public function close(): bool
    {
        $result = false;
        if ($this->_is_open) {
            $this->startNetworkTimer();
            $result = imap_close($this->_mailbox);
            $this->stopNetworkTimer();
        }

        return $result;
    }
}
