<?php

/**
 * @package Mediboard\Messagerie
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Messagerie\Repositories\AccountRepository;

/**
 * Displays a user's mail accounts (only reception accounts).
 *
 * @deprecated Do not maintain this file in its current state, except in the event of a SEGUR backport.
 * TODO: Turn it into a Controller GUI as soon as possible.
 */

CCanDo::checkAdmin();

$user_id   = CView::getRefCheckEdit('user_id', 'ref class|CMediusers default|' . CMediusers::get()->_id);
$only_list = CView::get('only_list', 'bool default|0');

CView::checkin();

$user = CMediusers::findOrFail($user_id);
$accounts   = [];
$repository = new AccountRepository();

// Medimail account
if ($module_medimail_active = CModule::getActive('medimail')) {
    $account = $repository->getMedimailAccount($user->_id);

    if ($account) {
        $accounts[] = $account;
    }
}

// Mailiz account
if ($module_mailiz_active = CModule::getActive('mssante')) {
    $account = $repository->getMailizAccount($user->_id);

    if ($account) {
        $accounts[] = $account;
    }
}

// Pop accounts
$pop_accounts = $repository->getPopAccounts($user->_id);
foreach ($pop_accounts as $pop_account) {
    $accounts[] = $pop_account;
}

// Smarty
$template = ($only_list) ? 'Account/inc_list_accounts.tpl' : 'Account/view_list_accounts.tpl';

$smarty = new CSmartyDP();
$smarty->assign('accounts', $accounts);
$smarty->assign('user', $user);
$smarty->assign('module_medimail_active', $module_medimail_active);
$smarty->assign('module_mailiz_active', $module_mailiz_active);
$smarty->display($template);
