<?php
/**
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Mediboard\Bloc\CBlocOperatoire;
use Ox\Mediboard\Bloc\CSSPI;
use Ox\Mediboard\Bloc\CSSPILink;

CCanDo::checkEdit();

$bloc_id = CView::get("bloc_id", "ref class|CBlocOperatoire");
$use_bloc_pref = CView::get("use_bloc_pref", "bool default|0");

CView::checkin();

$bloc = new CBlocOperatoire();

$sspi = new CSSPI();
$ljoin = ["sspi_link" => "sspi_link.sspi_id = sspi.sspi_id"];
$where = [
    "sspi_link.bloc_id" => CSQLDataSource::prepareIn($use_bloc_pref ?  explode("|", CAppUI::pref("selected_bloc_ids")) : [$bloc_id])
];
$sspis = $sspi->loadList($where, null, null, "sspi.sspi_id", $ljoin);

// Cr�ation du template
$smarty = new CSmartyDP();

$smarty->assign("sspis", $sspis);

$smarty->display("inc_select_sspi");
