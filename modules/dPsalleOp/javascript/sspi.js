/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

SSPI = window.SSPI || {
  path_blocs_preferences: '',
  path_sspis_preferences: '',
  selectBlocs: function (callback) {
    new Url()
      .setRoute(SSPI.path_blocs_preferences, 'bloc_blocs_preferences', 'bloc')
      .addParam('view', 'select_favorites')
      .modal({ width: 450, onClose: callback });
  },
  updateSelectSSPI: function (divId, bloc_ids, onComplete = null) {
    new Url()
      .addParam('bloc_ids[]', bloc_ids, true)
      .addParam('view', 'select_favorites')
      .setRoute(SSPI.path_sspis_preferences, 'bloc_sspis_preferences', 'bloc')
      .requestUpdate(divId, { onComplete: onComplete });
  },
  saveSelectBlocsPref: function (bloc_ids, callback = null) {
    App.savePref("selected_bloc_ids", bloc_ids.join("|"), callback)
  },
  saveSelectSSPIPref: function (sspi_ids, callback) {
    App.savePref("selected_sspi_ids", sspi_ids.join("|"), callback)
  }
};
