<?php
/**
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CMbDT;
use Ox\Core\CSmartyDP;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Mediboard\Bloc\CPosteSSPI;
use Ox\Mediboard\Etablissement\CGroups;

CCanDo::checkRead();

$keywords_preop = CView::get("_poste_preop_id_autocomplete", "str");
$keywords_sspi  = CView::get("_poste_sspi_id_autocomplete", "str");
$type           = CView::get("type", "enum list|sspi|preop");
$use_pref       = CView::get("use_pref", "bool default|0");

CView::checkin();

$poste_sspi = new CPosteSSPI();
$ds         = $poste_sspi->getDS();
$where      = [
    "actif" => "= '1'",
    "type"  => $ds->prepare("= ?", $type),
];

$sspi_ids = explode("|", CAppUI::pref("selected_sspi_ids"));

if ($use_pref) {
    $where[] = "sspi_id " . CSQLDataSource::prepareIn($sspi_ids);
} else {
    $sspis   = CGroups::loadCurrent()->loadSSPIs();
    $where[] = "sspi_id " . CSQLDataSource::prepareIn(array_column($sspis, '_id'));
}

$matches = $poste_sspi->getAutocompleteList($type === "sspi" ? $keywords_sspi : $keywords_preop, $where);

$post_field = $type === 'sspi' ? 'poste_sspi_id' : 'poste_preop_id';
$where[]    = $ds->prepare(
        "sspi_affectation.start IS NOT NULL AND sspi_affectation.end IS NULL AND ? > sspi_affectation.start",
        CMbDT::dateTime()
    ) . " AND sspi_affectation.start " .
    $ds->prepareLike(CMbDT::date() . " %");
$ljoin      = ["sspi_affectation" => "sspi_affectation.$post_field = poste_sspi.poste_sspi_id"];

$occupied_post_ids = $poste_sspi->loadIds($where, null, null, null, $ljoin);

$smarty = new CSmartyDP();

$smarty->assign("matches", $matches);
$smarty->assign("occupied_post_ids", $occupied_post_ids);
$smarty->assign('view_field', true);
$smarty->assign('field', '_view');
$smarty->assign('show_view', true);
$smarty->assign("nodebug", true);

$smarty->display("inc_field_sspipost_autocomplete");
