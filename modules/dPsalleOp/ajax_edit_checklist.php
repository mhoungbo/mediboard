<?php
/**
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

// Récupération des paramètres
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CSmartyDP;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Bloc\CBlocOperatoire;
use Ox\Mediboard\Bloc\CSalle;
use Ox\Mediboard\Bloc\CSSPI;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Personnel\CPersonnel;
use Ox\Mediboard\SalleOp\CDailyCheckList;
use Ox\Mediboard\SalleOp\CDailyCheckListType;

$date            = CView::get("date", 'date default|now', true);
$salle_id        = CView::get("salle_id", 'ref class|CSalle', true);
$type            = CView::get("type", "str default|ouverture_salle", true);
$multi_ouverture = CView::get("multi_ouverture", 'bool default|0');

CView::checkin();

// Récupération de l'utilisateur courant
$user = CUser::get();
$currUser = new CMediusers();
$currUser->load($user->_id);
$currUser->isAnesth();
$currUser->isPraticien();

$salle = new CSalle();
$salle->load($salle_id);

// Vérification de la check list journalière
$daily_check_lists = array();
$daily_check_list_types = array();
$check_list_by_types    = array();

// Settings
$conf_required        = 0;
$group                = CGroups::loadCurrent();
$conf_required_reveil = CAppUI::conf("dPsalleOp CDailyCheckList active_salle_reveil", $group);
$use_poste            = CAppUI::conf("dPplanningOp COperation use_poste");
$type_no_reveil       = ['ouverture_salle', 'fermeture_salle'];

if ($salle->_id) {
    $sspi_ids      = [];
    $blocs         = [];
    $sspis         = [];
    $objects       = [$salle];
    $conf_required = $salle->loadRefBloc()->checklist_everyday;
} else {
    $sspi_ids      = $use_poste ? explode("|", CAppUI::pref("selected_sspi_ids")) : [];
    $bloc_ids      = explode("|", CAppUI::pref("selected_bloc_ids"));
    $sspis         = (new CSSPI())->loadList(["sspi_id" => CSQLDataSource::prepareIn($sspi_ids)]);
    $objects       = $blocs = (new CBlocOperatoire())->loadList(
        ["bloc_operatoire_id" => CSQLDataSource::prepareIn($bloc_ids)]
    );
    $conf_required = in_array("1", array_column($blocs, "checklist_everyday"));
}

$active_salle_reveil = $conf_required_reveil && !in_array($type, $type_no_reveil);
$checklist_salle     = $salle->_id && $salle->cheklist_man;
$conf_required       = $conf_required && in_array($type, $type_no_reveil);

$require_check_list  = ($conf_required
        || $active_salle_reveil
        || $checklist_salle)
    && $date >= CMbDT::date();

if ($require_check_list) {
  [$check_list_not_validated, $daily_check_list_types, $daily_check_lists] = CDailyCheckList::getCheckListsWithSSPI($objects, $date, $type, $multi_ouverture, array_column($sspis, '_id'));
  //Permettre de faire plusieurs fois par jour les checklist d'ouverture de salle
  if ($multi_ouverture && !$check_list_not_validated) {
    $check_list_not_validated = count($daily_check_list_types);
    $list_type = array();

    /**  @var  CDailyCheckList $_daily_list_type */
      foreach ($daily_check_lists as $_daily_list_type) {
      $_list_type = new CDailyCheckList();
      $_list_type->object_class = $_daily_list_type->object_class;
      $_list_type->object_id = $_daily_list_type->object_id;
      $_list_type->list_type_id = $_daily_list_type->list_type_id;
      $_list_type->date = $date;
      $_list_type->type = null;
      $_list_type->loadMatchingObject("date, date_validate", "daily_check_list_id");
      if ($_list_type->validator_id) {
        $_list_type = new CDailyCheckList();
        $_list_type->object_class = $_daily_list_type->object_class;
        $_list_type->object_id = $_daily_list_type->object_id;
        $_list_type->list_type_id = $_daily_list_type->list_type_id;
        $_list_type->type = null;
        $_list_type->_id = null;
      }
      $_list_type->_ref_object = $_list_type->loadFwdRef("object_id");
      $_list_type->loadRefListType()->loadRefsCategories();
      $_list_type->loadItemTypes();
      $_list_type->clearBackRefCache('items');
      $_list_type->loadBackRefs('items');
      $_list_type->loadRefListType();
      $list_type[] = $_list_type;
    }
    $daily_check_lists = $list_type;

      /** @var CDailyCheckListType $_type */
      foreach ($daily_check_list_types as $_type) {
          if (!$_type->_ref_object->_id) {
              continue;
          }
      $where = array();
      $where["object_class"] = " = '{$_type->_ref_object->_class}'";
      $where["object_id"]    = " = '{$_type->_ref_object->_id}'";
      $where["list_type_id"] = " = '$_type->_id'";
      $where["date"]         = " = '$date'";
      $where["type"]         = " IS NULL";
      $where["validator_id"] = " IS NOT NULL";
      $list_type = new CDailyCheckList();
      $check_list_by_types["{$_type->_ref_object->_guid}-{$_type->_id}"] = $list_type->loadList($where, "date, date_validate", null, "daily_check_list_id");
    }
    $require_check_list = true;
  }

  if ($check_list_not_validated == 0) {
    $require_check_list = false;
  }
}

// Chargement des praticiens
$listAnesths = new CMediusers;
$listAnesths = $listAnesths->loadAnesthesistes(PERM_DENY);

$listChirs = new CMediusers;
$listChirs = $listChirs->loadPraticiens(PERM_DENY);

$type_personnel = array("op", "op_panseuse", "iade", "sagefemme", "manipulateur");
if (count($daily_check_list_types) && $require_check_list) {
  $type_personnel = array();
  foreach ($daily_check_list_types as $check_list_type) {
    $validateurs = explode("|", $check_list_type->type_validateur);
    foreach ($validateurs as $validateur) {
      $type_personnel[] = $validateur;
    }
  }
}

$listValidateurs = CPersonnel::loadListPers(array_unique(array_values($type_personnel)), true, true);
$operateurs_disp_vasc = implode("-", array_merge(CMbArray::pluck($listChirs, "_id"), CMbArray::pluck($listValidateurs, "user_id")));

$nb_op_no_close = 0;
if ($type == "fermeture_salle") {
  $salle->loadRefsForDay($date);

  // Calcul du nombre d'actes codé dans les interventions
  if ($salle->_ref_plages) {
    foreach ($salle->_ref_plages as $_plage) {
      foreach ($_plage->_ref_operations as $_operation) {
        if (!$_operation->sortie_salle && !$_operation->annulee) {
          $nb_op_no_close++;
        }
      }
      foreach ($_plage->_unordered_operations as $_operation) {
        if (!$_operation->sortie_salle && !$_operation->annulee) {
          $nb_op_no_close++;
        }
      }
    }
  }
}

// Si on est dans le cas d'un seul bloc selectionné
$bloc = sizeof($blocs) === 1 ? reset($blocs) : null;


// Création du template
$smarty = new CSmartyDP();

// Daily check lists
$smarty->assign("salle"                 , $salle);
$smarty->assign("bloc"                  , $bloc);
$smarty->assign("type"                  , $type);
$smarty->assign("date"                  , $date);
$smarty->assign("nb_op_no_close"        , $nb_op_no_close);
$smarty->assign("require_check_list"    , $require_check_list);
$smarty->assign("daily_check_lists"     , $daily_check_lists);
$smarty->assign("daily_check_list_types", $daily_check_list_types);
$smarty->assign("listValidateurs"       , $listValidateurs);
$smarty->assign("listChirs"             , $listChirs);
$smarty->assign("listAnesths"           , $listAnesths);
$smarty->assign("multi_ouverture"       , $multi_ouverture);
$smarty->assign("check_list_by_types"   , $check_list_by_types);

$smarty->display("vw_edit_checklist.tpl");
