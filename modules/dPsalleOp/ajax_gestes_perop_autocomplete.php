<?php

/**
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CMbObject;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\SalleOp\CAnesthPeropCategorie;
use Ox\Mediboard\SalleOp\CGestePerop;
use Ox\Mediboard\SalleOp\CProtocoleGestePerop;

CCanDo::checkRead();
$keywords    = CView::get('geste_perop_id_view', 'str');
$object_guid = CView::get('object_guid', 'str');
CView::checkin();
CView::enableSlave();

$gestes = [];

if ($object_guid) {
    $object = CMbObject::loadFromGuid($object_guid);

    if ($object instanceof CAnesthPeropCategorie) {
        $gestes = $object->loadRefsGestesPerop();
    } elseif ($object instanceof CProtocoleGestePerop) {
        $gestes = $object->loadRefsGestePerop();
    }
}

$group = CGroups::loadCurrent();

$geste_perop = new CGestePerop();

$ds = $geste_perop->getDS();

$where = ['geste_perop.actif' => $ds->prepare('= ?', 1)];

$order = "libelle ASC";

// User level
$where_user = $where;

$where_user['functions_mediboard.group_id'] = $ds->prepare('= ?', $group->_id);

$ljoin = [
    'users_mediboard'     => 'users_mediboard.user_id = geste_perop.user_id',
    'functions_mediboard' => 'functions_mediboard.function_id = users_mediboard.function_id',
];

$gestes_perop = $geste_perop->seek($keywords, $where_user, 100, false, $ljoin, $order);

// Function level
$where_function = $where;

$where_function['functions_mediboard.group_id'] = $ds->prepare('= ?', $group->_id);

$ljoin = [
    'functions_mediboard' => 'functions_mediboard.function_id = geste_perop.function_id',
];

$gestes_perop = array_merge(
    $gestes_perop,
    $geste_perop->seek($keywords, $where_function, 100, false, $ljoin, $order)
);

// Group level
$where_group                         = $where;
$where_group['geste_perop.group_id'] = $ds->prepare('= ?', $group->_id);

$gestes_perop = array_merge(
    $gestes_perop,
    $geste_perop->seek($keywords, $where_group, 100, false, $ljoin, $order)
);

$smarty = new CSmartyDP();
$smarty->assign("matches", $gestes_perop);
$smarty->display("CGestesPerop_autocomplete");
