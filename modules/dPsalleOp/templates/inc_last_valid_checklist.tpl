{{*
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=multi_ouverture value=false}}

{{mb_default var=sspi_id value=null}}
<div style="float: right;text-align: center;">
  <button class="checklist" type="button"
          onclick="EditCheckList.edit('{{$date}}', '{{$type}}', '{{$multi_ouverture}}');">
    {{tr}}CDailyCheckList._type.{{$type}}{{/tr}}
  </button>
</div>
