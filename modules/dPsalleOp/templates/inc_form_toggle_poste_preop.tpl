{{*
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=bloc script=sspi_affectation ajax=true}}

<script>
  Main.add(() => {
    SSPIAffectation.path_operation_list_affectations = '{{url name=bloc_operation_list_sspi_affectations operation_id='0'}}';
  })
</script>

{{mb_default var=type value="preop"}}
<form name="editPoste{{$type}}{{$_operation->_id}}" method="post" class="prepared">
  <input type="hidden" name="m" value="planningOp" />
  <input type="hidden" name="dosql" value="do_planning_aed" />
  {{mb_key object=$_operation}}
  <input type="hidden" name="_poste_preop_id" value="{{if isset($_operation->_ref_poste_preop|smarty:nodefaults)}}{{$_operation->_ref_poste_preop->_id}}{{/if}}"
         onchange="onSubmitFormAjax(this.form, {onComplete:function () {
           {{$callback}};

         {{if "patientMonitoring"|module_active && "patientMonitoring CMonitoringSession start_monitoring_session"|gconf}}
            App.loadJS({module: 'patientMonitoring', script: 'concentrator_common'}, function() {
              ConcentratorCommon.askPosteConcentrator(
                '{{$_operation->_id}}',
                '{{if $_operation->_ref_salle}}{{$_operation->_ref_salle->bloc_id}}{{/if}}',
                '{{$type}}',
           getForm('editPoste{{$type}}{{$_operation->_id}}'),
           {{$callback}},
           1,
           '{{$_operation->sspi_id}}');
            });
         {{/if}}

         }});"/>
  <input type="text" name="_poste_preop_id_autocomplete" placeholder="{{tr}}CSSPIAffectation-msg-new{{/tr}}" value="{{$_operation->_ref_poste_preop}}"/>
  {{if isset($_operation->_ref_poste_preop->_id|smarty:nodefaults)}}
    <button type="button" class="list notext me-tertiary me-small" title="{{tr}}History{{/tr}}"
            onclick="SSPIAffectation.openHistory('{{$_operation->_id}}', 'preop', refreshTabReveil.curry('{{$type}}'))">
    </button>
  {{/if}}
  <script>
    Main.add(function() {
      var form = getForm("editPoste{{$type}}{{$_operation->_id}}");
      var url = new Url("salleOp", "sspipost_autocomplete");
      url.addParam("type", "preop");
      url.addParam("use_pref", "1");
      url.autoComplete(form.elements._poste_preop_id_autocomplete, null, {
        minChars: 2,
        method: "get",
        select: "view",
        dropdown: true,
        afterUpdateElement: function(field,selected) {
          var guid = selected.getAttribute('id');
          if (guid) {
            $V(field.form.elements._poste_preop_id_autocomplete, selected.down('.view').innerHTML);
            $V(field.form['_poste_preop_id'], guid.split('-')[2]);
          }
        }
      });
    });
  </script>
</form>
