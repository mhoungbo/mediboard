{{*
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=salleOp script=sspi ajax=1}}

<script>
  Main.add(() => {
    SSPI.path_blocs_preferences = '{{url name=bloc_blocs_show_preferences}}';
    SSPI.path_sspis_preferences = '{{url name=bloc_sspis_show_preferences}}';
  })
</script>

<form name="selectBloc" method="get" action="?">
  <input type="hidden" name="m" value="{{$m}}" />
  <input type="hidden" name="tab" value="vw_reveil" />
  <span id="heure">{{$tnow|date_format:$conf.time}}</span> - {{$date|date_format:$conf.longdate}}
  <input type="hidden" name="date" class="date" value="{{$date}}" onchange="this.form.submit()" />

  <button type="button" class="search me-tertiary" onclick="SSPI.selectBlocs(() => {refreshTabReveil(window.reveil_tabs.activeContainer.id)})">
      {{tr}}common-Operating bloc-court|pl{{/tr}} {{if $conf.dPplanningOp.COperation.use_poste}}{{tr}}and{{/tr}} {{tr}}CSSPI{{/tr}}{{/if}}
  </button>
</form>

<script>
  Main.add(function() {
    Calendar.regField(getForm("selectBloc").date, null, {noView: true});
  });
</script>
