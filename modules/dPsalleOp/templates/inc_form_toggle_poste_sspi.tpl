{{*
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=bloc script=sspi_affectation ajax=true}}

{{mb_default var=type value="ops"}}

<script>
  Main.add(() => {
    SSPIAffectation.path_operation_list_affectations = '{{url name=bloc_operation_list_sspi_affectations operation_id='0'}}';
  })
</script>

{{assign var=use_concentrator value=false}}
{{assign var=use_concentrator value=""}}
{{if "patientMonitoring"|module_active && "patientMonitoring CMonitoringConcentrator active"|gconf}}
  {{assign var=use_concentrator value=true}}
  {{assign var=current_session value='Ox\Mediboard\PatientMonitoring\CMonitoringSession::getCurrentSession'|static_call:$_operation}}
{{/if}}
<form name="editPoste{{$type}}{{$_operation->_id}}" method="post" class="prepared">
  <input type="hidden" name="m" value="planningOp" />
  <input type="hidden" name="dosql" value="do_planning_aed" />
  {{mb_key object=$_operation}}
  <input type="hidden" name="_poste_sspi_id" value="{{if isset($_operation->_ref_poste|smarty:nodefaults)}}{{$_operation->_ref_poste->_id}}{{/if}}"
         onchange="onSubmitFormAjax(this.form, {onComplete:function () {
           {{$callback}};}})"/>
  <input type="text" name="_poste_sspi_id_autocomplete" placeholder="{{tr}}CSSPIAffectation-msg-new{{/tr}}" value="{{$_operation->_ref_poste}}"/>
  {{if isset($_operation->_ref_poste->_id|smarty:nodefaults)}}
    <button type="button" class="list notext me-tertiary me-small" title="{{tr}}History{{/tr}}"
            onclick="SSPIAffectation.openHistory('{{$_operation->_id}}', 'sspi', refreshTabReveil.curry('{{$type}}'))">
    </button>
  {{/if}}
  <script>
    Main.add(function() {
      var form = getForm("editPoste{{$type}}{{$_operation->_id}}");
      var poste_sspi_id = $V(form._poste_sspi_id);
      var url = new Url("salleOp", "sspipost_autocomplete");
      url.addParam("type", "sspi");
      url.addParam("use_pref", "1");
      url.autoComplete(form.elements._poste_sspi_id_autocomplete, null, {
        minChars: 2,
        method: "get",
        select: "view",
        dropdown: true,
        afterUpdateElement: function(field,selected) {
          var guid = selected.getAttribute('id');
          if (guid) {
            $V(field.form.elements._poste_sspi_id_autocomplete, selected.down('.view').innerHTML);
            $V(field.form['_poste_sspi_id'], guid.split('-')[2]);
          }

         {{if ($use_concentrator && $current_session) || ("patientMonitoring"|module_active && "patientMonitoring CMonitoringSession start_monitoring_session"|gconf)}}
          if (poste_sspi_id != guid.split('-')[2]) {
            let callback = function () {
              setTimeout(function(){
                refreshTabReveil('{{$type}}');
              }, 1000);
            };

            App.loadJS({module: "patientMonitoring", script: "concentrator_common"}, function(){
              ConcentratorCommon.askPosteConcentrator(
                "{{$_operation->_id}}",
                "{{$bloc_id}}",
                "sspi",
                form,
                callback,
               1
              );
            });
          }
        {{/if}}
        }
      });
    });
  </script>
</form>
