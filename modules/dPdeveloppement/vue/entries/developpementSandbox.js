import DeveloppementSandbox from "../views/DeveloppementSandbox/DeveloppementSandbox.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

createEntryPoint("sandbox", DeveloppementSandbox)
