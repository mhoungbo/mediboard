{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create('tabs-translations', true, {
      afterChange: function (container) {
        new Url().setRoute('{{url name=developpement_translations_list}}')
          .addParam('integrate', ((container.id === 'tab-vw-integrate-translations') ? '1' : '0'))
          .requestUpdate(container.id);
      }
    });
  });
</script>

<ul id="tabs-translations" class="control_tabs">
  <li><a href="#tab-vw-translations">{{tr}}mod-dPdeveloppement-tab-developpement_translations_list{{/tr}}</a></li>
    {{if $conf.debug}}
      <li>
        <a
          href="#tab-vw-integrate-translations">{{tr}}mod-dPdeveloppement-tab-developpement_translations_integrate{{/tr}}</a>
      </li>
    {{/if}}
</ul>

<div id="tab-vw-translations" style="display: none;"></div>
{{if $conf.debug}}
  <div id="tab-vw-integrate-translations" style="display: none;"></div>
{{/if}}
