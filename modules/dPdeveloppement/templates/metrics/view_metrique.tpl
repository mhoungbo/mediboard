{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  viewCurrent = function () {
    new Url().setRoute('{{url name=developpement_metrics_list}}')
      .addParam("view_group", 1)
      .requestUpdate("current");
  };

  Main.add(Control.Tabs.create.curry("main_tab_group"));
</script>

<ul id="main_tab_group" class="control_tabs">
  <li><a href="#general">{{tr}}dPdeveloppement-metrics-General{{/tr}}</a></li>
    {{if $nb_etabs > 1}}
      <li onmousedown="viewCurrent();"><a href="#current">{{$etab}}</a></li>
    {{/if}}
</ul>

<div id="general" style="display: none;">
  <table class="tbl main">
    <tr>
      <th>{{tr}}dPdeveloppement-metrics-Data type{{/tr}}</th>
      <th>{{tr}}dPdeveloppement-metrics-Quantity{{/tr}}</th>
      <th>{{tr}}dPdeveloppement-metrics-Last update{{/tr}}</th>
    </tr>
      {{foreach from=$result item=_result key=class}}
        <tr>
          <td>{{tr}}{{$class}}{{/tr}}</td>
          <td>{{$_result.Rows|integer}}</td>
          <td>
            <label title="{{$_result.Update_time|date_format:$conf.datetime}}">
                {{$_result.Update_time|rel_datetime}}
            </label>
          </td>
        </tr>
      {{/foreach}}
  </table>
</div>

{{if $nb_etabs > 1}}
  <div id="current" style="display: none;"></div>
{{/if}}
