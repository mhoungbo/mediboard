{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create('control-tab', true);
    const files = ['file_20', 'file_50', 'file_80', 'file_100'];
    files.each(function (file) {
      const img = $(file);
      img.on('load', checkPerfs);
    });
  });

  function checkPerfs() {
    const perfs = performance.getEntries();

    for (let i = 0; i < perfs.length; i++) {
      const perf = perfs[i];
      if (perf.name !== this.currentSrc) {
        continue;
      }

      const legend = $(this.id + '_legend');
      legend.innerHTML = 'Size : ' + parseInt(perf.transferSize / 1024, 10) + 'Kb';
      legend.innerHTML += '<br/>Temps de g�n�ration : ' + parseInt(perf.responseStart - perf.requestStart, 10) + 'ms';
      legend.innerHTML += '<br/>Width : ' + this.clientWidth + 'px<br/>Height : ' + this.clientHeight + 'px';
    }
  }
</script>

<ul id="control-tab" class="control_tabs">
  <li><a href="#thumbnail-version">Versions</a></li>
  <li><a href="#thumbnail-profile">Profils</a></li>
  <li><a href="#thumbnail-rotation">Rotations</a></li>
  <li><a href="#thumbnail-quality">Qualit� JPEG</a></li>
</ul>

<div id="thumbnail-version" style="display: none">
    {{assign var=version_error_class value='warning'}}
    {{if !$version.gd_exists && (!$version.imagick_exists || !array_key_exists(0, $version.version_imagemagick))}}
        {{assign var=version_error_class value='error'}}
    {{/if}}

  <table class="main tbl">
    <tr>
      <th colspan="3">
        GD ou ImageMagick + Imagick est n�cessaire
      </th>
      <th rowspan="2">Biblioth�que Imagine</th>
    </tr>

    <tr>
      <th>Extension php GD</th>
      <th>Biblioth�que ImageMagick</th>
      <th>Extension php Imagick</th>
    </tr>

    <tr>
        {{if $version.gd_exists}}
          <td class="ok" align="center">Extension pr�sente et charg�e</td>
        {{else}}
          <td class="{{$version_error_class}}" align="center">Extension absente</td>
        {{/if}}

        {{if array_key_exists(0, $version.version_imagemagick)}}
          <td class="ok" align="center">{{$version.version_imagemagick.0}}</td>
        {{else}}
          <td class="{{$error_class}}" align="center">Biblioth�que absente</td>
        {{/if}}

        {{if $version.imagick_exists}}
          <td class="ok" align="center">Extension pr�sente et charg�e</td>
        {{else}}
          <td class="{{$version_error_class}}" align="center">Extension absente</td>
        {{/if}}

        {{if $version.imagine}}
          <td class="ok" align="center">Biblioth�que charg�e</td>
        {{else}}
          <td class="error" align="center">Biblioth�que absente</td>
        {{/if}}
    </tr>
  </table>
</div>

<div id="thumbnail-profile" style="display: none">
  <div>
    <table class="main form">
        {{foreach from=$profile.profiles item=_profile}}
          <tr>
            <td>
              <fieldset>
                <legend>Thumbnail {{$_profile}} - Sans crop</legend>
                  {{thumbnail profile=$_profile document=$profile.file page=1 default_size=1}}
                  {{thumbnail profile=$_profile document=$profile.file page=2 default_size=1}}
                  {{thumbnail profile=$_profile document=$profile.file page=3 default_size=1}}
                  {{thumbnail profile=$_profile document=$profile.file page=4 default_size=1}}
                  {{thumbnail profile=$_profile document=$profile.file page=5 default_size=1}}
              </fieldset>
            </td>
          </tr>
        {{/foreach}}
        {{foreach from=$profile.profiles item=_profile}}
          <tr>
            <td>
              <fieldset>
                <legend>Thumbnail {{$_profile}} - Avec crop</legend>
                  {{thumbnail profile=$_profile document=$profile.file page=1 crop=1 default_size=1}}
                  {{thumbnail profile=$_profile document=$profile.file page=2 crop=1 default_size=1}}
                  {{thumbnail profile=$_profile document=$profile.file page=3 crop=1 default_size=1}}
                  {{thumbnail profile=$_profile document=$profile.file page=4 crop=1 default_size=1}}
                  {{thumbnail profile=$_profile document=$profile.file page=5 crop=1 default_size=1}}
              </fieldset>
            </td>
          </tr>
        {{/foreach}}
    </table>
  </div>
</div>

<div id="thumbnail-rotation" style="display: none">
  <table class="main form">
    <tr>
      <td>
        <fieldset>
          <legend>Thumbnail- Sans rotation</legend>
            {{foreach from=$profile.profiles item=_profile}}
                {{thumbnail profile=$_profile document=$profile.file rotate=0 page=1 default_size=1}}
            {{/foreach}}
        </fieldset>
      </td>
    </tr>
    <tr>
      <td>
        <fieldset>
          <legend>Thumbnail- Rotation de 90�</legend>
            {{foreach from=$profile.profiles item=_profile}}
                {{thumbnail profile=$_profile document=$profile.file rotate=90 page=1 default_size=1}}
            {{/foreach}}
        </fieldset>
      </td>
    </tr>
    <tr>
      <td>
        <fieldset>
          <legend>Thumbnail- Rotation de 180�</legend>
            {{foreach from=$profile.profiles item=_profile}}
                {{thumbnail profile=$_profile document=$profile.file rotate=180 page=1 default_size=1 default_size=1}}
            {{/foreach}}
        </fieldset>
      </td>
    </tr>
    <tr>
      <td>
        <fieldset>
          <legend>Thumbnail- Rotation de 270�</legend>
            {{foreach from=$profile.profiles item=_profile}}
                {{thumbnail profile=$_profile document=$profile.file rotate=270 page=1 default_size=1}}
            {{/foreach}}
        </fieldset>
      </td>
    </tr>
  </table>
</div>

<div id="thumbnail-quality" style="display: none">
  <table class="main tbl">
    <tr>
      <td>
        <fieldset>
          <legend>20%</legend>
          <legend id="file_20_legend"></legend>
            {{thumbnail id=file_20 document=$quality.file profile=large default_size=1 quality=low}}
        </fieldset>
      </td>
      <td>
        <fieldset>
          <legend>50%</legend>
          <legend id="file_50_legend"></legend>
            {{thumbnail id=file_50 document=$quality.file profile=large default_size=1 quality=medium}}
        </fieldset>
      </td>
    </tr>
    <tr>
      <td>
        <fieldset>
          <legend>80%</legend>
          <legend id="file_80_legend"></legend>
            {{thumbnail id=file_80 document=$quality.file profile=large default_size=1 quality=high}}
        </fieldset>
      </td>
      <td>
        <fieldset>
          <legend>100%</legend>
          <legend id="file_100_legend"></legend>
            {{thumbnail id=file_100 document=$quality.file profile=large default_size=1 quality=full}}
        </fieldset>
      </td>
    </tr>
  </table>
</div>

