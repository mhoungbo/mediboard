{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create('tabs-db-checks', true, {
      afterChange: function (container) {
        let route_path;
        switch (container.id) {
          case 'tables':
            route_path = '{{$url_class_specs}}';
            break;
          case 'indexes':
            route_path = '{{$url_indexes}}';
            break;
          case 'tables-integrity':
            route_path = '{{$url_integrity}}';
            break;
          case 'tables-row-sizes':
            route_path = '{{$url_row_sizes}}';
            break;
        }

        new Url().setRoute(route_path).requestUpdate(container);
      }
    });
  });
</script>

<ul id="tabs-db-checks" class="control_tabs">
  <li><a href="#tables">{{tr}}mod-dPdeveloppement-tab-developpement_database_class_spec{{/tr}}</a></li>
  <li><a href="#indexes">{{tr}}mod-dPdeveloppement-tab-developpement_database_indexes{{/tr}}</a></li>
  <li><a href="#tables-integrity">{{tr}}mod-dPdeveloppement-tab-developpement_database_tables_integrity{{/tr}}</a></li>
  <li><a href="#tables-row-sizes">{{tr}}mod-dPdeveloppement-tab-developpement_database_check_row_sizes{{/tr}}</a></li>
</ul>

<div id="tables" style="display: none;"></div>
<div id="indexes" style="display: none;"></div>
<div id="tables-integrity" style="display: none;"></div>
<div id="tables-row-sizes" style="display: none;"></div>
