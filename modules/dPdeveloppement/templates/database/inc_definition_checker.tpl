{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main">
  <tr>
    <td colspan="2">
      <table class="tbl">
        <tr>
          <th rowspan="2">Champ</th>
          <th rowspan="2">Spec object</th>
          <th colspan="8">Base de donn�es</th>
        </tr>
        <tr>
          <th>Type</th>
          <th>Default</th>
          <th>Index</th>
          <th>Extra</th>
          <th class="narrow">Row Size</th>
        </tr>

          {{assign var=idx value=0}}
          {{foreach from=$list_classes key=_class item=_class_details}}
              {{assign var=idx value=$idx+1}}
              {{if $list_errors.$_class || $list_classes|@count == 1}}
                <tr>
                  <th colspan="11" class="title">
                      {{if $_class_details.suggestion}}
                        <button id="sugg-{{$idx}}-trigger" class="edit" style="float: left;"
                                onclick="$('sugg-{{$idx}}').toggle()">
                            {{tr}}Suggestion{{/tr}}
                        </button>
                      {{/if}}
                      {{$_class}}
                      {{if $_class_details.fields|@count > 100}}
                        <span class="error me-float-right">
                    {{tr}}TableIntegrity-msg-error-Too many fields{{/tr}}: {{$_class_details.fields|@count}}
                  </span>
                      {{/if}}
                  </th>
                </tr>
                  {{if $_class_details.suggestion}}
                    <tr id="sugg-{{$idx}}" style="display: none;">
                      <td colspan="100">
                        <pre>{{$_class_details.suggestion}}</pre>
                      </td>
                    </tr>
                  {{/if}}

                  {{foreach from=$_class_details.fields key=_field_name item=_field}}

                      {{if $list_errors.$_class && $_field_name|array_key_exists:$list_errors.$_class && $list_errors.$_class.$_field_name || $_class_details.key == $_field_name || $class == $_class}}
                        <tr>
                          <td {{if $_class_details.key == $_field_name}}class="ok"
                              data-type="key"{{/if}}>{{$_field_name}}</td>

                            {{if !'spec'|array_key_exists:$_field.object}}
                              <td class="warning text">Aucune d�finition de propri�t�</td>
                            {{else}}
                              <td class="text" title="{{$_field.object.spec}}">
                                  {{$_field.object.spec|replace:"|":" | "}}
                              </td>
                            {{/if}}

                          <td class="text">
                              {{if $_field.object.db_spec}}
                                  {{$_field.object.db_spec.type}}

                                  {{if is_array($_field.object.db_spec.params) && $_field.object.db_spec.params|@count > 0}}
                                    (
                                      {{foreach from=$_field.object.db_spec.params item=param name=params}}
                                          {{$param}}{{if !$smarty.foreach.params.last}},{{/if}}
                                      {{/foreach}}
                                    )
                                  {{/if}}

                                  {{if $_field.object.db_spec.unsigned}}UNSIGNED{{/if}}
                                  {{if $_field.object.db_spec.zerofill}}ZEROFILL{{/if}}

                                  {{if !$_field.object.db_spec.null}}NOT NULL{{/if}}
                                  {{if $_field.object.db_spec.default !== null}}DEFAULT {{$_field.object.db_spec.default}}{{/if}}
                              {{else}}
                                <div class="error">
                                  Pas de spec pour cette colonne
                                </div>
                              {{/if}}
                            &nbsp;
                            <hr style="border: 0; border-top: 1px solid #CCC; margin: 1px;"/>

                              {{if !$_class_details.no_table}}
                                  {{if $_field.db}}
                                    <span {{if $_field.db.type != $_field.object.db_spec.type}}class="warning"{{/if}}>
                      {{$_field.db.type}}
                    </span>
                                    <span {{if $_field.db.params != $_field.object.db_spec.params}}class="warning"{{/if}}>
                      {{if is_array($_field.db.params) && $_field.db.params|@count > 0}}
                        (
                          {{foreach from=$_field.db.params item=param name=params}}
                              {{$param}}{{if !$smarty.foreach.params.last}},{{/if}}
                          {{/foreach}}
                        )
                      {{/if}}
                    </span>
                                    <span {{if $_field.db.unsigned != $_field.object.db_spec.unsigned}}class="warning"{{/if}}>
                      {{if $_field.db.unsigned}}UNSIGNED{{/if}}
                    </span>
                                    <span {{if $_field.db.zerofill != $_field.object.db_spec.zerofill}}class="warning"{{/if}}>
                      {{if $_field.db.zerofill}}ZEROFILL{{/if}}
                    </span>
                                    <span {{if $_field.db.null != $_field.object.db_spec.null}}class="warning"{{/if}}>
                      {{if !$_field.db.null}}NOT NULL{{/if}}
                    </span>
                                    <span {{if $_field.db.default != $_field.object.db_spec.default}}class="warning"{{/if}}>
                      {{if $_field.db.default !== null && $_field.db.default !== ''}}DEFAULT {{$_field.db.default}} {{/if}}
                    </span>
                                  {{else}}
                                    <div class="error">
                                      Pas de colonne pour cette spec
                                    </div>
                                  {{/if}}
                              {{else}}
                                <div class="error">
                                  Pas de table existante pour cette classe
                                </div>
                              {{/if}}
                          </td>

                          <td>
                              {{$_field.object.db_spec.default}}&nbsp;<hr
                              style="border: 0; border-top: 1px solid #CCC; margin: 1px;"/>
                            <span {{if $_field.db.default != $_field.object.db_spec.default}}class="warning"{{/if}}>
                  {{$_field.db.default}}&nbsp;
                </span>
                          </td>

                          <td>
                              {{if $_field.object.db_spec.index}}
                              {{if $_field.object.db_spec.index !== '1'}}
                                Multi({{$_field.object.db_spec.index}})
                              {{else}}
                                Oui
                              {{/if}}
                              {{else}}
                            Non
                              {{/if}}&nbsp;<hr style="border: 0; border-top: 1px solid #CCC; margin: 1px;"/>
                            <span
                  {{if $_field.object.db_spec.index && !$_field.db.index && (!$_field.object.spec || !strpos($_field.object.spec, 'index|0'))}}
                                {{if $_field.object.spec  && strpos($_field.object.spec,'ref') === 0}}
                                  class="error"
                                {{else}}
                                  class="warning"
                                {{/if}}
                                    {{else}}
                                {{if !$_field.object.db_spec.index && $_field.db.index}}
                                  class="ok"
                                {{elseif 'db_spec'|array_key_exists:$_field.object && strpos($_field.object.db_spec.index, ', ') !== false && $_field.object.db_spec.index !== $_field.db.index}}
                                  class="warning"
                                {{/if}}
                                    {{/if}}>
                  {{if $_field.db.index}}
                                {{if strpos($_field.db.index, ', ') !== false}}
                                  Multi({{$_field.db.index}})
                                {{else}}
                                  Oui
                                {{/if}}
                                {{else}}
                    Non
                  {{/if}}&nbsp;
                </span>
                          </td>

                          <td>
                              {{$_field.object.db_spec.extra}}&nbsp;<hr
                              style="border: 0; border-top: 1px solid #CCC; margin: 1px;"/>
                            <span {{if $_field.db.extra != $_field.object.db_spec.extra}}class="warning"{{/if}}>
                  {{$_field.db.extra}}&nbsp;
                </span>
                          </td>
                          <td class="me-text-align-right">
                              {{if 'row_size'|array_key_exists:$_field.db && $_field.db.row_size}}
                                &nbsp;
                                <hr style="border: 0; border-top: 1px solid #CCC; margin: 1px;"/>
                                  {{$_field.db.row_size}}
                              {{/if}}
                          </td>
                        </tr>
                      {{/if}}
                  {{/foreach}}

                  {{foreach from=$_class_details.fulltext_indexes item=_fulltext_index}}
                    <tr>
                      <td class="text info">
                          {{$_fulltext_index.name}}
                      </td>
                      <td></td>
                      <td class="text">
                        FULLTEXT INDEX (
                          {{foreach from=$_fulltext_index.required_fields key=_fulltext_index_key item=_fulltext_index_field}}
                              {{if $_fulltext_index_key != 0}},{{/if}}
                              {{$_fulltext_index_field}}
                          {{/foreach}}
                        )
                        <hr style="border: 0; border-top: 1px solid #CCC; margin: 1px;"/>
                          {{if $_fulltext_index.indexed_fields|@count > 0}}
                            FULLTEXT INDEX (
                              {{foreach from=$_fulltext_index.indexed_fields key=_fulltext_index_key item=_fulltext_index_field}}
                                  {{if $_fulltext_index_key != 0}},{{/if}}
                                  {{$_fulltext_index_field}}
                              {{/foreach}}
                            )
                          {{else}}
                            <span class="error">NO FULLTEXT INDEX</span>
                          {{/if}}
                      </td>
                      <td></td>
                      <td>
                          {{if $_fulltext_index.status === "valid"}}
                            <span class="ok">Oui</span>
                          {{elseif $_fulltext_index.status === "missing"}}
                            <span class="error">Non</span>
                          {{elseif $_fulltext_index.status === "incomplete"}}
                            <span class="error">Incomplet</span>
                          {{/if}}
                      </td>
                      <td></td>
                    </tr>
                  {{/foreach}}
              {{/if}}
          {{/foreach}}
      </table>
    </td>
  </tr>
</table>
