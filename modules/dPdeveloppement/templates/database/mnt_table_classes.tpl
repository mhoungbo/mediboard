{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=system script=class_indexer ajax=true}}

<script>
  Main.add(function () {
    const form = getForm('mntTable');
    ClassIndexer.autocomplete(form.autocomplete_input, form.object_class, {profile: 'full'});
  });

  submitFormTable = function () {
    var types = '';
    $$('input.enum_type').each(function (elt) {
      if (elt.checked) {
        if (types) {
          types += '|' + elt.value;
        } else {
          types = elt.value;
        }
      }
    });

    var form = getForm('mntTable');
    $V(form.elements.types, types);

    form.onsubmit();
  };

  resetClassChoice = function () {
    const form = getForm('mntTable');
    $V(form.class, '');
    $V(form.autocomplete_input, '');
  }
</script>

<div id="mnt-table-classes">
  <table class="main">
    <tr>
      <td style="text-align: center;">
        <form name="mntTable" method="get" onsubmit="return onSubmitFormAjax(this, null, 'result-mnt-table-classes');">
          <input type="hidden" name="@route" value="{{$url_check_class_specs}}"/>
          <input type="hidden" name="types" value=""/>

          <label for="module" title="Veuillez slectionner un module">Choix du module</label>
          <select class="str" name="module">
            <option value="">&mdash; Liste des erreurs</option>
              {{foreach from=$installed_modules item=_module}}
                <option value="{{$_module->mod_name}}">
                    {{$_module->mod_name}} - {{tr}}{{$_module}}{{/tr}}
                </option>
              {{/foreach}}
          </select>
          <br/>

          <label for="class" title="Veuillez slectionner une classe">Choix de la classe</label>
          <input type="text" name="autocomplete_input" size="40" value=""/>
          <input type="hidden" name="class" id="object_class" value=""/>
          <button type="button" class="erase notext" onclick="resetClassChoice()">{{tr}}Erase{{/tr}}</button>
          <br/>

            {{foreach from=$types item=type}}
              <input type="checkbox" name="enum_types[]" class="enum_type" value="{{$type}}"
                     checked="checked"/>{{$type}}
            {{/foreach}}
          <br/>

          <button name="button" class="search me-primary" onclick="submitFormTable();">Filtrer</button>
        </form>
      </td>
    </tr>

    <tr>
      <td>
        <div class="big-info">Pour chaque sp�cification de propri�t� :
          <ul>
            <li><strong>la premi�re ligne</strong> correspond au mapping objet => relationnel th�orique,</li>
            <li><strong>la deuxi�me ligne </strong>correspond � ce qui est r�ellement pr�sent dans la base de donn�es.
            </li>
          </ul>
        </div>
      </td>
    </tr>
  </table>
</div>

<div id="result-mnt-table-classes"></div>
