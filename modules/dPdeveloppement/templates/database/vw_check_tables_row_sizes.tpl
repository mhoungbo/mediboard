{{*
 * @package Mediboard\Developpement
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=dPdeveloppement script=data_model ajax=true}}

<script>
  Main.add(function () {
    $('check-tables-row-sizes-results-container').fixedTableHeaders();
    dataModel.pop_detail_path = '{{$url_details_class}}'
  });
</script>

<div class="small-info">
  {{tr}}mod-dPdeveloppement-msg-info-row size limit{{/tr}}&nbsp;:<b>{{$threshold}}</b>
</div>

{{if $error_count}}
  <div class="small-error">
    {{tr var1=$error_count var2=$threshold}}mod-dPdeveloppement-msg-error-%s tables row size are over the limit of %s{{/tr}}
  </div>
{{/if}}

{{if $warning_count}}
  <div class="small-warning">
    {{tr var1=$warning_count var2=$threshold}}mod-dPdeveloppement-msg-warning-%s tables row size are close to the limit of %s{{/tr}}
  </div>
{{/if}}

<div id="check-tables-row-sizes-results-container">
  <table class="main tbl">
    <thead>
      <tr>
        <th class="narrow me-text-align-center">%</th>
        <th class="narrow">Row size</th>
        <th class="narrow">Champs</th>
        <th>
          Table
          <input type="search" onkeyup="dataModel.filter(this, '.check-tables-row-sizes-line .table-name', 'check-tables-row-sizes-lines');"/>
        </th>
      </tr>
    </thead>
    <tbody id="check-tables-row-sizes-lines">
      {{foreach from=$data key=key item=item}}
        <tr id="check-tables-row-sizes-line-{{$key}}" class="check-tables-row-sizes-line">
          <td>
            {{if $item.status == 'ok'}}
              {{$item.ratio}}%
            {{else}}
              <b>{{$item.ratio}} %</b>
            {{/if}}
          </td>
          <td class="{{$item.status}} me-text-align-right">
            {{$item.row_size}}
          </td>
          <td class="{{if $item.columns_count > 200}}error{{elseif $item.columns_count > 100}}warning{{else}}ok{{/if}} me-text-align-right">
            {{$item.columns_count}}
          </td>
          <td>
            <span class="table-name">
              {{if $item.class}}
                <a style="cursor: pointer; color: #3F51B5"
                   onclick="dataModel.modal_details('{{$item.class}}', '1', '0', '0', '0', '0');">
                  {{$item.table_name}}
                </a>
              {{else}}
                  {{$item.table_name}}
              {{/if}}
            </span>
          </td>
        </tr>
      {{/foreach}}
    </tbody>
  </table>
</div>
