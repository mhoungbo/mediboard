{{*
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<div>
  <h1>Welcome to sandbox</h1>
  <p>Bienvenue dans la sandbox, ici vous pouvez tester votre d�veloppement. Attention � ne pas commit vos modifications !</p>
  <ul>
    <li><strong>Contr�leur :</strong> SandboxController::sandbox</li>
    <li><strong>Template :</strong> sandbox.tpl</li>
    <li><strong>VueJS :</strong> DeveloppementSandbox.vue</li>
  </ul>
  {{mb_entry_point entry_point=$sandbox}}
</div>
