{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{if $nb_logs > 0 }}
  <div class="divInfosLog">{{$nb_logs}} logs (obtenus en {{$exec_time}} ms)</div>
  <table class="table_log">
      {{foreach from=$logs item=_log}}
        <tr class="tr_log" style="color:{{$_log.color}}"
            onclick="ApplicationLogs.jsonViewer({{$_log.context_json}}, {{$_log.extra_json}})">
          <td style="white-space: nowrap;">{{$_log.date|html_entity_decode}}</td>
          <td>{{$_log.level|html_entity_decode}}</td>
          <td style="width:100%;">{{$_log.message|html_entity_decode}}</td>
          <td>{{$_log.context|html_entity_decode}}</td>
          <td>{{$_log.extra|html_entity_decode}}</td>
        </tr>
      {{/foreach}}
  </table>
  <div style="display: none;margin:10px;" id="application_log_modal">
    <b>Context: </b><span id="application_log_modal_context"></span>
    <br>
    <b>Extra: </b><span id="application_log_modal_extra"></span>
  </div>
{{/if}}

{{if $nb_logs == 1000 }}
  <div class="divShowMoreLog" onclick="ApplicationLogs.showMoreLog(this)"><i class="fas fa-arrow-circle-down"></i>
    Afficher plus de logs ...
  </div>
{{else}}
  <div class="divInfosLog">Tous les logs ont �t� charg�s</div>
{{/if}}
