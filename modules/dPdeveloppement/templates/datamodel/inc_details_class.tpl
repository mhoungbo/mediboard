{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main tbl" name="details_class">
  <tr>
    <th colspan="6">
        {{$data_model->class_select}} ({{tr}}{{$data_model->class_select}}{{/tr}})
    </th>
  </tr>

    {{if $data_model->show_refs}}
      <tr>
        <th colspan="6" class="section">{{tr}}ModelGraph-references{{/tr}} ({{$refs|@count}})</th>
      </tr>
      <tr>
        <th>{{tr}}ModelGraph-property-name{{/tr}}</th>
        <th>{{tr}}ModelGraph-property-name-desc{{/tr}}</th>
        <th>{{tr}}ModelGraph-description{{/tr}}</th>
        <th>{{tr}}ModelGraph-type{{/tr}}</th>
        <th colspan="2">{{tr}}ModelGraph-type-bdd{{/tr}}</th>
      </tr>
      <tr>
        {{foreach from=$refs key=_key item=_prop}}
          <tr>
            <td>
                {{$_key}}
            </td>
            <td class="text">
                {{tr}}{{$data_model->class_select}}-{{$_key}}{{/tr}}
            </td>
            <td class="text compact">
                {{tr}}{{$data_model->class_select}}-{{$_key}}-desc{{/tr}}
            </td>
            <td class="text">
                {{$_prop|smarty:nodefaults}}
            </td>
            <td class="text me-text-align-right" colspan="2">
                {{$db_spec[$_key]}}
            </td>
          </tr>
        {{/foreach}}
    {{/if}}
    {{if $data_model->show_properties}}
      <tr>
        <th colspan="6" class="section">{{tr}}ModelGraph-properties{{/tr}} ({{$plainfield|@count}})</th>
      </tr>
      <tr>
        <th>{{tr}}ModelGraph-property-name{{/tr}}</th>
        <th>{{tr}}ModelGraph-property-name-desc{{/tr}}</th>
        <th>{{tr}}ModelGraph-description{{/tr}}</th>
        <th>{{tr}}ModelGraph-type{{/tr}}</th>
        <th>{{tr}}ModelGraph-type-bdd{{/tr}}</th>
        <th class="me-text-align-right">{{tr}}ModelGraph-row-size-bdd{{/tr}} ({{$db_table_row_size}})</th>
      </tr>
      <tr>
        {{foreach from=$plainfield key=_key item=_prop}}
          <tr>
            <td>
                {{$_key}}
            </td>
            <td class="text">
                {{tr}}{{$data_model->class_select}}-{{$_key}}{{/tr}}
            </td>
            <td class="text compact">
                {{tr}}{{$data_model->class_select}}-{{$_key}}-desc{{/tr}}
            </td>
            <td class="text">
                {{$_prop|smarty:nodefaults}}
            </td>
            <td class="text">
                {{$db_spec[$_key]}}
            </td>
            <td class="text me-text-align-right">
                {{$db_table_columns_sizes[$_key]}}
            </td>
          </tr>
        {{/foreach}}
    {{/if}}
    {{if $data_model->show_formfields}}
      <tr>
        <th colspan="6" class="section">{{tr}}ModelGraph-form_fields{{/tr}} ({{$formfield|@count}})</th>
      </tr>
      <tr>
        <th>{{tr}}ModelGraph-property-name{{/tr}}</th>
        <th>{{tr}}ModelGraph-property-name-desc{{/tr}}</th>
        <th>{{tr}}ModelGraph-description{{/tr}}</th>
        <th colspan="3">{{tr}}ModelGraph-type{{/tr}}</th>
      </tr>
        {{foreach from=$formfield key=_key item=_prop}}
          <tr>
            <td>
                {{$_key}}
            </td>
            <td class="text">
                {{tr}}{{$data_model->class_select}}-{{$_key}}{{/tr}}
            </td>
            <td class="text compact">
                {{tr}}{{$data_model->class_select}}-{{$_key}}-desc{{/tr}}
            </td>
            <td class="text" colspan="3">
                {{$_prop|smarty:nodefaults}}
            </td>

          </tr>
        {{/foreach}}
    {{/if}}
    {{if $data_model->show_heritage}}
      <tr>
        <th colspan="6" class="section">{{tr}}ModelGraph-herited_fields{{/tr}} ({{$heritage|@count}})</th>
      </tr>
      <tr>
        <th>{{tr}}ModelGraph-property-name{{/tr}}</th>
        <th>{{tr}}ModelGraph-property-name-desc{{/tr}}</th>
        <th>{{tr}}ModelGraph-description{{/tr}}</th>
        <th colspan="3">{{tr}}ModelGraph-type{{/tr}}</th>
      </tr>
        {{foreach from=$heritage key=_key item=_heritage}}
          <tr>
            <td>
                {{$_key}}
            </td>
            <td class="text">
                {{tr}}{{$data_model->class_select}}-{{$_key}}{{/tr}}
            </td>
            <td class="text compact">
                {{tr}}{{$data_model->class_select}}-{{$_key}}-desc{{/tr}}
            </td>
            <td class="text" colspan="3">
                {{$_heritage|smarty:nodefaults}}
            </td>
          </tr>
        {{/foreach}}
    {{/if}}
    {{if $data_model->show_backs}}
      <tr>
        <th colspan="6" class="section">{{tr}}ModelGraph-collections{{/tr}} ({{$backprops|@count}})</th>
      </tr>
      <tr>
        <th colspan="3">{{tr}}ModelGraph-property-name{{/tr}}</th>
        <th colspan="3">{{tr}}ModelGraph-type{{/tr}}</th>
      </tr>
        {{foreach from=$backprops key=_key_back item=_back}}
          <tr>
            <td colspan="3">
                {{$_key_back}}
            </td>
            <td class="text" colspan="3">
                {{$_back}}
            </td>
          </tr>
        {{/foreach}}
    {{/if}}
</table>
