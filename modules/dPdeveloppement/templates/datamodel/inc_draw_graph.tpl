{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=dPdeveloppement script=data_model ajax=true}}

<script>
  Main.add(function () {
    dataModel.pop_detail_path = '{{$url_class_details}}';

    var graph = {{$graph|@json}};
    var backs = {{$backs|@json}};
    var backs_name = {{$backs_name|@json}};
    var opt = {
      hierarchy_sort: "{{$hierarchy_sort}}",
      show_hover:     "{{$show_hover}}"
    };
    var inv_class = {{$inv_class|@json}};
    dataModel.prepareGraph(graph, backs, backs_name, inv_class, opt);
  });
</script>
