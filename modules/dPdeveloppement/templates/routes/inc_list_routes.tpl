{{*
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create("tab-routes-list", true);
    $('routes-count').innerHTML = "({{$count_total}})";
  });
</script>

<form name="search" method="get" onsubmit="return onSubmitFormAjax(this, null, 'routes')">
    {{mb_route name=developpement_routes_list}}
  <input type="text" placeholder="Filtrer par module, route ..." size="50" name="filter_module" id="filter_module"
         value="{{$filter_module}}">
  <button type="button" onclick="$V(this.form.filter_module, '');" class="erase notext" title="Vider le champ"></button>

  <button type="submit" class="search"> {{tr}}Search{{/tr}}</button>
</form>

<table class="main">
  <tr>
    <td class="narrow">
      <ul id="tab-routes-list" class="control_tabs_vertical small" style="width:20em;">
        <li>
          <a href="#routes-list" style="line-height: 24px;">
            Routes
              {{if $count_total > 0}}
                  {{math equation='(x/y)*100' assign=ratio x=$count_routes y=$count_total}}
              {{else}}
                  {{assign var=ratio value='0'}}
              {{/if}}

            <small>({{$count_routes}} - {{$ratio|number_format:0:'.':' '}}%)</small>
          </a>
        </li>
        <li>
          <a href="#legacy-actions" style="line-height: 24px;">
            LegacyController
              {{if $count_total > 0}}
                  {{math equation='(x/y)*100' assign=ratio x=$count_actions y=$count_total}}
              {{else}}
                  {{assign var=ratio value='0'}}
              {{/if}}

            <small>({{$count_actions}} - {{$ratio|number_format:0:'.':' '}}%)</small>
          </a>
        </li>
        <li>
          <a href="#legacy-scripts" style="line-height: 24px;">
            modules/*/scripts.php
              {{if $count_total > 0}}
                  {{math equation='(x/y)*100' assign=ratio x=$count_scripts y=$count_total}}
              {{else}}
                  {{assign var=ratio value='0'}}
              {{/if}}

            <small>({{$count_scripts}} - {{$ratio|number_format:0:'.':' '}}%)</small>
          </a>
        </li>
        <li>
          <a href="#legacy-dosql" style="line-height: 24px;">
            controllers/dosql.php
              {{if $count_total > 0}}
                  {{math equation='(x/y)*100' assign=ratio x=$count_dosql y=$count_total}}
              {{else}}
                  {{assign var=ratio value='0'}}
              {{/if}}

            <small>({{$count_dosql}} - {{$ratio|number_format:0:'.':' '}}%)</small>
          </a>
        </li>
      </ul>
    </td>
    <td>

      <div id="routes-list">
        <table class="tbl">
          <tr>
            <th class="title">Module</th>
            <th class="title">Controller</th>
            <th class="title">Route</th>
            <th class="title">Action</th>
            <th class="title">Path</th>
            <th class="title">Method</th>
          </tr>
            {{url var=urlBase name=developpement_routes_show route_name=placeholder}}
            {{foreach from=$route_display item=_datas key=_name}}
              <tr
                onclick="new Url().setRoute('{{$urlBase|replace:'placeholder':$_name}}').requestModal(800,600, {title : '{{$_name}}' });">
                <td>{{mb_ditto name=module-routes-list  value=$_datas.module}}</td>
                <td>{{mb_ditto name=controller-routes-list value=$_datas.controller}}</td>
                <td>{{$_name}}</td>
                <td>{{$_datas.action}}</td>
                <td>{{$_datas.path}}</td>
                <td>{{$_datas.method}}</td>
              </tr>
            {{/foreach}}
        </table>
      </div>

      <div id="legacy-actions" style="display: none;">
        <table class="tbl">
          <tr>
            <th class="title">Module</th>
            <th class="title">LegacyController</th>
            <th class="title">Action</th>
          </tr>
            {{foreach from=$actions item=_datas key=_module}}
                {{foreach from=$_datas item=_controller key=_action}}
                  <tr>
                    <td>{{mb_ditto name=module-legacy-actions value=$_module}}</td>
                    <td>{{mb_ditto name=controller-legacy-actions value=$_controller}}</td>
                    <td>{{$_action}}</td>
                  </tr>
                {{/foreach}}
            {{/foreach}}
        </table>
      </div>


      <div id="legacy-scripts" style="display: none;">
        <table class="tbl">
          <tr>
            <th class="title">Module</th>
            <th class="title">script.php</th>
          </tr>
            {{foreach from=$scripts item=_datas key=_module}}
                {{foreach from=$_datas item=_script}}
                  <tr>
                    <td>{{mb_ditto name=module-scripts value=$_module}}</td>
                    <td>{{$_script}}</td>
                  </tr>
                {{/foreach}}
            {{/foreach}}
        </table>
      </div>

      <div id="legacy-dosql">
        <table class="tbl">
          <tr>
            <th class="title">Module</th>
            <th class="title">controllers/dosql.php</th>
          </tr>
            {{foreach from=$dosql_scripts item=_datas key=_module}}
                {{foreach from=$_datas item=_script}}
                  <tr>
                    <td>{{mb_ditto name=module-dosql value=$_module}}</td>
                    <td>{{$_script}}</td>
                  </tr>
                {{/foreach}}
            {{/foreach}}
        </table>
      </div>

    </td>
  </tr>
</table>
