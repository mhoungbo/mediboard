{{*
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create('main_tab_group', true, {
      afterChange: function (container) {
        if (container.id === 'routes') {
          new Url().setRoute('{{url name=developpement_routes_list}}').requestUpdate(container.id);
        }
      }
    });
  });
</script>

<style>
  #stat table tr:not(:first-of-type) th {
    text-align: left;
    width: 150px;
  }


  #stat table td {
    vertical-align: top;
  }

  #routes table {
    margin-top: 15px;
  }

  #routes table tr:hover {
    cursor: pointer;
  }

</style>

<ul id="main_tab_group" class="control_tabs">
  <li><a href="#stat">Statistiques</a></li>
  <li><a href="#routes">Routes<span id="routes-count"></span></a></li>
</ul>
<br>
<div id="stat">
  <table class="tbl main">
    <tr>
      <th class="title" colspan="2">
          {{$file}}
      </th>
    </tr>
    <tr>
      <th>Date de cr�ation</th>
      <td>{{$file_date|date_format:$conf.datetime}}</td>
    </tr>
    <tr>
      <th>Size</th>
      <td>{{$file_size}}</td>
    </tr>
    <tr>
      <th>Routes</th>
      <td>{{$routes_count|number_format:0:'.':' '}}</td>
    </tr>
  </table>
  <table class="tbl main">
    <tr>
      <th class="title" colspan="2">
          {{$root_dir}}/modules/*/config/routes/*.yml
      </th>
    </tr>
      {{foreach from=$ressources key=_key item=_ressource}}
        <tr>
          <th style="vertical-align: top">{{$_key}}</th>
          <td>
              {{foreach from=$_ressource item=_file}}
                  {{$_file}}
                <br>
              {{/foreach}}
          </td>
        </tr>
      {{/foreach}}
  </table>
</div>

<div id="routes">
  <!-- AJAX -->
</div>
