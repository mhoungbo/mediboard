{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<ul>
    {{foreach from=$routes item=route}}
      <li data-route_name="{{$route->getRouteName()}}">
        <div class="RouteAutocomplete">
          <div class="RouteAutocomplete-section" title="{{$route->getRouteName()}}
{{$route->getMethods()}} {{$route->getPath()}}
{{$route->getController()}}::{{$route->getAction()}}">
            <div class="RouteAutocomplete-title">
                {{$route->getRouteName()|emphasize:$search:'mark'}}
                {{if $only_offline}}
                  ({{tr}}mod-{{$route->getModule()}}-tab-{{$route->getRouteName()}}{{/tr}})
                {{/if}}
            </div>

            <div class="RouteAutocomplete-subtitle">
                {{$route->getMethods()|emphasize:$search:'mark'}} {{$route->getPath()|emphasize:$search:'mark'}}
            </div>
            <div class="RouteAutocomplete-subtitle">
                {{$route->getController()|emphasize:$search:'mark'}}::{{$route->getAction()|emphasize:$search:'mark'}}
            </div>
          </div>
        </div>
      </li>
        {{foreachelse}}
      <li class="empty">{{tr}}RoutesController-Empty route list{{/tr}}</li>
    {{/foreach}}
</ul>
