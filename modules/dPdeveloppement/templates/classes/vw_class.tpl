{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create('main_tab_group', true, {
      afterChange: function (container) {
        if (container === map) {
          new Url()
            .setRoute('{{url name=developpement_classes_classmap}}')
            .requestUpdate(container.id);
        }
      }
    });
  });
</script>

<style>
  #stat table tr:not(:first-of-type) th {
    text-align: left;
    width: 150px;
  }

  #stat table td {
    vertical-align: top;
  }

  #map table {
    margin-top: 15px;
  }

  #map table > tbody > tr:not(:first-of-type) td:hover {
    cursor: pointer;
  }
</style>

<ul id="main_tab_group" class="control_tabs">
  <li><a href="#stat">Statistiques</a></li>
  <li><a href="#module">Modules</a></li>
  <li><a href="#map">Class Map</a></li>
</ul>
<br>
<div id="stat">
  <table class="tbl main">
    <tr>
      <th class="title" colspan="2">
        Informations
      </th>
    </tr>
    <tr>
      <th>Fichier</th>
      <td>{{$classmap_file.file}}</td>
    </tr>
    <tr>
      <th>Date de cr�ation</th>
      <td>{{$classmap_file.date|date_format:$conf.datetime}}</td>
    </tr>
    <tr>
      <th>Size</th>
      <td>{{$classmap_file.size}}</td>
    </tr>
    <tr>
      <th>Lines</th>
      <td>{{$classmap_file.line|number_format:0:'.':' '}}</td>
    </tr>
    <tr>
      <th>Glob (pattern)</th>
      <td>
          {{foreach from=$classmap_file.dirs item=_dir}}
              {{$_dir}}
            <br>
          {{/foreach}}
      </td>
    </tr>
  </table>

  <br>

  <table class="tbl main">
    <tr>
      <th class="title" colspan="2">
        Classes
      </th>
    </tr>
    <tr>
      <th>Classes</th>
      <td>{{$count_classes.total|number_format:0:'.':' '}}</td>
    </tr>
    <tr>
      <th>Parents</th>
      <td>{{$count_classes.parent|number_format:0:'.':' '}}</td>
    </tr>
    <tr>
      <th>Children</th>
      <td>{{$count_classes.children|number_format:0:'.':' '}}</td>
    </tr>
    <tr>
      <th>Interfaces</th>
      <td>{{$count_classes.interface|number_format:0:'.':' '}}</td>
    </tr>
    <tr>
      <th>Traits</th>
      <td>{{$count_classes.trait|number_format:0:'.':' '}}</td>
    </tr>
  </table>

  <br>

  <table class="tbl main">
    <tr>
      <th class="title" colspan="2">
        Namespaces
      </th>
    </tr>
      {{foreach from=$count_classes.namespaces item=_count key=_namespace}}
        <tr>
          <th>Ox\{{$_namespace}}</th>
          <td>{{$_count|number_format:0:'.':' '}}</td>
        </tr>
      {{/foreach}}
  </table>

  <br>

  <table class="tbl main">
    <tr>
      <th class="title" colspan="2">
        References
      </th>
    </tr>
    <tr>
      <th>Fichier</th>
      <td>{{$classref_file.file}}</td>
    </tr>
    <tr>
      <th>Date de cr�ation</th>
      <td>{{$classref_file.date|date_format:$conf.datetime}}</td>
    </tr>
    <tr>
      <th>Size</th>
      <td>{{$classref_file.size}}</td>
    </tr>
    <tr>
      <th>Lines</th>
      <td>{{$classref_file.line|number_format:0:'.':' '}}</td>
    </tr>
    <tr>
      <th>Classes</th>
      <td>{{$classref_file.keys|number_format:0:'.':' '}}</td>
    </tr>
  </table>
</div>

<div id="module">
  <table class="tbl main">
    <thead>
    <tr>
      <th class="narrow">Module ({{$modules|@count}})</th>
      <th class="narrow">Path</th>
      <th class="narrow">Prefix PSR-4</th>
      <th class="narrow">Classes ({{$total_classes}})</th>
    </tr>
    </thead>
      {{foreach from=$modules item=value key=module}}
        <tr>
          <td>{{$module}} ({{tr}}module-{{$module}}-court{{/tr}})</td>
          <td>{{$value.path}}</td>
          <td>{{$value.namespace}}</td>
          <td>{{$value.classes|number_format:0:'.':' '}}</td>
        </tr>
      {{/foreach}}
  </table>
  <br>
</div>

<div id="map"><!-- ajax --></div>
