{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}
<script>
  Classmap = {
    page: function (start) {
      var form = getForm('search-classmap');
      $V(form.start, start);
      form.onsubmit();
    },
  }
</script>

<table class="tbl main">
  <tr>
    <th class="narrow">Classes ({{$maps|@count}})</th>
    <th class="narrow">Files</th>
  </tr>
    {{mb_include module=system template=inc_pagination change_page=Classmap.page total=$total current=$start step=50}}
    {{url var=urlBase name=developpement_classes_show class_name='placeholder'}}

    {{foreach from=$maps item=_map key=class}}
      <tr
        onclick="new Url()
          .setRoute('{{$urlBase|replace:'placeholder':$_map.name_encoded}}')
          .requestModal(800,600, {title : '{{$_map.short_name}}'});"
      >
        <td><b>{{$class}}</b></td>
        <td>{{$_map.file}}</td>
      </tr>
        {{foreachelse}}
      <tr>
        <td colspan="2" class="empty">{{tr}}No result{{/tr}}</td>
      </tr>
    {{/foreach}}
</table>
