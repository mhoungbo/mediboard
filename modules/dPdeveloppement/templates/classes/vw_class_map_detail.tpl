{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<style>
  #map-detail pre {
    max-height: 100%;
  }
</style>

<div id="class_exist" style="margin: 10px;">
  <div class="small-{{$class_exists}}">{{$msg}}</div>
</div>

{{if $map}}
  <h3>Class Map:</h3>
  <div id="map-detail">
      {{$map|highlight:json|smarty:nodefaults}}
  </div>
{{/if}}

{{if $ref}}
  <h3>Class Ref:</h3>
  <div id="ref-detail">
      {{$ref|highlight:json|smarty:nodefaults}}
  </div>
{{/if}}

