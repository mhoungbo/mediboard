{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}
<script>
  Main.add(function () {
    const form = getForm('search-classmap');
    form.onsubmit();
  });
</script>

<div id="classmap-list">
  <form name="search-classmap" action="?" method="get"
        onsubmit="return onSubmitFormAjax(this, null, 'classmap-list-results')">
      {{mb_route name=developpement_classmap_list}}
    <input type="hidden" name="start" value="{{$start}}"/>

    <input type="text" placeholder="Classes ..." size="50" name="search" id="search">
    <button type="button" onclick="$V(this.form.search, '');" class="erase notext" title="Vider le champ"></button>

    <button type="submit" class="search"> Rechercher</button>
  </form>
</div>

<div id="classmap-list-results"></div>
