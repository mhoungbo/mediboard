<?php
$locales['ApplicationLog-First Log'] = 'Premier journal';
$locales['ApplicationLog-First log'] = 'Premier journal';
$locales['ApplicationLog-Last Log'] = 'Dernier journal';
$locales['ApplicationLog-Last log'] = 'Dernier journal';
$locales['ApplicationLog-Log number'] = 'Nombre de journaux';
$locales['CError-error-Failed to delete %s files'] = '�chec de la suppression de %s fichiers';
$locales['CError-error-Failed to process %s files'] = '�chec du traitement de %s fichiers';
$locales['CError-msg-%s files deleted'] = '%s fichiers supprim�s';
$locales['CError-msg-%s files processed'] = '%s fichiers trait�s';
$locales['CError-msg-%s rows deleted'] = '%s lignes supprim�es';
$locales['CError-msg-locked'] = 'Verrou pr�sent';
$locales['CErrorLog-msg-emptied errors logs'] = 'Suppression des journaux d\'erreur dans la base de donn�es';
$locales['CErrorLog-whitelist-remove'] = 'Supprimer de la liste blanche';
$locales['CErrorLog.delete_all_whitelist'] = 'Vider la liste blanche';
$locales['CErrorLog.delete_all_whitelist_ask'] = 'Confirmez-vous la suppression de la liste blanche ?';
$locales['CErrorLog.delete_whitelist'] = 'Supprimer de la liste blanche';
$locales['CErrorLog.error_log_whitelist_info'] = 'Liste blanche';
$locales['CErrorLog.error_type.errorException'] = 'ErrorException';
$locales['CErrorLog.errors'] = 'erreurs';
$locales['CErrorLog.whitelist_added'] = 'Ajout� � la liste blanche';
$locales['CErrorLog.whitelist_already_in'] = 'D�ja pr�sent dans la liste blanche';
$locales['CErrorLogWhiteList-error-missing id'] = 'Identifiant manquant pour cet �l�ment de whitelist';
$locales['CErrorLogWhiteList-msg-deleted'] = '�l�ment supprim�';
$locales['CErrorLogWhiteList-msg-emptied'] = 'Liste blanche vid�e';
$locales['CIntegrityError'] = 'Erreur d\'int�grit� r�f�rentielle';
$locales['CIntegrityError-Count used'] = 'Nombre de fois utilis�';
$locales['CIntegrityError-Missing guid'] = 'Guid manquants';
$locales['CIntegrityError-integrity_error_id'] = 'Identifiant';
$locales['CIntegrityError-integrity_error_id-court'] = 'ID';
$locales['CIntegrityError-integrity_error_id-desc'] = 'Identifiant de l\'erreur d\'int�grit� r�f�rentielle';
$locales['CIntegrityError-missing_id'] = 'Identifiant manquant';
$locales['CIntegrityError-missing_id-court'] = 'ID manquant';
$locales['CIntegrityError-missing_id-desc'] = 'Identifiant cible manquant';
$locales['CIntegrityError-msg-create'] = 'Erreur d\'int�grit� r�f�rentielle cr��e';
$locales['CIntegrityError-msg-delete'] = 'Erreur d\'int�grit� r�f�rentielle supprim�e';
$locales['CIntegrityError-msg-modify'] = 'Erreur d\'int�grit� r�f�rentielle modifi�e';
$locales['CIntegrityError-ref_progression_id'] = 'Progression';
$locales['CIntegrityError-ref_progression_id-court'] = 'Progression';
$locales['CIntegrityError-ref_progression_id-desc'] = 'Progression correspondant � l\'erreur d\'int�grit� r�f�rentielle';
$locales['CIntegrityError-title-create'] = 'Cr�ation d\'une erreur d"int�grit� r�f�rentielle';
$locales['CIntegrityError-title-modify'] = 'Modification d\'une erreur d\'int�grit� r�f�rentielle';
$locales['CIntegrityError.all'] = 'Toutes les erreurs d\'int�grit� r�f�rentielle';
$locales['CIntegrityError.none'] = 'Aucune erreur d\'int�grit� r�f�rentielle';
$locales['CIntegrityError.one'] = 'Une erreur d\'int�grit� r�f�rentielle';
$locales['CIntegrityErrors-Count-ids'] = 'Nombre d\'identifiants non trouv�s';
$locales['CMbDT-cp_holidays'] = 'Jours f�ri�s par r�gion/canton/etc.';
$locales['CMbDT-holidays'] = 'Jours f�ri�s';
$locales['CPermModule._owner.template'] = 'Profil';
$locales['CPermModule._owner.user'] = 'Utilisateur';
$locales['CRefCheckField'] = 'V�rificateur de champ';
$locales['CRefCheckField-Count errors'] = 'Nb erreurs';
$locales['CRefCheckField-_duration'] = 'Dur�e';
$locales['CRefCheckField-back-errors'] = 'Erreurs';
$locales['CRefCheckField-back-errors.empty'] = 'Aucune erreur';
$locales['CRefCheckField-back-target_classes'] = 'V�rificateur de champ';
$locales['CRefCheckField-back-target_classes.empty'] = 'Aucun v�rificateur de champ';
$locales['CRefCheckField-count_nulls'] = 'Nombre de nuls';
$locales['CRefCheckField-count_nulls-court'] = 'Nb nuls';
$locales['CRefCheckField-count_nulls-desc'] = 'Nombre de r�f�rences nulles';
$locales['CRefCheckField-count_rows'] = 'Nombre de lignes';
$locales['CRefCheckField-count_rows-court'] = 'Nb lignes';
$locales['CRefCheckField-count_rows-desc'] = 'Nombre de lignes dans la table';
$locales['CRefCheckField-end_date'] = 'Date de fin';
$locales['CRefCheckField-end_date-court'] = 'Date de fin';
$locales['CRefCheckField-end_date-desc'] = 'Date de fin de la v�rification';
$locales['CRefCheckField-field'] = 'Champ';
$locales['CRefCheckField-field-court'] = 'Champ';
$locales['CRefCheckField-field-desc'] = 'Champ v�rifi�';
$locales['CRefCheckField-last_id'] = 'Dernier ID';
$locales['CRefCheckField-last_id-court'] = 'Dernier ID';
$locales['CRefCheckField-last_id-desc'] = 'Dernier ID v�rifi�';
$locales['CRefCheckField-main_ref_check_field_id'] = 'V�rificateur de champ principal';
$locales['CRefCheckField-main_ref_check_field_id-court'] = 'V�rificateur de champ principal';
$locales['CRefCheckField-main_ref_check_field_id-desc'] = 'V�rificateur de champ principal utilis� pour les champs m�ta';
$locales['CRefCheckField-max_id'] = 'ID max';
$locales['CRefCheckField-max_id-court'] = 'ID max';
$locales['CRefCheckField-max_id-desc'] = 'Identifiant maximum';
$locales['CRefCheckField-msg-create'] = 'V�rificateur de champ cr��';
$locales['CRefCheckField-msg-delete'] = 'V�rificateur de champ supprim�';
$locales['CRefCheckField-msg-modify'] = 'V�rificateur de champ modifi�';
$locales['CRefCheckField-ref_check_field_id'] = 'ID';
$locales['CRefCheckField-ref_check_field_id-court'] = 'ID';
$locales['CRefCheckField-ref_check_field_id-desc'] = 'Identifiant du v�rificateur';
$locales['CRefCheckField-ref_check_table_id'] = 'V�rificateur de table li�';
$locales['CRefCheckField-ref_check_table_id-court'] = 'V�rificateur de table';
$locales['CRefCheckField-ref_check_table_id-desc'] = 'V�rificateur de table auquel appartient le v�rificateur de champ';
$locales['CRefCheckField-start_date'] = 'Date d� d�but';
$locales['CRefCheckField-start_date-court'] = 'Date de d�but';
$locales['CRefCheckField-start_date-desc'] = 'Date de d�but de la v�rification';
$locales['CRefCheckField-target_class'] = 'Classe cible';
$locales['CRefCheckField-target_class-court'] = 'Classe cible';
$locales['CRefCheckField-target_class-desc'] = 'Classe cible';
$locales['CRefCheckField-title-create'] = 'Cr�er un v�rificateur de champ';
$locales['CRefCheckField-title-modify'] = 'Modifier un v�rificateur de champ';
$locales['CRefCheckField.all'] = 'Tous les v�rificateurs de champ';
$locales['CRefCheckField.none'] = 'Aucun v�rificateur de champ';
$locales['CRefCheckField.one'] = 'Un v�rificateur de champ';
$locales['CRefCheckTable'] = 'V�rificateur de r�f�rences';
$locales['CRefCheckTable-Count missing id'] = 'Nombre d\'identifiants diff�rents manquants';
$locales['CRefCheckTable-Density'] = 'Densit�';
$locales['CRefCheckTable-State'] = 'Avancement';
$locales['CRefCheckTable-Total progression'] = 'Avancement total';
$locales['CRefCheckTable-_duration'] = 'Dur�e';
$locales['CRefCheckTable-_error_count'] = 'Nb erreurs';
$locales['CRefCheckTable-_error_count-desc'] = 'Nombre d\'erreurs';
$locales['CRefCheckTable-action-Fill ref table'] = 'Remplir la table de v�rification d\'int�grit�';
$locales['CRefCheckTable-action-Reset'] = 'R�initialiser le v�rificateur';
$locales['CRefCheckTable-action-Start'] = 'Commencer la v�rification';
$locales['CRefCheckTable-action-Stop'] = 'Arr�ter la v�rification';
$locales['CRefCheckTable-action-Stop auto refresh'] = 'Arr�ter le rafraichissement automatique';
$locales['CRefCheckTable-back-ref_fields'] = 'V�rificateur de r�f�rences';
$locales['CRefCheckTable-back-ref_fields.empty'] = 'Aucun v�rificateur de r�f�rences';
$locales['CRefCheckTable-chunks-size'] = 'Taille des lots � traiter';
$locales['CRefCheckTable-class'] = 'Classe v�rifi�e';
$locales['CRefCheckTable-class-court'] = 'Classe';
$locales['CRefCheckTable-class-desc'] = 'Classe v�rifi�e';
$locales['CRefCheckTable-count_rows'] = 'Nombre de lignes';
$locales['CRefCheckTable-count_rows-court'] = 'Nb lignes';
$locales['CRefCheckTable-count_rows-desc'] = 'Nombre de lignes dans la table';
$locales['CRefCheckTable-delay'] = 'Temps entre deux v�rifications';
$locales['CRefCheckTable-end_date'] = 'Fin';
$locales['CRefCheckTable-end_date-court'] = 'Fin';
$locales['CRefCheckTable-end_date-desc'] = 'Date de fin';
$locales['CRefCheckTable-max_id'] = 'Identifiant max';
$locales['CRefCheckTable-max_id-court'] = 'Max ID';
$locales['CRefCheckTable-max_id-desc'] = 'Identifiant de la table le plus grand';
$locales['CRefCheckTable-msg-All over'] = 'Fin de la v�rification d\'int�grit�';
$locales['CRefCheckTable-msg-Confirm reset'] = '�tes-vous s�r de vouloir r�initialiser le v�rificateur de r�f�rences ? Cette action est irreversible.';
$locales['CRefCheckTable-msg-create'] = 'V�rificateur de table cr��';
$locales['CRefCheckTable-msg-delete'] = 'V�rificateur de table supprim�';
$locales['CRefCheckTable-msg-modify'] = 'V�rificateur de table modifi�';
$locales['CRefCheckTable-msg-over'] = 'V�rification termin�e pour %s';
$locales['CRefCheckTable-ref_check_table_id'] = 'ID';
$locales['CRefCheckTable-ref_check_table_id-court'] = 'ID';
$locales['CRefCheckTable-ref_check_table_id-desc'] = 'Identifiant du v�rificateur de table';
$locales['CRefCheckTable-start_date'] = 'Date de d�but';
$locales['CRefCheckTable-start_date-court'] = 'D�but';
$locales['CRefCheckTable-start_date-desc'] = 'Date de d�but';
$locales['CRefCheckTable-title-create'] = 'Cr�er un v�rificateur de table';
$locales['CRefCheckTable-title-modify'] = 'Modifier un v�rificateur de table';
$locales['CRefCheckTable.all'] = 'Tous les v�rificateurs de table';
$locales['CRefCheckTable.none'] = 'Aucune v�rification n\'a �t� lanc�e';
$locales['CRefCheckTable.one'] = 'Un v�rificateur de table';
$locales['CRefError'] = 'Erreur de r�f�rence';
$locales['CRefError-count_use'] = 'Nb fois utilis�';
$locales['CRefError-count_use-court'] = 'Nb fois utilis�';
$locales['CRefError-count_use-desc'] = 'Nombre de fois o� l\'identifiant manquant est r�f�renc�';
$locales['CRefError-missing_id'] = 'Identifiant manquant';
$locales['CRefError-missing_id-court'] = 'ID manquant';
$locales['CRefError-missing_id-desc'] = 'Identifiant de l\'objet non trouv�';
$locales['CRefError-msg-create'] = 'Erreur de r�f�rence cr��e';
$locales['CRefError-msg-delete'] = 'Erreur de r�f�rence supprim�e';
$locales['CRefError-msg-modify'] = 'Erreur de r�f�rence modifi�e';
$locales['CRefError-ref_check_field_id'] = 'V�rificateur de champ';
$locales['CRefError-ref_check_field_id-court'] = 'V�rificateur de champ';
$locales['CRefError-ref_check_field_id-desc'] = 'V�rificateur de champ auquel fait r�f�rence l\'erreur';
$locales['CRefError-ref_error_id'] = 'ID';
$locales['CRefError-ref_error_id-court'] = 'ID';
$locales['CRefError-ref_error_id-desc'] = 'Identifiant de l\'erreur de r�f�rence';
$locales['CRefError-title-create'] = 'Cr�er une erreur de r�f�rence';
$locales['CRefError-title-modify'] = 'Modifier une erreur de r�f�rence';
$locales['CRefError.all'] = 'Toutes les erreurs de r�f�rence';
$locales['CRefError.none'] = 'Aucune erreur de r�f�rence';
$locales['CRefError.one'] = 'Une erreur de r�f�rence';
$locales['CTranslationOverwrite-integrate translations'] = 'Int�grer les traductions';
$locales['CUserAction'] = 'Journaux utilisateur';
$locales['CUserActionData'] = 'journaux utilisateur donn�es';
$locales['CUserLog-_view'] = 'Journal utilisateur';
$locales['CUserLog-_view-desc'] = 'Journal utilisateur';
$locales['Debug'] = 'D�bogage';
$locales['ErrorLog-msg-%s documents deleted from Elasticsearch'] = '%s documents supprim�s d\'Elasticsearch';
$locales['ErrorLog-msg-Would you like to delete this error log?'] = 'Voulez-vous supprimer cette entr�e du journal d\'erreur ?';
$locales['ErrorLog-msg-Would you like to delete those %s errors logs?'] = 'Voulez-vous supprimer ces %s entr�es du journal d\'erreur ?';
$locales['ErrorLog-msg-Would you like to empty the error log?'] = 'Voulez-vous vider compl�tement le journal d\'erreur ?';
$locales['ErrorLog-msg-emptied errors logs'] = 'Suppression des journaux d\'erreur dans Elasticsearch';
$locales['HTTP-code'] = 'Code HTTP';
$locales['Humans'] = 'Humains';
$locales['IndexChecker-error_type'] = 'Type d\'erreur';
$locales['IndexChecker-key_type'] = 'Type de cl�';
$locales['IndexChecker-msg-Db info'] = 'Base de donn�es';
$locales['IndexChecker-msg-Info1'] = '  Cet outil v�rifie la pr�sence des index en base de donn�es pour les diff�rentes classes h�ritants de CStoredObject. Les champs dont la propri�t� est une r�f�rence, une date, une datetime ou un time doivent �tre index�s en base de donn�es.n Si on ne veut pas qu\'un champ d\'un des 4 type pr�c�dent soit index� en base de donn�es il faut ajouter dans la propri�t� de ce champ "index|0".n A l\'inverse si on veut qu\'un champ qui n\'est pas parmis les 4 types pr�c�dents soit index� en base de donn�es il faut ajouter dans la propri�t� de celui-ci "index|1".';
$locales['IndexChecker-msg-Info2'] = 'Cet outil indique les champs dont la propri�t� indique les index manquants en base de donn�es ainsi que les index non attendus mais pr�sents en base de donn�es.';
$locales['IndexChecker-msg-Info3'] = 'Les diff�rentes r�gles � savoir sur les index sont :';
$locales['IndexChecker-msg-Info3-Option1'] = 'Un index pour un propri�t� m�ta doit commencer par "object_class" et non par "object_id" sinon l\'efficacit� de l\'index est tr�s fortement r�duite';
$locales['IndexChecker-msg-Info3-Option2'] = 'Si la propri�t� A d\'une classe doit �tre index� et qu\'il existe un index (A, B, C) alors on consid�re qu\'il n\'y a pas � ajouter un index sur le champ A (ce n\'est pas le cas si l\'ordre de l\'index multiple est (B, A, C)).';
$locales['IndexChecker-msg-No error'] = 'Aucune erreur d\'index';
$locales['IndexChecker-msg-Not in DB'] = 'Non pr�sent en base de donn�es';
$locales['IndexChecker-msg-Not in properties'] = 'Non pr�sent dans les propri�t�s';
$locales['IndexChecker-msg-Property info'] = 'Propri�t�s';
$locales['IndexChecker-show_all_fields'] = 'Afficher les champs sans erreurs';
$locales['IndexChecker.all'] = 'Tous les index';
$locales['IndexChecker.index'] = 'Index';
$locales['IndexChecker.missing_in_db'] = 'Index manquants en base de donn�es';
$locales['IndexChecker.unexpected_in_db'] = 'Index non attendus en base de donn�es';
$locales['IndexChecker.unique'] = 'Cl�s uniques';
$locales['Log'] = 'Log';
$locales['Log-grep-cache-ok'] = 'Suppression du cache de recherche dans les logs';
$locales['ModelGrap-collections'] = 'Collections';
$locales['ModelGraph-class_select'] = 'Classe s�lectionn�e';
$locales['ModelGraph-class_select-desc'] = 'Classe principale du graphe';
$locales['ModelGraph-description'] = 'Description';
$locales['ModelGraph-form_fields'] = 'Propri�t�s calcul�es';
$locales['ModelGraph-herited_fields'] = 'Propri�t�s h�rit�es';
$locales['ModelGraph-hierarchy_sort'] = 'Algorithme d\'affichage du graphe';
$locales['ModelGraph-hierarchy_sort-desc'] = 'Algorithme qui sert � afficher le graphe';
$locales['ModelGraph-number'] = 'Profondeur du graphe';
$locales['ModelGraph-number-desc'] = 'Profondeur du graphe';
$locales['ModelGraph-properties'] = 'Propri�t�s';
$locales['ModelGraph-property-name'] = 'Attribut';
$locales['ModelGraph-property-name-desc'] = 'Nom';
$locales['ModelGraph-references'] = 'R�f�rences';
$locales['ModelGraph-row-size-bdd'] = 'Row Size';
$locales['ModelGraph-show_backprops'] = 'Type d\'affichage des collections';
$locales['ModelGraph-show_backprops-desc'] = 'Type d\'affichage des collection utilisant la classe s�lectionn�e';
$locales['ModelGraph-show_backs'] = 'Afficher les collections';
$locales['ModelGraph-show_backs-desc'] = 'Afficher les collections de la classe';
$locales['ModelGraph-show_formfields'] = 'Afficher les propri�t�s calcul�s';
$locales['ModelGraph-show_formfields-desc'] = 'Afficher les propri�t�s calcul�s de la classe, celles qui ne sont pas en base de donn�es';
$locales['ModelGraph-show_heritage'] = 'Afficher les propri�t�s h�rit�es';
$locales['ModelGraph-show_heritage-desc'] = 'Afficher les propri�t�s h�rit�es des classes parentes';
$locales['ModelGraph-show_hover'] = 'Affichage des informations au survol';
$locales['ModelGraph-show_hover-desc'] = 'Affichage des informations suppl�mentaires au survol des �l�ments';
$locales['ModelGraph-show_properties'] = 'Afficher les propri�t�s';
$locales['ModelGraph-show_properties-desc'] = 'Afficher les propri�t�s de la classe';
$locales['ModelGraph-show_props'] = 'Afficher les classes li�es';
$locales['ModelGraph-show_props-desc'] = 'Afficher les classes li�es � la classe s�lectionn�e';
$locales['ModelGraph-show_refs'] = 'Afficher les r�f�rences';
$locales['ModelGraph-show_refs-desc'] = 'Afficher les r�f�rences � d\'autres classes';
$locales['ModelGraph-type'] = 'Type de donn�es';
$locales['ModelGraph-type-bdd'] = 'Type dans la base de donn�es';
$locales['ModelGraph.class_select.CMbObject'] = 'S�lectionner une classe';
$locales['ModelGraph.hierarchy_sort.basic'] = 'Basic';
$locales['ModelGraph.hierarchy_sort.directed'] = 'Dirig�';
$locales['ModelGraph.hierarchy_sort.hierarchy'] = 'Hi�rarchique';
$locales['ModelGraph.hierarchy_sort.hubsize'] = 'Plus d\'arr�tes en haut';
$locales['ModelGraph.hierarchy_sort.none'] = 'Aucun';
$locales['ModelGraph.show_backprops.all'] = 'Toutes';
$locales['ModelGraph.show_backprops.none'] = 'Aucune';
$locales['ModelGraph.show_backprops.own'] = 'Propres � la classe';
$locales['Robots'] = 'Robots';
$locales['SystemLoggingController-warning-Local log file'] = 'Attention, vous regardez un fichier de logs local !';
$locales['TableIntegrity-class_name'] = 'Nom de la classe';
$locales['TableIntegrity-dsn'] = 'Source de donn�es';
$locales['TableIntegrity-msg-error-Too many fields'] = 'Le nombre de champs est trop �lev�';
$locales['TableIntegrity-row_count'] = 'Nombre de lignes';
$locales['TableIntegrity-table_name'] = 'Nom de la table';
$locales['TablesIntegrityChecker-dsn'] = 'Source de donn�es';
$locales['TablesIntegrityChecker-mod_name'] = 'Module';
$locales['TablesIntegrityChecker-type'] = 'Type';
$locales['TablesIntegrityChecker.type.all'] = 'Tout';
$locales['TablesIntegrityChecker.type.class_missing'] = 'Seulement les tables sans classe';
$locales['TablesIntegrityChecker.type.table_missing'] = 'Seulement les tables manquantes';
$locales['common-Class name'] = 'Nom de la classe';
$locales['common-Field|pl'] = 'Champs';
$locales['common-action-Parse'] = 'Parser';
$locales['common-key'] = 'Cl�';
$locales['common-value'] = 'Valeur';
$locales['dPdeveloppement-Action-Generate route'] = 'G�n�rer une route';
$locales['dPdeveloppement-Api-Error-msg-Controller is mandatory'] = 'Le controller de la route est obligatoire';
$locales['dPdeveloppement-Api-Error-msg-Method is mandatory'] = 'Au moins une m�thode doit �tre s�lectionn�e';
$locales['dPdeveloppement-Api-Error-msg-Path is mandatory'] = 'Le chemin de la route est obligatoire';
$locales['dPdeveloppement-Api-Error-msg-Route name is mandatory'] = 'Le nom de la route est obligatoire';
$locales['dPdeveloppement-Api-Options'] = 'Options de route';
$locales['dPdeveloppement-Api-Path result'] = 'Chemin r�sultant la route (avec les requirements)';
$locales['dPdeveloppement-Api-Route name'] = 'Nom de la route (doit �tre unique)';
$locales['dPdeveloppement-Api-Routing'] = 'Routage';
$locales['dPdeveloppement-Api-accept'] = 'Format de donn�es accept�';
$locales['dPdeveloppement-Api-body-content type'] = 'Type de contenu accept� pour le corp de la requ�te';
$locales['dPdeveloppement-Api-body-required'] = 'Le corp de la requ�te est-il n�cessaire';
$locales['dPdeveloppement-Api-bulk'] = 'Op�rations bulk autoris�es pour la route';
$locales['dPdeveloppement-Api-controller'] = 'Nom du controller et de sa fonction � appeler (@api doit �tre pr�sent dans les commentaires de la fonction)';
$locales['dPdeveloppement-Api-description'] = 'Description de la route pour l\'API';
$locales['dPdeveloppement-Api-methods'] = 'HTTP methods accept�es';
$locales['dPdeveloppement-Api-openapi'] = 'G�n�ration de la documentation openapi pour la route';
$locales['dPdeveloppement-Api-parameters'] = 'Param�tres de la requ�tes suppl�mentaires accept�s et leur type (?foo=bar)';
$locales['dPdeveloppement-Api-path'] = 'Chemin de la route';
$locales['dPdeveloppement-Api-permission'] = 'Permission n�cessaire pour acc�der � la route';
$locales['dPdeveloppement-Api-requirements'] = 'Variables dans le chemin de la route et leur type (w+, d+, ...)';
$locales['dPdeveloppement-Api-responses'] = 'R�ponses possibles de la route';
$locales['dPdeveloppement-Api-security'] = 'Type de s�curit� de la route';
$locales['dPdeveloppement-button-unlocalized_fix'] = 'Mais je suis courageux et je le corrige maintenant !';
$locales['dPdeveloppement-metrics-Data type'] = 'Type de donn�es';
$locales['dPdeveloppement-metrics-General'] = 'G�n�ral';
$locales['dPdeveloppement-metrics-Last update'] = 'Derni�re mise � jour';
$locales['dPdeveloppement-metrics-Quantity'] = 'Quantit�';
$locales['dPdeveloppement-ref_check-Check options'] = 'Options de v�rification';
$locales['dPdeveloppement-ref_check-Result'] = 'R�sultat de la v�rification';
$locales['dPdeveloppement-ref_check-Table sort by'] = 'Trier le tableau par';
$locales['dPdeveloppement-ref_check-msg-Reset ok'] = 'Le v�rificateur de r�f�rences � �t� remis � z�ro';
$locales['dPdeveloppement-translation-integration no ids'] = 'Aucun identifiant s�lectionn�';
$locales['dPdeveloppement-type.performance'] = 'Performances';
$locales['dPdeveloppement-type.regression'] = 'R�gression';
$locales['mod-dPdPdeveloppement-tab-developpement_integrity_class_result'] = 'Int�grit� d\'une classe';
$locales['mod-dPdeveloppement-msg-Attention'] = 'Attention...';
$locales['mod-dPdeveloppement-msg-Class is archive'] = 'Cette classe est une classe d\'archive, toutes ces traductions h�ritent de sa classe parente.';
$locales['mod-dPdeveloppement-msg-Congrats'] = 'F�licitations !';
$locales['mod-dPdeveloppement-msg-Filter translation module'] = 'Filtrer les traductions par module';
$locales['mod-dPdeveloppement-msg-Fully translated'] = 'Ce module est totalement traduit, ce qui est un gage de qualit� manifeste !';
$locales['mod-dPdeveloppement-msg-Fully translated-desc'] = 'Il compte un total de <strong>%d</strong> termes parfaitement traduits.';
$locales['mod-dPdeveloppement-msg-Important'] = 'Important';
$locales['mod-dPdeveloppement-msg-Infos are missing'] = 'Certaines informations sont manquantes au traitement de la traduction.';
$locales['mod-dPdeveloppement-msg-It has %d locations out of a total of %d terms, i.e. a completeness of %f'] = 'Il compte <strong>%d</strong> localisations sur un total de <strong>%d</strong> termes, soit une compl�tude de <strong>%f %%</strong>.';
$locales['mod-dPdeveloppement-msg-Locales file saved'] = 'Fichier de traduction sauvegard�';
$locales['mod-dPdeveloppement-msg-Not translated'] = 'Ce module n\'est absolument pas localis�, on ne peut lui garantir un affichage utilisable.';
$locales['mod-dPdeveloppement-msg-Not translated-desc'] = 'Il compte un total de <strong>%d</strong> termes � traduire.';
$locales['mod-dPdeveloppement-msg-Not very translated'] = 'Ce module est peu traduit, cela va probablement poser des probl�mes d\'affichage.';
$locales['mod-dPdeveloppement-msg-Reference'] = 'R�f�rence';
$locales['mod-dPdeveloppement-msg-Select at least one translation'] = 'Veuillez cocher au moins une traduction';
$locales['mod-dPdeveloppement-msg-Unfully translated'] = 'Ce module est en cours de traduction, c\'est un bon d�but, il reste encore des efforts � faire !';
$locales['mod-dPdeveloppement-msg-error-%s tables row size are over the limit of %s'] = '%s tables d�passent la limite de %s';
$locales['mod-dPdeveloppement-msg-info-row size limit'] = 'Taille limite de stockage d\'un enregistrement (row size)';
$locales['mod-dPdeveloppement-msg-update all icons'] = 'Mettre � jour toutes les ic�nes de module';
$locales['mod-dPdeveloppement-msg-update all icons-confirm'] = '�tes vous s�r de vouloir mettre � jour toutes les ic�nes ? Les anciennes ic�nes seront supprim�es. ';
$locales['mod-dPdeveloppement-msg-update an icon'] = 'Mette � jour l\'ic�ne de ce module';
$locales['mod-dPdeveloppement-msg-update an icon-confirm'] = '�tes vous s�r de vouloir mettre � jour cette ic�ne ? L\'ancienne ic�ne sera supprim�e.';
$locales['mod-dPdeveloppement-msg-warning-%s tables row size are close to the limit of %s'] = '%s tables sont proches la limite de %s';
$locales['mod-dPdeveloppement-tab-ajax_autocomplete_test'] = 'Test d\'autocmplete';
$locales['mod-dPdeveloppement-tab-ajax_check_documentation'] = 'V�rification de documentation';
$locales['mod-dPdeveloppement-tab-ajax_check_integrity'] = 'V�rification int�grit�';
$locales['mod-dPdeveloppement-tab-ajax_delete_error_buffer'] = 'Suppression erreur tampon';
$locales['mod-dPdeveloppement-tab-ajax_details_route'] = 'D�tails du chemin';
$locales['mod-dPdeveloppement-tab-ajax_display_errors'] = 'Affichage des erreurs';
$locales['mod-dPdeveloppement-tab-ajax_display_errors_details'] = 'D�tails des erreurs';
$locales['mod-dPdeveloppement-tab-ajax_display_field_errors'] = 'Affichage des erreurs des champs';
$locales['mod-dPdeveloppement-tab-ajax_fill_integrity_table'] = 'Int�grit�';
$locales['mod-dPdeveloppement-tab-ajax_get_server_config'] = 'Configuration serveur';
$locales['mod-dPdeveloppement-tab-ajax_list_logs'] = 'Liste des logs';
$locales['mod-dPdeveloppement-tab-ajax_list_routes'] = 'Liste des chemins';
$locales['mod-dPdeveloppement-tab-ajax_multiple_server_call'] = 'Appel multi-serveur';
$locales['mod-dPdeveloppement-tab-ajax_parse_log'] = 'Logs';
$locales['mod-dPdeveloppement-tab-ajax_php_config'] = 'Configuration PHP';
$locales['mod-dPdeveloppement-tab-ajax_pop_details'] = 'D�tails';
$locales['mod-dPdeveloppement-tab-ajax_show_log_infos'] = 'D�tail d\'un log mediboard';
$locales['mod-dPdeveloppement-tab-ajax_thumbnails_profiles'] = 'Miniature des profiles';
$locales['mod-dPdeveloppement-tab-ajax_thumbnails_quality'] = 'Qualit� miniature';
$locales['mod-dPdeveloppement-tab-ajax_thumbnails_rotation'] = 'Rotation miniature';
$locales['mod-dPdeveloppement-tab-ajax_thumbnails_version'] = 'Version de la miniature';
$locales['mod-dPdeveloppement-tab-ajax_vw_data_model'] = 'Graphe du mod�le de donn�es';
$locales['mod-dPdeveloppement-tab-ajax_vw_references'] = 'R�f�rences';
$locales['mod-dPdeveloppement-tab-bench'] = 'Benchmark';
$locales['mod-dPdeveloppement-tab-benchmark'] = 'Analyse charge';
$locales['mod-dPdeveloppement-tab-benchmark_client'] = 'Client de benchmark';
$locales['mod-dPdeveloppement-tab-big_file'] = 'Fichier volumineux';
$locales['mod-dPdeveloppement-tab-cache_tester'] = 'Test cache';
$locales['mod-dPdeveloppement-tab-cache_tester_metamodel'] = 'Test cache m�tamod�le';
$locales['mod-dPdeveloppement-tab-cache_tester_users'] = 'Test cache utilisateurs';
$locales['mod-dPdeveloppement-tab-check_zombie_objects'] = 'Int�grit� r�f.';
$locales['mod-dPdeveloppement-tab-configure'] = 'Configurer';
$locales['mod-dPdeveloppement-tab-css_test'] = 'Test CSS';
$locales['mod-dPdeveloppement-tab-csv_backref_classes'] = 'Backref des classes';
$locales['mod-dPdeveloppement-tab-deleteApplicationLogFile'] = 'Suppression de logs';
$locales['mod-dPdeveloppement-tab-developpement_classes_index'] = 'Classes';
$locales['mod-dPdeveloppement-tab-developpement_data_model_class_details'] = 'D�tails';
$locales['mod-dPdeveloppement-tab-developpement_data_model_index'] = 'Mod�le de donn�es';
$locales['mod-dPdeveloppement-tab-developpement_database_check_row_sizes'] = 'Row Sizes des tables';
$locales['mod-dPdeveloppement-tab-developpement_database_class_spec'] = 'Tables';
$locales['mod-dPdeveloppement-tab-developpement_database_index'] = 'Int�grit� de la BDD';
$locales['mod-dPdeveloppement-tab-developpement_database_indexes'] = 'Indexes';
$locales['mod-dPdeveloppement-tab-developpement_database_show'] = 'Exploration de la base de donn�es';
$locales['mod-dPdeveloppement-tab-developpement_database_tables_integrity'] = 'Int�grit� des tables en base de donn�es';
$locales['mod-dPdeveloppement-tab-developpement_gui_logs'] = 'Journal sys.';
$locales['mod-dPdeveloppement-tab-developpement_gui_logs_errors_whitelist'] = 'Liste blanche des erreurs';
$locales['mod-dPdeveloppement-tab-developpement_integrity_class_result'] = 'Int�grit� d\'une classe';
$locales['mod-dPdeveloppement-tab-developpement_integrity_field_error'] = 'Affichage des erreurs';
$locales['mod-dPdeveloppement-tab-developpement_integrity_index'] = 'Int�grit�';
$locales['mod-dPdeveloppement-tab-developpement_metrics_list'] = 'M�trique';
$locales['mod-dPdeveloppement-tab-developpement_routes_stats'] = 'Routes';
$locales['mod-dPdeveloppement-tab-developpement_sandbox'] = 'Bac � Sable';
$locales['mod-dPdeveloppement-tab-developpement_tests_css'] = 'Storybook legacy';
$locales['mod-dPdeveloppement-tab-developpement_tests_thumbnail'] = 'Test des aper�us';
$locales['mod-dPdeveloppement-tab-developpement_translations'] = 'Traductions';
$locales['mod-dPdeveloppement-tab-developpement_translations_integrate'] = 'Int�gration des traductions';
$locales['mod-dPdeveloppement-tab-developpement_translations_list'] = 'Traduction';
$locales['mod-dPdeveloppement-tab-launch_tests'] = 'Tests fonctionnels';
$locales['mod-dPdeveloppement-tab-listErrorLogs'] = 'Liste des logs d\'erreur';
$locales['mod-dPdeveloppement-tab-mnt_module_actions'] = 'Doc.';
$locales['mod-dPdeveloppement-tab-mnt_table_classes'] = 'Tables';
$locales['mod-dPdeveloppement-tab-multiple_server_call'] = 'script multi-server';
$locales['mod-dPdeveloppement-tab-mutex_tester'] = 'Test Mutex';
$locales['mod-dPdeveloppement-tab-routage'] = 'Routage';
$locales['mod-dPdeveloppement-tab-slave_tester'] = 'Test esclave';
$locales['mod-dPdeveloppement-tab-thumbnail_tester'] = 'Test des aper�us';
$locales['mod-dPdeveloppement-tab-view_data_model'] = 'Mod�le de donn�es';
$locales['mod-dPdeveloppement-tab-view_data_modele'] = 'Mod�le de donn�es';
$locales['mod-dPdeveloppement-tab-view_server_config'] = 'Param�trage Serveur';
$locales['mod-dPdeveloppement-tab-vw_class'] = 'Classes';
$locales['mod-dPdeveloppement-tab-vw_class_map'] = 'Classes';
$locales['mod-dPdeveloppement-tab-vw_class_map_detail'] = 'D�tail class-map';
$locales['mod-dPdeveloppement-tab-vw_create_route'] = 'Cr�ateur de route';
$locales['mod-dPdeveloppement-tab-vw_db_checks'] = 'Int�grit� de la BDD';
$locales['mod-dPdeveloppement-tab-vw_db_std'] = 'Exploration de la base de donn�es';
$locales['mod-dPdeveloppement-tab-vw_external_components'] = 'Composants externes';
$locales['mod-dPdeveloppement-tab-vw_gitlab_weekly_report'] = 'GitLab report';
$locales['mod-dPdeveloppement-tab-vw_icono'] = 'Icono. modules';
$locales['mod-dPdeveloppement-tab-vw_integrate_translations'] = 'Int�gration des traductions';
$locales['mod-dPdeveloppement-tab-vw_kernel'] = 'Infos kernel';
$locales['mod-dPdeveloppement-tab-vw_log_parser'] = 'Parser des logs';
$locales['mod-dPdeveloppement-tab-vw_redis_logs_details'] = 'D�tails des logs redis';
$locales['mod-dPdeveloppement-tab-vw_references'] = 'R�f�rences';
$locales['mod-dPdeveloppement-tab-vw_tables_integrity'] = 'Int�grit� des tables en base de donn�es';
$locales['mod-dPdeveloppement-tab-vw_tables_row_sizes'] = 'Row Sizes des tables';
$locales['mod-dPdeveloppement-tab-vw_tests'] = 'Performance et non regression';
$locales['mod-dPdeveloppement-tab-vw_translations'] = 'Traductions';
$locales['mod-developpement-tab-showLogInfos'] = 'D�tail d\'un log applicatif';
$locales['mod-importTools-query-slave-only'] = 'La recherche s\'effectuera sur le serveur slave s\'il est configur�';
$locales['module-dPdeveloppement-court'] = 'Outils';
$locales['module-dPdeveloppement-long'] = 'Outils de d�veloppement';
$locales['module-dPdeveloppement-trigramme'] = 'Out';
