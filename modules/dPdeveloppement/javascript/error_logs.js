/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ErrorLogs = window.ErrorLogs || {
  url_list_error_log:                  "",
  url_list_error_log_whitelist:        "",
  url_list_error_log_whitelist_toggle: "",

  filterError: function () {
    const type = $V("source-type");
    new Url().setRoute(ErrorLogs.url_list_error_log)
      .addParam("source_type", type)
      .addFormData(getForm("filter-error"))
      .requestUpdate("error-list");
    return false
  },

  toggleCheckboxes: function (checkbox) {
    const form = getForm("filter-error");

    checkbox.next("fieldset").select("input.type").invoke("writeAttribute", "checked", checkbox.checked);

    $V(form.start, 0)
  },

  listErrorLogWhitelist: function () {
    new Url()
      .setRoute(ErrorLogs.url_list_error_log_whitelist, "developpement_gui_logs_errors_whitelist", "developpement")
      .requestModal(800, 600)
  },

  toggleErrorLogWhitelist: function (error_log_id, token) {
    const type = $V("source-type");
    new Url()
      .setRoute(ErrorLogs.url_list_error_log_whitelist_toggle)
      .addParam("error_log_id", error_log_id)
      .addParam("is_elastic_log", type === "elastic")
      .addParam("token", token)
      .requestUpdate("systemMsg", {
        onComplete: function () {
          document.getElementById("btn-search-errors").click()
        }, method:  "post"
      })
  }
};
