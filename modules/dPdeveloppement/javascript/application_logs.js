/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ApplicationLogs = window.ApplicationLogs || {
  url_list_application_log: "",

  jsonViewer: function (context, extra) {
    document.getElementById("application_log_modal_context").innerHTML = JSON.stringify(context, null, 2);
    document.getElementById("application_log_modal_extra").innerHTML = JSON.stringify(extra, null, 2);

    Modal.open("application_log_modal", {title: $T("mod-developpement-tab-showLogInfos"), width: 500, showClose: true})
  },

  filterLog: function () {
    document.body.style.cursor = "wait";

    const waring_file = document.getElementById("div-warning-local-logging-file");
    const file_info = document.getElementById("application-log-file-info");
    const elastic_info = document.getElementById("application-log-elastic-info");

    const mode = $V("elasticsearch-or-file");
    if (mode === "elasticsearch") {
      waring_file.style.display = "none";
      if (file_info) {
        file_info.style.display = "none"
      }
      if (elastic_info) {
        elastic_info.style.display = "block"
      }
    } else {
      document.getElementById("div-warning-local-logging-file").style.display = "block";
      if (file_info) {
        file_info.style.display = "block"
      }
      if (elastic_info) {
        elastic_info.style.display = "none"
      }
    }
    new Url()
      .setRoute(ApplicationLogs.url_list_application_log)
      .addParam("mode", mode)
      .addFormData(getForm("filter-log"))
      .requestHTML(function (html) {
        const parent = document.getElementById("application-list");
        const divs = document.getElementsByClassName("divShowMoreLog");
        for (let pas = 0; pas < divs.length; pas++) {
          divs[pas].remove()
        }
        parent.insert(html);
        document.body.style.cursor = "auto"
      });

    document.getElementById("log_start").value = parseInt(document.getElementById("log_start").value) + 1000;

    return false
  },

  showMoreLog: function (element) {
    const i = element.querySelector("i");
    const class_list = i.classList;
    class_list.remove("fa-arrow-circle-down");
    class_list.add("fa-spinner");
    class_list.add("fa-spin");
    ApplicationLogs.filterLog()
  },

  refreshLog: function () {
    document.getElementById("application-list").innerHTML = "";
    document.getElementById("log_start").value = 0;
    const grep_search = document.getElementById("grep_search");
    if (grep_search) {
      grep_search.value = ""
    }

    ApplicationLogs.filterLog()
  },

  grepLog: function () {
    const grep_len = document.getElementById("grep_search").value.length;

    if (grep_len > 0 && grep_len < 3) {
      alert("La recherche doit dpasser 3 caractres.");
      return false
    }

    document.getElementById("application-list").innerHTML = "";
    document.getElementById("log_start").value = 0;
    ApplicationLogs.filterLog();

    return false
  },

  removeLogs: function () {
    if (confirm("Voulez-vous vider compltement le journal de log ?")) {
      new Url().setRoute(ApplicationLogs.url_list_application_log)
        .addParam("raw", 1)
        .addParam("delete", 1)
        .addFormData(getForm("filter-log"))
        .requestUpdate("application-list", {
          onComplete: function () {
            Control.Tabs.setTabCount("application-tab", "0io")
          }
        })
    }
  },

  download: function () {
    new Url().setRoute(ApplicationLogs.url_list_application_log)
      .addParam("raw", 1)
      .addParam("download", 1)
      .addFormData(getForm("filter-log"))
      .open("")
  }
};
