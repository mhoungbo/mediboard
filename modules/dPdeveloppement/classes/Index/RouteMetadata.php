<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Index;

use LogicException;
use Ox\Mediboard\Search\IIndexableObject;

/**
 * Class representing the route metadata as object for building object cache indexer
 */
class RouteMetadata implements IIndexableObject
{
    private string $id;
    private string $module;
    private string $controller;
    private string $route_name;
    private string $action;
    private string $path;
    private string $methods;
    private bool   $offline;
    private string $permission;

    public function __construct(
        string $id,
        string $module,
        string $controller,
        string $route_name,
        string $action,
        string $path,
        string $methods,
        bool   $offline,
        string $permission
    ) {
        $this->id         = $id;
        $this->module     = $module;
        $this->controller = $controller;
        $this->route_name = $route_name;
        $this->action     = $action;
        $this->path       = $path;
        $this->methods    = $methods;
        $this->offline    = $offline;
        $this->permission = $permission;
    }

    /**
     * Return actual object to array
     */
    public function toArray(): array
    {
        return [
            "module"     => $this->getModule(),
            "controller" => $this->getController(),
            "route_name" => $this->getRouteName(),
            "action"     => $this->getAction(),
            "path"       => $this->getPath(),
            "methods"    => $this->getMethods(),
            "id"         => $this->getId(),
            "offline"    => $this->getOffline(),
            "permission" => $this->getPermission(),
        ];
    }

    /**
     * Return actual object to string
     */
    public function __toString(): string
    {
        return $this->getModule()
            . " | " . $this->getController()
            . " | " . $this->getRouteName()
            . " | " . $this->getAction()
            . " | " . $this->getPath()
            . " | " . $this->getMethods()
            . " | " . ($this->getOffline() ? '1' : '0')
            . " | " . $this->getPermission();
    }

    /**
     * Transform a string to object
     */
    public static function fromString(string $id, string $data): self
    {
        return new self($id, ...explode(' | ', $data));
    }

    /**
     * @inheritDoc
     */
    public function getIndexableData(): array
    {
        return [
            "title" => $this->getRouteName(),
            "_id"   => $this->getId(),
            "body"  => $this->getIndexableBody((string)$this),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getIndexableBody($content): string
    {
        return $content;
    }

    /**
     * @inheritDoc
     *
     * @throws LogicException
     */
    public function getIndexablePatient()
    {
        throw new LogicException('Should not be called');
    }

    /**
     * @inheritDoc
     *
     * @throws LogicException
     */
    public function getIndexablePraticien()
    {
        throw new LogicException('Should not be called');
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getModule(): string
    {
        return $this->module;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function getRouteName(): string
    {
        return $this->route_name;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getMethods(): string
    {
        return $this->methods;
    }

    public function getOffline(): bool
    {
        return $this->offline;
    }

    public function getPermission(): string
    {
        return $this->permission;
    }

    public function equals(self $other): bool
    {
        return (
            ($this->getRouteName() === $other->getRouteName())
            && ($this->getModule() === $other->getModule())
            && ($this->getController() === $other->getController())
            && ($this->getAction() === $other->getAction())
            && ($this->getPath() === $other->getPath())
            && ($this->getMethods() === $other->getMethods())
            && ($this->getOffline() === $other->getOffline())
            && ($this->getPermission() === $other->getPermission())
        );
    }
}
