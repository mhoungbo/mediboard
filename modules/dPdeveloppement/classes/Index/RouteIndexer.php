<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Index;

use Exception;
use Ox\Core\CClassMap;
use Ox\Core\CObjectIndexer;
use Ox\Core\Kernel\Routing\RouteManager;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class building the route indexer and can be used for searching in this index
 */
class RouteIndexer
{
    private CObjectIndexer $indexer;

    /**
     * Construct a new object indexer with specific name and version
     * @throws Exception
     */
    public function __construct()
    {
        $this->indexer = new CObjectIndexer(
            $this->getName(),
            CClassMap::getSN(RouteMetadata::class),
            $this->getVersion(),
            fn() => $this->build(),
            null
        );
    }

    /**
     * Search method for routes indexer
     *
     * @param null|string $keywords
     * @param bool        $to_array
     *
     * @return RouteMetadata[]
     * @throws Exception|InvalidArgumentException
     */
    public function search(?string $keywords, bool $to_array = false): array
    {
        $routes = [];

        // If no keywords passed, get all indexes
        $search = (($keywords === null) || ($keywords === '') || (strlen(trim($keywords)) < 2))
            ? $this->indexer->all() : $this->indexer->search($keywords);

        foreach ($search as $_res) {
            $class    = RouteMetadata::fromString($_res['_id'], $_res['body']);
            $routes[] = ($to_array) ? $class->toArray() : $class;
        }

        return $routes;
    }

    /**
     * Get a route by his name
     *
     * @throws InvalidArgumentException
     */
    public function findByName(string $name): ?RouteMetadata
    {
        foreach ($this->search($name) as $route) {
            if ($route->getRouteName() === $name) {
                return $route;
            }
        }

        return null;
    }

    /**
     * Make a search in the routes with the option offline.
     *
     * @param string|null $keywords
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public function findOffline(?string $keywords): array
    {
        $routes = [];
        foreach ($this->search($keywords) as $route) {
            if ($route->getOffline()) {
                $routes[] = $route;
            }
        }

        return $routes;
    }

    /**
     * Building content for object indexer by reading data from classmap
     * @throws Exception
     */
    protected function build(): array
    {
        // Get all routes : Must not use the all_routes.yml file because of options that are not present in this file.
        $routes = (new RouteManager())->loadAllRoutes()->getRouteCollection()->all();

        $objects = [];
        foreach ($routes as $_name => $_route) {
            [$controller, $action] = explode('::', $_route->getDefault('_controller'));
            $module_ns = implode('\\', array_slice(explode('\\', $controller), 0, 3));

            $objects[] = new RouteMetadata(
                uniqid(),
                CClassMap::getInstance()->getModuleFromNamespace($module_ns),
                $controller,
                $_name,
                $action,
                $_route->getPath(),
                implode(',', $_route->getMethods()),
                $_route->getOption('offline') ?? false,
                $_route->getDefault('permission')
            );
        }

        return $objects;
    }

    /**
     * Return the name for building indexer.
     */
    protected function getName(): string
    {
        return "routes";
    }

    /**
     * Return an hash generated with all routes.
     * Version will change if a route have been added, deleted or modified.
     */
    protected function getVersion(): string
    {
        return (new RouteManager())->getRoutesSignature();
    }
}
