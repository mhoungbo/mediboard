<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Console;

use Ox\Cli\Console\AppBridge;
use Ox\Core\CMbArray;
use Ox\Mediboard\Developpement\Database\ClassDefinitionChecker;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Command to check database and object spec fields
 */
class ClassDefinitionCheckerCommand extends Command
{
    private AppBridge $app;

    public function __construct(AppBridge $app)
    {
        parent::__construct();

        $this->app = $app;
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this
            ->setName('ox-db:check-specs')
            ->setDescription('Check the diff between database and object spec')
            ->addOption('generate-baseline', 'b', InputOption::VALUE_OPTIONAL, 'Generate a baseline to the file')
            ->addOption('module-name', 'm', InputOption::VALUE_OPTIONAL, 'Module to perform check on');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $baseline    = $input->getOption('generate-baseline');
        $module_name = $input->getOption('module-name');

        $io = new SymfonyStyle($input, $output);

        if ($baseline && $module_name) {
            throw new LogicException('The generate-baseline and module-name option cannot be used at the same time');
        }

        $this->app->start();

        $checker = new ClassDefinitionChecker($module_name);
        $errors  = $checker->compileErrors();

        if ($errors === []) {
            $io->success('No error found');

            return self::SUCCESS;
        }

        $total = 0;
        foreach ($errors as $fields) {
            $total += count($fields);
        }

        $errors = CMbArray::mapRecursive(
            function ($data) {
                return is_string($data) ? mb_convert_encoding($data, 'UTF-8', 'ISO-8859-1') : $data;
            },
            $errors
        );


        $errors = json_encode($errors, JSON_PRETTY_PRINT);

        if ($baseline) {
            file_put_contents($baseline, $errors);

            $io->success(
                "Baseline generated in file '$baseline' with {$total} fields in error"
            );

            return self::SUCCESS;
        }

        $io->error("There are {$total} fields in error");
        $io->error($errors);

        return self::FAILURE;
    }
}
