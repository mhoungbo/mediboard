<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Database;

use Exception;
use Ox\Core\CClassMap;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\FieldSpecs\CDateSpec;
use Ox\Core\FieldSpecs\CDateTimeSpec;
use Ox\Core\FieldSpecs\CPasswordSpec;
use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Core\FieldSpecs\CTimeSpec;
use Ox\Mediboard\Maternite\DossierPerinat\SyntheseContextePsychoSocial;
use Throwable;

/**
 * Database and ObjectSpec integrity checker
 */
class IndexChecker
{
    public const ERROR_ALL           = 'all';
    public const ERROR_MISSING_DB    = 'missing_in_db';
    public const ERROR_UNEXPECTED_DB = 'unexpected_in_db';
    public const ERROR_TYPES         = [self::ERROR_ALL, self::ERROR_MISSING_DB, self::ERROR_UNEXPECTED_DB];

    public const OK = 'ok';

    public const KEY_INDEX  = 'index';
    public const KEY_UNIQUE = 'unique';
    public const KEY_TYPES  = [self::KEY_INDEX, self::KEY_UNIQUE];

    public const INDEX_TYPES = [CRefSpec::class, CDateTimeSpec::class, CDateSpec::class, CTimeSpec::class];

    public const SQL_KEY_NAME          = 'Key_name';
    public const SQL_SEQUENCE_IN_INDEX = 'Seq_in_index';
    public const SQL_COLUMN_NAME       = 'Column_name';

    private CSQLDataSource $ds;
    private ?string        $module;
    private array          $classes_to_check      = [];
    protected array        $errors                = [];
    private string         $error_type;
    private string         $key_type;
    private bool           $show_all_fields       = false;
    private int            $count_missing_db      = 0;
    private int            $count_not_expected_db = 0;

    public function __construct(
        string $key_type,
        string $error_type = self::ERROR_ALL,
        string $module = null,
        bool   $show_all_field = false
    ) {
        $this->error_type = (in_array($error_type, self::ERROR_TYPES)) ? $error_type : self::ERROR_ALL;
        $this->key_type   = (in_array($key_type, self::KEY_TYPES)) ? $key_type : self::KEY_INDEX;

        // Need to keep module in string for the getClassChildren to work
        $this->module                = $module;
        $this->count_missing_db      = 0;
        $this->count_not_expected_db = 0;
        $this->show_all_fields       = $show_all_field;
    }

    /**
     * Check indexes existing in DB and that should exists
     *
     * @throws Exception
     */
    public function check(): array
    {
        $this->init();

        foreach ($this->classes_to_check as $_class_name) {
            if (null === ($instance = $this->getInstance($_class_name))) {
                continue;
            }

            $spec = $instance->getSpec();

            if (!$spec->table || !$instance->getDS()) {
                continue;
            }

            $this->ds = $instance->getDS();

            $existing_indexes = $this->getExistingIndexes($spec->table);
            $expected_indexes = ($this->key_type === self::KEY_INDEX)
                ? $this->getExpectedIndexes($instance)
                : $this->getExpectedUniques($instance);

            if (($this->error_type === self::ERROR_ALL) || ($this->error_type === self::ERROR_MISSING_DB)) {
                $this->checkExpectedIndexes($_class_name, $expected_indexes, $existing_indexes);
            }

            if (($this->error_type === self::ERROR_ALL) || ($this->error_type === self::ERROR_UNEXPECTED_DB)) {
                $this->checkNonExpectedIndexes(
                    $_class_name,
                    $expected_indexes,
                    $existing_indexes,
                    ($this->key_type === self::KEY_INDEX) ? $this->getExpectedUniques($instance) : []
                );
            }
        }

        return $this->errors;
    }

    private function checkNonExpectedIndexes(
        string $class_name,
        array  $expected,
        array  $existing,
        array  $expect_uniques
    ): void {
        foreach ($existing as $_existing) {
            // Multi index if more than one field
            if (count($_existing) > 1) {
                $field_name = implode('|', $_existing);
                $index_ok   = $this->handleMultiDbIndex($_existing, $expected);
                if (!$index_ok && $expect_uniques) {
                    // Check if index has been declared as unique
                    $index_ok = $this->handleMultiDbIndex($_existing, $expect_uniques);
                }
            } else {
                $field_name = $_existing[1];
                $index_ok   = $this->handleSingleDbIndex($field_name, $expected);
                if (!$index_ok && $expect_uniques) {
                    // Check if index has been declared as unique
                    $index_ok = $this->handleSingleDbIndex($field_name, $expect_uniques);
                }
            }

            if (!$index_ok) {
                $this->errors[$class_name][$field_name] = self::ERROR_UNEXPECTED_DB;
                $this->count_not_expected_db++;
            } elseif ($this->show_all_fields) {
                $this->errors[$class_name][$field_name] = self::OK;
            }
        }
    }

    private function handleSingleDbIndex(string $field_name, array $expected): bool
    {
        foreach ($expected as $_expected) {
            if (
                (!is_array($_expected) && ($field_name == $_expected))
                || (is_array($_expected) && ($_expected[1] == $field_name))
            ) {
                // Index is okay if it is expected by the props
                return true;
            }
        }

        return false;
    }

    private function handleMultiDbIndex(array $existings, array $expected): bool
    {
        foreach ($expected as $_expected) {
            // Do not search if expected index have more items than the existing one
            if (!is_array($_expected) || (count($_expected) > count($existings))) {
                continue;
            }

            $all_ok = true;
            // Do not use array_diff, fields have to be in the same order
            foreach ($existings as $pos => $field) {
                if (!isset($_expected[$pos]) || ($_expected[$pos] != $field)) {
                    $all_ok = false;
                    break;
                }
            }

            if ($all_ok) {
                return true;
            }
        }

        return false;
    }

    private function checkExpectedIndexes(string $class_name, array $expected, array $existing): void
    {
        foreach ($expected as $_expected) {
            if (is_array($_expected) && (count($_expected) > 1)) {
                $field_name = implode('|', $_expected);
                $index_ok   = $this->handleMultiFieldIndex($_expected, $existing);
            } else {
                $_expected  = is_array($_expected) ? $_expected[1] : $_expected;
                $field_name = $_expected;
                $index_ok   = $this->handleSingleFieldIndex($_expected, $existing);
            }

            if (!$index_ok) {
                $this->errors[$class_name][$field_name] = self::ERROR_MISSING_DB;
                $this->count_missing_db++;
            } elseif ($this->show_all_fields) {
                $this->errors[$class_name][$field_name] = self::OK;
            }
        }
    }

    protected function handleSingleFieldIndex(string $expected, array $existing): bool
    {
        foreach ($existing as $_fields) {
            if ($_fields[1] == $expected) {
                return true;
            }
        }

        return false;
    }

    protected function handleMultiFieldIndex(array $expected, array $existing): bool
    {
        foreach ($existing as $fields) {
            // Multi index expected
            // There can be a longer index usable
            if (count($fields) < count($expected)) {
                continue;
            }

            if (($expected[1] == $fields[1]) && ($expected[2] == $fields[2])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @throws Exception
     */
    protected function getExistingIndexes(string $table_name): array
    {
        if (!$this->tableExists($table_name)) {
            return [];
        }

        $result = $this->getExistingIndexesFromDb($table_name);

        $indexes = [];
        foreach ($result as $_result) {
            if (!isset($indexes[$_result[self::SQL_KEY_NAME]])) {
                $indexes[$_result[self::SQL_KEY_NAME]] = [
                    $_result[self::SQL_SEQUENCE_IN_INDEX] => $_result[self::SQL_COLUMN_NAME],
                ];
            } else {
                $indexes[$_result[self::SQL_KEY_NAME]][$_result[self::SQL_SEQUENCE_IN_INDEX]]
                    = $_result[self::SQL_COLUMN_NAME];
            }
        }

        return $indexes;
    }

    protected function tableExists(string $table_name): bool
    {
        return $this->ds->hasTable($table_name);
    }

    protected function getInstance(string $class_name): ?CStoredObject
    {
        try {
            try {
                // Disable SF error_handler for tests purpose.
                // SF error_handler will transform errors to exceptions even with error_reporting(0)
                if (($_SERVER['APP_ENV'] === 'test') || (PHP_SAPI === 'cli')) {
                    set_error_handler(function () {
                    });
                }

                return new $class_name();
            } finally {
                if (($_SERVER['APP_ENV'] === 'test') || (PHP_SAPI === 'cli')) {
                    restore_error_handler();
                }
            }
        } catch (Throwable $e) {
            return null;
        }
    }

    /**
     * @throws Exception
     */
    private function init(): void
    {
        $class_map = CClassMap::getInstance();

        $this->ds = CSQLDataSource::get('std');

        if ($this->module) {
            $this->classes_to_check = $class_map->getClassChildren(CStoredObject::class, false, true, $this->module);
        } else {
            $this->classes_to_check = $class_map->getClassChildren(CStoredObject::class, false, true);
        }
    }

    /**
     * @throws Exception
     */
    protected function getExistingIndexesFromDb(string $table_name): array
    {
        $query = "SHOW INDEXES FROM `{$table_name}` WHERE Key_name != 'PRIMARY'";

        if ($this->key_type === self::KEY_UNIQUE) {
            $query .= ' AND Non_unique = 0';
        }

        return $this->ds->loadList($query);
    }

    /**
     * @throws Exception
     */
    protected function getExpectedIndexes(CStoredObject $object): array
    {
        $expected_indexes = [];
        $specs            = $this->getObjectSpecs($object);
        $primary_key      = $object->getSpec()->key;
        foreach ($specs as $field_name => $spec) {
            if ((strpos($field_name, '_') === 0) || ($field_name === $primary_key)) {
                continue;
            }

            // Index have to be on a field of expected type
            // Field doesn't have to have the spec index|0
            if (
                ($spec->index === "0")
                || (
                    !in_array(get_class($spec), self::INDEX_TYPES)
                    && ($spec->index === null)
                    && ($spec->autocomplete === null)
                )
            ) {
                if (!($spec instanceof CPasswordSpec) || !$spec->isEncrypted()) {
                    continue;
                }
            }

            if (($spec instanceof CPasswordSpec) && $spec->isEncrypted()) {
                $expected_indexes[] = $field_name;
                continue;
            }

            // Date, datetime and time specs does not have a meta field and cannot be a multi index
            if (property_exists($spec, 'meta') && $spec->meta) {
                $expected_indexes[] = [
                    1 => $spec->meta,
                    2 => $field_name,
                ];

                continue;
            }

            if (
                ($spec->index === '1') || ($spec->index === null)
                || (get_class($spec) !== CRefSpec::class && $spec->autocomplete)
            ) {
                $expected_indexes[] = $field_name;
                continue;
            }

            // If $spec->index but index !== 1 we may want to force a multi index
            $index_parts = explode('|', $spec->index);
            // Forced multi index
            $multi_index = [1 => $field_name];

            $i = 2;
            foreach ($index_parts as $part) {
                if (isset($specs[$part])) {
                    $multi_index[$i] = $part;
                }
                $i++;
            }

            $expected_indexes[] = $multi_index;
        }

        return $expected_indexes;
    }

    /**
     * @throws Exception
     */
    protected function getObjectSpecs(CStoredObject $object): array
    {
        return $object->getSpecs();
    }

    private function getExpectedUniques(CStoredObject $object): array
    {
        $expected_indexes = [];
        $uniques          = $object->getSpec()->uniques;

        // Add declared iodkus indexes
        if ($iodkus = $object->getSpec()->iodkus) {
            $uniques = array_merge($uniques, $iodkus);
        }

        foreach ($uniques as $_fields) {
            if (count($_fields) > 1) {
                $i     = 1;
                $multi = [];
                foreach ($_fields as $_field) {
                    $multi[$i] = $_field;
                    $i++;
                }
                $expected_indexes[] = $multi;
            } else {
                $expected_indexes[] = $_fields[0];
            }
        }

        return $expected_indexes;
    }

    public function getCountMissingDb(): int
    {
        return $this->count_missing_db;
    }

    public function getCountNotExpectedDb(): int
    {
        return $this->count_not_expected_db;
    }
}
