<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Database;

use Exception;
use Ox\Core\Autoload\IShortNameAutoloadable;
use Ox\Core\CClassMap;
use Ox\Core\CMbFieldSpec;
use Ox\Core\CMbString;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\FieldSpecs\CDateSpec;
use Ox\Core\FieldSpecs\CDateTimeSpec;
use Ox\Core\FieldSpecs\CEnumSpec;
use Ox\Core\FieldSpecs\CPasswordSpec;
use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Core\FieldSpecs\CStrSpec;
use Ox\Core\FieldSpecs\CTextSpec;
use Ox\Core\FieldSpecs\CTimeSpec;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Developpement\TablesIntegrityChecker;
use Throwable;

/**
 * Performs class definition checks
 */
class ClassDefinitionChecker implements IShortNameAutoloadable
{
    // Types d'erreurs qu'on peut prendre en compte
    public const ERROR_TYPES = ['type', 'params', 'unsigned', 'zerofill', 'null', 'default', 'index', 'extra'];

    // Les classes du module install�es
    private array $selected_classes = [];

    // Liste des classes analys�es
    private array $list_classes = [];

    // Les types s�lectionn�s
    private array $types       = [];
    private array $meta_fields = [];

    /**
     * ClassDefinitionChecker constructor.
     *
     * @param string $name Nom du module ou de la classe selectionn�
     *
     * @throws Exception
     */
    public function __construct(?string $name = null)
    {
        if ($name !== null) {
            if (class_exists($name)) {
                $this->selected_classes[] = $name;

                return;
            } elseif (CModule::getInstalled($name) instanceof CModule) {
                $this->selected_classes =
                    CClassMap::getInstance()->getClassChildren(CStoredObject::class, false, true, $name);

                return;
            }
        }

        $this->selected_classes = CClassMap::getInstance()->getClassChildren(CStoredObject::class, false, true);
    }

    /**
     * Build an array of duplicates values for key-value array collection.
     *
     * @param array  $array Key-value array collection (array of arrays)
     * @param string $key   The key to inspect
     */
    private function array_duplicates(array $array, string $key): array
    {
        $ret   = [];
        $count = count($array);
        for ($i = 0; $i < $count; $i++) {
            for ($j = 0; $j < $count; $j++) {
                if (($i != $j) && ($array[$i][$key] == $array[$j][$key])) {
                    $ret[$i][] = $array[$i];
                }
            }
        }

        return $ret;
    }

    /**
     * Construit la requete correspondant � la spec d'une classe
     * avec prise en compte des erreurs que l'on souhaite mettre en �vidence
     *
     * @param array $class Donn�es d'analyse de la classe
     */
    private function getQueryForClass(array $class): ?string
    {
        $change     = [];
        $add_index  = [];
        $drop_index = [];
        $ret        = '';

        if (!isset($class['fields']) || (count($class['fields']) === 0)) {
            return null;
        }

        // On compte les champs de BDD de la classe, si c'est null, on cr�e la classe
        $add_table = $class['no_table'];

        // Gestion des ALTER TABLE
        foreach ($class['fields'] as $name => $field) {
            $spec_obj = $field['object']['db_spec'];
            $spec_db  = $field['db'];

            $add_field = isset($spec_db['no_column']);

            // creation des lignes de specification des champs
            if (
                $add_field
                || $add_table
                || ($this->types['type'] && ($spec_obj['type'] != $spec_db['type']))
                || ($this->types['params'] && ($spec_obj['params'] != $spec_db['params']))
                || ($this->types['unsigned'] && ($spec_obj['unsigned'] != $spec_db['unsigned']))
                || ($this->types['zerofill'] && ($spec_obj['zerofill'] != $spec_db['zerofill']))
                || ($this->types['default'] && ($spec_obj['default'] != $spec_db['default']))
                || ($this->types['extra'] && ($spec_obj['extra'] != $spec_db['extra']))
                || ($this->types['null'] && ($spec_obj['null'] != $spec_db['null']))
            ) {
                if ($add_field && !$add_table) {
                    $change[$name] = "ADD `$name` ";
                } elseif ($add_table) {
                    $change[$name] = "`$name` ";
                } else {
                    $change[$name] = "CHANGE `$name` `$name` ";
                }

                if (isset($spec_obj['type'])) {
                    $change[$name] .= strtoupper($spec_obj['type']);
                }

                if (is_array($spec_obj['params']) && (count($spec_obj['params']) > 0)) {
                    $change[$name] .= ' (' . implode(',', $spec_obj['params']) . ')';
                }

                $change[$name] .=
                    ($spec_obj['unsigned'] ? ' UNSIGNED' : '') .
                    ($spec_obj['zerofill'] ? ' ZEROFILL' : '') .
                    ($spec_obj['null'] ? '' : ' NOT NULL') .
                    (($spec_obj['default'] !== null) ? " DEFAULT '{$spec_obj['default']}'" : '') .
                    ($spec_obj['extra'] ? " {$spec_obj['extra']}" : '') .
                    (($name === $class['key']) ? ' PRIMARY KEY' : '');
            }

            $check_drop_index = false;
            // creation des lignes d'ajout suppression des index
            if ($this->types['index'] && $spec_obj['index'] && !$spec_db['index'] && ($class['key'] !== $name)) {
                if (strpos($spec_obj['index'], ', ') !== false) {
                    $add_index[$name] = $this->addMultiIndex($name, $spec_obj['index']);
                } else {
                    $add_index[$name] = "ADD INDEX (`$name`)";
                }
            } elseif (
                isset($spec_obj['index'])
                && (strpos($spec_obj['index'], ', ') !== false)
                && ($spec_db['index'] != $spec_obj['index'])
            ) {
                $add_index[$name] = $this->addMultiIndex($name, $spec_obj['index']);
                $check_drop_index = true;
            }

            if ($check_drop_index || ($this->types['index'] && !$spec_obj['index'] && $spec_db['index'])) {
                $drop_index[$spec_db['index']] = "# DROP INDEX (`{$spec_db['index']}`)";
            }
        }

        $glue = ",\n                ";

        // creation / modification de la table
        if (count($change) > 0) {
            if ($add_table) {
                $ret = "CREATE TABLE `{$class['table']}` (\n                "
                    . implode($glue, $change) . "\n              )/*! ENGINE=MyISAM */;";
            } else {
                $ret = "ALTER TABLE `{$class['table']}` \n                "
                    . implode($glue, $change) . ";";
            }
        }

        $add_index = array_unique($add_index);
        // ajout / suppression des index
        if ((count($add_index) > 0) || (count($drop_index) > 0)) {
            $q = [];
            if (count($add_index) > 0) {
                $q[] = implode($glue, $add_index);
            }
            if (count($drop_index) > 0) {
                $q[] = implode($glue, $drop_index);
            }
            $ret .= "\nALTER TABLE `{$class['table']}` \n                "
                . implode($glue, $q) . ";";
        }

        if (count($class['duplicates']) > 0) {
            $ret .= "\n# Il y a probablement des index en double sur cette table";
        }

        // Gestion des INDEX FULLTEXT (ADD/DROP)
        if (count($class['fulltext_indexes']) > 0) {
            foreach ($class['fulltext_indexes'] as $ft_index) {
                $drop_index = false;
                $add_index  = false;
                switch ($ft_index['status']) {
                    case 'incomplete';
                        $drop_index = true;
                        $add_index  = true;
                        break;
                    case 'missing':
                        $add_index = true;
                        break;
                    default:
                        /* Do nothing */
                        break;
                }
                if ($drop_index) {
                    $ret .= "\nALTER TABLE `{$class['table']}` DROP INDEX `" . $ft_index['name'] . "`;";
                }
                if ($add_index) {
                    $ret .= "\nALTER TABLE `{$class['table']}` ADD FULLTEXT INDEX `" . $ft_index['name']
                        . "` (" . implode(',', $ft_index['required_fields']) . ");";
                }
            }
        }

        return $ret;
    }

    /**
     * Cr�e la requ�te pour ajouter un index multiple
     *
     * @param string $name        Le nom � donner � l'index
     * @param string $index_value Les champs de l'index s�par�s par une virgule
     */
    private function addMultiIndex(string $name, string $index_value): string
    {
        $prefix = explode('_', $name);

        return 'ADD INDEX ' . $prefix[0] . ' (' . $index_value . ')';
    }

    /**
     * Check the errors between Object specification and database representation.
     *
     * @throws Exception
     */
    public function checkErrors(bool $with_suggestions = true): array
    {
        $this->buildClassesData();

        // Tableau indiquant si chaque champ contient une erreur
        $list_errors = [];
        foreach ($this->list_classes as $_class => &$class_details) {
            $list_errors[$_class] = $this->buildErrorsListForClass($class_details);
        }

        if ($with_suggestions) {
            // Enregistre les suggestions pour chaque classe
            foreach ($this->list_classes as &$class_details) {
                $class_details['suggestion'] = $this->getQueryForClass($class_details);
            }
        }

        return $list_errors;
    }

    /**
     * @throws Exception
     */
    public function compileErrors(): array
    {
        $this->setErrorTypes(['type', 'params', 'unsigned', 'zerofill', 'null', 'default', 'extra']);
        $errors_list = $this->checkErrors(false);

        $compiled_errors = [];
        foreach ($errors_list as $class_name => $fields) {
            if (null === $fields) {
                continue;
            }

            foreach ($fields as $field_name => $errors) {
                if (!$errors) {
                    continue;
                }

                $object_spec = $this->list_classes[$class_name]['fields'][$field_name]['object'];
                $db_spec     = $this->list_classes[$class_name]['fields'][$field_name]['db'];

                if (!isset($object_spec['spec'])) {
                    $compiled_errors[$class_name][$field_name] = 'No spec for this column';
                    continue;
                }

                if (!isset($db_spec['type'])) {
                    $compiled_errors[$class_name][$field_name] = 'No column for the spec';
                    continue;
                }

                $errors = $this->compileSpecAndDb(
                    $object_spec['db_spec'],
                    $db_spec,
                );

                if ($errors) {
                    $compiled_errors[$class_name][$field_name] = $errors;
                }
            }
        }

        return $compiled_errors;
    }

    private function compileSpecAndDb(array $object_spec, array $db_spec): array
    {
        $diff = [];
        foreach ($this->types as $type => $check) {
            if ($check && ($object_spec[$type] !== $db_spec[$type])) {
                $diff[$type] = [
                    'object_spec' => $object_spec[$type],
                    'db_spec'     => $db_spec[$type],
                ];
            }
        }

        return $diff;
    }

    private function buildErrorsListForClass(array &$class_details): ?array
    {
        $show        = false;
        $list_errors = [];
        foreach ($class_details['fields'] as $curr_field_name => &$curr_field) {
            $list_errors[$curr_field_name] = false;

            if (!isset($curr_field['db'])) {
                $curr_field['db']              = [];
                $curr_field['db']['no_column'] = true;
            }

            if (!isset($curr_field['object'])) {
                $curr_field['object'] = [];
            }

            if (!isset($curr_field['object']['db_spec'])) {
                $curr_field['object']['db_spec'] = [];
            }

            $check_params = true;
            $check_default = isset($curr_field['object']['spec']) && $curr_field['object']['spec'] !== 'bool';
            foreach (self::ERROR_TYPES as $err) {
                if (!isset($curr_field['db'][$err])) {
                    $curr_field['db'][$err] = null;
                }

                if (!isset($curr_field['object']['db_spec'][$err])) {
                    $curr_field['object']['db_spec'][$err] = null;
                }

                if ($this->types[$err] && ($curr_field['db'][$err] != $curr_field['object']['db_spec'][$err])) {
                    // If the object spec is enum and the db spec is varchar do no check params
                    if ($err === 'params' && !$check_params) {
                        continue;
                    }

                    // TODO Remove with https://gitlab.com/openxtrem/mediboard/-/issues/100976
                    if ($err === 'default' && !$check_default) {
                        continue;
                    }

                    // Allow an object enum spec to be a varchar in database
                    if ($curr_field['db'][$err] === 'VARCHAR' && $curr_field['object']['db_spec'][$err] === 'ENUM') {
                        $check_params = false;
                        continue;
                    }

                    $list_errors[$curr_field_name] = true;
                    $show                          = true;
                }
            }
        }

        return $show ? $list_errors : null;
    }

    /**
     * @throws Exception
     */
    private function buildClassesData(): void
    {
        // Foreach selected class with a table gather data
        foreach ($this->selected_classes as $class) {
            $object = $this->getInstance($class);

            if (($object === null) || !$object->getSpec()->table) {
                continue;
            }

            $this->list_classes[$class] = $this->getDetailsSelectedClasses($object);
        }
    }

    protected function getInstance(string $class_name): ?CStoredObject
    {
        try {
            try {
                // Disable SF error_handler for tests and cli purpose.
                // SF error_handler will transform errors to exceptions even with error_reporting(0)
                if (($_SERVER['APP_ENV'] === 'test') || (PHP_SAPI === 'cli')) {
                    set_error_handler(function () {
                    });
                }

                return new $class_name();
            } finally {
                if (($_SERVER['APP_ENV'] === 'test') || (PHP_SAPI === 'cli')) {
                    restore_error_handler();
                }
            }
        } catch (Throwable $e) {
            dump($e->getMessage());

            return null;
        }
    }


    /**
     * Extrait les d�tails d'une classe et les ajoute dans la liste des classes
     *
     * @param CStoredObject $object L'objet � traiter
     *
     * @throws Exception
     */
    private function getDetailsSelectedClasses(CStoredObject $object): array
    {
        // Cl� de la table
        $details = [
            'table'            => $object->_spec->table,
            'key'              => $object->_spec->key,
            'db_key'           => null,
            'fields'           => [],
            'fulltext_indexes' => [],
            'suggestion'       => null,
        ];


        $details = $this->extractClassFields($object, $details);
        $details = $this->extractClassProperties($object, $details);
        $this->extractDBFields($object, $details);

        return $details;
    }

    /**
     * Extrait les informations des champs de la classe et les mets dans la liste des classes
     *
     * @param CStoredObject $object  L'objet dont on veut extraire les propri�t�s des champs
     * @param array         $details Tableau contenant les informations
     *
     * @throws Exception
     */
    protected function extractClassFields(CStoredObject $object, array $details): array
    {
        // Extraction des champs de la classe
        $fields = $this->getObjectFields($object);

        foreach ($fields as $field_name => $prop_value) {
            $details['fields'][$field_name]['object'] = [
                'spec'    => null,
                'db_spec' => null,
            ];

            $details['fields'][$field_name]['db'] = null;

            $is_key = $field_name === $details['key'];

            // db fields
            if (isset($object->_specs[$field_name]) && ($spec = $object->_specs[$field_name])) {
                $db_spec = CMbFieldSpec::parseDBSpec($spec->getDBSpec());

                $db_spec['index'] = $this->shouldFieldHaveIndex($object, $field_name, $is_key);

                $prefix = '';
                // R�cup�ration des champs metas
                if (
                    $db_spec['index']
                    && (
                        (($spec instanceof CRefSpec) && $spec->meta)
                        || ($spec instanceof CEnumSpec)
                        || ($spec instanceof CStrSpec)
                    )
                ) {
                    $prefix = $this->getPrefixFromField($object, $spec, $field_name);

                    // Si prefix et champs meta alors on attribue la valeur du champ pour l'index
                    if ($prefix && isset($spec->meta)) {
                        $db_spec['index'] = $spec->meta . ', ' . $spec->fieldName;
                        // Permet de ne pas effacer l'index si le champ correspondant n'est pas encore trait�
                        $this->meta_fields[$object->_class][$prefix] = $db_spec['index'];
                        // Fixe l'index si le champ correpondant n'est pas encore trait�
                        $details['fields'][$spec->meta]['object']['db_spec']['index'] = $db_spec['index'];
                    }
                }

                if (array_key_exists($object->_class, $this->meta_fields)
                    && array_key_exists($prefix, $this->meta_fields[$object->_class])
                ) {
                    $db_spec['index'] = $this->meta_fields[$object->_class][$prefix];
                }

                $db_spec['null'] = !isset($spec->notNull) && !$is_key;

                $db_spec['default'] = $this->getDefaultValue($spec);

                // Some keys from external tables are str
                if ($is_key && ($spec instanceof CRefSpec)) {
                    $db_spec['unsigned'] = true;
                }

                $db_spec['extra'] = '';
                if (($field_name === $details['key']) && $object->_spec->incremented) {
                    $db_spec['extra'] = 'auto_increment';
                }

                $details['fields'][$field_name]['object']['db_spec'] = $db_spec;
            }
        }

        return $details;
    }

    /**
     * @throws Exception
     */
    protected function getObjectFields(CStoredObject $object): array
    {
        $object->getSpecs();

        return $object->getPlainFields();
    }

    private function shouldCheckMeta(CStoredObject $object, string $field_name): bool
    {
        if (array_key_exists($object->_class, $this->meta_fields)) {
            $field_parts = explode('_', $field_name);
            if (
                (count($field_parts) === 2)
                && array_key_exists($field_parts[0], $this->meta_fields[$object->_class])
            ) {
                return true;
            }
        }

        return false;
    }

    private function shouldFieldHaveIndex(
        CStoredObject $object,
        string        $field_name,
        bool          $is_key
    ): bool {
        if ($this->shouldCheckMeta($object, $field_name)) {
            return true;
        }

        $spec = $object->_specs[$field_name];
        if (($spec->index === '0') || ($spec instanceof CTextSpec) || ($spec instanceof CPasswordSpec)) {
            return false;
        }

        if (
            ($spec instanceof CRefSpec)
            || ($spec instanceof CDateTimeSpec)
            || ($spec instanceof CDateSpec)
            || ($spec instanceof CTimeSpec)
        ) {
            return true;
        }

        if ($is_key || $spec->autocomplete || ($spec->index !== null)) {
            return true;
        }

        foreach ($object->_spec->uniques as $unique) {
            if (in_array($field_name, $unique)) {
                return true;
            }
        }

        return false;
    }

    private function getDefaultValue(CMbFieldSpec $spec): ?string
    {
        if (($spec->default === null) && !$spec->notNull) {
            return null;
        }

        if ($spec->default === "NULL") {
            return "NULL";
        }

        if ($spec->default !== null) {
            return "{$spec->default}";
        }

        return null;
    }

    /**
     * R�cup�re le prefix commun des champs metas (object_id -> object_class, ...)
     *
     * @param CStoredObject $object     L'objet � traiter
     * @param CMbFieldSpec  $spec       La sp�cification du champs que l'on traite
     * @param string        $field_name Le nom du champs que l'on traite
     */
    private function getPrefixFromField(CStoredObject $object, CMbFieldSpec $spec, string $field_name): ?string
    {
        $prefix = null;
        if ($spec instanceof CRefSpec) {
            $prefix = trim(CMbString::getCommonPrefix($spec->meta, $field_name) ?? '', "_");

            if (!$prefix) {
                $prefix = "$spec->meta $field_name";
            }
        } else {
            foreach ($object->getPlainFields() as $_field => $_other) {
                if (!array_key_exists($_field, $object->_specs)) {
                    continue;
                }

                $_other_spec = $object->_specs[$_field];

                if (($_field === $field_name) || !($_other_spec instanceof CRefSpec)) {
                    continue;
                }

                if ($_other_spec->meta === $field_name) {
                    $prefix = trim(CMbString::getCommonPrefix($field_name, $_field) ?? '', "_");

                    if (!$prefix) {
                        $prefix = "$field_name $_field";
                    }
                }
            }
        }

        return $prefix;
    }

    /**
     * R�cup�re les propri�t�s de la classe et les mets dans la liste des classes
     *
     * @param CStoredObject $object  L'objet � traiter
     * @param array         $details Tableau contenant les informations
     */
    private function extractClassProperties(CStoredObject $object, array $details): array
    {
        // Extraction des propri�t�s de la classe
        foreach ($object->getProps() as $k => $v) {
            if (isset($k[0]) && ($k[0] !== '_')) {
                $details['fields'][$k]['object']['spec'] = $v;
            }
        }

        return $details;
    }

    /**
     * Extrait les champs de la base de donn�es et les met dans la liste des classes
     *
     * @param CStoredObject $object  L'objet � traiter
     * @param array         $details Tableau contenant les informations
     *
     * @throws Exception
     */
    private function extractDBFields(CStoredObject $object, array &$details): void
    {
        $ds = $object->_spec->ds;

        if (!$ds || !$object->_spec->table || !$ds->loadTable($object->_spec->table)) {
            $details['no_table']   = true;
            $details['duplicates'] = [];

            return;
        }

        $details['no_table'] = false;

        $this->initFieldDbData($object, $ds, $details);

        $list_row_sizes = TablesIntegrityChecker::getFieldsRowSizePerTable($object);

        if (!empty($list_row_sizes)) {
            foreach ($list_row_sizes as $curr_index) {
                $details['fields'][$curr_index['Column_name']]['db']['row_size'] = $curr_index['Row_size'];
            }
        }

        $this->extractIndexes($object, $ds, $details);
        $this->extractFullTextIndexes($object, $ds, $details);
    }

    /**
     * @throws Exception
     */
    private function initFieldDbData(CStoredObject $object, CSQLDataSource $ds, array &$details): void
    {
        $list_fields = $ds->loadList("SHOW COLUMNS FROM `{$object->_spec->table}`");

        foreach ($list_fields as $curr_field) {
            $details['fields'][$curr_field['Field']]['db'] = [];
            if (!isset($details['fields'][$curr_field['Field']]['object'])) {
                $details['fields'][$curr_field['Field']]['object']         = [];
                $details['fields'][$curr_field['Field']]['object']['spec'] = null;
            }

            $props = CMbFieldSpec::parseDBSpec($curr_field['Type']);

            $details['fields'][$curr_field['Field']]['db'] = [
                'type'     => $props['type'],
                'params'   => $props['params'],
                'unsigned' => $props['unsigned'],
                'zerofill' => $props['zerofill'],
                'null'     => ($curr_field['Null'] !== 'NO'),
                'default'  => $curr_field['Default'],
                'index'    => null,
                'fulltext' => null,
                'extra'    => $curr_field['Extra'],
            ];
        }
    }

    /**
     * @throws Exception
     */
    private function extractIndexes(CStoredObject $object, CSQLDataSource $ds, array &$details): void
    {
        // Extraction des Index
        $sql = "SHOW INDEXES FROM `{$object->_spec->table}` WHERE `index_type` != 'FULLTEXT'";

        $list_indexes = $ds->loadList($sql);

        $details['duplicates'] = $this->array_duplicates($list_indexes, 'Column_name');

        foreach ($list_indexes as $curr_index) {
            $details['fields'][$curr_index['Column_name']]['db']['index'] = $curr_index['Key_name'];
            if (array_key_exists($object->_class, $this->meta_fields)
                && array_key_exists($curr_index['Key_name'], $this->meta_fields[$object->_class])
            ) {
                $details['fields'][$curr_index['Column_name']]['db']['index'] =
                    $this->meta_fields[$object->_class][$curr_index['Key_name']];
            }

            if ($curr_index['Key_name'] === 'PRIMARY') {
                $details['db_key'] = $curr_index['Column_name'];
                if ($object->_spec->incremented) {
                    $details['fields'][$curr_index['Column_name']]['object']['db_spec']['extra'] = 'auto_increment';
                }
            }
        }
    }

    /**
     * @throws Exception
     */
    private function extractFullTextIndexes(CStoredObject $object, CSQLDataSource $ds, array &$details): void
    {
        /* Extraction des index fulltext */
        if (isset($object->_spec->seek) && ($object->_spec->seek === 'match')) {
            /* Check only classes with the spec "seek" property set to 'match' where 'like' is the default value */
            $seekable_fields = $object->getSeekables();
            if (!empty($seekable_fields)) {
                $seekable_fields_names                              = array_keys($seekable_fields);
                $details['fulltext_indexes'][$object->_spec->table] = [
                    "required_fields" => $seekable_fields_names,
                    "indexed_fields"  => [],
                    "name"            => 'seeker',
                    "status"          => 'missing',
                ];
            }

            $query = new CRequest();
            $query->addSelect(['index_name', 'group_concat(column_name) AS columns']);
            $query->addTable('`information_schema`.STATISTICS');
            $query->addWhere(
                [
                    'table_schema' => $ds->prepare('= ?', $ds->config['dbname']),
                    'table_name'   => $ds->prepare('= ?', $object->_spec->table),
                    'index_type'   => "= 'FULLTEXT'",
                ]
            );
            $query->addGroup('index_name');
            $fulltext_indexes = $ds->loadList($query->makeSelect());

            foreach ($details['fulltext_indexes'] as $_required_index) {
                foreach ($fulltext_indexes as $_fulltext_index) {
                    $fulltext_fields_in_index = explode(',', $_fulltext_index['columns']);
                    if (count(array_diff($_required_index['required_fields'], $fulltext_fields_in_index)) === 0) {
                        $details['fulltext_indexes'][$object->_spec->table]['status'] = 'valid';
                    } else {
                        $details['fulltext_indexes'][$object->_spec->table]['status'] = 'incomplete';
                    }
                    $details['fulltext_indexes'][$object->_spec->table]['indexed_fields'] = $fulltext_fields_in_index;
                }
            }
        }
    }

    public function setErrorTypes(array $types): void
    {
        foreach (self::ERROR_TYPES as $type) {
            $this->types[$type] = in_array($type, $types);
        }
    }

    public function getTypes(): array
    {
        return $this->types;
    }

    public function getClassList(): array
    {
        return $this->list_classes;
    }
}
