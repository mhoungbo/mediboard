<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement;

use Ox\Core\CAppUI;
use Ox\Core\Module\AbstractTabsRegister;

/**
 * @codeCoverageIgnore
 */
class CTabsDeveloppement extends AbstractTabsRegister
{
    public function registerAll(): void
    {
        $this->registerRoute('developpement_gui_logs', TAB_READ);
        $this->registerRoute('developpement_metrics_list', TAB_READ);
        $this->registerRoute('developpement_database_index', TAB_READ);
        $this->registerRoute('developpement_integrity_index', TAB_ADMIN);

        if (CAppUI::conf("debug")) {
            $this->registerRoute('developpement_translations', TAB_EDIT);
        } else {
            $this->registerRoute('developpement_translations_list', TAB_EDIT);
        }

        $this->registerRoute('developpement_data_model_index', TAB_READ);

        $this->registerRoute('developpement_tests_css', TAB_READ);
        $this->registerRoute('developpement_tests_thumbnail', TAB_READ);
        $this->registerRoute('developpement_classes_index', TAB_READ);
        $this->registerRoute('developpement_routes_stats', TAB_READ);
        $this->registerRoute('developpement_sandbox', TAB_READ);
    }
}
