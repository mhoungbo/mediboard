<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Configs;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\Config\Conf;
use Ox\Core\CSQLDataSource;
use Ox\Mediboard\System\Pref;

class ConfigurationsChecker
{
    /** Expected values of configs */
    public const CONFIGURATIONS = [
        'debug'                       => '1',
        'instance_role'               => 'qualif',
        'log_all_queries'             => '1',
        'application_log_using_nosql' => '1',
        'error_log_using_nosql'       => '1',
        'locale_warn'                 => '1',
        'session_handler'             => 'redis',
    ];

    /** Expected values of preferences */
    public const PREFERENCES = [
        'INFOSYSTEM' => '1',
    ];

    /**
     * @throws Exception
     */
    public function compareAndReturnConfigs(): array
    {
        $conf = new Conf();

        $configs = [];
        foreach (self::CONFIGURATIONS as $config => $expected) {
            $configs[] = [
                'config' => $config,
                'actual' => $actual = $conf->get($config),
                'isOK'   => $actual == $expected,
            ];
        }

        return array_merge($configs, $this->getAdditionalConfigs());
    }

    /**
     * Add additional check that are not configs
     */
    private function getAdditionalConfigs(): array
    {
        $configs = [];

        // DSN Slave
        $configs[] = [
            'config' => "DSN 'slave'",
            'isOK'   => (bool)CSQLDataSource::get("slave", true),
        ];

        return $configs;
    }

    /**
     * @throws Exception
     */
    public function compareAndReturnPrefs(): array
    {
        // user not authenticate
        if (!CAppUI::$instance->_ref_user) {
            return [];
        }

        $preference = new Pref();
        $prefs      = [];
        foreach (self::PREFERENCES as $pref => $expected) {
            $prefs[] = [
                'pref'   => $pref,
                'actual' => $actual = $preference->get($pref),
                'isOK'   => $actual == $expected,
            ];
        }

        return $prefs;
    }
}
