<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Logger;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CMbPath;
use Ox\Core\Elastic\ElasticObjectRepositories;
use Ox\Core\Elastic\Exceptions\ElasticClientException;
use Ox\Core\Elastic\Exceptions\ElasticException;
use Ox\Core\Logger\LoggerLevels;
use Ox\Mediboard\System\Elastic\ApplicationLog;
use Ox\Mediboard\System\Elastic\ApplicationLogRepository as ApplicationLogRepositoryElastic;
use Psr\Log\LoggerInterface;

/**
 * @phpstan-type SearchParams array{log_start: string, grep_search: string, grep_regex: string, grep_sensitive: bool, nb_logs: int}
 */
class ApplicationLogRepository
{
    final public const MODE_ELASTIC = 'elasticsearch';
    final public const MODE_FILE    = 'file';

    public function __construct(private LoggerInterface $logger)
    {
    }

    /**
     * @throws ElasticException
     * @throws ElasticClientException
     */
    public function search(string $mode = 'file', array $params = []): array
    {
        $params = $this->prepareParams($params);

        return ($mode === static::MODE_ELASTIC) ? $this->listUsingElastic($params) : $this->listUsingFile($params);
    }

    /**
     * @param array $params
     * @return SearchParams
     */
    private function prepareParams(array $params): array
    {
        $defaults = [
            'log_start'      => '',
            'grep_search'    => '',
            'grep_regex'     => false,
            'grep_sensitive' => false,
            'nb_logs'        => 1000,
        ];

        return array_merge($defaults, $params);
    }

    /**
     * @param SearchParams $params
     * @return array
     * @throws ElasticException|ElasticClientException
     */
    private function listUsingElastic(array $params): array
    {
        $max_logs    = $params["nb_logs"];
        $grep_search = $params['grep_search'];
        $log_start   = $params['log_start'];
        $repository  = new ApplicationLogRepositoryElastic();
        if ($grep_search == "") {
            $logs = $repository->list(
                $log_start,
                $max_logs,
            );
        } elseif (!$params['grep_regex']) {
            $logs = $repository->searchWithHighlighting(
                $max_logs,
                $grep_search,
                ["message^2", "context"],
                $log_start,
                ElasticObjectRepositories::SORTING_DATE_DESC
            );
        } else {
            $logs = $repository->searchWithRegexAndHighlighting(
                $max_logs,
                $grep_search,
                ["message", "context"],
                $params['grep_sensitive'],
                $log_start,
                ElasticObjectRepositories::SORTING_DATE_DESC
            );
        }

        $logs_display = [];
        /** @var ApplicationLog[] $logs */
        foreach ($logs as $log) {
            $logs_display[] = $log->prepareToRender();
        }

        return $logs_display;
    }

    /**
     * @param SearchParams $params
     * @return array
     * @throws Exception
     */
    private function listUsingFile(array $params): array
    {
        $file        = CApp::getPathApplicationLog();
        $file_grep   = str_replace(".log", ".grep.log", $file);
        $log_start   = $params['log_start'];
        $grep_search = $params['grep_search'];
        $grep_regex  = $params['grep_regex'];
        $words       = [];

        if ($grep_search) {
            // new grep file
            $cmd = "grep ";
            if (!$params['grep_sensitive']) {
                $cmd .= " -i ";
            }

            if (!$grep_regex) {
                if (str_contains($grep_search, " ")) {
                    $words = array_unique(explode(" ", $grep_search));
                } else {
                    $words = [$grep_search];
                }

                $cmd_repeat = ' | ' . $cmd;
                foreach ($words as $key => $_word) {
                    $_word = str_replace(".", "\.", $_word);
                    $_word = str_replace("[", "\[", $_word);
                    $_word = str_replace("]", "\]", $_word);

                    if ($key === 0) {
                        $cmd .= '"' . $_word . '"' . ' ' . $file;
                    } else {
                        $cmd .= $cmd_repeat . '"' . $_word . '"';
                    }
                }
            } else {
                $cmd .= " \"{$grep_search}\" {$file} ";
            }

            $cmd .= " > {$file_grep}";
            shell_exec($cmd);

            $file = $file_grep;
        }

        $nb_lines = $params['nb_logs'];

        $pattern = "/\[(?P<date>.*?)\] \[(?P<level>\w+)\] (?P<message>.*?) \[context:(?P<context>.*\]?)\] \[extra:(?P<extra>.*?)\]/";
        $logs    = CMbPath::tailWithSkip($file, $nb_lines, $log_start);
        $logs    = explode("\n", $logs);
        $logs    = array_reverse($logs);
        $logs    = array_filter($logs);

        $logs_display = [];
        foreach ($logs as $_log) {
            preg_match($pattern, $_log, $data);
            $logs_display[] = $this->prepareLogFromFileToRender($data);
        }

        // highlight
        if (!$grep_regex && !empty($words)) {
            foreach ($logs_display as &$_log) {
                foreach ($words as $_word) {
                    $_log['date']    = $this->highlight($_word, $_log['date']);
                    $_log['level']   = $this->highlight($_word, $_log['level']);
                    $_log['message'] = $this->highlight($_word, $_log['message']);

                    if ($_log['context_json'] !== $this->highlight($_word, $_log['context_json'])) {
                        $_log['context'] = $this->highlight($_log['context'], $_log['context']);
                    }

                    if ($_log['extra_json'] !== $this->highlight($_word, $_log['extra_json'])) {
                        $_log['extra'] = $this->highlight($_log['extra'], $_log['extra']);
                    }
                }
            }
        }

        return $logs_display;
    }

    /**
     * @return string|string[]|null
     */
    private function highlight($word, $subject)
    {
        $pos = stripos($subject, $word);

        if ($pos === false) {
            return $subject;
        }

        $replace = substr($subject, $pos, strlen($word));

        return str_ireplace($word, '<span style="background-color:yellow">' . $replace . '</span>', $subject);
    }

    private function prepareLogFromFileToRender(array $log): array
    {
        $extra         = $log["extra"];
        $context       = $log["context"];
        $short_context = (strlen($context) > 2) ? ("[context:" . strlen($context) . "]") : "";

        return [
            'date'         => "[" . $log["date"] . "]",
            'level'        => "[" . $log["level"] . "]",
            'color'        => LoggerLevels::getLevelColor($log["level"]),
            'message'      => $log["message"],
            'context'      => $short_context,
            'context_json' => $context,
            'extra'        => (strlen($extra) > 2) ? ("[extra:" . strlen($extra) . "]") : "",
            'extra_json'   => $extra,
        ];
    }
}
