<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Mediboard\System\Forms\CExClass;
use Ox\Mediboard\System\Forms\CExObject;

class TablesIntegrityChecker
{
    private const TABLE_MISSING = 'table_missing';
    private const CLASS_MISSING = 'class_missing';

    private array $db_tables = [];

    private const ROW_SIZE_TABLE_LOCK_THRESHOLD = 90;
    private const ROW_SIZE_PER_TYPE_SUB_QUERY = "CASE cols.DATA_TYPE
            WHEN 'tinyint' THEN 1
            WHEN 'smallint' THEN 2
            WHEN 'mediumint' THEN 3
            WHEN 'int' THEN 4
            WHEN 'bigint' THEN 8
            WHEN 'float' THEN IF(cols.NUMERIC_PRECISION > 24, 8, 4)
            WHEN 'double' THEN 8
            WHEN 'decimal' THEN ((cols.NUMERIC_PRECISION - cols.NUMERIC_SCALE) DIV 9)*4  + (cols.NUMERIC_SCALE DIV 9)*4 + CEIL(MOD(cols.NUMERIC_PRECISION - cols.NUMERIC_SCALE,9)/2) + CEIL(MOD(cols.NUMERIC_SCALE,9)/2)
            WHEN 'bit' THEN (cols.NUMERIC_PRECISION + 7) DIV 8
            WHEN 'year' THEN 1
            WHEN 'date' THEN 3
            WHEN 'time' THEN 3 + CEIL(cols.DATETIME_PRECISION /2)
            WHEN 'datetime' THEN 5 + CEIL(cols.DATETIME_PRECISION /2)
            WHEN 'timestamp' THEN 4 + CEIL(cols.DATETIME_PRECISION /2)
            WHEN 'char' THEN cols.CHARACTER_OCTET_LENGTH
            WHEN 'binary' THEN cols.CHARACTER_OCTET_LENGTH
            WHEN 'varchar' THEN IF(cols.CHARACTER_OCTET_LENGTH > 255, 2, 1) + cols.CHARACTER_OCTET_LENGTH
            WHEN 'varbinary' THEN IF(cols.CHARACTER_OCTET_LENGTH > 255, 2, 1) + cols.CHARACTER_OCTET_LENGTH
            WHEN 'tinyblob' THEN 9
            WHEN 'tinytext' THEN 9
            WHEN 'blob' THEN 10
            WHEN 'text' THEN 10
            WHEN 'mediumblob' THEN 11
            WHEN 'mediumtext' THEN 11
            WHEN 'longblob' THEN 12
            WHEN 'longtext' THEN 12
            WHEN 'enum' THEN 2
            WHEN 'set' THEN 8
            ELSE 0
        END AS `Row_size`";

    private array $tables_ok = [];

    private array $classes_to_test = [];

    private array $tables_integrity = [];

    private array $ds = [];

    private ?string $module = null;

    private string $dsn = '';

    private string $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function setDsn(string $dsn): void
    {
        $this->dsn = $dsn;
    }

    public function setModule(string $module): void
    {
        $this->module = $module;
    }

    /**
     * @throws Exception
     */
    public function checkTablesIntegrity(): array
    {
        /** @var CStoredObject[] $classes_to_test */
        $this->classes_to_test = $this->getClassesToTest();

        /** @var CStoredObject $_instance */
        foreach ($this->classes_to_test as $_instance) {
            if (!$_instance->isModelObjectAbstract() && $_instance->_ref_module) {
                [$dsn, $table_name] = $this->getTableFromInstance($_instance);

                if (!$dsn) {
                    continue;
                }

                if ($this->dsn && ($dsn !== $this->dsn)) {
                    continue;
                }

                $this->tables_integrity[] = new TableIntegrity(
                    get_class($_instance),
                    $table_name,
                    $_instance->_ref_module,
                    $dsn
                );

                // Init DS
                if (!isset($this->ds[$dsn])) {
                    $ds             = CSQLDataSource::get($dsn, true);
                    $this->ds[$dsn] = ($ds) ? CAppUI::conf("db {$dsn} dbname") : false;
                }
            }
        }

        if ((!$this->dsn || ($this->dsn === 'std')) && (!$this->module || (!$this->module === 'system'))) {
            $this->tables_integrity = $this->addExObjectTables($this->tables_integrity);
        }

        $this->db_tables = $this->getTablesFromDb(array_keys($this->ds));
        $this->db_tables = $this->countRowForDs($this->ds);

        /** @var TableIntegrity $_integrity */
        foreach ($this->tables_integrity as $_key => &$_integrity) {
            if (isset($this->db_tables[$_integrity->getTableName()])) {
                $_integrity->setRowCount($this->db_tables[$_integrity->getTableName()]);
                $_integrity->setTableExists(true);

                $this->tables_ok[$_integrity->getTableName()] = $this->db_tables[$_integrity->getTableName()];

                unset($this->db_tables[$_integrity->getTableName()]);

                if ($this->type === self::TABLE_MISSING) {
                    unset($this->tables_integrity[$_key]);
                }
            } elseif (isset($this->tables_ok[$_integrity->getTableName()])) {
                $_integrity->setRowCount($this->tables_ok[$_integrity->getTableName()]);
                $_integrity->setTableExists(true);

                if ($this->type === self::TABLE_MISSING) {
                    unset($this->tables_integrity[$_key]);
                }
            }
        }

        if (!$this->module && ($this->type !== self::TABLE_MISSING)) {
            if ($this->db_tables && ($this->type === self::CLASS_MISSING)) {
                $this->tables_integrity = [];
            }
            foreach ($this->db_tables as $_table_name => $_count) {
                $table_integrity = new TableIntegrity(null, $_table_name);
                $table_integrity->setRowCount(($_count !== null) ? $_count : 0);
                $this->tables_integrity[] = $table_integrity;
            }
        }

        return $this->tables_integrity;
    }

    /**
     * @throws Exception
     */
    private function getClassesToTest(): array
    {
        return (CClassMap::getInstance())->getClassChildren(CStoredObject::class, true, true, $this->module);
    }

    private function getTableFromInstance(CStoredObject $instance): array
    {
        if ($instance instanceof CExObject) {
            return [null, null];
        }

        $spec = $instance->getSpec();

        return [
            $spec->dsn,
            $spec->table,
        ];
    }

    private function getTablesFromInstances(array $instances): array
    {
        $tables = [];
        /** @var CStoredObject $_instance */
        foreach ($instances as $_instance) {
            if ($_instance instanceof CExObject) {
                continue;
            }

            $spec = $_instance->getSpec();
            if (!isset($tables[$spec->dsn])) {
                $tables[$spec->dsn] = [];
            }

            $tables[$spec->dsn][get_class($_instance)] = $spec->table;
        }

        // Handle ex_object special table names
        $tables['std'] = array_merge($tables['std'], $this->addExobjectTables());

        return $tables;
    }

    private function getTablesFromDb(array $dsn): array
    {
        $tables = [];
        foreach ($dsn as $_dsn) {
            $tables = array_merge($tables, $this->getTablesForDsn($_dsn));
        }

        return $tables;
    }

    private function getTablesForDsn(string $dsn): array
    {
        if (!($ds = CSQLDataSource::get($dsn, true))) {
            return [];
        }

        return $ds->loadColumn("SHOW TABLES");
    }

    private function addExObjectTables(array $class_integrity = []): array
    {
        foreach ($this->getExObjectTables() as $_ex_object_name => $_table_name) {
            $class_integrity[] = new TableIntegrity($_ex_object_name, $_table_name, 'system', 'std');
        }

        return $class_integrity;
    }

    private function getExObjectTables(): array
    {
        $ex_object_tables = [];
        foreach ((new CExClass())->loadIds() as $_id) {
            $ex_object_tables["CExObject-{$_id}"] = "ex_object_{$_id}";
        }

        return $ex_object_tables;
    }

    /**
     * @throws Exception
     */
    private function countRowForDs(array $db_names): ?array
    {
        $ds = CSQLDataSource::get('std');

        $query = new CRequest();
        $query->addSelect(['TABLE_NAME', 'TABLE_ROWS']);
        $query->addTable('INFORMATION_SCHEMA.`TABLES`');
        $query->addWhere(
            [
                'TABLE_SCHEMA' => $ds->prepareIn($db_names),
            ]
        );

        return $ds->loadHashList($query->makeSelect());
    }

    /**
     * @throws Exception
     */
    public static function getRowSizeConfiguration(): int
    {
        return round(
            CSQLDataSource::get('std')->loadHash(
                "SHOW VARIABLES WHERE `Variable_name` = 'innodb_page_size'"
            )["Value"] / 2
        );
    }

    /**
     * Extraction de la Row size par champ pour un objet donn�
     * (stockage maximal estim� pour stocker la valeur en BDD, en Bytes)
     *
     * @throws Exception
     */
    public static function getFieldsRowSizePerTable(CStoredObject $object): array
    {
        $sql = "SELECT UNIQUE
                cols.COLUMN_NAME AS `Column_name`,
                " . self::getRowSizePerTypeSubQuery() . "
            FROM INFORMATION_SCHEMA.COLUMNS cols
            WHERE cols.TABLE_NAME = '" . $object->_spec->table . "';";

        return $object->getDS()->loadList($sql);
    }

    /**
     * Extraction de la Row size par table
     * (stockage maximal estim� pour stocker la valeur en BDD, en Bytes)
     *
     * @param string|null $table_name
     *
     * @return array
     * @throws Exception
     */
    public static function getTablesRowSize(?string $table_name = null): array
    {
        $ds      = CSQLDataSource::get('std');
        $db_name = CAppUI::conf("db $ds->dsn dbname");

        $sql = $ds->prepare("SELECT col_sizes.TABLE_NAME AS `table_name`, SUM(col_sizes.row_size) AS `row_size`, COUNT(col_sizes.column_name) AS `columns_count`
            FROM (
                SELECT
                    cols.TABLE_NAME,
                    cols.COLUMN_NAME,
                    " . self::getRowSizePerTypeSubQuery() . "
                FROM INFORMATION_SCHEMA.COLUMNS cols
                WHERE cols.TABLE_SCHEMA = ?1", $db_name);

        if (!empty($table_name)) {
            $sql .= $ds->prepare(" AND `table_name` = ?1", $table_name);
        }

        $sql .= "
            ) AS col_sizes
            GROUP BY `table_name`
            ORDER BY `row_size` DESC;";

        return $ds->loadList($sql);
    }

    /**
     * Extraction du ratio entre la valeur actuelle de row_size d'une table et la valeur maximale
     *
     * @param string|null $table_name
     *
     * @return float
     * @throws Exception
     */
    public static function getTableRowSizePercentage(?string $table_name): float
    {
        $row_size   = 0;
        $table_data = self::getTablesRowSize($table_name);
        if (empty($table_data)) {
            return 0;
        }

        foreach ($table_data as $items) {
            if ($items['table_name'] === $table_name) {
                $row_size = $items['row_size'];
            }
        }

        return round(
            ($row_size / self::getRowSizeConfiguration()) * 100,
            2
        );
    }

    /**
     * Indique si la row_size d'une table d�passe un seuil de verrouillage
     *
     * @param string $table_name
     *
     * @return bool
     * @throws Exception
     */
    public static function isTableLockedByRowSize(string $table_name): bool
    {
        return (self::getTableRowSizePercentage($table_name) >= self::ROW_SIZE_TABLE_LOCK_THRESHOLD);
    }

    /**
     * @return string
     */
    private static function getRowSizePerTypeSubQuery(): string
    {
        return self::ROW_SIZE_PER_TYPE_SUB_QUERY;
    }
}
