<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\Controller;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Files\CThumbnail;
use Symfony\Component\HttpFoundation\Response;

class TestsController extends Controller
{
    public function css(): Response
    {
        $matches = [];
        preg_match_all(
            '/button\:not\(\[class\^=v\-\]\)\.([^\:]+)\:\:before/',
            file_get_contents($this->getRootDir() . "/style/mediboard_ext/standard.css"),
            $matches
        );

        $button_classes = array_filter(
            array_unique(array_merge([], $matches[1])),
            fn($button_class) => strpos($button_class, '.') === false
        );

        $values_to_remove = [
            "notext",
            "me-notext",
            "me-btn-small",
            "rtl",
            "me-noicon",
            "me-small",
            "me-color-care[style*=forestgreen]",
            "me-color-care[style*=firebrick]",
            "me-dark",
            "me-secondary",
            "delete",
        ];

        foreach ($values_to_remove as $value) {
            CMbArray::removeValue($value, $button_classes);
        }

        return $this->renderSmarty(
            'tests/css_test.tpl',
            [
                "button_classes" => array_values($button_classes),
            ]
        );
    }

    public function thumbnail(): Response
    {
        $group = CGroups::loadCurrent();

        /** VERSION */
        $version = [];
        exec("magick --version", $ret);
        if (!$ret) {
            exec("convert --version", $ret);
        }

        $version['version_imagemagick'] = $ret;
        $version['imagick_exists']      = extension_loaded('Imagick');
        $version['gd_exists']           = extension_loaded('GD');
        $version['imagine']             = CThumbnail::checkImagineExists();

        /** PROFILE */
        $profile = [];
        $group->loadBackRefs('files', null, 1, null, null, null, null, [
            'file_name' => "= 'thumbnail_tester.pdf'",
        ]);
        $profile['file']     = $this->getPdfFileContent($group, 'thumbnail_tester.pdf', 'application/pdf');
        $profile['profiles'] = array_keys(CThumbnail::PROFILES);

        /** QUALITY */
        $quality = [];
        $group->loadBackRefs('files', null, 1, null, null, null, null, [
            'file_name' => "= 'thumbnail_jpeg.jpg'",
        ]);
        $quality['file'] = $this->getPdfFileContent($group, 'thumbnail_jpeg.jpg', 'image/jpg');

        return $this->renderSmarty(
            'tests/thumbnail_tester.tpl',
            [
                'version' => $version,
                'profile' => $profile,
                'quality' => $quality,
            ]
        );
    }

    /**
     * @throws Exception
     */
    private function getPdfFileContent(CGroups $group, string $filename, string $filetype): CFile
    {
        if (!$group->_back['files']) {
            $content = file_get_contents($this->getRootDir() . "/modules/dPdeveloppement/images/{$filename}");

            $file            = new CFile();
            $file->file_name = $filename;
            $file->setObject($group);
            $file->file_type = $filetype;
            $file->fillFields();

            $file->loadMatchingObjectEsc();
            $file->updateFormFields();
            $file->setContent($content);

            $file->file_date = CMbDT::dateTime();

            if ($file && !$file->_id) {
                if ($msg = $file->store()) {
                    CAppUI::stepAjax($msg, UI_MSG_ERROR);
                }
            }
        } else {
            /** @var CFile $file */
            $file = reset($group->_back['files']);
        }

        return $file;
    }
}
