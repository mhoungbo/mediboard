<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Ox\Core\Controller;
use Ox\Core\EntryPoint;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class SandboxController extends Controller
{
    public function sandbox(RouterInterface $router): Response
    {
        $entry_point = new EntryPoint('sandbox', $router);
        $entry_point->setScriptName('developpementSandbox');

        return $this->renderSmarty('sandbox/sandbox', [
            'sandbox' => $entry_point,
        ]);
    }
}
