<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Exception;
use Ox\Core\CMbObject;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Developpement\ModelGraph;
use Symfony\Component\HttpFoundation\Response;

/**
 * Handle the display of data model graph and classes properties.
 */
class DataModelController extends Controller
{
    /**
     * Display the data model graph.
     *
     * @throws Exception
     */
    public function show(RequestParams $params): Response
    {
        if ($object_class = $params->get("object_class", "str")) {
            return $this->renderDataModel($object_class, $params);
        }

        // Cr�ation d'une instance de ModelGraph et initialisation
        $data_model = new ModelGraph();
        $data_model->init(
            [
                'class_select'   => 'CPatient',
                'hierarchy_sort' => 'basic',
                'number'         => 1,
            ]
        );

        return $this->renderSmarty(
            'datamodel/vw_data_model.tpl',
            [
                'data_model'             => $data_model,
                'url_display_data_model' => $this->generateUrl('developpement_data_model_index'),
            ]
        );
    }

    /**
     * Display the details about a class. Display its properties, references...
     *
     * @throws Exception
     */
    public function showClassDetails(RequestParams $params): Response
    {
        $class           = $params->get("class", "str notNull");
        $show_properties = $params->get("show_properties", "bool default|0");
        $show_backs      = $params->get("show_backs", "bool default|0");
        $show_formfields = $params->get("show_formfields", "bool default|0");
        $show_heritage   = $params->get("show_heritage", "bool default|0");
        $show_refs       = $params->get("show_refs", "bool default|0");

        if (!class_exists($class)) {
            $this->addUiMsgError("Classe inexistante $class", true);

            return $this->renderEmptyResponse();
        }

        $data_model = new ModelGraph();
        $data_model->init(
            [
                'show_properties' => $show_properties,
                'show_backs'      => $show_backs,
                'show_formfields' => $show_formfields,
                'show_heritage'   => $show_heritage,
                'show_refs'       => $show_refs,
                'class_select'    => $class,
            ]
        );

        if (!$params->get('complete', 'bool default|0')) {
            return $this->renderClassDetails($data_model);
        }

        return $this->renderSmarty(
            'datamodel/inc_data_model.tpl',
            [
                'data_model'             => $data_model,
                'url_display_data_model' => $this->generateUrl('developpement_data_model_class_details'),
            ]
        );
    }

    /**
     * Render the details of the class.
     *
     * @throws Exception
     */
    private function renderClassDetails(ModelGraph $data_model): Response
    {
        /** @var CMbObject $object */
        $object    = new $data_model->class_select();
        $backprops = $object->getBackProps();

        // R�cup�ration des diff�rents champs de la classe (propri�t�s calcul�es, h�rit�es et normales)
        $fields_types = $data_model->getFields($object);
        $plainfield   = $fields_types["plainfield"];
        $refs         = $fields_types["refs"];

        return $this->renderSmarty(
            'datamodel/inc_details_class.tpl',
            [
                "plainfield"             => $plainfield,
                "formfield"              => $fields_types["formfield"],
                "backprops"              => $backprops,
                "heritage"               => $fields_types["heritage"],
                "db_spec"                => $data_model->getDB_Specs($plainfield, $refs, $object),
                "db_table_row_size"      => $data_model->getTableRowSize($object),
                "db_table_columns_sizes" => $data_model->getTableColumnSizes($object),
                "refs"                   => $refs,
                "data_model"             => $data_model,
            ]
        );
    }

    /**
     * Render the data model with $class as the target.
     *
     * @throws Exception
     */
    private function renderDataModel(string $class, RequestParams $params): Response
    {
        $number          = $params->get("number", "num min|1 default|1");
        $backsprops_show = $params->get(
            "show_backprops",
            "enum list|" . implode('|', ModelGraph::BACKPROPS_OPTIONS) . " default|none"
        );
        $hierarchy_sort  = $params->get(
            "hierarchy_sort",
            "enum list|" . implode('|', ModelGraph::HIERARCHY_OPTIONS) . " default|hubsize"
        );
        $show_hover      = $params->get("show_hover", "bool default|1");
        $show_props      = $params->get("show_props", "bool default|1");


        if (!class_exists($class)) {
            $this->addUiMsgError("La classe $class n'existe pas.", true);

            return $this->renderEmptyResponse();
        }

        // Instanciation de ModelGraph et initialisation avec les valeurs r�cup�r�es
        $data_model = new ModelGraph();
        $data_model->init(
            [
                'class_select'   => $class,
                'show_backprops' => $backsprops_show,
                'hierarchy_sort' => $hierarchy_sort,
                'number'         => $number,
                'show_hover'     => $show_hover,
                'show_props'     => $show_props,
            ]
        );

        // R�cup�ration des donn�es qui permettent de cr�er le graph
        $graph     = $data_model->getGraph();
        $inv_class = [];
        foreach ($graph[0]['links'] as $_value) {
            /** @var CMbObject $_class */
            $_class = new $_value();
            $_backs = $_class->getBackProps();
            foreach ($_backs as $_field_name => $_back_value) {
                $val = explode(" ", $_back_value);
                if ($val[0] == $graph[0]["class"]) {
                    if (array_key_exists($_value, $inv_class)) {
                        $inv_class[$_value] .= "/" . $_field_name;
                    } else {
                        $inv_class[$_value] = $_field_name;
                    }
                }
            }
        }
        // R�cup�ration des donn�es qui permettent de cr�er les backprops (en fonction des choix de l'utilisateur)
        $backs      = $data_model->showBackProps();
        $back_split = [];
        $backs_name = [];
        foreach ($backs as $_back) {
            $expl                 = explode(" ", $_back);
            $back_split[]         = $expl[0];
            $backs_name[$expl[0]] = $expl[1];
        }
        $back_split = array_unique($back_split);

        return $this->renderSmarty(
            'datamodel/inc_draw_graph.tpl',
            [
                "graph"             => $graph,
                "backs"             => $back_split,
                "inv_class"         => $inv_class,
                "backs_name"        => $backs_name,
                "hierarchy_sort"    => $data_model->hierarchy_sort,
                "show_hover"        => $data_model->show_hover,
                "url_class_details" => $this->generateUrl('developpement_data_model_class_details'),
            ]
        );
    }
}
