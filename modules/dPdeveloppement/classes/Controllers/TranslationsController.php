<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Exception;
use Ox\Core\Cache;
use Ox\Core\CAppUI;
use Ox\Core\CMbConfig;
use Ox\Core\CMbPath;
use Ox\Core\CMbString;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Core\Translation;
use Ox\Mediboard\System\CCSVImportTranslations;
use Ox\Mediboard\System\CTranslationOverwrite;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class TranslationsController extends Controller
{
    public function index(): Response
    {
        return $this->renderSmarty('translations/vw_translations.tpl');
    }

    public function listTranslations(RequestParams $params): Response
    {
        // If param integrate is true, only return integrate translations
        if ($params->get('integrate', 'bool default|0')) {
            return $this->listIntegrateTranslations($params);
        }

        $module    = $params->get('module', 'str default|system');
        $language  = $params->get('language', 'str default|' . $this->pref->get('LOCALE'));
        $reference = $params->get('reference', 'str default|' . $this->pref->get('FALLBACK_LOCALE'));
        $start     = $params->get('start', 'num default|0');
        $step      = $params->get('step', 'num default|500');
        $reference = ($reference === '') ? $this->pref->get('FALLBACK_LOCALE') : $reference;

        $translation  = new Translation($module, $language, $reference);
        $translations = $translation->getTranslations();

        // Modules + common + styles
        $modules   = array_keys(CModule::getInstalled());
        $modules[] = "common";
        sort($modules);

        $items         = $translation->getItems();
        $counter_total = 0;
        if (isset($items['Other'])) {
            foreach ($items['Other'] as $_item) {
                $counter_total += count($_item);
            }
        }

        return $this->renderSmarty(
            'translations/mnt_traduction_classes.tpl',
            [
                'total_count'   => $translation->getTotalCount(),
                'local_count'   => $translation->getLocalCount(),
                'completion'    => $translation->getCompletion(),
                'items'         => $translation->getItems(),
                'archives'      => $translation->getArchives(),
                'completions'   => $translation->getCompletions(),
                'locales'       => $translation->getLanguages(),
                'modules'       => $modules,
                'module'        => $module,
                'trans'         => $translations,
                'language'      => $language,
                'reference'     => $reference,
                'ref_items'     => $translation->getRefItems(),
                'start'         => $start,
                'step'          => $step,
                'counter_total' => $counter_total,
            ]
        );
    }

    public function saveTranslations(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('developpement_translations_save', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        // only user_type of Administrator (1) can access
        $can       = $this->request_context->getCan();
        $can->edit |= ($this->getCUser()->user_type != 1);
        $can->needsEdit();

        // If param integrate is true, save integrate translations
        if ($params->post('integrate', 'bool default|0')) {
            return $this->saveIntegrateTranslations($params);
        }

        $module_name = $params->post('module', 'str notNull');
        $strings     = $params->post('s', 'str');
        $language    = $params->post('language', 'enum list|fr|de|en|fr-de|nb-be|it');
        $trad_key    = $params->post('key', 'str');
        $trad_value  = $params->post('value', 'str');

        if (!$module_name || (!$trad_key && (!$strings || !is_array($strings)))) {
            $this->addUiMsgError('mod-dPdeveloppement-msg-Infos are missing');
        }

        if (!$strings && $trad_key) {
            $strings = [$trad_key => $trad_value];
        }

        // Redirect the translations to the CTranslationOverwrite in DB
        if (!$this->conf->get('debug')) {
            foreach ($strings as $_key => $_value) {
                if (!trim($_value)) {
                    continue;
                }

                $translation_overwrite           = new CTranslationOverwrite();
                $translation_overwrite->language = $language;
                $translation_overwrite->source   = trim($_key);
                $translation_overwrite->loadMatchingObjectEsc();

                if (!$translation_overwrite->_id && (CAppUI::localExists($_key, $_value, true))) {
                    continue;
                }

                $translation_overwrite->translation = trim(str_replace('\\', '', $_value));

                if ($msg = $translation_overwrite->store()) {
                    $this->addUiMsgWarning($msg);
                } else {
                    $this->addUiMsgOk(
                        "CTranslationOverwrite-msg-" . (($translation_overwrite->_id) ? 'modify' : 'create')
                    );
                }
            }

            return $this->renderEmptyResponse();
        }

        // Ecriture du fichier
        $translate_module             = new CMbConfig();
        $translate_module->options    = ["name" => "locales"];
        $translate_module->targetPath = ($module_name != "common") ? "modules/{$module_name}/locales/{$language}.php" : "locales/{$language}/common.php";
        $translate_module->sourcePath = $translate_module->targetPath;

        if (!is_file($translate_module->targetPath)) {
            try {
                CMbPath::forceDir(dirname($translate_module->targetPath));
                file_put_contents(
                    $translate_module->targetPath,
                    '<?php $locales["module-' . $module_name . '-court"] = "' . $module_name . '";'
                );
            } catch (Throwable $e) {
                $this->addUiMsgError($e->getMessage());
            }
        }

        $translate_module->load();

        if (!$strings && $trad_key) {
            $strings = [
                $trad_key => $trad_value,
            ];
        }

        foreach ($strings as $key => $value_string) {
            if ($value_string !== "") {
                $translate_module->values[$key] = CMbString::purifyHTML(stripslashes($value_string));
            } else {
                unset($translate_module->values[$key]);
            }
        }

        uksort($translate_module->values, "strnatcmp");

        // Initialize cache
        $cache = Cache::getCache(Cache::OUTER);

        try {
            if ($translate_module->update($translate_module->values, false)) {
                $cache->delete("locales-$language-");
                $this->addUiMsgOk('mod-dPdeveloppement-msg-Locales file saved');
            }
        } catch (Throwable $e) {
            $cache->delete("locales-$language-");
            $this->addUiMsgError('mod-dPdeveloppement-msg-Error while saving locales file', false, $e->getMessage());
        }

        return $this->renderEmptyResponse();
    }

    public function listIntegrateTranslations(RequestParams $params): Response
    {
        $start = $params->get('start', 'num default|0');
        $step  = $params->get('step', 'num default|30');

        if (!$this->conf->get('debug')) {
            $this->addUiMsgError('common-error-An error occurred');
        }

        $locales      = CAppUI::getLocalesFromFiles();
        $overwrite    = new CTranslationOverwrite();
        $translations = $overwrite->loadList(null, $overwrite->_spec->key . ' ASC', "$start,$step");

        $all_translations = [];
        foreach ($translations as $_translation) {
            [, $value, $prefix] = CAppUI::splitLocale($_translation->source);
            $module = CCSVImportTranslations::getCorrespondingModule($prefix, $value);

            if (!array_key_exists($module, $all_translations)) {
                $all_translations[$module] = [];
            }

            $all_translations[$module][] = [
                'id'        => $_translation->_id,
                'key'       => $_translation->source,
                'old_value' => $locales[$_translation->source] ?? '',
                'value'     => $_translation->translation,
                'language'  => $_translation->language,
            ];
        }

        return $this->renderSmarty(
            'translations/vw_integrate_translations.tpl',
            [
                'translations' => $all_translations,
                'total'        => $overwrite->countList(),
                'start'        => $start,
                'step'         => $step,
            ]
        );
    }

    public function saveIntegrateTranslations(RequestParams $params): Response
    {
        if (!$this->conf->get('debug')) {
            $this->addUiMsgError('common-error-An error occurred');
        }

        if (!($translations_ids = $params->post('translations_ids', 'str notNull'))) {
            $this->addUiMsgError('dPdeveloppement-translation-integration no ids');
        }

        // get each CTranslationOverwrite id in an array to load them
        $translations_ids = explode('|', $translations_ids);

        // For each translation we try to find the appropriate module or put it in common
        $all_translations = [];
        foreach ((new CTranslationOverwrite())->loadAll($translations_ids) as $_translation) {
            [, $value, $prefix] = CAppUI::splitLocale($_translation->source);
            $module = CCSVImportTranslations::getCorrespondingModule($prefix, $value);

            if (!array_key_exists($module, $all_translations)) {
                $all_translations[$module] = [];
            }

            if (!array_key_exists($_translation->language, $all_translations[$module])) {
                $all_translations[$module][$_translation->language] = [];
            }

            $all_translations[$module][$_translation->language][] = $_translation;
        }

        // Initialize cache
        $cache = Cache::getCache(Cache::OUTER);

        // Store the translations per module and lang
        $langs_to_update = [];
        foreach ($all_translations as $_module => $_values) {
            foreach ($_values as $_lang => $_vals) {
                // Store the different langs to empty cache for them
                if (!in_array($_lang, $langs_to_update)) {
                    $langs_to_update[] = $_lang;
                }

                // Get the file to update
                $translate_module          = new CMbConfig();
                $translate_module->options = ["name" => "locales"];

                // Update translation file
                $translate_module->targetPath = ($_module == 'common') ? "locales/{$_lang}/common.php" : "modules/{$_module}/locales/{$_lang}.php";
                $translate_module->sourcePath = $translate_module->targetPath;

                // Create the locales file if needed
                if (!is_file($translate_module->targetPath)) {
                    CMbPath::forceDir(dirname($translate_module->targetPath));
                    file_put_contents(
                        $translate_module->targetPath,
                        '<?php $locales["module-' . $_module . '-court"] = "' . $_module . '";'
                    );
                }

                $translate_module->load();

                // Prepare the translations to update or create
                /** @var CTranslationOverwrite $_trans */
                foreach ($_vals as $_trans) {
                    if ($_trans->translation !== "") {
                        $translate_module->values[$_trans->source] = CMbString::purifyHTML(
                            stripslashes($_trans->translation)
                        );
                    } else {
                        unset($translate_module->values[$_trans->source]);
                    }
                }

                uksort($translate_module->values, "strnatcmp");

                try {
                    if ($translate_module->update($translate_module->values, false)) {
                        $this->addUiMsgOk("mod-dPdeveloppement-msg-Locales file saved");

                        /** @var CTranslationOverwrite $_trad */
                        foreach ($_vals as $_trad) {
                            if ($msg = $_trad->delete()) {
                                $this->addUiMsgWarning($msg);
                            }
                        }
                    }
                } catch (Exception $e) {
                    $this->addUiMsgWarning(
                        'mod-dPdeveloppement-msg-Error while saving locales file',
                        false,
                        $e->getMessage()
                    );
                }

                // Remove the keys from SHM
                $cache->delete("locales-{$_lang}-");
            }
        }

        return $this->renderEmptyResponse();
    }
}
