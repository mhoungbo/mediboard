<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\System\CErrorLog;
use Ox\Mediboard\System\CErrorLogWhiteList;
use Ox\Mediboard\System\Elastic\ErrorLogRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class ErrorLogWhitelistController extends Controller
{
    public function list(): Response
    {
        $error_log_whitelist = new CErrorLogWhiteList();
        $list                = $error_log_whitelist->loadList();
        CStoredObject::massLoadFwdRef($list, 'user_id');

        return $this->renderSmarty(
            "logs/inc_list_error_log_whitelist.tpl",
            [
                "list" => $list,
            ]
        );
    }

    public function toggle(RequestParams $param, LoggerInterface $logger): Response
    {
        $id             = $param->post('error_log_id', 'str');
        $is_elastic_log = $param->post('is_elastic_log', 'str');

        $submitted_token = $param->getRequest()->request->get('token');

        if (!$this->isCsrfTokenValid('error_log_toogle_whitelist', $submitted_token)) {
            throw new InvalidCsrfTokenException();
        }

        if ($is_elastic_log === "true") {
            $error     = (new ErrorLogRepository())->findById($id);
            $error_log = $error->toCErrorLog();
        } else {
            $error_log = new CErrorLog();
            $error_log->load($id);
        }

        $error_log_whitelist       = new CErrorLogWhiteList();
        $error_log_whitelist->hash = $error_log->signature_hash;
        $error_log_whitelist->loadMatchingObject();

        if ($error_log_whitelist->_id) {
            $msg = $error_log_whitelist->delete();
            if ($msg) {
                $logger->error($msg);
            } else {
                $this->addUiMsgOk('CErrorLogWhiteList-msg-deleted');
            }
        } else {
            $error_log_whitelist->text        = $error_log->text;
            $error_log_whitelist->type        = $error_log->error_type;
            $error_log_whitelist->file_name   = $error_log->file_name;
            $error_log_whitelist->line_number = $error_log->line_number;
            $error_log_whitelist->user_id     = CAppUI::$instance->_ref_user->user_id;
            $error_log_whitelist->datetime    = CMbDT::dateTime();
            $error_log_whitelist->count       = 0;

            $msg = $error_log_whitelist->store();

            if ($error_log_whitelist->_id) {
                $this->addUiMsgOk('CErrorLog.whitelist_added');
            } else {
                $logger->error($msg);
            }
        }

        return $this->renderEmptyResponse();
    }

    public function delete(RequestParams $param): Response
    {
        $id  = $param->post("id", "num");
        $all = $param->post("all", "bool");

        if (!$this->isCsrfTokenValid('logs_errors_whitelist_delete', $param->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $wl = new CErrorLogWhiteList();

        if ($all) {
            $ds    = $wl->getDS();
            $query = "TRUNCATE {$wl->_spec->table}";
            $ds->exec($query);
            $this->addUiMsgOk('CErrorLogWhiteList-msg-emptied');
        } else {
            if (!$id) {
                trigger_error('CErrorLogWhiteList-error-missing id');
            }
            $wl->error_log_whitelist_id = $id;
            $wl->loadMatchingObject();
            $wl->delete();
            $this->addUiMsgOk('CErrorLogWhiteList-msg-deleted');
        }

        return $this->renderEmptyResponse();
    }
}
