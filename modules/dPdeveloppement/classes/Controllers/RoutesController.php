<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Ox\Core\CClassMap;
use Ox\Core\CMbString;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Kernel\Routing\RouteManager;
use Ox\Mediboard\Developpement\Index\RouteIndexer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class RoutesController extends Controller
{
    public function stats(): Response
    {
        $route_manager = new RouteManager();
        $ressources    = [];
        foreach ($route_manager->getRessources() as $file_path) {
            $info                           = pathinfo($file_path);
            $ressources[$info['dirname']][] = $info['basename'];
        }

        return $this->renderSmarty(
            'routes/vw_routes.tpl',
            [
                'file'         => $file = $route_manager->getAllRoutesPath(),
                'file_date'    => filectime($file),
                'file_size'    => CMbString::toDecaBinary(filesize($file)),
                'routes_count' => count($route_manager->loadAllRoutes(false)->getRouteCollection()),
                'ressources'   => $ressources,
                'root_dir'     => $this->getRootDir(),
            ]
        );
    }

    public function list(RequestParams $params): Response
    {
        // Common vars
        $filter_module = $params->get('filter_module', 'str');

        // Routes
        $routes = [];
        foreach ((new RouteIndexer())->search($filter_module) as $route) {
            $routes[$route->getRouteName()] = [
                'controller' => $route->getController(),
                'action'     => $route->getAction(),
                'path'       => $route->getPath(),
                'module'     => $route->getModule(),
                'method'     => str_replace(",", "|", $route->getMethods()),
            ];
        }

        // Actions
        $legacy_actions = CClassMap::getInstance()->getLegacyActions($filter_module);
        $legacy_actions = $filter_module ? [$filter_module => $legacy_actions] : $legacy_actions;
        $count_actions  = 0;
        foreach ($legacy_actions as $actions) {
            $count_actions += count($actions);
        }

        // Scripts
        $root         = $this->getRootDir();
        $glob_scripts = ($filter_module) ? glob($root . '/modules/' . $filter_module . '/*.php') : glob(
            $root . '/modules/*/*.php'
        );

        $scripts = [];
        foreach ($glob_scripts as $script) {
            $script = str_replace($root . '/modules/', '', $script);
            [$module, $script] = explode('/', $script);
            if (!isset($scripts[$module])) {
                $scripts[$module] = [];
            }
            $scripts[$module][] = $script;
        }

        // dosql
        $glob_dosql = ($filter_module) ? glob($root . '/modules/' . $filter_module . '/controllers/*.php') : glob(
            $root . '/modules/*/controllers/*.php'
        );

        $dosql_scripts = [];
        foreach ($glob_dosql as $dosql) {
            $dosql = str_replace($root . '/modules/', '', $dosql);
            [$module, $dosql] = explode('/', $dosql);
            if (!isset($dosql_scripts[$module])) {
                $dosql_scripts[$module] = [];
            }
            $dosql_scripts[$module][] = $dosql;
        }

        return $this->renderSmarty(
            'routes/inc_list_routes.tpl',
            [
                'filter_module' => $filter_module,
                'route_display' => $routes,
                'count_routes'  => $count_routes = count($routes),
                'actions'       => $legacy_actions,
                'count_actions' => $count_actions,
                'scripts'       => $scripts,
                'count_scripts' => $count_scripts = count($glob_scripts),
                'dosql_scripts' => $dosql_scripts,
                'count_dosql'   => $count_dosql = count($glob_dosql),
                'count_total'   => $count_dosql + $count_routes + $count_actions + $count_scripts,
            ]
        );
    }

    public function show(string $route_name, RouterInterface $router): Response
    {
        $route = $router->getRouteCollection()->get($route_name);

        return $this->renderSmarty(
            'routes/vw_details_route.tpl',
            [
                'json' => json_encode(
                    [
                        'Condition'    => $route->getCondition(),
                        'Defaults'     => $route->getDefaults(),
                        'Host'         => $route->getHost(),
                        'Methods'      => $route->getMethods(),
                        'Options'      => $route->getOptions(),
                        'Path'         => $route->getPath(),
                        'Requirements' => $route->getRequirements(),
                        'Schemes'      => $route->getSchemes(),
                    ],
                    JSON_PRETTY_PRINT
                ),
            ]
        );
    }

    public function autocomplete(RequestParams $params): Response
    {
        $this->enforceSlave();

        $search       = $params->get('search', 'str');
        $only_offline = $params->get('offline', 'bool default|0');

        $indexer = new RouteIndexer();
        $routes  = $only_offline ? $indexer->findOffline($search) : $indexer->search($search);

        return $this->renderSmarty(
            'routes/autocomplete_routes.tpl',
            [
                'routes'       => array_slice($routes, 0, 10),
                'search'       => $search,
                'only_offline' => $only_offline,
            ]
        );
    }
}
