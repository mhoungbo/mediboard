<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Exception;
use Ox\Core\CClassMap;
use Ox\Core\Config\Conf;
use Ox\Core\Controller;
use Ox\Core\FieldSpecs\CBoolSpec;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Developpement\Database\ClassDefinitionChecker;
use Ox\Mediboard\Developpement\Database\IndexChecker;
use Ox\Mediboard\Developpement\TablesIntegrityChecker;
use Symfony\Component\HttpFoundation\Response;

/**
 * Database integrity checks
 */
class DatabaseController extends Controller
{
    /**
     * Index of the different tools to check database integrity.
     *
     * @throws Exception
     */
    public function index(RequestParams $params, Conf $conf): Response
    {
        if ($params->get('class_specs', 'bool default|0')) {
            return $this->renderSmarty(
                'database/mnt_table_classes.tpl',
                [
                    'installed_modules'     => $this->getInstalledModules(),
                    'types'                 => ClassDefinitionChecker::ERROR_TYPES,
                    'url_check_class_specs' => $this->generateUrl('developpement_database_check_class_spec'),
                ]
            );
        } elseif ($params->get('indexes', 'bool default|0')) {
            $module = $params->get('index_module', 'str');

            return $this->renderSmarty(
                'database/vw_indexes.tpl',
                [
                    'error_types'       => IndexChecker::ERROR_TYPES,
                    'key_types'         => IndexChecker::KEY_TYPES,
                    'module_list'       => $this->getInstalledModules(),
                    'module'            => $module,
                    'url_check_indexes' => $this->generateUrl('developpement_database_check_indexes'),
                ]
            );
        } elseif ($params->get('tables_integrity', 'bool default|0')) {
            $dsn = array_keys($conf->get('db'));

            return $this->renderSmarty(
                'database/vw_tables_integrity.tpl',
                [
                    'dsn'                        => $dsn,
                    'modules'                    => $this->getInstalledModules(),
                    'url_check_tables_integrity' => $this->generateUrl('developpement_database_check_tables_integrity'),
                ]
            );
        }

        return $this->renderSmarty(
            'database/vw_db_checks.tpl',
            [
                'url_class_specs' => $this->generateUrl('developpement_database_index', ['class_specs' => 1]),
                'url_indexes'     => $this->generateUrl('developpement_database_index', ['indexes' => 1]),
                'url_integrity'   => $this->generateUrl('developpement_database_index', ['tables_integrity' => 1]),
                'url_row_sizes'   => $this->generateUrl('developpement_database_check_row_sizes'),
            ]
        );
    }

    /**
     * Compare the database columns types for each classes to the expected type from the field.
     *
     * @throws Exception
     */
    public function checkClassSpecs(RequestParams $params): Response
    {
        $module      = $params->get("module", "str");
        $class       = $params->get("class", "str");
        $error_types = $params->get("types", "str");

        $this->enforceSlave();

        $to_check = null;
        if ($module) {
            $to_check = $module;
        } elseif ($class) {
            $to_check = $class;
        }

        $definition_checker = new ClassDefinitionChecker($to_check);
        $definition_checker->setErrorTypes($error_types ? explode('|', $error_types) : []);
        CBoolSpec::$_default_no = false;
        $errors = $definition_checker->checkErrors();
        CBoolSpec::$_default_no = true;

        return $this->renderSmarty(
            'database/inc_definition_checker.tpl',
            [
                'list_classes' => $definition_checker->getClassList(),
                'list_errors'  => $errors,
                'class'        => $class,
            ]
        );
    }

    /**
     * Compare the indexes in database and expected from the objects properties.
     *
     * @throws Exception
     */
    public function checkIndexes(RequestParams $params): Response
    {
        $error_type      = $params->get(
            'error_type',
            'enum list|' . implode('|', IndexChecker::ERROR_TYPES) . ' default|all'
        );
        $key_type        = $params->get(
            'key_type',
            'enum list|' . implode('|', IndexChecker::KEY_TYPES) . ' default|index'
        );
        $module          = $params->get('index_module', 'str');
        $show_all_fields = $params->get('show_all_fields', 'bool default|0');

        $this->enforceSlave();

        $index_checker = new IndexChecker($key_type, $error_type, $module, (bool)$show_all_fields);

        return $this->renderSmarty(
            'database/inc_vw_indexes.tpl',
            [
                'errors'                => $index_checker->check(),
                'count_missing_db'      => $index_checker->getCountMissingDb(),
                'count_not_expected_db' => $index_checker->getCountNotExpectedDb(),
            ]
        );
    }

    /**
     * Check the presence of tables from objects specs in database and tables that are not linked to an object.
     *
     * @throws Exception
     */
    public function checkTablesIntegrity(RequestParams $params): Response
    {
        $dsn      = $params->get('dsn', 'str');
        $type     = $params->get('type', 'enum list|all|table_missing|class_missing default|all');
        $mod_name = $params->get('mod_name', 'str');

        $integrity = new TablesIntegrityChecker($type);

        if ($dsn) {
            $integrity->setDsn($dsn);
        }

        if ($mod_name) {
            $integrity->setModule($mod_name);
        }

        return $this->renderSmarty(
            'database/inc_check_tables_integrity.tpl',
            ['integrity_result' => $integrity->checkTablesIntegrity()]
        );
    }

    /**
     * Check the row sizes of each tables.
     *
     * @throws Exception
     */
    public function checkRowSizes(): Response
    {
        $this->enforceSlave();

        $threshold = TablesIntegrityChecker::getRowSizeConfiguration();
        $data      = TablesIntegrityChecker::getTablesRowSize();

        $error_count = $warning_count = 0;

        $classmap = CClassMap::getInstance();

        foreach ($data as $key => $item) {
            if ($item['row_size'] >= $threshold) {
                $status = 'error';
                $error_count++;
            } elseif ($item['row_size'] >= $threshold * 0.75) {
                $status = 'warning';
                $warning_count++;
            } else {
                $status = 'ok';
            }
            $data[$key]['status'] = $status;
            $data[$key]['ratio']  = ($threshold != 0) ? (round($item['row_size'] / $threshold, 2) * 100) : 0;
            $data[$key]['class']  = $classmap->getClassShortNameFromTable($item['table_name']);
        }

        return $this->renderSmarty(
            'database/vw_check_tables_row_sizes.tpl',
            [
                'data'              => $data,
                'threshold'         => $threshold,
                'error_count'       => $error_count,
                'warning_count'     => $warning_count,
                'url_details_class' => $this->generateUrl('developpement_data_model_class_details'),
            ]
        );
    }

    private function getInstalledModules(): array
    {
        $installed_modules = CModule::getInstalled();
        asort($installed_modules);

        return $installed_modules;
    }
}
