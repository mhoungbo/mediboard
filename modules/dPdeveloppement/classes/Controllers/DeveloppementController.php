<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Ox\Core\Controller;
use Symfony\Component\HttpFoundation\Response;

class DeveloppementController extends Controller
{
    public function showDatabase(): Response
    {
        return $this->renderSmarty('vw_db_std.tpl', ['slave_exists' => $this->conf->get('db slave dbhost')]);
    }
}
