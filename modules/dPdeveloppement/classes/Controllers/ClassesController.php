<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Exception;
use Ox\Core\CClassMap;
use Ox\Core\Controller;
use Ox\Core\FileUtil\CFileReader;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Symfony\Component\HttpFoundation\Response;

class ClassesController extends Controller
{
    /**
     * @throws Exception
     */
    public function index(): Response
    {
        $class_map = CClassMap::getInstance();

        // Classmap file info
        $classmap_info = CFileReader::getFileInfo(
            [
                'file' => $class_map->getClassMapFile(),
                'date' => null,
                'size' => null,
                'line' => 0,
                'dirs' => $class_map->getDirs(),
            ]
        );

        // Classref file info
        $classref_info = CFileReader::getFileInfo(
            [
                'file' => $class_map->getClassRefFile(),
                'date' => null,
                'size' => null,
                'line' => 0,
                'keys' => count($class_map->getClassRef()),
            ]
        );

        $results       = [];
        $list_modules  = CModule::getInstalled();
        $count_classes = $class_map->countClassesForModules($class_map->getClassMap());
        $root_dir      = $this->getRootDir();
        $total_classes = 0;

        ksort($list_modules);
        foreach ($list_modules as $module) {
            $mod_name = lcfirst($module->mod_name);
            if (!isset($count_classes['modules'][$mod_name])) {
                continue;
            }

            $count_classes_module = $count_classes['modules'][$mod_name];
            $total_classes        += $count_classes_module;

            $results[$mod_name] = [
                'path'      => "{$root_dir}/modules/{$mod_name}",
                'namespace' => $class_map->getNamespaceFromModule($mod_name),
                'classes'   => $count_classes_module,
            ];
        }

        array_shift($results);

        return $this->renderSmarty(
            'classes/vw_class.tpl',
            [
                "modules"       => $results,
                "count_classes" => $count_classes,
                "total_classes" => $total_classes,
                "classmap_file" => $classmap_info,
                "classref_file" => $classref_info,
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function classmap(): Response
    {
        return $this->renderSmarty('classes/vw_class_map.tpl', ['start' => 0]);
    }

    /**
     * @throws Exception
     */
    public function listClassmap(RequestParams $params): Response
    {
        $start  = $params->get('start', 'num default|0');
        $search = $params->get('search', 'str');
        $search = $search ? str_replace('\\\\', '\\', $search) : null;

        $maps = CClassMap::getInstance()->getClassMap();
        ksort($maps);

        foreach ($maps as $_class => &$_map) {
            if (!is_null($search) && (stripos($_class, $search) === false)) {
                unset($maps[$_class]);
                continue;
            }

            $_map['name_encoded'] = base64_encode($_class);
        }

        return $this->renderSmarty(
            'classes/inc_vw_class_map_list.tpl',
            [
                'maps'  => array_slice($maps, $start, 50, true),
                'start' => $start,
                'total' => count($maps),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function show(string $class_name): Response
    {
        $class_name = base64_decode($class_name);
        $class_map  = CClassMap::getInstance();

        $msg = "Class does not exist.";
        $map = null;
        $ref = null;

        try {
            $map = $class_map->getClassMap($class_name);
            $ref = $class_map->getClassRef()[$class_name] ?? null;
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }

        $class_exists = "error";
        if (class_exists($class_name)) {
            $msg          = "Class exists.";
            $class_exists = "success";
        } elseif (interface_exists($class_name)) {
            $msg          = "Interface exists.";
            $class_exists = "success";
        } elseif (trait_exists($class_name)) {
            $msg          = "Trait exists.";
            $class_exists = "success";
        }

        return $this->renderSmarty('classes/vw_class_map_detail.tpl', [
            'msg'          => $msg,
            'class_exists' => $class_exists,
            'map'          => ($map) ? json_encode($map, JSON_PRETTY_PRINT) : null,
            'ref'          => ($ref !== null) ? json_encode(
                $ref,
                JSON_PRETTY_PRINT
            ) : 'Class isn\'t a CModelObject child',
        ]);
    }
}
