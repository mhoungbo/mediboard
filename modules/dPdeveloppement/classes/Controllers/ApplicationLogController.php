<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Ox\Core\CApp;
use Ox\Core\CMbPath;
use Ox\Core\Controller;
use Ox\Core\Elastic\ElasticObjectManager;
use Ox\Core\Elastic\Exceptions\ElasticClientException;
use Ox\Core\Elastic\Exceptions\ElasticException;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Developpement\Logger\ApplicationLogRepository;
use Ox\Mediboard\System\Elastic\ApplicationLog;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * List, download and remove application log (sql and elastic)
 */
class ApplicationLogController extends Controller
{
    private const MODE_ELASTIC = ApplicationLogRepository::MODE_ELASTIC;
    private const MODE_FILE    = ApplicationLogRepository::MODE_FILE;

    public function list(RequestParams $params, LoggerInterface $logger, ApplicationLogRepository $repository): Response
    {
        if ($params->get('download', 'bool default|0') === '1') {
            return $this->download($params);
        }

        if ($params->get('delete', 'bool default|0') === '1') {
            return $this->delete($params, $logger);
        }

        $time_start = microtime(true);
        try {
            $mode = $params->get('mode', 'str default|file');
            $params = [
                'log_start'      => $params->get("log_start", "str"),
                'grep_search'    => $params->get("grep_search", "str"),
                'grep_regex'     => $params->get("grep_regex", "bool default|0"),
                'grep_sensitive' => $params->get("grep_sensitive", "bool default|0"),
            ];

            $logs    = $repository->search($mode, $params);
            $nb_logs = count($logs);
        } catch (ElasticClientException $e) {
            $message = $e->getMessage();
            $logger->error($message);
            $this->addUiMsgError($message);

            return $this->renderEmptyResponse();
        } catch (ElasticException $e) {
            $logger->error(
                $this->translator->tr("ElasticIndexManager-error-Connection failed"),
                ["message" => $e->getMessage()]
            );
            $this->addUiMsgError("ElasticIndexManager-error-Connection failed");
        }

        $exec_time = microtime(true) - $time_start;
        $exec_time = round($exec_time, 3) * 1000;

        return $this->renderSmarty(
            "logs/inc_list_application_logs.tpl",
            [
                "logs"      => $logs,
                "nb_logs"   => $nb_logs,
                "exec_time" => $exec_time,
            ]
        );
    }

    public function download(RequestParams $params): Response
    {
        $file = CApp::getPathApplicationLog();

        if ($params->get("elasticsearch_or_file", "str default|" . static::MODE_FILE) === static::MODE_ELASTIC) {
            $file = str_replace(".log", "-elastic.log", $file);
            try {
                (new ApplicationLogRepository())->dumpIndexIntoFile($file);
            } catch (ElasticException $e) {
                $this->addUiMsgError("ElasticObjectManager-error-No logs in elastic");
                $this->renderEmptyResponse();
            }
        }

        return $this->file($file);
    }

    public function delete(RequestParams $params, LoggerInterface $logger): Response
    {
        if (!$this->checkModulePermEdit()) {
            throw new AccessDeniedException(
                $this->translator->tr('common-msg-You are not allowed to access this information (%s)')
            );
        }

        if ($params->get("elasticsearch_or_file", "str default|" . static::MODE_FILE) === static::MODE_ELASTIC) {
            $manager = ElasticObjectManager::getInstance();
            $obj     = new ApplicationLog();

            try {
                $manager->deleteIndex($obj);
                $manager::init($obj);
            } catch (ElasticClientException $e) {
            } catch (ElasticException $e) {
                $logger->error($this->translator->tr('ElasticObjectManager-error-Can not delete Elastic index'));
                $this->addUiMsgError("ElasticObjectManager-error-Can not delete Elastic index", true);
            }
        } else {
            CMbPath::remove(CApp::getPathApplicationLog());
        }

        return $this->renderEmptyResponse();
    }
}
