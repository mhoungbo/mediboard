<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Exception;
use Ox\Core\CApp;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CChambre;
use Ox\Mediboard\Hospi\CLit;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Sante400\CIdSante400;
use Symfony\Component\HttpFoundation\Response;

class MetricsController extends Controller
{
    /**
     * @throws Exception
     */
    public function list(RequestParams $params): Response
    {
        // Get current group
        if ($params->get('view_group', 'bool default|0')) {
            return $this->viewCurrentGroup();
        } else { // general view
            return $this->viewGeneralMetrics();
        }
    }

    /**
     * @throws Exception
     */
    private function viewCurrentGroup(): Response
    {
        $current_group        = CGroups::loadCurrent()->_id;
        $result_current_group = [];

        // - Nombre de s�jours
        $result_current_group["CSejour-_NDA"] = (new CIdSante400())->countList(
            [
                'tag'          => "= '" . CSejour::getTagNDA($current_group) . "'",
                'object_class' => "= 'CSejour'",
            ]
        );

        // - Patients IPP
        $result_current_group["CPatient-_IPP"] = (new CIdSante400())->countList(
            [
                'tag'          => "= '" . CPatient::getTagIPP($current_group) . "'",
                'object_class' => "= 'CPatient'",
            ]
        );

        // - Nombre de consultations
        $result_current_group["CConsultation"] = (new CConsultation())->countList(
              [
                  'functions_mediboard.group_id' => " = '$current_group'",
              ]
            , null,
              [
                  'plageconsult'        => "consultation.plageconsult_id = plageconsult.plageconsult_id",
                  'users_mediboard'     => "plageconsult.chir_id = users_mediboard.user_id",
                  'functions_mediboard' => "users_mediboard.function_id = functions_mediboard.function_id",
              ]
        );

        // - Lits
        $result_current_group["CLit"] = (new CLit())->countList(
              [
                  'service.group_id' => " = '$current_group'",
              ]
            , null,
              [
                  'chambre' => "lit.chambre_id = chambre.chambre_id",
                  'service' => "chambre.service_id = service.service_id",
              ]
        );

        // - Chambres
        $result_current_group["CChambre"] = (new CChambre())->countList(
              [
                  'service.group_id' => " = '$current_group'",
              ]
            , null,
              [
                  'service' => "chambre.service_id = service.service_id",
              ]
        );

        // - Utilisateurs
        $result_current_group["CMediusers"] = (new CMediusers())->countList(
              [
                  'functions_mediboard.group_id' => " = '$current_group'",
              ]
            , null,
              [
                  'functions_mediboard' => "users_mediboard.function_id = functions_mediboard.function_id",
              ]
        );

        return $this->renderSmarty(
            'metrics/inc_metrique_current_etab.tpl',
            [
                'res_current_etab' => $result_current_group,
            ]
        );
    }

    /**
     * @throws Exception
     */
    private function viewGeneralMetrics(): Response
    {
        $result              = [];
        $mapping_table_class = [];

        foreach (CApp::getInstalledClasses([], true) as $class) {
            $spec = (new $class())->getSpec();

            if ($spec->measureable) {
                $mapping_table_class[$spec->table] = $class;
            }
        }

        if (count($mapping_table_class)) {
            $sql = "SHOW TABLE STATUS WHERE Name " . CSQLDataSource::prepareIn(
                    array_keys($mapping_table_class)
                );

            foreach (CSQLDataSource::get("std")->loadList($sql) as $table_status) {
                $result[$mapping_table_class[$table_status["Name"]]] = $table_status;
            }
        }

        ksort($result);

        return $this->renderSmarty(
            'metrics/view_metrique.tpl',
            [
                'result'   => $result,
                'etab'     => $etab = CGroups::loadCurrent(),
                'nb_etabs' => $etab->countList(),
            ]
        );
    }
}
