<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Controllers;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Developpement\CRefCheckField;
use Ox\Mediboard\Developpement\CRefCheckTable;
use Symfony\Component\HttpFoundation\Response;

/**
 * References integrity check controller.
 * The integrity checker will verify for each object's reference the existence in the target table of the identified
 * line.
 * For instance if class A have a field `ref_b` which is a reference to a class B the tool will check for each line of
 * A is the value of `ref_b` exists in the table B.
 */
class IntegrityController extends Controller
{
    /**
     * Display the main page of the references integrity checker or the results of checks.
     *
     * @throws Exception
     */
    public function showIntegrity(RequestParams $params): Response
    {
        if ($params->get('results', 'bool default|0')) {
            return $this->renderShowReferences($params);
        }

        return $this->renderSmarty(
            'integrity/vw_references.tpl',
            [
                'chunks'               => CRefCheckTable::CHUNKS_SIZES,
                'url_show_references'  => $this->generateUrl('developpement_integrity_index', ['results' => 1]),
                'url_check_references' => $this->generateUrl('developpement_integrity_check'),
            ]
        );
    }

    /**
     * Check the integrity of references of CStoredObjects.
     * With the reset argument reset the integrity checks instead.
     *
     * @throws Exception
     */
    public function check(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid("integrity-checker", $params->getRequest()->request->get('token'))) {
            throw new InvalidCsrfTokenException();
        }

        if ($params->post('reset', 'bool default|0')) {
            return $this->resetIntegrityCheck();
        }

        $continue   = $params->post('continue', 'bool default|0');
        $chunk_size = $params->post(
            'chunk_size',
            'enum list|' . implode('|', CRefCheckTable::CHUNKS_SIZES) . ' default|10000'
        );
        $delay      = $params->post('delay', 'num default|0');
        $js         = $params->post('js', 'bool default|0');

        $ref = CRefCheckTable::getRefToCheck();

        if ($ref && $ref->_id) {
            $ref->checkRefs($chunk_size);

            if ($js) {
                $class_short = CClassMap::getSN($ref->class);
                CAppUI::js("ReferencesCheck.updateInfos('{$class_short}', '{$ref->_current_ref_check_field->field}')");
            }
        } else {
            CAppUI::setMsg("CRefCheckTable-msg-All over", UI_MSG_OK);
            if ($js) {
                CAppUI::js("ReferencesCheck.stopIntegrityCheck();");
            }
        }

        if ($continue && $js) {
            $delay = (int)$delay * 1000;
            CAppUI::js("setTimeout(ReferencesCheck.nextStep, $delay)");
        }

        return $this->renderEmptyResponse();
    }

    /**
     * Display the result of the integrity check for a class.
     *
     * @param string $class_name Class to display integrity results for.
     */
    public function showClassIntegrity(string $class_name): Response
    {
        $this->enforceSlave();

        $ref_check        = new CRefCheckTable();
        $ref_check->class = $class_name;
        $ref_check->loadMatchingObjectEsc();
        $ref_check->prepareRefFields();

        return $this->renderSmarty(
            'integrity/vw_class_integrity.tpl',
            [
                'ref_check_table'  => $ref_check,
                'class'            => $class_name,
                'url_field_errors' => $this->generateUrl(
                    'developpement_integrity_field_error',
                    ['ref_check_field_id' => 0]
                ),
            ]
        );
    }

    /**
     * Display the integrity errors for a field_check.
     * Using get parameter class_name will display the errors for the target_class = class_name.
     *
     * @throws Exception
     */
    public function showFieldErrors(CRefCheckField $field_check, RequestParams $params): Response
    {
        $this->enforceSlave();

        $field_check->loadFwdRef('ref_check_table_id', true);

        if ($class_name = $params->get('class_name', 'str')) {
            return $this->showFieldErrorsDetails($field_check, $class_name, $params);
        }

        $errors = $field_check->countErrorsByClass();

        CMbArray::pluckSort($errors, SORT_DESC, 'count_errors');

        return $this->renderSmarty(
            'integrity/inc_display_errors.tpl',
            [
                'table'            => $field_check->_fwd['ref_check_table_id'],
                'field'            => $field_check,
                'errors'           => $errors,
                'meta'             => (count($errors) > 1),
                'url_field_errors' => $this->generateUrl(
                    'developpement_integrity_field_error',
                    ['ref_check_field_id' => $field_check->_id,]
                ),
            ]
        );
    }

    /**
     * Display the checked classes and futures classes to check.
     * For each checked class display the information aggregated from the references check.
     *
     * @throws Exception
     */
    private function renderShowReferences(RequestParams $params): Response
    {
        $class = $params->get('class', 'str');
        $order = $params->get('order', 'enum list|name|duration|state|errors default|name');

        $this->enforceSlave();

        $ref_check_table = new CRefCheckTable();

        $all_class_check = $ref_check_table->loadList(null, 'class ASC');

        $parsed_lines = 0;
        $max_lines    = 0;
        $start        = null;
        $end          = null;
        $all_ended    = true;
        foreach ($all_class_check as $_ref_check) {
            $_ref_check->loadState();
            $parsed_lines += $_ref_check->_total_lines;
            $max_lines    += $_ref_check->_max_lines;
            $start        = (($start == null) || ($_ref_check->start_date && ($_ref_check->start_date < $start))) ? $_ref_check->start_date : $start;
            $end          = (($end == null) || ($_ref_check->end_date > $end)) ? $_ref_check->end_date : $end;
            $all_ended    = ($all_ended && !$_ref_check->end_date) ? false : $all_ended;
            $_ref_check->prepareRefFields();
        }

        $end = ($all_ended) ? CMbDT::relativeDuration($start, $end) : null;

        $progression = ($max_lines == 0) ? 0 : (($parsed_lines / $max_lines) * 100);

        switch ($order) {
            case 'state':
                CMbArray::pluckSort($all_class_check, SORT_DESC, '_state');
                break;
            case 'errors':
                CMbArray::pluckSort($all_class_check, SORT_DESC, '_error_count');
                break;
            default:
                // Do nothing
        }

        return $this->renderSmarty(
            'integrity/vw_integrity.tpl',
            [
                'class'                    => $class,
                'progression'              => $progression,
                'max_lines'                => $max_lines,
                'parsed_lines'             => $parsed_lines,
                'start'                    => $start,
                'end'                      => $end ? $end['locale'] : '',
                'classes'                  => $all_class_check,
                'order'                    => $order,
                'url_show_class_integrity' => $this->generateUrl(
                    'developpement_integrity_class_result',
                    ['class_name' => 'class_placeholder']
                ),
            ]
        );
    }

    /**
     * Reset the reference's integrity checker.
     *
     * @throws Exception
     */
    private function resetIntegrityCheck(): Response
    {
        $delete_errors = "TRUNCATE ref_errors";
        $delete_fields = "TRUNCATE ref_check_field";
        $delete_tables = "TRUNCATE ref_check_table";

        $ds = CSQLDataSource::get('std');

        $ds->exec($delete_errors);
        $ds->exec($delete_fields);
        $ds->exec($delete_tables);

        $this->addUiMsgOk('dPdeveloppement-ref_check-msg-Reset ok', true);

        return $this->renderEmptyResponse();
    }

    /**
     * Display the details of the errors about a field for a target class (in case of meta refs).
     *
     * @throws Exception
     */
    private function showFieldErrorsDetails(
        CRefCheckField $ref_field,
        string $class_name,
        RequestParams $params
    ): Response {
        $start = $params->get('start', 'num default|0');
        $step  = $params->get('step', 'num default|30');

        if (!$ref_field->target_class) {
            $ds = $ref_field->getDS();

            $where = [
                'target_class' => $ds->prepare('= ?', $class_name),
            ];

            /** @var CRefCheckField $back_field */
            $back_field = $ref_field->loadBackRefs('target_classes', null, 1, null, null, null, null, $where);
            $back_field = reset($back_field);
        } else {
            $back_field = $ref_field;
        }

        $errors = $back_field->loadBackRefs('errors', 'count_use DESC, missing_id ASC', "$start,$step");
        $total  = $back_field->countBackRefs('errors');

        return $this->renderSmarty(
            'integrity/inc_display_errors_details.tpl',
            [
                'start'           => $start,
                'step'            => $step,
                'total'           => $total,
                'errors'          => $errors,
                'class_name'      => $class_name,
                'url_page_change' => $this->generateUrl(
                    'developpement_integrity_field_error',
                    [
                        'ref_check_field_id' => $ref_field->_id,
                        'class_name'         => $class_name,
                    ]
                ),
            ]
        );
    }
}
