<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Unit\Database;

use Ox\Core\CMbArray;
use Ox\Core\FieldSpecs\CDateSpec;
use Ox\Core\FieldSpecs\CDateTimeSpec;
use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Core\FieldSpecs\CStrSpec;
use Ox\Core\FieldSpecs\CTimeSpec;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Developpement\Database\IndexChecker;
use Ox\Mediboard\Sample\Entities\CSampleMovie;
use Ox\Tests\OxUnitTestCase;

class IndexCheckerTest extends OxUnitTestCase
{
    public function testLockMissingOrUnexpectedIndexes(): void
    {
        $checker = new IndexChecker(IndexChecker::KEY_INDEX);
        $result = $checker->check();

        $expected_result = json_decode(
            file_get_contents(dirname(__DIR__, 2) . '/Resources/lock_missing_or_unexpected_indexes.json'),
            true
        );

        // New index error added
        if (false !== ($diff = CMbArray::diffRecursive($result, $expected_result))) {
            $this->fail(
                count($diff) . " new index errors have been added : \n"
                . json_encode($diff, JSON_PRETTY_PRINT)
            );
        }

        $this->expectNotToPerformAssertions();
    }

    public function testGetExistingIndexes(): void
    {
        $mock = $this->getMockBuilder(IndexChecker::class)
                     ->onlyMethods(['tableExists', 'getExistingIndexesFromDb'])
                     ->disableOriginalConstructor()
                     ->getMock();
        $mock->method('tableExists')->willReturnOnConsecutiveCalls(false, true);
        $mock->method('getExistingIndexesFromDb')->willReturn(
            [
                [
                    IndexChecker::SQL_KEY_NAME          => 'key1',
                    IndexChecker::SQL_SEQUENCE_IN_INDEX => 1,
                    IndexChecker::SQL_COLUMN_NAME       => 'column1',
                ],
                [
                    IndexChecker::SQL_KEY_NAME          => 'key1',
                    IndexChecker::SQL_SEQUENCE_IN_INDEX => 2,
                    IndexChecker::SQL_COLUMN_NAME       => 'column2',
                ],
                [
                    IndexChecker::SQL_KEY_NAME          => 'key2',
                    IndexChecker::SQL_SEQUENCE_IN_INDEX => 1,
                    IndexChecker::SQL_COLUMN_NAME       => 'column3',
                ],
            ]
        );

        $this->assertEquals([], $this->invokePrivateMethod($mock, 'getExistingIndexes', 'foo'));
        $this->assertEquals(
            [
                'key1' => [
                    1 => 'column1',
                    2 => 'column2',
                ],
                'key2' => [
                    1 => 'column3',
                ],
            ],
            $this->invokePrivateMethod($mock, 'getExistingIndexes', 'bar')
        );
    }

    public function testGetExpectedIndexes(): void
    {
        $mock = $this->getMockBuilder(IndexChecker::class)
                     ->disableOriginalConstructor()
                     ->onlyMethods(['getObjectSpecs'])
                     ->getMock();
        $mock->method('getObjectSpecs')->willReturn($this->buildSpecsForGetExpectedIndexes());

        $this->assertEquals(
            [
                [1 => 'meta_2', 2 => 'meta_1'],
                'index_1',
                'index_ok',
                [1 => 'multi_1', 2 => 'multi_2', 3 => 'multi_3'],
                [1 => 'other_1', 2 => 'other_2'],
            ],
            $this->invokePrivateMethod($mock, 'getExpectedIndexes', new CUser())
        );
    }

    /**
     * @dataProvider handleSingleFieldIndexProvider
     */
    public function testHandleSingleFieldIndex(string $search, array $arr, bool $expected): void
    {
        $checker = new IndexChecker(IndexChecker::KEY_INDEX);
        $this->assertEquals($expected, $this->invokePrivateMethod($checker, 'handleSingleFieldIndex', $search, $arr));
    }

    public function handleSingleFieldIndexProvider(): array
    {
        return [
            'empty_empty_arr' => ['foo', [], false],
            'found'           => ['foo', [[1 => 'foo']], true],
            'not_found'       => ['foo', [[0 => 'foo', 1 => null]], false],
        ];
    }

    /**
     * @dataProvider handleMultiFieldIndexProvider
     */
    public function testHandleMultiFieldIndex(array $arr1, array $arr2, bool $expected): void
    {
        $checker = new IndexChecker(IndexChecker::KEY_INDEX);
        $this->assertEquals($expected, $this->invokePrivateMethod($checker, 'handleMultiFieldIndex', $arr1, $arr2));
    }

    public function handleMultiFieldIndexProvider(): array
    {
        return [
            'both_empty'  => [[], [], false],
            'ok'          => [[1 => 'foo', 2 => 'bar'], [[1 => 'foo', 2 => 'bar', 3 => 'test']], true],
            'no_in_array' => [[1 => 'foo', 2 => 'bar'], [[1 => 'bar', 2 => 'foo']], false],
        ];
    }

    public function testCheckExpectedIndexes(): void
    {
        $mock = $this->getMockBuilder(IndexChecker::class)
                     ->onlyMethods(['handleMultiFieldIndex', 'handleSingleFieldIndex'])
                     ->setConstructorArgs([IndexChecker::KEY_INDEX, IndexChecker::ERROR_ALL, 'system', true])
                     ->getMock();
        $mock->method('handleSingleFieldIndex')->willReturnOnConsecutiveCalls(true, false);
        $mock->method('handleMultiFieldIndex')->willReturnOnConsecutiveCalls(true, false);

        $this->invokePrivateMethod(
            $mock,
            'checkExpectedIndexes',
            'CUser',
            ['single1', 'single2', ['multi1', 'multi2'], ['other_multi1', 'other_multi2']],
            []
        );

        $this->assertEquals(2, $mock->getCountMissingDb());
        $this->assertEquals(
            [
                'CUser' => [
                    'single1'                   => IndexChecker::OK,
                    'single2'                   => IndexChecker::ERROR_MISSING_DB,
                    'multi1|multi2'             => IndexChecker::OK,
                    'other_multi1|other_multi2' => IndexChecker::ERROR_MISSING_DB,
                ],
            ],
            $this->getPrivateProperty($mock, 'errors')
        );
    }

    public function testCheck(): void
    {
        $checker = $this->buildMockForCheck();
        $this->assertEquals(
            [
                CSampleMovie::class => [
                    'not_field' => IndexChecker::ERROR_MISSING_DB,
                    'column1'   => IndexChecker::ERROR_UNEXPECTED_DB,
                ],
            ],
            $checker->check()
        );
    }

    private function buildSpecsForGetExpectedIndexes(): array
    {
        return [
            'user_id'        => new CRefSpec('CUser', 'user_id'),
            '_invalid_field' => new CDateTimeSpec('CUser', '_invalid_field'),
            'no_index_field' => new CStrSpec('CUser', 'no_index_field'),
            'index_0'        => new CDateSpec('CUser', 'index_0', 'date index|0'),
            'meta_1'         => new CRefSpec('CUser', 'meta_1', 'ref class|CPatient meta|meta_2'),
            'meta_2'         => new CStrSpec('CUser', 'meta_2'),
            'index_1'        => new CStrSpec('CUser', 'index_1', 'str index|1'),
            'index_ok'       => new CTimeSpec('CUser', 'index_ok'),
            'multi_1'        => new CStrSpec('CUser', 'multi_1', 'str index|multi_2|multi_3'),
            'multi_2'        => new CStrSpec('CUser', 'multi_2', 'str'),
            'multi_3'        => new CStrSpec('CUser', 'multi_3', 'str'),
            'other_1'        => new CStrSpec('CUser', 'other_1', 'str index|other_2'),
            'other_2'        => new CStrSpec('CUser', 'other_2', 'str'),
        ];
    }

    private function buildMockForCheck(): IndexChecker
    {
        $checker = $this->getMockBuilder(IndexChecker::class)
                        ->onlyMethods(['getInstance', 'getExistingIndexes', 'getExpectedIndexes'])
                        ->setConstructorArgs([IndexChecker::KEY_INDEX, IndexChecker::ERROR_ALL, 'sample'])
                        ->getMock();

        $checker->method('getExistingIndexes')->willReturn(
            [
                'key1' => [1 => 'column1'],
                'key2' => [1 => 'column2', 2 => 'column1'],
                'key3' => [1 => 'column3'],
            ]
        );
        $checker->method('getExpectedIndexes')->willReturn(
            [
                [1 => 'column2', 2 => 'column1',],
                'not_field',
                'column3',
            ]
        );
        $checker->method('getInstance')->willReturnCallback(function (string $class_name) {
            if ($class_name === CSampleMovie::class) {
                return new CSampleMovie();
            }

            return null;
        });

        return $checker;
    }
}
