<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Unit\Database;

use Ox\Core\CMbArray;
use Ox\Core\CMbFieldSpec;
use Ox\Core\CStoredObject;
use Ox\Core\FieldSpecs\CNumSpec;
use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Core\FieldSpecs\CStrSpec;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Developpement\Database\ClassDefinitionChecker;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Sample\Entities\CSampleMovie;
use Ox\Mediboard\System\CTableStatus;
use Ox\Tests\OxUnitTestCase;

class ClassDefinitionCheckerTest extends OxUnitTestCase
{
    public function testLockSpecAndDatabaseDiff(): void
    {
        $checker = new ClassDefinitionChecker();
        $result = $checker->compileErrors();

        $expected_result = json_decode(
            file_get_contents(dirname(__DIR__, 2) . '/Resources/lock_object_spec_and_db_diff.json'),
            true
        );

        $result = CMbArray::mapRecursive(
            function ($data) {
                return is_string($data) ? mb_convert_encoding($data, 'UTF-8', 'ISO-8859-1') : $data;
            },
            $result
        );

        // New index error added
        if (false !== ($diff = CMbArray::diffRecursive($result, $expected_result))) {
            $this->fail(
                count($diff) . " new errors have been added : \n"
                . json_encode($diff, JSON_PRETTY_PRINT)
            );
        }

        $this->expectNotToPerformAssertions();
    }

    /**
     * @dataProvider shouldFieldHaveIndexProvider
     */
    public function testShouldFieldHaveIndex(
        CStoredObject $object,
        string        $field_name,
        bool          $is_key,
        bool          $expected
    ): void {
        $definition_checker = new ClassDefinitionChecker(CUser::class);
        $this->assertEquals(
            $expected,
            $this->invokePrivateMethod($definition_checker, 'shouldFieldHaveIndex', $object, $field_name, $is_key)
        );
    }

    public function shouldFieldHaveIndexProvider(): array
    {
        return [
            'index_0'       => [new CTableStatus(), 'create_time', false, false],
            'ref_spec'      => [new CSampleMovie(), 'category_id', false, true],
            'dateTime_spec' => [new CUser(), 'user_last_login', false, true],
            'date_spec'     => [new CSampleMovie(), 'release', false, true],
            'time_spec'     => [new CSampleMovie(), 'duration', false, true],
            'is_key'        => [new CSampleMovie(), 'name', true, true],
            'str_spec'      => [new CSampleMovie(), 'name', false, true],
        ];
    }

    /**
     * @dataProvider getPrefixFromFieldProvider
     */
    public function testGetPrefixFromField(
        CStoredObject $object,
        CMbFieldSpec  $spec,
        string        $field_name,
        ?string       $expected
    ): void {
        $definition_checker = new ClassDefinitionChecker(CUser::class);
        $this->assertEquals(
            $expected,
            $this->invokePrivateMethod($definition_checker, 'getPrefixFromField', $object, $spec, $field_name)
        );
    }

    public function getPrefixFromFieldProvider(): array
    {
        $file = new CFile();

        return [
            'basic_meta'   => [$file, $file->getSpecs()['object_id'], 'object_id', 'object'],
            'reverse_meta' => [$file, $file->getSpecs()['object_class'], 'object_class', 'object'],
            'no_meta'      => [$file, $file->getSpecs()['file_name'], 'file_name', null],
        ];
    }

    public function testExtractClassFields(): void
    {
        $checker = $this->getDefinitionCheckerMock();
        $object  = $this->getObject();

        $details = ['key' => 'foo'];

        $details = $this->invokePrivateMethod($checker, 'extractClassFields', $object, $details);

        $this->assertEquals(
            [
                'fields' => [
                    'field_not_in_spec'  => [
                        'object' => [
                            'spec'    => null,
                            'db_spec' => null,
                        ],
                        'db'     => null,
                    ],
                    'field_ref'          => [
                        'object' => [
                            'spec'    => null,
                            'db_spec' => [
                                'index'    => 'class_field, field_ref',
                                'null'     => true,
                                'default'  => null,
                                'extra'    => '',
                                'type'     => 'INT',
                                'params'   => [11],
                                'unsigned' => true,
                                'zerofill' => false,
                            ],
                        ],
                        'db'     => null,
                    ],
                    'class_field'        => [
                        'object' => [
                            'spec'    => null,
                            'db_spec' => [
                                'index'    => false,
                                'null'     => true,
                                'default'  => null,
                                'extra'    => '',
                                'type'     => 'VARCHAR',
                                'params'   => [255],
                                'unsigned' => false,
                                'zerofill' => false,
                            ],
                        ],
                        'db'     => null,
                    ],
                    'field_with_default' => [
                        'object' => [
                            'spec'    => null,
                            'db_spec' => [
                                'index'    => false,
                                'null'     => true,
                                'default'  => '10',
                                'extra'    => '',
                                'type'     => 'INT',
                                'params'   => [11],
                                'unsigned' => false,
                                'zerofill' => false,
                            ],
                        ],
                        'db'     => null,
                    ],
                ],
                'key'    => 'foo',
            ],
            $details
        );
    }

    private function getDefinitionCheckerMock(): ClassDefinitionChecker
    {
        $mock = $this->getMockBuilder(ClassDefinitionChecker::class)
                     ->onlyMethods(['getObjectFields'])
                     ->setConstructorArgs([CUser::class])
                     ->getMock();
        $mock->method('getObjectFields')->willReturn(
            [
                'field_not_in_spec'  => new CStrSpec('stdClass', 'field_not_in_spec'),
                'field_ref'          => new CRefSpec('stdClass', 'field_ref', 'ref class|CUser meta|class_field'),
                'class_field'        => new CStrSpec('stdClass', 'class_field', 'str notNull'),
                'field_with_default' => new CNumSpec('stdClass', 'field_with_default', 'num default|10'),
            ]
        );

        return $mock;
    }

    private function getObject(): CUser
    {
        $object                               = new CUser();
        $object->_specs['field_ref']          = new CRefSpec(
            'stdClass', 'field_ref', 'ref class|CUser meta|class_field'
        );
        $object->_specs['class_field']        = new CStrSpec('stdClass', 'class_field');
        $object->_specs['field_with_default'] = new CNumSpec('stdClass', 'field_with_default', 'num default|10');

        return $object;
    }
}
