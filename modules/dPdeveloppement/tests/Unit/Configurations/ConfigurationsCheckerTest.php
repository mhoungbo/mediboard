<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Developpement\Tests\Unit\Configurations;

use Exception;
use Ox\Mediboard\Developpement\Configs\ConfigurationsChecker;
use Ox\Tests\OxUnitTestCase;

class ConfigurationsCheckerTest extends OxUnitTestCase
{
    /**
     * @throws Exception
     * @config locale_warn 0
     */
    public function testCompareAndReturnConfigs(): void
    {
        foreach ((new ConfigurationsChecker())->compareAndReturnConfigs() as $config) {
            $this->assertArrayHasKey('config', $config);
            $this->assertArrayHasKey('isOK', $config);

            if ($config['config'] === 'locale_warn') {
                $this->assertFalse($config['isOK']);
            }
        }
    }

    /**
     * @throws Exception
     * @pref INFOSYSTEM 0
     */
    public function testCompareAndReturnPrefs(): void
    {
        foreach ((new ConfigurationsChecker())->compareAndReturnPrefs() as $pref) {
            $this->assertArrayHasKey('pref', $pref);
            $this->assertArrayHasKey('isOK', $pref);

            if ($pref['pref'] === 'INFOSYSTEM') {
                $this->assertFalse($pref['isOK']);
            }
        }
    }
}
