<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Unit\Index;

use Ox\Mediboard\Developpement\Index\RouteMetadata;
use Ox\Tests\OxUnitTestCase;

class RouteMetadataTest extends OxUnitTestCase
{
    protected RouteMetadata $route_metadata;

    protected string $id         = "indexroute01";
    protected string $module     = "dPdeveloppement";
    protected string $controller = "Ox\Mediboard\Developpement\Controllers\RoutesController";
    protected string $route_name = "admin_identicate";
    protected string $action     = "identicate";
    protected string $path       = "/api/identicate";
    protected string $methods    = "GET";
    protected string $permission = "read";

    protected function setUp(): void
    {
        parent::setUp();
        $this->route_metadata = new RouteMetadata(
            $this->id,
            $this->module,
            $this->controller,
            $this->route_name,
            $this->action,
            $this->path,
            $this->methods,
            false,
            $this->permission
        );
    }

    public function testToArray(): void
    {
        $expected = [
            "module"     => $this->route_metadata->getModule(),
            "controller" => $this->route_metadata->getController(),
            "route_name" => $this->route_metadata->getRouteName(),
            "action"     => $this->route_metadata->getAction(),
            "path"       => $this->route_metadata->getPath(),
            "methods"    => $this->route_metadata->getMethods(),
            "id"         => $this->route_metadata->getId(),
            'offline'    => $this->route_metadata->getOffline(),
            'permission' => $this->route_metadata->getPermission(),
        ];

        $this->assertEquals(
            $expected,
            $this->route_metadata->toArray()
        );
    }

    public function testToString(): void
    {
        $this->assertEquals(
            "{$this->module} | {$this->controller} | {$this->route_name} | {$this->action} | {$this->path} | {$this->methods} | 0 | {$this->permission}",
            (string)$this->route_metadata
        );
    }

    public function testFromString(): void
    {
        $string = "{$this->module} | {$this->controller} | {$this->route_name}"
            . " | {$this->action} | {$this->path} | {$this->methods} | 0 | {$this->permission}";

        $this->assertEquals(
            $this->route_metadata,
            $this->route_metadata->fromString($this->id, $string)
        );
    }

    public function testGetIndexableData(): void
    {
        $expected = [
            "title" => $this->route_name,
            "_id"   => $this->route_metadata->getId(),
            "body"  => (string)$this->route_metadata,
        ];

        $this->assertEquals(
            $expected,
            $this->route_metadata->getIndexableData()
        );
    }
}
