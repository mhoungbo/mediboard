<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Index;

use Exception;
use Ox\Mediboard\Developpement\Index\RouteIndexer;
use Ox\Mediboard\Developpement\Index\RouteMetadata;
use Ox\Tests\OxUnitTestCase;

class RouteIndexerTest extends OxUnitTestCase
{
    protected RouteIndexer  $route_indexer;
    protected RouteMetadata $route_metadata_1;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->route_indexer    = new RouteIndexer();
        $this->route_metadata_1 = new RouteMetadata(
            "indexroute01",
            "system",
            "Ox\Mediboard\System\Controllers\SystemController",
            "system_gui_about",
            "about",
            "/gui/system/about",
            "GET",
            false,
            'none'
        );
    }

    public function testFindByNameSuccess(): void
    {
        $route = $this->route_indexer->findByName($this->route_metadata_1->getRouteName());

        $this->assertObjectEquals($this->route_metadata_1, $route);
    }

    public function testFindByNameWithoutMatch(): void
    {
        $route = $this->route_indexer->findByName('lorem_ipsum_dolor');
        $this->assertNull($route);
    }
}
