<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Exception;
use Ox\Core\Locales\Translator;
use Ox\Tests\OxWebTestCase;

class MetricsControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testViewGeneralMetrics(): void
    {
        $crawler = self::createClient()->request('get', '/gui/developpement/metrics');

        $node = $crawler->filterXPath('//table[@class="tbl main"][1]');
        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(
            21,
            $node->filterXPath('//tr')
        );
        $this->assertEquals(
            (new Translator())->tr('CAdministration'),
            mb_convert_encoding(
                $node->filterXPath('//tr[2]//td[1]')->innerText(),
                'ISO-8859-1',
                'UTF-8'
            )
        );
    }

    /**
     * @throws Exception
     */
    public function testViewGroupMetrics(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/developpement/metrics', [
            'view_group' => 1,
        ]);

        $node = $crawler->filterXPath('//table[@class="tbl main"][1]');
        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(
            7,
            $node->filterXPath('//tr')
        );
        $this->assertEquals(
            (new Translator())->tr('CSejour-_NDA'),
            mb_convert_encoding(
                $node->filterXPath('//tr[2]//td[1]')->innerText(),
                'ISO-8859-1',
                'UTF-8'
            )
        );
    }
}
