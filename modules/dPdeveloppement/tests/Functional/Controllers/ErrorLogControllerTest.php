<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Ox\Core\CApp;
use Ox\Mediboard\System\CErrorLog;
use Ox\Tests\OxWebTestCase;

class ErrorLogControllerTest extends OxWebTestCase
{
    public function testIndex(): void
    {
        $client  = self::createClient();
        $crawler = $client->request(
            'get',
            "/gui/developpement/logs"
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString(
            CApp::getPathApplicationLog(),
            $crawler->filterXPath('//*[@id="application-tab"]')->text()
        );
    }

    public function testList(): void
    {
        $client  = self::createClient();
        $crawler = $client->request(
            'get',
            "/gui/developpement/logs/errors",
            [
                'source_type' => 'sql',
                'start'       => '0',
                'order_by'    => 'date',
                'ajax'        => '1',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $error  = new CErrorLog();
        $errors = $error->loadList(null, ['error_log_id desc'], '');
        if (count($errors) > 0) {
            $error = end($errors);
            $this->assertEquals(1, $crawler->filterXPath('//*[@id="details-error-log-' . $error->_id . '"]')->count());
        }
    }

    public function testDelete(): void
    {
        $params = [
            'is_elastic_log' => '0',
            'ajax'           => '1',
        ];

        $error  = new CErrorLog();
        $errors = $error->loadList(null, ['error_log_id desc'], '');
        if (count($errors) > 0) {
            $error             = end($errors);
            $params['log_ids'] = $error->_id;
        } else {
            $params['purge'] = '1';
        }

        $client  = self::createClient();
        $crawler = $client->request('post', '/gui/developpement/logs/errors', $params);
        $this->assertResponseStatusCodeSame(200);

        $this->stringContains("Suppression des journaux d'erreur", $crawler->text());
    }
}
