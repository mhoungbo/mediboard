<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Ox\Core\Locales\Translator;
use Ox\Tests\OxWebTestCase;

class RoutesControllerTest extends OxWebTestCase
{
    public function testAutocompleteWithoutMatch(): void
    {
        $crawler = self::createClient()->request(
            'get',
            '/gui/developpement/routes/autocomplete',
            ['ajax' => 1, 'search' => 'loremipsum']
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertEquals(
            (new Translator())->tr('RoutesController-Empty route list'),
            mb_convert_encoding($crawler->filterXPath('//li[@class="empty"]')->innerText(), 'ISO-8859-1', 'UTF-8')
        );
    }

    public function testAutocompleteOk(): void
    {
        $crawler = self::createClient()->request(
            'get',
            '/gui/developpement/routes/autocomplete',
            ['ajax' => 1, 'search' => 'about']
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertEquals(1, $crawler->filterXPath('//li[@data-route_name="system_gui_about"]')->count());
    }
}
