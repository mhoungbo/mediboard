<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Ox\Core\CMbString;
use Ox\Mediboard\System\CErrorLog;
use Ox\Mediboard\System\CErrorLogWhiteList;
use Ox\Tests\OxWebTestCase;

class ErrorLogWhitelistControllerTest extends OxWebTestCase
{

    public function testWhitelist(): void
    {
        $client  = self::createClient();
        $crawler = $client->request(
            'get',
            "/gui/developpement/logs/errors/whitelist",
            ['ajax' => '1']
        );
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(1, $crawler->filterXPath('//*[@id="error-whitelist"]')->count());
    }

    public function testWhitelistDelete(): void
    {
        $client  = self::createClient();
        $crawler = $client->request(
            'post',
            "/gui/developpement/logs/errors/whitelist",
            [
                'ajax' => '1',
                'all'  => '1',
            ]
        );
        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString("Liste blanche vid�e", CMbString::utf8Decode($crawler->text()));
    }

    public function testWhitelistToggle(): void
    {
        $error  = new CErrorLog();
        $errors = $error->loadList(null, ['error_log_id desc'], '');
        if (count($errors) > 0) {
            $error                     = end($errors);
            $error_log_whitelist       = new CErrorLogWhiteList();
            $error_log_whitelist->hash = $error->signature_hash;
            $error_log_whitelist->loadMatchingObject();
        }

        $client = self::createClient();
        $client->request(
            'post',
            "/gui/developpement/logs/errors/whitelist/toggle",
            [
                'error_log_id'   => $error->_id,
                'ajax'           => '1',
                'is_elastic_log' => '0',
            ]
        );
        $content = $client->getResponse()->getContent();
        if ($error->_id) {
            $this->assertResponseStatusCodeSame(200);
            if ($error_log_whitelist->_id) {
                $this->assertStringContainsString("�l�ment supprim�", $content);
            } else {
                $this->assertStringContainsString("Ajout� � la liste blanche", $content);
            }
        } else {
            $this->assertResponseStatusCodeSame(500);
        }
    }
}
