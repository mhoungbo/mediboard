<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Ox\Core\Locales\Translator;
use Ox\Core\Module\CModule;
use Ox\Mediboard\System\CTranslationOverwrite;
use Ox\Tests\OxWebTestCase;

class TranslationsControllerTest extends OxWebTestCase
{
    public function testListTranslations(): void
    {
        $crawler = self::createClient()->request(
            'get',
            '/gui/developpement/translations/list',
            ['reference' => 'fr']
        );

        $first_class = CModule::getClassesFor('system')[0];
        $translator  = new Translator();

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringStartsWith(
            $translator->tr($first_class),
            $crawler->filterXPath('//ul[@id="tab-classes"]//li[1]')->text()
        );

        $this->assertEquals(
            $translator->tr('Language') . ': ' . $translator->tr('language.fr'),
            mb_convert_encoding(
                $crawler->filterXPath(
                    '//form[@name="translate-' . $first_class . '"]//th[@class="title"][2]'
                )->innerText(),
                'ISO-8859-1',
                'UTF-8'
            )
        );
    }

    public function testListIntegrateTranslations(): void
    {
        $trad                     = 'CTranslationOverwrite-integrate translations';
        $translation              = new CTranslationOverwrite();
        $translation->source      = $trad;
        $translation->translation = $trad;
        $translation->language    = 'fr';

        if (!$translation->loadMatchingObject()) {
            $this->storeOrFailed($translation);
        }

        $crawler = self::createClient()->request(
            'get',
            '/gui/developpement/translations/list',
            ['integrate' => true]
        );

        $this->assertEquals($trad, $crawler->filterXPath('//table[@class="main tbl"]//tr[5]//td[2]')->text());
        $translation->delete();
    }

    public function testSaveIntegrateTranslations(): void
    {
        $this->markTestSkipped('R�activer une fois que le composant qui �crit les traductions dans les fichiers peut �tre mock� pour ne pas �crire dans le cadre des tests');
        $trad                     = 'CTranslationOverwrite-integrate translations';
        $translation              = new CTranslationOverwrite();
        $translation->source      = $trad;
        $translation->translation = $trad;
        $translation->language    = 'fr';

        if (!$translation->loadMatchingObject()) {
            $this->storeOrFailed($translation);
        }

        $crawler = self::createClient()->request(
            'post',
            '/gui/developpement/translations',
            ['integrate' => true, 'translations_ids' => $translation->_id]
        );

        $this->assertEquals(
            'Fichier de traduction sauvegard�',
            mb_convert_encoding(
                $crawler->filterXPath(
                    '//div[@class="info"]'
                )->innerText(),
                'ISO-8859-1',
                'UTF-8'
            )
        );
    }
}
