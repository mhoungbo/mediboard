<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Controllers;

use Ox\Core\Locales\Translator;
use Ox\Tests\OxWebTestCase;

class DataModelControllerTest extends OxWebTestCase
{
    public function testShow(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/dataModel', ['ajax' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $form = $crawler->filterXPath('//form[@name="filter_class"]');
        $this->assertCount(1, $form);
        $this->assertStringEndsWith(
            '/gui/developpement/dataModel',
            $form->filterXPath('//input[@name="@route"]')->attr('value')
        );
        $this->assertEquals('CPatient', $form->filterXPath('//input[@name="object_class"]')->attr('value'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="graph_draw"]'));
    }

    public function testShowWithObjectClass(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/dataModel', ['ajax' => 1, 'object_class' => 'CUser']);

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $script_content = $crawler->filterXPath('//script[position()=2]')->text();
        $this->assertStringContainsString('dataModel.prepareGraph', $script_content);
        $this->assertStringContainsString('/gui/developpement/dataModel/details', $script_content);
    }

    public function testShowWithNonExistantObjectClass(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/dataModel', ['ajax' => 1, 'object_class' => 'foo']);

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString('La classe foo n\'existe pas.', $client->getResponse()->getContent());
    }

    public function testShowClassDetails(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/gui/developpement/dataModel/details',
            ['ajax' => 1, 'class' => 'CUser', 'complete' => 1]
        );

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $form = $crawler->filterXPath('//form[@name="form_details_class"]');
        $this->assertCount(1, $form);
        $this->assertStringEndsWith(
            '/gui/developpement/dataModel/details',
            $form->filterXPath('//input[@name="@route"]')->attr('value')
        );
        $this->assertEquals('CUser', $form->filterXPath('//input[@name="class"]')->attr('value'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="details_class_div"]'));
    }

    public function testShowClassDetailsWithNonExistantClass(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/gui/developpement/dataModel/details',
            ['ajax' => 1, 'class' => 'foo']
        );

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString('Classe inexistante foo', $client->getResponse()->getContent());
    }

    public function testShowClassDetailsComplete(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/dataModel/details', ['ajax' => 1, 'class' => 'CUser']);

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $table = $crawler->filterXPath('//table[@name="details_class"]');
        $this->assertCount(1, $table);

        $translator = new Translator();
        $this->assertEquals(
            'CUser (' . $translator->tr('CUser') . ')',
            $table->filterXPath('//th[position()=1]')->text()
        );
    }
}
