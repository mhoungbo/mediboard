<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Ox\Core\CMbString;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\Developpement\CRefCheckField;
use Ox\Mediboard\Developpement\CRefCheckTable;
use Ox\Mediboard\Developpement\CRefError;
use Ox\Tests\OxWebTestCase;

class IntegrityControllerTest extends OxWebTestCase
{
    private CRefCheckTable $ref;
    private CRefCheckField $field;

    public function testShowIntegrity(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/referencesIntegrity', ['ajax' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $form = $crawler->filterXPath('//form[@name="exec-integrity-checker"]');
        $this->assertCount(1, $form);
        $this->assertStringEndsWith(
            '/gui/developpement/referencesIntegrity/check',
            $form->filterXPath('//input[@name="@route"]')->attr('value')
        );
        $this->assertCount(1, $form->filterXPath('//input[@name="class"]'));
        $this->assertCount(1, $form->filterXPath('//input[@name="field"]'));
        $this->assertCount(1, $form->filterXPath('//input[@name="delay"]'));
        $this->assertCount(1, $form->filterXPath('//button[@id="integrity-start"]'));

        $select = $form->filterXPath('//select[@name="chunk_size"]');
        $this->assertCount(1, $select);
        $this->assertCount(count(CRefCheckTable::CHUNKS_SIZES), $select->filterXPath('//option'));

        $this->assertCount(1, $crawler->filterXPath('//div[@id="result-check-integrity"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="ref-check-tables"]'));
    }

    public function testCheckWithReset(): void
    {
        $client = self::createClient();
        $client->request('POST', '/gui/developpement/referencesIntegrity/check', ['reset' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            '<div class=\'info\'>'
            . CMbString::htmlEntities($this->translator->tr('dPdeveloppement-ref_check-msg-Reset ok'))
            . '<\/div>',
            $client->getResponse()->getContent()
        );
    }

    /**
     * @depends testCheckWithReset
     */
    public function testShowIntegrityWithResult(): void
    {
        $this->createRefCheck();

        try {
            $client = self::createClient();
            $client->request('GET', '/gui/developpement/referencesIntegrity', ['ajax' => 1, 'results' => 1]);

            $this->assertResponseStatusCodeSame(200);

            $crawler = $client->getCrawler();

            // Check filters for order
            $form_order = $crawler->filterXPath('//form[@name="order-way-integrity"]');
            $this->assertCount(1, $form_order);
            $this->assertCount(3, $form_order->filterXPath('//input[@type="radio" and @name="order"]'));

            // Main display
            $main_div = $crawler->filterXPath('//div[@id="list-classes-integrity"]');
            $this->assertCount(1, $main_div);
            $this->assertStringStartsWith(
                'ReferencesCheck.displayClass',
                $main_div->filterXPath('//tr[@id="line-CUser"]')->attr('onclick')
            );
        } finally {
            $this->removeRefCheck();
        }
    }

    public function testCheck(): void
    {
        $this->createRefCheck();

        try {
            $client = self::createClient();
            $client->request('POST', '/gui/developpement/referencesIntegrity/check', ['js' => 1, 'continue' => 1]);

            $this->assertResponseStatusCodeSame(200);

            $content = $client->getResponse()->getContent();

            $this->assertStringContainsString(
                '<div class="info">' . $this->translator->tr('CRefCheckTable-msg-over', 'CUser') . '</div>',
                $content
            );
            $this->assertStringContainsString('ReferencesCheck.updateInfos(\'CUser\',', $content);
            $this->assertStringContainsString('setTimeout(ReferencesCheck.nextStep, 0)', $content);
        } finally {
            $this->removeRefCheck();
        }
    }

    public function testCheckWithtoutJs(): void
    {
        $this->createRefCheck();

        try {
            $client = self::createClient();
            $client->request('POST', '/gui/developpement/referencesIntegrity/check');

            $this->assertResponseStatusCodeSame(200);
            $this->assertEquals(
                '<div class="info">' . $this->translator->tr('CRefCheckTable-msg-over', 'CUser') . '</div>',
                $client->getResponse()->getContent()
            );
        } finally {
            $this->removeRefCheck();
        }
    }

    public function testShowClassIntegrity(): void
    {
        $this->createRefCheck();

        try {
            $client = self::createClient();
            $client->request('GET', '/gui/developpement/referencesIntegrity/CUser', ['ajax' => 1]);

            $this->assertResponseStatusCodeSame(200);

            $crawler = $client->getCrawler();
            $table   = $crawler->filterXPath('//table[@class="main tbl"]');
            $this->assertCount(1, $table);

            // Check headers
            $this->assertEquals(
                $this->translator->tr('CRefCheckTable-class-court'),
                mb_convert_encoding($table->filterXPath('//th[position()=1]')->text(), 'ISO-8859-1', 'UTF-8')
            );
            $this->assertEquals(
                $this->translator->tr('CRefCheckField-field-court'),
                mb_convert_encoding($table->filterXPath('//th[position()=2]')->text(), 'ISO-8859-1', 'UTF-8')
            );
            $this->assertEquals(
                $this->translator->tr('CRefCheckField-target_class-court'),
                mb_convert_encoding($table->filterXPath('//th[position()=3]')->text(), 'ISO-8859-1', 'UTF-8')
            );
            $this->assertEquals(
                $this->translator->tr('CRefCheckField-start_date-court'),
                mb_convert_encoding($table->filterXPath('//th[position()=4]')->text(), 'ISO-8859-1', 'UTF-8')
            );
            $this->assertEquals(
                $this->translator->tr('CRefCheckField-_duration'),
                mb_convert_encoding($table->filterXPath('//th[position()=5]')->text(), 'ISO-8859-1', 'UTF-8')
            );
            $this->assertEquals(
                $this->translator->tr('CRefCheckField-last_id-court'),
                mb_convert_encoding($table->filterXPath('//th[position()=6]')->text(), 'ISO-8859-1', 'UTF-8')
            );
            $this->assertEquals(
                $this->translator->tr('CRefCheckField-Count errors'),
                mb_convert_encoding($table->filterXPath('//th[position()=7]')->text(), 'ISO-8859-1', 'UTF-8')
            );

            // Check row
            $this->assertEquals('CUser', $table->filterXPath('//td[position()=1]')->text());
            $this->assertEquals('profile_id', $table->filterXPath('//td[position()=2]')->text());
            $this->assertEquals('CUser', $table->filterXPath('//td[position()=3]')->text());
            $this->assertCount(1, $table->filterXPath('//td[position()=4]'));
            $this->assertCount(1, $table->filterXPath('//td[position()=5]'));
            $this->assertCount(1, $table->filterXPath('//td[position()=6]'));

            $this->assertStringStartsWith(
                'ReferencesCheck.displayFieldErrors(\'',
                $table->filterXPath('//td[position()=8]//button')->attr('onclick')
            );
        } finally {
            $this->removeRefCheck();
        }
    }

    public function testShowFieldErrors(): void
    {
        $this->createRefCheck();

        try {
            $client = self::createClient();
            $client->request('GET', '/gui/developpement/referencesIntegrity/' . $this->field->_id, ['ajax' => 1]);

            $this->assertResponseStatusCodeSame(200);

            $crawler = $client->getCrawler();

            $this->assertEquals('CUser - profile_id', $crawler->filterXPath('//h2')->text());
            $this->assertCount(1, $crawler->filterXPath("//div[@id=\"{$this->field->field}\"]"));
            $this->assertStringContainsString(
                '/gui/developpement/referencesIntegrity/' . $this->field->_id,
                $crawler->filterXPath('//script')->text()
            );
        } finally {
            $this->removeRefCheck();
        }
    }

    public function testShowFieldErrorsWithClassName(): void
    {
        $this->createRefCheck();
        try {
            $client = self::createClient();
            $client->request(
                'GET',
                '/gui/developpement/referencesIntegrity/' . $this->field->_id,
                ['class_name' => 'CUser', 'ajax' => 1]
            );

            $this->assertResponseStatusCodeSame(200);

            $crawler = $client->getCrawler();

            $this->assertEquals(
                $this->translator->tr('CRefError-missing_id-court'),
                mb_convert_encoding($crawler->filterXPath('//th[position()=1]')->text(), 'ISO-8859-1', 'UTF-8')
            );
            $this->assertEquals(
                $this->translator->tr('CRefError-count_use-court'),
                mb_convert_encoding($crawler->filterXPath('//th[position()=2]')->text(), 'ISO-8859-1', 'UTF-8')
            );

            $this->assertEquals(2, $crawler->filterXPath('//tr[position()=3]//td[position()=1]')->text());
            $this->assertEquals(1, $crawler->filterXPath('//tr[position()=3]//td[position()=2]')->text());
        } finally {
            $this->removeRefCheck();
        }
    }

    public function testShowFieldErrorsWithMetaRef(): void
    {
        $this->createCheckRefWithMeta();

        try {
            $client = self::createClient();
            $client->request(
                'GET',
                '/gui/developpement/referencesIntegrity/' . $this->field->_id,
                ['ajax' => 1]
            );

            $this->assertResponseStatusCodeSame(200);

            $crawler = $client->getCrawler();

            $this->assertEquals('CFile - object_id', $crawler->filterXPath('//h2')->text());
            $this->assertStringContainsString(
                'Control.Tabs.create(\'tabs_errors_classes\',',
                $crawler->filterXPath('//script')->text()
            );

            $ul = $crawler->filterXPath('//ul[@id="tabs_errors_classes"]');
            $this->assertCount(1, $ul);
            $this->assertEquals(
                'CUser',
                $ul->filterXPath('//li[position()=1]//a')->text()
            );
            $this->assertEquals(
                'CMediusers',
                $ul->filterXPath('//li[position()=2]//a')->text()
            );
            $this->assertEquals('CUser', $crawler->filterXPath('//div[@id="CUser"]')->text());
            $this->assertEquals('CMediusers', $crawler->filterXPath('//div[@id="CMediusers"]')->text());
        } finally {
            $this->removeRefCheck();
        }
    }

    private function createRefCheck(): void
    {
        $this->ref        = new CRefCheckTable();
        $this->ref->class = 'CUser';
        if (!$this->ref->loadMatchingObjectEsc()) {
            $this->storeOrFailed($this->ref);
        }


        $this->field                     = new CRefCheckField();
        $this->field->field              = 'profile_id';
        $this->field->ref_check_table_id = $this->ref->_id;
        $this->field->target_class       = 'CUser';
        $this->storeOrFailed($this->field);

        $error                     = new CRefError();
        $error->ref_check_field_id = $this->field->_id;
        $error->missing_id         = '2';
        $error->count_use          = '1';
        $this->storeOrFailed($error);
    }

    private function createCheckRefWithMeta(): void
    {
        $this->ref        = new CRefCheckTable();
        $this->ref->class = 'CFile';
        if (!$this->ref->loadMatchingObjectEsc()) {
            $this->storeOrFailed($this->ref);
        }

        $this->field                     = new CRefCheckField();
        $this->field->field              = 'object_id';
        $this->field->ref_check_table_id = $this->ref->_id;
        $this->storeOrFailed($this->field);

        $sub_field1                          = new CRefCheckField();
        $sub_field1->field                   = 'object_id';
        $sub_field1->target_class            = 'CUser';
        $sub_field1->main_ref_check_field_id = $this->field->_id;
        $sub_field1->ref_check_table_id      = $this->ref->_id;
        $this->storeOrFailed($sub_field1);

        $error1                     = new CRefError();
        $error1->ref_check_field_id = $sub_field1->_id;
        $error1->missing_id         = '2';
        $error1->count_use          = '1';
        $this->storeOrFailed($error1);

        $sub_field2                          = new CRefCheckField();
        $sub_field2->field                   = 'object_id';
        $sub_field2->target_class            = 'CMediusers';
        $sub_field2->main_ref_check_field_id = $this->field->_id;
        $sub_field2->ref_check_table_id      = $this->ref->_id;
        $this->storeOrFailed($sub_field2);

        $error2                     = new CRefError();
        $error2->ref_check_field_id = $sub_field2->_id;
        $error2->missing_id         = '2';
        $error2->count_use          = '1';
        $this->storeOrFailed($error2);
    }

    private function removeRefCheck(): void
    {
        $this->deleteOrFailed($this->ref);
    }
}
