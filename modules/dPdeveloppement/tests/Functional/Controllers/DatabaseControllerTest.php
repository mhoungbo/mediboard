<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Ox\Core\Locales\Translator;
use Ox\Mediboard\Developpement\Database\IndexChecker;
use Ox\Tests\OxWebTestCase;

class DatabaseControllerTest extends OxWebTestCase
{
    public function testIndex(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/database/index', ['suppressHeaders' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();
        $this->assertCount(1, $crawler->filterXPath('//div[@id="tables"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="indexes"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="tables-integrity"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="tables-row-sizes"]'));
    }

    public function testIndexWithClassSpec(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/database/index', ['suppressHeaders' => 1, 'class_specs' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $this->assertCount(1, $crawler->filterXPath('//form[@name="mntTable"]'));
        $this->assertStringEndsWith(
            '/gui/developpement/database/checkClassSpecs',
            $crawler->filterXPath('//input[@name="@route"]')->attr('value')
        );
    }

    public function testIndexWithIndexes(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/gui/developpement/database/index',
            ['suppressHeaders' => 1, 'indexes' => 1, 'index_module' => 'dPdeveloppement']
        );

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();
        $this->assertCount(1, $crawler->filterXPath('//form[@name="search-indexes"]'));
        $this->assertStringEndsWith(
            '/gui/developpement/database/checkIndexes',
            $crawler->filterXPath('//input[@name="@route"]')->attr('value')
        );
        $this->assertCount(1, $crawler->filterXPath('//div[@id="result-search-indexes"]'));
        $this->assertCount(
            count(IndexChecker::ERROR_TYPES),
            $crawler->filterXPath('//input[@type="radio" and @name="error_type"]')
        );
        $this->assertCount(
            count(IndexChecker::KEY_TYPES),
            $crawler->filterXPath('//input[@type="radio" and @name="key_type"]')
        );
        $this->assertCount(2, $crawler->filterXPath('//input[@type="radio" and @name="show_all_fields"]'));
        $this->assertCount(1, $crawler->filterXPath('//select[@name="index_module"]'));
    }

    public function testIndexWithTableIntegrity(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/database/index', ['suppressHeaders' => 1, 'tables_integrity' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $this->assertCount(1, $crawler->filterXPath('//form[@name="check-tables-integrity"]'));
        $this->assertStringEndsWith(
            '/gui/developpement/database/checkTablesIntegrity',
            $crawler->filterXPath('//input[@name="@route"]')->attr('value')
        );
        $this->assertCount(1, $crawler->filterXPath('//select[@name="dsn"]'));
        $this->assertEquals('std', $crawler->filterXPath('//select[@name="dsn"]/option[@value="std"]')->text());
        $this->assertCount(3, $crawler->filterXPath('//input[@type="radio" and @name="type"]'));
        $this->assertCount(1, $crawler->filterXPath('//select[@name="mod_name"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="result-check-tables-integrity"]'));
    }

    public function testCheckClassSpec(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/gui/developpement/database/checkClassSpecs',
            ['suppressHeaders' => 1, 'class' => 'CUser']
        );

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();
        $this->assertEquals('user_id', $crawler->filterXPath('//td[@class="ok" and @data-type="key"]')->text());
    }

    public function testCheckIndexes(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/gui/developpement/database/checkIndexes',
            ['suppressHeaders' => 1, 'index_module' => 'admin', 'show_all_fields' => 1]
        );

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $this->assertCount(1, $crawler->filterXPath('//div[@id="tab_indexes"]'));
        $this->assertGreaterThan(1, $crawler->filterXPath('//td[@data-type="class_name"]')->count());
    }

    public function testCheckTablesIntegrity(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/gui/developpement/database/checkTablesIntegrity',
            ['suppressHeaders' => 1, 'dsn' => 'std', 'mod_name' => 'admin']
        );

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $tr = $crawler->filterXPath('//tr[@data-class="Ox\\Mediboard\\Admin\\CUser"]');
        $this->assertCount(1, $tr);

        $this->assertEquals('std', $tr->filterXPath('//td[position()=1]')->text());
        $this->assertEquals('Ox\\Mediboard\\Admin\\CUser', $tr->filterXPath('//td[position()=2]')->text());
        $this->assertEquals('users', $tr->filterXPath('//td[position()=3]')->text());
        $this->assertGreaterThan(0, $tr->filterXPath('//td[position()=4]')->text());
    }

    public function testCheckRowSizes(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/developpement/database/checkRowSizes', ['suppressHeaders' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();

        $this->assertCount(1, $crawler->filterXPath('//div[@id="check-tables-row-sizes-results-container"]'));

        $tr_head = $crawler->filterXPath('//thead/tr');
        $this->assertEquals('%', $tr_head->filterXPath('//th[position()=1]')->text());
        $this->assertEquals('Row size', $tr_head->filterXPath('//th[position()=2]')->text());
        $this->assertEquals('Champs', $tr_head->filterXPath('//th[position()=3]')->text());
        $this->assertEquals('Table', $tr_head->filterXPath('//th[position()=4]')->text());
    }
}
