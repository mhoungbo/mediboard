<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Ox\Core\Config\Conf;
use Ox\Core\Locales\Translator;
use Ox\Tests\OxWebTestCase;

class ClassesControllerTest extends OxWebTestCase
{
    public function testListStats(): void
    {
        $crawler = self::createClient()->request(
            'get',
            "/gui/developpement/classes"
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(
            (new Conf())->get('root_dir') . '/includes/classmap.php',
            $crawler->filterXPath('//tr[2]//td')->innerText()
        );
    }

    public function testListClassmapWithResults(): void
    {
        $crawler = self::createClient()->request(
            'get',
            "/gui/developpement/classes/classmap/list",
            ['ajax' => 1]
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(
            51,
            $crawler->filterXPath('//tr')
        );
    }

    public function testListClassmapWithoutResults(): void
    {
        $crawler = self::createClient()->request(
            'get',
            "/gui/developpement/classes/classmap/list",
            ['ajax' => 1, 'search' => 'loremipsumdolor']
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(
            (new Translator())->tr('No result'),
            mb_convert_encoding($crawler->filterXPath('//td[@class="empty"]')->innerText(), 'ISO-8859-1', 'UTF-8')
        );
    }

    public function testShowClassmapWithMatch(): void
    {
        $class_name_encoded = base64_encode('Ox\Mediboard\Developpement\CSetupDeveloppement');

        $crawler = self::createClient()->request(
            'get',
            "/gui/developpement/classes/classmap/{$class_name_encoded}",
            ['ajax' => 1]
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('Class exists.', $crawler->filterXPath('//div[@class="small-success"]')->text());
    }

    public function testShowClassmapWithoutMatch(): void
    {
        $class_name_encoded = base64_encode('OxMediboardDeveloppementCSetupDeveloppement');

        $crawler = self::createClient()->request(
            'get',
            "/gui/developpement/classes/classmap/{$class_name_encoded}",
            ['ajax' => 1]
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(
            'Invalid classmap index : OxMediboardDeveloppementCSetupDeveloppement',
            $crawler->filterXPath('//div[@class="small-error"]')->text()
        );
    }
}
