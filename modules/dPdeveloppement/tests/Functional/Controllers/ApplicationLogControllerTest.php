<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Developpement\Tests\Functional\Controllers;

use Ox\Tests\OxWebTestCase;

class ApplicationLogControllerTest extends OxWebTestCase
{

    public function testListApplication(): void
    {
        $client  = self::createClient();
        $crawler = $client->request(
            'get',
            "/gui/developpement/logs/application",
            [
                'ajax' => "1",
                'mode' => 'file',
            ]
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('logs (obtenus en ', $crawler->text());
    }


    public function testListGrepApplication(): void
    {
        $client  = self::createClient();
        $crawler = $client->request(
            'get',
            "/gui/developpement/logs/application",
            [
                'ajax' => "1",
                'grep_search' => "Took",
                'mode' => 'file',
            ]
        );

        $this->assertResponseStatusCodeSame(200);
        $this->assertStringContainsString('[INFO] CACHE : Took', $crawler->text());
    }
}
