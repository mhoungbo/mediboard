{{*
 * @package Mediboard\Ssr
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script type="text/javascript">
    displayDataSourceVersion = function() {
        new Url()
            .setRoute('{{url name=ssr_gui_csarr_datasource_version}}')
            .requestUpdate('csarr_datasources_version');
    };

    importDataSource = function () {
        new Url()
            .setRoute('{{url name=ssr_gui_csarr_datasource_import}}')
            .requestUpdate('csarr_import', {
                onComplete: displayDataSourceVersion.curry()
            });
    };

    Main.add(function() {
        displayDataSourceVersion();
    });
</script>

{{mb_include module=system template=datasources/configure_dsn dsn=csarr}}

<h2>{{tr}}Import_bdd_.csarr.title{{/tr}}</h2>

<div class="me-display-flex" style="align-items: center;">
    <div id="csarr_datasources_version"></div>
    <div class="me-flex-grow-1">
        <table class="tbl">
          <tr>
            <th>{{tr}}Action{{/tr}}</th>
            <th>{{tr}}Status{{/tr}}</th>
          </tr>

          <tr>
            <td>
              <button class="tick" onclick="importDataSource();" >
                {{tr}}Import_bdd_.csarr{{/tr}}</button>
              </td>
            <td id="csarr_import"></td>
          </tr>
        </table>
    </div>
</div>
