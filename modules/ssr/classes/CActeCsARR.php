<?php

/**
 * @package Mediboard\Ssr
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ssr;

use Ox\Core\CAppUI;
use Ox\Core\CMbObjectSpec;

/**
 * Actes SSR de la nomenclature CsARR
 */
class CActeCsARR extends CActeSSR
{
    public $acte_csarr_id;

    // DB Fields
    public $modulateurs;
    public $phases;
    public $commentaire;
    public $extension;

    // Derived feilds
    public $_modulateurs = [];
    public $_modulateur_patient;
    public $_modulateur_lieu;
    public $_modulateur_technique;
    public $_phases      = [];
    public $_fabrication;

    /** @var CActiviteCsARR */
    public $_ref_activite_csarr;

    /**
     * @see parent::getSpec()
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = 'acte_csarr';
        $spec->key   = 'acte_csarr_id';

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    public function getProps(): array
    {
        $props                      = parent::getProps();
        $props["sejour_id"]         .= " back|actes_csarr";
        $props["administration_id"] .= " back|actes_csarr";
        $props["evenement_ssr_id"]  .= " back|actes_csarr";
        $props["code"]              = "str notNull length|7 show|0";
        $props["modulateurs"]       = "str maxLength|20";
        $props["phases"]            = "str maxLength|3";
        $props["commentaire"]       = "text";
        $props["extension"]         = "str length|2";

        return $props;
    }

    /**
     * @see parent::updateFormFields()
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();
        if ($this->modulateurs) {
            $this->_view        .= "-$this->modulateurs";
            $this->_modulateurs = explode("-", $this->modulateurs);

            foreach ($this->_modulateurs as $modulateur) {
                switch (CModulateurCsARR::getModulatorType($modulateur)) {
                    case CModulateurCsARR::MODULATEUR_TYPE_PATIENT:
                        $this->_modulateur_patient = $modulateur;
                        break;
                    case CModulateurCsARR::MODULATEUR_TYPE_LIEU:
                        $this->_modulateur_lieu = $modulateur;
                        break;
                    case CModulateurCsARR::MODULATEUR_TYPE_TECHNIQUE:
                        $this->_modulateur_technique = $modulateur;
                        break;
                    default:
                }
            }
        }

        if ($this->phases) {
            $this->_view   .= ".$this->phases";
            $this->_phases = str_split($this->phases);
        }
    }

    /**
     * @see parent::updatePlainFields()
     */
    public function updatePlainFields(): void
    {
        parent::updatePlainFields();
        $this->modulateurs = $this->_modulateurs ? implode("-", $this->_modulateurs) : "";
        $this->phases      = $this->_phases ? implode("", $this->_phases) : "";
    }

    /**
     * @see parent::check()
     */
    public function check(): ?string
    {
        if (is_array($this->_modulateurs) && count($this->_modulateurs) && $error = $this->checkModulatorTypes()) {
            return $error;
        }

        return parent::check();
    }

    /**
     * Checks that only one modulator of each type is coded
     *
     * @return string|null
     */
    protected function checkModulatorTypes(): ?string
    {
        $patient = [];
        $technique = [];
        $lieu = [];

        foreach ($this->_modulateurs as $modulateur) {
            switch (CModulateurCsARR::getModulatorType($modulateur)) {
                case CModulateurCsARR::MODULATEUR_TYPE_PATIENT:
                    $patient[] = $modulateur;
                    break;
                case CModulateurCsARR::MODULATEUR_TYPE_LIEU:
                    $lieu[] = $modulateur;
                    break;
                case CModulateurCsARR::MODULATEUR_TYPE_TECHNIQUE:
                    $technique[] = $modulateur;
                    break;
                default:
            }
        }

        if (count($patient) > 1) {
            return CAppUI::tr(
                'CActeCsARR-error-multiple_modulators_of_type',
                CModulateurCsARR::MODULATEUR_TYPE_PATIENT,
                implode(', ', $patient)
            );
        } elseif (count($technique) > 1) {
            return CAppUI::tr(
                'CActeCsARR-error-multiple_modulators_of_type',
                CModulateurCsARR::MODULATEUR_TYPE_TECHNIQUE,
                implode(', ', $technique)
            );
        } elseif (count($lieu) > 1) {
            return CAppUI::tr(
                'CActeCsARR-error-multiple_modulators_of_type',
                CModulateurCsARR::MODULATEUR_TYPE_LIEU,
                implode(', ', $lieu)
            );
        }

        return null;
    }

    /**
     * @see parent::store()
     */
    public function store(): ?string
    {
        $this->completeField("commentaire");

        $recalculate = $this->_id && $this->fieldModified("commentaire") ? true : false;
        if ($this->_id) {
            $rhs = $this->loadRefEvenementSSR()->getRHS();
            if ($rhs->_id && $rhs->facture) {
                return "CRHS-failed-rhs-facture";
            }
        } else {
            $this->loadRefEvenementSSR();
            /* Reset the cache of the most used codes uppon the creation of a new CsArr act */
            if ($this->_ref_evenement_ssr) {
                CActiviteCsARR::resetUsedCodesCache($this->_ref_evenement_ssr->therapeute_id);
            }
        }
        // Standard store
        if ($msg = parent::store()) {
            return $msg;
        }
        if ($recalculate) {
            $this->loadRefEvenementSSR()->getRHS()->recalculate();
        }

        return null;
    }

    /**
     * Chargement de l'activit� associ�e
     *
     * @return CActiviteCsARR
     */
    public function loadRefActiviteCsarr(): ?CActiviteCsARR
    {
        $activite           = CActiviteCsARR::get($this->code);
        $this->_fabrication = strpos($activite->hierarchie, "09.02.02.") === 0;
        $activite->loadRefHierarchie();

        return $this->_ref_activite_csarr = $activite;
    }

    /**
     * @see parent::loadView()
     */
    public function loadView(): void
    {
        parent::loadView();
        $this->loadRefActiviteCsARR();
    }

    public function getElementPrescriptionToCsArrComment(): ?string
    {
        $comment   = "";
        $event_ssr = $this->loadRefEvenementSSR();
        if ($event_ssr->_id) {
            $line_element = $event_ssr->loadRefPrescriptionLineElement();
            if ($line_element->_id) {
                $presc_element = $line_element->loadRefElement();
                if ($presc_element->_id) {
                    $elements_to_csarr = $presc_element->loadRefsCsarrs();
                    foreach ($elements_to_csarr as $element) {
                        if ($element->code !== $this->code) {
                            continue;
                        }

                        return $element->commentaire;
                    }
                }
            }
        }

        return $comment;
    }
}
