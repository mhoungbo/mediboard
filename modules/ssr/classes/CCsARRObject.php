<?php
/**
 * @package Mediboard\Ssr
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ssr;

use Ox\Core\CStoredObject;

/**
 * Object CsARR
 */
class CCsARRObject extends CStoredObject
{

    /**
     * @see parent::getSpec()
     */
    function getSpec()
    {
        $spec              = parent::getSpec();
        $spec->dsn         = 'csarr';
        $spec->incremented = false;

        return $spec;
    }

    /**
     * Return the current version and the next available version of the CsARR database
     *
     * @return array (string current version, string next version)
     */
    public static function getDatabaseVersions()
    {
        return [
            "< v2015" => [
                [
                    "table_name" => "activite",
                    "filters"    => [],
                ],
            ],
            "v2015"   => [
                [
                    "table_name" => "activite",
                    "filters"    => [
                        "code" => "= 'AGR+102'",
                    ],
                ],
            ],
            "v2016"   => [
                [
                    "table_name" => "activite",
                    "filters"    => [
                        "code" => "= 'ZZM+193'",
                    ],
                ],
            ],
            "v2017"   => [
                [
                    "table_name" => "activite",
                    "filters"    => [
                        "code" => "= 'PEB+196'",
                    ],
                ],
            ],
            "v2018"   => [
                [
                    "table_name" => "activite",
                    "filters"    => [
                        "code"    => "= 'PBR+256'",
                        "libelle" => "LIKE '%ance de mobilisation articulaire passive'",
                    ],
                ],
            ],
            "v2019"   => [
                [
                    "table_name" => "activite",
                    "filters"    => [
                        "code" => "= 'ZFR+031'",
                    ],
                ],
            ],
            "v2020"   => [
                [
                    "table_name" => "activite",
                    "filters"    => [
                        "code"    => "= 'ZZQ+085'",
                        "libelle" => "= 'Évaluation des capacités sensitives et motrices nécessaires pour la conduite d\'un véhicule automobile sans adaptation personnalisée'",
                    ],
                ],
            ],
            '2023' => [
                [
                    'table_name' => 'code_extension',
                    'filters' => [
                        'code' => "= 'A4'",
                    ],
                ],
            ],
        ];
    }
}
