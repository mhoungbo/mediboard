<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ssr\Controllers;

use Exception;
use Ox\Core\CMbArray;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Ssr\CActeCsARR;
use Symfony\Component\HttpFoundation\Response;

/**
 * Ssr Event Controller
 */
class SsrEventController extends Controller
{
    /**
     * @throws Exception
     */
    public function saveCsArrActComment(RequestParams $params): Response
    {
        $acte_id     = $params->get('acte_id', 'str');
        $commentaire = $params->get('commentaire', 'text');

        $acte              = CActeCsARR::findOrFail($acte_id);
        $acte->commentaire = $commentaire;

        if ($msg = $acte->store()) {
            $response = ['error' => true, 'msg' => $msg];
        } else {
            $response = ['error' => false, 'msg' => $this->translator->tr('CActeCsARR-msg-modify')];
        }

        return $this->renderJsonResponse(CMbArray::toJSON($response));
    }
}
