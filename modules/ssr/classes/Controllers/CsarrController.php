<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ssr\Controllers;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\Controller;
use Ox\Mediboard\Ssr\CCsarrImport;
use Symfony\Component\HttpFoundation\Response;

/**
 * The controller for handling the CsARR datasource
 */
class CsarrController extends Controller
{
    /**
     * Displays the state of the LPP datasource
     *
     * @return Response
     * @throws Exception
     */
    public function getDataSourceVersion(): Response
    {
        $import = new CCsarrImport();

        $available_version = $import->getLastAvailableVersion();
        $installed_version = $import->getInstalledVersion();
        $is_up_to_date = $import->isDataSourceUpToDate();

        if ((!$installed_version || !$available_version) && $import->hasMessages()) {
            foreach ($import->getMessages() as $message) {
                $this->addMessage(...$message);
            }

            return $this->renderEmptyResponse();
        } else {
            return $this->renderSmarty('datasources_versions', [
                'datasources' => [
                    'csarr' => [
                        'name' => 'CCsARRObject',
                        'available_version' => $available_version,
                        'installed_version' => $installed_version,
                        'is_up_to_date'     => $is_up_to_date
                    ]
                ],
            ], 'system');
        }
    }

    /**
     * Import the CsArr database
     *
     * @return Response
     * @throws Exception
     */
    public function importDatasource(): Response
    {
        $import = new CCsarrImport();
        $import->import();

        foreach ($import->getMessages() as $message) {
            $this->addMessage(...$message);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * A method that has the same parameter order than CAppUI:setMsg to simplify the handling of the import messages
     *
     * @param string $msg
     * @param int    $type
     * @param        ...$args
     *
     * @return void
     */
    private function addMessage(string $msg, int $type, ...$args): void
    {
        switch ($type) {
            case CAppUI::UI_MSG_OK:
                $this->addUiMsgOk($msg, false, ...$args);
                break;
            case CAppUI::UI_MSG_ALERT:
                $this->addUiMsgAlert($msg, false, ...$args);
                break;
            case CAppUI::UI_MSG_WARNING:
                $this->addUiMsgWarning($msg, false, ...$args);
                break;
            case CAppUI::UI_MSG_ERROR:
                $this->addUiMsgError($msg, false, ...$args);
                break;
        }
    }
}
