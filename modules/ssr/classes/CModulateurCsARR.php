<?php

/**
 * @package Mediboard\Ssr
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ssr;

use Ox\Core\CMbObjectSpec;

/**
 * Modulateur d'activit�s CsARR
 */
class CModulateurCsARR extends CCsARRObject
{
    public const MODULATEUR_TYPE_PATIENT   = 'patient';
    public const MODULATEUR_TYPE_LIEU      = 'lieu';
    public const MODULATEUR_TYPE_TECHNIQUE = 'technique';

    // DB Fields
    public $code       = null;
    public $modulateur = null;

    // Derived Fields
    public $_libelle;
    public $_type;

    public $_ref_code = null;

    public static array $libelles = [
        "ZV" => "R�alisation de l'acte au lit du patient",
        "ME" => "R�alisation de l'acte en salle de soins",
        "TF" => "R�alisation de l'acte en �tablissement, en ext�rieur sans �quipement",
        "RW" => "R�alisation de l'acte en �tablissement, en ext�rieur avec �quipement",
        "HW" => "R�alisation de l'acte hors �tablissement en milieu urbain",
        "LJ" => "R�alisation de l'acte hors �tablissement en milieu naturel",
        "XH" => "R�alisation de l'acte sur le lieu d'activit�' du patient",
        "EZ" => "R�alisation fractionn�e de l'acte",
        "BN" => "N�cessit� de recours � un interpr�te",
        "QM" => "R�alisation de l'acte sur un plateau technique sp�cialis�, Baln�oth�rapie",
        "QS" => "R�alisation de l'acte sur un plateau technique sp�cialis�, R��ducation assist�e du membre sup�rieur",
        'QF' => "R�alisation de l'acte sur un plateau technique sp�cialis�, R��ducation intensive des membres inf�rieurs",
        'QI' => "R�alisation de l'acte sur un plateau technique sp�cialis�, Isocin�tisme",
        'QC' => "R�alisation de l'acte sur un plateau technique sp�cialis�, Simulateur et/ou v�hicule adapt� pour une r��ducation du retour � la conduite",
        'QQ' => "R�alisation de l'acte sur un plateau technique sp�cialis�, Laboratoire d'analyse quantifi�e de la marche et du mouvement",
    ];

    /** @var array */
    public static array $_types_modulateurs = [
        self::MODULATEUR_TYPE_PATIENT   => ['EZ'],
        self::MODULATEUR_TYPE_LIEU      => ['HW', 'ZV', 'XH', 'TF', 'ME', 'RW', 'LJ'],
        self::MODULATEUR_TYPE_TECHNIQUE => ['QC', 'QF', 'QI', 'QQ', 'QM', 'QS'],
    ];

    /**
     * @see parent::getSpec()
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = 'modulateur';

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        // DB Fields
        $props["code"]       = "str notNull length|7";
        $props["modulateur"] = "str notNull length|2";

        // Plain Fields
        $props["_libelle"] = "str";

        return $props;
    }

    /**
     * @see parent::loadView()
     */
    public function loadView(): void
    {
        parent::loadView();
        $this->loadRefCode();
    }

    /**
     * @see parent::updateFormFields()
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();
        $this->_libelle = self::$libelles[$this->modulateur];
        $this->_view    = "$this->modulateur: $this->_libelle";
        $this->_type    = self::getModulatorType($this->modulateur);
    }

    /**
     * Chargement du d�tail d'activit� CsARR
     *
     * @return CActiviteCsARR
     */
    public function loadRefCode(): ?CActiviteCsARR
    {
        return $this->_ref_code = CActiviteCsARR::get($this->code);
    }

    /**
     * Return the type of the given modulator
     *
     * @param string $modulator
     *
     * @return string|null
     */
    public static function getModulatorType(string $modulator): ?string
    {
        $type = null;
        foreach (self::$_types_modulateurs as $_type => $modulators) {
            if (in_array($modulator, $modulators)) {
                $type = $_type;
                break;
            }
        }

        return $type;
    }
}
