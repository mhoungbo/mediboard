<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CSmartyDP;
use Ox\Core\Module\CModule;

CCanDo::checkAdmin();

$modules = [
    "achilles",
    "axisante",
    "dpe",
    "lifeline",
    "osoft",
    "surgica",
];

$modules_info = [];
foreach ($modules as $_module) {
    $modules_info[$_module] = [
        "module_name" => $_module,
        "etat"        => "Absent",
        "doc_exist"   => 0,
    ];

    if (CModule::exists($_module)) {
        $modules_info[$_module]["etat"] = "Pr�sent";
    }

    if (CModule::getInstalled($_module)) {
        $modules_info[$_module]["etat"] = "Install�";
    }

    if (CModule::getActive($_module)) {
        $modules_info[$_module]["etat"] = "Actif";
    }

    if (file_exists("modules/$_module/vw_doc.php")) {
        $modules_info[$_module]["doc_exist"] = 1;
    }
}

$smarty = new CSmartyDP();
$smarty->assign("modules", $modules_info);
$smarty->display("vw_import_tools.tpl");
