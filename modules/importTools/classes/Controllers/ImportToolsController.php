<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\ImportTools\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\CRequest;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Sante400\CIdSante400;
use Symfony\Component\HttpFoundation\Response;

class ImportToolsController extends Controller
{
    /**
     * @throws Exception
     */
    public function showImportTagObjects(RequestParams $params): Response
    {
        $this->enforceSlave();

        if (!($import_tag = trim($params->get('import_tag', 'str')))) {
            $this->addUiMsgError("CPatientXMLImport-Error-Import tag missing");
        }

        $id_sante400 = new CIdSante400();
        $ds          = $id_sante400->getDS();

        $query = (new CRequest())
            ->addSelect(['object_class', 'COUNT(*) as total', 'MAX(last_update) as last_update'])
            ->addTable($id_sante400->getSpec()->table)
            ->addWhere(['tag' => $ds->prepare('= ?', $import_tag)])
            ->addGroup('object_class')
            ->addOrder('total DESC')
            ->makeSelect();

        return $this->renderSmarty(
            "inc_vw_import_search_tag_details",
            [
                "tag" => $import_tag,
                "list" => $ds->loadList($query),
            ]
        );
    }
}
