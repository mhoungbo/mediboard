<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\ImportTools\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\Handlers\Facades\HandlerManager;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Import\ImportTools\Purge\CImportPurgeLog;
use Ox\Import\ImportTools\Purge\ImportPurge;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

/**
 * Handle the deletion of imported objects.
 */
class ImportPurgeController extends Controller
{
    public function index(): Response
    {
        return $this->renderSmarty('purge/vw_purge.tpl');
    }

    public function search(RequestParams $params): Response
    {
        $this->enforceSlave();

        try {
            $start        = $params->get('start', 'num default|0');
            $step         = $params->get('step', 'num default|100 max|1000');
            $tag          = $params->get('tag', 'str notNull');
            $object_class = $params->get('object_class', 'str');
            $date_min     = $params->get('date_min', 'date notNull');
            $date_max     = $params->get('date_max', 'date notNull');
        } catch (Exception $e) {
            $this->addUiMsgError($e->getMessage(), true);

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }

        $purge = new ImportPurge($tag, $date_min, $date_max, $object_class, $start, $step);

        if ($params->get('stats_per_class', 'bool default|0')) {
            return $this->renderSmarty(
                'purge/vw_count_per_class',
                ['stats' => $purge->countIdexPerClass()]
            );
        }

        if (!$object_class) {
            $this->addUiMsgError('ImportPurge-error-Object class is mandatory', true);

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }


        return $this->renderSmarty(
            'purge/inc_vw_purge',
            [
                'objects'      => $purge->listObjectsToPurge(),
                'counts'       => $purge->getCounts(),
                'total'        => $purge->countIdex(),
                'start'        => $start,
                'step'         => $step,
                'tag'          => $tag,
                'object_class' => $object_class,
                'date_min'     => $date_min,
                'date_max'     => $date_max,
            ]
        );
    }

    public function purge(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('import_tools_purge', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $this->enforceSlave();

        $ids          = $params->post('purge_ids', 'str notNull');
        $tag          = $params->post('tag', 'str notNull');
        $object_class = $params->post('object_class', 'str notNull');
        $date_min     = $params->post('date_min', 'date notNull');
        $date_max     = $params->post('date_max', 'date notNull');

        // Disable object handlers
        HandlerManager::disableObjectHandlers();

        $purge = new ImportPurge($tag, $date_min, $date_max, $object_class);

        $result = $purge->purgeObjects($ids);

        foreach ($result['deletion_errors'] as $data) {
            $this->addUiMsgError('Erreur de suppression : ' . $data);
        }

        foreach ($result['not_found'] as $data) {
            $this->addUiMsgWarning('Objet non trouv� : ' . $data);
        }

        foreach ($result['outdated'] as $data) {
            $this->addUiMsgWarning('Objet modifi� : ' . $data);
        }

        foreach ($result['deleted'] as $data) {
            $this->addUiMsgOk($object_class . '-msg-delete');
        }

        return $this->renderEmptyResponse();
    }

    public function showLogs(RequestParams $params, CSQLDataSource $ds): Response
    {
        $this->enforceSlave();

        $user_id = $params->get('user_id', 'ref class|CMediusers');
        $guid    = $params->get('guid', 'str');
        $start   = $params->get('start', 'num default|0');
        $step    = $params->get('step', 'num default|100');

        $where = [];
        if ($user_id) {
            $where['user_id'] = $ds->prepare('= ?', $user_id);
        }

        if ($guid) {
            $where['guid'] = $ds->prepareLike("$guid%");
        }

        $log  = new CImportPurgeLog();
        $logs = $log->loadList($where, 'datetime DESC', "$start,$step");

        return $this->renderSmarty(
            'purge/inc_vw_logs.tpl',
            [
                'logs'  => $logs,
                'start' => $start,
                'step'  => $step,
                'total' => $log->countList($where),
            ]
        );
    }
}
