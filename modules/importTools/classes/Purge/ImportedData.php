<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\ImportTools\Purge;

use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Cabinet\CActeNGAP;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Facturation\CFacture;
use Ox\Mediboard\Facturation\CFactureCabinet;
use Ox\Mediboard\Facturation\CFactureItem;
use Ox\Mediboard\Facturation\CFactureLiaison;
use Ox\Mediboard\Facturation\CReglement;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CAntecedent;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\CEvenementPatient;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CTraitement;
use Ox\Mediboard\Patients\IPatientRelated;
use Ox\Mediboard\SalleOp\CActeCCAM;
use Ox\Mediboard\Sample\Entities\CSamplePerson;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Mediboard\System\CNote;
use Ox\Mediboard\System\CUserLog;

class ImportedData
{
    public const STATES      = [self::STATE_RED, self::STATE_GREEN, self::STATE_WHITE];
    public const STATE_RED   = 'red';
    public const STATE_GREEN = 'green';
    public const STATE_WHITE = 'white';

    private const LOG_TIME_DELTA = 1;

    private CIdSante400      $idx;
    private ?CStoredObject   $object            = null;
    private ?CUserLog        $last_log          = null;
    private ?CGroups         $owner_group       = null;
    private ?CGroups         $patient_group     = null;
    private ?CUser           $tag_creation_user = null;
    private ?CImportPurgeLog $log               = null;
    private string           $state;

    public function __construct(CIdSante400 $idx)
    {
        $this->idx    = $idx;
        $creation_log = $this->idx->loadCreationLog();
        if ($creation_log->_id) {
            $this->tag_creation_user = $creation_log->loadRefUser();
        }

        if ($this->idx->_count['import_purge_log'] > 0) {
            $this->log = reset($this->idx->_back['import_purge_log']);
        }

        $this->object = $idx->loadFwdRef('object_id', true);

        if ($this->object && $this->object->_id) {
            $this->last_log = $this->object->loadLastLog();
        }

        $this->computeState();
    }

    private function computeState(): void
    {
        if (!$this->object || !$this->object->_id) {
            $this->state = self::STATE_WHITE;

            return;
        }

        if (!$this->last_log || !$this->last_log->_id) {
            $this->state = self::STATE_WHITE;

            return;
        }

        if (CMbDT::dateTime(
                '+' . self::LOG_TIME_DELTA . 'HOURS',
                $this->idx->datetime_create
            ) <= $this->last_log->date) {
            $this->state = self::STATE_RED;

            return;
        }

        if (
            !($this->patient_group = $this->loadPatientGroup())
            || !($this->owner_group = $this->loadOwnerGroup())
        ) {
            $this->state = self::STATE_WHITE;

            return;
        }


        if ($this->patient_group->_id !== $this->owner_group->_id) {
            $this->state = self::STATE_GREEN;

            return;
        }

        $this->state = self::STATE_WHITE;
    }

    private function loadOwnerGroup(): ?CGroups
    {
        switch (get_class($this->object)) {
            case CFile::class:
            case CCompteRendu::class:
                $user = $this->object->loadFwdRef('author_id', true);
                break;
            case CAntecedent::class:
            case CConsultation::class:
            case CTraitement::class:
                $user = $this->object->loadFwdRef('owner_id', true);
                break;
            case CFactureCabinet::class:
            case CEvenementPatient::class:
                $user = $this->object->loadFwdRef('praticien_id', true);
                break;
            case CConstantesMedicales::class:
            case CNote::class:
                $user = $this->object->loadFwdRef('user_id', true);
                break;
            case CPlageconsult::class:
                $user = $this->object->loadFwdRef('chir_id', true);
                break;
            case CActeCCAM::class:
            case CActeNGAP::class:
            case CFactureItem::class:
                $user = $this->object->loadFwdRef('executant_id', true);
                break;
            case CReglement::class:
                /** @var CFacture $facture */
                $facture = $this->object->loadFwdRef('object_id', true);

                $user = $facture->loadFwdRef('praticien_id', true);
                break;
            case CFactureLiaison::class:
                $facture = $this->object->loadFwdRef('facture_id', true);

                $user = $facture->loadFwdRef('praticien_id', true);
                break;
            default:
                return null;
        }

        /** @var ?CMediusers $user */
        if (!$user || !$user->_id) {
            return null;
        }

        return $user->loadRefFunction()->loadRefGroup();
    }

    private function loadPatientGroup(): ?CGroups
    {
        if (!$this->object instanceof IPatientRelated) {
            return null;
        }

        /** @var CPatient $patient */
        $patient = $this->object->loadRelPatient();
        if (!$patient || !$patient->_id || !$patient->group_id) {
            return null;
        }

        return $patient->loadRefGroup();
    }

    /**
     * @return CIdSante400
     */
    public function getIdx(): CIdSante400
    {
        return $this->idx;
    }

    /**
     * @return CStoredObject|null
     */
    public function getObject(): ?CStoredObject
    {
        return $this->object;
    }

    /**
     * @return CUserLog|null
     */
    public function getLastLog(): ?CUserLog
    {
        return $this->last_log;
    }

    /**
     * @return CGroups|null
     */
    public function getOwnerGroup(): ?CGroups
    {
        return $this->owner_group;
    }

    /**
     * @return CGroups|null
     */
    public function getPatientGroup(): ?CGroups
    {
        return $this->patient_group;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    public function getTagCreator(): ?CUser
    {
        return $this->tag_creation_user;
    }

    public function getLog(): ?CImportPurgeLog
    {
        return $this->log;
    }
}
