<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\ImportTools\Purge;

use Exception;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Sante400\CIdSante400;
use ReflectionProperty;
use Throwable;

/**
 * Delete objects linked to CIdSante400 objects.
 */
class ImportPurge
{
    private string         $tag;
    private ?string        $object_class;
    private string         $date_min;
    private string         $date_max;
    private int            $start;
    private int            $step;
    private CSQLDataSource $slave;
    private int            $green_count = 0;
    private int            $red_count   = 0;
    private int            $white_count = 0;

    public function __construct(
        string  $tag,
        string  $date_min,
        string  $date_max,
        ?string $object_class = null,
        int     $start = 0,
        int     $step = 50
    ) {
        $this->tag          = $tag;
        $this->object_class = $object_class;
        $this->date_min     = $date_min;
        $this->date_max     = $date_max;
        $this->start        = $start;
        $this->step         = $step;
        $this->slave        = CSQLDataSource::get('slave');
    }

    /**
     * List the objects linked to the CIdSante400 objects.
     *
     * @return ImportedData[]
     * @throws Exception
     */
    public function listObjectsToPurge(): array
    {
        $idx_list = $this->listIdex();
        if ([] === $idx_list || null === $idx_list) {
            return [];
        }

        CStoredObject::massLoadFwdRef($idx_list, 'object_id');
        CStoredObject::massLoadBackRefs($idx_list, 'import_purge_log');

        $data = [];
        /** @var CIdSante400 $idx */
        foreach ($idx_list as $idx) {
            $imported_data = new ImportedData($idx);

            match ($imported_data->getState()) {
                ImportedData::STATE_WHITE => $this->white_count++,
                ImportedData::STATE_GREEN => $this->green_count++,
                ImportedData::STATE_RED => $this->red_count++,
            };

            $data[] = $imported_data;
        }

        return $data;
    }

    public function countIdex(): int
    {
        return (new CIdSante400())->countList($this->buildCondition());
    }

    public function countIdexPerClass(): array
    {
        $request = new CRequest();
        $request->addSelect(['object_class', 'total' => 'COUNT(*)']);
        $request->addTable('id_sante400');
        $request->addWhere(
            [
                'tag'             => $this->slave->prepare('= ?', $this->tag),
                'datetime_create' => $this->slave->prepareBetween($this->date_min, $this->date_max),
            ]
        );
        $request->addGroup('object_class');
        $request->addOrder('total DESC');

        return $this->slave->loadHashList($request->makeSelect());
    }

    /**
     * Delete objects targeted by CIdSante400 with an ID in $idx_ids.
     */
    public function purgeObjects(array $idx_ids): array
    {
        $idx_list = (new CIdSante400())->loadAll($idx_ids);

        CStoredObject::massLoadFwdRef($idx_list, 'object_id');

        $results = [
            'not_found'       => [],
            'outdated'        => [],
            'deleted'         => [],
            'deletion_errors' => [],
        ];

        // Check if the object can be deleted on the slave, then make the deletion on std.
        $ref_property = new ReflectionProperty(CStoredObject::class, 'check_delete_on_slave');
        $ref_property->setAccessible(true);
        $ref_property->setValue(true);

        // Keep deleted files on the FS.
        $ref_property = new ReflectionProperty(CFile::class, 'unlink_file');
        $ref_property->setAccessible(true);
        $ref_property->setValue(false);

        foreach ($idx_list as $idx) {
            // Avoid passing random ids to purge random objects
            if (
                $idx->object_class !== $this->object_class
                || $idx->tag !== $this->tag
                || $idx->datetime_create < $this->date_min
                || $idx->datetime_create > $this->date_max
            ) {
                continue;
            }

            // Object is already deleted
            if (!isset($idx->_fwd['object_id'])) {
                $results['not_found'][$idx->_id] = $idx->_guid . ' : ' . $idx->object_class . '-' . $idx->object_id;
                continue;
            }

            $target      = $idx->_fwd['object_id'];
            $target_view = $target->_guid . ' - ' . $target->_view;

            // This part is actually not used but might be activated if necessary.
            if ($target->_ref_last_log && $target->_ref_last_log->date > $idx->datetime_create) {
                $results['outdated'][$idx->_id] = $target_view . ' : '
                    . $target->_ref_last_log->date . ' > ' . $idx->datetime_create;
                continue;
            }

            $log         = new CImportPurgeLog();
            $log->idx_id = $idx->_id;
            $log->guid   = $target->_guid;
            $log->loadMatchingObjectEsc();

            try {
                $msg = $target->delete();
            } catch (Throwable $e) {
                $msg = $e->getMessage();
            } finally {
                // If message is null, empty the field
                $log->error = $msg ?? '';
                $log->cascade = implode('|', $this->flattenDeletedGuids($target));
                $log->store();

                if (null === $msg) {
                    $results['deleted'][$idx->_id] = $target_view;
                } else {
                    $results['deletion_errors'][$idx->_id] = $target_view . ' - ' . $msg;
                }
            }
        }

        return $results;
    }

    private function listIdex(): ?array
    {
        return (new CIdSante400())->loadList(
            $this->buildCondition(),
            ['datetime_create DESC', 'id_sante400_id DESC'],
            "$this->start,$this->step"
        );
    }

    private function buildCondition(): array
    {
        return [
            'tag'             => $this->slave->prepare('= ?', $this->tag),
            'object_class'    => $this->slave->prepare('= ?', $this->object_class),
            'datetime_create' => $this->slave->prepareBetween(
                $this->date_min . ' 00:00:00',
                $this->date_max . ' 23:59:59'
            ),
        ];
    }

    private function flattenDeletedGuids(CStoredObject $object): ?array
    {
        $guids = [];
        if (!($deleted_guids = $object->getDeletedGuids())) {
            return $guids;
        }

        foreach ($deleted_guids as $guid => $object) {
            $guids[] = $guid;

            $sub_guids = $this->flattenDeletedGuids($object);
            if (is_array($sub_guids)) {
                $guids = array_merge($guids, $sub_guids);
            }
        }

        return $guids;
    }

    public function getCacheKey(): string
    {
        return 'ImportPurge-purge-' . $this->tag . '-' . $this->object_class;
    }

    public function getCounts(): array
    {
        return [
            ImportedData::STATE_RED   => $this->red_count,
            ImportedData::STATE_GREEN => $this->green_count,
            ImportedData::STATE_WHITE => $this->white_count,
        ];
    }
}
