<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Import\ImportTools\Purge;

use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\Mediusers\CMediusers;

class CImportPurgeLog extends CMbObject
{
    /** @var int */
    public $import_purge_log_id;
    /** @var int */
    public $idx_id;
    /** @var string */
    public $guid;
    /** @var int */
    public $user_id;
    /** @var string */
    public $datetime;
    /** @var string */
    public $cascade;
    /** @var string */
    public $error;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table    = 'import_purge_log';
        $spec->key      = 'import_purge_log_id';
        $spec->loggable = CMbObjectSpec::LOGGABLE_NEVER;

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['idx_id']   = 'ref class|CIdSante400 unlink back|import_purge_log';
        $props['guid']     = 'str notNull index|1';
        $props['user_id']  = 'ref class|CMediusers notNull back|import_purge_logs';
        $props['datetime'] = 'dateTime notNull index|0';
        $props['cascade']  = 'str maxLength|1000';
        $props['error']    = 'str maxLength|1000';

        return $props;
    }

    public function store()
    {
        if (!$this->user_id) {
            $this->user_id = CMediusers::get()->_id;
        }

        if (!$this->datetime) {
            $this->datetime = CMbDT::dateTime();
        }

        return parent::store();
    }

    public static function getTableCreation(): string
    {
        return "CREATE TABLE `import_purge_log` (
                    `import_purge_log_id` INT (11) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
                    `idx_id` INT (11) UNSIGNED,
                    `guid` VARCHAR (255) NOT NULL,
                    `user_id` INT (11) UNSIGNED NOT NULL,
                    `datetime` DATETIME NOT NULL,
                    `cascade` VARCHAR (1000),
                    `error` VARCHAR (1000),
                    INDEX (`user_id`),
                    INDEX (`guid`),
                    INDEX (`idx_id`)
                )/*! ENGINE=MyISAM */;";
    }
}
