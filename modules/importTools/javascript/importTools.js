/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

ImportTools = {
  path_export_group: '',
  exportObject:      function () {
    var form = getForm("export_group");
    var group_id = form.object_id.value;
    const url = new Url().setRoute((ImportTools.path_export_group).replace('999999', group_id));

    var function_select = $V('function_select');

    url.addParam('function_select', function_select);
    url.popup(400, 150);
  },

  emptyFieldGroup: function () {
    var form = getForm("export_group");
    form.object_id.value = "";
    form.object_view.value = "";
  },

  updateFunctionList: function (group_id) {
    var url = new Url('importTools', 'listFunctionsByGroup');
    url.addParam('group_id', group_id);

    url.requestUpdate('listFunctions');
  },

  executeRedirectImportExport: function (redirect, module) {
    var url = new Url(module, redirect, 'tab');
    url.open();
  },

  executeRedirect: function (route_name) {
    new Url().setRoute(route_name).open();
  },

  searchImportTag: function () {
    new Url("patients", "ajaxSearchImportTag")
      .addParam('import_tag', $V('import_tag'))
      .requestUpdate("import_tag_result");

    $('import_tag_result').show();
    $("import_tag_result").removeClassName("unfold");
  },

  showTagDetail: function (elt) {
    new Url().setRoute(elt.get('url'), 'importtools_gui_tag_details', 'importTools')
      .addParam('import_tag', $V('import_tag'))
      .requestModal('30%', '40%');
  },
};
