{{*
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    $('imported-objects-results-container').fixedTableHeaders();
  });
</script>

<div id="imported-objects-results-container" style="margin: 8px;">
  <table class="main tbl me-w100" id="imported-objects-results-container-table">
    <thead>
    <tr>
      <th colspan="3">Tag : {{$tag}}</th>
    </tr>
      <tr>
        <th class="narrow">{{mb_label class=CIdSante400 field=object_class}}</th>
        <th class="narrow">{{tr}}Total{{/tr}}</th>
        <th class="narrow">{{mb_label class=CIdSante400 field=last_update}}</th>
      </tr>
    </thead>
    <tbody>
    {{foreach from=$list item=entity}}
      <tr>
        <td class="text">
            {{tr}}{{$entity.object_class}}{{/tr}} ({{$entity.object_class}})
        </td>
        <td class="text">
            {{$entity.total}}
        </td>
        <td class="text"
            title="{{$entity.last_update|date_format:$conf.date}} - {{$entity.last_update|iso_time}}">
            {{mb_ditto name=date value=$entity.last_update|date_format:$conf.date}}
          <span>{{mb_ditto name=time value=$entity.last_update|date_format:$conf.time}}</span>
        </td>
      </tr>
        {{foreachelse}}
      <tr>
        <td class="me-text-align-center" colspan="11" class="empty">
            {{tr}}importTools-import-tag-empty{{/tr}}
        </td>
      </tr>
    {{/foreach}}
    </tbody>
  </table>
</div>
