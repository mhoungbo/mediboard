{{*
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=importTools script=importTools}}

<script>
  Main.add(function () {
    $('import_tag_result').hide();
  });
</script>

<input type="text" name="import_tag" id="import_tag" size="25" value="{{$user_guid}}"/>
<button type="button" class="search notext" onclick="ImportTools.searchImportTag();"></button>

<a id="import_tag_result" class="trigger-detail"
   data-url="{{url name=importtools_gui_tag_details}}"
   onclick="ImportTools.showTagDetail(this)"
   title="{{tr}}CPatientXMLImport-Info-Click to see details{{/tr}}"></a>
