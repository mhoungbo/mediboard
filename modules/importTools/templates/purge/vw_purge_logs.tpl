{{*
 * @package Mediboard\ImportTools
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  changePageLog = function (start) {
    const form = getForm('purge-logs');
    $V(form.start, start);
    form.onsubmit();
  };

  Main.add(function () {
    let form = getForm('purge-logs');
    let element = form.elements.user_view;
    new Url("system", "ajax_seek_autocomplete")
    .addParam("object_class", "CMediusers")
    .addParam("input_field", element.name)
    .autoComplete(element, null, {
      minChars: 3,
      method: "get",
      select: "view",
      dropdown: true,
      afterUpdateElement: function (field, selected) {
        $V(form.user_id, selected.getAttribute("id").split("-")[2]);

        if ($V(element) == "") {
          $V(element, selected.down('.view').innerHTML);
        }
      }
    });
  });
</script>

<form name="purge-logs" method="get" onsubmit="return onSubmitFormAjax(this, null, 'result-purge-logs')">
  {{mb_route name=importtools_gui_purge_logs}}
  <input type="hidden" name="start" value="0"/>
  <input type="hidden" name="step" value="100"/>
  <input type="hidden" name="user_id" value=""/>

  <table class="main form">
    <tr>
      <th>{{mb_label class=CImportPurgeLog field=guid}}</th>
      <td>
        <input type="text" name="guid">
      </td>
      <th>{{mb_label class=CImportPurgeLog field=user_id}}</th>
      <td>
          <input type="text" class="autocomplete" name="user_view"/>
        <button type="button" class="erase notext" onclick="$V(this.form.user_id, '');$V(this.form.user_view, '');">
          {{tr}}Erase{{/tr}}
        </button>
      </td>
    </tr>

    <tr>
      <td class="button" colspan="4">
        <button type="submit" class="search">{{tr}}Search{{/tr}}</button>
      </td>
    </tr>
  </table>
</form>

<div id="result-purge-logs"></div>
