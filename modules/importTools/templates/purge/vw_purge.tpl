{{*
 * @package Mediboard\ImportTools
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create('tabs-purge-data');
  });
</script>

<ul id="tabs-purge-data" class="control_tabs">
  <li><a href="#purge-info">{{tr}}ImportPurge{{/tr}}</a></li>
  <li><a href="#purge-logs">{{tr}}CImportPurgeLog{{/tr}}</a></li>
</ul>

<div id="purge-info" style="display: none;">
  {{mb_include template="purge/vw_purge_form"}}
</div>

<div id="purge-logs" style="display: none;">
  {{mb_include template="purge/vw_purge_logs"}}
</div>
