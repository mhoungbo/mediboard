{{*
 * @package Mediboard\ImportTools
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_include module=system template=inc_pagination change_page="changePageLog" total=$total current=$start step=$step}}

<table class="main tbl">
  <tr>
    <th>{{mb_title class=CImportPurgeLog field=guid}}</th>
    <th>{{mb_title class=CImportPurgeLog field=user_id}}</th>
    <th>{{mb_title class=CImportPurgeLog field=datetime}}</th>
    <th>{{mb_title class=CImportPurgeLog field=cascade}}</th>
    <th>{{mb_title class=CImportPurgeLog field=error}}</th>
  </tr>

    {{foreach from=$logs item=log}}
      <tr>
        <td>
            {{if $log->error}}
              <span onmouseover="ObjectTooltip.createEx(this, '{{$log->guid}}')">{{mb_value object=$log field=guid}}</span>
                {{else}}
              {{mb_value object=$log field=guid}}
            {{/if}}
        </td>
        <td>
            <span onmouseover="ObjectTooltip.createEx(this, 'CMediusers-{{$log->user_id}}');">
              {{mb_value object=$log field=user_id}}
            </span>
        </td>
        <td>{{mb_value object=$log field=datetime}}</td>
        <td>{{mb_value object=$log field=cascade}}</td>
        <td>{{mb_value object=$log field=error}}</td>
      </tr>
        {{foreachelse}}
      <tr>
        <td class="empty" colspan="5">
            {{tr}}CImportPurgeLog.none{{/tr}}
        </td>
      </tr>
    {{/foreach}}
</table>
