{{*
 * @package Mediboard\ImportTools
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_include module=system template=inc_pagination change_page="changePagePurge" total=$total current=$start step=$step}}

<script>
  selectIdx = function (form) {
    if (!confirm($T('ImportPurge-Ask-Do you really want to delete those objects ?'))) {
      return false;
    }

    form.onsubmit();
  };

  toggleAll = function (checkbox) {
    $$('input.checkbox-purge-idex').each(function (elem) {
      $V(elem, $V(checkbox));
    });
  }
</script>

<table class="tbl" style="max-width: 200px;">
  <tr>
    <th>{{tr}}ImportedData-green_count{{/tr}}</th>
    <th>{{tr}}ImportedData-red_count{{/tr}}</th>
    <th>{{tr}}ImportedData-white_count{{/tr}}</th>
  </tr>
  <tr>
    <td class="ok">{{$counts.green}}</td>
    <td class="error">{{$counts.red}}</td>
    <td>{{$counts.white}}</td>
  </tr>
</table>

<form name="do-purge-idex" method="post"
      onsubmit="return onSubmitFormAjax(this, {onComplete: function () {getForm('purge-imported-data').onsubmit()}});">
    {{mb_route name=importtools_gui_purge}}
    {{csrf_token id=import_tools_purge}}
  <input type="hidden" name="tag" value="{{$tag}}"/>
  <input type="hidden" name="object_class" value="{{$object_class}}"/>
  <input type="hidden" name="date_min" value="{{$date_min}}"/>
  <input type="hidden" name="date_max" value="{{$date_max}}"/>

  <table class="main tbl">
    <tr>
      <th class="narrow">
        <input type="checkbox" onclick="toggleAll(this)"/>
      </th>
      <th>{{tr}}ImportedData-state{{/tr}}</th>
      <th class="narrow">{{tr}}CIdSante400{{/tr}} : {{tr}}CIdSante400-id400{{/tr}}</th>
      <th class="narrow">{{tr}}CIdSante400{{/tr}} : {{tr}}CIdSante400-datetime_create{{/tr}}</th>
      <th class="narrow">{{tr}}ImportPurge-Title-Updated{{/tr}}</th>
      <th>{{tr}}Object{{/tr}}</th>
      <th>{{tr}}ImportedData-tag_creation_user{{/tr}}</th>
      <th>{{tr}}ImportedData-patient_group{{/tr}}</th>
      <th>{{tr}}ImportedData-owner_group{{/tr}}</th>
      <th>{{tr}}common-Error|pl{{/tr}}</th>
      <th class="narrow">
        <button class="trash" type="button" onclick="selectIdx(this.form)">{{tr}}Delete{{/tr}}</button>
      </th>
    </tr>

      {{foreach from=$objects item=datum}}
          {{assign var=idex value=$datum->getIdx()}}
          {{assign var=idex_id value=$idex->_id}}
          {{assign var=object value=$datum->getObject()}}
          {{assign var=state value=$datum->getState()}}
          {{assign var=owner_group value=$datum->getOwnerGroup()}}
          {{assign var=patient_group value=$datum->getPatientGroup()}}
          {{assign var=tag_creator value=$datum->getTagCreator()}}
          {{assign var=last_log value=$datum->getLastLog()}}
          {{assign var=purge_log value=$datum->getLog()}}

        <tr>
          <td>
            <input type="checkbox" name="purge_ids[{{$idex->_id}}]"
                    {{if $state === 'green'}}class="checkbox-purge-idex"{{/if}}
                   value="{{$idex->_id}}"/>
          </td>
          <td {{if $state === 'green'}}class="ok"{{elseif $state === 'red'}}class="error"{{/if}}></td>
          <td>{{mb_value object=$idex field=id400}}</td>
          <td>{{mb_value object=$idex field=datetime_create}}</td>
          <td class="clickable" {{if $object}}onclick="guid_log('{{$object->_guid}}'){{/if}}">
              {{if $object && $last_log}}
                  {{mb_value object=$last_log field=date}}
              {{/if}}
          </td>
          <td>
              {{if $object}}
                <span onmouseover="ObjectTooltip.createEx(this, '{{$object->_guid}}')">{{$object}}</span>
              {{else}}
                  {{tr}}common-Removed object{{/tr}}
              {{/if}}
          </td>
          <td>
              {{if $tag_creator}}
                <span onmouseover="ObjectTooltip.createEx(this, '{{$tag_creator->_guid}}')">{{$tag_creator}}</span>
              {{/if}}
          </td>
          <td>
            {{if $patient_group}}
                <span onmouseover="ObjectTooltip.createEx(this, '{{$patient_group->_guid}}')">{{$patient_group}}</span>
            {{/if}}
          </td>
          <td>
              {{if $owner_group}}
                <span onmouseover="ObjectTooltip.createEx(this, '{{$owner_group->_guid}}')">{{$owner_group}}</span>
              {{/if}}
          </td>
          <td colspan="2" class="text">
              {{if $purge_log}}
                  {{$purge_log->error}}
              {{/if}}
          </td>
        </tr>
          {{foreachelse}}
        <tr>
          <td colspan="2" class="empty">{{tr}}None{{/tr}}</td>
        </tr>
      {{/foreach}}
  </table>
</form>
