{{*
 * @package Mediboard\ImportTools
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    const form = getForm('purge-imported-data');

    Calendar.regField(form.elements.date_min);
    Calendar.regField(form.elements.date_max);
  });

  changePagePurge = function (start) {
    const form = getForm('purge-imported-data');
    $V(form.start, start);
    form.onsubmit();
  };

  showTagsPerClass = function (form) {
    new Url().setRoute('{{url name=importtools_gui_purge_search}}')
      .addParam('stats_per_class', 1)
      .addParam('tag', $V(form.tag))
      .addParam('date_min', $V(form.date_min))
      .addParam('date_max', $V(form.date_max))
      .popup(500, 500);
  }
</script>

<form name="purge-imported-data" method="get"
      onsubmit="return onSubmitFormAjax(this, null, 'result-purge-imported-data')">
    {{mb_route name=importtools_gui_purge_search}}
  <input type="hidden" name="start" value="0"/>

  <table class="main form">
    <tr>
      <th>{{mb_title class=CIdSante400 field=tag}}</th>
      <td>
        <input type="text" name="tag"/>
        <button type="button" class="search notext" onclick="showTagsPerClass(this.form)"></button>
      </td>

      <th>{{tr}}common-Class{{/tr}}</th>
      <td colspan="3"><input type="text" name="object_class"/></td>
    </tr>

    <tr>
      <th>{{tr}}Dates{{/tr}}</th>
      <td>
        <input type="hidden" class="date notNull" name="date_min"/>
        �
        <input type="hidden" class="date notNull" name="date_max"/>
      </td>

      <th>{{tr}}Step{{/tr}}</th>
      <td>
        <select name="step">
          <option value="10">10</option>
          <option value="100">100</option>
          <option value="1000">1000</option>
        </select>
      </td>
    </tr>

    <tr>
      <td colspan="6" class="button">
        <button type="submit" class="search" onclick="$V(this.form.start, 0);">{{tr}}Search{{/tr}}</button>
      </td>
    </tr>
  </table>
</form>

<div id="result-purge-imported-data"></div>
