{{*
 * @package Mediboard\ImportTools
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main tbl">
  <tr>
    <th>{{tr}}common-Class{{/tr}}</th>
    <th>{{tr}}Count{{/tr}}</th>
  </tr>

  {{foreach from=$stats key=class_name item=count}}
      <tr>
        <td>{{$class_name}}</td>
        <td>{{$count}}</td>
      </tr>
  {{foreachelse}}
      <tr>
        <td class="empty" colspan="2">{{tr}}CIdSante400.none{{/tr}}</td>
      </tr>
  {{/foreach}}
</table>
