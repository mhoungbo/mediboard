/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class BlocOperatoire extends OxObject {
    constructor () {
        super()
        this.type = "bloc"
    }

    get nom (): OxAttr<string> {
        return super.get("nom")
    }

    get view (): OxAttr<string> {
        return this.nom
    }
}
