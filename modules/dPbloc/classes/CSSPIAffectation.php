<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc;

use DateTimeImmutable;
use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Mediboard\PlanningOp\COperation;

/**
 * Affectation SSPI linked to an operation, the patient monitoring is identified by the SSPI and the SSPI post
 */
class CSSPIAffectation extends CMbObject
{
    /** @var int Primary key */
    public $sspi_affectation_id;
    /** @var int */
    public $sspi_id;
    /** @var int */
    public $poste_sspi_id;
    /** @var int */
    public $poste_preop_id;
    /** @var int */
    public $operation_id;
    /** @var string */
    public $start;
    /** @var string */
    public $end;

    // forein keys ref
    /** @var CPosteSSPI */
    public $_ref_poste;
    /** @var COperation */
    public $_ref_operation;

    // behaviour
    public bool $_force_maj = true;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table        = 'sspi_affectation';
        $spec->key          = 'sspi_affectation_id';
        $spec->xor["poste"] = ["poste_sspi_id", "poste_preop_id"];

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['poste_sspi_id']  = "ref class|CPosteSSPI back|sspi_affectations";
        $props['poste_preop_id'] = "ref class|CPosteSSPI back|sspi_preop_affectations";
        $props['operation_id']   = "ref class|COperation notNull cascade back|sspi_affectations";
        $props['start']          = "dateTime notNull";
        $props['end']            = "dateTime";

        return $props;
    }

    /**
     * @inheritDoc
     */
    public function store(): ?string
    {
        if ($this->_force_maj) {
            $this->_force_maj = false;

            /** @var COperation $op */
            $op               = $this->loadRefOperation();
            $last_affectation = $this->poste_sspi_id ?
                $op->loadRefLastSSPIAffectation() :
                $op->loadRefLastSSPIPreopAffectation();

            // === Determine End ===
            // 1. Use the exit timing
            if ($this->poste_sspi_id && $op->sortie_reveil_reel) {
                $this->end = $op->sortie_reveil_reel;
            } elseif ($this->poste_preop_id && $op->fin_prepa_preop) {
                $this->end = $op->fin_prepa_preop;
            }

            // === Determine Start ===
            // 1. Use the last affectation existing
            if ($last_affectation->_id && $last_affectation->_id !== $this->_id) {
                if (!$last_affectation->end) {
                    if ($msg = $last_affectation->setEnd(
                        DateTimeImmutable::createFromFormat(
                            "Y-m-d H:i:s",
                            max($last_affectation->start, CMbDT::dateTime())
                        )
                    )) {
                        return $msg;
                    }
                }
                $this->start = $last_affectation->end;
            } // 2. Use the entry timing
            elseif ($this->poste_sspi_id && $op->entree_reveil) {
                $this->start = $op->entree_reveil;
            } elseif ($this->poste_preop_id && $op->debut_prepa_preop) {
                $this->start = $op->debut_prepa_preop;
            } // 3. Use current time
            else {
                $now         = CMbDT::dateTime();
                $this->start = $this->end ? min($now, $this->end) : $now;
            }
        }

        return parent::store();
    }

    /**
     * load SSPI post
     *
     * @return CStoredObject
     * @throws Exception
     */
    function loadRefPoste(): CStoredObject
    {
        return $this->_ref_poste = $this->loadFwdRef($this->poste_preop_id ? "poste_preop_id" : "poste_sspi_id", true);
    }

    /**
     * load operation
     *
     * @return CStoredObject
     * @throws Exception
     */
    function loadRefOperation(): CStoredObject
    {
        return $this->_ref_operation = $this->loadFwdRef("operation_id", true);
    }

    /**
     * Forcing start on the affectation
     * @throws Exception
     */
    public function setStart(?DateTimeImmutable $dateTime)
    {
        $this->start = $dateTime ? $dateTime->format("Y-m-d H:i:s") : "";
        return $this->storeInterval();
    }

    /**
     * Forcing end on the affectation
     * @throws Exception
     */
    public function setEnd(?DateTimeImmutable $dateTime)
    {
        $this->end = $dateTime ? $dateTime->format("Y-m-d H:i:s") : "";
        return $this->storeInterval();
    }

    /**
     * Forcing start and end on the affectation
     * @throws Exception
     */
    public function setInterval(DateTimeImmutable $start, DateTimeImmutable $end)
    {
        $this->end   = $end->format("Y-m-d H:i:s");
        $this->start = $start->format("Y-m-d H:i:s");
        return $this->storeInterval();
    }

    /**
     * Use _force_maj so it doesn't determine a new start/end
     * @throws Exception
     */
    private function storeInterval(): ?string
    {
        $this->_force_maj = false;
        return $this->store();
    }
}
