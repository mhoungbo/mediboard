<?php

/**
 * @package Mediboard\Bloc
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc;

use Exception;
use Ox\Core\CMbObject;

/**
 * Poste de SSPI (lit en salle de reveil)
 * Class CPosteSSPI
 */
class CPosteSSPI extends CMbObject
{
    // API Const
    public const RESOURCE_TYPE = 'poste_sspi';

    public $poste_sspi_id;

    // DB References
    public $sspi_id;

    // DB Fields
    public $nom;
    public $type;
    public $actif;

    /** @var CSSPI */
    public $_ref_sspi;

    /**
     * @see parent::getSpec()
     */
    function getSpec()
    {
        $spec        = parent::getSpec();
        $spec->table = "poste_sspi";
        $spec->key   = "poste_sspi_id";

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    function getProps()
    {
        $props            = parent::getProps();
        $props["sspi_id"] = "ref class|CSSPI back|postes_sspi";
        $props["nom"]     = "str notNull seekable";
        $props["type"]    = "enum list|sspi|preop default|sspi";
        $props["actif"]   = "bool notNull default|1";

        return $props;
    }

    /**
     * @see parent::updateFormFields()
     */
    function updateFormFields()
    {
        parent::updateFormFields();

        $this->_view = $this->nom;
    }

    /**
     * Chargement de la SSPI.
     *
     * @return CSSPI|null
     * @throws Exception
     */
    public function loadRefSSPI(): ?CSSPI
    {
        /** @var CSSPI _ref_sspi */
        return $this->_ref_sspi = $this->loadFwdRef("sspi_id", true);
    }
}
