<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Services;

use DateTimeImmutable;
use Exception;
use Ox\Core\Autoload\IShortNameAutoloadable;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Bloc\CSSPIAffectation;
use Ox\Mediboard\PlanningOp\COperation;

class OperationSSPiAffectationService implements IShortNameAutoloadable
{
    public const TYPE_PREOP = "preop";
    public const TYPE_SSPI  = "sspi";
    public const TYPES      = [self::TYPE_SSPI, self::TYPE_PREOP];

    public COperation $operation;
    public string     $type;

    /**
     * @throws Exception
     */
    public function __construct(COperation $operation, string $type)
    {
        if (!in_array($type, self::TYPES)) {
            throw new Exception("Wrong Type");
        }
        $this->type      = $type;
        $this->operation = $operation;
    }

    /**
     * @return CSSPIAffectation[]
     * @throws Exception
     */
    public function retrieveAffectations(): array
    {
        $order        = "start ASC, sspi_affectation_id ASC";
        $affectations = $this->type === self::TYPE_SSPI ?
            $this->operation->loadRefsSSPIAffectations($order) :
            $this->operation->loadRefsSSPIPreopAffectations($order);

        $this->prepareAffectations($affectations);

        return $affectations;
    }

    /**
     * @throws Exception
     */
    public function repairSSPIAffectations(bool $entree_changed, bool $sortie_changed): ?string
    {
        if ($this->type === self::TYPE_SSPI) {
            $affectations = $this->operation->loadRefsSSPIAffectations("start ASC, sspi_affectation_id ASC");
            $start_field  = "entree_reveil";
            $end_field    = "sortie_reveil_reel";
        } else {
            $affectations = $this->operation->loadRefsSSPIPreopAffectations("start ASC, sspi_affectation_id ASC");
            $start_field  = "debut_prepa_preop";
            $end_field    = "fin_prepa_preop";
        }

        if (
            (!$entree_changed && !$sortie_changed)
            || ($entree_changed && !$this->operation->$start_field && !$sortie_changed)
        ) {
            return null;
        }

        if (!sizeof($affectations)) {
            return null;
        }

        $affectations     = array_combine(array_column($affectations, "_id"), $affectations);
        $last_affectation = end($affectations);

        if ($this->operation->$end_field && $this->operation->$start_field > $this->operation->$end_field) {
            $last_affectation->setEnd(DateTimeImmutable::createFromFormat("Y-m-d H:i:s", $last_affectation->start));
            return null;
        }

        if ($entree_changed && $this->operation->$start_field) {
            // Take the affectation concerned by the new entry
            $entry = DateTimeImmutable::createFromFormat("Y-m-d H:i:s", $this->operation->$start_field);
            $aff   = $this->retrieveAffectationAt(
                $entry,
                $affectations
            );

            // If no affectation found it's the first we want the start to extend
            if (!$aff) {
                $aff = reset($affectations);
            } // Else get the previous one and extend it's end
            elseif ($prev_aff = $this->retrievePreviousAffectation($aff, $affectations)) {
                $prev_aff->setEnd($entry);
            }

            $aff->setStart($entry);
        }

        if ($sortie_changed) {
            $exit = null;
            $aff  = end($affectations);
            if ($this->operation->$end_field) {
                // Take the affectation concerned by the new exit
                $exit = DateTimeImmutable::createFromFormat("Y-m-d H:i:s", $this->operation->$end_field);
                $aff  = $this->retrieveAffectationAt(
                    $exit,
                    $affectations
                );

                // If no affectation found it's the last we want the end to extend
                if (!$aff) {
                    $aff = end($affectations);
                } // Else get the previous one and extend it's end
                elseif ($nex_affs = $this->retrieveNextAffectations($aff, $affectations)) {
                    foreach ($nex_affs as $_affectation) {
                        if ($msg = $_affectation->delete()) {
                            return $msg;
                        }
                    }
                }
            }

            if ($msg = $aff->setEnd($exit)) {
                return $msg;
            }
        }

        return null;
    }

    /**
     * @param DateTimeImmutable  $dateTime
     * @param CSSPIAffectation[] $haystack ORDER start ASC
     *
     * @return CSSPIAffectation|null
     */
    public function retrieveAffectationAt(DateTimeImmutable $dateTime, array $haystack): ?CSSPIAffectation
    {
        $needle = null;

        foreach (array_reverse($haystack) as $_affectation) {
            $start = DateTimeImmutable::createFromFormat(
                "Y-m-d H:i:s",
                $_affectation->start
            );
            $end   = $_affectation->end ? DateTimeImmutable::createFromFormat("Y-m-d H:i:s", $_affectation->end) : null;
            if ($start <= $dateTime && ($end >= $dateTime || !$_affectation->end)) {
                $needle = $_affectation;
                break;
            }
        }

        return $needle;
    }

    /**
     * @param CSSPIAffectation   $affectation
     * @param CSSPIAffectation[] $haystack
     *
     * @return CSSPIAffectation|null
     */
    public function retrievePreviousAffectation(CSSPIAffectation $affectation, array $haystack): ?CSSPIAffectation
    {
        if (!isset ($haystack[$affectation->_id])) {
            return null;
        }

        $previous_affectation = null;

        $position = array_search($affectation->_id, array_keys($haystack));
        if (isset(array_keys($haystack)[$position - 1])) {
            $previous_affectation = $haystack[array_keys($haystack)[$position - 1]];
        }

        return $previous_affectation;
    }

    /**
     * @param CSSPIAffectation   $affectation
     * @param CSSPIAffectation[] $haystack
     *
     * @return CSSPIAffectation|null
     */
    public function retrieveNextAffectation(CSSPIAffectation $affectation, array $haystack): ?CSSPIAffectation
    {
        if (!isset ($haystack[$affectation->_id])) {
            return null;
        }

        $next_affectation = null;

        $position = array_search($affectation->_id, array_keys($haystack));
        if (isset(array_keys($haystack)[$position + 1])) {
            $next_affectation = $haystack[array_keys($haystack)[$position + 1]];
        }

        return $next_affectation;
    }

    /**
     * @param CSSPIAffectation   $affectation
     * @param CSSPIAffectation[] $haystack
     *
     * @return CSSPIAffectation[]
     */
    public function retrieveNextAffectations(CSSPIAffectation $affectation, array $haystack): array
    {
        if (!isset ($haystack[$affectation->_id])) {
            return [];
        }

        $position = array_search($affectation->_id, array_keys($haystack));

        return array_slice($haystack, $position + 1, null, true);
    }

    /**
     * @param array $haystack
     *
     * @return CSSPIAffectation|null
     */
    public function findAffectationBeforeStart(array $haystack): ?CSSPIAffectation
    {
        $affectation_after_start = null;

        $start_field = $this->type === OperationSSPiAffectationService::TYPE_SSPI ? "entree_reveil" : "debut_prepa_preop";

        if ($this->operation->$start_field) {
            $start          = DateTimeImmutable::createFromFormat("Y-m-d H:i:s", $this->operation->$start_field);
            $affectation_at = $affectation_after_start = $this->retrieveAffectationAt(
                $start,
                array_reverse(
                    $haystack,
                    true
                )
            );
            if ($affectation_at) {
                $affectation_next = $this->retrieveNextAffectation($affectation_at, $haystack);
                if ($affectation_next) {
                    $at_end     = $affectation_at->end;
                    $next_start = $affectation_next->start;
                    if ($this->operation->$start_field <= $next_start && $this->operation->$start_field >= $at_end && $next_start !== $affectation_next->end) {
                        $affectation_after_start = $affectation_next;
                    }
                }
            }
        }

        return $affectation_after_start;
    }

    /**
     * @param CSSPIAffectation[] $affectations
     *
     * @return void
     * @throws Exception
     */
    private function prepareAffectations(array $affectations): void
    {
        CStoredObject::massLoadFwdRef($affectations, "poste_sspi_id");
        CStoredObject::massLoadFwdRef($affectations, "poste_preop_id");
        foreach ($affectations as $_affectation) {
            $_affectation->loadRefPoste();
        }
    }
}
