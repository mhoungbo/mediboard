<?php
/**
 * @package Mediboard\Bloc
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Controller;
use Ox\Mediboard\Bloc\CSalle;
use Ox\Mediboard\Etablissement\CGroups;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CSallesController
 */
class CSallesController extends Controller
{
    /**
     * @param RequestApi $request_api
     *
     * @return Response
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function list(RequestApi $request_api): Response
    {
        $salle = new CSalle();

        $ds = $salle->getDS();

        $where = [
            'bloc_operatoire.group_id' => $ds->prepare('= ?', CGroups::loadCurrent()->_id),
            'sallesbloc.actif'         => "= '1'",
            'bloc_operatoire.actif'    => "= '1'",
        ];

        if ($group_filter = $request_api->getRequestFilter()->getFilter("group_id")) {
            $where['bloc_operatoire.group_id'] = $ds->prepare("= ?", $group_filter->getValue());
        }

        if ($actif_filter = $request_api->getRequestFilter()->getFilter("actif")) {
            $where["sallesbloc.actif"] = $ds->prepare("= ?", $actif_filter->getValue());
            if ($actif_filter->getValue() === '0') {
                unset($where["bloc_operatoire.actif"]);
            }
        }

        $ljoin = [
            'bloc_operatoire' => 'bloc_operatoire.bloc_operatoire_id = sallesbloc.bloc_id',
        ];

        $salles = $salle->loadListWithPerms(
            PERM_READ,
            $where,
            $request_api->getSortAsSql("bloc_operatoire.nom ASC, sallesbloc.nom ASC"),
            $request_api->getLimitAsSql(),
            null,
            $ljoin
        );

        $collection = Collection::createFromRequest($request_api, $salles);

        /** @var Item $item */
        foreach ($collection as $item) {
            $item->addAdditionalDatas(["view" => $item->getDatas()->_view]);
        }

        return $this->renderApiResponse($collection);
    }
}
