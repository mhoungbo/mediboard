<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Etablissement\CGroups;
use Symfony\Component\HttpFoundation\Response;

class BlocController extends Controller
{
    /**
     * @throws Exception
     */
    public function showEditPreferences(): Response
    {
        $pref_bloc_ids = explode("|", $this->pref->get("selected_bloc_ids") ?? "");
        $group         = CGroups::loadCurrent();
        $blocs_list    = $group->loadBlocs(PERM_READ, true, "nom", ["actif" => "= '1'"]);

        return $this->renderSmarty(
            "vw_select_blocs",
            [
                "pref_bloc_ids" => $pref_bloc_ids,
                "blocs_list"    => $blocs_list,
            ]
        );
    }
}
