<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Controller;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Bloc\CSSPI;
use Ox\Mediboard\Bloc\CSSPILink;
use Ox\Mediboard\Etablissement\CGroups;
use Symfony\Component\HttpFoundation\Response;

class SSPIController extends Controller
{
    /**
     * List of SSPIs
     *
     * @param RequestApi $request_api
     *
     * @return Response
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function listSSPIs (RequestApi $request_api): Response
    {
        $sspi = new CSSPI();
        $ds = $sspi->getDS();

        $where = [
            "sspi.group_id" => $ds->prepare("= %", CGroups::loadCurrent()->_id)
        ];

        $sspis = $sspi->loadList($where, "libelle", $request_api->getLimitAsSql());
        $total = $sspi->countList($where);

        $resource = Collection::createFromRequest($request_api, $sspis);
        $resource->createLinksPagination($request_api->getOffset(), $request_api->getLimit(), $total);

        return $this->renderApiResponse($resource);
    }

    public function showEditPreferences(RequestParams $params): Response
    {
        $bloc_ids = $params->get("bloc_ids", "str");

        $links         = (new CSSPILink())->loadListWithPerms(
            PERM_READ,
            ['bloc_id' => CSQLDataSource::prepareIn($bloc_ids)]
        );
        $sspi_list     = CStoredObject::massLoadFwdRef($links, "sspi_id");
        $pref_sspi_ids = $this->pref->get("selected_sspi_ids") ?? "";

        return $this->renderSmarty(
            "vw_select_sspi",
            [
                "pref_sspi_ids" => explode('|', $pref_sspi_ids),
                "sspi_list"     => $sspi_list,
            ]
        );
    }
}
