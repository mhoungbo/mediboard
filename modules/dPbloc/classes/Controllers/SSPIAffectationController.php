<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Controllers;

use Exception;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Bloc\Services\OperationSSPiAffectationService;
use Ox\Mediboard\PlanningOp\COperation;
use Symfony\Component\HttpFoundation\Response;

class SSPIAffectationController extends Controller
{
    /**
     * @throws Exception
     */
    public function listFromOperation(RequestParams $params, COperation $operation): Response
    {
        $type = $params->get("type", "enum list|preop|sspi default|sspi");

        $service = new OperationSSPiAffectationService($operation, $type);

        if ($type === OperationSSPiAffectationService::TYPE_SSPI) {
            $start_field = "entree_reveil";
            $end_field   = "sortie_reveil_reel";
        } else {
            $start_field = "debut_prepa_preop";
            $end_field   = "fin_prepa_preop";
        }

        $affectations            = $service->retrieveAffectations();
        $affectation_after_start = $service->findAffectationBeforeStart($affectations);

        return $this->renderSmarty("vw_sspi_affectations_history", [
            'affectations'               => $affectations,
            'affectation_id_after_start' => $affectation_after_start->_id ?? null,
            'end_field'                  => $end_field,
            'operation'                  => $operation,
            'start_field'                => $start_field,
            'type'                       => $type,
        ]);
    }
}
