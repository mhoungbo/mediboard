<?php

/**
 * @author  SAS XtremSante <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Tests\Fixtures;

use Exception;
use Ox\Mediboard\Bloc\CBlocOperatoire;
use Ox\Mediboard\Bloc\CSalle;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Fixture for rosp stats
 */
class CSalleFixtures extends Fixtures implements GroupFixturesInterface
{
    public const TAG_USER = 'salle_fixtures-user-1';


    public const TAG_BLOC_A = 'salle_fixtures-bloc-a';
    public const TAG_BLOC_B = 'salle_fixtures-bloc-b';

    public const TAG_SALLE_A  = 'salle_fixtures-salle-a';
    public const TAG_SALLE_B1 = 'salle_fixtures-salle-B1';
    public const TAG_SALLE_B2 = 'salle_fixtures-salle-B2';

    public const TAG_SALLE_B3_INACTIVE = 'salle_fixtures-salle-B3-inactive';

    public static function getGroup(): array
    {
        return ['salle_fixtures'];
    }

    /**
     * @inheritDoc
     * @throws FixturesException
     */
    public function load(): void
    {
        $bloc_a = $this->generateBloc("A", self::TAG_BLOC_A);
        $bloc_b = $this->generateBloc("B", self::TAG_BLOC_B);

        $this->generateSalle($bloc_a, "A", self::TAG_SALLE_A);

        $this->generateSalle($bloc_b, "B1", self::TAG_SALLE_B1);
        $this->generateSalle($bloc_b, "B2", self::TAG_SALLE_B2);
        $this->generateSalle($bloc_b, "B3", self::TAG_SALLE_B3_INACTIVE, false);
    }

    /**
     * @throws FixturesException
     * @throws Exception
     */
    private function generateBloc(string $nom, string $tag): CBlocOperatoire
    {
        /** @var CBlocOperatoire $bloc */
        $bloc           = CBlocOperatoire::getSampleObject();
        $bloc->nom      = $nom;
        $bloc->group_id = CGroups::loadCurrent()->_id;
        $this->store($bloc, $tag);

        return $bloc;
    }

    /**
     * @throws FixturesException
     * @throws Exception
     */
    private function generateSalle(CBlocOperatoire $bloc, string $nom, string $tag, bool $active = true)
    {
        /** @var CSalle $salle */
        $salle          = CSalle::getSampleObject();
        $salle->nom     = $nom;
        $salle->bloc_id = $bloc->_id;
        $salle->actif   = $active ? '1' : '0';
        $this->store($salle, $tag);
    }
}
