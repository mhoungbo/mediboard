<?php

/**
 * @author  SAS XtremSante <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Tests\Fixtures;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Bloc\CPosteSSPI;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Bloc\CSSPIAffectation;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;
use Ox\Tests\Fixtures\GroupFixturesInterface;

/**
 * Fixture for rosp stats
 */
class CSSPIAffectationFixtures extends Fixtures implements GroupFixturesInterface
{
    public const TAG_USER                                  = 'sspi_affectation-user-1';
    public const TAG_PATIENT                               = 'sspi_affectation-patient-1';
    public const TAG_OPERATION_NEUTRAL                     = 'sspi_affectation-operation-1';
    public const TAG_OPERATION_WITH_ENTRY                  = 'sspi_affectation-operation-2';
    public const TAG_OPERATION_WITH_EXIT                   = 'sspi_affectation-operation-3';
    public const TAG_OPERATION_WITH_ENTRY_EXIT             = 'sspi_affectation-operation-4';
    public const TAG_OPERATION_NEUTRAL_WITH_HISTORY        = 'sspi_affectation-operation-5';
    public const TAG_OPERATION_WITH_ENTRY_AND_HISTORY      = 'sspi_affectation-operation-6';
    public const TAG_OPERATION_WITH_EXIT_AND_HISTORY       = 'sspi_affectation-operation-7';
    public const TAG_OPERATION_WITH_ENTRY_EXIT_AND_HISTORY = 'sspi_affectation-operation-8';
    public const TAG_OPERATION_TEST_ENTRY                  = 'sspi_affectation-operation-9';
    public const TAG_OPERATION_TEST_EXIT                   = 'sspi_affectation-operation-10';
    public const TAG_OPERATION_TEST_DATES                  = 'sspi_affectation-operation-11';
    public const TAG_SEJOUR                                = 'sspi_affectation-sejour-1';
    public const TAG_ENTRY_SSPI_AFFECTATION_10H            = 'sspi_affectation-sspi_affectation-1';
    public const TAG_ENTRY_SSPI_AFFECTATION_12H            = 'sspi_affectation-sspi_affectation-2';
    public const TAG_ENTRY_SSPI_AFFECTATION_15H            = 'sspi_affectation-sspi_affectation-3';
    public const TAG_ENTRY_SSPI_AFFECTATION_15H_2          = 'sspi_affectation-sspi_affectation-4';
    public const TAG_EXIT_SSPI_AFFECTATION_10H             = 'sspi_affectation-sspi_affectation-5';
    public const TAG_EXIT_SSPI_AFFECTATION_12H             = 'sspi_affectation-sspi_affectation-6';
    public const TAG_EXIT_SSPI_AFFECTATION_15H             = 'sspi_affectation-sspi_affectation-7';
    public const TAG_SSPI_AFFECTATION_TOMORROW             = 'sspi_affectation-sspi_affectation-8';
    public const TAG_POSTE_SSPI                            = 'sspi_affectation-poste_sspi-1';

    /** @var CPatient */
    public $patient;
    /** @var CSejour */
    public $sejour;
    /** @var CMediusers */
    public $user;
    /** @var COperation */
    public $operation;
    /** @var CPosteSSPI */
    public $poste_SSPI;

    public static function getGroup(): array
    {
        return ['sspi_affectation'];
    }

    /**
     * @inheritDoc
     * @throws FixturesException
     * @throws CModelObjectException
     */
    public function load(): void
    {
        $this->user       = $this->generateUser();
        $this->patient    = $this->generatePatient();
        $this->sejour     = $this->generateSejour();
        $this->poste_SSPI = $this->generatePosteSSPI();
        $date             = CMbDT::date();

        // Fixtures for testing CSSPIaffectation::store
        $datas = [
            [
                'entry'                => null,
                'exit'                 => null,
                'previous_affectation' => false,
                'tag'                  => self::TAG_OPERATION_NEUTRAL,
            ],
            [
                'entry'                => "$date 10:00:00",
                'exit'                 => null,
                'previous_affectation' => false,
                'tag'                  => self::TAG_OPERATION_WITH_ENTRY,
            ],
            [
                'entry'                => null,
                'exit'                 => "$date 12:00:00",
                'previous_affectation' => false,
                'tag'                  => self::TAG_OPERATION_WITH_EXIT,
            ],
            [
                'entry'                => "$date 10:00:00",
                'exit'                 => "$date 12:00:00",
                'previous_affectation' => false,
                'tag'                  => self::TAG_OPERATION_WITH_ENTRY_EXIT,
            ],
            [
                'entry'                => null,
                'exit'                 => null,
                'previous_affectation' => true,
                'tag'                  => self::TAG_OPERATION_NEUTRAL_WITH_HISTORY,
            ],
            [
                'entry'                => "$date 10:00:00",
                'exit'                 => null,
                'previous_affectation' => true,
                'tag'                  => self::TAG_OPERATION_WITH_ENTRY_AND_HISTORY,
            ],
            [
                'entry'                => null,
                'exit'                 => "$date 12:00:00",
                'previous_affectation' => true,
                'tag'                  => self::TAG_OPERATION_WITH_EXIT_AND_HISTORY,
            ],
            [
                'entry'                => "$date 10:00:00",
                'exit'                 => "$date 12:00:00",
                'previous_affectation' => true,
                'tag'                  => self::TAG_OPERATION_WITH_ENTRY_EXIT_AND_HISTORY,
            ],
        ];

        foreach ($datas as $data) {
            $op = $this->generateOperation($data['tag'], $data['entry'], $data['exit']);
            if ($data['previous_affectation']) {
                $this->generateSSPIAffectation($op);
            }
        }

        // Fixtures for testing the relation between COperation and CSSPIAffectation
        $op = $this->generateOperation(self::TAG_OPERATION_TEST_ENTRY);
        $this->generateSSPIAffectation($op, "$date 10:00:00", "$date 12:00:00", self::TAG_ENTRY_SSPI_AFFECTATION_10H);
        $this->generateSSPIAffectation($op, "$date 12:00:00", "$date 14:00:00", self::TAG_ENTRY_SSPI_AFFECTATION_12H);
        $this->generateSSPIAffectation($op, "$date 15:00:00", "$date 15:00:00", self::TAG_ENTRY_SSPI_AFFECTATION_15H);
        $this->generateSSPIAffectation($op, "$date 15:00:00", null, self::TAG_ENTRY_SSPI_AFFECTATION_15H_2);

        $op = $this->generateOperation(self::TAG_OPERATION_TEST_EXIT, "$date 10:00:00");
        $this->generateSSPIAffectation($op, "$date 10:00:00", "$date 12:00:00", self::TAG_EXIT_SSPI_AFFECTATION_10H);
        $this->generateSSPIAffectation($op, "$date 12:00:00", "$date 14:00:00", self::TAG_EXIT_SSPI_AFFECTATION_12H);
        $this->generateSSPIAffectation($op, "$date 15:00:00", null, self::TAG_EXIT_SSPI_AFFECTATION_15H);

        // Fixtures for testing the affectations on different dates
        $tomorrow = CMbDT::dateTime('+1 DAY');
        $op       = $this->generateOperation(self::TAG_OPERATION_TEST_DATES);
        $this->generateSSPIAffectation($op, $tomorrow, null, self::TAG_SSPI_AFFECTATION_TOMORROW);
    }

    /**
     * @throws FixturesException
     * @throws Exception
     */
    private function generateUser(): CMediusers
    {
        $user = $this->getUser(false);
        $this->store($user, self::TAG_USER);

        return $user;
    }

    /**
     * @throws FixturesException
     * @throws CModelObjectException
     */
    private function generatePatient(): CPatient
    {
        $patient = CPatient::getSampleObject();
        $this->store($patient, self::TAG_PATIENT);

        return $patient;
    }

    /**
     * @throws CModelObjectException
     * @throws FixturesException
     */
    public function generateSejour(): CSejour
    {
        /** @var CSejour $sejour */
        $sejour               = CSejour::getSampleObject();
        $sejour->patient_id   = $this->patient->_id;
        $sejour->praticien_id = $this->user->_id;
        $sejour->group_id     = CGroups::loadCurrent()->_id;
        $this->store($sejour, self::TAG_SEJOUR);

        return $sejour;
    }

    /**
     * @throws FixturesException
     */
    public function generateOperation(string $tag, ?string $entry = null, ?string $exit = null): COperation
    {
        $operation                     = new COperation();
        $operation->sejour_id          = $this->sejour->_id;
        $operation->chir_id            = $this->user->_id;
        $operation->entree_reveil      = $entry;
        $operation->sortie_reveil_reel = $operation->sortie_reveil_possible = $exit;

        $this->store($operation, $tag);

        return $operation;
    }

    /**
     * @throws FixturesException
     */
    public function generateSSPIAffectation(
        COperation $operation,
        ?string    $start = null,
        ?string    $end = null,
        ?string    $tag = null
    ): void {
        $sspi_affectation                = new CSSPIAffectation();
        $sspi_affectation->operation_id  = $operation->_id;
        $sspi_affectation->poste_sspi_id = $this->poste_SSPI->_id;

        if ($start || $end) {
            $sspi_affectation->_force_maj = false;
            $sspi_affectation->start      = $start;
            $sspi_affectation->end        = $end;
        }

        $this->store($sspi_affectation, $tag);
    }

    /**
     * @throws FixturesException
     */
    public function generatePosteSSPI(): CPosteSSPI
    {
        $poste_sspi      = new CPosteSSPI();
        $poste_sspi->nom = "sspiaffectation-fixtures";

        $this->store($poste_sspi, self::TAG_POSTE_SSPI);

        return $poste_sspi;
    }
}
