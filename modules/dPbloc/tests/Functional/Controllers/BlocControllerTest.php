<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Tests\Functional\Controllers;

use Exception;
use Ox\Tests\OxWebTestCase;

class BlocControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testShowEditPreferences(): void
    {
        $client = self::createClient();

        $client->request('GET', '/gui/bloc/blocsOperatoires/show_preferences');

        $this->assertResponseStatusCodeSame(200);
    }
}
