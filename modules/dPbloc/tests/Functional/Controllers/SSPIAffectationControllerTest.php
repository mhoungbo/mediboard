<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Tests\Functional\Controllers;

use Exception;
use Ox\Mediboard\Bloc\Tests\Fixtures\CSSPIAffectationFixtures;
use Ox\Tests\OxWebTestCase;
use Ox\Mediboard\PlanningOp\COperation;

class SSPIAffectationControllerTest extends OxWebTestCase
{
    /**
     * @throws Exception
     */
    public function testListFromOperation(): void
    {
        $client = self::createClient();

        $op = $this->getObjectFromFixturesReference(COperation::class, CSSPIAffectationFixtures::TAG_OPERATION_NEUTRAL);

        $client->request('GET', "/gui/bloc/operations/{$op->_id}/sspiAffectations", ["type" => "sspi"]);

        $this->assertResponseStatusCodeSame(200);
    }
}
