<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Bloc\CSalle;
use Ox\Mediboard\Bloc\Tests\Fixtures\CSalleFixtures;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class SallesControllerTest extends OxWebTestCase
{
    /**
     * Test default use on list
     *
     * @throws Exception
     */
    public function testListDefault(): void
    {
        $client = self::createClient();

        $salle_a  = $this->getSalle(CSalleFixtures::TAG_SALLE_A);
        $salle_b1 = $this->getSalle(CSalleFixtures::TAG_SALLE_B1);
        $salle_b2 = $this->getSalle(CSalleFixtures::TAG_SALLE_B2);
        $salle_b3 = $this->getSalle(CSalleFixtures::TAG_SALLE_B3_INACTIVE);

        $client->request('GET', '/api/bloc/salles');

        $this->assertResponseIsSuccessful();

        $collection = $this->getJsonApiCollection($client);
        $salle_ids  = [];

        // Test if we have our additionnal data view
        /** @var Item $item */
        foreach ($collection as $item) {
            $this->assertTrue($item->hasAttribute('view'));
            $salle_ids[] = $item->getId();
        }

        // Test if we receive by default only active rooms
        $this->assertNotContains($salle_b3->_id, $salle_ids);
        $this->assertContains($salle_a->_id, $salle_ids);
        $this->assertContains($salle_b1->_id, $salle_ids);
        $this->assertContains($salle_b2->_id, $salle_ids);

        // Test the order by default is nom ASC
        $this->assertTrue(array_search($salle_b1->_id, $salle_ids) < array_search($salle_b2->_id, $salle_ids));
    }

    /**
     * Test retrieve rooms but one room has a deny permission
     *
     * @return void
     * @throws Exception
     */
    public function testListWithSalleWithoutPermission(): void
    {
        $current_user = CUser::get();

        $salle_a = $this->getSalle(CSalleFixtures::TAG_SALLE_A);

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$salle_a->_class][$salle_a->_id] ?? null;

            CPermObject::$users_cache[$current_user->_id][$salle_a->_class][$salle_a->_id] = PERM_DENY;

            $client = self::createClient();
            $client->request('GET', '/api/bloc/salles');

            $salle_ids = [];

            $collection = $this->getJsonApiCollection($client);

            /** @var Item $item */
            foreach ($collection as $item) {
                $salle_ids[] = $item->getId();
            }

            // Assert that the denied room is not in our list
            $this->assertNotContains($salle_a->_id, $salle_ids);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$salle_a->_class][$salle_a->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$salle_a->_class][$salle_a->_id] = $actual_perm;
            }
        }
    }

    /**
     * @throws TestsException
     */
    private function getSalle(string $tag): CStoredObject
    {
        return $this->getObjectFromFixturesReference(CSalle::class, $tag);
    }
}
