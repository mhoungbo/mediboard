<?php

/**
 * @author  SAS XtremSante <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Tests\Unit;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Bloc\CPosteSSPI;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\Bloc\CSSPIAffectation;
use Ox\Mediboard\Bloc\Tests\Fixtures\CSSPIAffectationFixtures;
use Ox\Tests\OxUnitTestCase;

class CSSPIAffectationTest extends OxUnitTestCase
{
    /**
     * @throws Exception
     * @dataProvider providerStore
     */
    public function testStore(?string $expected_start, ?string $expected_end, string $tag): void
    {
        /** @var CPosteSSPI $poste */
        $poste = $this->getPosteSSPI();
        /** @var COperation $operation */
        $operation                  = $this->getOperation($tag);
        $affectation                = new CSSPIAffectation();
        $affectation->operation_id  = $operation->_id;
        $affectation->poste_sspi_id = $poste->_id;

        $affectation->store();
        if (property_exists($affectation, $expected_start)) {
            $last_aff = $this->loadLastAffectationThatIsNotTheTest($affectation);
            $this->assertEquals($last_aff->$expected_start, $affectation->start);
        } else {
            if ($expected_start === 'now') {
                $expected_start = CMbDT::dateTime();
            }
            $this->assertThat(
                $expected_start,
                $this->logicalOr(
                    $this->equalTo($affectation->start),
                    // In case the store is 1 second before the test
                    $this->equalTo(CMbDT::dateTime('+1 SECOND', $affectation->start)),
                )
            );
        }
        $this->assertEquals($expected_end, $affectation->end);
    }

    public function providerStore(): array
    {
        $date = CMbDT::date();
        $now  = CMbDT::dateTime();

        return [
            'neutral'                 => [
                'expected_start' => 'now',
                'expected_end'   => null,
                'tag'            => CSSPIAffectationFixtures::TAG_OPERATION_NEUTRAL,
            ],
            'withEntry'               => [
                'expected_start' => $date . ' 10:00:00',
                'expected_end'   => null,
                'tag'            => CSSPIAffectationFixtures::TAG_OPERATION_WITH_ENTRY,
            ],
            'withExit'                => [
                'expected_start' => min($now, $date . ' 12:00:00') === $now ? 'now' : $date . ' 12:00:00',
                'expected_end'   => $date . ' 12:00:00',
                'tag'            => CSSPIAffectationFixtures::TAG_OPERATION_WITH_EXIT,
            ],
            'withEntryExit'           => [
                'expected_start' => $date . ' 10:00:00',
                'expected_end'   => $date . ' 12:00:00',
                'tag'            => CSSPIAffectationFixtures::TAG_OPERATION_WITH_ENTRY_EXIT,
            ],
            'neutralWithHistory'      => [
                'expected_start' => 'end',
                'expected_end'   => null,
                'tag'            => CSSPIAffectationFixtures::TAG_OPERATION_NEUTRAL_WITH_HISTORY,
            ],
            'withEntryAndHistory'     => [
                'expected_start' => 'end',
                'expected_end'   => null,
                'tag'            => CSSPIAffectationFixtures::TAG_OPERATION_WITH_ENTRY_AND_HISTORY,
            ],
            'withExitAndHistory'      => [
                'expected_start' => 'end',
                'expected_end'   => $date . ' 12:00:00',
                'tag'            => CSSPIAffectationFixtures::TAG_OPERATION_WITH_EXIT_AND_HISTORY,
            ],
            'withEntryExitAndHistory' => [
                'expected_start' => 'end',
                'expected_end'   => $date . ' 12:00:00',
                'tag'            => CSSPIAffectationFixtures::TAG_OPERATION_WITH_ENTRY_EXIT_AND_HISTORY,
            ],
        ];
    }

    /**
     * @throws Exception
     */
    protected function getOperation(string $tag): CStoredObject
    {
        return $this->getObjectFromFixturesReference(COperation::class, $tag);
    }

    /**
     * @throws Exception
     */
    protected function getPosteSSPI(): CStoredObject
    {
        return $this->getObjectFromFixturesReference(
            CPosteSSPI::class,
            CSSPIAffectationFixtures::TAG_POSTE_SSPI
        );
    }

    protected function loadLastAffectationThatIsNotTheTest(CSSPIAffectation $affectation): CSSPIAffectation
    {
        $sspi_affectation = new CSSPIAffectation();

        $where = [
            "operation_id"        => $sspi_affectation->getDS()->prepare("= ?", $affectation->operation_id),
            "poste_sspi_id"       => "IS NOT NULL",
            "sspi_affectation_id" => $sspi_affectation->getDS()->prepare("!= ?", $affectation->_id),
        ];

        $sspi_affectation->loadObject($where, "start DESC");

        return $sspi_affectation;
    }
}
