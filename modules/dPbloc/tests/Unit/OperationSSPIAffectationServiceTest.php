<?php

/**
 * @author  SAS XtremSante <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Bloc\Tests\Unit;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\Bloc\CSSPIAffectation;
use Ox\Mediboard\Bloc\Tests\Fixtures\CSSPIAffectationFixtures;
use Ox\Tests\OxUnitTestCase;

class OperationSSPIAffectationServiceTest extends OxUnitTestCase
{
    /**
     * @throws Exception
     */
    public function testModifyOperationEntryTiming()
    {
        /** @var COperation $op */
        $op   = $this->getOperation(CSSPIAffectationFixtures::TAG_OPERATION_TEST_ENTRY);
        $date = CMbDT::date();

        // 1. The first affectation is adjusted with the entry time
        $op->entree_reveil = "$date 09:00:00";
        $op->store();

        /** @var CSSPIAffectation $aff_10h */
        $aff_10h = $this->getSSPIAffectation(CSSPIAffectationFixtures::TAG_ENTRY_SSPI_AFFECTATION_10H);
        $this->assertEquals($op->entree_reveil, $aff_10h->start);

        // 2. The LAST affectation that has 15h in it's range and the previous one is adjusted
        $op->entree_reveil = "$date 16:00:00";
        $op->store();

        /** @var CSSPIAffectation $aff_15h */
        $aff_15h = $this->getSSPIAffectation(CSSPIAffectationFixtures::TAG_ENTRY_SSPI_AFFECTATION_15H);
        /** @var CSSPIAffectation $aff_15h_2 */
        $aff_15h_2 = $this->getSSPIAffectation(CSSPIAffectationFixtures::TAG_ENTRY_SSPI_AFFECTATION_15H_2);

        $this->assertEquals($op->entree_reveil, $aff_15h->end);
        $this->assertEquals($op->entree_reveil, $aff_15h_2->start);
    }

    /**
     * @throws Exception
     */
    public function testModifyOperationExitTiming()
    {
        /** @var COperation $op */
        $op   = $this->getOperation(CSSPIAffectationFixtures::TAG_OPERATION_TEST_EXIT);
        $date = CMbDT::date();

        // 1. The last affectation is adjusted with the end time
        $op->sortie_reveil_reel = $op->sortie_reveil_possible = "$date 22:00:00";
        $op->store();

        /** @var CSSPIAffectation $aff_15h */
        $aff_15h = $this->getSSPIAffectation(CSSPIAffectationFixtures::TAG_EXIT_SSPI_AFFECTATION_15H);
        $this->assertEquals($op->sortie_reveil_reel, $aff_15h->end);

        // 2. Exit is removed
        $op->sortie_reveil_reel = $op->sortie_reveil_possible = "";
        $op->store();

        /** @var CSSPIAffectation $aff_15h */
        $aff_15h = (new CSSPIAffectation())->load($aff_15h->_id);
        $this->assertNull($aff_15h->end);

        // 3. The LAST affectation that has 9h in it's range is adjusted and the next ones are deleted
        $op->sortie_reveil_reel = $op->sortie_reveil_possible = "$date 11:00:00";
        $op->store();

        /** @var CSSPIAffectation $aff_10h */
        $aff_10h = $this->getSSPIAffectation(CSSPIAffectationFixtures::TAG_EXIT_SSPI_AFFECTATION_10H);
        $this->assertEquals($op->sortie_reveil_reel, $aff_10h->end);

        $affectations = $op->loadRefsSSPIAffectations();
        $this->assertEquals([$aff_10h->_id], array_column($affectations, "_id"));
    }

    public function testAffectationTomorrow() {
        /** @var COperation $operation */
        $operation = $this->getOperation(CSSPIAffectationFixtures::TAG_OPERATION_TEST_DATES);
        $affectation = $operation->loadRefLastSSPIAffectation(CMbDT::date());

        $affectation_tomorrow = $this->getSSPIAffectation(CSSPIAffectationFixtures::TAG_SSPI_AFFECTATION_TOMORROW);
        $this->assertNotEquals($affectation_tomorrow->_id, $affectation->_id);

        $affectation = $operation->loadRefLastSSPIAffectation(CMbDT::date('+1 DAY'));
        $this->assertEquals($affectation_tomorrow->_id, $affectation->_id);
    }


    /**
     * @throws Exception
     */
    protected function getOperation(string $tag): CStoredObject
    {
        return $this->getObjectFromFixturesReference(COperation::class, $tag);
    }

    /**
     * @throws Exception
     */
    protected function getSSPIAffectation(string $tag): CStoredObject
    {
        return $this->getObjectFromFixturesReference(CSSPIAffectation::class, $tag);
    }

}
