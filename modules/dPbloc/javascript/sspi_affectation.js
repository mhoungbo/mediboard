/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

SSPIAffectation = window.SSPIAffectation || {
  path_operation_list_affectations: '',
  openHistory:   function (operation_id, type = 'sspi', onClose) {
    new Url()
      .addParam('operation_id', operation_id)
      .addParam('type', type)
      .setRoute(SSPIAffectation.path_operation_list_affectations.replace('0', operation_id), 'bloc_operation_list_sspi_affectations', 'bloc')
      .modal({width: '400px', onClose: onClose});
  },
  updateHistory: function (div_id, operation_id, type = 'sspi') {
    new Url()
      .addParam('operation_id', operation_id)
      .addParam('type', type)
      .setRoute(SSPIAffectation.path_operation_list_affectations.replace('0', operation_id), 'bloc_operation_list_sspi_affectations', 'bloc')
      .requestUpdate(div_id);
  }
};
