{{*
* @package Mediboard\PlanningOp
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{unique_id var=div_id}}
{{assign var=date value=""}}

<div id="affectationHistory{{$div_id}}" class="me-padding-left-10 me-padding-right-10">
{{if !$affectation_id_after_start && $operation->$start_field}}
  <div class="MessagingIcon small-success me-text-align-center">
      {{tr}}COperation-{{$start_field}}{{/tr}} {{$operation->$start_field|date_format:$conf.datetime}}
  </div>
{{/if}}
{{foreach from=$affectations item=_affectation name=affectation_list}}
  {{if $affectation_id_after_start === $_affectation->_id}}
    <div class="MessagingIcon small-success me-text-align-center">
       {{tr}}COperation-{{$start_field}}{{/tr}} {{$operation->$start_field|date_format:$conf.datetime}}
    </div>
  {{/if}}
  <table class="tbl me-w100">
    <tr>
      <td class="me-text-align-left" style="width: 130px;">
        {{if $smarty.foreach.affectation_list.first || $_affectation->start|date_format:$conf.date !== $date}}
          {{assign var=date value=$_affectation->start|date_format:$conf.date}}
          {{$_affectation->start|date_format:$conf.datetime}}
        {{else}}
          {{$_affectation->start|date_format:$conf.time}}
        {{/if}}
      </td>
      <td class="me-text-align-left">
        {{$_affectation->_ref_poste}}
      </td>
      {{if $_affectation->end}}
      <td class="me-text-align-right">
          {{$_affectation->end|date_format:$conf.time}}
      </td>
      {{else}}
      <td class="me-text-align-right empty">
          {{tr}}common-In progress{{/tr}}
      </td>
      {{/if}}
    </tr>
  </table>
{{foreachelse}}
  {{if $operation->$start_field}}
    <div class="MessagingIcon small-success me-text-align-center">
       {{tr}}COperation-{{$start_field}}{{/tr}} {{$operation->$start_field|date_format:$conf.datetime}}
    </div>
  {{/if}}
  <table class="tbl me-w100">
    <tr>
      <td class="me-text-align-center empty">
        {{tr}}CSSPIAffectation.none{{/tr}}
      </td>
    </tr>
  </table>
{{/foreach}}
<table class="tbl me-w100">
  <tr>
    <td class="me-text-align-center">
    {{assign value=$operation->_id var=operation_id}}
    {{if $type === "preop" }}
      {{mb_include module=salleOp template=inc_form_toggle_poste_preop _operation=$operation type=""
      callback="SSPIAffectation.updateHistory('affectationHistory$div_id', '$operation_id', '$type')"}}
    {{else}}
      {{mb_include module=dPsalleOp template=inc_form_toggle_poste_sspi _operation=$operation type=""
      callback="SSPIAffectation.updateHistory('affectationHistory$div_id', '$operation_id', '$type')"}}
     {{/if}}
    </td>
  </tr>
</table>
{{if $operation->$end_field}}
  <div class="MessagingIcon small-error me-text-align-center">
     {{tr}}COperation-{{$end_field}}{{/tr}} {{$operation->$end_field|date_format:$conf.datetime}}
  </div>
{{/if}}
</div>
