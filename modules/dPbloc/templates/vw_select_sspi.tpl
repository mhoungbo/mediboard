{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}


<table class="tbl">
  <tr>
    <th>{{tr}}CSSPI{{/tr}}</th>
  </tr>
  <tr>
    <td>
      <div style="display:flex; flex-direction: column; gap: 8px">
        {{foreach from=$sspi_list item=_sspi}}
        <label>
          <input
            type="checkbox"
            class="sspi_id"
            name="sspi_ids[{{$_sspi->_id}}]"
            {{if in_array($_sspi->_id, $pref_sspi_ids)}}checked{{/if}}
            value="{{$_sspi->_id}}"
            onchange="SSPI.saveSelectSSPIPref($$('input.sspi_id:checked').pluck('value'))"
          />
          <span class="me-margin-left-8">{{$_sspi}}</span>
        </label>
        {{foreachelse}}
        <span class="empty">{{tr}}CSSPI.none{{/tr}}</span>
        {{/foreach}}
      </div>
    </td>
  </tr>
</table>


