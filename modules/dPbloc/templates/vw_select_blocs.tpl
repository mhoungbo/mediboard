{{*
* @author  SAS OpenXtrem <dev@openxtrem.com>
* @license https://www.gnu.org/licenses/gpl.html GNU General Public License
* @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=salleOp script=sspi ajax=1}}

<script>
  Main.add(function () {
    SSPI.path_blocs_preferences = '{{url name=bloc_blocs_show_preferences}}';
    SSPI.path_sspis_preferences = '{{url name=bloc_sspis_show_preferences}}';
    {{if $conf.dPplanningOp.COperation.use_poste}}
      refreshSelectSSPI = function (save_new_sspi = false) {
        let onComplete = null
        if (save_new_sspi) {
          onComplete = function () {
            const sspi_ids = $$('input.sspi_id:checked') ? $$('input.sspi_id:checked').pluck('value') : [];
            SSPI.saveSelectSSPIPref(sspi_ids)
          }
        }
        SSPI.updateSelectSSPI("sspi_filter", $$('input.bloc_id:checked').pluck('value'), onComplete);
      }

      refreshSelectSSPI();
    {{/if}}
  })
</script>

<div class="me-display-flex me-flex-row">
    <div class="me-padding-8 {{if $conf.dPplanningOp.COperation.use_poste}}halfPane{{else}}me-w100{{/if}}">
      <table class="tbl">
        <tr>
          <th>{{tr}}common-Operating bloc-court|pl{{/tr}}</th>
        </tr>
        <tr>
          <td>
            <div class="me-display-flex me-flex-column" style="gap: 8px">
              {{foreach from=$blocs_list item=_bloc}}
              <label>
                <input
                  type="checkbox"
                  class="bloc_id"
                  name="bloc_ids[{{$_bloc->_id}}]"
                  {{if in_array($_bloc->_id, $pref_bloc_ids)}}checked{{/if}}
                  value="{{$_bloc->_id}}"
                  onchange="SSPI.saveSelectBlocsPref($$('input.bloc_id:checked').pluck('value'){{if $conf.dPplanningOp.COperation.use_poste}}, refreshSelectSSPI.curry(true){{/if}})"
                />
                <span class="me-margin-left-8">{{$_bloc}}</span>
              </label>
              {{/foreach}}
            </div>
          </td>
        </tr>
      </table>
  </div>

  {{if $conf.dPplanningOp.COperation.use_poste}}
    <div id="sspi_filter" class="me-padding-8 halfPane"></div>
  {{/if}}
</div>
