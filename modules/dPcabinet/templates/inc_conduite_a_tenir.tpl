{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{if $dossier->_niveau_alerte_cesar || $dossier->_rques_conduite_a_tenir || $dossier->_conduite_a_tenir_acc}}
  <div class="me-float-right me-w33
              {{if $dossier->_niveau_alerte_cesar == 1}}
                small-info" style="background-color: lightgreen"
                  {{elseif $dossier->_niveau_alerte_cesar == 2}}
                    small-warning"
                  {{elseif $dossier->_niveau_alerte_cesar == 3}}
                    small-error"
                  {{else}}
                    small-info"
                  {{/if}}
  >
    {{if $dossier->_conduite_a_tenir_acc}}
        {{mb_value object=$dossier field=_conduite_a_tenir_acc}}
      <br/>
    {{/if}}
    {{if $dossier->_niveau_alerte_cesar}}
        {{mb_value object=$dossier field=_niveau_alerte_cesar}}
    {{/if}}
    {{if $dossier->_rques_conduite_a_tenir}}
        {{if !$dossier->_niveau_alerte_cesar && !$dossier->_conduite_a_tenir_acc}}{{tr}}CDossierPerinat-_rques_conduite_a_tenir{{/tr}} : {{else}}<br />{{/if}}<span title="{{$dossier->_rques_conduite_a_tenir}}">{{$dossier->_rques_conduite_a_tenir|truncate:60:"..."}}</span>
    {{/if}}
  </div>
{{/if}}
