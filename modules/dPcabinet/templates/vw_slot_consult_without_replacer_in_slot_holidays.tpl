{{*
 * @package Mediboard\dPcabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{unique_id var=editplage_id}}

<table class="main tbl">
    <thead>
        <tr>
            <th>{{tr}}CMediusers-back-plages_consult{{/tr}}</th>
            <th>{{mb_title class=CPlageconsult field=remplacant_id}}</th>
            <th>{{mb_title class=CPlageconsult field=pct_retrocession}}</th>
        </tr>
    </thead>
    <tbody>
        {{foreach from=$plage_consult item=plage}}
        <tr id="plage-{{$plage->_id}}">
            <td>
                <span>{{$plage->date|date_format:$conf.date}} : {{$plage->debut|date_format:$conf.time}} - {{$plage->fin|date_format:$conf.time}}</span>
            </td>
            <td>
                <select name="remplacant_id" class="{{$plage->_specs.remplacant_id}}">
                    <option value="">&mdash; {{tr}}Choose{{/tr}}</option>
                    {{mb_include module=mediusers template=inc_options_mediuser list=$replacers selected=$plage_conge->replacer_id}}
                </select>
            </td>
            <td>
                {{mb_field object=$plage_conge field=pct_retrocession size="3" increment=true form="editplage-$editplage_id"}}
            </td>
        </tr>
        {{/foreach}}
        <tr>
            <td class="button" colspan="3">
                <button class="submit" type="button" onclick="PlageConsultation.saveModifPlageWithReplacement({{$user_id}}, this)">{{tr}}Save{{/tr}}</button>
            </td>
        </tr>
    </tbody>
</table>
