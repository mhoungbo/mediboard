<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Exceptions\Repositories\Api;

use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Mediboard\Cabinet\Repositories\Api\ConsultationRepository;

/**
 * Exception class for ConsultationRepository
 * @see ConsultationRepository
 */
class ConsultationRepositoryException extends CMbException
{
    public static function noIdAvailable(CMbObject $object): self
    {
        return new self('ConsultationRepositoryException-No ID available for object: %s.', $object->_class);
    }
}
