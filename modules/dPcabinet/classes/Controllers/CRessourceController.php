<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Controllers;

use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Exceptions\ApiRequestException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Cabinet\CRessourceCab;
use Ox\Core\Api\Resources\Collection;
use Symfony\Component\HttpFoundation\Response;

class CRessourceController extends Controller
{
    /**
     * @param RequestApi $request_api
     *
     * @throws ApiRequestException
     * @throws ApiException
     * @throws \Exception
     * @api
     */
    public function listRessourcesCab(RequestApi $request_api): Response
    {
        $ressource_cab = new CRessourceCab();

        $ressources_cab = $ressource_cab->loadListFromRequestApi($request_api);

        CStoredObject::filterByPerm($ressources_cab);

        $collection = Collection::createFromRequest($request_api, $ressources_cab);

        return $this->renderApiResponse($collection);
    }
}
