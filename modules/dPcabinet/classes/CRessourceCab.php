<?php

/**
 * @package Mediboard\Cabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet;

use Ox\Core\Api\Resources\Collection;
use Ox\Core\CMbObject;
use Ox\Mediboard\Mediusers\CMediusers;

/**
 * Description
 */
class CRessourceCab extends CMbObject
{
    /** @var string */
    public const RESOURCE_TYPE = 'ressourceCab';

    /** @var string */
    public const RELATION_PLAGE_RESSOURCE_CAB = 'plageRessourceCab';

    /** @var string */
    public const FIELDSET_DEFAULT = 'default';

    /** @var string */
    public const FIELDSET_DETAILS = 'details';

    /** @var integer Primary key */
    public $ressource_cab_id;

    // DB fields
    /** @var integer */
    public $function_id;
    /** @var integer */
    public $owner_id;
    /** @var string */
    public $libelle;
    /** @var string */
    public $description;
    /** @var string */
    public $color;
    /** @var boolean */
    public $actif;
    /** @var string */
    public $in_charge;

    // Form fields
    /** @var CPlageRessourceCab */
    public $_ref_plages;

    /** @var CMediusers */
    public $_ref_owner;

    /**
     * @inheritdoc
     */
    public function getSpec()
    {
        $spec = parent::getSpec();
        $spec->table = "ressource_cab";
        $spec->key   = "ressource_cab_id";
        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps()
    {
        $props = parent::getProps();
        $props["function_id"] = "ref class|CFunctions notNull back|ressources fieldset|default";
        $props["owner_id"]    = "ref class|CMediusers notNull back|ressources fieldset|default";
        $props["libelle"]     = "str fieldset|details";
        $props["description"] = "text fieldset|details";
        $props["color"]       = "color fieldset|details";
        $props["actif"]       = "bool default|1";
        $props["in_charge"]   = "str";
        return $props;
    }

    /**
     * @inheritdoc
     */
    public function updateFormFields()
    {
        parent::updateFormFields();

        $this->_view = $this->libelle;
    }

    /**
     * Charge les plages associ�es � la ressource
     *
     * @return CPlageRessourceCab[]
     * @throws \Exception
     */
    public function loadRefsPlages(): array
    {
        return $this->_ref_plages = $this->loadBackRefs("plages_cab", "date");
    }

    /**
     * Charge les plages associ�es � la ressource
     *
     * @return CMediusers
     * @throws \Exception
     */
    public function loadRefOwner(): CMediusers
    {
        return $this->_ref_owner = $this->loadFwdRef("owner_id", true);
    }

    /**
     * @return Collection|array
     * @throws \Ox\Core\Api\Exceptions\ApiException
     * @throws \Exception
     */
    public function getResourcePlageRessourceCab(): Collection|array
    {
        $plages = $this->loadRefsPlages();
        if (!$plages) {
            return [];
        }
        return new Collection($plages);
    }
}
