<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Repositories\Api;

use DateTimeInterface;
use Exception;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\Exceptions\Repositories\Api\ConsultationRepositoryException;
use Ox\Mediboard\Patients\CPatient;

/**
 * Repository of CConsultation.
 *
 * This class does not override ::find unlike the ConsultationRepository (see below) class
 * and which is problematic in some cases.
 *
 * @see \Ox\Mediboard\Cabinet\Repositories\ConsultationRepository
 */
class ConsultationRepository extends AbstractRequestApiRepository
{
    /**
     * Add condition 'patient_id' for request.
     *
     * @throws ConsultationRepositoryException
     */
    public function withPatient(CPatient $patient): self
    {
        if ($patient->_id) {
            $this->where['consultation.patient_id'] = $this->object->getDS()->prepare('= ?', $patient->_id);

            return $this;
        } else {
            throw ConsultationRepositoryException::noIdAvailable($patient);
        }
    }

    /**
     * Find the consultations that are greater than or equal to the datetime parameter.
     *
     * @throws Exception
     */
    public function findWithGreaterThanOrEqualDate(DateTimeInterface $datetime): array
    {
        $consultations = parent::find();
        $this->massLoadConsultationRange($consultations);

        foreach ($consultations as $consultation) {
            $consultation->loadRefPlageConsult();

            if ($consultation->_datetime <= $datetime->format('Y-m-d H:i:s')) {
                unset($consultations[$consultation->_id]);
            }
        }

        return $consultations;
    }

    /**
     * Find the consultations that are less than or equal to the datetime parameter.
     *
     * @throws Exception
     */
    public function findWithLessThanOrEqualDate(DateTimeInterface $datetime): array
    {
        $consultations = parent::find();
        $this->massLoadConsultationRange($consultations);

        foreach ($consultations as $consultation) {
            $consultation->loadRefPlageConsult();

            if ($consultation->_datetime >= $datetime->format('Y-m-d H:i:s')) {
                unset($consultations[$consultation->_id]);
            }
        }

        return $consultations;
    }

    /**
     * @inheritDoc
     */
    protected function getObjectInstance(): CStoredObject
    {
        return new CConsultation();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    protected function massLoadRelation(array $objects, string $relation): void
    {
        switch ($relation) {
            case RequestRelations::QUERY_KEYWORD_ALL:
                $this->massLoadFiles($objects);
                $this->massLoadMedicalReports($objects);
                $this->massLoadPatient($objects);
                $this->massLoadConsultationRange($objects);
                $this->massLoadPrescriptions($objects);
                break;
            case CConsultation::RELATION_FILES:
                $this->massLoadFiles($objects);

                break;
            case CConsultation::RELATION_MEDICAL_REPORTS:
                $this->massLoadMedicalReports($objects);

                break;
            case CConsultation::RELATION_PATIENT:
                $this->massLoadPatient($objects);

                break;
            case CConsultation::RELATION_PLAGE_CONSULT:
                $this->massLoadConsultationRange($objects);

                break;
            case CConsultation::RELATION_MEDICAMENT_LINES:
                $this->massLoadPrescriptions($objects);

                break;
            default:
                // Do nothing.
        }
    }

    /**
     * MassLoad patient.
     *
     * @throws Exception
     */
    private function massLoadPatient(array $objects): void
    {
        CStoredObject::massLoadFwdRef($objects, 'patient_id');
    }

    /**
     * MassLoad consultation range.
     *
     * @throws Exception
     */
    public function massLoadConsultationRange(array $objects): void
    {
        CStoredObject::massLoadFwdRef($objects, 'plageconsult_id');
    }

    /**
     * MassLoad files.
     *
     * @throws Exception
     */
    private function massLoadFiles(array $objects): void
    {
        CStoredObject::massLoadBackRefs($objects, 'files');
    }

    /**
     * MassLoad medical reports.
     *
     * @throws Exception
     */
    private function massLoadMedicalReports(array $objects): void
    {
        CStoredObject::massLoadBackRefs($objects, 'documents');
    }

    /**
     * MassLoad prescriptions.
     *
     * @throws Exception
     */
    private function massLoadPrescriptions(array $objects): void
    {
        CStoredObject::massLoadBackRefs($objects, 'prescriptions');
    }
}
