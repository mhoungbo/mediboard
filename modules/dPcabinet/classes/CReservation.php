<?php

/**
 * @package Mediboard\Cabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet;

use DateTime;
use Exception;
use Ox\Core\CMbObject;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Patients\CPatient;

/**
 * Description
 */
class CReservation extends CMbObject
{
    /** @var string */
    public const RESOURCE_TYPE = 'reservation';

    /** @var string */
    public const FIELDSET_DEFAULT = 'default';

    /** @var string */
    public const FIELDSET_DETAILS = 'details';

    /** @var string */
    public const FIELDSET_TIME = 'time';

    /** @var int Primary key */
    public $reservation_id;

    // DB fields
    /** @var int */
    public $plage_ressource_cab_id;
    /** @var int */
    public $patient_id;
    /** @var string */
    public $date;
    /** @var string */
    public $heure;
    /** @var int */
    public $duree;
    /** @var string */
    public $motif;

    // References
    /** @var CPatient */
    public $_ref_patient;
    /** @var CPlageRessourceCab */
    public $_ref_plage_ressource;

    /**
     * @inheritdoc
     */
    public function getSpec()
    {
        $spec        = parent::getSpec();
        $spec->table = "reservation";
        $spec->key   = "reservation_id";

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps()
    {
        $props                           = parent::getProps();
        $props["plage_ressource_cab_id"] = "ref class|CPlageRessourceCab notNull back|reservations fieldset|default";
        $props["patient_id"]             = "ref class|CPatient back|reservations fieldset|default";
        $props["date"]                   = "date notNull fieldset|time";
        $props["heure"]                  = "time notNull fieldset|time";
        $props["duree"]                  = "num min|1 notNull fieldset|time";
        $props["motif"]                  = "text fieldset|details";

        return $props;
    }

    /**
     * Chargement de la consultation
     *
     * @return CStoredObject|CPatient
     * @throws Exception
     */
    public function loadRefPatient(): CPatient
    {
        return $this->_ref_patient = $this->loadFwdRef("patient_id", true);
    }

    /**
     * @return CStoredObject|CPlageRessourceCab
     * @throws Exception
     */
    public function loadRefPlageRessource(): CPlageRessourceCab
    {
        return $this->_ref_plage_ressource = $this->loadFwdRef("plage_ressource_cab_id", true);
    }
}
