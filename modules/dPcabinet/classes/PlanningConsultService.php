<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Bloc\CPlageOp;
use Ox\Mediboard\Ccam\CCodable;
use Ox\Mediboard\Facturation\CFactureCabinet;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Personnel\CPlageConge;
use Ox\Mediboard\PlanningOp\COperation;

/**
 * Permet de construire un tableau par date des consultations, plage de conges, de consult... pour l'affichage du
 * planning
 */
class PlanningConsultService extends PlanningConsultSlotService
{
    /** @var array */
    private $contents_by_date;
    /** @var array */
    private $holidays;
    /** @var CMediusers */
    private $chir;
    /** @var bool */
    private $actes;
    /** @var int|null */
    private $line_element_id;

    /** @var string */
    private $date_begin;
    /** @var string */
    private $date_end;

    /**
     * @param int|string $chir_id
     * @param array|string $chrono
     */
    public function __construct(
        string $date_debut,
        string $date_fin,
               $chir_id,
               $chrono = null,
        string $facture = null,
        string $actes = null,
        string $consult_cancelled = null,
        string $line_element_id = ''
    ) {
        $this->chir              = CMediusers::get($chir_id);
        if (!is_array($chrono)) {
            $this->chrono            = $chrono == '' ? null : [$chrono];
        } else {
            $this->chrono            = !count($chrono) ? null : $chrono;
        }
        $this->facture           = $facture == '' ? null : $facture;
        $this->actes             = $actes == '' ? null : $actes;
        $this->consult_cancelled = $consult_cancelled == '' ? null : $consult_cancelled;
        $this->line_element_id   = $line_element_id == '' ? null : $line_element_id;

        $this->holidays         = array_merge(CMbDT::getHolidays($date_debut), CMbDT::getHolidays($date_fin));
        $date                   = $date_debut;
        $this->contents_by_date = [];
        $this->date_begin = $date_debut;
        $this->date_end = $date_fin;

        if ($date == $date_fin) {
            $this->createArrayContentsByDate($date);
        } else {
            while ($date <= $date_fin) {
                $this->createArrayContentsByDate($date);

                $date = CMbDT::date('+1 day', $date);
            }
        }

        $this->createContentsByDate();
    }

    /**
     * @param string $date
     *
     * @return void
     */
    private function createArrayContentsByDate(string $date): void
    {
        $this->contents_by_date[$date] = [
            'plage_op'          => [],
            'interv_hors_plage' => [],
            'conges'            => [],
            'plage_consult'     => [],
            'consults'          => [],
        ];
    }

    /**
     * @return void
     */
    private function createContentsByDate(): void
    {
        $this->intervContents();
        $this->congesContents();
        $this->plageConsultContents();
    }

    /**
     * Charge les plages d'intervention et les interventions hors plages
     *
     * @return void
     * @throws Exception
     */
    private function intervContents(): void
    {
        /** @var CPlageOp[] $plage_ops */
        $plage_ops = (new CPlageOp())->loadList([
            'date' => "BETWEEN '{$this->date_begin}' AND '{$this->date_end}'",
            "chir_id {$this->chir->getUserSQLClause()} OR spec_id = '{$this->chir->function_id}'"
        ]);
        CStoredObject::massLoadFwdRef($plage_ops, 'chir_id');
        foreach ($plage_ops as $plage) {
            if (
                CAppUI::pref('showIntervPlanning')
                && (!array_key_exists($plage->date, $this->holidays) || CAppUI::pref('show_plage_holiday'))
            ) {
                $this->contents_by_date[$plage->date]['plage_op'][] = $plage;
            }
        }

        /** @var COperation[] $hors_plages */
        $hors_plages = (new COperation())->loadList([
            'date'       => "BETWEEN '{$this->date_begin}' AND '{$this->date_end}'",
            'plageop_id' => ' IS NULL',
            'chir_id'    => $this->chir->getUserSQLClause()
        ]);
        foreach ($hors_plages as $hors_plage) {
            if (
                CAppUI::pref('showIntervPlanning')
                && (!array_key_exists($hors_plage->date, $this->holidays) || CAppUI::pref('show_plage_holiday'))
            ) {
                $this->contents_by_date[$hors_plage->date]['interv_hors_plage'][] = $hors_plage;
            }
        }
    }

    /**
     * Charge les plages de cong�s
     *
     * @return void
     * @throws Exception
     */
    private function congesContents(): void
    {
        if (CModule::getActive('dPpersonnel')) {
            /** @var CPlageconge[] $conges */
            $conges = (new CPlageConge())->loadList([
                "DATE(date_debut) <= '$this->date_end'",
                "DATE(date_fin) >= '$this->date_begin'",
                'user_id'    => "= '{$this->chir->_id}'",
            ]);

            CStoredObject::massLoadFwdRef($conges, 'replacer_id');
            foreach ($conges as $_conge) {
                $_conge->loadRefReplacer();

                for ($date = $this->date_begin; $date <= $this->date_end; $date = CMbDT::date('+1 day', $date)) {
                    if ($date >= CMbDT::date($_conge->date_debut) && $date <= CMbDT::date($_conge->date_fin)) {
                        $this->contents_by_date[$date]['conges'][] = $_conge;
                    }
                }
            }
        }
    }

    private function plageConsultContents(): void
    {
        $where         = [
            'date' => "BETWEEN '{$this->date_begin}' AND '{$this->date_end}'",
            "chir_id {$this->chir->getUserSQLClause()} OR remplacant_id {$this->chir->getUserSQLClause()}",
        ];

        $libelles      = CPlageconsult::getLibellesPref();
        if (count($libelles)) {
            $where['libelle'] = CSQLDataSource::prepareIn($libelles);
        }

        if ($this->line_element_id) {
            $where['pour_tiers'] = "= '1'";
        }

        /** @var CPlageConsult[] $plages */
        $plages = (new CPlageconsult())->loadList($where, 'date, debut');

        $this->massLoadPlages($plages);

        foreach ($plages as $_plage) {
            if ($_plage->libelle != 'automatique_suivi_patient') {
                $_plage->loadRefChir();
                $_plage->loadRefRemplacant();
                $_plage->loadRefPourCompte();
                $_plage->loadRefChir()->loadRefFunction();
                $_plage->loadRefAgendaPraticien();
                $_plage->colorPlanning($this->chir->_id);

                $this->slotContents($_plage, $_plage->date);

                $this->contents_by_date[$_plage->date]['plage_consult'][] = $_plage;
            }
        }
    }

    /**
     * @throws Exception
     */
    protected function consultContentsByDate(string $date, CConsultation $consult): void
    {
        $consult->loadRefsActes();

        if ($this->actes !== null) {
            if ($this->actes && !$consult->_count_actes) {
                return;
            }
            if (!$this->actes && $consult->_count_actes > 0) {
                return;
            }
        }

        $_consult_anesth = $consult->loadRefConsultAnesth();
        if ($_consult_anesth && $_consult_anesth->_id) {
            $consult->_alert_docs        += $_consult_anesth->_alert_docs;
            $consult->_locked_alert_docs += $_consult_anesth->_locked_alert_docs;
        }

        $consult->loadRefFacture();
        $consult->loadPosition();
        $consult->colorPlanning();
        $consult->loadRefPatient();

        if (CModule::getActive('teleconsultation')) {
            $consult->loadRefRoom();
        }

        $consult->loadRefReservedRessources();
        $consult->checkDHE();

        if (CModule::getActive('notifications')) {
            $consult->loadRefNotification();
        }

        $consult->loadRefAdresseParPraticien();
        $consult->loadRefCategorie();
        $consult->canDo();
        $consult->loadRefReunion();

        $plage_resource_id = CStoredObject::massLoadFwdRef(
            $consult->_ref_reserved_ressources,
            'plage_ressource_cab_id'
        );
        CStoredObject::massLoadFwdRef($plage_resource_id, 'ressource_cab_id');
        foreach ($consult->_ref_reserved_ressources as $_reserved) {
            $consult = $_reserved->loadRefPlageRessource()->loadRefRessource();
        }

        $this->contents_by_date[$date]['consults'][] = $consult;
    }

    public function getContentsByDate(): array
    {
        return $this->contents_by_date;
    }

    protected function consultContents(CConsultation $consult, int $plage_id): void
    {
    }

    /**
     * Make the necessary massloads for the CPlageconsult
     *
     * @param CPlageconsult[] $plages
     */
    protected function massLoadPlages(array $plages): void
    {
        $chirs_plages = CStoredObject::massLoadFwdRef($plages, 'chir_id');
        CStoredObject::massLoadFwdRef($chirs_plages, 'function_id');
        CStoredObject::massLoadFwdRef($plages, 'remplacant_id');
        CStoredObject::massLoadFwdRef($plages, 'pour_compte_id');
        CStoredObject::massLoadFwdRef($plages, 'agenda_praticien_id');
        $slots = CStoredObject::massLoadBackRefs($plages, 'slots');
        $consultations = CStoredObject::massLoadFwdRef($slots, 'consultation_id');
        CCodable::massLoadActes($consultations);
        $anesths = CStoredObject::massLoadBackRefs($consultations, 'consult_anesth');
        CStoredObject::massLoadFwdRef($anesths, 'chir_id');
        CStoredObject::massLoadFwdRef($consultations, 'patient_id');
        if (CModule::getActive('teleconsultation')) {
            CStoredObject::massLoadBackRefs($consultations, 'rooms');
        }
        CStoredObject::massLoadFwdRef($consultations, 'adresse_par_prat_id');
        CStoredObject::massLoadFwdRef($consultations, 'categorie_id');
        CStoredObject::massLoadFwdRef($consultations, 'reunion_id');
        if (CModule::getActive('notifications')) {
            $notifications = CStoredObject::massLoadBackRefs($consultations, 'context_notifications');
            CStoredObject::massLoadFwdRef($notifications, 'message_id');
        }
        CFactureCabinet::massLoadForConsultation($consultations);
        CMbObject::countAlertDocs($consultations);
        CMbObject::countAlertDocs($anesths);
        CMbObject::countLockedAlertDocs($consultations);
        CMbObject::countLockedAlertDocs($anesths);
    }
}
