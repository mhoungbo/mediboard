<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;

/**
 * Cr�neau dans une plage de consultation
 */
class CSlot extends CStoredObject
{
    /** @var string */
    public const RESOURCE_TYPE = 'slot';

    /** @var string */
    public const RELATION_CONSULTATIONS = 'consultation';

    /** @var string */
    public const RELATION_PLAGE = 'plage';

    /** @var string */
    public const FIELDSET_STATUS = 'status';

    /** @var string */
    public const FIELDSET_RELATIONS = 'relations';

    /** @var int Primary key */
    public $slot_id;

    /** @var int */
    public $plageconsult_id;
    /** @var int */
    public $consultation_id;
    /** @var string */
    public $start;
    /** @var string */
    public $end;
    /** @var bool */
    public $overbooked;
    /** @var string */
    public $status;

    /** @var CConsultation */
    public $_ref_consultation;
    /** @var CPlageconsult */
    public $_ref_plageconsult;

    /** @var string */
    public $_date;
    /** @var string */
    public $_heure;
    /** @var string */
    public $_nb_week;

    /**
     * @inheritdoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec = parent::getSpec();

        $spec->table    = 'slot';
        $spec->key      = 'slot_id';
        $spec->loggable = false;

        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props["plageconsult_id"] = "ref notNull class|CPlageconsult back|slots cascade fieldset|relations";
        $props["consultation_id"] = "ref class|CConsultation back|slots fieldset|relations";
        $props["start"]           = "dateTime notNull fieldset|default";
        $props["end"]             = "dateTime notNull fieldset|default";
        $props["overbooked"]      = "bool default|0 fieldset|status";
        $props["status"]          = "enum list|busy|free|busy-unavailable|busy-tentative|entered-in-error default|free fieldset|status";

        return $props;
    }

    /**
     * @throws Exception
     */
    public function loadRefConsultation(): CConsultation
    {
        return $this->_ref_consultation = $this->loadFwdRef("consultation_id", true);
    }

    /**
     * @throws Exception
     */
    public function loadRefPlageconsult(): CPlageconsult
    {
        return $this->_ref_plageconsult = $this->loadFwdRef("plageconsult_id", true);
    }

    /**
     * @return Item|null
     * @throws ApiException
     * @throws Exception
     */
    public function getResourceConsultation(): ?Item
    {
        if (!$consultations = $this->loadRefConsultation()) {
            return null;
        }

        return new Item($consultations);
    }

    /**
     * @return Item|null
     * @throws ApiException
     * @throws Exception
     */
    public function getResourcePlage(): ?Item
    {
        if (!$plage = $this->loadRefPlageconsult()) {
            return null;
        }

        return new Item($plage);
    }
}
