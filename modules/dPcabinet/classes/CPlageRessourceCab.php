<?php

/**
 * @package Mediboard\Cabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\CMbDT;
use Ox\Core\CPlageHoraire;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Api\Resources\Collection;

/**
 * Description
 */
class CPlageRessourceCab extends CPlageHoraire
{
    /** @var string */
    public const RESOURCE_TYPE = 'plageRessourceCab';

    /** @var string */
    public const RELATION_RESSOURCE_CAB = 'ressourceCab';

    /** @var string */
    public const RELATION_RESERVATION = 'reservation';

    /** @var string */
    public const FIELDSET_DEFAULT = 'default';

    /** @var string */
    public const FIELDSET_DETAILS = 'details';

    /** @var string */
    public const FIELDSET_TIME = 'time';

    /** @var int Primary key */
    public $plage_ressource_cab_id;

    // DB Fields
    /** @var int */
    public $ressource_cab_id;
    /** @var string */
    public $libelle;
    /** @var string */
    public $freq;
    /** @var string */
    public $color;

    // References
    /** @var CReservation[] */
    public $_ref_reservations = [];
    /** @var CRessourceCab */
    public $_ref_ressource;

    // Form fields
    /** @var string */
    public $_freq;
    /** @var int */
    public $_cumulative_minutes;
    /** @var int */
    public $_repeat;
    /** @var string */
    public $_type_repeat;
    /** @var int */
    public $_count_duplicated_plages;

    /**
     * @inheritdoc
     */
    public function getSpec()
    {
        $spec = parent::getSpec();
        $spec->table = "plage_ressource_cab";
        $spec->key = "plage_ressource_cab_id";
        $spec->collision_keys = array("ressource_cab_id");
        return $spec;
    }

    /**
     * @inheritdoc
     */
    public function getProps() {
        $props = parent::getProps();
        $props["ressource_cab_id"] = "ref class|CRessourceCab notNull back|plages_cab fieldset|default";
        $props["libelle"]      = "str fieldset|default";
        $props["freq"]         = "time notNull min|00:05:00 fieldset|time";
        $props["color"]        = "color fieldset|details";
        $props["date"]         = "date notNull fieldset|time";
        $props["debut"]        = "time notNull fieldset|time";
        $props["fin"]          = "time notNull fieldset|time";
        $props["_freq"]        = "";
        $props["_repeat"]      = "";
        $props["_type_repeat"] = "enum list|simple|double|triple|quadruple|quintuple|sextuple|septuple|octuple|sameweek";
        return $props;
    }

    /**
     * @inheritdoc
     */
    public function updateFormFields()
    {
        parent::updateFormFields();

        $this->_view = $this->libelle;

        if ($this->freq == "1:00:00" || $this->freq == "01:00:00") {
            $this->_freq = "60";
        } else {
            $this->_freq         = substr($this->freq, 3, 2);
            $this->_freq_minutes = CMbDT::minutesRelative("00:00:00", $this->freq);
        }
    }

    /**
     * @inheritdoc
     */
    public function updatePlainFields()
    {
        parent::updateFormFields();

        $this->completeField("freq");
        if ($this->_freq !== null) {
            if ($this->_freq == "60") {
                $this->freq = "01:00:00";
            } else {
                $this->freq = sprintf("00:%02d:00", $this->_freq);
            }
        }
    }

    /**
     * Chargement des r�servations
     *
     * @return CReservation[]
     * @throws Exception
     */
    public function loadRefsReservations()
    {
        return $this->_ref_reservations = $this->loadBackRefs("reservations", "heure");
    }

    /**
     * @return CRessourceCab
     * @throws Exception
     */
    public function loadRefRessource()
    {
        return $this->_ref_ressource = $this->loadFwdRef("ressource_cab_id", true);
    }

    public function countDuplicatedPlages()
    {
        $where = array(
            'ressource_cab_id' => "= $this->ressource_cab_id",
            'freq'             => "= '$this->freq'",
            'debut'            => "= '$this->debut'",
            'fin'              => "= '$this->fin'",
            'date'             => "> '$this->date'",
            "WEEKDAY(`date`) = WEEKDAY('$this->date')"
        );

        return $this->_count_duplicated_plages = $this->countList($where);
    }

    public function becomeNext()
    {
        $week_jumped = 0;

        $mapping_repeat = array(
            "octuple"   => 8,
            "septuple"  => 7,
            "sextuple"  => 6,
            "quintuple" => 5,
            "quadruple" => 4,
            "triple"    => 3,
            "double"    => 2,
            "simple"    => 1
        );

        switch ($this->_type_repeat) {
            case "octuple":
            case "septuple":
            case "sextuple":
            case "quintuple":
            case "quadruple":
            case "triple":
            case "double":
            case "simple":
                $ratio = $mapping_repeat[$this->_type_repeat];
                $this->date = CMbDT::date("+$ratio WEEK", $this->date);
                $week_jumped += $ratio;
                break;
            case "sameweek":
                $week_number = CMbDT::weekNumberInMonth($this->date);
                $next_month  = CMbDT::monthNumber(CMbDT::date("+1 MONTH", $this->date));
                $i           = 0;
                do {
                    $this->date = CMbDT::date("+1 WEEK", $this->date);
                    $week_jumped++;
                    $i++;
                } while (
                    $i < 10 &&
                    (CMbDT::monthNumber($this->date) < $next_month) ||
                    (CMbDT::weekNumberInMonth($this->date) != $week_number)
                );
                break;
            default:
                return ++$week_jumped;
        }

        // Stockage des champs modifi�s
        $debut   = $this->debut;
        $fin     = $this->fin;
        $freq    = $this->freq;
        $libelle = $this->libelle;
        $color   = $this->color;

        // Recherche de la plage suivante
        $where["date"]    = "= '$this->date'";
        $where["ressource_cab_id"] = "= '$this->ressource_cab_id'";
        $where[]          = "`debut` = '$this->debut' OR `fin` = '$this->fin'";
        if (!$this->loadObject($where)) {
            $this->plage_ressource_cab_id = null;
        }

        // Remise en place des champs modifi�s
        $this->debut   = $debut;
        $this->fin     = $fin;
        $this->freq    = $freq;
        $this->libelle = $libelle;

        $this->updateFormFields();

        return $week_jumped;
    }

    public function getUtilisation()
    {
        $utilisation = array();
        $old = $this->debut;

        for ($i = $this->debut; $i < $this->fin; $i = CMbDT::addTime("+" . $this->freq, $i)) {
            if ($old > $i) {
                break;
            }
            $utilisation[$i] = 0;
            $old = $i;
        }

        foreach ($this->_ref_reservations as $_reservation) {
            if (!isset($utilisation[$_reservation->heure])) {
                continue;
            }
            $emplacement = $_reservation->heure;
            for ($i = 0; $i < $_reservation->duree; $i++) {
                if (isset($utilisation[$emplacement])) {
                    $utilisation[$emplacement]++;
                }
                $emplacement = CMbDT::addTime("+" . $this->freq, $emplacement);
            }
        }

        return $utilisation;
    }

    /**
     * @throws Exception
     */
    public function getResourceRessourceCab(): ?Item
    {
        $ressource = $this->loadRefRessource();
        if (!$ressource || !$ressource->_id) {
            return null;
        }

        return new Item($ressource);
    }

    /**
     * @throws ApiException
     * @throws Exception
     */
    public function getResourceReservation(): Collection|array
    {
        $reservation = $this->loadRefsReservations();
        if (!$reservation) {
            return [];
        }

        return new Collection($reservation);
    }
}
