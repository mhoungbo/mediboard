<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet;

use DateInterval;
use DateTimeImmutable;
use Exception;
use Ox\Core\CMbDT;

abstract class PlanningConsultSlotService
{
    /** @var bool */
    public $consult_cancelled = false;
    /** @var array */
    public $chrono = null;
    /** @var bool */
    public $facture = null;

    /**
     * Récupération des slots d'une plage de consultation
     *
     * @throws Exception
     */
    protected function slotContents(CPlageconsult $plage, string $date = null): void
    {
        if (!$plage->countBackRefs("slots")) {
            //"Création" des slots
            $heures_debut  = [];
            $consultations = $plage->loadRefsConsultations($this->consult_cancelled, true, true, $this->chrono);
            foreach ($consultations as $_consultation) {
                $heure_debut = $_consultation->heure;
                $heure_fin   = CMbDT::addTime($_consultation->heure, $plage->freq);
                for ($i = 1; $i <= $_consultation->duree; $i++) {
                    $heures_debut[] = $heure_debut;
                    $heure_debut    = $heure_fin;
                    $heure_fin      = CMbDT::addTime($heure_debut, $plage->freq);
                }
                if ($date) {
                    $this->consultContentsByDate($date, $_consultation);
                } else {
                    $this->consultContents($_consultation, $plage->_id);
                }
            }


            $current = new DateTimeImmutable("$plage->date $plage->debut");
            $end   = new DateTimeImmutable("$plage->date $plage->fin");
            $interval = new DateInterval(sprintf('PT%dH%dM%dS', ...sscanf($plage->freq, '%d:%d:%d')));

            while ($current < $end) {
                $end_slot = $current->add($interval);
                $slot                  = new CSlot();
                $slot->plageconsult_id = $plage->_id;

                $slot->start           = $current->format('Y-m-d H:i:s');
                $slot->end             = $end_slot->format('Y-m-d H:i:s');

                if (in_array($current->format('H:i:s'), $heures_debut)) {
                    $slot->status = "busy";
                } else {
                    $slot->status = "free";
                }
                $plage->_ref_slots[] = $slot;

                $current = $end_slot;
            }
        } else {
            //Récupération des slots
            $consult_ids = [];
            $slots       = $plage->loadRefsSlots("start ASC");

            if ($this->consult_cancelled) {
                $where                  = ["plageconsult_id" => "= '$plage->_id'", "annule" => "= '1'"];
                $consultation_annulee   = new CConsultation();
                $consultations_annulees = $consultation_annulee->loadList($where);
                foreach ($consultations_annulees as $_consultation_annulee) {
                    $this->consultContentsByDate($date, $_consultation_annulee);
                }
            }

            foreach ($slots as $_slot) {
                if ($_slot->consultation_id && !in_array($_slot->consultation_id, $consult_ids)) {
                    $consult_ids[] = $_slot->consultation_id;
                    $_slot->loadRefConsultation();
                    if (
                        ($this->chrono === null || in_array($_slot->_ref_consultation->chrono, $this->chrono))
                        && ($this->facture === null || $_slot->_ref_consultation->facture == $this->facture)
                    ) {
                        if ($_slot->_ref_consultation->type_consultation == 'consultation') {
                            if ($date) {
                                $this->consultContentsByDate($date, $_slot->_ref_consultation);
                            } else {
                                $this->consultContents($_slot->_ref_consultation, $plage->_id);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Make the necessary massloads for the CPlageconsult
     *
     * @param CPlageconsult[] $plages
     */
    abstract protected function massLoadPlages(array $plages): void;

    abstract protected function consultContentsByDate(string $date, CConsultation $consult): void;

    abstract protected function consultContents(CConsultation $consult, int $plage_id): void;
}
