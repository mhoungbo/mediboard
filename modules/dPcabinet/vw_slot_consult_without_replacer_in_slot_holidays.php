<?php
/**
 * @package Mediboard\Cabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CMbDT;
use Ox\Core\CMbRange;
use Ox\Core\CSmartyDP;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Personnel\CPlageConge;

CCanDo::checkRead();

$user_id        = CView::get("user_id", "ref class|CMediusers");
$plage_conge_id = CView::get("plage_conge_id", "ref class|CPlageConge");

CView::checkin();

$user   = CMediusers::get($user_id);
$plage_conge = new CPlageConge();
$plage_conge->load($plage_conge_id);

$ds = CSQLDataSource::get('std');
$date_day = CMbDT::date();
$date_start_plage = CMbDT::date($plage_conge->date_debut);
$date_start = ($date_day > $date_start_plage ? $date_day : $date_start_plage);
$time_start = ($date_day > $date_start_plage ? CMbDT::time() : CMbDT::time($plage_conge->date_debut));

$user->loadBackRefPlageConsult([
    'date'  => $ds->prepareBetween($date_start, CMbDT::date($plage_conge->date_fin)),
    'remplacant_id' => $ds->prepare('IS NULL')
]);

foreach ($user->_ref_plages_consult as $key => $plageconsult) {
    $start_plage = $plageconsult->date . " " . $plageconsult->debut;
    $end_plage   = $plageconsult->date . " " . $plageconsult->fin;
    if (!CMbRange::collides($plage_conge->date_debut, $plage_conge->date_fin, $start_plage, $end_plage)) {
        unset($user->_ref_plages_consult[$key]);
    }
}
// Liste des chirurgiens
$mediusers = new CMediusers();
$replacers = $mediusers->loadProfessionnelDeSanteByPref(PERM_EDIT);

$smarty = new CSmartyDP();
$smarty->assign("user_id", $user_id);
$smarty->assign("plage_consult", $user->_ref_plages_consult);
$smarty->assign("plage_conge", $plage_conge);
$smarty->assign("replacers", $replacers);
$smarty->display("vw_slot_consult_without_replacer_in_slot_holidays.tpl");
