<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Tests\Unit\Repositories\Api;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\Exceptions\Repositories\Api\ConsultationRepositoryException;
use Ox\Mediboard\Cabinet\Tests\Fixtures\ConsultationFixtures;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\OxUnitTestCase;
use Ox\Mediboard\Cabinet\Repositories\Api\ConsultationRepository;
use Ox\Tests\TestsException;

class ConsultationRepositoryTest extends OxUnitTestCase
{
    /**
     * Test: Check that the requested patient's consultations are retrieved.
     *
     * @throws ConsultationRepositoryException
     * @throws TestsException
     * @throws Exception
     */
    public function testWithPatient(): void
    {
        $patient = $this->getPatient();

        $consultation_repository = new ConsultationRepository();
        $consultations           = $consultation_repository->withPatient($patient)
                                                           ->find();

        /** @var CConsultation $consultation */
        foreach ($consultations as $consultation) {
            // Asserting: Consultation is linked to the requested patient.
            $this->assertEquals($consultation->patient_id, $patient->_id);
        }
    }

    /**
     * Test: Checks that the withPatient cannot be used on a patient without identifier.
     *
     * @throws ConsultationRepositoryException
     * @throws TestsException
     * @throws Exception
     */
    public function testWithPatientException(): void
    {
        $patient      = $this->getPatient();
        $patient_id   = $patient->_id;

        try {
            $patient->_id = null;

            $this->expectException(ConsultationRepositoryException::class);

            $consultation_repository = new ConsultationRepository();
            $consultation_repository->withPatient($patient)
                                    ->find();
        } finally {
            $patient->_id = $patient_id;
        }
    }

    /**
     * Test: Check that the only next consultations for the date indicated are retrieved.
     *
     * @throws ConsultationRepositoryException
     * @throws TestsException
     * @throws Exception
     * @dataProvider dataProviderFindWithGreaterLessThanOrEqualDate
     */
    public function testFindWithGreaterThanOrEqualDate(DateTimeInterface $datetime): void
    {
        $patient = $this->getPatient();

        $consultation_repository = new ConsultationRepository();
        $consultations           = $consultation_repository->withPatient($patient)
                                                           ->findWithGreaterThanOrEqualDate($datetime);

        /** @var CConsultation $consultation */
        foreach ($consultations as $consultation) {
            // Asserting: Consultation datetime is greater than or equal to $datetime.
            $this->assertTrue(($consultation->_datetime >= $datetime->format('Y-m-d H:i:s')));
        }
    }

    /**
     * Test: Check that the only previous consultations of the indicated date are retrieved.
     *
     * @throws TestsException
     * @throws ConsultationRepositoryException
     * @throws Exception
     * @dataProvider dataProviderFindWithGreaterLessThanOrEqualDate
     */
    public function testFindWithLessThanOrEqualDate(DateTimeInterface $datetime): void
    {
        $patient = $this->getPatient();

        $consultation_repository = new ConsultationRepository();
        $consultations           = $consultation_repository->withPatient($patient)
                                                           ->findWithLessThanOrEqualDate($datetime);

        /** @var CConsultation $consultation */
        foreach ($consultations as $consultation) {
            // Asserting: Consultation datetime is less than or equal to $datetime.
            $this->assertTrue(($consultation->_datetime <= $datetime->format('Y-m-d H:i:s')));
        }
    }

    public function dataProviderFindWithGreaterLessThanOrEqualDate(): array
    {
        return [
            'nowDate'       => [(new DateTimeImmutable())->setTime(8, 0)],
            'yesterdayDate' => [(new DateTimeImmutable())->modify('-1 DAY')->setTime(14, 0)],
            'tomorrowDate'  => [(new DateTimeImmutable())->modify('+1 DAY')->setTime(9, 0)],
        ];
    }

    /**
     * Get patient.
     *
     * @throws TestsException
     */
    private function getPatient(): CPatient
    {
        /** @var CPatient */
        return $this->getObjectFromFixturesReference(
            CPatient::class,
            ConsultationFixtures::TAG_CONSULTATION_PATIENT
        );
    }
}
