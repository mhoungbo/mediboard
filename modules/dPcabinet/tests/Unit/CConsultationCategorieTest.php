<?php

/**
 * @package Mediboard\Cabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace Ox\Mediboard\Cabinet\Tests\Unit;

use Exception;
use Ox\Core\CMbArray;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Cabinet\CConsultationCategorie;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;

/**
 * Tests for CConsultationCategorie class
 */
class CConsultationCategorieTest extends OxUnitTestCase
{
    /**
     * @throws TestsException
     * @throws CModelObjectException
     * @throws Exception
     */
    public function testLoadForMediuserReturnsPratCategories()
    {
        /** @var CMediusers $praticien */
        $praticien = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_MEDECIN);

        $cat_praticien = CConsultationCategorie::getSampleObject();

        $cat_praticien->praticien_id = $praticien->_id;
        $cat_praticien->store();

        $actual  = (new CConsultationCategorie())->loadForMediuser($praticien);
        $cat_ids = CMbArray::flip(CMbArray::pluck($actual, "_id"));

        $this->assertArrayHasKey($cat_praticien->_id, $cat_ids);

        $cat_praticien->delete();
    }

    /**
     * @throws TestsException
     * @throws CModelObjectException
     * @throws Exception
     */
    public function testLoadForMediuserReturnsFunctionCategories()
    {
        /** @var CMediusers $praticien */
        $praticien = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_MEDECIN);
        $function  = $this->getObjectFromFixturesReference(CFunctions::class, UsersFixtures::REF_FIXTURES_FUNCTION);

        $cat_function = CConsultationCategorie::getSampleObject();

        $cat_function->function_id = $function->_id;
        $cat_function->store();

        $actual  = (new CConsultationCategorie())->loadForMediuser($praticien);
        $cat_ids = CMbArray::flip(CMbArray::pluck($actual, "_id"));

        $this->assertArrayHasKey($cat_function->_id, $cat_ids);

        $cat_function->delete();
    }
}
