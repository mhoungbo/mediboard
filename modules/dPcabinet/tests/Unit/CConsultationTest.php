<?php

/**
 * @package Mediboard\Cabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace Ox\Mediboard\Cabinet\Tests\Unit;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CModelObject;
use Ox\Mediboard\Cabinet\CActeNGAP;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Cabinet\Tests\Fixtures\CabinetFixtures;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\Tests\Fixtures\SimplePatientFixtures;
use Ox\Tests\OxUnitTestCase;

/**
 * Tests for CConsultation class
 */
class CConsultationTest extends OxUnitTestCase
{
    /**
     * @param CConsultation $consultation
     *
     * @dataProvider plageConsultOwnerProvider
     */
    public function testLoadRefPraticienReturnPlageConsultOwner(CConsultation $consultation): void
    {
        $consultation->loadRefPraticien();
        $this->assertEquals($consultation->_ref_praticien->_id, $consultation->_ref_plageconsult->chir_id);
    }

    /**
     * @throws Exception
     */
    public function plageConsultOwnerProvider(): array
    {
        /** @var CConsultation $consult */
        $consult = $this->getObjectFromFixturesReference(CConsultation::class, CabinetFixtures::TAG_CONSULT_PRAT);
        /** @var CConsultation $consult_rempl */
        $consult_rempl = $this->getObjectFromFixturesReference(
            CConsultation::class,
            CabinetFixtures::TAG_CONSULT_REMPL
        );

        return [
            "consult normale"           => [$consult],
            "consult avec remplacement" => [$consult_rempl],
        ];
    }

    /**
     * @dataProvider cancelConsultationProvider
     *
     * @param CConsultation $consultation
     * @param string|null   $expected_result
     *
     * @return void
     */
    public function testCancelConsultation(CConsultation $consultation, ?string $expected_result): void
    {
        $consultation->annule = '1';
        $this->assertEquals($expected_result, $consultation->store());
    }

    public function cancelConsultationProvider(): array
    {
        /** @var CPlageconsult $plage */
        $plage = $this->getObjectFromFixturesReference(CPlageconsult::class, CabinetFixtures::TAG_PLAGE_MEDECIN_GENERALISTE);
        $patient_prime = $this->getObjectFromFixturesReference(CPatient::class, SimplePatientFixtures::SAMPLE_PATIENT);
        $patient_bis = $this->getObjectFromFixturesReference(CPatient::class, SimplePatientFixtures::SAMPLE_PATIENT_BIS);
        $slots = $plage->getEmptySlots();
        $slot = reset($slots);

        $consultation_success = new CConsultation();
        $consultation_success->plageconsult_id = $plage->_id;
        $consultation_success->chrono = 8;
        $consultation_success->heure = $slot['hour'];
        $consultation_success->patient_id = $patient_prime->_id;
        $consultation_success->store();

        $slots = $plage->getEmptySlots();
        $slot = reset($slots);

        $consultation_failure = new CConsultation();
        $consultation_failure->plageconsult_id = $plage->_id;
        $consultation_failure->chrono = 8;
        $consultation_failure->heure = $slot['hour'];
        $consultation_failure->patient_id = $patient_bis->_id;
        $consultation_failure->store();

        $acte = new CActeNGAP();
        $acte->object_class = $consultation_failure->_class;
        $acte->object_id = $consultation_failure->_id;
        $acte->executant_id = $plage->loadRefChir()->_id;
        $acte->execution = $slot['date'] . ' ' . $slot['hour'];
        $acte->code = 'APC';
        $acte->quantite = 1;
        $acte->coefficient = 1;
        $msg = $acte->store();

        return [
            [$consultation_success, null],
            [$consultation_failure, CAppUI::tr('CConsultation') . CAppUI::tr('CMbObject-msg-check-failed') . CAppUI::tr('CConsultation-error-cancel_consultation_with_acts')],
        ];
    }
}
