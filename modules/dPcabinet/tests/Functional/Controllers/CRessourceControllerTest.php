<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Tests\Functional\Controllers;

use Exception;
use Ox\Core\Api\Request\RequestFilter;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tamm\Cabinet\Tests\Fixtures\TAMMPlanningFixtures;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Psr\SimpleCache\InvalidArgumentException;

class CRessourceControllerTest extends OxWebTestCase
{
    private CMediusers $user1;

    /**
     * @inheritdoc
     * @throws TestsException
     */
    public function setUp(): void
    {
        parent::setUp();
        /** @var CMediusers $user1 */
        $user1 = $this->getObjectFromFixturesReference(
            CMediusers::class,
            TAMMPlanningFixtures::TAG_PRACTITIONER_1
        );
        $this->user1 = $user1;
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testListRessourceCabByFonctionId(): void
    {
        $client = self::createClient(user: $this->user1->loadRefUser());
        $client->request(
            'GET',
            '/api/cabinet/ressourcesCab',
            [
                'filter' => 'function_id.' . RequestFilter::FILTER_IN . '.' . $this->user1->function_id,
            ]
        );

        $collection = $this->getJsonApiCollection($client);

        /** @var Item $item */
        foreach ($collection as $item) {
            $this->assertEquals($this->user1->function_id, $item->getAttribute('function_id'));
        }

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testListRessourceCabWithoutRight(): void
    {
        /** @var CMediusers $user_without_right */
        $user_without_right = $this->getObjectFromFixturesReference(
            CMediusers::class,
            TAMMPlanningFixtures::TAG_PRACTITIONER_WITHOUT_RIGHT
        );

        $client = self::createClient(user: $user_without_right->loadRefUser());
        $client->request('GET', '/api/cabinet/ressourcesCab');

        $this->assertResponseStatusCodeSame(403);
    }
}
