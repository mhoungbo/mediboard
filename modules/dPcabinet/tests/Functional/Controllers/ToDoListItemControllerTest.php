<?php

/**
 * @author  SARL OpenXtrem <dev@openxtrem.com>
 * @license GNU General Public License, see http://www.gnu.org/licenses/gpl.html
 */

namespace Ox\Mediboard\Cabinet\Tests\Functional\Controllers;

use Exception;
use Ox\Mediboard\Cabinet\CToDoListItem;
use Ox\Mediboard\Cabinet\Tests\Fixtures\TodoListItemsFixtures;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class ToDoListItemControllerTest extends OxWebTestCase
{
    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testList()
    {
        $todo_item = $this->getObjectFromFixturesReference(
            CToDoListItem::class,
            TodoListItemsFixtures::TAG_TODO_LIST_ITEM
        );
        $client    = static::createClient();
        $client->request('GET', '/api/cabinet/todolistitems');

        $collection = $this->getJsonApiCollection($client);

        $this->assertResponseIsSuccessful();

        $this->assertEquals(1, $collection->count());
        $this->assertEquals($todo_item->_id, $collection->getFirstItem()->getId());
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testGet()
    {
        $todo_item = $this->getObjectFromFixturesReference(
            CToDoListItem::class,
            TodoListItemsFixtures::TAG_TODO_LIST_ITEM
        );
        $client    = static::createClient();
        $client->request('GET', '/api/cabinet/todolistitems/' . $todo_item->_id);

        $item = $this->getJsonApiItem($client);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($todo_item->_id, $item->getId());
        $this->assertEquals($todo_item::RESOURCE_TYPE, $item->getType());
    }

    /**
     * @throws Exception
     */
    public function testDelete()
    {
        $todo_item          = new CToDoListItem();
        $todo_item->libelle = 'delete';
        $this->storeOrFailed($todo_item);

        $client = static::createClient();
        $client->request('DELETE', '/api/cabinet/todolistitems/' . $todo_item->_id);

        $this->assertResponseStatusCodeSame(204);

        $this->assertEmpty($client->getResponse()->getContent());

        $this->assertFalse(CToDoListItem::find($todo_item->_id));
    }

    /**
     * @throws Exception
     */
    public function testPatch()
    {
        $todo_item = $this->getObjectFromFixturesReference(
            CToDoListItem::class,
            TodoListItemsFixtures::TAG_TODO_LIST_ITEM
        );
        $update    = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID         => $todo_item->_id,
                    Item::TYPE       => $todo_item::RESOURCE_TYPE,
                    Item::ATTRIBUTES => [
                        'libelle' => 'update',
                    ],
                ],
            ]
        );

        $client = static::createClient();
        $this->setJsonApiContentTypeHeader($client)->request(
            'PATCH',
            '/api/cabinet/todolistitems/' . $todo_item->_id,
            [],
            [],
            [],
            json_encode($update)
        );

        $this->assertResponseIsSuccessful();

        $item = $this->getJsonApiItem($client);
        $this->assertEquals($todo_item::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($todo_item->_id, $item->getId());
        $this->assertEquals('update', $item->getAttribute('libelle'));
    }

    /**
     * @throws Exception
     */
    public function testPost()
    {
        $item = new Item(CToDoListItem::RESOURCE_TYPE, null);
        $item->setAttributes(
            [
                'libelle' => 'post',
            ]
        );

        try {
            $client        = static::createClient();
            $this->setJsonApiContentTypeHeader($client)->request(
                'POST',
                '/api/cabinet/todolistitems',
                [],
                [],
                [],
                json_encode($item)
            );

            $this->assertResponseIsSuccessful();

            $item = $this->getJsonApiCollection($client)->getFirstItem();
            $this->assertEquals(CToDoListItem::RESOURCE_TYPE, $item->getType());
            $this->assertNotNull($item->getId());
            $this->assertEquals('post', $item->getAttribute('libelle'));
        } finally {
            $this->deleteOrFailed((new CToDoListItem)->load($item->getId()));
        }
    }
}
