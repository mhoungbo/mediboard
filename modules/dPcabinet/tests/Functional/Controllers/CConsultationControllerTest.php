<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Tests\Functional\Controllers;

use Exception;
use Ox\Core\Api\Request\RequestFilter;
use Ox\Core\Cache;
use Ox\Core\CMbObject;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Cabinet\Tests\Fixtures\ConsultationFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tamm\Cabinet\Planning\Event\PlanningEvent;
use Ox\Tamm\Cabinet\Tests\Fixtures\TAMMPlanningFixtures;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class CConsultationControllerTest extends OxWebTestCase
{
    /**
     * Test: Show a consultation by 'consultation_id' with permission.
     *
     * @throws TestsException
     * @throws ReflectionException
     * @throws Exception
     */
    public function testShowWithPermission(): void
    {
        $consultation = $this->getConsultation();

        // Create client.
        $client = self::createClient();
        $client->request('GET', "api/cabinet/consultations/{$consultation->_id}", ['relations' => 'all']);

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($consultation::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($consultation->_id, $item->getId());

        // Asserting: Relation PlageConsult.
        $this->assertTrue($item->hasRelationship(CConsultation::RELATION_PLAGE_CONSULT));
        $this->assertEquals(
            CPlageconsult::RESOURCE_TYPE,
            $item->getRelationship(CConsultation::RELATION_PLAGE_CONSULT)->getType()
        );
        $this->assertEquals(
            $consultation->plageconsult_id,
            $item->getRelationship(CConsultation::RELATION_PLAGE_CONSULT)->getId()
        );

        // Asserting: Relation User.
        $this->assertTrue($item->hasRelationship(CConsultation::RELATION_MEDIUSER));
        $this->assertEquals(
            CMediusers::RESOURCE_TYPE,
            $item->getRelationship(CConsultation::RELATION_MEDIUSER)->getType()
        );
        $this->assertEquals(
            $consultation->loadRefPraticien()->_id,
            $item->getRelationship(CConsultation::RELATION_MEDIUSER)->getId()
        );
    }

    /**
     * Test: Show a consultation by 'consultation_id' without permission.
     *
     * @throws TestsException
     * @throws Exception
     * @dataProvider dataProviderShowWithoutPermission
     */
    public function testShowWithoutPermission(CMbObject $object): void
    {
        $consultation = $this->getConsultation();
        $current_user = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "api/cabinet/consultations/{$consultation->_id}");

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$object->_class][$object->_id] = $actual_perm;
            }
        }
    }

    /**
     * DataProvider for testShowWithoutPermission.
     *
     * @throws TestsException
     */
    public function dataProviderShowWithoutPermission(): array
    {
        return [
            'dataObject'  => [$this->getConsultation()],
            'dataContext' => [$this->getConsultation()->loadRefPatient()],
        ];
    }

    /**
     * Get consultation.
     *
     * @throws TestsException
     */
    private function getConsultation(): CConsultation
    {
        /** @var CConsultation */
        return $this->getObjectFromFixturesReference(
            CConsultation::class,
            ConsultationFixtures::TAG_NEXT_CONSULTATION_ . 1
        );
    }

    /**
     * @throws TestsException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function testPatchConsultation(): void
    {
        /** @var CMediusers $user */
        $user = $this->getObjectFromFixturesReference(
            CMediusers::class,
            TAMMPlanningFixtures::TAG_PRACTITIONER_1
        );

        /** @var CConsultation $consultation */
        $consultation = $this->getObjectFromFixturesReference(
            CConsultation::class,
            TAMMPlanningFixtures::TAG_CONSULTATION_1
        );

        $update = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID         => $consultation->_id,
                    Item::TYPE       => $consultation::RESOURCE_TYPE,
                    Item::ATTRIBUTES => [
                        'chrono' => '32',
                    ],
                ],
            ]
        );

        $client = self::createClient(user: $user->loadRefUser());
        $this->setJsonApiContentTypeHeader($client)->request(
            'PATCH',
            "/api/cabinet/consultations/{$consultation->_id}",
            content: json_encode($update)
        );

        // Suppression du cache de la consultation
        $cache = Cache::getCache(Cache::INNER);
        $cache->delete(PlanningEvent::KEY_CONSULTATION . $consultation->_id);

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals("32", $item->getAttribute('chrono'));
    }
}
