<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Tests\Fixtures;

use DateTimeImmutable;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

/**
 * @description Simple consultations
 */
class ConsultationFixtures extends Fixtures
{
    public const TAG_CONSULTATION_USER      = 'consultation_user';
    public const TAG_CONSULTATION_PATIENT   = 'consultation_patient';
    public const TAG_PREVIOUS_CONSULTATION_ = 'previous_consultation_';
    public const TAG_NEXT_CONSULTATION_     = 'next_consultation_';

    private const COUNT_CONSULTATIONS = 5;

    private CMediusers    $user;
    private CPatient      $patient;
    private CPlageconsult $previous_consultation_range;
    private CPlageconsult $next_consultation_range;

    /**
     * @inheritDoc
     * @throws FixturesException
     * @throws CModelObjectException
     */
    public function load(): void
    {
        $this->user                        = $this->createUser();
        $this->patient                     = $this->createPatient();
        $this->previous_consultation_range = $this->createConsultationRange('-1 DAY');
        $this->next_consultation_range     = $this->createConsultationRange('+1 DAY');

        $this->createConsultations();
    }

    /**
     * Create a medic user.
     *
     * @throws FixturesException
     */

    private function createUser(): CMediusers
    {
        $user             = $this->getUser(false);
        $user->_user_type = 13;

        $this->store($user, self::TAG_CONSULTATION_USER);

        return $user;
    }

    /**
     * Create a patient.
     *
     * @throws FixturesException
     * @throws CModelObjectException
     * @throws \Exception
     */
    private function createPatient(): CPatient
    {
        $patient        = CPatient::getSampleObject();
        $patient->deces = null;

        $this->store($patient, self::TAG_CONSULTATION_PATIENT);

        return $patient;
    }

    /**
     * Create consultation range.
     *
     * @throws FixturesException
     */
    private function createConsultationRange(string $day_modify): CPlageconsult
    {
        $consultation_range          = new CPlageconsult();
        $consultation_range->chir_id = $this->user->_id;
        $consultation_range->libelle = 'Fixtures';
        $consultation_range->date    = (new DateTimeImmutable())->modify($day_modify)->format('Y-m-d');
        $consultation_range->debut   = (new DateTimeImmutable())->modify($day_modify)->setTime(8, 0)->format('H:i:s');
        $consultation_range->fin     = (new DateTimeImmutable())->modify($day_modify)->setTime(20, 0)->format('H:i:s');
        $consultation_range->freq    = (new DateTimeImmutable())->modify($day_modify)->setTime(1, 0)->format('H:i:s');

        $this->store($consultation_range);

        return $consultation_range;
    }

    /**
     * Create consultations.
     *
     * @throws FixturesException
     */
    private function createConsultations(): void
    {
        for ($index = 0; $index < self::COUNT_CONSULTATIONS; $index++) {
            // Previous
            $previous_consultation                    = new CConsultation();
            $previous_consultation->owner_id          = $this->user->_id;
            $previous_consultation->patient_id        = $this->patient->_id;
            $previous_consultation->plageconsult_id   = $this->previous_consultation_range->_id;
            $previous_consultation->chrono            = CConsultation::EN_COURS;
            $previous_consultation->heure             = (new DateTimeImmutable())->modify('-1 DAY')
                                                                                 ->setTime(8 + $index, 0)
                                                                                 ->format('H:i:s');
            $previous_consultation->motif             = 'Fixtures';
            $previous_consultation->type_consultation = 'consultation';

            $this->store($previous_consultation, self::TAG_PREVIOUS_CONSULTATION_ . $index);

            // Next
            $next_consultation                    = new CConsultation();
            $next_consultation->owner_id          = $this->user->_id;
            $next_consultation->patient_id        = $this->patient->_id;
            $next_consultation->plageconsult_id   = $this->next_consultation_range->_id;
            $next_consultation->chrono            = CConsultation::EN_COURS;
            $next_consultation->heure             = (new DateTimeImmutable())->modify('+1 DAY')
                                                                             ->setTime(8 + $index, 0)
                                                                             ->format('H:i:s');
            $next_consultation->motif             = 'Fixtures';
            $next_consultation->type_consultation = 'consultation';

            $this->store($next_consultation, self::TAG_NEXT_CONSULTATION_ . $index);
        }
    }
}
