<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Tests\Fixtures;

use Ox\Mediboard\Cabinet\CToDoListItem;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

class TodoListItemsFixtures extends Fixtures
{
    public const TAG_TODO_LIST_ITEM          = "todo_list_item";

    /**
     * @inheritDoc
     * @throws FixturesException
     */
    public function load()
    {
        $todo_item          = new CToDoListItem();
        $todo_item->libelle = "fixtures";

        $this->store($todo_item, self::TAG_TODO_LIST_ITEM);
    }
}
