/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class Reunion extends OxObject {
    constructor () {
        super()
        this.type = "reunion"
    }

    get motif (): OxAttr<string> {
        return super.get("motif")
    }

    set motif (value: OxAttr<string>) {
        super.set("motif", value)
    }
}
