/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class PlageConsult extends OxObject {
    protected _relationsTypes = {}

    constructor () {
        super()
        this.type = "plageConsult"
    }

    get date (): OxAttr<string> {
        return super.get("date")
    }

    set date (date: OxAttr<string>) {
        super.set("date", date)
    }

    get locked (): OxAttr<boolean> {
        return super.get("locked")
    }

    set locked (value: OxAttr<boolean>) {
        super.set("locked", value)
    }
}
