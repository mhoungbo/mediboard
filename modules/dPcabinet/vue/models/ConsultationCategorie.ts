/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class ConsultationCategorie extends OxObject {
    constructor () {
        super()
        this.type = "consultationCategory"
    }

    get nomCategorie (): OxAttr<string> {
        return super.get("nom_categorie")
    }

    set nomCategorie (value: OxAttr<string>) {
        super.set("nom_categorie", value)
    }

    get nomIcone (): OxAttr<string> {
        return super.get("nom_icone")
    }

    set nomIcone (value: OxAttr<string>) {
        super.set("nom_icone", value)
    }

    get path (): string {
        return "modules/dPcabinet/images/categories/" + this.nomIcone
    }
}
