/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import Patient from "@modules/dPpatients/vue/models/Patient"
import { OxAttr } from "@/core/types/OxObjectTypes"
import PracticeInvoice from "@modules/dPfacturation/vue/models/PracticeInvoice"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import OxDate from "@oxify/utils/OxDate"
import PlageConsult from "@modules/dPcabinet/vue/models/PlageConsult"
import { isModuleActive } from "@/core/utils/OxModulesManager"
import ConsultationCategorie from "@modules/dPcabinet/vue/models/ConsultationCategorie"

// Dynamic import
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
let File: any
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
let file: any

/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
let CompteRendu: any
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
let compteRendu: any

/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
let MedicamentLine: any
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
let medicamentLine: any

/**
 * File import
 */
if (isModuleActive("dPfiles")) {
    // @ts-ignore
    import("@modules/dPfiles/vue/models/File").then((mod) => {
        File = mod.default

        /** @type { File } */
        file = new File()
    })
}

/**
 * Report import
 */
if (isModuleActive("dPcompteRendu")) {
    // @ts-ignore
    import("@modules/dPcompteRendu/vue/models/CompteRendu").then((mod) => {
        CompteRendu = mod.default

        /** @type { CompteRendu } */
        compteRendu = new CompteRendu()
    })
}

/**
 * Prescription import
 */
if (isModuleActive("dPprescription")) {
    // @ts-ignore
    import("@modules/dPprescription/vue/models/MedicamentLine").then((mod) => {
        MedicamentLine = mod.default

        /** @type { MedicamentLine } */
        medicamentLine = new MedicamentLine()
    })
}

export enum CancellationReasons {
    NOT_ARRIVED = "not_arrived",
    BY_PATIENT = "by_patient",
    OTHER = "other"
}

export enum ConsultationChrono {
    ASKED = "8",
    PLANNED = "16",
    PATIENT_ARRIVED = "32",
    CHRONO_PROGRESS = "48",
    CHRONO_FINISH = "64"
}

export default class Consultation extends OxObject {
    public static RELATION_PATIENT = "patient"
    public static RELATION_PRACTICE_INVOICE = "practiceInvoice"
    public static RELATION_MEDIUSER = "mediuser"
    public static RELATION_CATEGORY = "categorie"

    public static FIELDSET_EXAMEN = "examen"
    public static FIELDSET_DEFAULT = "default"
    public static FIELDSET_STATUS = "status"
    public static FIELDSET_PREGNANCY = "pregnancy"

    public static RELATION_PLAGE_CONSULT = "plageConsult"
    public static RELATION_FILES = "files"
    public static RELATION_MEDICAL_REPORT = "medicalReports"
    public static RELATION_MEDICAMENT_LINES = "medicamentLines"
    public static UNPAID_SUM_PARAMETER = "unpaid_sum"
    public static COUNT_DOC_PARAMETER = "count_docs"

    protected _relationsTypes = {
        patient: Patient,
        practice_invoice: PracticeInvoice,
        mediuser: Mediuser,
        plageConsult: PlageConsult,
        file: File,
        medicalReports: CompteRendu,
        lineMedicament: MedicamentLine,
        consultationCategory: ConsultationCategorie
    }

    public static TYPES = {
        consultation: "consultation",
        monitoring: "suivi_patient"
    }

    constructor () {
        super()
        this.type = "consultation"
    }

    get appointmentDate (): Date {
        return new Date(super.get("_datetime"))
    }

    get first (): OxAttr<boolean> {
        return super.get("premiere")
    }

    get chrono (): OxAttr<string> {
        return super.get("chrono")
    }

    set chrono (chrono: OxAttr<string>) {
        super.set("chrono", chrono)
    }

    get cancelled (): OxAttr<boolean> {
        return super.get("annule")
    }

    set cancelled (cancelled: OxAttr<boolean>) {
        super.set("annule", cancelled)
    }

    get hour (): OxAttr<string> {
        return super.get("heure")
    }

    set hour (heure: OxAttr<string>) {
        super.set("heure", heure)
    }

    get formattedHour (): string {
        const date = new Date(OxDate.getYMD() + " " + this.hour)
        return OxDate.getHm(date)
    }

    get duree (): OxAttr<string> {
        return super.get("duree")
    }

    set duree (duree: OxAttr<string>) {
        super.set("duree", duree)
    }

    get patient (): Patient | undefined {
        return this.loadForwardRelation<Patient>(Consultation.RELATION_PATIENT)
    }

    set patient (patient: Patient | undefined) {
        this.setForwardRelation<Patient>(Consultation.RELATION_PATIENT, patient)
    }

    get practiceInvoice (): PracticeInvoice | undefined {
        return this.loadForwardRelation<PracticeInvoice>(Consultation.RELATION_PRACTICE_INVOICE)
    }

    set practiceInvoice (invoice: PracticeInvoice | undefined) {
        this.setForwardRelation(Consultation.RELATION_PRACTICE_INVOICE, invoice)
    }

    get medicamentLines (): typeof medicamentLine[] {
        return this.loadBackwardRelation(Consultation.RELATION_MEDICAMENT_LINES)
    }

    get plageConsult (): PlageConsult | undefined {
        return this.loadForwardRelation<PlageConsult>(Consultation.RELATION_PLAGE_CONSULT)
    }

    get files (): typeof file[] {
        return this.loadBackwardRelation(Consultation.RELATION_FILES)
    }

    get medicalReports (): typeof compteRendu[] {
        return this.loadBackwardRelation(Consultation.RELATION_MEDICAL_REPORT)
    }

    get practitioner (): Mediuser | undefined {
        return this.loadForwardRelation<Mediuser>(Consultation.RELATION_MEDIUSER)
    }

    get category (): ConsultationCategorie | undefined {
        return this.loadForwardRelation<ConsultationCategorie>(Consultation.RELATION_CATEGORY)
    }

    get reason (): OxAttr<string> {
        return super.get("motif")
    }

    set reason (reason: OxAttr<string>) {
        super.set("motif", reason)
    }

    get unpaidSum (): string {
        return super.get("unpaid_consultations_sum") ? parseFloat(super.get("unpaid_consultations_sum")).toFixed(2) : ""
    }

    set unpaidSum (sum: string) {
        super.set("unpaid_consultations_sum", sum)
    }

    get docCount (): number {
        return this.fileCount + this.reportCount
    }

    get reportCount (): number {
        return parseInt(super.get("report_count")) ?? 0
    }

    set reportCount (count: number) {
        super.set("report_count", count ?? 0)
    }

    get fileCount (): number {
        return parseInt(super.get("file_count")) ?? 0
    }

    set fileCount (count: number) {
        super.set("file_count", count ?? 0)
    }

    get prescCount (): number {
        return parseInt(super.get("presc_count")) ?? 0
    }

    set prescCount (count: number) {
        super.set("presc_count", count ?? 0)
    }

    get formCount (): number {
        return parseInt(super.get("form_count")) ?? 0
    }

    set formCount (count: number) {
        super.set("form_count", count ?? 0)
    }

    get arrival (): OxAttr<string> {
        return super.get("arrivee")
    }

    set arrival (arrivee: OxAttr<string> | null) {
        super.set("arrivee", arrivee)
    }

    get arrivalDate (): Date | null {
        return super.get("arrivee") !== null ? new Date(super.get("arrivee")) : null
    }

    set arrivalDate (arrivee: Date | null) {
        super.set("arrivee", arrivee)
    }

    get plageconsultId (): OxAttr<number> {
        return super.get("plageconsult_id")
    }

    set plageconsultId (plageconsultId: OxAttr<number>) {
        super.set("plageconsult_id", plageconsultId)
    }

    get patientId (): OxAttr<number> {
        return super.get("patient_id")
    }

    set patientId (patientId: OxAttr<number>) {
        super.set("patient_id", patientId)
    }

    get consultationType (): OxAttr<string> {
        return super.get("type_consultation")
    }

    set consultationType (type: OxAttr<string>) {
        super.set("type_consultation", type)
    }

    get remark (): OxAttr<string> {
        return super.get("rques")
    }

    set remark (remark: OxAttr<string>) {
        super.set("rques", remark)
    }

    get cancellationReason (): OxAttr<string> {
        return super.get("motif_annulation")
    }

    set cancellationReason (reason: OxAttr<string>) {
        super.set("motif_annulation", reason)
    }

    get guid (): string {
        return "CConsultation-" + super.id
    }

    get teleconsultation (): OxAttr<boolean> {
        return super.get("teleconsultation")
    }

    set teleconsultation (value: OxAttr<boolean>) {
        super.set("teleconsultation", value)
    }

    get lastUpdate (): Date {
        return new Date(super.get("last_update"))
    }

    get pregnancyId (): OxAttr<number> {
        return super.get("grossesse_id")
    }

    set pregnancyId (pregnancyId: OxAttr<number>) {
        super.set("grossesse_id", pregnancyId)
    }

    get exam (): OxAttr<string> {
        return super.get("examen")
    }

    set exam (exam: OxAttr<string>) {
        super.set("examen", exam)
    }

    get anamnesis (): OxAttr<string> {
        return super.get("histoire_maladie")
    }

    set anamnesis (anamnesis: OxAttr<string>) {
        super.set("histoire_maladie", anamnesis)
    }

    get comment (): OxAttr<string> {
        return super.get("rques")
    }

    set comment (comment: OxAttr<string>) {
        super.set("rques", comment)
    }

    get conclusion (): OxAttr<string> {
        return super.get("conclusion")
    }

    set conclusion (conclusion: OxAttr<string>) {
        super.set("conclusion", conclusion)
    }

    get nextMeeting (): OxAttr<boolean> {
        return super.get("next_meeting")
    }

    set nextMeeting (nextMeeting: OxAttr<boolean>) {
        super.set("next_meeting", nextMeeting)
    }

    get invoice (): OxAttr<boolean> {
        return super.get("facture")
    }

    set invoice (facture: OxAttr<boolean>) {
        super.set("facture", facture)
    }
}
