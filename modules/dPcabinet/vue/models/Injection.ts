/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class Injection extends OxObject {
    constructor () {
        super()
        this.type = "injection"
    }

    get speciality (): OxAttr<string> {
        return super.get("speciality")
    }

    get date (): OxAttr<string> {
        return super.get("injection_date")
    }
}
