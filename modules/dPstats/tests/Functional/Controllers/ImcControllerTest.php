<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Stats\Tests\Functional\Controllers;

use Ox\Core\CMbDT;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Stats\Tests\Fixtures\ImcControllerFixtures;
use Ox\Tests\OxWebTestCase;

class ImcControllerTest extends OxWebTestCase
{
    public function testIndexImc(): void
    {
        $client = self::createClient();

        $client->request('GET', '/gui/stats/imc');

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();
        $table   = $crawler->filterXPath('//table[@class="main form"]');
        $this->assertCount(1, $table);
    }

    public function testListImcWithTypeImcBas(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/gui/stats/imc/list',
            [
                "_date_min"       => CMbDT::dateTime("+360 DAYS"),
                "_date_max"       => CMbDT::dateTime("+380 DAYS"),
                "patient_present" => "0",
                "type"            => "imc_bas",
                "page"            => "0",
                "csv"             => "0",
                "services_id"     => "",
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();
        $table   = $crawler->filterXPath('//table[@class="main tbl"]');
        $this->assertCount(1, $table);

        /** @var CSejour $sejour */
        $sejour  = $this->getObjectFromFixturesReference(
            CSejour::class,
            ImcControllerFixtures::IMC_CONTROLLER_IMC_BAS
        );
        $patient = $sejour->loadRefPatient();
        $patient->loadRefLatestConstantes(null, ["poids", "taille"], $sejour);
        $patient->loadIPP();
        $sejour->loadNDA();

        $this->stringContains($sejour->_ref_patient->nom, $table->filterXPath('//td[position()=1]')->text());
        $this->stringContains($sejour->_ref_patient->prenom, $table->filterXPath('//td[position()=1]')->text());

        $this->stringContains($sejour->_ref_patient->nom, $table->filterXPath('//td[position()=2]')->text());
        $this->stringContains($sejour->_ref_patient->prenom, $table->filterXPath('//td[position()=2]')->text());
        $this->stringContains(
            CMbDT::format($sejour->entree, "%Y/%m/%d"),
            $table->filterXPath('//td[position()=2]')->text()
        );
        $this->stringContains(
            CMbDT::format($sejour->sortie, "%Y/%m/%d"),
            $table->filterXPath('//td[position()=2]')->text()
        );

        $this->assertEquals(
            $sejour->_ref_patient->_IPP,
            $table->filterXPath('//td[position()=3]')->text()
        );

        $this->assertEquals(
            $sejour->_NDA,
            $table->filterXPath('//td[position()=4]')->text()
        );

        $this->stringContains(
            $sejour->_ref_patient->_ref_constantes_medicales->datetime,
            $table->filterXPath('//td[position()=5]')->text()
        );

        $this->assertEquals(
            $sejour->_ref_patient->_ref_constantes_medicales->_imc,
            $table->filterXPath('//td[position()=6]')->text()
        );
    }

    public function testListImcWithTypeSansImc(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/gui/stats/imc/list',
            [
                "_date_min"       => CMbDT::dateTime("+360 DAYS"),
                "_date_max"       => CMbDT::dateTime("+380 DAYS"),
                "patient_present" => "0",
                "type"            => "sans_imc",
                "page"            => "0",
                "csv"             => "0",
                "services_id"     => "",
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $crawler = $client->getCrawler();
        $table   = $crawler->filterXPath('//table[@class="main tbl"]');
        $this->assertCount(1, $table);

        /** @var CSejour $sejour */
        $sejour  = $this->getObjectFromFixturesReference(
            CSejour::class,
            ImcControllerFixtures::IMC_CONTROLLER_SANS_IMC
        );
        $patient = $sejour->loadRefPatient();
        $patient->loadRefLatestConstantes(null, ["poids", "taille"], $sejour);
        $patient->loadIPP();
        $sejour->loadNDA();

        $this->stringContains($sejour->_ref_patient->nom, $table->filterXPath('//td[position()=1]')->text());
        $this->stringContains($sejour->_ref_patient->prenom, $table->filterXPath('//td[position()=1]')->text());

        $this->stringContains($sejour->_ref_patient->nom, $table->filterXPath('//td[position()=2]')->text());
        $this->stringContains($sejour->_ref_patient->prenom, $table->filterXPath('//td[position()=2]')->text());
        $this->stringContains(
            CMbDT::format($sejour->entree, "%Y/%m/%d"),
            $table->filterXPath('//td[position()=2]')->text()
        );
        $this->stringContains(
            CMbDT::format($sejour->sortie, "%Y/%m/%d"),
            $table->filterXPath('//td[position()=2]')->text()
        );

        $this->assertEquals(
            $sejour->_ref_patient->_IPP,
            $table->filterXPath('//td[position()=3]')->text()
        );

        $this->assertEquals(
            $sejour->_NDA,
            $table->filterXPath('//td[position()=4]')->text()
        );
    }
}
