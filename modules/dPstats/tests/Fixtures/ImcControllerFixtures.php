<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Stats\Tests\Fixtures;

use Ox\Core\CMbDT;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\Fixtures\Fixtures;

class ImcControllerFixtures extends Fixtures
{
    public const IMC_CONTROLLER_IMC_BAS  = "imc_controller_imc_bas";
    public const IMC_CONTROLLER_SANS_IMC = "imc_controller_sans_imc";

    public function load()
    {
        $this->loadSejourImcBas();
        $this->loadSejourSansImc();
    }

    private function loadSejourSansImc(): void
    {
        $patient                  = new CPatient();
        $patient->prenom          = "sans_imc";
        $patient->nom_jeune_fille = "controller";
        $patient->naissance       = CMbDT::date("-20 YEARS");
        $patient->tutelle         = "tutelle";
        $this->store($patient);

        $sejour                = new CSejour();
        $sejour->patient_id    = $patient->_id;
        $sejour->praticien_id  = $this->getUser()->_id;
        $sejour->group_id      = CGroups::loadCurrent()->_id;
        $sejour->entree_prevue = CMbDT::dateTime("+365 DAYS");
        $sejour->sortie_prevue = CMbDT::dateTime("+369 DAYS");
        $this->store($sejour, self::IMC_CONTROLLER_SANS_IMC);
    }

    private function loadSejourImcBas(): void
    {
        $patient                  = new CPatient();
        $patient->prenom          = "imc_bas";
        $patient->nom_jeune_fille = "controller";
        $patient->naissance       = CMbDT::date("-20 YEARS");
        $patient->tutelle         = "tutelle";
        $this->store($patient);

        $sejour                = new CSejour();
        $sejour->patient_id    = $patient->_id;
        $sejour->praticien_id  = $this->getUser()->_id;
        $sejour->group_id      = CGroups::loadCurrent()->_id;
        $sejour->entree_prevue = CMbDT::dateTime("+365 DAYS");
        $sejour->sortie_prevue = CMbDT::dateTime("+369 DAYS");
        $this->store($sejour, self::IMC_CONTROLLER_IMC_BAS);

        $constante_medicale                = new CConstantesMedicales();
        $constante_medicale->patient_id    = $patient->_id;
        $constante_medicale->context_id    = $sejour->_id;
        $constante_medicale->context_class = "CSejour";
        $constante_medicale->datetime      = CMbDT::dateTime();
        $constante_medicale->poids         = 30;
        $constante_medicale->taille        = 180;
        $this->store($constante_medicale);
    }
}
