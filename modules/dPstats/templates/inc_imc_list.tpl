{{*
 * @package Mediboard\Stats
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<button class="download" onclick="IMC.exportCSV('{{$type}}')">{{tr}}Exporter fichier CSV{{/tr}}</button>
<div id="notice_export"></div>
<table class="main tbl">
  {{if $total > $limit}}
      {{mb_include module=system template=inc_pagination total=$total current=$page change_page="IMC.changePage.curry('$type')" step=$limit}}
  {{/if}}
  <tr>
    <th class="title" {{if $type == "imc_bas"}}colspan="6"{{else}}colspan="4"{{/if}}>
      {{tr}}denutrition-{{$type}}-title{{/tr}}
    </th>
  </tr>
  <tr>
    <th class="title">
        {{tr}}CPatient{{/tr}}
    </th>
    <th class="title">
        {{tr}}CSejour{{/tr}}
    </th>
    <th class="title">{{tr}}CPatient-_IPP{{/tr}}</th>
    <th class="title">{{tr}}CPatient-NDA{{/tr}}</th>
    {{if $type == "imc_bas"}}
      <th class="title">{{tr}}denutrition-last_imc_datetime{{/tr}}</th>
      <th class="title">{{tr}}denutrition-last_imc{{/tr}}</th>
    {{/if}}
  </tr>
    {{foreach from=$sejours item=_sejour}}
      <tr>
        <td>
          <span onmouseover="ObjectTooltip.createEx(this, '{{$_sejour->_ref_patient->_guid}}');">
            {{$_sejour->_ref_patient}}
          </span>
        </td>
        <td>
          <span onmouseover="ObjectTooltip.createEx(this, '{{$_sejour->_guid}}');">
            {{$_sejour}}
          </span>
        </td>
        <td>
            {{$_sejour->_ref_patient->_IPP}}
        </td>
        <td>
            {{$_sejour->_NDA}}
        </td>
        {{if $type == "imc_bas"}}
          <td>
              {{$_sejour->_ref_patient->_ref_constantes_medicales->datetime|date_format:$conf.datetime}}
          </td>
          <td>
              {{$_sejour->_ref_patient->_ref_constantes_medicales->_imc}}
          </td>
        {{/if}}
      </tr>
        {{foreachelse}}
      <tr>
        <td>
          {{tr}}CSejour.none{{/tr}}
        </td>
      </tr>
    {{/foreach}}
</table>
