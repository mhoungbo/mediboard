{{*
 * @package Mediboard\Stats
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=stats script=imc}}

<script>
  Main.add(function () {
    IMC.url_list_imc = "{{url name=stats_gui_imc_list}}"
  })
</script>

<form name="form_stats_imc" action="?" method="get" onsubmit="">
  <table class="main form">
    <tr>
      <th>{{mb_label object=$sejour field=_date_min}}</th>
      <td>{{mb_field object=$sejour field=_date_min form="form_stats_imc" canNull="false" register=true}}</td>
      <th>{{tr}}CPatient-NDA{{/tr}}</th>
      <td>{{mb_field object=$sejour field=_NDA}}</td>
      <th>{{mb_label object=$sejour field=_service}}</th>
      <td rowspan="2">
        <select name="services_id" size="5" multiple>
          <option value="" selected>&ndash; {{tr}}CService.all{{/tr}}</option>
            {{foreach from=$services item=_service}}
              <option value="{{$_service->_id}}">{{$_service->nom}}</option>
            {{/foreach}}
        </select>
      </td>
    </tr>
    <tr>
      <th>{{mb_label object=$sejour field=_date_max}}</th>
      <td>{{mb_field object=$sejour field=_date_max form="form_stats_imc" canNull="false" register=true}} </td>
      <th>{{mb_label class=CPatient field=_IPP}}</th>
      <td>{{mb_field class=CPatient field=_IPP}}</td>
    </tr>
    <tr>
      <th></th>
      <td></td>
      <th>{{tr}}denutrition-filter_imc-patient_present{{/tr}}</th>
      <td><input type="checkbox" name="patient_present"/></td>
    </tr>
    <tr>
      <td class="button" colspan="6">
        <button type="button" class="search" onclick="IMC.listImc('imc_bas', true)">{{tr}}denutrition-imc_bas{{/tr}}</button>
        <button type="button" class="search" onclick="IMC.listImc('sans_imc', true)">{{tr}}denutrition-sans_imc{{/tr}}</button>
      </td>
    </tr>
  </table>
</form>

<div id="list_result"></div>
