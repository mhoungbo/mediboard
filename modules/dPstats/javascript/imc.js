/**
 * @package Mediboard\Stats
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

IMC = {
  url_list_imc: '',
  page:         '',
  current_page: '',

  listImc: function (type, reset_page = false) {
    let form = getForm('form_stats_imc');
    new Url()
      .setRoute(IMC.url_list_imc)
      .addFormData(form)
      .addParam("services_id", [$V(form.services_id)].flatten().join(","))
      .addParam("type", type)
      .addNotNullParam('page', reset_page ? 0 : IMC.current_page)
      .requestUpdate("list_result");
  },

  changePage: function (type, page) {
    IMC.current_page = page;
    IMC.listImc(type);
  },

  exportCSV: function (type) {
    new Url(null, null, 'raw')
      .setRoute(IMC.url_list_imc)
      .addFormData(getForm('form_stats_imc'))
      .addParam("type", type)
      .addParam("csv", 1)
      .pop(500, 500, $T('Export-CSV'));
    let text = $T('denutrition-' + type + '-desc');
    $('notice_export').innerHTML = `<div class="info">` + text + `</div>`;
  }
};
