<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Stats;

use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbDT;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Patients\CConstantesMedicales;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;

class ImcService
{
    private string $type;
    private string $date_min;
    private string $date_max;
    /** @var string|null */
    private $nda;
    /** @var string|null */
    private                $ipp;
    private array          $services_id;
    private string         $patient_present;
    private int            $page;
    private int            $limit;
    private bool           $csv;
    private array          $sejours;
    private int            $total;
    private CSQLDataSource $ds;

    public function __construct(
        string $type,
        int    $page,
        int    $limit,
        string $date_min,
        string $date_max,
        string $nda = null,
        string $ipp = null,
        array  $services_id = null,
        string $patient_present = null,
        bool   $csv = false
    ) {
        $this->type            = $type;
        $this->page            = $page;
        $this->limit           = $limit;
        $this->date_min        = $date_min;
        $this->date_max        = $date_max;
        $this->nda             = $nda;
        $this->ipp             = $ipp;
        $this->services_id     = $services_id;
        $this->patient_present = $patient_present;
        $this->csv             = $csv;
        $this->ds              = CSQLDataSource::get("std");
    }

    public function calculImc(): void
    {
        $sejours  = [];
        $nb_total = 0;
        switch ($this->type) {
            case "imc_bas":
                ["sejours" => $sejours, "nb_total" => $nb_total] = $this->calculImcBas();
                break;
            case "sans_imc":
                ["sejours" => $sejours, "nb_total" => $nb_total] = $this->calculSansImc();
                break;
            default:
                break;
        }

        $this->sejours = $sejours;
        $this->total   = $nb_total;
    }

    private function getWhereSejour(): array
    {
        $where = [];
        if ($this->date_min && $this->date_max) {
            $where[] = $this->ds->prepare('sejour.entree < ?', $this->date_max);
            $where[] = $this->ds->prepare('sejour.sortie > ?', $this->date_min);
        }

        return $where;
    }

    private function getWhereNDA(): array
    {
        $where  = [];
        $ljoin  = [];
        $sejour = new CSejour();
        if ($this->nda) {
            $where[] = $this->ds->prepare('id_sante400_nda.id400 = ?', $this->nda);
            $where[] = $this->ds->prepare('id_sante400_nda.tag = ?', $sejour->getTagNDA());
            $ljoin[] = "id_sante400 AS id_sante400_nda ON id_sante400_nda.object_id = sejour.sejour_id AND id_sante400_nda.object_class = 'CSejour'";
        }

        return ["where" => $where, "ljoin" => $ljoin];
    }

    private function getWhereIPP(): array
    {
        $where   = [];
        $ljoin   = [];
        $patient = new CPatient();
        if ($this->ipp) {
            $where[] = $this->ds->prepare('id_sante400_ipp.id400 = ?', $this->ipp);
            $where[] = $this->ds->prepare('id_sante400_ipp.tag = ?', $patient->getTagIPP());
            $ljoin[] = "id_sante400 AS id_sante400_ipp ON id_sante400_ipp.object_id = sejour.patient_id AND id_sante400_ipp.object_class = 'CPatient'";
        }

        return ["where" => $where, "ljoin" => $ljoin];
    }

    private function getWhereService(): array
    {
        $where = [];
        $ljoin = [];
        if (count($this->services_id)) {
            $where[] = "affectation.service_id " . $this->ds->prepareIn($this->services_id);
            $ljoin[] = "affectation ON affectation.sejour_id = sejour.sejour_id";
        }

        return ["where" => $where, "ljoin" => $ljoin];
    }

    private function getWherePatientPresent(): array
    {
        $where = [];
        if ($this->patient_present) {
            $where[] = $this->ds->prepare('sejour.entree <= ?', CMbDT::dateTime());
            $where[] = $this->ds->prepare('sejour.sortie >= ?', CMbDT::dateTime());
        }

        return $where;
    }

    /**
     * S�jours o� le patient r�pond aux conditions suivantes : (Age > 69 ET IMC < 22) OU (17 < Age < 70 ET IMC < 18,5)
     */
    public function calculImcBas(): array
    {
        $sejours_id = [];
        $nb_sejour  = 0;

        $ljoin   = [];
        $ljoin[] = "patients ON patients.patient_id = sejour.patient_id";
        $ljoin[] = "constantes_medicales ON constantes_medicales.context_id = sejour.sejour_id AND constantes_medicales.context_class = 'CSejour'";

        $where   = [];
        $where[] = "patients.naissance < '" . CMbDT::date("-69 YEARS") . "'";
        $where[] = "constantes_medicales.poids IS NOT NULL OR constantes_medicales.taille IS NOT NULL";
        $where[] = "sejour.annule = '0'";
        $where[] = "sejour.group_id = " . CGroups::loadCurrent()->_id;
        $where[] = $this->getWhereSejour();
        $where[] = $this->getWherePatientPresent();
        ["where" => $where_service, "ljoin" => $ljoin_service] = $this->getWhereService();
        $where[] = $where_service;
        $ljoin[] = $ljoin_service;
        ["where" => $where_nda, "ljoin" => $ljoin_nda] = $this->getWhereNDA();
        $where[] = $where_nda;
        $ljoin[] = $ljoin_nda;
        ["where" => $where_ipp, "ljoin" => $ljoin_ipp] = $this->getWhereIPP();
        $where[] = $where_ipp;
        $ljoin[] = $ljoin_ipp;

        $request = new CRequest();
        $request->addTable("sejour");
        $request->addSelect(
            [
                "sejour.sejour_id",
                "GROUP_CONCAT(
                    CONCAT(datetime, '#', poids)
                    ORDER BY datetime DESC
                ) AS poids",
                "GROUP_CONCAT(
                     CONCAT(datetime, '#', taille)
                    ORDER BY datetime DESC
                ) AS taille",
            ]
        );
        $request->addWhere(CMbArray::array_flatten($where));
        $request->addLJoin(CMbArray::array_flatten($ljoin));
        $request->addGroup("sejour.sejour_id");

        $results = $this->ds->loadHashAssoc($request->makeSelect());
        $this->verifyIMC(22, $sejours_id, $nb_sejour, $results);

        $where   = [];
        $ljoin   = [];
        $ljoin[] = "patients ON patients.patient_id = sejour.patient_id";
        $ljoin[] = "constantes_medicales ON constantes_medicales.context_id = sejour.sejour_id AND constantes_medicales.context_class = 'CSejour'";
        $where[] = 'patients.naissance BETWEEN "' . CMbDT::date("-70 YEARS") . '" AND "' . CMbDT::date(
                "-17 YEARS"
            ) . '"';
        $where[] = "constantes_medicales.poids IS NOT NULL OR constantes_medicales.taille IS NOT NULL";
        $where[] = "sejour.annule = '0'";
        $where[] = "sejour.group_id = " . CGroups::loadCurrent()->_id;
        $where[] = $this->getWhereSejour();
        $where[] = $this->getWherePatientPresent();
        ["where" => $where_service, "ljoin" => $ljoin_service] = $this->getWhereService();
        $where[] = $where_service;
        $ljoin[] = $ljoin_service;
        ["where" => $where_nda, "ljoin" => $ljoin_nda] = $this->getWhereNDA();
        $where[] = $where_nda;
        $ljoin[] = $ljoin_nda;
        ["where" => $where_ipp, "ljoin" => $ljoin_ipp] = $this->getWhereIPP();
        $where[] = $where_ipp;
        $ljoin[] = $ljoin_ipp;

        $request = new CRequest();
        $request->addTable("sejour");
        $request->addSelect(
            [
                "sejour.sejour_id",
                "GROUP_CONCAT(
                    CONCAT(datetime, '#', poids)
                    ORDER BY datetime DESC
                ) AS poids",
                "GROUP_CONCAT(
                     CONCAT(datetime, '#', taille)
                    ORDER BY datetime DESC
                ) AS taille",
            ]
        );
        $request->addWhere(CMbArray::array_flatten($where));
        $request->addLJoin(CMbArray::array_flatten($ljoin));
        $request->addGroup("sejour.sejour_id");

        $results = $this->ds->loadHashAssoc($request->makeSelect());
        $this->verifyIMC(18.5, $sejours_id, $nb_sejour, $results);

        return ["sejours" => $this->loadAllSejours($sejours_id, "imc_bas"), "nb_total" => $nb_sejour];
    }

    private function verifyIMC(float $imc_ref, array &$sejours_id, int &$nb_sejour, array $results): void
    {
        foreach ($results as $_sejour_id => $_result) {
            if (!$_result["poids"] || !$_result["taille"]) {
                continue;
            }
            [$datetime_poids, $poids] = explode("#", explode(",", $_result["poids"])[0]);
            [$datetime_taille, $taille] = explode("#", explode(",", $_result["taille"])[0]);
            $imc = round($poids / ($taille * $taille * 0.0001), 2);
            if ($imc < $imc_ref) {
                if ((count($sejours_id) <= $this->limit && $nb_sejour >= $this->page) || $this->csv) {
                    $sejours_id[$_sejour_id] = [
                        "sejour_id"    => $_sejour_id,
                        "imc"          => $imc,
                        "datetime_imc" => max($datetime_poids, $datetime_taille),
                    ];
                }
                $nb_sejour++;
            }
        }
    }

    private function computeIMC(array $results): array
    {
        $imc_by_datetime = [];
        foreach ($results as $sejour_id => $_result) {
            if (!$_result["poids"] || !$_result["taille"]) {
                continue;
            }
            $poids_by_datetime     = explode(",", $_result["poids"]);
            $taille_by_datetime    = explode(",", $_result["taille"]);
            $const_sorted_datetime = [];
            foreach ($poids_by_datetime as $_poids_by_datetime) {
                [$datetime, $poids] = explode("#", $_poids_by_datetime);
                $const_sorted_datetime[$datetime]["poids"] = $poids;
            }
            foreach ($taille_by_datetime as $_taille_by_datetime) {
                [$datetime, $taille] = explode("#", $_taille_by_datetime);
                $const_sorted_datetime[$datetime]["taille"] = $taille;
            }
            ksort($const_sorted_datetime);
            $temp_poids  = null;
            $temp_taille = null;
            foreach ($const_sorted_datetime as $_datetime => $_values) {
                if (isset($_values["poids"])) {
                    $temp_poids = $_values["poids"];
                }
                if (isset($_values["taille"])) {
                    $temp_taille = $_values["taille"];
                }
                if ($temp_poids && $temp_taille) {
                    $imc                                     = round(
                        $temp_poids / ($temp_taille * $temp_taille * 0.0001),
                        2
                    );
                    $imc_by_datetime[$sejour_id][$_datetime] = $imc;
                }
            }
        }

        return $imc_by_datetime;
    }

    private function loadAllSejours(array $sejours_id, string $type): array
    {
        $sejours  = (new CSejour())->loadAll(array_column($sejours_id, "sejour_id"));
        $patients = CStoredObject::massLoadFwdRef($sejours, "patient_id");
        CSejour::massLoadNDA($sejours);
        CPatient::massLoadIPP($patients);
        foreach ($sejours as $_sejour) {
            $_patient = $_sejour->loadRefPatient();
            if ($type == "imc_bas") {
                $_patient->_ref_constantes_medicales           = new CConstantesMedicales();
                $_patient->_ref_constantes_medicales->_imc     = $sejours_id[$_sejour->_id]["imc"];
                $_patient->_ref_constantes_medicales->datetime = $sejours_id[$_sejour->_id]["datetime_imc"];
            }
        }

        return $sejours;
    }

    /**
     * liste des s�jours de plus de 2 nuits dont le patient r�pond aux conditions suivantes : (Date IMC >= Date
     * d'entr�e du s�jour + 7 jours) ou Aucun IMC
     */
    public function calculSansImc(): array
    {
        $sejours_id     = [];
        $all_sejour_ids = [];

        $ljoin   = [];
        $ljoin[] = "constantes_medicales ON constantes_medicales.context_id = sejour.sejour_id AND constantes_medicales.context_class = 'CSejour'";

        $where   = [];
        $where[] = 'DATEDIFF(sejour.sortie, sejour.entree) > 2';
        $where[] = "sejour.annule = '0'";
        $where[] = "sejour.group_id = " . CGroups::loadCurrent()->_id;
        $where[] = $this->getWhereSejour();
        $where[] = $this->getWherePatientPresent();
        ["where" => $where_service, "ljoin" => $ljoin_service] = $this->getWhereService();
        $where[] = $where_service;
        $ljoin[] = $ljoin_service;
        ["where" => $where_nda, "ljoin" => $ljoin_nda] = $this->getWhereNDA();
        $where[] = $where_nda;
        $ljoin[] = $ljoin_nda;
        ["where" => $where_ipp, "ljoin" => $ljoin_ipp] = $this->getWhereIPP();
        $where[] = $where_ipp;
        $ljoin[] = $ljoin_ipp;

        $request = new CRequest();
        $request->addTable("sejour");
        $request->addSelect(
            [
                "sejour.sejour_id",
                "GROUP_CONCAT(
                    CONCAT(datetime, '#', poids)
                    ORDER BY datetime ASC
                ) AS poids",
                "GROUP_CONCAT(
                     CONCAT(datetime, '#', taille)
                    ORDER BY datetime ASC
                ) AS taille",
                "sejour.entree",
            ]
        );
        $request->addWhere(CMbArray::array_flatten($where));
        $request->addLJoin(CMbArray::array_flatten($ljoin));
        $request->addGroup("sejour.sejour_id");

        $results         = $this->ds->loadHashAssoc($request->makeSelect());
        $imc_by_datetime = $this->computeIMC($results);

        foreach ($results as $_sejour_id => $_result) {
            if (!$_result["poids"] || !$_result["taille"]) {
                if ((count($sejours_id) <= $this->limit && count($all_sejour_ids) >= $this->page) || $this->csv) {
                    $sejours_id[$_sejour_id] = ["sejour_id" => $_sejour_id];
                }
                $all_sejour_ids[$_sejour_id] = $_sejour_id;
                continue;
            }
            foreach ($imc_by_datetime[$_sejour_id] as $_datetime => $_imc) {
                if ($_datetime < CMbDT::dateTime("+7 DAYS", $results[$_sejour_id]["entree"])) {
                    continue;
                }
                if ((count($sejours_id) <= $this->limit && count($all_sejour_ids) >= $this->page) || $this->csv) {
                    $sejours_id[$_sejour_id] = ["sejour_id" => $_sejour_id];
                }
                $all_sejour_ids[$_sejour_id] = $_sejour_id;
            }
        }

        return ["sejours" => $this->loadAllSejours($sejours_id, "sans_imc"), "nb_total" => count($all_sejour_ids)];
    }

    public function getExportCSV(): string
    {
        $this->calculImc();
        $sejours = $this->getSejours();
        $csv     = new CCSVFile(null, CCSVFile::PROFILE_AUTO);
        if ($this->type == "imc_bas") {
            $csv->writeLine(
                [
                    "nom",
                    "prenom",
                    "ipp",
                    "nda",
                    "date entr�e",
                    "date sortie",
                    "dur�e s�jour",
                    "age",
                    "imc",
                ]
            );
            foreach ($sejours as $_sejour) {
                $csv->writeLine(
                    [
                        $_sejour->_ref_patient->nom,
                        $_sejour->_ref_patient->prenom,
                        $_sejour->_ref_patient->_IPP,
                        $_sejour->_NDA,
                        CMbDT::format($_sejour->entree, CAppUI::conf("datetime")),
                        CMbDT::format($_sejour->sortie, CAppUI::conf("datetime")),
                        $_sejour->_duree . " jours",
                        $_sejour->_ref_patient->_age,
                        $_sejour->_ref_patient->_ref_constantes_medicales->_imc,
                    ]
                );
            }
        } else {
            $csv->writeLine(
                [
                    'nom',
                    'pr�nom',
                    'ipp',
                    'nda',
                    'date entr�e',
                    "date sortie",
                    "dur�e s�jour",
                    'age',
                ]
            );
            foreach ($sejours as $_sejour) {
                $csv->writeLine(
                    [
                        $_sejour->_ref_patient->nom,
                        $_sejour->_ref_patient->prenom,
                        $_sejour->_ref_patient->_IPP,
                        $_sejour->_NDA,
                        CMbDT::format($_sejour->entree, CAppUI::conf("datetime")),
                        CMbDT::format($_sejour->sortie, CAppUI::conf("datetime")),
                        $_sejour->_duree . " jours",
                        $_sejour->_ref_patient->_age,
                    ]
                );
            }
        }

        $content = $csv->getContent();
        $csv->close();

        return $content;
    }


    public function getSejours(): array
    {
        return $this->sejours;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}
