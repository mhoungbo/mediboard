<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Stats\Controllers;

use Ox\Core\CMbDT;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\Hospi\CService;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Stats\ImcService;
use Symfony\Component\HttpFoundation\Response;

class ImcController extends Controller
{
    public function index(): Response
    {
        $sejour            = new CSejour();
        $sejour->_date_min = CMbDT::dateTime("-1 YEAR");
        $sejour->_date_max = CMbDT::dateTime();

        $service  = new CService();
        $services = $service->loadGroupList(["cancelled" => "= '0'"]);

        return $this->renderSmarty("vw_imc", ["sejour" => $sejour, "services" => $services]);
    }

    public function list(RequestParams $params): Response
    {
        $date_min        = $params->get("_date_min", "dateTime", true);
        $date_max        = $params->get("_date_max", "dateTime", true);
        $nda             = $params->get("_NDA", "str", true);
        $ipp             = $params->get("_IPP", "str", true);
        $patient_present = $params->get("patient_present", "str default|0", true);
        $type            = $params->get("type", "enum list|imc_bas|sans_imc", true);
        $page            = $params->get("page", "num default|0");
        $csv             = $params->get("csv", "bool default|0");
        $services_id     = $params->get("services_id", "str");

        Controller::enforceSlave();

        $limit = 20;
        if ($services_id !== "") {
            $services_id = explode(",", $services_id);
        } else {
            $services_id = [];
        }
        $imc_service = new ImcService(
            $type,
            $page,
            $limit,
            $date_min,
            $date_max,
            $nda != "" ? $nda : null,
            $ipp != "" ? $ipp : null,
            $services_id,
            $patient_present ?? null,
            $csv
        );

        if ($csv) {
            $csv = $imc_service->getExportCSV();

            return $this->renderFileResponse($csv, "export_{$type}_" . CMbDT::date() . ".csv", "application/csv");
        } else {
            $imc_service->calculImc();
            $sejours = $imc_service->getSejours();
            $total   = $imc_service->getTotal();

            return $this->renderSmarty(
                "inc_imc_list",
                [
                    "sejours" => $sejours,
                    "total"   => $total,
                    "limit"   => $limit,
                    "type"    => $type,
                    "page"    => $page,
                ]
            );
        }
    }
}
