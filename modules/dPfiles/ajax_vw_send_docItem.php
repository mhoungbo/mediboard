<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CCanDo;
use Ox\Core\CMbObject;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Interop\Eai\CInteropActor;
use Ox\Interop\Sisra\CSisraTools;
use Ox\Mediboard\Files\CDocumentItem;

CCanDo::checkRead();

$docItem_guid = CView::get("docItem_guid", "guid class|CDocumentItem");
$receiver_guid = CView::get("receiver_guid", "guid class|CInteropActor");
$module_name = CView::get("module_name", "str");
$step = CView::get("step", "num");
$total = CView::get("total", "num");
CView::checkin();

/** @var CInteropActor $receiver */
$receiver = CMbObject::loadFromGuid($receiver_guid);

/** @var CDocumentItem $docItem */
$docItem = CMbObject::loadFromGuid($docItem_guid);
$docItem->loadRefAuthor();
$patient = $docItem->loadRelPatient();

$docItem->loadRefLastFileTraceability($receiver);

$doctors = [];
if ($module_name == "dmp") {
    $docItem->checkSynchroDMP($receiver);
    $docItem->loadRefLastDMPDocument();
}

$doctor_contact = null;
if ($module_name == "sisra") {
    $docItem->checkSynchroSisra($receiver);
    $patient->loadRefsCorrespondants();
    $medecin_traitant = $patient->_ref_medecin_traitant;
    if ($medecin_traitant->_id && CSisraTools::isDoctorSynchronised($medecin_traitant->_id)) {
        $doctors[] = $medecin_traitant;

        $doctor_contact = "$medecin_traitant->_id#$medecin_traitant->mssante_address|";
    }

    foreach ($patient->_ref_medecins_correspondants as $_correspondant) {
        $medecin_correspondant = $_correspondant->loadRefMedecin();
        if ($medecin_correspondant->_id && CSisraTools::isDoctorSynchronised($medecin_correspondant->_id)) {
            $doctors[] = $medecin_correspondant;

            $doctor_contact .= "$medecin_correspondant->_id#$medecin_correspondant->mssante_address|";
        }
    }
}

if ($module_name == "appFineClient") {
    $docItem->checkSynchroAppFine($receiver);
}

if ($module_name == "oxSIHCabinet") {
    $docItem->checkSynchroSIHCabinet($receiver);
}

if ($module_name == "oxCabinetSIH") {
    $docItem->checkSynchroCabinetSIH($receiver);
}

$smarty = new CSmartyDP("modules/$module_name");
$smarty->assign('module_name', $module_name);
$smarty->assign('docItem', $docItem);
$smarty->assign('receiver', $receiver);
$smarty->assign('patient', $patient);
$smarty->assign('total', $total);
$smarty->assign('step', $step);
$smarty->assign('doctors', $doctors);
$smarty->assign('doctor_contact', $doctor_contact);
$smarty->display('inc_send_doc_item.tpl');
