<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CApp;
use Ox\Core\CCanDo;
use Ox\Core\CMbDT;
use Ox\Core\CSmartyDP;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Files\Extractor;

CCanDo::checkAdmin();

$file = new CFile();

$smarty = new CSmartyDP();
$smarty->assign("listNbFiles", range(1, 10));
$smarty->assign("today", CMbDT::date());
$smarty->assign("nb_files", $file->countList() / 100);

// Extract data
$smarty->assign("source_available", Extractor::isSourceAvailable());
$smarty->assign("source", Extractor::getSource());
$smarty->assign("base_url", rtrim(CApp::getBaseUrl(), '/'));
$smarty->assign("profiles", Extractor::getProfiles());
$smarty->display("configure.tpl");
