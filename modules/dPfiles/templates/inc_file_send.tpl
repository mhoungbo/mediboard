{{*
 * @package Mediboard\Files
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=adaptive value=false}}

{{if "medimail"|module_active && count($docItem->_is_send_mssante) > 0}}
  <div class="mss_file_icons">
      {{if $docItem->_is_send_mssante.MSSantePatient == true}}
          {{mb_include module=medimail template=icon/inc_icon_mss mss_type="patient" adaptive=$adaptive}}
      {{/if}}

      {{if $docItem->_is_send_mssante.MSSantePro == true}}
          {{mb_include module=medimail template=icon/inc_icon_mss mss_type="pro" adaptive=$adaptive}}
      {{/if}}
  </div>
{{/if}}
