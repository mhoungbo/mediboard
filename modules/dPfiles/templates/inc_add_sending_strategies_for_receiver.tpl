{{*
 * @package Mediboard\Files
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var=form_add_strategy_name value="CFilesSendToReceiverStrategy-add-strategy-"|cat:$related_receiver->_guid}}

<script>
  Main.add(() => {
    showParametersForSending = function (select) {
      const form = getForm('{{$form_add_strategy_name}}');
      Array.from(document.getElementsByClassName("strategy_block")).forEach((element) => {
        element.classList.remove('me-display-flex')
        element.classList.add('me-no-display')
      });

      const strategy_name = select.value;
      Array.from(document.getElementsByClassName(strategy_name)).forEach((element) => {
        element.classList.add('me-display-flex')
        element.classList.remove('me-no-display')
      });

      $V(form.elements.strategy, strategy_name);
      $V(form.elements.value, '')
    };

    storeStrategies = function (form_add) {
      if ($V(form_add.elements.value) === "" || $V(form_add.elements.value) === undefined) {
        return false;
      }

      return onSubmitFormAjax(form_add, {onComplete: () => Control.Modal.refresh()});
    };

    updateValueDaysAndHours = function (form) {
      const days = $V(form.elements['_days_sending']) ? parseInt($V(form.elements['_days_sending'])) * 24 : 0;
      const hours = $V(form.elements['_hours_sending']) ? parseInt($V(form.elements['_hours_sending'])) : 0;

      $V(form.elements.value, days + hours)
    };

    updateValueTime = function (form, element) {
      $V(form.elements.value, $V(element))
    };
  })
</script>

<tr id="line-add-strategy-{{$related_receiver->_guid}}" style="display: none">
  <th style="text-align: right"><label for="choice_strategy"># {{$next_strategy_index}}</label></th>
  <td>
    <form name="{{$form_add_strategy_name}}" method="post" action="?" onsubmit="return storeStrategies(this)">
      <input type="hidden" name="@class" value="CFilesSendToReceiverStrategy"/>
      <input type="hidden" name="strategy_index" value="{{$next_strategy_index}}"/>
      <input type="hidden" name="files_category_to_receiver_id" value="{{$related_receiver->_id}}"/>
      <input type="hidden" name="strategy" value=""/>
      <input type="hidden" name="value" value=""/>
      <div class="me-display-flex">
          {{me_form_field animated=false nb_cells=0 mb_class=CFilesSendToReceiverStrategy mb_field="strategy"}}
            <select name="choice_strategy" onchange="showParametersForSending(this)">
              <option value="">&mdash;</option>
                {{foreach from=$strategies item=strategy}}
                  <option value="{{$strategy}}">
                      {{tr}}CFilesSendToReceiverStrategy.strategy.{{$strategy}}{{/tr}}
                  </option>
                {{/foreach}}
            </select>
          {{/me_form_field}}

          {{* before_start_event_scheduled | after_end_event_scheduled *}}
        <div class="strategy_block before_start_event_scheduled after_end_event_scheduled" style="display: none">
            {{me_form_field animated=false nb_cells=0 label="Days"}}
            {{mb_field
            class=CFilesSendToReceiverStrategy
            field="_days_sending"
            prop="num"
            increment=true
            onchange="updateValueDaysAndHours(this.form)"
            form="$form_add_strategy_name"
            }}
            {{/me_form_field}}

            {{me_form_field animated=false nb_cells=0 label="Hours"}}
            {{mb_field
            class=CFilesSendToReceiverStrategy
            field="_hours_sending"
            prop="num"
            increment=true
            onchange="updateValueDaysAndHours(this.form)"
            form="$form_add_strategy_name"
            }}
            {{/me_form_field}}

          <button type="submit" class="save notext" title="{{tr}}Save{{/tr}}"></button>
          <button type="button" class="erase me-tertiary notext" onclick="Control.Modal.refresh()"
                  title="{{tr}}Delete{{/tr}}"></button>
        </div>

          {{* time_slot_scheduled *}}
        <div class="strategy_block time_slot_scheduled" style="display: none">
            {{mb_field
            class=CFilesSendToReceiverStrategy
            field="_time_sending"
            register=true
            prop="time"
            onchange="updateValueTime(this.form, this)"
            form="$form_add_strategy_name"
            }}
          <button type="submit" class="save notext" title="{{tr}}Save{{/tr}}"></button>
          <button type="button" class="erase me-tertiary notext" onclick="Control.Modal.refresh()"
                  title="{{tr}}Delete{{/tr}}"></button>
        </div>
      </div>
    </form>
  </td>
</tr>
