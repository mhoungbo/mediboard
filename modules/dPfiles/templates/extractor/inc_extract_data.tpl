{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create("tabs-actions", true, {});
  });
</script>

<table>
  <tr>
    <td style="vertical-align: top;">
      <ul id="tabs-actions" class="control_tabs_vertical small">
        <li><a href="#config-source">Configuration de la source HTTP</a></li>
        <li><a href="#extractor-test">Testeur d'extraction</a></li>
      </ul>
    </td>
    <td style="vertical-align: top; width: 100%;">
      <div id="config-source" style="display: none;">
          {{mb_include module=system template=inc_config_exchange_source source=$source}}
      </div>
      <div id="extractor-test" style="display: none">
          {{mb_include module=dPfiles template=extractor/vw_extractor_test type='source'}}
      </div>
    </td>
  </tr>
</table>
