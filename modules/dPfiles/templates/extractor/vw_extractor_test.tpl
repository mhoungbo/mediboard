{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}
<script>
  submitParsing = function (form) {
    const options = {
      useFormData: true,
      method:      'post',
      params:      {
        ajax:  1,
        m:     'dPfiles',
        dosql: 'testParseFile'
      },
      contentType: 'multipart/form-data',
      title:       $T('dPfiles-extractor-Extraction result')
    };

    options.postBody = serializeForm(form, options);

    new Url().requestModal('80%', '80%', options);

    return false;
  };
</script>

<form name="file-parser" method="post"
      enctype="multipart/form-data"
      onsubmit="return submitParsing(this)">
  <input type="hidden" name="m" value="dPfiles"/>
  <input type="hidden" name="a" value="testParseFile"/>
  <input type="hidden" name="type_extraction" value="{{$type}}"/>

  <table class="main form">
    <tr>
      <th class="narrow">
        <h2>{{tr}}dPfiles-file-parser{{/tr}}</h2>
      </th>
    </tr>
    <tr>
      <th>
        <label for="file" title="Fichier">{{tr}}File{{/tr}}</label>
      </th>
      <td>
          {{mb_include module=system template=inc_inline_upload lite=true multi=false}}
      </td>
    </tr>
    <tr>
      <th class="narrow">
        <h2>{{tr}}Config{{/tr}}</h2>
      </th>
    </tr>
    <tr>
      <th>
        <label for="file" title="Profil">Profil</label>
      </th>
      <td>
        <select name="profile">
            {{foreach from=$profiles item=_profile}}
              <option value="{{$_profile}}">{{tr}}CExtractDataLog.profile.{{$_profile}}{{/tr}}</option>
            {{/foreach}}
        </select>
      </td>
    </tr>
    <tr>
      <td colspan="2" class="button">
        <button id="import_button" type="submit" class="import">
            {{tr}}common-action-Parse{{/tr}}
        </button>
      </td>
    </tr>
  </table>
</form></br></br>
</br>

<div id="result_parse_files"></div>
