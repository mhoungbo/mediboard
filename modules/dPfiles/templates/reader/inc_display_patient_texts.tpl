{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<style>
  :target {
    background-color: #feebcd;
  }
</style>

<div style="text-align: left;">
  <h2>Sommaire</h2>
  <ul>
  {{foreach from=$texts key=ami_id item=_text}}
    <li>
      <a href="#zpat-{{$ami_id}}">{{$_text.title}}</a> ({{$_text.items|@count}})
    </li>
  {{/foreach}}
  </ul>

{{foreach from=$texts key=ami_id  item=_text}}
  <div id="zpat-{{$ami_id}}">
    <h2>{{$_text.title}}</h2>

    <table class="main tbl">
      {{foreach from=$_text.items item=_item}}
      <tr>
        <th class="narrow category" style="vertical-align: top;">{{$_item.date|date_format:$conf.datetime}}</th>
        <td><pre>{{$_item.value}}</pre></td>
      </tr>
      {{/foreach}}
    </table>
  </div>
{{/foreach}}
</div>
