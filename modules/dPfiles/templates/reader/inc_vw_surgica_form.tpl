{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=readonly value=true}}

<style>
  .surgica-form * {
    box-sizing: border-box;
    margin: 0;
  }

  .surgica-form input:disabled,
  .surgica-form textarea:disabled,
  .surgica-form select:disabled {
    color: black;
    opacity: 1.0 !important;
  }

  .delphi-object {
    position: absolute;
    vertical-align: middle;
  }

  .delphi-object-TDynaNumeric,
  .delphi-object-TDynaEdit,
  .delphi-object-TDynaCombo {
    background-color: #fff;
    border: 1px solid #ccc;
  }

  .delphi-object-TDynaNumeric input,
  .delphi-object-TDynaEdit input,
  .delphi-object-TDynaCombo select {
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    position: absolute;
    border: none;
    margin: 0;
  }

  .delphi-object-TScrollBox,
  .delphi-object-TDynaScrollBox,
  .delphi-object-TDynaDossier{
    position: relative;
  }

  .delphi-object-TScrollBox,
  .delphi-object-TDynaScrollBox {
    overflow: scroll;
    height: 800px;
  }

  .delphi-object-TDynaPage {
    position: relative;
    width: 100%;
    height: 100%;
    border: 1px solid #ccc;
    margin: 2px;
    background-color: #fff;
  }

  .delphi-alignment-taCenter {
    text-align: center;
  }

  .delphi-font_style-fsBold {
    font-weight: bold;
  }

  .delphi-borderstyle-sbsSunken {
    border: 1px inset;
  }
</style>

<div class="surgica-form">
  {{mb_include module=dPfiles template='reader/inc_vw_surgica_form_field' id=$id object=$object}}
</div> 
