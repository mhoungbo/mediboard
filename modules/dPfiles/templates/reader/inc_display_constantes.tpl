{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function () {
    Control.Tabs.create('resurgences-constantes');
  });
</script>

<table class="main layout">
  <tr>
    <td class="narrow">
      <ul id="resurgences-constantes" class="control_tabs_vertical">
        {{foreach from=$content key=_guid item=_constantes}}
          {{assign var=sejour value=$sejours.$_guid}}

            <li style="white-space: nowrap;">
              <a href="#resurgences-const-{{$_guid}}">
                <span onmouseover="ObjectTooltip.createEx(this, '{{$sejour->_guid}}');">
                  {{$sejour}}
                </span>
              </a>
            </li>
        {{/foreach}}
      </ul>
    </td>

    <td>
      {{foreach from=$content key=_guid item=_constantes}}
        {{assign var=sejour value=$sejours.$_guid}}

        <div id="resurgences-const-{{$_guid}}" style="display: none;">
          <h2>
            <span onmouseover="ObjectTooltip.createEx(this, '{{$sejour->_guid}}');">
              {{$sejour}}
            </span>
          </h2>

          {{foreach from=$_constantes key=_k item=_constante}}
            <table class="main tbl">
              <tr>
                <th class="title" colspan="{{$_constante.values|@count}}">
                  {{$_constante.date}}
                </th>
              </tr>

              <tr>
                {{foreach from=$_constante.values key=_field item=_value}}
                  <th>{{tr}}CResUrgences-legend-{{$_field}}{{/tr}}</th>
                {{/foreach}}
              </tr>

              <tr>
                {{foreach from=$_constante.values key=_field item=_value}}
                  <td style="text-align: center;">{{$_value}}</td>
                {{/foreach}}
              </tr>

              {{if $_constante.text}}
                <tr>
                  <td class="text" colspan="{{$_constante.values|@count}}">
                    <hr />
                    {{$_constante.text|nl2br}}
                  </td>
                </tr>
              {{/if}}

              {{if $_constante.info}}
                <tr>
                  <td class="center" colspan="{{$_constante.values|@count}}">
                    <fieldset>
                      <legend>{{tr}}common-Information|pl{{/tr}}</legend>

                      <table class="main tbl">
                        {{foreach name=constantes_info from=$_constante.info item=_info}}
                          {{if $smarty.foreach.constantes_info.first}}
                            <tr>
                              <th></th>

                              {{foreach from=$_info.values key=_field item=_value}}
                                <th>{{tr}}CResUrgences-legend-{{$_field}}{{/tr}}</th>
                              {{/foreach}}
                            </tr>
                          {{/if}}
                        {{/foreach}}

                        {{foreach name=constantes_info from=$_constante.info item=_info}}
                          <tr>
                            <th>{{$_info.heure}}</th>

                            {{foreach from=$_info.values key=_field item=_value}}
                              <td style="text-align: center;">{{$_value}}</td>
                            {{/foreach}}
                          </tr>
                        {{/foreach}}
                      </table>
                    </fieldset>
                  </td>
                </tr>
              {{/if}}
            </table>
          {{/foreach}}

        </div>
      {{/foreach}}
    </td>
  </tr>
</table>
