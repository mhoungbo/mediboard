{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="main form">
  <tr>
    <th class="title" colspan="2">{{$type}} - v.{{$version}}</th>
  </tr>
  {{foreach from=$zones key=_key item=_zone}}
    <tr>
      <th>{{$_key}}</th>
      <td class="text" style="word-wrap: break-word; overflow-wrap:break-word">
        {{if $_zone.raw === "0"}}
          {{tr}}No{{/tr}}
        {{elseif $_zone.raw === "1"}}
          {{tr}}Yes{{/tr}}
        {{else}}
          {{$_zone.raw|spancate:100}}
        {{/if}}
      </td>
    </tr>
  {{/foreach}}
</table>
