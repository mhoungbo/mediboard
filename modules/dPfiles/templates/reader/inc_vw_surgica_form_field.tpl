{{*
 * @package Mediboard\Surgica
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<div id="surgica-form-{{$object->id}}"
     class="delphi-object delphi-object-{{$object->classname}}

     {{foreach from=","|explode:"alignment,font_style,borderstyle" item=prop}}
       {{if isset($object->$prop|smarty:nodefaults)}}
         {{if !is_array($object->$prop)}}
           delphi-{{$prop}}-{{$object->$prop}}
         {{else}}
           {{foreach from=$object->$prop item=_value}}
             delphi-{{$prop}}-{{$_value}}
           {{/foreach}}
         {{/if}}
       {{/if}}
     {{/foreach}}"

     style="
         {{foreach from=","|explode:"width,height,top,left" item=prop}}
           {{if isset($object->$prop|smarty:nodefaults) && $object->$prop}}
             {{if $object->$prop}}{{$prop}}: {{$object->$prop}}px;{{/if}}
           {{/if}}
         {{/foreach}}

         {{if $object->csscolor}} background-color: {{$object->csscolor}};{{/if}}
         ">

  {{if $object->classname == "TDynaLabel" && isset($object->caption|smarty:nodefaults) && $object->caption != $object->id}}
  <span style="white-space: pre;">{{$object->caption}}</span>
  {{/if}}

  {{if isset($object->children|smarty:nodefaults)}}
    {{foreach from=$object->children item=_object key=_id}}
      {{mb_include module=surgica template=inc_vw_surgica_form_field id=$_id object=$_object}}
    {{/foreach}}
  {{/if}}

  {{if isset($object->picture_data|smarty:nodefaults)}}
    <img src="{{$object->getImageDataUri()}}" />
  {{/if}}

  {{if $object->classname == "TDynaRadio"}}
    {{if isset($object->columns|smarty:nodefaults)}}
      <table class="main layout">
        <tr>
          {{foreach from=$object->items_strings item=_string}}
            <td style="text-align: center">
              <label style="white-space: nowrap; display: inline-block; height: 24px; line-height: 24px;">
                <input type="radio" name="surgica-element-{{$object->id}}" {{if $object->valeur == $_string}}checked{{/if}} {{if $readonly}} disabled {{/if}} /> {{$_string}}
              </label>
            </td>
          {{/foreach}}
        </tr>
      </table>
    {{else}}
      <table class="main layout" style="height: 100%;">
        {{foreach from=$object->items_strings item=_string}}
          <tr>
            <td style="text-align: center">
              <label style="white-space: nowrap; display: inline-block; height: 24px; line-height: 24px;">
                <input type="radio" name="surgica-element-{{$object->id}}" {{if $object->valeur == $_string}}checked{{/if}} {{if $readonly}} disabled {{/if}} /> {{$_string}}
              </label>
            </td>
          </tr>
        {{/foreach}}
      </table>
    {{/if}}
  {{/if}}

  {{if $object->classname == "TDynaCheck"}}
    <label style="white-space: nowrap; display: inline-block; height: 24px; line-height: 24px;">
      <input type="checkbox" name="surgica-element-{{$object->id}}" {{if $object->valeur == "1"}} checked {{/if}} {{if $readonly}} disabled {{/if}} /> {{$object->caption}}
    </label>
  {{/if}}

  {{if $object->classname == "TDynaEdit" || $object->classname == "TDynaNumeric"}}
    <input type="text" name="surgica-element-{{$object->id}}" {{if $readonly}} disabled {{/if}}
        {{if isset($object->valeur|smarty:nodefaults)}}
          value="{{$object->valeur}}"
        {{/if}}
        />
  {{/if}}

  {{if $object->classname == "TDynaVariable"}}
    {{if isset($object->valeur|smarty:nodefaults)}}
      <span style="white-space: pre;">{{$object->valeur}}</span>
    {{/if}}
  {{/if}}

  {{if $object->classname == "TDynaCombo"}}
    <select name="surgica-element-{{$object->id}}" {{if $readonly}} disabled {{/if}}>
      {{foreach from=$object->items_strings item=_string}}
        <option {{if $object->valeur == $_string}} selected {{/if}}>{{$_string}}</option>
      {{/foreach}}
    </select>
  {{/if}}
</div> 