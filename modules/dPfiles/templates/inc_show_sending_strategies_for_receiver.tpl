{{*
 * @package Mediboard\Files
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{foreach from=$strategy_objects item=strategy}}
  <tr>
    <th>
      <label># {{$strategy->strategy_index}}</label>
    </th>
    <td>
        {{$strategy->_view}}

        <form name="form_deletion-{{$strategy->_guid}}" method="post">
            {{mb_class object=$strategy}}
            {{mb_key   object=$strategy}}

          <button type="button" class="trash notext" onclick="confirmDeletion(this.form, {ajax: 1}, {onComplete: () => {Control.Modal.refresh()}})"></button>
        </form>
    </td>
  </tr>
{{/foreach}}
