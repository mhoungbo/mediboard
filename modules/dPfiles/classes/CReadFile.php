<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CMbObject;
use Ox\Core\CStoredObject;

/**
 * Manage the acknowledgment of reading on docitems
 */
class CReadFile extends CMbObject
{
    /** @var int */
    public $file_read_id;

    /** @var string */
    public $object_class;

    /** @var int */
    public $object_id;

    /** @var string */
    public $datetime;

    /** @var int */
    public $user_id;

    /** @var CDocumentItem */
    public $_ref_docitem;

    /**
     * @inheritdoc
     */
    public function getSpec()
    {
        $spec           = parent::getSpec();
        $spec->table    = "files_read";
        $spec->key      = "file_read_id";
        $spec->loggable = false;

        return $spec;
    }

    /**
     * Get the properties of our class as strings
     */
    public function getProps(): array
    {
        $props                 = parent::getProps();
        $props['object_class'] = 'enum list|CFile|CCompteRendu';
        $props['object_id']    = 'ref notNull class|CMbObject meta|object_class back|read_files';
        $props['datetime']     = 'dateTime notNull';
        $props['user_id']      = 'ref class|CUser notNull back|read_files';

        return $props;
    }

    /**
     * @throws Exception
     */
    public static function getUnread(array $contexts): array
    {
        $notReadFiles = [];

        /** @var CMbObject $context */
        foreach ($contexts as $context) {
            $context->loadRefsDocItems();

            $notReadFiles[$context->_id] = [];

            CStoredObject::massCountBackRefs($context->_ref_documents, 'read_files');
            CStoredObject::massCountBackRefs($context->_ref_files, 'read_files');

            foreach ([$context->_ref_documents, $context->_ref_files] as $docitems_by_class) {
                foreach ($docitems_by_class as $docitem) {
                    if (!$docitem->_count['read_files']) {
                        $notReadFiles[$context->_id][] = $docitem;
                    }
                }
            }
        }

        return $notReadFiles;
    }

    public static function getAllUnreadFilesForSejours(array $sejours): array
    {
        $not_read_files = CReadFile::getUnread($sejours);
        CStoredObject::massLoadBackRefs($sejours, "operations");
        foreach ($sejours as $_sejour) {
            $_sejour->loadRefsOperations();
            $context_interv = [];
            foreach ($_sejour->_ref_operations as $_operation) {
                $context_interv[] = $_operation;
            }
            if (!array_key_exists($_sejour->_id, $not_read_files)) {
                $not_read_files[$_sejour->_id] = [];
            }
            foreach (CReadFile::getUnread($context_interv) as $_unread_files_by_operations) {
                foreach ($_unread_files_by_operations as $_unread_file) {
                    $not_read_files[$_sejour->_id][] = $_unread_file;
                }
            }
        }

        return $not_read_files;
    }

    /**
     * @throws Exception
     */
    public function loadRefDocItem(): CDocumentItem
    {
        return $this->_ref_docitem = $this->loadFwdRef('object_id', true);
    }

    /**
     * @inheritDoc
     */
    public function store(): ?string
    {
        if ($msg = parent::store()) {
            return $msg;
        }

        $file_traceability = $this->loadRefDocItem()->loadRefLastFileTraceability();

        if ($file_traceability->_id) {
            if ($msg = $file_traceability->store()) {
                return $msg;
            }
        }

        return null;
    }
}
