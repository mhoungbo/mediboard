<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Controllers\Legacy;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CLegacyController;
use Ox\Core\CValue;
use Ox\Core\CView;
use Ox\Mediboard\ExtractData\Interpreters\IdInterpreter;
use Ox\Mediboard\Files\Extractor;

class IdInterpreterLegacyController extends CLegacyController
{
    /**
     * @throws Exception
     */
    public function idInterpreter(): void
    {
        $this->checkPermEdit();

        $patient_id = CView::get('patient_id', 'ref class|CPatient');

        CView::checkin();

        $this->renderSmarty('id_interpreter', ['patient_id' => $patient_id]);
    }

    /**
     * @throws Exception
     */
    public function doIdInterpreter(): void
    {
        $this->checkPermEdit();
        $file = CValue::files("formfile");
        CView::checkin();

        // No files
        if (!$file || (count($file["tmp_name"]) === 0)) {
            echo json_encode(["error" => "no_files"]);
            CApp::rip();
        }

        $file_path      = $file["tmp_name"][0];
        $file_name      = $file["name"][0];
        $file_extension = $file["type"][0];

        // Not an image
        if (strpos($file_extension, "image") !== 0) {
            echo json_encode(["error" => "not_an_image"]);
            CApp::rip();
        }

        $data = [];

        // Use exchange source for extract data service
        try {
            $picture_data = Extractor::extract(
                Extractor::PROFILE_ID_PICTURE,
                $file_path,
                $file_name,
                $file_extension
            );
            $mrz_data     = Extractor::extract(
                Extractor::PROFILE_MRZ,
                $file_path,
                $file_name,
                $file_extension
            );
        } catch (Exception $e) {
            echo json_encode(["error" => $e->getMessage()]);
            CApp::rip();
        }

        // Get text images
        try {
            $data = array_merge($mrz_data['data']['attributes'], $picture_data['data']['attributes']);
        } catch (Exception $e) {
            echo json_encode(["error" => "$e"]);
            CApp::rip();
        }

        // No data error
        if (!$data) {
            echo json_encode(["error" => "an_error_occured"]);
            CApp::rip();
        }

        // Additional text treatment
        if (isset($data["sex"])) {
            $data["sex"] = strtolower($data["sex"]);
            if (!in_array($data["sex"], ["f", "m"])) {
                $data["sex"] = "";
            }
        }

        if (isset($data["birthdate"])) {
            $data["birthdate"] = IdInterpreter::convertMRZBirth($data['birthdate']);
        }

        echo json_encode($data);
        CApp::rip();
    }
}
