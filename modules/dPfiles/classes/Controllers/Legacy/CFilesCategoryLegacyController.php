<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Controllers\Legacy;

use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CView;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Files\CFilesCategory;
use Ox\Mediboard\Files\CFilesCategoryDispatcher;
use Ox\Mediboard\Files\CFilesCategoryToReceiver;
use Ox\Mediboard\Files\Traces\CFilesSendToReceiverStrategy;

class CFilesCategoryLegacyController extends CLegacyController
{
    public function vw_dispatch_docs(): void
    {
        $this->checkPermAdmin();

        $cat_id = CView::getRefCheckEdit('cat_id', 'ref class|CFilesCategory notNull');

        CView::checkin();

        CView::enforceSlave();

        $cat = CFilesCategory::findOrFail($cat_id);
        if ($cat->group_id !== null) {
            CAppUI::stepAjax('CFilesCategoryDispatcher-Error-Only files without group can be dispatch', UI_MSG_ERROR);
        }

        $dispatcher = new CFilesCategoryDispatcher($cat);
        $stats      = $dispatcher->getStats();

        $this->renderSmarty(
            'files_category_dispatch/vw_dispatch_docs',
            [
                'stats' => $stats,
                'cat'   => $cat,
            ]
        );
    }

    public function do_dispatch_files_cat(): void
    {
        $this->checkPermAdmin();

        $cat_id   = CView::postRefCheckEdit('cat_id', 'ref class|CFilesCategory notNull');
        $group_id = CView::postRefCheckEdit('group_id', 'ref class|CGroups notNull');

        CView::checkin();

        CApp::setMemoryLimit('2048M');
        CApp::setTimeLimit(300);

        $cat = CFilesCategory::findOrFail($cat_id);
        if ($cat->group_id !== null) {
            CAppUI::stepAjax('CFilesCategoryDispatcher-Error-Only files without group can be dispatch', UI_MSG_ERROR);
        }

        $group = CGroups::findOrFail($group_id);

        $dispatcher = new CFilesCategoryDispatcher($cat);
        foreach ($dispatcher->dispatch($group) as $msg) {
            CAppUI::setMsg(...$msg);
        }

        echo CAppUI::getMsg();

        CApp::rip();
    }

    public function showSendingStrategies(): void
    {
        $this->checkPermAdmin();
        $related_receiver_id = CView::getRefCheckEdit('related_receiver_id', 'ref class|CFilesCategoryToReceiver');
        CView::checkin();

        $related_receiver = new CFilesCategoryToReceiver();
        $related_receiver->load($related_receiver_id);

        $strategy_objects = $related_receiver->loadRefFilesSendReceiverStrategies();

        // enable add strategy only if exist compatible strategies
        $could_add_strategy = true;
        if ($strategy_objects) {
            $last_strategy_object = reset($strategy_objects);
            $could_add_strategy   = count($last_strategy_object->getCompatibleStrategies()) > 0;
        }

        $this->renderSmarty(
            'inc_show_sending_strategies_for_receiver',
            [
                'related_receiver'   => $related_receiver,
                'strategy_objects'   => $strategy_objects,
                'could_add_strategy' => $could_add_strategy,
            ]
        );
    }

    public function refreshSendingStrategies(): void
    {
        $this->checkPermAdmin();
        $related_receiver_id = CView::getRefCheckEdit('related_receiver_id', 'ref class|CFilesCategoryToReceiver');
        CView::checkin();

        $related_receiver = new CFilesCategoryToReceiver();
        $related_receiver->load($related_receiver_id);

        $strategies_stored = $related_receiver->loadRefFilesSendReceiverStrategies();
        $strategy_sending  = new CFilesSendToReceiverStrategy();
        if ($strategies_stored) {
            /** @var CFilesSendToReceiverStrategy $strategy_sending */
            $strategy_sending = end($strategies_stored);
            $strategies       = [];
            foreach ($strategy_sending->getCompatibleStrategies() as $strategy_object) {
                $strategies[$strategy_object->getStrategyName()] = get_class($strategy_object);
            }
        } else {
            $strategies = $strategy_sending->getAvailableStrategies();
        }

        $available_strategy_objects = array_map(function ($strategy_class) {
            return new $strategy_class();
        }, $strategies);

        $this->renderSmarty(
            'inc_add_sending_strategies_for_receiver',
            [
                'related_receiver'    => $related_receiver,
                'strategies'          => array_keys($strategies),
                'strategy_objects'    => $available_strategy_objects,
                'form_name'           => "editRelatedReceiverToCategory-" . $related_receiver->_guid,
                'next_strategy_index' => count($strategies_stored) ? count($strategies_stored) + 1 : 1,
            ]
        );
    }
}
