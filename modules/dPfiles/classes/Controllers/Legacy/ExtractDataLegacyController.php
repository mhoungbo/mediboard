<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Controllers\Legacy;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CValue;
use Ox\Core\CView;
use Ox\Mediboard\ExtractData\ExtractorFactory;
use Ox\Mediboard\Files\Extractor;
use Ox\Mediboard\Mediusers\CMediusers;

class ExtractDataLegacyController extends CLegacyController
{
    /**
     * @throws Exception
     */
    public function testParseFile(): void
    {
        $this->checkPermRead();
        $file            = CValue::files('formfile');
        $profile         = CView::post("profile", "str");
        $type_extraction = CView::post("type_extraction", "str");
        CView::checkin();

        $result = $this->parseFile($file, $profile, $type_extraction);

        $content = json_encode($result, JSON_PRETTY_PRINT);

        if ($content === false) {
            throw new Exception();
        }

        $this->renderSmarty(
            'extractor/vw_extractor_test_result',
            [
                "results" => $content,
            ]
        );
    }

    /**
     * @param mixed       $file
     * @param string      $profile
     * @param string|null $type_extraction
     *
     * @return array
     * @throws Exception
     */
    public function parseFile($file, string $profile, string $type_extraction = null): array
    {
        if (!$file) {
            CAppUI::stepAjax('dPfiles-extractor-No file to decode', UI_MSG_ERROR);
        }

        if (($profile === Extractor::PROFILE_MRZ) || ($profile === Extractor::PROFILE_CONTENT)) {
            $host = @CAppUI::conf("extractData tika host");
            $port = @CAppUI::conf("extractData tika port");

            if (!$host || !$port) {
                CAppUI::stepAjax('dPfiles-extractor-No config found', UI_MSG_ERROR);
            }
        }

        $file_path = $file["tmp_name"][0];
        $file_name = $file["name"][0];
        $extension = $file["type"][0];

        if (!file_exists($file_path) || !$file_path) {
            CAppUI::stepAjax(CAppUI::tr('CFile-not-exists', $file_name), UI_MSG_ERROR);
        }

        $options = [
            "extension" => strtolower(explode('/', mime_content_type($file_path))[1]),
            "file_name" => $file_name,
        ];

        $result = [];

        if ($type_extraction === 'source') {
            try {
                $result['content'] = Extractor::extract($profile, $file_path, $file_name, $extension);
            } catch (Exception $e) {
                CAppUI::stepAjax($e->getMessage());
            }
        } else {
            // Return a specific extractor depending on parameters passed in factory
            $extractor = ExtractorFactory::create($profile);

            try {
                $result['content'] = $extractor->extract($file_path, CMediusers::get()->_id, $options);
            } catch (Exception $e) {
                CAppUI::stepAjax(CAppUI::tr('extractData-msg-Error occured when parsing %s', $e->getMessage()));
            }
        }

        return $result;
    }
}
