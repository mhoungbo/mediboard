<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Files\CFile;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * CRUD for CFile items
 */
class FileController extends Controller
{
    /**
     * List the files for the chosen context
     * Permissions are checked on each files
     *
     * @throws InvalidArgumentException|ApiException
     * @throws Exception
     * @api
     */
    public function list(string $resource_type, string $resource_id, RequestApi $request): Response
    {
        /** @var CMbObject $context */
        if (!(($context = $this->getObjectFromResourceTypeAndId($resource_type, $resource_id)) instanceof CMbObject)) {
            throw new ApiException($this->translator->tr("CFile-error-%s can not contains files", $resource_type));
        }

        // Load all files of requested context
        $context->loadRefsFiles([], $request->getRequest()->query->getBoolean('with_cancelled'));
        $files = $context->_ref_files;

        // Remove the relation context from the asked relations because they are already massloaded
        $this->massLoadRelations($files, array_diff($request->getRelations(), [CFile::RELATION_CONTEXT]));

        /** @var Collection $collection */
        $collection = Collection::createFromRequest($request, $files);

        // Check perm on each files
        /** @var Item $item */
        foreach ($collection as $item) {
            /** @var CFile $file */
            $file = $item->getDatas();

            if ($file->getPerm(PERM_READ)) {
                $file->_fwd['object_id'] = $context;
                $item->addLinks($file->buildThumbnailLink());

                if ($request->getRequest()->query->getBoolean('with_file_content')) {
                    $item->addAdditionalDatas(['file_content' => $file->getDataURI()]);
                }
            }
        }

        return $this->renderApiResponse($collection);
    }

    /**
     * Display a single file
     *
     * @throws ApiException
     * @throws Exception
     * @api
     */
    public function show(CFile $file, RequestApi $request): Response
    {
        $item = Item::createFromRequest($request, $file);

        if ($request->getRequest()->query->getBoolean('with_file_content')) {
            $item->addAdditionalDatas(['file_content' => $file->getDataURI()]);
        }

        $item->addLinks($file->buildThumbnailLink());

        return $this->renderApiResponse($item);
    }

    /**
     * Update a file.
     *
     * @throws ApiException|CMbException
     * @api
     */
    public function update(CFile $file, RequestApi $request): Response
    {
        /** @var CFile $file_from_request */
        $file_from_request = $request->getModelObject($file, [], ['annule', 'file_name']);

        return $this->renderApiResponse(
            Item::createFromRequest($request, $this->storeObject($file_from_request, false))
        );
    }

    /**
     * Delete the file.
     *
     * @throws CMbException
     * @api
     */
    public function delete(CFile $file): Response
    {
        $this->deleteObject($file);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Massload the relations from the files.
     *
     * @throws Exception
     */
    private function massLoadRelations(array $files, array $relations): void
    {
        if (
            in_array(RequestRelations::QUERY_KEYWORD_ALL, $relations)
        ) {
            CStoredObject::massLoadFwdRef($files, 'object_id');
            CStoredObject::massLoadFwdRef($files, 'file_category_id');
            CStoredObject::massLoadFwdRef($files, 'nature_file_id');

            return;
        }

        if (in_array(CFile::RELATION_CONTEXT, $relations)) {
            CStoredObject::massLoadFwdRef($files, 'object_id');
        }

        if (in_array(CFile::RELATION_CATEGORY, $relations)) {
            CStoredObject::massLoadFwdRef($files, 'file_category_id');
        }

        if (in_array(CFile::RELATION_NATURE_FILE, $relations)) {
            CStoredObject::massLoadFwdRef($files, 'nature_file_id');
        }
    }
}
