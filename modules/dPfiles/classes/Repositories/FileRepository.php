<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Repositories;

use Exception;
use Ox\Mediboard\Files\CDocumentItem;
use Ox\Mediboard\Files\CFile;

class FileRepository
{
    private CFile $model;

    public function __construct()
    {
        $this->model = new CFile();
    }

    /**
     * @return CFile[]
     * @throws Exception
     */
    public function findCertificateRelatedFiles(): array
    {
        CDocumentItem::$check_send_problem = false;

        $file               = clone $this->model;
        $file->file_name    = 'certificat_signature.crt';
        $file->private      = 1;
        $file->object_class = 'CMediusers';

        $files = $file->loadMatchingList();

        CDocumentItem::$check_send_problem = true;

        return (is_array($files)) ? $files : [];
    }

    public function findCertificateFile(int $user_id): ?CFile
    {
        CDocumentItem::$check_send_problem = false;

        $file               = clone $this->model;
        $file->file_name    = 'certificat_auth.crt';
        $file->private      = 1;
        $file->object_class = 'CMediusers';
        $file->object_id    = $user_id;
        $file->loadMatchingObjectEsc();

        CDocumentItem::$check_send_problem = true;

        if ($file && $file->_id) {
            return $file;
        }

        return null;
    }
}
