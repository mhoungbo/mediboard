<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CStoredObject;
use Ox\Core\Handlers\ObjectHandler;
use Ox\Core\Logger\LoggerLevels;
use Ox\Core\Module\CModule;
use Ox\Interop\Dmp\CDMPSas;
use Ox\Mediboard\Cabinet\CConsultAnesth;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Files\Traces\CalculatorDateSending;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Prescription\CPrescription;

/**
 * Domain handler
 */
class CFileTraceabilityHandler extends ObjectHandler
{
    protected static array $handled = ["CFile", "CCompteRendu"];

    public bool $create = false;

    /**
     * @inheritdoc
     */
    public static function isHandled(CStoredObject $object)
    {
        return in_array($object->_class, self::$handled);
    }

    /**
     * @inheritdoc
     */
    public function onBeforeStore(CStoredObject $object)
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        if (!$object->_id) {
            $this->create = true;
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function onAfterStore(CStoredObject $object): bool
    {
        if (!$this->isHandled($object)) {
            return false;
        }

        /** @var CDocumentItem $docItem */
        $docItem = $object;

        // Si on vient de retirer le type doc dmp => on enl�ve les traces
        if (CModule::getActive('dmp')) {
            $old_object = $docItem->loadOldObject();
            if ($old_object && $old_object->_id && $old_object->type_doc_dmp && !$docItem->type_doc_dmp) {
                CFileTraceability::deleteTrace($docItem, CDMPSas::getTag());
            }
        }

        if ($docItem->annule) {
            CFileTraceability::deleteTrace($docItem);

            return false;
        }

        // Document non finalis� dans Mediboard
        if (!$docItem->send) {
            return false;
        }

        // Si pas de cat�gorie on ne peut pas cr�er de trace
        if (!$docItem->file_category_id) {
            return false;
        }

        $file_category = $docItem->loadRefCategory();

        // Si la cat�gorie n'est pas �ligible � une remont�e d'alerte
        if (!$file_category->send_auto) {
            return false;
        }

        $where                                      = [];
        $where["files_category_to_receiver.active"] = "= '1'";
        if (!$file_category->countRelatedReceivers($where) > 0) {
            return false;
        }

        $target = $docItem->loadTargetObject();
        if (
            !($target instanceof CSejour) && !($target instanceof CConsultation)
            && !($target instanceof CConsultAnesth) && !($target instanceof COperation)
            && !($target instanceof CPrescription)
        ) {
            return false;
        }

        // Dans le cas d'un compte-rendu, il faut envoyer au sas que si c'est une cr�ation ou que le contenu a chang�
        if (($docItem instanceof CCompteRendu) && $docItem->_old->_id && !$docItem->fieldModified('version')) {
            return false;
        }

        $calculator_date_sending = new CalculatorDateSending();
        foreach ($file_category->loadRefRelatedReceivers($where) as $_related_receivers) {
            /** @var CFilesCategoryToReceiver $_related_receivers */
            $receiver = $_related_receivers->loadRefReceiver();

            if (!$receiver->_id) {
                continue;
            }

            $file_traceability = CFileTraceability::createTrace($docItem, $receiver);
            if (!$file_traceability) {
                continue;
            }

            $send_strategies = $_related_receivers->loadRefFilesSendReceiverStrategies();
            if (empty($send_strategies)) {
                continue;
            }

            // try to calculate a date for send file after this date
            $datetime_to_send = $calculator_date_sending->determineDatetime($object, $send_strategies);
            if ($datetime_to_send) {
                $file_traceability->send_after = $datetime_to_send;
                if ($msg = $file_traceability->store()) {
                    CApp::log($msg, ["traceability" => $file_traceability], LoggerLevels::LEVEL_ERROR);
                }
            }
        }

        return true;
    }
}
