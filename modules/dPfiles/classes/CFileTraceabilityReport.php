<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files;

use Ox\Core\CAppUI;
use Ox\Core\CStoredObject;
use Ox\Interop\Eai\CItemReport;
use Ox\Interop\Eai\CReport;

class CFileTraceabilityReport extends CReport
{
    public function addItemSearch(CStoredObject $object, int $severity): CItemReport
    {
        $item_search = new CItemReport(CAppUI::tr($object->_class), $severity);
        $this->addItem($item_search);

        return $item_search;
    }
}
