<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files;

use Exception;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\Contracts\Client\HTTPClientInterface;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Mediboard\System\CSourceHTTP;

class Extractor
{
    public const TYPE = "extracted_data";

    public const PROFILE_MRZ        = "mrz";
    public const PROFILE_ID_PICTURE = "id_picture";
    public const PROFILE_QR_CODE    = "qr_code";
    public const PROFILE_CONTENT    = "content";

    // Profiles
    public const PROFILES = [
        self::PROFILE_MRZ,
        self::PROFILE_ID_PICTURE,
        self::PROFILE_QR_CODE,
        self::PROFILE_CONTENT,
    ];

    private const EXTRACT_PATH = "/api/extractData";

    /**
     * Extract a file using HTTP source
     *
     * @param string      $profile
     * @param string      $file_path
     * @param string|null $original_file_name
     * @param string|null $extension
     *
     * @return array
     * @throws Exception
     */
    public static function extract(
        string $profile,
        string $file_path,
        string $original_file_name = null,
        string $extension = null
    ): array {
        if (!self::isSourceAvailable()) {
            throw new Exception(CAppUI::tr('dPfiles-extractor-No source found'));
        }

        $item = new Item(
            [
                'file_path'      => $file_path,
                'file_name'      => $original_file_name,
                'file_extension' => $extension,
            ]
        );
        $item->setType(self::TYPE);

        try {
            $response = (self::getClient())->request(
                "POST",
                (self::getSource())->host . self::EXTRACT_PATH,
                [
                    'query' => ['profile' => $profile],
                    'json'  => json_encode($item),
                ]
            );
        } catch (Exception $e) {
            throw new Exception($e);
        }

        $api_response = json_decode((string)$response->getBody(), true);

        if (!$api_response) {
            throw new Exception(CAppUI::tr('dPfiles-extractor-No data to return'));
        }

        return $api_response;
    }

    /**
     * Get the http client
     * @throws CMbException
     * @throws Exception
     */
    public static function getClient(): HTTPClientInterface
    {
        return self::getSource()->getClient();
    }

    /**
     * Get the CSourceHTTP that will be used for request.
     * @throws Exception
     */
    public static function getSource(): CSourceHTTP
    {
        /** @var CSourceHTTP */
        return CExchangeSource::get('Extract Data Service', CSourceHTTP::TYPE, true, null, false);
    }

    /**
     * Tell if the CSourceHTTP has been found.
     * @throws Exception
     */
    public static function isSourceAvailable(): bool
    {
        return (bool)static::getSource()->_id;
    }

    /**
     * Return all profiles of document
     */
    public static function getProfiles(): array
    {
        return self::PROFILES;
    }
}
