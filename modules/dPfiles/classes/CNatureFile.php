<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files;

use DateTime;
use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Patients\CPatient;

/**
 * Entity class defining a file nature.
 *
 * File nature is a way to categorize given `CFile` entities.
 * For example, an ID card, a proof of address, and so on.
 *
 * @package Ox\Mediboard\Files
 */
class CNatureFile extends CStoredObject
{
    /**
     * Defines the `JSON:API` unique resource type of this entity.
     *
     * @var string
     */
    public const RESOURCE_TYPE = 'natureFile';

    /**
     * @var string
     */
    public const RELATION_CONTEXT = 'context';

    /**
     * Defines the unique entity identifier.
     *
     * @var int
     */
    public $nature_file_id;

    /**
     * Defines the related entity identifier.
     *
     * @var int
     */
    public $object_id;

    /**
     * Defines the related entity type.
     *
     * @var string
     */
    public $object_class;

    /**
     * Defines a state that this entity is active, and thus actually categorizing related `CFile` entity.
     *
     * If this state is disabled (false), so related entity is no longer categorized.
     *
     * @var bool
     */
    public $active;

    /**
     * Defines the category type, categorizing related `CFile` entity.
     *
     * 0: `Autre`
     * 1: `Carte Vitale`
     * 2: `Carte de Mutuelle`
     * 3: `Justificatif de Domicile`
     * 4: `Pi�ce d'identit�`
     *
     * @var int
     */
    public $type;

    /**
     * Defines the entity's creation datetime.
     *
     * @var string
     */
    public $create_datetime;

    /**
     * Defines the entity's last update datetime.
     * @var DateTime
     */
    public $last_update;

    /**
     * Defines a `Forward Reference` property, for a related entity based on `object_class` and `object_id` properties.
     *
     * @var CFile|CPatient
     * @see CPatient
     * @see CFile
     */
    public $_ref_object;

    /**
     * @inheritDoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = 'nature_file';
        $spec->key   = 'nature_file_id';

        return $spec;
    }

    /**
     * @inheritDoc
     */
    public function getProps(): array
    {
        $props = parent::getProps();

        $props['object_id']       = 'ref notNull meta|object_class back|nature_file fieldset|default';
        $props['object_class']    = 'enum notNull list|CPatient fieldset|default';
        $props['active']          = 'bool default|1 fieldset|default';
        $props['type']            = 'enum notNull list|0|1|2|3|4 fieldset|default';
        $props['create_datetime'] = 'dateTime notNull fieldset|default';
        $props['last_update']     = 'dateTime notNull fieldset|default';

        return $props;
    }

    /**
     * @inheritDoc
     */
    public function store(): ?string
    {
        if (!$this->_id) {
            $this->create_datetime = $this->last_update = 'now';
        }

        if ($this->objectModified()) {
            $this->last_update = 'now';
        }

        return parent::store();
    }

    /**
     * @inheritDoc
     */
    public function check(): string
    {
        $nature_file_id = $this->loadMatchingObjectEsc();
        /** @var CNatureFile $nature_file */
        $nature_file = self::loadFromGuid("CNatureFile-$nature_file_id");

        if (
            $nature_file
            && $nature_file->type === $this->type
        ) {
            $stored_nature_file       = new self();
            $stored_nature_file->type = $this->type;
            $stored_nature_file_id    = $stored_nature_file->loadMatchingObjectEsc();
            /** @var CNatureFile $stored_nature_file */
            $stored_nature_file = $this::loadFromGuid("CNatureFile-$stored_nature_file_id");

            if (
                $stored_nature_file
                && $stored_nature_file->active
                && $this->type != 0
            ) {
                return 'CNatureFile-msg-check matching type not unique';
            }
        }

        return parent::check();
    }

    /**
     * Creates a `JSON:API` resource, for `context` relation.
     *
     * @return Item|null
     * @throws ApiException
     * @throws Exception
     */
    public function getResourceContext(): ?Item
    {
        $object = $this->loadRefObject();

        if (!$object) {
            return null;
        }

        return new Item($object);
    }

    /**
     * Loads related entity, by `object_class` and `object_id`
     *
     * @return CFile|CPatient
     * @throws Exception
     */
    public function loadRefObject(): ?CStoredObject
    {
        return $this->_ref_object = CMbObject::loadFromGuid("$this->object_class-$this->object_id");
    }
}
