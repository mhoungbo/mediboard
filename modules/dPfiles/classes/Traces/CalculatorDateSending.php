<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Traces;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Cabinet\CConsultAnesth;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Files\CDocumentItem;
use Ox\Mediboard\Files\Traces\SendStrategies\SendStrategyInterface;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\Prescription\CPrescription;

class CalculatorDateSending
{
    public const SUPPORTED_EVENTS = [
        CConsultation::class,
        CSejour::class,
        COperation::class,
    ];

    public const SUPPORTED_TARGET = [
        CConsultAnesth::class,
        CPrescription::class,
        CDocumentItem::class,
        ...self::SUPPORTED_EVENTS,
    ];

    /**
     * @param CStoredObject           $object
     * @param SendStrategyInterface[] $send_strategies
     * @param string|mixed            $value
     *
     * @return string|null
     * @throws CMbException
     * @throws Exception
     */
    public function determineDatetime(CStoredObject $object, array $send_strategies, $value = null): ?string
    {
        $event = $this->determineEvent($object);

        if (!$this->isSupported($event)) {
            throw new CMbException("CalculatorDateSending-msg-not supported context", CAppui::tr($object->_class));
        }

        $calculated_datetime = CMbDT::dateTime();
        foreach ($send_strategies as $send_strategy) {
            // try to calculate the datetime to send
            $calculated_datetime = $send_strategy->determineDatetime($event, $calculated_datetime, $value);

            if ($calculated_datetime === null) {
                break;
            }
        }

        return $calculated_datetime;
    }

    /**
     * @throws Exception
     */
    private function determineEvent(CStoredObject $object): CStoredObject
    {
        $event = $object;
        if ($event instanceof CDocumentItem) {
            $event = $event->loadTargetObject();
        }

        if ($event instanceof CPrescription) {
            $event->loadRefObject();
        }

        if ($event instanceof CConsultAnesth) {
            $event = $event->loadRefConsultation();
        }

        return $event;
    }

    /**
     * Is Calculation of datetime to send is available for event class
     */
    private function isSupported(CStoredObject $event): bool
    {
        foreach (self::SUPPORTED_EVENTS as $event_class) {
            if ($event instanceof $event_class) {
                return true;
            }
        }

        return false;
    }
}
