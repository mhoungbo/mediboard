<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Traces;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Cache;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbException;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Files\CFilesCategoryToReceiver;
use Ox\Mediboard\Files\Traces\SendStrategies\SendStrategyInterface;
use Psr\SimpleCache\InvalidArgumentException;

class CFilesSendToReceiverStrategy extends CStoredObject implements SendStrategyInterface
{
    /**
     * @var int Primary key
     */
    public $files_send_to_receiver_strategy_id;

    public $files_category_to_receiver_id;
    public $strategy;
    public $value;
    public $strategy_index;

    /** @var CFilesCategoryToReceiver */
    public $_ref_files_cat_to_receiver;

    /** @var string */
    public $_time_sending;
    public $_days_sending;
    public $_hours_sending;

    /**
     * @inheritDoc
     */
    public function getSpec()
    {
        $spec                           = parent::getSpec();
        $spec->table                    = "files_send_to_receiver_strategy";
        $spec->key                      = "files_send_to_receiver_strategy_id";
        $spec->uniques["receiver_link"] = ["files_send_to_receiver_strategy_id", "strategy_index"];

        return $spec;
    }

    /**
     * Get the properties of our class as strings
     *
     * @return array
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function getProps()
    {
        $strategy_names                         = array_keys($this->getAvailableStrategies());
        $props                                  = parent::getProps();
        $props["files_category_to_receiver_id"] = "ref class|CFilesCategoryToReceiver back|files_send_strategies";
        $props["strategy_index"]                = "num";
        $props["value"]                         = "str";
        $props["strategy"]                      = "enum list|" . implode('|', $strategy_names);
        $props['_time_sending']                 = 'time';
        $props['_hours_sending']                = 'num';
        $props['_days_sending']                 = 'num';

        return $props;
    }

    /**
     * Load files category
     *
     * @throws Exception
     */
    public function loadRefFilesCategoryToReceiver(): CFilesCategoryToReceiver
    {
        /** @var CFilesCategoryToReceiver */
        return $this->_ref_files_cat_to_receiver = $this->loadFwdRef("files_category_to_receiver_id", true);
    }

    /**
     * Get Strategies available.
     * Each entry has for key the name of strategy (unique) and value the classname of strategy
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function getAvailableStrategies(): array
    {
        $cache = Cache::getCache(Cache::INNER);
        $key   = $this->_class . '-availableStrategies';

        if (!($strategies = $cache->get($key))) {
            $strategies = CClassMap::getInstance()->getClassChildren(SendStrategyInterface::class, false, true);
            foreach ($strategies as $key => $strategy_class) {
                if ($strategy_class !== self::class) {
                    /** @var SendStrategyInterface $strategy */
                    $strategy                                 = new $strategy_class();
                    $strategies[$strategy->getStrategyName()] = $strategy_class;
                }
                unset($strategies[$key]);
            }

            $cache->set($key, $strategies);
        }

        return $strategies;
    }

    /**
     * Get Strategy object
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function getStrategy(): ?SendStrategyInterface
    {
        if (!$this->strategy) {
            return null;
        }

        $strategy = $this->getAvailableStrategies()[$this->strategy] ?? null;

        return $strategy ? new $strategy() : null;
    }

    /**
     * @inheritDoc
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->strategy ? $this->getView($this) : '';
    }

    /**
     * @inheritDoc
     *
     * @throws CMbException
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function getStrategyName(): string
    {
        $strategy = $this->getStrategy();
        if ($strategy === null) {
            throw new CMbException("CFilesSendToReceiverStrategy-msg-invalid strategy");
        }

        return $strategy->getStrategyName();
    }

    /**
     * @inheritDoc
     *
     * @throws CMbException
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function determineDatetime(CStoredObject $event, ?string $ref_datetime = null, $value = null): ?string
    {
        $strategy = $this->getStrategy();
        if ($strategy === null) {
            throw new CMbException("CFilesSendToReceiverStrategy-msg-invalid strategy");
        }

        return $strategy->determineDatetime($event, $ref_datetime, $value ?: $this->value);
    }

    public function store()
    {
        $msg = parent::store();

        CApp::log('message log : ' . $msg);

        return $msg;
    }

    /**
     * @inheritDoc
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function getCompatibleStrategies(): array
    {
        return $this->getStrategy()->getCompatibleStrategies();
    }

    /**
     * @throws InvalidArgumentException
     * @throws CouldNotGetCache
     */
    public function getView(CFilesSendToReceiverStrategy $file_strategy): string
    {
        if (!$file_strategy->strategy) {
            return '';
        }

        return CAppUI::tr(
                "CFilesSendToReceiverStrategy.strategy." . $file_strategy->strategy
            ) . ' ' . $this->getStrategy()->getView($file_strategy);
    }
}
