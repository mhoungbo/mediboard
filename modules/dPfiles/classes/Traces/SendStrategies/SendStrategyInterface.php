<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Traces\SendStrategies;

use Ox\Core\CStoredObject;
use Ox\Mediboard\Files\Traces\CFilesSendToReceiverStrategy;

interface SendStrategyInterface
{
    /**
     * Get the name of strategy
     */
    public function getStrategyName(): string;

    /**
     * Get algorithms compatbile with this for chain result
     *
     * @return SendStrategyInterface[]
     */
    public function getCompatibleStrategies(): array;

    /**
     * Try to calculate a date for send file after this date
     */
    public function determineDatetime(CStoredObject $event, ?string $ref_datetime = null, $value = null): ?string;

    /**
     * Get view of strategy application
     */
    public function getView(CFilesSendToReceiverStrategy $file_strategy): string;
}
