<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Traces\SendStrategies;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Files\Traces\CFilesSendToReceiverStrategy;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;

class AfterEndEventScheduled implements SendStrategyInterface
{
    public const STRATEGY_NAME = 'after_end_event_scheduled';

    /**
     * @inheritDoc
     */
    public function getStrategyName(): string
    {
        return self::STRATEGY_NAME;
    }

    /**
     * The parameter $ref_datetime is not used in this algorithm
     * @inheritDoc
     * @throws Exception
     */
    public function determineDatetime(CStoredObject $event, ?string $ref_datetime = null, $value = null): ?string
    {
        $datetime_from_event = $this->getDatetimeFromEvent($event);
        if (!$datetime_from_event) {
            return null;
        }

        $increase_datetime = null;
        if ($value) {
            $value             = ($value > 0) ? "+$value" : "$value";
            $increase_datetime = "$value HOURS";
        }

        $datetime = CMbDT::dateTime($increase_datetime, $datetime_from_event);

        return ($datetime < CMbDT::dateTime()) ? null : $datetime;
    }

    /**
     * Pick the end date of the medical event
     *
     * @throws Exception
     */
    public static function getDatetimeFromEvent(CStoredObject $event): ?string
    {
        if ($event instanceof CSejour) {
            return $event->sortie;
        }

        if ($event instanceof CConsultation) {
            if (!$event->_ref_plageconsult && $event->_id) {
                $event->loadRefPlageConsult();
            }

            return $event->_date_fin;
        }

        if ($event instanceof COperation) {
            if (!($date_stop = $event->_datetime_reel_fin)) {
                $date_stop = $event->loadRefSejour()->sortie;
            }

            return $date_stop;
        }

        return null;
    }


    /**
     * @inheritDoc
     */
    public function getCompatibleStrategies(): array
    {
        return [new TimeSlotScheduled()];
    }

    public function getView(CFilesSendToReceiverStrategy $file_strategy): string
    {
        return BeforeStartEventScheduled::formatViewHours($file_strategy->value);
    }
}
