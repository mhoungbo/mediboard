<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Traces\SendStrategies;

use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Files\Traces\CFilesSendToReceiverStrategy;

class TimeSlotScheduled implements SendStrategyInterface
{
    public const STRATEGY_NAME = 'time_slot_scheduled';

    /**
     * @inheritDoc
     */
    public function determineDatetime(CStoredObject $event, ?string $ref_datetime = null, $value = null): ?string
    {
        if ($value === null) {
            return null;
        }

        $time_to_send     = CMbDT::time(null, $value);
        $date_to_send     = CMbDT::date(null, $ref_datetime);
        $datetime_to_send = $date_to_send . ' ' . $time_to_send;

        // try to send tomorrow (in function of date given) only when current date === $ref_datetime
        if (($datetime_to_send < CMbDT::dateTime(null, $ref_datetime)) && (CMbDT::date() === $date_to_send)) {
            $datetime_to_send = CMbDT::dateTime('+ 1 DAY', $datetime_to_send);
        }

        // if even tomorrow is already past, it's need to send now
        if ($datetime_to_send <= CMbDT::dateTime()) {
            return null;
        }

        return $datetime_to_send;
    }

    /**
     * @inheritDoc
     */
    public function getStrategyName(): string
    {
        return self::STRATEGY_NAME;
    }

    /**
     * @inheritDoc
     */
    public function getCompatibleStrategies(): array
    {
        return [];
    }

    public function getView(CFilesSendToReceiverStrategy $file_strategy): string
    {
        return "(" . CAppUI::tr("common-from_long") .
            ' ' . CMbDT::time(null, $file_strategy->value) . ")";
    }
}
