<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Traces\SendStrategies;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Files\Traces\CFilesSendToReceiverStrategy;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;

class BeforeStartEventScheduled implements SendStrategyInterface
{
    public const STRATEGY_NAME = 'before_start_event_scheduled';

    /**
     * @inheritDoc
     */
    public function getStrategyName(): string
    {
        return self::STRATEGY_NAME;
    }

    /**
     * The parameter $ref_datetime is not used in this algorithm
     * @inheritDoc
     * @throws Exception
     */
    public function determineDatetime(CStoredObject $event, ?string $ref_datetime = null, $value = null): ?string
    {
        $datetime_from_event = $this->getDatetimeFromEvent($event);
        if (!$datetime_from_event) {
            return null;
        }

        $decrease_datetime = null;
        if ($value) {
            $value             = ($value > 0) ? "-$value" : ("+" . abs($value));
            $decrease_datetime = "$value HOURS";
        }

        $datetime = CMbDT::dateTime($decrease_datetime, $datetime_from_event);

        return ($datetime < CMbDT::dateTime()) ? null : $datetime;
    }

    /**
     * Pick the end date of the medical event
     *
     * @throws Exception
     */
    public static function getDatetimeFromEvent(CStoredObject $event): ?string
    {
        if ($event instanceof CSejour) {
            return $event->entree;
        }

        if ($event instanceof CConsultation) {
            if (!$event->_ref_plageconsult && $event->_id) {
                $event->loadRefPlageConsult();
            }

            return $event->_datetime;
        }

        if ($event instanceof COperation) {
            if (!($date_start = $event->_datetime_best)) {
                $date_start = $event->loadRefSejour()->entree;
            }

            return $date_start;
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getCompatibleStrategies(): array
    {
        return [new TimeSlotScheduled()];
    }

    public function getView(CFilesSendToReceiverStrategy $file_strategy): string
    {
        return self::formatViewHours($file_strategy->value);
    }

    public static function formatViewHours(int $hours): string
    {
        if (!$hours) {
            return '';
        }

        $days  = intval($hours / 24);
        $hours = $hours - ($days * 24);

        $view = "(";
        if ($days > 0) {
            $view .= "$days " . CAppUI::tr('day' . (($days > 1) ? 's' : ''));
        }

        if ($hours) {
            $view .= ($days > 0) ? " " : '';
            $view .= "$hours " . CAppUI::tr('hour' . (($hours > 1) ? 's' : ''));
        }

        return $view . ")";
    }
}
