<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files;

use Ox\Core\CAppUI;
use Ox\Core\FileUtil\DirectoryAccessChecker;
use Ox\Core\Module\Requirements\CRequirementsManager;

class FilesRequirements extends CRequirementsManager
{
    public function checkTimeoutAvailable(): void
    {
        $this->assertTrue(
            (new DirectoryAccessChecker())->isTimeoutAvailable(),
            CAppUI::tr('FilesRequirements-msg-Command timeout must be installed on the server')
        );
    }
}
