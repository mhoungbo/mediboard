<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Reader;

use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;
use Ox\Core\CMbString;
use Ox\Core\CSmartyDP;
use Ox\Core\CStoredObject;
use Ox\Core\FileUtil\CDFMReader;

class ImportFileReader
{
    /**
     * Transforme un fichier XML Resurgences en fichier HTML
     *
     * @param string $content Le contenu � transformer en HTML
     *
     * @return mixed|string
     * @throws Exception
     */
    public function readResurgencesFile(string $content)
    {
        if (!$content) {
            return '';
        }

        $dom = new DOMDocument();
        $dom->loadXML($content);
        $xpath = new DOMXPath($dom);

        $sejours = [];
        $content = [];

        /** @var DOMElement[] $constantes */
        $constantes = $xpath->query('//constante');
        foreach ($constantes as $_constante) {
            $constante = [
                'sejour' => null,
                'date'   => null,
                'values' => [
                    'arret_coeur' => null,
                    'ceton'       => null,
                    'douleur'     => null,
                    'eva_ap'      => null,
                    'eva_av'      => null,
                    'glucose'     => null,
                    'leuco'       => null,
                    'nitrite'     => null,
                    'ph'          => null,
                    'poids'       => null,
                    'proteine'    => null,
                    'sang'        => null,
                    'taille'      => null,
                    'normal'      => null,
                ],
                'text'   => null,
                'info'   => [],
            ];

            $guid = $_constante->getAttribute('sejour');

            if ($guid) {
                $sejour = CStoredObject::loadFromGuid($guid);

                if ($sejour && $sejour->_id) {
                    $constante['sejour']     = $sejour;
                    $sejours[$sejour->_guid] = $sejour;
                }
            }

            $date              = $_constante->getAttribute('modif_date');
            $constante['date'] = ($date) ?: null;

            foreach ($constante['values'] as $_field => $_v) {
                $_value                       = $_constante->getAttribute($_field);
                $constante['values'][$_field] = ($_value) ?: null;
            }

            $text = $xpath->query('texte', $_constante)->item(0);

            $constante['text'] = ($text->nodeValue) ? CMbString::utf8Decode($text->nodeValue) : null;

            $info = $xpath->query('info', $_constante);

            /** @var DOMElement $_info */
            foreach ($info as $_info) {
                $detail = [
                    'heure'  => null,
                    'values' => [
                        'debit_exp'       => null,
                        'diurese'         => null,
                        'echelle_douleur' => null,
                        'etco2'           => null,
                        'freq_card'       => null,
                        'freq_resp'       => null,
                        'glasgow'         => null,
                        'glycemie'        => null,
                        'hb_micromethode' => null,
                        'ht_micromethode' => null,
                        'niveau_douleur'  => null,
                        'poids'           => null,
                        'sat_oxy'         => null,
                        'selles'          => null,
                        'temp'            => null,
                        'tension'         => null,
                        'vomissement'     => null,
                    ],
                ];

                $heure           = $_info->getAttribute('heure');
                $detail['heure'] = ($heure) ?: null;

                foreach ($detail['values'] as $_field => $_v) {
                    $_value                    = $_info->getAttribute($_field);
                    $detail['values'][$_field] = ($_value) ?: null;
                }

                $constante['info'][] = $detail;
            }

            if ($guid && !isset($content[$guid])) {
                $content[$guid] = [];
            }

            $content[$guid][] = $constante;
        }

        $smarty = new CSmartyDP();
        $smarty->assign('content', $content);
        $smarty->assign('sejours', $sejours);

        return $smarty->fetch('reader/inc_display_constantes.tpl');
    }

    /**
     * Transforme un fichier XML AMI en fichier HTML
     *
     * @param string $content Le contenu � transformer en HTML
     *
     * @return mixed|string
     * @throws Exception
     */
    public function readAMIFile($content)
    {
        if (!$content) {
            return '';
        }

        $dom = new DOMDocument();
        $dom->loadXML($content);
        $xpath = new DOMXPath($dom);

        $texts = [];

        /** @var DOMElement[] $zpats */
        $zpats = $xpath->query("/patient_text/zpats/zpat");
        foreach ($zpats as $_zpat) {
            /** @var DOMElement $_zone_sto */
            $_zone_sto = $xpath->query("zone_sto", $_zpat)->item(0);

            if (!$_zone_sto) {
                continue;
            }

            $_zone_id = $_zone_sto->getAttribute("ami_id");
            if (!isset($texts[$_zone_id])) {
                $texts[$_zone_id] = [
                    "title" => CMbString::utf8Decode($_zone_sto->getAttribute("ami_name")),
                    "items" => [],
                ];
            }

            $_items = [];
            $_datum = $xpath->query("data/datum", $_zpat);
            foreach ($_datum as $_data) {
                $_items[] = trim($_data->nodeValue);
            }

            $item = [
                "date"  => $_zpat->getAttribute("dcre"),
                "value" => CMbString::utf8Decode(implode("\n", $_items)),
            ];

            $texts[$_zone_id]["items"][] = $item;
        }

        $smarty = new CSmartyDP();
        $smarty->assign("texts", $texts);

        return $smarty->fetch("reader/inc_display_patient_texts.tpl");
    }

    /**
     * Transforme un fichier XML Medistory en fichier HTML
     *
     * @param string $content Le contenu � transformer en HTML
     *
     * @return mixed|string
     * @throws Exception
     */
    public function readMedistoryFile($content)
    {
        if (!$content) {
            return '';
        }

        $dom = new DOMDocument();
        $dom->loadXML($content);
        $xpath = new DOMXPath($dom);

        /** @var DOMElement $formulaire */
        $formulaire = $dom->firstChild;

        $type    = $formulaire->getAttribute("type");
        $version = $formulaire->getAttribute("version");

        $zones      = [];
        $zone_nodes = $xpath->query("Zone", $formulaire);
        foreach ($zone_nodes as $_zone) {
            $zones[CMbString::utf8Decode($_zone->getAttribute("name"))] = [
                "raw" => CMbString::utf8Decode($_zone->nodeValue),
            ];
        }

        $smarty = new CSmartyDP();
        $smarty->assign("type", $type);
        $smarty->assign("version", $version);
        $smarty->assign("zones", $zones);

        return $smarty->fetch("reader/inc_display_medistory_form.tpl");
    }

    /**
     * Transforme un fichier Surgica en fichier HTML
     *
     * @param string $content Le contenu � transformer en HTML
     *
     * @return mixed|string
     * @throws Exception
     */
    public function readSurgicaFile(string $content)
    {
        if (!$content) {
            return '';
        }

        $json = json_decode($content, true);

        $doc    = base64_decode($json["doc"]);
        $modele = base64_decode($json["modele"]);

        $tmpfile = tempnam(sys_get_temp_dir(), "dfm_");
        file_put_contents($tmpfile, $modele);

        $reader = new CDFMReader($tmpfile);
        $reader->fillFromContent($doc);
        $objects = $reader->parse();

        // Close and remove file
        $reader->close();
        unlink($tmpfile);

        $object = reset($objects);

        $smarty = new CSmartyDP();
        $smarty->assign("object", $object);
        $smarty->assign("id", "root");

        return $smarty->fetch("reader/inc_vw_surgica_form.tpl");
    }

    /**
     * Transforme un fichier Osoft Historique en fichier HTML
     *
     * @param string $content Le contenu � transformer en HTML
     *
     * @throws Exception
     */
    public function readOsoftHistoriqueFile(string $content): string
    {
        if (!$content) {
            return '';
        }

        $str = preg_replace("/�\.Imprime;[^�]+�/", "", $content);

        // Bas de page et Haut de page
        $footer = $this->formatString($this->extractArea($str, "Bas de page"));
        $header = $this->formatString($this->extractArea($str, "Haut de page"));
        $body   = $this->formatString($str);

        return "<table class=\"main layout\">
      <thead><tr><td><pre class=\"document\">$header</pre></td></tr></thead>
      <tbody><tr><td><pre class=\"document\">$body</pre></td></tr></tbody>
      <tfoot><tr><td><pre class=\"document\">$footer</pre></td></tr></tfoot>
    </table>";
    }

    /**
     * Transforme un fichier Osoft Dossier en fichier HTML
     *
     * @param string $content Le contenu � transformer en HTML
     *
     * @throws Exception
     */
    public function readOsoftDossierFile(string $content): string
    {
        if (!$content) {
            return '';
        }

        // Mark bad characters
        $marker = "�����";
        // Full selection
        // $chars = "a-z0-9���������������������������";
        // French selection makes it proper
        $chars = "a-z0-9������������������";
        $chars .= " %�\\$'\.,;:&\~@#!\?" . chr(0xA4); // euro
        $chars .= "\t\n\r\"\+\-\*\/\\\\(\)\[\]\{\}";
        $str   = preg_replace("/([^$chars]+)/i", $marker, $content);

        // Remove fonts keywords
        $str = preg_replace("/(arial|ms sans serif|times new roman)/i", "", $str);

        // Incrementally remove nice characters enchased between markers
        // All but new lines
        $chars = preg_replace("/[\r\n]/", "", $chars);
        $count = true;
        while ($count) {
            $str = preg_replace("/$marker([$chars]{1,8})$marker/i", $marker, $str, -1, $count);
        }

        // Remove leading markers after new lines
        $str = preg_replace("/^([$chars]{1,4})($marker)/i", "", $str);
        $str = preg_replace("/([\r\n^])([$chars]{1,4})($marker)/i", "$1", $str);

        // Remove remaining markers
        $str = preg_replace("/($marker)/i", "", $str);

        // Remove nearly empty lines
        $count = true;
        while ($count) {
            $str = preg_replace("/([\r\n])([$chars]{1,3})([\r\n])/i", "$1", $str, -1, $count);
        }

        return trim($str);
    }

    /**
     * @param string $str  The string from which extract the area
     * @param string $name The name to remove from text
     *
     * @return mixed
     */
    private function extractArea(&$str, $name)
    {
        if (preg_match("/�\.{$name}�(.*)$/ms", $str, $matches)) {
            $str = preg_replace("/(�\.{$name}�.*)$/ms", "", $str);

            return $matches[1];
        }

        return null;
    }

    /**
     * @param string $str The string to format
     *
     * @return null|string|string[]
     */
    private function formatString($str)
    {
        $str = preg_replace("/�(\d+)�/", '&#0$1', $str); // HTML entities
        $str = preg_replace("/�R�gle;[^�]+�/", "<hr style='border: none; border-top: 1px solid #999;' />", $str);
        $str = preg_replace(
            "/�Style;([^:]+):(\d+);(\d+);\d;\d;\d�/",
            "<span style='font-family:$1; font-size:$3px;'>",
            $str
        );
        $str = preg_replace("/�Style;;(\d+);\d?;\d?;\d?�/", "<span style='font-size:$1px;'>", $str);
        $str = preg_replace("/�Style;;;0;\d?;\d?�/", "<span style='font-weight:normal;font-style:normal;'>", $str);
        $str = preg_replace("/�Style;;;1;\d?;\d?�/", "<span style='font-weight:normal;font-style:italic;'>", $str);
        $str = preg_replace("/�Style;;;2;\d?;\d?�/", "<span style='font-weight:bold;  font-style:normal;'>", $str);
        $str = preg_replace("/�Style;;;3;\d?;\d?�/", "<span style='font-weight:normal;font-style:italic;'>", $str);
        $str = preg_replace("/�Style;;;5;\d?;\d?�/", "<span style='font-weight:bold;  font-style:normal;'>", $str);

        return $str;
    }
}
