<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Tests\Unit\Traces\SendStrategies;

use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Files\Traces\CalculatorDateSending;
use Ox\Mediboard\Files\Traces\CFilesSendToReceiverStrategy;
use Ox\Mediboard\Files\Traces\SendStrategies\AfterEndEventScheduled;
use Ox\Mediboard\Files\Traces\SendStrategies\BeforeStartEventScheduled;
use Ox\Mediboard\Files\Traces\SendStrategies\TimeSlotScheduled;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;

class CalculatorDateSendingTest extends OxUnitTestCase
{
    public function providerCasesWhenDateIsPast(): array
    {
        $sejour         = new CSejour();
        $sejour->entree = CMbDT::dateTime("-20 DAYS");
        $sejour->sortie = CMbDT::dateTime("-18 DAYS");

        $strategy_set_time           = new CFilesSendToReceiverStrategy();
        $strategy_set_time->strategy = (new TimeSlotScheduled())->getStrategyName();
        $strategy_set_time->value    = "10:00:00";

        // 1 Before event in past + set time ==> null
        $strategy_before_event           = new CFilesSendToReceiverStrategy();
        $strategy_before_event->strategy = (new BeforeStartEventScheduled())->getStrategyName();
        $strategy_before_event->value    = '48'; // two days before entree

        // 2 after event in past + set time ==> null
        $strategy_after_event           = new CFilesSendToReceiverStrategy();
        $strategy_after_event->strategy = (new AfterEndEventScheduled())->getStrategyName();
        $strategy_after_event->value    = '48'; // two days after sortie

        return [
            "only before event"         => [$sejour, [$strategy_before_event]],
            "only after event"          => [$sejour, [$strategy_after_event]],
            "before event and set time" => [$sejour, [$strategy_after_event, $strategy_set_time]],
            "after event and set time"  => [$sejour, [$strategy_before_event, $strategy_set_time]],
        ];
    }

    /**
     * @dataProvider providerCasesWhenDateIsPast
     *
     * @param CStoredObject                  $object
     * @param CFilesSendToReceiverStrategy[] $strategies
     *
     * @return void
     * @throws \Ox\Core\CMbException
     */
    public function testCasesWhenDateIsPast(CStoredObject $object, array $strategies): void
    {
        $calculator          = new CalculatorDateSending();
        $calculated_datetime = $calculator->determineDatetime($object, $strategies);
        $this->assertNull($calculated_datetime);
    }

    public function providerCumulativeStrategiesInFutur(): array
    {
        $sejour         = new CSejour();
        $sejour->entree = CMbDT::date("+10 DAYS") . ' 10:00:00';
        $sejour->sortie = CMbDT::date("+12 DAYS") . ' 15:00:00';

        $strategy_set_time           = new CFilesSendToReceiverStrategy();
        $strategy_set_time->strategy = (new TimeSlotScheduled())->getStrategyName();
        $strategy_set_time->value    = "08:00:00";

        // 1 Before event in past + set time ==> null
        $strategy_before_event           = new CFilesSendToReceiverStrategy();
        $strategy_before_event->strategy = (new BeforeStartEventScheduled())->getStrategyName();
        $strategy_before_event->value    = '48'; // two days before entree
        $strategy_1                      = [$strategy_before_event, $strategy_set_time];
        $expected_datetime_1             = CMbDT::date("+8 DAYS") . ' 08:00:00';

        // 2 after event in past + set time ==> null
        $strategy_after_event           = new CFilesSendToReceiverStrategy();
        $strategy_after_event->strategy = (new AfterEndEventScheduled())->getStrategyName();
        $strategy_after_event->value    = '48'; // two days after sortie
        $strategy_2                     = [$strategy_after_event, $strategy_set_time];
        // +1 day because time to set is already past so, we add one day
        $expected_datetime_2 = CMbDT::date("+14 DAYS") . ' 08:00:00';

        return [
            "before event and set time" => [$sejour, $strategy_1, $expected_datetime_1],
            "after event and set time"  => [$sejour, $strategy_2, $expected_datetime_2],
        ];
    }

    /**
     * @dataProvider providerCumulativeStrategiesInFutur
     *
     * @param CStoredObject                  $object
     * @param CFilesSendToReceiverStrategy[] $strategies
     * @param string                         $expected_datetime
     *
     * @return void
     * @throws \Ox\Core\CMbException
     */
    public function testCumulativeStrategiesInFutur(
        CStoredObject $object,
        array         $strategies,
        string        $expected_datetime
    ): void {
        $calculator          = new CalculatorDateSending();
        $calculated_datetime = $calculator->determineDatetime($object, $strategies);

        $this->assertEquals($expected_datetime, $calculated_datetime);
    }
}
