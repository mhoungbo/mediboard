<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Tests\Unit\Traces\SendStrategies;

use Ox\Core\CMbDT;
use Ox\Mediboard\Files\Traces\SendStrategies\TimeSlotScheduled;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;

class TimeSlotScheduledTest extends OxUnitTestCase
{
    public function testValueNotSet(): void
    {
        $time_slot_scheduled = new TimeSlotScheduled();

        $this->assertNull($time_slot_scheduled->determineDatetime(new CSejour(), null, null));
    }

    public function providerValueWithTimeInFuture(): array
    {
        return [
            "same time"      => [CMbDT::time(null, '08:00:00')],
            "time in future" => [CMbDT::time(null, '10:00:00')],
        ];
    }

    /**
     * @dataProvider providerValueWithTimeInFuture
     *
     * @param string $time
     *
     * @return void
     */
    public function testValueWithTimeInFuture(string $time): void
    {
        $time_slot_scheduled = new TimeSlotScheduled();

        $futur_date           = CMbDT::date("+1 DAY");
        $calculated_send_date = $time_slot_scheduled->determineDatetime(
            new CSejour(),
            $futur_date . ' 08:00:00',
            $time
        );

        $this->assertEquals($futur_date . ' ' . $time, $calculated_send_date);
    }

    public function testValueWithUsageOfDatetime(): void
    {
        $time_slot_scheduled = new TimeSlotScheduled();

        $datetime             = CMbDT::date() . ' ' . '08:00:00';
        $calculated_send_date = $time_slot_scheduled->determineDatetime(
            new CSejour(),
            CMbDT::dateTime("+1 DAY"),
            $datetime
        );

        $this->assertEquals(CMbDT::date("+1 DAY") . ' ' . '08:00:00', $calculated_send_date);
    }

    public function testValueWithTimeInFuturAlreadyPast(): void
    {
        $time_slot_scheduled = new TimeSlotScheduled();

        $date_in_futur        = CMbDT::date('+1 DAY');
        $datetime_in_futur    = $date_in_futur . "08:00:00";
        $time                 = '06:00:00';
        $calculated_send_date = $time_slot_scheduled->determineDatetime(new CSejour(), $datetime_in_futur, $time);

        $date_should_be_tomorrow = $date_in_futur . ' ' . $time;
        $this->assertEquals($date_should_be_tomorrow, $calculated_send_date);
    }

    public function testValueWithTimeAlreadyPast(): void
    {
        $time_slot_scheduled = new TimeSlotScheduled();

        $datetime_in_past     = CMbDT::dateTime("-2 DAYS -20 MINUTES");
        $calculated_send_date = $time_slot_scheduled->determineDatetime(
            new CSejour(),
            $datetime_in_past,
            CMbDT::time()
        );

        $this->assertNull($calculated_send_date);
    }
}
