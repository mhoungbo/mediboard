<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Tests\Unit\Traces\SendStrategies;

use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Files\Traces\SendStrategies\AfterEndEventScheduled;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;

class AfterEndEventScheduledTest extends OxUnitTestCase
{
    public function providerInvalidEventObject(): array
    {
        return [
            'invalid object'                             => [new CStoredObject(), null, null],
            'invalid object with value'                  => [new CStoredObject(), null, 4],
            'invalid object with ref datetime'           => [new CStoredObject(), CMbDT::dateTime(), null],
            'invalid object with ref datetime and value' => [new CStoredObject(), CMbDT::dateTime(), 4],
            'empty Consultation'                         => [new CConsultation(), null, null],
            'empty Operation'                            => [new COperation(), null, null],
            'empty Sejour'                               => [new CSejour(), null, null],
        ];
    }

    /**
     * @param CStoredObject $event
     * @param string|null   $ref_datetime
     * @param mixed         $value
     *
     * @dataProvider providerInvalidEventObject
     * @return void
     * @throws Exception
     */
    public function testInvalidEventObject(CStoredObject $event, ?string $ref_datetime, $value): void
    {
        $algorithm           = new AfterEndEventScheduled();
        $calculated_datetime = $algorithm->determineDatetime($event, $ref_datetime, $value);

        $this->assertNull($calculated_datetime);
    }

    public function testReturnNullWhenCalculatedDatetimeIsInPast(): void
    {
        $sejour         = new CSejour();
        $sejour->sortie = CMbDT::dateTime("-8 DAY");

        // two hours after sortie
        $algorithm           = new AfterEndEventScheduled();
        $calculated_datetime = $algorithm->determineDatetime($sejour, null, "2");

        $this->assertNull($calculated_datetime);
    }

    public function providerReturnCalculatedDatetime(): array
    {
        // date in furture to avoid return null because time calculated is already past compared to current time
        $tomorrow_date     = CMbDT::date("+1 DAY");
        $tomorrow_time     = CMbDT::time("+1 DAY");
        $tomorrow_datetime = $tomorrow_date . ' ' . $tomorrow_time;

        return [
            'Same date'           => [$tomorrow_datetime, 0, $tomorrow_datetime],
            'One hour plus'       => [$tomorrow_datetime, 1, CMbDT::dateTime('+1 hour', $tomorrow_datetime)],
            'One day plus'        => [$tomorrow_datetime, 24, CMbDT::dateTime('+1 day', $tomorrow_datetime)],
            'One hour before end' => [$tomorrow_datetime, -1, CMbDT::dateTime('-1 hour', $tomorrow_datetime)],
        ];
    }

    /**
     * @dataProvider providerReturnCalculatedDatetime
     *
     * @param string $datetime_sortie
     * @param string $value
     * @param string $expected_datetime
     *
     * @throws Exception
     */
    public function testReturnCalculatedDatetime(string $datetime_sortie, int $value, string $expected_datetime): void
    {
        $sejour         = new CSejour();
        $sejour->sortie = $datetime_sortie;

        // two hours after sortie
        $algorithm           = new AfterEndEventScheduled();
        $calculated_datetime = $algorithm->determineDatetime($sejour, null, $value);

        $this->assertEquals($expected_datetime, $calculated_datetime);
    }
}
