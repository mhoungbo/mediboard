<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Files\Tests\Functional\Controllers;

use Exception;
use Ox\Core\CMbModelNotFoundException;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;

class FileControllerTest extends OxWebTestCase
{
    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    private function createFile(): CFile
    {
        $file_to_test = __DIR__ . '/../../Resources/loremipsum.txt';

        $user               = CMediusers::get();
        $file               = new CFile();
        $file->object_class = $user->_class;
        $file->object_id    = $user->_id;
        $file->file_name    = 'loremipsum.txt';
        $file->file_type    = 'text/plain';
        $file->file_date    = '2023-01-01 10:06:02';
        $file->updateFormFields();
        $file->fillFields();
        $file->setContent(file_get_contents($file_to_test));
        $this->storeOrFailed($file);

        return CFile::findOrFail($file->_id);
    }

    /**
     * @throws TestsException
     * @throws Exception
     */
    public function testShow(): CFile
    {
        $file = $this->createFile();

        $client = self::createClient();
        $client->request('GET', "/api/files/files/{$file->_id}", ['with_file_content' => true]);

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals(CFile::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($file->_id, $item->getId());
        $this->assertEquals($file->annule, $item->getAttribute('annule'));
        $this->assertEquals($file->annule, $item->getAttribute('annule'));
        $this->assertStringContainsString('lorem ipsum', base64_decode($item->getAttribute('file_content')));
        $this->assertEquals("?m=files&raw=thumbnail&document_id={$file->_id}&thumb=1", $item->getLink('thumbnail'));

        return $file;
    }

    /**
     * @depends testShow
     * @throws Exception
     */
    public function testUpdate(CFile $file): CFile
    {
        $update = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID         => $file->_id,
                    Item::TYPE       => $file::RESOURCE_TYPE,
                    Item::ATTRIBUTES => [
                        'annule'    => '1',
                        'file_name' => 'loremipsumdolor.txt',
                    ],
                ],
            ]
        );

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('PATCH', "/api/files/files/{$file->_id}", [], [], [], json_encode($update));

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals(CFile::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($file->_id, $item->getId());
        $this->assertEquals('1', $item->getAttribute('annule'));

        return $file;
    }

    /**
     * @depends testUpdate
     * @throws Exception
     */
    public function testUpdateFieldsNotPermitted(CFile $file): CFile
    {
        $update = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID         => $file->_id,
                    Item::TYPE       => $file::RESOURCE_TYPE,
                    Item::ATTRIBUTES => [
                        'file_type'  => 'image/jpg',
                        'etat_envoi' => 'oui',
                    ],
                ],
            ]
        );

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('PATCH', "/api/files/files/{$file->_id}", [], [], [], json_encode($update));

        $this->assertResponseStatusCodeSame(200);

        $item = $this->getJsonApiItem($client);
        $this->assertEquals('non', $item->getAttribute('etat_envoi'));
        $this->assertEquals('text/plain', $item->getAttribute('file_type'));

        return CFile::findOrFail($file->_id);
    }

    /**
     * @depends testUpdateFieldsNotPermitted
     * @throws Exception
     */
    public function testDelete(CFile $file): void
    {
        $client = self::createClient();
        $client->request('DELETE', "/api/files/files/{$file->_id}");

        $this->assertResponseStatusCodeSame(204);
        $this->assertEquals('', $client->getResponse()->getContent());

        $this->assertFalse(CFile::find($file->_id));
    }

    /**
     * @throws TestsException
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public function testList(): void
    {
        $this->createFile();

        $user       = CMediusers::get();
        $first_file = $user->loadFirstBackRef('files');

        $client = self::createClient();
        $client->request(
            'GET',
            '/api/files/files/' . $user::RESOURCE_TYPE . '/' . $user->_id,
            ['relations' => 'context']
        );

        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);

        $file = $collection->getFirstItem();
        $this->assertEquals(CFile::RESOURCE_TYPE, $file->getType());
        $this->assertEquals($user->_id, $file->getRelationship('context')->getId());

        $this->deleteOrFailed($first_file);
    }
}
