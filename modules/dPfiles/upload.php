<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\Kernel\Exception\ControllerStoppedException;
use Ox\Mediboard\Files\CUploader;

CCanDo::checkAdmin();

$uploader = new CUploader();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (!$uploader->checkChunks()) {
        throw new ControllerStoppedException(404);
    }
} else {
    $uploader->handleRequest();
}


foreach ($uploader->getMessages() as $_msg) {
    CAppUI::setMsg(...$_msg);
}

echo CAppUI::getMsg();
