/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class PracticeInvoice extends OxObject {
    constructor () {
        super()
        this.type = "practice_invoice"
    }

    get patientDue (): OxAttr<number> {
        return super.get("du_patient")
    }

    set patientDue (number: OxAttr<number>) {
        super.set("du_patient", number)
    }

    get remainingPatientDue (): number {
        return super.get("_du_restant_patient") ?? 0
    }

    set remainingPatientDue (number) {
        super.set("_du_restant_patient", number ?? 0)
    }

    get opening (): OxAttr<string> {
        return super.get("ouverture")
    }

    get cancelled (): OxAttr<boolean> {
        return super.get("annule")
    }

    set cancelled (value: OxAttr<boolean>) {
        super.set("annule", value)
    }
}
