<?php

/**
 * @package Mediboard\Facturation
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Facturation;

use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Mediboard\Cabinet\CConsultation;

/**
 * Facture li�e � une ou plusieurs consultations
 *
 */
class CFactureCabinet extends CFacture
{
    public const RESOURCE_TYPE = 'practice_invoice';

    // DB Table key
    public $facture_id;

    /**
     * @see parent::getSpec()
     **/
    function getSpec()
    {
        $spec        = parent::getSpec();
        $spec->table = 'facture_cabinet';
        $spec->key   = 'facture_id';

        return $spec;
    }

    /**
     * @see parent::getProps()
     */
    function getProps()
    {
        $props                        = parent::getProps();
        $props["group_id"]            .= " back|group_fact_cab";
        $props["patient_id"]          .= " back|facture_patient_consult";
        $props["extourne_id"]         = "ref class|CFactureCabinet back|extourne_cab";
        $props["category_id"]         .= " back|facture_category_cab";
        $props["coeff_id"]            .= " back|coeff_fact_cab";
        $props["assurance_maladie"]   .= " back|fact_consult_maladie";
        $props["assurance_accident"]  .= " back|fact_consult_accident";
        $props["praticien_id"]        .= " back|praticien_facture_cabinet";
        $props["bill_user_printed"]   .= " back|bill_user_printed_cab";
        $props["justif_user_printed"] .= " back|justif_user_printed_cab";

        return $props;
    }

    /**
     * @see parent::updateFormFields()
     **/
    function updateFormFields()
    {
        parent::updateFormFields();
        $this->_view = sprintf("FA%08d", $this->_id);
        $this->_view .= " / $this->num_compta";
    }

    /**
     * Chargement des r�glements de la facture
     *
     * @param bool $cache cache
     *
     * @return CReglement[]
     **/
    function loadRefsReglements($cache = true)
    {
        $this->_ref_reglements = $this->loadBackRefs("reglements", 'date') ?: [];

        return parent::loadRefsReglements($cache);
    }

    /**
     * @see parent::store()
     */
    function store()
    {
        $this->loadRefsConsultation();
        // A v�rifier pour le == 0 s'il faut faire un traitement
        if ($this->statut_envoi !== 'non_envoye' && $this->_id) {
            foreach ($this->_ref_consults as $_consultation) {
                if ($this->statut_envoi == 'echec' && $_consultation->facture == 1) {
                    $_consultation->facture = 0;
                    $_consultation->store();
                } elseif ($this->statut_envoi == "envoye" && $_consultation->facture == 0) {
                    $_consultation->facture = 0;
                    $_consultation->store();
                }
            }
        }

        $this->loadRefsRelances();
        $this->loadRefsReglements();

        if ($this->annule == '1' && $this->fieldModified('annule') && count($this->_ref_reglements)) {
            return CAppUI::tr('CFactureCabinet-error-cancel_invoice_has_payments');
        }

        return parent::store();
    }

    /**
     * @see parent::delete()
     */
    function delete()
    {
        $this->_ref_reglements        = [];
        $this->_ref_relances          = [];
        $this->_count["relance_fact"] = 0;
        $this->_count["reglements"]   = 0;
        $this->loadRefsReglements();
        $this->loadRefsRelances();

        return parent::delete();
    }

    //Ne pas supprimer cette fonction!

    /**
     * loadRefPlageConsult
     *
     * @return void
     **/
    function loadRefPlageConsult()
    {
    }

    /**
     * Fonction permettant � partir d'un num�ro de r�f�rence de retrouver la facture correspondante
     *
     * @param string $num_reference le num�ro de r�f�rence
     *
     * @return CFactureCabinet
     **/
    function findFacture($num_reference)
    {
        $facture                = new CFactureCabinet();
        $facture->num_reference = $num_reference;
        $facture->loadMatchingObject();

        if (!$facture->_id) {
            $echeance                = new CEcheance();
            $echeance->object_class  = "CFactureCabinet";
            $echeance->num_reference = $num_reference;
            $echeance->loadMatchingObject();
            if ($echeance->_id) {
                $facture = $echeance->loadTargetObject();
            }
        }

        return $facture;
    }

    /**
     * Chargement des relances de la facture
     *
     * @return CRelance[]
     **/
    function loadRefsRelances()
    {
        $this->_ref_relances = $this->loadBackRefs("relance_fact", 'date');
        $this->isRelancable();

        return $this->_ref_relances;
    }

    /**
     * Chargement des �ch�ances de la facture
     *
     * @return CEcheance[]
     **/
    function loadRefsEcheances()
    {
        $this->_ref_echeances = $this->loadBackRefs("echeances", "date");
        $this->loadEcheancesMontant();

        return $this->_ref_echeances;
    }

    /**
     * @see parent::fillTemplate(), used to be detected as context for the documents models
     */
    function fillTemplate(&$template)
    {
        parent::fillTemplate($template);
    }

    /**
     * Chargement des rejets de facture par les assurances
     *
     * @return CFactureRejet[]
     **/
    function loadRefsRejets()
    {
        return $this->_ref_rejets = $this->loadBackRefs("rejets");
    }

    /**
     * Chargement des liaisons de facture
     *
     * @return CFactureLiaison[]|null
     */
    function loadRefsLiaisons()
    {
        return $this->_ref_liaisons = $this->loadBackRefs("facture_liaison");
    }

    /**
     * @param CConsultation[] $consultations
     *
     * @return array
     * @throws \Exception
     */
    public static function massLoadForConsultation(array $consultations): array
    {
        $ids = CMbArray::pluck($consultations, "_id");
        CMbArray::removeValue("", $ids);

        $liaisons = self::massLoadBackRefs($consultations, 'facturable');
        $factures = self::massLoadFwdRef($liaisons, 'facture_id');
        self::massLoadFwdRef($factures, 'assurance_maladie');
        self::massLoadFwdRef($factures, 'assurance_accident');

        foreach ($consultations as $consultation) {
            $ref_factures = [];
            /** @var CFactureLiaison $link */
            foreach ($consultation->_back['facturable'] as $link) {
                $ref_factures[$link->facture_id] = $link->loadRefFacture();
                $ref_factures[$link->facture_id] = $link->loadRefFacture();
            }

            if (!is_array($consultation->_ref_factures)) {
                $consultation->_ref_factures = [];
            }
            $consultation->_ref_factures = array_replace($consultation->_ref_factures, $ref_factures);

            $consultation->_ref_facture = reset($consultation->_ref_factures);
            $consultation->_factures_mass_loaded = true;
        }

        return $factures;
    }
}
