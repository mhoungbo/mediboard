<?php
/**
 * @package Mediboard\Facturation
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CSmartyDP;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Cabinet\CActeNGAP;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\Facturation\CReglement;
use Ox\Mediboard\Lpp\CActeLPP;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CEvenementPatient;
use Ox\Tamm\Cabinet\CAppelSIH;
use Symfony\Component\HttpFoundation\Response;

CCanDo::checkEdit();
$evenement_guid = CView::get("evenement_guid", "str");
CView::checkin();

/* @var CEvenementPatient $evenement */
$op_ccams        = [];
$coding_locked   = null;
$show_sih_coding = false;
$user            = CMediusers::get();
$evenement       = CMbObject::loadFromGuid($evenement_guid);
$pract           = $evenement->loadRefPraticien();

$evenement->isCoded();
$evenement->loadRefsActes();
$evenement->countActes();
$evenement->loadExtCodesCCAM();
$evenement->getAssociationCodesActes();
$evenement->loadPossibleActes();
$evenement->canDo();
$evenement->loadRefsId400SIH();


$evenement->loadRefsCodagesCCAM();
foreach ($evenement->_ref_codages_ccam as $_codages_by_prat) {
  foreach ($_codages_by_prat as $_codage) {
    $_codage->loadPraticien()->loadRefFunction();
    $_codage->loadActesCCAM();
    $_codage->getTarifTotal();
    foreach ($_codage->_ref_actes_ccam as $_acte) {
      $_acte->getTarif();
    }
  }
}

if ($evenement->_ref_sih_id400) {
    $sih_id    = $evenement->_ref_sih_id400->id400;
    $appel_sih = new CAppelSIH();
    $user      = CMediusers::get();
    if ($pract->rpps && ($user->_id === $pract->_id || $user->isSecretaire())) {
        try {
            $token = $appel_sih->getAccessCabinet($pract->rpps);
        } catch (CMbException $e) {
            $token = null;
        }
        if (is_array($token) && isset($token['list_sih'][$sih_id]['type'])) {
            $evenement->_type_sih = $token['list_sih'][$sih_id]['type'];
        }
    }
    if (
        $evenement->_type_sih === "MB" &&
        isset($evenement->_ref_context_id400->id400) &&
        [$context_class, $context_id] = explode('-', $evenement->_ref_context_id400->id400)
    ) {
        $ccams_response = $appel_sih->getCCamActsResponse($pract->rpps, $context_id, $sih_id, $evenement->_type_sih);

        if ($show_sih_coding = $ccams_response->getStatusCode() === Response::HTTP_OK) {
            $op_ccams       = $appel_sih->retrieveCCamActsFromResponse($ccams_response);
            $coding_locked  = $appel_sih->retrieveCCamCodingLockedFromResponse($ccams_response);
            foreach ($op_ccams as $op_ccam) {
                $op_ccam->updateFormFields();
            }
        }
    }
}

$user->isPraticien();
$user->isProfessionnelDeSante();

$listChirs = CConsultation::loadPraticiens(PERM_EDIT);
$listAnesths = $user->loadAnesthesistes(PERM_DENY);

$divers = array();
if (CAppUI::gconf("dPccam frais_divers use_frais_divers_CEvenementPatient")) {
  $divers = $evenement->loadRefsFraisDivers(count($evenement->_ref_factures)+1);
  $evenement->loadRefsFraisDivers(null);
}

/* LPP */
$acte_lpp = null;
if (CModule::getActive('lpp') && CAppUI::gconf('lpp General cotation_lpp')) {
    $evenement->loadRefsActesLPP();

    foreach ($evenement->_ref_actes_lpp as $_acte) {
        $_acte->loadRefExecutant();
        $_acte->_ref_executant->loadRefFunction();
    }

    $acte_lpp = CActeLPP::createFor($evenement);
}

// Cr�ation du template
$smarty = new CSmartyDP();

$smarty->assign("frais_divers", $divers);
$smarty->assign("evenement", $evenement);
$smarty->assign("user"     , $user);
$smarty->assign("listChirs", $listChirs);
$smarty->assign('listAnesths', $listAnesths);
$smarty->assign("acte_ngap", CActeNGAP::createEmptyFor($evenement));
$smarty->assign("reglement", new CReglement());
$smarty->assign('acte_lpp', $acte_lpp);
$smarty->assign('op_ccams', $op_ccams);
$smarty->assign('coding_locked', $coding_locked);
$smarty->assign('show_sih_coding', $show_sih_coding);

$smarty->display("vw_edit_facture_evt");
