{{*
 * @package Mediboard\Facturation
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{assign var="object" value=$evenement}}
{{assign var="do_subject_aed" value="do_evenement_patient_aed"}}
{{mb_include module=salleOp template=js_codage_ccam}}
{{mb_script module=facturation script=facture   ajax=true}}
{{mb_script module=cabinet     script=reglement ajax=true}}

<script>
  Main.add(function () {
    var tabsReglement = Control.Tabs.create('tab-evt-reglement', true);
    if (tabsReglement.activeLink.key == "reglement_evt") {
      Facture.reloadEvt('{{$evenement->_guid}}');
    }
    window.evenement_guid = '{{$evenement->_guid}}';
    window.evenement_id = '{{$evenement->_id}}';
    window.user_id = '{{$evenement->_ref_praticien->_id}}';
  });
</script>

{{if $show_sih_coding}}
<div>
  <table class="tbl me-margin-bottom-10">
      <tr>
        <th class="title" colspan="8">
          {{tr var1=$evenement->_ref_praticien->_view}}CEvenementPatient-msg-Operation CCAM coding on the HIS for %s{{/tr}}
          {{if $coding_locked !== null}}<span class="texticon">{{tr}}CCodageCCAM-msg-codage_{{if $coding_locked}}locked{{else}}unlocked{{/if}}{{/tr}}</span>{{/if}}
        </th>
      </tr>
      <tr>
        <th>{{mb_title class=CActeCCAM field=code_acte}}</th>
        <th>{{mb_title class=CActeCCAM field=modificateurs}}</th>
        <th>{{mb_title class=CActeCCAM field=code_association}}</th>
        <th>{{mb_title class=CActeCCAM field=execution}}</th>
        <th>{{mb_title class=CActeCCAM field=coding_datetime}}</th>
        <th>{{mb_title class=CActeCCAM field=montant_base}}</th>
        <th>{{mb_title class=CActeCCAM field=montant_depassement}}</th>
        <th>{{mb_title class=CActeCCAM field=_montant_facture}}</th>
      </tr>
    {{foreach from=$op_ccams item=ccam}}
      {{assign var=acte_code value=$ccam->code_acte}}
      {{if $ccam->code_activite != ""}}
        {{assign var=acte_code value="$acte_code-{$ccam->code_activite}"}}
        {{if $ccam->code_phase != ""}}
            {{assign var=acte_code value="$acte_code-{$ccam->code_phase}"}}
        {{/if}}
      {{/if}}
      <tr>
          <td>
            <a href="#CodeCCAM-show-{{$acte_code}}" style="display:inline-block;">
              {{mb_value object=$ccam field=code_acte}}
            </a>
            <span class="circled" style="background-color: #eeffee;">
              {{$ccam->code_activite}}-{{$ccam->code_phase}}
            </span>
          </td>
          <td>
            {{foreach from=$ccam->_modificateurs item=modificateur}}
              <span class="circled me-color-black-high-emphasis">{{$modificateur}}</span>
            {{/foreach}}
          </td>
          <td>{{mb_value object=$ccam field=code_association}}</td>
          <td>{{mb_value object=$ccam field=execution}}</td>
          <td>{{mb_value object=$ccam field=coding_datetime}}</td>
          <td>{{mb_value object=$ccam field=montant_base}}</td>
          <td>{{mb_value object=$ccam field=montant_depassement}}</td>
          <td>{{mb_value object=$ccam field=_montant_facture}}</td>
      </tr>
    {{foreachelse}}
      <tr>
        <td class="empty">
          {{tr}}CActe.none{{/tr}}
        </td>
      </tr>
    {{/foreach}}
  </table>
</div>
{{/if}}

<div>
  <!-- Formulaire pour r�actualise�r -->
  <form name="editFrmFinish" method="get">
    {{mb_key object=$object}}
  </form>

  <ul id="tab-evt-reglement" class="control_tabs small">
    <li onmousedown="Facture.reloadEvt('{{$evenement->_guid}}');" id="a_reglements_evt"><a href="#reglement_evt">{{tr}}module-dPfacturation-court{{/tr}}</a></li>
    {{if $app->user_prefs.ccam_consultation == 1}}
      {{if "dPccam codage use_cotation_ccam"|gconf == "1"}}
        <li><a href="#ccam"{{if $object->_ref_actes_ccam|@count == 0}} class="empty"{{/if}}>{{tr}}CActeCCAM{{/tr}} <small id="count_ccam_{{$object->_guid}}">({{$object->_ref_actes_ccam|@count}})</small></a></li>
        <li><a href="#ngap"{{if $object->_ref_actes_ngap|@count == 0}} class="empty"{{/if}}>{{tr}}CActeNGAP{{/tr}} <small id="count_ngap_{{$object->_guid}}">({{$object->_ref_actes_ngap|@count}})</small></a></li>
      {{/if}}
      {{if 'lpp'|module_active && "lpp General cotation_lpp"|gconf}}
          <li>
              <a href="#lpp" {{if $object->_ref_actes_lpp|@count ==0}} class="empty"{{/if}}>
                  {{tr}}CActeLPP|pl{{/tr}}
                  <small>({{$object->_ref_actes_lpp|@count}})</small>
              </a>
          </li>
      {{/if}}
      {{if "dPccam frais_divers use_frais_divers_CEvenementPatient"|gconf && "dPccam codage use_cotation_ccam"|gconf}}
        <li><a href="#fraisdivers">{{tr}}CFraisDivers{{/tr}}</a></li>
      {{/if}}
    {{/if}}
  </ul>

  <div id="reglement_evt" style="display: none;"></div>

  {{if $app->user_prefs.ccam_consultation == 1}}
    <div id="ccam" style="display: none;">
      {{assign var="module" value="oxCabinet"}}
      {{assign var="subject" value=$object}}
      {{mb_include module=salleOp template=inc_codage_ccam}}
    </div>

    <div id="ngap" style="display: none;">
      <div id="listActesNGAP" data-object_id="{{$object->_id}}" data-object_class="{{$object->_class}}">
        {{assign var="_object_class" value="CConsultation"}}
        {{mb_include module=cabinet template=inc_codage_ngap object=$object}}
      </div>
    </div>

      {{if 'lpp'|module_active && "lpp General cotation_lpp"|gconf}}
          <div id="lpp" style="display: none;">
              {{mb_include module=lpp template=inc_codage_lpp codable=$object}}
          </div>
      {{/if}}

    {{if "dPccam frais_divers use_frais_divers_CEvenementPatient"|gconf && "dPccam codage use_cotation_ccam"|gconf}}
      <div id="fraisdivers" style="display: none;">
        {{mb_include module=ccam template=inc_frais_divers object=$object}}
      </div>
    {{/if}}
  {{/if}}
</div>
