models_api_medical_reports_list:
  path: /api/models/reports/{resource_type}/{resource_id}
  controller: Ox\Mediboard\CompteRendu\Controllers\MedicalReportsController::list
  methods: GET
  requirements:
    resource_type: '\w+'
    resource_id: '\d+'
  defaults:
    permission: read
  options:
    summary: List reports for a context.
    description: |
      **List reports by 'resource_type' and 'resource_id' request parameter.**
      
      Available relations:
      `author`, `files`
      
      Available fieldsets:
      `author`
    accept:
      - application/json
    responses:
      200: The response's body contains the report in json_api format.
      403: Access deny.
      500: Internal server error.

models_api_medical_reports_show:
  path: /api/models/reports/{compte_rendu_id}
  controller: Ox\Mediboard\CompteRendu\Controllers\MedicalReportsController::show
  methods: GET
  requirements:
    compte_rendu_id: '\d+'
  defaults:
    permission: read
  options:
    summary: Show a report.
    description: |
      **Show a report by 'compte_rendu_id' request parameter.**
      
      Available relations:
      `author`, `files`

      Available fieldsets:
      `author`
    accept:
      - application/json
    responses:
      200: The response's body contains the report in json_api format.
      403: Access deny.
      404: Not found.
      500: Internal server error.

models_api_medical_reports_update:
  path: /api/models/reports/{compte_rendu_id}
  controller: Ox\Mediboard\CompteRendu\Controllers\MedicalReportsController::update
  methods: PATCH
  requirements:
    compte_rendu_id: '\d+'
  defaults:
    permission: edit
  options:
    summary: Partially update a report.
    description: |
      **Update a report identified by 'compte_rendu_id' parameter.**
      
      Available updatable fields:
      `annule` (*), `nom` (*).
    accept:
      - application/json
    body:
      required: true
      content-type:
        - application/json
    responses:
      200: |
        The report identified by 'compte_rendu_id' has been updated.
        The response's body contains the updated report in json_api format.
      403: Access deny.
      404: Not found.
      500: Internal server error.

models_api_medical_reports_delete:
  path: /api/models/reports/{compte_rendu_id}
  controller: Ox\Mediboard\CompteRendu\Controllers\MedicalReportsController::delete
  methods: DELETE
  defaults:
    permission: edit
  requirements:
    compte_rendu_id: '\d+'
  options:
    summary: Delete a report.
    description: |
      **Delete a report identified by 'compte_rendu_id' parameter.**
    accept:
      - application/json
    responses:
      204: The report identified by 'compte_rendu_id' parameter has been deleted.
      403: Access deny.
      404: Not found.
      500: Internal server error.
