<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Functional\Controllers;

use DateTimeImmutable;
use Exception;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\CompteRendu\Tests\Fixtures\MedicalReportFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\System\CContentHTML;
use Ox\Tests\JsonApi\AbstractObject;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Ox\Tests\TestsException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test class for MedicalReportsController.
 */
class MedicalReportsControllerTest extends OxWebTestCase
{
    /**
     * Test: List a report by 'resource_type' and 'resource_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListWithPermission(): CCompteRendu
    {
        /** @var CPatient $patient */
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            MedicalReportFixtures::MEDICAL_REPORT_PATIENT_TAG
        );

        $resource_type = $patient::RESOURCE_TYPE;
        $resource_id   = $patient->_id;

        // Create client.
        $client = self::createClient();
        $client->request(
            'GET',
            "/api/models/reports/{$resource_type}/{$resource_id}",
            ['limit' => 3]
        );

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Collection (data).
        $collection = $this->getJsonApiCollection($client);
        $this->assertEquals(3, $collection->count());

        foreach ($collection as $item) {
            $this->assertEquals(CCompteRendu::RESOURCE_TYPE, $item->getType());
            $this->assertNotNull($item->getId());
        }

        return CCompteRendu::findOrFail($collection->getFirstItem()->getId());
    }

    /**
     * Test: List a report by 'resource_type' and 'resource_id' with no permission on context.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListWithoutPermissionOnContext(): void
    {
        /** @var CPatient $patient */
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            MedicalReportFixtures::MEDICAL_REPORT_PATIENT_TAG
        );

        $resource_type = $patient::RESOURCE_TYPE;
        $resource_id   = $patient->_id;
        $current_user  = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "/api/models/reports/{$resource_type}/{$resource_id}");

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$patient->_class][$patient->_id] = $actual_perm;
            }
        }
    }

    /**
     * Test: List a report by 'resource_type' and 'resource_id' with no permission on one report.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testListWithoutPermissionOnOneReport(): void
    {
        /** @var CPatient $patient */
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            MedicalReportFixtures::MEDICAL_REPORT_PATIENT_TAG
        );
        $report = $this->getObjectFromFixturesReference(
            CCompteRendu::class,
            MedicalReportFixtures::MEDICAL_REPORT_TAG . 1
        );

        $resource_type = $patient::RESOURCE_TYPE;
        $resource_id   = $patient->_id;
        $current_user  = CUser::get();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$report->_class][$report->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$report->_class][$report->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request(
                'GET',
                "/api/models/reports/{$resource_type}/{$resource_id}",
                ['limit' => 3]
            );

            // Asserting: Successful response.
            $this->assertResponseStatusCodeSame(Response::HTTP_OK);

            // Asserting: Collection (data).
            $collection = $this->getJsonApiCollection($client);
            $this->assertEquals(2, $collection->count());

            foreach ($collection as $item) {
                $this->assertEquals(CCompteRendu::RESOURCE_TYPE, $item->getType());
                $this->assertNotNull($item->getId());
            }
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$report->_class][$report->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$report->_class][$report->_id] = $actual_perm;
            }
        }
    }

    /**
     * Test: Show a report by 'compte_rendu_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     * @depends testListWithPermission
     */
    public function testShowWithPermission(CCompteRendu $medical_report): void
    {
        // Create client.
        $client = self::createClient();
        $client->request('GET', "/api/models/reports/{$medical_report->_id}", ['relations' => 'all']);

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($medical_report::RESOURCE_TYPE, $item->getType());
        $this->assertEquals($medical_report->_id, $item->getId());

        // Asserting: Relation author.
        $this->assertTrue($item->hasRelationship(CCompteRendu::RELATION_AUTHOR));
        $this->assertEquals(
            CMediusers::RESOURCE_TYPE,
            $item->getRelationship(CCompteRendu::RELATION_AUTHOR)->getType()
        );
        $this->assertEquals($medical_report->author_id, $item->getRelationship(CCompteRendu::RELATION_AUTHOR)->getId());
    }

    /**
     * Test: Show a report by 'compte_rendu_id' without permission.
     *
     * @throws Exception
     * @depends testListWithPermission
     */
    public function testShowWithoutPermission(CCompteRendu $medical_report): void
    {
        $current_user = CUser::get();
        $context      = $medical_report->loadTargetObject();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('GET', "/api/models/reports/{$medical_report->_id}");

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] = $actual_perm;
            }
        }
    }

    /**
     * Test: Update a report by 'compte_rendu_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     * @depends testListWithPermission
     */
    public function testUpdateWithPermission(CCompteRendu $medical_report): void
    {
        $data = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID         => $medical_report->_id,
                    Item::TYPE       => $medical_report::RESOURCE_TYPE,
                    Item::ATTRIBUTES => [
                        'annule'     => '1',
                        'nom'        => 'MedicalReportTest',
                        'etat_envoi' => 'oui',
                        'doc_size'   => '20',
                    ],
                ],
            ]
        );

        // Create client.
        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request('PATCH', "/api/models/reports/{$medical_report->_id}", [], [], [], json_encode($data));

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Asserting: Item (data).
        $item = $this->getJsonApiItem($client);
        $this->assertEquals($data->getType(), $item->getType());
        $this->assertEquals($data->getId(), $item->getId());

        // Asserting: Item (attributes modified).
        $this->assertEquals($data->getAttribute('annule'), $item->getAttribute('annule'));
        $this->assertEquals($data->getAttribute('nom'), $item->getAttribute('nom'));

        // Asserting: Item (attributes not modified).
        $this->assertNotEquals($data->getAttribute('etat_envoi'), $item->getAttribute('etat_envoi'));
        $this->assertNotEquals($data->getAttribute('doc_size'), $item->getAttribute('doc_size'));
    }

    /**
     * Test: Update a report by 'compte_rendu_id' without permission.
     *
     * @throws TestsException
     * @throws Exception
     * @depends testListWithPermission
     */
    public function testUpdateWithoutPermission(CCompteRendu $medical_report): void
    {
        $data = Item::createFromArray(
            [
                AbstractObject::DATA => [
                    Item::ID         => $medical_report->_id,
                    Item::TYPE       => $medical_report::RESOURCE_TYPE,
                    Item::ATTRIBUTES => [
                        'annule'     => '1',
                        'nom'        => 'MedicalReportTest',
                        'etat_envoi' => 'oui',
                        'doc_size'   => '20',
                    ],
                ],
            ]
        );

        $current_user = CUser::get();
        $context      = $medical_report->loadTargetObject();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $this->setJsonApiContentTypeHeader($client)
                 ->request('PATCH', "/api/models/reports/{$medical_report->_id}", [], [], [], json_encode($data));

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] = $actual_perm;
            }
        }
    }

    /**
     * Test: Delete a report by 'compte_rendu_id' with permission.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testDeleteWithPermission(): void
    {
        $medical_report = $this->createMedicalReport();

        // Create client.
        $client = self::createClient();
        $client->request('DELETE', "/api/models/reports/{$medical_report->_id}");

        // Asserting: Successful response.
        $this->assertResponseStatusCodeSame(Response::HTTP_NO_CONTENT);
        $this->assertEmpty($client->getResponse()->getContent());

        // Asserting: Report deleted.
        $this->assertFalse(CCompteRendu::find($medical_report->_id));
    }

    /**
     * Test: Delete a report by 'compte_rendu_id' without permission.
     *
     * @throws TestsException
     * @throws Exception
     */
    public function testDeleteWithoutPermission(): void
    {
        $medical_report = $this->createMedicalReport();

        $current_user = CUser::get();
        $context      = $medical_report->loadTargetObject();

        try {
            $actual_perm = CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] ?? null;
            CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] = PERM_DENY;

            // Create client.
            $client = self::createClient();
            $client->request('DELETE', "/api/models/reports/{$medical_report->_id}");

            // Asserting: Error response.
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
        } finally {
            if ($actual_perm === null) {
                unset(CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id]);
            } else {
                CPermObject::$users_cache[$current_user->_id][$context->_class][$context->_id] = $actual_perm;
            }

            $this->deleteOrFailed($medical_report);
        }
    }

    /**
     * Create medical report for delete test.
     *
     * @throws TestsException
     * @throws Exception
     */
    private function createMedicalReport(): CCompteRendu
    {
        $user    = $this->getObjectFromFixturesReference(
            CMediusers::class,
            MedicalReportFixtures::MEDICAL_REPORT_USER_TAG
        );
        $patient = $this->getObjectFromFixturesReference(
            CPatient::class,
            MedicalReportFixtures::MEDICAL_REPORT_PATIENT_TAG
        );

        // Create content.
        $content_html                = new CContentHTML();
        $content_html->last_modified = (new DateTimeImmutable())->format('Y-m-d H:i:s');
        $content_html->content       = bin2hex(random_bytes(10));
        $this->storeOrFailed($content_html);

        // Create medical report.
        $medical_report               = new CCompteRendu();
        $medical_report->content_id   = $content_html->_id;
        $medical_report->object_class = $patient->_class;
        $medical_report->object_id    = $patient->_id;
        $medical_report->nom          = bin2hex(random_bytes(10));
        $this->storeOrFailed($medical_report);

        $medical_report->author_id = $user->_id;
        $this->storeOrFailed($medical_report);

        return $medical_report;
    }
}
