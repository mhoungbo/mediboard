<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Functional\Controllers;

use Ox\Core\CAppUI;
use Ox\Core\CMbString;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\CompteRendu\CAideSaisie;
use Ox\Mediboard\CompteRendu\HelpedText\HelpedTextManager;
use Ox\Mediboard\CompteRendu\Tests\Fixtures\HelpedTextFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\JsonApi\Item;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\StreamedResponse;

class HelpedTextControllerTest extends OxWebTestCase
{
    public function testListForContext(): void
    {
        /** @var CMediusers $user */
        $user = $this->getObjectFromFixturesReference(CMediusers::class, HelpedTextFixtures::HELPED_TEXT_USER);

        $client = self::createClient([], [], $user->loadRefUser());
        $client->request(
            'GET',
            '/api/models/helped_texts/consultation/rques',
            [
                'relations'         => 'owner',
                'fieldsets'         => 'default,target,depends',
                'with_words_tokens' => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);
        // Cannot assert exactly 4 because there can be other helped texts for the instance
        $this->assertGreaterThanOrEqual(4, $collection->count());

        $first_helped_text = $collection->getFirstItem();
        $this->assertEquals('helped_text', $first_helped_text->getType());

        $group_in_included    = false;
        $function_in_included = false;
        $user_in_included     = false;
        $includes             = $collection->getIncluded();
        $this->assertCount(3, $includes);

        /** @var Item $include */
        foreach ($includes as $include) {
            if ($include->getType() === 'group' && $include->getId() === $user->loadRefFunction()->group_id) {
                $group_in_included = true;
                continue;
            }

            if ($include->getType() === 'function' && $include->getId() === $user->loadRefFunction()->_id) {
                $function_in_included = true;
                continue;
            }

            if ($include->getType() === 'mediuser' && $include->getId() === $user->_id) {
                $user_in_included = true;
            }
        }

        $this->assertTrue($group_in_included);
        $this->assertTrue($function_in_included);
        $this->assertTrue($user_in_included);

        $tokens = $collection->getMeta('words_tokens');
        $this->assertCount(4, $tokens['testfoo']);
        $this->assertCount(4, $tokens['testbar']);
        $this->assertCount(1, $tokens['testinstance']);
        $this->assertCount(1, $tokens['testgroup']);
        $this->assertCount(1, $tokens['testfunction']);
        $this->assertCount(1, $tokens['testuser']);
    }

    public function testListForContextWithParameterAll(): void
    {
        /** @var CMediusers $user */
        $user = $this->getObjectFromFixturesReference(CMediusers::class, HelpedTextFixtures::HELPED_TEXT_USER);

        $client = self::createClient([], [], $user->loadRefUser());
        $client->request(
            'GET',
            '/api/models/helped_texts/consultation/rques',
            [
                'all'               => '1',
                'with_words_tokens' => 1,
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $collection = $this->getJsonApiCollection($client);
        // No total should be added because no pagination is used with the parameter all
        $this->assertFalse($collection->hasMeta('total'));

        $this->assertGreaterThanOrEqual(4, $collection->count());
    }

    public function testCreate(): void
    {
        $content = new Item('helped_text');
        $content->setAttributes(
            [
                'name'  => 'new helped text',
                'text'  => 'some text that will be searched',
                'class' => 'consultation',
                'field' => 'rques',
            ]
        );

        $client = self::createClient();
        $this->setJsonApiContentTypeHeader($client)
             ->request(
                 'POST',
                 '/api/models/helped_texts?fieldsets=default,target',
                 [],
                 [],
                 [],
                 json_encode($content)
             );

        $this->assertResponseStatusCodeSame(201);

        $item = $this->getJsonApiCollection($client)->getFirstItem();
        $this->assertNotNull($item->getId());
        $this->assertEquals('helped_text', $item->getType());
        $this->assertEquals('new helped text', $item->getAttribute('name'));
        $this->assertEquals('some text that will be searched', $item->getAttribute('text'));
        $this->assertEquals('consultation', $item->getAttribute('class'));
        $this->assertEquals('rques', $item->getAttribute('field'));
    }

    public function testIndex(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/models/helped_texts/index', ['ajax' => 1]);

        $this->assertResponseStatusCodeSame(200);

        $script = $crawler->filterXPath('//script[position()=3]');
        $this->assertCount(1, $script);

        $this->assertStringContainsString('Aide.edit_url =', $script->text());
        $this->assertStringContainsString('Aide.search_url =', $script->text());
        $this->assertStringContainsString('Aide.import_url =', $script->text());
        $this->assertStringContainsString('Aide.depends_url =', $script->text());

        $this->assertCount(1, $crawler->filterXPath('//button[@class="new" and @onclick="Aide.edit()"]'));

        $form = $crawler->filterXPath('//form[@name="filterFrm"]');
        $this->assertCount(1, $form);
        $this->assertEquals(0, $form->filterXPath('//input[@type="hidden" and @name="start_user"]')->attr('value'));
        $this->assertEquals(0, $form->filterXPath('//input[@type="hidden" and @name="start_function"]')->attr('value'));
        $this->assertEquals(0, $form->filterXPath('//input[@type="hidden" and @name="start_group"]')->attr('value'));
        $this->assertEquals(0, $form->filterXPath('//input[@type="hidden" and @name="start_instance"]')->attr('value'));
        $this->assertEquals(
            '',
            $form->filterXPath('//input[@type="hidden" and @name="order_col_aide"]')->attr('value')
        );
        $this->assertEquals('', $form->filterXPath('//input[@type="hidden" and @name="order_way"]')->attr('value'));

        $this->assertCount(1, $crawler->filterXPath('//div[@id="tabs_aides"]'));
    }

    /**
     * @config [CConfiguration] dPcompteRendu CAideSaisie access_group 1
     * @config [CConfiguration] dPcompteRendu CAideSaisie access_function 1
     */
    public function testVwList(): void
    {
        $crawler = self::createClient()->request('GET', '/gui/models/helped_texts/vwList', ['ajax' => 1]);
        $this->assertResponseStatusCodeSame(200);

        $this->assertStringContainsString(
            "Control.Tabs.create('tabs-owner', true);",
            $crawler->filterXPath('//script')->text()
        );

        $ul = $crawler->filterXPath('//ul[@id="tabs-owner"]');
        $this->assertCount(1, $ul);

        $this->assertCount(1, $ul->filterXPath('//li//a[@href="#owner-user"]'));
        $this->assertCount(1, $ul->filterXPath('//li//a[@href="#owner-func"]'));
        $this->assertCount(1, $ul->filterXPath('//li//a[@href="#owner-etab"]'));
        $this->assertCount(1, $ul->filterXPath('//li//a[@href="#owner-instance"]'));

        $this->assertCount(1, $crawler->filterXPath('//div[@id="owner-user"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="owner-func"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="owner-etab"]'));
        $this->assertCount(1, $crawler->filterXPath('//div[@id="owner-instance"]'));
    }

    public function testVwListExport(): void
    {
        $client = self::createClient();
        ob_start();
        $client->request('GET', '/gui/models/helped_texts/vwList', ['export' => 1, 'type' => 'User']);
        $content = ob_get_contents();
        ob_end_clean();

        $this->assertResponseStatusCodeSame(200);
        $this->assertInstanceOf(StreamedResponse::class, $client->getResponse());
        $this->assertStringStartsWith('class,field,name,text,depend_value_1,depend_value_2,chapitre', $content);
    }

    /**
     * @config [CConfiguration] dPcompteRendu CAideSaisie access_group 1
     * @config [CConfiguration] dPcompteRendu CAideSaisie access_function 1
     */
    public function testVwEdit(): void
    {
        $user = $this->getObjectFromFixturesReference(CMediusers::class, HelpedTextFixtures::HELPED_TEXT_USER);
        /** @var CAideSaisie $helped_text */
        $helped_text = $user->loadFirstBackRef('aides_saisie');
        $crawler     = self::createClient()->request(
            'GET',
            '/gui/models/helped_texts/vwEdit',
            ['ajax' => 1, 'aide_id' => $helped_text->_id]
        );

        $this->assertResponseStatusCodeSame(200);

        $script = $crawler->filterXPath('//script[position()=2]');
        $this->assertCount(1, $script);
        $this->assertStringContainsString('Aide.classes =', $script->text());
        $this->assertStringContainsString("Aide.loadClasses('$helped_text->class');", $script->text());
        $this->assertStringContainsString("Aide.loadFields('$helped_text->field');", $script->text());
        $this->assertStringContainsString(
            "Aide.loadDependances('$helped_text->depend_value_1', '$helped_text->depend_value_2');",
            $script->text()
        );
        $this->assertStringContainsString(
            "HyperTextLink.getListFor('$helped_text->_id', '$helped_text->_class');",
            $script->text()
        );
        $this->assertStringContainsString('Aide.makeUserAutocomplete(form);', $script->text());
        $this->assertStringContainsString('Aide.makeFunctionAutocomplete(form);', $script->text());
        $this->assertStringContainsString('Aide.makeGroupAutocomplete(form);', $script->text());

        $this->assertEmpty($crawler->filterXPath('//ul[@id="aide-info"]'));

        $form = $crawler->filterXPath('//form[@name="editFrm" and @method="post"]');
        $this->assertCount(1, $form);
        $this->assertStringContainsString('Aide.loadTabsAides();', $form->attr('onsubmit'));
        $this->assertStringEndsWith(
            '/gui/models/helped_texts',
            $form->filterXPath('//input[@type="hidden" and @name="@route"]')->attr('value')
        );
        $this->assertEquals(
            $helped_text->_id,
            $form->filterXPath('//input[@type="hidden" and @name="aide_id"]')->attr('value')
        );
        $this->assertEquals(0, $form->filterXPath('//input[@type="hidden" and @name="del"]')->attr('value'));
        $this->assertEquals(0, $form->filterXPath('//input[@type="hidden" and @name="restricted"]')->attr('value'));
    }

    /**
     * @config [CConfiguration] dPcompteRendu CAideSaisie access_group 1
     * @config [CConfiguration] dPcompteRendu CAideSaisie access_function 1
     */
    public function testVwEditRestricted(): void
    {
        $crawler = self::createClient()->request(
            'GET',
            '/gui/models/helped_texts/vwEdit',
            ['ajax' => 1, 'restricted' => 1, 'class' => 'CCompteRendu', 'field' => '_source']
        );

        $this->assertResponseStatusCodeSame(200);

        $script = $crawler->filterXPath('//script[position()=2]');
        $this->assertCount(1, $script);

        $this->assertStringNotContainsString("Aide.loadClasses(';", $script->text());
        $this->assertStringNotContainsString("Aide.loadFields(';", $script->text());
        $this->assertStringContainsString("Aide.loadTabsAides('CCompteRendu', '_source');", $script->text());
        $this->assertStringContainsString("Control.Tabs.create('aide-info');", $script->text());
        $this->assertStringNotContainsString("HyperTextLink.getListFor('", $script->text());

        $this->assertStringContainsString('Aide.makeUserAutocomplete(form);', $script->text());
        $this->assertStringContainsString('Aide.makeFunctionAutocomplete(form);', $script->text());
        $this->assertStringContainsString('Aide.makeGroupAutocomplete(form);', $script->text());

        $translator = new Translator();
        $ul         = $crawler->filterXPath('//ul[@id="aide-info"]');
        $this->assertCount(1, $ul);
        $this->assertEquals(
            $translator->tr('CAideSaisie'),
            mb_convert_encoding($ul->filterXPath('//li//a[@href="#edit-aide"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
        $this->assertEquals(
            $translator->tr('CAideSaisie|pl'),
            mb_convert_encoding($ul->filterXPath('//li//a[@href="#tabs_aides"]')->text(), 'ISO-8859-1', 'UTF-8')
        );
    }

    public function testDoEditCreate(): CAideSaisie
    {
        $name   = 'test-' . uniqid();
        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/models/helped_texts',
            [
                'user_id' => CMediusers::get()->_id,
                'class'   => 'CConsultation',
                'field'   => 'rques',
                'name'    => $name,
                'text'    => uniqid(),
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $translator = new Translator();
        $this->assertStringContainsString(
            $translator->tr('CAideSaisie-msg-create'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $helped_text       = new CAideSaisie();
        $helped_text->name = $name;
        $helped_text->loadMatchingObjectEsc();

        return $helped_text;
    }

    /**
     * @depends testDoEditCreate
     */
    public function testDoEditUpdate(CAideSaisie $helped_text): CAideSaisie
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/models/helped_texts',
            [
                'aide_id' => $helped_text->_id,
                'text'    => 'New text',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $translator = new Translator();
        $this->assertStringContainsString(
            $translator->tr('CAideSaisie-msg-modify'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $helped_text = CAideSaisie::findOrFail($helped_text->_id);
        $this->assertEquals('New text', $helped_text->text);

        return $helped_text;
    }

    /**
     * @depends testDoEditUpdate
     */
    public function testDoEditRestricted(CAideSaisie $helped_text): CAideSaisie
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/models/helped_texts',
            [
                'restricted' => 1,
                'aide_id'    => $helped_text->_id,
                'text'       => 'New text bis',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $translator = new Translator();
        $this->assertStringContainsString(
            $translator->tr('CAideSaisie-msg-modify'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $this->assertStringContainsString(
            "AideSaisie.create('', '', '', '', '', '', '', '', '', {$helped_text->_id});",
            html_entity_decode($client->getResponse()->getContent())
        );

        $helped_text = CAideSaisie::findOrFail($helped_text->_id);
        $this->assertEquals('New text bis', $helped_text->text);

        return $helped_text;
    }

    /**
     * @depends testDoEditRestricted
     */
    public function testDoEditDelete(CAideSaisie $helped_text): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/gui/models/helped_texts',
            [
                'del'     => 1,
                'aide_id' => $helped_text->_id,
                'text'    => 'New text',
            ]
        );

        $this->assertResponseStatusCodeSame(200);

        $translator = new Translator();
        $this->assertStringContainsString(
            $translator->tr('CAideSaisie-msg-delete'),
            html_entity_decode($client->getResponse()->getContent())
        );

        $this->assertFalse(CAideSaisie::find($helped_text->_id));
    }

    public function testImportGet(): void
    {
        $crawler = self::createClient()
                       ->request('GET', '/gui/models/helped_texts/import', ['owner_guid' => 'Instance']);

        $this->assertResponseStatusCodeSame(200);

        $form = $crawler->filterXPath('//form[@name="import" and @method="post"]');
        $this->assertCount(1, $form);
        $this->assertStringEndsWith('/gui/models/helped_texts/import', $form->attr('action'));
        $this->assertEquals(
            'Instance',
            $form->filterXPath('//input[@type="hidden" and @name="owner_guid"]')->attr('value')
        );
        $this->assertEquals(
            '4096000',
            $form->filterXPath('//input[@type="hidden" and @name="MAX_FILE_SIZE"]')->attr('value')
        );
        $this->assertEquals('import', $form->filterXPath('//input[@type="file"]')->attr('name'));
        $this->assertEquals((new Translator())->tr('Save'), $form->filterXPath('//button[@type="submit"]')->text());
    }

    public function testImportPost(): void
    {
        $manager = $this->getMockBuilder(HelpedTextManager::class)
                        ->onlyMethods(['importHelpedTexts'])
                        ->getMock();
        $manager->method('importHelpedTexts')->willReturn(
            [
                CAppUI::UI_MSG_OK    => [
                    'message 1 ok',
                    'message 2 ok',
                ],
                CAppUI::UI_MSG_ERROR => [
                    'message 1 error',
                ],
            ]
        );

        $client    = self::createClient();
        $container = $client->getContainer();
        $container->set(HelpedTextManager::class, $manager);

        $client->request(
            'POST',
            '/gui/models/helped_texts/import?ajax=1',
            ['owner_guid' => 'Instance'],
            ['import' => new UploadedFile('/foo', 'foo', null, UPLOAD_ERR_NO_FILE, true)]
        );

        $this->assertResponseStatusCodeSame(200);

        $content = html_entity_decode($client->getResponse()->getContent());

        $translator = new Translator();
        $this->assertStringContainsString(
            '<div class="info">' . $translator->tr('message 1 ok') . '</div><div class="info">'
            . $translator->tr('message 2 ok') . '</div><div class="error">'
            . $translator->tr('message 1 error') . '</div>',
            $content
        );
    }

    public function testListDepends(): void
    {
        $crawler = self::createClient()
                       ->request('GET', '/gui/models/helped_texts/depends/CCompteRendu/_source', ['ajax' => 1]);

        $translator = new Translator();
        $this->assertStringEndsWith($translator->tr('None'), $crawler->filterXPath('//option[@value=""]')->text());
    }

    public function testGetJson(): void
    {
        /** @var CMediusers $user */
        $user = $this->getObjectFromFixturesReference(CMediusers::class, HelpedTextFixtures::HELPED_TEXT_USER);
        $function = $user->loadRefFunction();

        $client = self::createClient([], [], $user->loadRefUser());
        $client->request('GET', '/gui/models/helped_texts/consultation/rques');

        $this->assertResponseStatusCodeSame(200);

        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('expire', $response);
        $this->assertArrayHasKey('aides', $response['data']);
        $this->assertGreaterThanOrEqual(4, count($response['data']['aides']));
        $this->assertArrayHasKey('by_token', $response['data']);
        $this->assertArrayHasKey('owners', $response['data']);
        $this->assertArrayHasKey($user->_id, $response['data']['owners']['u']);
        $this->assertArrayHasKey($function->_id, $response['data']['owners']['f']);
        $this->assertArrayHasKey($function->group_id, $response['data']['owners']['g']);
    }
}
