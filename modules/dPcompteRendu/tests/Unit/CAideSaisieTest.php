<?php
/**
 * @package Mediboard\CompteRendu\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Unit;

use Ox\Core\Api\Request\Content\JsonApiItem;
use Ox\Core\CMbException;
use Ox\Mediboard\Cabinet\CConsultation;
use Ox\Mediboard\CompteRendu\CAideSaisie;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Tests\OxUnitTestCase;

class CAideSaisieTest extends OxUnitTestCase
{

    public function test__construct()
    {
        $aide = new CAideSaisie();
        $this->assertInstanceOf(CAideSaisie::class, $aide);
    }

    public function testLoadRefFunction()
    {
        $aide = new CAideSaisie();
        $this->assertInstanceOf(CFunctions::class, $aide->loadRefFunction());
    }

    public function testLoadRefUser()
    {
        $aide = new CAideSaisie();
        $this->assertInstanceOf(CMediusers::class, $aide->loadRefUser());
    }

    public function testLoadRefGroup()
    {
        $aide = new CAideSaisie();
        $this->assertInstanceOf(CGroups::class, $aide->loadRefGroup());
    }

    /**
     * @return array
     */
    public function classesDependValues()
    {
        return [
            "consultation" => [CConsultation::class],
            "sejour"       => [CSejour::class],
        ];
    }

    public function testSetResourceOwnerWithNull(): void
    {
        $helped_text              = new CAideSaisie();
        $helped_text->user_id     = 1;
        $helped_text->function_id = 2;
        $helped_text->group_id    = 3;

        $helped_text->setResourceOwner(null);
        $this->assertTrue($helped_text->user_id === '');
        $this->assertTrue($helped_text->function_id === '');
        $this->assertTrue($helped_text->group_id === '');
    }

    public function testSetResourceOwnerWithInvalidApiType(): void
    {
        $this->expectExceptionObject(new CMbException('CAideSaisie-Error-Owner must be user, function or group'));

        (new CAideSaisie())->setResourceOwner(new JsonApiItem(['type' => 'token', 'id' => 10]));
    }

    public function testSetResourceOwner(): void
    {
        $helped_text              = new CAideSaisie();
        $helped_text->user_id     = 1;
        $helped_text->function_id = 2;
        $helped_text->group_id    = 3;

        $current_user = CMediusers::get();

        $helped_text->setResourceOwner(new JsonApiItem(['type' => 'mediuser', 'id' => $current_user->_id]));
        $this->assertEquals($current_user->_id, $helped_text->user_id);
        $this->assertTrue($helped_text->function_id === '');
        $this->assertTrue($helped_text->group_id === '');

        $function = $current_user->loadRefFunction();
        $helped_text->setResourceOwner(new JsonApiItem(['type' => 'function', 'id' => $function->_id]));
        $this->assertEquals($function->_id, $helped_text->function_id);
        $this->assertTrue($helped_text->user_id === '');
        $this->assertTrue($helped_text->group_id === '');

        $helped_text->setResourceOwner(new JsonApiItem(['type' => 'group', 'id' => $function->group_id]));
        $this->assertEquals($function->group_id, $helped_text->group_id);
        $this->assertTrue($helped_text->user_id === '');
        $this->assertTrue($helped_text->function_id === '');
    }

    /**
     * @dataProvider storeDataProvider
     */
    public function testStore (string $name, string $text, string $expectedName, string $expectedText) {
        $helpedText = new CAideSaisie();
        $helpedText->_owner = "user";
        $helpedText->user_id = CMediusers::get()->_id;
        $helpedText->class = "sample_movie";
        $helpedText->field = "description";
        $helpedText->name = $name;
        $helpedText->text = $text;

        $helpedText->store();

        self::assertEquals($expectedName, $helpedText->name);
        self::assertEquals($expectedText, $helpedText->text);

        $helpedText->delete();
    }

    public function storeDataProvider () {
        return [
            "default" => [
                "name",
                "text",
                "name",
                "text"
            ],
            "With linebreak" => [
                "name\nfoo",
                "text\r\nlorem",
                "name\nfoo",
                "text\nlorem"
            ],
            "With html" => [
                "<h1>name</h1><p>lorem</p>",
                "<p>ipsum</p><p> lorem</p>",
                "namelorem",
                "ipsum lorem",
            ]
        ];
    }
}
