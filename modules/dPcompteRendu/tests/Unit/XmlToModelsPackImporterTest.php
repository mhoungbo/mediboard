<?php

/**
 * @package Mediboard\CompteRendu\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Unit;

use Ox\Core\CMbException;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\CompteRendu\XmlToModelsPackImporter;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use ReflectionException;

class XmlToModelsPackImporterTest extends OxUnitTestCase
{
    /**
     * @throws CMbException
     */
    public function testConstructThrowsException(): void
    {
        $this->expectExceptionMessage("common-error-No file found.");
        new XmlToModelsPackImporter(null, null, []);
    }

    /**
     * @param string $owner_guid
     * @param array  $expected
     *
     * @return void
     * @throws CMbException
     * @throws TestsException
     * @throws ReflectionException
     * @dataProvider getOwnerProvider
     */
    public function testGetOwnerReturnsExpectedArray(string $owner_guid, array $expected): void
    {
        $importer  = new XmlToModelsPackImporter(null, $owner_guid, ["lorem"]);
        $get_owner = $this->invokePrivateMethod($importer, "getOwner");

        $this->assertEquals($expected, $get_owner);
    }

    /**
     * @throws TestsException
     */
    public function getOwnerProvider()
    {
        $user     = $this->getObjectFromFixturesReference(CMediusers::class, UsersFixtures::REF_USER_LOREM_IPSUM);
        $function = $this->getObjectFromFixturesReference(CFunctions::class, UsersFixtures::REF_FIXTURES_FUNCTION);
        $group    = $this->getObjectFromFixturesReference(CGroups::class, UsersFixtures::REF_FIXTURES_GROUP);

        return [
            "Instance"      => ["Instance", ["", "", ""]],
            "User"          => [$user->_guid, [$user->_id, "", ""]],
            "Fonction"      => [$function->_guid, ["", $function->_id, ""]],
            "Etablissement" => [$group->_guid, ["", "", $group->_id]],
        ];
    }
}
