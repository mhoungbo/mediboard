<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Unit\HelpedText;

use Ox\Core\Cache;
use Ox\Core\CMbException;
use Ox\Core\CModelObjectCollection;
use Ox\Core\Config\Conf;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\CompteRendu\CAideSaisie;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\CompteRendu\HelpedText\HelpedTextManager;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Sante400\CHyperTextLink;
use Ox\Tests\OxUnitTestCase;
use stdClass;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class HelpedTextManagerTest extends OxUnitTestCase
{
    public function testTokenize(): void
    {
        $aide_with_name       = new CAideSaisie();
        $aide_with_name->_id  = 1;
        $aide_with_name->name = 'bar/t�st&@mail.com';
        $aide_with_name->text = 'bar/t�st&@mail.com';

        $aide_without_name       = new CAideSaisie();
        $aide_without_name->_id  = 2;
        $aide_without_name->text = 'foo{another|ne#~test_com';
        $aide_without_name->name = 'something';

        $tokens = (new HelpedTextManager())->tokenize([$aide_with_name, $aide_without_name], ['ne' => true]);

        $this->assertEquals(
            [
                'bar'       => [1],
                'test'      => [1, 2],
                'mail'      => [1],
                'com'       => [1, 2],
                'something' => [2],
                'foo'       => [2],
                'another'   => [2],
            ],
            $tokens
        );
    }

    public function testGetHelpedClassesWithFields(): void
    {
        $manager = $this->getMockBuilder(HelpedTextManager::class)
                        ->setConstructorArgs([new Conf(), Cache::getCache(Cache::NONE)])
                        ->onlyMethods(['getClasses'])
                        ->getMock();
        $manager->method('getClasses')->willReturn(
            ['CCompteRendu', 'CIdSante400', 'CDossierMedical']
        );

        $helped_classes = $manager->getHelpedClassesWithFields();
        $this->assertArrayHasKey('_source', $helped_classes['CCompteRendu']);
        $this->assertEquals(
            (new CCompteRendu())->_specs['_list_classes']->_list,
            array_keys($helped_classes['CCompteRendu']['_source']['depend_value_1'])
        );
        $this->assertArrayNotHasKey('CIdSante400', $helped_classes);
        $this->assertEquals(['facteurs_risque' => null], $helped_classes['CDossierMedical']);
    }

    public function testChangeClassNameForApiTypeWithInvalidType(): void
    {
        $helped_text        = new CAideSaisie();
        $helped_text->class = HelpedTextManager::class;

        if (PHP_VERSION_ID >= 80_000) {
            $this->expectError();
            (new HelpedTextManager())->changeClassNameForApiType([$helped_text]);
        } else {
            $results = @(new HelpedTextManager())->changeClassNameForApiType([$helped_text]);
            $result  = array_pop($results);
            $this->assertNull($result->class);
        }
    }

    public function testChangeClassNameForApiType(): void
    {
        $helped_text_user            = new CAideSaisie();
        $helped_text_user->class     = 'CUser';
        $helped_text_mediuser        = new CAideSaisie();
        $helped_text_mediuser->class = 'CMediusers';

        $results = (new HelpedTextManager())->changeClassNameForApiType([$helped_text_user, $helped_text_mediuser]);

        $helped_text_user = array_shift($results);
        $this->assertEquals('user', $helped_text_user->class);

        $helped_text_mediuser = array_shift($results);
        $this->assertEquals('mediuser', $helped_text_mediuser->class);
    }

    public function testCorrectHelpedTextWithOtherObject(): void
    {
        $this->expectExceptionObject(new CMbException('HelpedTextManager-Error-Expected an helped text object'));
        (new HelpedTextManager())->correctHelpedTexts(new CModelObjectCollection([new CUser()]));
    }

    public function testCorrectHelpedTextWithAdmin(): void
    {
        $manager = $this->getMockBuilder(HelpedTextManager::class)
                        ->onlyMethods(['isAdmin'])
                        ->getMock();
        $manager->method('isAdmin')->willReturn(true);

        $helped_text_1              = new CAideSaisie();
        $helped_text_1->function_id = 1;
        $helped_text_1->class       = 'user';

        $helped_text_2          = new CAideSaisie();
        $helped_text_2->user_id = 1;

        $collection = new CModelObjectCollection([$helped_text_1, $helped_text_2]);
        $manager->correctHelpedTexts($collection);

        $helped_text_1 = $collection[0];
        $this->assertEquals(1, $helped_text_1->function_id);
        $this->assertEquals('CUser', $helped_text_1->class);

        $helped_text_2 = $collection[1];
        $this->assertEquals(1, $helped_text_2->user_id);
    }

    public function testCorrectHelpedTextWithoutAdmin(): void
    {
        $manager = $this->getMockBuilder(HelpedTextManager::class)
                        ->onlyMethods(['isAdmin'])
                        ->getMock();
        $manager->method('isAdmin')->willReturn(false);

        $helped_text_1              = new CAideSaisie();
        $helped_text_1->function_id = 1;
        $helped_text_1->class       = 'user';

        $helped_text_2          = new CAideSaisie();
        $helped_text_2->user_id = 1;

        $helped_text_3           = new CAideSaisie();
        $helped_text_3->group_id = 1;

        $collection = new CModelObjectCollection([$helped_text_1, $helped_text_2, $helped_text_3]);
        $manager->correctHelpedTexts($collection);

        $current_user = CMediusers::get();

        $helped_text_1 = $collection[0];
        $this->assertNull($helped_text_1->group_id);
        $this->assertNull($helped_text_1->function_id);
        $this->assertEquals($current_user->_id, $helped_text_1->user_id);
        $this->assertEquals('CUser', $helped_text_1->class);

        $helped_text_2 = $collection[1];
        $this->assertNull($helped_text_2->group_id);
        $this->assertNull($helped_text_2->function_id);
        $this->assertEquals($current_user->_id, $helped_text_2->user_id);

        $helped_text_3 = $collection[2];
        $this->assertNull($helped_text_3->group_id);
        $this->assertNull($helped_text_3->function_id);
        $this->assertEquals($current_user->_id, $helped_text_3->user_id);
    }

    /**
     * @dataProvider importHelpedTextsProvider
     */
    public function testImportHelpedTexts(
        string $owner_guid,
        ?int   $user_id,
        ?int   $function_id,
        ?int   $group_id,
        ?int   $cat_group
    ): void {
        $manager = $this->getMockBuilder(HelpedTextManager::class)
                        ->onlyMethods(['openFile', 'importLine', 'readLine'])
                        ->getMock();
        $manager->method('readLine')->willReturnOnConsecutiveCalls(
            ['foo', 'bar'],
            ['foo' => 1, 'bar' => 2],
            ['foo' => 3, 'bar' => 1],
            []
        );
        $manager->method('openFile')->willReturn(new CCSVFile());
        $manager->expects($this->exactly(2))->method('importLine')->withConsecutive(
            [['foo' => 1, 'bar' => 2], $user_id, $function_id, $group_id, $cat_group],
            [['foo' => 3, 'bar' => 1], $user_id, $function_id, $group_id, $cat_group]
        );

        $manager->importHelpedTexts(
            new UploadedFile('/foo', 'foo', null, UPLOAD_ERR_NO_FILE, true),
            $owner_guid
        );
    }

    public function testHelpedTextsToArray(): void
    {
        $hp_group           = new CAideSaisie();
        $hp_group->_id      = 1;
        $hp_group->name     = 'hp_group';
        $hp_group->text     = 'hp_group';
        $hp_group->class    = 'CCompteRendu';
        $hp_group->field    = '_source';
        $hp_group->group_id = 1;

        $hp_function                     = new CAideSaisie();
        $hp_function->_id                = 2;
        $hp_function->name               = 'hp_function';
        $hp_function->text               = 't�st�';
        $hp_function->class              = 'CConsultation';
        $hp_function->field              = 'rques';
        $hp_function->function_id        = 2;
        $hp_function->_vw_depend_field_1 = 'vw_depend_field_1';
        $hp_function->_vw_depend_field_2 = 'vw_depend_field_2';
        $hp_function->depend_value_1     = 'depend_value_1';
        $hp_function->depend_value_2     = 'depend_value_2';

        $link1       = new CHyperTextLink();
        $link1->_id  = 1;
        $link1->name = 'link1';
        $link1->link = 'http://link�.com';

        $link2       = new CHyperTextLink();
        $link2->_id  = 2;
        $link2->name = 'link�2';
        $link2->link = 'http://link�2.com';

        $hp_user                            = new CAideSaisie();
        $hp_user->_id                       = 3;
        $hp_user->name                      = 'hp_us�r';
        $hp_user->text                      = 'text foo bar';
        $hp_user->field                     = 'field_name';
        $hp_user->user_id                   = 3;
        $hp_user->_count['hypertext_links'] = 2;
        $hp_user->_back['hypertext_links']  = [1 => $link1, 2 => $link2];

        $hp_instance        = new CAideSaisie();
        $hp_instance->_id   = 4;
        $hp_instance->name  = 'hp_instance';
        $hp_instance->text  = 'hp_instance';
        $hp_instance->field = 'field hp_instance';

        $this->assertEquals(
            [
                1 => [
                    'n'   => "hp_group\n",
                    'f'   => '_source',
                    'gid' => 1,
                ],
                2 => [
                    'n'    => "hp_function\n",
                    'f'    => 'rques',
                    't'    => mb_convert_encoding("t�st�\n", 'UTF-8', 'ISO-8859-1'),
                    'fid'  => 2,
                    '_vd1' => 'vw_depend_field_1',
                    '_vd2' => 'vw_depend_field_2',
                    'd1'   => 'depend_value_1',
                    'd2'   => 'depend_value_2',
                ],
                3 => [
                    'n'   => mb_convert_encoding("hp_us�r\n", 'UTF-8', 'ISO-8859-1'),
                    'f'   => 'field_name',
                    't'   => "text foo bar\n",
                    'uid' => 3,
                    'links' => [
                        [
                            'id' => 1,
                            'name' => 'link1',
                            'link' => mb_convert_encoding('http://link�.com', 'UTF-8', 'ISO-8859-1')
                        ],
                        [
                            'id' => 2,
                            'name' => mb_convert_encoding('link�2', 'UTF-8', 'ISO-8859-1'),
                            'link' => mb_convert_encoding('http://link�2.com', 'UTF-8', 'ISO-8859-1')
                        ],
                    ]
                ],
                4 => [
                    'n' => "hp_instance\n",
                    'f' => 'field hp_instance',
                ],
            ],
            (new HelpedTextManager())->helpedTextsToArray([$hp_group, $hp_function, $hp_user, $hp_instance])
        );
    }

    public function importHelpedTextsProvider(): array
    {
        $user = CMediusers::get();

        $function = $user->loadRefFunction();

        $group = $function->loadRefGroup();

        return [
            'owner_user'     => [$user->_guid, (int)$user->_id, null, null, (int)$function->group_id],
            'owner_function' => [$function->_guid, null, (int)$function->_id, null, (int)$function->group_id],
            'owner_group'    => [$group->_guid, null, null, (int)$group->_id, (int)$group->_id],
            'owner_instance' => ['Instance', null, null, null, null],
            'owner_other'    => ['Other', null, null, null, null],
        ];
    }
}
