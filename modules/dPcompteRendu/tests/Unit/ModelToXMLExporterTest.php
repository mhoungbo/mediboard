<?php

/**
 * @package Mediboard\CompteRendu\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Unit;

use Ox\Mediboard\CompteRendu\ModelToXMLExporter;
use Ox\Tests\OxUnitTestCase;

class ModelToXMLExporterTest extends OxUnitTestCase
{
    public function testConstructThrowException(): void
    {
        $this->expectExceptionMessage("ModelToXMLExporter-error-nothing to export");
        new ModelToXMLExporter("", null);
    }
}
