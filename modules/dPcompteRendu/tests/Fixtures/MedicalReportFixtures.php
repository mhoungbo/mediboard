<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Fixtures;

use DateTimeImmutable;
use Exception;
use Ox\Core\CModelObjectException;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\System\CContentHTML;
use Ox\Tests\Fixtures\Fixtures;
use Ox\Tests\Fixtures\FixturesException;

/**
 * @description Fixture MedicalReport with random data.
 */
class MedicalReportFixtures extends Fixtures
{
    public const MEDICAL_REPORT_USER_TAG    = 'medical_report_user';
    public const MEDICAL_REPORT_PATIENT_TAG = 'medical_report_patient';
    public const MEDICAL_REPORT_TAG         = 'medical_report_';

    /** @var int Number of medical reports to be created. */
    private const COUNT_MEDICAL_REPORTS = 3;

    private CMediusers $user;
    private CPatient   $patient;

    /**
     * @inheritDoc
     * @throws FixturesException
     * @throws CModelObjectException
     */
    public function load(): void
    {
        $this->user    = $this->createUser();
        $this->patient = $this->createPatient();

        $this->createMedicalReports();
    }

    /**
     * Create a medic user.
     *
     * @throws FixturesException
     */
    private function createUser(): CMediusers
    {
        $user             = $this->getUser(false);
        $user->_user_type = 13;

        $this->store($user, self::MEDICAL_REPORT_USER_TAG);

        return $user;
    }

    /**
     * Create a patient.
     *
     * @throws FixturesException
     * @throws CModelObjectException
     */
    private function createPatient(): CPatient
    {
        $patient        = CPatient::getSampleObject();
        $patient->deces = null;

        $this->store($patient, self::MEDICAL_REPORT_PATIENT_TAG);

        return $patient;
    }

    /**
     * Create medical reports.
     *
     * @throws FixturesException
     * @throws Exception
     */
    private function createMedicalReports(): void
    {
        for ($index = 0; $index < self::COUNT_MEDICAL_REPORTS; $index++) {
            // Create content.
            $content_html                = new CContentHTML();
            $content_html->last_modified = (new DateTimeImmutable())->format('Y-m-d H:i:s');
            $content_html->content       = bin2hex(random_bytes(10));

            $this->store($content_html);

            // Create medical report.
            $medical_report = new CCompteRendu();

            $medical_report->object_class = $this->patient->_class;
            $medical_report->object_id    = $this->patient->_id;
            $medical_report->content_id   = $content_html->_id;
            $medical_report->nom          = bin2hex(random_bytes(10));

            // Store.
            $this->store($medical_report);

            $medical_report->author_id = $this->user->_id;

            // Store (Due to author_id).
            $this->store($medical_report, self::MEDICAL_REPORT_TAG . $index);
        }
    }
}
