<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Tests\Fixtures;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Mediboard\CompteRendu\CAideSaisie;
use Ox\Tests\Fixtures\Fixtures;

class HelpedTextFixtures extends Fixtures
{
    public const HELPED_TEXT_USER = 'helped_text_user';

    public function load()
    {
        $user = $this->getUser(false);
        $this->store($user, self::HELPED_TEXT_USER);

        $helped_text_instance        = new CAideSaisie();
        $helped_text_instance->name  = 'testinstance';
        $helped_text_instance->text  = 'testfoo testinstance testbar';
        $helped_text_instance->class = 'CConsultation';
        $helped_text_instance->field = 'rques';
        $this->store($helped_text_instance);

        $helped_text_group           = new CAideSaisie();
        $helped_text_group->group_id = $user->loadRefFunction()->group_id;
        $helped_text_group->name     = 'testgroup';
        $helped_text_group->text     = 'testfoo testgroup testbar';
        $helped_text_group->class    = 'CConsultation';
        $helped_text_group->field    = 'rques';
        $this->store($helped_text_group);

        $helped_text_function              = new CAideSaisie();
        $helped_text_function->function_id = $user->loadRefFunction()->_id;
        $helped_text_function->name        = 'testfunction';
        $helped_text_function->text        = 'testfoo testfunction testbar';
        $helped_text_function->class       = 'CConsultation';
        $helped_text_function->field       = 'rques';
        $this->store($helped_text_function);

        $helped_text_user          = new CAideSaisie();
        $helped_text_user->user_id = $user->_id;
        $helped_text_user->name    = 'testuser';
        $helped_text_user->text    = 'testfoo testuser testbar';
        $helped_text_user->class   = 'CConsultation';
        $helped_text_user->field   = 'rques';
        $this->store($helped_text_user);
    }
}
