{{*
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(Control.Tabs.create.curry('tabs-owner', true));
</script>

<ul id="tabs-owner" class="control_tabs">
    {{foreach from=$packs key=owner item=_packs}}
      <li>
        <a href="#owner-{{$owner}}" {{if !$_packs|@count}}class="empty"{{/if}}>
            {{$owners.$owner}} <small>({{$_packs|@count}})</small>
        </a>
      </li>
    {{/foreach}}
</ul>
{{if $can->admin}}
    {{csrf_token id="models_export_packs" var=export_token}}
    {{foreach from=$packs key=owner item=_pack}}
      <div id="owner-{{$owner}}" style="display: none;" class="me-margin-0 me-padding-0">
        <button class="download me-secondary me-margin-2" onclick="Pack.exportPack('{{$owners.$owner->_guid}}', '{{$export_token}}')">
            {{tr}}Export-XML{{/tr}}
        </button>
        <button class="upload me-secondary me-margin-2" onclick="Pack.importPack('{{$owners.$owner->_guid}}')">
            {{tr}}Import-XML{{/tr}}
        </button>
          {{mb_include template=inc_packs packs=$packs.$owner}}
      </div>
    {{/foreach}}
{{/if}}
