{{*
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<h2>{{tr var1=$owner}}CPack-import_for{{/tr}}</h2>

<form name="editImport" method="post" enctype="multipart/form-data"
      onsubmit="return onSubmitFormAjax(this,Control.Modal.close)">
    {{mb_route name=models_gui_import_pack}}
    {{csrf_token id=models_import_pack}}
  <input type="hidden" name="owner_guid" value="{{$owner->_guid}}"/>
  <div>
    <select name="object_class">
      <option value="">{{tr}}CCompteRendu-import-Default context{{/tr}}</option>
        {{foreach from=$classes key=_class item=_class_view}}
          <option value="{{$_class}}">{{tr}}{{$_class_view}}{{/tr}}</option>
        {{/foreach}}
    </select>
  </div>

  <div class="me-margin-top-10">
      {{mb_include module=system template=inc_inline_upload lite=true paste=false extensions='xml'}}
  </div>

  <div class="me-text-align-center">
    <button class="tick">{{tr}}Import{{/tr}}</button>
  </div>
</form>
