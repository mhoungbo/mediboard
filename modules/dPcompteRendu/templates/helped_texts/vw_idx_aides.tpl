{{*
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=sante400 script=hyperTextLink}}
{{mb_script module=compteRendu script=aide}}

<script>
  Main.add(function () {
    Aide.edit_url = '{{$url_edit_helped_text}}';
    Aide.search_url = '{{$url_vw_list}}';
    Aide.import_url = '{{$url_import_helped_text}}';
    Aide.depends_url = '{{$url_list_depends}}';

    const form = getForm("filterFrm");
    form.onsubmit();

    Aide.makeUserAutocomplete(form);

      {{if $access_function}}
        Aide.makeFunctionAutocomplete(form);
      {{/if}}
  });
</script>

<div class="me-margin-bottom-8 me-margin-top-4">
  <button class="new" onclick="Aide.edit()">{{tr}}CAideSaisie-title-create{{/tr}}</button>
</div>

<div>
  <form name="filterFrm" method="get" onsubmit="return Aide.loadTabsAides()">
    <input type="hidden" name="start_user" value="0" onchange="this.form.onsubmit()"/>
    <input type="hidden" name="start_function" value="0" onchange="this.form.onsubmit()"/>
    <input type="hidden" name="start_group" value="0" onchange="this.form.onsubmit()"/>
    <input type="hidden" name="start_instance" value="0" onchange="this.form.onsubmit()"/>
    <input type="hidden" name="order_col_aide" value=""/>
    <input type="hidden" name="order_way" value=""/>

    <table class="form me-no-align">
      <tr>
        <th class="category" colspan="10">{{tr}}CAideSaisie.filter{{/tr}}</th>
      </tr>

      <tr>
          {{me_form_field nb_cells=2 class=CAideSaisie mb_field=user_id}}
          {{mb_field class=CAideSaisie field=user_id hidden=1 onchange="Aide.emptyFunction(this.form)"}}
            <input type="text" name="user_id_view" value=""/>
          {{/me_form_field}}

          {{if $access_function}}
              {{me_form_field nb_cells=2 class=CAideSaisie mb_field=function_id}}
              {{mb_field class=CAideSaisie field=function_id hidden=1 onchange="Aide.emptyUser(this.form)"}}
                <input type="text" name="function_id_view" value=""/>
              {{/me_form_field}}
          {{/if}}

          {{me_form_field nb_cells=2 title_label="CAideSaisie-object_type-filter" label="common-Object type"}}
            <select name="class" onchange="this.form.onsubmit()" style="width: 12em;">
              <option value="">&mdash; {{tr}}CAideSaisie-object_type-all{{/tr}}</option>
                {{foreach from=$classes key=class item=fields}}
                  <option value="{{$class}}">
                      {{tr}}{{$class}}{{/tr}}
                  </option>
                {{/foreach}}
            </select>
          {{/me_form_field}}

          {{me_form_field nb_cells=2 label="Keywords"}}
            <input type="text" name="keywords" value=""/>
          {{/me_form_field}}

        <td>
          <button type="submit" class="search notext">{{tr}}Filter{{/tr}}</button>
        </td>
      </tr>
    </table>
  </form>
</div>

<div id="tabs_aides"></div>
