{{*
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_script module=dPcompteRendu script=aide ajax=true}}

<script>
  Aide.classes = {{$classes|@json}};

  if (!Aide.search_url)  {
    Aide.search_url = '{{$search_url}}'
  }

  if (!Aide.groups_autocomplete_url)  {
      Aide.groups_autocomplete_url = '{{$url_groups_autocomplete}}'
  }

  Main.add(function () {
    {{if !$restricted}}
      Aide.loadClasses('{{$aide->class}}');
      Aide.loadFields('{{$aide->field}}');
    {{/if}}
    Aide.loadDependances('{{$aide->depend_value_1}}', '{{$aide->depend_value_2}}');

    {{if $restricted}}
      Aide.loadTabsAides('{{$aide->class}}', '{{$aide->field}}');
      Control.Tabs.create('aide-info');
    {{/if}}

    {{if $aide->_id}}
      HyperTextLink.getListFor('{{$aide->_id}}', '{{$aide->_class}}');
    {{/if}}

    const form = getForm("editFrm");

    Aide.makeUserAutocomplete(form);

      {{if $access_function}}
    Aide.makeFunctionAutocomplete(form);
      {{/if}}

      {{if $access_group}}
    Aide.makeGroupAutocomplete(form);
      {{/if}}
  });
</script>

{{if $restricted}}
  <div>
    <ul id="aide-info" class="control_tabs">
      <li><a href="#edit-aide">{{tr}}CAideSaisie{{/tr}}</a></li>
      <li><a href="#tabs_aides">{{tr}}CAideSaisie|pl{{/tr}}</a></li>
    </ul>
  </div>
{{/if}}

<div id="edit-aide" {{if $restricted}}style="display: none;"{{/if}}>
  <form name="editFrm" method="post" onsubmit="return onSubmitFormAjax(this, (function() {
    AideSaisie.removeLocalStorage();
    Control.Modal.close();
  {{if !$restricted}}
    Aide.loadTabsAides();
  {{/if}}

    }).bind(this));">
    <input type="hidden" name="@route" value="{{$url_edit_aide}}"/>
    {{csrf_token id=models_gui_helped_text_edit}}
    <input type="hidden" name="aide_id" value="{{$aide->_id}}"/>
    <input type="hidden" name="del" value="0"/>
    <input type="hidden" name="restricted" value="{{$restricted}}"/>

    <table class="form">
        {{mb_include module=system template=inc_form_table_header object=$aide}}

      <tr>
          {{me_form_field nb_cells=2 mb_object=$aide mb_field=user_id}}
          {{mb_field object=$aide field=user_id hidden=1
          onchange="Aide.emptyFunction(this.form, false);Aide.emptyGroup(this.form, false);"}}
            <input type="text" name="user_id_view" value="{{$aide->_ref_user}}"/>
            <button type="button" class="user notext compact me-tertiary " title="{{tr}}CMediusers.current{{/tr}}" onclick="Aide.setCurrentUser('{{$current_user->_id}}', '{{$current_user->_view}}')"></button>
          {{/me_form_field}}
      </tr>

        {{if $access_function}}
          <tr>
              {{me_form_field nb_cells=2 mb_object=$aide mb_field=function_id}}
              {{mb_field object=$aide field=function_id hidden=1
              onchange="Aide.emptyUser(this.form, false);Aide.emptyGroup(this.form, false);"}}
                <input type="text" name="function_id_view" value="{{$aide->_ref_function}}"/>
                <button type="button" class="function notext compact me-tertiary" title="{{tr}}CFunctions.current{{/tr}}" onclick="Aide.setCurrentFunction('{{$current_function->_id}}', '{{$current_function->_view}}');"></button>
              {{/me_form_field}}
          </tr>
        {{/if}}

        {{if $access_group}}
          <tr>
              {{me_form_field nb_cells=2 mb_object=$aide mb_field=group_id}}
              {{mb_field object=$aide field=group_id hidden=1
              onchange="Aide.emptyUser(this.form, false);Aide.emptyFunction(this.form, false);"}}
                <input type="text" name="group_id_view" value="{{$aide->_ref_group}}"/>
                <button type="button" class="group notext compact me-tertiary" title="{{tr}}CGroups.current{{/tr}}" onclick="Aide.setCurrentGroup('{{$current_group->_id}}', '{{$current_group->_view}}');"></button>
              {{/me_form_field}}
          </tr>
        {{/if}}

      <tr>
          {{me_form_field nb_cells=2 mb_object=$aide mb_field="class"}}
          {{if $restricted}}
              {{mb_field object=$aide field=class readonly=true}}
          {{else}}
            <select name="class" class="{{$aide->_props.class}}" onchange="Aide.loadFields()" style="width: 12em;">
              <option value="">&mdash; {{tr}}CAideSaisie-Choose an object type{{/tr}}</option>
            </select>
          {{/if}}

          {{/me_form_field}}
      </tr>

      <tr>
          {{me_form_field nb_cells=2 mb_object=$aide mb_field="field"}}
          {{if $restricted}}
              {{mb_field object=$aide field=field readonly=true}}
          {{else}}
            <select name="field" class="{{$aide->_props.field}}" onchange="Aide.loadDependances()" style="width: 12em;">
              <option value="">&mdash; {{tr}}CAideSaisie-Choose a field{{/tr}}</option>
            </select>
          {{/if}}

          {{/me_form_field}}
      </tr>

      <tr>
          {{me_form_field nb_cells=2 label="CAideSaisie-depend_value_1"}}
          {{if $aide->_is_ref_dp_1}}
              {{mb_field object=$aide field="depend_value_1" hidden=true}}
            <input type="hidden" name="_ref_class_depend_value_1" value="{{$aide->_class_dp_1}}"/>
            <input type="text" name="_depend_value_2_view" value="{{$aide->_vw_depend_field_1}}"/>
            <script>
              Main.add(function () {
                var form = getForm("editFrm");

                var url = new Url("system", "ajax_seek_autocomplete");
                url.addParam("object_class", $V(form._ref_class_depend_value_1));
                url.addParam("field", "depend_value_1");
                url.addParam("input_field", "_depend_value_1_view");
                url.addParam("show_view", "true");
                url.autoComplete(form.elements._depend_value_1_view, null, {
                  minChars:           3,
                  method:             "get",
                  select:             "view",
                  dropdown:           true,
                  afterUpdateElement: function (field, selected) {
                    $V(field.form.elements.depend_value_1, selected.get("id"));
                  }
                });
              });
            </script>
          {{else}}
            <select name="depend_value_1" class="{{$aide->_props.depend_value_1}}">
              <option value="">&mdash; {{tr}}common-all|pl{{/tr}}</option>
            </select>
          {{/if}}
          {{/me_form_field}}
      </tr>

      <tr>
          {{me_form_field nb_cells=2 label="CAideSaisie-depend_value_2"}}
          {{if $aide->_is_ref_dp_2}}
              {{mb_field object=$aide field="depend_value_2" hidden=true}}
            <input type="hidden" name="_ref_class_depend_value_2" value="{{$aide->_class_dp_2}}"/>
            <input type="text" name="_depend_value_2_view" value="{{$aide->_vw_depend_field_2}}"/>
            <script>
              Main.add(function () {
                var form = getForm("editFrm");

                var url = new Url("system", "ajax_seek_autocomplete");
                url.addParam("object_class", $V(form._ref_class_depend_value_2));
                url.addParam("field", "depend_value_2");
                url.addParam("input_field", "_depend_value_2_view");
                url.addParam("show_view", "true");
                url.autoComplete(form.elements._depend_value_2_view, null, {
                  minChars:           3,
                  method:             "get",
                  select:             "view",
                  dropdown:           true,
                  afterUpdateElement: function (field, selected) {
                    $V(field.form.elements.depend_value_2, selected.get("id"));
                  }
                });
              });
            </script>
          {{else}}
            <select name="depend_value_2" class="{{$aide->_props.depend_value_2}}">
              <option value="">&mdash; {{tr}}common-all|pl{{/tr}}</option>
            </select>
          {{/if}}
          {{/me_form_field}}
      </tr>

      <tr>
          {{me_form_field nb_cells=2 label="CAideSaisie-name"}}
          {{mb_field object=$aide field="name"}}
          {{/me_form_field}}
      </tr>

      <tr>
          {{me_form_field nb_cells=2 label="CAideSaisie-text"}}
          {{mb_field object=$aide field="text"}}
          {{/me_form_field}}
      </tr>

      <tr>
          {{if $aide->_id}}
              {{me_form_field nb_cells=2 label="CHyperTextLink"}}
                <div id="list-hypertext_links" class="me-align-auto"></div>
              {{/me_form_field}}
          {{/if}}
      </tr>

        {{if "loinc"|module_active && $aide->_id}}
          <tr>
            <th>{{tr}}CLoinc-Loinc Codes{{/tr}}</th>
            <td>
                {{foreach from=$aide->_ref_codes_loinc item=_code name=count_code}}
                  <span onmouseover="ObjectTooltip.createEx(this, '{{$_code->_guid}}');">{{$_code->code}}</span>
                    {{if !$smarty.foreach.count_code.last}},{{/if}}
                {{/foreach}}
            </td>
          </tr>
        {{/if}}

        {{if "snomed"|module_active && $aide->_id}}
          <tr>
            <th>{{tr}}CSnomed-Snomed Codes{{/tr}}</th>
            <td>
                {{foreach from=$aide->_ref_codes_snomed item=_code name=count_code}}
                  <span onmouseover="ObjectTooltip.createEx(this, '{{$_code->_guid}}');">{{$_code->code}}</span>
                    {{if !$smarty.foreach.count_code.last}},{{/if}}
                {{/foreach}}
            </td>
          </tr>
        {{/if}}

      <tr>
        <td class="button" colspan="2">
            {{if $aide->aide_id}}
              <button class="modify me-primary" type="button" onclick="this.form.onsubmit()">{{tr}}Save{{/tr}}</button>
              <button class="trash" type="button"
                      onclick="Aide.remove(this.form, '{{$aide->name|smarty:nodefaults|escape:'javascript'}}');Control.Modal.close();">
                  {{tr}}Delete{{/tr}}
              </button>
                {{if "loinc"|module_active || "snomed"|module_active}}
                  <button type="button" title="{{tr}}CAideSaisie-Nomenclature|pl-desc{{/tr}}"
                          onclick="Aide.showNomenclatures('{{$aide->_guid}}');">
                    <i class="far fa-eye"></i> {{tr}}CAideSaisie-Nomenclature|pl{{/tr}}
                  </button>
                {{/if}}

            {{else}}
              <button class="submit me-primary" type="button" onclick="this.form.onsubmit()">{{tr}}Create{{/tr}}</button>
            {{/if}}

            {{if $restricted}}
              <button class="new me-secondary" type="button"
                      onclick="if (confirm('{{tr}}CAideSaise-action-newConfirm{{/tr}}')) {
                        Control.Modal.close();
                        AideSaisie.create('{{$aide->class}}', {}, '{{$aide->field}}', '{{$aide->depend_value_1}}', '{{$aide->depend_value_2}}', '', null, '{{$aide->_class_dp_1}}', '{{$aide->_class_dp_2}}');
                        }">
                  {{tr}}CAideSaise-action-Create new{{/tr}}
              </button>
            {{/if}}
        </td>
      </tr>
    </table>
  </form>
</div>

{{if $restricted}}
    <div id="tabs_aides" style="display: none;"></div>
{{/if}}
