{{*
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=filter_class value=""}}

{{csrf_token id=models_helped_text_import var=csrf_token}}
{{csrf_token id=models_gui_helped_text_edit var=csrf_token_do_edit}}

{{if !$restricted && $can->admin}}
  {{me_button label="Export-CSV" icon=hslip onclick="Aide.exportAidesCSV('$type', '$class')"}}
  {{me_button label="Import-CSV" icon=hslip onclick="Aide.popupImport('`$owner->_guid`', '$class');"}}

  {{me_dropdown_button button_label=Actions button_icon="opt"
       button_class="notext me-tertiary" container_class="me-dropdown-button-right me-float-right"}}
{{/if}}

{{if !$restricted}}
  {{mb_include module=system template=inc_pagination change_page="Aide.changePage$type" total=$count current=$start step=30}}
{{/if}}

{{url name=models_gui_helped_text_edit var=edit_route_name}}

<table class="tbl me-no-align me-no-box-shadow">
  <tr>
    <th>{{mb_colonne class=CAideSaisie field=class order_col=$order_col_aide order_way=$order_way function=Aide.sortList}}</th>
    <th>{{mb_colonne class=CAideSaisie field=field order_col=$order_col_aide order_way=$order_way function=Aide.sortList}}</th>
    <th class="narrow">{{mb_colonne class=CAideSaisie field=depend_value_1 order_col=$order_col_aide order_way=$order_way function=Aide.sortList}}</th>
    <th class="narrow">{{mb_colonne class=CAideSaisie field=depend_value_2 order_col=$order_col_aide order_way=$order_way function=Aide.sortList}}</th>
    <th>{{mb_colonne class=CAideSaisie field=name order_col=$order_col_aide order_way=$order_way function=Aide.sortList}}</th>
    <th>{{mb_title class=CAideSaisie field=text}}</th>
      {{if "loinc"|module_active || "snomed"|module_active}}
        {{if in_array($class, 'Ox\Mediboard\Loinc\CLoinc'|static:binding_classes) || !$filter_class}}
          <th class="narrow" title="{{tr}}CAntecedent-Nomenclature-desc{{/tr}}">{{tr}}CAntecedent-Nomenclature{{/tr}}</th>
        {{/if}}
      {{/if}}
    <th class="narrow"></th>
  </tr>

  {{foreach from=$aides item=_aide}}

  {{assign var=readonly value=0}}

  {{if $_aide->_is_for_instance && !$can->admin()}}
    {{assign var=readonly value=1}}
  {{/if}}

  <tr {{if $_aide->_id == $aide_id}}class="selected"{{/if}}>
    {{assign var="class" value=$_aide->class}}
    {{assign var="field" value=$_aide->field}}

    <td class="text">
      {{if !$readonly}}
        <a style="color: #283593; cursor: pointer"
           onclick="{{if $restricted}}Control.Modal.close();AideSaisie.create('', '', '', '', '', '', '', '', '', '{{$_aide->_id}}');
           {{else}}Aide.edit('{{$_aide->_id}}'){{/if}}">
            {{tr}}{{$class}}{{/tr}}
        </a>
      {{else}}
          {{tr}}{{$class}}{{/tr}}
      {{/if}}
    </td>
    <td class="text">{{tr}}{{$class}}-{{$field}}{{/tr}}</td>
    <td>
      {{if $_aide->_depend_field_1 && !$_aide->_is_ref_dp_1}}
        <form name="edit-CAidesSaisie-depend1-{{$_aide->_id}}" method="post" onsubmit="return onSubmitFormAjax(this)">
          <input type="hidden" name="@route" value="{{$url_edit_helped_text}}"/>
          <input type="hidden" name="token" value="{{$csrf_token_do_edit}}"/>
          {{mb_key object=$_aide}}

          {{me_form_field animated=false}}
            <select
              style="width: 10em;"
              onchange="this.form.onsubmit()"
              name="depend_value_1"
              onmouseover="Aide.getListDependValues(this, '{{$class}}', '{{$_aide->_depend_field_1}}')"
              {{if $readonly || $restricted}}
                disabled
              {{/if}}>
              <option value="{{$_aide->depend_value_1}}">
                {{if $_aide->depend_value_1}}
                  {{tr}}{{$class}}.{{$_aide->_depend_field_1}}.{{$_aide->depend_value_1}}{{/tr}}
                {{else}}
                  &mdash; {{tr}}None{{/tr}}
                {{/if}}
              </option>
            </select>
          {{/me_form_field}}
        </form>
      {{elseif $_aide->_is_ref_dp_1}}
        {{$_aide->_vw_depend_field_1}}
      {{else}}
        &mdash;
      {{/if}}
    </td>
    <td>
      {{if $_aide->_depend_field_2 && !$_aide->_is_ref_dp_2}}
        <form name="edit-CAidesSaisie-depend2-{{$_aide->_id}}" method="post" onsubmit="return onSubmitFormAjax(this)">
          <input type="hidden" name="@route" value="{{$url_edit_helped_text}}"/>
          <input type="hidden" name="token" value="{{$csrf_token_do_edit}}"/>
          {{mb_key   object=$_aide}}

          <select
            style="width: 10em;"
            onchange="this.form.onsubmit()"
            name="depend_value_2"
            onmouseover="Aide.getListDependValues(this, '{{$class}}', '{{$_aide->_depend_field_2}}')"
            {{if $readonly || $restricted}}
              disabled
            {{/if}}>
            <option value="{{$_aide->depend_value_2}}">
              {{if $_aide->depend_value_2}}
                {{tr}}{{$class}}.{{$_aide->_depend_field_2}}.{{$_aide->depend_value_2}}{{/tr}}
              {{else}}
                &mdash; {{tr}}None{{/tr}}
              {{/if}}
            </option>
          </select>
        </form>
      {{elseif $_aide->_is_ref_dp_2}}
        {{$_aide->_vw_depend_field_2}}
      {{else}}
        &mdash;
      {{/if}}
    </td>

    <td class="text">{{mb_value object=$_aide field=name}}</td>
    <td class="text compact" title="{{$_aide->text}}">
      <div style="float: right;">
        {{mb_include module=sante400 template=inc_hypertext_links object=$_aide}}
      </div>
      {{mb_value object=$_aide field=text truncate=60}}
    </td>
     {{if "loinc"|module_active || "snomed"|module_active}}
      {{if in_array($_aide->class, 'Ox\Mediboard\Loinc\CLoinc'|static:binding_classes)}}
          <td>
            {{if "loinc"|module_active}}
              {{mb_include module=loinc  template=inc_vw_tag_loinc  object=$_aide}}
            {{/if}}
            {{if "snomed"|module_active}}
              {{mb_include module=snomed template=inc_vw_tag_snomed object=$_aide}}
            {{/if}}
          </td>
      {{elseif !$filter_class}}
        <td></td>
      {{/if}}
     {{/if}}

    <td>
      {{if !$readonly && !$restricted}}
        <form name="remove-aide-{{$_aide->_id}}" method="post" onsubmit="return onSubmitFormAjax(this, function () {
          AideSaisie.removeLocalStorage();
          Aide.loadTabsAides();
        })">
          <input type="hidden" name="@route" value="{{$edit_route_name}}"/>
          <input type="hidden" name="token" value="{{$csrf_token_do_edit}}"/>
          <input type="hidden" name="aide_id" value="{{$_aide->_id}}"/>
          <input type="hidden" name="del" value="1"/>

          <button class="trash notext" type="button" onclick="Aide.remove(this.form, '{{$_aide->name|smarty:nodefaults|escape:'javascript'}}')">
              {{tr}}Delete{{/tr}}
          </button>
        </form>

      {{/if}}
    </td>
  </tr>
  {{foreachelse}}
  <tr>
    <td colspan="10" class="empty">{{tr}}CAideSaisie.none{{/tr}}</td>
  </tr>
  {{/foreach}}
</table>
