{{*
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  Main.add(function() {
    Control.Tabs.create('tabs-owner', true);

    if (!Aide.edit_url) {
      Aide.edit_url = '{{$url_vw_edit_helped_text}}';
    }
  });
</script>

<ul id="tabs-owner" class="control_tabs">
  {{if 'user'|array_key_exists:$aides}}
  <li>
    <a href="#owner-user" {{if $aides.user.count === 0}}class="empty"{{/if}}>
      {{$user}} <small>({{$aides.user.count}})</small>
    </a>
  </li>
  {{/if}}

  {{if 'func'|array_key_exists:$aides}}
  <li>
    <a href="#owner-func" {{if $aides.func.count == 0}}class="empty"{{/if}}>
      {{if $function->_id}}{{$function}}{{else}}{{$user->_ref_function}}{{/if}} <small>({{$aides.func.count}})</small>
    </a>
  </li>
  {{/if}}

  {{if 'etab'|array_key_exists:$aides}}
  <li>
    <a href="#owner-etab" {{if $aides.etab.count == 0}}class="empty"{{/if}}>
      {{if $function->_id}}{{$function->_ref_group}}{{else}}{{$user->_ref_function->_ref_group}}{{/if}}
      <small>({{$aides.etab.count}})</small>
    </a>
  </li>
  {{/if}}

  {{if 'instance'|array_key_exists:$aides}}
    <li>
      <a href="#owner-instance" {{if $aides.instance.count == 0}}class="empty"{{/if}}>
        {{tr}}Instance{{/tr}} <small>({{$aides.instance.count}})</small>
      </a>
    </li>
  {{/if}}
</ul>

{{if 'user'|array_key_exists:$aides}}
  <div id="owner-user" style="display: none;" class="me-no-align">
    {{mb_include template="helped_texts/inc_list_aides" owner=$user aides=$aides.user.aides count=$aides.user.count type=User filter_class=$class start=$start_user}}
  </div>
{{/if}}

{{if 'func'|array_key_exists:$aides}}
  <div id="owner-func" style="display: none;" class="me-no-align">
    {{assign var=owner value=$user->_ref_function}}
    {{if $function->_id}}
      {{assign var=owner value=$function}}
    {{/if}}

    {{mb_include template="helped_texts/inc_list_aides" owner=$owner aides=$aides.func.aides count=$aides.func.count type=Func filter_class=$class start=$start_function}}
  </div>
{{/if}}

{{if 'etab'|array_key_exists:$aides}}
  <div id="owner-etab" style="display: none;" class="me-no-align">
    {{assign var=owner value=$user->_ref_function->_ref_group}}
    {{if $function->_id}}
      {{assign var=owner value=$function->_ref_group}}
    {{/if}}

    {{mb_include template="helped_texts/inc_list_aides" owner=$owner aides=$aides.etab.aides count=$aides.etab.count type=Etab filter_class=$class start=$start_group}}
  </div>
{{/if}}

{{if 'instance'|array_key_exists:$aides}}
  <div id="owner-instance" style="display: none;" class="me-no-align">
    {{assign var=owner value=$owners.instance}}
    {{mb_include template="helped_texts/inc_list_aides" owner=$owner aides=$aides.instance.aides count=$aides.instance.count type=Instance filter_class=$class start=$start_instance}}
  </div>
{{/if}}
