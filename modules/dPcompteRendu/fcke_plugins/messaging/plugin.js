/**
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

CKEDITOR.plugins.add('messaging', {
  requires: ['dialog'],
  init:     function (editor) {
    editor.addCommand('openMail', {
      exec: () => {
        openWindowMail();
      }
    });
    editor.addCommand('openApicrypt', {
      exec: () => {
        openWindowApicrypt();
      }
    });
    editor.addCommand('openMailiz', {
      exec: () => {
        openWindowMSSante();
      }
    });
    editor.addCommand('openMedimail', {
      exec: () => {
        openWindowMedimail();
      }
    });
    editor.addCommand('openMedimailIHE', {
      exec: () => {
        openWindowMedimail(1);
      }
    });

    let items = [
      {
        icon:     (editor.send_mailbox && editor.send_mailbox.mail) ? './style/mediboard_ext/images/buttons/mail_send.png' : './style/mediboard_ext/images/buttons/mail.png',
        icon_alt: 'Mail',
        label:    'Mail',
        onClick:  `CKEDITOR.instances.htmlarea.execCommand('openMail')`,
        status:   window.parent.available_mailbox.mail,
        title:    $T('CCompteRendu.send_mail')
      },
      {
        icon:     (editor.send_mailbox && editor.send_mailbox.apicrypt) ? './style/mediboard_ext/images/buttons/mailApicrypt_send.png' : './style/mediboard_ext/images/buttons/mailApicrypt.png',
        icon_alt: $T('module-apicrypt-court'),
        label:    $T('module-apicrypt-court'),
        onClick:  `CKEDITOR.instances.htmlarea.execCommand('openApicrypt')`,
        status:   window.parent.available_mailbox.apicrypt,
        title:    $T('CCompteRendu.send_mail_apicrypt')
      },
      {
        icon:     (editor.send_mailbox && editor.send_mailbox.mailiz) ? './style/mediboard_ext/images/buttons/mailMSSante_send.png' : './style/mediboard_ext/images/buttons/mailMSSante.png',
        icon_alt: $T('module-mssante-court'),
        label:    $T('module-mssante-court'),
        onClick:  `CKEDITOR.instances.htmlarea.execCommand('openMailiz')`,
        status:   window.parent.available_mailbox.mailiz,
        title:    $T('CCompteRendu.send_mail_mssante')
      },
      {
        icon:     './style/mediboard_ext/images/buttons/medimail.png',
        icon_alt: $T('module-medimail-court'),
        label:    $T('module-medimail-court'),
        onClick:  `CKEDITOR.instances.htmlarea.execCommand('openMedimail')`,
        status:   window.parent.available_mailbox.medimail,
        title:    $T('CCompteRendu.send_mail_medimail')
      },
      {
        icon:     './style/mediboard_ext/images/buttons/medimailIHEXDM.png',
        icon_alt: $T('module-medimail-court') + ' IHE-XDM',
        label:    $T('module-medimail-court') + ' IHE-XDM',
        onClick:  `CKEDITOR.instances.htmlarea.execCommand('openMedimailIHE')`,
        status:   window.parent.available_mailbox.medimail,
        title:    $T('CCompteRendu.send_mail_medimail_ihe_xdm')
      }
    ];

    editor.ui.add('messaging', CKEDITOR.UI_PANELBUTTON, {
      label:   'Envoyer par',
      icon:    '../../style/mediboard_ext/images/buttons/mail.png',
      modes:   {
        wysiwyg: 1
      },
      panel:   {
        attributes: {
          role: 'listbox'
        }
      },
      onBlock: function (menu, list) {
        list.element.getDocument().getElementsByTag('head').getItem(0).setHtml(
          DOM.link(
            {
              href: './modules/dPcompteRendu/css/editor.css',
              rel:  'stylesheet',
              type: 'text/css'
            }
          ).outerHTML
        );

        createMenu(items, list.element);
      }
    });
  }
});

/**
 * Create messaging menu
 *
 * @param {{}}     item_list
 * @param {object} target
 */
function createMenu(item_list, target) {
  let dom_list = DOM.div(
    {
      className: 'CkeMessaging-list'
    }
  );

  item_list.forEach(item => {
    if (item.status) {
      dom_list.insert(
        DOM.div(
          {
            className: 'CkeMessaging-item',
            onClick:   item.onClick,
            title:     item.title
          },
          DOM.img(
            {
              alt:       item.icon_alt,
              className: 'CkeMessaging-itemIcon',
              src:       item.icon
            }
          ),
          DOM.div(
            {
              className: 'CkeMessaging-itemContent'
            },
            item.label
          )
        )
      );
    }
  });

  target.addClass('CkeMessaging');
  target.setHtml(dom_list.outerHTML);
}
