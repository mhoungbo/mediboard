import { OxAttr } from "@/core/types/OxObjectTypes"
import Preferences from "@modules/system/vue/models/Preferences"

export default class ModelsPreferences extends Preferences {
    get pdfAndThumbs (): OxAttr<string> {
        return super.get("pdf_and_thumbs")
    }

    get saveOnPrint (): OxAttr<string> {
        return super.get("saveOnPrint")
    }

    get choicePratCab (): OxAttr<string> {
        return super.get("choicepratcab")
    }

    get listDefault (): OxAttr<string> {
        return super.get("listDefault")
    }

    get listBrPrefix (): OxAttr<string> {
        return super.get("listBrPrefix")
    }

    get listInlineSeparator (): OxAttr<string> {
        return super.get("listInlineSeparator")
    }

    get aideTimestamp (): OxAttr<string> {
        return super.get("aideTimestamp")
    }

    get aideOwner (): OxAttr<string> {
        return super.get("aideOwner")
    }

    get aideFastMode (): OxAttr<string> {
        return super.get("aideFastMode")
    }

    get aideAutoComplete (): OxAttr<string> {
        return super.get("aideAutoComplete")
    }

    get aideShowOver (): OxAttr<string> {
        return super.get("aideShowOver")
    }

    get modePlay (): OxAttr<string> {
        return super.get("mode_play")
    }

    get multipleDocs (): OxAttr<string> {
        return super.get("multiple_docs")
    }

    get autoCapitalize (): OxAttr<string> {
        return super.get("auto_capitalize")
    }

    get autoReplacehelper (): OxAttr<string> {
        return super.get("auto_replacehelper")
    }

    get hprimMedHeader (): OxAttr<string> {
        return super.get("hprim_med_header")
    }

    get showOldPrint (): OxAttr<string> {
        return super.get("show_old_print")
    }

    get sendDocumentSubject (): OxAttr<string> {
        return super.get("send_document_subject")
    }

    get sendDocumentBody (): OxAttr<string> {
        return super.get("send_document_body")
    }

    get multipleDocCorrespondants (): OxAttr<string> {
        return super.get("multiple_doc_correspondants")
    }

    get showCreationDate (): OxAttr<string> {
        return super.get("show_creation_date")
    }

    get secureSignature (): OxAttr<string> {
        return super.get("secure_signature")
    }

    get checkToEmptyField (): OxAttr<string> {
        return super.get("check_to_empty_field")
    }

    get timeAutosave (): OxAttr<string> {
        return super.get("time_autosave")
    }

    get showFavorites (): OxAttr<string> {
        return super.get("show_favorites")
    }
}
