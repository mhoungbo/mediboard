/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"

export default class CompteRendu extends OxObject {
    readonly CLASSNAME = "CCompteRendu"
    constructor () {
        super()
        this.type = "medicalReports"
    }

    get name (): OxAttr<string> {
        return super.get("nom")
    }

    set name (name: OxAttr<string>) {
        super.set("nom", name)
    }

    get cancelled (): OxAttr<boolean> {
        return super.get("annule")
    }

    set cancelled (cancelled: OxAttr<boolean>) {
        super.set("annule", cancelled)
    }

    get objectClass (): OxAttr<string> {
        return super.get("object_class")
    }

    get objectId (): OxAttr<number> {
        return super.get("object_id")
    }

    get objectGuid (): string {
        return this.objectClass + "-" + this.objectId
    }

    get creationDate (): OxAttr<string> {
        return super.get("creation_date")
    }

    get guid (): string {
        return "CCompteRendu-" + super.id
    }
}
