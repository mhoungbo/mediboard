import OxObject from "@/core/models/OxObject"
import { OxAttr, OxAttrNullable } from "@/core/types/OxObjectTypes"
import sanitizeHtml from "sanitize-html"

export default class OxHelpedText extends OxObject {
    searchWords: string[]
    searchRegex: RegExp

    constructor () {
        super()
        this.type = "helped_text"
        this.searchWords = []
        this.searchRegex = /$/gui
    }

    protected _relationsTypes = {
        group: OxObject,
        function: OxObject,
        mediuser: OxObject
    }

    get name (): OxAttr<string> {
        return super.get("name")
    }

    set name (value: OxAttr<string>) {
        this.set("name", value)
    }

    get nameHTML (): string {
        const name = super.get("name") as string
        return this.matchTextToSearchedWords(name)
    }

    get text (): OxAttr<string> {
        return super.get("text")
    }

    set text (value: OxAttr<string>) {
        this.set("text", value)
    }

    get textHTML (): string {
        const text = super.get("text") as string
        return this.matchTextToSearchedWords(text)
    }

    get class (): OxAttr<string> {
        return super.get("class")
    }

    set class (value: OxAttr<string>) {
        this.set("class", value)
    }

    get field (): OxAttr<string> {
        return super.get("field")
    }

    set field (value: OxAttr<string>) {
        this.set("field", value)
    }

    get owner (): OxObject | undefined {
        return this.loadForwardRelation<OxObject>("owner")
    }

    set owner (value: OxObject | undefined) {
        this.setForwardRelation("owner", value)
    }

    get dependValue1 (): OxAttrNullable<string> {
        return super.get("depend_value_1")
    }

    set dependValue1 (value: OxAttrNullable<string>) {
        this.set("depend_value_1", value)
    }

    get dependValue2 (): OxAttrNullable<string> {
        return super.get("depend_value_2")
    }

    set dependValue2 (value: OxAttrNullable<string>) {
        this.set("depend_value_2", value)
    }

    get viewDependField1 (): OxAttrNullable<string> {
        return super.get("_vw_depend_field_1")
    }

    get viewDependField2 (): OxAttrNullable<string> {
        return super.get("_vw_depend_field_2")
    }

    /**
     * Icon name getter
     * From the owner type get the corresponding icon
     */
    get iconName (): string {
        switch (this.owner?.type) {
        case "mediuser":
            return "account"
        case "function":
            return "accountGroup"
        case "group":
        default:
            return "building"
        }
    }

    /**
     * Build up the search regex from the input words
     * @param words
     */
    search (words: string[]) {
        this.searchWords = words
        let wordsRegex = ""
        this.searchWords.forEach((word, index) => {
            wordsRegex += this.replaceCharToDiacriticsRegex(word)
            if (index !== this.searchWords.length - 1) {
                wordsRegex += "|"
            }
        })
        this.searchRegex = new RegExp(`(${wordsRegex})`, "giu")
    }

    /**
     * Return text with span surrounding the matched text
     * @param text
     */
    matchTextToSearchedWords (text: string): string {
        if (this.searchWords.length > 0) {
            text = text.replaceAll(this.searchRegex, "<span class=\"matched\">$1</span>")
        }

        return sanitizeHtml(
            text,
            {
                allowedTags: ["span"],
                allowedClasses: {
                    span: ["matched"]
                },
                disallowedTagsMode: "escape"
            }
        )
    }

    /**
     * For each simple letter we allow to match their diacritics version
     * @param word
     */
    replaceCharToDiacriticsRegex (word: string): string {
        const diacriticsReplacement = {
            a: "[a\u00E0-\u00E6]",
            e: "[e\u00E8-\u00EB]",
            i: "[i\u00EC-\u00EF]",
            o: "[o\u00F2-\u00F6]",
            u: "[u\u00F9-\u00FC]",
            y: "[y\u00FD-\u00FF]"
        }

        let wordRegexDiacritics = ""
        word.split("").forEach((letter) => {
            if (letter in diacriticsReplacement) {
                wordRegexDiacritics += diacriticsReplacement[letter]
            }
            else {
                wordRegexDiacritics += letter
            }
        })

        return wordRegexDiacritics
    }
}
