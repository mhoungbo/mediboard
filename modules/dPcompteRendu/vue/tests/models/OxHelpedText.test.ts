/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxHelpedText from "@modules/dPcompteRendu/vue/models/OxHelpedText"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import OxObject from "@/core/models/OxObject"
import { storeObject } from "@/core/utils/OxStorage"

describe(
    "OxHelpedText",
    () => {
        beforeAll(() => {
            setActivePinia(pinia)
        })

        test.each([
            { word: "zrtpqsdfghjklmwxcvbn", expected: "zrtpqsdfghjklmwxcvbn" },
            { word: "aeiouy", expected: "[a\u00E0-\u00E6][e\u00E8-\u00EB][i\u00EC-\u00EF][o\u00F2-\u00F6][u\u00F9-\u00FC][y\u00FD-\u00FF]" },
            { word: "fleur", expected: "fl[e\u00E8-\u00EB][u\u00F9-\u00FC]r" }
        ])("test replace chars to diacritics regex with $word expect to be $expected", ({ word, expected }) => {
            const helpedText = new OxHelpedText()
            expect(helpedText.replaceCharToDiacriticsRegex(word)).toEqual(expected)
        })

        test.each([
            {
                words: ["zrtpqsdfghjklmwxcvbn"],
                expected: "zrtpqsdfghjklmwxcvbn"
            },
            {
                words: ["aeiouy"],
                expected: "[a\u00E0-\u00E6][e\u00E8-\u00EB][i\u00EC-\u00EF][o\u00F2-\u00F6][u\u00F9-\u00FC][y\u00FD-\u00FF]"
            },
            {
                words: ["fleur"],
                expected: "fl[e\u00E8-\u00EB][u\u00F9-\u00FC]r"
            },
            {
                words: ["zrtd", "pmlk", "nvbc", "tgfd", "wxcq"],
                expected: "zrtd|pmlk|nvbc|tgfd|wxcq"
            },
            {
                words: ["ayu", "oiau", "yeo"],
                expected: "[a\u00E0-\u00E6][y\u00FD-\u00FF][u\u00F9-\u00FC]|[o\u00F2-\u00F6][i\u00EC-\u00EF][a\u00E0-\u00E6][u\u00F9-\u00FC]|[y\u00FD-\u00FF][e\u00E8-\u00EB][o\u00F2-\u00F6]"
            },
            {
                words: ["fleur", "champs"],
                expected: "fl[e\u00E8-\u00EB][u\u00F9-\u00FC]r|ch[a\u00E0-\u00E6]mps"
            }
        ])("test search with $words to expect searchRegex to be $expected ", ({ words, expected }) => {
            const helpedText = new OxHelpedText()
            helpedText.search(words)
            expect(helpedText.searchWords).toEqual(words)
            expect(helpedText.searchRegex).toEqual(new RegExp(`(${expected})`, "giu"))
        })

        test.each([
            { owner: "mediuser", iconName: "account" },
            { owner: "function", iconName: "accountGroup" },
            { owner: "group", iconName: "building" },
            { owner: null, iconName: "building" },
            { owner: "lorem", iconName: "building" }
        ])("test iconName with owner $owner to match $iconName ", ({ owner, iconName }) => {
            const helpedText = new OxHelpedText()
            if (owner === null) {
                helpedText.relationships = {
                    owner: {
                        data: null
                    }
                }
            }
            else {
                const ownerObject = new OxObject()
                ownerObject.id = "1"
                ownerObject.type = owner
                storeObject(ownerObject)

                helpedText.relationships = {
                    owner: {
                        data: ownerObject
                    }
                }
            }

            expect(helpedText.iconName).toEqual(iconName)
        })

        test.each([
            {
                words: ["lorem", "ipsum"],
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                html: "<span class=\"matched\">Lorem</span> <span class=\"matched\">ipsum</span> dolor sit amet, consectetur adipiscing elit."
            },
            {
                words: ["dolor", "adipiscing"],
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                html: "Lorem ipsum <span class=\"matched\">dolor</span> sit amet, consectetur <span class=\"matched\">adipiscing</span> elit."
            },
            {
                words: ["ip", "ame"],
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                html: "Lorem <span class=\"matched\">ip</span>sum dolor sit <span class=\"matched\">ame</span>t, consectetur ad<span class=\"matched\">ip</span>iscing elit."
            }
        ])("test iconName with owner $owner to match $iconName ", ({ words, text, html }) => {
            const helpedText = new OxHelpedText()
            helpedText.attributes = {
                name: text,
                text
            }
            helpedText.search(words)

            expect(helpedText.name).toEqual(text)
            expect(helpedText.text).toEqual(text)
            expect(helpedText.nameHTML).toEqual(html)
            expect(helpedText.textHTML).toEqual(html)
        })
    }
)
