/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxHelpedTextAutocomplete from "@modules/dPcompteRendu/vue/components/OxHelpTextAutocomplete/OxHelpTextAutocomplete.vue"
import OxHelpedText from "@modules/dPcompteRendu/vue/models/OxHelpedText"
import OxTest from "@oxify/utils/OxTest"

/* eslint-disable dot-notation */

export default class OxHelpedTextAutocompleteTest extends OxTest {
    protected component = OxHelpedTextAutocomplete

    private helpedText = new OxHelpedText()

    protected beforeAllTests () {
        super.beforeAllTests()

        this.helpedText.id = "1"
        this.helpedText.type = "helped_text"
        this.helpedText.attributes = {
            name: "Lorem",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            class: "sample_movie",
            field: "description",
            dependValue1: "fict",
            _vw_depend_field_1: "Fiction"
        }
        this.helpedText.relationships = {
            owner: {
                data: null
            }
        }
        this.helpedText.search(["lorem"])
    }

    public testHasViewDependField1 () {
        const helpTextAutocomplete = this.vueComponent({ helpText: this.helpedText })

        expect(this.privateCall(helpTextAutocomplete, "hasViewDependField1")).toBeTruthy()
    }

    public testHasViewDependField2 () {
        const helpTextAutocomplete = this.vueComponent({ helpText: this.helpedText })

        expect(this.privateCall(helpTextAutocomplete, "hasViewDependField2")).toBeFalsy()
    }
}

(new OxHelpedTextAutocompleteTest()).launchTests()
