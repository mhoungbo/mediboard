<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Repositories;

use DateTimeImmutable;
use Exception;
use Ox\Core\CMbDT;
use Ox\Core\CPDODataSource;
use Ox\Core\CSQLDataSource;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;

/**
 * Repository to fetch CSampleMovie objects.
 */
class CompteRenduRepository
{
    private array $where = [];
    /** @var CPDODataSource|CSQLDataSource */
    private              $ds;
    private CCompteRendu $cr;

    public function __construct()
    {
        $this->cr = new CCompteRendu();
        $this->ds = $this->cr->getDS();
    }

    /**
     * @param CMediusers[] $users
     *
     * @return int
     * @throws Exception
     */
    public function countSigned(array $users = [], bool $signed = false): int
    {
        $this->addWhereSigned($signed);
        $this->addObjectIdNotNull();

        if ($users) {
            $this->addWhereSignatoryIdIn(array_column($users, '_id'));
        }

        $date_min = DateTimeImmutable::createFromFormat('Y-m-d', CMbDT::date('-1 YEAR'))->setTime(0, 0);
        $date_max = DateTimeImmutable::createFromFormat('Y-m-d', CMbDT::date())->setTime(23, 59, 59);

        $this->addWhereBetweenDates($date_min, $date_max);

        return $this->cr->countList($this->where);
    }

    private function addWhereSigned(bool $signed): void
    {
        if (!isset($this->where['compte_rendu.valide'])) {
            $this->where['compte_rendu.valide'] = $this->ds->prepare(
                '= ?' . ($signed ? '' : ' OR compte_rendu.valide IS NULL'),
                $signed ? '1' : '0'
            );
        }
    }

    private function addWhereSignatoryId(CMediusers $user): void
    {
        if (!isset($this->where['compte_rendu.signataire_id'])) {
            $this->where['compte_rendu.signataire_id'] = $this->ds->prepare('= ?', $user->_id);
        }
    }


    public function addWhereSignatoryIdIn(array $users_id): void
    {
        if (!isset($this->where['compte_rendu.signataire_id'])) {
            $this->where['compte_rendu.signataire_id'] = $this->ds->prepareIn($users_id);
        }
    }

    public function addWhereBetweenDates(DateTimeImmutable $date_min, DateTimeImmutable $date_max): void
    {
        if (!isset($this->where['compte_rendu.creation_date'])) {
            $this->where['compte_rendu.creation_date'] = $this->ds->prepareBetween(
                $date_min->format('Y-m-d H:i:s'),
                $date_max->format('Y-m-d H:i:s')
            );
        }
    }

    public function addObjectIdNotNull(): void
    {
        $this->where["compte_rendu.object_id"] = "IS NOT NULL";
    }
}
