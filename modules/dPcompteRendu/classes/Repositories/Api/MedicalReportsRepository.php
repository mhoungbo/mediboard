<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Repositories\Api;

use Exception;
use Ox\Core\Api\Request\RequestRelations;
use Ox\Core\CMbObject;
use Ox\Core\CStoredObject;
use Ox\Core\Repositories\AbstractRequestApiRepository;
use Ox\Mediboard\CompteRendu\CCompteRendu;

/**
 * Repository of CCompteRendu.
 */
class MedicalReportsRepository extends AbstractRequestApiRepository
{
    /**
     * Find Reports by a context.
     *
     * @throws Exception
     */
    public function findByContext(CMbObject $context): array
    {
        $this->where['object_id']    = $this->object->getDS()->prepare('= ?', $context->_id);
        $this->where['object_class'] = $this->object->getDS()->prepare('= ?', $context->_class);

        return $this->find();
    }

    /**
     * @inheritDoc
     */
    protected function getObjectInstance(): CStoredObject
    {
        return new CCompteRendu();
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    protected function massLoadRelation(array $objects, string $relation): void
    {
        switch ($relation) {
            case RequestRelations::QUERY_KEYWORD_ALL:
                $this->massLoadAuthor($objects);
                $this->massLoadFiles($objects);
                break;
            case CCompteRendu::RELATION_AUTHOR:
                $this->massLoadAuthor($objects);
                break;
            case CCompteRendu::RELATION_FILES:
                $this->massLoadFiles($objects);
                break;
            default:
                // Do nothing
        }
    }

    /**
     * MassLoad the author of reports.
     *
     * @throws Exception
     */
    private function massLoadAuthor(array $objects): void
    {
        CStoredObject::massLoadFwdRef($objects, 'author_id');
    }

    /**
     * MassLoad the files related to reports.
     *
     * @throws Exception
     */
    private function massLoadFiles(array $objects): void
    {
        CStoredObject::massLoadBackRefs($objects, 'files', 'file_date DESC');
    }
}
