<?php
/**
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\Content\JsonApiItem;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\CMbString;
use Ox\Core\CRequest;
use Ox\Core\CSQLDataSource;
use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Core\Module\CModule;
use Ox\Interop\Fhir\Resources\R4\ConceptMap\CFHIRResourceConceptMap;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Loinc\CLoinc;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Mediboard\Snomed\CSnomed;

/**
 * Remplacement d'un mot-cl� par une plus longue cha�ne de caract�res
 * S'associe sur toute propri�t� d'une classe dont la spec contient helped
 */
class CAideSaisie extends CMbObject
{
    public const RESOURCE_TYPE = "helped_text";

    public const RELATION_OWNER = 'owner';

    public const RELATION_USER     = 'user';
    public const RELATION_FUNCTION = 'function';
    public const RELATION_GROUP    = 'group';

    public const FIELDSET_AUTHOR = 'author';
    /** @var string */
    public const FIELDSET_TARGET = 'target';

    public const FIELDSET_DEPENDENCE = 'depends';

    // DB Table key
    public $aide_id;

    // DB References
    public $user_id;
    public $function_id;
    public $group_id;

    // DB fields
    public $class;
    public $field;
    public $name;
    public $text;
    public $depend_value_1;
    public $depend_value_2;

    // Form Fields
    public $_depend_field_1;
    public $_depend_field_2;
    public $_owner;
    public $_vw_depend_field_1;
    public $_vw_depend_field_2;
    public $_is_ref_dp_1;
    public $_is_ref_dp_2;
    public $_class_dp_1;
    public $_class_dp_2;
    public $_applied;
    public $_ref_object_dp_1;
    public $_ref_object_dp_2;
    public $_owner_view;
    public $_is_for_instance;

    /** @var CMediusers */
    public $_ref_user;

    /** @var CFunctions */
    public $_ref_function;

    /** @var CGroups */
    public $_ref_group;

    /** @var CMediusers|CFunctions|CGroups */
    public $_ref_owner;

    static $_load_lite = false;

    /** @var CLoinc[] */
    public $_ref_codes_loinc;
    /** @var CSnomed[] */
    public $_ref_codes_snomed;

    public $_ref_concept_map;
    public $_ref_codes_cim10_concept_map;
    public $_ref_codes_snomed_concept_map;

    /**
     * @inheritDoc
     */
    public function getSpec(): CMbObjectSpec
    {
        $spec        = parent::getSpec();
        $spec->table = 'aide_saisie';
        $spec->key   = 'aide_id';
        $spec->seek  = 'match';

        return $spec;
    }

    /**
     * @inheritDoc
     */
    public function getProps(): array
    {
        $props                = parent::getProps();
        $props["user_id"]     = "ref class|CMediusers back|aides_saisie fieldset|" . self::FIELDSET_AUTHOR;
        $props["function_id"] = "ref class|CFunctions back|aides fieldset|" . self::FIELDSET_AUTHOR;
        $props["group_id"]    = "ref class|CGroups back|aides_saisie fieldset|" . self::FIELDSET_AUTHOR;

        $props["class"]          = "str notNull fieldset|" . self::FIELDSET_TARGET;
        $props["field"]          = "str notNull fieldset|" . self::FIELDSET_TARGET;
        $props["name"]           = "str notNull index|text seekable fieldset|" . self::FIELDSET_DEFAULT;
        $props["text"]           = "text notNull seekable fieldset|" . self::FIELDSET_DEFAULT;
        $props["depend_value_1"] = "str fieldset|" . self::FIELDSET_DEPENDENCE;
        $props["depend_value_2"] = "str fieldset|" . self::FIELDSET_DEPENDENCE;

        $props["_depend_field_1"]    = "str";
        $props["_depend_field_2"]    = "str";
        $props["_vw_depend_field_1"] = "str fieldset|" . self::FIELDSET_DEPENDENCE;
        $props["_vw_depend_field_2"] = "str fieldset|" . self::FIELDSET_DEPENDENCE;
        $props["_owner"]             = "enum list|user|func|etab";
        $props["_owner_view"]        = "str";

        return $props;
    }

    /**
     * V�rifie l'unicit� d'une aide � la saisie
     *
     * @return string
     */
    public function check(): ?string
    {
        $msg = "";

        $ds = $this->_spec->ds;

        $where = [];
        if ($this->user_id) {
            $where["user_id"] = $ds->prepare("= %", $this->user_id);
        } elseif ($this->function_id) {
            $where["function_id"] = $ds->prepare("= %", $this->function_id);
        } elseif ($this->group_id) {
            $where["group_id"] = $ds->prepare("= %", $this->group_id);
        } else {
            $where[] = "user_id IS NULL AND function_id IS NULL AND group_id IS NULL";
        }

        $where["class"]          = $ds->prepare("= %", $this->class);
        $where["field"]          = $ds->prepare("= %", $this->field);
        $where["depend_value_1"] = $ds->prepare("= %", $this->depend_value_1);
        $where["depend_value_2"] = $ds->prepare("= %", $this->depend_value_2);
        $where["text"]           = $ds->prepare("= %", $this->text);
        $where["aide_id"]        = $ds->prepare("!= %", $this->aide_id);

        $sql = new CRequest();
        $sql->addSelect("count(aide_id)");
        $sql->addTable("aide_saisie");
        $sql->addWhere($where);

        $nb_result = $ds->loadResult($sql->makeSelect());

        if ($nb_result) {
            $msg .= "Cette aide existe d�j�<br />";
        }

        return $msg . parent::check();
    }

    /**
     * @inheritDoc
     */
    public function updateFormFields(): void
    {
        parent::updateFormFields();

        $this->_view = $this->name;

        $this->class = self::getSanitizedClassName($this->class);

        // Owner
        if ($this->user_id) {
            $this->_owner = "user";
        }
        if ($this->function_id) {
            $this->_owner = "func";
        }
        if ($this->group_id) {
            $this->_owner = "etab";
        }
        if (!$this->user_id && !$this->function_id && !$this->group_id) {
            $this->_owner = "instance";
        }

        $this->isForInstance();

        if ($this->class) {
            switch ($this->class) {
                case "CTransmissionMedicale":
                    $this->_class_dp_2 = "CCategoryPrescription";
                    break;
                case "CObservationResult":
                    $this->_class_dp_1 = "CObservationValueType";
                    $this->_class_dp_2 = "CObservationValueUnit";
                    break;
                case "CPrescriptionLineElement":
                    $this->_class_dp_1 = "CElementPrescription";
            }
        }

        if (!self::$_load_lite) {
            $this->loadDependValues();
        }
    }

    /**
     * @inheritDoc
     */
    public function store(): ?string
    {
        if ($msg = CCompteRendu::checkOwner($this)) {
            return $msg;
        }

        if ($this->name) {
            $this->name = CMbString::removeHtml($this->name);
        }
        if ($this->text) {
            $this->text = CMbString::removeHtml($this->text);
        }

        return parent::store();
    }

    /**
     * Charge l'utilisateur associ� � l'aide
     *
     * @param bool $cached Charge l'utilisateur depuis le cache
     *
     * @return CMediusers|null
     */
    public function loadRefUser(bool $cached = true): ?CMediusers
    {
        return $this->_ref_user = $this->loadFwdRef("user_id", $cached);
    }

    /**
     * Charge la fonction associ�e � l'aide
     *
     * @param bool $cached Charge la fonction depuis le cache
     *
     * @return CFunctions|null
     */
    public function loadRefFunction(bool $cached = true): ?CFunctions
    {
        return $this->_ref_function = $this->loadFwdRef("function_id", $cached);
    }

    /**
     * Charge l'�tablissement associ� � l'aide
     *
     * @param bool $cached Charge l'�tablissement depuis le cache
     *
     * @return CGroups|null
     */
    public function loadRefGroup(bool $cached = true): ?CGroups
    {
        return $this->_ref_group = $this->loadFwdRef("group_id", $cached);
    }

    /**
     * Charge le propri�taire de l'aide
     *
     * @return CMediusers|CFunctions|CGroups|null
     */
    public function loadRefOwner()
    {
        if ($this->_ref_owner) {
            return $this->_ref_owner;
        }
        if ($this->user_id) {
            return $this->_ref_owner = $this->loadRefUser();
        }
        if ($this->function_id) {
            return $this->_ref_owner = $this->loadRefFunction();
        }
        if ($this->group_id) {
            return $this->_ref_owner = $this->loadRefGroup();
        }

        return null;
    }

    /**
     * Permission generic check
     *
     * @param int $permType Type of permission : PERM_READ|PERM_EDIT|PERM_DENY
     *
     * @return bool
     */
    public function getPerm($permType): bool
    {
        if ($owner = $this->loadRefOwner()) {
            return $owner->getPerm($permType);
        }

        // No owner, the helped text is for the instance
        return CMediusers::get()->isAdmin();
    }

    /**
     * Traduit les depend fields
     *
     * @param CMbObject $object L'objet sur lequel sont appliqu�es les valeurs de d�pendances
     *
     * @return void
     */
    private function getDependValues($object): void
    {
        $locale                   = "$object->_class.$this->_depend_field_1.$this->depend_value_1";
        $this->_vw_depend_field_1 = (CAppUI::isTranslated($locale) ? CAppUI::tr($locale) : $this->depend_value_1);

        $locale                   = "$object->_class.$this->_depend_field_2.$this->depend_value_2";
        $this->_vw_depend_field_2 = (CAppUI::isTranslated($locale) ? CAppUI::tr($locale) : $this->depend_value_2);
    }

    /**
     * Charge les objets r�f�renc�s par l'aide
     *
     * @return void
     */
    private function loadDependObjects(): void
    {
        $this->_is_ref_dp_1 = false;
        $this->_is_ref_dp_2 = false;

        if (!class_exists($this->class)) {
            return;
        }

        $object = new $this->class();
        $field  = $this->field;
        $helped = [];
        if (isset($object->_specs[$field]) && $object->_specs[$field]->helped && !is_bool(
                $object->_specs[$field]->helped
            )) {
            if (!is_array($object->_specs[$field]->helped)) {
                $helped = [$object->_specs[$field]->helped];
            } else {
                $helped = $object->_specs[$field]->helped;
            }
        }
        foreach ($helped as $i => $depend_field) {
            $spec = $object->_specs[$depend_field];
            if ($spec instanceof CRefSpec) {
                $key        = "_is_ref_dp_" . ($i + 1);
                $this->$key = true;
                $key        = "depend_value_" . ($i + 1);
                if (is_numeric($this->$key)) {
                    $key_class                            = "_class_dp_" . ($i + 1);
                    $object_helped                        = new $this->{$key_class}();
                    $object_helped                        = $object_helped->getCached($this->$key);
                    $key_field                            = "_vw_depend_field_" . ($i + 1);
                    $this->$key_field                     = $object_helped->_view;
                    $this->{"_ref_object_dp_" . ($i + 1)} = $object_helped;
                }
            }
        }
    }

    public function loadDependValues(): void
    {
        // Depend fields
        if ($this->class && class_exists($this->class)) {
            $object                = new $this->class();
            $helped                = isset($object->_specs[$this->field]) ? $object->_specs[$this->field]->helped : [];
            $this->_depend_field_1 = $helped[0] ?? null;
            $this->_depend_field_2 = $helped[1] ?? null;

            $this->getDependValues($object);
        }
        $this->loadDependObjects();
    }

    public static function massLoadDependObjects($aides = []): void
    {
        if (!count($aides)) {
            return;
        }

        $first_aide = reset($aides);

        if (!class_exists($first_aide->class)) {
            return;
        }

        $object = new $first_aide->class();
        $field  = $first_aide->field;

        $helped = [];

        if ($object->_specs[$field]->helped && !is_bool($object->_specs[$field]->helped)) {
            if (!is_array($object->_specs[$field]->helped)) {
                $helped = [$object->_specs[$field]->helped];
            } else {
                $helped = $object->_specs[$field]->helped;
            }
        }
        foreach ($helped as $i => $depend_field) {
            $spec = $object->_specs[$depend_field];
            if ($spec instanceof CRefSpec) {
                $key_class     = "_class_dp_" . ($i + 1);
                $object_helped = new $first_aide->{$key_class}();
                $key           = "depend_value_" . ($i + 1);
                $ids           = CMbArray::pluck($aides, $key);

                $object_helped->loadList([$object_helped->_spec->key => CSQLDataSource::prepareIn($ids)]);
            }
        }
    }

    /**
     * Return idex type if it's special
     *
     * @param CIdSante400 $idex Idex
     *
     * @return string|null
     */
    public function getSpecialIdex(CIdSante400 $idex): ?string
    {
        if (CModule::getActive('snomed') && ($idex->tag == CSnomed::getSnomedTag())) {
            return "SNOMED";
        }

        if (CModule::getActive('loinc') && ($idex->tag == CLoinc::getLoincTag())) {
            return "LOINC";
        }

        return null;
    }

    /**
     * Get all CLoinc[] backrefs
     *
     * @return CLoinc[]|null
     */
    public function loadRefsCodesLoinc(): ?array
    {
        if (!CModule::getActive('loinc')) {
            return null;
        }

        return $this->_ref_codes_loinc = $this->loadRefCodes(CLoinc::class, CLoinc::getLoincTag());
    }

    /**
     * Get all CSnomed[] backrefs
     *
     * @return CSnomed[]|null
     */
    public function loadRefsCodesSnomed(): ?array
    {
        if (!CModule::getActive('snomed')) {
            return null;
        }

        return $this->_ref_codes_snomed = $this->loadRefCodes(CSnomed::class, CSnomed::getSnomedTag());
    }

    private function loadRefCodes(string $class_name, string $tag): array
    {
        $codes = [];

        $idexes = CIdSante400::getMatches($this->_class, $tag, null, $this->_id);
        foreach ($idexes as $idex) {
            $code = new $class_name();
            $code->load($idex->id400);

            $codes[$code->_id] = $code;
        }

        return $codes;
    }

    /**
     * Load codes cim10 from concept map
     *
     * @return array
     * @throws \Exception
     *
     */
    public function loadRefsCodesCIM10ConceptMap(): array
    {
        return $this->_ref_codes_cim10_concept_map = $this->loadRefsCodesConceptMap(
            CFHIRResourceConceptMap::TAG_CODE_CIM10
        );
    }

    /**
     * Load codes snomed from concept map
     *
     * @return array
     * @throws Exception
     */
    public function loadRefsCodesSnomedConceptMap(): array
    {
        return $this->_ref_codes_snomed_concept_map = $this->loadRefsCodesConceptMap(
            CFHIRResourceConceptMap::TAG_CODE_SNOMED
        );
    }

    private function loadRefsCodesConceptMap(string $tag): array
    {
        $idex                  = new CIdSante400();
        $ds                    = $idex->getDS();
        $where                 = [];
        $where["object_class"] = $ds->prepare('= ?', $this->_class);
        $where["object_id"]    = $ds->prepare('= ?', $this->_id);
        $where["tag"]          = $ds->prepare('= ?', $tag);
        $idexes                = $idex->loadList($where);

        $codes = [];
        foreach ($idexes as $_idex) {
            $datas_code = explode("|", $_idex->id400);
            $codes[]    = [
                "code"        => CMbArray::get($datas_code, 0),
                "display"     => CMbArray::get($datas_code, 1),
                "equivalence" => CMbArray::get($datas_code, 2),
            ];
        }

        return $codes;
    }

    /**
     * @return Item|null
     * @throws ApiException
     */
    public function getResourceUser(): ?Item
    {
        $user = $this->loadRefUser();
        if (!$user || !$user->_id) {
            return null;
        }

        return new Item($user);
    }

    public function getResourceFunction(): ?Item
    {
        $function = $this->loadRefFunction();
        if (!$function || !$function->_id) {
            return null;
        }

        return new Item($function);
    }


    public function getResourceGroup(): ?Item
    {
        $group = $this->loadRefGroup();
        if (!$group || !$group->_id) {
            return null;
        }

        return new Item($group);
    }

    public function getResourceOwner(): ?Item
    {
        if ($this->user_id) {
            return $this->getResourceUser();
        }

        if ($this->function_id) {
            return $this->getResourceFunction();
        }

        if ($this->group_id) {
            return $this->getResourceGroup();
        }

        return null;
    }

    public function setResourceOwner(?JsonApiItem $api_item): void
    {
        if ($api_item === null) {
            $this->user_id     = '';
            $this->function_id = '';
            $this->group_id    = '';

            return;
        }

        switch ($api_item->getClassName(false)) {
            case CMediusers::class:
                /** @var CMediusers $user */
                $user = $api_item->createModelObject(CMediusers::class, false)->getModelObject();
                $user->needsRead();
                $this->user_id     = $user->_id;
                $this->function_id = '';
                $this->group_id    = '';
                break;
            case CFunctions::class:
                /** @var CFunctions $function */
                $function = $api_item->createModelObject(CFunctions::class, false)->getModelObject();
                $function->needsRead();
                $this->function_id = $function->_id;
                $this->user_id     = '';
                $this->group_id    = '';
                break;
            case CGroups::class:
                /** @var CGroups $group */
                $group = $api_item->createModelObject(CGroups::class, false)->getModelObject();
                $group->needsRead();
                $this->group_id    = $group->_id;
                $this->user_id     = '';
                $this->function_id = '';
                break;
            default:
                throw new CMbException('CAideSaisie-Error-Owner must be user, function or group');
        }
    }


    /**
     * D�tecte si une aide � la saisie est d'instance
     *
     * @return bool
     */
    public function isForInstance()
    {
        return $this->_is_for_instance = (!$this->user_id && !$this->function_id && !$this->group_id);
    }

    public static function getSanitizedClassName($class): string
    {
        if ($class === 'CCerfa') {
            return 'Cerfa';
        }

        return $class;
    }

    public function getCSVFields()
    {
        $fields = parent::getCSVFields();

        if ($this->class === "CTransmissionMedicale" && $this->depend_value_2) {
            $fields["depend_value_2"] = $this->_vw_depend_field_2;
            $fields["chapitre"]       = $this->_ref_object_dp_2 ? $this->_ref_object_dp_2->chapitre : '';
        } else {
            $fields['chapitre'] = null;
        }

        return $fields;
    }
}
