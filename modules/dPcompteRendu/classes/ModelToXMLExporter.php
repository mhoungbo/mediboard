<?php

namespace Ox\Mediboard\CompteRendu;

use DOMElement;
use DOMException;
use Exception;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbString;
use Ox\Core\CMbXMLDocument;
use Ox\Core\CSQLDataSource;

class ModelToXMLExporter
{
    private const MODELS_ELEMENT_NAME       = "modeles";
    private const COMPTE_RENDU_ELEMENT_NAME = "CCompteRendu";
    private const MODEL_ID_ELEMENT_NAME     = "modele_id";
    private const CATEGORY_ELEMENT_NAME     = "cat";
    private const LISTS_ELEMENT_NAME        = "listes";
    private const LIST_ELEMENT_NAME         = "liste";
    private const NAME_ELEMENT_NAME         = "nom";
    private const CHOICE_ELEMENT_NAME       = "choix";

    /**
     * @var CCompteRendu[]|null
     */
    public ?array $models;

    public ?CSQLDataSource $ds;

    public CMbXMLDocument $xml;

    public string $filename;

    /**
     * @param string $model_id
     *
     * @throws CMbException
     */
    public function __construct(string $models_ids)
    {
        $this->ds     = CSQLDataSource::get("std");
        $this->models = $this->getModelFromIds($models_ids);
        if (!count($this->models)) {
            throw new CMbException("ModelToXMLExporter-error-nothing to export");
        }
        $this->xml      = new CMbXMLDocument();
        $this->filename = "ModeleExport.xml";
    }

    /**
     * @throws DOMException
     */
    public function doExport(): void
    {
        $root = $this->xml->createElement(self::MODELS_ELEMENT_NAME);
        foreach ($this->models as $model) {
            //Model components export
            $this->checkForComponents($model, $root, $this->xml);
            //Model export
            $model_element = $this->createModelElement($model, $this->xml);
            $root->appendChild($model_element);
        }
        $this->xml->appendChild($root);
    }

    /**
     * @throws CMbException
     * @throws Exception
     */
    private function getModelFromIds(string $models_ids)
    {
        $models_ids = explode("-", $models_ids);

        return (new CCompteRendu())->loadAll($models_ids);
    }

    /**
     * @throws DOMException
     * @throws Exception
     */
    public function createModelElement(CCompteRendu $model, CMbXMLDocument $xml): DOMElement
    {
        $model_element = $xml->createElement(self::COMPTE_RENDU_ELEMENT_NAME);
        $model->loadContent();

        foreach ($model->getProps() as $_field => $_spec) {
            if (
                in_array($_field, CCompteRendu::$fields_exclude_export)
                || $model->$_field === null
                || ($_field[0] === "_" && $_field !== "_source")
            ) {
                continue;
            }
            ${$_field} = $xml->createElement($_field);
            $textnode  = $xml->createTextNode(CMbString::utf8Encode($model->$_field));
            ${$_field}->appendChild($textnode);
            $model_element->appendChild(${$_field});
        }

        // Attribut modele_id
        $model_attribute = $xml->createAttribute(self::MODEL_ID_ELEMENT_NAME);
        $value           = $xml->createTextNode($model->_id);
        $model_attribute->appendChild($value);
        $model_element->appendChild($model_attribute);

        // Catégorie
        $cat           = $model->loadRefCategory();
        $cat_attribute = $xml->createAttribute(self::CATEGORY_ELEMENT_NAME);
        $value         = $xml->createTextNode(CMbString::utf8Encode($cat->nom));
        $cat_attribute->appendChild($value);
        $model_element->appendChild($cat_attribute);

        $this->checkForChoicesList($model, $model_element, $xml);

        return $model_element;
    }

    /**
     * @throws Exception
     */
    private function checkForChoicesList(CCompteRendu $model, DOMElement $model_element, CMbXMLDocument $xml)
    {
        preg_match_all("/\[Liste - ([^]]+)]/", $model->_source, $matches);

        $listes = $xml->createElement(self::LISTS_ELEMENT_NAME);
        if (count($matches[1])) {
            $model->loadRefUser()->loadRefFunction();
            $model->loadRefFunction();
            $model->loadRefGroup();

            foreach ($matches[1] as $_match) {
                $liste_choix = new CListeChoix();
                $where       = [
                    "nom" => $this->ds->prepare(" = ?", html_entity_decode($_match, ENT_COMPAT)),
                ];

                switch ($model->_owner) {
                    case "prat":
                        $where[] = "liste_choix.user_id     =     '$model->user_id' OR
                    liste_choix.function_id = '" . $model->_ref_user->function_id . "' OR
                    liste_choix.group_id    = '" . $model->_ref_user->_ref_function->group_id . "'";
                        break;
                    case "func":
                        $where[] = "liste_choix.function_id =     '$model->function_id' OR
                    liste_choix.group_id    = '" . $model->_ref_function->group_id . "'";
                        break;
                    case "etab":
                        $where[] = "liste_choix.group_id = '" . $model->group_id . "'";
                        break;
                    default:
                        break;
                }

                if ($liste_choix->loadObject($where)) {
                    $liste = $xml->createElement(self::LIST_ELEMENT_NAME);
                    $nom   = $xml->createAttribute(self::NAME_ELEMENT_NAME);
                    $value = $xml->createTextNode(CMbString::utf8Encode($_match));
                    $nom->appendChild($value);
                    $liste->appendChild($nom);

                    foreach ($liste_choix->_valeurs as $_valeur) {
                        $choix = $xml->createElement(self::CHOICE_ELEMENT_NAME);
                        $value = $xml->createTextNode(CMbString::utf8Encode($_valeur));
                        $choix->appendChild($value);
                        $liste->appendChild($choix);
                    }

                    $listes->appendChild($liste);
                }
            }
        }
        $model_element->appendChild($listes);
    }

    /**
     * @throws DOMException
     */
    public function checkForComponents(CCompteRendu $model, DOMElement $root, CMbXMLDocument $xml): array
    {
        $components = [];
        $model->loadComponents();
        if ($model->_ref_header->_id) {
            $header_element                       = $this->createModelElement($model->_ref_header, $xml);
            $components[$model->_ref_header->_id] = $model->_ref_header;
            $root->appendChild($header_element);
        }
        if ($model->_ref_footer->_id) {
            $footer_element                       = $this->createModelElement($model->_ref_footer, $xml);
            $components[$model->_ref_footer->_id] = $model->_ref_footer;
            $root->appendChild($footer_element);
        }
        if ($model->_ref_preface->_id) {
            $preface_element                       = $this->createModelElement($model->_ref_preface, $xml);
            $components[$model->_ref_preface->_id] = $model->_ref_preface;
            $root->appendChild($preface_element);
        }
        if ($model->_ref_ending->_id) {
            $ending_element                       = $this->createModelElement($model->_ref_ending, $xml);
            $components[$model->_ref_ending->_id] = $model->_ref_ending;
            $root->appendChild($ending_element);
        }

        return $components;
    }
}
