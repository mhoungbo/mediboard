<?php

namespace Ox\Mediboard\CompteRendu;

use DOMElement;
use DOMException;
use Exception;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CMbString;
use Ox\Core\CMbXMLDocument;
use Ox\Core\CModelObject;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;

class ModelsPackToXMLExporter
{
    private const PACKS_ELEMENT_NAME      = "packs";
    private const CPACK_ELEMENT_NAME      = "CPack";
    private const MODELS_ELEMENT_NAME     = "modeles";
    private const CATEGORY_ATTRIBUTE_NAME = "cat";
    /**
     * @var CMbObject|CModelObject|CStoredObject|null
     */
    private $owner;

    private string $filename;

    private CSQLDataSource $ds;

    private CMbXMLDocument $xml;

    private array $packs;

    /**
     * @throws CMbException
     * @throws Exception
     */
    public function __construct(string $packs_ids, ?string $owner_guid)
    {
        $this->ds    = CSQLDataSource::get("std");
        $this->packs = $this->getModelPacksFromIds($packs_ids);

        if (!count($this->packs)) {
            throw new CMbException("ModelsPackToXMLExporter-error-nothing to export");
        }
        $this->xml      = new CMbXMLDocument();
        $this->owner    = $owner_guid ? CMbObject::loadFromGuid($owner_guid) : CCompteRendu::getInstanceObject();
        $this->filename = $this->owner ? "Packs-" . $this->owner->_view . "-Export.xml" : "PackExport.xml";
    }

    /**
     * @return CMbXMLDocument
     */
    public function getXml(): CMbXMLDocument
    {
        return $this->xml;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @throws DOMException
     * @throws CMbException
     */
    public function doExport(): void
    {
        $root = $this->xml->createElement(self::PACKS_ELEMENT_NAME);
        foreach ($this->packs as $model_pack) {
            $pack_element = $this->createPackElement($model_pack, $this->xml);
            $root->appendChild($pack_element);
        }
        $this->xml->appendChild($root);
    }

    /**
     * @throws CMbException
     * @throws Exception
     */
    private function getModelPacksFromIds(string $packs_ids): array
    {
        $packs_ids = explode("-", $packs_ids);

        CMbArray::removeValue([], $packs_ids);

        $where = [
            "pack_id" => $this->ds->prepareIn($packs_ids),
        ];

        return (new CPack())->loadList($where);
    }

    /**
     * @throws DOMException|CMbException
     */
    private function createPackElement(CPack $model_pack, CMbXMLDocument $xml): DOMElement
    {
        $pack_element = $xml->createElement(self::CPACK_ELEMENT_NAME);

        foreach ($model_pack->getProps() as $_field => $_spec) {
            if (
                in_array($_field, CPack::$fields_exclude_export)
                || $model_pack->$_field === null
                || ($_field[0] === "_" && $_field !== "_source")
            ) {
                continue;
            }
            ${$_field} = $xml->createElement($_field);
            $textnode  = $xml->createTextNode(CMbString::utf8Encode($model_pack->$_field));
            ${$_field}->appendChild($textnode);
            $pack_element->appendChild(${$_field});
        }

        // Create category attribute
        $category = $model_pack->loadRefCategory();
        if ($category) {
            $cat_attribute = $xml->createAttribute(self::CATEGORY_ATTRIBUTE_NAME);
            $value         = $xml->createTextNode(CMbString::utf8Encode($category->nom));
            $cat_attribute->appendChild($value);
            $pack_element->appendChild($cat_attribute);
        }

        $model_pack->getModelesIds();
        if (count($model_pack->_modeles_ids)) {
            $models_list_element = $xml->createElement(self::MODELS_ELEMENT_NAME);
            foreach ($model_pack->_modeles_ids as $model_id) {
                $modele_exporter = new ModelToXMLExporter($model_id);
                $model           = CCompteRendu::findOrFail($model_id);
                //Model components export
                $modele_exporter->checkForComponents($model, $models_list_element, $this->xml);
                $model_element = $modele_exporter->createModelElement($model, $xml);
                $models_list_element->appendChild($model_element);
            }
            $pack_element->appendChild($models_list_element);
        }

        return $pack_element;
    }
}
