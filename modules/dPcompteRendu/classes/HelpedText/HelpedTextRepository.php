<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\HelpedText;

use Ox\Core\Api\Request\RequestApi;
use Ox\Core\CClassMap;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Mediboard\CompteRendu\CAideSaisie;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;

class HelpedTextRepository
{
    private CSQLDataSource $ds;
    private CAideSaisie    $aide;

    private int    $limit  = 0;
    private int    $offset = 0;
    private string $order  = '';

    public function __construct(CSQLDataSource $ds, CAideSaisie $aide = null)
    {
        $this->ds   = $ds;
        $this->aide = $aide ?? new CAideSaisie();
    }

    public function findForUser(CMediusers $user, string $class, string $field, string $keywords): array
    {
        return $this->find(['user_id' => $this->ds->prepare('= ?', $user->_id)], $class, $field, $keywords);
    }

    public function findForFunction(CFunctions $function, string $class, string $field, string $keywords): array
    {
        return $this->find(['function_id' => $this->ds->prepare('= ?', $function->_id)], $class, $field, $keywords);
    }

    public function findForGroup(CGroups $group, string $class, string $field, string $keywords): array
    {
        return $this->find(['group_id' => $this->ds->prepare('= ?', $group->_id)], $class, $field, $keywords);
    }

    public function findForInstance(string $class, string $field, string $keywords): array
    {
        return $this->find(
            ['user_id' => 'IS NULL', 'function_id' => 'IS NULL', 'group_id' => 'IS NULL'],
            $class,
            $field,
            $keywords
        );
    }

    public function findFromRequest(
        RequestApi $request,
        string     $class,
        string     $field,
        bool       $ignore_limit = false
    ): array {
        $where   = $request->getFilterAsSQL($this->ds);
        $where[] = $this->buildWhereForOwner();

        if ($order = $request->getSortAsSql()) {
            $this->setOrder($order);
        }

        if ($ignore_limit) {
            $this->setLimit(10_000);
        } else {
            $this->setOffset($request->getOffset());
            $this->setLimit($request->getLimit());
        }

        return $this->find($where, $class, $field, $request->getRequest()->query->get('search', '%'));
    }

    public function massLoadOwners(array $helped_texts): void
    {
        CStoredObject::massLoadFwdRef($helped_texts, 'user_id');
        CStoredObject::massLoadFwdRef($helped_texts, 'function_id');
        CStoredObject::massLoadFwdRef($helped_texts, 'group_id');
    }

    protected function find(array $where, string $class, string $field, string $keywords): array
    {
        if ($class) {
            $where['class'] = $this->ds->prepare('= ?', $class);
        }

        if ($field) {
            $where['field'] = $this->ds->prepare('= ?', $field);
        }

        $limit = $this->limit ? "$this->offset,$this->limit" : null;

        if ('' === $keywords) {
            $keywords = '%';
        }

        $this->aide->_totalSeek = 0;

        return $this->aide->seek($keywords, $where, $limit, true, null, $this->order);
    }

    private function buildWhereForOwner(): string
    {
        $user = CMediusers::get();
        $user->loadRefFunction();

        return implode(
            ' OR ',
            [
                $this->ds->prepare(
                    'user_id = ?1 OR function_id = ?2 OR group_id = ?3',
                    $user->_id,
                    $user->function_id,
                    $user->_ref_function->group_id
                ),
                '(user_id IS NULL AND function_id IS NULL AND group_id IS NULL)',
            ]
        );
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    public function setOrder(string $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getLastCount(): int
    {
        return $this->aide->_totalSeek;
    }
}
