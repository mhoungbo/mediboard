<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\HelpedText;

use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Cache;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CMbString;
use Ox\Core\CModelObject;
use Ox\Core\CModelObjectCollection;
use Ox\Core\Config\Conf;
use Ox\Core\CStoredObject;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Core\Module\CModule;
use Ox\Mediboard\CompteRendu\CAideSaisie;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Prescription\CCategoryPrescription;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class HelpedTextManager
{
    // 1 day TTL
    private const HELPED_CLASSES_TTL       = 60 * 60 * 24;
    private const HELPED_CLASSES_CACHE_KEY = 'HelpedTextManager-helped-classes';

    private Conf           $conf;
    private CacheInterface $cache;

    private array $messages = [];

    public function __construct(Conf $conf = null, CacheInterface $inner_outer_cache = null)
    {
        $this->conf  = $conf ?? new Conf();
        $this->cache = $inner_outer_cache ?? Cache::getCache(Cache::INNER_OUTER);
    }

    /**
     * Extract each word of each helped_text and put them in an array with the ids of the helped_texts where the word
     * can be found
     *
     * @param array $helped_texts
     * @param array $stop_words
     *
     * @return array
     */
    public function tokenize(array $helped_texts, array $stop_words, bool $to_utf8 = false): array
    {
        $all_tokens = [];
        foreach ($helped_texts as $helped_text) {
            $tokens = ($helped_text->name !== $helped_text->text ? "$helped_text->name " : null) . $helped_text->text;
            $tokens = CMbString::canonicalize($tokens);

            // Liste r�cup�r�e depuis http://www.regular-expressions.info/posixbrackets.html (Character class :punct:)
            $tokens = array_unique(preg_split("/[\s!\"\#$%&'()*+,\-\.\/:;<=>?@\[\]\\^_`{|}~]+/", $tokens));

            $name = CMbString::canonicalize($helped_text->name);

            if ($to_utf8) {
                $name = mb_convert_encoding($name, 'UTF-8', 'ISO-8859-1');
            }

            foreach ($tokens as $token) {
                if ($token === "") {
                    continue;
                }

                if ($to_utf8) {
                    $token = mb_convert_encoding($token, 'UTF-8', 'ISO-8859-1');
                }

                if ($token === $name || !array_key_exists($token, $stop_words)) {
                    if (!isset($all_tokens[$token])) {
                        $all_tokens[$token] = [];
                    }

                    $all_tokens[$token][] = (int)$helped_text->_id;
                }
            }
        }

        return $all_tokens;
    }

    /**
     * Convert an array of helped texts objects to their representation in array for javascript cache.
     *
     * @param CAideSaisie[] $helped_texts
     *
     * @return array
     */
    public function helpedTextsToArray(array $helped_texts): array
    {
        $helped_array = [];
        foreach ($helped_texts as $helped_text) {
            $arr = [
                'n' => mb_convert_encoding($helped_text->name, 'UTF-8', 'ISO-8859-1') . "\n",
                'f' => mb_convert_encoding($helped_text->field, 'UTF-8', 'ISO-8859-1'),
            ];

            if ($helped_text->name !== $helped_text->text) {
                $arr['t'] = mb_convert_encoding($helped_text->text, 'UTF-8', 'ISO-8859-1') . "\n";
            }

            if ($helped_text->group_id) {
                $arr['gid'] = $helped_text->group_id;
            } elseif ($helped_text->function_id) {
                $arr['fid'] = $helped_text->function_id;
            } elseif ($helped_text->user_id) {
                $arr['uid'] = $helped_text->user_id;
            }

            if ($helped_text->_vw_depend_field_1) {
                $arr['_vd1'] = mb_convert_encoding($helped_text->_vw_depend_field_1, 'UTF-8', 'ISO-8859-1');
            }

            if ($helped_text->_vw_depend_field_2) {
                $arr['_vd2'] = mb_convert_encoding($helped_text->_vw_depend_field_2, 'UTF-8', 'ISO-8859-1');
            }

            if ($helped_text->depend_value_1) {
                $arr['d1'] = mb_convert_encoding($helped_text->depend_value_1, 'UTF-8', 'ISO-8859-1');
            }

            if ($helped_text->depend_value_2) {
                $arr['d2'] = mb_convert_encoding($helped_text->depend_value_2, 'UTF-8', 'ISO-8859-1');
            }

            if ($links = $helped_text->loadRefsHyperTextLink()) {
                $arr['links'] = [];
                foreach ($links as $link) {
                    $arr['links'][] = [
                        'id' => (int) $link->_id,
                        'name' => mb_convert_encoding($link->name, 'UTF-8', 'ISO-8859-1'),
                        'link' => mb_convert_encoding($link->link, 'UTF-8', 'ISO-8859-1'),
                    ];
                }
            }

            $helped_array[$helped_text->_id] = $arr;
        }

        return $helped_array;
    }

    /**
     * Return the list of classes with their helped fields. If a class have no helped field it is not returned.
     *
     * @return array
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function getHelpedClassesWithFields(): array
    {
        if (!($helped_classes = $this->cache->get(self::HELPED_CLASSES_CACHE_KEY, null, self::HELPED_CLASSES_TTL))) {
            $classes = $this->getClasses();

            $helped_classes = [];
            foreach ($classes as $class_name) {
                /** @var CStoredObject $object */
                $object = new $class_name();

                foreach ($object->_specs as $field => $spec) {
                    if (!isset($spec->helped)) {
                        continue;
                    }

                    if (!isset($helped_classes[$class_name])) {
                        $helped_classes[$class_name] = [];
                    }

                    if (!is_array($spec->helped)) {
                        $helped_classes[$class_name][$field] = null;
                        continue;
                    }

                    // Build depends values
                    foreach ($spec->helped as $i => $depend_field) {
                        $key                                       = "depend_value_" . ($i + 1);
                        $helped_classes[$class_name][$field][$key] = [];
                        // Because some depend_fields are not enums (like object_class from CCompteRendu)
                        if (!isset($object->_specs[$depend_field]->_list)) {
                            continue;
                        }

                        foreach ($object->_specs[$depend_field]->_list as $value) {
                            $helped_classes[$class_name][$field][$key][$value] = "$object->_class.$depend_field.$value";
                        }
                    }
                }
            }

            ksort($helped_classes);

            $this->cache->set(self::HELPED_CLASSES_CACHE_KEY, $helped_classes, self::HELPED_CLASSES_TTL);
        }

        return $helped_classes;
    }

    public function changeClassNameForApiType(iterable $helped_texts): iterable
    {
        /** @var CAideSaisie $helped_text */
        foreach ($helped_texts as $helped_text) {
            if ($helped_text->class) {
                $helped_text->class = constant("{$helped_text->class}::RESOURCE_TYPE");
            }
        }

        return $helped_texts;
    }

    /**
     * For each helped_text if the user is not admin set the user to the current user and empty group and function.
     * Transform the api_type to the class name for the class attribute.
     *
     * @param CModelObjectCollection $helped_texts
     *
     * @return void
     */
    public function correctHelpedTexts(CModelObjectCollection $helped_texts): void
    {
        $current_user          = CMediusers::get();
        $current_user_is_admin = $this->isAdmin($current_user);

        foreach ($helped_texts as $helped_text) {
            if (!$helped_text instanceof CAideSaisie) {
                throw new CMbException('HelpedTextManager-Error-Expected an helped text object');
            }

            // If the user is not admin force current user
            if (!$current_user_is_admin) {
                $helped_text->user_id     = $current_user->_id;
                $helped_text->function_id = null;
                $helped_text->group_id    = null;
            }

            // Convert the api type of the class to the short class name
            if ($helped_text->class) {
                $helped_text->class = CModelObject::getClassNameByResourceType($helped_text->class, true);
            }
        }
    }

    public function canAccessFunction(int $group_id): bool
    {
        $module   = CModule::getActive("dPcompteRendu");
        $is_admin = $module && $module->canAdmin();

        return $is_admin
            || $this->conf->getForGroupId("dPcompteRendu CAideSaisie access_function", $group_id);
    }

    public function canAccessGroup(int $group_id): bool
    {
        $module   = CModule::getActive("dPcompteRendu");
        $is_admin = $module && $module->canAdmin();

        return $is_admin
            || $this->conf->getForGroupId("dPcompteRendu CAideSaisie access_group", $group_id);
    }

    public function getCsvFieldsNames(): array
    {
        return array_keys((new CAideSaisie())->getCSVFields());
    }

    public function importHelpedTexts(UploadedFile $file, string $owner_guid): array
    {
        $this->messages = [];

        $csv = $this->openFile($file);

        $user_id = $function_id = $group_id = $cat_group = null;

        if ($owner_guid !== 'Instance' && ($owner = CMbObject::loadFromGuid($owner_guid))) {
            switch ($owner->_class) {
                case "CMediusers":
                    /** @var CMediusers $owner */
                    $user_id   = $owner->_id;
                    $cat_group = $owner->loadRefFunction()->group_id;
                    break;
                case "CFunctions":
                    $function_id = $owner->_id;
                    $cat_group   = $owner->group_id;
                    break;
                case "CGroups":
                    $group_id  = $owner->_id;
                    $cat_group = $owner->_id;
            }
        }

        $csv->setColumnNames($this->readLine($csv, false));
        while ($line = $this->readLine($csv)) {
            $this->importLine($line, $user_id, $function_id, $group_id, $cat_group);
        }

        $csv->close();

        return $this->messages;
    }

    protected function getClasses(): array
    {
        return CApp::getInstalledClasses([], true);
    }

    protected function isAdmin(CMediusers $user): bool
    {
        return $user->isAdmin();
    }

    protected function openFile(UploadedFile $file): CCSVFile
    {
        $fp = fopen($file->getPathname(), 'r');

        return new CCSVFile($fp, CCSVFile::PROFILE_OPENOFFICE);
    }

    protected function readLine(CCSVFile $csv, bool $assoc = true): array
    {
        return $csv->readLine($assoc, true, true) ?: [];
    }

    protected function importLine(array $line, ?int $user_id, ?int $function_id, ?int $group_id, ?int $cat_group): void
    {
        $helped_text                 = new CAideSaisie();
        $helped_text->class          = $line['class'] ?? null;
        $helped_text->field          = $line['field'] ?? null;
        $helped_text->name           = $line['name'] ?? null;
        $helped_text->text           = $line['text'] ?? null;
        $helped_text->depend_value_1 = $line['depend_value_1'] ?? null;
        $helped_text->depend_value_2 = $line['depend_value_2'] ?? null;
        $helped_text->user_id        = $user_id;
        $helped_text->function_id    = $function_id;
        $helped_text->group_id       = $group_id;

        if ($helped_text->class === 'CTransmissionMedicale' && isset($line['chapitre'])) {
            $cat           = new CCategoryPrescription();
            $cat->nom      = $line['depend_value_2'];
            $cat->chapitre = $line['chapitre'];
            $cat->group_id = $cat_group;
            if (!$cat->loadMatchingObjectEsc()) {
                $cat->store();
            }

            $helped_text->depend_value_2 = $cat->_id;
        }

        $exists = $helped_text->loadMatchingObjectEsc();

        if ($msg = $helped_text->store()) {
            $this->messages[CAppUI::UI_MSG_ERROR][] = $msg;

            return;
        }

        $this->messages[CAppUI::UI_MSG_OK][] = $exists ? 'Aide � la saisie d�j� pr�sente' : 'CAideSaisie-msg-create';
    }
}
