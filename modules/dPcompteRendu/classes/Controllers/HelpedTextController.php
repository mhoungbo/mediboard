<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Controllers;

use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CMbPath;
use Ox\Core\CMbString;
use Ox\Core\CModelObject;
use Ox\Core\Controller;
use Ox\Core\CStoredObject;
use Ox\Core\FieldSpecs\CEnumSpec;
use Ox\Core\FileUtil\CCSVFile;
use Ox\Core\Kernel\Exception\ControllerException;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Module\CModule;
use Ox\Mediboard\CompteRendu\CAideSaisie;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\CompteRendu\HelpedText\HelpedTextManager;
use Ox\Mediboard\CompteRendu\HelpedText\HelpedTextRepository;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Prescription\CCategoryPrescription;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Throwable;

/**
 * Controller to display or create helped texts
 */
class HelpedTextController extends Controller
{
    public const QUERY_ALL = 'all';

    /**
     * @api
     *
     * List helped texts for the current user linked to the resource_type and field_name.
     */
    public function listForContext(
        string               $resource_type,
        string               $field_name,
        RequestApi           $request,
        HelpedTextManager    $manager,
        HelpedTextRepository $repository
    ): Response {
        $this->enforceSlave();

        $object_class = CModelObject::getClassNameByResourceType($resource_type, true);

        if (!is_subclass_of($object_class, CStoredObject::class)) {
            throw new ControllerException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $this->translator->tr('Controller-Error-Resource type must be storable', $resource_type)
            );
        }

        $ignore_limit = $request->getRequest()->query->getBoolean(self::QUERY_ALL);

        $helped_texts = $repository->findFromRequest($request, $object_class, $field_name, $ignore_limit);

        /** @var CStoredObject[] $helped_texts */
        $helped_texts = $manager->changeClassNameForApiType($helped_texts);

        if ($request->hasRelation(CAideSaisie::RELATION_OWNER)) {
            $repository->massLoadOwners($helped_texts);
        }

        $collection = Collection::createFromRequest($request, $helped_texts);

        if (!$ignore_limit) {
            $collection->createLinksPagination(
                $request->getOffset(),
                $request->getLimit(),
                $repository->getLastCount()
            );
        }

        if ($request->getRequest()->get('with_words_tokens')) {
            $collection->addMeta(
                'words_tokens',
                $manager->tokenize(
                    $helped_texts,
                    array_flip(explode(" ", $this->translator->tr("CAideSaisie-stop_words")))
                )
            );
        }

        return $this->renderApiResponse($collection);
    }

    public function getJson(
        string               $object_class,
        string               $field_name,
        HelpedTextRepository $repository,
        HelpedTextManager    $manager
    ): JsonResponse {
        $this->enforceSlave();

        if (!is_subclass_of($object_class, CStoredObject::class)) {
            $object_class = CModelObject::getClassNameByResourceType($object_class, true);
        }

        $current_user = $this->getCMediuser();
        $function     = $current_user->loadRefFunction();
        $group        = $function->loadRefGroup();

        $user_helped_texts     = $repository->findForUser($current_user, $object_class, $field_name, '%');
        $function_helped_texts = $repository->findForFunction($function, $object_class, $field_name, '%');
        $group_helped_texts    = $repository->findForGroup($group, $object_class, $field_name, '%');
        // Must use + instead of array_merge to keep the keys
        $helped_texts = $user_helped_texts + $function_helped_texts + $group_helped_texts
            + $repository->findForInstance($object_class, $field_name, '%');

        $repository->massLoadOwners($helped_texts);
        CStoredObject::massLoadBackRefs($helped_texts, 'hypertext_links');

        $owners = ['g' => [], 'f' => [], 'u' => []];
        if ($user_helped_texts !== []) {
            $owners['u'][$current_user->_id] = mb_convert_encoding($current_user->_view, 'UTF-8', 'ISO-8859-1');
        }

        if ($function_helped_texts !== []) {
            $owners['f'][$function->_id] = mb_convert_encoding($function->_view, 'UTF-8', 'ISO-8859-1');
        }

        if ($group_helped_texts !== []) {
            $owners['g'][$group->_id] = mb_convert_encoding($group->_view, 'UTF-8', 'ISO-8859-1');
        }

        return new JsonResponse(
            [
                'expire' => $this->getExpire(),
                'data'   => [
                    'aides'    => $manager->helpedTextsToArray($helped_texts),
                    'by_token' => $manager->tokenize(
                        $helped_texts,
                        array_flip(
                            explode(' ', $this->translator->tr('CAideSaisie-stop_words'))
                        ),
                        true
                    ),
                    'owners'   => $owners,
                ],
            ]
        );
    }

    /**
     * @api
     */
    public function create(RequestApi $request, HelpedTextManager $manager, HelpedTextRepository $repository): Response
    {
        $helped_texts = $request->getModelObjectCollection(
            CAideSaisie::class,
            [CModelObject::FIELDSET_DEFAULT, CAideSaisie::FIELDSET_TARGET, CAideSaisie::FIELDSET_DEPENDENCE]
        );

        $manager->correctHelpedTexts($helped_texts);

        $helped_texts = $this->storeCollection($helped_texts, false);
        if ($request->hasRelation(CAideSaisie::RELATION_OWNER)) {
            $repository->massLoadOwners($helped_texts);
        }

        $helped_texts = $manager->changeClassNameForApiType($helped_texts);

        return $this->renderApiResponse(Collection::createFromRequest($request, $helped_texts), Response::HTTP_CREATED);
    }

    public function index(HelpedTextManager $manager): Response
    {
        return $this->renderSmarty(
            'helped_texts/vw_idx_aides',
            [
                'access_function'         => $manager->canAccessFunction($this->request_context->getGroupId()),
                'classes'                 => $manager->getHelpedClassesWithFields(),
                'url_vw_list'             => $this->generateUrl('models_gui_helped_text_view_list'),
                'url_edit_helped_text'    => $this->generateUrl('models_gui_helped_text_view_edit'),
                'url_import_helped_text'  => $this->generateUrl('models_gui_helped_text_import'),
                'url_list_depends'        => $this->generateUrl(
                    'models_gui_helped_text_depend_values',
                    [
                        'object_class' => 'class_placeholder',
                        'field'        => 'field_placeholder',
                    ]
                ),
            ]
        );
    }

    public function vwList(
        RequestParams        $params,
        HelpedTextManager    $manager,
        HelpedTextRepository $repository
    ): Response {
        $this->enforceSlave();

        $class = $params->get("class", "str") ?? '';
        if ($params->get('export', 'bool default|0')) {
            $type = $params->get('type', 'enum list|User|Func|Etab|Instance notNull');

            return $this->exportHelpedText($repository, $manager, $type, $class);
        }

        $user_id        = $params->get("user_id", "ref class|CMediusers", PERM_READ);
        $function_id    = $params->get("function_id", "ref class|CFunctions", PERM_READ);
        $start_user     = $params->get("start_user", "num default|0");
        $start_function = $params->get("start_function", "num default|0");
        $start_group    = $params->get("start_group", "num default|0");
        $start_instance = $params->get("start_instance", "num default|0");
        $step           = $params->get("step", "num default|30");
        $keywords       = $params->get("keywords", "str") ?? '';
        $order_col_aide = $params->get("order_col_aide", "str");
        $order_way      = $params->get("order_way", "str");
        $aide_id        = $params->get("aide_id", "str");
        $field          = $params->get("field", "str") ?? '';
        $restricted     = $params->get("restricted", "bool default|0");

        if ($order_col_aide) {
            $repository->setOrder("$order_col_aide $order_way");
        }

        if (!$restricted && $step) {
            $repository->setLimit($step);
        }

        $user = CMediusers::find($user_id);
        if (!$user) {
            $user = $this->getCMediuser();
        }

        $owners = $user->getOwners();

        $function = new CFunctions();
        if ($function_id) {
            $function->load($function_id);
            $function->loadRefGroup();
        }

        $repository->setOffset($start_instance);
        $aides = [
            'instance' => [
                'aides' => $repository->findForInstance($class, $field, $keywords),
                'count' => $repository->getLastCount(),
            ],
        ];

        $all_aides = $aides['instance']['aides'];

        if (!$function_id) {
            $repository->setOffset($start_user);

            $aides['user'] = [
                'aides' => $repository->findForUser($user, $class, $field, $keywords),
                'count' => $repository->getLastCount(),
            ];

            $all_aides += $aides['user']['aides'];
        }

        if ($manager->canAccessFunction($this->request_context->getGroupId())) {
            $repository->setOffset($start_function);
            $aides['func'] = [
                'aides' => $repository->findForFunction(
                    $function_id ? $function : $user->loadRefFunction(),
                    $class,
                    $field,
                    $keywords
                ),
                'count' => $repository->getLastCount(),
            ];

            $all_aides += $aides['func']['aides'];
        }

        if ($manager->canAccessGroup($this->request_context->getGroupId())) {
            $repository->setOffset($start_group);
            $aides['etab'] = [
                'aides' => $repository->findForGroup(
                    $function_id ? $function->loadRefGroup() : $user->loadRefFunction()->loadRefGroup(),
                    $class,
                    $field,
                    $keywords
                ),
                'count' => $repository->getLastCount(),
            ];

            $all_aides += $aides['etab']['aides'];
        }

        $repository->massLoadOwners($all_aides);
        CStoredObject::massLoadBackRefs($all_aides, 'hypertext_links');

        // TODO : Find a way to massload those
        foreach ($all_aides as $aide) {
            $aide->loadRefsCodesLoinc();
            $aide->loadRefsCodesSnomed();
        }

        return $this->renderSmarty(
            'helped_texts/inc_tabs_aides',
            [
                "user"                    => $user,
                "function"                => $function,
                "aides"                   => $aides,
                "class"                   => $class,
                "field"                   => $field,
                "order_col_aide"          => $order_col_aide,
                "order_way"               => $order_way,
                "aide_id"                 => $aide_id,
                "owners"                  => $owners,
                "start_instance"          => $start_instance,
                "start_user"              => $start_user,
                "start_function"          => $start_function,
                "start_group"             => $start_group,
                "restricted"              => $restricted,
                'url_edit_helped_text'    => $this->generateUrl('models_gui_helped_text_edit'),
                'url_vw_edit_helped_text' => $this->generateUrl('models_gui_helped_text_view_edit'),
            ]
        );
    }

    public function vwEdit(RequestParams $params, HelpedTextManager $manager): Response
    {
        $this->enforceSlave();

        $restricted = $params->get('restricted', 'bool default|0');
        $aide_id    = $params->get("aide_id", "ref class|CAideSaisie");
        $user_id    = $params->get("user_id", "ref class|CMediusers");

        $current_user = $this->getCMediuser();

        // Aide sélectionnée
        $aide = $aide_id ? CAideSaisie::findOrFail($aide_id) : new CAideSaisie();

        $access_function = $manager->canAccessFunction($this->request_context->getGroupId());
        $access_group    = $manager->canAccessGroup($this->request_context->getGroupId());

        if ($aide->_id) {
            $aide->needsEdit();

            // This should be in the getPerm of the CAideSaisie object
            if ($aide->function_id && !$access_function) {
                CAppUI::accessDenied();
            } elseif ($aide->group_id && !$access_group) {
                CAppUI::accessDenied();
            }

            $aide->loadRefFunction();
            $aide->loadRefGroup();
            $aide->loadRefsCodesLoinc();
            $aide->loadRefsCodesSnomed();
        } elseif ($restricted || !$current_user->isAdmin()) {
            $aide->user_id = $user_id ?: $current_user->_id;

            if ($restricted) {
                if ($resource_type = $params->get('resource_type', 'str')) {
                    $aide->class = CModelObject::getClassNameByResourceType($resource_type, true);
                } else {
                    $aide->class = $params->get('class', 'str notNull');

                    if (!is_subclass_of($aide->class, CStoredObject::class)) {
                        $aide->class = CModelObject::getClassNameByResourceType($aide->class, true);
                    }
                }

                $aide->field          = $params->get('field', 'str notNull');
                $aide->depend_value_1 = $params->get('depend_value_1', 'str');
                $aide->depend_value_2 = $params->get('depend_value_2', 'str');
                $aide->_class_dp_1    = $params->get('class_depend_value_1', 'str');
                $aide->_class_dp_2    = $params->get('class_depend_value_2', 'str');
            }
        }

        $aide->loadRefUser();

        return $this->renderSmarty(
            'helped_texts/inc_edit_aide',
            [
                "restricted"              => $restricted,
                'current_user'            => $current_user,
                'current_function'        => CFunctions::getCurrent(),
                'current_group'           => CGroups::findOrFail($this->request_context->getGroupId()),
                "aide"                    => $aide,
                "classes"                 => $manager->getHelpedClassesWithFields(),
                "access_function"         => $access_function,
                "access_group"            => $access_group,
                "url_edit_aide"           => $this->generateUrl('models_gui_helped_text_edit'),
                "search_url"              => $this->generateUrl('models_gui_helped_text_view_list'),
                'url_groups_autocomplete' => $this->generateUrl('etablissement_groups_autocomplete'),
            ]
        );
    }

    public function doEdit(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('models_gui_helped_text_edit', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $helped_text_id = $params->post('aide_id', 'ref class|CAideSaisie', PERM_EDIT);

        if ($params->post('del', 'bool default|0')) {
            return $this->deleteHelpedText($helped_text_id);
        }

        $helped_text                 = $helped_text_id ? CAideSaisie::findOrFail($helped_text_id) : new CAideSaisie();
        $helped_text->user_id        = $params->post('user_id', 'ref class|CMediusers', PERM_READ);
        $helped_text->function_id    = $params->post('function_id', 'ref class|CFunctions', PERM_READ);
        $helped_text->group_id       = $params->post('group_id', 'ref class|CGroups', PERM_READ);
        $helped_text->class          = $params->post('class', 'str' . ($helped_text_id ? '' : ' notNull'));
        $helped_text->field          = $params->post('field', 'str' . ($helped_text_id ? '' : ' notNull'));
        $helped_text->depend_value_1 = $params->post('depend_value_1', 'str');
        $helped_text->depend_value_2 = $params->post('depend_value_2', 'str');

        $name              = $params->post('name', 'str' . ($helped_text_id ? '' : ' notNull'));
        $helped_text->name = $name ? stripslashes($name) : null;
        $text              = $params->post('text', 'str' . ($helped_text_id ? '' : ' notNull'));
        $helped_text->text = $text ? stripslashes($text) : null;

        try {
            $this->storeObject($helped_text, false);
            $this->addUiMsgOk($helped_text_id ? 'CAideSaisie-msg-modify' : 'CAideSaisie-msg-create', true);

            if ($params->post('restricted', 'bool default|0')) {
                CAppUI::js("AideSaisie.create('', '', '', '', '', '', '', '', '', {$helped_text->_id});");
            }
        } catch (CMbException $e) {
            $this->addUiMsgError($e->getMessage(), true);

            return $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->renderEmptyResponse();
    }

    public function import(RequestParams $params, HelpedTextManager $manager): Response
    {
        if ($params->getRequest()->getMethod() === Request::METHOD_POST) {
            if (!$this->isCsrfTokenValid('models_helped_text_import', $params->post('token', 'str'))) {
                throw new InvalidCsrfTokenException();
            }

            $this->importHelpedTexts($params, $manager);
            $owner_guid = $params->post('owner_guid', 'str notNull');
        } else {
            $owner_guid = $params->get("owner_guid", "str notNull");
        }


        return $this->renderSmarty(
            'helped_texts/aides_import_csv.tpl',
            [
                'owner'               => $owner_guid === 'Instance'
                    ? CCompteRendu::getInstanceObject()
                    : CMbObject::loadFromGuid($owner_guid),
                'owner_guid'          => $owner_guid,
                'object_class'        => $params->get("object_class", "str"),
                'import_url'          => $this->generateUrl('models_gui_helped_text_import'),
                'cats'                => CCategoryPrescription::$chapitres_elt,
                'prescription_active' => CModule::getActive('prescription') instanceof CModule,
            ]
        );
    }

    public function listDepends(string $object_class, string $field): Response
    {
        $object = new $object_class();
        $list   = [];

        if (isset($object->_specs[$field]) && $object->_specs[$field] instanceof CEnumSpec) {
            $list = $object->_specs[$field]->_locales;
        }

        array_unshift($list, " - " . $this->translator->tr("None"));

        ksort($list);

        return $this->renderSmarty('helped_texts/inc_select_enum_values.tpl', ['list' => $list]);
    }

    private function deleteHelpedText(int $helped_text_id): Response
    {
        $helped_text = CAideSaisie::findOrFail($helped_text_id);
        try {
            $this->deleteObject($helped_text);
            $this->addUiMsgOk('CAideSaisie-msg-delete', true);
        } catch (CMbException $e) {
            $this->addUiMsgError($e->getMessage(), true);
            $this->renderEmptyResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->renderEmptyResponse();
    }

    private function exportHelpedText(
        HelpedTextRepository $repository,
        HelpedTextManager    $manager,
        string               $type,
        string               $class = ''
    ): StreamedResponse {
        $repository->setLimit(100_000);

        switch (CMbString::lower($type)) {
            case 'user':
                $helped_texts = $repository->findForUser($this->getCMediuser(), $class, '', '');
                break;
            case 'func':
                $helped_texts = $repository->findForFunction(CFunctions::getCurrent(), $class, '', '');
                break;
            case 'etab':
                $helped_texts = $repository->findForGroup(
                    CGroups::findOrFail($this->request_context->getGroupId()),
                    $class,
                    '',
                    ''
                );
                break;
            case 'instance':
                $helped_texts = $repository->findForInstance($class, '', '');
                break;
            default:
                throw new ControllerException($this->translator->tr('HelpedTextController-Error-Invalid type', $type));
        }

        try {
            $file = tempnam('', 'aide_');

            $csv = new CCSVFile(fopen($file, 'w+'), CCSVFile::PROFILE_OPENOFFICE);
            $csv->writeLine($manager->getCsvFieldsNames());

            /** @var CAideSaisie $helped_text */
            foreach ($helped_texts as $helped_text) {
                $csv->writeLine($helped_text->getCSVFields());
            }
            $csv->close();
        } catch (Throwable $e) {
            if (isset($file) && file_exists($file)) {
                CMbPath::remove($file);
            }

            throw $e;
        }

        $filename = 'Aides saisie' . ($type ? " - $type" : '') . ($class ? " - "
                . $this->translator->tr($class) : '') . '.csv';

        return new StreamedResponse(
            function () use ($file) {
                readfile($file);
                CMbPath::remove($file);
            },
            Response::HTTP_OK,
            [
                'Content-Type'              => 'application/csv',
                'Content-Disposition'       => 'attachment; filename="' . $filename . '";',
                'Content-Transfer-Encoding' => 'binary',
                'Content-Length'            => filesize($file),
            ]
        );
    }

    private function importHelpedTexts(RequestParams $params, HelpedTextManager $manager): void
    {
        $owner_guid = $params->post("owner_guid", "str notNull");

        /** @var UploadedFile $file */
        $file = $params->getRequest()->files->get('import');

        $messages = $manager->importHelpedTexts($file, $owner_guid);

        if (isset($messages[CAppUI::UI_MSG_OK])) {
            foreach ($messages[CAppUI::UI_MSG_OK] as $message) {
                $this->addUiMsgOk($message);
            }
        }

        if (isset($messages[CAppUI::UI_MSG_ERROR])) {
            foreach ($messages[CAppUI::UI_MSG_ERROR] as $message) {
                $this->addUiMsgError($message);
            }
        }
    }

    protected function getExpire(): int
    {
        return time() + 3600;
    }
}
