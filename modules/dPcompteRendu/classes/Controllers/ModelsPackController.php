<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Controllers;

use Exception;
use Ox\Core\CMbObject;
use Ox\Core\Controller;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\CompteRendu\ModelsPackToXMLExporter;
use Ox\Mediboard\CompteRendu\XmlToModelsPackImporter;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for models packs
 */
class ModelsPackController extends Controller
{
    /**
     * @throws Exception
     */
    public function importPackModal(RequestParams $params): Response
    {
        $owner_guid = $params->get("owner_guid", "str");

        $owner = $owner_guid === "Instance" ?
            CCompteRendu::getInstanceObject() : CMbObject::loadFromGuid($owner_guid);

        return $this->renderSmarty(
            "inc_import_model_pack",
            [
                "owner"   => $owner,
                "classes" => CCompteRendu::getTemplatedClasses(),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function importXmlToPacks(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('models_import_pack', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $object_class = $params->post("object_class", "str");
        $owner_guid   = $params->post("owner_guid", "str");
        $file         = $params->getRequest()->files->get("formfile");

        $pack_importer = new XmlToModelsPackImporter($object_class, $owner_guid, $file);
        $pack_importer->doImport();

        foreach ($pack_importer->getResult() as $result) {
            $this->addUiMsgOk($result, true);
        }

        return $this->renderEmptyResponse();
    }

    /**
     * @throws Exception
     */
    public function exportPacksToXml(RequestParams $params): Response
    {
        if (!$this->isCsrfTokenValid('models_export_packs', $params->post('token', 'str'))) {
            throw new InvalidCsrfTokenException();
        }

        $this->enforceSlave();
        $pack_ids   = $params->post("pack_ids", "str");
        $owner_guid = $params->get("owner_guid", "str");

        $models_pack_exporter = new ModelsPackToXMLExporter($pack_ids, $owner_guid);
        $models_pack_exporter->doExport();

        $content = $models_pack_exporter->getXml()->saveXML();

        return $this->renderFileResponse($content, $models_pack_exporter->getFilename(), "text/xml");
    }
}
