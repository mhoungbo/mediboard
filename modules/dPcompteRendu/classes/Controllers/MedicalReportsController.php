<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\CompteRendu\Controllers;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\Content\RequestContentException;
use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\Controller;
use Ox\Mediboard\CompteRendu\CCompteRendu;
use Ox\Mediboard\CompteRendu\Repositories\Api\MedicalReportsRepository;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * CRUD API CCompteRendu.
 * Controller that answer the api calls on CCompteRendu.
 */
class MedicalReportsController extends Controller
{
    /**
     * Load a list of reports using the request parameters.
     *
     * Security:
     * => READ Perm needed.
     *
     * @throws ApiException
     * @throws InvalidArgumentException
     * @throws Exception
     * @api
     */
    public function list(
        string                   $resource_type,
        string                   $resource_id,
        MedicalReportsRepository $report_repository,
        RequestApi               $request_api
    ): Response {
        /** @var CMbObject $context */
        $context = $this->getObjectFromResourceTypeAndId($resource_type, $resource_id);
        $context->needsRead();

        $reports = $report_repository->findByContext($context);
        $reports = array_filter($reports, fn($report) => $this->checkPermRead($report));

        $report_repository->massLoadRelations($reports, $request_api->getRelations());

        $collection = Collection::createFromRequest($request_api, $reports);
        $collection->createLinksPagination(
            $request_api->getOffset(),
            $request_api->getLimit(),
            count($reports)
        );

        return $this->renderApiResponse($collection);
    }

    /**
     * Show a report identified by 'compte_rendu_id' parameter.
     *
     * Security:
     * => READ Perm needed.
     *
     * @throws ApiException
     * @api
     */
    public function show(CCompteRendu $report, RequestApi $request_api): Response
    {
        return $this->renderApiResponse(Item::createFromRequest($request_api, $report));
    }

    /**
     * Update an existing report identified by 'compte_rendu_id' parameter and using the body of the request.
     *
     * Security:
     * => EDIT Perm needed.
     *
     * @throws ApiException
     * @throws RequestContentException
     * @throws CMbException
     * @api
     */
    public function update(CCompteRendu $report, RequestApi $request_api): Response
    {
        /** @var CCompteRendu $report_from_request */
        $report_from_request = $request_api->getModelObject($report, [], ['annule', 'nom']);

        $item = $this->storeObject($report_from_request);
        $item->setModelFieldsets($request_api->getFieldsets());
        $item->setModelRelations($request_api->getRelations());

        return $this->renderApiResponse($item, Response::HTTP_OK);
    }

    /**
     * Delete a report identified by 'compte_rendu_id' parameter.
     *
     * Security:
     * => EDIT Perm needed.
     *
     * @throws Exception
     * @api
     */
    public function delete(CCompteRendu $report): Response
    {
        $this->deleteObject($report);

        return $this->renderResponse(null, Response::HTTP_NO_CONTENT);
    }
}
