<?php

namespace Ox\Mediboard\CompteRendu;

use DOMElement;
use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbArray;
use Ox\Core\CMbException;
use Ox\Core\CMbObject;
use Ox\Core\CMbString;
use Ox\Core\CMbXMLDocument;
use Ox\Core\CModelObject;
use Ox\Core\CStoredObject;
use Ox\Mediboard\Files\CFilesCategory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class XmlToModelsPackImporter
{
    private const PACKS_ELEMENT_NAME      = "packs";
    private const MODELS_ELEMENT_NAME     = "modeles";
    private const CATEGORY_ATTRIBUTE_NAME = "cat";

    /**
     * @var CMbObject|CModelObject|CStoredObject|null
     */
    public $owner;

    private string $object_class;

    private array $files;

    private array $result = [];

    /**
     * @param string|null  $object_class
     * @param string|null  $owner_guid
     * @param UploadedFile $file
     *
     * @throws CMbException
     * @throws Exception
     */
    public function __construct(?string $object_class, ?string $owner_guid, ?array $files)
    {
        if (!$files || !count($files)) {
            throw new CMbException('common-error-No file found.');
        }

        $this->files        = $files;
        $this->object_class = $object_class ?? "";
        $this->owner        = $owner_guid === "Instance" ?
            CCompteRendu::getInstanceObject() :
            CMbObject::loadFromGuid($owner_guid);
    }

    /**
     * @throws Exception
     */
    public function doImport(): void
    {
        CCompteRendu::$import = true;
        foreach ($this->files as $file) {
            /** @var UploadedFile $file */
            $doc = file_get_contents($file->getPathname());
            $xml = new CMbXMLDocument(null);
            $xml->loadXML($doc);
            $this->readXml($xml);
        }
        CCompteRendu::$import = false;
    }

    /**
     * @throws Exception
     */
    private function readXml(CMbXMLDocument $xml): void
    {
        $root = $xml->firstChild;

        if ($root->nodeName == self::PACKS_ELEMENT_NAME) {
            $root = $root->childNodes;
        } else {
            $root = [$xml->firstChild];
        }

        if ($root) {
            foreach ($root as $pack_element) {
                /** @var DOMElement $pack_element */
                $this->createPackFromDomElement($pack_element);
            }
        }
    }

    /**
     * @throws Exception
     */
    private function createPackFromDomElement(DOMElement $pack_element): void
    {
        $pack              = new CPack();
        $pack->category_id = $this->getCategoryId($pack_element);
        $modeles_ids       = [];
        // Read pack attributes and models from nodes
        foreach ($pack_element->childNodes as $node) {
            if (
                $node->nodeName !== self::MODELS_ELEMENT_NAME &&
                !in_array($node->nodeName, CPack::$fields_exclude_export) &&
                property_exists($pack->_class, $node->nodeName)
            ) {
                $pack->{$node->nodeName} = $node->nodeValue;
            } else {
                $modeles_ids = $this->importModelsFromDomElement($node);
            }
        }
        $pack->nom = CMbString::utf8Decode($pack->nom);

        // Assign context and owner
        $pack->object_class = $this->object_class;
        [$user_id, $function_id, $group_id] = $this->getOwner();
        $pack->user_id     = $user_id;
        $pack->function_id = $function_id;
        $pack->group_id    = $group_id;

        if ($msg = $pack->store()) {
            throw new CMbException($msg);
        }
        $this->result[] = $pack->nom;
        if (count($modeles_ids) && $pack->_id) {
            $this->linkModelsToPack($modeles_ids, $pack);
        }
    }

    private function importModelsFromDomElement(DOMElement $models_node): array
    {
        $modeles     = [];
        $modeles_ids = [];
        [$user_id, $function_id, $group_id] = $this->getOwner();
        foreach ($models_node->childNodes as $model_node) {
            $modeles[] = CCompteRendu::importModele(
                $model_node,
                $user_id,
                $function_id,
                $group_id,
                $this->object_class,
                $modeles_ids
            );
            foreach ($modeles as $key => $modele) {
                // We only link bodies model to pack
                if ($modele->type !== "body") {
                    unset($modeles[$key]);
                }
                $this->result[] = $modele->nom;
            }
        }

        return CMbArray::pluck($modeles, "_id");
    }

    /**
     * @throws Exception
     */
    private function getCategoryId(DOMElement $pack_element): ?string
    {
        if (!count($pack_element->attributes)) {
            return null;
        }
        $category_name = $pack_element->getAttribute(self::CATEGORY_ATTRIBUTE_NAME);
        $category      = new CFilesCategory();
        $category->nom = $category_name;
        $category->loadMatchingObjectEsc();
        if (!$category->_id) {
            if ($msg = $category->store()) {
                throw new CMbException($msg);
            }
            $this->result[] = $category->nom;
        }

        return $category->_id;
    }

    /**
     * @throws Exception
     */
    private function linkModelsToPack(array $modeles_ids, CPack $pack): void
    {
        foreach ($modeles_ids as $model_id) {
            $modele_to_pack            = new CModeleToPack();
            $modele_to_pack->modele_id = $model_id;
            $modele_to_pack->pack_id   = $pack->_id;
            if ($msg = $modele_to_pack->store()) {
                throw new CMbException($msg);
            }
        }
    }

    private function getOwner(): array
    {
        $user_id     = "";
        $function_id = "";
        $group_id    = "";

        switch ($this->owner->_class) {
            case "CMediusers":
                $user_id = $this->owner->_id;
                break;
            case "CFunctions":
                $function_id = $this->owner->_id;
                break;
            case "CGroups":
                $group_id = $this->owner->_id;
                break;
            default:
                //No owner
                break;
        }

        return [$user_id, $function_id, $group_id];
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
