/**
 * @package Mediboard\CompteRendu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

Aide = window.Aide || {
  classes: null,
  search_url: '',
  edit_url: '',
  import_url: '',
  depends_url: '',
  groups_autocomplete_url: '',
  edit: function(aide_id, user_id) {
    new Url().setRoute(Aide.edit_url, 'models_gui_helped_text_view_edit', 'dPcompteRendu')
      .addNotNullParam("aide_id", aide_id)
      .addNotNullParam("user_id", user_id)
      .requestModal("60%", "60%");
  },

  changePageUser: function (page) {
    const form = getForm('filterFrm');
    $V(form.start_user, page, false);
    Aide.loadTabsAides();
  },

  changePageFunc: function (page) {
    const form = getForm('filterFrm');
    $V(form.start_function, page, false);
    Aide.loadTabsAides();
  },

  changePageEtab: function (page) {
    const form = getForm('filterFrm');
    $V(form.start_group, page, false);
    Aide.loadTabsAides();
  },

  changePageInstance: function (page) {
    const form = getForm('filterFrm');
    $V(form.start_instance, page, false);
    Aide.loadTabsAides();
  },

  loadTabsAides: function(object_class, field) {
    const form = getForm('filterFrm');
    let url = new Url().setRoute(Aide.search_url);

    if (form) {
      url.addFormData(form)
    } else {
      url.addParam('class', object_class)
        .addParam('field', field)
        .addParam('restricted', '1')
    }

    url.requestUpdate("tabs_aides");
    return false;
  },

  remove: function(form, name) {
    if (confirm($T('CAideSaisie-action-deleteConfirm', name))) {
      $V(form.del, 1);
      form.onsubmit();
    }
  },

  exportAidesCSV: function (type, class_name) {
    new Url()
      .setRoute(Aide.search_url)
      .addParam('export', 1)
      .addParam('type', type)
      .addParam('class', class_name)
      .pop(400, 300);
  },

  popupImport: function(owner_guid, object_class) {
    new Url().setRoute(Aide.import_url, 'models_gui_helped_text_import', 'dPcompteRendu')
      .addParam("owner_guid", owner_guid)
      .addParam("object_class", object_class)
      .pop(750, 500);
  },

  getListDependValues: function(select, object_class, field) {
    if (select.hasClassName("loaded")) {
      return;
    }
    let oldValue = $V(select);
    let oldValueHTML = select.selectedOptions[0].innerHTML;
    new Url().setRoute(Aide.depends_url.replace('class_placeholder', object_class).replace('field_placeholder', field))
    .requestUpdate(select, function() {
      select.addClassName("loaded");
      // Si la valeur n'est plus pr�sente dans le select, on l'ajoute
      if ($A(select.options).pluck("value").indexOf(oldValue) == -1) {
        select.insert(DOM.option({value: oldValue}, oldValueHTML));
      }
      $V(select, oldValue, false);
    });
  },
  /**
   * Show different nomenclatures (eg: Loinc, Snomed,...)
   *
   * @param object_guid
   */
  showNomenclatures: function (object_guid) {
    new Url('patients', 'ajax_vw_nomenclatures')
      .addParam('object_guid', object_guid)
      .requestModal('60%', '80%', {onClose: Control.Modal.refresh});
  },

  sortList: function (order_col, order_way) {
    const form = getForm("filterFrm");
      $V(form.order_col_aide, order_col);
      $V(form.order_way, order_way);
      form.onsubmit();
  },

  emptyGroup: function (form, submit = true) {
    $V(form.group_id, '', false);

    if (form.group_id_view) {
      $V(form.group_id_view, '', false);
    }

    if (submit) {
      form.onsubmit();
    }
  },

  emptyFunction: function (form, submit = true) {
    $V(form.function_id, '', false);
    if (form.function_id_view) {
      $V(form.function_id_view, '', false);
    }

    if (submit) {
      form.onsubmit();
    }
  },

  emptyUser: function (form, submit = true) {
    $V(form.user_id, '', false);
    if (form.user_id_view) {
      $V(form.user_id_view, '', false);
    }

    if (submit) {
      form.onsubmit();
    }
  },

  setCurrentUser: function (user_id, user_view) {
    const form = getForm('editFrm');
    $V(form.user_id_view, user_view);
    $V(form.user_id, user_id);
  },

  setCurrentFunction: function (function_id, function_view) {
    const form = getForm('editFrm');
    $V(form.function_id_view, function_view);
    $V(form.function_id, function_id);
  },

  setCurrentGroup: function (group_id, group_view) {
    const form = getForm('editFrm');
    $V(form.group_id_view, group_view);
    $V(form.group_id, group_id);
  },

  makeUserAutocomplete: function (form) {
    new Url("mediusers", "ajax_users_autocomplete")
      .addParam("edit", "1")
      .addParam("input_field", "user_id_view")
      .autoComplete(form.user_id_view, null, {
        minChars:           0,
        method:             "get",
        select:             "view",
        dropdown:           true,
        afterUpdateElement: function (field, selected) {
          $V(form.user_id, selected.getAttribute("id").split("-")[2]);
        }
      });
  },

  makeFunctionAutocomplete: function (form) {
    new Url("mediusers", "ajax_functions_autocomplete")
      .addParam("edit", "1")
      .addParam("input_field", "function_id_view")
      .addParam("view_field", "text")
      .autoComplete(form.function_id_view, null, {
        minChars:           0,
        method:             "get",
        select:             "view",
        dropdown:           true,
        afterUpdateElement: function (field, selected) {
          $V(form.function_id, selected.getAttribute("id").split("-")[2]);
        }
      });
  },

  makeGroupAutocomplete: function (form) {
    new Url()
      .setRoute(Aide.groups_autocomplete_url)
      .addParam("edit", "1")
      .addParam("input_field", "group_id_view")
      .addParam("view_field", "text")
      .autoComplete(form.group_id_view, null, {
        minChars:           0,
        method:             "get",
        select:             "view",
        dropdown:           true,
        afterUpdateElement: function (field, selected) {
          $V(form.group_id, selected.getAttribute("id").split("-")[2]);
        }
      });
  },

  loadClasses: function(value) {
    let select = getForm("editFrm").elements['class'];

    // delete all former options except first
    while (select.length > 1) {
      select.options[1] = null;
    }

    // insert new ones
    for (let elm in Aide.classes) {
      if (typeof(Aide.classes[elm]) != "function") {
        select.options[select.length] = new Option($T(elm), elm);
      }
    }

    $V(select, value);
    Aide.loadFields();
  },

  loadFields: function (value) {
    const form = getForm("editFrm");
    let select = form.elements['field'];
    const class_name = form.elements['class'].value;

    // delete all former options except first
    while (select.length > 1) {
      select.options[1] = null;
    }

    // insert new ones
    for (const elm in Aide.classes[class_name]) {
      if (typeof (options[elm]) != "function") {
        select.options[select.length] = new Option($T(class_name + "-" + elm), elm);
      }
    }

    $V(select, value);
    Aide.loadDependances();
  },

  loadDependances: function (depend_value_1, depend_value_2) {
    const form = getForm('editFrm');
    let select_depend_1 = form.elements['depend_value_1'];
    let select_depend_2 = form.elements['depend_value_2'];
    const class_name = form.elements['class'].value;
    const field_name = form.elements['field'].value;

    // delete all former options except first
    // {{if !$aide->_is_ref_dp_1}}
    while (select_depend_1.length > 1) {
      select_depend_1.options[1] = null;
    }

    while (select_depend_2.length > 1) {
      select_depend_2.options[1] = null;
    }

    if (!Aide.classes[class_name] || !Aide.classes[class_name][field_name]) {
      return;
    }

    const options_depend_1 = Aide.classes[class_name][field_name]['depend_value_1'];
    for (const elm in options_depend_1) {
      if (typeof (options_depend_1[elm]) != "function") {
        select_depend_1.options[select_depend_1.length] = new Option(
          $T(options_depend_1[elm]),
          elm,
          elm === depend_value_1
        );
      }
    }
    $V(select_depend_1, depend_value_1);

    const options_depend_2 = Aide.classes[class_name][field_name]['depend_value_2'];
    for (const elm in options_depend_2) {
      if (typeof (options_depend_2[elm]) != "function") {
        select_depend_2.options[select_depend_2.length] = new Option(
          $T(options_depend_2[elm]),
          elm,
          elm === depend_value_2
        );
      }
    }

    $V(select_depend_2, depend_value_2);
  }
};
