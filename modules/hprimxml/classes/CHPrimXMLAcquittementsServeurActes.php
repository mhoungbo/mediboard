<?php
/**
 * @package Mediboard\Hprimxml
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hprimxml;

use Ox\Core\CAppUI;

/**
 * Class CHPrimXMLAcquittementsServeurActes
 */
class CHPrimXMLAcquittementsServeurActes extends CHPrimXMLAcquittementsServeurActivitePmsi
{
    /**
     * @see parent::__construct
     */
    function __construct()
    {
        $this->evenement = "evt_serveuractes";
        $this->acquittement = "acquittementsServeurActes";

        parent::__construct();

        $this->setSchemaFilename(null);
    }

    public function setSchemaFilename(?string $version): string
    {
        if (!$version) {
            $version = $this->getInstanceVersion($this->evenement);
        }

        $version = $this->getSchemanameFilenameVersion($version);

        return $this->schemafilename = "$this->schemapath/msgAcquittementsServeurActes$version.xsd";
    }
}

