<?php

/**
 * @package Mediboard\Hprimxml
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Hprimxml;

use Ox\Interop\Eai\ConfigurationActorInterface;
use Ox\Mediboard\Cabinet\CActeNGAP;
use Ox\Mediboard\System\AbstractConfigurationRegister;
use Ox\Mediboard\System\CConfiguration;

/**
 * Class CConfigurationHprimxml
 *
 * @codeCoverageIgnore
 */
class CConfigurationHprimxml extends AbstractConfigurationRegister implements ConfigurationActorInterface
{
    private const SECTION_NAME = 'CEchangeHprim';

    /**
     * @return void
     */
    public function register(): void
    {
        CConfiguration::register(
            [
                "CGroups" => [
                    "hprimxml" => [
                        "CHPrimXMLDocument" => [
                            "emetteur_application_code" => "str default|Mediboard",
                            "emetteur_application_libelle" => "str default|Mediboard SIH",
                        ],
                        "CActeNGAP" => [
                            "change_mpc_to_mpcp_for_psychiatry" => "bool default|0",
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @return string[]
     */
    public function getPrefixSectionActor(): array
    {
        return ['CEchangeHprim'];
    }

    /**
     * Get configurations actor object
     *
     * @return array
     */
    public function getConfigurationsActor(): array
    {
        return array_merge($this->getConfigurationsSender(), $this->getConfigurationsReceiver());
    }

    /**
     * Get configurations for sender object
     *
     * @return string[][][]
     */
    private function getConfigurationsSender(): array
    {
        $common_configs = [
            // Format
            self::SECTION_NAME . '-format'      => [
                'display_errors' => 'bool default|1',
            ],

            // Handle
            self::SECTION_NAME . '-handle'      => [
                'use_sortie_matching'      => 'bool default|1',
                'fully_qualified'          => 'bool default|1',
                'check_similar'            => 'bool default|0',
                'att_system'               => 'enum list|acteur|application|syst�me|finessgeographique|finessjuridique default|syst�me localize',
                'insc_integrated'          => 'bool default|0',
                'frais_divers'             => 'enum list|fd|presta default|fd localize',
                'prestation'               => 'enum list|nom|idex default|nom localize',
                'force_birth_rank_if_null' => 'bool default|0',
            ],

            // Digit
            self::SECTION_NAME . '-digit'       => [
                'type_sej_hospi'   => 'str',
                'type_sej_ambu'    => 'str',
                'type_sej_urg'     => 'str',
                'type_sej_exte'    => 'str',
                'type_sej_scanner' => 'str',
                'type_sej_chimio'  => 'str',
                'type_sej_dialyse' => 'str',
                'type_sej_pa'      => 'str',
            ],

            // Purge
            self::SECTION_NAME . '-purge'       => [
                'purge_idex_movements' => 'bool default|0',
            ],

            // Repair
            self::SECTION_NAME . '-auto-repair' => [
                'repair_patient' => 'bool default|1',
            ],

            // AppFine
            self::SECTION_NAME . '-appFine'     => [
                'handle_appFine' => 'bool default|0',
            ],

            self::SECTION_NAME . '-TAMM-SIH' => [
                'handle_tamm_sih' => 'str',
            ],
        ];

        return [
            'CSenderSOAP'       => $common_configs,
            'CSenderSFTP'       => $common_configs,
            'CSenderFTP'        => $common_configs,
            'CSenderHTTP'       => $common_configs,
            'CSenderFileSystem' => $common_configs,
            'CSenderMLLP'       => $common_configs,
        ];
    }

    /**
     * Configurations for receiver object
     *
     * @return array
     */
    private function getConfigurationsReceiver(): array
    {

        $events_versions = [];
        foreach (CHPrimXML::$versions as $_events) {
            foreach ($_events as $_event_name => $_versions) {
                $events_versions[$_event_name] = "enum list|" . implode('|', $_versions);
            }
        }

        $config_hprim = [
            self::SECTION_NAME . '-format' => [
                'encoding' => "enum list|UTF-8|ISO-8859-1 default|UTF-8",
                'uppercase_fields' => "bool default|0",
            ],

            self::SECTION_NAME . '-version' => $events_versions,

            self::SECTION_NAME . '-send-trigger' => [
                'send_sortie_prevue' => "bool default|1",
                "send_all_patients" => "bool default|0",
                "send_default_serv_with_type_sej" => "bool default|0",
                "send_volet_medical" => "bool default|0",
                "send_birth" => "bool default|0",
                "send_movement_location" => "bool default|0",
                "send_insured_without_admit" => "bool default|0",
                "send_child_admit" => "bool default|1",
                "send_no_facturable"              => "enum list|0|1|2 default|1",
                "send_timing_bloc"                => "bool default|0",
                "send_prescripteur_ngap"          => "enum list|acte|demande default|acte",
                "send_actes"                      => "enum list|ccamngap|ccam|ngap default|ccamngap",
                "send_actes_only_functions"       => "str",
            ],

            self::SECTION_NAME . '-send-appFine' => [
                'send_appFine' => "bool default|0",
            ],

            self::SECTION_NAME . '-send-sih-cabinet' => [
                'sih_cabinet_id' => 'num',
            ],

            self::SECTION_NAME . '-build' => [
                "build_id_sejour_tag"             => "str",
                "build_frais_divers"              => "enum list|fd|presta default|fd",
                "build_id_professionnel_sante"    => "enum list|adeli|rpps default|adeli",
                "transform_X_code_CIM"            => "bool default|0",
            ],

            self::SECTION_NAME . '-application' => [
                'receive_ack' => "bool default|1"
            ],
        ];

        return [
            'CDestinataireHprim' => $config_hprim,
        ];
    }
}
