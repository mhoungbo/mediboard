{{*
 * @package Mediboard\Hospi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=show_oxlabo_alert value=0}}

{{mb_script module=hospi script=vw_affectations ajax=1}}
{{mb_script module=planningOp script=prestations ajax=1}}
{{if "oxLaboClient"|module_active && 'Ox\Mediboard\OxLaboClient\OxLaboClient::canShowOxLabo'|static_call:null}}
    {{mb_script module=oxLaboClient script=oxlaboalert ajax=true}}
    {{assign var=show_oxlabo_alert value=1}}
{{/if}}

<script>
  Main.add(function () {
    {{if "dPImeds"|module_active && "dPhospi vue_tableau show_labo_results"|gconf}}
      ImedsResultsWatcher.loadResults();
    {{/if}}
    {{if "oxLaboClient"|module_active && 'Ox\Mediboard\OxLaboClient\OxLaboClient::canShowOxLabo'|static_call:null && "dPhospi vue_tableau show_labo_results"|gconf}}
      OxLaboAlert.loadResults();
    {{/if}}
  });
</script>

<table class="main me-padding-top-4 me-w100">
  <tr>
    <td id="tableauAffectations">
      {{mb_include module=hospi template="inc_tableau_affectations_lits"}}
    </td>
    <td class="me-padding-left-4 narrow">
      {{mb_include module=hospi template="inc_patients_a_placer"}}
    </td>
  </tr>
</table>
