{{*
 * @package Mediboard\Hospi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<div class="big-info">
  <h2>{{tr}}dPhospi-title-prestations csv export{{/tr}}</h2>

  <strong>{{tr}}dPhospi-subtitle-export for current group{{/tr}}</strong>
  <br />
  <br />

  {{tr}}common-exported_fields{{/tr}} :
  <ol>
    <li><strong>prestation</strong> : {{tr}}CPrestationExpert-nom-desc{{/tr}}</li>
    <li><strong>type</strong> : {{tr}}CItemPrestation-object_class-desc{{/tr}}</li>
    <li><strong>type_admission</strong> : {{mb_label class=CPrestationExpert field=type_hospi}} ({{$types_admission}})
    </li>
    <li><strong>M</strong> : {{tr}}CPrestationExpert-type_pec-M-desc{{/tr}}</li>
    <li><strong>C</strong> : {{tr}}CPrestationExpert-type_pec-C-desc{{/tr}}</li>
    <li><strong>O</strong> : {{tr}}CPrestationExpert-type_pec-O-desc{{/tr}}</li>
    <li><strong>SSR</strong> : {{tr}}CPrestationExpert-type_pec-SSR-desc{{/tr}}</li>
    <li><strong>item</strong> : {{tr}}CItemPrestation{{/tr}}</li>
    <li><strong>rang</strong> : {{tr}}CItemPrestation-rank-desc{{/tr}}</li>
    <li><strong>identifiant_externe</strong> : {{tr}}dPhospi-export_csv-idex description{{/tr}}
    </li>
  </ol>
  <br />
</div>

<table class="main form">
  <tr>
    <td class="button">
      <a class="button fas fa-external-link-alt" target="_blank"
         href="?m=dPhospi&raw=ajax_export_prestation_csv">{{tr}}Export{{/tr}}</a>
    </td>
  </tr>
</table>
