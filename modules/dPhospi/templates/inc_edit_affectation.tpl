{{*
 * @package Mediboard\Hospi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<script>
  updateModeEntree = (select) => {
    let selected = select.options[select.selectedIndex];
    let form = select.form;
    $V(form.elements.mode_entree, selected.dataset.mode);
    $V(form.elements.provenance, selected.dataset.provenance);
  };

  updateModeSortie = (select) => {
    let selected = select.options[select.selectedIndex];
    let form = select.form;
    $V(form.elements.mode_sortie, Object.isUndefined(selected.dataset.mode) ? '' : selected.dataset.mode);
  };

  loadTransfert = (mode_sortie) => {
    $('listEtabExterne').setVisible(parseInt(mode_sortie) === 0 || parseInt(mode_sortie) === 7);
  };

  Main.add(function () {
    var form = getForm('editAffect');
    var div_content = form.up('div.content');
    {{if !$IS_MEDIBOARD_EXT_DARK}}
     div_content.setStyle({overflow: 'visible'});
    {{/if}}
    div_content.up('div.modal').setStyle({overflow: 'visible'});

    {{if $affectation->_id && $affectation->sejour_id}}
    var url = new Url("hospi", "ajax_lit_autocomplete");
    url.addParam('group_id', '{{$affectation->_ref_sejour->group_id}}');
    url.autoComplete(form.keywords, null, {
      minChars: 2,
      method: "get",
      select: "view",
      dropdown: true,
      afterUpdateElement: function (field, selected) {
        var value = selected.id.split('-')[2];
        $V(form.lit_id, value);
      }
    });
    {{/if}}
  });
</script>

<form name="editAffect" method="post"
      onsubmit="return onSubmitFormAjax(this, (function() {
        Control.Modal.close();
        if (window.refreshMouvements) {
        if ((this._lock_all_lits && this._lock_all_lits.checked)) {
        refreshMouvements(window.loadNonPlaces);
        }
        else {
        var lit_id = $V(this.lit_id);
        if (lit_id && lit_id != '{{$affectation->lit_id}}') {
        refreshMouvements(null, lit_id);
        }
        refreshMouvements(window.loadNonPlaces, '{{$affectation->lit_id}}');
        }
        }
        }).bind(this));">
  <input type="hidden" name="m" value="hospi" />
  <input type="hidden" name="dosql" value="do_affectation_aed" />
  <input type="hidden" name="del" value="0" />
  {{mb_key object=$affectation}}
  {{mb_field object=$affectation field=lit_id      hidden=true}}
  {{mb_field object=$affectation field=function_id hidden=true}}

  <table class="form">
    <tr>
      {{if $affectation->_id}}
        <th class="title" colspan="4">
          {{if $affectation->sejour_id}}
            {{$affectation->_ref_sejour->_ref_patient}}
          {{else}}
            Lit bloqu�
          {{/if}}
        </th>
      {{/if}}
    </tr>
    {{if !$affectation->_id}}
      {{if !$urgence || !$mod_urgence}}
        <tr>
          <th>
            <input type="checkbox" name="_lock_all_lits" value="1" onchange="{{if $urgence}}$V(this.form._lock_all_lits_urgences, this.checked ? 1 : 0);{{/if}}"/>
            <input type="hidden" name="_lock_all_lits_urgences" value="0" />
          </th>
          <td colspan="3">Bloquer tous les lits du service {{$lit->_ref_chambre->_ref_service->nom}}</td>
        </tr>
      {{/if}}
    {{/if}}
    <tr>
      <th>
        {{mb_label object=$affectation field=entree}}
      </th>
      <td>
        {{mb_field object=$affectation field=entree form=editAffect register=true}}
      </td>
      <th>
        {{mb_label object=$affectation field=sortie}}
      </th>
      <td>
        {{mb_field object=$affectation field=sortie form=editAffect register=true}}
      </td>
    </tr>

    {{if $affectation->_id && $affectation->sejour_id}}
      <tr>
        <th>
          {{mb_label object=$affectation field=lit_id}}
        </th>
        <td colspan="3">
          <input type="text" name="keywords" value="{{$lit}}" />
        </td>
      </tr>
      <tr>
        <th>
            {{mb_label object=$affectation field=mode_entree}}
        </th>
        <td>
            {{assign var=modes_entree value='Ox\Mediboard\PlanningOp\CModeEntreeSejour::listModeEntree'|static_call:$affectation->_ref_sejour->group_id}}

            {{if $conf.dPplanningOp.CSejour.use_custom_mode_entree && $modes_entree|@count}}
              {{mb_field object=$affectation field=mode_entree hidden=true}}
              <select name="mode_entree_id" class="{{$affectation->_props.mode_entree_id}}" onchange="updateModeEntree(this);">
                  <option value="">&mdash; {{tr}}Choose{{/tr}}</option>
                  {{foreach from=$modes_entree item=mode_entree}}
                    <option value="{{$mode_entree->_id}}" data-mode="{{$mode_entree->mode}}"
                            data-provenance="{{$mode_entree->provenance}}"
                            {{if $affectation->mode_entree_id == $mode_entree->_id}}selected{{/if}}>
                        {{$mode_entree->_view}}
                    </option>
                  {{/foreach}}
              </select>
            {{else}}
                {{mb_field object=$affectation field=mode_entree emptyLabel="Choose"}}
            {{/if}}
        </td>
        <th>
            {{mb_label object=$affectation field=mode_sortie}}
        </th>
        <td>
            {{assign var=modes_sortie value='Ox\Mediboard\PlanningOp\CModeSortieSejour::listModeSortie'|static_call:$affectation->_ref_sejour->group_id}}
            {{if $conf.dPplanningOp.CSejour.use_custom_mode_sortie && $modes_sortie|@count}}
              {{mb_field object=$affectation field=mode_sortie onchange="loadTransfert(this.value);" hidden=true}}
              <select name="mode_sortie_id" class="{{$affectation->_props.mode_sortie_id}}" onchange="updateModeSortie(this);">
                <option value="">&mdash; {{tr}}Choose{{/tr}}</option>
                  {{foreach from=$modes_sortie item=mode_sortie}}
                    <option value="{{$mode_sortie->_id}}" data-mode="{{$mode_sortie->_code_mode_sortie}}"
                            {{if $affectation->mode_sortie_id == $mode_sortie->_id}}selected{{/if}}>
                        {{$mode_sortie->_view}}
                    </option>
                  {{/foreach}}
              </select>
            {{else}}
                {{mb_field object=$affectation field=mode_sortie emptyLabel="Choose" onchange="loadTransfert(this.value);"}}
            {{/if}}

            <div id="listEtabExterne" {{if !$affectation->etablissement_sortie_id}}style="display:none"{{/if}}>
              {{mb_field object=$affectation field="etablissement_sortie_id" hidden=true}}
              <input type="text" name="etablissement_sortie_id_view"
                     value="{{$affectation->_ref_etablissement_transfert}}" style="width: 12em;" />

              <script>
                Main.add(function () {
                  new Url().setRoute('{{url name=etablissement_externals_autocomplete}}')
                    .addParam('field', 'etablissement_sortie_id')
                    .addParam('input_field', 'etablissement_sortie_id_view')
                    .addParam('view_field', 'nom')
                    .autoComplete(getForm('editAffect').etablissement_sortie_id_view, null, {
                      minChars: 0,
                      method: 'get',
                      select: 'view',
                      dropdown: true,
                      afterUpdateElement: function (field, selected) {
                        var id = selected.getAttribute("id").split("-")[2];
                        $V(field.form.etablissement_sortie_id, id);
                      }
                    });
                });
              </script>
            </div>
        </td>
      </tr>
      <tr>
        <th>
            {{mb_label object=$affectation field=provenance}}
        </th>
        <td>
            {{mb_field object=$affectation field=provenance emptyLabel="Choose"}}
        </td>
        <th>
            {{mb_label object=$affectation field=destination}}
        </th>
        <td>
            {{mb_field object=$affectation field=destination emptyLabel="Choose"}}
        </td>
      </tr>
    {{/if}}

    {{if !$affectation->sejour_id}}
      <tr>
        <th>
          {{mb_label object=$affectation field=rques}}
        </th>
        <td colspan="3">
          {{mb_field object=$affectation field=rques form=editAffect}}
        </td>
      </tr>
    {{/if}}

    <tr>
      <td colspan="4" class="button">
        <button type="button" class="save me-primary" onclick="this.form.onsubmit();">
          {{if $affectation->_id}}
            {{tr}}Save{{/tr}}
          {{else}}
            {{tr}}Create{{/tr}}
          {{/if}}
          {{if $affectation->_id}}
            <button type="button" class="cancel" onclick="$V(this.form.del, 1); this.form.onsubmit();">{{tr}}Delete{{/tr}}</button>
          {{/if}}
      </td>
    </tr>
  </table>
</form>
{{if $affectation->_id && $affectation->sejour_id}}
  {{mb_include module=hospi template=inc_other_actions}}
{{/if}}
