<?php

/**
 * @package Mediboard\Hospi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CAppUI;
use Ox\Core\CCanDo;
use Ox\Core\CFlotrGraph;
use Ox\Core\CMbDT;
use Ox\Core\CRequest;
use Ox\Core\CSmartyDP;
use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Hospi\CLit;
use Ox\Mediboard\Hospi\CService;

CCanDo::checkRead();
$date_min     = CView::get("date_min", "date default|" . CMbDT::date("-1 month"), true);
$date_max     = CView::get("date_max", "date default|" . CMbDT::date(), true);
$service_id   = CView::get("service_id", "ref class|CService", true);
$spec         = [
    "str",
    "default" => ["ouvert" => 1, "prevu" => 1, "affecte" => 1, "entree" => 1],
];
$display_stat = CView::get("display_stat", $spec, true);
CView::checkin();

$group              = CGroups::loadCurrent();
$service            = new CService();
$where              = [];
$where["group_id"]  = "= '$group->_id'";
$where["cancelled"] = "= '0'";
$order              = "nom";
$services           = $service->loadListWithPerms(PERM_READ, $where, $order);

// Template avec �chec
$smarty = new CSmartyDP();
$smarty->assign("date_min", $date_min);
$smarty->assign("date_max", $date_max);
$smarty->assign("type", "occupation");
$smarty->assign("service_id", $service_id);
$smarty->assign("services", $services);

if (!$service_id) {
    $smarty->display("inc_form_stats.tpl");
    CAppUI::stepMessage(UI_MSG_ALERT, "warning-hospi-stats-choose_service");

    return;
}

$ds        = CSQLDataSource::get("std");
$dates     = [];
$date_temp = $date_min;
$series    = [];

while ($date_temp <= $date_max) {
    $dates[]   = [count($dates), CMbDT::dateToLocale($date_temp)];
    $date_temp = CMbDT::date("+1 day", $date_temp);
}

// Nombre de lits totaux sur le service
$lit = new CLit();

$where = [];
$ljoin = [];

$ljoin["chambre"]        = "chambre.chambre_id = lit.chambre_id";
$where["service_id"]     = " = '$service_id'";
$where["lit.annule"]     = " = '0'";
$where["chambre.annule"] = " = '0'";

$nb_lits = $lit->countList($where, null, $ljoin);
if (!$nb_lits) {
    $smarty->display("inc_form_stats");
    CAppUI::stepMessage(UI_MSG_WARNING, "warning-hospi-stats-no_beds");

    return;
}

// Lits ouverts (non bloqu�s - non compris les blocages des urgence)
$serie = [
    "data"    => [],
    "label"   => "Disponibles",
    "markers" => ["show" => true],
];

// Sauvegarde des lits ouverts par date
$lits_ouverts_par_date = [];

foreach ($dates as $key => $_date) {
    $date = CMbDT::dateFromLocale($_date[1]);

    $request = new CRequest();
    $request->addSelect("COUNT(DISTINCT lit.lit_id) as lits_ouverts");
    $request->addTable('lit');
    $request->addRJoin(
        [
            'affectation' => "affectation.lit_id = lit.lit_id
                AND affectation.entree < '$date 23:59:59'
                AND affectation.sortie > '$date 00:00:00'
                AND affectation.sejour_id IS NOT NULL",
            'chambre'     => 'chambre.chambre_id = lit.chambre_id',
        ]
    );
    $request->addWhere(['chambre.service_id' => $ds->prepare('= ?', $service_id)]);

    $lits_pris = $ds->loadResult($request->makeSelect());

    $serie['data'][]              =
        [
            count($serie['data'])/* - 0.3*/,
            $nb_lits - $lits_pris,
            ($nb_lits - $lits_pris) / $nb_lits,
        ];
    $lits_ouverts_par_date[$date] = $nb_lits - $lits_pris;
}

// Pour les autres stats, on a besoin du nombre de lits ouverts,
// donc la calculer dans tous les cas

if (isset($display_stat["ouvert"])) {
    $series[] = $serie;
}

// Pr�vu (s�jours)
// WHERE s.service_id = '$service_id' => le service_id est pas notNull (en config)

if (isset($display_stat["prevu"])) {
    $serie = [
        "data"    => [],
        "label"   => "Pr�vu",
        "markers" => ["show" => true],
    ];

    foreach ($dates as $key => $_date) {
        $date = CMbDT::dateFromLocale($_date[1]);

        $request = new CRequest();
        $request->addSelect('COUNT(sejour_id) as nb_prevu');
        $request->addTable('sejour');
        $request->addWhere(
            [
                'entree_prevue' => $ds->prepare('<= ?', "$date 00:00:00"),
                'sortie_prevue' => $ds->prepare('>= ?', "$date 00:00:00"),
                'service_id'    => $ds->prepare('= ?', $service_id)
            ]
        );

        $prevu_count = $ds->loadResult($request->makeSelect());

        $serie["data"][] =
            [
                count($serie["data"])/* - 0.1*/,
                $prevu_count,
                $prevu_count / $nb_lits,
            ];
    }

    $series[] = $serie;
}

// R�el (affectations)
if (isset($display_stat["affecte"])) {
    $serie = [
        "data"    => [],
        "label"   => "Affect�s",
        "markers" => ["show" => true],
    ];

    foreach ($dates as $key => $_date) {
        $date       = CMbDT::dateFromLocale($_date[1]);
        $request = new CRequest();
        $request->addSelect('count(affectation_id) as nb_reel');
        $request->addTable('affectation');
        $request->addWhere(
            [
                "entree < '$date 00:00:00' AND sortie >= '$date 00:00:00'",
                'affectation.service_id' => $ds->prepare('= ?', $service_id)
            ]
        );

        $reel_count = $ds->loadResult($request->makeSelect());

        $serie["data"][] =
            [
                count($serie['data'])/* + 0.1*/,
                $reel_count,
                $reel_count / $nb_lits,
            ];
    }

    $series[] = $serie;
}

// Entr�es dans la journ�e (nb de placements sur tous les lits sur chaque journ�e)
// Ne pas compter les blocages

if (isset($display_stat["entree"])) {
    $serie = [
        "data"    => [],
        "label"   => "Entr�es",
        "markers" => ["show" => true],
    ];

    foreach ($dates as $key => $_date) {
        $date = CMbDT::dateFromLocale($_date[1]);

        $request = new CRequest();
        $request->addSelect('COUNT(affectation_id) as entrees');
        $request->addTable('affectation');
        $request->addWhere(
            [
                "affectation.entree <= '$date 23:59:59' AND affectation.sortie >= '$date 00:00:00'",
                'affectation.sejour_id'  => 'IS NOT NULL',
                'affectation.service_id' => $ds->prepare('= ?', $service_id),
            ]
        );

        $entrees_journee = $ds->loadHash($request->makeSelect());

        $serie["data"][] =
            [
                count($serie['data'])/* + 0.3*/,
                $entrees_journee["entrees"],
                $entrees_journee["entrees"] / $nb_lits,
            ];
    }

    $series[] = $serie;
}

$options = CFlotrGraph::merge("bars", [
    "title" => "Occupation des lits",
    "xaxis" => ["ticks" => $dates],
    "yaxis" => ["min" => 0],
    "grid"  => ["verticalLines" => true],
    "bars"  => ["barWidth" => 0.15, "stacked" => false],
]);

$graph = ["series" => $series, "options" => $options];

$smarty = new CSmartyDP();
$smarty->assign("date_min", $date_min);
$smarty->assign("date_max", $date_max);
$smarty->assign("services", $services);
$smarty->assign("graph", $graph);
$smarty->assign("service_id", $service_id);
$smarty->assign("display_stat", $display_stat);
$smarty->display("inc_vw_stats_occupation");
