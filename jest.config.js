const buildModules = require("./javascript/config/module-generation")

// Module generation
buildModules()

module.exports = {
    workerIdleMemoryLimit: '2048MB',
    testEnvironmentOptions: {
        // https://github.com/facebook/jest/issues/6766
        url: 'http://localhost/'
    },
    testEnvironment: 'jsdom',
    moduleFileExtensions: [
        'js',
        'json',
        // tell Jest to handle *.vue files
        'vue',
        'ts'
    ],
    transform: {
        // process *.vue files with vue-jest
        '^.+\\.vue$': "@vue/vue2-jest",
        '.+\\.(css|styl|less|sass|scss|jpg|jpeg|png|svg|gif|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga|avif)$':
                      "jest-transform-stub",
        '^.+\\.jsx?$': "babel-jest",
        '^.+\\.tsx?$': "ts-jest"
    },
    testMatch: ["<rootDir>/javascript/src/tests/**/*.test.ts", "<rootDir>/modules/*/vue/tests/**/*.test.ts"],
    reporters: ["default", "jest-junit"],
    testResultsProcessor: "jest-junit",
    setupFiles: ["./javascript/config/jest-prestart.js"],
    moduleNameMapper: {
        "^@\/(.*)$": "<rootDir>/javascript/src/$1",
        "^@oxify\/(.*)$": "<rootDir>/javascript/src/core/oxify/$1",
        "^@modules\/(.*)$": "<rootDir>/modules/$1",
        "vuetify/lib(.*)": "<rootDir>/node_modules/vuetify/es5$1"
    },
    transformIgnorePatterns: [
        "<rootDir>/node_modules/(?!(vuetify|axios)/)"
    ],
    // serializer for snapshots
    snapshotSerializers: [
        'jest-serializer-vue'
    ],
    watchPlugins: [
        require.resolve('jest-watch-typeahead/filename'),
        require.resolve('jest-watch-typeahead/testname')
    ],
    cacheDirectory: "<rootDir>/tmp/jest_cache",
    coverageReporters: ["html", "json-summary", "text"],
    collectCoverageFrom: [
        "<rootDir>/javascript/src/components/**/*.{vue,ts}",
        "<rootDir>/javascript/src/core/**/*.{vue,ts}",
        "<rootDir>/modules/*/vue/**/*.{vue,ts}",
        "!<rootDir>/javascript/src/core/types/**",
        "!<rootDir>/javascript/src/core/oxify/types/**",
        "!<rootDir>/javascript/src/components/Appbar/types/**",
        "!<rootDir>/javascript/src/tests/mocks/**",
        "!<rootDir>/modules/*/vue/types/**",
        "!<rootDir>/modules/oxCabinet/vue/TammDhe/**",
        "!<rootDir>/modules/*/vue/tests/mocks/**",
    ],
    coverageProvider: "v8"
}
