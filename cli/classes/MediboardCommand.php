<?php
/**
 * @package Mediboard\Cli
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli;

use Ox\Core\CMbDT;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Mediboard command executer
 */
class MediboardCommand extends Command
{
    /**
     * @see parent::initialize()
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $style = new OutputFormatterStyle('blue', null, ['bold']);
        $output->getFormatter()->setStyle('b', $style);

        $style = new OutputFormatterStyle(null, 'red', ['bold']);
        $output->getFormatter()->setStyle('error', $style);
    }

    /**
     * Output timed text
     *
     * @param OutputInterface $output Output interface
     * @param string          $text   Text to print
     *
     * @return void
     */
    protected function out(OutputInterface $output, string $text): void
    {
        $output->writeln(CMbDT::strftime("[%Y-%m-%d %H:%M:%S]") . " - $text");
    }

    /**

     * Get configuration value from a path and a tree of configuration values
     *
     * @param string $path Config path
     * @param array  $conf Config tree
     *
     * @return null|string|array
     */
    public function getConf(string $path, array $conf)
    {
        if (!$path) {
            return $conf;
        }

        $items = explode(' ', $path);
        foreach ($items as $part) {
            // dP ugly hack
            if (!array_key_exists($part, $conf) && array_key_exists("dP$part", $conf)) {
                $part = "dP$part";
            }

            if (!@$conf[$part]) {
                return null;
            }

            $conf = $conf[$part];
        }

        return $conf;
    }
}
