<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Console;

use Exception;
use Ox\Cli\CommandLinePDO;
use PDO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\RuntimeException;
use Symfony\Component\Process\Process;

/**
 * This command allows to convert MySQL tables engine from MyISAM to InnoDB
 */
class DBEngineConvert extends Command
{
    final protected const SQL_FILE = 'tmp/db_engine_convert.sql';
    private const SOURCE_ENGINE = 'MyISAM';
    private const TARGET_ENGINE = 'InnoDB';

    protected OutputInterface $output;
    protected InputInterface $input;
    protected SymfonyStyle $io;
    protected string $path;
    protected CommandLinePDO $pdo;
    protected array $params;
    protected array $tables;
    protected int $conversions = 0;
    protected array $errors;

    protected bool $dry_run = false;
    protected string $db_host;
    protected string $db_user;
    protected string $db_pass;
    protected string $db_name;
    protected string $db_name_copy;

    protected QuestionHelper $question_helper;

    /**
     * @see parent::configure()
     */
    protected function configure(): void
    {
        $this
            ->setName('ox-db:engine-convert')
            ->setDescription('Converts database tables to an other engine')
            ->addOption(
                'interactive',
                'i',
                InputOption::VALUE_NONE,
                'Use interactive mode to manually enter parameters'
            )
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Simulates the conversion without execution')
            ->addOption('db_host', null, InputOption::VALUE_REQUIRED, 'The db host name', 'db')
            ->addOption('db_user', null, InputOption::VALUE_REQUIRED, 'The db username', 'dev')
            ->addOption('db_pass', null, InputOption::VALUE_REQUIRED, 'The db password', false)
            ->addOption('db_name', null, InputOption::VALUE_REQUIRED, 'The db name', 'mediboard');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception|InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output          = $output;
        $this->input           = $input;
        $this->io              = new SymfonyStyle($this->input, $this->output);
        $this->question_helper = $this->getHelper('question');

        $this->setParameters();
        $this->setUp();

        $this->findTables();

        if (empty($this->tables)) {
            $this->io->success('No tables that need engine conversion were found.');
            return self::SUCCESS;
        } else {
            $this->io->info(count($this->tables) . ' tables using engine "' . self::SOURCE_ENGINE . '" were found.');
        }

        $this->copyDatabase();
        $this->convertTables();

        if (!empty($this->errors)) {
            $table = $this->io->createTable()->setHeaders(['Table', 'Error']);
            foreach ($this->errors as $name => $error) {
                $table->addRow(
                    [
                        $name,
                        substr($error, 0, 50) . '...'
                    ]
                );
            }
            $table->render();

            $this->io->error(count($this->errors) . ' tables engine conversion failed !');

            return self::FAILURE;
        }

        $this->io->success($this->conversions . ' tables had their engine successfully converted.');

        return self::SUCCESS;
    }

    /**
     * @return void
     * @throws Exception
     */
    private function setUp(): void
    {
        try {
            $this->pdo = new CommandLinePDO($this->db_host, $this->db_user, $this->db_pass);
        } catch (Exception $e) {
            if ($this->output->getVerbosity() === OutputInterface::VERBOSITY_VERBOSE) {
                throw $e;
            }
            throw new LogicException(
                "Unable to connect to mysql host '{$this->db_host}' with user '{$this->db_user}'."
            );
        }

        $this->io->info('Connected to database host.');
    }

    /**
     * @return void
     * @throws Exception
     */
    private function connectDatabase(): void
    {
        try {
            $this->pdo = new CommandLinePDO($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        } catch (Exception $e) {
            if ($this->output->getVerbosity() === OutputInterface::VERBOSITY_VERBOSE) {
                throw $e;
            }
            throw new LogicException(
                "Unable to connect to database '{$this->db_name}' on mysql:host={$this->db_host}."
            );
        }
    }

    /**
     * @throws Exception
     */
    private function copyDatabase(): void
    {
        if (true === $this->dry_run) {
            return;
        }

        $this->connectDatabase();

        if ($this->pdo->isDatabaseExists($this->db_name_copy)) {
            if (!$this->pdo->dropDatabase($this->db_name_copy)) {
                throw new LogicException("Unable to drop database {$this->db_name_copy}.");
            }
        }

        $this->io->writeln('Creating database copy for conversion...');

        if (!$this->pdo->createDatabase($this->db_name_copy)) {
            throw new LogicException("Unable to create database {$this->db_name_copy}.");
        }

        $ps = Process::fromShellCommandline(
            'mysqldump' .
            ' --single-transaction ' .
            ' -h ' .
            $this->db_host .
            ' -u ' .
            $this->db_user .
            ' -p' . $this->db_pass . ' ' .
            $this->db_name . ' > ' . self::SQL_FILE
        );
        $ps->setTimeout(3600);
        $ps->run();
        if (!$ps->isSuccessful()) {
            throw new RuntimeException('Dumping database to SQL file has failed.');
        }

        $ps = Process::fromShellCommandline(
            'mysql' .
            ' -h ' .
            $this->db_host .
            ' -u ' .
            $this->db_user .
            ' -p' . $this->db_pass . ' ' .
            $this->db_name_copy . ' < ' . self::SQL_FILE
        );
        $ps->setTimeout(3600);
        $ps->run();
        if (!$ps->isSuccessful()) {
            throw new RuntimeException('Importing database copy from SQL file has failed.');
        }

        $this->io->info('Database "' . $this->db_name_copy . '" successfully copied !');

        (new Filesystem())->remove(self::SQL_FILE);
    }

    /**
     * @throws Exception
     */
    private function findTables(): void
    {
        $this->connectDatabase();

        $statement = $this->pdo->prepare(
            "SELECT TABLE_NAME FROM information_schema.TABLES where TABLE_SCHEMA = :schema and ENGINE = :engine"
        );
        $statement->bindValue(':schema', $this->db_name);
        $statement->bindValue(':engine', self::SOURCE_ENGINE);

        if (!$statement->execute()) {
            throw new Exception($statement->errorInfo()[2]);
        }

        $this->tables = $statement->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * @throws Exception
     */
    private function convertTables(): void
    {
        if (true === $this->dry_run) {
            return;
        }

        $this->io->writeln('Converting tables on database "' . $this->db_name_copy . '"...');

        try {
            $this->pdo = new CommandLinePDO($this->db_host, $this->db_user, $this->db_pass, $this->db_name_copy);
        } catch (Exception $e) {
            if ($this->output->getVerbosity() === OutputInterface::VERBOSITY_VERBOSE) {
                throw $e;
            }
            throw new LogicException(
                "Unable to connect to database '{$this->db_name_copy}' on mysql:host={$this->db_host}."
            );
        }

        $this->io->progressStart(count($this->tables));
        foreach ($this->tables as $table) {
            $statement = $this->pdo->prepare(
                'ALTER TABLE ' . $table . ' ENGINE ' . self::TARGET_ENGINE
            );
            try {
                $statement->execute();
                $this->conversions++;
            } catch (Exception $e) {
                $this->errors[$table] = $statement->errorInfo()[2];
            }
            $this->io->progressAdvance();
        }
        $this->io->progressFinish();
    }

    /**
     * @throws Exception
     */
    private function setParameters(): void
    {
        $this->db_host = $this->input->getOption('db_host');
        $this->db_name = $this->input->getOption('db_name');
        $this->db_user = $this->input->getOption('db_user');
        $this->db_pass = $this->input->getOption('db_pass');
        $this->dry_run = $this->input->getOption('dry-run');

        if ($this->input->getOption('interactive')) {
            $this->db_host = $this->ask(
                new Question(
                    'Please enter the host name (default: ' . $this->db_host . ')',
                    $this->db_host
                )
            );
            $this->db_name = $this->ask(
                new Question(
                    'Enter the database name (default: ' . $this->db_name . ')',
                    $this->db_name
                )
            );
            $this->db_user = $this->ask(
                new Question(
                    'Enter the user name (default: ' . $this->db_user . ')',
                    $this->db_user
                )
            );
            $this->db_pass = $this->ask(
                new Question('Enter the user password -> ', ''),
                true
            );
        }

        if (empty($this->db_pass)) {
            $this->io->warning('You have set an empty password');
        }

        if (!empty($this->db_name)) {
            $this->db_name_copy = $this->db_name . '_innodb';
        }
    }

    /**
     * @param Question $question
     * @param bool     $hidden
     *
     * @return false|mixed|string|null
     */
    private function ask(Question $question, bool $hidden = false)
    {
        if ($hidden) {
            $question->setHidden(true);
        }

        return $this->question_helper->ask($this->input, $this->output, $question);
    }
}
