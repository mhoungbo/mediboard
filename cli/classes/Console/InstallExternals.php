<?php

/**
 * @package Mediboard\Cli
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Console;

use Exception;
use Ox\Cli\CommandLinePDO;
use Ox\Core\Cache;
use Ox\Core\CClassMap;
use Ox\Core\Import\ExternalDataSourceImport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class InstallExternals
 *
 * @package Ox\Cli\Console
 */
class InstallExternals extends Command
{
    final protected const CONFIG_FILE = 'includes/config.php';

    protected OutputInterface $output;
    protected InputInterface $input;
    protected SymfonyStyle $io;
    protected Filesystem $fs;
    private Stopwatch $stopwatch;
    protected string $path;
    protected array $params = [];
    protected array $names = [];
    protected bool $delete;
    protected CommandLinePDO $pdo;
    protected array $created = [];
    protected array $durations;
    private ?array $config = null;

    /** @var ExternalDataSourceImport[] */
    protected array $imports;

    /**
     * @see parent::configure()
     */
    protected function configure(): void
    {
        $this
            ->setName('ox-install:externals')
            ->setDescription('Install external databases with data import')
            ->addArgument(
                'names',
                InputArgument::IS_ARRAY,
                'Which database do you want to create (separate multiple names with a space) ?'
            )
            ->addOption(
                'path',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Working copy root',
                dirname(__DIR__, 3) . '/'
            )->addOption(
                'import',
                'i',
                InputOption::VALUE_NONE,
                'Import data',
            )->addOption(
                'delete',
                'd',
                InputOption::VALUE_NONE,
                'Delete existing databases',
            );
    }

    /**
     * @see parent::showHeader()
     */
    protected function showHeader(): void
    {
        $this->io->title("External databases installation");
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception|InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output    = $output;
        $this->input     = $input;
        $this->io        = new SymfonyStyle($this->input, $this->output);
        $this->fs        = new Filesystem();
        $this->stopwatch = new Stopwatch(true);
        $this->names     = $this->input->getArgument('names');
        $this->delete    = $this->input->getOption('delete');
        $this->path      = $this->input->getOption('path');

        $this->showHeader();

        $this->loadConfigurationValues();

        Cache::init($this->path);

        $this->loadImports();

        $this->importAllData();

        if ($this->io->isVerbose()) {
            $this->displayImportDurations();
        }
        $this->io->success(count($this->created) . ' External databases data imported.');

        return self::SUCCESS;
    }

    private function loadConfigurationValues(): void
    {
        if ($this->fs->exists($this->path . self::CONFIG_FILE)) {
            global $dPconfig;
            require $this->path . self::CONFIG_FILE;
            $this->config = $dPconfig;
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    private function loadImports(): void
    {
        $imports = CClassMap::getInstance()->getClassChildren(ExternalDataSourceImport::class, false, true);

        /** @var string $importClass */
        foreach ($imports as $importClass) {
            $this->imports[] = new $importClass();
        }
    }

    /**
     * Create pdo & check connexion
     *
     * @param ExternalDataSourceImport $import
     *
     * @return self
     * @throws Exception
     */
    private function checkConnexion(ExternalDataSourceImport $import): self
    {
        $host     = $this->getDatabaseConfig($import, 'dbhost');
        $user     = $this->getDatabaseConfig($import, 'dbuser');
        $password = $this->getDatabaseConfig($import, 'dbpass');

        try {
            $this->pdo = new CommandLinePDO($host, $user, $password);
        } catch (Exception $e) {
            if ($this->io->isVerbose()) {
                throw $e;
            }
            throw new LogicException("Unable to connect to host: {$host} !");
        }

        return $this;
    }

    /**
     * @param ExternalDataSourceImport $import
     *
     * @return void
     * @throws Exception
     * @throws LogicException
     */
    private function dropDatabase(ExternalDataSourceImport $import): void
    {
        $this->checkConnexion($import);
        $name = $import->getSourceNameForSQL();

        if (!empty($this->names) && !in_array($name, $this->names)) {
            return;
        }

        if ($this->pdo->isDatabaseExists($name)) {
            if (!$this->pdo->dropDatabase($name)) {
                throw new LogicException("Unable to drop external database {$name}.");
            } elseif ($this->io->isDebug()) {
                $this->io->info("External database {$name} dropped.");
            }
        }
    }

    /**
     * @param ExternalDataSourceImport $import
     * @param bool                     $delete
     *
     * @return void
     * @throws Exception|LogicException
     */
    private function createDatabase(ExternalDataSourceImport $import, bool $delete = false): void
    {
        $name = $this->getDatabaseConfig($import, 'dbname');
        if (!empty($this->names) && !in_array($name, $this->names)) {
            return;
        }

        if (true === $delete) {
            $this->dropDatabase($import);
        }

        $this->checkConnexion($import);

        if (!$this->pdo->isDatabaseExists($name)) {
            if (!$this->pdo->createDatabase($name)) {
                throw new LogicException("Unable to create external database {$name}.");
            } else {
                $this->created[] = $name;
                if ($this->io->isDebug()) {
                    $this->io->info("External database {$name} created.");
                }
            }
        } elseif ($this->io->isVerbose()) {
            $this->io->info("External database {$name} already exists.");
        }
    }

    /**
     * @return self
     * @throws Exception
     */
    private function importAllData(): self
    {
        $this->io->progressStart(count($this->created));

        foreach ($this->imports as $import) {
            $databaseName = $this->getDatabaseConfig($import, 'dbname');
            if (!empty($this->names) && !in_array($databaseName, $this->names)) {
                continue;
            }

            if ($this->delete) {
                $this->dropDatabase($import);
                $this->io->success("External database named : '" . $databaseName . " deleted.");
            }

            $this->createDatabase($import);

            if (!in_array($databaseName, $this->created)) {
                continue;
            }

            $name = get_class($import);
            $this->stopwatch->start($name);

            try {
                if ($this->io->isVerbose()) {
                    $this->io->info("Importing database named : '" . $databaseName . "'...");
                }
                $importResult = $import->import();
            } catch (Exception $e) {
                $this->io->error($e->getMessage());
                $importResult = false;
                $this->dropDatabase($import);
            }

            if ($this->io->isVerbose()) {
                $this->io->createTable()->setHeaderTitle('Messages for ' . $name)
                         ->setRows($import->getMessages())
                         ->render();
                if ($importResult) {
                    $this->io->success($name);
                } else {
                    $this->io->error($name);
                }
            }

            $this->durations[$name] = $this->stopwatch->stop($name)->getDuration();

            $this->io->progressAdvance();
        }

        $this->io->progressFinish();

        return $this;
    }

    private function displayImportDurations(): void
    {
        if (empty($this->durations)) {
            return;
        }

        $durations = $this->durations;
        arsort($durations);

        $table = $this->io->createTable()->setHeaderTitle('Durations')->setHeaders(['name', 'duration (ms)']);

        foreach ($durations as $name => $duration) {
            $table->addRow([$name, $duration]);
        }
        $table->render();
    }

    /**
     * @throws Exception
     */
    private function getDatabaseConfig(ExternalDataSourceImport $import, string $name): string
    {
        $conf = $this->config;
        $path = 'db ' . $import->getSourceName() . ' ' . $name;

        try {
            $items = explode(' ', $path);
            foreach ($items as $part) {
                $conf = $conf[$part];
            }
            return $conf;
        } catch (Exception $e) {
            $this->io->warning($e->getMessage());
            throw new Exception("Datasource '" . $name . "' is not properly configured.");
        }
    }
}
