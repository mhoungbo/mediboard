<?php

/**
 * @package Mediboard\Cli
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Console;

use CzProject\GitPhp\Commit;
use CzProject\GitPhp\GitException;
use CzProject\GitPhp\GitRepository;
use DateTimeInterface;
use DOMDocument;
use DOMElement;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class ReleaseMakeXML extends Command
{
    private GitRepository $wc;
    private Filesystem $fs;
    private SymfonyStyle $io;

    /**
     * @see parent::configure()
     */
    protected function configure()
    {
        $this
            ->setName('ox-release:makexml')
            ->setAliases(['ox-release:mx'])
            ->setDescription('Make release XML file')
            ->setHelp('Makes a release.xml file containing release information')
            ->addOption(
                'path',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Working copy root for which we want to build release.xml',
                realpath(__DIR__ . "/../../../")
            )->addOption(
                'output',
                'o',
                InputOption::VALUE_OPTIONAL,
                'Output location for release.xml',
                realpath(__DIR__ . "/../../../")
            );
    }

    /**
     * @throws GitException
     * @throws Exception
     * @see parent::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        $path       = $input->getOption('path');
        $outputPath = $input->getOption('output');

        $this->fs = new Filesystem();

        if (!$this->fs->exists($path)) {
            throw new InvalidArgumentException("'$path' is not a valid directory");
        }

        try {
            $this->wc = new GitRepository($path);
        } catch (GitException $e) {
            return self::FAILURE;
        }

        $current_branch = $this->getCurrentBranchName();

        $matches = [];
        if (preg_match("@^release/(.*)@", $current_branch, $matches)) {
            $current_branch = $matches[1];
        } else {
            $this->io->error("Current branch is not a release branch: " . $current_branch);
            return self::FAILURE;
        }

        $output->writeln("Current release branch: '<b>$current_branch</b>'");

        $this->fs->dumpFile(
            "$outputPath/release.xml",
            $this->getReleaseXML($current_branch)->saveXML()
        );

        $this->io->success("release.xml file written in: '$outputPath/release.xml'");

        return self::SUCCESS;
    }

    /**
     * @param string $current_branch
     *
     * @return DOMDocument
     * @throws Exception
     */
    public function getReleaseXML(string $current_branch)
    {
        $last_commit = $this->getLastCommit();

        $release_element = new DOMElement("release");

        $dom_release = new DOMDocument();
        $dom_release->appendChild($release_element);

        $release_element->setAttribute("code", $current_branch);
        $release_element->setAttribute(
            "date",
            $last_commit->getDate()->format(DateTimeInterface::ATOM)
        );
        $release_element->setAttribute("revision", $last_commit->getId());

        return $dom_release;
    }

    /**
     * @throws GitException
     */
    public function getLastCommit(): Commit
    {
        return $this->wc->getCommit($this->wc->getLastCommitId());
    }

    /**
     * @throws GitException
     */
    public function getCurrentBranchName(): string
    {
        return $this->wc->getCurrentBranchName();
    }

}
