<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Console;

use Exception;
use Ox\Core\Autoload\CAutoloadAlias;
use Ox\Core\Cache;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CClassMap;
use Ox\Core\CMbConfig;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\System\CConfiguration;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;

class AppBridge
{
    private RouterInterface $router;
    private LoggerInterface $logger;

    public function __construct(RouterInterface $router, LoggerInterface $logger)
    {
        $this->router = $router;
        $this->logger = $logger;
    }

    public function start()
    {
        // Init
        $config = CApp::includeConfigs(dirname(__DIR__, 3));
        Cache::init(CApp::getAppIdentifier());
        CAppUI::init();
        CClassMap::init();
        CAutoloadAlias::register();
        CModule::loadModules();

        // Timezone
        date_default_timezone_set($config['timezone']);

        // Include config in DB
        CMbConfig::loadValuesFromDB();

        // Init shared memory, must be after DB init and Modules loading
        Cache::initDistributed();

        // Register configuration
        CConfiguration::registerAllConfiguration();

        $app = CApp::getInstance();
        $app->setLogger($this->logger);
        $app->setRouter($this->router);
        $app->startChrono();
    }

    public function authAdmin()
    {
        $user                = new CUser();
        $user->user_username = CUser::USER_ADMIN;
        $user->loadMatchingObjectEsc();
        $this->authUser($user);
    }

    public function authPhpUnit()
    {
        $user                = new CUser();
        $user->user_username = CUser::USER_PHPUNIT;
        $user->loadMatchingObjectEsc();
        $this->authUser($user);
    }

    private function authUser(Cuser $user)
    {
        if (!$user->user_id) {
            throw new Exception(sprintf('Missing %s', CUser::USER_ADMIN));
        }

        $mediuser = $user->loadRefMediuser();

        // CAppUI
        CAppUI::initInstance();
        CAppUI::$instance->_ref_user = $mediuser;
        CAppUI::$instance->user_id   = $user->_id;

        // Perm
        CPermModule::loadUserPerms();
        CPermObject::loadUserPerms();
    }
}
