<?php

/**
 * @package Mediboard\Cli
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Console;

use Exception;
use Ox\Core\CMbConfig;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Core\Security\Random\Random;
use Ox\Mediboard\System\CConfiguration;
use Ox\Mediboard\System\Keys\CKeyMetadata;
use Ox\Mediboard\System\Keys\KeyBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class EncryptionKeyBuild
 *
 * @package Ox\Cli\Console
 */
class EncryptionKeyBuild extends Command
{
    protected OutputInterface $output;
    protected InputInterface  $input;
    protected SymfonyStyle    $io;
    protected CMbConfig       $config;

    private KeyBuilder $builder;
    private Filesystem $fs;
    private AppBridge $app;

    protected const STORAGE_DIRECTORY_PATH = 'tmp/metadata_keys';

    public function __construct(AppBridge $app)
    {
        parent::__construct();

        $this->app = $app;
    }

    protected function configure(): void
    {
        $this
            ->setName('ox-encrypt:build-keys')
            ->setDescription('Generate encryption keys and configuration then store keys');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;
        $this->input  = $input;
        $this->io     = new SymfonyStyle($this->input, $this->output);

        $this->app->start();
        // Must be admin to manage Key metadata
        $this->app->authAdmin();

        $this->builder = new KeyBuilder(new Hasher(), new Random());
        $this->fs      = new Filesystem();

        $this->setStorageDirectoryConfiguration();

        $keys                 = $this->findKeysToGenerate();
        $generated_keys_count = 0;

        foreach ($keys as $key) {
            $this->builder->generateKey($key);
            $generated_keys_count++;
        }

        $this->io->success($generated_keys_count . ' keys have been generated');

        return self::SUCCESS;
    }

    /**
     * @return CKeyMetadata[]
     * @throws Exception
     */
    private function findKeysToGenerate(): array
    {
        $key_metadata = new CKeyMetadata();

        return $key_metadata->loadList(
            [
                'last_update' => "IS NULL",
            ]
        );
    }

    /**
     * @throws Exception
     */
    private function setStorageDirectoryConfiguration(): void
    {
        $config          = new CConfiguration();
        $config->feature = "system KeyChain directory_path";
        $config->static  = 1;

        $config->loadMatchingObject();
        if (
            $config->_id &&
            (
                ($config->value !== self::STORAGE_DIRECTORY_PATH) ||
                ($config->alt_value !== self::STORAGE_DIRECTORY_PATH)
            )
        ) {
            throw new Exception(
                'Configuration for metadata keys storage directory has already been set and must not be altered'
            );
        }

        $config->value = $config->alt_value = self::STORAGE_DIRECTORY_PATH;
        if ($msg = $config->store()) {
            throw new Exception($msg);
        }

        if (!$this->fs->exists(self::STORAGE_DIRECTORY_PATH)) {
            $this->fs->mkdir(self::STORAGE_DIRECTORY_PATH);
        }
    }
}
