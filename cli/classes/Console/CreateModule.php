<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Console;

use Exception;
use Ox\Cli\CommandLinePDO;
use Ox\Core\CMbException;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Developpement\ModuleBuilder;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateModule extends Command
{

    protected OutputInterface $output;

    protected InputInterface $input;

    protected SymfonyStyle $io;

    protected QuestionHelper $question_helper;

    protected CommandLinePDO $pdo;

    protected string $path;

    private array $params = [];

    private AppBridge $app;

    public function __construct(AppBridge $app)
    {
        parent::__construct();

        $this->app = $app;
    }

    /**
     * @see parent::configure()
     */
    protected function configure(): void
    {
        $this
            ->setName('ox-module:create')
            ->setDescription('Create a Mediboard module')
            ->addOption(
                'path',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Working copy root',
                dirname(__DIR__, 3)
            );
    }

    protected function showHeader(): void
    {
        $this->output->writeln('<fg=yellow;bg=black>OX Mediboard module creation</fg=yellow;bg=black>');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws CMbException
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output          = $output;
        $this->input           = $input;
        $this->io              = new SymfonyStyle($this->input, $this->output);
        $this->question_helper = $this->getHelper('question');
        $this->path            = $input->getOption('path');

        $this->app->start();

        $this->showHeader();

        if (!$this->askQuestions()) {
            return self::FAILURE;
        }

        $create_module = $this
            ->createModule();

        if (!$create_module) {
            throw new LogicException('Module creation failed');
        }

        $this->io->success('Module successfully created !');

        return self::SUCCESS;
    }

    /**
     * @throws CMbException
     */
    private function createModule(): bool
    {
        $this->params['namespace'] = str_replace(
            '\\\\',
            '\\',
            "Ox\\{$this->params['namespace_category']}\\" . ucfirst(
                $this->params['namespace']
            )
        );

        $builder = new ModuleBuilder(
            $this->path,
            $this->params['canonical_name'],
            $this->params['namespace'],
            $this->params['short_name'],
            $this->params['long_name'],
            $this->params['licence'],
            $this->params['trigram'],
            $this->params['package'],
            $this->params['category'],
            $this->params['namespace_category']
        );

        $builder->build();

        return $builder->checkModuleDir();
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function askQuestions()
    {
        $ns_category = $this->ask(
            'namespace_category',
            new ChoiceQuestion(
                'Please select the namespace category : ',
                CModule::MOD_NS_CATEGORIES
            )
        );

        $this->ask(
            'namespace',
            new Question(
                'Enter the namespace : ' . "Ox\\{$ns_category}\\",
            )
        );

        $question = new Question('Enter the canonical name : ');
        $question->setValidator(function ($answer) {
            if (!preg_match('/^[a-zA-Z0-9_]+$/i', $answer)) {
                throw new RuntimeException(
                    'The canonical name must respect pattern [a-zA-Z0-9_]'
                );
            }

            return $answer;
        });
        $question->setMaxAttempts(2);
        $this->ask('canonical_name', $question);

        $this->ask('short_name', new Question('Enter the short name : '));
        $this->ask('long_name', new Question('Enter the long name : '));
        $this->ask('trigram', new Question('Enter the trigram : '));

        $this->ask(
            'category',
            new ChoiceQuestion(
                'Select the category : ',
                CModule::MOD_CATEGORIES,
            )
        );

        $this->ask(
            'package',
            new ChoiceQuestion(
                'Select the package : ',
                CModule::MOD_PACKAGES,
            )
        );

        $this->ask(
            'licence',
            new ChoiceQuestion(
                'Select the licence type : ',
                array_keys(CModule::MOD_LICENCES),
            )
        );

        /* Display configurations before confirming */
        $params_resume = [];
        foreach ($this->params as $key => $value) {
            $params_resume[] = [$key, $value];
        }

        $this->io->createTable()
                 ->setHeaderTitle('Modules parameters')
                 ->setHeaders(['param', 'value'])
                 ->setRows($params_resume)
                 ->render();

        return $this->question_helper->ask(
            $this->input,
            $this->output,
            new ConfirmationQuestion('Do you confirm this settings [Y/n] ?', true)
        );
    }

    /**
     * @param string   $name The param name
     * @param Question $question
     *
     * @return string|null
     */
    private function ask(string $name, Question $question): ?string
    {
        return $this->params[$name] = $this->question_helper->ask($this->input, $this->output, $question);
    }
}
