<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Exception;
use Ox\Cli\Console\DBMySQLBackup;
use Ox\Core\CAppUI;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 * @runTestsInSeparateProcesses
 */
class DBMySQLBackupTest extends OxUnitTestCase
{
    /**
     * @return Command
     */
    private function makeCommand(): Command
    {
        $application = new Application();
        $application->add(new DBMySQLBackup());

        return $application->find('db:mysqlbackup');
    }

    /**
     * @throws Exception
     */
    public function testNonInteractiveExecution(): void
    {
        $commandTester = new CommandTester($this->makeCommand());
        $commandTester->execute(
            [
                '-s' => CAppUI::conf('db std dbhost'),
                '-u' => CAppUI::conf('db std dbuser'),
                '-p' => CAppUI::conf('db std dbpass'),
                '-d' => CAppUI::conf('db std dbname'),
                '-o' => true,
            ]
        );

        /**
         * Running the command this way is successful but no backup has in fact been done...
         */
        $commandTester->assertCommandIsSuccessful();

        $this->assertStringContainsString('Successful backup', $commandTester->getDisplay());
    }
}
