<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Exception;
use Ox\Core\CAppUI;
use Ox\Cli\Console\DBEngineConvert;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 * @runTestsInSeparateProcesses
 */
class DBEngineConvertTest extends OxUnitTestCase
{
    private string $host;
    private string $user;
    private string $db;
    private string $pwd;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->host = CAppUI::conf('db std dbhost');
        $this->user = CAppUI::conf('db std dbuser');
        $this->db   = CAppUI::conf('db std dbname');
        $this->pwd  = CAppUI::conf('db std dbpass');
    }

    /**
     * @return Command
     */
    private function makeCommand(): Command
    {
        $application = new Application();
        $application->add(new DBEngineConvert());

        return $application->find('ox-db:engine-convert');
    }

    /**
     * Not providing password ends up in a refused connection
     *
     * @throws Exception
     */
    public function testConnectionRefused(): void
    {
        $this->expectExceptionMessageMatches(
            '/^Unable to connect to mysql host \''
            . $this->host . '\' with user \'' . $this->user . '\'\.$/'
        );

        $command = $this->makeCommand();
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--db_host' => $this->host,
                '--db_user' => $this->user,
            ]
        );

        $commandOutput = $commandTester->getDisplay();

        $this->assertStringContainsString('You have set an empty password', $commandOutput);
    }

    /**
     * @throws Exception
     */
    public function testInteractiveMode(): void
    {
        $this->expectExceptionMessageMatches(
            '/^Unable to connect to mysql host \''
            . $this->host . '\' with user \'' . $this->user . '\'\.$/'
        );

        $command = $this->makeCommand();
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(
            [
                $this->host,
                $this->user,
            ]
        );
        $commandTester->execute(['-i' => true]);

        $commandOutput = $commandTester->getDisplay();
        $this->assertStringContainsString('You have set an empty password', $commandOutput);
    }

    /**
     * @throws Exception
     */
    public function testDatabaseNotFound(): void
    {
        $name = 'not_found';
        $this->expectExceptionMessageMatches(
            '/^Unable to connect to database \''
            . $name . '\' on mysql:host=' . $this->host . '\.$/'
        );

        $command = $this->makeCommand();
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--db_host' => $this->host,
                '--db_user' => $this->user,
                '--db_pass' => $this->pwd,
                '--db_name' => $name,
            ]
        );

        $commandOutput = $commandTester->getDisplay();
        $this->assertStringContainsString('Connected to database host.', $commandOutput);
    }

    /**
     * @throws Exception
     */
    public function testNoConversionNeeded(): void
    {
        $command = $this->makeCommand();
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--db_host' => CAppUI::conf('db std dbhost'),
                '--db_user' => CAppUI::conf('db std dbuser'),
                '--db_pass' => CAppUI::conf('db std dbpass'),
                '--db_name' => CAppUI::conf('db std dbname'),
            ]
        );

        $commandOutput = $commandTester->getDisplay();
        $this->assertStringContainsString('No tables that need engine conversion were found.', $commandOutput);
    }
}
