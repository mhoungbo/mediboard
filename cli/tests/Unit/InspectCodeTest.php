<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 */
class InspectCodeTest extends KernelTestCase
{
    /**
     * @dataProvider createInspectCodeInputsProvider
     */
    public function testCommandBuild($data, $expected): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('ox-inspect:code');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs($data);
        $commandTester->execute([]);

        $this->assertStringContainsString($expected, $commandTester->getDisplay());
    }

    public function createInspectCodeInputsProvider(): array
    {
        return [
            'phpstan-path' => [
                [
                    '0', // tool
                    '0', // context
                    'bin', // path
                    '--verbose', // command shown
                    'n', // exit
                ],
                'vendor/bin/phpstan'
            ],
            'phpcs-path' => [
                [
                    '1', // tool
                    '0', // context
                    'bin', // path
                    '--verbose', // command shown
                    'n', // exit
                ],
                'vendor/bin/phpcs'
            ],
            'phpcbf-path' => [
                [
                    '2', // tool
                    '0', // context
                    'bin', // path
                    '--verbose', // command shown
                    'n', // exit
                ],
                'vendor/bin/phpcbf'
            ],
            'phpcompatibility-path' => [
                [
                    '3', // tool
                    '0', // context
                    'bin', // path
                    '--verbose', // command shown
                    'n', // exit
                ],
                'vendor/bin/phpcs'
            ],
            'phplint-path' => [
                [
                    '4', // tool
                    '0', // context
                    'bin', // path
                    '--verbose', // command shown
                    'n', // exit
                ],
                'vendor/bin/parallel-lint'
            ],
        ];
    }
}
