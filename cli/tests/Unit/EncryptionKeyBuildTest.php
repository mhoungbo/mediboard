<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Ox\Cli\Console\AppBridge;
use Ox\Cli\Console\EncryptionKeyBuild;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 * @runTestsInSeparateProcesses
 */
class EncryptionKeyBuildTest extends OxUnitTestCase
{
    /**
     * @return Command
     */
    private function makeCommand(): Command
    {
        $application = new Application();
        $application->add(
            new EncryptionKeyBuild(
                $this->getMockBuilder(AppBridge::class)
                     ->disableOriginalConstructor()
                     ->onlyMethods(['start'])
                     ->getMock()
            )
        );

        return $application->find('ox-encrypt:build-keys');
    }

    /**
     * @description No keys have to be generated on test environment, user must manually generated them otherwise
     */
    public function testSuccess(): void
    {
        $command = $this->makeCommand();
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $commandTester->assertCommandIsSuccessful();
        $this->assertStringContainsString('0 keys have been generated', $commandTester->getDisplay());
    }
}
