<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Ox\Cli\Console\Diagnose;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 * @runTestsInSeparateProcesses
 */
class DiagnoseTest extends OxUnitTestCase
{
    protected const EXPECTED_TESTS_COUNT = 28;

    /**
     * @return Command
     */
    private function makeCommand(): Command
    {
        $application = new Application();
        $application->add(new Diagnose());

        return $application->find('ox-install:diagnose');
    }

    public function testRun(): void
    {
        $command = $this->makeCommand();
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--json' => true
            ]
        );

        $this->assertStringContainsString(
            'Number of tests performed : ' . self::EXPECTED_TESTS_COUNT,
            $commandTester->getDisplay()
        );

        $this->assertFileExists(Diagnose::JSON_REPORT);

        $report = json_decode(file_get_contents(Diagnose::JSON_REPORT), true);

        $this->assertArrayHasKey('suite', $report);

        foreach ($report['suite'] as $test) {
            $this->assertMatchesRegularExpression("/^(OK|WARNING|ERROR)$/", $test["Result"]);
        }
    }
}
