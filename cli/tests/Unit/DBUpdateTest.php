<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Ox\Cli\Console\AppBridge;
use Ox\Cli\Console\DBUpdate;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 * @runTestsInSeparateProcesses
 */
class DBUpdateTest extends OxUnitTestCase
{
    /**
     * @return Command
     */
    private function makeCommand(): Command
    {
        $application = new Application();
        $application->add(
            new DBUpdate(
                $this->getMockBuilder(AppBridge::class)
                     ->disableOriginalConstructor()
                     ->onlyMethods(['start'])
                     ->getMock()
            )
        );

        return $application->find('ox-db:update');
    }

    /**
     * @description At this point, all modules are up to date
     */
    public function testIsUpToDate(): void
    {
        $command       = $this->makeCommand();
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--dry-run' => true]);

        $commandTester->assertCommandIsSuccessful();
        $this->assertStringContainsString('are up to date', $commandTester->getDisplay());
    }
}
