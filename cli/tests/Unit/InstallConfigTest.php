<?php

/**
 * @package Mediboard\\Cli
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Ox\Core\CMbConfig;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class InstallConfigTest
 * @group schedules
 */
class InstallConfigTest extends KernelTestCase
{
    private const PATH = 'tmp';
    private Filesystem $fs;

    public function setUp(): void
    {
        parent::setUp();

        $this->fs = new Filesystem();
        $this->setConfigurationDirectory();

    }

    public function testAutoSuccessfulGeneration()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('ox-install:config');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--ci-mode'          => true,
                '--externals'        => true,
                '--path'             => "tmp",
                '--external-db-host' => "db",
                '--external-db-user' => "dev",
                '--external-db-pass' => "WeDoNotCare",
            ]
        );

        $commandTester->assertCommandIsSuccessful();
        $this->assertStringContainsString('Configurations successfully saved', $commandTester->getDisplay());
    }

    public function testManualSuccessfulGeneration()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('ox-install:config');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                '--path' => "tmp",
            ]
        );

        $commandTester->assertCommandIsSuccessful();
        $this->assertStringContainsString('Configurations successfully saved', $commandTester->getDisplay());
    }

    public function testAlreadyExistingConfiguration()
    {
        $this->setConfigurationDirectory(true);

        $this->expectExceptionMessageMatches('/^The configuration file .* already exists\.$/');

        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('ox-install:config');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
    }



    private function setConfigurationDirectory(bool $withFile = false): void
    {
        $configDir = self::PATH . DIRECTORY_SEPARATOR . 'includes';
        if ($this->fs->exists($configDir)) {
            $this->fs->remove($configDir);
        }

        $this->fs->mkdir($configDir);

        if (true === $withFile) {
            $this->fs->touch(self::PATH . DIRECTORY_SEPARATOR . CMbConfig::CONFIG_FILE);
        }
    }
}
