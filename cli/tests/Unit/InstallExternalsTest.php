<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Ox\Cli\Console\InstallExternals;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 * @runTestsInSeparateProcesses
 */
class InstallExternalsTest extends OxUnitTestCase
{
    /**
     * @return Command
     */
    private function makeCommand(): Command
    {
        $application = new Application();
        $application->add(new InstallExternals());

        return $application->find('ox-install:externals');
    }

    /**
     * @description At this point, all externals should be provisioned
     */
    public function testExecution(): void
    {
        $commandTester = new CommandTester($this->makeCommand());
        $commandTester->execute([]);
        $commandTester->assertCommandIsSuccessful();

        $this->assertStringContainsString('External databases data imported', $commandTester->getDisplay());
    }

    /**
     * @depends testExecution
     * @description At this point, all externals should be provisioned
     */
    public function testRestore(): void
    {
        $name = 'sae';

        $commandTester = new CommandTester($this->makeCommand());
        $commandTester->execute(
            [
                'names'     => [$name],
                '-d'        => true,
            ],
            [
                'verbosity' => OutputInterface::VERBOSITY_VERBOSE
            ]
        );
        $commandTester->assertCommandIsSuccessful();
        $commandOutput = $commandTester->getDisplay();

        // Check external database removal was displayed
        $this->assertStringContainsString('Importing database', $commandOutput);

        // Check sql file import was displayed
        $this->assertStringContainsString($name . '.sql', $commandOutput);

        // Check one external database import was displayed
        $this->assertStringContainsString('1 External databases data imported', $commandOutput);
    }
}
