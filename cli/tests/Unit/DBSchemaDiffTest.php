<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Exception;
use Ox\Cli\Console\DBSchemaDiff;
use Ox\Cli\Console\InstallDatabase;
use Ox\Core\CAppUI;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 * @runTestsInSeparateProcesses
 */
class DBSchemaDiffTest extends OxUnitTestCase
{
    private const TEST_DB_NAME = 'mediboard_schema_diff';
    private string $host;
    private string $user;
    private string $pass;
    private string $name;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->host = CAppUI::conf('db std dbhost');
        $this->user = CAppUI::conf('db std dbuser');
        $this->pass = CAppUI::conf('db std dbpass');
        $this->name = CAppUI::conf('db std dbname');
    }

    /**
     * @return Command
     */
    private function makeCommand(): Command
    {
        $application = new Application();
        $application->add(new DBSchemaDiff());

        return $application->find('db:schemadiff');
    }

    /**
     * @throws Exception
     */
    private function provisionTestDatabase(): void
    {
        $application = new Application();
        $application->add(new InstallDatabase());

        $command = $application->find('ox-install:database');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(
            [
                'no',
                $this->host,
                self::TEST_DB_NAME,
                $this->user,
                $this->pass,
                'azerty123',
                'yes',
            ]
        );
        $commandTester->execute(['--delete' => true]);
        $commandTester->assertCommandIsSuccessful();
    }

    /**
     * @throws Exception
     */
    public function testHasNoDiff(): void
    {
        $command = $this->makeCommand();
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'host1'   => $this->host,
                'schema1' => $this->name,
                'user1'   => $this->user,
                'pass1'   => $this->pass,
                'host2'   => $this->host,
                'schema2' => $this->name,
                'user2'   => $this->user,
                'pass2'   => $this->pass,
            ]
        );

        $commandTester->assertCommandIsSuccessful();

        $commandOutput = $commandTester->getDisplay();

        $this->assertStringContainsString('Database schemas are identical', $commandOutput);
    }

    /**
     * @throws Exception
     */
    public function testHasDiff(): void
    {
        $this->provisionTestDatabase();

        $command = $this->makeCommand();

        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'host1'     => $this->host,
                'schema1'   => $this->name,
                'user1'     => $this->user,
                'pass1'     => $this->pass,
                'host2'     => $this->host,
                'schema2'   => self::TEST_DB_NAME,
                'user2'     => $this->user,
                'pass2'     => $this->pass,
            ],
            [
                'verbosity' => OutputInterface::VERBOSITY_VERBOSE
            ]
        );

        $this->assertEquals($commandTester->getStatusCode(), $command::FAILURE);

        $commandOutput = $commandTester->getDisplay();

        $this->assertStringContainsString('missing tables', $commandOutput);
        $this->assertStringContainsString('tables with differences', $commandOutput);
        /* Validates verbose output with diff per table */
        $this->assertStringContainsString('Index Length', $commandOutput);
    }
}
