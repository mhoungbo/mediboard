<?php

/**
 * @package Mediboard\Tests
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use CzProject\GitPhp\InvalidArgumentException;
use Exception;
use CzProject\GitPhp\Commit;
use CzProject\GitPhp\CommitId;
use DateTimeImmutable;
use DOMDocument;
use Ox\Cli\Console\ReleaseMakeXML;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @group schedules
 * @runTestsInSeparateProcesses
 */
class ReleaseMakeXMLTest extends OxUnitTestCase
{
    private Filesystem $fs;
    private string $revision;
    private const OUTPUT_XML_PATH = 'tmp/release.xml';
    private const SAMPLE_REVISION_LENGTH = 40;
    private const SAMPLE_NAME  = 'Test OX';
    private const SAMPLE_EMAIL  = 'test@openxtrem.com';
    private const SAMPLE_DESCRIPTION  = 'Fnc: Modification';
    private const SAMPLE_DATE = '2022-02-22';
    private const SAMPLE_RELEASE_CODE = '2022_02';
    private const SAMPLE_RELEASE_BRANCH = 'release/' . self::SAMPLE_RELEASE_CODE;
    private const SAMPLE_INVALID_BRANCH = 'master';

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->fs = new Filesystem();

        $this->revision = substr(sha1(rand()), 0, self::SAMPLE_REVISION_LENGTH);
    }

    /**
     * @param string $branch
     *
     * @return Command
     * @throws InvalidArgumentException
     */
    private function makeCommand(string $branch): Command
    {
        $command = $this->getMockBuilder(ReleaseMakeXML::class)
                    ->onlyMethods(
                        [
                            'getLastCommit',
                            'getCurrentBranchName',
                        ]
                    )->getMock();

        $command->expects($this->any())->method('getCurrentBranchName')->willReturn($branch);
        $command->expects($this->any())->method('getLastCommit')->willReturn(
            new Commit(
                new CommitId($this->revision),
                self::SAMPLE_DESCRIPTION,
                self::SAMPLE_DESCRIPTION,
                self::SAMPLE_EMAIL,
                self::SAMPLE_NAME,
                new DateTimeImmutable(self::SAMPLE_DATE),
                self::SAMPLE_EMAIL,
                self::SAMPLE_NAME,
                new DateTimeImmutable(self::SAMPLE_DATE),
            )
        );

        $application = new Application();
        $application->add($command);

        return $application->find('ox-release:makexml');
    }

    /**
     * @return void
     */
    public function testSuccessOnRelease(): void
    {
        $command = $this->makeCommand(self::SAMPLE_RELEASE_BRANCH);

        $commandTester = new CommandTester($command);
        $commandTester->execute(['--output' => 'tmp']);

        $commandTester->assertCommandIsSuccessful();
        $this->assertStringContainsString('release.xml file written', $commandTester->getDisplay());
        $this->assertTrue($this->fs->exists(self::OUTPUT_XML_PATH));

        $document = new DOMDocument();
        $document->load(self::OUTPUT_XML_PATH);

        $this->assertXpathMatch($document, 1, 'count(//release)');
        $this->assertXpathMatch($document, $this->revision, 'string(//release/@revision)');
        $this->assertXpathMatch($document, self::SAMPLE_RELEASE_CODE, 'string(//release/@code)');
    }

    /**
     * @throws InvalidArgumentException
     */
    public function testFailureOnMaster(): void
    {
        $command = $this->makeCommand(self::SAMPLE_INVALID_BRANCH);

        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $this->assertEquals(
            $command::FAILURE,
            $commandTester->getStatusCode()
        );

        $this->assertStringContainsString(
            'Current branch is not a release branch',
            $commandTester->getDisplay()
        );
    }
}
