<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Cli\Tests\Unit;

use Exception;
use Ox\Cli\Console\AppBridge;
use Ox\Cli\Console\CreateModule;
use Ox\Core\CAppUI;
use Ox\Core\CMbPath;
use Ox\Tests\LoggerTest;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\RouterTest;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @group schedules
 */
class CreateModuleTest extends OxUnitTestCase
{
    /**
     * @throws Exception
     */
    public function testFailedModuleCreatedAlreadyExists(): void
    {
        try {
            $application = new Application();
            $application->add(
                new CreateModule(
                    // Should not start App again
                    $this->getMockBuilder(AppBridge::class)->disableOriginalConstructor()->getMock()
                )
            );

            $command = $application->find('ox-module:create');

            $inputs = ['0', 'loremipsum', 'loremipsum', 'loremipsum', 'loremipsum', 'lorem', '0', '3', '1'];

            $command_tester = new CommandTester($command);
            $command_tester->setInputs(
                $inputs
            );
            $command_tester->execute([]);

            $command_tester = new CommandTester($command);
            $command_tester->setInputs(
                $inputs
            );

            $this->expectExceptionMessage('Module \'loremipsum\' existe d�j�');
            $command_tester->execute([]);
        } finally {
            $this->deleteModuleDir();
        }
    }

    /**
     * @throws Exception
     */
    public function testSuccessfulModuleCreated(): void
    {
        try {
            $application = new Application();
            $application->add(
                new CreateModule(
                    $this->getMockBuilder(AppBridge::class)->disableOriginalConstructor()->getMock()
                )
            );

            $command = $application->find('ox-module:create');

            $command_tester = new CommandTester($command);
            $command_tester->setInputs(
                ['0', 'loremipsum2', 'loremipsum2', 'loremipsum2', 'loremipsum2', 'lorem2', '0', '3', '1']
            );
            $command_tester->execute([]);

            $command_tester->assertCommandIsSuccessful();
            $this->assertStringContainsString('Module successfully created !', $command_tester->getDisplay());
        } finally {
            $this->deleteModuleDir();
        }
    }

    /**
     * @throws Exception
     */
    private function deleteModuleDir()
    {
        $root_dir = CAppUI::conf('root_dir');

        $dir = "{$root_dir}/modules/loremipsum";
        if (is_dir($dir)) {
            CMbPath::remove($dir);
        }

        $dir = "{$root_dir}/modules/loremipsum2";
        if (is_dir($dir)) {
            CMbPath::remove($dir);
        }
    }
}
