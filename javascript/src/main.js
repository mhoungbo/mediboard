import Appbar from "./components/Appbar/Appbar.vue"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"

window.addEventListener("DOMContentLoaded", () => {
    if (document.getElementById("Appbar") === null) {
        return
    }
    createEntryPoint("Appbar", Appbar)
})
