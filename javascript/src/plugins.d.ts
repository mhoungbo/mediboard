import Vue from "vue"
import { OxEntryPointConfigs, OxEntryPointPrefs } from "@/core/types/OxEntryPointTypes"
import { configs, prefs } from "@/core/utils/OxInjector"

declare module "vue/types/vue" {
    interface Vue {
        $tr: (key: string, plural: boolean = false, ...values: string[]) => string
        /**
         * @deprecated Use $isModuleActive instead
         * @param moduleName
         * @param modulesListJson
         */
        $moduleExists: (moduleName: string, modulesListJson: ModulesListJson = null) => boolean
        $isModuleActive: (moduleName: string) => boolean
        [configs]: OxEntryPointConfigs
        [prefs]: OxEntryPointPrefs
    }
}
