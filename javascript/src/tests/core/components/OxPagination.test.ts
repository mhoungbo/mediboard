/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import OxPagination from "@/core/components/OxPagination/OxPagination.vue"
import OxObject from "@/core/models/OxObject"
import oxApiService from "@/core/utils/OxApiService"
import OxCollection from "@/core/models/OxCollection"
import { OxSchema } from "@/core/types/OxSchema"
import { OxCollectionLinks, OxCollectionMeta } from "@/core/types/OxCollectionTypes"
import pinia from "@/core/plugins/OxPiniaCore"
import { storeSchemas } from "@/core/utils/OxStorage"
import { setActivePinia } from "pinia"

/* eslint-disable dot-notation */

const mockList = {
    data: [
        {
            type: "test_object",
            id: "3",
            attributes: {
                field_str: "Test 3",
                field_date: "01/01/1995",
                field_time: "01:33:00"
            }
        }
    ],
    meta: {
        date: "2022-09-12 11:07:50+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 1,
        total: 3
    },
    links: {
        self: "http://localhost/api/sample/tests?limit=2&offset=2&schema=true",
        prev: "http://localhost/api/sample/tests?limit=2&offset=0&schema=true",
        first: "http://localhost/api/sample/tests?limit=2&offset=0&schema=true",
        last: "http://localhost/api/sample/tests?limit=2&offset=2&schema=true"
    }
}
jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: mockList }))

/**
 * Test pour OxPagination
 */
export default class OxPaginationTest extends OxTest {
    protected component = OxPagination

    private collection1 = new OxCollection()
    private collection2 = new OxCollection()
    private collection3 = new OxCollection()
    private testObject1 = new TestObject()
    private testObject2 = new TestObject()
    private testObject3 = new TestObject()

    protected beforeAllTests () {
        super.beforeAllTests()
        const schema = [
            {
                id: "489f3046fbdf81481652a4b19b45a25c",
                owner: "test_object",
                field: "field_str",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Field str",
                label: "Field str",
                description: "Field str description"
            },
            {
                id: "1c1f00ed017758b7de04174e5a21177f",
                owner: "test_object",
                field: "field_date",
                type: "date",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                libelle: "Field date",
                label: "Field date",
                description: "Field date description"
            },
            {
                id: "efd8ccabedcdca0b495941110eb85d63",
                owner: "test_object",
                field: "field_time",
                type: "time",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Field time",
                label: "Field time",
                description: "Field time description"
            }
        ] as unknown as OxSchema[]

        this.testObject1.id = "1"
        this.testObject1.type = "test_object"
        this.testObject1.attributes = {
            field_str: "Test 1",
            field_date: "01/01/1980",
            field_time: "01:00:00"
        }

        this.testObject2.id = "2"
        this.testObject2.type = "test_object"
        this.testObject2.attributes = {
            field_str: "Test 2",
            field_date: "01/01/1990",
            field_time: "01:30:00"
        }

        this.testObject3.id = "3"
        this.testObject3.type = "test_object"
        this.testObject3.attributes = {
            field_str: "Test 3",
            field_date: "01/01/1995",
            field_time: "01:33:00"
        }

        this.collection1.objects = [
            this.testObject1,
            this.testObject2,
            this.testObject3
        ] as unknown as TestObject[]
        this.collection1.meta = {
            count: 1,
            total: 3,
            schema
        } as OxCollectionMeta
        this.collection1.links = {
            self: "http://localhost/api/sample/tests?limit=2&offset=2&schema=true",
            prev: "http://localhost/api/sample/tests?limit=2&offset=0&schema=true",
            first: "http://localhost/api/sample/tests?limit=2&offset=0&schema=true",
            last: "http://localhost/api/sample/tests?limit=2&offset=2&schema=true"
        } as OxCollectionLinks

        this.collection2.objects = []
        this.collection2.meta = {
            count: 0,
            total: 0,
            schema
        } as OxCollectionMeta
        this.collection2.links = {} as OxCollectionLinks

        this.collection3.objects = []
        this.collection3.meta = {
            count: 0,
            total: 20,
            schema
        } as OxCollectionMeta
        this.collection3.links = {
            self: "http://localhost/api/sample/tests?limit=5&offset=20&schema=true",
            prev: "http://localhost/api/sample/tests?limit=5&offset=15&schema=true",
            first: "http://localhost/api/sample/tests?limit=5&offset=0&schema=true",
            last: "http://localhost/api/sample/tests?limit=5&offset=15&schema=true"
        } as OxCollectionLinks

        setActivePinia(pinia)
        storeSchemas(schema)
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testHasObjectClass () {
        const pagination = this.vueComponent({ value: this.collection1 })

        this.assertTrue(this.privateCall(pagination, "hasObjectClass"))
    }

    public testHasNotObjectClass () {
        // @ts-ignore, Ignore console.warn from created
        window.console.warn = jest.fn()
        const pagination = this.vueComponent({ value: this.collection2 })
        expect(window.console.warn).toBeCalled()
        this.assertFalse(this.privateCall(pagination, "hasObjectClass"))
    }

    public testPerPageWithoutSelf () {
        const pagination = this.vueComponent({ value: this.collection2 })
        this.assertEqual(this.privateCall(pagination, "perPage"), 0)
    }

    public testPerPageWithSelf () {
        const pagination = this.vueComponent({ value: this.collection1 })
        this.assertEqual(this.privateCall(pagination, "perPage"), 2)
    }

    public testCurrentPageWithoutSelf () {
        const pagination = this.vueComponent({ value: this.collection2 })
        this.assertEqual(this.privateCall(pagination, "currentPage"), 1)
    }

    public testCurrentPageWithSelf () {
        const pagination = this.vueComponent({ value: this.collection1 })
        this.assertEqual(this.privateCall(pagination, "currentPage"), 2)
    }

    public async testRefreshCurrentPageWithItems () {
        const pagination = this.mountComponent({ value: this.collection1 })
        await this.privateCall(pagination.vm, "refresh")

        expect(oxApiService.get).toBeCalledWith(expect.stringContaining("tests?limit=2&offset=2&schema=true"))
        const events = pagination.emitted("input")
        this.assertHaveLength(events, 1)
    }

    public async testRefreshGivenPage () {
        const pagination = this.mountComponent({ value: this.collection1 })
        await this.privateCall(pagination.vm, "refresh", 1)

        expect(oxApiService.get).toBeCalledWith(expect.stringContaining("tests?limit=2&offset=0&schema=true"))
        const events = pagination.emitted("input")
        this.assertHaveLength(events, 1)
    }

    public async testAutoReloadCollectionWithRightOffset () {
        const mockList = {
            data: [],
            meta: {
                date: "2022-09-12 11:07:50+02:00",
                copyright: "OpenXtrem-2022",
                authors: "dev@openxtrem.com",
                count: 0,
                total: 20
            },
            links: {
                self: "http://localhost/api/sample/tests?limit=5&offset=20&schema=true",
                prev: "http://localhost/api/sample/tests?limit=5&offset=15&schema=true",
                first: "http://localhost/api/sample/tests?limit=5&offset=0&schema=true",
                last: "http://localhost/api/sample/tests?limit=5&offset=15&schema=true"
            }
        }
        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: mockList }))

        const pagination = this.mountComponent({ value: this.collection3 })
        await this.privateCall(pagination.vm, "refresh")

        expect(oxApiService.get).toHaveBeenNthCalledWith(1, expect.stringContaining("tests?limit=5&offset=20&schema=true"))
        expect(oxApiService.get).toHaveBeenNthCalledWith(2, expect.stringContaining("tests?limit=5&offset=15&schema=true"))

        const events = pagination.emitted("input")
        this.assertHaveLength(events, 1)
    }
}

class TestObject extends OxObject {
    constructor () {
        super()
        this.type = "test_object"
    }
}

(new OxPaginationTest()).launchTests()
