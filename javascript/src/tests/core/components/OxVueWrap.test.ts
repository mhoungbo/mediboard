/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import OxVueWrap from "@/core/components/OxVueWrap/OxVueWrap.vue"
import Vuetify from "vuetify"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import { useContextStore } from "@/core/stores/context"
import { useUserStore } from "@/core/stores/user"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import Function from "@modules/mediusers/vue/Models/Function"
import Group from "@modules/dPetablissement/vue/models/Group"

/**
 * Test for OxVueWrap
 */
export default class OxVueWrapTest extends OxTest {
    protected component = OxVueWrap

    private localesSpy = jest.fn()
    private contextStore
    private userStore

    protected beforeAllTests () {
        super.beforeAllTests()
        setActivePinia(pinia)
        this.contextStore = useContextStore()
        this.userStore = useUserStore()
    }

    protected beforeTest () {
        super.beforeTest()
        Object.defineProperty(window, "guiLocales", {
            configurable: true,
            set: this.localesSpy,
            get: () => ""
        })
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
        this.contextStore.$reset()
        this.userStore.$reset()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                vuetify: new Vuetify()
            }
        )
    }

    public testGuiLocalesSet () {
        const locales = {
            key1: "value1",
            key2: "value2",
            key3: "value3"
        }

        this.mountComponent({ locales })
        expect(this.localesSpy).toBeCalledWith(locales)
    }

    public testGuiLocalesNotSet () {
        this.mountComponent({})
        expect(this.localesSpy).not.toBeCalled()
    }

    public testMountWithCurrentVars () {
        const currentUser = {
            id: "1",
            guid: "CMediusers-1",
            view: "userLastName userFirstName",
            login: "username",
            first_name: "Test",
            last_name: "User",
            color: "FFFFFF"
        }
        const userFunction = {
            id: "2",
            guid: "CFunctions-2",
            view: "function2"
        }
        const userGroup = {
            id: "3",
            guid: "CGroups-3",
            view: "group3"
        }

        this.mountComponent({
            currentUser,
            userFunction,
            userGroup
        })

        expect(this.userStore.user).toBeInstanceOf(Mediuser)
        expect(this.userStore.userFunction).toBeInstanceOf(Function)
        expect(this.userStore.userGroup).toBeInstanceOf(Group)
    }
}

(new OxVueWrapTest()).launchTests()
