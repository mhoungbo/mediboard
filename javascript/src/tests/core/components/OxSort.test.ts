/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import OxSort from "@/core/components/OxSort/OxSort.vue"
import Vuetify from "vuetify"
import { setActivePinia } from "pinia"
import { OxSchema } from "@/core/types/OxSchema"
import pinia from "@/core/plugins/OxPiniaCore"
import { storeSchemas } from "@/core/utils/OxStorage"
import OxCollection from "@/core/models/OxCollection"
import OxObject from "@/core/models/OxObject"
import { OxCollectionLinks, OxCollectionMeta } from "@/core/types/OxCollectionTypes"
import oxApiService from "@/core/utils/OxApiService"

const mockList = {
    data: [
        {
            type: "test_object",
            id: "1",
            attributes: {
                field_str: "Test 1",
                field_date: "01/01/1980",
                field_time: "01:00:00"
            }
        },
        {
            type: "test_object",
            id: "2",
            attributes: {
                field_str: "Test 2",
                field_date: "01/01/1990",
                field_time: "01:30:00"
            }
        }
    ],
    meta: {
        date: "2022-09-12 11:07:50+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2,
        total: 2
    },
    links: {
        self: "http://localhost/mediboard/api/sample/movies?limit=4&offset=0",
        next: "http://localhost/mediboard/api/sample/movies?limit=4&offset=4",
        first: "http://localhost/mediboard/api/sample/movies?limit=4&offset=0",
        last: "http://localhost/mediboard/api/sample/movies?limit=4&offset=280"
    }
}
jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: mockList }))

/**
 * Test for OxSort
 */
export default class OxSortTest extends OxTest {
    protected component = OxSort

    private collection1 = new OxCollection()
    private collection2 = new OxCollection()
    private collection3 = new OxCollection()
    private testObject1 = new TestObject()
    private testObject2 = new TestObject()

    protected beforeAllTests () {
        super.beforeAllTests()
        const schema = [
            {
                id: "489f3046fbdf81481652a4b19b45a25c",
                owner: "test_object",
                field: "field_str",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Field str",
                label: "Field str",
                description: "Field str description"
            },
            {
                id: "1c1f00ed017758b7de04174e5a21177f",
                owner: "test_object",
                field: "field_date",
                type: "date",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                libelle: "Field date",
                label: "Field date",
                description: "Field date description"
            },
            {
                id: "efd8ccabedcdca0b495941110eb85d63",
                owner: "test_object",
                field: "field_time",
                type: "time",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Field time",
                label: "Field time",
                description: "Field time description"
            }
        ] as unknown as OxSchema[]

        this.testObject1.id = "1"
        this.testObject1.type = "test_object"
        this.testObject1.attributes = {
            field_str: "Test 1",
            field_date: "01/01/1980",
            field_time: "01:00:00"
        }

        this.testObject2.id = "1"
        this.testObject2.type = "test_object"
        this.testObject2.attributes = {
            field_str: "Test 2",
            field_date: "01/01/1990",
            field_time: "01:30:00"
        }

        this.collection1.objects = [
            this.testObject1,
            this.testObject2
        ] as unknown as TestObject[]
        this.collection1.meta = {
            count: 2,
            total: 2,
            schema
        } as OxCollectionMeta
        this.collection1.links = {
            self: "http://localhost/api/sample/tests?offset=0&sort=-field_date&schema=true"
        } as OxCollectionLinks

        this.collection2.objects = []
        this.collection2.meta = {
            count: 2,
            total: 2,
            schema
        } as OxCollectionMeta
        this.collection2.links = {
            self: "http://localhost/api/sample/tests?offset=0&schema=true"
        } as OxCollectionLinks

        this.collection3.objects = [
            this.testObject1,
            this.testObject2
        ] as unknown as TestObject[]
        this.collection3.meta = {
            count: 2,
            total: 2,
            schema
        } as OxCollectionMeta
        this.collection3.links = {} as OxCollectionLinks

        setActivePinia(pinia)
        storeSchemas(schema)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                vuetify: new Vuetify(),
                pinia
            }
        )
    }

    public testValidAspectValue () {
        // @ts-ignore
        const validator = OxSort.options.props.aspect.validator
        expect(validator("select")).toBeTruthy()
        expect(validator("text")).toBeTruthy()
    }

    public testInvalidAspectValue () {
        // @ts-ignore
        const validator = OxSort.options.props.aspect.validator
        expect(validator("invalidValue")).toBeFalsy()
    }

    public testHasObjectClass () {
        const sort = this.mountComponent({
            resourceName: "test_object",
            choices: ["field_str", "field_date", "field_time"],
            value: this.collection1
        })

        this.assertTrue(this.privateCall(sort.vm, "hasObjectClass"))
    }

    public testHasNotObjectClass () {
        window.console.warn = jest.fn()
        const sort = this.mountComponent({
            resourceName: "test_object",
            choices: ["field_str", "field_date", "field_time"],
            value: this.collection2
        })

        expect(window.console.warn).toBeCalledWith("Impossible type inference")
        this.assertFalse(this.privateCall(sort.vm, "hasObjectClass"))
    }

    public testSortCreation () {
        const sort = this.mountComponent({
            resourceName: "test_object",
            choices: ["field_str", "field_date", "field_time"],
            value: this.collection1
        })

        this.assertEqual(
            this.privateCall(sort.vm, "sorterChoices"),
            [
                {
                    value: "field_str",
                    label: "Field str"
                },
                {
                    value: "field_date",
                    label: "Field date"
                },
                {
                    value: "field_time",
                    label: "Field time"
                }
            ]
        )
        this.assertEqual(this.privateCall(sort.vm, "defaultChoice"), "field_date")
        this.assertEqual(this.privateCall(sort.vm, "defaultSort"), "DESC")
    }

    public async testChangeSortWithCall () {
        const sort = this.mountComponent({
            resourceName: "test_object",
            choices: ["field_str", "field_date", "field_time"],
            value: this.collection1
        })

        await this.privateCall(
            sort.vm,
            "changeSort",
            {
                choice: "field_str",
                sort: "ASC"
            }
        )

        expect(oxApiService.get).toBeCalledWith(expect.stringContaining("/tests?sort=field_str&schema=true"))
        const events = sort.emitted("input")
        this.assertHaveLength(events, 1)
    }

    public async testChangeSortWithoutCall () {
        const sort = this.mountComponent({
            resourceName: "test_object",
            choices: ["field_str", "field_date", "field_time"],
            value: this.collection3
        })

        await this.privateCall(
            sort.vm,
            "changeSort",
            {
                choice: "field_str",
                sort: "ASC"
            }
        )

        const events = sort.emitted("update")
        this.assertHaveLength(events, 1)
        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [{
                choice: "field_str",
                sort: "ASC"
            }]
        )
    }
}

class TestObject extends OxObject {
    constructor () {
        super()
        this.type = "test_object"
    }
}

(new OxSortTest()).launchTests()
