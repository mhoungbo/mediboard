/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import OxNotify from "@/core/components/OxNotify/OxNotify.vue"
import Vuetify from "vuetify"
import { addError, addInfo, localStorageKey, markInfoAsRead } from "@/core/utils/OxNotifyManager"

/* eslint-disable dot-notation */

/**
 * Test for OxNotify
 */
export default class OxNotifyTest extends OxTest {
    protected component = OxNotify

    protected beforeTest () {
        super.beforeTest()
        localStorage.clear()
        jest.useFakeTimers()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                vuetify: new Vuetify()
            }
        )
    }

    /**
     * Obsolete notif => read = true OR isError = true notifications
     */
    public testRemoveObsoleteOnMounted () {
        addInfo("Info 1")
        addInfo("Info 2")
        const notifications = this.getNotificationsFromLocalStorage()
        markInfoAsRead(notifications[0].id)
        addError("Error")

        const notify = this.mountComponent({})

        expect(notify.vm["errors"]).toHaveLength(0)

        expect(notify.vm["infos"]).toBeInstanceOf(Array)
        expect(notify.vm["infos"]).toHaveLength(1)
        expect(notify.vm["infos"][0]).toMatchObject({
            message: "Info 2",
            isError: false,
            minTime: 500,
            maxTime: 4000,
            read: false,
            closable: true
        })
    }

    public testShowErrors () {
        const notify = this.mountComponent({})
        addError("Error")

        this.assertTrue(this.privateCall(notify.vm, "showErrors"))
    }

    public testNotShowErrors () {
        addError("Error")
        const notify = this.mountComponent({})

        this.assertFalse(this.privateCall(notify.vm, "showErrors"))
    }

    public testInfoShown () {
        addInfo("Info 1")
        addInfo("Info 2")
        const notify = this.mountComponent({})

        const infoNotif = this.privateCall(notify.vm, "infoShown")
        expect(infoNotif).toMatchObject({
            message: "Info 1",
            isError: false,
            minTime: 500,
            maxTime: 4000,
            read: false,
            closable: true
        })
    }

    public testInfoNotShownCauseEmpty () {
        const notify = this.mountComponent({})

        this.assertNull(this.privateCall(notify.vm, "infoShown"))
    }

    public testInfoNotShownCauseHavingErrors () {
        const notify = this.mountComponent({})
        addError("Error")

        this.assertNull(this.privateCall(notify.vm, "infoShown"))
    }

    public testCloseNotifyHavingMultipleErrors () {
        const notify = this.mountComponent({})
        addError("Error 1")
        addError("Error 2")
        let notifications = this.getNotificationsFromLocalStorage()

        this.privateCall(notify.vm, "closeNotify", notifications[0].id)
        notifications = this.getNotificationsFromLocalStorage()
        expect(notifications).toBeInstanceOf(Array)
        expect(notifications).toHaveLength(1)
        expect(notifications[0]).toMatchObject({
            message: "Error 2",
            isError: true,
            minTime: 500,
            maxTime: 4000,
            read: false,
            closable: true
        })
    }

    public testCloseNotifyWithoutMultipleErrors () {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const timeoutSpy = jest.spyOn(global, "setTimeout")
        const notify = this.mountComponent({})
        addError("Error 1")

        const notifications = this.getNotificationsFromLocalStorage()
        this.privateCall(notify.vm, "closeNotify", notifications[0].id)

        expect(timeoutSpy).toHaveBeenCalledTimes(1)
        expect(timeoutSpy).toHaveBeenLastCalledWith(expect.any(Function), 100)
    }

    public async testInfoShownUpdate () {
        const notify = this.mountComponent({})
        addInfo("Info 1")
        let notifications = this.getNotificationsFromLocalStorage()

        await notify.vm.$nextTick()

        jest.runAllTimers()

        notifications = this.getNotificationsFromLocalStorage()

        this.assertTrue(notifications[0].read)
    }

    public async testDestroy (): Promise<void> {
        Object.defineProperty(window, "removeEventListener", {
            value: jest.fn()
        })
        const notify = this.mountComponent({})

        await notify.destroy()
        expect(window.removeEventListener).toHaveBeenCalled()
        expect(window.removeEventListener).toHaveBeenCalledWith(
            "notify",
            notify.vm["updateNotifications"]
        )
    }

    private getNotificationsFromLocalStorage () {
        return JSON.parse(localStorage.getItem(localStorageKey) || "")
    }
}

(new OxNotifyTest()).launchTests()
