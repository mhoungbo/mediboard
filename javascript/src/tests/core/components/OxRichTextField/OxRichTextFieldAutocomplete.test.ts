/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"

import OxHelpedText from "@modules/dPcompteRendu/vue/models/OxHelpedText"
import OxTranslator from "@/core/plugins/OxTranslator"
import OxRichTextFieldAutocomplete
    from "@/core/components/OxRichTextField/OxRichTextFieldAutocomplete/OxRichTextFieldAutocomplete.vue"
import OxTest from "@oxify/utils/OxTest"
import { MedicalCodes } from "@/tests/mocks/OxRichTextFieldMocks"
import User from "@modules/admin/vue/models/User"
import OxObject from "@/core/models/OxObject"
import Patient from "@modules/dPpatients/vue/models/Patient"
import MedicalCode from "@modules/system/vue/models/MedicalCode"

const localVue = createLocalVue()
localVue.use(OxTranslator)

const h1 = new OxHelpedText()
const h2 = new OxHelpedText()
const h3 = new OxHelpedText()

h1.id = "1"
h1.attributes = {
    name: "Lorem",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    class: "sample_movie",
    field: "description"
}

h2.id = "2"
h2.attributes = {
    name: "WithDependField",
    text: "DependField 1 is active",
    class: "sample_movie",
    field: "description",
    depend_value_1: "scifi",
    _vw_depend_field_1: "Science-Fiction"
}

h3.id = "3"
h3.attributes = {
    name: "WithBothDependField",
    text: "DependField 1 is active and DependField 2 is active",
    class: "sample_movie",
    field: "description",
    depend_value_1: "scifi",
    depend_value_2: "csa18",
    _vw_depend_field_1: "Science-Fiction",
    _vw_depend_field_2: "Interdit au moins de 18"
}

/* eslint-disable dot-notation */

export default class OxRichTextFieldAutocompleteTest extends OxTest {
    protected component = OxRichTextFieldAutocomplete

    protected beforeTest () {
        super.beforeTest()
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {
            MedicalCodeFilterHeader: true,
            MedicalCodeAutocomplete: true,
            OxHelpTextAutocomplete: true,
            PatientAutocomplete: true,
            UserAutocomplete: true
        },
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {
            MedicalCodeFilterHeader: true,
            MedicalCodeAutocomplete: true,
            OxHelpTextAutocomplete: true,
            PatientAutocomplete: true,
            UserAutocomplete: true
        },
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testOnCreatedAddTheWindowListerForKeydown () {
        window.addEventListener = jest.fn()

        const autocomplete = this.mountComponent({
            objects: [h1]
        })

        expect(window.addEventListener).toHaveBeenCalledTimes(1)
        // @ts-ignore
        expect(window.addEventListener).toHaveBeenCalledWith("keydown", autocomplete.vm.manageKeydownListener)
    }

    public testOnDestroyRemoveTheWindowListerForKeydown () {
        window.removeEventListener = jest.fn()

        const autocomplete = this.mountComponent({
            objects: [h1]
        })

        autocomplete.destroy()

        expect(window.removeEventListener).toHaveBeenCalledTimes(1)
        // @ts-ignore
        expect(window.removeEventListener).toHaveBeenCalledWith("keydown", autocomplete.vm.manageKeydownListener)
    }

    public async testResetAutocompleteSelectedOnPropsChanged () {
        const autocomplete = this.mountComponent({
            objects: [h1, h2]
        })

        autocomplete.vm.$data.autocompleteSelected = 1
        autocomplete.setProps({
            objects: [h1, h2, h3]
        })
        await autocomplete.vm.$nextTick()

        expect(autocomplete.vm.$data.autocompleteSelected).toEqual(0)
    }

    public testDisplayTheRightAmountOfHelpedText () {
        const autocompleteWithThree = this.mountComponent({
            objects: [h1, h2, h3]
        })
        const autocompleteWithOne = this.mountComponent({
            objects: [h1]
        })

        const expectThreeFields = autocompleteWithThree.findAll(".OxRichTextFieldAutocomplete-field")
        const expectOneField = autocompleteWithOne.findAll(".OxRichTextFieldAutocomplete-field")

        expect(expectThreeFields.length).toEqual(3)
        expect(expectOneField.length).toEqual(1)
    }

    public testHelpedTextsCustomPropsValidator () {
        // @ts-ignore
        const validator = OxRichTextFieldAutocomplete.options.props.objects.validator

        expect(validator([])).toBeFalsy()
        expect(validator([h1])).toBeTruthy()
    }

    public testEmitAutocompleteOnClick () {
        const autocomplete = this.mountComponent({
            objects: [h1, h2, h3]
        })

        const autocompleteFields = autocomplete.findAll(".OxRichTextFieldAutocomplete-field")
        const firstField = autocompleteFields.at(0)
        firstField.trigger("click")

        expect(autocomplete.emitted()).toHaveProperty("autocomplete", [[h1]])
    }

    public testCodesForTypeShouldBeEmptyIfObjectsAreNotCodes () {
        const autocomplete = this.vueComponent({
            objects: [h1, h2, h3]
        })

        autocomplete["selectedCode"] = "ccam"

        const codes = autocomplete["codesForType"]
        expect(codes.length).toEqual(0)
        expect(codes).toEqual([])
    }

    public testCodesForTypeForCCAM () {
        const autocomplete = this.vueComponent({
            objects: MedicalCodes
        })

        autocomplete["selectedCode"] = "ccam"

        const codes = autocomplete["codesForType"]
        expect(codes.length).toEqual(4)
        expect(codes).toEqual([MedicalCodes[0], MedicalCodes[1], MedicalCodes[2], MedicalCodes[3]])
    }

    public testCodesForTypeForLOINC () {
        const autocomplete = this.vueComponent({
            objects: MedicalCodes
        })

        autocomplete["selectedCode"] = "loinc"

        const codes = autocomplete["codesForType"]
        expect(codes.length).toEqual(1)
        expect(codes).toEqual([MedicalCodes[7]])
    }

    public testSelectCode () {
        const autocomplete = this.vueComponent({
            objects: MedicalCodes
        })

        autocomplete["mutatedObjects"] = []
        autocomplete["selectedCode"] = "ngap"

        autocomplete["selectCode"]("loinc")

        expect(autocomplete["selectedCode"]).toEqual("loinc")
        expect(autocomplete["mutatedObjects"]).toEqual([MedicalCodes[7]])
    }

    public testIsUserTrue () {
        const autocomplete = this.vueComponent({
            objects: [h1, h2, h3]
        })

        expect(autocomplete["isUser"](new User())).toBeTruthy()
    }

    public testIsUserFalse () {
        const autocomplete = this.vueComponent({
            objects: [h1, h2, h3]
        })

        expect(autocomplete["isUser"](new OxObject())).toBeFalsy()
    }

    public testIsPatientTrue () {
        const autocomplete = this.vueComponent({
            objects: [h1, h2, h3]
        })

        expect(autocomplete["isPatient"](new Patient())).toBeTruthy()
    }

    public testIsPatientFalse () {
        const autocomplete = this.vueComponent({
            objects: [h1, h2, h3]
        })

        expect(autocomplete["isPatient"](new OxObject())).toBeFalsy()
    }

    public testIsMedicalCodeTrue () {
        const autocomplete = this.vueComponent({
            objects: [h1, h2, h3]
        })

        expect(autocomplete["isMedicalCode"](new MedicalCode())).toBeTruthy()
    }

    public testIsMedicalCodeFalse () {
        const autocomplete = this.vueComponent({
            objects: [h1, h2, h3]
        })

        expect(autocomplete["isMedicalCode"](new OxObject())).toBeFalsy()
    }
}

(new OxRichTextFieldAutocompleteTest()).launchTests()

describe(
    "OxRichTextFieldAutocomplete",
    () => {
        test.each([
            {
                moveSet: [],
                selected: 0,
                emit: null
            },
            {
                moveSet: ["ArrowUp", "ArrowDown"],
                selected: 0,
                emit: null
            },
            {
                moveSet: ["Enter"],
                selected: 0,
                emit: { type: "autocomplete", value: h1 }
            },
            {
                moveSet: ["Escape"],
                selected: 0,
                emit: { type: "exit" }
            },
            {
                moveSet: ["ArrowDown", "ArrowDown", "Enter"],
                selected: 2,
                emit: { type: "autocomplete", value: h3 }
            },
            {
                moveSet: ["ArrowUp", "Enter"],
                selected: 2,
                emit: { type: "autocomplete", value: h3 }
            },
            {
                moveSet: ["ArrowDown", "ArrowDown", "ArrowDown", "Escape"],
                selected: 0,
                emit: { type: "exit" }
            },
            {
                moveSet: ["ArrowUp", "ArrowUp", "Escape"],
                selected: 1,
                emit: { type: "exit" }
            }
        ])("test autocomplete keyboard input '$moveSet' to have selected $selected and emitted $emit",
            ({ moveSet, selected, emit }) => {
                const autocomplete = shallowMount(OxRichTextFieldAutocomplete, {
                    propsData: {
                        objects: [h1, h2, h3]
                    },
                    stubs: {
                        OxHelpTextAutocomplete: true
                    }
                })

                moveSet.forEach((move) => {
                    const keyboardInput = { key: move, preventDefault: jest.fn() } as unknown as KeyboardEvent
                    autocomplete.vm["manageKeydownListener"](keyboardInput)
                })

                expect(autocomplete.vm.$data.autocompleteSelected).toEqual(selected)
                if (emit) {
                    const emitted = autocomplete.emitted(emit.type)
                    expect(emitted).not.toBeUndefined()

                    if (emitted && emit.value) {
                        expect(emitted[0][0]).toEqual(emit.value)
                    }
                }
            })
    }
)
