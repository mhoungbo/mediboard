/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxRichTextFieldButton from "@/core/components/OxRichTextField/OxRichTextFieldButton/OxRichTextFieldButton.vue"
import { shallowMount } from "@vue/test-utils"

/* eslint-disable dot-notation */

describe(
    "OxRichTextFieldButton",
    () => {
        test("test button class with active false", () => {
            const button = shallowMount(OxRichTextFieldButton, {
                propsData: {
                    icon: "help",
                    isActive: false
                }
            }).vm

            expect(button["buttonClass"]).toEqual({ OxRichTextFieldButton: true, active: false })
        })

        test("test button class with active true", () => {
            const button = shallowMount(OxRichTextFieldButton, {
                propsData: {
                    icon: "help",
                    isActive: true
                }
            }).vm

            expect(button["buttonClass"]).toEqual({ OxRichTextFieldButton: true, active: true })
        })
    }
)
