/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@/core/oxify/utils/OxTest"
import * as OxTextControl from "@/core/utils/OxTextControl"
import OxRichTextFieldContent from "@/core/components/OxRichTextField/OxRichTextFieldContent/OxRichTextFieldContent.vue"
import { shallowMount } from "@vue/test-utils"

/* eslint-disable dot-notation */

export default class OxRichTextFieldContentTest extends OxTest {
    protected component = OxRichTextFieldContent

    protected beforeTest () {
        super.beforeTest()
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
    }

    public testDefaultContent () {
        const content = this.vueComponent({
            isHelped: true,
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            ready: true
        })

        expect(content["isMarkdown"]).toEqual(false)
        expect(content["minLineCount"]).toEqual(5)
        expect(content["placeholder"]).toBeUndefined()
        expect(this.privateCall(content, "richTextStyle")).toEqual({ "min-height": "100px" })
    }

    public testFunctionChoseActionWithoutLongEnoughText () {
        const content = this.mountComponent({
            isHelped: true,
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. m",
            isMarkdown: true,
            ready: true
        })

        jest.spyOn(OxTextControl, "getCurrentNodeText").mockImplementation(() => {
            return { text: "m", isMiddle: false }
        })

        content.trigger("click")

        expect(content.emitted()).toHaveProperty("noActions", [["m"]])
        expect(content.emitted()).not.toHaveProperty("helpedText")
    }

    public testFunctionChoseActionWithText () {
        const content = this.mountComponent({
            isHelped: true,
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. morph",
            isMarkdown: true,
            ready: true
        })

        jest.spyOn(OxTextControl, "getCurrentNodeText").mockImplementation(() => {
            return { text: "morph", isMiddle: false }
        })

        content.trigger("click")

        expect(content.emitted()).not.toHaveProperty("noActions")
        expect(content.emitted()).toHaveProperty("helpedText", [["morph"]])
    }

    public testRemitInput () {
        const text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        const content = this.mountComponent({
            isHelped: true,
            text,
            isMarkdown: false,
            ready: true
        })

        content.trigger("input")

        expect(content.emitted()).toHaveProperty("input", [[text]])
    }

    public testUserCodesPatientQuickActionActiveFalse () {
        const text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        const content = this.vueComponent({
            isHelped: true,
            text,
            ready: true,
            quickActionsOptions: {
                usersQuickAction: false,
                codesQuickAction: false,
                patientsQuickAction: false
            }
        })

        expect(content["isUsersQuickActionActive"]).toBeFalsy()
        expect(content["isCodesQuickActionActive"]).toBeFalsy()
        expect(content["isPatientsQuickActionActive"]).toBeFalsy()
    }

    public testUserCodesPatientQuickActionActiveTrue () {
        const text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        const content = this.vueComponent({
            isHelped: true,
            text,
            ready: true,
            quickActionsOptions: {
                usersQuickAction: true,
                codesQuickAction: true,
                patientsQuickAction: true
            }
        })

        expect(content["isUsersQuickActionActive"]).toBeTruthy()
        expect(content["isCodesQuickActionActive"]).toBeTruthy()
        expect(content["isPatientsQuickActionActive"]).toBeTruthy()
    }

    public testUserCodesPatientQuickActionActiveTrueByDefault () {
        const text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        const content = this.vueComponent({
            isHelped: true,
            text,
            ready: true
        })

        expect(content["isUsersQuickActionActive"]).toBeTruthy()
        expect(content["isCodesQuickActionActive"]).toBeTruthy()
        expect(content["isPatientsQuickActionActive"]).toBeTruthy()
    }

    public testOnMountedAddEventListener () {
        window.addEventListener = jest.fn()

        const content = this.vueComponent({
            isHelped: true,
            ready: true,
            text: ""
        })

        expect(window.addEventListener).toHaveBeenCalledTimes(2)
        // @ts-ignore
        expect(window.addEventListener).toHaveBeenCalledWith("keydown", content.eventDisableLinks)
        // @ts-ignore
        expect(window.addEventListener).toHaveBeenCalledWith("keyup", content.eventActivateLinks)
    }

    public testOnDestroyedRemoveEventListener () {
        window.removeEventListener = jest.fn()

        const content = this.mountComponent({
            isHelped: true,
            ready: true,
            text: ""
        })

        content.destroy()

        expect(window.removeEventListener).toHaveBeenCalledTimes(2)
        // @ts-ignore
        expect(window.removeEventListener).toHaveBeenCalledWith("keydown", content.vm.eventDisableLinks)
        // @ts-ignore
        expect(window.removeEventListener).toHaveBeenCalledWith("keyup", content.vm.eventActivateLinks)
    }

    public testEventActivateLinks () {
        const content = this.vueComponent({
            isHelped: true,
            ready: true,
            text: ""
        })
        // @ts-ignore
        const linkEditionSpy = jest.spyOn(content, "setLinksEdition").mockImplementation()

        content["eventActivateLinks"]({ key: "Control" } as KeyboardEvent)

        expect(linkEditionSpy).toHaveBeenCalled()
        expect(linkEditionSpy).toHaveBeenCalledWith(true)
    }

    public testEventDisableLinks () {
        const content = this.vueComponent({
            isHelped: true,
            ready: true,
            text: ""
        })
        // @ts-ignore
        const linkEditionSpy = jest.spyOn(content, "setLinksEdition").mockImplementation()

        content["eventDisableLinks"]({ key: "Control" } as KeyboardEvent)

        expect(linkEditionSpy).toHaveBeenCalled()
        expect(linkEditionSpy).toHaveBeenCalledWith(false)
    }
}

(new OxRichTextFieldContentTest()).launchTests()

describe(
    "OxRichTextFieldContent",
    () => {
        test.each([
            { key: "i" },
            { key: "b" }
        ])("testPreventDefaultIfNotMarkdown", ({ key }) => {
            const prevent = jest.fn()
            const eventCTRL = { key, ctrlKey: true, preventDefault: prevent } as unknown as KeyboardEvent
            document.execCommand = jest.fn()

            const content = shallowMount(OxRichTextFieldContent, {
                propsData: {
                    isHelped: true,
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    isMarkdown: false,
                    ready: true
                }
            }).vm

            content["preventIfNotMarkdown"](eventCTRL)

            expect(prevent).toHaveBeenCalledTimes(1)
            expect(document.execCommand).toHaveBeenCalledTimes(0)
        })

        test.each([
            { key: "i", executedCommand: "italic" },
            { key: "b", executedCommand: "bold" }
        ])("testStyleUsingShortcutBeingApplied", ({ key, executedCommand }) => {
            const prevent = jest.fn()
            const eventCTRL = { key, ctrlKey: true, preventDefault: prevent } as unknown as KeyboardEvent
            document.execCommand = jest.fn()

            const content = shallowMount(OxRichTextFieldContent, {
                propsData: {
                    isHelped: true,
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    isMarkdown: true,
                    ready: true
                }
            }).vm

            content["preventIfNotMarkdown"](eventCTRL)

            expect(prevent).toHaveBeenCalledTimes(1)
            expect(document.execCommand).toHaveBeenCalledTimes(1)
            expect(document.execCommand).toHaveBeenCalledWith(executedCommand)
        })
    }
)
