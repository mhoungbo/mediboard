/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxRichTextFieldControlBar
    from "@/core/components/OxRichTextField/OxRichTextFieldControlBar/OxRichTextFieldControlBar.vue"
import OxRichTextFieldButton from "@/core/components/OxRichTextField/OxRichTextFieldButton/OxRichTextFieldButton.vue"
import OxTest from "@oxify/utils/OxTest"
import OxDivider from "@oxify/components/OxDivider/OxDivider.vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import { Component } from "vue"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

export default class OxRichTextFieldControlBarTest extends OxTest {
    protected component = OxRichTextFieldControlBar

    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testDefaultNumberOfButton () {
        const controlBar = this.mountComponent({
            isHelped: true
        })

        const buttons = controlBar.findAllComponents(OxRichTextFieldButton)
        expect(buttons.length).toEqual(3)
    }

    public testHas6ButtonWhenAddFastIsActive () {
        const controlBar = this.mountComponent({
            isHelped: true,
            addFast: true
        })

        const buttons = controlBar.findAllComponents(OxRichTextFieldButton)
        expect(buttons.length).toEqual(6)
    }

    public testHas9ButtonWhenMarkdownIsActive () {
        const controlBar = this.mountComponent({
            isHelped: true,
            isMarkdown: true
        })

        const buttons = controlBar.findAllComponents(OxRichTextFieldButton)
        expect(buttons.length).toEqual(9)
    }

    public testHas12ButtonWhenAllIsActive () {
        const controlBar = this.mountComponent({
            isHelped: true,
            isMarkdown: true,
            addFast: true,
            timestamp: true
        })

        const buttons = controlBar.findAllComponents(OxRichTextFieldButton)
        const dividers = controlBar.findAllComponents(OxDivider)
        expect(buttons.length).toEqual(12)
        expect(dividers.length).toEqual(3)
    }

    public testHas2ButtonWhenAllIsDisabled () {
        const controlBar = this.mountComponent({
            isHelped: true,
            isMarkdown: false,
            addFast: false,
            timestamp: false
        })

        const buttons = controlBar.findAllComponents(OxRichTextFieldButton)
        const dividers = controlBar.findAllComponents(OxDivider)
        expect(buttons.length).toEqual(2)
        expect(dividers.length).toEqual(1)
    }

    public testByDefaultExpandedShouldBeFalse () {
        const controlBar = this.mountComponent({
            isHelped: true,
            isMarkdown: true,
            addFast: true,
            timestamp: true
        })

        expect(controlBar.vm.$data.expanded).toEqual(false)
    }

    public async testOnMouseEnterAdd () {
        const controlBar = this.vueComponent({
            isHelped: true,
            isMarkdown: true,
            addFast: true,
            timestamp: true
        })

        this.privateCall(controlBar, "onMouseEnter")

        expect(controlBar.$data.expanded).toEqual(true)
    }

    public async testAddFastClass () {
        const controlBar = this.mountComponent({
            isHelped: true,
            isMarkdown: true,
            addFast: true,
            timestamp: true
        })

        const notExpandedClasses = controlBar.find(".OxRichTextFieldControlBar-addFast").classes()

        controlBar.vm.$data.expanded = true
        await controlBar.vm.$nextTick()

        const expandedClasses = controlBar.find(".OxRichTextFieldControlBar-addFast").classes()

        expect(notExpandedClasses).not.toContain("expanded")
        expect(expandedClasses).toContain("expanded")
    }
}

(new OxRichTextFieldControlBarTest()).launchTests()
