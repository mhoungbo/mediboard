/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxRichTextField from "@/core/components/OxRichTextField/OxRichTextField.vue"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import { OxQuickActionEmits, OxRichTextOptions } from "@/core/types/OxRichTextTypes"
import oxApiService from "@/core/utils/OxApiService"
import {
    ModelsConfigurationsApiMock,
    ModelsPreferencesApiMock,
    OxHelpedTextApiMock,
    OxHelpedTextListMock,
    sampleMovieDescriptionSchemaMockMarkdown
} from "@/tests/mocks/OxRichTextFieldMocks"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import OxHelpedText from "@modules/dPcompteRendu/vue/models/OxHelpedText"
import * as OxApiManager from "@/core/utils/OxApiManager"
import { getCollectionFromJsonApi, getObjectFromJsonApi } from "@/core/utils/OxApiManager"
import Configurations from "@modules/system/vue/models/Configurations"
import * as OxNotifyManager from "@/core/utils/OxNotifyManager"
import * as OxCursorControl from "@/core/utils/OxCursorControl"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"
import { prepareForm } from "@/core/utils/OxSchemaManager"
import OxCollection from "@/core/models/OxCollection"
import OxTest, { addActiveModules } from "@oxify/utils/OxTest"
import ModelsPreferences from "@modules/dPcompteRendu/vue/models/ModelsPreferences"
import User from "@modules/admin/vue/models/User"
import MedicalCode from "@modules/system/vue/models/MedicalCode"
import Patient from "@modules/dPpatients/vue/models/Patient"
import OxObject from "@/core/models/OxObject"
import { useUserStore } from "@/core/stores/user"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import Function from "@modules/mediusers/vue/Models/Function"
import Group from "@modules/dPetablissement/vue/models/Group"

const localVue = createLocalVue()
localVue.use(OxTranslator)

let userStore

const user = new Mediuser()
user.id = "1"
user.username = "jlebrun"
user.attributes._view = "LEBRUN John"
user.color = "FFFFFF"
user.lastName = "LEBRUN"
user.firstName = "John"

const userFunction = new Function()
userFunction.id = "1"

const userGroup = new Group()
userGroup.id = "1"

/* eslint-disable dot-notation */

export default class OxRichTextFieldTest extends OxTest {
    protected component = OxRichTextField

    protected beforeAllTests () {
        super.beforeAllTests()
        jest.useFakeTimers()
        jest.setSystemTime(new Date(2023, 3, 21, 11, 34, 28))

        // TODO: remove when the User will be in the APP context

        // TODO: Set User, userFunction and userGroup in pinia store

        setActivePinia(pinia)
        userStore = useUserStore()
        userStore.user = user
        userStore.userFunction = userFunction
        userStore.userGroup = userGroup
        addActiveModules("dPcompteRendu", "dPpatients")
    }

    protected afterAllTests () {
        super.afterAllTests()
        jest.useRealTimers()
    }

    protected async beforeTest () {
        super.beforeTest()

        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: sampleMovieDescriptionSchemaMockMarkdown })
            .mockResolvedValueOnce({ data: ModelsPreferencesApiMock })

        await prepareForm("sample_movie", ["details"])
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testMatchSearchTokensToHelpedTextId () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" } as OxRichTextOptions
        })

        const searchTokens = ["leur", "oise"]
        const helpedTextTokens = {
            oiseleurs: [12],
            leur: [15, 28],
            fleur: [12, 13],
            oie: [8]
        }
        const expectedMatch = {
            leur: new Set([12, 15, 28, 13]),
            oise: new Set([12])
        }

        expect(this.privateCall(
            richTextField,
            "matchSearchTokensToHelpedTextId",
            searchTokens,
            helpedTextTokens
        )).toEqual(expectedMatch)
    }

    public testOnClickOutside () {
        const richTextField = this.vueComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })
        richTextField["isFocused"] = true
        richTextField["displayAutocomplete"] = true

        this.privateCall(richTextField, "onClickOutside")

        expect(richTextField["isFocused"]).toBeFalsy()
        expect(richTextField["displayAutocomplete"]).toBeFalsy()
    }

    public testNoActionsEvent () {
        const richTextField = this.vueComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })
        richTextField["lastCursorNodes"] = undefined
        richTextField["displayAutocomplete"] = true

        const node = document.createTextNode("Lorem ipsum")

        // @ts-ignore
        document.getSelection = () => {
            return {
                getRangeAt () {
                    const range = new Range()
                    range.setStart(node, 2)
                    range.setEnd(node, 7)

                    return range
                }
            }
        }

        this.privateCall(richTextField, "noActions")

        expect(richTextField["lastCursorNodes"]).toEqual({
            startNode: node,
            endNode: node,
            startPos: 2,
            endPos: 7
        })
        expect(richTextField["displayAutocomplete"]).toBeFalsy()
    }

    public async testLoadingHelpedText () {
        const richTextField = this.vueComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: OxHelpedTextApiMock })
        const collection = getCollectionFromJsonApi(OxHelpedText, OxHelpedTextApiMock)
        await this.privateCall(richTextField, "onHelpedText", "lorem test")
        expect(richTextField["helpedTextCollection"]).toEqual(collection)
    }

    public async testOnHelpedTextShouldLoadOnceAndOnlyTakeLastValue () {
        const richTextField = this.vueComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })
        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: OxHelpedTextApiMock })
        // @ts-ignore
        const loadSpy = jest.spyOn(richTextField, "loadHelpedTextCollection")
        // @ts-ignore
        const helpedTextSpy = jest.spyOn(richTextField, "onHelpedText")
        // @ts-ignore
        const tokenizeTextSpy = jest.spyOn(richTextField, "tokenizeText")
        const firstCall = this.privateCall(richTextField, "onHelpedText", "lorem")
        await this.privateCall(richTextField, "onHelpedText", "tata")
        await firstCall
        expect(loadSpy).toHaveBeenCalledTimes(1)
        expect(helpedTextSpy).toHaveBeenNthCalledWith(1, "lorem")
        expect(helpedTextSpy).toHaveBeenNthCalledWith(2, "tata")
        expect(tokenizeTextSpy).toHaveBeenCalledTimes(1)
        expect(tokenizeTextSpy).toHaveBeenCalledWith("tata")
    }

    public async testLoadModuleConfig () {
        const richTextField = this.vueComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })

        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: ModelsConfigurationsApiMock })

        const config = await this.privateCall(richTextField, "loadModuleConfigs")

        expect(config).toEqual(getObjectFromJsonApi(Configurations, ModelsConfigurationsApiMock))
    }

    public async testLoadModulePrefs () {
        const richTextField = this.vueComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })

        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: ModelsPreferencesApiMock })

        const prefs = await richTextField["loadModulePrefs"]()

        // @ts-ignore
        expect(prefs).toEqual(getObjectFromJsonApi(ModelsPreferences, ModelsPreferencesApiMock))
    }

    public testCompleteAutocomplete () {
        const richTextField = this.mountComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })

        // @ts-ignore
        const hideAutocompleteSpy = jest.spyOn(richTextField.vm, "hideAutocomplete")
        // @ts-ignore
        const addTextToCursorSpy = jest.spyOn(richTextField.vm, "addTextToCursor").mockImplementation()

        richTextField.vm["lastTextMatched"] = "lorem"

        const helpedText = OxHelpedTextListMock[1]

        this.privateCall(richTextField.vm, "completeAutocomplete", helpedText)

        expect(hideAutocompleteSpy).toHaveBeenCalledTimes(1)
        expect(addTextToCursorSpy).toHaveBeenCalledTimes(1)
        expect(addTextToCursorSpy).toHaveBeenCalledWith(helpedText.text, 5)
        expect(richTextField.vm["executedActionsMutated"].helpedTexts).toEqual([helpedText])
        expect(richTextField.emitted("update:executedActions")).toEqual(
            [[{
                helpedTexts: [helpedText],
                users: [],
                patients: [],
                medicalCodes: []
            }]]
        )
    }

    public testCompleteAutocompleteQuickActionUser () {
        const richTextField = this.mountComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })

        // @ts-ignore
        const hideAutocompleteSpy = jest.spyOn(richTextField.vm, "hideAutocomplete")
        // @ts-ignore
        const addTextToCursorSpy = jest.spyOn(richTextField.vm, "addTextToCursor").mockImplementation()

        richTextField.vm["lastTextMatched"] = "@jle"

        const user = new User()
        user.username = "jlebrun"
        user.firstName = "John"
        user.lastName = "LEBRUN"

        this.privateCall(richTextField.vm, "completeAutocomplete", user)

        expect(hideAutocompleteSpy).toHaveBeenCalledTimes(1)
        expect(addTextToCursorSpy).toHaveBeenCalledTimes(1)
        expect(addTextToCursorSpy).toHaveBeenCalledWith(user.shortView, 4)
        expect(richTextField.vm["executedActionsMutated"].users).toEqual([user])
        expect(richTextField.emitted("update:executedActions")).toEqual(
            [[{
                helpedTexts: [],
                users: [user],
                patients: [],
                medicalCodes: []
            }]]
        )
    }

    public testCompleteAutocompleteQuickActionPatient () {
        const richTextField = this.mountComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })

        // @ts-ignore
        const hideAutocompleteSpy = jest.spyOn(richTextField.vm, "hideAutocomplete")
        // @ts-ignore
        const addTextToCursorSpy = jest.spyOn(richTextField.vm, "addTextToCursor").mockImplementation()

        richTextField.vm["lastTextMatched"] = "$lebru"

        const patient = new Patient()
        patient.firstName = "John"
        patient.lastName = "LEBRUN"

        this.privateCall(richTextField.vm, "completeAutocomplete", patient)

        expect(hideAutocompleteSpy).toHaveBeenCalledTimes(1)
        expect(addTextToCursorSpy).toHaveBeenCalledTimes(1)
        expect(addTextToCursorSpy).toHaveBeenCalledWith(patient.shortView, 6)
        expect(richTextField.vm["executedActionsMutated"].patients).toEqual([patient])
        expect(richTextField.emitted("update:executedActions")).toEqual(
            [[{
                helpedTexts: [],
                users: [],
                patients: [patient],
                medicalCodes: []
            }]]
        )
    }

    public testCompleteAutocompleteQuickActionCode () {
        const richTextField = this.mountComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })

        // @ts-ignore
        const hideAutocompleteSpy = jest.spyOn(richTextField.vm, "hideAutocomplete")
        // @ts-ignore
        const addTextToCursorSpy = jest.spyOn(richTextField.vm, "addTextToCursor").mockImplementation()

        richTextField.vm["lastTextMatched"] = "#morphine"

        const code = new MedicalCode()
        code.code = "ccam"
        code.description = "6-monoacétylmorphine [Masse/Volume]"
        code.codeType = "10975-1"

        this.privateCall(richTextField.vm, "completeAutocomplete", code)

        expect(hideAutocompleteSpy).toHaveBeenCalledTimes(1)
        expect(addTextToCursorSpy).toHaveBeenCalledTimes(1)
        expect(addTextToCursorSpy).toHaveBeenCalledWith(code.toString(), 9)
        expect(richTextField.vm["executedActionsMutated"].medicalCodes).toEqual([code])
        expect(richTextField.emitted("update:executedActions")).toEqual([[{
            helpedTexts: [],
            users: [],
            patients: [],
            medicalCodes: [code]
        }]]
        )
    }

    public async testOnHelpedTextSearchComplete () {
        const richTextField = this.vueComponent({
            options: {
                objectType: "sample_movie",
                field: "description"
            }
        })

        jest.resetAllMocks()
        jest.spyOn(oxApiService, "get")
            .mockResolvedValueOnce({ data: OxHelpedTextApiMock })

        // @ts-ignore
        const showAutocompleteSpy = jest.spyOn(richTextField, "showAutocomplete").mockImplementation()

        await this.privateCall(richTextField, "onHelpedText", "lorem")

        const allHelpedText = richTextField["helpedTextCollection"] as OxCollection<OxHelpedText>
        allHelpedText.objects.forEach((obj) => {
            obj.search(["lorem"])
        })
        const expectedObjects = [allHelpedText.objects[0], allHelpedText.objects[1]]

        expect(richTextField["autocompleteObjects"]).toEqual(expectedObjects)
        expect(showAutocompleteSpy).toHaveBeenCalledTimes(1)
        expect(showAutocompleteSpy).toHaveBeenCalledWith("lorem")
    }

    public testAutocompleteLaunch () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        const node = document.createTextNode("Lorem ipsum")
        const cursorNode = {
            startNode: node,
            endNode: node,
            startPos: 2,
            endPos: 7
        }
        richTextField["autocompleteActive"] = true
        richTextField["isFocused"] = false

        // @ts-ignore
        const autocompleteDebouncedSpy = jest.spyOn(richTextField, "autocompleteDebounced").mockImplementation()
        jest.spyOn(OxCursorControl, "getCursorNodes").mockReturnValue(cursorNode)

        this.privateCall(richTextField, "autocomplete", "lorem")

        expect(richTextField["isFocused"]).toBeTruthy()
        expect(richTextField["lastCursorNodes"]).toEqual(cursorNode)
        expect(autocompleteDebouncedSpy).toHaveBeenCalledTimes(1)
        expect(autocompleteDebouncedSpy).toHaveBeenCalledWith("lorem")
    }

    public testAutocompleteLaunchWithDisabledPref () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        const node = document.createTextNode("Lorem ipsum")
        const cursorNode = {
            startNode: node,
            endNode: node,
            startPos: 2,
            endPos: 7
        }
        richTextField["autocompleteActive"] = false

        // @ts-ignore
        const autocompleteDebouncedSpy = jest.spyOn(richTextField, "autocompleteDebounced").mockImplementation()
        jest.spyOn(OxCursorControl, "getCursorNodes").mockReturnValue(cursorNode)

        this.privateCall(richTextField, "autocomplete", "lorem")

        expect(richTextField["lastCursorNodes"]).toBeUndefined()
        expect(autocompleteDebouncedSpy).not.toHaveBeenCalled()
    }

    public testAutocompleteShouldNotBeCallIfTheFieldIsNotFocused () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        richTextField["isFocused"] = false

        // @ts-ignore
        const onHelpedTextSpy = jest.spyOn(richTextField, "onHelpedText")

        this.privateCall(richTextField, "autocompleteDebounced", "lorem")

        jest.runAllTimers()

        expect(onHelpedTextSpy).not.toHaveBeenCalled()
    }

    public testAutocompleteShouldBeCallIfTheFieldIsFocused () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        richTextField["isFocused"] = true

        // @ts-ignore
        const onHelpedTextSpy = jest.spyOn(richTextField, "onHelpedText")

        this.privateCall(richTextField, "autocompleteDebounced", "lorem")

        jest.runAllTimers()

        expect(onHelpedTextSpy).toHaveBeenCalled()
    }

    public testQuickActionAutocompleteShouldBeCallIfTheFieldIsFocused () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        richTextField["isFocused"] = true

        // @ts-ignore
        const onQuickActionSpy = jest.spyOn(richTextField, "onQuickAction")

        this.privateCall(richTextField, "quickActionAutocompleteDebounced", { type: "user", word: "lorem" } as OxQuickActionEmits)

        jest.runAllTimers()

        expect(onQuickActionSpy).toHaveBeenCalled()
    }

    public async testWriteTimestamp () {
        // TODO: Implements when a date utils is available

        // @ts-ignore
        window.DateFormat = {
            format () {
                return "-- %n %p - 23/03/2023 11:34"
            }
        }

        const richTextField = this.vueComponent(
            {
                options: { objectType: "sample_movie", field: "description" }
            }
        )

        const horodatageConfig = "-- %n %p - dd/MM/y HH:mm"
        const expectedValue = "-- LEBRUN John - 23/03/2023 11:34"

        richTextField["timestampConfig"] = horodatageConfig

        const result = await this.privateCall(richTextField, "generateHelpedTimestamp")

        expect(result).toEqual(expectedValue)
    }

    public testQuickActionAutocompleteLaunch () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        const node = document.createTextNode("Lorem ipsum")
        const cursorNode = {
            startNode: node,
            endNode: node,
            startPos: 2,
            endPos: 7
        }
        richTextField["autocompleteActive"] = true
        richTextField["isFocused"] = false

        // @ts-ignore
        const quickActionAutocompleteDebouncedSpy = jest.spyOn(richTextField, "quickActionAutocompleteDebounced").mockImplementation()
        jest.spyOn(OxCursorControl, "getCursorNodes").mockReturnValue(cursorNode)

        this.privateCall(richTextField, "quickActionAutocomplete", { type: "user", word: "@lorem" } as OxQuickActionEmits)

        expect(richTextField["isFocused"]).toBeTruthy()
        expect(richTextField["lastCursorNodes"]).toEqual(cursorNode)
        expect(quickActionAutocompleteDebouncedSpy).toHaveBeenCalledTimes(1)
        expect(quickActionAutocompleteDebouncedSpy).toHaveBeenCalledWith({ type: "user", word: "@lorem" })
    }

    public async testOnQuickActionShouldSearchUser () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        // @ts-ignore
        const searchUserSpy = jest.spyOn(richTextField, "searchUser")
            .mockImplementation(() => Promise.resolve([]) as Promise<Array<User>>)

        // @ts-ignore
        const hideAutocompleteSpy = jest.spyOn(richTextField, "hideAutocomplete")

        await richTextField["onQuickAction"]({ type: "user", word: "@lorem" } as OxQuickActionEmits)

        expect(hideAutocompleteSpy).toHaveBeenCalled()
        expect(searchUserSpy).toHaveBeenCalled()
        expect(searchUserSpy).toHaveBeenCalledWith("lorem")
    }

    public async testOnQuickActionShouldSearchCode () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        // @ts-ignore
        const searchCodeSpy = jest.spyOn(richTextField, "searchCode")
            .mockImplementation(() => Promise.resolve([]) as Promise<Array<MedicalCode>>)

        // @ts-ignore
        const hideAutocompleteSpy = jest.spyOn(richTextField, "hideAutocomplete")

        await richTextField["onQuickAction"]({ type: "code", word: "#lorem" } as OxQuickActionEmits)

        expect(hideAutocompleteSpy).toHaveBeenCalled()
        expect(searchCodeSpy).toHaveBeenCalled()
        expect(searchCodeSpy).toHaveBeenCalledWith("lorem")
    }

    public async testOnQuickActionShouldSearchPatient () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        // @ts-ignore
        const searchPatientSpy = jest.spyOn(richTextField, "searchPatient")
            .mockImplementation(() => Promise.resolve([]) as Promise<Array<Patient>>)

        // @ts-ignore
        const hideAutocompleteSpy = jest.spyOn(richTextField, "hideAutocomplete")

        await richTextField["onQuickAction"]({ type: "patient", word: "$lorem" } as OxQuickActionEmits)

        expect(hideAutocompleteSpy).toHaveBeenCalled()
        expect(searchPatientSpy).toHaveBeenCalled()
        expect(searchPatientSpy).toHaveBeenCalledWith("lorem")
    }

    public async testSearchUser () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        const getCollectionSpy = jest.spyOn(OxApiManager, "getCollectionFromJsonApiRequest")
            .mockImplementation(() => Promise.resolve(new OxCollection<OxObject>()))

        await richTextField["searchUser"]("lorem")

        expect(getCollectionSpy).toHaveBeenCalled()
        expect(getCollectionSpy).toHaveBeenCalledWith(User, "api/admin/users?autocomplete=1&keywords=lorem")
    }

    public async testSearchCode () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        const getCollectionSpy = jest.spyOn(OxApiManager, "getCollectionFromJsonApiRequest")
            .mockImplementation(() => Promise.resolve(new OxCollection<OxObject>()))

        await richTextField["searchCode"]("lorem")

        expect(getCollectionSpy).toHaveBeenCalled()
        expect(getCollectionSpy).toHaveBeenCalledWith(MedicalCode, "api/system/codes?keywords=lorem")
    }

    public async testSearchPatient () {
        const richTextField = this.vueComponent({
            options: { objectType: "sample_movie", field: "description" }
        })

        const getCollectionSpy = jest.spyOn(OxApiManager, "getCollectionFromJsonApiRequest")
            .mockImplementation(() => Promise.resolve(new OxCollection<OxObject>()))

        await richTextField["searchPatient"]("lorem")

        expect(getCollectionSpy).toHaveBeenCalled()
        expect(getCollectionSpy).toHaveBeenCalledWith(Patient, "api/dossierpatient/patients?keywords=lorem")
    }
}

(new OxRichTextFieldTest()).launchTests()

describe(
    "OxRichTextField",
    () => {
        beforeEach(async () => {
            jest.spyOn(oxApiService, "get")
                .mockResolvedValueOnce({ data: sampleMovieDescriptionSchemaMockMarkdown })
                .mockResolvedValueOnce({ data: ModelsPreferencesApiMock })

            await prepareForm("sample_movie", ["details"])
        })

        afterEach(() => {
            jest.resetAllMocks()
        })

        test.each([
            {
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                expectedText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            },
            {
                text: "<b>Lorem ipsum dolor sit amet</b>, <i>consectetur adipiscing elit</i>.",
                expectedText: "**Lorem ipsum dolor sit amet**, _consectetur adipiscing elit_."
            },
            {
                text: "<b>Lorem ipsum dolor sit amet</b>,<br> <i>consectetur adipiscing elit</i>.",
                expectedText: "**Lorem ipsum dolor sit amet**,  \n_consectetur adipiscing elit_."
            }
        ])("testOnInputFunctionShouldEmitInput with $text to be $expectedText", ({ text, expectedText }) => {
            const richTextField = shallowMount(OxRichTextField, {
                propsData: {
                    options: { objectType: "sample_movie", field: "description" },
                    isMarkdown: true
                },
                localVue
            })

            richTextField.vm["extractMarkdownFromHTML"](text)

            expect(richTextField.emitted()).toHaveProperty("input", [[expectedText]])
        })

        test.each([
            {
                text: "Lorem",
                expectedTokens: ["lorem"]
            },
            {
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                expectedTokens: ["lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit"]
            },
            {
                text: "a au ce d de des du en et il je l le les ou ses un une",
                expectedTokens: []
            },
            {
                text: "Le promeneur, berc\u00E9 par le chant des oiseleurs.",
                expectedTokens: ["promeneur", "berce", "par", "chant", "oiseleurs"]
            }
        ])("testTokenizeText with $text to be $expectedTokens", ({ text, expectedTokens }) => {
            const richTextField = shallowMount(OxRichTextField, {
                propsData: {
                    options: { objectType: "sample_movie", field: "description" }
                },
                localVue
            }).vm

            // @ts-ignore
            window.$T = function () { return "a au ce d de des du en et il je l le les ou ses un une" }

            expect(richTextField["tokenizeText"](text)).toEqual(expectedTokens)

            // @ts-ignore Reset the $T to the jest-prestart value
            window.$T = function (trad) { return trad }
        })

        test.each([
            {
                matchedIdsForWord: {
                    leur: new Set([12, 15, 28, 13])
                },
                expectedIds: [12, 15, 28, 13]
            },
            {
                matchedIdsForWord: {
                    leur: new Set([12, 15, 28, 13]),
                    oise: new Set([12])
                },
                expectedIds: [12]
            },
            {
                matchedIdsForWord: {
                    leur: new Set([12, 15, 28, 13]),
                    oise: new Set([12]),
                    lorem: new Set()
                },
                expectedIds: []
            }
        ])("testGetMatchedHelpedTextIdsFromMatchedWords with matchedIdsForWord '$matchedIdsForWord' to match '$expectedIds' ",
            ({ matchedIdsForWord, expectedIds }) => {
                const richTextField = shallowMount(OxRichTextField, {
                    propsData: {
                        options: { objectType: "sample_movie", field: "description" }
                    },
                    localVue
                }).vm

                expect(
                    richTextField["getMatchedHelpedTextIdsFromMatchedWords"](matchedIdsForWord as { [key: string]: Set<number> })
                ).toEqual(expectedIds)
            })

        test.each([
            {
                options: { objectType: "sample_movie", field: "description" },
                matchedIds: [3, 4],
                expectedHelpedText: [OxHelpedTextListMock[3], OxHelpedTextListMock[4]]
            },
            {
                options: { objectType: "sample_movie", field: "description" },
                matchedIds: [0, 1, 2, 3, 4, 5, 6, 7],
                expectedHelpedText: OxHelpedTextListMock
            },
            {
                options: { objectType: "sample_movie", field: "description" },
                matchedIds: [],
                expectedHelpedText: []
            },
            {
                options: { objectType: "sample_movie", field: "description", dependValue1: "scifi" },
                matchedIds: [3, 4],
                expectedHelpedText: []
            },
            {
                options: { objectType: "sample_movie", field: "description", dependValue1: "scifi" },
                matchedIds: [1, 4, 5],
                expectedHelpedText: [OxHelpedTextListMock[5]]
            },
            {
                options: { objectType: "sample_movie", field: "description", dependValue1: "scifi", dependValue2: "csa12" },
                matchedIds: [0, 1, 2, 3, 4, 5, 6, 7],
                expectedHelpedText: []
            },
            {
                options: { objectType: "sample_movie", field: "description", dependValue1: "drame", dependValue2: "csa12" },
                matchedIds: [0, 1, 2, 3, 4, 5, 6, 7],
                expectedHelpedText: [OxHelpedTextListMock[6]]
            },
            {
                options: { objectType: "sample_movie", field: "description", dependValue2: "csa12" },
                matchedIds: [0, 1, 2, 5, 6, 7],
                expectedHelpedText: [OxHelpedTextListMock[6], OxHelpedTextListMock[7]]
            }
        ])("testFilterOxHelpedText with options '$options' to match ids '$matchedIds'", ({ options, matchedIds, expectedHelpedText }) => {
            const richTextField = shallowMount(OxRichTextField, {
                propsData: {
                    options
                },
                localVue
            }).vm

            expect(richTextField["filterOxHelpedText"](OxHelpedTextListMock, matchedIds)).toEqual(expectedHelpedText)
        })

        test.each([
            {
                html: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                expectedText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            },
            {
                html: "Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit.",
                expectedText: "Lorem ipsum dolor sit amet,\n consectetur adipiscing elit."
            },
            {
                html: "<p>Lorem ipsum <em>dolor</em> sit amet,<br> consectetur <strong>adipiscing</strong> elit.</p>",
                expectedText: "Lorem ipsum dolor sit amet,\n consectetur adipiscing elit."
            }
        ])("testConvertHTMLToTextWithLineBreaks with $html to be $expectedText", ({ html, expectedText }) => {
            const richTextField = shallowMount(OxRichTextField, {
                propsData: {
                    options: { objectType: "sample_movie", field: "description" }
                },
                localVue
            }).vm

            const actual = richTextField["convertHTMLToTextWithLineBreaks"](html)

            expect(actual).toEqual(expectedText)
        })

        test.each([
            {
                type: "mediuser",
                title: "Lorem ipsum",
                text: "Lorem ipsum",
                settings: { objectType: "sample_movie", field: "description" }
            },
            {
                type: "function",
                title: "Lorem ipsum dolor",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                settings: { objectType: "sample_movie", field: "description", dependValue1: "scifi" }
            },
            {
                type: "group",
                title: "Et le ciel,",
                text: "Et le ciel, tout en bleu, devient rose avec douceur.",
                settings: { objectType: "sample_movie", field: "description", dependValue1: "drame", dependValue2: "csa12" }
            }
        ])("testAddHelpedText with type '$type' with settings '$settings'", async ({ type, title, text, settings }) => {
            const richTextField = shallowMount(OxRichTextField, {
                propsData: {
                    options: settings
                },
                localVue
            }).vm
            const helpedText = {
                _id: "",
                _type: "helped_text",
                _attributes: {
                    name: title,
                    text,
                    class: settings.objectType,
                    field: settings.field,
                    depend_value_1: settings.dependValue1,
                    depend_value_2: settings.dependValue2
                },
                _relationships: {
                    owner: {
                        data: {
                            type,
                            id: "1"
                        }
                    }
                }
            }
            const createJsonApiObjectsSpy = jest.spyOn(OxApiManager, "createJsonApiObjects").mockImplementation()
            const addInfoSpy = jest.spyOn(OxNotifyManager, "addInfo")
            // @ts-ignore
            const removeCacheSpy = jest.spyOn(richTextField, "removeHelpedTextFromCache").mockImplementation()
            // @ts-ignore
            jest.spyOn(richTextField, "convertHTMLToTextWithLineBreaks").mockReturnValue(text)
            await richTextField["addHelpedText"](type as "mediuser" | "function" |"group")
            expect(removeCacheSpy).toHaveBeenCalledTimes(1)
            expect(addInfoSpy).toHaveBeenCalledTimes(1)
            expect(addInfoSpy).toHaveBeenCalledWith("CAideSaisie-msg-create")
            expect(createJsonApiObjectsSpy).toHaveBeenCalledWith(
                expect.objectContaining(helpedText),
                expect.stringContaining(OxUrlDiscovery.createHelpedText())
            )
        })
    }
)
