/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxDatalist from "@/core/components/OxDatalist/OxDatalist.vue"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import pinia from "@/core/plugins/OxPiniaCore"
import OxTranslator from "@/core/plugins/OxTranslator"
import OxCollection from "@/core/models/OxCollection"
import { OxUrlBuilder } from "@/core/utils/OxUrlTools"
import { OxCollectionLinks, OxCollectionMeta } from "@/core/types/OxCollectionTypes"
import { storeSchemas } from "@/core/utils/OxStorage"
import { OxSchema } from "@/core/types/OxSchema"
import { setActivePinia } from "pinia"
import OxObject from "@/core/models/OxObject"
import { tr } from "@/core/utils/OxTranslator"
import oxApiService from "@/core/utils/OxApiService"
import OxTest from "@oxify/utils/OxTest"
import { cloneDeep } from "lodash"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

const mockPersonList = {
    data: [
        {
            type: "sample_person",
            id: "1",
            attributes: {
                last_name: "Doe",
                first_name: "John",
                is_director: false,
                birthdate: "1990-06-26",
                sex: "m",
                activity_start: "1998-03-03"
            },
            relationships: {
                nationality: {
                    data: {
                        type: "sample_nationality",
                        id: "17"
                    }
                }
            },
            links: {
                self: "/api/sample/persons/1",
                schema: "/api/schemas/sample_person?fieldsets=default,extra",
                history: "/api/history/sample_person/1",
                profile_picture: "?m=files&raw=thumbnail&document_id=36909&thumb=0"
            }
        },
        {
            type: "sample_person",
            id: "2",
            attributes: {
                last_name: "Harper",
                first_name: "Lily",
                is_director: false,
                birthdate: "1995-01-26",
                sex: "f",
                activity_start: "2005-05-03"
            },
            relationships: {
                nationality: {
                    data: {
                        type: "sample_nationality",
                        id: "113"
                    }
                }
            },
            links: {
                self: "/api/sample/persons/2",
                schema: "/api/schemas/sample_person?fieldsets=default,extra",
                history: "/api/history/sample_person/2",
                profile_picture: "?m=files&raw=thumbnail&document_id=36909&thumb=0"
            }
        }
    ],
    meta: {
        date: "2022-08-01 16:50:12+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2,
        total: 539
    },
    links: {
        self: "http://localhost/api/sample/persons?fieldsets=default,extra&limit=2&offset=2&permissions=true&relations=nationality&schema=true",
        next: "http://localhost/api/sample/persons?fieldsets=default,extra&limit=2&offset=4&permissions=true&relations=nationality&schema=true",
        first: "http://localhost/api/sample/persons?fieldsets=default,extra&limit=2&offset=0&permissions=true&relations=nationality&schema=true",
        last: "http://localhost/api/sample/persons?fieldsets=default,extra&limit=2&offset=538&permissions=true&relations=nationality&schema=true"
    }
}

/**
 * Test pour OxDatalist
 */
export default class OxDatalistTest extends OxTest {
    protected component = OxDatalist

    private columns = [
        {
            text: "Acteur",
            value: "fullName",
            filterValues: ["first_name", "last_name"]
        },
        {
            text: "R�alisateur",
            value: "isDirectorString",
            filterValues: "is_director"
        },
        {
            value: "sex"
        },
        {
            value: "birthdate"
        },
        {
            text: "Nationalit�",
            value: "nationality.name",
            filterValues: ""
        },
        {
            value: "activityStart"
        }
    ]

    private personsCollection = new OxCollection<SamplePerson>()
    private personsCollection2 = new OxCollection<SamplePerson>()
    private personsCollection3 = new OxCollection<SamplePerson>()
    private person1 = new SamplePerson()
    private person2 = new SamplePerson()
    private nationality = new SampleNationality()

    protected beforeAllTests () {
        super.beforeAllTests()
        const schema = [
            {
                id: "489f3046fbdf81481652a4b19b45a25c",
                owner: "sample_person",
                field: "last_name",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Nom",
                label: "Nom",
                description: "Nom de famille"
            },
            {
                id: "9b835cdbdf32d4ba811093336b069970",
                owner: "sample_person",
                field: "first_name",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Pr�nom",
                label: "Pr�nom",
                description: "Pr�nom"
            },
            {
                id: "0f18b60317532cf3493bc79132667421",
                owner: "sample_person",
                field: "is_director",
                type: "bool",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: "0",
                libelle: "R�alisateur",
                label: "R�alisateur",
                description: "Est un r�alisateur"
            },
            {
                id: "6f9a18d762968f4c7af7d50581bee48f",
                owner: "sample_person",
                field: "birthdate",
                type: "birthDate",
                fieldset: "extra",
                autocomplete: null,
                placeholder: "99/99/9999",
                notNull: null,
                confidential: null,
                default: null,
                libelle: "Date de naissance",
                label: "Naissance",
                description: "Date de naissance"
            },
            {
                id: "b187c3c5dca8287f28623e931943509a",
                owner: "sample_person",
                field: "sex",
                type: "enum",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                values: [
                    "m",
                    "f"
                ],
                translations: {
                    m: "Masculin",
                    f: "F�minin"
                },
                libelle: "Sexe",
                label: "Sexe",
                description: "Sexe"
            },
            {
                id: "1c1f00ed017758b7de04174e5a21177f",
                owner: "sample_person",
                field: "activity_start",
                type: "date",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                libelle: "D�but d'activit�",
                label: "D�but d'activit�",
                description: "Date de d�but d'activit�"
            }
        ] as unknown as OxSchema[]

        setActivePinia(pinia)

        this.nationality.id = "1"
        this.nationality.type = "sample_nationality"
        this.nationality.attributes = {
            name: "Nationality 1",
            code: "NA",
            flag: null
        }

        this.person1.id = "1"
        this.person1.type = "sample_person"
        this.person1.attributes = {
            last_name: "Lambertini",
            first_name: "Lucia",
            is_director: false,
            birthdate: "1926-06-26",
            sex: "f",
            activity_start: "1998-03-03"
        }
        this.person1.nationality = this.nationality
        this.person1.links = {
            self: "self",
            schema: "schema",
            history: "history",
            profile_picture: "profilepicture"
        }
        this.person1.meta = {
            permissions: {
                perm: "edit"
            }
        }

        this.person2.id = "2"
        this.person2.type = "sample_person"
        this.person2.attributes = {
            last_name: "Romance",
            first_name: "Viviane",
            is_director: true,
            birthdate: "1912-07-04",
            sex: "f",
            activity_start: "1998-03-03"
        }
        this.person2.nationality = this.nationality
        this.person2.links = {
            self: "self",
            schema: "schema",
            history: "history",
            profile_picture: "profilepicture"
        }
        this.person2.meta = {
            permissions: {
                perm: "edit"
            }
        }

        this.personsCollection.objects = [
            this.person1,
            this.person2
        ] as unknown as SamplePerson[]
        this.personsCollection.links = {
            self: "self",
            next: "next",
            first: "first",
            last: "last"
        } as OxCollectionLinks
        this.personsCollection.meta = {
            date: "2022-08-01 16:50:12+02:00",
            copyright: "OpenXtrem-2022",
            authors: "dev@openxtrem.com",
            count: 2,
            schema,
            total: 539
        } as OxCollectionMeta

        this.personsCollection2.objects = [
            this.person1,
            this.person2
        ] as unknown as SamplePerson[]
        this.personsCollection2.links = {
            self: "self",
            first: "first",
            last: "last"
        } as OxCollectionLinks
        this.personsCollection2.meta = {
            date: "2022-08-01 16:50:12+02:00",
            copyright: "OpenXtrem-2022",
            authors: "dev@openxtrem.com",
            count: 2,
            schema,
            total: 2
        } as OxCollectionMeta

        this.personsCollection3.objects = []
        this.personsCollection3.links = {
            self: "self",
            first: "first",
            last: "last"
        } as OxCollectionLinks
        this.personsCollection3.meta = {
            date: "2022-08-01 16:50:12+02:00",
            copyright: "OpenXtrem-2022",
            authors: "dev@openxtrem.com",
            count: 0,
            total: 0
        } as OxCollectionMeta

        storeSchemas(schema)
    }

    protected beforeTest () {
        super.beforeTest()

        jest.spyOn(oxApiService, "get")
            .mockImplementation(() => Promise.resolve({ data: mockPersonList }))
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue,
                pinia
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public async testWatchItemsData () {
        const datalist = this.mountComponent({ columns: this.columns, value: this.personsCollection })
        this.privateCall(datalist.vm, "inputDrag", [this.person2, this.person1])

        await datalist.vm.$nextTick()
        const collection = cloneDeep(this.personsCollection)
        collection.objects = [this.person2, this.person1]

        const events = datalist.emitted("input")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [collection]
        )
    }

    public testDatalistCreationDefault () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        this.assertEqual(datalist["itemsData"], [this.person1, this.person2])
        this.assertEqual(datalist["totalItems"], 539)
    }

    public testDatalistCreationWithNotEmptyCollection () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        this.assertEqual(datalist["objectClass"], SamplePerson)
        this.assertEqual(datalist["resourceType"], "sample_person")
    }

    public testDatalistCreationWithEmptyCollection () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection3, oxObject: SamplePerson })

        this.assertEqual(datalist["objectClass"], SamplePerson)
        this.assertEqual(datalist["resourceType"], "sample_person")
    }

    public testDatalistCreationWithEmptyCollectionAndNoOxObjectProp () {
        /// Ignore console.error from created
        window.console.error = jest.fn()
        expect(() => this.vueComponent({ columns: this.columns, value: this.personsCollection3 }))
            .toThrowError(new Error("Impossible type inference"))
    }

    public testDatalistClassesDefault () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })
        this.assertEqual(this.privateCall(datalist, "datalistClasses"), "OxDatalist-table")
    }

    public async testDatalistClassesWithMassActionsEnabled () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        datalist["showMassActions"] = true

        this.assertEqual(
            this.privateCall(datalist, "datalistClasses"),
            "massActionsEnabled OxDatalist-table"
        )
    }

    public testSearchLabelTextDefault () {
        const datalist = this.vueComponent({
            columns: this.columns,
            value: this.personsCollection
        })
        this.assertEqual(this.privateCall(datalist, "searchLabelText"), "common-search")
    }

    public testSearchLabelTextCustom () {
        const datalist = this.vueComponent({
            columns: this.columns,
            value: this.personsCollection,
            searchLabel: "Search label"
        })
        this.assertEqual(this.privateCall(datalist, "searchLabelText"), "Search label")
    }

    public testHeaders () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection, showActions: true })
        const headers = [
            {
                class: "OxDatalist-tableHeader",
                cellClass: "OxDatalist-tableCell",
                groupable: false,
                sortable: false,
                text: "Acteur",
                value: "fullName"
            },
            {
                class: "OxDatalist-tableHeader",
                cellClass: "OxDatalist-tableCell",
                groupable: false,
                sortable: false,
                text: "R�alisateur",
                value: "isDirectorString"
            },
            {
                class: "OxDatalist-tableHeader",
                cellClass: "OxDatalist-tableCell",
                groupable: false,
                sortable: false,
                text: "Sexe",
                value: "sex"
            },
            {
                class: "OxDatalist-tableHeader",
                cellClass: "OxDatalist-tableCell",
                groupable: false,
                sortable: false,
                text: "Date de naissance",
                value: "birthdate"
            },
            {
                class: "OxDatalist-tableHeader",
                cellClass: "OxDatalist-tableCell",
                groupable: false,
                sortable: false,
                text: "Nationalit�",
                value: "nationality.name"
            },
            {
                class: "OxDatalist-tableHeader",
                cellClass: "OxDatalist-tableCell",
                groupable: false,
                sortable: false,
                text: "D�but d'activit�",
                value: "activityStart"
            },
            {
                cellClass: "OxDatalist-tableCell OxDatalist-tableActions",
                text: "",
                value: "actions",
                sortable: false,
                groupable: false
            }
        ]

        this.assertEqual(this.privateCall(datalist, "headers"), headers)
    }

    public testSelectionTextSingular () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })
        this.assertEqual(this.privateCall(datalist, "selectionText"), "common-selection")
    }

    public async testSelectionTextPlural () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        datalist["selectedItems"] = [this.person1, this.person2]

        this.assertEqual(
            this.privateCall(datalist, "selectionText"),
            "common-selection|pl"
        )
    }

    public testSearchFieldCancelIcon () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })
        datalist["search"] = "my search"
        this.assertEqual(this.privateCall(datalist, "searchFieldIcon"), "cancel")
    }

    public testSearchFieldSearchIcon () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })
        this.assertEqual(this.privateCall(datalist, "searchFieldIcon"), "search")
    }

    public testDefaultNoDataText () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })
        this.assertEqual(this.privateCall(datalist, "customableNoDataText"), "common-No data")
    }

    public testCustomNoDataText () {
        const datalist = this.vueComponent({ columns: this.columns, noDataText: "No data custom", value: this.personsCollection })
        this.assertEqual(this.privateCall(datalist, "customableNoDataText"), "No data custom")
    }

    public testDraggableAnimationNameIsDrag () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })
        datalist["drag"] = true
        this.assertEqual(this.privateCall(datalist, "draggableAnimationName"), null)
    }

    public testDraggableAnimationNameIsNotDrag () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })
        this.assertEqual(this.privateCall(datalist, "draggableAnimationName"), "flip-list")
    }

    public async testRefreshNoUrl () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        // @ts-ignore
        const spy = jest.spyOn(datalist, "refreshRequest")

        const newUrl = new OxUrlBuilder(this.personsCollection.self)
        await this.privateCall(datalist, "refresh")

        expect(spy).toBeCalledWith(newUrl)
    }

    public async testRefreshAddingSearch () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        // @ts-ignore
        const spy = jest.spyOn(datalist, "refreshRequest")

        const newUrl = new OxUrlBuilder(this.personsCollection.self + "&search=a")
        await this.privateCall(datalist, "refresh", newUrl)

        expect(spy).toBeCalledWith(newUrl)
    }

    public async testRefreshAddingFilters () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        // @ts-ignore
        const spy = jest.spyOn(datalist, "refreshRequest")

        const newUrl = new OxUrlBuilder(this.personsCollection.self + "&filter=is_director.equal.1")
        await this.privateCall(datalist, "refresh", newUrl)

        expect(spy).toBeCalledWith(newUrl)
    }

    public async testRefreshRemovingSearch () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        // @ts-ignore
        const spy = jest.spyOn(datalist, "refreshRequest")
        datalist["refreshUrl"] = new OxUrlBuilder(this.personsCollection.self + "&search=a")

        const newUrl = new OxUrlBuilder(this.personsCollection.self)
        await this.privateCall(datalist, "refresh", newUrl)

        expect(spy).toBeCalledWith(newUrl)
    }

    public async testRefreshRemovingFilters () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection })

        // @ts-ignore
        const spy = jest.spyOn(datalist, "refreshRequest")
        datalist["refreshUrl"] = new OxUrlBuilder(this.personsCollection.self + "&filter=is_director.equal.1")

        const newUrl = new OxUrlBuilder(this.personsCollection.self)
        await this.privateCall(datalist, "refresh", newUrl)

        expect(spy).toBeCalledWith(newUrl)
    }

    public testRowItemClassesStripped () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2, stripped: true })
        this.assertEqual(this.privateCall(datalist, "rowItemClasses"), "OxDatalist-tableRow stripped")
    }

    public testRowItemClassesNotStripped () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        this.assertEqual(this.privateCall(datalist, "rowItemClasses"), "OxDatalist-tableRow")
    }

    public testRowItemClassesDragged () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        datalist["drag"] = true
        datalist["currentIndex"] = 1
        this.assertEqual(
            this.privateCall(datalist, "rowItemClasses", 1),
            "OxDatalist-tableRow dragged"
        )
    }

    public testRowItemClassesDragActive () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        datalist["drag"] = true
        datalist["currentIndex"] = 1
        this.assertEqual(
            this.privateCall(datalist, "rowItemClasses", 2),
            "OxDatalist-tableRow dragActive"
        )
    }

    public testItemValueDefault () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "itemValue", this.person1, "fullName")).toBe("Lucia Lambertini")
    }

    public testItemValueRelationship () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "itemValue", this.person1, "nationality.name")).toBe("Nationality 1")
    }

    public testIsActionsColumn () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "isActionsColumn", "actions")).toBe(true)
    }

    public testIsNotActionsColumn () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "isActionsColumn", "notactions")).toBe(false)
    }

    public testIsCheckboxIndeterminate () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "isCheckboxIndeterminate", true, false)).toBe(true)
    }

    public testIsNotCheckboxIndeterminateCauseEveryItem () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "isCheckboxIndeterminate", true, true)).toBe(false)
    }

    public testIsNotCheckboxIndeterminateCauseNotSomeItems () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "isCheckboxIndeterminate", false, true)).toBe(false)
    }

    public testShowDraggableIconWithoutSelect () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "showDraggableIconWithoutSelect", 0)).toBe(true)
    }

    public testNotShowDraggableIconWithoutSelectCauseIndex () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2 })
        expect(this.privateCall(datalist, "showDraggableIconWithoutSelect", 1)).toBe(false)
    }

    public testNotShowDraggableIconWithoutSelectCauseShowSelect () {
        const datalist = this.vueComponent({ columns: this.columns, value: this.personsCollection2, showSelect: true })
        expect(this.privateCall(datalist, "showDraggableIconWithoutSelect", 0)).toBe(false)
    }

    public testUpdateItemEvent (): void {
        const datalist = this.mountComponent({ columns: this.columns, value: this.personsCollection2 })
        this.privateCall(datalist.vm, "updateItem", this.person1)

        const events = datalist.emitted("updateItem")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [this.person1]
        )
    }

    public testDeleteItemEvent (): void {
        const datalist = this.mountComponent({ columns: this.columns, value: this.personsCollection2 })
        this.privateCall(datalist.vm, "deleteItem", this.person1)

        const events = datalist.emitted("deleteItem")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [this.person1]
        )
    }

    public testFilterItemsEvent (): void {
        const datalist = this.mountComponent({ columns: this.columns, value: this.personsCollection })
        this.privateCall(datalist.vm, "filterItems", ["filter1"])

        const events = datalist.emitted("filterItems")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [["filter1"]]
        )
    }

    public testInputEvent (): void {
        const datalist = this.mountComponent({ columns: this.columns, value: this.personsCollection })
        this.privateCall(datalist.vm, "input", [this.person1])

        this.assertEqual(
            datalist.vm["selectedItems"],
            [this.person1]
        )

        this.assertEqual(
            datalist.vm["showMassActions"],
            true
        )

        const events = datalist.emitted("selectedItems")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [[this.person1]]
        )
    }

    public testStartDrag () {
        const datalist = this.mountComponent({ columns: this.columns, value: this.personsCollection })
        const evt = { oldIndex: 1 }
        this.privateCall(datalist.vm, "startDrag", evt)
        expect(datalist.vm["drag"]).toBe(true)
        expect(datalist.vm["currentIndex"]).toBe(1)

        const events = datalist.emitted("startDrag")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [evt]
        )
    }

    public testEndDrag () {
        const datalist = this.mountComponent({ columns: this.columns, value: this.personsCollection })
        const evt = { oldIndex: 1 }
        this.privateCall(datalist.vm, "endDrag", evt)
        expect(datalist.vm["drag"]).toBe(false)

        const events = datalist.emitted("endDrag")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [evt]
        )
    }

    public testInputDrag () {
        const datalist = this.mountComponent({ columns: this.columns, value: this.personsCollection })
        this.privateCall(datalist.vm, "inputDrag", [this.person1, this.person2])
        expect(datalist.vm["itemsData"]).toMatchObject([this.person1, this.person2])
    }
}

class SamplePerson extends OxObject {
    constructor () {
        super()
        this.type = "sample_person"
    }

    protected _relationsTypes = {
        sample_nationality: SampleNationality
    }

    get firstName (): string {
        return super.get("first_name")
    }

    set firstName (value: string) {
        super.set("first_name", value)
    }

    get lastName (): string {
        return super.get("last_name")
    }

    set lastName (value: string) {
        super.set("last_name", value)
    }

    get fullName (): string {
        return this.firstName + " " + this.lastName
    }

    get isDirector (): boolean {
        return super.get("is_director")
    }

    set isDirector (value: boolean) {
        super.set("is_director", value)
    }

    get isDirectorString (): string {
        return this.isDirector ? tr("CSamplePerson.is_director.y") : tr("CSamplePerson.is_director.n")
    }

    get birthdate (): string {
        return this.birthdateData ? new Date(this.birthdateData).toLocaleDateString("fr") : ""
    }

    get birthdateData (): string {
        return super.get("birthdate")
    }

    set birthdateData (value: string) {
        super.set("birthdate", value)
    }

    get sex (): string {
        return super.get("sex")
    }

    set sex (value: string) {
        super.set("sex", value)
    }

    get sexIcon (): string | undefined {
        if (!this.sex) {
            return undefined
        }

        return this.sex === "m" ? "male" : "female"
    }

    get activityStart (): number | string {
        return this.activityStartData ? new Date(this.activityStartData).getFullYear() : ""
    }

    get activityStartData (): string {
        return super.get("activity_start")
    }

    set activityStartData (value: string) {
        super.set("activity_start", value)
    }

    get profilePicture (): string | undefined {
        return this.links.profile_picture
    }

    get nationality (): SampleNationality | undefined {
        return this.loadForwardRelation<SampleNationality>("nationality")
    }

    set nationality (value: SampleNationality | undefined) {
        this.setForwardRelation("nationality", value)
    }
}

class SampleNationality extends OxObject {
    constructor () {
        super()
        this.type = "sample_nationality"
    }

    get name (): string {
        if (!this.attributes.name) {
            return ""
        }
        return this.attributes.name.charAt(0).toUpperCase() + this.attributes.name.slice(1)
    }

    get code (): string {
        return this.attributes.code
    }

    get flag (): string {
        return this.attributes.flag
    }
}

(new OxDatalistTest()).launchTests()
