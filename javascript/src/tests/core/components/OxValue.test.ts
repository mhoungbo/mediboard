/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { shallowMount } from "@vue/test-utils"
import OxValue from "@/core/components/OxValue/OxValue.vue"
import { OxSchema } from "@/core/types/OxSchema"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import { storeSchemas } from "@/core/utils/OxStorage"

/* eslint-disable dot-notation */

describe(
    "OxValue",
    () => {
        beforeAll(() => {
            const schema = [
                {
                    id: "489f3046fbdf81481652a4b19b45a25c",
                    owner: "test_object",
                    field: "field_str",
                    type: "str",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    notNull: true,
                    confidential: null,
                    default: null,
                    libelle: "Field str",
                    label: "Field str",
                    description: "Field str description"
                },
                {
                    id: "efd8ccabedcdca0b495941110eb85d63",
                    owner: "test_object",
                    field: "field_text",
                    type: "text",
                    fieldset: "default",
                    autocomplete: null,
                    placeholder: null,
                    markdown: true,
                    notNull: true,
                    confidential: null,
                    default: null,
                    libelle: "Field text",
                    label: "Field text",
                    description: "Field text description"
                }
            ] as unknown as OxSchema[]

            setActivePinia(pinia)
            storeSchemas(schema)
        })
        test("Field with type text and defined as markdown is displayed as markdown", () => {
            const component = shallowMount(OxValue, {
                propsData: {
                    resourceName: "test_object",
                    fieldName: "field_text",
                    value: "value"
                }
            })
            expect(component.vm["isMarkdown"]).toBe(true)
        })
        test("Fields not defined as markdown are displayed with OxBeautify", () => {
            const component = shallowMount(OxValue, {
                propsData: {
                    resourceName: "test_object",
                    fieldName: "field_str",
                    value: "value"
                }
            })
            expect(component.vm["isMarkdown"]).toBe(false)
        })
    }
)
