/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxInfiniteScroll from "@/core/components/OxInfiniteScroll/OxInfiniteScroll.vue"
import Vuetify from "vuetify"
import { setActivePinia } from "pinia"
import { OxSchema } from "@/core/types/OxSchema"
import pinia from "@/core/plugins/OxPiniaCore"
import { storeSchemas } from "@/core/utils/OxStorage"
import OxCollection from "@/core/models/OxCollection"
import OxObject from "@/core/models/OxObject"
import { OxCollectionLinks, OxCollectionMeta } from "@/core/types/OxCollectionTypes"
import oxApiService from "@/core/utils/OxApiService"
import OxTranslator from "@/core/plugins/OxTranslator"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

// @ts-ignore
global.ResizeObserver = jest.fn().mockImplementation(() => ({
    observe: jest.fn(),
    disconnect: jest.fn()
}))

const mockList = {
    data: [
        {
            type: "test_object",
            id: "3",
            attributes: {
                field_str: "Test 3",
                field_date: "01/01/1980",
                field_time: "01:00:00"
            }
        },
        {
            type: "test_object",
            id: "4",
            attributes: {
                field_str: "Test 4",
                field_date: "01/01/1990",
                field_time: "01:30:00"
            }
        }
    ],
    meta: {
        date: "2022-09-12 11:07:50+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2,
        total: 4
    },
    links: {
        self: "http://localhost/mediboard/api/sample/movies?limit=2&offset=0",
        first: "http://localhost/mediboard/api/sample/movies?limit=2&offset=0",
        last: "http://localhost/mediboard/api/sample/movies?limit=2&offset=2"
    }
}
jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: mockList }))

/**
 * Test for OxInfiniteScroll
 */
export default class OxInfiniteScrollTest extends OxTest {
    protected component = OxInfiniteScroll

    private collection1 = new OxCollection()
    private collection2 = new OxCollection()
    private collection3 = new OxCollection()
    private testObject1 = new TestObject()
    private testObject2 = new TestObject()
    private testObject3 = new TestObject()

    protected beforeAllTests () {
        super.beforeAllTests()
        const schema = [
            {
                id: "489f3046fbdf81481652a4b19b45a25c",
                owner: "test_object",
                field: "field_str",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Field str",
                label: "Field str",
                description: "Field str description"
            },
            {
                id: "1c1f00ed017758b7de04174e5a21177f",
                owner: "test_object",
                field: "field_date",
                type: "date",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                libelle: "Field date",
                label: "Field date",
                description: "Field date description"
            },
            {
                id: "efd8ccabedcdca0b495941110eb85d63",
                owner: "test_object",
                field: "field_time",
                type: "time",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Field time",
                label: "Field time",
                description: "Field time description"
            }
        ] as unknown as OxSchema[]

        this.testObject1.id = "1"
        this.testObject1.type = "test_object"
        this.testObject1.attributes = {
            field_str: "Test 1",
            field_date: "01/01/1980",
            field_time: "01:00:00"
        }

        this.testObject2.id = "2"
        this.testObject2.type = "test_object"
        this.testObject2.attributes = {
            field_str: "Test 2",
            field_date: "01/01/1990",
            field_time: "01:30:00"
        }

        this.testObject3.id = "3"
        this.testObject3.type = "test_object"
        this.testObject3.attributes = {
            field_str: "Test 3",
            field_date: "01/01/1995",
            field_time: "01:33:00"
        }

        this.collection1.objects = [
            this.testObject1,
            this.testObject2
        ] as unknown as TestObject[]
        this.collection1.meta = {
            count: 2,
            total: 3,
            schema
        } as OxCollectionMeta
        this.collection1.links = {
            self: "http://localhost/api/sample/tests?offset=0&schema=true",
            next: "http://localhost/api/sample/tests?limit=2&offset=2&schema=true",
            first: "http://localhost/api/sample/tests?limit=2&offset=0&schema=true",
            last: "http://localhost/api/sample/tests?limit=2&offset=2&schema=true"
        } as OxCollectionLinks

        this.collection2.objects = []
        this.collection2.meta = {
            count: 0,
            total: 0,
            schema
        } as OxCollectionMeta
        this.collection2.links = {
            self: "http://localhost/api/sample/tests?offset=0&schema=true"
        } as OxCollectionLinks

        this.collection3.objects = [
            this.testObject1,
            this.testObject2
        ] as unknown as TestObject[]
        this.collection3.meta = {
            count: 2,
            total: 20,
            schema
        } as OxCollectionMeta
        this.collection3.links = {
            self: "http://localhost/api/sample/tests?limit=2&offset=18&schema=true",
            prev: "http://localhost/api/sample/tests?limit=2&offset=16&schema=true",
            first: "http://localhost/api/sample/tests?limit=2&offset=0&schema=true",
            last: "http://localhost/api/sample/tests?limit=2&offset=1&schema=true"
        } as OxCollectionLinks

        setActivePinia(pinia)
        storeSchemas(schema)
    }

    protected beforeTest () {
        super.beforeTest()
        Object.defineProperty(Element.prototype, "scrollHeight", {
            value: 3000
        })
        Object.defineProperty(Element.prototype, "clientHeight", {
            value: 800
        })
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                vuetify: new Vuetify(),
                localVue,
                pinia
            }
        )
    }

    public testHasObjectClass () {
        const infScroll = this.mountComponent({ value: this.collection1 })

        this.assertTrue(this.privateCall(infScroll.vm, "hasObjectClass"))
    }

    public testHasNotObjectClass () {
        // @ts-ignore, Ignore console.warn from created
        window.console.warn = jest.fn()
        const infScroll = this.mountComponent({ value: this.collection2 })
        expect(window.console.warn).toBeCalled()
        this.assertFalse(this.privateCall(infScroll.vm, "hasObjectClass"))
    }

    public async testScrollWithoutLoadNewCollection () {
        Object.defineProperty(Element.prototype, "scrollTop", {
            value: 1900
        })
        const infScroll = this.mountComponent({ value: this.collection1, bottomPxTrigger: 200 })
        await infScroll.vm.$nextTick()
        // @ts-ignore
        const spy = jest.spyOn(infScroll.vm, "loadNextCollection")
        await this.privateCall(infScroll.vm, "scroll")
        expect(spy).not.toHaveBeenCalled()
    }

    public async testScrollLoadNewCollection () {
        Object.defineProperty(Element.prototype, "scrollTop", {
            value: 2100
        })
        const infScroll = this.mountComponent({ value: this.collection1, bottomPxTrigger: 200 })
        await infScroll.vm.$nextTick()
        // @ts-ignore
        const spy = jest.spyOn(infScroll.vm, "loadNextCollection")
        await this.privateCall(infScroll.vm, "scroll")
        expect(spy).toHaveBeenCalled()
    }

    public async testIsScrollable () {
        const infScroll = this.mountComponent({ value: this.collection1 })
        await infScroll.vm.$nextTick()
        this.assertTrue(this.privateCall(infScroll.vm, "isScrollable"))
    }

    public async testIsNotScrollable () {
        Object.defineProperty(Element.prototype, "scrollHeight", {
            value: 750
        })
        const infScroll = this.mountComponent({ value: this.collection3 })
        await infScroll.vm.$nextTick()
        this.assertFalse(this.privateCall(infScroll.vm, "isScrollable"))
    }

    public async testNotLoadNextCollection () {
        const infScroll = this.mountComponent({ value: this.collection3 })
        await infScroll.vm.$nextTick()
        const result = await this.privateCall(infScroll.vm, "loadNextCollection")
        expect(result).toBe(true)
    }

    public async testLoadNextCollection () {
        const infScroll = this.mountComponent({ value: this.collection1 })
        await infScroll.vm.$nextTick()
        const result = await this.privateCall(infScroll.vm, "loadNextCollection")

        expect(oxApiService.get).toBeCalledWith(expect.stringContaining("tests?limit=2&offset=2&schema=true"))
        const events = infScroll.emitted("input")
        this.assertHaveLength(events, 1)
        expect(result).toBe(false)
    }

    public async testDestroy (): Promise<void> {
        const infScroll = this.mountComponent({ value: this.collection1 })
        await infScroll.vm.$nextTick()
        Object.defineProperty(infScroll.vm["containerElement"], "removeEventListener", {
            value: jest.fn()
        })
        await infScroll.destroy()
        expect(infScroll.vm["containerElement"].removeEventListener).toHaveBeenCalled()
        expect(infScroll.vm["containerElement"].removeEventListener).toHaveBeenCalledWith(
            "scroll",
            infScroll.vm["scroll"]
        )
    }

    public async testMakeContainerScrollable () {
        const element = document.createElement("div")
        const infScroll = this.mountComponent({ value: this.collection1, element })
        await infScroll.vm.$nextTick()

        expect(element.style.overflow).toBe("auto")
    }

    public async testEmitsEventsToIndicateLoading () {
        const infScroll = this.mountComponent({ value: this.collection1 })
        await infScroll.vm.$nextTick()
        await this.privateCall(infScroll.vm, "loadNextCollection")

        expect(infScroll.emitted("loading")).toEqual([[true], [false]])
    }

    public async testNotLoadingNextCollectionIfAlreadyLoading () {
        const infScroll = this.mountComponent({ value: this.collection1 })
        await infScroll.vm.$nextTick()
        infScroll.vm["loading"] = true
        const result = await infScroll.vm["loadNextCollection"]()

        expect(result).toBe(false)
        expect(oxApiService.get).not.toBeCalled()
    }
}

class TestObject extends OxObject {
    constructor () {
        super()
        this.type = "test_object"
    }
}

(new OxInfiniteScrollTest()).launchTests()
