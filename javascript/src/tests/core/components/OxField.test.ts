/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxField from "@/core/components/OxField/OxField.vue"
import Vuetify from "vuetify"
import { setActivePinia } from "pinia"
import { OxSchema } from "@/core/types/OxSchema"
import pinia from "@/core/plugins/OxPiniaCore"
import { storeSchemas } from "@/core/utils/OxStorage"
import OxTranslator from "@/core/plugins/OxTranslator"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * Test for OxField
 */
export default class OxFieldTest extends OxTest {
    protected component = OxField

    protected beforeAllTests () {
        super.beforeAllTests()
        const schema = [
            {
                id: "489f3046fbdf81481652a4b19b45a25c",
                owner: "sample_test",
                field: "field_str",
                type: "str",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Field str",
                label: "Field str",
                description: "Field str description"
            },
            {
                id: "489f3046fbdf81481652a4b19b45a25j",
                owner: "sample_test",
                field: "field_text",
                type: "text",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                markdown: false,
                helped: false,
                confidential: null,
                default: null,
                libelle: "Field text",
                label: "Field text",
                description: "Field text description"
            },
            {
                id: "489f3046fbdf81481652a4b19b45a25j",
                owner: "sample_test",
                field: "field_text_markdown_helped",
                type: "text",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                markdown: true,
                helped: true,
                confidential: null,
                default: null,
                libelle: "Field text",
                label: "Field text",
                description: "Field text description"
            },
            {
                id: "0f18b60317532cf3493bc79132667421",
                owner: "sample_test",
                field: "field_bool",
                type: "bool",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: "0",
                libelle: "Field bool",
                label: "Field bool",
                description: "Field bool description"
            },
            {
                id: "6f9a18d762968f4c7af7d50581bee48f",
                owner: "sample_test",
                field: "field_birthdate",
                type: "birthDate",
                fieldset: "extra",
                autocomplete: null,
                placeholder: "99/99/9999",
                notNull: null,
                confidential: null,
                default: null,
                libelle: "Field birthDate",
                label: "Field birthDate",
                description: "Field birthDate description"
            },
            {
                id: "1c1f00ed017758b7de04174e5a21177f",
                owner: "sample_test",
                field: "field_date",
                type: "date",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                libelle: "Field date",
                label: "Field date",
                description: "Field date description"
            },
            {
                id: "efd8ccabedcdca0b495941110eb85d63",
                owner: "sample_test",
                field: "field_time",
                type: "time",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: true,
                confidential: null,
                default: null,
                libelle: "Field time",
                label: "Field time",
                description: "Field time description"
            },
            {
                id: "b187c3c5dca8287f28623e931943509a",
                owner: "sample_test",
                field: "field_enum",
                type: "enum",
                fieldset: "extra",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                values: [
                    "value1",
                    "value2"
                ],
                translations: {
                    value1: "Value 1",
                    value2: "Value 2"
                },
                libelle: "Field enum",
                label: "Field enum",
                description: "Field enum description"
            },
            {
                id: "99457059e40536032606aff4a4d8ff33",
                owner: "sample_test",
                field: "field_set",
                type: "set",
                fieldset: "default",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: "value1",
                values: [
                    "value1",
                    "value2",
                    "value3"
                ],
                translations: {
                    value1: "Value 1",
                    value2: "Value 2",
                    value3: "Value 3"
                },
                libelle: "Field set",
                label: "Field set",
                description: "Field set description"
            }
        ] as unknown as OxSchema[]

        setActivePinia(pinia)
        storeSchemas(schema)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {
            OxRichTextField: true,
            OxTextField: true,
            OxTextarea: true,
            OxCheckbox: true,
            OxSelect: true,
            OxDatepicker: true,
            OxRadioGroup: true,
            OxRadio: true
        },
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                vuetify: new Vuetify(),
                localVue,
                pinia
            }
        )
    }

    public testFieldComponentTypeStr () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-text-field")
    }

    public testFieldComponentTypeText () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text",
            value: "Doe"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-textarea")
    }

    public testFieldComponentTypeRichText () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text_markdown_helped",
            value: "Doe"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-rich-text-field")
    }

    public testFieldComponentCustomTypeDuration () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_time",
            value: "01:00",
            customSchema: { type: "duration" }
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-text-field")
    }

    public testFieldComponentTypeDate () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_date",
            value: "01/01/2022"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-datepicker")
    }

    public testFieldComponentTypeBirthDate () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_birthdate",
            value: "01/01/2022"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-datepicker")
    }

    public testFieldComponentTypeTime () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_time",
            value: "01:00:00"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-datepicker")
    }

    public testFieldComponentTypeSet () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_set",
            value: "value1"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-select")
    }

    public testFieldDefaultComponent () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_enum",
            value: "value1"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldComponent"), "ox-text-field")
    }

    public testFieldDefaultValue () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_set",
            value: undefined
        })
        this.assertEqual(this.privateCall(field.vm, "fieldValue"), "value1")
    }

    public testFieldDefinedValue () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldValue"), "Doe")
    }

    public testFieldNullValue () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: null
        })
        this.assertEqual(this.privateCall(field.vm, "fieldValue"), null)
    }

    public testFieldDefaultLibelle () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldWording"), "Field str")
    }

    public testFieldCustomLibelle () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe",
            customSchema: { libelle: "Custom libelle" }
        })
        this.assertEqual(this.privateCall(field.vm, "fieldWording"), "Custom libelle")
    }

    public testFieldDefaultPlaceholder () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_birthdate",
            value: "01/01/2022"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldPlaceholder"), "99/99/9999")
    }

    public testFieldCustomPlaceholder () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe",
            customSchema: { placeholder: "Custom placeholder" }
        })
        this.assertEqual(this.privateCall(field.vm, "fieldPlaceholder"), "Custom placeholder")
    }

    public testFieldIsNotNullableInSchema () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertTrue(this.privateCall(field.vm, "isNotNull"))
    }

    public testFieldNotNullIsOverride () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe",
            customSchema: { notNull: false }
        })
        this.assertFalse(this.privateCall(field.vm, "isNotNull"))
    }

    public testFieldIsNotNullableCustom () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_bool",
            value: "Doe",
            customSchema: { notNull: true }
        })
        this.assertTrue(this.privateCall(field.vm, "isNotNull"))
    }

    public testFieldIsNullable () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_bool",
            value: "Doe"
        })
        this.assertFalse(this.privateCall(field.vm, "isNotNull"))
    }

    public testFieldNullableisOverride () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_bool",
            value: "Doe",
            customSchema: { notNull: true }
        })
        this.assertTrue(this.privateCall(field.vm, "isNotNull"))
    }

    public testFieldIsNotMarkdownInSchema () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text",
            value: "Doe"
        })
        expect(this.privateCall(field.vm, "isMarkdown")).toBeFalsy()
    }

    public testFieldIsMarkdownOverride () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text",
            value: "Doe",
            customSchema: { markdown: true }
        })
        expect(this.privateCall(field.vm, "isMarkdown")).toBeTruthy()
    }

    public testFieldIsMarkdownInSchema () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text_markdown_helped",
            value: "Doe"
        })
        expect(this.privateCall(field.vm, "isMarkdown")).toBeTruthy()
    }

    public testFieldIsNotMarkdownOverride () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_bool",
            value: "Doe",
            customSchema: { markdown: false }
        })
        expect(this.privateCall(field.vm, "isMarkdown")).toBeFalsy()
    }

    public testFieldIsNotHelpedInSchema () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text",
            value: "Doe"
        })
        expect(this.privateCall(field.vm, "isHelped")).toBeFalsy()
    }

    public testFieldIsHelpedOverride () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text",
            value: "Doe",
            customSchema: { helped: true }
        })
        expect(this.privateCall(field.vm, "isHelped")).toBeTruthy()
    }

    public testFieldIsHelpedInSchema () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text_markdown_helped",
            value: "Doe"
        })
        expect(this.privateCall(field.vm, "isHelped")).toBeTruthy()
    }

    public testFieldIsNotHelpedOverride () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_text_markdown_helped",
            value: "Doe",
            customSchema: { helped: false }
        })
        expect(this.privateCall(field.vm, "isHelped")).toBeFalsy()
    }

    public testFieldDefaultDescription () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertEqual(this.privateCall(field.vm, "fieldDescription"), "Field str description")
    }

    public testFieldCustomDescription () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe",
            customSchema: { description: "Custom description" }
        })
        this.assertEqual(this.privateCall(field.vm, "fieldDescription"), "Custom description")
    }

    public testFieldFormatDateForTypeDate () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_date",
            value: "01/01/2022"
        })
        this.assertEqual(this.privateCall(field.vm, "formatDate"), "date")
    }

    public testFieldFormatDateForTypeBirthDate () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_birthdate",
            value: "01/01/2022"
        })
        this.assertEqual(this.privateCall(field.vm, "formatDate"), "date")
    }

    public testFieldFormatDateForTypeTime () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_time",
            value: "01:00:00"
        })
        this.assertEqual(this.privateCall(field.vm, "formatDate"), "time")
    }

    public testFieldListEmpty () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertEqual(this.privateCall(field.vm, "list"), [])
    }

    public testFieldListNotEmpty () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_set",
            value: "value1"
        })
        this.assertEqual(
            this.privateCall(field.vm, "list"),
            [
                {
                    _id: "value1",
                    view: "Value 1"
                },
                {
                    _id: "value2",
                    view: "Value 2"
                },
                {
                    _id: "value3",
                    view: "Value 3"
                }
            ]
        )
    }

    public testFieldHasMultipleSelectOption () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_set",
            value: "value1",
            options: { multiple: true }
        })
        this.assertTrue(this.privateCall(field.vm, "multipleSelect"))
    }

    public testFieldIsNotMultipleSelectByValue () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_set",
            value: "value1"
        })
        this.assertFalse(this.privateCall(field.vm, "multipleSelect"))
    }

    public testFieldIsNotMultipleSelectByValueType () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_bool",
            value: true
        })
        this.assertFalse(this.privateCall(field.vm, "multipleSelect"))
    }

    public testFieldIsMultipleSelectByValue () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_set",
            value: "value1|value2"
        })
        this.assertTrue(this.privateCall(field.vm, "multipleSelect"))
    }

    public testFieldIsRadio () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_enum",
            value: "value1"
        })
        this.assertTrue(this.privateCall(field.vm, "showRadio"))
    }

    public testFieldIsNotRadio () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertFalse(this.privateCall(field.vm, "showRadio"))
    }

    public testFieldIsCheckbox () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_bool",
            value: "1"
        })
        this.assertTrue(this.privateCall(field.vm, "showCheckbox"))
    }

    public testFieldIsNotCheckbox () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertFalse(this.privateCall(field.vm, "showCheckbox"))
    }

    public testDefinedInputMask () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_time",
            value: "01:00",
            customSchema: { type: "duration" }
        })
        this.assertEqual(this.privateCall(field.vm, "inputMask"), "##:##")
    }

    public testUndefinedInputMask () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.assertUndefined(this.privateCall(field.vm, "inputMask"))
    }

    public testChangeEvent () {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        const expectedNewValue = "new value"
        this.privateCall(field.vm, "change", expectedNewValue)

        const events = field.emitted("input")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }

    public testKeyup (): void {
        const field = this.mountComponent({
            ready: true,
            resourceName: "sample_test",
            fieldName: "field_str",
            value: "Doe"
        })
        this.privateCall(
            field.vm,
            "keyup"
        )
        this.assertHaveLength(field.emitted("keyup"), 1)
    }
}

(new OxFieldTest()).launchTests()
