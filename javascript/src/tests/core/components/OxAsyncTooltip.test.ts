/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import oxApiService from "@/core/utils/OxApiService"
import OxAsyncTooltip from "@/core/components/OxAsyncTooltip/OxAsyncTooltip.vue"
import OxObject from "@/core/models/OxObject"

/* eslint-disable dot-notation */

/**
 * OxAsyncTooltip tests
 */
export default class OxAsyncTooltipTest extends OxTest {
    protected component = OxAsyncTooltip

    private collectionMock = {}
    private objectMock = {}

    protected beforeAllTests () {
        super.beforeAllTests()

        this.collectionMock = {
            data: [
                {
                    type: "test_object",
                    id: "1",
                    attributes: {
                        attr_1: "value 1",
                        attr_2: "value 2"
                    },
                    links: {
                        self: "self"
                    }
                },
                {
                    type: "test_object",
                    id: "2",
                    attributes: {
                        attr_1: "value 3",
                        attr_2: "value 4"
                    },
                    links: {
                        self: "self"
                    }
                }
            ],
            meta: {
                date: "2022-09-19 11:06:39+02:00",
                copyright: "OpenXtrem-2022",
                authors: "dev@openxtrem.com",
                count: 2,
                total: 2
            },
            links: {
                self: "self",
                first: "first",
                last: "last"
            }
        }

        this.objectMock = {
            data: {
                type: "test_object",
                id: "1",
                attributes: {
                    attr_1: "value 1",
                    attr_2: "value 2"
                },
                links: {
                    self: "self"
                }
            },
            meta: {
                date: "2022-09-19 11:06:39+02:00",
                copyright: "OpenXtrem-2022",
                authors: "dev@openxtrem.com"
            }
        }
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testDataNotDisplayableCauseNoData () {
        const asyncTooltip = this.vueComponent({
            oxObject: TestObject,
            url: "url",
            maxHeight: "500px",
            width: "500px",
            loaded: true
        })

        this.assertFalse(this.privateCall(asyncTooltip, "dataDisplayable"))
    }

    public testDataNotDisplayableCauseNoLoaded () {
        const asyncTooltip = this.vueComponent({
            oxObject: TestObject,
            url: "url",
            maxHeight: "500px",
            width: "500px"
        })

        asyncTooltip["data"] = { data: "Some data" } as unknown as OxObject

        this.assertFalse(this.privateCall(asyncTooltip, "dataDisplayable"))
    }

    public testDataDisplayable () {
        const asyncTooltip = this.vueComponent({
            oxObject: TestObject,
            url: "url",
            maxHeight: "500px",
            width: "500px",
            loaded: true
        })

        asyncTooltip["data"] = { data: "Some data" } as unknown as OxObject

        this.assertTrue(this.privateCall(asyncTooltip, "dataDisplayable"))
    }

    public async testBeforeDisplayCallCollection () {
        const asyncTooltip = this.mountComponent({
            oxObject: TestObject,
            url: "url",
            isCollection: true,
            maxHeight: "500px",
            width: "500px"
        })
        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: this.collectionMock }))

        await this.privateCall(asyncTooltip.vm, "beforeDisplayCall")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "url"
            )
        )
        this.assertTrue(this.privateCall(asyncTooltip.vm, "dataDisplayable"))
        expect(this.privateCall(asyncTooltip.vm, "data")).toBeInstanceOf(Array)
        expect(this.privateCall(asyncTooltip.vm, "total")).toBe(2)
        expect(this.privateCall(asyncTooltip.vm, "links")).toMatchObject({
            self: "self",
            first: "first",
            last: "last"
        })
        expect(this.privateCall(asyncTooltip.vm, "localLoaded")).toBe(true)
        expect(asyncTooltip.emitted("callDone")).toBeTruthy()
        expect(asyncTooltip.emitted("update:loaded")).toBeTruthy()
    }

    public async testBeforeDisplayCallObject () {
        const asyncTooltip = this.mountComponent({
            oxObject: TestObject,
            url: "url",
            maxHeight: "500px",
            width: "500px"
        })
        jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: this.objectMock }))

        await this.privateCall(asyncTooltip.vm, "beforeDisplayCall")

        expect(oxApiService.get).toBeCalledWith(
            expect.stringContaining(
                "url"
            )
        )
        this.assertTrue(this.privateCall(asyncTooltip.vm, "dataDisplayable"))
        expect(this.privateCall(asyncTooltip.vm, "data")).toBeInstanceOf(TestObject)
        expect(this.privateCall(asyncTooltip.vm, "localLoaded")).toBe(true)
        expect(asyncTooltip.emitted("callDone")).toBeTruthy()
        expect(asyncTooltip.emitted("update:loaded")).toBeTruthy()
    }

    public testBeforeDisplayCallAlreadyLoaded () {
        const asyncTooltip = this.mountComponent({
            oxObject: TestObject,
            url: "url",
            maxHeight: "500px",
            width: "500px",
            loaded: true
        })

        expect(oxApiService.get).not.toBeCalled()
        expect(asyncTooltip.emitted("callDone")).toBeFalsy()
    }

    public testClick (): void {
        const asyncTooltip = this.mountComponent({
            oxObject: TestObject,
            url: "url",
            maxHeight: "500px",
            width: "500px"
        })

        this.privateCall(asyncTooltip.vm, "click")
        this.assertTrue(asyncTooltip.emitted("click"))
    }

    public testShow (): void {
        const asyncTooltip = this.mountComponent({
            oxObject: TestObject,
            url: "url",
            maxHeight: "500px",
            width: "500px"
        })

        this.privateCall(asyncTooltip.vm, "show")
        this.assertTrue(asyncTooltip.emitted("show"))
    }
}

class TestObject extends OxObject {
    constructor () {
        super()
        this.type = "test_object"
    }
}

(new OxAsyncTooltipTest()).launchTests()
