/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { shallowMount } from "@vue/test-utils"
import OxLoaderHandler from "@/core/components/OxLoaderHandler/OxLoaderHandler.vue"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"

/* eslint-disable dot-notation */

const mountComponent = (props?: object) => {
    return shallowMount(OxLoaderHandler, {
        propsData: { ...props }
    })
}

describe(
    "OxLoaderHandler",
    () => {
        beforeAll(() => {
            setActivePinia(oxPiniaCore)
        })

        afterEach(() => {
            jest.resetAllMocks()
        })

        test("loader should BE lowered if appbar exists",
            () => {
                const appbar = document.createElement("div")
                jest.spyOn(document, "querySelector").mockReturnValue(appbar)
                const loader = mountComponent().vm

                expect(loader["loaderClass"]).toEqual({
                    OxLoaderHandler: true,
                    lowered: true
                })
            }
        )

        test("loader should NOT BE lowered if appbar exists",
            () => {
                jest.spyOn(document, "querySelector").mockReturnValue(null)
                const loader = mountComponent().vm

                expect(loader["loaderClass"]).toEqual({
                    OxLoaderHandler: true,
                    lowered: false
                })
            }
        )
    }
)
