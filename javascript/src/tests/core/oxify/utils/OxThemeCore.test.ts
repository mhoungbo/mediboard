/**
 * @package Oxify\OxThemeCore
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxThemeCore from "@oxify/utils/OxThemeCore"
import OxTest from "@oxify/utils/OxTest"

/**
 * OxThemeCore test class
 */
export default class OxThemeCoreTest extends OxTest {
    protected component = OxThemeCore

    public testAlphaWithNoColor (): void {
        const result = OxThemeCore.alpha("", 0.5)
        this.assertEqual(result, "")
    }

    public testAlphaWithColor (): void {
        const result = OxThemeCore.alpha(OxThemeCore.primary, 0.2)
        this.assertEqual(result, "rgba(var(--primary-500-rgb), 0.2)")
    }
}

(new OxThemeCoreTest()).launchTests()
