/**
 * @package Oxify\OxDate
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxDate from "@oxify/utils/OxDate"

/**
 * OxDate test class
 */
export default class OxDateTest extends OxTest {
    protected component = "OxDate"

    /**
     * Long datetime extraction test
     */
    public testDateYMDHISExtraction (): void {
        this.assertEqual(
            OxDate.getYMDHms(new Date("2020-01-01 10:11:23")),
            "2020-01-01 10:11:23"
        )
    }

    /**
     * Short datetime extraction test
     */
    public testDateYMDHIExtraction (): void {
        this.assertEqual(
            OxDate.getYMDHm(new Date("2020-01-01 10:11:23")),
            "2020-01-01 10:11"
        )
    }

    /**
     * Date extraction test
     */
    public testDateYMDExtraction (): void {
        this.assertEqual(
            OxDate.getYMD(new Date("2020-01-01 10:11:23")),
            "2020-01-01"
        )
    }

    /**
     * Long hour extraction test
     */
    public testDateHISExtraction (): void {
        this.assertEqual(
            OxDate.getHms(new Date("2020-01-01 10:11:23")),
            "10:11:23"
        )
    }

    /**
     * Short hour extraction test
     */
    public testDateHIExtraction (): void {
        this.assertEqual(
            OxDate.getHm(new Date("2020-01-01 10:11:23")),
            "10:11"
        )
    }

    /**
     * Date subtraction test
     */
    public testDiff (): void {
        const firstDate = "2020-04-03 06:05:04"
        const secondDate = "2020-04-06 10:10:10"

        const diff = OxDate.diff(new Date(firstDate), new Date(secondDate))
        this.assertEqual(
            diff.day,
            3
        )
        this.assertEqual(
            diff.hou,
            4
        )
        this.assertEqual(
            diff.min,
            5
        )
        this.assertEqual(
            diff.sec,
            6
        )
    }

    /**
     * Automatic detection for classic date test
     */
    public testAutoFormatDate (): void {
        this.assertEqual(
            OxDate.getAutoFormat("2020-01-01"),
            "date"
        )
    }

    /**
     * Automatic detection for time test
     */
    public testAutoFormatTime (): void {
        this.assertEqual(
            OxDate.getAutoFormat("10:06"),
            "time"
        )
        this.assertEqual(
            OxDate.getAutoFormat("10:06:05"),
            "time"
        )
    }

    /**
     * Automatic detection for datetime test
     */
    public testAutoFormatDateTime (): void {
        this.assertEqual(
            OxDate.getAutoFormat("2020-01-01 10:06:50"),
            "datetime"
        )
    }
}

(new OxDateTest()).launchTests()
