/**
 * @package Oxify\OxIcon
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { mdiPlus, mdiHelp } from "@mdi/js"
import OxTest from "@oxify/utils/OxTest"
import OxIconCore from "@oxify/components/OxIcon/OxIconCore"

export default class OxIconCoreTest extends OxTest {
    protected component = OxIconCore

    /**
     * Existing SVG icon test
     */
    public testExistingSvgIcon (): void {
        const iconSvg = OxIconCore.get("add")
        this.assertEqual(iconSvg, mdiPlus)
    }

    /**
     * Nonexistent SVG icon test
     */
    public testNonexistentSvgIcon (): void {
        const iconSvg = OxIconCore.get("nonexistent")
        this.assertEqual(iconSvg, mdiHelp)
    }
}

(new OxIconCoreTest()).launchTests()
