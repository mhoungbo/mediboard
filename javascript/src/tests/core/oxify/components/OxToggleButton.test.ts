/**
 * @package Oxify\OxBadge
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { shallowMount } from "@vue/test-utils"
import OxToggleButton from "@oxify/components/OxToggleButton/OxToggleButton.vue"

/* eslint-disable dot-notation */

describe(
    "OxToggleButton",
    () => {
        test("input is emitted", () => {
            const toggleBtn = shallowMount(OxToggleButton)
            toggleBtn.vm["input"](1)
            expect(toggleBtn.emitted().input).toEqual([[1]])
        })
    }
)
