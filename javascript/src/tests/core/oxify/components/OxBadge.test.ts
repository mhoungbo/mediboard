/**
 * @package Oxify\OxBadge
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { shallowMount } from "@vue/test-utils"
import OxBadge from "@oxify/components/OxBadge/OxBadge.vue"
import OxThemeCore from "@oxify/utils/OxThemeCore"

/* eslint-disable dot-notation */

describe(
    "OxBadge",
    () => {
        test("is displayed by default", () => {
            const badge = shallowMount(OxBadge)
            expect(badge.vm["displayed"]).toBe(true)
        })

        test.each([
            { propColor: "primary", expected: OxThemeCore.primary },
            { propColor: "error", expected: OxThemeCore.error },
            { propColor: "secondary", expected: OxThemeCore.secondary200 }
        ])("with prop color $propColor should be color $expected", ({ propColor, expected }) => {
            const badge = shallowMount(OxBadge, {
                propsData: { color: propColor }
            })
            expect(badge.vm["badgeColor"]).toBe(expected)
        })
    }
)
