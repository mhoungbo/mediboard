/**
 * @package Oxify\OxIcon
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"

/**
 * OxIcon test class
 */
export default class OxIconTest extends OxTest {
    protected component = OxIcon

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object = {}) {
        return super.vueComponent(props)
    }

    /**
     * Icon default class name test
     */
    public testIconDefaultClassName (): void {
        const icon = this.vueComponent()
        this.assertEqual(this.privateCall(icon, "classes"), "OxIcon")
    }

    /**
     * Icon with added class name test
     */
    public testIconWithAddedClassName (): void {
        const icon = this.vueComponent({ className: "custom" })
        this.assertEqual(this.privateCall(icon, "classes"), "OxIcon custom")
    }

    /**
     * Icon classes with left prop defined test
     */
    public testIconClassesWithLeftProp (): void {
        const icon = this.vueComponent({ left: true })
        this.assertEqual(this.privateCall(icon, "classes"), "OxIcon left")
    }

    /**
     * Icon classes with right prop defined test
     */
    public testIconClassesWithRightProp (): void {
        const icon = this.vueComponent({ right: true })
        this.assertEqual(this.privateCall(icon, "classes"), "OxIcon right")
    }

    /**
     * Icon default custom style test
     */
    public testDefaultCustomStyle (): void {
        const icon = this.vueComponent({ icon: "add" })
        this.assertEqual(
            this.privateCall(icon, "customStyle"),
            {
                height: "24px",
                width: "24px",
                color: false
            }
        )
    }

    /**
     * Icon custom style with custom size test
     */
    public testCustomStyleWithCustomSize (): void {
        const icon = this.vueComponent({ icon: "dmp", size: 30 })
        this.assertEqual(
            this.privateCall(icon, "customStyle"),
            {
                height: "30px",
                width: "30px",
                color: false
            }
        )
    }

    /**
     * Icon custom style with custom color test
     */
    public testCustomStyleWithCustomColor (): void {
        const icon = this.vueComponent({ icon: "dmp", color: "red" })
        this.assertEqual(
            this.privateCall(icon, "customStyle"),
            {
                height: "24px",
                width: "24px",
                color: "red"
            }
        )
    }
}

(new OxIconTest()).launchTests()
