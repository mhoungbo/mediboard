/**
 * @package Oxify\OxTabs
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { shallowMount } from "@vue/test-utils"

import OxTabs from "@oxify/components/OxTabs/OxTabs.vue"
import OxThemeCore from "@oxify/utils/OxThemeCore"

/* eslint-disable dot-notation */

const mountComponent = (props?: object) => {
    return shallowMount(OxTabs, {
        propsData: { value: null, ...props }
    })
}

describe(
    "OxTabs",
    () => {
        test("tabsBackgroundColor should be backgroundDefault by default",
            () => {
                const tabs = mountComponent().vm

                expect(tabs["tabsBackgroundColor"]).toEqual(OxThemeCore.backgroundDefault)
            }
        )

        test("tabsBackgroundColor should be override by backgroundColor props",
            () => {
                const tabs = mountComponent({
                    backgroundColor: "primary"
                }).vm

                expect(tabs["tabsBackgroundColor"]).toEqual("primary")
            }
        )

        test("tabsClass should have vertical as false by default",
            () => {
                const tabs = mountComponent().vm

                expect(tabs["tabsClass"]).toEqual({
                    OxTabs: true,
                    vertical: false
                })
            }
        )

        test("tabsClass should have vertical as true",
            () => {
                const tabs = mountComponent({
                    vertical: true
                }).vm

                expect(tabs["tabsClass"]).toEqual({
                    OxTabs: true,
                    vertical: true
                })
            }
        )

        test("tabChanged should emit input",
            () => {
                const tabs = mountComponent()
                tabs.vm["tabChanged"](0)

                expect(tabs.emitted().input).toEqual([[0]])
            }
        )
    }
)
