/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxRichTooltip from "@oxify/components/OxRichTooltip/OxRichTooltip.vue"

Vue.use(Vuetify)

/* eslint-disable dot-notation */

/**
 * OxRichTooltip test class
 */
export default class OxRichTooltipTest extends OxTest {
    protected component = OxRichTooltip

    protected beforeTest () {
        super.beforeTest()
        jest.useFakeTimers()
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
        jest.runAllTimers()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testValidPositionValue () {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const validator = OxRichTooltip.options.props.position.validator
        expect(validator("bottom")).toBeTruthy()
        expect(validator("left")).toBeTruthy()
        expect(validator("right")).toBeTruthy()
        expect(validator("top")).toBeTruthy()
    }

    public testInvalidPositionValue () {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const validator = OxRichTooltip.options.props.position.validator
        expect(validator("invalidValue")).toBeFalsy()
    }

    public testShowTooltip (): void {
        const tooltip = this.vueComponent({ maxHeight: "500px", width: "500px" })
        tooltip["flag"] = true
        this.assertTrue(this.privateCall(tooltip, "showTooltip"))
    }

    public testNotShowTooltipCauseCloseForced (): void {
        const tooltip = this.vueComponent({ maxHeight: "500px", width: "500px", forceClose: true })
        tooltip["flag"] = true
        this.assertFalse(this.privateCall(tooltip, "showTooltip"))
    }

    public testNotShowTooltipCauseFlag (): void {
        const tooltip = this.vueComponent({ maxHeight: "500px", width: "500px" })
        this.assertFalse(this.privateCall(tooltip, "showTooltip"))
    }

    public async testWatchForceClose (): Promise<void> {
        const tooltip = this.vueComponent({ maxHeight: "500px", width: "500px", forceClose: true })
        await tooltip.$nextTick()
        this.assertEqual(tooltip["closeForced"], true)
    }

    public async testDefaultMouseEnter (): Promise<void> {
        const tooltip = this.mountComponent({ maxHeight: "500px", width: "500px" })
        await tooltip.vm.$nextTick()

        const timeoutSpy = jest.spyOn(window, "setTimeout")

        this.privateCall(tooltip.vm, "mouseEnter")
        this.assertEqual(tooltip.vm["closeForced"], false)

        expect(timeoutSpy).toHaveBeenCalledTimes(2)
        expect(timeoutSpy).toHaveBeenNthCalledWith(1, expect.any(Function), 300)
        expect(timeoutSpy).toHaveBeenNthCalledWith(2, expect.any(Function), 500)

        jest.advanceTimersByTime(300)
        this.assertTrue(tooltip.emitted("beforeDisplay"))
        this.assertEqual(tooltip.vm["flag"], false)

        jest.advanceTimersByTime(200)
        this.assertTrue(tooltip.emitted("show"))
        this.assertEqual(tooltip.vm["flag"], true)
    }

    public testMouseEnterWithCustomDelays (): void {
        const tooltip = this.mountComponent(
            {
                maxHeight: "500px",
                width: "500px",
                delayBeforeCall: 250,
                delayBeforeShow: 350
            }
        )

        const timeoutSpy = jest.spyOn(window, "setTimeout")
        this.privateCall(tooltip.vm, "mouseEnter")

        expect(timeoutSpy).toHaveBeenCalledTimes(2)
        expect(timeoutSpy).toHaveBeenNthCalledWith(1, expect.any(Function), 250)
        expect(timeoutSpy).toHaveBeenNthCalledWith(2, expect.any(Function), 350)
    }

    public testMouseLeave (): void {
        const tooltip = this.vueComponent({ maxHeight: "500px", width: "500px" })

        this.privateCall(tooltip, "mouseEnter")

        // Changes flag = true with "beforeShow" timeout
        jest.advanceTimersByTime(500)
        this.assertEqual(tooltip["flag"], true)

        const timeoutSpy = jest.spyOn(window, "setTimeout")
        this.privateCall(tooltip, "mouseLeave")

        expect(timeoutSpy).toHaveBeenLastCalledWith(expect.any(Function), 200)

        // Changes flag = false with "afterLeave" timeout
        jest.advanceTimersByTime(200)
        this.assertEqual(tooltip["flag"], false)
    }

    public testMouseLeaveBeforeTimersFinished (): void {
        const tooltip = this.mountComponent({ maxHeight: "500px", width: "500px" })

        this.privateCall(tooltip.vm, "mouseEnter")
        jest.advanceTimersByTime(200)
        this.privateCall(tooltip.vm, "mouseLeave")

        jest.advanceTimersByTime(100)
        this.assertUndefined(tooltip.emitted("beforeDisplay"))

        jest.advanceTimersByTime(200)
        this.assertUndefined(tooltip.emitted("show"))
    }

    public testKeepTooltipOpened (): void {
        const tooltip = this.vueComponent({ maxHeight: "500px", width: "500px" })

        this.privateCall(tooltip, "mouseEnter")
        jest.advanceTimersByTime(500)
        this.assertEqual(tooltip["flag"], true)

        this.privateCall(tooltip, "mouseLeave")
        jest.advanceTimersByTime(100)
        this.assertEqual(tooltip["flag"], true)

        this.privateCall(tooltip, "mouseEnter")
        jest.advanceTimersByTime(200)
        this.assertEqual(tooltip["flag"], true)

        this.privateCall(tooltip, "mouseLeave")
        jest.advanceTimersByTime(200)
        this.assertEqual(tooltip["flag"], false)
    }

    public testClick (): void {
        const tooltip = this.mountComponent({ maxHeight: "500px", width: "500px" })

        this.privateCall(tooltip.vm, "click")
        this.assertTrue(tooltip.emitted("click"))
    }
}

(new OxRichTooltipTest()).launchTests()

describe(
    "OxRichTooltip",
    () => {
        test.each([
            { position: "bottom" },
            { position: "left" },
            { position: "right" },
            { position: "top" }
        ])("test position with position: $position", ({ position }) => {
            const tooltip = shallowMount(OxRichTooltip, {
                propsData: { position, maxHeight: "500px", width: "500px" }
            }).vm

            expect(tooltip[position]).toBeTruthy()
        })

        test.each([
            { position: "bottom", expectedAnimation: "slide-y-transition" },
            { position: "left", expectedAnimation: "slide-x-reverse-transition" },
            { position: "right", expectedAnimation: "slide-x-transition" },
            { position: "top", expectedAnimation: "slide-y-reverse-transition" }
        ])("test animation with position: $position, expect animation: $expectedAnimation ", ({ position, expectedAnimation }) => {
            const tooltip = shallowMount(OxRichTooltip, {
                propsData: { position, maxHeight: "500px", width: "500px" }
            }).vm

            expect(tooltip["animation"]).toEqual(expectedAnimation)
        })
    }
)
