/**
 * @package Oxify\OxSnackbar
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxSnackbar from "@oxify/components/OxSnackbar/OxSnackbar.vue"
import OxThemeCore from "@oxify/utils/OxThemeCore"

Vue.use(Vuetify)

/**
 * OxSnackbar test class
 */
export default class OxSnackbarTest extends OxTest {
    protected component = OxSnackbar

    protected beforeTest () {
        super.beforeTest()
        jest.useFakeTimers()
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testCreationWithTimer (): void {
        const timeoutSpy = jest.spyOn(global, "setTimeout")

        this.mountComponent({})

        expect(timeoutSpy).toHaveBeenCalledTimes(1)
        expect(timeoutSpy).toHaveBeenLastCalledWith(expect.any(Function), 5000)
    }

    public testCreationWithoutTimer (): void {
        const timeoutSpy = jest.spyOn(global, "setTimeout")

        this.mountComponent({ error: true })

        expect(timeoutSpy).toHaveBeenCalledTimes(0)
    }

    public testSnackErrorClass (): void {
        const snackbar = this.vueComponent({ error: true })
        this.assertEqual(
            this.privateCall(snackbar, "snackbarClasses"),
            {
                snackError: true,
                OxSnackbar: true
            }
        )
    }

    public testNoSnackErrorClass (): void {
        const snackbar = this.vueComponent({})
        this.assertEqual(
            this.privateCall(snackbar, "snackbarClasses"),
            {
                snackError: false,
                OxSnackbar: true
            }
        )
    }

    public testButtonDefaultColor (): void {
        const snackbar = this.vueComponent({})
        this.assertEqual(this.privateCall(snackbar, "buttonColor"), OxThemeCore.primary200)
    }

    public testButtonErrorColor (): void {
        const snackbar = this.vueComponent({ error: true })
        this.assertEqual(this.privateCall(snackbar, "buttonColor"), OxThemeCore.onPrimaryHighEmphasis)
    }

    /**
     * Snackbar click close test
     */
    public testCloseSnackbar (): void {
        const snackbar = this.mountComponent({})
        this.privateCall(snackbar.vm, "close", {})
        this.assertTrue(snackbar.emitted("click:close"))
        this.assertFalse(this.privateCall(snackbar.vm, "showSnackbar"))
    }
}

(new OxSnackbarTest()).launchTests()
