/**
 * @package Oxify\OxExpansionPanel
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxExpansionPanel from "@oxify/components/OxExpansionPanel/OxExpansionPanel.vue"

Vue.use(Vuetify)

class OxExpansionPanelTest extends OxTest {
    component = OxExpansionPanel

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testValidShowSidesValue () {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const validator = OxExpansionPanel.options.props.showSides.validator
        expect(validator("left")).toBeTruthy()
        expect(validator("right")).toBeTruthy()
        expect(validator("both")).toBeTruthy()
        expect(validator("none")).toBeTruthy()
    }

    public testInvalidShowSidesValue () {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const validator = OxExpansionPanel.options.props.showSides.validator
        expect(validator("invalidValue")).toBeFalsy()
    }

    public testHasNotClassSticky () {
        const expansionPanel = this.vueComponent({})
        expect(this.privateCall(expansionPanel, "classes")).toEqual({
            OxExpansionPanel: true,
            sticky: false
        })
    }

    public testHasClassSticky () {
        const expansionPanel = this.vueComponent({ sticky: true })
        expect(this.privateCall(expansionPanel, "classes")).toEqual({
            OxExpansionPanel: true,
            sticky: true
        })
    }

    public testShowLeftCauseShowSideLeft () {
        const expansionPanel = this.vueComponent({ showSides: "left" })
        expect(this.privateCall(expansionPanel, "showLeft")).toBe(true)
    }

    public testShowLeftCauseShowSideBoth () {
        const expansionPanel = this.vueComponent({ showSides: "both" })
        expect(this.privateCall(expansionPanel, "showLeft")).toBe(true)
    }

    public testNotShowLeft () {
        const expansionPanel = this.vueComponent({})
        expect(this.privateCall(expansionPanel, "showLeft")).toBe(false)
    }

    public testShowRightCauseShowSideRight () {
        const expansionPanel = this.vueComponent({ showSides: "right" })
        expect(this.privateCall(expansionPanel, "showRight")).toBe(true)
    }

    public testShowRightCauseShowSideBoth () {
        const expansionPanel = this.vueComponent({ showSides: "both" })
        expect(this.privateCall(expansionPanel, "showRight")).toBe(true)
    }

    public testNotShowRight () {
        const expansionPanel = this.vueComponent({ showSides: "left" })
        expect(this.privateCall(expansionPanel, "showRight")).toBe(false)
    }

    public testStyle () {
        const expansionPanel = this.vueComponent({ top: "10px" })
        expect(this.privateCall(expansionPanel, "style")).toEqual({ top: "10px" })
    }

    public testDefaultIconRightUp () {
        const expansionPanel = this.vueComponent({})
        expect(this.privateCall(expansionPanel, "defaultIconRight", true)).toEqual("chevronUp")
    }

    public testDefaultIconRightDown () {
        const expansionPanel = this.vueComponent({})
        expect(this.privateCall(expansionPanel, "defaultIconRight", false)).toEqual("chevronDown")
    }
}

(new OxExpansionPanelTest()).launchTests()
