/**
 * @package Oxify\OxChip
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue from "vue"
import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxChip from "@oxify/components/OxChip/OxChip.vue"
import OxThemeCore from "@oxify/utils/OxThemeCore"

Vue.use(Vuetify)

/**
 * OxChip test class
 */
export default class OxChipTest extends OxTest {
    protected component = OxChip

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object = {}) {
        return super.vueComponent(props)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return super.mountComponent(props)
    }

    /**
     * Classic chip text color test
     */
    public testTextColorDefaultChip (): void {
        const chip = this.vueComponent({})
        this.assertEqual(this.privateCall(chip, "textColor"), "")
    }

    /**
     * Classic chip background color test
     */
    public testBackgroundColorDefaultChip (): void {
        const chip = this.vueComponent({})
        this.assertEqual(this.privateCall(chip, "backgroundColor"), "")
    }

    /**
     * Error chip text color test
     */
    public testErrorChipTextColor (): void {
        const chip = this.vueComponent({ color: "error" })
        this.assertEqual(this.privateCall(chip, "textColor"), OxThemeCore.errorTextDefault)
        this.assertEqual(
            this.privateCall(chip, "backgroundColor"),
            OxThemeCore.alpha(OxThemeCore.errorTextDefault, 0.12)
        )
    }

    /**
     * Error chip background color test
     */
    public testErrorChipBackgroundColor (): void {
        const chip = this.vueComponent({ color: "error" })
        this.assertEqual(
            this.privateCall(chip, "backgroundColor"),
            OxThemeCore.alpha(OxThemeCore.errorTextDefault, 0.12)
        )
    }

    /**
     * Warning chip test color test
     */
    public testWarningChipTextColor (): void {
        const chip = this.vueComponent({ color: "warning" })
        this.assertEqual(this.privateCall(chip, "textColor"), OxThemeCore.warningTextDefault)
    }

    /**
     * Warning chip background color test
     */
    public testWarningChipBackgroundColor (): void {
        const chip = this.vueComponent({ color: "warning" })
        this.assertEqual(
            this.privateCall(chip, "backgroundColor"),
            OxThemeCore.alpha(OxThemeCore.warningTextDefault, 0.12)
        )
    }

    /**
     * Success chip text color test
     */
    public testSuccessChipTextColor (): void {
        const chip = this.vueComponent({ color: "success" })
        this.assertEqual(this.privateCall(chip, "textColor"), OxThemeCore.successTextDefault)
    }

    /**
     * Success chip background color test
     */
    public testSuccessChipBackgroundColor (): void {
        const chip = this.vueComponent({ color: "success" })
        this.assertEqual(
            this.privateCall(chip, "backgroundColor"),
            OxThemeCore.alpha(OxThemeCore.successTextDefault, 0.12)
        )
    }

    /**
     * Primary chip text color test
     */
    public testPrimaryChipTextColor (): void {
        const chip = this.vueComponent({ color: "primary" })
        this.assertEqual(this.privateCall(chip, "textColor"), OxThemeCore.primary)
    }

    /**
     * Primary chip background color test
     */
    public testPrimaryChipBackgroundColor (): void {
        const chip = this.vueComponent({ color: "primary" })
        this.assertEqual(
            this.privateCall(chip, "backgroundColor"),
            OxThemeCore.alpha(OxThemeCore.primary, 0.12)
        )
    }

    /**
     * Secondary chip text color test
     */
    public testSecondaryChipTextColor (): void {
        const chip = this.vueComponent({ color: "secondary" })
        this.assertEqual(this.privateCall(chip, "textColor"), OxThemeCore.secondary)
    }

    /**
     * Secondary chip background color test
     */
    public testSecondaryChipBackgroundColor (): void {
        const chip = this.vueComponent({ color: "secondary" })
        this.assertEqual(
            this.privateCall(chip, "backgroundColor"),
            OxThemeCore.alpha(OxThemeCore.secondary, 0.12)
        )
    }
}

(new OxChipTest()).launchTests()
