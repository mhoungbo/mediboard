/**
 * @package Oxify\OxBeautify
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxBeautify from "@oxify/components/OxBeautify/OxBeautify.vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import OxTest from "@oxify/utils/OxTest"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * OxBeautify test class
 */
export default class OxBeautifyTest extends OxTest {
    protected component = OxBeautify

    /**
     * @inheritDoc
     */
    protected vueComponent (props = {}) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                methods: {},
                localVue
            }
        ).vm
    }

    /**
     * Undefined value recognition test
     */
    public testUndefinedRecognition (): void {
        const toto = this.vueComponent()
        this.assertTrue(
            this.privateCall(
                this.vueComponent({}),
                "isVoid"
            )
        )
    }

    /**
     * Date recognition test
     */
    public testDateRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: "2020-01-01"
                    }
                ),
                "isDate"
            )
        )
    }

    /**
     * Time recognition test
     */
    public testTimeRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: "10:15:58"
                    }
                ),
                "isTime"
            )
        )
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: "32:44"
                    }
                ),
                "isTime"
            )
        )
        this.assertFalse(
            this.privateCall(
                this.vueComponent(
                    {
                        value: "10:75:58"
                    }
                ),
                "isTime"
            )
        )
    }

    /**
     * Boolean recognition test
     */
    public testBooleanRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: true
                    }
                ),
                "isBoolean"
            )
        )
    }

    /**
     * No recognition of a string as boolean test
     */
    public testStringNonBooleanRecognition (): void {
        this.assertFalse(
            this.privateCall(
                this.vueComponent(
                    {
                        value: ""
                    }
                ),
                "isBoolean"
            )
        )
    }

    /**
     * No recognition of a number as boolean test
     */
    public testNumberNonBooleanRecognition (): void {
        this.assertFalse(
            this.privateCall(
                this.vueComponent(
                    {
                        value: 0
                    }
                ),
                "isBoolean"
            )
        )
    }

    /**
     * String recognition test
     */
    public testStringRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: "Some string"
                    }
                ),
                "isString"
            )
        )
    }

    /**
     * Number recognition test
     */
    public testNumberRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: 75
                    }
                ),
                "isNumber"
            )
        )
    }

    /**
     * Recognition of a string containing numbers as number test
     */
    public testStringNumberRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: "75"
                    }
                ),
                "isNumber"
            )
        )
    }

    /**
     * Empty string recognition test
     */
    public testEmptyStringRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: ""
                    }
                ),
                "isVoid"
            )
        )
    }

    /**
     * Blank space recognition test
     */
    public testSpaceStringRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: " "
                    }
                ),
                "isVoid"
            )
        )
    }

    /**
     * Array recognition test
     */
    public testArrayRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: [
                            "first item",
                            "second item"
                        ]
                    }
                ),
                "isArray"
            )
        )
    }

    /**
     * Empty array recognition test
     */
    public testEmptyArrayRecognition (): void {
        this.assertTrue(
            this.privateCall(
                this.vueComponent(
                    {
                        value: []
                    }
                ),
                "isEmpty"
            )
        )
    }

    /**
     * Void label customization test
     */
    public testVoidLabel (): void {
        const label = "Test"
        this.assertEqual(
            this.privateCall(this.vueComponent({ value: "", voidLabel: label }), "view"),
            label
        )
    }

    /**
     * Empty label customization test
     */
    public testEmptyLabel (): void {
        const label = "Test empty"
        this.assertEqual(
            this.privateCall(this.vueComponent({ value: [], voidLabel: label }), "view"),
            label
        )
    }

    /**
     * Date display test
     */
    public testViewDate (): void {
        const beautify = this.vueComponent({ value: "2021-04-30" })
        this.assertEqual(this.privateCall(beautify, "view"), "30/04/2021")
    }
}

(new OxBeautifyTest()).launchTests()
