/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@/core/oxify/utils/OxTest"
import OxFieldCore, { MANDATORY_SYMBOL } from "@oxify/components/OxField/OxFieldCore"
import OxCheckbox from "@oxify/components/OxField/OxCheckbox/OxCheckbox.vue"
import OxFormValidator from "@/core/oxify/components/OxForm/OxFormValidator"

/**
 * Test pour la classe OxFieldCore
 */
export default class OxFieldCoreTest extends OxTest {
    // Using OxCheckbox as a basic component to test OxFieldCore (need a template for assembly)
    protected component = OxFieldCore

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object = {}) {
        // @ts-ignore
        this.component = OxCheckbox
        const vue = super.vueComponent(props)
        this.component = OxFieldCore
        return vue
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        // @ts-ignore
        this.component = OxCheckbox
        const mounted = super.mountComponent(props)
        this.component = OxFieldCore
        return mounted
    }

    /**
     * Test g?n?ration des classes
     */
    public testFieldClasses (): void {
        const classes = this.privateCall(
            this.vueComponent({}),
            "fieldClasses"
        )
        this.assertFalse(classes.labelled)
        this.assertFalse(classes["not-null"])
        this.assertTrue(
            this.privateCall(
                this.vueComponent({ label: "Test" }),
                "fieldClasses"
            ).labelled
        )
        this.assertTrue(
            this.privateCall(
                this.vueComponent({ notNull: true }),
                "fieldClasses"
            )["not-null"]
        )
    }

    /**
     * Test List initialisation
     */
    public testViewList (): void {
        const planList = ["1", "2", "3"]
        const complexList = [
            { view: "1", _id: "1" },
            { view: "2", _id: "2" },
            { view: "3", _id: "3" }
        ]
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ list: planList }),
                "viewList"
            ),
            complexList
        )
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ list: complexList }),
                "viewList"
            ),
            complexList
        )
    }

    /**
     * Test field text color
     */
    public testFieldColor (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ onPrimary: true }),
                "fieldColor"
            ),
            "rgba(255, 255, 255, 0.7)"
        )
        this.assertUndefined(
            this.privateCall(
                this.vueComponent({}),
                "fieldColor"
            )
        )
    }

    /**
     * Test field background color
     */
    public testFieldBG (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ onPrimary: true }),
                "fieldBG"
            ),
            "#5C6BC0"
        )
        this.assertUndefined(
            this.privateCall(
                this.vueComponent({}),
                "fieldBG"
            )
        )
    }

    /**
     * Test r?cup?ration message informatif du champ
     */
    public testHint (): void {
        const message = "test"
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ message }),
                "hint"
            ),
            "test"
        )
    }

    /**
     * Test Label mis en forme
     */
    public testLabelComputed (): void {
        const basicLabel = "test"
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ label: basicLabel }),
                "labelComputed"
            ),
            basicLabel
        )
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ label: basicLabel, notNull: true }),
                "labelComputed"
            ),
            basicLabel + " " + MANDATORY_SYMBOL
        )
    }

    /**
     * Test injection de r?gle de validation
     */
    public testRulesWithoutValidatorAndNullableField (): void {
        const firstRule = v => (!!v || "Error")
        this.assertContain(
            this.privateCall(
                this.vueComponent({ value: "1", rules: [firstRule] }),
                "fieldRules"
            ),
            firstRule
        )
    }

    public async testRulesWithValidator (): Promise<void> {
        const firstRule = v => (!!v || "Error")
        const validator = new OxFormValidator({ field: [firstRule] })

        const field = this.mountComponent({ value: "1", name: "field", validator })

        this.assertEqual(
            this.privateCall(
                field.vm,
                "fieldRules"
            ),
            []
        )

        validator.bindRules()
        await field.vm.$nextTick()

        this.assertEqual(
            this.privateCall(
                field.vm,
                "fieldRules"
            ),
            [firstRule]
        )
    }
}

(new OxFieldCoreTest()).launchTests()
