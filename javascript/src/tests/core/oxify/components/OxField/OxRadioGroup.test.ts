/**
 * @package Oxify\OxRadioGroup
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxRadioGroup from "@oxify/components/OxField/OxRadioGroup/OxRadioGroup.vue"
import { shallowMount } from "@vue/test-utils"

/**
 * OxRadioGroup test class
 */
export default class OxRadioGroupTest extends OxTest {
    protected component = OxRadioGroup

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object = {}) {
        return shallowMount(
            this.component,
            {
                propsData: props
            }
        )
    }

    /**
     * RadioGroup change value test
     */
    public testChangeValue (): void {
        const radioGroup = this.mountComponent({})
        const expectedNewValue = true
        this.privateCall(radioGroup.vm, "changeValue", expectedNewValue)

        const events = radioGroup.emitted("input")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }
}

(new OxRadioGroupTest()).launchTests()
