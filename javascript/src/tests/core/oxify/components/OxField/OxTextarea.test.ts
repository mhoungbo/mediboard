/**
 * @package Oxify\OxField
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxTextarea from "@oxify/components/OxField/OxTextarea/OxTextarea.vue"
import { mount } from "@vue/test-utils"

/**
 * OxTextarea test class
 */
export default class OxTextareaTest extends OxTest {
    protected component = OxTextarea

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object = {}) {
        return super.mountComponent(props)
    }

    /**
     * Textarea value change test
     */
    public testValueChange (): void {
        const expectedValue = "New value"
        const textarea = this.mountComponent({ value: "Old value" })
        this.privateCall(
            textarea.vm,
            "change",
            expectedValue
        )

        const changeEvents = textarea.emitted("change")
        const inputEvents = textarea.emitted("input")
        this.assertHaveLength(changeEvents, 1)
        this.assertHaveLength(inputEvents, 1)

        const lastChangeEvent = Array.isArray(changeEvents) ? changeEvents[0] : false
        const lastInputEvent = Array.isArray(inputEvents) ? inputEvents[0] : false
        this.assertEqual(lastChangeEvent, [expectedValue])
        this.assertEqual(lastInputEvent, [expectedValue])
    }

    /**
     * Focus on component test
     */
    public testFocus (): void {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "focus"
        )
        this.assertHaveLength(field.emitted("focus"), 1)
    }

    /**
     * Lose the focus on component test
     */
    public testBlur (): void {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "blur"
        )
        this.assertHaveLength(field.emitted("blur"), 1)
    }

    public testKeyup (): void {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "keyup"
        )
        this.assertHaveLength(field.emitted("keyup"), 1)
    }

    public testBindFocus (): void {
        const div = document.createElement("div")
        div.id = "root"
        document.body.appendChild(div)
        const field = mount(this.component, { attachTo: "#root" })
        this.privateCall(
            field.vm,
            "bindFocus"
        )

        const input = field.vm.$el.querySelector("textarea")
        expect(input).toBe(document.activeElement)
    }
}

(new OxTextareaTest()).launchTests()
