/**
 * @package Oxify\OxField
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxTextField from "@oxify/components/OxField/OxTextField/OxTextField.vue"
import { mount, shallowMount } from "@vue/test-utils"

/**
 * OxTextField test class
 */
export default class OxTextFieldTest extends OxTest {
    protected component = OxTextField

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object = {}) {
        const mask = jest.fn()

        // @ts-ignore
        return shallowMount(OxTextField,
            {
                propsData: props,
                directives: {
                    mask
                }
            }
        )
    }

    /**
     * Type = "number" test
     */
    public testFieldTypeNumber (): void {
        const field = this.mountComponent({ number: true })
        this.assertEqual(this.privateCall(field.vm, "fieldType"), "number")
    }

    /**
     * Type = "text" test
     */
    public testFieldTypeText (): void {
        const field = this.mountComponent({})
        this.assertEqual(this.privateCall(field.vm, "fieldType"), "text")
    }

    /**
     * Click on component test
     */
    public async testClick (): Promise<void> {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "click"
        )
        await field.vm.$nextTick()
        this.assertHaveLength(field.emitted("click"), 1)
    }

    /**
     * Keydown on component test
     */
    public async testKeydown (): Promise<void> {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "keydown",
            new Event("keydown")
        )
        await field.vm.$nextTick()
        this.assertHaveLength(field.emitted("keydown"), 1)
        // @ts-ignore
        this.assertInstanceOf(field.emitted("keydown")[0][0], Event)
    }

    /**
     * Value change test
     */
    public testChange (): void {
        const expectedNewValue = "Test"
        const field = this.mountComponent({ value: "old" })
        this.privateCall(
            field.vm,
            "change",
            expectedNewValue
        )

        const changeEvents = field.emitted("change")
        const inputEvents = field.emitted("input")
        this.assertHaveLength(changeEvents, 1)
        this.assertHaveLength(inputEvents, 1)

        const lastChangeEvent = Array.isArray(changeEvents) ? changeEvents[0] : false
        const lastInputEvent = Array.isArray(inputEvents) ? inputEvents[0] : false
        this.assertEqual(lastChangeEvent, [expectedNewValue])
        this.assertEqual(lastInputEvent, [expectedNewValue])
    }

    /**
     * Focus on component test
     */
    public testFocus (): void {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "focus"
        )
        this.assertHaveLength(field.emitted("focus"), 1)
    }

    /**
     * Lose the focus on component test
     */
    public testBlur (): void {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "blur"
        )
        this.assertHaveLength(field.emitted("blur"), 1)
    }

    public testBindFocus (): void {
        const div = document.createElement("div")
        div.id = "root"
        document.body.appendChild(div)
        // @ts-ignore
        const field = mount(OxTextField, { attachTo: "#root" })
        this.privateCall(
            field.vm,
            "bindFocus"
        )

        const input = field.vm.$el.querySelector("input")
        expect(input).toBe(document.activeElement)
    }
}

(new OxTextFieldTest()).launchTests()
