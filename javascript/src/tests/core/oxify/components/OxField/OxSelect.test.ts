/**
 * @package Oxify\OxField
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxSelect from "@oxify/components/OxField/OxSelect/OxSelect.vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * Test pour la classe OxSelect
 */
export default class OxSelectTest extends OxTest {
    protected component = OxSelect

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object = {}) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                methods: {},
                localVue
            }
        )
    }

    public testClassesDefault (): void {
        const select = this.vueComponent({ label: "Label" })

        this.assertEqual(
            this.privateCall(select, "classes"),
            {
                OxSelect: true,
                small: false
            }
        )
    }

    public testClassesSmall (): void {
        const select = this.vueComponent({ label: "Label", small: true })

        this.assertEqual(
            this.privateCall(select, "classes"),
            {
                OxSelect: true,
                small: true
            }
        )
    }

    public testLabelSelectDefault (): void {
        const select = this.vueComponent({ label: "Label" })

        this.assertEqual(this.privateCall(select, "labelSelect"), "Label")
    }

    public testLabelSelectSmall (): void {
        const select = this.vueComponent({ label: "Label", small: true })

        this.assertUndefined(this.privateCall(select, "labelSelect"))
    }

    /**
     * Test rendu de la liste pour l'affichage
     */
    public testItems (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({
                    list: [
                        { _id: 1, view: "One" },
                        { _id: 2, view: "Two" },
                        { _id: 3, view: "Three" }
                    ]
                }),
                "items"
            ),
            [
                { id: 1, text: "One" },
                { id: 2, text: "Two" },
                { id: 3, text: "Three" }
            ]
        )
    }

    /**
     * Test rendu de la liste pour l'affichage
     */
    public testItemsFromStrings (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({
                    list: ["One", "Two", "Three"]
                }),
                "items"
            ),
            [
                { id: "One", text: "One" },
                { id: "Two", text: "Two" },
                { id: "Three", text: "Three" }
            ]
        )
    }

    /**
     * Test rendu d'une liste custom pour l'affichage
     */
    public testCustomOptionsItems (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({
                    list: [
                        { identifier: 1, label: "One" },
                        { identifier: 2, label: "Two" },
                        { identifier: 3, label: "Three" }
                    ],
                    optionId: "identifier",
                    optionView: "label"
                }),
                "items"
            ),
            [
                { id: 1, text: "One" },
                { id: 2, text: "Two" },
                { id: 3, text: "Three" }
            ]
        )
    }

    /**
     * Test rendu de la valeur
     */
    public testMutatedValueStr (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({}),
                "mutatedValueStr"
            ),
            ""
        )
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ value: "2" }),
                "mutatedValueStr"
            ),
            "2"
        )
    }

    /**
     * Multiple values test
     */
    public testMutatedValueStrMultiple (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ value: "1|5|7", multiple: true }),
                "mutatedValueStr"
            ),
            ["1", "5", "7"]
        )
    }

    /**
     * Change string value test
     *
     * @return {Promise<void>}
     */
    public async testChangeDefault (): Promise<void> {
        const expectedValue = "2"
        const component = this.mountComponent({
            list: [
                { _id: 1, view: "One" },
                { _id: 2, view: "Two" },
                { _id: 3, view: "Three" }
            ],
            value: "1"
        })
        this.privateCall(component.vm, "changeValue", expectedValue)
        await component.vm.$nextTick()
        const changeEvents = component.emitted("change")
        const inputEvents = component.emitted("input")
        this.assertHaveLength(changeEvents, 1)
        this.assertHaveLength(inputEvents, 1)

        const lastChangeEvent = Array.isArray(changeEvents) ? changeEvents[0] : false
        const lastInputEvent = Array.isArray(inputEvents) ? inputEvents[0] : false
        this.assertEqual(lastChangeEvent, [expectedValue])
        this.assertEqual(lastInputEvent, [expectedValue])
    }

    /**
     * Change multiple values test
     *
     * @return {Promise<void>}
     */
    public async testChangeMultiple (): Promise<void> {
        const givenValue = ["2", "3"]
        const expectedValue = "2|3"
        const component = this.mountComponent({
            list: [
                { _id: 1, view: "One" },
                { _id: 2, view: "Two" },
                { _id: 3, view: "Three" }
            ],
            value: "1",
            multiple: true
        })
        this.privateCall(component.vm, "changeValue", givenValue)
        await component.vm.$nextTick()
        const changeEvents = component.emitted("change")
        const inputEvents = component.emitted("input")
        this.assertHaveLength(changeEvents, 1)
        this.assertHaveLength(inputEvents, 1)

        const lastChangeEvent = Array.isArray(changeEvents) ? changeEvents[0] : false
        const lastInputEvent = Array.isArray(inputEvents) ? inputEvents[0] : false
        this.assertEqual(lastChangeEvent, [expectedValue])
        this.assertEqual(lastInputEvent, [expectedValue])
    }
}

(new OxSelectTest()).launchTests()
