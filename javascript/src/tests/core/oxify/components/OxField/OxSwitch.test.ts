/**
 * @package Oxify\OxField
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxSwitch from "@oxify/components/OxField/OxSwitch/OxSwitch.vue"
import OxBigIcon from "@oxify/components/OxBigIcon/OxBigIcon.vue"

/**
 * OxSwitch test class
 */
export default class OxSwitchTest extends OxTest {
    protected component = OxSwitch

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return super.mountComponent(props)
    }

    public testValidLabelPositionValue (): void {
        // @ts-ignore
        const validator = OxSwitch.options.props.labelPosition.validator
        expect(validator("left")).toBeTruthy()
        expect(validator("right")).toBeTruthy()
    }

    public testInvalidLabelPositionValue (): void {
        // @ts-ignore
        const validator = OxSwitch.options.props.labelPosition.validator
        expect(validator("invalidValue")).toBeFalsy()
    }

    /**
     * Change switch value test
     */
    public testChange (): void {
        const initialValue = false
        const switchComponent = this.mountComponent({ value: initialValue, label: "" })
        this.privateCall(switchComponent.vm, "change", !initialValue)
        const events = switchComponent.emitted("change")
        this.assertHaveLength(events, 1)
        const lastEvent = Array.isArray(events) ? events[0] : undefined
        this.assertEqual(lastEvent, [!initialValue])
    }

    public testHasIsReverseClass (): void {
        const switchComponent = this.vueComponent({ labelPosition: "left" })
        expect(this.privateCall(switchComponent, "isReverse")).toMatchObject({ isReverse: true })
    }

    public testHasNotIsReverseClass (): void {
        const switchComponent = this.vueComponent({})
        expect(this.privateCall(switchComponent, "isReverse")).toMatchObject({ isReverse: false })
    }
}

(new OxSwitchTest()).launchTests()
