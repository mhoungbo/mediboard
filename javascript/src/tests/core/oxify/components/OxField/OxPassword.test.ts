/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxIconCore from "@/core/oxify/components/OxIcon/OxIconCore"
import OxTest from "@/core/oxify/utils/OxTest"
import OxPassword from "@oxify/components/OxField/OxPassword/OxPassword.vue"

/**
 * OxPassword class test
 */
export default class OxPasswordTest extends OxTest {
    protected component = OxPassword

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object = {}) {
        return super.mountComponent(props)
    }

    /**
     * Eye icon "on" version test
     */
    public testEyeIconOn (): void {
        const field = this.mountComponent({})
        this.assertEqual(this.privateCall(field.vm, "eyeIcon"), "eye")
    }

    /**
     * Eye icon "off" version test
     */
    public testEyeIconOff (): void {
        const field = this.mountComponent({})
        this.privateCall(field.vm, "toggleType")
        this.assertEqual(this.privateCall(field.vm, "eyeIcon"), "eyeOff")
    }

    /**
     * Type = "password" test
     */
    public testFieldTypePassword (): void {
        const field = this.mountComponent({})
        this.assertEqual(this.privateCall(field.vm, "fieldType"), "password")
    }

    /**
     * Type = "text" test
     */
    public testFieldTypeText (): void {
        const field = this.mountComponent({})
        this.privateCall(field.vm, "toggleType")
        this.assertEqual(this.privateCall(field.vm, "fieldType"), "text")
    }

    /**
     * Eye icon "on" getter test
     */
    public testGetEyeIconOn (): void {
        const expectedIcon = OxIconCore.get("eye")

        this.assertEqual(
            this.privateCall(
                this.vueComponent({}),
                "iconName"
            ),
            expectedIcon
        )
    }

    /**
     * Eye icon "off" getter test
     */
    public testGetEyeIconOff (): void {
        const field = this.vueComponent({})
        const expectedIcon = OxIconCore.get("eyeOff")

        this.privateCall(field, "toggleType")
        this.assertEqual(
            this.privateCall(
                field,
                "iconName"
            ),
            expectedIcon
        )
    }

    /**
     * Click on component test
     */
    public testClickOnField (): void {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "click"
        )
        this.assertHaveLength(field.emitted("click"), 1)
    }

    /**
     * Focus on component test
     */
    public testFocus (): void {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "focus"
        )
        this.assertHaveLength(field.emitted("focus"), 1)
    }

    /**
     * Lose the focus on component test
     */
    public testBlur (): void {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "blur"
        )
        this.assertHaveLength(field.emitted("blur"), 1)
    }

    /**
     * Keydown on component test
     */
    public async testKeydown (): Promise<void> {
        const field = this.mountComponent({})
        this.privateCall(
            field.vm,
            "keydown",
            new Event("keydown")
        )
        await field.vm.$nextTick()
        this.assertHaveLength(field.emitted("keydown"), 1)
        // @ts-ignore
        this.assertInstanceOf(field.emitted("keydown")[0][0], Event)
    }

    /**
     * Test field type modification
     */
    public async testToggleType (): Promise<void> {
        const pwd = this.mountComponent({}).vm
        this.assertTrue(this.privateCall(pwd, "isPassword"))
        this.privateCall(pwd, "toggleType")
        this.assertFalse(this.privateCall(pwd, "isPassword"))
    }
}

(new OxPasswordTest()).launchTests()
