/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { shallowMount } from "@vue/test-utils"
import OxTest from "@oxify/utils/OxTest"
import OxFieldStrCore from "@oxify/components/OxField/OxFieldStrCore"
import OxTextField from "@oxify/components/OxField/OxTextField/OxTextField.vue"
import OxIconCore from "@oxify/components/OxIcon/OxIconCore"

/**
 * Test pour la classe OxFieldStrCore
 */
export default class OxFieldStrCoreTest extends OxTest {
    // Using OxTextField as a basic component to test OxFieldStrCore (need a template for mounting)
    protected component = OxFieldStrCore

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object = {}) {
        const mask = jest.fn()
        // @ts-ignore
        const vue = shallowMount(OxTextField,
            {
                propsData: props,
                directives: {
                    mask
                }
            }
        ).vm
        this.component = OxFieldStrCore
        return vue
    }

    /**
     * Test de la recuperation d'icone
     */
    public testIconName (): void {
        const testIcon = "add"
        const expectedIcon = OxIconCore.get(testIcon)
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ icon: testIcon }),
                "iconName"
            ),
            expectedIcon
        )
    }
}

(new OxFieldStrCoreTest()).launchTests()
