/**
 * @package Oxify\OxRadio
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxRadio from "@oxify/components/OxField/OxRadio/OxRadio.vue"
import { shallowMount } from "@vue/test-utils"

/**
 * OxRadio test class
 */
export default class OxRadioTest extends OxTest {
    protected component = OxRadio

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object = {}) {
        return shallowMount(
            this.component,
            {
                propsData: props
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object = {}) {
        return this.mountComponent(props).vm
    }

    /**
     * Adding "expand" CSS class when "expand" prop is set test
     */
    public testExpandClass (): void {
        const radio = this.vueComponent({ expand: true })
        this.assertEqual(
            this.privateCall(radio, "radioClasses"),
            ["OxRadio", "high", { disabled: false, expand: true }]
        )
    }

    /**
     * No adding "expand" & "disabled" CSS classes when "expand" & "disabled" props are not set test
     */
    public testNoExpandClass (): void {
        const radio = this.vueComponent({})
        this.assertEqual(
            this.privateCall(radio, "radioClasses"),
            ["OxRadio", "high", { disabled: false, expand: false }]
        )
    }

    /**
     * Adding "disabled" CSS class when "disabled" prop is set test
     */
    public testDisabledClass (): void {
        const radio = this.vueComponent({ disabled: true })
        this.assertEqual(
            this.privateCall(radio, "radioClasses"),
            ["OxRadio", "high", { disabled: true, expand: false }]
        )
    }

    /**
     * Update CSS class related with "emphasis" prop is set test
     */
    public testEmphasisRelatedClass (): void {
        const radio = this.vueComponent({ emphasis: "medium" })
        this.assertEqual(
            this.privateCall(radio, "radioClasses"),
            ["OxRadio", "medium", { disabled: false, expand: false }]
        )
    }
}

(new OxRadioTest()).launchTests()
