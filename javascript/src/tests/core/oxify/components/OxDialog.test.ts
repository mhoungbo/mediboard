/**
 * @package Oxify\OxDialog
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxDialog from "@oxify/components/OxDialog/OxDialog.vue"
import OxDivider from "@oxify/components/OxDivider/OxDivider.vue"

jest.mock("lodash", () => {
    const module = jest.requireActual("lodash")
    module.uniqueId = (prefix) => prefix + "ID"
    return module
})

Vue.use(Vuetify)

/* eslint-disable dot-notation */

/**
 * OxDialog test class
 */
export default class OxDialogTest extends OxTest {
    protected component = OxDialog
    private div

    protected beforeTest () {
        super.beforeTest()
        this.div = document.createElement("div")
        this.div.id = "root"
        this.div.className = "OxDialog"
        document.body.appendChild(this.div)
    }

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    /**
     * Dialog has actions test
     */
    public testDialogHasActions (): void {
        const dialog = this.vueComponent({ title: "Test Modal" }, {}, { actions: "slot actions" })
        this.assertTrue(this.privateCall(dialog, "hasActions"))
    }

    /**
     * Dialog has not actions test
     */
    public testDialogHasNotActions (): void {
        const dialog = this.vueComponent({ title: "Test Modal" })
        this.assertFalse(this.privateCall(dialog, "hasActions"))
    }

    /**
     * Dialog default transition test
     */
    public testDialogDefaultTransition (): void {
        const dialog = this.vueComponent({ title: "Test Modal" })
        this.assertEqual(this.privateCall(dialog, "dialogBottomTransition"), "dialog-transition")
    }

    /**
     * Dialog bottom transition test
     */
    public testDialogBottomTransition (): void {
        const dialog = this.vueComponent({ title: "Test Modal", fullscreen: true })
        this.assertEqual(this.privateCall(dialog, "dialogBottomTransition"), "dialog-bottom-transition")
    }

    /**
     * Dialog show fullscreen header
     */
    public testDialogShowHeader (): void {
        const dialog = this.vueComponent({ title: "Test Modal", fullscreen: true })
        this.assertTrue(this.privateCall(dialog, "showFullscreenHeader"))
    }

    /**
     * Dialog hide fullscreen header
     */
    public testDialogHideHeader (): void {
        const dialog = this.vueComponent({ title: "Test Modal", fullscreen: true, hideHeader: true })
        this.assertFalse(this.privateCall(dialog, "showFullscreenHeader"))
    }

    public testDialogShowDivider (): void {
        const dialog = this.vueComponent({ title: "Test Modal", separated: true })
        this.assertTrue(this.privateCall(dialog, "showDivider"))
    }

    public testDialogNotShowDividerCauseFullscreen (): void {
        const dialog = this.vueComponent({ title: "Test Modal", separated: true, fullscreen: true })
        this.assertFalse(this.privateCall(dialog, "showDivider"))
    }

    public testDialogNotShowDividerCauseNotSeparated (): void {
        const dialog = this.vueComponent({ title: "Test Modal" })
        this.assertFalse(this.privateCall(dialog, "showDivider"))
    }

    /**
     * Dialog CSS default class test
     */
    public testDefaultClass (): void {
        const dialog = this.vueComponent({ title: "Test Modal" })
        this.assertEqual(this.privateCall(dialog, "dialogClasses"), "OxDialog dialog_ID")
    }

    /**
     * Dialog CSS compact class test
     */
    public testCompactClass (): void {
        const dialog = this.vueComponent({ title: "Test Modal", compact: true })
        this.assertEqual(this.privateCall(dialog, "dialogClasses"), "OxDialog dialog_ID compact")
    }

    /**
     * Dialog CSS fullHeight class test
     */
    public testFullHeightClass (): void {
        const dialog = this.vueComponent({ title: "Test Modal", fullHeight: true })
        this.assertEqual(this.privateCall(dialog, "dialogClasses"), "OxDialog dialog_ID fullHeight")
    }

    /**
     * Dialog CSS fullscreen class test
     */
    public testFullscreenClass (): void {
        const dialog = this.vueComponent({ title: "Test Modal", fullscreen: true })
        this.assertEqual(this.privateCall(dialog, "dialogClasses"), "OxDialog dialog_ID fullscreen")
    }

    /**
     * Dialog CSS overflowAuthorized class test
     */
    public testOverflowAuthorizedClass (): void {
        const dialog = this.vueComponent({ title: "Test Modal", overflowAuthorized: true })
        this.assertEqual(this.privateCall(dialog, "dialogClasses"), "OxDialog dialog_ID overflowAuthorized")
    }

    /**
     * Dialog CSS scrolled class test
     */
    public testScrolledClass (): void {
        const dialog = this.vueComponent({ title: "Test Modal" })
        dialog["isScrolled"] = true
        this.assertEqual(this.privateCall(dialog, "dialogClasses"), "OxDialog dialog_ID scrolled")
    }

    /**
     * Dialog CSS custom class test
     */
    public testCustomClass (): void {
        const dialog = this.vueComponent({ title: "Test Modal", contentClass: "custom" })
        this.assertEqual(this.privateCall(dialog, "dialogClasses"), "OxDialog dialog_ID custom")
    }

    /**
     * Dialog CSS compact + fullHeight + fullscreen + scrolled classes test
     */
    public testAllClasses (): void {
        const dialog = this.vueComponent(
            {
                title: "Test Modal",
                compact: true,
                fullHeight: true,
                fullscreen: true,
                overflowAuthorized: true,
                contentClass: "custom"
            }
        )
        dialog["isScrolled"] = true

        this.assertEqual(
            this.privateCall(dialog, "dialogClasses"),
            "OxDialog dialog_ID compact fullHeight fullscreen overflowAuthorized scrolled custom"
        )
    }

    /**
     * Dialog change value test
     */
    public testChangeValue (): void {
        const dialog = this.mountComponent({})
        const expectedNewValue = true
        this.privateCall(dialog.vm, "changeValue", expectedNewValue)

        const events = dialog.emitted("input")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }

    /**
     * Dialog Toolbar click test
     */
    public testClick (): void {
        const dialog = this.mountComponent({})
        this.privateCall(dialog.vm, "click", {})
        this.assertTrue(dialog.emitted("click"))
    }

    public async testAddScrollEvent (): Promise<void> {
        Object.defineProperty(this.div, "addEventListener", {
            value: jest.fn()
        })

        const dialog = this.vueComponent({})
        dialog["dialogContainer"] = this.div

        this.privateCall(dialog, "toggleScrollEvent", true)
        await dialog.$nextTick()

        expect(this.div.addEventListener).toHaveBeenCalled()
        expect(this.div.addEventListener).toHaveBeenCalledWith(
            "scroll",
            dialog["toggleIsScrolled"]
        )
    }

    public testRemoveScrollEvent (): void {
        Object.defineProperty(this.div, "removeEventListener", {
            value: jest.fn()
        })

        const dialog = this.vueComponent({})
        dialog["dialogContainer"] = this.div

        this.privateCall(dialog, "toggleScrollEvent", false)

        expect(this.div.removeEventListener).toHaveBeenCalled()
        expect(this.div.removeEventListener).toHaveBeenCalledWith(
            "scroll",
            dialog["toggleIsScrolled"]
        )
    }

    public testScrollShouldBeResetOnRemoveScrollEvent (): void {
        Object.defineProperty(this.div, "removeEventListener", {
            value: jest.fn()
        })

        const dialog = this.vueComponent({})
        this.div.scrollTop = 10
        dialog["dialogContainer"] = this.div
        dialog["isScrolled"] = true

        this.privateCall(dialog, "toggleScrollEvent", false)

        expect(this.div.removeEventListener).toHaveBeenCalled()
        expect(this.div.scrollTop).toEqual(0)
        expect(dialog["isScrolled"]).toBeFalsy()
    }

    public async testToggleScrollEventWithoutADialogContainer (): Promise<void> {
        Object.defineProperty(this.div, "addEventListener", {
            value: jest.fn()
        })

        Object.defineProperty(this.div, "removeEventListener", {
            value: jest.fn()
        })

        const dialog = this.vueComponent({})

        this.privateCall(dialog, "toggleScrollEvent", true)
        await dialog.$nextTick()

        expect(this.div.addEventListener).not.toHaveBeenCalled()
        expect(this.div.removeEventListener).not.toHaveBeenCalled()
    }

    public testToggleIsScrolled (): void {
        const dialog = this.vueComponent({})

        this.div.scrollTop = 10
        const event = { type: "scroll", target: this.div } as Event

        this.privateCall(dialog, "toggleIsScrolled", event)
        this.assertTrue(this.privateCall(dialog, "isScrolled"))
    }

    public testToggleIsNotScrolled (): void {
        const dialog = this.vueComponent({})

        this.div.scrollTop = 0
        const event = { type: "scroll", target: this.div } as Event

        this.privateCall(dialog, "toggleIsScrolled", event)
        this.assertFalse(this.privateCall(dialog, "isScrolled"))
    }
}

(new OxDialogTest()).launchTests()

describe(
    "OxDialog scenarios",
    () => {
        test.each([
            { compact: false, radialInset: 16 },
            { compact: true, radialInset: 0 }
        ])("with compact ($compact) should expect radialInset: $radialInset", ({ compact, radialInset }) => {
            const dialog = shallowMount(OxDialog, {
                propsData: { compact, separated: true }
            })

            const divider = dialog.findComponent(OxDivider)

            expect(dialog.vm["dividerRadialInset"]).toEqual(radialInset)
            if (compact) {
                expect(divider.classes()).toContain("OxDialog-dividerCompact")
            }
            else {
                expect(divider.classes()).not.toContain("OxDialog-dividerCompact")
            }
        })
    }
)
