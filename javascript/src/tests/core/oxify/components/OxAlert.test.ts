/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import OxAlert from "@oxify/components/OxAlert/OxAlert.vue"
import OxTest from "@oxify/utils/OxTest"

/**
 * OxAlert test class
 */
export default class OxAlertTest extends OxTest {
    protected component = OxAlert

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    /**
     * Alert change value test
     */
    public testChangeValue (): void {
        const alert = this.mountComponent({})
        const expectedNewValue = false
        this.privateCall(alert.vm, "changeValue", expectedNewValue)

        const events = alert.emitted("input")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }

    /**
     * Checks if "close" event not emitted when modal is not displaying
     */
    public testCloseEmittedWhenChangeValue (): void {
        const alert = this.mountComponent({})
        const expectedNewValue = false
        this.privateCall(alert.vm, "changeValue", expectedNewValue)

        const events = alert.emitted("close")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }

    /**
     * Checks if "close" event is not emitted when displaying modal
     */
    public testCloseNotEmittedWhenChangeValue (): void {
        const alert = this.mountComponent({})
        this.privateCall(alert.vm, "changeValue", true)

        this.assertFalse(alert.emitted("close"))
    }

    /**
     * Checks if "cancel" event is emitted when clicking on cancel button test
     */
    public testCancelEmittedOnCancel (): void {
        const alert = this.mountComponent({})
        this.privateCall(alert.vm, "onCancel", {})
        this.assertTrue(alert.emitted("cancel"))
    }

    /**
     * Checks if "close" event is emitted when clicking on cancel button test
     */
    public testCloseEmittedOnCancel (): void {
        const alert = this.mountComponent({})
        this.privateCall(alert.vm, "onCancel", {})
        this.assertTrue(alert.emitted("close"))
    }

    /**
     * Checks if "input" event is emitted when clicking on cancel button test
     */
    public testInputEmittedOnCancel (): void {
        const alert = this.mountComponent({})
        this.privateCall(alert.vm, "onCancel", {})
        this.assertTrue(alert.emitted("input"))
    }

    /**
     * Checks if "accept" event is emitted when clicking on accept button test
     */
    public testAcceptEmittedOnAccept (): void {
        const alert = this.mountComponent({})
        this.privateCall(alert.vm, "onAccept", {})
        this.assertTrue(alert.emitted("accept"))
    }

    /**
     * Checks if "close" event is emitted when clicking on accept button test
     */
    public testCloseEmittedOnAccept (): void {
        const alert = this.mountComponent({})
        this.privateCall(alert.vm, "onAccept", {})
        this.assertTrue(alert.emitted("close"))
    }

    /**
     * Checks if "input" event is emitted when clicking on accept button test
     */
    public testInputEmittedOnAccept (): void {
        const alert = this.mountComponent({})
        this.privateCall(alert.vm, "onAccept", {})
        this.assertTrue(alert.emitted("input"))
    }

    /**
     * Checks if accept button is not shown when no label is defined
     */
    public testHideAcceptButtonWhenNoLabel () {
        const alert = this.mountComponent({})
        this.assertFalse(this.privateCall(alert.vm, "showAccept"))
    }

    /**
     * Checks if cancel button is not shown when no label is defined
     */
    public testHideCancelButtonWhenNoLabel () {
        const alert = this.mountComponent()
        this.assertFalse(this.privateCall(alert.vm, "showCancel"))
    }

    /**
     * Checks if accept button is shown when label is defined
     */
    public testShowAcceptButtonWhenNoLabel () {
        const alert = this.mountComponent({ labelAccept: "Label" })
        this.assertTrue(this.privateCall(alert.vm, "showAccept"))
    }

    /**
     * Checks if cancel button is shown when label is defined
     */
    public testShowCancelButtonWhenNoLabel () {
        const alert = this.mountComponent({ labelCancel: "Label" })
        this.assertTrue(this.privateCall(alert.vm, "showCancel"))
    }
}

(new OxAlertTest()).launchTests()
