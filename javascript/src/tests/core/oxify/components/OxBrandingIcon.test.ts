/**
 * @package Oxify\OxBrandingIcon
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { shallowMount } from "@vue/test-utils"
import OxTest from "@oxify/utils/OxTest"
import OxBrandingIconCore from "@oxify/components/OxBrandingIcon/OxBrandingIconCore"
import { Component } from "vue"
import OxBrandingIcon from "@oxify/components/OxBrandingIcon/OxBrandingIcon.vue"

/* eslint-disable dot-notation */

/**
 * OxBrandingIcon test class
 */
export default class OxBrandingIconTest extends OxTest {
    protected component = OxBrandingIcon

    private testPaths = [
        {
            d: "testpath",
            fill: "#fff"
        }
    ]

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    /**
     * Get SVG paths for predefined icon and no custom paths test
     */
    public testGetPathsPredefinedIconNoCustomPaths (): void {
        const icon = this.vueComponent({ type: "prosante" })
        this.assertEqual(this.privateCall(icon, "pathsIcon"), OxBrandingIconCore.prosantePaths)
    }

    /**
     * Get SVG paths for predefined icon and custom paths test
     */
    public testGetPathsPredefinedIconCustomPaths (): void {
        const icon = this.vueComponent({ type: "prosante", paths: this.testPaths })
        this.assertEqual(this.privateCall(icon, "pathsIcon"), this.testPaths)
    }

    /**
     * Get SVG paths for undefined icon and no custom paths test
     */
    public testGetPathsUndefinedIconNoCustomPaths (): void {
        const icon = this.vueComponent({ type: "cpx" })
        this.assertEqual(this.privateCall(icon, "pathsIcon"), [])
    }

    /**
     * Get SVG paths for undefined icon and custom paths test
     */
    public testGetPathsUndefinedIconCustomPaths (): void {
        const icon = this.vueComponent({ type: "cpx", paths: this.testPaths })
        this.assertEqual(this.privateCall(icon, "pathsIcon"), this.testPaths)
    }

    public testDefaultIconStyle (): void {
        const icon = this.vueComponent({ type: "gitlab" })
        expect(icon["iconStyle"]).toEqual({ width: "24px" })
    }

    public testCustomIconStyle (): void {
        const icon = this.vueComponent({ type: "gitlab", size: 64 })
        expect(icon["iconStyle"]).toEqual({ width: "64px" })
    }
}

(new OxBrandingIconTest()).launchTests()
