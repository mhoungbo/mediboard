/**
 * @package Oxify\OxCaution
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxThemeCore from "@oxify/utils/OxThemeCore"
import OxIconCore from "@oxify/components/OxIcon/OxIconCore"
import OxCaution from "@oxify/components/OxCaution/OxCaution.vue"

Vue.use(Vuetify)

/* eslint-disable dot-notation */

/**
 * OxCaution test class
 */
export default class OxCautionTest extends OxTest {
    protected component = OxCaution

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    /**
     * Adding "defaultPlain" CSS class when undefined type and plain caution test
     */
    public testDefaultPlainUndefinedTypePlain (): void {
        const caution = this.vueComponent({ plain: true })
        this.assertEqual(this.privateCall(caution, "defaultPlainClass"), "defaultPlain")
    }

    /**
     * No adding "defaultPlain" CSS class when defined type and plain caution test
     */
    public testDefaultPlainDefinedTypePlain (): void {
        const caution = this.vueComponent({ type: "success", plain: true })
        this.assertEqual(this.privateCall(caution, "defaultPlainClass"), "")
    }

    /**
     * No adding "defaultPlain" CSS class when no plain caution test
     */
    public testDefaultPlainUndefinedTypeNoPlain (): void {
        const caution = this.vueComponent({ plain: false })
        this.assertEqual(this.privateCall(caution, "defaultPlainClass"), "")
    }

    /**
     * Caution is outlined test
     */
    public testCautionIsOutlined (): void {
        const caution = this.vueComponent({ plain: false })
        this.assertTrue(this.privateCall(caution, "isOutlined"))
    }

    /**
     * Caution is not outlined test
     */
    public testCautionIsNotOutlined (): void {
        const caution = this.vueComponent({ plain: true })
        this.assertFalse(this.privateCall(caution, "isOutlined"))
    }

    /**
     * Divider color dark test
     */
    public testDividerColorDark (): void {
        const caution = this.vueComponent({ plain: false })
        this.assertEqual(this.privateCall(caution, "dividerColor"), "dark")
    }

    /**
     * Divider color light test
     */
    public testDividerColorLight (): void {
        const caution = this.vueComponent({ plain: true })
        this.assertEqual(this.privateCall(caution, "dividerColor"), "light")
    }

    /**
     * Caution has title test
     */
    public testCautionHasTitle (): void {
        const caution = this.vueComponent({ title: "Title" })
        this.assertTrue(this.privateCall(caution, "hasTitle"))
    }

    /**
     * Caution has no title test
     */
    public testCautionHasNoTitle (): void {
        const caution = this.vueComponent({ plain: true })
        this.assertFalse(this.privateCall(caution, "hasTitle"))
    }

    /**
     * Caution has content test
     */
    public testCautionHasContent (): void {
        const caution = this.vueComponent({ plain: true }, {}, { default: "slot content" })
        this.assertTrue(this.privateCall(caution, "hasContent"))
    }

    /**
     * Caution has no content test
     */
    public testCautionHasNoContent (): void {
        const caution = this.vueComponent({ plain: true })
        this.assertFalse(this.privateCall(caution, "hasContent"))
    }

    /**
     * Caution has subcontent test
     */
    public testCautionHasSubcontent (): void {
        const caution = this.vueComponent({ plain: true }, {}, { subcontent: "slot subcontent" })
        this.assertTrue(this.privateCall(caution, "hasSubcontent"))
    }

    /**
     * Caution has no subcontent test
     */
    public testCautionHasNoSubcontent (): void {
        const caution = this.vueComponent({ plain: true })
        this.assertFalse(this.privateCall(caution, "hasSubcontent"))
    }

    /**
     * Caution default color test
     */
    public testCautionDefaultColor (): void {
        const caution = this.vueComponent({})
        this.assertEqual(this.privateCall(caution, "alertColor"), OxThemeCore.grey500)
    }

    /**
     * Caution no icon test
     */
    public testNoIconSvg (): void {
        const caution = this.vueComponent({ showIcon: false })
        this.assertFalse(this.privateCall(caution, "iconSvg"))
    }

    /**
     * Caution custom icon test
     */
    public testCustomIconSvg (): void {
        const caution = this.vueComponent({ showIcon: true, icon: "add" })
        this.assertEqual(this.privateCall(caution, "iconSvg"), OxIconCore.get("add"))
    }
}

(new OxCautionTest()).launchTests()

describe(
    "OxCaution scenarios",
    () => {
        test.each([
            { typeProp: "success", colorExpected: OxThemeCore.successTextDefault },
            { typeProp: "error", colorExpected: OxThemeCore.errorTextDefault },
            { typeProp: "info", colorExpected: OxThemeCore.infoTextDefault },
            { typeProp: "warning", colorExpected: OxThemeCore.warningTextDefault }
        ])("with $typeProp should expect color : $expected", ({ typeProp, colorExpected }) => {
            const caution = shallowMount(OxCaution, {
                propsData: { showIcon: true, type: typeProp }
            })

            expect(caution.vm["alertColor"]).toEqual(colorExpected)
        })

        test.each([
            { typeProp: "success", iconExpected: OxIconCore.get("check") },
            { typeProp: "error", iconExpected: OxIconCore.get("alertCircle") },
            { typeProp: "info", iconExpected: OxIconCore.get("information") },
            { typeProp: "warning", iconExpected: OxIconCore.get("alert") }
        ])("with $typeProp should expect icon : $expected", ({ typeProp, iconExpected }) => {
            const caution = shallowMount(OxCaution, {
                propsData: { showIcon: true, type: typeProp }
            })

            expect(caution.vm["iconSvg"]).toEqual(iconExpected)
        })
    }
)
