/**
 * @package Oxify\OxDropdownButton
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxDropdownButton from "@oxify/components/OxDropdownButton/OxDropdownButton.vue"
import { OxDropdownButtonActionsGroups, OxDropdownButtonActionTypes } from "@oxify/types/OxDropdownButtonActionTypes"
import OxThemeCore from "@oxify/utils/OxThemeCore"

/* eslint-disable dot-notation */

/**
 * OxDropdownButton test class
 */
export default class OxDropdownButtonTest extends OxTest {
    component = OxDropdownButton

    private dropdownActionsWithIcon: Array<OxDropdownButtonActionTypes> = [
        {
            label: "Action 1",
            eventName: "actionOne",
            icon: "add"
        },
        {
            label: "Alternative Action  2",
            eventName: "actionTwo"
        }
    ]

    private dropdownActionsWithoutIcon: Array<OxDropdownButtonActionTypes> = [
        {
            label: "Action 1",
            eventName: "actionOne"
        },
        {
            label: "Alternative Action  2",
            eventName: "actionTwo",
            disabled: true
        }
    ]

    private dropdownActionsWithIconHidden: Array<OxDropdownButtonActionTypes> = [
        {
            label: "Action 1",
            eventName: "actionOne",
            icon: "add",
            hide: true
        },
        {
            label: "Alternative Action  2",
            eventName: "actionTwo",
            disabled: true
        }
    ]

    private dropdownActionsGroupWithIcon: Array<OxDropdownButtonActionsGroups> = [
        {
            label: "Group 1",
            actions: [
                {
                    label: "Action 1",
                    eventName: "actionOne",
                    icon: "add"
                },
                {
                    label: "Alternative Action  2",
                    eventName: "actionTwo"
                }
            ]
        }
    ]

    private dropdownActionsGroupWithoutIcon: Array<OxDropdownButtonActionsGroups> = [
        {
            label: "Group 1",
            actions: [
                {
                    label: "Action 1",
                    eventName: "actionOne"
                },
                {
                    label: "Alternative Action  2",
                    eventName: "actionTwo",
                    disabled: true
                }
            ]
        }
    ]

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return super.mountComponent(props)
    }

    /**
     * Default dropdown button detection test
     */
    public testDetectionDefaultButton (): void {
        const button = this.vueComponent({ label: "label", altActions: [] })
        this.assertTrue(this.privateCall(button, "buttonDefault"))
    }

    /**
     * No detection split button to default dropdown button test
     */
    public testNonDetectionSplitButtonInDefault (): void {
        const button = this.vueComponent({
            label: "label",
            split: true,
            altActions: []
        })
        this.assertFalse(this.privateCall(button, "buttonDefault"))
    }

    /**
     * No detection notext button to default dropdown button test
     */
    public testNonDetectionNoTextButtonInDefault (): void {
        const button = this.vueComponent({ altActions: [] })
        this.assertFalse(this.privateCall(button, "buttonDefault"))
    }

    /**
     * Split dropdown button detection test
     */
    public testDetectionSplitButton (): void {
        const button = this.vueComponent({
            label: "label",
            split: true,
            altActions: []
        })
        const button2 = this.vueComponent({
            label: "label",
            split: "",
            altActions: []
        })
        this.assertTrue(this.privateCall(button, "buttonSplit"))
        this.assertTrue(this.privateCall(button2, "buttonSplit"))
    }

    /**
     * No detection default button to split dropdown button test
     */
    public testNonDetectionDefaultButtonInSplit (): void {
        const button = this.vueComponent({ label: "label", altActions: [] })
        this.assertFalse(this.privateCall(button, "buttonSplit"))
    }

    /**
     * No detection notext button to split dropdown button test
     */
    public testNonDetectionNoTextButtonInSplit (): void {
        const button = this.vueComponent({ altActions: [] })
        this.assertFalse(this.privateCall(button, "buttonSplit"))
    }

    /**
     * No detection tertiary button to split dropdown button test
     */
    public testNonDetectionTertiaryButtonInSplit (): void {
        const button = this.vueComponent({
            label: "Label",
            split: true,
            buttonStyle: "tertiary",
            altActions: []
        })
        this.assertFalse(this.privateCall(button, "buttonSplit"))
    }

    /**
     * No detection tertiary-dark button to split dropdown button test
     */
    public testNonDetectionTertiaryDarkButtonInSplit (): void {
        const button = this.vueComponent({
            label: "Label",
            split: true,
            buttonStyle: "tertiary-dark",
            altActions: []
        })
        this.assertFalse(this.privateCall(button, "buttonSplit"))
    }

    /**
     * Notext dropdown button detection test
     */
    public testDetectionNoTextButton (): void {
        const button = this.vueComponent({ altActions: [] })
        this.assertTrue(this.privateCall(button, "buttonNoText"))
    }

    /**
     * No detection button with label to NoText dropdown button test
     */
    public testNonDetectionLabelButtonInNoText (): void {
        const button = this.vueComponent({ label: "Label", altActions: [] })
        this.assertFalse(this.privateCall(button, "buttonNoText"))
    }

    /**
     * Modifier class test
     */
    public testButtonClass (): void {
        const buttonSplit = this.vueComponent({
            label: "label",
            split: true,
            altActions: []
        })
        const buttonDefault = this.vueComponent({ label: "label", altActions: [] })
        const buttonNoText = this.vueComponent({ altActions: [] })
        this.assertEqual(this.privateCall(buttonSplit, "buttonClass"), "split")
        this.assertEqual(this.privateCall(buttonDefault, "buttonClass"), "default")
        this.assertEqual(this.privateCall(buttonNoText, "buttonClass"), "")
    }

    /**
     * Icon no detection in dropdown actions test
     */
    public testNonExistIcon (): void {
        const buttonWithoutIcons = this.vueComponent({
            label: "label",
            altActions: this.dropdownActionsWithoutIcon
        })
        this.assertFalse(this.privateCall(buttonWithoutIcons, "existIcon"))
    }

    public testNonExistIconWithHiddenIcon (): void {
        const buttonWithoutIcons = this.vueComponent({
            label: "label",
            altActions: this.dropdownActionsWithIconHidden
        })
        this.assertFalse(this.privateCall(buttonWithoutIcons, "existIcon"))
    }

    public testWithGroupActionsNonExistIcon (): void {
        const buttonWithoutIcons = this.vueComponent({
            label: "label",
            altActions: this.dropdownActionsGroupWithoutIcon
        })
        this.assertFalse(this.privateCall(buttonWithoutIcons, "existIcon"))
    }

    public testWithGroupActionsExistIcon (): void {
        const buttonWithoutIcons = this.vueComponent({
            label: "label",
            altActions: this.dropdownActionsGroupWithIcon
        })
        this.assertTrue(this.privateCall(buttonWithoutIcons, "existIcon"))
    }

    /**
     * No detection of an icon when there is no dropdown action test
     */
    public testNonExistIconVoidDropdown (): void {
        const buttonWithoutDropdownAction = this.vueComponent({ label: "label", altActions: [] })
        this.assertFalse(this.privateCall(buttonWithoutDropdownAction, "existIcon"))
    }

    /**
     * Primary button color test
     */
    public testButtonPrimaryColor (): void {
        const button = this.vueComponent({ buttonStyle: "primary", altActions: [] })
        this.assertEqual(this.privateCall(button, "buttonColor"), "primary")
    }

    /**
     * Secondary button color test
     */
    public testButtonSecondaryColor (): void {
        const button = this.vueComponent({ buttonStyle: "secondary", altActions: [] })
        this.assertEqual(this.privateCall(button, "buttonColor"), "primary")
    }

    /**
     * Tertiary button color test
     */
    public testButtonTertiaryColor (): void {
        const button = this.vueComponent({ buttonStyle: "tertiary", altActions: [] })
        this.assertEqual(this.privateCall(button, "buttonColor"), "secondary")
    }

    /**
     * Tertiary dark button color test
     */
    public testButtonTertiaryDarkColor (): void {
        const button = this.vueComponent({ buttonStyle: "tertiary-dark", altActions: [] })
        this.assertEqual(this.privateCall(button, "buttonColor"), OxThemeCore.grey600)
    }

    /**
     * Check button is not text when primary
     */
    public testPrimaryButtonIsNotText (): void {
        const button = this.vueComponent({ buttonStyle: "primary", altActions: [] })
        this.assertFalse(this.privateCall(button, "buttonText"))
    }

    /**
     * Check button is not text when secondary
     */
    public testSecondaryButtonIsNotText (): void {
        const button = this.vueComponent({ buttonStyle: "secondary", altActions: [] })
        this.assertFalse(this.privateCall(button, "buttonText"))
    }

    /**
     * Check button is text when tertiary
     */
    public testTertiaryButtonIsNotText (): void {
        const button = this.vueComponent({ buttonStyle: "tertiary", altActions: [] })
        this.assertTrue(this.privateCall(button, "buttonText"))
    }

    /**
     * Check button is text when tertiary dark
     */
    public testTertiaryDarkButtonIsNotText (): void {
        const button = this.vueComponent({ buttonStyle: "tertiary-dark", altActions: [] })
        this.assertTrue(this.privateCall(button, "buttonText"))
    }

    /**
     * Main button click in split mode test
     */
    public testMainClickOnDropdownSplit (): void {
        const button = this.mountComponent({
            label: "label",
            split: true,
            altActions: this.dropdownActionsWithIcon
        })
        this.privateCall(button.vm, "mainActionClick", {})
        this.assertTrue(button.emitted().click)
    }
}

(new OxDropdownButtonTest()).launchTests()
