/**
 * @package Oxify\OxSnackbar
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxSections from "@oxify/components/OxSections/OxSections.vue"

Vue.use(Vuetify)

/* eslint-disable dot-notation */

/**
 * OxSections test class
 */
export default class OxSectionsTest extends OxTest {
    protected component = OxSections

    private sections = [
        {
            title: "Section 1",
            name: "section_1"
        },
        {
            title: "Section 2",
            name: "section_2"
        },
        {
            title: "Section 3",
            name: "section_3"
        }
    ]

    protected afterTest () {
        super.afterTest()
        jest.resetAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = { sections: this.sections },
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = { sections: this.sections },
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testMounted (): void {
        const sections = this.vueComponent()
        expect(sections["currentSectionIndex"]).toBe(0)
    }

    public testHasActiveClass (): void {
        const sections = this.vueComponent()
        expect(
            this.privateCall(
                sections,
                "activeClass",
                "section_1"
            )
        ).toBe("active")
    }

    public testHasNotActiveClass (): void {
        const sections = this.vueComponent()
        expect(
            this.privateCall(
                sections,
                "activeClass",
                "section_2"
            )
        ).toBe("")
    }

    public testCurrentElement () {
        const sections = this.vueComponent()
        sections["currentSectionIndex"] = 1
        const element = this.privateCall(sections, "currentElement")
        expect(element.id).toBe("section_2")
    }

    public testCurrentSection () {
        const sections = this.vueComponent()
        sections["currentSectionIndex"] = 1
        const section = this.privateCall(sections, "currentSection")
        expect(section).toMatchObject({
            title: "Section 2",
            name: "section_2"
        })
    }

    public testScrollToLastSection (): void {
        Object.defineProperty(Element.prototype, "scrollHeight", {
            value: 3000
        })
        Object.defineProperty(Element.prototype, "scrollTop", {
            value: 1800
        })
        Object.defineProperty(Element.prototype, "clientHeight", {
            value: 1200
        })
        const sections = this.vueComponent()
        this.privateCall(sections, "scrollSection")

        expect(sections["currentSectionIndex"]).toBe(2)
    }

    public testScrollPreviousSection () {
        Object.defineProperty(Element.prototype, "scrollHeight", {
            value: 3000
        })
        Object.defineProperty(Element.prototype, "scrollTop", {
            value: 800
        })
        Object.defineProperty(Element.prototype, "clientHeight", {
            value: 1200
        })

        jest.spyOn(Element.prototype, "getBoundingClientRect")
            // @ts-ignore
            .mockReturnValueOnce({ top: 600 })
            // @ts-ignore
            .mockReturnValueOnce({ top: 300 })

        const sections = this.vueComponent()
        sections["currentSectionIndex"] = 1
        this.privateCall(sections, "scrollSection")
        expect(sections["currentSectionIndex"]).toBe(0)
    }

    public testScrollNextSection () {
        Object.defineProperty(Element.prototype, "scrollHeight", {
            value: 3000
        })
        Object.defineProperty(Element.prototype, "scrollTop", {
            value: 800
        })
        Object.defineProperty(Element.prototype, "clientHeight", {
            value: 1200
        })

        jest.spyOn(Element.prototype, "getBoundingClientRect")
            // @ts-ignore
            .mockReturnValueOnce({ top: 400 })
            // @ts-ignore
            .mockReturnValueOnce({ top: 300 })
            // @ts-ignore
            .mockReturnValueOnce({ top: 400 })
            // @ts-ignore
            .mockReturnValueOnce({ top: 210 })

        const sections = this.vueComponent()
        sections["currentSectionIndex"] = 1
        this.privateCall(sections, "scrollSection")
        expect(sections["currentSectionIndex"]).toBe(2)
    }

    public async testDestroy (): Promise<void> {
        const sections = this.mountComponent()
        Object.defineProperty(sections.vm["containerElement"], "removeEventListener", {
            value: jest.fn()
        })

        await sections.destroy()
        expect(sections.vm["containerElement"].removeEventListener).toHaveBeenCalledWith(
            "scroll",
            sections.vm["scrollSection"]
        )
    }
}

(new OxSectionsTest()).launchTests()
