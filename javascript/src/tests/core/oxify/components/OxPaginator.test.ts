/**
 * @package Oxify\OxPaginator
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxPaginator from "@oxify/components/OxPaginator/OxPaginator.vue"

Vue.use(Vuetify)

/* eslint-disable dot-notation */

class OxPaginatorTest extends OxTest {
    component = OxPaginator

    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    public testTotalPageWithMultiplePage () {
        const paginator = this.mountComponent({ total: 122, perPage: 20 })
        expect(paginator.vm["totalPages"]).toEqual(7)
    }

    public testTotalPageWithSinglePage () {
        const paginator = this.mountComponent({ total: 22, perPage: 50 })
        expect(paginator.vm["totalPages"]).toEqual(1)
    }

    public testTotalPageWithNoItems () {
        const paginator = this.mountComponent({ total: 0, perPage: 10 })
        expect(paginator.vm["totalPages"]).toEqual(1)
    }

    public testInputEmit () {
        const paginator = this.mountComponent({ total: 122, perPage: 20 })
        paginator.vm["changePage"](2)
        expect(paginator.emitted().input).toEqual([[2]])
    }
}

(new OxPaginatorTest()).launchTests()
