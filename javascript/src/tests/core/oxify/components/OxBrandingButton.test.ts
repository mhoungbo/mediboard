/**
 * @package Oxify\OxBrandingButton
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { Wrapper } from "@vue/test-utils"
import OxTest from "@oxify/utils/OxTest"
import OxBrandingButton from "@oxify/components/OxBrandingButton/OxBrandingButton.vue"

/**
 * OxBrandingButton test class
 */
export default class OxBrandingButtonTest extends OxTest {
    protected component = OxBrandingButton

    private testPaths = [
        {
            d: "testpath",
            fill: "#fff"
        }
    ]

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object = {}) {
        return super.vueComponent(props)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return super.mountComponent(props)
    }

    /**
     * Adding "iconOnly" CSS class when showing icon and no label test
     */
    public testIconOnlyShowingIconNoLabel (): void {
        const button = this.vueComponent({ type: "prosante" })
        this.assertEqual(this.privateCall(button, "iconOnly"), "iconOnly")
    }

    /**
     * No adding "iconOnly" CSS class when showing icon and label test
     */
    public testNoIconOnlyShowingIconWithLabel (): void {
        const button = this.vueComponent({ type: "prosante", label: "Test" })
        this.assertEqual(this.privateCall(button, "iconOnly"), "")
    }

    /**
     * No adding "iconOnly" CSS class when no showing icon and no label test
     */
    public testNoIconOnlyNoShowingIconNoLabel (): void {
        const button = this.vueComponent({ type: "cpx" })
        this.assertEqual(this.privateCall(button, "iconOnly"), "")
    }

    /**
     * No adding "iconOnly" CSS class when no showing icon and label test
     */
    public testNoIconOnlyNoShowingIconWithLabel (): void {
        const button = this.vueComponent({ type: "cpx", label: "Test" })
        this.assertEqual(this.privateCall(button, "iconOnly"), "")
    }

    /**
     * Button is "iconOnly" test
     */
    public testButtonIsIconOnly (): void {
        const button = this.vueComponent({ type: "prosante" })
        this.assertTrue(this.privateCall(button, "isIconOnly"))
    }

    /**
     * Button is not "iconOnly" test
     */
    public testButtonIsNotIconOnly (): void {
        const button = this.vueComponent({ type: "prosante", label: "Test" })
        this.assertFalse(this.privateCall(button, "isIconOnly"))
    }

    /**
     * Show icon for type with predefined icon and no custom paths test
     */
    public testShowIconPredefinedIconNoCustomPaths (): void {
        const button = this.vueComponent({ type: "prosante" })
        this.assertTrue(this.privateCall(button, "showIcon"))
    }

    /**
     * Show icon for type without predefined icon and no empty custom paths test
     */
    public testShowIconNoPredefinedIconNoEmptyCustomPaths (): void {
        const button = this.vueComponent({ type: "cpx", paths: this.testPaths })
        this.assertTrue(this.privateCall(button, "showIcon"))
    }

    /**
     * No show icon for type without predefined icon and no custom paths test
     */
    public testNoShowIconNoPredefinedIconNoCustomPaths (): void {
        const button = this.vueComponent({ type: "cpx" })
        this.assertFalse(this.privateCall(button, "showIcon"))
    }

    /**
     * No show icon for empty custom paths test
     */
    public testNoShowIconEmptyCustomPaths (): void {
        const button = this.vueComponent({ paths: [] })
        this.assertFalse(this.privateCall(button, "showIcon"))
    }

    /**
     * Button click test
     */
    public testClick (): void {
        const button = this.mountComponent({})
        this.privateCall(button.vm, "click", {})
        this.assertTrue(button.emitted("click"))
    }
}

(new OxBrandingButtonTest()).launchTests()
