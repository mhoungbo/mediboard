/**
 * @package Oxify\OxTooltip
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { shallowMount } from "@vue/test-utils"

import Vue from "vue"
import Vuetify from "vuetify"
import OxTooltip from "@oxify/components/OxTooltip/OxTooltip.vue"

Vue.use(Vuetify)

/* eslint-disable dot-notation */

describe(
    "OxTooltip",
    () => {
        test.each([
            { position: "bottom" },
            { position: "right" },
            { position: "top" },
            { position: "left" }
        ])("test position with position: $position", ({ position }) => {
            const tooltip = shallowMount(OxTooltip, {
                propsData: { position }
            }).vm

            expect(tooltip[position]).toBeTruthy()
        })
    }
)
