/**
 * @package Oxify\OxTab
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { shallowMount } from "@vue/test-utils"
import OxTab from "@oxify/components/OxTab/OxTab.vue"

/* eslint-disable dot-notation */

describe(
    "OxTab",
    () => {
        test.each([
            { position: "bottom" },
            { position: "right" },
            { position: "top" },
            { position: "left" }
        ])("test iconPosition with position: $position",
            ({ position }) => {
                const tab = shallowMount(OxTab, {
                    propsData: { iconPosition: position }
                }).vm

                expect(tab["contentClass"]).toEqual({
                    "OxTab-tabContent": true,
                    [position]: true
                })
            }
        )

        test("emitClick should forward event",
            () => {
                const tab = shallowMount(OxTab)

                tab.vm["emitClick"]("")

                expect(tab.emitted()).toHaveProperty("click")
            }
        )
    }
)
