/**
 * @package Oxify\OxProgressLinear
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue from "vue"
import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxProgressLinear from "@oxify/components/OxProgressLinear/OxProgressLinear.vue"
import OxThemeCore from "@oxify/utils/OxThemeCore"

Vue.use(Vuetify)

/* eslint-disable dot-notation */

export default class OxProgressLinearTest extends OxTest {
    component = OxProgressLinear

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object) {
        return super.vueComponent(props)
    }

    public testDefaultColor (): void {
        const progressLinear = this.vueComponent({})
        this.assertEqual(progressLinear["color"], OxThemeCore.primary)
    }

    public testDefaultBufferValue (): void {
        const progressLinear = this.vueComponent({})
        this.assertEqual(progressLinear["bufferValue"], 0)
    }
}

(new OxProgressLinearTest()).launchTests()
