/**
 * @package Oxify\OxButton
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxButton from "@oxify/components/OxButton/OxButton.vue"
import OxTest from "@oxify/utils/OxTest"

export default class OxButtonTest extends OxTest {
    component = OxButton

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return super.mountComponent(props)
    }

    /**
     * Button click test
     */
    public testClick (): void {
        const button = this.mountComponent({})
        this.privateCall(button.vm, "click", {})
        this.assertTrue(button.emitted("click"))
    }

    /**
     * Click while loading test
     */
    public testClickWhileLoading (): void {
        const button = this.mountComponent({ loading: true })
        this.privateCall(button.vm, "click", {})
        this.assertFalse(button.emitted("click"))
    }

    /**
     * Button default class test
     */
    public testButtonDefaultClass (): void {
        const button = this.vueComponent({ buttonStyle: "primary" })
        this.assertEqual(this.privateCall(button, "buttonClasses"), " OxButton")
    }

    /**
     * Button custom class test
     */
    public testButtonCustomClass (): void {
        const button = this.vueComponent({ buttonStyle: "primary", customClass: "test" })
        this.assertEqual(this.privateCall(button, "buttonClasses"), "test OxButton")
    }

    /**
     * Button tertiaryDark class test
     */
    public testButtonTertiaryDarkClass (): void {
        const button = this.vueComponent({ buttonStyle: "tertiary-dark" })
        this.assertEqual(this.privateCall(button, "buttonClasses"), " tertiaryDark OxButton")
    }

    public testButtonCurrentColorClass (): void {
        const button = this.vueComponent({ color: "currentColor" })
        this.assertEqual(this.privateCall(button, "buttonClasses"), " currentColor OxButton")
    }

    public testButtonWithCustomColor (): void {
        const button = this.vueComponent({ color: "#227722" })
        this.assertEqual(this.privateCall(button, "color"), "#227722")
    }

    /**
     * Primary button color test
     */
    public testButtonPrimaryColor (): void {
        const button = this.vueComponent({ buttonStyle: "primary" })
        this.assertEqual(this.privateCall(button, "buttonColor"), "primary")
    }

    /**
     * Secondary button color test
     */
    public testButtonSecondaryColor (): void {
        const button = this.vueComponent({ buttonStyle: "secondary" })
        this.assertEqual(this.privateCall(button, "buttonColor"), "primary")
    }

    /**
     * Tertiary button color test
     */
    public testButtonTertiaryColor (): void {
        const button = this.vueComponent({ buttonStyle: "tertiary" })
        this.assertEqual(this.privateCall(button, "buttonColor"), "secondary")
    }

    /**
     * Tertiary dark button without label color test
     */
    public testButtonTertiaryDarkWithoutLabelColor (): void {
        const button = this.vueComponent({ buttonStyle: "tertiary-dark" })
        this.assertNull(this.privateCall(button, "buttonColor"))
    }

    /**
     * Tertiary dark button with label color test
     */
    public testButtonTertiaryDarkWithLabelColor (): void {
        const button = this.vueComponent({ buttonStyle: "tertiary-dark", label: "Button" })
        this.assertEqual(this.privateCall(button, "buttonColor"), "rgba(0, 0, 0, 0)")
    }

    /**
     * Default icon size test
     */
    public testDefaultIconSize (): void {
        const button = this.vueComponent({ smallPrimaryIconOnly: false })
        this.assertEqual(this.privateCall(button, "iconSize"), 24)
    }

    /**
     * Small primary icon-only icon size test
     */
    public testSmallIconSize (): void {
        const button = this.vueComponent({ smallPrimaryIconOnly: true })
        this.assertEqual(this.privateCall(button, "iconSize"), 18)
    }

    /**
     * Small primary icon-only button size test
     */
    public testSmallButtonSize (): void {
        const button = this.vueComponent({ smallPrimaryIconOnly: true })
        this.assertEqual(this.privateCall(button, "buttonSize"), 24)
    }

    /**
     * Default button size test
     */
    public testDefaultButtonSize (): void {
        const button = this.vueComponent({ smallPrimaryIconOnly: false })
        this.assertUndefined(this.privateCall(button, "buttonSize"))
    }
}

(new OxButtonTest()).launchTests()
