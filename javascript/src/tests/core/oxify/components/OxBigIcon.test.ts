/**
 * @package Oxify\OxBigIcon
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { Component } from "vue"
import { shallowMount } from "@vue/test-utils"
import OxBigIcon from "@oxify/components/OxBigIcon/OxBigIcon.vue"
import OxTest from "@oxify/utils/OxTest"
/**
 * OxBigIcon test class
 */
export default class OxBigIconTest extends OxTest {
    protected component = OxBigIcon

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testValidContextValue (): void {
        // @ts-ignore
        const validator = OxBigIcon.options.props.context.validator
        expect(validator("onBackground")).toBeTruthy()
        expect(validator("onPrimary")).toBeTruthy()
    }

    public testInvalidContextValue (): void {
        // @ts-ignore
        const validator = OxBigIcon.options.props.context.validator
        expect(validator("invalidValue")).toBeFalsy()
    }
}

(new OxBigIconTest()).launchTests()
