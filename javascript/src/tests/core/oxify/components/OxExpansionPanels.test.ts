/**
 * @package Oxify\OxExpansionPanel
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxExpansionPanels from "@oxify/components/OxExpansionPanels/OxExpansionPanels.vue"

Vue.use(Vuetify)

class OxExpansionPanelsTest extends OxTest {
    component = OxExpansionPanels

    protected beforeAllTests () {
        super.beforeAllTests()
        window.console.error = jest.fn()
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testValidPropsWithMultiple () {
        this.vueComponent({ multiple: true, value: [1, 2] })
        expect(window.console.error).not.toBeCalled()
    }

    public testValidPropsWithoutMultiple () {
        this.vueComponent({ value: 1 })
        expect(window.console.error).not.toBeCalled()
    }

    public testInvalidPropsWithMultiple () {
        this.vueComponent({ multiple: true, value: ["val1", "val2"] })
        expect(window.console.error).toBeCalledWith("Value has to be an array of number")
    }

    public testInvalidPropsWithoutMultiple () {
        this.vueComponent({ value: "test" })
        expect(window.console.error).toBeCalledWith("Value has to be a number")
    }

    public testHasNotClassTransparent () {
        const expansionPanels = this.vueComponent({ value: 1 })
        expect(this.privateCall(expansionPanels, "classes")).toEqual({
            OxExpansionPanels: true,
            transparent: false
        })
    }

    public testHasClassTransparent () {
        const expansionPanels = this.vueComponent({ transparent: true, value: 1 })
        expect(this.privateCall(expansionPanels, "classes")).toEqual({
            OxExpansionPanels: true,
            transparent: true
        })
    }

    public testInput () {
        const expansionPanels = this.mountComponent({ value: 1 })
        const expectedNewValue = 2
        this.privateCall(expansionPanels.vm, "input", expectedNewValue)

        const events = expansionPanels.emitted("input")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }
}

(new OxExpansionPanelsTest()).launchTests()
