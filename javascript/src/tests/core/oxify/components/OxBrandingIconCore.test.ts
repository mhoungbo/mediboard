/**
 * @package Oxify\OxBrandingIcon
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@oxify/utils/OxTest"
import OxBrandingIconCore from "@oxify/components/OxBrandingIcon/OxBrandingIconCore"

/**
 * OxBrandingIconCore test class
 */
export default class OxBrandingIconCoreTest extends OxTest {
    protected component = OxBrandingIconCore

    /**
     * Existing SVG icon test
     */
    public testExistingSvgIcon (): void {
        const iconSvg = OxBrandingIconCore.get("prosante")
        this.assertEqual(iconSvg, OxBrandingIconCore.prosantePaths)
    }

    /**
     * Nonexistent SVG icon test
     */
    public testNonexistentSvgIcon (): void {
        const iconSvg = OxBrandingIconCore.get("nonexistent")
        this.assertEqual(iconSvg, [])
    }
}

(new OxBrandingIconCoreTest()).launchTests()
