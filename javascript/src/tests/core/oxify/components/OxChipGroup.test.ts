/**
 * @package Oxify\OxChipGroup
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue from "vue"
import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxChipGroup from "@oxify/components/OxChipGroup/OxChipGroup.vue"

Vue.use(Vuetify)

/**
 * OxChipGroup test class
 */
export default class OxChipGroupTest extends OxTest {
    protected component = OxChipGroup

    private choices: Array<{ label: string; value: string }> = [
        {
            label: "Label1",
            value: "value1"
        },
        {
            label: "Label2",
            value: "value2"
        },
        {
            label: "Label3",
            value: "value3"
        }
    ]

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object = {}) {
        return super.vueComponent(props)
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object) {
        return super.mountComponent(props)
    }

    /**
     * Return value generation test for a non-multiple OxChipGroup
     */
    public testGenerateSingleSelectedWithValue (): void {
        const chipGroup = this.vueComponent({
            choices: this.choices
        })
        this.assertEqual(
            this.privateCall(chipGroup, "generateSelectedWithValue", 0),
            this.choices[0].value
        )
    }

    /**
     * Return value generation test for a multiple OxChipGroup
     */
    public testGenerateMultipleSelectedWithValue (): void {
        const chipGroup = this.vueComponent({
            choices: this.choices,
            multiple: true
        })
        this.assertEqual(
            this.privateCall(chipGroup, "generateSelectedWithValue", [1, 2]),
            [this.choices[1].value, this.choices[2].value]
        )
    }

    /**
     * Return value generation with empty selection test
     */
    public testGenerateSelectedWithNoSelected (): void {
        const chipGroup = this.vueComponent({
            choices: this.choices
        })
        this.assertUndefined(this.privateCall(chipGroup, "generateSelectedWithValue", {}))
    }

    /**
     * Change event emission on value change test
     */
    public async testEmitInput (): Promise<void> {
        const chipGroup = this.mountComponent({
            choices: this.choices,
            multiple: true
        })
        this.privateCall(chipGroup.vm, "emitInput", {})
        await chipGroup.vm.$nextTick()
        this.assertTrue(chipGroup.emitted("input"))
    }
}

(new OxChipGroupTest()).launchTests()
