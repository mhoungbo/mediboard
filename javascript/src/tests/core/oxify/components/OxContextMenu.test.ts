/**
 * @package Oxify\OxContextMenu
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxContextMenu from "@oxify/components/OxContextMenu/OxContextMenu.vue"

Vue.use(Vuetify)

/**
 * OxContextMenu test class
 */
export default class OxContextMenuTest extends OxTest {
    protected component = OxContextMenu

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {}
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public async testDisplayMenu (): Promise<void> {
        const contextmenu = this.vueComponent({})
        this.privateCall(contextmenu, "displayMenu", new MouseEvent("contextmenu"))
        await contextmenu.$nextTick()
        this.assertTrue(this.privateCall(contextmenu, "showMenu"))
    }
}

(new OxContextMenuTest()).launchTests()
