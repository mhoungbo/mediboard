/**
 * @package Oxify\OxDate
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import OxTest from "@/core/oxify/utils/OxTest"
import OxDate from "@oxify/components/OxDate/OxDate.vue"

/**
 * OxDate test class
 */
export default class OxDateTest extends OxTest {
    protected component = OxDate

    /**
     * Empty date format test
     */
    public testDateFormatNull (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({}),
                "dateFormat"
            ),
            ""
        )
    }

    /**
     * Datetime date format test
     */
    public testDateFormatDateTime (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ dateString: "2021-05-21 15:35:10", mode: "datetime" }),
                "dateFormat"
            ),
            "21/05/2021 15:35"
        )
    }

    /**
     * Month date format test
     */
    public testDateFormatMonth (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ dateString: "2021-05-21 15:35:10", mode: "month" }),
                "dateFormat"
            ),
            "OxDate-ShortMonth-Mai. 2021"
        )
    }

    /**
     * Hour date format test$
     */
    public testDateFormatTime (): void {
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ dateString: "2021-05-21 15:35:10", mode: "time" }),
                "dateFormat"
            ),
            "15:35"
        )
    }

    /**
     * Current date retrieving test
     */
    public testGetDate (): void {
        this.assertFalse(
            this.privateCall(
                this.vueComponent({}),
                "getDate"
            )
        )

        const objectDate = new Date()
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ date: objectDate }),
                "getDate"
            ),
            objectDate
        )

        const dateString = "2020-01-01 15:35:10"
        this.assertEqual(
            this.privateCall(
                this.vueComponent({ dateString }),
                "getDate"
            ),
            new Date(dateString)
        )
    }
}

(new OxDateTest()).launchTests()
