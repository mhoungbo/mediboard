/**
 * @package Oxify\OxSorter
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue, { Component } from "vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"

import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxSorter from "@oxify/components/OxSorter/OxSorter.vue"
import OxTranslator from "@/core/plugins/OxTranslator"

Vue.use(Vuetify)

const localVue = createLocalVue()
localVue.use(OxTranslator)

/**
 * OxSorter test class
 */
export default class OxSorterTest extends OxTest {
    protected component = OxSorter

    private choices = [
        {
            label: "First criteria",
            value: "first_criteria"
        },
        {
            label: "Second criteria",
            value: "second_criteria"
        },
        {
            label: "Third criteria",
            value: "third_criteria"
        }
    ]

    /**
     * @inheritDoc
     */
    protected mountComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                slots,
                stubs,
                methods: {},
                localVue
            }
        )
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (
        props: object = {},
        stubs: { [key: string]: Component | string | boolean } | string[] = {},
        slots: { [key: string]: (Component | string)[] | Component | string } = {}
    ) {
        return this.mountComponent(props, stubs, slots).vm
    }

    public testValidAspectValue () {
        // @ts-ignore
        const validator = OxSorter.options.props.aspect.validator
        expect(validator("select")).toBeTruthy()
        expect(validator("text")).toBeTruthy()
    }

    public testInvalidAspectValue () {
        // @ts-ignore
        const validator = OxSorter.options.props.aspect.validator
        expect(validator("invalidValue")).toBeFalsy()
    }

    public testCurrentIconAsc (): void {
        const sorter = this.vueComponent({})
        this.assertEqual(this.privateCall(sorter, "currentIcon"), "sortAscending")
    }

    public testCurrentIconDesc (): void {
        const sorter = this.vueComponent({ defaultSort: "desc" })
        this.assertEqual(this.privateCall(sorter, "currentIcon"), "sortDescending")
    }

    public testCurrentChoiceLabelWithoutDefaultValue (): void {
        const sorter = this.vueComponent({ choices: this.choices })
        this.assertEqual(this.privateCall(sorter, "currentChoiceLabel"), "No filter")
    }

    public testCurrentChoiceLabelWithDefaultValue (): void {
        const sorter = this.vueComponent({ choices: this.choices, defaultChoice: "first_criteria" })
        this.assertEqual(this.privateCall(sorter, "currentChoiceLabel"), "First criteria")
    }

    public testCurrentChoiceLabelWithInvalidValue (): void {
        const sorter = this.vueComponent({ choices: this.choices, defaultChoice: "invalid_value" })
        this.assertEqual(this.privateCall(sorter, "currentChoiceLabel"), "Invalid value")
    }

    public testShowSortIcon (): void {
        const sorter = this.vueComponent({ choices: this.choices, defaultChoice: "first_criteria" })
        this.assertTrue(this.privateCall(sorter, "showSortIcon"))
    }

    public testNotShowSortIcon (): void {
        const sorter = this.vueComponent({ choices: this.choices })
        this.assertFalse(this.privateCall(sorter, "showSortIcon"))
    }

    public testEmptyLabelClass (): void {
        const sorter = this.vueComponent({ choices: this.choices })
        this.assertEqual(this.privateCall(sorter, "labelClass"), "empty")
    }

    public testNoLabelClass (): void {
        const sorter = this.vueComponent({ choices: this.choices, defaultChoice: "first_criteria" })
        this.assertEqual(this.privateCall(sorter, "labelClass"), "")
    }

    public testChangeSortFromAsctoDesc (): void {
        const sorter = this.mountComponent({ choices: this.choices, defaultChoice: "first_criteria" })
        const expectedNewValue = {
            choice: "first_criteria",
            sort: "DESC"
        }
        this.privateCall(sorter.vm, "changeSort")

        const events = sorter.emitted("update")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }

    public testChangeSortFromDescToAsc (): void {
        const sorter = this.mountComponent({
            choices: this.choices,
            defaultChoice: "first_criteria",
            defaultSort: "DESC"
        })
        const expectedNewValue = {
            choice: "first_criteria",
            sort: "ASC"
        }
        this.privateCall(sorter.vm, "changeSort")

        const events = sorter.emitted("update")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }

    public testChangeValue (): void {
        const sorter = this.mountComponent({ choices: this.choices, defaultChoice: "first_criteria" })
        const expectedNewValue = {
            choice: "second_criteria",
            sort: "ASC"
        }
        this.privateCall(sorter.vm, "changeValue", "second_criteria")

        const events = sorter.emitted("update")
        this.assertHaveLength(events, 1)

        const lastEvent = Array.isArray(events) ? events[0] : false
        this.assertEqual(
            lastEvent,
            [expectedNewValue]
        )
    }
}

(new OxSorterTest()).launchTests()
