/**
 * @package Oxify\OxDivider
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import Vue from "vue"
import Vuetify from "vuetify"
import OxTest from "@oxify/utils/OxTest"
import OxDivider from "@oxify/components/OxDivider/OxDivider.vue"

Vue.use(Vuetify)

/**
 * OxDivider test class
 */
export default class OxDividerTest extends OxTest {
    protected component = OxDivider

    private inset = 5
    private axialHorizontalStyle = {
        "margin-left": this.inset + "px",
        "max-width": "calc(100% - 2 * " + this.inset + "px)"
    }

    private axialVerticalStyle = {
        "margin-top": this.inset + "px",
        "max-height": "calc(100% - 2 * " + this.inset + "px)"
    }

    private radialHorizontalStyle = {
        "margin-bottom": this.inset + "px",
        "margin-top": this.inset + "px"
    }

    private radialVerticalStyle = {
        "margin-left": this.inset + "px",
        "margin-right": this.inset + "px"
    }

    private axialRadialHorizontalStyleConcat = {
        "margin-left": this.inset + "px",
        "max-width": "calc(100% - 2 * " + this.inset + "px)",
        "margin-bottom": this.inset + "px",
        "margin-top": this.inset + "px"
    }

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object = {}) {
        return super.vueComponent(props)
    }

    /**
     * Divider has "axial inset" test
     */
    public testDividerHasAxialInset (): void {
        const divider = this.vueComponent({ insetAxial: this.inset })
        this.assertTrue(this.privateCall(divider, "hasAxialInset"))
    }

    /**
     * Divider has not "axial inset" test
     */
    public testDividerHasNotAxialInset (): void {
        const divider = this.vueComponent()
        this.assertFalse(this.privateCall(divider, "hasAxialInset"))
    }

    /**
     * Divider has "radial inset" test
     */
    public testDividerHasRadialInset (): void {
        const divider = this.vueComponent({ insetRadial: this.inset })
        this.assertTrue(this.privateCall(divider, "hasRadialInset"))
    }

    /**
     * Divider has not "radial inset" test
     */
    public testDividerHasNotRadialInset (): void {
        const divider = this.vueComponent()
        this.assertFalse(this.privateCall(divider, "hasRadialInset"))
    }

    /**
     * No CSS axial style add if divider has not axial inset test
     */
    public testNoAxialInsetStyle (): void {
        const divider = this.vueComponent()
        this.assertEqual(this.privateCall(divider, "axialInsetStyle"), {})
    }

    /**
     * Horizontal CSS axial style add if divider has axial inset and no vertical specified test
     */
    public testHorizontalAxialInsetStyle (): void {
        const divider = this.vueComponent({ insetAxial: this.inset })
        this.assertEqual(this.privateCall(divider, "axialInsetStyle"), this.axialHorizontalStyle)
    }

    /**
     * Vertical CSS axial style add if divider has axial inset and vertical specified test
     */
    public testVerticalAxialInsetStyle (): void {
        const divider = this.vueComponent({ insetAxial: this.inset, vertical: true })
        this.assertEqual(this.privateCall(divider, "axialInsetStyle"), this.axialVerticalStyle)
    }

    /**
     * No CSS radial style add if divider has not radial inset test
     */
    public testNoRadialInsetStyle (): void {
        const divider = this.vueComponent()
        this.assertEqual(this.privateCall(divider, "radialInsetStyle"), {})
    }

    /**
     * Horizontal CSS radial style add if divider has radial inset and no vertical specified test
     */
    public testHorizontalRadialInsetStyle (): void {
        const divider = this.vueComponent({ insetRadial: this.inset })
        this.assertEqual(this.privateCall(divider, "radialInsetStyle"), this.radialHorizontalStyle)
    }

    /**
     * Vertical CSS radial style add if divider has radial inset and vertical specified test
     */
    public testVerticalRadialInsetStyle (): void {
        const divider = this.vueComponent({ insetRadial: this.inset, vertical: true })
        this.assertEqual(this.privateCall(divider, "radialInsetStyle"), this.radialVerticalStyle)
    }

    /**
     * Axial + radial CSS style concatenation test
     */
    public testInsetStyleConcatenation (): void {
        const divider = this.vueComponent({ insetAxial: this.inset, insetRadial: this.inset })
        this.assertEqual(this.privateCall(divider, "insetStyle"), this.axialRadialHorizontalStyleConcat)
    }
}

(new OxDividerTest()).launchTests()
