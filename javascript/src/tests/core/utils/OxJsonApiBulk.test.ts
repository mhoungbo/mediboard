/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { OxJsonApiBulk } from "@/core/utils/OxJsonApiBulk"
import OxObject from "@/core/models/OxObject"
import {
    createJsonApiPostSkeleton,
    createJsonApiUpdateFieldsSkeleton,
    createJsonApiUpdateSkeleton
} from "@/core/utils/OxJsonApiTransformer"
import { OxJsonApiBulkOperation, OxJsonApiData } from "@/core/types/OxApiTypes"
import oxApiService from "@/core/utils/OxApiService"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import OxCollection from "@/core/models/OxCollection"

const bulkResponse = [
    {
        id: "bulk1",
        status: 200,
        body: {
            data: {
                type: "test_object",
                id: "444",
                attributes: {
                    title: "Test object",
                    description: "",
                    value: 3
                },
                links: {
                    self: "self",
                    schema: "schema"
                }
            }
        }
    },
    {
        id: "bulk2",
        status: 404,
        body: {
            errors: {
                type: "T3hcQ29yZVxDTWJNb2RlbE5vdEZvdW5kRXhjZXB0aW9u",
                code: 0,
                message: "Objet non trouv�"
            }
        }
    },
    {

        id: "bulk3",
        status: 200,
        body: {
            data: [
                {
                    type: "test_object",
                    id: "444",
                    attributes: {
                        title: "Test object",
                        description: "",
                        value: 3
                    },
                    links: {
                        self: "self",
                        schema: "schema"
                    }
                },
                {
                    type: "test_object",
                    id: "555",
                    attributes: {
                        title: "Test object 2",
                        description: "Test desc",
                        value: 5
                    },
                    links: {
                        self: "self",
                        schema: "schema"
                    }
                },
                {
                    type: "test_object",
                    id: "777",
                    attributes: {
                        title: "Test",
                        description: "Object",
                        value: 0,
                        active: true
                    },
                    links: {
                        self: "self",
                        schema: "schema"
                    }
                }
            ],
            meta: {
                count: 3,
                total: 5
            },
            links: {
                self: "self",
                next: "next",
                first: "first",
                last: "last"
            },
            included: [
                {
                    type: "another_object",
                    id: "3",
                    attributes: {
                        title: "Another Object",
                        active: false
                    },
                    links: {
                        picture: "picture"
                    }
                },
                {
                    type: "another_object",
                    id: "4",
                    attributes: {
                        title: "Another Object 2",
                        active: true
                    },
                    links: {
                        picture: "picture"
                    }
                },
                {
                    type: "another_object",
                    id: "5",
                    attributes: {
                        title: "Another Object 3",
                        active: false
                    },
                    links: {
                        picture: "picture"
                    }
                }
            ]
        }
    }
]

jest.spyOn(oxApiService, "post").mockImplementation(() => Promise.resolve({ data: bulkResponse }))

/* eslint-disable dot-notation */

/**
 * OxJsonApiBulk tests
 */
export default class OxJsonApiBulkTest extends OxTest {
    protected component = "OxJsonApiBulk"

    protected beforeAllTests () {
        super.beforeAllTests()
        setActivePinia(pinia)
    }

    protected afterTest () {
        super.afterTest()
        jest.clearAllMocks()
    }

    public testDeleteObjectWithoutSelf () {
        const objectToDelete = new TestObject()
        const bulk = new OxJsonApiBulk()
        expect(() => bulk.delete("delete", objectToDelete)).toThrowError("Missing self links on object")
    }

    public testDeleteObject () {
        const objectToDelete = new TestObject()
        objectToDelete.links.self = "self"
        const bulk = new OxJsonApiBulk()
        const result = bulk.delete("delete", objectToDelete)
        expect(result).toBe(bulk)
        expect(bulk.operations).toEqual([{
            id: "delete",
            method: "DELETE",
            path: "self"
        }])
        expect(bulk["_objectMapping"].delete).toEqual({
            object: objectToDelete.constructor,
            isCollection: false
        })
    }

    public testCreateObject () {
        const objectToAdd = new TestObject()
        const bulk = new OxJsonApiBulk()
        const result = bulk.create("create", objectToAdd, "url", { param: "test" })
        expect(result).toBe(bulk)
        expect(bulk.operations).toEqual([{
            id: "create",
            method: "POST",
            path: "url",
            parameters: { param: "test" },
            body: createJsonApiPostSkeleton(objectToAdd)
        }])
        expect(bulk["_objectMapping"].create).toEqual({
            object: objectToAdd.constructor,
            isCollection: false
        })
    }

    public testCreateMultipleObjects () {
        const object1 = new TestObject()
        const object2 = new TestObject()
        const object3 = new TestObject()
        const bulk = new OxJsonApiBulk()
        const result = bulk.create("create", [object1, object2, object3], "url")
        expect(result).toBe(bulk)
        expect(bulk.operations).toEqual([{
            id: "create",
            method: "POST",
            path: "url",
            parameters: undefined,
            body: createJsonApiPostSkeleton([object1, object2, object3])
        }])
        expect(bulk["_objectMapping"].create).toEqual({
            object: object1.constructor,
            isCollection: true
        })
    }

    public testUpdateNoMatchingObject () {
        const object1 = new TestObject()
        const object2 = new AnotherObject()
        const bulk = new OxJsonApiBulk()
        expect(() => bulk.update("update", object1, object2))
            .toThrowError("No matching object type between TestObject and AnotherObject")
    }

    public testUpdateObjectWithoutSelfLink () {
        const object1 = new TestObject()
        const object2 = new TestObject()
        const bulk = new OxJsonApiBulk()
        expect(() => bulk.update("update", object1, object2))
            .toThrowError("Missing self links on object")
    }

    public testUpdateObject () {
        const object1 = new TestObject()
        object1.id = "1"
        object1.title = "Title 1"
        object1.description = "Description"
        object1.active = true
        const objectModified = new TestObject()
        objectModified.id = "1"
        objectModified.title = "Title 10"
        objectModified.description = "New description"
        objectModified.active = true
        objectModified.links.self = "self"

        const bulk = new OxJsonApiBulk()
        const result = bulk.update("update", object1, objectModified, { param: "test" })
        expect(result).toBe(bulk)
        expect(bulk.operations).toEqual([{
            id: "update",
            method: "PATCH",
            path: "self",
            parameters: { param: "test" },
            body: createJsonApiUpdateSkeleton(object1, objectModified)
        }])
        expect(bulk["_objectMapping"].update).toEqual({
            object: object1.constructor,
            isCollection: false
        })
    }

    public testUpdateFieldsWithoutSelf () {
        const object1 = new TestObject()
        const bulk = new OxJsonApiBulk()
        expect(() => bulk.updateFields("update", object1, { title: "test" }))
            .toThrowError("Missing self links on object")
    }

    public testUpdateFields () {
        const object1 = new TestObject()
        object1.links.self = "self"
        const bulk = new OxJsonApiBulk()
        const result = bulk.updateFields("updateFields", object1, { title: "test" })
        expect(result).toBe(bulk)
        expect(bulk.operations).toEqual([{
            id: "updateFields",
            method: "PATCH",
            path: "self",
            parameters: undefined,
            body: createJsonApiUpdateFieldsSkeleton(object1, { title: "test" })
        }])
        expect(bulk["_objectMapping"].updateFields).toEqual({
            object: object1.constructor,
            isCollection: false
        })
    }

    public testGetObject () {
        const bulk = new OxJsonApiBulk()
        const result = bulk.getObject("object", TestObject, "url", { param: "test" })
        expect(result).toBe(bulk)
        expect(bulk.operations).toEqual([{
            id: "object",
            method: "GET",
            path: "url",
            parameters: { param: "test" }
        }])
        expect(bulk["_objectMapping"].object).toEqual({
            object: TestObject,
            isCollection: false
        })
    }

    public testGetCollection () {
        const bulk = new OxJsonApiBulk()
        const result = bulk.getCollection("collection", TestObject, "url")
        expect(result).toBe(bulk)
        expect(bulk.operations).toEqual([{
            id: "collection",
            method: "GET",
            path: "url",
            parameters: undefined
        }])
        expect(bulk["_objectMapping"].collection).toEqual({
            object: TestObject,
            isCollection: true
        })
    }

    public testStopOnFailure () {
        const bulk = new OxJsonApiBulk(false)
        const result = bulk.stopOnFailure()
        expect(result).toBe(bulk)
        expect(bulk["_stopOnFailure"]).toBe(true)
    }

    public testAddOperation () {
        const operationGet: OxJsonApiBulkOperation = {
            id: "get",
            method: "GET",
            path: "path",
            parameters: { param: "true" }
        }
        const operationPost: OxJsonApiBulkOperation = {
            id: "post",
            method: "POST",
            path: "path",
            body: {
                data: {
                    id: "id",
                    type: "test_object",
                    attributes: {
                        title: "Test"
                    }
                } as unknown as OxJsonApiData
            }
        }
        const bulk = new OxJsonApiBulk()
        bulk["addOperation"](operationGet)
        bulk["addOperation"](operationPost)
        expect(bulk.operations).toEqual([operationGet, operationPost])
    }

    public testMaxLimitOperations () {
        const bulk = new OxJsonApiBulk()
        for (let i = 0; i < bulk["OPERATION_LIMIT"]; i++) {
            bulk["addOperation"]({
                id: `id${i}`,
                method: "GET",
                path: "path"
            })
        }
        expect(() => bulk["addOperation"]({
            id: "idMax",
            method: "GET",
            path: "path"
        })).toThrowError("Max bulk operation limit reached")
    }

    public async testResolveBulk () {
        const bulk = new OxJsonApiBulk()
        bulk.getObject("bulk1", TestObject, "url")
            .getObject("bulk2", AnotherObject, "url")
            .getCollection("bulk3", TestObject, "url")
        const { bulk1, bulk2, bulk3 } = await bulk.resolve()
        expect(bulk1.body).toBeInstanceOf(TestObject)
        expect((bulk1.body as TestObject).title).toEqual("Test object")
        expect(bulk2.body).toEqual("Objet non trouv�")
        expect(bulk3.body).toBeInstanceOf(OxCollection)
        expect((bulk3.body as OxCollection<TestObject>).length).toEqual(3)
    }

    public async testResolveWithStopOnFailure () {
        const bulk = new OxJsonApiBulk(true)
        bulk.getObject("bulk1", TestObject, "url")
            .getObject("bulk2", AnotherObject, "url")
            .getCollection("bulk3", TestObject, "url")
        await bulk.resolve()
        expect(oxApiService.post).toBeCalledWith(expect.stringContaining("?stopOnFailure=true"), expect.anything())
    }

    public testAddExistingOperationKey () {
        const bulk = new OxJsonApiBulk()
        bulk.getObject("object", TestObject, "url")
        expect(() => bulk.getObject("object", TestObject, "url")).toThrowError("Key object already exists on bulk")
    }
}

class TestObject extends OxObject {
    constructor () {
        super()
        this.type = "test_object"
    }

    _relationsTypes = {
        another_object: AnotherObject
    }

    get description (): string {
        return super.get("description")
    }

    set description (value: string) {
        this.set("description", value)
    }

    get title (): string {
        return super.get("title")
    }

    set title (value: string) {
        this.set("title", value)
    }

    get value (): number {
        return super.get("value")
    }

    set value (value: number) {
        this.set("value", value)
    }

    get active (): boolean {
        return super.get("active")
    }

    set active (value: boolean) {
        this.set("active", value)
    }
}

class AnotherObject extends OxObject {
    constructor () {
        super()
        this.type = "another_object"
    }
}

(new OxJsonApiBulkTest()).launchTests()
