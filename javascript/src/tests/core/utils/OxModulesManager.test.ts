/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { isModuleActive, moduleExists } from "@/core/utils/OxModulesManager"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"
import { useNavStore } from "@/core/stores/nav"
import Module from "@/core/models/Module"

const modulesListJson = {
    modules: [
        "dmi",
        "dPpatients",
        "sample",
        "system"
    ]
}
let navStore

describe("OxModulesManager moduleExists", () => {
    beforeEach(() => {
        jest.resetAllMocks()
    })

    /**
     * Checks if core module "system" exists in the generated "javascript/src/core/utils/modulesList.json" file
     *   - "modulesList.json" is generated with Vue-CLI build
     *   - "moduleExists" function has an optional parameter "modulesListJson"
     *     => if this parameter is defined, the function uses it
     *     => if not, the function checks in the "modulesList.json" file if the module is into it
     *   - we use a core module like "system" for this test to be sure he is in every instance
     *
     *   /!\ If the "modulesList.json" file is not generated for some reason
     *         => this test crashes cause the "moduleExists" function returns "false"
     */
    test("Core module exists in json file", () => {
        expect(moduleExists("system")).toBe(true)
    })

    /**
     * Checks if core module "thismoduledoesnotexists" doesn't exists in the generated
     * "javascript/src/core/utils/modulesList.json" file
     */
    test("Core module not exists in josn file", () => {
        expect(moduleExists("thismoduledoesnotexists")).toBe(false)
    })

    /**
     * Checks if module "sample" exists in the custom Json property modulesListJson
     */
    test("Module exists in custom json", () => {
        expect(moduleExists("sample", modulesListJson)).toBe(true)
    })

    /**
     * Checks if module "thismoduledoesnotexists" doesn't exists in the custom Json property modulesListJson
     */
    test("Module not exists in custom json", () => {
        expect(moduleExists("thismoduledoesnotexists", modulesListJson)).toBe(false)
    })
})

describe("OxModulesManager isModuleActive", () => {
    beforeAll(() => {
        setActivePinia(oxPiniaCore)
        navStore = useNavStore()
        const moduleDMI = new Module("dmi")
        const modulePatients = new Module("dPpatients")
        const moduleSample = new Module("sample")
        const moduleSystem = new Module("system")
        navStore.modules = [moduleDMI, modulePatients, moduleSample, moduleSystem]
    })

    test("Module defined in navStore is active", () => {
        expect(isModuleActive("sample")).toBe(true)
    })

    test("Module not defined in navStore is inactive", () => {
        expect(isModuleActive("thismoduledoesnotexists")).toBe(false)
    })
})
