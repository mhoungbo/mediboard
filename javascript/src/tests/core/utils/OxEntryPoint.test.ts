/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { createEntryPoint } from "@/core/utils/OxEntryPoint"
import Vue from "vue"

const spyMountVue = jest.fn()
jest.mock("@/core/plugins/OxVue", () => {
    return {
        __esModule: true,
        default: (...args) => spyMountVue(args)
    }
})

// eslint-disable-next-line vue/one-component-per-file
const component1 = Vue.extend({
    name: "TestComponent1",
    props: {
    },
    data () {
        return {
            test: "Test"
        }
    },
    template: `
        <div class="TestComponent1"><span class="TestComponent1-label">Hello {{test}}!</span></div>
    `
})

// eslint-disable-next-line vue/one-component-per-file
const component2 = Vue.extend({
    name: "TestComponent2",
    props: {
    },
    data () {
        return {
            test: "Test"
        }
    },
    template: `
        <div class="TestComponent2"><span class="TestComponent2-label">Hello2 {{test}}!</span></div>
    `
})

/**
 * OxEntryPoint tests
 */
export default class OxEntryPointTest extends OxTest {
    protected component = "OxEntryPoint"

    protected afterTest () {
        super.afterTest()
        delete window.__core
        jest.clearAllMocks()
    }

    public testCreateFirstEntryPoint () {
        document.body.innerHTML = `
            <div id="testEntryPoint"></div>
        `
        createEntryPoint("testEntryPoint", component1)
        expect(spyMountVue).toHaveBeenCalledWith([component1, expect.anything(), true, "", ""])
    }

    public testCreateEntryPointWithAttributes () {
        document.body.innerHTML = `
            <div id="testEntryPoint" 
                 vue-attr1="attr1"
                 :vue-attr2="{ name: 'attr2', context: 'vueAttr'}"
                 :vue-prefs="[{ pref: 'pref1', value: true}, { pref: 'pref2', value: false}]"
                 :vue-configs="[]"
                 :vue-locales="{ locale1: 'Test1', locale2: 'Test2'}"
                 :vue-meta="{ meta1: 'Meta'}"
             > 
            </div>
        `
        createEntryPoint("testEntryPoint", component1)
        expect(spyMountVue).toHaveBeenCalledWith([
            component1,
            expect.anything(),
            true,
            ":prefs='[{ pref: 'pref1', value: true}, { pref: 'pref2', value: false}]' :configs='[]' :locales='{ locale1: 'Test1', locale2: 'Test2'}' :meta='{ meta1: 'Meta'}' ",
            "attr1='attr1' :attr2='{ name: 'attr2', context: 'vueAttr'}' "
        ])
    }

    public testCreateEntryPointWithAttributesAndUser () {
        document.body.innerHTML = `
            <div id="testEntryPoint" 
                 vue-attr1="attr1"
                 :vue-attr2="{ name: 'attr2', context: 'vueAttr'}"
                 :vue-current-user="{ id: '1', guid: 'CMediusers-1', view: 'LEBRUN John', login: 'jlebrun', type: '1' }"
                 :vue-user-function="{ id: '1', guid: 'CFunctions-1', view: 'function1' }"
                 :vue-user-group="{ id: '1', guid: 'C-1', view: 'group1' }"
                 :vue-prefs="[{ pref: 'pref1', value: true}, { pref: 'pref2', value: false}]"
                 :vue-configs="[]"
                 :vue-locales="{ locale1: 'Test1', locale2: 'Test2'}"
                 :vue-meta="{ meta1: 'Meta'}"
             > 
            </div>
        `
        createEntryPoint("testEntryPoint", component1)
        expect(spyMountVue).toHaveBeenCalledWith([
            component1,
            expect.anything(),
            true,
            ":current-user='{ id: '1', guid: 'CMediusers-1', view: 'LEBRUN John', login: 'jlebrun', type: '1' }' :user-function='{ id: '1', guid: 'CFunctions-1', view: 'function1' }' :user-group='{ id: '1', guid: 'C-1', view: 'group1' }' :prefs='[{ pref: 'pref1', value: true}, { pref: 'pref2', value: false}]' :configs='[]' :locales='{ locale1: 'Test1', locale2: 'Test2'}' :meta='{ meta1: 'Meta'}' ",
            "attr1='attr1' :attr2='{ name: 'attr2', context: 'vueAttr'}' "
        ])
    }

    public testCreateTwoEntryPointsCreateOnceRoot () {
        document.body.innerHTML = `
            <div id="testEntryPoint1"></div>
            <div id="testEntryPoint2"></div>
        `
        createEntryPoint("testEntryPoint1", component1)
        createEntryPoint("testEntryPoint2", component2)
        expect(spyMountVue).toHaveBeenNthCalledWith(
            1,
            [component1, expect.anything(), true, "", ""]
        )
        expect(spyMountVue).toHaveBeenNthCalledWith(
            2,
            [component2, expect.anything(), false, "", ""]
        )
    }

    public testCreateEntryPointWithoutElement () {
        document.body.innerHTML = `
            <div id="testEntryPoint"></div>
        `
        expect(() => createEntryPoint("nonExistantId", component1)).toThrowError(
            new Error("Element with id 'nonExistantId' not found")
        )
    }
}

(new OxEntryPointTest()).launchTests()
