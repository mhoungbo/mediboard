/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { changePage, sanitizeHtml } from "@/core/utils/OxFunctions"

/**
 * OxFunctions tests
 */
export default class OxFunctionsTest extends OxTest {
    protected component = "OxFunctions"

    public testSanitizeHtml () {
        const dom = "<div>Error &#58; <span>value</span> can not be <b>null</b>. Could be <ul><li>string</li><li>boolean</li><li>number</li></ul></div>"
        const result = sanitizeHtml(dom)
        expect(result).toEqual("Error : value can not be null. Could be stringbooleannumber")
    }

    public testChangePageByDefault () {
        const hrefSpy = jest.fn()
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            get: () => "http://localhost/",
            set: hrefSpy
        })
        changePage("url")
        expect(hrefSpy).toHaveBeenCalledWith("url")
    }

    public testChangePageInNewTab () {
        const focusSpy = jest.fn()
        const spy = jest.fn(() => {
            return {
                focus: focusSpy
            }
        })
        // @ts-ignore
        window.open = spy
        changePage("url", true)
        expect(spy).toHaveBeenCalledWith("url", "_blank")
    }
}

(new OxFunctionsTest()).launchTests()
