/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { getAutocompletePosition, getCursorNodes, setCursor, setCursorToAChild } from "@/core/utils/OxCursorControl"

// eslint-disable-next-line @typescript-eslint/no-var-requires
const OxCursorControl = require("@/core/utils/OxCursorControl")

describe(
    "OxCursorControl",
    () => {
        test("getCursorNode with selection",
            () => {
                const node = new Text("Lorem ipsum dolor set")

                // @ts-ignore
                jest.spyOn(window, "getSelection").mockReturnValue({
                    getRangeAt (): Range {
                        // @ts-ignore
                        return {
                            startOffset: 5,
                            startContainer: node,
                            endOffset: 11,
                            endContainer: node
                        }
                    }
                })
                expect(getCursorNodes()).toEqual(
                    {
                        startNode: node,
                        endNode: node,
                        startPos: 5,
                        endPos: 11
                    }
                )
            }
        )

        test("getCursorNode without selection",
            () => {
                jest.spyOn(window, "getSelection").mockReturnValue(null)
                expect(getCursorNodes()).toBeUndefined()
            }
        )

        test("calculatePositionForAutocomplete should be using top and left",
            () => {
                global.innerWidth = 1000
                global.innerHeight = 1000
                expect(OxCursorControl.calculatePositionForAutocomplete(
                    100, 150, 300, 600, 260, 16, 300, 16
                )).toEqual({
                    top: "100px",
                    left: "150px",
                    maxWidth: "818px",
                    maxHeight: "624px"
                })
            }
        )

        test("calculatePositionForAutocomplete should be using top and right",
            () => {
                global.innerWidth = 1000
                global.innerHeight = 1000
                expect(OxCursorControl.calculatePositionForAutocomplete(
                    150, 600, 200, 280, 80, 16, 100, 16
                )).toEqual({
                    top: "150px",
                    right: "280px",
                    maxWidth: "688px",
                    maxHeight: "754px"
                })
            }
        )

        test("calculatePositionForAutocomplete should be using bottom and left",
            () => {
                global.innerWidth = 1000
                global.innerHeight = 1000
                // 200 + 500 should be superior to (1000 / 5) * 3 = 600 to switch to bottom
                expect(OxCursorControl.calculatePositionForAutocomplete(
                    200, 180, 120, 600, 500, 16, 100, 16
                )).toEqual({
                    bottom: "120px",
                    left: "180px",
                    maxWidth: "788px",
                    maxHeight: "764px"
                })
            }
        )

        test("calculatePositionForAutocomplete should be using bottom and right",
            () => {
                global.innerWidth = 1000
                global.innerHeight = 1000
                // 180 + 500 should be superior to (1000 / 5) * 3 = 600 to switch to bottom
                expect(OxCursorControl.calculatePositionForAutocomplete(
                    180, 480, 80, 300, 500, 100, 100, 16
                )).toEqual({
                    bottom: "80px",
                    right: "300px",
                    maxWidth: "668px",
                    maxHeight: "804px"
                })
            }
        )

        test("setCursor with the same node for start and end",
            () => {
                const removeAllRanges = jest.fn()
                const addRange = jest.fn()
                // @ts-ignore
                jest.spyOn(window, "getSelection").mockReturnValue({
                    removeAllRanges,
                    addRange
                })
                const node = new Text("Lorem ipsum dolor set")

                setCursor(node, 5, node, 8)

                const range = addRange.mock.calls[0][0]
                expect(range).toBeDefined()
                expect(range).toHaveProperty("startContainer", node)
                expect(range).toHaveProperty("endContainer", node)
                expect(range).toHaveProperty("startOffset", 5)
                expect(range).toHaveProperty("endOffset", 8)
            }
        )

        test("setCursorToAChild should set cursor start and end at the same position",
            () => {
                const removeAllRanges = jest.fn()
                const addRange = jest.fn()
                // @ts-ignore
                jest.spyOn(window, "getSelection").mockReturnValue({
                    removeAllRanges,
                    addRange
                })
                const node = new Text("Lorem ipsum dolor set")

                setCursorToAChild(node, 5)

                const range = addRange.mock.calls[0][0]
                expect(range).toBeDefined()
                expect(range).toHaveProperty("startContainer", node)
                expect(range).toHaveProperty("endContainer", node)
                expect(range).toHaveProperty("startOffset", 5)
                expect(range).toHaveProperty("endOffset", 5)
            }
        )

        test("calculateParentsOffset without any offsets",
            () => {
                const element = document.createElement("div")

                const result = OxCursorControl.calculateParentsOffset(element)

                expect(result).toEqual({ offsetTop: 0, offsetLeft: 0 })
            }
        )

        test("calculateParentsOffset with 4 different element and only one have offsets",
            () => {
                const element = document.createElement("div")
                const parent1 = document.createElement("div")
                const parent2 = document.createElement("div")
                const parent3 = document.createElement("div")
                Object.defineProperty(parent3, "offsetTop", { value: 10 })
                Object.defineProperty(parent3, "offsetLeft", { value: 20 })

                Object.defineProperty(parent2, "offsetParent", { value: parent3 })
                Object.defineProperty(parent1, "offsetParent", { value: parent2 })
                Object.defineProperty(element, "offsetParent", { value: parent1 })

                const result = OxCursorControl.calculateParentsOffset(element)

                expect(result).toEqual({ offsetTop: 10, offsetLeft: 20 })
            }
        )

        test("calculateParentsOffset with 5 different element and all of them have offsets",
            () => {
                const element = document.createElement("div")
                const parent1 = document.createElement("div")
                const parent2 = document.createElement("div")
                const parent3 = document.createElement("div")
                const parent4 = document.createElement("div")

                Object.defineProperty(parent1, "offsetTop", { value: 10 })
                Object.defineProperty(parent1, "offsetLeft", { value: 20 })

                Object.defineProperty(parent2, "offsetTop", { value: 5 })
                Object.defineProperty(parent2, "offsetLeft", { value: 30 })
                Object.defineProperty(parent2, "offsetParent", { value: parent1 })

                Object.defineProperty(parent3, "offsetTop", { value: 8 })
                Object.defineProperty(parent3, "offsetLeft", { value: 15 })
                Object.defineProperty(parent3, "offsetParent", { value: parent2 })

                Object.defineProperty(parent4, "offsetTop", { value: 3 })
                Object.defineProperty(parent4, "offsetLeft", { value: 7 })
                Object.defineProperty(parent4, "offsetParent", { value: parent3 })

                Object.defineProperty(element, "offsetTop", { value: 6 })
                Object.defineProperty(element, "offsetLeft", { value: 24 })
                Object.defineProperty(element, "offsetParent", { value: parent4 })

                const result = OxCursorControl.calculateParentsOffset(element)

                expect(result).toEqual({ offsetTop: 32, offsetLeft: 96 })
            }
        )

        test("getAutocompletePosition should return undefined if there is no selection",
            () => {
            // Arrange
                const word = "example"
                const element = document.createElement("div")
                window.getSelection = jest.fn(() => null)

                // Act
                const result = getAutocompletePosition(word, element)

                // Assert
                expect(result).toBeUndefined()
            }
        )

        test("getAutocompletePosition should return undefined if the selection is outside the container",
            () => {
            // Arrange
                const word = "example"
                const element = document.createElement("div")
                const selection = {
                    rangeCount: 1,
                    getRangeAt: jest.fn(() => {
                        const range = document.createRange()
                        const outsideContainerNode = document.createElement("span")
                        range.setStart(outsideContainerNode, 0)
                        range.setEnd(outsideContainerNode, 0)
                        return range
                    })
                }
                jest.spyOn(window, "getSelection").mockReturnValue(selection as unknown as Selection)

                const result = getAutocompletePosition(word, element)

                expect(result).toBeUndefined()
            }
        )
    }
)
