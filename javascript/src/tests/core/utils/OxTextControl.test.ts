/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createNodeFromTextWithBrTags } from "@/core/utils/OxTextControl"

describe(
    "OxTextControl",
    () => {
        test.each([
            {
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                expectedNode: "<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>"
            },
            {
                text: "Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit.",
                expectedNode: "<span>Lorem ipsum dolor sit amet,<br>consectetur adipiscing elit.</span>"
            }
        ])("createNodeFromTextWithBrTags with '$text' sould return '$expectedNode'", ({ text, expectedNode }) => {
            expect(createNodeFromTextWithBrTags(text).outerHTML).toEqual(expectedNode)
        })
    }
)
