/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { convertHTMLToMarkdown, convertMarkdownToHTML } from "@/core/utils/OxMarkdown"

describe(
    "OxMarkdown",
    () => {
        test.each([
            {
                markdownString: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit.",
                htmlString: "<p>Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem **ipsum** dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem <strong>ipsum</strong> dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem *ipsum* dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem <em>ipsum</em> dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem ~~ipsum~~ dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem <s>ipsum</s> dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem `ipsum` dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem <code>ipsum</code> dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem ipsum dolor sit amet\r\n```\r\nconsectetur\r\nadipiscing\r\nelit\r\n```",
                htmlString: "<p>Lorem ipsum dolor sit amet</p>\n<pre><code>consectetur\nadipiscing\nelit\n</code></pre>\n"
            },
            {
                markdownString: "Lorem [ipsum](https://example.com/) dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem <a href=\"https://example.com/\">ipsum</a> dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem [ipsum](https://example.com/ \"example\") dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem <a href=\"https://example.com/\" title=\"example\">ipsum</a> dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem [ipsum](javascript:alert('ipsum')) dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem [ipsum](javascript:alert('ipsum')) dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem ![ipsum](https://example.com/example.png) dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem <img src=\"https://example.com/example.png\" alt=\"ipsum\"> dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem ![ipsum](https://example.com/example.png \"example\") dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem <img src=\"https://example.com/example.png\" alt=\"ipsum\" title=\"example\"> dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem ![ipsum](https://example.com/example.png\" onload=\"javascript:alert('ipsum')\") dolor sit amet, consectetur adipiscing elit.",
                htmlString: "<p>Lorem ![ipsum](https://example.com/example.png&quot; onload=&quot;javascript:alert('ipsum')&quot;) dolor sit amet, consectetur adipiscing elit.</p>\n"
            },
            {
                markdownString: "Lorem ipsum dolor sit amet\r\n- consectetur\r\n- adipiscing\r\n- elit",
                htmlString: "<p>Lorem ipsum dolor sit amet</p>\n<ul>\n<li>consectetur</li>\n<li>adipiscing</li>\n<li>elit</li>\n</ul>\n"
            },
            {
                markdownString: "Lorem ipsum dolor sit amet\r\n1. consectetur\r\n2. adipiscing\r\n3. elit",
                htmlString: "<p>Lorem ipsum dolor sit amet</p>\n<ol>\n<li>consectetur</li>\n<li>adipiscing</li>\n<li>elit</li>\n</ol>\n"
            }
        ])("convertMarkdownToHTML with '$markdownString' sould return '$htmlString'", ({ markdownString, htmlString }) => {
            expect(convertMarkdownToHTML(markdownString)).toEqual(htmlString)
        })

        test.each([
            {
                htmlString: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem ipsum dolor sit amet, <br>consectetur adipiscing elit.</p>",
                markdownString: "Lorem ipsum dolor sit amet,  \nconsectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <strong>ipsum</strong> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem **ipsum** dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <em>ipsum</em> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem _ipsum_ dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <s>ipsum</s> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem ~~ipsum~~ dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <code>ipsum</code> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem `ipsum` dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem ipsum dolor sit amet</p><pre><code>consectetur\nadipiscing\nelit\n</code></pre>",
                markdownString: "Lorem ipsum dolor sit amet\n\n```\nconsectetur\nadipiscing\nelit\n```"
            },
            {
                htmlString: "<p>Lorem <a href=\"https://example.com/\">ipsum</a> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem [ipsum](https://example.com/) dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <a href=\"https://example.com/\" title=\"example\">ipsum</a> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem [ipsum](https://example.com/ \"example\") dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <a href=\"javascript:alert('ipsum')\">ipsum</a> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem [ipsum](javascript:alert('ipsum')) dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <img src=\"https://example.com/example.png\" alt=\"ipsum\" /> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem ![ipsum](https://example.com/example.png) dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <img src=\"https://example.com/example.png\" alt=\"ipsum\" title=\"example\" /> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem ![ipsum](https://example.com/example.png \"example\") dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem <img src=\"https://example.com/example.png\" alt=\"ipsum\" onload=\"alert('ipsum')\" /> dolor sit amet, consectetur adipiscing elit.</p>",
                markdownString: "Lorem ![ipsum](https://example.com/example.png) dolor sit amet, consectetur adipiscing elit."
            },
            {
                htmlString: "<p>Lorem ipsum dolor sit amet</p><ul><li>consectetur</li><li>adipiscing</li><li>elit</li></ul>",
                markdownString: "Lorem ipsum dolor sit amet\n\n-   consectetur\n-   adipiscing\n-   elit"
            },
            {
                htmlString: "<p>Lorem ipsum dolor sit amet</p><ol><li>consectetur</li><li>adipiscing</li><li>elit</li></ol>",
                markdownString: "Lorem ipsum dolor sit amet\n\n1.  consectetur\n2.  adipiscing\n3.  elit"
            }
        ])("convertHTMLToMarkdown with '$htmlString' sould return '$markdownString'", ({ htmlString, markdownString }) => {
            expect(convertHTMLToMarkdown(htmlString)).toEqual(markdownString)
        })
    }
)
