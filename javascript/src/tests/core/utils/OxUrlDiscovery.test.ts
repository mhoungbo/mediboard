/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"

/* eslint-disable dot-notation */

/**
 * OxUrlDiscovery tests
 */
export default class OxUrlDiscoveryTest extends OxTest {
    protected component = "OxUrlDiscovery"

    public testLoginPageLink () {
        expect(OxUrlDiscovery.loginPage()).toEqual(OxUrlDiscovery["LOGIN_PAGE_LINK"])
    }

    public testLogoutLink () {
        expect(OxUrlDiscovery.logout()).toEqual(OxUrlDiscovery["LOGOUT_LINK"])
    }

    public testStatusLink () {
        expect(OxUrlDiscovery.status()).toEqual(OxUrlDiscovery["STATUS_LINK"])
    }

    public testOpenApiLink () {
        expect(OxUrlDiscovery.openapi()).toEqual(OxUrlDiscovery["OPENAPI_LINK"])
    }

    public testModuleConfigurationsLink () {
        expect(OxUrlDiscovery.moduleConfigurations("module"))
            .toEqual(`${OxUrlDiscovery["CONFIGURATIONS_LINK"]}/module`)
    }

    public testPreferencesLink () {
        expect(OxUrlDiscovery.preferences("module"))
            .toEqual(`${OxUrlDiscovery["PREFERENCES_LINK"]}/module`)
    }

    public testUserPreferencesLink () {
        expect(OxUrlDiscovery.userPreferences("module", "userId"))
            .toEqual(`${OxUrlDiscovery["PREFERENCES_LINK"]}/module/userId`)
    }

    public testDefaultLocalesLink () {
        expect(OxUrlDiscovery.locales("module"))
            .toEqual(`${OxUrlDiscovery["LOCALES_LINK"]}/core/module`)
    }

    public testLanguageLocalesLink () {
        expect(OxUrlDiscovery.locales("module", "fr"))
            .toEqual(`${OxUrlDiscovery["LOCALES_LINK"]}/fr/module`)
    }

    public testBulkLink () {
        expect(OxUrlDiscovery.bulk()).toEqual(OxUrlDiscovery["BULK_LINK"])
    }

    public testSchemaLink () {
        expect(OxUrlDiscovery.schema()).toEqual(OxUrlDiscovery["SCHEMA_LINK"])
    }

    public testSchemaLinkWithResource () {
        expect(OxUrlDiscovery.schema("resource_test")).toEqual(OxUrlDiscovery["SCHEMA_LINK"] + "/resource_test")
    }

    public testCreateHelpedText () {
        expect(OxUrlDiscovery.createHelpedText()).toEqual(OxUrlDiscovery["HELPED_TEXTS_LINK"])
    }

    public testListHelpedTexts () {
        expect(OxUrlDiscovery.listHelpedTexts("resource_test", "field_test"))
            .toEqual(OxUrlDiscovery["HELPED_TEXTS_LINK"] + "/resource_test/field_test")
    }

    public testEditHelpedTexts () {
        expect(OxUrlDiscovery.editHelpedTexts()).toEqual(OxUrlDiscovery["HELPED_TEXTS_EDIT_LINK"])
    }

    public testIdexLink () {
        expect(OxUrlDiscovery.idex()).toEqual(OxUrlDiscovery["IDEX_LINK"])
    }

    public testUsersLink () {
        expect(OxUrlDiscovery.listUsers()).toEqual(OxUrlDiscovery["USERS_LINK"])
    }

    public testMedicalCodesLink () {
        expect(OxUrlDiscovery.listMedicalCodes()).toEqual(OxUrlDiscovery["MEDICAL_CODES_LINK"])
    }

    public testPatientsLink () {
        expect(OxUrlDiscovery.listPatients()).toEqual(OxUrlDiscovery["PATIENTS_LINK"])
    }
}

(new OxUrlDiscoveryTest()).launchTests()
