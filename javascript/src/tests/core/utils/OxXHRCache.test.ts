/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import {
    deleteOldCaches, getCachedData,
    getCacheName, getCurrentTimestamp,
    getDataFromCache,
    openCache, setDataToCache, updateCacheItemTimestamp
} from "@/core/utils/OxXHRCache"
import oxApiService from "@/core/utils/OxApiService"
import * as OxApiManager from "@/core/utils/OxApiManager"

const spyDelete = jest.fn()
const spyOpen = jest.fn()
const spyPut = jest.fn()

// @ts-ignore, Mock Response object because jest doesn't implement it
window.Response = class {
    private data: Object
    constructor (data: string) {
        this.data = JSON.parse(data)
    }
}

// @ts-ignore, Mock CacheStorage API object because jest doesn't implement it
window.caches = {
    // @ts-ignore
    cachesList: [],
    // @ts-ignore
    open (cacheName) {
        spyOpen(cacheName)
        // @ts-ignore
        this.cachesList.push(cacheName)
        return {
            cacheItems: [],
            keys () {
                // @ts-ignore
                return this.cacheItems
            },
            match (url) {
                // @ts-ignore
                if (url === "cachedUrlResource") {
                    return {
                        ok: true,
                        json () {
                            return {
                                data: "JSON Response",
                                etag: "resourceEtag",
                                timestamp: (Date.now() / 1000) - 60
                            }
                        }
                    }
                }
                else {
                    return { ok: false }
                }
            },
            put (url, response) {
                spyPut(url, response)
            }
        }
    },
    keys (): Promise<string[]> {
        // @ts-ignore
        return this.cachesList
    },
    // @ts-ignore
    delete (cacheName: string) {
        spyDelete(cacheName)
        // @ts-ignore
        this.cachesList = this.cachesList.filter(cache => cache !== cacheName)
    }
}

const mockList = {
    data: [
        {
            type: "test_object",
            id: "3",
            attributes: {
                field_str: "Test 3",
                field_date: "01/01/1980",
                field_time: "01:00:00"
            }
        },
        {
            type: "test_object",
            id: "4",
            attributes: {
                field_str: "Test 4",
                field_date: "01/01/1990",
                field_time: "01:30:00"
            }
        }
    ],
    meta: {
        date: "2022-09-12 11:07:50+02:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 2,
        total: 4
    },
    links: {
        self: "http://localhost/mediboard/api/sample/movies?limit=2&offset=0",
        first: "http://localhost/mediboard/api/sample/movies?limit=2&offset=0",
        last: "http://localhost/mediboard/api/sample/movies?limit=2&offset=2"
    }
}
jest.spyOn(oxApiService, "get").mockImplementation(() => Promise.resolve({ data: mockList, headers: { etag: "etagList" } }))

/**
 * OxXHRCache tests
 */
export default class OxXHRCacheTest extends OxTest {
    protected component = "OxXHRCache"

    protected beforeTest () {
        super.beforeTest()
        // @ts-ignore
        window.caches.cachesList = []
    }

    protected afterTest () {
        super.afterTest()
        // @ts-ignore
        jest.clearAllMocks()
    }

    public testGetCacheName () {
        const session = "sessionHash"
        expect(getCacheName(session)).toEqual("XHR-CACHE-sessionHash-1")
    }

    public async testOpenCache () {
        await openCache("session")
        expect(spyOpen).toHaveBeenCalledWith(getCacheName("session"))
        expect(await caches.keys()).toEqual([getCacheName("session")])
    }

    public async testOpenCacheForFirstTimeDeleteOldCaches () {
        await openCache("session1")
        await openCache("session2")
        expect(await caches.keys()).toEqual([getCacheName("session2")])
        expect(spyDelete).toHaveBeenCalledWith(getCacheName("session1"))
    }

    public async testDeleteOnlyXHRCache () {
        // @ts-ignore
        window.caches.cachesList = ["XHR-FOREIGN-CACHE-1", "XHR-FOREIGN-CACHE-2", "XHR-FOREIGN-CACHE-3"]
        await deleteOldCaches("")
        expect(spyDelete).not.toHaveBeenCalled()
        expect(await caches.keys()).toEqual(["XHR-FOREIGN-CACHE-1", "XHR-FOREIGN-CACHE-2", "XHR-FOREIGN-CACHE-3"])
    }

    public async testGetDataFromCache () {
        const cachedData = await getDataFromCache("sessionTest", "cachedUrlResource")
        expect(cachedData?.data).toEqual("JSON Response")
    }

    public async testGetNonCachedDataFromCache () {
        const cachedData = await getDataFromCache("sessionTest", "nonCachedUrlResource")
        expect(cachedData).toBeUndefined()
    }

    public async testUpdateCacheItemTimestamp () {
        await updateCacheItemTimestamp("sessionTest", "urlTest", { timestamp: getCurrentTimestamp(), data: { data: [], links: {}, included: [] }, etag: "etag" })
        expect(spyPut).toHaveBeenCalledWith("urlTest", { data: { data: { data: [], included: [], links: {} }, etag: "etag", timestamp: expect.anything() } })
    }

    public async testSetDataToCache () {
        // @ts-ignore
        await setDataToCache("sessionTest", "urlTest", { data: { data: [], links: {}, included: [] }, status: 200, config: {}, headers: { etag: "etag2" }, request: {}, statusText: "OK" })
        expect(spyPut).toHaveBeenCalledWith("urlTest", { data: { data: { data: [], included: [], links: {} }, etag: "etag2", timestamp: expect.anything() } })
    }

    public async testGetNonCachedData () {
        const result = await getCachedData("sessionTest", "urlGet", 30, false)
        await this.flushPromises()
        expect(oxApiService.get).toHaveBeenCalledWith("urlGet")
        expect(spyOpen).toHaveBeenNthCalledWith(1, getCacheName("sessionTest"))
        expect(spyOpen).toHaveBeenNthCalledWith(2, getCacheName("sessionTest"))
        expect(spyPut).toHaveBeenCalledWith("urlGet", expect.anything())
        expect(result).toEqual(mockList)
    }

    public async testGetUpToDateCachedData () {
        const result = await getCachedData("sessionTest", "cachedUrlResource", 120, false)
        await this.flushPromises()
        expect(result).toEqual("JSON Response")
    }

    public async testGetOutOfDateCachedDataWithUpToDateEtag () {
        // Mock return for etag check, etag valid
        jest.spyOn(OxApiManager, "checkChanges").mockImplementation(() => Promise.resolve(false))
        const result = await getCachedData("sessionTest", "cachedUrlResource", 30, true)
        await this.flushPromises()
        expect(spyPut).toHaveBeenCalledWith("cachedUrlResource", expect.anything())
        expect(result).toEqual("JSON Response")
    }

    public async testGetOutOfDateCachedDataWithOutOfDateEtag () {
        // @ts-ignore, Mock return for etag check, etag out of date
        jest.spyOn(OxApiManager, "checkChanges").mockImplementation(() => Promise.resolve({ data: "Updated object", headers: { etag: "etagChanged" } }))
        const result = await getCachedData("sessionTest", "cachedUrlResource", 30, true)
        await this.flushPromises()
        expect(spyPut).toHaveBeenCalledWith("cachedUrlResource", expect.anything())
        expect(result).toEqual("Updated object")
    }
}

(new OxXHRCacheTest()).launchTests()
