/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { configs, injectConfigs, injectPrefs, keyConfigs, keyPrefs, prefs } from "@/core/utils/OxInjector"

/**
 * OxInjector tests
 */
export default class OxInjectorTest extends OxTest {
    protected component = "OxInjector"

    public testConfigsInjection () {
        expect(injectConfigs()).toEqual({
            [configs]: {
                from: keyConfigs,
                default: {}
            }
        })
    }

    public testPreferencesInjection () {
        expect(injectPrefs()).toEqual({
            [prefs]: {
                from: keyPrefs,
                default: {}
            }
        })
    }
}

(new OxInjectorTest()).launchTests()
