/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import { OxAppContext } from "@/core/utils/OxAppContext"

/**
 * OxAppContext tests
 */
export default class OxAppContextTest extends OxTest {
    protected component = "OxAppContext"

    protected afterTest () {
        super.afterTest()
        delete window.__core
    }

    public testFirstStart () {
        OxAppContext.start()
        expect(window.__core).toEqual({
            mounted: false
        })
    }

    public testSecondStart () {
        OxAppContext.start()
        OxAppContext.mounted = true
        OxAppContext.start()
        expect(window.__core).toEqual({
            mounted: true
        })
    }

    public testMounted () {
        OxAppContext.start()
        expect(OxAppContext.mounted).toBe(false)
    }

    public testGetEnv () {
        expect(OxAppContext.env).toBe("test")
    }
}

(new OxAppContextTest()).launchTests()
