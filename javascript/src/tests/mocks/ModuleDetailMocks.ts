import Tab from "@modules/system/vue/models/Tab"
import Module from "@/core/models/Module"

const tab1 = new Tab()
tab1.moduleName = "ModuleTest"
tab1.name = "Tab1"
tab1.isStandard = true
tab1.isParam = false
tab1.isConfig = false
tab1.pinnedOrder = 0
tab1.url = "url-1"

const tab2 = new Tab()
tab2.moduleName = "ModuleTest"
tab2.name = "Tab2"
tab2.isStandard = true
tab2.isParam = false
tab2.isConfig = false
tab2.pinnedOrder = null
tab2.url = "url-2"

const tab3 = new Tab()
tab3.moduleName = "ModuleTest"
tab3.name = "Tab3"
tab3.isStandard = true
tab3.isParam = false
tab3.isConfig = false
tab3.pinnedOrder = null
tab3.url = "url-3"

const tab4 = new Tab()
tab4.moduleName = "ModuleTest"
tab4.name = "Tab4"
tab4.isStandard = false
tab4.isParam = true
tab4.isConfig = false
tab4.pinnedOrder = null
tab4.url = "url-4"

const tab5 = new Tab()
tab5.moduleName = "ModuleTest"
tab5.name = "Tab5"
tab5.isStandard = false
tab5.isParam = false
tab5.isConfig = true
tab5.pinnedOrder = null
tab5.url = "url-5"

const currentModule = new Module()
currentModule.name = "ModuleTest"
currentModule.addTabs([tab1, tab2, tab3, tab4, tab5])

export { currentModule }
