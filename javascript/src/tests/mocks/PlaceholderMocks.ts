import { Placeholder } from "@/components/Appbar/types/AppbarTypes"

export const placeholderWithAction: Placeholder = {
    _id: "1",
    label: "Label",
    icon: "time",
    disabled: false,
    action: "action",
    action_args: [],
    init_action: "",
    counter: ""
}

export const placeholderWithCounter: Placeholder = {
    _id: "11",
    label: "Counter",
    icon: "count",
    disabled: false,
    action: "",
    action_args: [],
    init_action: "",
    counter: "22"
}

export const placeholderWithInitAction: Placeholder = {
    _id: "2",
    label: "ActionArgs",
    icon: "accountGroup",
    disabled: false,
    action: "actionArgs",
    action_args: ["arg1", "arg2"],
    init_action: "testFunc",
    counter: ""
}

export const appFinePlaceholder: Placeholder = {
    _id: "3",
    label: "Appfine",
    icon: "appfine",
    disabled: false,
    action: "",
    action_args: [],
    init_action: "",
    counter: "0"
}

export const ecapPlaceholder: Placeholder = {
    _id: "4",
    label: "Ecap",
    icon: "ecap",
    disabled: false,
    action: "",
    action_args: [],
    init_action: "",
    counter: ""
}

export const placeholderWithoutAction: Placeholder = {
    _id: "5",
    label: "NoAction",
    icon: "icon",
    disabled: false,
    action: "",
    action_args: [],
    init_action: "",
    counter: ""
}
