import OxHelpedText from "@modules/dPcompteRendu/vue/models/OxHelpedText"
import { OxJsonApi } from "@/core/types/OxApiTypes"
import MedicalCode from "@modules/system/vue/models/MedicalCode"

export const OxHelpedTextApiMock = {
    data: [
        {
            type: "helped_text",
            id: "1",
            attributes: {
                name: "Lorem",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                class: "sample_movie",
                field: "description"
            },
            relationships: {
                owner: {
                    data: {
                        type: "mediuser",
                        id: "1"
                    }
                }
            },
            links: {
                schema: "/api/schemas/helped_text?fieldsets=default,target",
                history: "/api/history/helped_text/1"
            }
        },
        {
            type: "helped_text",
            id: "2",
            attributes: {
                name: "Lorem BR",
                text: "Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit.",
                class: "sample_movie",
                field: "description"
            },
            relationships: {
                owner: {
                    data: {
                        type: "function",
                        id: "1"
                    }
                }
            },
            links: {
                schema: "/api/schemas/helped_text?fieldsets=default,target",
                history: "/api/history/helped_text/2"
            }
        },
        {
            type: "helped_text",
            id: "3",
            attributes: {
                name: "Promeneur",
                text: "Le promeneur, berc� par le chant des oiseleurs.",
                class: "sample_movie",
                field: "description"
            },
            relationships: {
                owner: {
                    data: {
                        type: "group",
                        id: "1"
                    }
                }
            },
            links: {
                schema: "/api/schemas/helped_text?fieldsets=default,target",
                history: "/api/history/helped_text/3"
            }
        },
        {
            type: "helped_text",
            id: "4",
            attributes: {
                name: "Soleil",
                text: "Le soleil d�cline � l'horizon charmeur.",
                class: "sample_movie",
                field: "description"
            },
            relationships: {
                owner: {
                    data: null
                }
            },
            links: {
                schema: "/api/schemas/helped_text?fieldsets=default,target",
                history: "/api/history/helped_text/4"
            }
        }
    ],
    meta: {
        date: "2023-03-21 14:15:16+01:00",
        copyright: "OpenXtrem-2023",
        authors: "dev@openxtrem.com",
        count: 4,
        total: 4,
        words_tokens: {
            lorem: [1, 2],
            ipsum: [1, 2],
            dolor: [1, 2],
            sit: [1, 2],
            amet: [1, 2],
            consectetur: [1, 2],
            adipiscing: [1, 2],
            elit: [1, 2],
            br: [2],
            promeneur: [3],
            berce: [3],
            par: [3],
            chant: [3],
            oiseleurs: [3],
            soleil: [4],
            decline: [4],
            horizon: [4],
            charmeur: [4]
        }
    },
    links: {
        self: "http://localhost/api/models/helped_texts/sample_movie/description?fieldsets=default,target&limit=50&offset=0&relations=owner&with_words_tokens=1",
        first: "http://localhost/api/models/helped_texts/sample_movie/description?fieldsets=default,target&limit=50&offset=0&relations=owner&with_words_tokens=1",
        last: "http://localhost/api/models/helped_texts/sample_movie/description?fieldsets=default,target&limit=50&offset=0&relations=owner&with_words_tokens=1"
    },
    included: [
        {
            type: "mediuser",
            id: "1",
            attributes: {
                initials: "VS",
                color: "9900ff",
                _color: "9900ff",
                actif: true,
                deb_activite: null,
                fin_activite: null,
                _user_first_name: "John",
                _user_last_name: "LEBRUN",
                _user_sexe: "m"
            },
            relationships: {}
        },
        {
            type: "function",
            id: "1",
            attributes: {
                text: "Praticien"
            },
            relationships: {}
        },
        {
            type: "group",
            id: "1",
            attributes: {
                text: "Etablissement",
                raison_sociale: null
            },
            relationships: {}
        }
    ]
} as OxJsonApi

export const ModelsConfigurationsApiMock = {
    data: {
        type: "configurations",
        id: "1418268e6f7808fb42e0f29c9d036c60",
        attributes: {
            instance: {
                "dPcompteRendu CCompteRendu timestamp": "-- %n %p - dd/MM/y HH:mm",
                "dPcompteRendu CCompteRendu default_font": "Georgia",
                "dPcompteRendu CCompteRendu default_fonts": "Arial/Arial, Helvetica, sans-serif;Carlito/Carlito, Helvetica, sans-serif;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Symbol/Symbol;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif;ZapfDingBats/ZapfDingBats;",
                "dPcompteRendu CCompteRendu font_dir": ""
            },
            static: [],
            context: {
                CGroups: {
                    1: {
                        "dPcompteRendu CCompteRenduPrint same_print": "1",
                        "dPcompteRendu CCompteRenduPrint time_before_thumbs": "0",
                        "dPcompteRendu CCompteRendu default_size": "small",
                        "dPcompteRendu CCompteRendu header_footer_fly": "0",
                        "dPcompteRendu CCompteRendu dompdf_host": "0",
                        "dPcompteRendu CCompteRendu unlock_doc": "1",
                        "dPcompteRendu CCompteRendu shrink_pdf": "0",
                        "dPcompteRendu CCompteRendu purge_lifetime": "1000",
                        "dPcompteRendu CCompteRendu purge_limit": "100",
                        "dPcompteRendu CCompteRendu private_owner_func": "function",
                        "dPcompteRendu CCompteRendu probability_regenerate": "100",
                        "dPcompteRendu CCompteRendu days_to_lock": "30",
                        "dPcompteRendu CCompteRendu cancel_and_replace": "",
                        "dPcompteRendu CCompteRenduAcces access_group": "1",
                        "dPcompteRendu CCompteRenduAcces access_function": "1",
                        "dPcompteRendu CAideSaisie access_group": "0",
                        "dPcompteRendu CAideSaisie access_function": "1",
                        "dPcompteRendu CListeChoix access_group": "1",
                        "dPcompteRendu CListeChoix access_function": "1"
                    }
                }
            }
        },
        relationships: {}
    },
    meta: {
        date: "2023-03-27 16:56:05+02:00",
        copyright: "OpenXtrem-2023",
        authors: "dev@openxtrem.com"
    },
    included: [],
    links: {}
} as OxJsonApi

export const ModelsPreferencesApiMock = {
    data: {
        type: "preference",
        id: "614649be2a473e88580c7785fe4aef59",
        attributes: {
            pdf_and_thumbs: "1",
            saveOnPrint: "1",
            choicepratcab: "prat",
            listDefault: "ulli",
            listBrPrefix: "&bull;",
            listInlineSeparator: ";",
            aideTimestamp: "1",
            aideOwner: "1",
            aideFastMode: "1",
            aideAutoComplete: "1",
            aideShowOver: "1",
            mode_play: "0",
            multiple_docs: "0",
            auto_capitalize: "0",
            auto_replacehelper: "0",
            hprim_med_header: "0",
            show_old_print: "0",
            send_document_subject: "Mediboard - Envoi de document",
            send_document_body: "Ce document vous a �t� envoy� via l'application Mediboard.",
            multiple_doc_correspondants: "0",
            show_creation_date: "0",
            secure_signature: "0",
            check_to_empty_field: "1",
            time_autosave: "0",
            show_favorites: "0"
        }
    },
    meta: {
        date: "2023-04-05 12:06:21+02:00",
        copyright: "OpenXtrem-2023",
        authors: "dev@openxtrem.com"
    }
}

export const OxHelpedTextListMock = [] as OxHelpedText[]

export const sampleMovieDescriptionSchemaMockMarkdown = {
    data: [
        {
            type: "schema",
            id: "847f7aa830e06013fa44298843f20aae",
            attributes: {
                owner: "sample_movie",
                field: "description",
                type: "text",
                fieldset: "details",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                helped: true,
                markdown: true,
                libelle: "Synopsis",
                label: "Synopsis",
                description: "Synopsis du film"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 6
    }
}

export const sampleMovieDescriptionSchemaMockNotMarkdown = {
    data: [
        {
            type: "schema",
            id: "847f7aa830e06013fa44298843f20aae",
            attributes: {
                owner: "sample_movie",
                field: "description",
                type: "text",
                fieldset: "details",
                autocomplete: null,
                placeholder: null,
                notNull: null,
                confidential: null,
                default: null,
                helped: true,
                markdown: false,
                libelle: "Synopsis",
                label: "Synopsis",
                description: "Synopsis du film"
            }
        }
    ],
    meta: {
        date: "2023-02-13 15:45:11+01:00",
        copyright: "OpenXtrem-2022",
        authors: "dev@openxtrem.com",
        count: 6
    }
}

export const MedicalCodes = [] as MedicalCode[]

function createMedicalCodes () {
    const ccam1 = new MedicalCode()
    ccam1.id = "0"
    ccam1.codeType = "ccam"
    ccam1.code = "BAAA001"
    ccam1.description = "Allongement du muscle releveur de la paupi�re sup�rieure et/ou section de sa lame profonde [muscle de M�ller], avec interposition de mat�riau inerte ou autogreffe"

    const ccam2 = new MedicalCode()
    ccam2.id = "1"
    ccam2.codeType = "ccam"
    ccam2.code = "BAAA001"
    ccam2.description = "Allongement du muscle releveur de la paupi�re sup�rieure et/ou section de sa lame profonde [muscle de M�ller], avec interposition de mat�riau inerte ou autogreffe"

    const ccam3 = new MedicalCode()
    ccam3.id = "2"
    ccam3.codeType = "ccam"
    ccam3.code = "BAAA001"
    ccam3.description = "Allongement du muscle releveur de la paupi�re sup�rieure et/ou section de sa lame profonde [muscle de M�ller], avec interposition de mat�riau inerte ou autogreffe"

    const ccam4 = new MedicalCode()
    ccam4.id = "3"
    ccam4.codeType = "ccam"
    ccam4.code = "BAAA001"
    ccam4.description = "Allongement du muscle releveur de la paupi�re sup�rieure et/ou section de sa lame profonde [muscle de M�ller], avec interposition de mat�riau inerte ou autogreffe"

    const cim10 = new MedicalCode()
    cim10.id = "4"
    cim10.codeType = "cim10"
    cim10.code = "A00.0"
    cim10.description = "Chol�ra classique"

    const snomed1 = new MedicalCode()
    snomed1.id = "5"
    snomed1.codeType = "snomed"
    snomed1.code = "DE-35900"
    snomed1.description = "Affection coronavirus"

    const snomed2 = new MedicalCode()
    snomed2.id = "6"
    snomed2.codeType = "snomed"
    snomed2.code = "DE-35900"
    snomed2.description = "Affection coronavirus"

    const loinc = new MedicalCode()
    loinc.id = "7"
    loinc.codeType = "loinc"
    loinc.code = "94504-8"
    loinc.description = "S�rologie Coronavirus SARS-CoV-2"

    const ngap = new MedicalCode()
    ngap.id = "8"
    ngap.codeType = "ngap"
    ngap.code = "COE"
    ngap.description = "Consultation obligatoire enfant"

    MedicalCodes.push(...[
        ccam1,
        ccam2,
        ccam3,
        ccam4,
        cim10,
        snomed1,
        snomed2,
        loinc,
        ngap
    ])
}

createMedicalCodes()

function createOxHelpedText () {
    let helpedText = new OxHelpedText()
    helpedText.id = "0"
    helpedText.attributes = {
        name: "HelpedText",
        text: "Default Helped Text",
        class: "sample_movie",
        field: "description"
    }
    OxHelpedTextListMock.push(helpedText)

    helpedText = new OxHelpedText()
    helpedText.id = "1"
    helpedText.attributes = {
        name: "Lorem",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        class: "sample_movie",
        field: "description"
    }
    OxHelpedTextListMock.push(helpedText)

    helpedText = new OxHelpedText()
    helpedText.id = "2"
    helpedText.attributes = {
        name: "Lorem BR",
        text: "Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit.",
        class: "sample_movie",
        field: "description"
    }
    OxHelpedTextListMock.push(helpedText)

    helpedText = new OxHelpedText()
    helpedText.id = "3"
    helpedText.attributes = {
        name: "Promeneur",
        text: "Le promeneur, berc� par le chant des oiseleurs.",
        class: "sample_movie",
        field: "description"
    }
    OxHelpedTextListMock.push(helpedText)

    helpedText = new OxHelpedText()
    helpedText.id = "4"
    helpedText.attributes = {
        name: "Soleil",
        text: "Le soleil d�cline � l'horizon charmeur.",
        class: "sample_movie",
        field: "description"
    }
    OxHelpedTextListMock.push(helpedText)

    helpedText = new OxHelpedText()
    helpedText.id = "5"
    helpedText.attributes = {
        name: "Ciel",
        text: "Et le ciel, tout en bleu, devient rose avec douceur.",
        class: "sample_movie",
        field: "description",
        depend_value_1: "scifi",
        _vw_depend_field_1: "Science-Fiction"
    }
    OxHelpedTextListMock.push(helpedText)

    helpedText = new OxHelpedText()
    helpedText.id = "6"
    helpedText.attributes = {
        name: "Travailleur",
        text: "Le travailleur, en sueur, abat sa t�che avec ardeur.",
        class: "sample_movie",
        field: "description",
        depend_value_1: "drame",
        depend_value_2: "csa12",
        _vw_depend_field_1: "Drame",
        _vw_depend_field_2: "Interdit au moins de 12 ans"
    }
    OxHelpedTextListMock.push(helpedText)

    helpedText = new OxHelpedText()
    helpedText.id = "7"
    helpedText.attributes = {
        name: "R�compense",
        text: "R�compensent ses efforts, avec bonheur et ferveur.",
        class: "sample_movie",
        field: "description",
        depend_value_1: "action",
        depend_value_2: "csa12",
        _vw_depend_field_1: "Action",
        _vw_depend_field_2: "Interdit au moins de 12 ans"
    }
    OxHelpedTextListMock.push(helpedText)
}

createOxHelpedText()
