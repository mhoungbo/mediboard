
export const currentModuleData = {
    datas: {
        mod_name: "ModuleTest",
        mod_active: true,
        mod_ui_active: "1",
        mod_category: "category_test",
        mod_package: "test"
    },
    links: {
        self: "/mediboard/api/modules/ModuleTest",
        schema: "/mediboard/api/schemas/module",
        history: "/mediboard/api/history/module/42",
        module_url: "?m=ModuleTest",
        tabs: "/mediboard/api/modules/ModuleTest/tabs"
    }
}

export const functionsData = [
    {
        datas: {
            _id: "22",
            text: "Fonction 22",
            group_id: 10,
            is_main: true
        }
    },
    {
        datas: {
            _id: 12,
            text: "Fonction 12",
            group_id: 10,
            is_main: false
        }
    }
]

export const groupData = {
    datas: {
        _id: 10,
        text: "MB-Etab 10",
        raison_sociale: null
    }
}

export const moduleTabsData = [
    {
        datas: {
            mod_name: "ModuleTest",
            tab_name: "Tab1",
            is_standard: true,
            is_param: false,
            is_config: false,
            pinned_order: null
        },
        links: { tab_url: "?m=dPcim10&tab=Tab1" }
    }, {
        datas: {
            mod_name: "ModuleTest",
            tab_name: "Tab2",
            is_standard: true,
            is_param: false,
            is_config: false,
            pinned_order: 0
        },
        links: { tab_url: "?m=dPcim10&tab=Tab2" }
    }, {
        datas: {
            mod_name: "ModuleTest",
            tab_name: "configure",
            is_standard: false,
            is_param: false,
            is_config: true,
            pinned_order: null
        },
        links: { tab_url: "?m=dPcim10&tab=configure" }
    }
]

export const placeholdersData = [
    {
        datas: {
            _id: "368145e1a559e52d8da68f489a2bbe13",
            label: "Saisir une prestation",
            icon: "time",
            disabled: false,
            action: null,
            action_args: null,
            init_action: null,
            counter: null
        }
    },
    {
        datas: {
            _id: "decba95f634235b27a2b3e9407fe871f",
            label: "Acces au porte documents",
            icon: "folderOpen",
            disabled: false,
            action: null,
            action_args: null,
            init_action: null,
            counter: 1
        }
    },
    {
        datas: {
            _id: "dfb234702cbe3adf51eff9ca9ba90d4f",
            label: "Acces a la messagerie interne",
            icon: "accountGroup",
            disabled: false,
            action: null,
            action_args: ["internal"],
            init_action: null,
            counter: 0
        }
    },
    {
        datas: {
            _id: "7ce3ae8be3a241302195c4a0500e4c6f",
            label: "Acces a la messagerie",
            icon: "email",
            disabled: false,
            action: "",
            action_args: null,
            init_action: null,
            counter: 0
        }
    }
]

export const userData = {
    datas: {
        initials: null,
        color: null,
        _color: "CCCCFF",
        actif: true,
        deb_activite: null,
        fin_activite: null,
        _user_first_name: "Yvan",
        _user_last_name: "GRADMIN",
        _user_sexe: "u",
        _user_username: "yvang",
        _initial: "YG",
        _font_color: "000000",
        _can_change_password: true,
        _is_patient: false
    },
    links: {
        self: "/mediboard/api/mediuser/mediusers/985",
        schema: "/mediboard/api/schemas/mediuser",
        history: "/mediboard/api/history/mediuser/985",
        edit_infos: "?m=mediusers&a=edit_infos",
        logout: "?logout=-1",
        default: "?m=dPpatients",
        impersonation: "/switch",
        change_password: "/change_password",
        list: "/users"
    }
}

export const defaultModules = [
    {
        datas: {
            mod_name: "ModuleTest",
            mod_ui_active: "1",
            mod_category: "category_test"
        },
        links: {
            tabs: "/mediboard/api/modules/ModuleTest/tabs"
        }
    },
    {
        datas: {
            mod_name: "ModuleTest2",
            mod_ui_active: "1",
            mod_category: "category_test"
        },
        links: {
            tabs: "/mediboard/api/modules/ModuleTest2/tabs"
        }
    },
    {
        datas: {
            mod_name: "ModuleTest3",
            mod_ui_active: "0",
            mod_category: "category_test_2"
        },
        links: {
            tabs: "/mediboard/api/modules/ModuleTest3/tabs"
        }
    }
]
