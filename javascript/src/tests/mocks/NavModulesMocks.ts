import Module from "@/core/models/Module"
import Tab from "@modules/system/vue/models/Tab"

const module1 = new Module()
module1.name = "Module1"
module1.category = "Category1"
module1.visible = true
module1.url = "url-1"

const module2 = new Module()
module2.name = "Module2"
module2.category = "Category2"
module2.url = "url-2"
module2.visible = true

const module3 = new Module()
module3.name = "Module3"
module3.category = "Category3"
module3.url = "url-3"
module3.visible = true
module3.tabsUrl = "tabs3"

const module4 = new Module()
module4.name = "Module4"
module4.category = "Category4"
module4.url = "url-4"
module4.visible = true

const module5 = new Module()
module5.name = "Module5"
module5.category = "Category5"
module5.url = "url-5"
module5.visible = true
module5.tabsUrl = "tabs5"

const tab1 = new Tab()
tab1.moduleName = "Module1"
tab1.name = "Tab1"
tab1.isStandard = true
tab1.isParam = false
tab1.isConfig = false
tab1.pinnedOrder = 0
tab1.url = "url-1"

const tab11 = new Tab()
tab11.moduleName = "Module1"
tab11.name = "Tab11"
tab11.isStandard = true
tab11.isParam = false
tab11.isConfig = false
tab11.pinnedOrder = null
tab11.url = "url-11"

const tab12 = new Tab()
tab12.moduleName = "Module1"
tab12.name = "Tab12"
tab12.isStandard = false
tab12.isParam = false
tab12.isConfig = true
tab12.pinnedOrder = null
tab12.url = "url-12"

const tab13 = new Tab()
tab13.moduleName = "Module1"
tab13.name = "Tab13"
tab13.isStandard = false
tab13.isParam = true
tab13.isConfig = false
tab13.pinnedOrder = null
tab13.url = "url-13"

module1.addTabs([tab1, tab11, tab12, tab13])

const tab2 = new Tab()
tab2.moduleName = "Module2"
tab2.name = "Tab2"
tab2.isStandard = true
tab2.isParam = false
tab2.isConfig = false
tab2.pinnedOrder = null
tab2.url = "url-2"

const tab3 = new Tab()
tab3.moduleName = "Module3"
tab3.name = "Tab3"
tab3.isStandard = true
tab3.isParam = false
tab3.isConfig = false
tab3.pinnedOrder = null
tab3.url = "url-3"

const tab4 = new Tab()
tab4.moduleName = "Module2"
tab4.name = "Tab4"
tab4.isStandard = false
tab4.isParam = true
tab4.isConfig = false
tab4.pinnedOrder = null
tab4.url = "url-4"

module2.addTab(tab4)

const tab5 = new Tab()
tab5.moduleName = "Module3"
tab5.name = "Tab5"
tab5.isStandard = false
tab5.isParam = false
tab5.isConfig = true
tab5.pinnedOrder = null
tab5.url = "url-5"

module3.addTab(tab5)

export const modules = [module1, module2, module3, module4, module5]
export const tabShortcuts = [tab1, tab2, tab3, tab4]
