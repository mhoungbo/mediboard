import Function from "@modules/mediusers/vue/Models/Function"
import Group from "@modules/dPetablissement/vue/models/Group"
import { cloneObject } from "@/core/utils/OxObjectTools"

const function1 = new Function()
function1.id = "1"
function1.name = "Function 1"
function1.groupId = 1
function1.isMain = true

const function2 = new Function()
function2.id = "2"
function2.name = "Function 2"
function2.groupId = 3
function2.isMain = false

export const functions = [function1, function2]

const group1 = new Group()
group1.id = "1"
group1.name = "Group 1"
group1.isMain = true
group1.isSecondary = false

const group2 = new Group()
group2.id = "2"
group2.name = "Group 2"
group2.isMain = false
group2.isSecondary = false

const group3 = new Group()
group3.id = "3"
group3.name = "Group 3"
group3.isMain = false
group3.isSecondary = false

export const groups = [group1, group2, group3]

export const groupSelected = cloneObject(group1)
