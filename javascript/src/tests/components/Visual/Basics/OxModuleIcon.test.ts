/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxTest from "@oxify/utils/OxTest"
import OxModuleIcon from "@/components/Visual/Basics/OxModuleIcon/OxModuleIcon"
import { shallowMount, Wrapper } from "@vue/test-utils"
import { nextTick } from "vue"

/* eslint-disable dot-notation */

/**
 * Test pour la classe OxModuleIcon
 */
export default class OxModuleIconTest extends OxTest {
    protected component = OxModuleIcon

    /**
     * @inheritDoc
     */
    protected vueComponent (props: object): OxModuleIcon {
        return this.mountComponent(props).vm as OxModuleIcon
    }

    /**
     * @inheritDoc
     */
    protected mountComponent (props: object): Wrapper<OxModuleIcon> {
        return super.mountComponent(props) as Wrapper<OxModuleIcon>
    }

    public async testShowCategoryIcon () {
        const oxModuleIcon = this.vueComponent({
            moduleName: "ModuleTest",
            moduleCategory: "CagetoryTest"
        })
        await oxModuleIcon.$nextTick()
        this.assertTrue(this.privateCall(oxModuleIcon, "showCategoryIcon"))
    }

    public async testDefaultIconSize () {
        const oxModuleIcon = this.vueComponent({
            moduleName: "ModuleTest",
            moduleCategory: "CagetoryTest"
        })
        await oxModuleIcon.$nextTick()
        this.assertEqual(this.privateCall(oxModuleIcon, "iconSize"), 26)
    }

    public async testSmallIconSize () {
        const oxModuleIcon = this.vueComponent({
            moduleName: "ModuleTest",
            moduleCategory: "CagetoryTest",
            small: true
        })
        await oxModuleIcon.$nextTick()
        this.assertEqual(this.privateCall(oxModuleIcon, "iconSize"), 20)
    }
}

(new OxModuleIconTest()).launchTests()

describe(
    "OxModuleIcon",
    () => {
        test.each([
            { category: "interoperabilite", expectedIcon: "shareVariant" },
            { category: "import", expectedIcon: "databaseImport" },
            { category: "dossier_patient", expectedIcon: "badgeAccountHorizontal" },
            { category: "circuit_patient", expectedIcon: "hospitalBuilding" },
            { category: "erp", expectedIcon: "packageVariant" },
            { category: "administratif", expectedIcon: "tune" },
            { category: "referentiel", expectedIcon: "compass" },
            { category: "plateau_technique", expectedIcon: "heartPulse" },
            { category: "systeme", expectedIcon: "tools" },
            { category: "parametrage", expectedIcon: "cogs" },
            { category: "reporting", expectedIcon: "chartPie" },
            { category: "", expectedIcon: "bookmark" }
        ])("with category $category should be icon $expectedIcon", async ({ category, expectedIcon }) => {
            const moduleIcon = shallowMount(OxModuleIcon, {
                propsData: {
                    moduleName: "ModuleTest",
                    moduleCategory: category,
                    small: true
                }
            })
            await nextTick()
            expect(moduleIcon.vm["getCategoryIcon"]).toBe(expectedIcon)
        })
    }
)
