/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import ModuleLine from "@/components/Appbar/ModuleLine/ModuleLine.vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import { currentModule } from "@/tests/mocks/ModuleDetailMocks"
import OxTranslator from "@/core/plugins/OxTranslator"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

const mountComponent = (props?: object) => {
    const defaultProps = {
        module: currentModule
    }

    return shallowMount(ModuleLine, {
        propsData: { ...defaultProps, ...props },
        localVue
    })
}

describe("ModuleLine", function () {
    beforeAll(() => {
        setActivePinia(pinia)
    })

    test("Has proper class when active", () => {
        const c = mountComponent({ isActive: true })
        expect(c.vm["classes"]).toBe("active")
    })

    test("Has no class by default", () => {
        const c = mountComponent()
        expect(c.vm["classes"]).toBe("")
    })

    test("Has proper class when focus", () => {
        const c = mountComponent({ isFocus: true })
        expect(c.vm["classes"]).toBe(" focused")
    })

    test("Has proper class when active and focus", () => {
        const c = mountComponent({ isActive: true, isFocus: true })
        expect(c.vm["classes"]).toBe("active focused")
    })

    test("Click on detail with mouse", async () => {
        const c = mountComponent()
        c.vm["clickDetail"](new MouseEvent("click"))
        await c.vm.$nextTick()

        expect(c.emitted("detailClick")).toEqual([[{ module: currentModule, fromKeyboard: false }]])
    })

    test("Click on detail with keyboard", async () => {
        const c = mountComponent()
        c.vm["clickDetail"](new KeyboardEvent("keydown"), true)
        await c.vm.$nextTick()

        expect(c.emitted("detailClick")).toEqual([[{ module: currentModule, fromKeyboard: true }]])
    })

    test("Emits click without newTab event when clicked", async () => {
        const c = mountComponent()
        const event = new MouseEvent("click")
        c.vm["click"](event)
        await c.vm.$nextTick()

        expect(c.emitted("click")).toEqual([[{ module: currentModule, newTab: false }]])
    })

    test("Emits click with newTab event when clicked with ctrlKey press", async () => {
        const c = mountComponent()
        const event = new MouseEvent("click", {
            ctrlKey: true
        })
        c.vm["click"](event)
        await c.vm.$nextTick()

        expect(c.emitted("click")).toEqual([[{ module: currentModule, newTab: true }]])
    })
})
