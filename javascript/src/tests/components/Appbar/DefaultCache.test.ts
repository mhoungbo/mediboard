/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import {
    getKeyCache,
    getTabForModule,
    getUserDefaultTabs, removeTabForModule,
    setTabForModule, setUserDefaultTabs
} from "@/components/Appbar/utils/DefaultCache"
import OxTest from "@oxify/utils/OxTest"

/**
 * Module default tabs cache tests
 */
export default class DefaultCacheTest extends OxTest {
    protected component = "DefaultCache"

    public testGetKeyCache () {
        const key = getKeyCache("sessionsHash")
        expect(key).toEqual("defaultTabs-sessionsHash")
    }

    public testSetFirstTabForModule () {
        setTabForModule("sessionsHash", "module1", "url1")
        expect(localStorage[getKeyCache("sessionsHash")]).toEqual("{\"module1\":\"url1\"}")
    }

    public testSetTabForModule () {
        setTabForModule("sessionsHash", "module2", "url2")
        expect(localStorage[getKeyCache("sessionsHash")]).toEqual("{\"module1\":\"url1\",\"module2\":\"url2\"}")
    }

    public testUpdateTabForModule () {
        setTabForModule("sessionsHash", "module2", "url2bis")
        expect(localStorage[getKeyCache("sessionsHash")]).toEqual("{\"module1\":\"url1\",\"module2\":\"url2bis\"}")
    }

    public testGetUserDefaultTabs () {
        const result = getUserDefaultTabs("sessionsHash")
        expect(result).toEqual({
            module1: "url1",
            module2: "url2bis"
        })
    }

    public testGetTabForExistingModule () {
        const result = getTabForModule("sessionsHash", "module2")
        expect(result).toEqual("url2bis")
    }

    public testGetTabForNonExistingModule () {
        const result = getTabForModule("sessionsHash", "NonExistingModule")
        expect(result).toBeUndefined()
    }

    public testGetTabForNonExistingUser () {
        setTabForModule("sessionHash", "moduleTest", "urlTest")
        getUserDefaultTabs("anotherSessionHash")
        expect(getUserDefaultTabs("sessionHash")).toBeNull()
    }

    public testRemoveOldKeyOnFirstOpening () {
        const result = getTabForModule("nonExistingUser", "module2")
        expect(result).toBeUndefined()
    }

    public testGetUserDefaultTabsForNonExistingUser () {
        const result = getUserDefaultTabs("nonExistingUser")
        expect(result).toBeNull()
    }

    public testSetUserDefaultTabs () {
        const defaultTabs = {
            module1: "url1bis",
            module2: "url2ter",
            module3: "url3"
        }
        setUserDefaultTabs("sessionsHash", defaultTabs)
        expect(localStorage[getKeyCache("sessionsHash")]).toEqual("{\"module1\":\"url1bis\",\"module2\":\"url2ter\",\"module3\":\"url3\"}")
    }

    public testRemoveTabForModule () {
        removeTabForModule("sessionsHash", "module2")
        expect(localStorage[getKeyCache("sessionsHash")]).toEqual("{\"module1\":\"url1bis\",\"module3\":\"url3\"}")
    }
}

(new DefaultCacheTest()).launchTests()
