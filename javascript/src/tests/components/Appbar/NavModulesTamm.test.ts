/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import { getTabForModule } from "@/components/Appbar/utils/DefaultCache"
import OxTranslator from "@/core/plugins/OxTranslator"
import { modules, tabShortcuts } from "@/tests/mocks/NavModulesMocks"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"
import { useNavStore } from "@/core/stores/nav"
import NavModulesTamm from "@/components/Appbar/NavModules/NavModulesTamm/NavModulesTamm.vue"
import { flushPromises } from "@oxify/utils/OxTest"
import * as AppbarUtils from "@/components/Appbar/utils/AppbarUtils"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)
const assignMock = jest.fn()
let navStore

// @ts-ignore
delete window.location
// @ts-ignore
window.location = { assign: assignMock }
// @ts-ignore
window.WaitingMessage = { show: assignMock }

jest.spyOn(AppbarUtils, "updateModulesDefaultTabs").mockImplementation(async (sessionHash, url) => {})

const mountComponent = (props?: object) => {
    const defaultProps = {
        tabShortcuts,
        homeLink: "homeLink",
        defaultTabsLink: "defaultTabsLink",
        tammMenu: []
    }

    return shallowMount(NavModulesTamm, {
        propsData: { ...defaultProps, ...props },
        stubs: {
            ModuleDetailMobile: true
        },
        localVue
    })
}

describe("NavModules", () => {
    beforeAll(() => {
        setActivePinia(oxPiniaCore)
        navStore = useNavStore()
        navStore.modules = modules
    })
    afterEach(() => {
        assignMock.mockClear()
    })

    test("Has no default class", () => {
        const c = mountComponent()
        expect(c.vm["classes"]).toBe("")
    })

    test("Expand works", () => {
        const c = mountComponent()
        c.vm["toggleNav"]()

        expect(c.vm["expand"]).toBe(true)
    })

    test("Collapse works", () => {
        const c = mountComponent()
        c.vm["toggleNav"]()
        c.vm["toggleNav"]()

        expect(c.vm["expand"]).toBe(false)
    })

    test("Doesn't display empty state by default", () => {
        const c = mountComponent()
        expect(c.vm["displayEmpty"]).toBe(false)
    })

    test("Display empty state when no matching result with search", async () => {
        const c = mountComponent({ canSeeModules: true })
        await flushPromises()

        c.vm["filterModules"]("NonExistentModule")

        expect(c.vm["displayEmpty"]).toBe(true)
        expect(c.vm["modulesClasses"]).toBe("empty")
    })

    test("Search module by module name", () => {
        const c = mountComponent()
        c.vm["filterModules"]("dule2")

        const result = c.vm["modulesDisplayed"]

        expect(result).toHaveLength(1)
        expect(result[0]).toEqual(modules[1])
    })

    test("Resert search", () => {
        const c = mountComponent()
        c.vm["filterModules"]("dule2")
        c.vm["resetSearch"]()

        const result = c.vm["modulesDisplayed"]

        expect(result).toHaveLength(5)
        expect(c.vm["moduleFilter"]).toBe("")
    })

    test("Emits input with false payload when clicked outside", async () => {
        const c = mountComponent()
        c.vm["clickOutside"]()
        await c.vm.$nextTick()

        expect(c.emitted("input")).toEqual([[false]])
    })

    test("Affect detailled module", async () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[1] })
        await c.vm.$nextTick()

        expect(c.vm["checkActive"]("Module2")).toBe(true)
    })

    test("Update module default tab on detailled module access", () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[3] })

        expect(getTabForModule("", "Module4")).toEqual("url-4")
    })

    test("Unselected module is not active", () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[1] })

        expect(c.vm["checkActive"]("Module1")).toBe(false)
    })

    test("Get module category", () => {
        const c = mountComponent()
        expect(c.vm["getModuleCategory"]("Module1")).toBe("Category1")
    })

    test("Reset state on deactivated", async () => {
        const c = mountComponent()
        c.vm["showDetail"] = true
        c.vm["detailModule"] = true
        c.vm["filterModules"]("ule")
        // Deactivated
        c.vm["resetNavModules"]()
        await c.vm.$nextTick()

        expect(c.vm["showDetail"]).toBe(false)
        expect(c.vm["detailModule"]).toBe(false)
        expect(c.vm["moduleFilter"]).toBe("")
    })
})
