/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import GroupLine from "@/components/Appbar/GroupLine/GroupLine.vue"
import { shallowMount } from "@vue/test-utils"
import Group from "@modules/dPetablissement/vue/models/Group"
import { cloneObject } from "@/core/utils/OxObjectTools"

/* eslint-disable dot-notation */

const group = new Group()
group.id = "1"
group.name = "Group test"
group.isMain = false
group.isSecondary = false

describe("GroupLine", () => {
    test("Active line has corresponding class", () => {
        const groupLine = shallowMount(GroupLine, {
            propsData: {
                group,
                functionName: false,
                actived: true
            }
        })

        expect(groupLine.vm["lineClass"]).toBe("active")
    })

    test("Inactive line has no class", () => {
        const groupLine = shallowMount(GroupLine, {
            propsData: {
                group,
                functionName: false,
                actived: false
            }
        })

        expect(groupLine.vm["lineClass"]).toBe("")
    })

    test("Line for main group has corresponding class", () => {
        const mainGroup = cloneObject(group)
        mainGroup.isMain = true
        const groupLine = shallowMount(GroupLine, {
            propsData: {
                group: mainGroup,
                functionName: false,
                actived: false
            }
        })

        expect(groupLine.vm["functionClass"]).toBe("mainFunction")
    })

    test("Line with secondary group has corresponding class", () => {
        const secondaryGroup = cloneObject(group)
        secondaryGroup.isSecondary = true
        const groupLine = shallowMount(GroupLine, {
            propsData: {
                group: secondaryGroup,
                functionName: false,
                actived: false
            }
        })

        expect(groupLine.vm["functionClass"]).toBe("secondaryFunction")
    })

    test("Line with basic group has no class", () => {
        const groupLine = shallowMount(GroupLine, {
            propsData: {
                group,
                functionName: false,
                actived: false
            }
        })

        expect(groupLine.vm["functionClass"]).toBe("")
    })

    test("Click on line", async () => {
        const groupLine = shallowMount(GroupLine, {
            propsData: {
                group,
                functionName: false,
                actived: false
            }
        })
        groupLine.vm["selectGroup"]()
        await groupLine.vm.$nextTick()

        expect(groupLine.emitted("click")).toBeTruthy()
        expect(groupLine.emitted("click")).toHaveLength(1)
        expect(groupLine.emitted("click")![0]).toEqual([group.id])
    })
})
