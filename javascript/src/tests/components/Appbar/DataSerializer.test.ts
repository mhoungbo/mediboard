/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import {
    appbarPropTransformer, createFunctionFromAppbarProp,
    createGroupFromAppbarProp,
    createMediuserFromAppbarProp, createModuleFromAppbarProp, createTabFromAppbarProp
} from "@/components/Appbar/utils/DataSerializer"
import { AppbarProp } from "@/components/Appbar/types/AppbarTypes"
import Group from "@modules/dPetablissement/vue/models/Group"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import Function from "@modules/mediusers/vue/Models/Function"
import Tab from "@modules/system/vue/models/Tab"
import Module from "@/core/models/Module"

describe("DataSerializer", () => {
    test("Appbar prop transformer works", () => {
        const propData: AppbarProp = {
            datas: {
                a: "test",
                b: 2,
                c: true
            },
            links: {
                link1: "test",
                link2: "test2"
            }
        }
        expect(appbarPropTransformer(propData)).toEqual({
            a: "test",
            b: 2,
            c: true,
            _links: {
                link1: "test",
                link2: "test2"
            }
        })
    })

    test("Create group from appbar prop works", () => {
        const propData: AppbarProp = {
            datas: {
                _id: "1",
                text: "Group Test"
            },
            links: {
                groups: "groups_url"
            }
        }
        const result = createGroupFromAppbarProp(propData)

        expect(result).toBeInstanceOf(Group)
        expect(result.id).toBe("1")
        expect(result.name).toBe("Group Test")
        expect(result.groupsUrl).toBe("groups_url")
    })

    test("Create Mediuser from appbar prop works", () => {
        const propData: AppbarProp = {
            datas: {
                _can_change_password: true,
                _color: "AEAEAE",
                _font_color: "EBEBEB",
                _initial: "QD",
                _is_admin: true,
                _is_patient: false,
                _user_first_name: "User",
                _user_last_name: "TEST",
                _user_username: "usertest",
                impersonator: {
                    firstname: "Ator",
                    lastname: "IMPER"
                }
            },
            links: {
                change_password: "change_password_url",
                default: "default_url",
                edit_infos: "edit_infos_url",
                impersonation: "impersonation_url",
                exit_impersonation: "exit_impersonation_url",
                list: "list_url"
            }
        }
        const result = createMediuserFromAppbarProp(propData)

        expect(result).toBeInstanceOf(Mediuser)
        expect(result.canChangePassword).toBe(true)
        expect(result.color).toBe("#AEAEAE")
        expect(result.fontColor).toBe("#EBEBEB")
        expect(result.initials).toBe("QD")
        expect(result.isAdmin).toBe(true)
        expect(result.isPatient).toBe(false)
        expect(result.firstName).toBe("User")
        expect(result.lastName).toBe("TEST")
        expect(result.username).toBe("usertest")
        expect(result.impersonator).toEqual({ firstname: "Ator", lastname: "IMPER" })
        expect(result.changePasswordUrl).toBe("change_password_url")
        expect(result.defaultUrl).toBe("default_url")
        expect(result.editInfosUrl).toBe("edit_infos_url")
        expect(result.impersonationUrl).toBe("impersonation_url")
        expect(result.exitImpersonationUrl).toBe("exit_impersonation_url")
        expect(result.listUrl).toBe("list_url")
    })

    test("Create function from appbar prop works", () => {
        const propData: AppbarProp = {
            datas: {
                _id: "1",
                group_id: 10,
                is_main: true,
                text: "Function Test"
            }
        }
        const result = createFunctionFromAppbarProp(propData)

        expect(result).toBeInstanceOf(Function)
        expect(result.id).toBe("1")
        expect(result.groupId).toBe(10)
        expect(result.isMain).toBe(true)
        expect(result.name).toBe("Function Test")
    })

    test("Create module from appbar prop works", () => {
        const propData: AppbarProp = {
            datas: {
                mod_name: "ModuleTest",
                mod_category: "category_test",
                mod_ui_active: "1"
            },
            links: {
                tabs: "tabs_url",
                default_tab: "module_url"
            }
        }
        const result = createModuleFromAppbarProp(propData)

        expect(result).toBeInstanceOf(Module)
        expect(result.name).toBe("ModuleTest")
        expect(result.category).toBe("category_test")
        expect(result.visible).toBe(true)
        expect(result.tabsUrl).toBe("tabs_url")
        expect(result.url).toBe("module_url")
    })

    test("Create tab from appbar prop works", () => {
        const propData: AppbarProp = {
            datas: {
                tab_name: "Tab1",
                mod_name: "ModuleTest",
                is_config: false,
                is_param: false,
                is_standard: true,
                pinned_order: null
            },
            links: {
                tab_url: "tab_url"
            }
        }
        const result = createTabFromAppbarProp(propData)

        expect(result).toBeInstanceOf(Tab)
        expect(result.name).toBe("Tab1")
        expect(result.moduleName).toBe("ModuleTest")
        expect(result.isConfig).toBe(false)
        expect(result.isParam).toBe(false)
        expect(result.isStandard).toBe(true)
        expect(result.pinnedOrder).toBe(null)
        expect(result.url).toBe("tab_url")
    })
})
