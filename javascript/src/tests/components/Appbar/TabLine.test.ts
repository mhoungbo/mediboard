/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import TabLine from "@/components/Appbar/TabLine/TabLine.vue"
import Tab from "@modules/system/vue/models/Tab"
import OxTranslator from "@/core/plugins/OxTranslator"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)

const tab = new Tab()
tab.name = "TabTest"
tab.moduleName = "ModuleTest"
tab.url = "tab-url"

const mountComponent = (props?: object) => {
    const defaultProps = {
        tab,
        moduleName: "ModuleTest"
    }

    return shallowMount(TabLine, {
        propsData: { ...defaultProps, ...props },
        localVue
    })
}

describe("TabLine", () => {
    test("Has proper classes when pinned", () => {
        const c = mountComponent({ pined: true })
        expect(c.vm["classes"]).toEqual({
            pined: true,
            active: false,
            pinable: true,
            focused: false
        })
    })

    test("Has proper classes when not pinable", () => {
        const c = mountComponent({ showPin: false })
        expect(c.vm["classes"]).toEqual({
            pined: false,
            active: false,
            pinable: false,
            focused: false
        })
    })

    test("Has proper classes when active", () => {
        const c = mountComponent({ isActive: true })
        expect(c.vm["classes"]).toEqual({
            pined: false,
            active: true,
            pinable: true,
            focused: false
        })
    })

    test("Click on pin emits changePin event", async () => {
        const c = mountComponent()
        c.vm["clickPin"]()
        await c.vm.$nextTick()

        expect(c.emitted("changePin")).toEqual([[tab]])
    })

    test("Default tab name", () => {
        const c = mountComponent()
        expect(c.vm["tabName"]).toBe("mod-ModuleTest-tab-TabTest")
    })

    test("Param tab name", () => {
        const c = mountComponent({ param: true })
        expect(c.vm["tabName"]).toBe("settings")
    })

    test("Get existent tab link", () => {
        const c = mountComponent()
        expect(c.vm["tabLink"]).toBe(tab.url)
    })

    test("Get non existent tab link", () => {
        const tabWithoutUrl = new Tab()
        tabWithoutUrl.name = "TabTest"
        tabWithoutUrl.moduleName = "ModuleTest"
        const c = mountComponent({ tab: tabWithoutUrl })

        expect(c.vm["tabLink"]).toBe("#")
    })

    test("Emits changePin event when key P is pressed", async () => {
        const c = mountComponent()
        c.vm["keydownTabLine"](new KeyboardEvent("keydown", { code: "KeyP" }))
        await c.vm.$nextTick

        expect(c.emitted("changePin")).toEqual([[tab]])
    })

    test("Emits unsetFocus event when arrow left is pressed", async () => {
        const c = mountComponent()
        c.vm["keydownTabLine"](new KeyboardEvent("keydown", { code: "ArrowLeft" }))
        await c.vm.$nextTick

        expect(c.emitted("unsetFocus")).toBeTruthy()
    })
})
