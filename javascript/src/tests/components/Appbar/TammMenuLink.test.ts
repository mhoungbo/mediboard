/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import TammMenuLink from "@/components/Appbar/NavModules/NavModulesTamm/TammMenuLink/TammMenuLink.vue"
import { MenuLink } from "@/components/Appbar/types/AppbarTypes"
import { shallowMount } from "@vue/test-utils"

/* eslint-disable dot-notation */

const actionLink: MenuLink = {
    title: "LinkTest",
    href: null,
    action: "TestObject.funcTest"
}

const simpleActionLink: MenuLink = {
    title: "LinkTest",
    href: null,
    action: "funcTest"
}

const hrefLink: MenuLink = {
    title: "LinkTest",
    href: "?m=moduleTest",
    action: null
}

const externalHrefLink: MenuLink = {
    title: "LinkTest",
    href: "https://link-test",
    action: null
}

describe("TammMenuLink", () => {
    test("Considers href as a link", () => {
        const c = shallowMount(TammMenuLink, {
            propsData: { link: hrefLink }
        })
        expect(c.vm["isLink"]).toBe(true)
    })

    test("Not considers action as a link", () => {
        const c = shallowMount(TammMenuLink, {
            propsData: { link: actionLink }
        })
        expect(c.vm["isLink"]).toBe(false)
    })

    test("Target for internal link", () => {
        const c = shallowMount(TammMenuLink, {
            propsData: { link: hrefLink }
        })
        expect(c.vm["target"]).toBe("_self")
    })

    test("Target for external link", () => {
        const c = shallowMount(TammMenuLink, {
            propsData: { link: externalHrefLink }
        })
        expect(c.vm["target"]).toBe("_blank")
    })

    test("Call JS Function if exists", async () => {
        const c = shallowMount(TammMenuLink, {
            propsData: { link: simpleActionLink }
        })
        const mockedFunc = jest.fn()
        // @ts-ignore
        window.funcTest = mockedFunc
        c.vm["callFunction"]()
        await c.vm.$nextTick()

        expect(mockedFunc).toBeCalled()
    })

    test("Do not call function if link is given", async () => {
        const c = shallowMount(TammMenuLink, {
            propsData: { link: hrefLink }
        })
        const mockedFunc = jest.fn()
        // @ts-ignore
        window.funcTest = mockedFunc
        c.vm["callFunction"]()
        await c.vm.$nextTick()

        expect(mockedFunc).not.toBeCalled()
    })
})
