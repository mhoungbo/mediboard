/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { shallowMount } from "@vue/test-utils"
import TabBadge from "@/components/Appbar/TabBadge/TabBadge.vue"
import Tab from "@modules/system/vue/models/Tab"
import { TabBadgeModel } from "@/components/Appbar/types/AppbarTypes"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"
import { useNavStore } from "@/core/stores/nav"

/* eslint-disable dot-notation */

const tabWithBadge = new Tab()
tabWithBadge.name = "TabTest"
tabWithBadge.moduleName = "ModuleTest"
tabWithBadge.isStandard = true

const tabWithoutBadge = new Tab()
tabWithoutBadge.name = "TabTest-2"
tabWithoutBadge.moduleName = "ModuleTest"
tabWithoutBadge.isStandard = true

const tabBadge: TabBadgeModel = {
    tab_name: "TabTest",
    module_name: "ModuleTest",
    color: "blue",
    counter: 22
}

let navStore

describe("TabBadge", () => {
    beforeAll(() => {
        setActivePinia(oxPiniaCore)
        navStore = useNavStore()
        navStore.updateTabBadge(tabBadge)
    })

    test("Show counter if exists", () => {
        const c = shallowMount(TabBadge, {
            propsData: {
                tab: tabWithBadge
            }
        })
        expect(c.vm["showCounter"]).toBe(true)
    })

    test("Hide counter if it doesn't exists", () => {
        const c = shallowMount(TabBadge, {
            propsData: {
                tab: tabWithoutBadge
            }
        })
        expect(c.vm["showCounter"]).toBe(false)
    })

    test("Get counter if defined", () => {
        const c = shallowMount(TabBadge, {
            propsData: {
                tab: tabWithBadge
            }
        })

        expect(c.vm["counter"]).toBe(22)
    })

    test("Counter equal 0 if not exists", () => {
        const c = shallowMount(TabBadge, {
            propsData: {
                tab: tabWithoutBadge
            }
        })

        expect(c.vm["counter"]).toBe(0)
    })

    test("Get color if exists", () => {
        const c = shallowMount(TabBadge, {
            propsData: {
                tab: tabWithBadge
            }
        })

        expect(c.vm["badgeColor"]).toBe("blue")
    })

    test("Get color if not exists", () => {
        const c = shallowMount(TabBadge, {
            propsData: {
                tab: tabWithoutBadge
            }
        })

        expect(c.vm["badgeColor"]).toBe("")
    })
})
