/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import TabSelector from "@/components/Appbar/TabSelector/TabSelector.vue"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"
import { useNavStore } from "@/core/stores/nav"
import { modules } from "@/tests/mocks/NavModulesMocks"
import { shallowMount } from "@vue/test-utils"
import Tab from "@modules/system/vue/models/Tab"

/* eslint-disable dot-notation */

let navStore

const mountComponent = (props?: object) => {
    const defaultProps = {
        tabs: [...modules[0].pinnedTabs, ...modules[0].standardTabs, ...modules[0].paramTabs, ...modules[0].configureTabs]
    }

    return shallowMount(TabSelector, {
        propsData: { ...defaultProps, ...props }
    })
}

describe("TabSelector", () => {
    beforeAll(() => {
        setActivePinia(oxPiniaCore)
        navStore = useNavStore()
        navStore.modules = modules
        navStore.currentModuleName = "Module1"
        navStore.tabActive = "Tab11"
    })

    test("Param tab generation", () => {
        const c = mountComponent()
        const result = c.vm["paramTab"]
        expect(result).toBeInstanceOf(Tab)
        expect(result.url).toBe("url-13")
    })

    test("Config tab generation", () => {
        const c = mountComponent()
        const result = c.vm["configTab"]
        expect(result).toBeInstanceOf(Tab)
        expect(result.url).toBe("url-12")
    })

    test("Show divider when there is tabs and param tab", () => {
        const c = mountComponent({ param: true })
        expect(c.vm["showDivider"]).toBe(true)
    })

    test("Show divider when there is tabs and config tab", () => {
        const c = mountComponent({ configure: true })
        expect(c.vm["showDivider"]).toBe(true)
    })

    test("Hide divider when no param and no config tabs", () => {
        const c = mountComponent()
        expect(c.vm["showDivider"]).toBe(false)
    })

    test("Hide divider when no standard tabs", () => {
        const c = mountComponent({ tabs: [], param: true, configure: true })
    })

    test("Pin tab emits addPin event", async () => {
        const c = mountComponent()
        const tab = modules[0].standardTabs[0]
        c.vm["pinTab"](tab)
        await c.vm.$nextTick()

        expect(c.emitted("addPin")).toEqual([[tab]])
    })
})
