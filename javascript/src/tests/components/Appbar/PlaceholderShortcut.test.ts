/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { shallowMount } from "@vue/test-utils"
import PlaceholderShortcut from "@/components/Appbar/PlaceholderShortcut/PlaceholderShortcut.vue"
import {
    appFinePlaceholder, ecapPlaceholder,
    placeholderWithAction, placeholderWithCounter,
    placeholderWithInitAction,
    placeholderWithoutAction
} from "@/tests/mocks/PlaceholderMocks"

/* eslint-disable dot-notation */

describe("PlaceholderShortcut", () => {
    test("Show counter if is exists", () => {
        const c = shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: placeholderWithCounter
            }
        })

        expect(c.vm["showCounter"]).toBe(true)
    })

    test("Hide counter if is not exists", () => {
        const c = shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: placeholderWithAction
            }
        })

        expect(c.vm["showCounter"]).toBe(false)
    })

    test("Call init action on mounted if it's define", () => {
        const mockedFunc = jest.fn((x, y) => [x, y])
        // @ts-ignore
        window.testFunc = mockedFunc
        shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: placeholderWithInitAction
            }
        })

        expect(mockedFunc).toBeCalled()
        expect(mockedFunc.mock.results[0].value).toEqual(["2_placeholder", "2_counter"])
    })

    test("Click on placeholder call action", () => {
        const c = shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: placeholderWithAction
            }
        })
        const mockedClickFunc = jest.fn()
        // @ts-ignore
        window.action = mockedClickFunc

        c.vm["click"]()

        expect(mockedClickFunc).toBeCalled()
    })

    test("Click on placeholder emit event if there is no action", async () => {
        const c = shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: placeholderWithoutAction
            }
        })

        c.vm["click"]()
        await c.vm.$nextTick()

        expect(c.emitted("click")).toBeTruthy()
    })

    test("Click on placeholder emit event if flat prop", async () => {
        const c = shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: placeholderWithAction,
                flat: true
            }
        })

        c.vm["click"]()
        await c.vm.$nextTick()

        expect(c.emitted("click")).toBeTruthy()
    })

    test("Show appfine icon", () => {
        const c = shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: appFinePlaceholder
            }
        })

        expect(c.vm["showAppfine"]).toBe(true)
        expect(c.vm["showIcon"]).toBe(false)
    })

    test("Show ecap icon", () => {
        const c = shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: ecapPlaceholder
            }
        })

        expect(c.vm["showEcap"]).toBe(true)
        expect(c.vm["showIcon"]).toBe(false)
    })

    test("Show default icon", () => {
        const c = shallowMount(PlaceholderShortcut, {
            propsData: {
                placeholder: placeholderWithAction
            }
        })

        expect(c.vm["showIcon"]).toBe(true)
        expect(c.vm["showEcap"]).toBe(false)
        expect(c.vm["showAppfine"]).toBe(false)
    })
})
