/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import ModuleDetailMobile from "@/components/Appbar/ModuleDetail/ModuleDetailMobile/ModuleDetailMobile.vue"
import OxTranslator from "@/core/plugins/OxTranslator"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import { useNavStore } from "@/core/stores/nav"
import { currentModule } from "@/tests/mocks/ModuleDetailMocks"
import { cloneObject } from "@/core/utils/OxObjectTools"
import Tab from "@modules/system/vue/models/Tab"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)
let navStore

const mountComponent = (props?: object) => {
    const defaultProps = {
        module: currentModule
    }

    return shallowMount(ModuleDetailMobile, {
        propsData: { ...defaultProps, ...props },
        stubs: {
            Draggable: true
        },
        localVue
    })
}

describe("ModuleDetailMobile", function () {
    beforeAll(() => {
        setActivePinia(pinia)
        navStore = useNavStore()
        navStore.currentModuleName = currentModule.name
        navStore.modules = [currentModule]
        navStore.tabActive = "Tab1"
    })

    test("Show footer when there is param tab", () => {
        const module = cloneObject(currentModule)
        module.configureTabs.length = 0
        const c = mountComponent({ module })

        expect(c.vm["showParam"]).toBe(true)
        expect(c.vm["showConfig"]).toBe(false)
        expect(c.vm["showFooter"]).toBe(true)
    })

    test("Show footer when there is config tab", () => {
        const module = cloneObject(currentModule)
        module.paramTabs.length = 0
        const c = mountComponent({ module })

        expect(c.vm["showParam"]).toBe(false)
        expect(c.vm["showConfig"]).toBe(true)
        expect(c.vm["showFooter"]).toBe(true)
    })

    test("Hide footer when there is no param and no config tab", () => {
        const module = cloneObject(currentModule)
        module.paramTabs.length = 0
        module.configureTabs.length = 0
        const c = mountComponent({ module })

        expect(c.vm["showParam"]).toBe(false)
        expect(c.vm["showConfig"]).toBe(false)
        expect(c.vm["showFooter"]).toBe(false)
    })

    test("Param tab generation", () => {
        const c = mountComponent()
        expect(c.vm["paramTab"]).toBeInstanceOf(Tab)
        expect(c.vm["paramTab"].name).toBe("")
        expect(c.vm["paramTab"].url).toBe("url-4")
    })

    test("Config tab generation", () => {
        const c = mountComponent()
        expect(c.vm["configTab"]).toBeInstanceOf(Tab)
        expect(c.vm["configTab"].name).toBe("Tab5")
        expect(c.vm["configTab"].url).toBe("url-5")
    })

    test("Doesn't detect active tab for different module", () => {
        const module = cloneObject(currentModule)
        module.name = "OtherModule"
        const c = mountComponent({ module })

        expect(c.vm["checkTabActive"]("Tab1")).toBe(false)
    })

    test("Detect active tab", () => {
        const c = mountComponent()
        expect(c.vm["checkTabActive"]("Tab1")).toBe(true)
    })

    test("Detect non active tab", () => {
        const c = mountComponent()
        expect(c.vm["checkTabActive"]("Tab2")).toBe(false)
    })

    test("Detect param tab is active", () => {
        navStore.tabActive = "Tab4"
        const c = mountComponent()

        expect(c.vm["checkTabActive"]("", "param")).toBe(true)
    })

    test("Detect config tab is active", () => {
        navStore.tabActive = "Tab5"
        const c = mountComponent()

        expect(c.vm["checkTabActive"]("", "config")).toBe(true)
    })

    test("Emits close event on back button click", async () => {
        const c = mountComponent()
        c.vm["back"]()
        await c.vm.$nextTick()

        expect(c.emitted("close")).toBeTruthy()
    })
})
