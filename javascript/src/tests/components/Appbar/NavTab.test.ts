/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import NavTab from "@/components/Appbar/NavTabs/NavTab/NavTab.vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"
import Tab from "@modules/system/vue/models/Tab"
import { useNavStore } from "@/core/stores/nav"
import OxTranslator from "@/core/plugins/OxTranslator"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)
let navStore
const tab = new Tab()
tab.name = "TabTest"

const mountComponent = (props?: object) => {
    const defaultProps = {
        tab
    }

    return shallowMount(NavTab, {
        propsData: { ...defaultProps, ...props },
        localVue
    })
}

describe("NavTab", () => {
    beforeAll(() => {
        jest.useFakeTimers()
        setActivePinia(oxPiniaCore)
        navStore = useNavStore()
        navStore.currentModuleName = "ModuleTest"
    })

    test("Has no content class by default", () => {
        const c = mountComponent()
        expect(c.vm["contentClasses"]).toBe("")
    })

    test("Has no class by default", () => {
        const c = mountComponent()
        expect(c.vm["classes"]).toBe(" ")
    })

    test("Has animation classes when active", () => {
        const c = mountComponent({ tabActive: true })
        expect(c.vm["classes"]).toBe("active animated")
    })

    test("Animation class disappear after 250ms", () => {
        const c = mountComponent({ tabActive: true })
        jest.advanceTimersByTime(251)
        expect(c.vm["classes"]).toBe("active ")
    })

    test("Has not active class when parent is hover", () => {
        const c = mountComponent({ tabActive: true, parentHover: true })
        expect(c.vm["classes"]).toBe(" animated")
    })

    test("Classic tab name", () => {
        const c = mountComponent()
        expect(c.vm["tabName"]).toBe("mod-ModuleTest-tab-TabTest")
    })

    test("Param tab name", () => {
        const c = mountComponent({ param: true })
        expect(c.vm["tabName"]).toBe("settings")
    })

    test("Click on pin tab action", async () => {
        const c = mountComponent()
        c.vm["addPinnedClick"]()
        await c.vm.$nextTick()

        expect(c.emitted("addPin")).toEqual([[tab]])
    })

    test("Click on remove pinned tab action", async () => {
        const c = mountComponent()
        c.vm["removePinnedClick"]()
        await c.vm.$nextTick()

        expect(c.emitted("removePin")).toEqual([[tab]])
    })

    test("Mouseover tab emits enter event", async () => {
        const c = mountComponent()
        c.vm["enterTab"]()
        await c.vm.$nextTick()

        expect(c.emitted("enter")).toBeTruthy()
    })

    test("Mouseout tab emits leave event", async () => {
        const c = mountComponent()
        c.vm["leaveTab"]()
        await c.vm.$nextTick()

        expect(c.emitted("leave")).toBeTruthy()
    })
})
