/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import NavTabs from "@/components/Appbar/NavTabs/NavTabs.vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"
import { useNavStore } from "@/core/stores/nav"
import { modules } from "@/tests/mocks/NavModulesMocks"
import { cloneObject } from "@/core/utils/OxObjectTools"
import Module from "@/core/models/Module"
import { flushPromises } from "@oxify/utils/OxTest"
import OxTranslator from "@/core/plugins/OxTranslator"

/* eslint-disable dot-notation */

let navStore
const localVue = createLocalVue()
localVue.use(OxTranslator)

const mountComponent = () => {
    return shallowMount(NavTabs, {
        stubs: {
            TabSelector: true
        },
        localVue
    })
}

describe("NavTabs", () => {
    beforeAll(() => {
        setActivePinia(oxPiniaCore)
        navStore = useNavStore()
    })

    beforeEach(() => {
        navStore.modules = modules
        navStore.currentModuleName = "Module1"
        navStore.tabActive = "Tab11"
    })

    test("Show more tab CTA when there is more standard tabs", () => {
        const c = mountComponent()
        expect(c.vm["showMoreTabs"]).toBe(true)
    })

    test("Show more tab CTA whene there is a param tab", () => {
        navStore.currentModuleName = "Module2"
        const c = mountComponent()
        expect(c.vm["showMoreTabs"]).toBe(true)
    })

    test("Show more tab CTA whene there is a config tab", () => {
        navStore.currentModuleName = "Module3"
        const c = mountComponent()
        expect(c.vm["showMoreTabs"]).toBe(true)
    })

    test("Don't show more tab CTA when there is no tab", () => {
        navStore.currentModuleName = "Module4"
        const c = mountComponent()
        expect(c.vm["showMoreTabs"]).toBe(false)
    })

    test("Show current standard if is active", () => {
        const c = mountComponent()
        expect(c.vm["showCurrentStandardTab"]).toBe(true)
    })

    test("Don't show standard tab if current tab is a pinned one", () => {
        navStore.tabActive = "Tab1"
        const c = mountComponent()
        expect(c.vm["showCurrentStandardTab"]).toBe(false)
    })

    test("Don't show standard tab if there is no current tab", () => {
        navStore.tabActive = ""
        const c = mountComponent()
        expect(c.vm["showCurrentStandardTab"]).toBe(false)
    })

    test("Current tab is pinnable if it is standard", () => {
        const c = mountComponent()
        expect(c.vm["tabActiveIsPinnable"]).toBe(true)
    })

    test("Current tab is not pinnable if it is standard", () => {
        navStore.tabActive = "Tab1"
        const c = mountComponent()
        expect(c.vm["tabActiveIsPinnable"]).toBe(false)
    })

    test("Show pinned tabs if they exist", () => {
        const c = mountComponent()
        expect(c.vm["showPinnedTabs"]).toBe(true)
    })

    test("Don't show pinned tabs if they don't exist", () => {
        navStore.currentModuleName = "Module4"
        const c = mountComponent()
        expect(c.vm["showPinnedTabs"]).toBe(false)
    })

    test("Disable drag if there is less than two pinned tabs", () => {
        const c = mountComponent()
        expect(c.vm["disableDrag"]).toBe(true)
    })

    test("Enabled drag if there is more than one pinned tab", async () => {
        const module = cloneObject(navStore.currentModule) as Module
        module.tabsUrl = "tabs"
        module.pinTab(module.standardTabs[0])
        await flushPromises()
        module.name = "ModuleDrag"
        navStore.modules.push(module)
        navStore.currentModuleName = "ModuleDrag"
        const c = mountComponent()
        expect(c.vm["disableDrag"]).toBe(false)
    })

    test("More tabs button class when current tab is pinned", () => {
        navStore.tabActive = "Tab1"
        const c = mountComponent()
        expect(c.vm["moreTabsTabClass"]).toEqual({ active: false, standard: false })
    })

    test("More tabs button class when current tab is standard", () => {
        const c = mountComponent()
        expect(c.vm["moreTabsTabClass"]).toEqual({ active: false, standard: true })
    })

    test("Active more tabs button class", () => {
        navStore.tabActive = "Tab1"
        const c = mountComponent()
        c.vm["moreClick"]()
        expect(c.vm["moreTabsTabClass"]).toEqual({ active: true, standard: false })
    })

    test("Detect active tab", () => {
        const c = mountComponent()
        expect(c.vm["isActive"]("Tab11")).toBe(true)
    })

    test("Detect inactive tab", () => {
        const c = mountComponent()
        expect(c.vm["isActive"]("Tab1")).toBe(false)
    })

    test("Standard tab class when not alone", () => {
        const c = mountComponent()
        expect(c.vm["standardTabClasses"]).toEqual({ standard: true, lonely: false, round: false })
    })

    test("Standard tab class when no more tab", () => {
        navStore.currentModuleName = "Module3"
        navStore.tabActive = "Tab5"
        const c = mountComponent()
        expect(c.vm["standardTabClasses"]).toEqual({ standard: true, lonely: true, round: true })
    })
})
