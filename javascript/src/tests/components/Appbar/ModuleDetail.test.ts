/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import ModuleDetail from "@/components/Appbar/ModuleDetail/ModuleDetail.vue"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import { currentModule } from "@/tests/mocks/ModuleDetailMocks"
import { useNavStore } from "@/core/stores/nav"
import { cloneObject } from "@/core/utils/OxObjectTools"
import Tab from "@modules/system/vue/models/Tab"
import OxTranslator from "@/core/plugins/OxTranslator"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)
let navStore

const mountComponent = (props?: object) => {
    const defaultProps = {
        module: currentModule
    }

    return shallowMount(ModuleDetail, {
        propsData: { ...defaultProps, ...props },
        stubs: {
            Draggable: true
        },
        localVue
    })
}

describe("ModuleDetail", function () {
    beforeAll(() => {
        setActivePinia(pinia)
        navStore = useNavStore()
        navStore.currentModuleName = currentModule.name
        navStore.modules = [currentModule]
        navStore.tabActive = "Tab1"
    })

    test("Show pinned tabs if they exist", () => {
        const c = mountComponent()
        expect(c.vm["showPinnedTabs"]).toBe(true)
    })

    test("Hide pinned tabs if they don't exist", () => {
        const moduleWithoutPinnedTab = cloneObject(currentModule)
        moduleWithoutPinnedTab.pinnedTabs = []
        const c = mountComponent({ module: moduleWithoutPinnedTab })

        expect(c.vm["showPinnedTabs"]).toBe(false)
    })

    test("Show standard tabs if they exist", () => {
        const c = mountComponent()
        expect(c.vm["showStandardTabs"]).toBe(true)
    })

    test("Hide standard tabs if they don't exist", () => {
        const moduleWithoutStandardTabs = cloneObject(currentModule)
        moduleWithoutStandardTabs.standardTabs = []
        const c = mountComponent({ module: moduleWithoutStandardTabs })

        expect(c.vm["showStandardTabs"]).toBe(false)
    })

    test("Show detail if is available", () => {
        const c = mountComponent()
        expect(c.vm["showDetail"]).toBe(true)
    })

    test("Hide detail if is not available", () => {
        const c = mountComponent({ module: false })
        expect(c.vm["showDetail"]).toBe(false)
    })

    test("Show footer when there is param tab", () => {
        const module = cloneObject(currentModule)
        module.configureTabs.length = 0
        const c = mountComponent({ module })

        expect(c.vm["showParam"]).toBe(true)
        expect(c.vm["showConfig"]).toBe(false)
        expect(c.vm["showFooter"]).toBe(true)
    })

    test("Show footer when there is config tab", () => {
        const module = cloneObject(currentModule)
        module.paramTabs.length = 0
        const c = mountComponent({ module })

        expect(c.vm["showParam"]).toBe(false)
        expect(c.vm["showConfig"]).toBe(true)
        expect(c.vm["showFooter"]).toBe(true)
    })

    test("Hide footer when there is no param and no config tab", () => {
        const module = cloneObject(currentModule)
        module.paramTabs.length = 0
        module.configureTabs.length = 0
        const c = mountComponent({ module })

        expect(c.vm["showParam"]).toBe(false)
        expect(c.vm["showConfig"]).toBe(false)
        expect(c.vm["showFooter"]).toBe(false)
    })

    test("Change current pinned tabs order emit event", async () => {
        const c = mountComponent()
        const newPinnedTabs = [currentModule.pinnedTabs[1], currentModule.pinnedTabs[0]]
        c.vm["currentPinnedTabs"] = newPinnedTabs
        await c.vm.$nextTick()

        expect(c.emitted("changePin")).toEqual([[newPinnedTabs]])
    })

    test("Drag is disabled if there are less than two pinned tabs", () => {
        const c = mountComponent()
        expect(c.vm["disableDrag"]).toBe(true)
    })

    test("Drag is enabled if there are more than one pinned tab", () => {
        const module = cloneObject(currentModule)
        const newTab = new Tab()
        newTab.name = "Tab new"
        newTab.pinnedOrder = 1
        module.addTab(newTab)

        const c = mountComponent({ module })

        expect(c.vm["disableDrag"]).toBe(false)
    })

    test("Param tab generation", () => {
        const c = mountComponent()
        expect(c.vm["paramTab"]).toBeInstanceOf(Tab)
        expect(c.vm["paramTab"].name).toBe("")
        expect(c.vm["paramTab"].url).toBe("url-4")
    })

    test("Config tab generation", () => {
        const c = mountComponent()
        expect(c.vm["configTab"]).toBeInstanceOf(Tab)
        expect(c.vm["configTab"].name).toBe("Tab5")
        expect(c.vm["configTab"].url).toBe("url-5")
    })

    test("Don't focus param tab when focusing standard tab", () => {
        const c = mountComponent({ focusTabIndex: 2 })
        expect(c.vm["checkFocusForParam"]).toBe(false)
    })

    test("Focus param tab when focusing one step after standard tab", () => {
        const c = mountComponent({ focusTabIndex: 3 })
        expect(c.vm["checkFocusForParam"]).toBe(true)
    })

    test("Don't focus param tab if it doesn't exists", () => {
        const module = cloneObject(currentModule)
        module.paramTabs.length = 0
        const c = mountComponent({ module, focusTabIndex: 3 })
        expect(c.vm["checkFocusForParam"]).toBe(false)
    })

    test("Focus on config tab when focusing one step after param", () => {
        const c = mountComponent({ focusTabIndex: 4 })
        expect(c.vm["checkFocusForConfig"]).toBe(true)
    })

    test("Don't focus config tab when focusing param tab", () => {
        const c = mountComponent({ focusTabIndex: 3 })
        expect(c.vm["checkFocusForConfig"]).toBe(false)
    })

    test("Don't focus config tab if it doesn't exist", () => {
        const module = cloneObject(currentModule)
        module.configureTabs.length = 0
        const c = mountComponent({ module, focusTabIndex: 4 })
        expect(c.vm["checkFocusForConfig"]).toBe(false)
    })

    test("Focus config tab when focusing one step after standard tabs and there is no param tab", () => {
        const module = cloneObject(currentModule)
        module.paramTabs.length = 0
        const c = mountComponent({ module, focusTabIndex: 3 })
        expect(c.vm["checkFocusForConfig"]).toBe(true)
    })

    test("Pin tab emits addPin event with corresponding tab", async () => {
        const c = mountComponent()
        c.vm["addPin"](currentModule.standardTabs[0])
        await c.vm.$nextTick()

        expect(c.emitted("addPin")).toEqual([[currentModule.standardTabs[0]]])
    })

    test("Remove pinned tab emits removePin event with corresponding tab", async () => {
        const c = mountComponent()
        const tab = currentModule.pinnedTabs[0]
        c.vm["removePin"](tab)
        await c.vm.$nextTick()

        expect(c.emitted("removePin")).toEqual([[tab]])
    })

    test("Doesn't detect active tab for different module", () => {
        const module = cloneObject(currentModule)
        module.name = "OtherModule"
        const c = mountComponent({ module })

        expect(c.vm["checkTabActive"]("Tab1")).toBe(false)
    })

    test("Detect active tab", () => {
        const c = mountComponent()
        expect(c.vm["checkTabActive"]("Tab1")).toBe(true)
    })

    test("Detect non active tab", () => {
        const c = mountComponent()
        expect(c.vm["checkTabActive"]("Tab2")).toBe(false)
    })

    test("Detect param tab is active", () => {
        navStore.tabActive = "Tab4"
        const c = mountComponent()

        expect(c.vm["checkTabActive"]("", "param")).toBe(true)
    })

    test("Detect config tab is active", () => {
        navStore.tabActive = "Tab5"
        const c = mountComponent()

        expect(c.vm["checkTabActive"]("", "config")).toBe(true)
    })

    test("Emits unsetFocus event", async () => {
        const c = mountComponent()
        c.vm["unsetFocus"]()
        await c.vm.$nextTick()

        expect(c.emitted("unsetFocus")).toBeTruthy()
    })
})
