/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import NavModules from "@/components/Appbar/NavModules/NavModules.vue"
import { getTabForModule } from "@/components/Appbar/utils/DefaultCache"
import OxTranslator from "@/core/plugins/OxTranslator"
import { modules, tabShortcuts } from "@/tests/mocks/NavModulesMocks"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"
import { useNavStore } from "@/core/stores/nav"
import { flushPromises } from "@oxify/utils/OxTest"
import * as AppbarUtils from "@/components/Appbar/utils/AppbarUtils"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)
const assignMock = jest.fn()
let navStore

// @ts-ignore
delete window.location
// @ts-ignore
window.location = { assign: assignMock }
// @ts-ignore
window.WaitingMessage = { show: assignMock }

jest.spyOn(AppbarUtils, "updateModulesDefaultTabs").mockImplementation(async (sessionHash, url) => {})

const mountComponent = (props?: object) => {
    const defaultProps = {
        tabShortcuts,
        homeLink: "homeLink",
        defaultTabsLink: "defaultTabsLink"
    }

    return shallowMount(NavModules, {
        propsData: { ...defaultProps, ...props },
        stubs: {
            ModuleDetailMobile: true
        },
        localVue
    })
}

describe("NavModules", () => {
    beforeAll(() => {
        setActivePinia(oxPiniaCore)
        navStore = useNavStore()
        navStore.modules = modules
    })
    afterEach(() => {
        assignMock.mockClear()
    })

    test("Expand on search", () => {
        const c = mountComponent()
        c.vm["filterModules"]("test")

        expect(c.vm["classes"]).toBe("expand")
    })

    test("Collapse when search becomes is empty", () => {
        const c = mountComponent()
        c.vm["filterModules"]("test")
        c.vm["filterModules"]("")

        expect(c.vm["classes"]).toBe("")
    })

    test("Has no default class", () => {
        const c = mountComponent()
        expect(c.vm["classes"]).toBe("")
    })

    test("Doesn't display empty state by default", () => {
        const c = mountComponent()
        expect(c.vm["displayEmpty"]).toBe(false)
    })

    test("Display empty state when no matching result with search", async () => {
        const c = mountComponent()
        await flushPromises()

        c.vm["filterModules"]("NonExistentModule")

        expect(c.vm["displayEmpty"]).toBe(true)
        expect(c.vm["modulesClasses"]).toBe("empty")
    })

    test("Search module by module name", () => {
        const c = mountComponent()
        c.vm["filterModules"]("dule2")

        const result = c.vm["modulesDisplayed"]

        expect(result).toHaveLength(1)
        expect(result[0]).toEqual(modules[1])
    })

    test("Resert search", () => {
        const c = mountComponent()
        c.vm["filterModules"]("dule2")
        c.vm["resetSearch"]()

        const result = c.vm["modulesDisplayed"]

        expect(result).toHaveLength(5)
        expect(c.vm["moduleFilter"]).toBe("")
    })

    test("Show favorite tabs when they exist", () => {
        const c = mountComponent()
        expect(c.vm["showFavTabs"]).toBe(true)
    })

    test("Hide favorite tabs when they don't exist", () => {
        const c = mountComponent({ tabShortcuts: [] })
        expect(c.vm["showFavTabs"]).toBe(false)
    })

    test("Emits input with false payload when clicked outside", async () => {
        const c = mountComponent()
        c.vm["clickOutside"]()
        await c.vm.$nextTick()

        expect(c.emitted("input")).toEqual([[false]])
    })

    test("Affect detailled module", async () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[1], fromKeyboard: false })
        await c.vm.$nextTick()

        expect(c.vm["checkActive"]("Module2")).toBe(true)
    })

    test("Update module default tab on detailled module access", () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[3], fromKeyboard: false })

        expect(getTabForModule("", "Module4")).toEqual("url-4")
    })

    test("Affect detailled module from keyboard", () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[0], fromKeyboard: true })

        expect(c.vm["checkActive"]("Module1")).toBe(true)
        expect(c.vm["focusTabIndex"]).toBe(0)
        expect(c.vm["tabLimit"]).toBe(3)
    })

    test("Unselected module is not active", () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[1], fromKeyboard: false })

        expect(c.vm["checkActive"]("Module1")).toBe(false)
    })

    test("Get module category", () => {
        const c = mountComponent()
        expect(c.vm["getModuleCategory"]("Module1")).toBe("Category1")
    })

    test("Arrow up navigation do nothing when focus first module", () => {
        const c = mountComponent()
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowUp" }))

        expect(c.vm["focusModuleIndex"]).toBe(-1)
    })

    test("Arrow down navigation works", () => {
        const c = mountComponent()
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))

        expect(c.vm["focusModuleIndex"]).toBe(0)
    })

    test("Arrow down navigation do nothing when focus last module", () => {
        const c = mountComponent()
        // Normal navigation
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))

        // Continue to arrow down while last module is selected
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))

        expect(c.vm["focusModuleIndex"]).toBe(4)
    })

    test("Arrow up navigation works", () => {
        const c = mountComponent()
        // Down to third module
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))

        // Up to second module
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowUp" }))

        expect(c.vm["focusModuleIndex"]).toBe(1)
    })

    test("Arrow down navigation impact tabs when module detail is displayed", () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[0], fromKeyboard: true })
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        expect(c.vm["focusModuleIndex"]).toBe(-1)
        expect(c.vm["focusTabIndex"]).toBe(1)
    })

    test("Arrow up in tabs navigation do nothing at top", () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[0], fromKeyboard: true })
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowUp" }))
        expect(c.vm["focusModuleIndex"]).toBe(-1)
        expect(c.vm["focusTabIndex"]).toBe(0)
    })

    test("Arrow down in tabs navigation do nothing at bottom", () => {
        const c = mountComponent()
        c.vm["affectDetailledModule"]({ module: modules[0], fromKeyboard: true })
        // Down to last tab
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        // Keep going down
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))

        expect(c.vm["focusTabIndex"]).toBe(3)
    })

    test("Quit focus on module detail", () => {
        const c = mountComponent()
        c.vm["keyboardShortcutAccess"](new KeyboardEvent("keydown", { code: "ArrowDown" }))
        c.vm["affectDetailledModule"]({ module: modules[0], fromKeyboard: true })
        c.vm["unsetFocusDetail"]()

        expect(c.vm["focusTabIndex"]).toBe(-1)
        expect(c.vm["focusModuleIndex"]).toBe(0)
        expect(c.vm["saveFocusModuleIndex"]).toBe(-1)
    })

    test("Reset state on deactivated", async () => {
        const c = mountComponent()
        c.vm["showDetail"] = true
        c.vm["detailModule"] = true
        c.vm["focusModuleIndex"] = 4
        c.vm["focusTabIndex"] = 2
        c.vm["filterModules"]("ule")
        // Deactivated
        c.vm["resetNavModules"]()
        await c.vm.$nextTick()

        expect(c.vm["showDetail"]).toBe(false)
        expect(c.vm["detailModule"]).toBe(false)
        expect(c.vm["focusModuleIndex"]).toBe(-1)
        expect(c.vm["focusTabIndex"]).toBe(-1)
        expect(c.vm["moduleFilter"]).toBe("")
    })
})
