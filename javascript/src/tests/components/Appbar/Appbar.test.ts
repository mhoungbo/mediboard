/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { createLocalVue, shallowMount } from "@vue/test-utils"
import Appbar from "@/components/Appbar/Appbar.vue"
import Vuetify from "vuetify"
import OxTranslator from "@/core/plugins/OxTranslator"
import User from "@modules/admin/vue/models/User"
import * as OxNotifyManager from "@/core/utils/OxNotifyManager"
import * as OxGUIManager from "@/core/utils/OxGUIManager"
import * as DefaultCache from "@/components/Appbar/utils/DefaultCache"
import {
    currentModuleData, defaultModules,
    functionsData,
    groupData,
    moduleTabsData,
    userData
} from "@/tests/mocks/AppbarMocks"
import { setActivePinia } from "pinia"
import pinia from "@/core/plugins/OxPiniaCore"
import { useNavStore } from "@/core/stores/nav"
import { useUserStore } from "@/core/stores/user"
import { useContextStore } from "@/core/stores/context"
import Group from "@modules/dPetablissement/vue/models/Group"
import Module from "@/core/models/Module"
import Function from "@modules/mediusers/vue/Models/Function"

/* eslint-disable dot-notation */

// Pinia stores
let navStore
let userStore
let contextStore

let defaultWindowLocation
const flashesNotify = {
    error: [],
    info: []
}
const localVue = createLocalVue()
localVue.use(OxTranslator)

const mountAppbar = (props?: object) => {
    const defaultProps = {
        currentModuleData,
        defaultModules,
        functionsData,
        groupData,
        moduleTabs: moduleTabsData,
        placeholdersData: [],
        tabActive: "Tab1",
        userData,
        infoMaj: {},
        tabShortcutsData: [],
        dateNow: "now",
        aboutLink: "/about",
        psCguLink: "/ps_cgu",
        links: {},
        flashesNotify
    }
    return shallowMount(Appbar, {
        propsData: { ...defaultProps, ...props },
        vuetify: new Vuetify(),
        stubs: {
            NavModules: true,
            NavModulesTamm: true,
            UserAccount: true,
            GroupSelector: true,
            PlaceholderShortcut: true,
            PlaceholderLine: true,
            ModuleDetailMobile: true
        },
        localVue
    })
}

describe("Appbar",
    () => {
        beforeAll(() => {
            defaultWindowLocation = window.location
            setActivePinia(pinia)
            navStore = useNavStore()
            userStore = useUserStore()
            contextStore = useContextStore()
        })
        beforeEach(() => {
            window.location = defaultWindowLocation
        })
        afterEach(() => {
            localStorage.clear()
            jest.clearAllMocks()
            userStore.$reset()
        })

        test("NavModule is hide by default", () => {
            const appbar = mountAppbar()

            expect(appbar.vm["moduleClass"]).toBe("")
        })

        test("Display NavModule works", () => {
            const appbar = mountAppbar()

            appbar.vm["displayNavModule"]()

            expect(appbar.vm["moduleClass"]).toBe("active")
        })

        test("NavModule is hide on click outside", () => {
            const appbar = mountAppbar()

            appbar.vm["displayNavModule"]()
            appbar.vm["clickOutside"]()

            expect(appbar.vm["moduleClass"]).toBe("")
        })

        test("User account is hide by default", () => {
            const appbar = mountAppbar()

            expect(appbar.vm["accountClass"]).toBe("")
        })

        test("Show user account works", () => {
            const appbar = mountAppbar()

            appbar.vm["displayAccount"]()

            expect(appbar.vm["accountClass"]).toBe("active")
        })

        test("Group selector is hide by default", () => {
            const appbar = mountAppbar()

            expect(appbar.vm["groupClass"]).toBe("")
        })

        test("Show group selector works", () => {
            const appbar = mountAppbar()

            appbar.vm["displayGroups"]()

            expect(appbar.vm["groupClass"]).toBe("active")
        })

        test("Placeholders are hide by default", () => {
            const appbar = mountAppbar()

            expect(appbar.vm["showPlaceholders"]).toBe(false)
        })

        test("Show placeholders works", () => {
            const appbar = mountAppbar()

            appbar.vm["openPlaceholders"]()

            expect(appbar.vm["showPlaceholders"]).toBe(true)
        })

        test("Close placeholders works", () => {
            const appbar = mountAppbar()

            appbar.vm["openPlaceholders"]()
            appbar.vm["closePlaceholders"]()

            expect(appbar.vm["showPlaceholders"]).toBe(false)
        })

        test("Initialization", () => {
            const appbar = mountAppbar()

            expect(navStore.currentModuleName).toBe("ModuleTest")
            expect(navStore.tabActive).toBe("Tab1")
            expect(navStore.modules).toHaveLength(3)
            expect(navStore.modules[0]).toBeInstanceOf(Module)
            expect(contextStore.currentGroup).toBeInstanceOf(Group)
            expect(contextStore.currentGroup.id).toBe(10)
            expect(contextStore.functions).toHaveLength(2)
            expect(contextStore.functions[0]).toBeInstanceOf(Function)
            expect(appbar.vm["homeLink"]).toBe("?m=dPpatients")
            expect(appbar.vm["userListUrl"]).toBe("users?autocomplete=")
        })

        test("Sets default tab in cache if defined", () => {
            const spy = jest.spyOn(DefaultCache, "setTabForModule")
            userStore.sessionHash = "sessionHash"
            mountAppbar({
                currentModuleData: { ...currentModuleData, ...{ links: { default_tab: "urlDefault" } } }
            })

            expect(spy).toHaveBeenCalledWith("sessionHash", "ModuleTest", "urlDefault")
        })

        test("Detect that screen size is XXL", () => {
            const appbar = mountAppbar()

            appbar.vm["screenWidth"] = 1300

            expect(appbar.vm["isXXL"]).toBe(true)
            expect(appbar.vm["isXL"]).toBe(true)
            expect(appbar.vm["isL"]).toBe(true)
            expect(appbar.vm["isM"]).toBe(true)
        })

        test("Detect that screen size is XL", () => {
            const appbar = mountAppbar()

            appbar.vm["screenWidth"] = 1000

            expect(appbar.vm["isXXL"]).toBe(false)
            expect(appbar.vm["isXL"]).toBe(true)
            expect(appbar.vm["isL"]).toBe(true)
            expect(appbar.vm["isM"]).toBe(true)
        })

        test("Detect that screen size is L", () => {
            const appbar = mountAppbar()

            appbar.vm["screenWidth"] = 900

            expect(appbar.vm["isXXL"]).toBe(false)
            expect(appbar.vm["isXL"]).toBe(false)
            expect(appbar.vm["isL"]).toBe(true)
            expect(appbar.vm["isM"]).toBe(true)
        })

        test("Detect that screen size is M", () => {
            const appbar = mountAppbar()

            appbar.vm["screenWidth"] = 600

            expect(appbar.vm["isXXL"]).toBe(false)
            expect(appbar.vm["isXL"]).toBe(false)
            expect(appbar.vm["isL"]).toBe(false)
            expect(appbar.vm["isM"]).toBe(true)
        })

        test("Detect that screen size is S", () => {
            const appbar = mountAppbar()

            appbar.vm["screenWidth"] = 300

            expect(appbar.vm["isXXL"]).toBe(false)
            expect(appbar.vm["isXL"]).toBe(false)
            expect(appbar.vm["isL"]).toBe(false)
            expect(appbar.vm["isM"]).toBe(false)
        })

        test("Open tabs works", () => {
            const appbar = mountAppbar()

            appbar.vm["displayTabs"]()

            expect(appbar.vm["showModuleDetail"]).toBe(true)
        })

        test("Close tabs works", () => {
            const appbar = mountAppbar()

            appbar.vm["displayTabs"]()
            appbar.vm["closeTabs"]()

            expect(appbar.vm["showModuleDetail"]).toBe(false)
        })

        test("Listen badge event on window", () => {
            mountAppbar()

            const badge = {
                module_name: "ModuleTest",
                tab_name: "Tab1",
                counter: 22,
                color: "info"
            }
            const event = new CustomEvent("badge", { detail: badge })
            window.dispatchEvent(event)

            expect(navStore.tabBadges).toEqual([badge])
        })

        test("Change user modal opening", () => {
            const appbar = mountAppbar()

            appbar.vm["showChangeUserModal"]()

            expect(appbar.vm["changeUserDialog"]).toBe(true)
        })

        test("CGU modal opening", async () => {
            const spyModal = jest.spyOn(OxGUIManager, "loadGUIEntrypointIframe")
            const appbar = mountAppbar()

            appbar.vm["displayCguModal"]()
            await appbar.vm.$nextTick()

            expect(spyModal).toHaveBeenCalledWith("/ps_cgu", expect.anything())
            expect(appbar.vm["showCguModal"]).toBe(true)
        })

        test("Validate user change", () => {
            const appbar = mountAppbar()

            // @TODO: Create utils to mock href ?
            const hrefSpy = jest.fn()
            const origin = window.location.origin
            // @ts-ignore
            delete window.location
            // @ts-ignore
            window.location = {}
            Object.defineProperty(window.location, "href", {
                set: hrefSpy,
                get: () => "baseHref"
            })
            Object.defineProperty(window.location, "origin", {
                set: hrefSpy,
                get: () => origin
            })
            const userSelected = new User()
            userSelected.username = "usertest"
            appbar.vm["changeUser"] = userSelected
            appbar.vm["validateChangeUser"]()

            expect(hrefSpy).toBeCalledWith(expect.stringContaining("switch?_switch_user=usertest"))
        })

        test("Validate password change", () => {
            const spyAddInfo = jest.spyOn(OxNotifyManager, "addInfo")
            const appbar = mountAppbar()

            appbar.vm["validateChangePassword"]()

            expect(spyAddInfo).toBeCalledWith("common-msg-Password updated")
            expect(appbar.vm["showChangePasswordModal"]).toBe(false)
        })

        test("Show change password modale", async () => {
            const spyModal = jest.spyOn(OxGUIManager, "loadGUIEntrypointIframe")
            const appbar = mountAppbar()

            appbar.vm["changePassword"]()
            await appbar.vm.$nextTick()

            expect(spyModal).toHaveBeenCalledWith("/change_password", expect.anything())
            expect(appbar.vm["showChangePasswordModal"]).toBe(true)
        })

        test("Display about modale", async () => {
            const spyModal = jest.spyOn(OxGUIManager, "loadGUIEntrypointIframe")
            const appbar = mountAppbar()

            appbar.vm["displayAboutModal"]()
            await appbar.vm.$nextTick()

            expect(spyModal).toHaveBeenCalledWith("/about", expect.anything())
            expect(appbar.vm["showAboutModal"]).toBe(true)
        })

        test("Initialization with notifications", () => {
            const spyAddInfo = jest.spyOn(OxNotifyManager, "addInfo")
            const spyAddError = jest.spyOn(OxNotifyManager, "addError")
            mountAppbar({
                flashesNotify: {
                    error: ["Error1", "Error2"],
                    info: ["Info1", "Info2", "Info3"]
                }
            })

            expect(spyAddInfo).toHaveBeenNthCalledWith(1, "Info1")
            expect(spyAddInfo).toHaveBeenNthCalledWith(2, "Info2")
            expect(spyAddInfo).toHaveBeenNthCalledWith(3, "Info3")
            expect(spyAddError).toHaveBeenNthCalledWith(1, "Error1")
            expect(spyAddError).toHaveBeenNthCalledWith(2, "Error2")
        })
    }
)
