/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import PlaceholderLine from "@/components/Appbar/PlaceholderLine/PlaceholderLine.vue"
import { Placeholder } from "@/components/Appbar/types/AppbarTypes"
import { shallowMount } from "@vue/test-utils"

/* eslint-disable dot-notation */

const placeholder: Placeholder = {
    _id: "2",
    label: "Acces au porte documents",
    icon: "folderOpen",
    disabled: false,
    action: "funcTest",
    action_args: ["testArg1", "testArg2"],
    init_action: "",
    counter: "1"
}

describe("PlaceholderLine", () => {
    test("Call action on click", () => {
        const c = shallowMount(PlaceholderLine, {
            propsData: {
                placeholder
            },
            stubs: {
                PlaceholderShortcut: true
            }
        })
        const mockedFunc = jest.fn((x, y) => (x + y))
        // @ts-ignore
        window.funcTest = mockedFunc
        c.vm["click"]()

        expect(mockedFunc).toBeCalled()
        expect(mockedFunc.mock.results[0].value).toEqual("testArg1testArg2")
    })
})
