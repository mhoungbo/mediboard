/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import UserAccount from "@/components/Appbar/UserAccount/UserAccount.vue"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import OxTranslator from "@/core/plugins/OxTranslator"
import oxApiService from "@/core/utils/OxApiService"
import { setActivePinia } from "pinia"
import oxPiniaCore from "@/core/plugins/OxPiniaCore"
import { useUserStore } from "@/core/stores/user"
import { useContextStore } from "@/core/stores/context"
import Group from "@modules/dPetablissement/vue/models/Group"
import { functions } from "@/tests/mocks/GroupSelectorMocks"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import Preferences from "@modules/system/vue/models/Preferences"
import Configurations from "@modules/system/vue/models/Configurations"
import { cloneObject } from "@/core/utils/OxObjectTools"
import { OxUrlBuilder } from "@/core/utils/OxUrlTools"

/* eslint-disable dot-notation */

jest.spyOn(oxApiService, "post").mockImplementation(() => Promise.resolve({}))

const localVue = createLocalVue()
localVue.use(OxTranslator)

let userStore
let contextStore

const user = new Mediuser()
user.canChangePassword = true
user.color = "CCCCFF"
user.id = "985"
user.initials = "UT"
user.firstName = "User"
user.lastName = "Test"
user.username = "utest"
user.isPatient = false
user.exitImpersonationUrl = "/exit_impersonation"
user.editInfosUrl = "/infos"

const preferences = new Preferences()
preferences.attributes.is_dark = false

const configurations = new Configurations()
configurations.attributes.is_distinct_groups = false

const mountComponent = (props?: object) => {
    const defaultProps = {
        infoMaj: {
            release_title: "M�j il y a 5 semaines",
            title: "\nMise � jour le 22/07/2023 05:30:00\nR�vision : 2277"
        },
        groupSelected: new Group(),
        functions
    }

    return shallowMount(UserAccount, {
        propsData: { ...defaultProps, ...props },
        stubs: {
            GroupSelector: true
        },
        localVue
    })
}

describe("UserAccount", () => {
    beforeAll(() => {
        setActivePinia(oxPiniaCore)
        userStore = useUserStore()
        contextStore = useContextStore()
        userStore.preferences = preferences
        contextStore.configurations = configurations
    })
    beforeEach(() => {
        userStore.user = user
    })

    test("Show password if user can change", () => {
        const c = mountComponent()
        expect(c.vm["showPassword"]).toBe(true)
    })

    test("Show password if user is a patient", () => {
        const _user = cloneObject(user)
        _user.canChangePassword = false
        _user.isPatient = true
        userStore.user = _user

        const c = mountComponent()

        expect(c.vm["showPassword"]).toBe(true)
    })

    test("Hide password if not patient and can't change", () => {
        const _user = cloneObject(user)
        _user.canChangePassword = false
        userStore.user = _user

        const c = mountComponent()

        expect(c.vm["showPassword"]).toBe(false)
    })

    test("Hide password if impersonated", () => {
        const _user = cloneObject(user)
        _user.impersonator = { firstname: "Test", lastname: "User2" }
        userStore.user = _user

        const c = mountComponent()

        expect(c.vm["showPassword"]).toBe(false)
    })

    test("Show update infos if they exist", () => {
        const c = mountComponent()
        expect(c.vm["showMajInfo"]).toBe(true)
    })

    test("Hide update infos if they don't exist", () => {
        const c = mountComponent({ infoMaj: { release_title: "", title: "" } })
        expect(c.vm["showMajInfo"]).toBe(false)
    })

    test("Emits input with false payload on click outside", async () => {
        const c = mountComponent()

        c.vm["clickOutside"]()
        await c.vm.$nextTick()

        expect(c.emitted("input")).toEqual([[false]])
    })

    test("Emits changeuser event on switchUser click", async () => {
        const c = mountComponent()

        c.vm["switchUser"]()
        await c.vm.$nextTick()

        expect(c.emitted("changeuser")).toBeTruthy()
    })

    test("Activate dark theme", async () => {
        const c = mountComponent()
        const mockedFunc = jest.fn((x, y, z) => [x, y, z])
        // @ts-ignore
        window.App = {}
        // @ts-ignore
        window.App.savePref = mockedFunc

        c.vm["switchTheme"](true)
        await c.vm.$nextTick()

        expect(mockedFunc).toBeCalled()
        expect(mockedFunc.mock.results[0].value[0]).toEqual("mediboard_ext_dark")
        expect(mockedFunc.mock.results[0].value[1]).toEqual("1")
        expect(mockedFunc.mock.results[0].value[2]).toBeInstanceOf(Function)
    })

    test("Deactivate dark theme", async () => {
        const c = mountComponent()
        const mockedFunc = jest.fn((x, y, z) => [x, y, z])
        // @ts-ignore
        window.App = {}
        // @ts-ignore
        window.App.savePref = mockedFunc

        c.vm["switchTheme"](false)
        await c.vm.$nextTick()

        expect(mockedFunc).toBeCalled()
        expect(mockedFunc.mock.results[0].value[0]).toEqual("mediboard_ext_dark")
        expect(mockedFunc.mock.results[0].value[1]).toEqual("0")
        expect(mockedFunc.mock.results[0].value[2]).toBeInstanceOf(Function)
    })

    test("Emits changepassword event on changePassword click", async () => {
        const c = mountComponent()

        c.vm["changePassword"]()
        await c.vm.$nextTick()

        expect(c.emitted("changepassword")).toBeTruthy()
    })

    test("Emits showcgu event on showCGU click", async () => {
        const c = mountComponent()

        c.vm["showCGU"]()
        await c.vm.$nextTick()

        expect(c.emitted("showcgu")).toBeTruthy()
    })

    test("Exit impersonation", () => {
        const a = new OxUrlBuilder(user.exitImpersonationUrl)
        const hrefSpy = jest.fn()
        const origin = window.location.origin
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            set: hrefSpy,
            get: () => "baseHref"
        })
        Object.defineProperty(window.location, "origin", {
            set: hrefSpy,
            get: () => origin
        })

        const c = mountComponent()
        c.vm["exitImpersonation"]()

        expect(hrefSpy).toBeCalledWith("exit_impersonation")
    })

    test("Logout", () => {
        // @TODO: Create utils to mock href ?
        const hrefSpy = jest.fn()
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            set: hrefSpy,
            get: () => "baseHref"
        })

        const c = mountComponent()
        c.vm["logout"]()

        expect(hrefSpy).toBeCalledWith(expect.stringContaining("/logout"))
    })

    test("Get impersonator initials when not defined", () => {
        const c = mountComponent()
        expect(c.vm["userInitials"]).toBe("")
    })

    test("Get impersonator initials", () => {
        const _user = cloneObject(user)
        _user.impersonator = { firstname: "Test", lastname: "User2" }
        userStore.user = _user

        const c = mountComponent()

        expect(c.vm["userInitials"]).toBe("TU")
    })

    test("Detects substitution", () => {
        const _user = cloneObject(user)
        _user.impersonator = { firstname: "Test", lastname: "User2" }
        userStore.user = _user

        const c = mountComponent()

        expect(c.vm["isSubstitution"]).toBe(true)
    })

    test("Detects non substitution", () => {
        const c = mountComponent()

        expect(c.vm["isSubstitution"]).toBe(false)
    })

    test("Emits showabout event on showAbout click", async () => {
        const c = mountComponent()

        c.vm["showAbout"]()
        await c.vm.$nextTick()

        expect(c.emitted("showabout")).toBeTruthy()
    })

    test("Hide password reminder", () => {
        const c = mountComponent()
        expect(c.vm["showPasswordReminder"]).toBe(false)
    })

    test("Show password reminder", () => {
        const _user = cloneObject(user)
        _user.attributes.psw_remaining_days = "3"
        userStore.user = _user

        const c = mountComponent()

        expect(c.vm["showPasswordReminder"]).toBe(true)
    })

    test("Close password reminder", () => {
        const _user = cloneObject(user)
        _user.dismissReminderUrl = "/dissmis_url"
        _user.attributes.psw_remaining_days = "3"
        userStore.user = _user

        const c = mountComponent()
        c.vm["closeReminder"]()

        expect(oxApiService.post).toBeCalledWith(expect.stringContaining("/dissmis_url"))
        expect(userStore.user.passwordRemainingDays).toBeUndefined()
    })
})
