/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import GroupRadio from "@/components/Appbar/GroupRadio/GroupRadio.vue"
import { shallowMount } from "@vue/test-utils"
import Group from "@modules/dPetablissement/vue/models/Group"

/* eslint-disable dot-notation */

const group = new Group()
group.id = "1"
group.name = "Group test"
group.isMain = false
group.isSecondary = false

describe("GroupRadio", () => {
    test("Has proper class when active", () => {
        const groupRadio = shallowMount(GroupRadio, {
            propsData: {
                group,
                functionName: false,
                actived: true
            }
        })

        expect(groupRadio.vm["radioClass"]).toBe("active")
    })

    test("Has no class when inactive", () => {
        const groupRadio = shallowMount(GroupRadio, {
            propsData: {
                group,
                functionName: false,
                actived: false
            }
        })

        expect(groupRadio.vm["radioClass"]).toBe("")
    })

    test("Emits click when clicked", async () => {
        const groupRadio = shallowMount(GroupRadio, {
            propsData: {
                group,
                functionName: false,
                actived: false
            }
        })
        groupRadio.vm["selectGroup"]()
        await groupRadio.vm.$nextTick()

        expect(groupRadio.emitted("click")).toBeTruthy()
        expect(groupRadio.emitted("click")).toHaveLength(1)
        expect(groupRadio.emitted("click")![0]).toEqual([group.id])
    })
})
