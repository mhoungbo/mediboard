/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { functions, groups, groupSelected } from "@/tests/mocks/GroupSelectorMocks"
import Group from "@modules/dPetablissement/vue/models/Group"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import GroupSelector from "@/components/Appbar/GroupSelector/GroupSelector.vue"
import OxTranslator from "@/core/plugins/OxTranslator"

const localVue = createLocalVue()
localVue.use(OxTranslator)

/* eslint-disable dot-notation */

describe("GroupSelector", () => {
    test("Show search field when there are more than six groups", () => {
        const allGroups = [...groups]
        const group4 = new Group()
        group4.id = "4"
        group4.name = "Group 4"
        group4.isMain = false
        group4.isSecondary = false
        const group5 = new Group()
        group5.id = "5"
        group5.name = "Group 5"
        group5.isMain = false
        group5.isSecondary = false
        const group6 = new Group()
        group6.id = "6"
        group6.name = "Group 6"
        group6.isMain = false
        group6.isSecondary = false
        const group7 = new Group()
        group7.id = "7"
        group7.name = "Group 7"
        group7.isMain = false
        group7.isSecondary = false
        allGroups.push(group4, group5, group6, group7)
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: allGroups
            },
            localVue
        })

        expect(groupSelector.vm["useSearchField"]).toBe(true)
    })

    test("Hide search field when there are less than seven groups", () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["useSearchField"]).toBe(false)
    })

    test("Display current group if no groups are given", () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: []
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["showCurrentGroup"]).toBe(true)
    })

    test("Emits input with payload false on click outside", async () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        groupSelector.vm["clickOutside"]()
        await groupSelector.vm.$nextTick()

        expect(groupSelector.emitted("input")).toEqual([[false]])
    })

    test("Detect selected group", () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["isSelected"](groupSelected)).toBe(true)
    })

    test("Detect unselected group", () => {
        const group = new Group()
        group.id = "22"
        group.name = "AnotherGroup"
        group.isMain = false
        group.isSecondary = false
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["isSelected"](group)).toBe(false)
    })

    test("Get existent function name", () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["getFunctionName"](groups[2])).toBe("Function 2")
    })

    test("Get non existent function name return false", () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["getFunctionName"](groups[1])).toBe(false)
    })

    test("Search field works", async () => {
        const allGroups = [...groups]
        const group4 = new Group()
        group4.id = "4"
        group4.name = "Group 4"
        group4.isMain = false
        group4.isSecondary = false
        const group5 = new Group()
        group5.id = "5"
        group5.name = "Group 5"
        group5.isMain = false
        group5.isSecondary = false
        const group6 = new Group()
        group6.id = "6"
        group6.name = "Group 6"
        group6.isMain = false
        group6.isSecondary = false
        const group7 = new Group()
        group7.id = "7"
        group7.name = "Group 7"
        group7.isMain = false
        group7.isSecondary = false
        allGroups.push(group4, group5, group6, group7)
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: allGroups
            },
            localVue,
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        groupSelector.vm["filter"] = "oup 4"
        await groupSelector.vm.$nextTick()
        const result = groupSelector.vm["filtredGroups"]

        expect(result).toEqual([group4])
    })

    test("Reset search field works", async () => {
        const allGroups = [...groups]
        const group4 = new Group()
        group4.id = "4"
        group4.name = "Group 4"
        group4.isMain = false
        group4.isSecondary = false
        const group5 = new Group()
        group5.id = "5"
        group5.name = "Group 5"
        group5.isMain = false
        group5.isSecondary = false
        const group6 = new Group()
        group6.id = "6"
        group6.name = "Group 6"
        group6.isMain = false
        group6.isSecondary = false
        const group7 = new Group()
        group7.id = "7"
        group7.name = "Group 7"
        group7.isMain = false
        group7.isSecondary = false
        allGroups.push(group4, group5, group6, group7)
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: allGroups
            },
            localVue,
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        groupSelector.vm["filter"] = "oup 4"
        await groupSelector.vm.$nextTick()
        groupSelector.vm["resetFilter"]()
        const result = groupSelector.vm["filtredGroups"]

        expect(result).toHaveLength(7)
    })

    test("Get function by group", () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["getFunctionByGroupId"]("1")).toEqual(functions[0])
    })

    test("Display GroupRadio when specified", () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups,
                showRadio: true
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["groupLineOrGroupRadioComponent"]).toBe("group-radio")
    })

    test("Display GroupLine by default", () => {
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })

        expect(groupSelector.vm["groupLineOrGroupRadioComponent"]).toBe("group-line")
    })

    test("Select group change href", () => {
        const hrefSpy = jest.fn()
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            get: () => "http://localhost/mediboard",
            set: hrefSpy
        })
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })
        groupSelector.vm["selectGroup"]("2")

        expect(hrefSpy).toBeCalledWith("http://localhost/mediboard?g=2")
    })

    test("Select group without function reset function param in href", () => {
        const hrefSpy = jest.fn()
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            get: () => "http://localhost/mediboard?f=22",
            set: hrefSpy
        })
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })
        groupSelector.vm["selectGroup"]("2")

        expect(hrefSpy).toBeCalledWith("http://localhost/mediboard?g=2")
    })

    test("Select group with function change href", () => {
        const hrefSpy = jest.fn()
        // @ts-ignore
        delete window.location
        // @ts-ignore
        window.location = {}
        Object.defineProperty(window.location, "href", {
            get: () => "http://localhost/mediboard",
            set: hrefSpy
        })
        const groupSelector = shallowMount(GroupSelector, {
            propsData: {
                groupSelected,
                functions,
                groupsData: groups
            },
            stubs: {
                GroupLine: true,
                GroupRadio: true
            }
        })
        groupSelector.vm["selectGroup"]("3")

        expect(hrefSpy).toBeCalledWith("http://localhost/mediboard?g=3&f=2")
    })
})
