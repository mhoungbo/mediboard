/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import oxApiService from "@/core/utils/OxApiService"
import { updateModulesDefaultTabs } from "@/components/Appbar/utils/AppbarUtils"
import { getKeyCache } from "@/components/Appbar/utils/DefaultCache"

describe("AppbarUtils", () => {
    beforeAll(() => {
        jest.spyOn(oxApiService, "get").mockImplementation(() => {
            return new Promise((resolve) => {
                resolve({
                    data: {
                        data: {
                            type: "default_tabs",
                            id: "40cd750bba9870f18aada2478b24840a",
                            attributes: [],
                            links: {
                                module1: "/module/1",
                                module2: "?m=module2&tab=2",
                                module3: "/module/3"
                            }
                        }
                    }
                })
            })
        })
    })

    test("update module default tabs in localStorage", async () => {
        await updateModulesDefaultTabs("username", "url")
        expect(localStorage[getKeyCache("username")]).toEqual("{\"module1\":\"/module/1\",\"module2\":\"?m=module2&tab=2\",\"module3\":\"/module/3\"}")
    })
})
