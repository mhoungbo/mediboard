/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import Tab from "@modules/system/vue/models/Tab"
import { createLocalVue, shallowMount } from "@vue/test-utils"
import TabShortcut from "@/components/Appbar/TabShortcut/TabShortcut.vue"
import { cloneObject } from "@/core/utils/OxObjectTools"
import OxTranslator from "@/core/plugins/OxTranslator"

/* eslint-disable dot-notation */

const localVue = createLocalVue()
localVue.use(OxTranslator)
const tab = new Tab()
tab.name = "TabTest"
tab.moduleName = "ModuleTest"
tab.url = "tab-url"

const mountComponent = (props?: object) => {
    const defaultProps = {
        tab
    }

    return shallowMount(TabShortcut, {
        propsData: { ...defaultProps, ...props },
        localVue
    })
}

describe("TabShortcut", () => {
    test("Get module name", () => {
        const c = mountComponent()
        expect(c.vm["moduleName"]).toBe("module-ModuleTest-court")
    })

    test("Get classic tab name", () => {
        const c = mountComponent()
        expect(c.vm["tabName"]).toBe("mod-ModuleTest-tab-TabTest")
    })

    test("Get param tab name", () => {
        const paramTab = cloneObject(tab)
        paramTab.isParam = true
        const c = mountComponent({ tab: paramTab })
        expect(c.vm["tabName"]).toBe("settings")
    })

    test("get tab link", () => {
        const c = mountComponent()
        expect(c.vm["tabLink"]).toBe("tab-url")
    })
})
