import {
    OxEntryPointCurrentFunction,
    OxEntryPointCurrentGroup,
    OxEntryPointCurrentUser,
    OxEntryPointLocales
} from "@/core/types/OxEntryPointTypes"

declare global {
    interface Window {
        guiLocales?: OxEntryPointLocales
        __core?: {
            env?: string
            mounted: boolean,
            sessionHash?: string,
            currentUser?: OxEntryPointCurrentUser
            currentFunction?: OxEntryPointCurrentFunction
            currentGroup?: OxEntryPointCurrentGroup
        }
    }
}
