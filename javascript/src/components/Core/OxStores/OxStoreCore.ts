/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import Vue from "vue"
import Vuex from "vuex"
import { Notification } from "@/components/Core/OxNotify/OxNotifyModel"
import { Alert } from "@/components/Core/OxAlert/OxAlertModel"
// import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        /**
         * Main parameters
         */
        rootUrl: "",
        baseUrl: "",
        loadings: ([] as string[]),

        /**
         * Global configurations
         */
        configurations: {},
        configuration_promises: [],
        group_configurations: {},
        group_configuration_promises: [],
        preferences: {},

        /**
         * Field specifications
         */
        specs: [],
        saved_specs: ([] as string[]),

        /**
         * Api cache
         */
        /* eslint-disable  @typescript-eslint/no-explicit-any */
        api_cache: ([] as any[]),

        /**
         * Notifications
         */
        notifications: [] as Notification[],

        /**
         * Alerts
         */
        alert: null as Alert|null
    },
    getters: {
        /**
         * Main parameters
         */
        url: (state) => {
            return state.baseUrl
        },
        rooturl: (state) => {
            return state.rootUrl
        },
        loading: (state) => {
            return state.loadings.length > 0
        },

        /**
         * Global configurations
         */
        conf: (state) => (conf) => {
            if (typeof (state.configurations[conf]) !== "undefined") {
                return state.configurations[conf]
            }
            return "undefined"
        },
        gconf: (state) => (gconf) => {
            if (typeof (state.group_configurations[gconf]) !== "undefined") {
                return state.group_configurations[gconf]
            }
            return "undefined"
        },
        pref: (state) => (label) => {
            return typeof (state.preferences[label]) !== "undefined" ? state.preferences[label] : false
        },

        hasConfigurationPromise: (state) => (conf) => {
            return typeof (state.configuration_promises[conf]) !== "undefined"
        },
        configurationPromise: (state) => (conf) => {
            return typeof (state.configuration_promises[conf]) === "undefined" ? false : state.configuration_promises[conf]
        },
        hasGroupConfigurationPromise: (state) => (conf) => {
            return typeof (state.group_configuration_promises[conf]) !== "undefined"
        },
        groupConfigurationPromise: (state) => (conf) => {
            return typeof (state.group_configuration_promises[conf]) === "undefined" ? false : state.group_configuration_promises[conf]
        },

        /**
         * Field specifications
         */
        spec: (state) => (objectType, objectField) => {
            return state.specs[objectType] && state.specs[objectType][objectField] ? state.specs[objectType][objectField] : false
        },
        // hasSpecByType: (state) => (objectType) => { return objectType },
        hasSpecByFieldset: (state) => (objectType, objectFieldset) => {
            if (state.saved_specs.indexOf(objectType + "-" + objectFieldset) === -1) {
                return false
            }
            return true
        },
        hasSpecByFieldsets: (state) => (objectType, objectFieldsets) => {
            for (let i = 0; i < objectFieldsets.length; i++) {
                if (state.saved_specs.indexOf(objectType + "-" + objectFieldsets[i]) === -1) {
                    return false
                }
            }
            return true
        },

        /**
         * API Cache
         */
        getApiCache: (state) => (key) => {
            if (typeof (state.api_cache[key]) === "undefined") {
                return false
            }
            return state.api_cache[key]
        },

        /**
         * Notifications
         */
        getNotifications: (state) => {
            return state.notifications
        },

        /**
         * Alert
         */
        getAlert: (state) => {
            return state.alert
        }
    },
    mutations: {
        /**
         * Main parameters
         */
        setBaseUrl: (state, baseUrl: string) => {
            state.baseUrl = baseUrl
        },
        setRootUrl: (state, rootUrl: string) => {
            state.rootUrl = rootUrl
        },
        addLoading: (state) => {
            state.loadings.push("_")
        },
        removeLoading: (state) => {
            state.loadings.shift()
        },
        resetLoading: (state) => {
            state.loadings = []
        },
        /**
         * Global configurations
         */
        /* eslint-disable  @typescript-eslint/no-explicit-any */
        setConfiguration: (state, conf: { name: string; value: any }) => {
            state.configurations[conf.name] = conf.value
        },
        /* eslint-disable  @typescript-eslint/no-explicit-any */
        setGroupConfiguration: (state, conf: { name: string; value: any }) => {
            state.group_configurations[conf.name] = conf.value
        },
        setPreference: (state, pref: { name: string; value: string }) => {
            state.preferences[pref.name] = pref.value
        },
        setPreferences: (state, prefs: {name: string; value: string}[]) => {
            state.preferences = prefs
        },
        setConfigurationPromise: (state, params: { conf: string; promise: Promise<string> }) => {
            if (typeof (state.configuration_promises[params.conf]) !== "undefined") {
                return
            }
            state.configuration_promises[params.conf] = params.promise
        },
        removeConfigurationPromise: (state, conf: string) => {
            if (typeof (state.configuration_promises[conf]) === "undefined") {
                return
            }
            delete (state.configuration_promises[conf])
        },
        setGroupConfigurationPromise: (state, params: { conf: string; promise: Promise<string> }) => {
            if (typeof (state.group_configuration_promises[params.conf]) !== "undefined") {
                return
            }
            state.group_configuration_promises[params.conf] = params.promise
        },
        removeGroupConfigurationPromise: (state, conf: string) => {
            if (typeof (state.group_configuration_promises[conf]) === "undefined") {
                return
            }
            delete (state.group_configuration_promises[conf])
        },

        /**
         * Field specifications
         */
        setSpec: (state, object: { type: string; specs: any; fieldsets: string[] }) => {
            if (!state.specs[object.type]) {
                state.specs[object.type] = object.specs
            }
            else {
                Object.assign(state.specs[object.type], object.specs)
            }
            object.fieldsets.forEach(
                (_fieldset) => {
                    state.saved_specs.push(object.type + "-" + _fieldset)
                }
            )
        },

        /**
         * Api Cache
         */
        setApiCache: (state, cache: { key: string; value: any }) => {
            state.api_cache[cache.key] = cache.value
        },

        /**
         * Notifications
         */
        addNotification: (state, notification: Notification) => {
            state.notifications.push(notification)
        },
        removeNotification: (state, key: number) => {
            const notificationIndex = state.notifications.findIndex(notification => notification.key === key)
            if (notificationIndex !== -1) {
                state.notifications.splice(notificationIndex, 1)
            }
        },
        callbackDoneNotification: (state, key: number) => {
            const notificationIndex = state.notifications.findIndex(notification => notification.key === key)
            if (notificationIndex !== -1) {
                state.notifications[notificationIndex].callbackDone = true
            }
        },
        removeAllNotifications: (state) => {
            state.notifications = []
        },
        /**
         * Alert
         */
        setAlert: (state, alert: Alert) => {
            state.alert = alert
        },
        unsetAlert: (state) => {
            state.alert = null
        }
    }
    // plugins: [ vuexSession.plugin ]
})
