/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Call JS function available on window
 * @param functionName - Function's name to execute (use dot for hierarchy)
 * @param args - Function args
 */
export const callJSFunction = (functionName: string, args: Array<any> = []) => {
    const contexts = functionName.split(".")
    let func = window
    for (let i = 0; i < contexts.length; i++) {
        func = func[contexts[i]]
    }
    if (typeof func !== "function") {
        throw new Error(`Function ${functionName} is not define on window`)
    }
    if (args) {
        // @ts-ignore
        func(...args)
    }
    else {
        // @ts-ignore
        func()
    }
}

/* eslint-enable  */
