/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

export interface InfoMaj {
    title: string
    release_title: string
}

export interface Placeholder {
    _id: string
    action: string
    action_args: Array<string>
    counter: string
    icon: string | number
    init_action: string
    label: string
    disabled?: boolean
}

export interface MenuLink {
    title: string
    href: string | null
    action: string | null
}

export interface MenuSection {
    title: string
    links: Array<MenuLink>
}

export interface TabBadgeModel {
    module_name: string
    tab_name: string,
    counter: number,
    color: string
}

export interface FlashMessages {
    error: Array<string>
    info: Array<string>
}

export interface AppbarProp {
    datas: {
        [key: string]: any
    },
    links?: {
        [key: string]: string
    }
}
