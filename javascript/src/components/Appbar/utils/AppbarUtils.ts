/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { getObjectFromJsonApiRequest } from "@/core/utils/OxApiManager"
import OxObject from "@/core/models/OxObject"
import { setUserDefaultTabs } from "@/components/Appbar/utils/DefaultCache"

/**
 * Update module default tabs cache entries for user
 * @param {string} sessionHash - Active user's session hash
 * @param {string} url - Url to call to get module default tabs
 */
export async function updateModulesDefaultTabs (sessionHash: string, url: string) {
    const result = await getObjectFromJsonApiRequest(OxObject, url)
    setUserDefaultTabs(sessionHash, (result.links as { [key: string]: string }))
}
