/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

type DefaultTabs = {
    [key: string]: string
}

/**
 * Functions to use cache for module default tabs management
 */

export function getTabForModule (sessionHash: string, module: string): string | undefined {
    const defaultTabs = getUserDefaultTabs(sessionHash)
    return defaultTabs?.[module] ?? undefined
}

export function setTabForModule (sessionHash: string, module: string, url: string) {
    let defaultTabs = getUserDefaultTabs(sessionHash)
    if (defaultTabs === null) {
        defaultTabs = { [module]: url }
    }
    else {
        defaultTabs[module] = url
    }
    setUserDefaultTabs(sessionHash, defaultTabs)
}

export function getUserDefaultTabs (sessionHash: string): DefaultTabs | null {
    const key = getKeyCache(sessionHash)
    const defaultTabs = localStorage.getItem(key)
    if (defaultTabs) {
        return JSON.parse(defaultTabs)
    }
    else {
        // If first access to this cache, remove old caches
        removeOldKeys()
        return null
    }
}

export function setUserDefaultTabs (sessionHash: string, defaultTabs: DefaultTabs) {
    const key = getKeyCache(sessionHash)
    localStorage.setItem(key, JSON.stringify(defaultTabs))
}

export function removeTabForModule (sessionHash: string, module: string) {
    const defaultTabs = getUserDefaultTabs(sessionHash)
    if (defaultTabs) {
        delete defaultTabs[module]
        setUserDefaultTabs(sessionHash, defaultTabs)
    }
}

export function getKeyCache (sessionHash: string): string {
    return `defaultTabs-${sessionHash}`
}

function removeOldKeys () {
    Object.keys(localStorage)
        .filter(x => x.startsWith("defaultTabs-"))
        .forEach(x => localStorage.removeItem(x))
}
