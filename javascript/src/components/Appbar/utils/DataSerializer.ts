/**
 * @package Openxtrem\Core
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { AppbarProp } from "@/components/Appbar/types/AppbarTypes"
import Group from "@modules/dPetablissement/vue/models/Group"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import Function from "@modules/mediusers/vue/Models/Function"
import Tab from "@modules/system/vue/models/Tab"
import Module from "@/core/models/Module"

/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export function appbarPropTransformer (prop: AppbarProp): { [key: string]: any, _links: any } {
    return { ...prop.datas, _links: prop.links }
}

export function createGroupFromAppbarProp (prop: AppbarProp): Group {
    const group = new Group()
    group.id = prop.datas._id
    group.name = prop.datas.text
    group.groupsUrl = prop.links?.groups

    return group
}

export function createMediuserFromAppbarProp (prop: AppbarProp): Mediuser {
    const mediuser = new Mediuser()
    mediuser.canChangePassword = prop.datas._can_change_password
    mediuser.color = prop.datas._color
    mediuser.fontColor = prop.datas._font_color
    mediuser.initials = prop.datas._initial
    mediuser.isAdmin = prop.datas._is_admin
    mediuser.isPatient = prop.datas._is_patient
    mediuser.firstName = prop.datas._user_first_name
    mediuser.lastName = prop.datas._user_last_name
    mediuser.username = prop.datas._user_username
    mediuser.impersonator = prop.datas.impersonator
    mediuser.changePasswordUrl = prop.links?.change_password
    mediuser.defaultUrl = prop.links?.default
    mediuser.editInfosUrl = prop.links?.edit_infos
    mediuser.impersonationUrl = prop.links?.impersonation
    mediuser.exitImpersonationUrl = prop.links?.exit_impersonation
    mediuser.listUrl = prop.links?.list

    return mediuser
}

export function createFunctionFromAppbarProp (prop: AppbarProp): Function {
    const userFunction = new Function()
    userFunction.id = prop.datas._id
    userFunction.groupId = prop.datas.group_id
    userFunction.isMain = prop.datas.is_main
    userFunction.name = prop.datas.text

    return userFunction
}

export function createModuleFromAppbarProp (prop: AppbarProp): Module {
    const module = new Module()
    module.name = prop.datas.mod_name
    module.category = prop.datas.mod_category
    module.visible = prop.datas.mod_ui_active === "1"
    module.tabsUrl = prop.links?.tabs
    module.url = prop.links?.default_tab

    return module
}

export function createTabFromAppbarProp (prop: AppbarProp): Tab {
    const tab = new Tab()
    tab.name = prop.datas.tab_name
    tab.moduleName = prop.datas.mod_name
    tab.isConfig = prop.datas.is_config
    tab.isParam = prop.datas.is_param
    tab.isStandard = prop.datas.is_standard
    tab.pinnedOrder = prop.datas.pinned_order
    tab.url = prop.links?.tab_url

    return tab
}
