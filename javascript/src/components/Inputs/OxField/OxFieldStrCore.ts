/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { Component, Prop } from "vue-property-decorator"
import OxIconCore from "@oxify/components/OxIcon/OxIconCore"
import OxFieldCore from "@/components/Inputs/OxField/OxFieldCore"

@Component
export default class OxFieldStrCore extends OxFieldCore {
    @Prop({ default: undefined })
    protected counter!: boolean | number

    @Prop({ default: undefined })
    protected maxlength!: number

    /**
     * Icon name to display
     */
    protected get iconName (): string {
        return OxIconCore.get(this.icon)
    }
}
