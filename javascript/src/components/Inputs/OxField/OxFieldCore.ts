/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { Component, Prop, Vue, Watch } from "vue-property-decorator"
import { OxSelectElement } from "@oxify/types/OxFieldTypes"
import OxFormValidator from "@oxify/components/OxForm/OxFormValidator"

export const MANDATORY_SYMBOL = "*"

/**
 * OxField
 *
 * Field model component
 */
@Component
export default class OxFieldCore extends Vue {
    // Field label
    @Prop({ default: "" })
    protected label!: string

    // TODO: Set default value as null for mutatedValue remove ref
    @Prop({ default: "" })
    protected value!: string | boolean

    @Prop({ default: false })
    protected disabled!: boolean

    @Prop({ default: false })
    protected dense!: boolean

    @Prop({ default: false })
    protected notNull!: boolean

    @Prop({
        default: () => {
            return []
        }
    })
    protected list!: OxSelectElement[] | string[]

    @Prop({ default: "" })
    protected message!: string

    @Prop({ default: "" })
    protected icon!: string

    @Prop({ default: false })
    protected onPrimary!: boolean

    @Prop({ default: false })
    protected showLoading!: boolean

    @Prop({ default: undefined })
    private placeholder!: string

    @Prop({
        default: () => {
            return []
        }
    })
    protected rules!: Array<Function>

    @Prop({ default: undefined })
    protected name!: string

    @Prop()
    protected validator!: OxFormValidator

    static MANDATORY_SYMBOL = "*"

    private fieldId!: string

    protected mutatedValue: string | boolean | string[] = ""

    /**
     * Field value synchronization
     */
    @Watch("value")
    protected updateMutatedValue (): void {
        this.mutatedValue = this.value ? this.value.toString() : ""
    }

    /**
     * Initial container base classes
     *
     * @return {Object}
     */
    protected get fieldClasses (): object {
        return {
            labelled: this.label !== "",
            "not-null": this.notNull
        }
    }

    /**
     * Selectable item list
     *
     *
     * @return {Array<OxSelectElement>}
     */
    public get viewList (): OxSelectElement[] {
        return (this.list as OxSelectElement[]).map(
            (_el: OxSelectElement | string) => {
                return typeof (_el) === "string" ? { view: _el, _id: _el } : _el
            }
        )
    }

    /**
     * Field text color
     *
     * @return {string|undefined}
     */
    protected get fieldColor (): string | undefined {
        return this.onPrimary ? "rgba(255, 255, 255, 0.7)" : undefined
    }

    /**
     * Field background color
     *
     * @return {string|undefined}
     */
    protected get fieldBG () : string | undefined {
        return this.onPrimary ? "#5C6BC0" : undefined
    }

    /**
     * Current field information message
     *
     * @return {string}
     */
    protected get hint (): string {
        return this.message.toString()
    }

    /**
     * Field label
     *
     * @return {string}
     */
    protected get labelComputed (): string {
        return this.label + (this.notNull && this.label ? " " + OxFieldCore.MANDATORY_SYMBOL : "")
    }

    /**
     * Component created
     */
    protected created (): void {
        this.fieldId = Math.ceil(Math.random() * Math.pow(10, 10)).toString()
    }

    /**
     * Component mounted
     */
    protected mounted (): void {
        this.updateMutatedValue()
    }

    /**
     * Field value change event
     * @param {string | boolean} value - New value
     */
    protected change (value: string | boolean | string[]): void {
        this.$emit("change", value)
        // Emit input event for double-way binding. TODO ref to remove change event
        this.$emit("input", value)
    }

    /**
     * Changing the current value
     * @param {string | boolean} value - New value
     */
    protected changeValue (value: string | boolean | string[]): void {
        this.mutatedValue = value
        this.change(value)
    }

    /**
     * Generation of rules based on the specs rules and props
     *
     * @return {Array}
     */
    protected get fieldRules (): Array<Function> {
        const rules = this.rules

        if (this.validator) {
            this.validator.setRules(this.name, rules)
            return this.validator.getValidationRules(this.name)
        }

        return rules
    }
}
