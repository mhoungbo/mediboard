/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { Component, Prop } from "vue-property-decorator"
import { VueMaskDirective } from "v-mask"
import OxFieldStrCore from "@/components/Inputs/OxField/OxFieldStrCore"

@Component({
    directives: {
        mask: VueMaskDirective
    }
})
export default class OxTextField extends OxFieldStrCore {
    @Prop({ default: false })
    protected autofocus!: boolean

    @Prop({ default: false })
    private number!: boolean

    @Prop({ default: false })
    private rounded!: boolean

    @Prop()
    private min!: number

    @Prop()
    private max!: number

    @Prop()
    private mask!: string

    private get fieldType (): string {
        return this.number ? "number" : "text"
    }

    /**
     * Mounted component
     */
    protected mounted (): void {
        this.updateMutatedValue()
    }

    /**
     * Public method to manually focus this component
     */
    public bindFocus (): void {
        const inputElement = this.$el.querySelector("input") as HTMLDivElement
        if (inputElement) {
            inputElement.focus()
        }
    }

    /**
     * Blur event
     */
    private blur (): void {
        this.$emit("blur")
    }

    /**
     * Click event
     */
    private click (): void {
        this.$emit("click")
    }

    /**
     * Append icon click event
     */
    private clickAppend (): void {
        this.click()
        this.$emit("click:append")
    }

    /**
     * Focus event
     */
    private focus (): void {
        this.$emit("focus")
    }

    /**
     * Keydown event
     * @param {Event} event - Trigger event
     * @private
     */
    private keydown (event: Event): void {
        this.$emit("keydown", event)
    }
}
