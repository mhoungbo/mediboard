<!--
  @author  SAS OpenXtrem <dev@openxtrem.com>
  @license https://www.gnu.org/licenses/gpl.html GNU General Public License
  @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
-->

<script lang="ts">
import Vue, { PropType } from "vue"
import OxCollection from "@/core/models/OxCollection"
import OxObject from "@/core/models/OxObject"
import { OxUrlBuilder } from "@/core/utils/OxUrlTools"
import { getCollectionFromJsonApiRequest } from "@/core/utils/OxApiManager"
import { DataTableHeader } from "vuetify"
import { OxDatagridColumn } from "@/core/types/OxDatagridTypes"
import { getSchema } from "@/core/utils/OxStorage"
import { cloneDeep, isEmpty } from "lodash"
import OxButton from "@oxify/components/OxButton/OxButton.vue"
import OxChipGroup from "@oxify/components/OxChipGroup/OxChipGroup.vue"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"
import OxTextField from "@oxify/components/OxField/OxTextField/OxTextField.vue"
import { OxChipGroupChoice } from "@oxify/types/OxChipGroupTypes"
import OxThemeCore from "@oxify/utils/OxThemeCore"
import Draggable from "vuedraggable"
import { cloneObject } from "@/core/utils/OxObjectTools"

/**
 * OxDatalist allows to create a datagrid where items order is manipulable with a drag & drop.
 *
 * Consequently, features like pagination, grouping by attribute or sorting are disabled on this component.
 */
export default Vue.extend({
    name: "OxDatalist",
    components: {
        OxButton,
        OxChipGroup,
        OxIcon,
        OxTextField,
        Draggable
    },
    props: {
        columns: {
            type: [] as PropType<OxDatagridColumn[]>,
            required: true
        },
        filters: Array as PropType<OxChipGroupChoice[]>,
        fixedHeaders: {
            type: Boolean,
            default: false
        },
        hideHeader: {
            type: Boolean,
            default: false
        },
        noDataText: String,
        oxObject: Function as PropType<new() => OxObject>,
        searchable: {
            type: Boolean,
            default: false
        },
        searchLabel: String,
        showActions: {
            type: Boolean,
            default: false
        },
        showSelect: {
            type: Boolean,
            default: false
        },
        stripped: {
            type: Boolean,
            default: false
        },
        value: {
            type: Object as PropType<OxCollection<OxObject>>,
            required: true
        }
    },
    data () {
        return {
            currentIndex: null as number | null,
            displaySearchQuery: false,
            drag: false,
            itemsData: [] as OxObject[],
            loading: true,
            objectClass: Function as unknown as new() => OxObject,
            primaryColor: OxThemeCore.primary,
            resourceType: "",
            search: "",
            searchQuery: "",
            selectedFilters: [] as string[],
            selectedItems: [] as OxObject[],
            showMassActions: false,
            totalItems: 0
        }
    },
    computed: {
        /**
         * Returns the CSS classes for datalist table
         *
         * @return {string} The CSS classes
         */
        datalistClasses (): string {
            return this.showMassActions ? "massActionsEnabled OxDatalist-table" : "OxDatalist-table"
        },

        /**
         * Returns the input search label text
         *
         * @return {string} The input search label
         */
        searchLabelText (): string {
            return this.searchLabel ? this.searchLabel : this.$tr("common-search")
        },

        /**
         * Transforms the given prop "columns" in an array interpreted by the v-data-table component from Vuetify
         *   - adds default CSS classes
         *   - removes the "filterValues" property
         *   - defines a default value from the schema store (for columns without "text" property)
         *   - automatically adds the "actions" column if the "showActions" prop = true
         *
         * @return {DataTableHeader[]} An array containing the Datalist's columns description
         */
        headers (): DataTableHeader[] {
            const headers = this.columns.map(
                column => {
                    const rColumn = { ...column }
                    rColumn.class = "OxDatalist-tableHeader"
                    rColumn.cellClass = "OxDatalist-tableCell"
                    rColumn.groupable = false
                    rColumn.sortable = false
                    delete rColumn.filterValues

                    if (!rColumn.text && !isEmpty(this.resourceType)) {
                        /*
                         * Attempting to retrieve the schema from the store for the given resource & field name
                         *   For example :
                         *   - resourceType = "sample_person"
                         *   - rColumn.value = "birthdate"
                         *
                         * We use the replace function to be able to match the store value (the true field name) with
                         * the camel case value set in the "column" prop description
                         *   For example :
                         *   - rColumn.value = "activityStart" (=> SampleMovieSettings.vue)
                         */
                        const attrSchema = getSchema(
                            this.resourceType,
                            rColumn.value.replace(
                                /[A-Z]/g,
                                letter => `_${letter.toLowerCase()}`
                            )
                        )

                        if (attrSchema) {
                            rColumn.text = attrSchema.libelle
                        }
                    }

                    return rColumn
                }
            ) as DataTableHeader[]

            if (this.showActions) {
                headers.push(
                    {
                        cellClass: "OxDatalist-tableCell OxDatalist-tableActions",
                        text: "",
                        value: "actions",
                        sortable: false,
                        groupable: false
                    }
                )
            }

            return headers
        },

        /**
         * Returns the right translation for "selection", by adding plural if number of selected items > 1
         *
         * @return {string} The "selection" translation text
         */
        selectionText (): string {
            return this.$tr("common-selection", this.selectedItems.length > 1)
        },

        /**
         * Returns the right icon name displayed in the search field
         *
         * @return {string} The icon name displayed in the search field
         */
        searchFieldIcon (): string {
            return this.search ? "cancel" : "search"
        },

        /**
         * Returns the right text displayed when the Datalist is empty
         *
         * @return {string} The text displayed when the Datalist is empty
         */
        customableNoDataText (): string {
            return this.noDataText ?? this.$tr("common-No data")
        },

        draggableAnimationName () : string | null {
            return !this.drag ? "flip-list" : null
        }
    },
    watch: {
        itemsData: {
            handler (newVal: OxObject[]) {
                const collection = cloneDeep(this.value)
                collection.objects = newVal
                this.$emit("input", collection)
            }
        }
    },
    mounted () {
        /*
         * https://bugs.chromium.org/p/chromium/issues/detail?id=410328#c5
         * https://stackoverflow.com/questions/17946886/hover-sticks-to-element-on-drag-and-drop
         */
        const tableRows = document.querySelectorAll(".OxDatalist-tableRow")
        for (let i = 0, n = tableRows.length; i < n; i++) {
            const tableRow = tableRows[i]

            tableRow.addEventListener("mouseover", () => {
                tableRow.classList.add("hovered")
            })
            tableRow.addEventListener("mouseout", () => {
                tableRow.classList.remove("hovered")
            })
        }
    },
    created () {
        // Deactivates the default duplication behaviour on the dragged element
        document.addEventListener(
            "dragstart",
            function (event) {
                const img = new Image()
                img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs="
                event.dataTransfer?.setDragImage(img, 0, 0)
            },
            false
        )

        this.itemsData = this.value.objects
        this.totalItems = this.value.total
        this.loading = false

        // If we have a non-empty OxCollection defined in the OxDatalist v-model
        if (this.value &&
            this.itemsData.length > 0 &&
            typeof this.value === "object"
        ) {
            // We can deduce the object class and type from the first item
            this.objectClass = this.itemsData[0].constructor as new() => OxObject
            this.resourceType = this.itemsData[0].type
        }
        else if (this.oxObject) {
            // Else the "oxObject" prop has to be defined (to be able to get the object class and type)
            this.objectClass = this.oxObject
            /* eslint-disable-next-line new-cap */
            this.resourceType = new this.objectClass().type
        }
        else {
            throw new Error("Impossible type inference")
        }
    },
    methods: {
        /**
         * Returns the CSS classes for table rows
         *
         * @return {string} The CSS classes
         */
        rowItemClasses (index: number): string {
            let classes = "OxDatalist-tableRow"

            if (this.drag) {
                if (this.currentIndex !== null && index === this.currentIndex) {
                    classes += " dragged"
                }
                else {
                    classes += " dragActive"
                }
            }

            if (this.stripped) {
                classes += " stripped"
            }

            return classes
        },

        /**
         * Performs the API request to refresh the Datalist
         *
         * @param {OxUrlBuilder} url
         */
        async refreshRequest (url: OxUrlBuilder): Promise<void> {
            this.loading = true

            const data = await getCollectionFromJsonApiRequest(
                this.objectClass,
                url.toString()
            )

            this.itemsData = data.objects
            this.totalItems = data.total
            this.selectedItems = []
            this.showMassActions = false
            this.loading = false

            this.$emit("input", data)
        },

        /**
         * Refreshes the Datalist :
         *   - by immediately doing a new API request with the new given URL
         *
         * @param {OxUrlBuilder} url
         */
        async refresh (url?: OxUrlBuilder): Promise<void> {
            const newUrl = url || new OxUrlBuilder(this.value.self)
            await this.refreshRequest(newUrl)
        },

        /**
         * Adds the "search" query parameter in the API request.
         * Makes a new API request with the new params by refreshing the Datalist
         */
        async searchItems (): Promise<void> {
            const url = new OxUrlBuilder(this.value.self)

            url.withSearch(this.search)

            await this.refresh(url)
            this.searchQuery = this.search
            this.displaySearchQuery = !!this.searchQuery
        },

        /**
         * Removes the "search" query parameter in the API request.
         * Makes a new API request with the new params by refreshing the Datalist
         */
        async resetSearch (): Promise<void> {
            const url = new OxUrlBuilder(this.value.self)

            url.withSearch(null)
            this.search = ""

            await this.refresh(url)
            this.displaySearchQuery = false
        },

        itemValue (item: OxObject, headerValue: string): string {
            const valueSplit = headerValue.split(".")

            if (valueSplit.length > 1) {
                let tmpItem: OxObject | string = cloneObject(item)

                valueSplit.every((val) => {
                    if (!tmpItem[val]) {
                        tmpItem = ""
                        return false
                    }

                    tmpItem = tmpItem[val]
                    return true
                })

                return tmpItem as unknown as string
            }

            return item[headerValue]
        },

        /**
         * Checks if it is the "actions" column
         *
         * @param {string} headerValue - The header column value
         * @return {boolean}
         */
        isActionsColumn (headerValue: string): boolean {
            return headerValue === "actions"
        },

        /**
         * Checks the indeterminate state for the header simple checkbox
         *
         * @param {boolean} someItems
         * @param {boolean} everyItem
         * @return {boolean}
         */
        isCheckboxIndeterminate (someItems: boolean, everyItem: boolean): boolean {
            return someItems && !everyItem
        },

        /**
         * Checks if the draggable icon is displayed in the first column
         *
         * @param {number} index
         * @return {boolean}
         */
        showDraggableIconWithoutSelect (index: number): boolean {
            return index === 0 && !this.showSelect
        },

        /**
         * Update item event propagation
         *
         * @param {OxObject} item - The updated item
         */
        updateItem (item: OxObject): void {
            this.$emit("updateItem", item)
        },

        /**
         * Delete item event propagation
         *
         * @param {OxObject} item - The deleted item
         */
        deleteItem (item: OxObject): void {
            this.$emit("deleteItem", item)
        },

        /**
         * Filter items event propagation
         *
         * @param {string[]} selectedFilters - The selected filters
         */
        filterItems (selectedFilters: string[]): void {
            this.$emit("filterItems", selectedFilters)
        },

        /**
         * Input event propagation
         *
         * @param {OxObject[]} selectedItems - The selected items
         */
        input (selectedItems: OxObject[]): void {
            this.selectedItems = selectedItems
            this.showMassActions = this.selectedItems.length > 0
            this.$emit("selectedItems", selectedItems)
        },

        /**
         * Start drag event propagation
         *
         * @param {CustomEvent} event
         */
        startDrag (event): void {
            this.drag = true
            this.currentIndex = event.oldIndex
            this.$emit("startDrag", event)
        },

        /**
         * End drag event propagation
         *
         * @param {CustomEvent} event
         */
        endDrag (event): void {
            this.drag = false
            this.$emit("endDrag", event)
        },

        inputDrag (items: OxObject[]): void {
            this.itemsData = items
        }
    }
})
</script>
<template>
  <div class="OxDatalist">
    <v-data-table
      v-model="selectedItems"
      :checkbox-color="primaryColor"
      :class="datalistClasses"
      :fixed-header="fixedHeaders"
      :headers="headers"
      height="100%"
      hide-default-footer
      :hide-default-header="hideHeader"
      :item-class="rowItemClasses"
      :items="itemsData"
      :loading="loading"
      :loading-text="$tr('Loading in progress')"
      :no-results-text="$tr('common-error-Search-no-results')"
      :search="search"
      :server-items-length="totalItems"
      :show-select="showSelect"
      @input="input"
    >
      <template #body="slotProps">
        <tbody v-if="!slotProps.originalItemsLength">
          <tr class="v-data-table__empty-wrapper">
            <td :colspan="slotProps.headers.length">
              {{ customableNoDataText }}
            </td>
          </tr>
        </tbody>
        <transition-group
          is="draggable"
          v-else
          animation="150"
          :name="draggableAnimationName"
          tag="tbody"
          type="transition"
          :value="slotProps.items"
          @end="endDrag"
          @input="inputDrag"
          @start="startDrag"
        >
          <tr
            v-for="(item, index) in slotProps.items"
            :key="item.id"
            :class="rowItemClasses(index)"
          >
            <td
              v-if="showSelect"
              class="OxDatalist-selectCell"
            >
              <div class="OxDatalist-draggable">
                <ox-icon
                  icon="dragVertical"
                  :size="20"
                />
              </div>
              <v-simple-checkbox
                :color="primaryColor"
                ripple
                :value="slotProps.isSelected(item)"
                @input="slotProps.select(item, !slotProps.isSelected(item))"
              />
            </td>
            <td
              v-for="(header, i) in headers"
              :key="i"
              class="OxDatalist-tableCell"
            >
              <div
                v-if="showDraggableIconWithoutSelect(i)"
                class="OxDatalist-draggable"
              >
                <ox-icon
                  icon="dragVertical"
                  :size="20"
                />
              </div>
              <slot
                v-if="!isActionsColumn(header.value)"
                :item="item"
                :name="[`item.${header.value}`]"
                :value="itemValue(item, header.value)"
              >
                {{ itemValue(item, header.value) }}
              </slot>
              <div
                v-else-if="showActions"
                class="OxDatalist-tableActionsButtons"
              >
                <slot
                  :button-delete-attrs="{buttonStyle: 'tertiary-dark', icon: 'delete', title: $tr('Delete')}"
                  :button-edit-attrs="{buttonStyle: 'tertiary-dark', icon: 'edit', title: $tr('Edit')}"
                  :item="item"
                  name="item.actions"
                  :value="item[header.value]"
                >
                  <ox-button
                    button-style="tertiary-dark"
                    icon="edit"
                    :title="$tr('Edit')"
                    @click="updateItem(item)"
                  />
                  <ox-button
                    button-style="tertiary-dark"
                    icon="delete"
                    :title="$tr('Delete')"
                    @click="deleteItem(item)"
                  />
                </slot>
              </div>
            </td>
          </tr>
        </transition-group>
      </template>
      <template #top>
        <div
          v-if="searchable"
          class="OxDatalist-search"
        >
          <ox-text-field
            v-model="search"
            hide-details
            :icon="searchFieldIcon"
            :label="searchLabelText"
            rounded
            single-line
            @click:append="resetSearch"
            @keydown.enter="searchItems"
          />
          <p
            v-if="displaySearchQuery"
            class="OxDatalist-searchQuery"
          >
            {{ $tr("common-label-Search results", false, searchQuery) }}
          </p>
        </div>
        <div
          v-if="filters"
          class="OxDatalist-filters"
        >
          <div class="OxDatalist-filterIcon">
            <ox-icon icon="filter" />
          </div>
          <ox-chip-group
            v-model="selectedFilters"
            :choices="filters"
            :multiple="true"
            @input="filterItems(selectedFilters)"
          />
        </div>
      </template>
      <template #header.data-table-select="{ props, on }">
        <div class="OxDatalist-selectCell">
          <div class="OxDatalist-selectCellEmpty" />
          <v-simple-checkbox
            v-model="props.value"
            v-ripple
            :color="primaryColor"
            :indeterminate="props.indeterminate"
            @input="on['input']"
          />
        </div>
      </template>
      <template
        v-if="showMassActions"
        #header="{ props, on }"
      >
        <thead class="OxDatalist-customHeader">
          <tr>
            <th class="OxDatalist-customHeaderCell">
              <div class="OxDatalist-selectCell">
                <div class="OxDatalist-selectCellEmpty" />
                <v-simple-checkbox
                  v-model="props.everyItem"
                  v-ripple
                  :color="primaryColor"
                  :indeterminate="isCheckboxIndeterminate(props.someItems, props.everyItem)"
                  @input="on['toggle-select-all']"
                />
              </div>
            </th>
            <th
              class="OxDatalist-customHeaderCell"
              :colspan="props.headers.length - 1"
            >
              <div class="OxDatalist-tableHeaderActions">
                <div class="OxDatalist-nbSelected">
                  {{ selectedItems.length }} {{ selectionText }}
                </div>
                <div class="OxDatalist-massActions">
                  <slot
                    name="mass-actions"
                    :selected-items="selectedItems"
                  />
                </div>
              </div>
            </th>
          </tr>
        </thead>
      </template>
      <template
        v-for="column in headers"
        #[`item.${column.value}`]="{ item, value: columnValue }"
      >
        <slot
          :item="item"
          :name="`item.${column.value}`"
          :value="columnValue"
        >
          {{ columnValue }}
        </slot>
      </template>
      <template
        v-if="showActions"
        #item.actions="{ item, value: actionsColumnValue }"
      >
        <div class="OxDatalist-tableActionsButtons">
          <slot
            :button-delete-attrs="{buttonStyle: 'tertiary-dark', icon: 'delete', title: $tr('Delete')}"
            :button-edit-attrs="{buttonStyle: 'tertiary-dark', icon: 'edit', title: $tr('Edit')}"
            :item="item"
            name="item.actions"
            :value="actionsColumnValue"
          >
            <ox-button
              button-style="tertiary-dark"
              icon="edit"
              :title="$tr('Edit')"
              @click="updateItem(item)"
            />
            <ox-button
              button-style="tertiary-dark"
              icon="delete"
              :title="$tr('Delete')"
              @click="deleteItem(item)"
            />
          </slot>
        </div>
      </template>
    </v-data-table>
  </div>
</template>

<style lang="scss" src="./OxDatalist.scss" />
