<!--
  @author  SAS OpenXtrem <dev@openxtrem.com>
  @license https://www.gnu.org/licenses/gpl.html GNU General Public License
  @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
-->
<script lang="ts">

import Vue, { PropType } from "vue"
import { addInfo } from "@/core/utils/OxNotifyManager"
import { isModuleActive } from "@/core/utils/OxModulesManager"
import { OxUrlBuilder } from "@/core/utils/OxUrlTools"
import { openCache } from "@/core/utils/OxXHRCache"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"
import { cloneDeep, debounce } from "lodash"
import { convertHTMLToMarkdown } from "@/core/utils/OxMarkdown"
import {
    createJsonApiObjects,
    getCollectionFromJsonApiRequest,
    getObjectFromJsonApiRequest
} from "@/core/utils/OxApiManager"
import {
    OxCursorNodes,
    OxQuickActionEmits,
    OxQuickActionsOptions,
    OxRichTextExecutedActions,
    OxRichTextFieldAutocompletePosition,
    OxRichTextOptions
} from "@/core/types/OxRichTextTypes"
import {
    getAutocompletePosition,
    getCursorNodes,
    setCursor,
    setCursorToAChild
} from "@/core/utils/OxCursorControl"

import OxObject from "@/core/models/OxObject"
import Preferences from "@modules/system/vue/models/Preferences"
import OxCollection from "@/core/models/OxCollection"
import Configurations from "@modules/system/vue/models/Configurations"

import OxRichTextFieldContent from "@/core/components/OxRichTextField/OxRichTextFieldContent/OxRichTextFieldContent.vue"
import OxRichTextFieldControlBar
    from "@/core/components/OxRichTextField/OxRichTextFieldControlBar/OxRichTextFieldControlBar.vue"
import User from "@modules/admin/vue/models/User"
import MedicalCode from "@modules/system/vue/models/MedicalCode"
import { mapState } from "pinia"
import { useUserStore } from "@/core/stores/user"

const OxRichTextFieldAutocomplete = () => import("@/core/components/OxRichTextField/OxRichTextFieldAutocomplete/OxRichTextFieldAutocomplete.vue")

// Conditional import for the classes HelpedText and ModelsPreferences from the dPcompteRendu module
const modulesModelsName = "dPcompteRendu"
let OxHelpedText = OxObject
let helpedText = new OxObject()
let ModelsPreferences = Preferences
let modelsPreferences = new Preferences()

const modulesPatientName = "dPpatients"
let Patient = OxObject
let patient = new OxObject()

export default Vue.extend({
    name: "OxRichTextField",
    components: { OxRichTextFieldAutocomplete, OxRichTextFieldContent, OxRichTextFieldControlBar },
    provide () {
        return {
            authorizedCodes: this.quickActionsOptions.authorizedCodes
        }
    },
    props: {
        value: {
            type: String
        },
        ready: {
            type: Boolean,
            default: true
        },
        options: {
            type: Object as PropType<OxRichTextOptions>,
            required: true
        },
        quickActionsOptions: {
            type: Object as PropType<OxQuickActionsOptions>,
            default: () => { return {} as OxQuickActionsOptions }
        },
        placeholder: {
            type: String
        },
        executedActions: {
            type: Object as PropType<OxRichTextExecutedActions>,
            default: () => {
                return {} as OxRichTextExecutedActions
            }
        },
        isMarkdown: {
            type: Boolean,
            default: false
        },
        isHelped: {
            type: Boolean,
            default: false
        },
        minLineCount: {
            type: Number,
            default: 5
        },
        autofocus: {
            type: Boolean,
            default: false
        },
        attach: {
            type: Element
        }
    },
    data: function () {
        return {
            // DATA
            text: "",
            currentText: "",
            // LAST VALUES
            lastCursorNodes: undefined as OxCursorNodes | undefined,
            lastTextMatched: "",
            // AUTOCOMPLETE DISPLAY
            autocompletePosition: {} as OxRichTextFieldAutocompletePosition,
            displayAutocomplete: false,
            isFocused: false,
            // OBJECTS
            helpedTextCollection: null as OxCollection<typeof helpedText> | null,
            autocompleteObjects: [] as OxObject[],
            autocompleteCodes: [] as MedicalCode[],
            selectedCode: "" as string,
            executedActionsMutated: {
                helpedTexts: [],
                users: [],
                patients: [],
                medicalCodes: []
            } as OxRichTextExecutedActions,
            timestampConfig: "",
            // BUTTONS
            activeBold: false,
            activeItalic: false,
            activeStrikethrough: false,
            activeList: false,
            activeListNum: false,
            // LOADING
            helpedTextLoading: false,
            configLoading: false,
            prefsLoading: false,
            // USER
            addFastActive: true,
            autocompleteActive: true,
            timestampActive: true
        }
    },
    computed: {
        ...mapState(useUserStore, ["user", "userFunction", "userGroup"]),
        richTextFieldClass (): { [className: string]: boolean } {
            return { OxRichTextField: true, richTextLoading: this.isLoading }
        },
        richTextFieldContentClass (): { [className: string]: boolean } {
            return { empty: this.isContentEmpty, OxMarkdown: this.isMarkdown }
        },
        buttonsActive (): { [className: string]: boolean } {
            return {
                bold: this.activeBold,
                italic: this.activeItalic,
                strikethrough: this.activeStrikethrough,
                list: this.activeList,
                listNum: this.activeListNum
            }
        },
        isContentEmpty (): boolean {
            return this.currentText === "" || this.currentText === " " || this.currentText === "<br>"
        },
        isHelpedTextActive (): boolean {
            return this.isHelped && isModuleActive(modulesModelsName)
        },
        isLoading (): boolean {
            return this.configLoading || this.prefsLoading || this.helpedTextLoading || !this.ready
        },
        isModulePatientActive (): boolean {
            return isModuleActive(modulesPatientName)
        }
    },
    watch: {
        ready: {
            handler () {
                this.$nextTick().then(() => {
                    if (this.autofocus) {
                        ((this.$refs["rich-text"] as Vue).$el as HTMLDivElement).focus()
                    }
                })
            },
            immediate: true
        },
        lastCursorNodes (value: OxCursorNodes | undefined) {
            this.resetButtonsState()
            const rootElement = (this.$refs["rich-text"] as Vue).$el
            if (value && value.startNode !== rootElement) {
                let parent = value.startNode.parentNode
                while (parent && parent !== rootElement) {
                    switch (parent.nodeName) {
                    case "STRONG":
                    case "B":
                        this.activeBold = true
                        break
                    case "EM":
                    case "I":
                        this.activeItalic = true
                        break
                    case "DEL":
                    case "STRIKE":
                    case "S":
                        this.activeStrikethrough = true
                        break
                    case "UL":
                        this.activeList = true
                        break
                    case "OL":
                        this.activeListNum = true
                        break
                    }
                    parent = parent.parentNode
                }
            }
        }
    },
    created () {
        this.text = this.value || ""
        this.currentText = this.value || ""

        /*
         * Only execute the import if the module exists
         * The "ts-ignore" in front of the import is mandatory because it will crash the ts compilation if the path isn't found
         */
        if (isModuleActive(modulesModelsName)) {
            // @ts-ignore
            import("@modules/dPcompteRendu/vue/models/OxHelpedText").then((mod) => {
                OxHelpedText = mod.default
                helpedText = new OxHelpedText()
            })

            // @ts-ignore
            import("@modules/dPcompteRendu/vue/models/ModelsPreferences").then(mod => {
                ModelsPreferences = mod.default
                modelsPreferences = new ModelsPreferences()
            })
        }

        if (isModuleActive(modulesPatientName)) {
            // @ts-ignore
            import("@modules/dPpatients/vue/models/Patient").then((mod) => {
                Patient = mod.default
                patient = new Patient()
            })
        }
    },
    async mounted () {
        if (this.autofocus) {
            ((this.$refs["rich-text"] as Vue).$el as HTMLDivElement).focus()
        }
        if (this.isHelpedTextActive) {
            const prefs = await this.loadModulePrefs()
            /* eslint-disable dot-notation */
            this.addFastActive = prefs.attributes["aideFastMode"] === "1"
            this.autocompleteActive = prefs.attributes["aideAutoComplete"] === "1"
            this.timestampActive = prefs.attributes["aideTimestamp"] === "1"
            /* eslint-enable dot-notation */
        }
    },
    methods: {
        resetButtonsState () {
            this.activeBold = false
            this.activeItalic = false
            this.activeStrikethrough = false
            this.activeList = false
            this.activeListNum = false
        },
        autocomplete (text: string) {
            if (!this.autocompleteActive) {
                return
            }
            this.isFocused = true
            this.lastCursorNodes = getCursorNodes()
            this.autocompleteDebounced(text)
        },
        autocompleteDebounced: debounce(
            async function (this, text: string) {
                if (this.isFocused) {
                    await this.onHelpedText(text)
                }
            },
            300
        ),
        setTheCursorBackToHisPosition () {
            if (this.lastCursorNodes) {
                setCursor(
                    this.lastCursorNodes.startNode,
                    this.lastCursorNodes.startPos,
                    this.lastCursorNodes.endNode,
                    this.lastCursorNodes.endPos
                )
            }
        },
        /**
         * TODO: Replace using Input Events Level 2 when it will be ready or recode it
         * Warning the execCommand function is deprecated, for now we still use it
         * This allow to simply create style, this is currently used by gmail and outlook
         * @deprecated
         */
        execBold () {
            if (this.lastCursorNodes) {
                this.setTheCursorBackToHisPosition()
                this.activeBold = !this.activeBold
                document.execCommand("bold")
                this.lastCursorNodes = getCursorNodes()
                this.extractMarkdownFromHTML((this.$refs["rich-text"] as Vue).$el.innerHTML)
            }
        },
        /**
         * TODO: Replace using Input Events Level 2 when it will be ready or recode it
         * @deprecated
         */
        execItalic () {
            if (this.lastCursorNodes) {
                this.setTheCursorBackToHisPosition()
                this.activeItalic = !this.activeItalic
                document.execCommand("italic")
                this.lastCursorNodes = getCursorNodes()
                this.extractMarkdownFromHTML((this.$refs["rich-text"] as Vue).$el.innerHTML)
            }
        },
        /**
         * TODO: Replace using Input Events Level 2 when it will be ready or recode it
         * @deprecated
         */
        execStrikethrough () {
            if (this.lastCursorNodes) {
                this.setTheCursorBackToHisPosition()
                this.activeStrikethrough = !this.activeStrikethrough
                document.execCommand("strikethrough")
                this.lastCursorNodes = getCursorNodes()
                this.extractMarkdownFromHTML((this.$refs["rich-text"] as Vue).$el.innerHTML)
            }
        },
        /**
         * TODO: Replace using Input Events Level 2 when it will be ready or recode it
         * @deprecated
         */
        execList () {
            if (this.lastCursorNodes) {
                this.setTheCursorBackToHisPosition()
                this.activeListNum = false
                this.activeList = !this.activeList
                document.execCommand("insertUnorderedList")
                this.lastCursorNodes = getCursorNodes()
                this.extractMarkdownFromHTML((this.$refs["rich-text"] as Vue).$el.innerHTML)
            }
        },
        /**
         * TODO: Replace using Input Events Level 2 when it will be ready or recode it
         * @deprecated
         */
        execListNum () {
            if (this.lastCursorNodes) {
                this.setTheCursorBackToHisPosition()
                this.activeList = false
                this.activeListNum = !this.activeListNum
                document.execCommand("insertOrderedList")
                this.lastCursorNodes = getCursorNodes()
                this.extractMarkdownFromHTML((this.$refs["rich-text"] as Vue).$el.innerHTML)
            }
        },
        /**
         * TODO: Replace using Input Events Level 2 when it will be ready or recode it
         * @deprecated
         */
        execLink () {
            const text = document.getSelection()?.toString() || ""
            if (text === "") {
                addInfo(this.$tr("OxRichTextField-msg-Please, make a selection to create a link"))
            }
            else {
                const link = prompt("Enter an URL:")
                if (link === null || link === "" || link === " ") {
                    addInfo(this.$tr("OxRichTextField-msg-Please, enter a valid url"))
                }
                else {
                    document.execCommand("createLink", false, link)
                }
            }
            this.extractMarkdownFromHTML((this.$refs["rich-text"] as Vue).$el.innerHTML)
        },
        /**
         * Place the text at the current cursor position
         * Wraps the linebreak as BR to render them
         * @param text
         * @param deleteBeforeCursor
         */
        addTextToCursor (text: string, deleteBeforeCursor = 0) {
            if (this.lastCursorNodes) {
                const { parent, child, index } = this.getRightElements(this.lastCursorNodes.endNode, this.lastCursorNodes.endPos)

                // We split at the cursor position
                const { before, after } = this.splitChildNode(child, index, deleteBeforeCursor)
                const lines = text.split(/\r?\n|\r|\n/g)

                /*
                 * We replace the text node, note: the | represent the cursor
                 * Before : "Lorem |ipsum"
                 * After : "Lorem " + node + " |ipsum"
                 */
                parent.insertBefore(after, child.nextSibling)
                // Add each lines and then add BR for the line break, we need to reverse the list due to insertBefore
                lines.reverse().forEach((text, index) => {
                    const c = document.createTextNode(text)
                    parent.insertBefore(c, child.nextSibling)
                    if (index !== (lines.length - 1)) {
                        parent.insertBefore(document.createElement("br"), child.nextSibling)
                    }
                })
                parent.replaceChild(before, child)

                setCursorToAChild(after, 1)
                this.lastCursorNodes = getCursorNodes()

                const rootElementInnerHTML = (this.$refs["rich-text"] as Vue).$el.innerHTML
                this.currentText = rootElementInnerHTML
                this.$emit("input", this.prepareOutputText(rootElementInnerHTML))
            }
        },
        getRightElements (child: Node, index: number): { parent: Node, child: Node, index: number} {
            const rootElement = (this.$refs["rich-text"] as Vue).$el
            let parent = (child.parentNode || child.parentElement) as Node

            if (child === rootElement) {
                parent = child
                if (index === 0) {
                    child = document.createElement("div")
                    parent.insertBefore(child, parent.firstChild)
                }
                else {
                    child = parent.childNodes[index - 1]
                    index = (child.textContent || "").length
                }
            }

            return { parent, child, index }
        },
        /**
         * This function split a node at cursor position and remove char if needed
         * This add also a space on the right side node in the case of the node was empty.
         * @param child
         * @param cursor
         * @param deleteBeforeCursor
         */
        splitChildNode (child: Node, cursor: number, deleteBeforeCursor = 0): { before: Text, after: Text } {
            const before = document.createTextNode(child.textContent?.substring(0, cursor - deleteBeforeCursor) || "")
            const after = document.createTextNode("\u00A0" + (child.textContent?.substring(cursor) || ""))

            return { before, after }
        },
        /**
         * This function allow to the user to simply write the time and date
         */
        async writeHelpedTimestamp () {
            const timestamp = await this.generateHelpedTimestamp()

            this.addTextToCursor(timestamp)
        },
        /**
         * This function generate the helped horodatage from the horodatage config in dPcompteRendu
         * This is link to a config called horodatage witch specify the format
         * We use old script in this for now (DateFormat and User)
         * User will be replace by the current user being passed to every entry point
         * For DateFormat, we will need to find a replacement
         */
        async generateHelpedTimestamp (): Promise<string> {
            // TODO : Find an alternative to the usage of DateFormat and User

            if (this.timestampConfig === "") {
                const object = await this.loadModuleConfigs()
                if (object.instance !== undefined) {
                    this.timestampConfig = object.instance["dPcompteRendu CCompteRendu timestamp"]
                }
            }

            // @ts-ignore
            // eslint-disable-next-line no-undef
            let timestamp = DateFormat.format(new Date(), this.timestampConfig)
            const userFullName = this.user.view.split(" ")

            timestamp = timestamp.replace(/%p/g, userFullName[1])
            timestamp = timestamp.replace(/%n/g, userFullName[0])
            timestamp = timestamp.replace(/%i/g, userFullName[1].charAt(0) + ". " + userFullName[0].charAt(0) + ". ")

            return timestamp
        },
        async addHelpedText (type: "mediuser" | "function" | "group") {
            const text = this.convertHTMLToTextWithLineBreaks((this.$refs["rich-text"] as Vue).$el.innerHTML)

            if (text !== "" && text !== " ") {
                const owner = new OxObject()
                owner.type = type
                switch (type) {
                case "function":
                    owner.id = this.userFunction.id
                    break
                case "group":
                    owner.id = this.userGroup.id
                    break
                case "mediuser":
                    owner.id = this.user.id
                    break
                }

                const helpedText = new OxHelpedText()

                // @ts-ignore
                helpedText.name = text.split(" ").slice(0, 3).join(" ")
                // @ts-ignore
                helpedText.text = text
                // @ts-ignore
                helpedText.class = this.options.objectType
                // @ts-ignore
                helpedText.field = this.options.field
                // @ts-ignore
                helpedText.dependValue1 = this.options.dependValue1
                // @ts-ignore
                helpedText.dependValue2 = this.options.dependValue2
                // @ts-ignore
                helpedText.owner = owner

                await this.removeHelpedTextFromCache()

                const url = new OxUrlBuilder(OxUrlDiscovery.createHelpedText())
                    .addFieldset("default")
                    .addFieldset("depends")
                    .addRelation("owner")

                await createJsonApiObjects(helpedText, url.toString())
                addInfo(this.$tr("CAideSaisie-msg-create"))
            }
        },
        /**
         * This will display the quickAction at a specific position (x, y)
         * determined by the cursor position minus the word length
         * @param userSearched
         */
        showAutocomplete (userSearched: string) {
            const autocompletePos = getAutocompletePosition(userSearched, (this.attach ?? this.$el) as HTMLDivElement)
            if (autocompletePos) {
                this.autocompletePosition = autocompletePos
                this.displayAutocomplete = true
                if (this.attach) {
                    this.$nextTick().then(() => {
                        this.attach.appendChild((this.$refs.autocomplete as Element))
                    })
                }
            }
            else {
                addInfo(this.$tr("OxRichTextField-msg-Please, make a selection inside the field in order to show the helps."))
            }
        },
        /**
         * This hide the autocomplete
         */
        hideAutocomplete () {
            this.displayAutocomplete = false
        },
        /**
         * This function will gather the helped text for the field (executed on mounted)
         */
        async loadHelpedTextCollection (): Promise<OxCollection<typeof helpedText>> {
            this.helpedTextLoading = true
            const url = new OxUrlBuilder(OxUrlDiscovery.listHelpedTexts(this.options.objectType, this.options.field))
            url.addFieldset("default")
            url.addFieldset("depends")
            url.addRelation("owner")
            url.addParameter("with_words_tokens", "1")
            url.addParameter("all", "1")
            let collection = await getCollectionFromJsonApiRequest(
                OxHelpedText,
                url.toString(),
                { useCache: true, freshnessTime: 3600, useEtag: true }
            )
            if (collection.objects.length === 0) {
                collection = await getCollectionFromJsonApiRequest(
                    OxHelpedText,
                    url.toString(),
                    { useCache: true, freshnessTime: 300, useEtag: true }
                )
            }
            this.helpedTextLoading = false
            return collection
        },
        async removeHelpedTextFromCache () {
            this.helpedTextCollection = null
            const userStore = useUserStore()
            const cache = await openCache(userStore.sessionHash)
            const keys = await cache.keys()
            keys.forEach((request) => {
                if (request.url.includes(OxUrlDiscovery.listHelpedTexts(this.options.objectType, this.options.field))) {
                    cache.delete(request)
                }
            })
        },
        async loadModuleConfigs (): Promise<Configurations> {
            this.configLoading = true
            const config = await getObjectFromJsonApiRequest(
                Configurations,
                OxUrlDiscovery.moduleConfigurations(modulesModelsName),
                { useCache: true, freshnessTime: 300, useEtag: true }
            )
            this.configLoading = false
            return config
        },
        async loadModulePrefs (): Promise<typeof modelsPreferences> {
            this.prefsLoading = true
            const prefs = await getObjectFromJsonApiRequest(
                ModelsPreferences,
                OxUrlDiscovery.userPreferences(modulesModelsName, this.user.id),
                { useCache: true, freshnessTime: 300, useEtag: true }
            )
            this.prefsLoading = false
            return prefs
        },
        /**
         * This function is executed when an autocomplete is emitted
         * So, we will get the helpedText and create a text node from it
         * And then we add it to the cursor position
         * @param object
         */
        completeAutocomplete (object: OxObject) {
            this.hideAutocomplete()

            if (object.type === "helped_text") {
                // @ts-ignore
                this.addTextToCursor(object.text?.toString() || "", this.lastTextMatched.length)

                this.executedActionsMutated.helpedTexts.push(object)
                this.$emit("update:executedActions", this.executedActionsMutated)
            }
            else if (object.type === "code" && object instanceof MedicalCode) {
                this.addTextToCursor(object.toString(), this.lastTextMatched.length)

                this.executedActionsMutated.medicalCodes.push(object)
                this.$emit("update:executedActions", this.executedActionsMutated)
            }
            else if (object.type === "user" && object instanceof User) {
                this.addTextToCursor(object.shortView, this.lastTextMatched.length)

                this.executedActionsMutated.users.push(object)
                this.$emit("update:executedActions", this.executedActionsMutated)
            }
            else if (object.type === "patient") {
                // @ts-ignore
                this.addTextToCursor(object.shortView, this.lastTextMatched.length)

                this.executedActionsMutated.patients.push(object)
                this.$emit("update:executedActions", this.executedActionsMutated)
            }
            else {
                const text = `${object.type}:${object.id}`
                this.addTextToCursor(text, this.lastTextMatched.length)
            }
        },
        /**
         * Open legacy modal to create a helped text
         */
        openModal () {
            this.removeHelpedTextFromCache()

            // @ts-ignore
            // eslint-disable-next-line no-undef
            const url = new Url().setRoute(OxUrlDiscovery.editHelpedTexts())
            url.addParam("restricted", "1")
            url.requestModal("80%", "80%", {
                title: this.$tr("CAideSaisie-title-create"),
                method: "get",
                showReload: false,
                aboveNewModal: true,
                getParameters: {
                    user_id: this.user.id,
                    class: this.options.objectType,
                    field: this.options.field,
                    depend_value_1: this.options.dependValue1,
                    depend_value_2: this.options.dependValue2,
                    class_depend_value_1: this.options.classDependsValue1,
                    class_depend_value_2: this.options.classDependsValue2
                }
            })
        },
        /**
         * Transfers emit to parent for the v-model
         * @param text
         */
        extractMarkdownFromHTML (text: string) {
            this.currentText = text
            this.$emit("input", this.prepareOutputText(text))
        },
        /**
         * Tokenize written text:
         * - Split on specific chars
         * - Filter empty value
         * - Remove stop words
         * - Remove diacritics
         * @param text
         */
        tokenizeText (text: string): string[] {
            const stopWords = this.$tr("CAideSaisie-stop_words").split(" ")
            let tokenizedText = text.toLowerCase().split(/[\s!"#$%&'()*+,\-./:;<=>?@[\]\\^_`{|}~]+/)
            tokenizedText = tokenizedText.filter(value => value !== "")
            tokenizedText = tokenizedText.filter(word => !stopWords.includes(word))
            tokenizedText = tokenizedText.map(word => word.normalize("NFD").replace(/[\u0300-\u036F]/g, ""))

            return tokenizedText
        },
        /**
         * For each Token (in the HelpedText)
         * We check if a word is contained in the token (ex. voleur --matches--> leur, vol)
         * Then we create for each word the matchedHelpedText (set of HelpedTextId, we doesn't want duplicates)
         */
        matchSearchTokensToHelpedTextId (searchTokens: string[], helpedTextTokens: { [key: string]: number[] }): { [key: string]: Set<number> } {
            const matchedHelpedTextForWords = {} as { [key: string]: Set<number> }

            Object.entries(helpedTextTokens).forEach(([token, ids]) => {
                searchTokens.forEach((word) => {
                    if (!(word in matchedHelpedTextForWords)) {
                        matchedHelpedTextForWords[word] = new Set<number>()
                    }
                    if (token.includes(word)) {
                        ids.forEach(id => matchedHelpedTextForWords[word].add(id))
                    }
                })
            })
            return matchedHelpedTextForWords
        },
        /**
         * Since we have a list of matched HelpText for each word
         * We can match all the words together (intersect of the ids)
         * For that we push the first word's HelpText ids
         * And then we filter it from the other words
         */
        getMatchedHelpedTextIdsFromMatchedWords (matchedHelpedTextForWords: { [key: string]: Set<number> }): number[] {
            let matchedHelpedTextIds = [] as number[]
            Object.values(matchedHelpedTextForWords).forEach((value, index) => {
                if (index === 0) {
                    matchedHelpedTextIds.push(...value)
                }
                else {
                    matchedHelpedTextIds = matchedHelpedTextIds.filter((id) => value.has(id))
                }
            })
            return matchedHelpedTextIds
        },
        /**
         * Using the "dependValues" and the matched ids we filter the helpedText objects
         * @param helpedTexts
         * @param matchedHelpedTextIds
         */
        filterOxHelpedText (helpedTexts: typeof helpedText[], matchedHelpedTextIds: number[]): typeof helpedText[] {
            return helpedTexts.filter((obj) => {
                // @ts-ignore
                if (this.options.dependValue1 && obj.dependValue1 !== this.options.dependValue1) {
                    return false
                }
                // @ts-ignore
                if (this.options.dependValue2 && obj.dependValue2 !== this.options.dependValue2) {
                    return false
                }
                return matchedHelpedTextIds.includes(parseInt(obj.id))
            })
        },
        /**
         * On no actions executed save the last cursor nodes and
         */
        noActions (text: string) {
            this.lastTextMatched = text
            this.lastCursorNodes = getCursorNodes()
            this.hideAutocomplete()
        },
        /**
         * This function used the stored helpedText and search into it
         * For that we get the current text matched, we remove the stopwords, lowercase, and tokenize
         * We use the tokens from the helpedText to match the right helpedText
         * @param text
         */
        async onHelpedText (text: string) {
            this.lastTextMatched = text

            await this.searchHelpedText()
        },
        async searchHelpedText () {
            if (this.helpedTextLoading) {
                return
            }

            if (this.helpedTextCollection === null) {
                this.helpedTextCollection = await this.loadHelpedTextCollection()
            }

            let helpedTexts = [] as OxObject[]

            if (this.lastTextMatched === "") {
                this.autocompleteObjects = this.helpedTextCollection.objects
            }
            else {
                const searchTokens = this.tokenizeText(this.lastTextMatched)
                const helpedTextTokens: { [key: string]: number[] } = this.helpedTextCollection.meta?.words_tokens || {}

                // Gather, matched words with their matched ids
                const matchedHelpedTextForWords = this.matchSearchTokensToHelpedTextId(searchTokens, helpedTextTokens)
                // Filter, all the gathered ids to only have those whom match them all
                const matchedHelpedTextIds = this.getMatchedHelpedTextIdsFromMatchedWords(matchedHelpedTextForWords)
                // Finally, we filter the object list using the right HelpText ids
                helpedTexts = cloneDeep(this.filterOxHelpedText(this.helpedTextCollection.objects, matchedHelpedTextIds))

                // @ts-ignore
                helpedTexts.forEach((helpedText) => helpedText.search(searchTokens))

                this.autocompleteObjects = helpedTexts
            }

            if (this.autocompleteObjects.length > 0) {
                this.showAutocomplete(this.lastTextMatched)
            }
            else {
                this.hideAutocomplete()
            }
        },
        convertHTMLToTextWithLineBreaks (htmlText: string): string {
            htmlText = htmlText.replace(/<br\/?>/gi, "\r\n")

            const tempDiv = document.createElement("div")
            tempDiv.innerHTML = htmlText

            return tempDiv.textContent || ""
        },
        prepareOutputText (text: string) {
            if (!this.isMarkdown) {
                const tempHtml = document.createElement("div")
                tempHtml.innerHTML = text

                return tempHtml.textContent || ""
            }

            const regexBrBeforeTag = /<br>(<\/[^>]*>)/g
            text = text.replace(regexBrBeforeTag, "$1<br>")

            return convertHTMLToMarkdown(text)
        },
        onClickOutside (event) {
            if (!this.$refs.autocomplete || !(this.$refs.autocomplete as HTMLDivElement).contains(event.target)) {
                this.hideAutocomplete()
                this.isFocused = false
            }
        },
        quickActionAutocomplete (quickAction: OxQuickActionEmits) {
            this.autocompleteCodes = []
            if (!this.autocompleteActive) {
                return
            }
            this.isFocused = true
            this.lastCursorNodes = getCursorNodes()
            this.quickActionAutocompleteDebounced(quickAction)
        },
        quickActionAutocompleteDebounced: debounce(
            async function (this, quickAction: OxQuickActionEmits) {
                if (this.isFocused) {
                    await this.onQuickAction(quickAction)
                }
            },
            300
        ),
        async onQuickAction (quickAction: OxQuickActionEmits) {
            this.lastTextMatched = quickAction.word
            const search = quickAction.word.substring(1)
            switch (quickAction.type) {
            case "user":
                this.autocompleteObjects = await this.searchUser(search)
                break
            case "code":
                this.autocompleteObjects = await this.searchCode(search)
                break
            case "patient":
                this.autocompleteObjects = await this.searchPatient(search)
            }

            if (this.autocompleteObjects.length > 0) {
                this.showAutocomplete(this.lastTextMatched)
            }
            else {
                this.hideAutocomplete()
            }
        },
        async searchUser (search: string): Promise<Array<User>> {
            const url = new OxUrlBuilder(OxUrlDiscovery.listUsers())
            url.addParameter("autocomplete", "1")
            url.addParameter("keywords", search)

            return (await getCollectionFromJsonApiRequest(User, url.toString())).objects
        },
        async searchCode (search: string): Promise<Array<MedicalCode>> {
            const url = new OxUrlBuilder(OxUrlDiscovery.listMedicalCodes())
            url.addParameter("keywords", search)

            if (this.quickActionsOptions.authorizedCodes) {
                url.addFilter("types", "in", this.quickActionsOptions.authorizedCodes.join("."))
            }

            return (await getCollectionFromJsonApiRequest(MedicalCode, url.toString())).objects
        },
        async searchPatient (search: string): Promise<Array<typeof patient>> {
            if (!this.isModulePatientActive) {
                return []
            }

            const url = new OxUrlBuilder(OxUrlDiscovery.listPatients())
            url.addParameter("keywords", search)

            return (await getCollectionFromJsonApiRequest(Patient, url.toString())).objects
        }
    }
})
</script>

<template>
  <div
    v-click-outside="onClickOutside"
    class="OxRichTextField-container"
  >
    <div :class="richTextFieldClass">
      <ox-rich-text-field-control-bar
        v-if="isMarkdown || isHelpedTextActive"
        :add-fast="addFastActive"
        :buttons-active="buttonsActive"
        :is-helped="isHelpedTextActive"
        :is-markdown="isMarkdown"
        :timestamp="timestampActive"
        @bold="execBold"
        @clock="writeHelpedTimestamp"
        @italic="execItalic"
        @link="execLink"
        @list-bulleted="execList"
        @list-numbered="execListNum"
        @plus="openModal"
        @plusFast="addHelpedText"
        @search="searchHelpedText"
        @strikethrough="execStrikethrough"
      />
      <ox-rich-text-field-content
        ref="rich-text"
        :class="richTextFieldContentClass"
        :is-autocomplete-active="displayAutocomplete"
        :is-helped="isHelpedTextActive"
        :is-markdown="isMarkdown"
        :min-line-count="minLineCount"
        :placeholder="placeholder"
        :quick-actions-options="quickActionsOptions"
        :ready="ready"
        :text="text"
        @helpedText="autocomplete"
        @input="extractMarkdownFromHTML"
        @noActions="noActions"
        @quickAction="quickActionAutocomplete"
      />
      <v-progress-linear
        v-if="isLoading"
        class="OxRichTextField-loader"
        height="2"
        indeterminate
      />
    </div>

    <div
      v-if="displayAutocomplete && isHelpedTextActive"
      ref="autocomplete"
      class="OxRichTextField-autocomplete"
      :style="autocompletePosition"
    >
      <ox-rich-text-field-autocomplete
        :objects="autocompleteObjects"
        @autocomplete="completeAutocomplete"
        @exit="hideAutocomplete"
      />
    </div>
  </div>
</template>

<style src="./OxRichTextField.scss" lang="scss"></style>
