<!--
  @author  SAS OpenXtrem <dev@openxtrem.com>
  @license https://www.gnu.org/licenses/gpl.html GNU General Public License
  @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
-->
<script lang="ts">
import Vue, { PropType } from "vue"
import {
    OxQuickActionsOptions,
    OxRichTextFieldAutocompleteEscape,
    OxRichTextFieldAutocompleteShortcuts,
    OxRichTextFieldLineHeight
} from "@/core/types/OxRichTextTypes"
import { convertMarkdownToHTML } from "@/core/utils/OxMarkdown"
import { createNodeFromTextWithBrTags, getCurrentNodeText, getCurrentWord } from "@/core/utils/OxTextControl"
import { MedicalCodeFilterHeaderShortcuts } from "@modules/system/vue/types/MedicalCodesTypes"

const minCharsToEmitHelpedText = 2
const maxCharsToEmitHelpedText = 64
const quickActionUser = "@"
const quickActionCode = "#"
const quickActionPatient = "$"
const boldKeyboardKey = "b"
const italicKeyboardKey = "i"

export default Vue.extend({
    name: "OxRichTextFieldContent",
    props: {
        text: {
            type: String,
            required: true
        },
        isHelped: {
            type: Boolean,
            required: true
        },
        ready: {
            type: Boolean,
            required: true
        },
        isMarkdown: {
            type: Boolean,
            default: false
        },
        minLineCount: {
            type: Number,
            default: 5
        },
        isAutocompleteActive: {
            type: Boolean,
            default: false
        },
        placeholder: {
            type: String
        },
        quickActionsOptions: {
            type: Object as PropType<OxQuickActionsOptions>,
            default: () => { return {} as OxQuickActionsOptions }
        }
    },
    computed: {
        richTextStyle (): { "min-height": string } {
            return {
                "min-height": `${this.minLineCount * OxRichTextFieldLineHeight}px`
            }
        },
        isUsersQuickActionActive (): boolean {
            return this.quickActionsOptions.usersQuickAction ?? true
        },
        isCodesQuickActionActive (): boolean {
            return this.quickActionsOptions.codesQuickAction ?? true
        },
        isPatientsQuickActionActive (): boolean {
            return this.quickActionsOptions.patientsQuickAction ?? true
        }
    },
    mounted () {
        window.addEventListener("keydown", this.eventDisableLinks)
        window.addEventListener("keyup", this.eventActivateLinks)
    },
    destroyed () {
        window.removeEventListener("keydown", this.eventDisableLinks)
        window.removeEventListener("keyup", this.eventActivateLinks)
    },
    methods: {
        eventActivateLinks (event: KeyboardEvent) {
            if (event.key === "Control" || event.key === "Meta") {
                this.setLinksEdition(true)
            }
        },
        eventDisableLinks (event: KeyboardEvent) {
            if (event.key === "Control" || event.key === "Meta") {
                this.setLinksEdition(false)
            }
        },
        renderText (text: string) {
            if (!this.isMarkdown) {
                return text
            }

            return convertMarkdownToHTML(text)
        },
        /**
         * This method only emit one action at a time :
         * if helpedText is detected, so we emit "helpedText"
         * if no action is matched, so we emit "cursorChangedPosition"
         */
        choseAction (event: KeyboardEvent|MouseEvent) {
            if (event instanceof KeyboardEvent && event.key === OxRichTextFieldAutocompleteEscape) {
                return
            }
            if (this.isAutocompleteActive) {
                if (event instanceof KeyboardEvent &&
                    (
                        OxRichTextFieldAutocompleteShortcuts.includes(event.key) ||
                        MedicalCodeFilterHeaderShortcuts.includes(event.key)
                    )
                ) {
                    return
                }
            }

            const { text, isMiddle } = getCurrentNodeText()
            const word = getCurrentWord()

            const quickActionUsersDetected = (word.startsWith(quickActionUser) && this.isUsersQuickActionActive)
            const quickActionCodesDetected = (word.startsWith(quickActionCode) && this.isCodesQuickActionActive)
            const quickActionPatientsDetected = (word.startsWith(quickActionPatient) && this.isPatientsQuickActionActive)

            if (quickActionUsersDetected || quickActionCodesDetected || quickActionPatientsDetected) {
                if (word.length >= 3) {
                    if (quickActionUsersDetected) {
                        this.$emit("quickAction", { type: "user", word })
                        return
                    }
                    if (quickActionCodesDetected) {
                        this.$emit("quickAction", { type: "code", word })
                        return
                    }
                    if (quickActionPatientsDetected) {
                        this.$emit("quickAction", { type: "patient", word })
                        return
                    }
                }

                this.$emit("noActions", text)
                return
            }

            // Emit "helpedText" while in the range of minCharsToEmitHelpedText to maxCharsToEmitHelpedText
            if (!isMiddle && this.isHelped && text.length >= minCharsToEmitHelpedText && text.length <= maxCharsToEmitHelpedText) {
                this.$emit("helpedText", text)
            }
            else {
                this.$emit("noActions", text)
            }
        },
        /**
         * Prevent action if not markdown
         * @param event
         */
        preventIfNotMarkdown (event: KeyboardEvent) {
            if (this.isMarkdown) {
                switch (event.key) {
                case boldKeyboardKey:
                    document.execCommand("bold")
                    break
                case italicKeyboardKey:
                    document.execCommand("italic")
                    break
                }
            }
            event.preventDefault()
        },
        /**
         * Avoid any html pasting in the content editable
         * @param event
         */
        pastePlainText (event) {
            event.preventDefault()

            const element = this.$refs["rich-text"] as HTMLDivElement
            const text = event.clipboardData.getData("text")
            const selection = document.getSelection()
            if (selection && selection.rangeCount) {
                const node = createNodeFromTextWithBrTags(text)
                selection.deleteFromDocument()
                selection.getRangeAt(0).insertNode(node)
                selection.collapseToEnd()

                this.$emit("input", element.innerHTML)
            }
        },
        /**
         * We doesn't want the user to copy style from the content editable
         * @param event
         */
        copyPlainText (event) {
            event.preventDefault()
            const selection = document.getSelection()
            if (selection && selection.rangeCount) {
                event.clipboardData.setData("text/plain", selection.toString())
            }
        },
        /**
         * We doesn't want the user to copy style from the content editable
         * @param event
         */
        cutPlainText (event) {
            event.preventDefault()

            const element = this.$refs["rich-text"] as HTMLDivElement
            const selection = document.getSelection()
            if (selection && selection.rangeCount) {
                event.clipboardData.setData("text/plain", selection.toString())
                selection.deleteFromDocument()

                this.$emit("input", element.innerHTML)
            }
        },
        /**
         * Forward the input emit to the parent
         */
        inputCatch () {
            const element = this.$refs["rich-text"] as HTMLDivElement

            this.$emit("input", element.innerHTML)
        },
        setLinksEdition (editable: boolean) {
            const nodes = (this.$refs["rich-text"] as HTMLDivElement).querySelectorAll("a")
            nodes.forEach((node) => {
                node.contentEditable = editable ? "true" : "false"
            })
        }
    }
})
</script>

<template>
  <!--  eslint-disable vue/no-v-html -->
  <div
    ref="rich-text"
    class="OxRichTextFieldContent"
    :contenteditable="ready"
    :data-placeholder="placeholder"
    :style="richTextStyle"
    @click="choseAction"
    @copy="copyPlainText"
    @cut="cutPlainText"
    @input="inputCatch"
    @keydown.b.ctrl="preventIfNotMarkdown"
    @keydown.b.meta="preventIfNotMarkdown"
    @keydown.i.ctrl="preventIfNotMarkdown"
    @keydown.i.meta="preventIfNotMarkdown"
    @keydown.u.ctrl="preventIfNotMarkdown"
    @keydown.u.meta="preventIfNotMarkdown"
    @keyup="choseAction"
    @paste="pastePlainText"
    v-html="renderText(text)"
  />
  <!--  eslint-enable vue/no-v-html -->
</template>

<style src="./OxRichTextFieldContent.scss" lang="scss"></style>
