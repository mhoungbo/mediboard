/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import Vue, { Component } from "vue"
import OxVueWrap from "@/core/components/OxVueWrap/OxVueWrap.vue"
import { loadGUIEntrypointIframe } from "@/core/utils/OxGUIManager"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"
import { mapState } from "pinia"
import { useAppStore } from "@/core/stores/app"
import vuetify from "@/core/plugins/OxVuetifyCore"
import pinia from "@/core/plugins/OxPiniaCore"
const OxNotify = () => import("@/core/components/OxNotify/OxNotify.vue")
const OxDialog = () => import("@/core/oxify/components/OxDialog/OxDialog.vue")
const OxLoaderHandler = () => import("@/core/components/OxLoaderHandler/OxLoaderHandler.vue")

/**
 * Create Vue instance that contain component on element
 * @param {Component} component - The Vue component to mount
 * @param {HTMLElement} element - The HTMLElement used to start Vue
 * @param {boolean} isRoot - Is root component of the page
 * @param {string} coreProps - Props passed both to wrapper component and component
 * @param {string} vueProps - Props passed only to component
 */
export default function (component: Component, element: HTMLElement, isRoot: boolean, coreProps: string, vueProps: string) {
    new Vue({
        vuetify,
        pinia,
        components: {
            ViewComponent: component,
            OxVueWrap,
            OxNotify,
            OxDialog,
            OxLoaderHandler
        },
        data () {
            return {
                isRoot,
                showDialog: false
            }
        },
        computed: {
            ...mapState(useAppStore, {
                userMustReconnect (store) {
                    // @ts-ignore, this is not typed correctly, see: https://pinia.vuejs.org/core-concepts/state.html#usage-with-the-options-api
                    return Boolean(this.isRoot && store.mustReconnect)
                }
            })
        },
        watch: {
            userMustReconnect (value) {
                if (!value) {
                    this.showDialog = false
                    return
                }
                this.showDialog = true
                this.$nextTick(async () => {
                    const element = this.$refs.loginDialog as HTMLDivElement | undefined
                    if (element) {
                        loadGUIEntrypointIframe(OxUrlDiscovery.loginPage(true), element)
                    }
                })
            }
        },
        created () {
            if (isRoot) {
                // @legacy:  Event emits by login_reconnect_ok.html.twig
                document.addEventListener("ox-core-logout-done", this.reconnectDone)
            }
        },
        methods: {
            reconnectDone () {
                const app = useAppStore()
                app.mustReconnect = false
            }
        },
        template: `
          <ox-vue-wrap ${coreProps}>
            <ox-loader-handler v-if="isRoot" />
            <view-component ${vueProps} />
            <ox-dialog
                v-if="isRoot"
                v-model="showDialog"
                compact
                full-height
                hide-header
                persistent
                width="656"
            >
              <div ref="loginDialog" style="height: 700px" />
            </ox-dialog>
            <ox-notify v-if="isRoot" />
          </ox-vue-wrap>
        `
    }).$mount(element)
}
