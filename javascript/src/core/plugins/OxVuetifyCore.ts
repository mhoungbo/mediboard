/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import Vue from "vue"
import Vuetify from "vuetify/lib"
import fr from "vuetify/src/locale/fr"

Vue.use(Vuetify)

export default new Vuetify({
    lang: {
        locales: { fr },
        current: "fr"
    },
    icons: {
        iconfont: "mdiSvg"
    },
    theme: {
        themes: {
            light: {
                primary: "#3F51B5",
                secondary: "#03A9F4",
                accent: "#0277BD",
                error: "#FF3100",
                info: "#1E88E5",
                success: "#43A047",
                warning: "#FF9800",
                anchor: "#03A9F4"
            },
            dark: {
                primary: "#9FA8DA",
                secondary: "#4FC3F7",
                accent: "#84D5F9",
                error: "#FF3100",
                info: "#1E88E5",
                success: "#43A047",
                warning: "#FF9800",
                anchor: "#4FC3F7"
            }
        }
    }
})
