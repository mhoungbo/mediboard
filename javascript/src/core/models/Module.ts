/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import { OxAttr } from "@/core/types/OxObjectTypes"
import Tab from "@modules/system/vue/models/Tab"
import { cloneDeep } from "lodash"
import { createJsonApiObjects } from "@/core/utils/OxApiManager"
import { useUserStore } from "@/core/stores/user"
import { removeTabForModule, setTabForModule } from "@/components/Appbar/utils/DefaultCache"
import { addError } from "@/core/utils/OxNotifyManager"

export default class Module extends OxObject {
    private _standardTabs: Array<Tab>
    private _pinnedTabs: Array<Tab>
    private _paramTabs: Array<Tab>
    private _configureTabs: Array<Tab>
    // Default tabs order
    private _tabsOrder: Array<string>
    // For v-lazy component
    public isActive: boolean

    constructor (name?: string) {
        super()
        this.type = "module"
        this._standardTabs = []
        this._pinnedTabs = []
        this._paramTabs = []
        this._configureTabs = []
        this._tabsOrder = []
        this.isActive = false

        if (name) {
            this.name = name
        }
    }

    get name (): string {
        return super.get("mod_name")
    }

    set name (value: string) {
        super.set("mod_name", value)
    }

    get category (): string {
        return super.get("mod_category")
    }

    set category (value: string) {
        super.set("mod_category", value)
    }

    get activated (): boolean {
        return super.get("mod_active")
    }

    get visible (): boolean {
        return super.get("mod_ui_active")
    }

    set visible (value: boolean) {
        super.set("mod_ui_active", value)
    }

    get tabsUrl (): OxAttr<string> {
        return this.links.tabs
    }

    set tabsUrl (value: OxAttr<string>) {
        this.links.tabs = value
    }

    get url (): OxAttr<string> {
        return this.links.module_url
    }

    set url (value: OxAttr<string>) {
        this.links.module_url = value
    }

    get standardTabs (): Array<Tab> {
        // Slice to break reference
        return this._standardTabs.slice().sort((a, b) => {
            return this._tabsOrder.indexOf(a.name) - this._tabsOrder.indexOf(b.name)
        })
    }

    set standardTabs (value: Array<Tab>) {
        this._standardTabs = cloneDeep(value)
    }

    get pinnedTabs (): Array<Tab> {
        return this._pinnedTabs
    }

    set pinnedTabs (value: Array<Tab>) {
        this._pinnedTabs = cloneDeep(value)
    }

    get configureTabs (): Array<Tab> {
        return this._configureTabs
    }

    set configureTabs (value: Array<Tab>) {
        this._configureTabs = cloneDeep(value)
    }

    get paramTabs (): Array<Tab> {
        return this._paramTabs
    }

    set paramTabs (value: Array<Tab>) {
        this._paramTabs = cloneDeep(value)
    }

    get tabsLoaded (): boolean {
        return this._tabsOrder.length > 0
    }

    addTab (tab: Tab) {
        if (tab.pinnedOrder !== null && tab.pinnedOrder !== undefined) {
            this._pinnedTabs.splice(tab.pinnedOrder, 0, tab)
            this._tabsOrder.push(tab.name)
            return
        }
        if (tab.isStandard) {
            this._standardTabs.push(tab)
            this._tabsOrder.push(tab.name)
            return
        }
        if (tab.isParam) {
            this._paramTabs.push(tab)
            return
        }
        if (tab.isConfig) {
            this._configureTabs.push(tab)
        }
    }

    addTabs (tabs: Array<Tab>) {
        tabs.forEach((tab) => {
            this.addTab(tab)
        })
    }

    pinTab (unpinnedTab: Tab) {
        // Module data management
        const tab = this.standardTabs.find((tab) => {
            return tab.name === unpinnedTab.name
        })
        if (tab === undefined) {
            throw new Error(`Tab ${unpinnedTab.name} does not exists in module ${this.name} standard tabs`)
        }
        this._pinnedTabs.push(tab)
        this._standardTabs = this.standardTabs.filter((standardTab) => {
            return standardTab.name !== tab.name
        })

        this.updatePinnedTabs()

        // Update default module tab in cache
        if (this.pinnedTabs.length === 1 && tab.url) {
            const userStore = useUserStore()
            setTabForModule(userStore.sessionHash, this.name, tab.url)
        }
    }

    unpinTab (pinnedTab: Tab) {
        // Module data management
        const tab = this.pinnedTabs.find((tab) => {
            return tab.name === pinnedTab.name
        })
        if (tab === undefined) {
            throw new Error(`Tab ${pinnedTab.name} does not exists in module ${this.name} pinned tabs`)
        }
        this._standardTabs.push(tab)
        this._pinnedTabs = this._pinnedTabs.filter((pinnedTab) => {
            return pinnedTab.name !== tab.name
        })

        this.updatePinnedTabs()

        // Update default module tab in cache
        const userStore = useUserStore()
        if (this.pinnedTabs.length === 0) {
            removeTabForModule(userStore.sessionHash, this.name)
        }
        else {
            if (this.pinnedTabs[0].url !== undefined) {
                setTabForModule(userStore.sessionHash, this.name, this.pinnedTabs[0].url)
            }
        }
    }

    setPinnedTabs (pinnedTabs: Array<Tab>) {
        // Module data management
        this._pinnedTabs = pinnedTabs

        this.updatePinnedTabs()

        // Update default module tab in cache
        if (this.pinnedTabs[0].url !== undefined) {
            const userStore = useUserStore()
            setTabForModule(userStore.sessionHash, this.name, this.pinnedTabs[0].url)
        }
    }

    /**
     * Update pinned tabs for current module in database (via api call)
     * @private
     */
    private async updatePinnedTabs () {
        // Change type from tab to pinned_tab to suit the backend
        const pinnedTabs = this.pinnedTabs.map((tab) => {
            // create new tab to not alter reference
            const _tab = new Tab()
            _tab.type = "pinned_tab"
            _tab.attributes = { _tab_name: tab.name }
            return _tab
        })
        if (this.tabsUrl) {
            await createJsonApiObjects(pinnedTabs, this.tabsUrl)
        }
        else {
            addError(`No tabs url defined for module ${this.name}`)
            console.error(`No tabs url defined for module ${this.name}`)
        }
    }
}
