<!--
  @package Oxify\OxDialog
  @author  SAS OpenXtrem <dev@openxtrem.com>
  @license https://www.gnu.org/licenses/gpl.html GNU General Public License
-->

<script lang="ts">
import Vue from "vue"
import OxThemeCore from "@oxify/utils/OxThemeCore"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"
import OxDivider from "@oxify/components/OxDivider/OxDivider.vue"
import { uniqueId } from "lodash"

export default Vue.extend({
    name: "OxDialog",
    components: {
        OxDivider,
        OxIcon
    },
    props: {
        buttonLabel: String,
        fullscreen: Boolean,
        hideHeader: {
            type: Boolean,
            default: false
        },
        separated: Boolean,
        scrollable: Boolean,
        showButton: {
            type: Boolean,
            default: true
        },
        title: String,
        value: Boolean,
        width: [String, Number],
        maxWidth: [String, Number],
        compact: Boolean,
        fullHeight: Boolean,
        eager: Boolean,
        overflowAuthorized: Boolean,
        contentClass: String,
        persistent: {
            type: Boolean,
            default: false
        }
    },
    data () {
        return {
            iconColorOnBackground: OxThemeCore.onBackgroundMediumEmphasis,
            iconColorOnPrimary: OxThemeCore.onPrimaryHighEmphasis,
            toolbarColor: OxThemeCore.primary600,
            isScrolled: false,
            dialogContainer: null as HTMLDivElement | null,
            dialogId: uniqueId("dialog_")
        }
    },
    computed: {
        /**
         * Check if dialog has actions slot defined
         *
         * @return {boolean}
         */
        hasActions (): boolean {
            return !!this.$slots.actions
        },

        /**
         * Defines a default dialog transition for default & fullscreen modes
         *
         * @return {string}
         */
        dialogBottomTransition (): string {
            return this.fullscreen ? "dialog-bottom-transition" : "dialog-transition"
        },

        /**
         * Defines if we can show the dialog header on fullscreen mode
         *
         * @return {boolean}
         */
        showFullscreenHeader (): boolean {
            return !this.hideHeader && this.fullscreen
        },

        /**
         * Defines if we can show OxDivider components or "separated" classes in the component
         *
         * @return {boolean}
         */
        showDivider (): boolean {
            return this.separated && !this.fullscreen
        },

        dialogClasses (): string {
            let classes = "OxDialog " + this.dialogId

            if (this.compact) {
                classes += " compact"
            }

            if (this.fullHeight) {
                classes += " fullHeight"
            }

            if (this.fullscreen) {
                classes += " fullscreen"
            }

            if (this.overflowAuthorized) {
                classes += " overflowAuthorized"
            }

            if (this.isScrolled) {
                classes += " scrolled"
            }

            if (this.contentClass) {
                classes += ` ${this.contentClass}`
            }

            return classes
        },
        dividerRadialInset (): number {
            return this.compact ? 0 : 16
        },
        dividerClass (): { "OxDialog-dividerCompact": boolean } {
            return { "OxDialog-dividerCompact": this.compact }
        }
    },
    watch: {
        value: {
            async handler (newVal: boolean): Promise<void> {
                if (newVal) {
                    await this.$nextTick()

                    if (this.dialogContainer === null) {
                        this.dialogContainer = document.querySelector(`.OxDialog.${this.dialogId}`)
                    }
                    this.$emit("opened", this.dialogContainer)
                }
                this.toggleScrollEvent(newVal)
            }
        }
    },
    methods: {
        /**
         * Toggles "scroll" event on "OxDialog" element depending on the dialog is opened
         *
         * @param {boolean} dialogValue
         */
        toggleScrollEvent (dialogValue: boolean): void {
            if (this.dialogContainer === null) {
                return
            }
            if (dialogValue) {
                this.$nextTick(() => {
                    this.dialogContainer?.addEventListener("scroll", this.toggleIsScrolled)
                })
            }
            else {
                this.isScrolled = false
                this.dialogContainer.scrollTop = 0
                this.dialogContainer.removeEventListener("scroll", this.toggleIsScrolled)
            }
        },

        /**
         * Changing the current value by emitting new "input" event (=> enables v-model use on this component)
         *   => also toggle the "scroll" event on "OxDialog" element
         *
         * @param {boolean} value - New value
         */
        changeValue (value: boolean): void {
            this.toggleScrollEvent(value)
            this.$emit("input", value)
        },

        /**
         * Click event propagation
         *
         * @param {MouseEvent} event - click event
         */
        click (event: MouseEvent): void {
            this.$emit("click", event)
        },

        /**
         * Toggles isScrolled data depending on a scroll is active in the dialog
         *
         * @param {MouseEvent} event - scroll event
         */
        toggleIsScrolled (event: Event) {
            this.isScrolled = (event.target as HTMLElement).scrollTop > 0
        }
    }
})
</script>

<template>
  <v-dialog
    :content-class="dialogClasses"
    :eager="eager"
    :fullscreen="fullscreen"
    :max-width="maxWidth"
    :persistent="persistent"
    :scrollable="scrollable"
    style="z-index: 900;"
    :transition="dialogBottomTransition"
    :value="value"
    :width="width"
    @input="changeValue"
  >
    <template #activator="{ on, attrs }">
      <slot
        :attrs="attrs"
        name="activator"
        :on="on"
      />
    </template>
    <v-card>
      <!-- Fullscreen header -->
      <v-toolbar
        v-if="showFullscreenHeader"
        :color="toolbarColor"
      >
        <v-btn
          icon
          @click="changeValue(false)"
        >
          <ox-icon
            :color="iconColorOnPrimary"
            icon="cancel"
          />
        </v-btn>
        <v-toolbar-title>
          <h5 class="OxDialog-title fullscreen">
            {{ title }}
          </h5>
        </v-toolbar-title>
        <v-spacer />
        <v-toolbar-items v-if="showButton">
          <v-btn
            text
            @click="click"
          >
            <span class="OxDialog-buttonLabel">{{ buttonLabel }}</span>
          </v-btn>
        </v-toolbar-items>
      </v-toolbar>
      <!-- Default header -->
      <v-card-title
        v-else-if="!hideHeader"
        class="OxDialog-cardTitle"
      >
        <h5 class="OxDialog-title">
          {{ title }}
        </h5>
        <v-spacer />
        <v-btn
          icon
          @click="changeValue(false)"
        >
          <ox-icon
            :color="iconColorOnBackground"
            icon="cancel"
          />
        </v-btn>
      </v-card-title>
      <!-- Content -->
      <ox-divider
        v-if="showDivider"
        :class="dividerClass"
        :inset-radial="dividerRadialInset"
      />
      <v-card-text>
        <div
          class="OxDialog-content"
          :class="{fullscreen: fullscreen, separated: showDivider}"
        >
          <slot />
        </div>
      </v-card-text>
      <template v-if="hasActions">
        <ox-divider v-if="showDivider" />
        <v-card-actions>
          <v-spacer />
          <slot name="actions" />
        </v-card-actions>
      </template>
    </v-card>
  </v-dialog>
</template>

<style src="./OxDialog.scss" lang="scss"></style>
