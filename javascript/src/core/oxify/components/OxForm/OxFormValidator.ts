/**
 * @package Oxify\OxForm
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

export default class OxFormValidator {
    private _rules: { [key: string]: Array<Function> } = {}
    private currentRules: { [key: string]: Array<Function> } = {}

    /**
     * Initialize all the rules we want to automatically validate by the form
     *
     * @param {{ [key: string]: Array<Function> }} rules - The rules to validate (sorted by input "name")
     */
    constructor (rules?: { [key: string]: Array<Function> }) {
        if (rules) {
            this._rules = rules
        }
    }

    get rules (): { [p: string]: Array<Function> } {
        return this._rules
    }

    set rules (rules: { [key: string]: Array<Function> }) {
        this._rules = rules
    }

    /**
     * Retrieves all current validation rules available for an input
     *   => it returns empty array until the "bindRules" method hasn't be called (by the OxForm)
     *     => to avoid rules being validates before submitting form
     *
     * @param {string} inputName - Has to match with an input "name" prop in the current DOM
     * @return {Array<Function>}
     */
    getValidationRules (inputName: string): Array<Function> {
        return this.currentRules[inputName] ?? []
    }

    /**
     * Fills all current validation rules with the rules given at instance init
     */
    bindRules (): void {
        this.currentRules = this._rules
    }

    /**
     * Merges given rules for the given field name with rules already defined in the validator
     *
     * @param {string} name - Has to match with an input "name" prop in the current DOM
     * @param {Array<Function>} rules - The rules we want to add
     */
    setRules (name: string, rules: Array<Function>): void {
        let validatorRules: Array<Function> = []

        // Check for duplicate rules
        if (this._rules[name]) {
            this._rules[name].filter((rule) => {
                return !rules.includes(rule)
            })

            validatorRules = this._rules[name]
        }

        this._rules[name] = [...validatorRules, ...rules]
    }
}
