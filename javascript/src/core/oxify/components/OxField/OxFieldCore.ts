/**
 * @package Oxify\OxField
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { defineComponent, PropType } from "vue"
import { OxSelectElement } from "@oxify/types/OxFieldTypes"
import OxFormValidator from "@oxify/components/OxForm/OxFormValidator"
import { tr } from "@/core/utils/OxTranslator"

export const MANDATORY_SYMBOL = "*"

/**
 * OxField
 *
 * Field model component
 */
export default defineComponent({
    name: "OxFieldCore",
    props: {
        label: {
            type: String,
            default: ""
        },
        value: {
            /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
            type: [String, Boolean, Number, Array] as PropType<string | boolean | number | Array<any>>,
            default: null
        },
        disabled: {
            type: Boolean,
            default: false
        },
        dense: {
            type: Boolean,
            default: false
        },
        notNull: {
            type: Boolean,
            default: false
        },
        list: {
            type: Array as PropType<Array<OxSelectElement | string>>,
            default: () => []
        },
        message: {
            type: String,
            default: ""
        },
        icon: {
            type: String,
            default: ""
        },
        onPrimary: {
            type: Boolean,
            default: false
        },
        showLoading: {
            type: Boolean,
            default: false
        },
        placeholder: String,
        rules: {
            type: Array as PropType<Array<Function>>,
            default: () => []
        },
        name: String,
        validator: Object as PropType<OxFormValidator>
    },
    computed: {
        /**
         * Initial container base classes
         *
         * @return {Object}
         */
        fieldClasses (): object {
            return {
                labelled: this.label !== "",
                "not-null": this.notNull
            }
        },

        /**
         * Selectable item list
         *
         *
         * @return {Array<OxSelectElement>}
         */
        viewList (): OxSelectElement[] {
            return (this.list as OxSelectElement[]).map(
                (_el: OxSelectElement | string) => {
                    return typeof (_el) === "string" ? { view: _el, _id: _el } : _el
                }
            )
        },

        /**
         * Field text color
         *
         * @return {string|undefined}
         */
        fieldColor (): string | undefined {
            return this.onPrimary ? "rgba(255, 255, 255, 0.7)" : undefined
        },

        /**
         * Field background color
         *
         * @return {string|undefined}
         */
        fieldBG () : string | undefined {
            return this.onPrimary ? "#5C6BC0" : undefined
        },

        /**
         * Current field information message
         *
         * @return {string}
         */
        hint (): string {
            return this.message
        },

        /**
         * Field label
         *
         * @return {string}
         */
        labelComputed (): string {
            return this.label + (this.notNull && this.label ? " " + MANDATORY_SYMBOL : "")
        },

        /**
         * Generation of rules based on the specs rules and props
         *
         * @return {Array}
         */
        fieldRules (): Array<Function> {
            const rules = this.rules

            if (this.notNull) {
                rules.push(v => (!!v || tr("Missing-field")))
            }

            if (this.validator && this.name) {
                this.validator.setRules(this.name, rules)
                return this.validator.getValidationRules(this.name)
            }

            return rules
        }
    },
    methods: {
        /**
         * Field value change event
         * @param {string | boolean} value - New value
         */
        change (value: string | boolean | string[]): void {
            this.$emit("change", value)
            // Emit input event for double-way binding. TODO ref to remove change event
            this.$emit("input", value)
        }
    }
})
