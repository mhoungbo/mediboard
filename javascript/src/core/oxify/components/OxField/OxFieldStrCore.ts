/**
 * @package Oxify\OxField
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { defineComponent } from "vue"
import OxIconCore from "@oxify/components/OxIcon/OxIconCore"
import OxFieldCore from "@oxify/components/OxField/OxFieldCore"

export default defineComponent({
    name: "OxFieldStrCore",
    extends: OxFieldCore,
    props: {
        counter: [Boolean, Number],
        maxlength: Number
    },
    computed: {
        iconName (): string {
            return OxIconCore.get(this.icon)
        }
    }
})
