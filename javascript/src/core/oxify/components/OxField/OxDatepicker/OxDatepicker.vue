<!--
  @author  SAS OpenXtrem <dev@openxtrem.com>
  @license https://www.gnu.org/licenses/gpl.html GNU General Public License
  @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
-->

<script lang="ts">

import { defineComponent, PropType } from "vue"
import OxTextField from "@oxify/components/OxField/OxTextField/OxTextField.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"
import OxDate from "@/core/oxify/utils/OxDate"
import OxIconCore from "@oxify/components/OxIcon/OxIconCore"

export default defineComponent({
    name: "OxDatepicker",
    components: {
        OxButton
    },
    extends: OxTextField,
    props: {
        format: {
            type: String as PropType<"date" | "time" | "datetime" | "month">,
            default: "date"
        },
        defaultPickerDate: {
            type: String,
            default: ""
        },
        defaultPickerTime: {
            type: String,
            default: ""
        },
        showNow: {
            type: Boolean,
            default: true
        },
        showToday: {
            type: Boolean,
            default: true
        },

        // Props forwarding for datepicker limits dates
        minDate: {
            type: String,
            default: ""
        },
        maxDate: {
            type: String,
            default: ""
        },
        allowedDates: Function,

        // Props forwarding for timepicker limits times
        minTime: {
            type: String,
            default: ""
        },
        maxTime: {
            type: String,
            default: ""
        },
        allowedHours: [Function, Array] as PropType<Function | Array<number>>,
        allowedMinutes: [Function, Array] as PropType<Function | Array<number>>,
        allowedSeconds: [Function, Array] as PropType<Function | Array<number>>
    },
    data () {
        return {
            tmpDate: "",
            tmpTime: "",
            modal: false,
            modalMode: "date" as "date" | "time"
        }
    },
    computed: {
        /**
         * Gets label for date
         *
         * @return {string}
         */
        dateValue (): string {
            if (this.value === "") {
                return ""
            }
            const dateView = OxDate.formatStatic(
                new Date((this.format === "time" ? OxDate.getYMD(new Date()) + " " : "") + this.value),
                this.format
            )
            if (dateView.indexOf("NaN") >= 0) {
                return ""
            }

            return dateView
        },

        /**
         * Retrieves the right icon for the date format
         *
         * @return {string}
         */
        calendarIcon (): string {
            return OxIconCore.get(this.format === "time" ? "time" : "calendar")
        },

        /**
         * Retrieves the date type
         *
         * @return {string}
         */
        type (): string {
            return this.format === "month" ? "month" : "date"
        }
    },
    watch: {
        /**
         * Updates the current value
         * @Watch value
         */
        value () {
            this.updateTmpValues()
        },
        /**
         * Updates the value after a format change
         * @Watch format
         */
        format () {
            this.updateTmpValues()
        },
        /**
         * Updates the value after a format change
         * @Watch modal
         */
        modal () {
            this.resetModalMode()
        }
    },
    mounted () {
        this.updateTmpValues()
    },
    methods: {
        updateTmpValues (): void {
            this.setTmpValues(this.value ? this.value.toString() : "")
            this.resetModalMode()
        },
        resetModalMode (): void {
            if (!this.modal) {
                return
            }
            this.modalMode = this.format === "time" ? "time" : "date"
        },
        /**
         * Updates the current value from the entered value
         * @param {string} value - Entered value
         */
        setTmpValues (value: string): void {
            if (this.format === "date" || this.format === "month") {
                this.tmpDate = value || this.defaultPickerDate
            }
            else if (this.format === "time") {
                this.tmpTime = value && value !== this.tmpDate ? value : this.defaultPickerTime
            }
            else if (this.format === "datetime") {
                if (!value) {
                    this.tmpDate = this.defaultPickerDate ?? ""
                    this.tmpTime = this.defaultPickerTime ?? ""

                    if (this.defaultPickerDate && this.defaultPickerTime) {
                        value = this.defaultPickerDate + " " + this.defaultPickerTime
                    }
                }

                if (value && value.length) {
                    this.tmpDate = value.length >= 10 ? value.substr(0, 10) : ""
                    this.tmpTime = value.length >= 16 ? value.substr(11, 10) : ""
                }
            }

            if (this.tmpDate === "") {
                this.tmpDate = OxDate.getYMD(new Date())
            }
            if (this.tmpTime === "") {
                this.tmpTime = OxDate.getHms(new Date())
            }
        },

        /**
         * Current date assignment
         */
        today (): void {
            this.tmpDate = OxDate.getYMD(new Date())
        },

        /**
         * Current time assignment
         */
        now (): void {
            this.tmpTime = OxDate.getHms(new Date())
        },

        /**
         * New date value change assignment
         */
        updateDate (): void {
            this.modal = false
            const newValue = ((this.format === "date" || this.format === "month" || this.format === "datetime") ? this.tmpDate : "") +
        ((this.format === "datetime") ? " " : "") +
        ((this.format === "datetime" || this.format === "time") ? this.tmpTime : "")
            this.change(newValue)
        },

        /**
         * Year selection
         * @param {string} year - Year selected
         */
        selectYear (year: string): void {
            const date = new Date(this.tmpDate)
            date.setFullYear(parseInt(year))
            this.tmpDate = OxDate.getYMD(date)
        },

        /**
         * Month selection
         * @param {string} month - Month selected
         */
        selectMonth (month: string): void {
            const date = new Date(this.tmpDate)
            date.setMonth(parseInt(month.split("-")[1]) - 1)
            this.tmpDate = OxDate.getYMD(date)
        },

        /**
         * Day selection
         * @param {string} day - Day selected
         */
        selectDate (day: string): void {
            this.tmpDate = day
        },

        /**
         * Hour selection
         * @param {string} hour - Hour selected
         */
        selectHour (hour: string): void {
            const date = new Date(this.tmpDate + " " + this.tmpTime)
            date.setHours(parseInt(hour))
            this.tmpTime = OxDate.getHms(date)
        },

        /**
         * Minute selection
         * @param {string} minute - Minute selected
         */
        selectMinute (minute: string): void {
            const date = new Date(this.tmpDate + " " + this.tmpTime)
            date.setMinutes(parseInt(minute))
            this.tmpTime = OxDate.getHms(date)
        }
    }
})
</script>

<template>
  <v-dialog
    ref="dialog-date"
    v-model="modal"
    width="400px"
  >
    <template #activator="{ on }">
      <v-text-field
        :append-icon="calendarIcon"
        :background-color="fieldBG"
        :clearable="!notNull"
        :color="fieldColor"
        :dark="onPrimary"
        :disabled="disabled"
        filled
        hide-details="auto"
        :hint="hint"
        :label="labelComputed"
        :loading="showLoading"
        persistent-hint
        readonly
        :rules="fieldRules"
        :value="dateValue"
        v-on="on"
        @click:append="modal = true"
        @input="change"
      />
    </template>
    <v-date-picker
      v-if="modalMode === 'date'"
      v-model="tmpDate"
      :allowed-dates="allowedDates"
      first-day-of-week="1"
      full-width
      :max="maxDate"
      :min="minDate"
      scrollable
      :type="type"
      @click:date="selectDate"
      @click:month="selectMonth"
      @click:year="selectYear"
    >
      <ox-button
        v-if="showToday"
        button-style="tertiary"
        :label="$tr('Today')"
        @click="today"
      />
      <v-spacer v-if="showToday" />
      <ox-button
        button-style="secondary"
        :label="$tr('Cancel')"
        @click="modal = false"
      />
      <v-spacer />
      <ox-button
        v-if="format === 'datetime'"
        button-style="primary"
        :label="$tr('Next')"
        @click="modalMode = 'time'"
      />
      <ox-button
        v-else
        button-style="primary"
        :label="$tr('Validate')"
        @click="updateDate"
      />
    </v-date-picker>
    <v-time-picker
      v-else-if="modalMode === 'time'"
      v-model="tmpTime"
      :allowed-hours="allowedHours"
      :allowed-minutes="allowedMinutes"
      :allowed-seconds="allowedSeconds"
      format="24hr"
      :max="maxTime"
      :min="minTime"
      @click:hour="selectHour"
      @click:minute="selectMinute"
    >
      <ox-button
        v-if="showNow"
        button-style="tertiary"
        :label="$tr('Now')"
        @click="now"
      />
      <v-spacer v-if="showNow" />
      <ox-button
        v-if="format === 'time'"
        button-style="secondary"
        :label="$tr('Cancel')"
        @click="modal = false"
      />
      <ox-button
        v-if="format === 'datetime'"
        button-style="secondary"
        :label="$tr('common-Return')"
        @click="modalMode = 'date'"
      />
      <v-spacer />
      <ox-button
        button-style="primary"
        :label="$tr('Validate')"
        @click="updateDate"
      />
    </v-time-picker>
  </v-dialog>
</template>
