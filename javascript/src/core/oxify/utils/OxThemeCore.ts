/**
 * @package Oxify\OxThemeCore
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

/**
 * OxThemeCore
 */
export default class OxThemeCore {
    public static primary50 = "var(--primary-50)"
    public static primary100 = "var(--primary-100)"
    public static primary200 = "var(--primary-200)"
    public static primary300 = "var(--primary-300)"
    public static primary400 = "var(--primary-400)"
    public static primary600 = "var(--primary-600)"
    public static primary700 = "var(--primary-700)"
    public static primary800 = "var(--primary-800)"
    public static primary900 = "var(--primary-900)"
    public static primary = "var(--primary-500)"

    public static secondary = "var(--secondary-500)"
    public static secondary50 = "var(--secondary-50)"
    public static secondary100 = "var(--secondary-100)"
    public static secondary200 = "var(--secondary-200)"
    public static secondary300 = "var(--secondary-300)"
    public static secondary400 = "var(--secondary-400)"
    public static secondary600 = "var(--secondary-600)"
    public static secondary700 = "var(--secondary-700)"
    public static secondary800 = "var(--secondary-800)"
    public static secondary900 = "var(--secondary-900)"

    public static onBackgroundMediumEmphasis = "var(--color-on-background-medium)"
    public static onBackgroundHighEmphasis = "var(--color-on-background-high)"
    public static onPrimaryMediumEmphasis = "var(--color-on-primary-medium)"
    public static onPrimaryHighEmphasis = "var(--color-on-primary-high)"

    public static backgroundDefaultLight = "var(--background-default-light)"
    public static backgroundDefault = "var(--background-default)"

    public static yellowText = "var(--color-text-yellow)"
    public static blueText = "var(--color-text-blue)"
    public static pinkText = "var(--color-text-pink)"
    public static pinkTextLighter = "var(--color-text-pink-lighter)"
    public static orangeText = "var(--color-text-orange)"

    public static pink = "var(--color-pink)"
    public static purple = "var(--color-purple)"
    public static deepPurple = "var(--color-deep-purple)"
    public static blue = "var(--color-blue)"
    public static cyan = "var(--color-cyan)"
    public static teal = "var(--color-teal)"
    public static green = "var(--color-green)"
    public static lightGreen = "var(--color-light-green)"
    public static lime = "var(--color-lime)"
    public static yellow = "var(--color-yellow)"
    public static orange = "var(--color-orange)"

    public static successTextDefault = "var(--success-text-default)"
    public static successSurfaceDefault = "var(--success-surface-default)"
    public static successSurfaceLight = "var(--success-surface-light)"
    public static errorTextDefault = "var(--error-text-default)"
    public static errorSurfaceDefault = "var(--error-surface-default)"
    public static errorSurfaceLight = "var(--error-surface-light)"
    public static warningTextDefault = "var(--warning-text-default)"
    public static warningSurfaceDefault = "var(--warning-surface-default)"
    public static warningSurfaceLight = "var(--warning-surface-light)"
    public static infoTextDefault = "var(--info-text-default)"
    public static infoSurfaceDefault = "var(--info-surface-default)"
    public static infoSurfaceLight = "var(--info-surface-light)"

    public static grey = "var(--grey-900)"
    public static grey50 = "var(--grey-50)"
    public static grey100 = "var(--grey-100)"
    public static grey200 = "var(--grey-200)"
    public static grey300 = "var(--grey-300)"
    public static grey400 = "var(--grey-400)"
    public static grey500 = "var(--grey-500)"
    public static grey600 = "var(--grey-600)"
    public static grey700 = "var(--grey-700)"
    public static grey800 = "var(--grey-800)"
    public static grey900 = "var(--grey-900)"

    public static error = "var(--error-surface-default)"

    public static transparent: "transparent"

    public static colorSurfacePrimary800 = "var(--color-surface-primary-800)"
    public static colorSurfacePrimary700 = "var(--color-surface-primary-700)"
    public static colorSurfacePrimary600 = "var(--color-surface-primary-600)"
    public static colorSurfacePrimary500 = "var(--color-surface-primary-500)"
    public static colorSurfacePrimary300 = "var(--color-surface-primary-300)"
    public static colorSurfacePrimary100 = "var(--color-surface-primary-100)"

    public static colorSurfaceSecondary900 = "var(--color-surface-secondary-900)"
    public static colorSurfaceSecondary800 = "var(--color-surface-secondary-800)"

    public static colorPink = "var(--color-pink)"
    public static colorPurple = "var(--color-purple)"
    public static colorDeepPurple = "var(--color-deep-purple)"
    public static colorBlue = "var(--color-blue)"
    public static colorCyan = "var(--color-cyan)"
    public static colorTeal = "var(--color-teal)"
    public static colorGreen = "var(--color-green)"
    public static colorLightGreen = "var(--color-light-green)"
    public static colorLime = "var(--color-lime)"
    public static colorYellow = "var(--color-yellow)"
    public static colorOrange = "var(--color-orange)"

    /**
     * Convert Hex color value to rgba
     * @param { string } color - The color to convert
     * @param { number } opacity - Opacity from 0 to 1
     */
    public static alpha (color: string, opacity: number): string {
        if (color === "") {
            return ""
        }
        if (color.startsWith("var")) {
            const rgbColor = color.slice(0, -1) + "-rgb)"
            return `rgba(${rgbColor}, ${opacity})`
        }
        if (color.startsWith("#")) {
            return color + Math.round(opacity * 255).toString(16)
        }
        return ""
    }
}
