/**
 * @package Oxify\OxTest
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

import { VueClass, Wrapper, shallowMount } from "@vue/test-utils"
import { Component } from "vue"
import ProvidesCallback = jest.ProvidesCallback
import Module from "@/core/models/Module"
import { useNavStore } from "@/core/stores/nav"

/**
 * Test Class Wrapper
 */
export default class OxTest {
    /* eslint-disable  @typescript-eslint/no-explicit-any */
    protected component!: VueClass<any> | any
    /* eslint-disable  @typescript-eslint/no-explicit-any */
    protected scenarios!: {[key: string]: any[][]}

    /**
     * Retrieving the class wrapper for the test
     * @param props
     * @param stubs
     *
     * @return {Wrapper<Vue>}
     */
    private getWrapper (props: object = {}, stubs: { [key: string]: Component | string | boolean } | string[] = {}): Wrapper<Vue> {
        return shallowMount(
            this.component,
            {
                propsData: props,
                mocks: {},
                stubs,
                methods: {}
            }
        ) as Wrapper<Vue>
    }

    /**
     * Mounted component
     * @param props {object} - Tested component initialization parameters
     * @param stubs {object} - Mocked child elements
     *
     * @return {Vue}
     */
    protected mountComponent (props: object = {}, stubs: { [key: string]: Component | string | boolean } | string[] = {}): Wrapper<Vue> {
        return this.getWrapper(props, stubs) as Wrapper<Vue>
    }

    /**
     * Tested Vue component
     * @param props {object} - Tested component initialization parameters
     * @param stubs {object} - Mocked child elements
     *
     * @return {Vue}
     */
    protected vueComponent (props: object = {}, stubs: { [key: string]: Component | string | boolean } | string[] = {}): Vue {
        return this.mountComponent(props, stubs).vm as Vue
    }

    /*
     * protected mockAxios (): void {
     *
     * }
     *
     * protected mockAxiosOnce (): void {
     *
     * }
     *
     * protected mockAxiosByUrl (url: string): void {
     *
     * }
     */

    /**
     * Treatment applied before testing a class
     */
    /* eslint-disable  @typescript-eslint/no-empty-function */
    protected beforeAllTests (): void {
    }

    /**
     * Treatment applied before each test of a class
     */
    /* eslint-disable  @typescript-eslint/no-empty-function */
    protected beforeTest (): void {
    }

    /**
     * Treatment applied after testing a class
     */
    /* eslint-disable  @typescript-eslint/no-empty-function */
    protected afterAllTests (): void {
    }

    /**
     * Treatment applied after each test of a class
     */
    /* eslint-disable  @typescript-eslint/no-empty-function */
    protected afterTest (): void {
    }

    /**
     * Launch of class tests
     */
    public launchTests (): void {
        describe(
            this.component.name ? this.component.name : this.component,
            () => {
                beforeAll(() => this.beforeAllTests())
                afterAll(() => this.afterAllTests())
                beforeEach(() => this.beforeTest())
                afterEach(() => this.afterTest())
                const obj = Object.getPrototypeOf(this)
                for (const methodName of Object.getOwnPropertyNames(obj)) {
                    if (!methodName.match(/^test[A-Z]/g)) {
                        continue
                    }
                    const method = this[methodName]
                        .bind(this)
                    if (!this.scenarios || !this.scenarios[methodName]) {
                        this.launchTest(this.labelizeTest(methodName), method)
                        continue
                    }
                    for (const scenario of this.scenarios[methodName]) {
                        const scenarioName = scenario.shift()
                        this.launchTest(
                            this.labelizeTest(methodName, scenarioName),
                            async () => {
                                await method(...scenario)
                            }
                        )
                    }
                }
            }
        )
    }

    /**
     * Launch of a specific test
     * @param testLabel {string} - Test label
     * @param testMethod {Function} - Function to execute
     */
    private launchTest (testLabel: string, testMethod: ProvidesCallback): void {
        test(testLabel, testMethod)
    }

    /**
     * Scenario decorator (Use like "OxTest.scenarios(['Scenario Name'], firstInjectedVar, secondInjectedVar, ...])")
     * @param scenariosValues { Array<Array<any>> }
     */
    static scenarios (...scenariosValues: any[]): Function {
        return function (target: OxTest, propertyKey: string) {
            if (!target.scenarios) {
                target.scenarios = {}
            }
            target.scenarios[propertyKey] = scenariosValues
        }
    }

    /**
     * Create test label
     * @param methodName {object} - Method name
     *
     * @return {Vue}
     */
    private labelizeTest (methodName: string, scenario = ""): string {
        return this.component.name + ":" + methodName.replace("test", "") + scenario
    }

    /**
     * Private method call wizard
     * @param object {any} - Calling object
     * @param property {string} - Method or property name
     * @param functionArgs {Array[any]} - Method called arguments
     *
     * @throws {Error}
     * @return any
     */
    protected privateCall (object: any, property: string, ...functionArgs: any[]): any {
        /*
         * if (typeof object[property] === "undefined") {
         *     throw new Error("Undefined property name " + property + " in " + object.constructor.name)
         * }
         */
        if (typeof object[property] === "function") {
            return object[property](...functionArgs)
        }
        return object[property]
    }

    /**
     * Universal Object Protected-Private Setter
     * @param object {any} - Target object
     * @param property {string} - NProperty name
     * @param newValue {any} - Value to insert
     *
     * @throws {Error}
     * @return any
     */
    protected setPrivateData (object: any, property: string, newValue: any): any {
        if (typeof object[property] === "undefined") {
            throw new Error("Undefined property name " + property)
        }
        object[property] = newValue
        return object[property]
    }

    /**
     * Waiting for a set number of seconds
     * @param delay {number} - Number of seconds
     *
     * @return {Promise<void>}
     */
    protected async wait (delay: number): Promise<void> {
        await new Promise((resolve) => {
            setTimeout(resolve, delay)
        })
    }

    /**
     * Use to check that an object has a .length property and it is set to a certain numeric value
     * @param testedElement {any} - Tested element
     * @param number {number} - Comparative length
     */
    protected assertHaveLength (testedElement: any, number: number): void {
        expect(testedElement).toHaveLength(number)
    }

    /**
     * Use to check that an object has a .length property and it is not set to a certain numeric value
     * @param testedElement {any} - Tested element
     * @param number {number} - Comparative length
     */
    protected assertNotHaveLength (testedElement: any, number: number): void {
        expect(testedElement).not.toHaveLength(number)
    }

    /**
     * Use to check if property at provided reference keyPath exists for an object.
     * Optionally, you can provide a value to check if it's equal to the value present at keyPath on the target object.
     * @param testedElement {any} - Tested element
     * @param keyPath {string} - Wanted property
     * @param value {any} - Wanted property value
     */
    protected assertHaveProperty (testedElement: any, keyPath: string, value?: any): void {
        expect(testedElement).toHaveProperty(keyPath, value)
    }

    /**
     * Use to check if property at provided reference keyPath doesn't exists for an object.
     * @param testedElement {any} - Tested element
     * @param keyPath {string} - Wanted property
     * @param value {any} - Wanted property value
     */
    protected assertNotHaveProperty (testedElement: any, keyPath: string, value?: any): void {
        expect(testedElement).not.toHaveProperty(keyPath, value)
    }

    /**
     * Use to check the approximate approach between two floating point numbers
     * @param testedElement {number} - Tested element
     * @param number {number} - Compared number
     * @param numDigits {number} - Number of digit after comma, comparison precision
     */
    protected assertCloseTo (testedElement: number, number: number, numDigits?: number): void {
        expect(testedElement).toBeCloseTo(number, numDigits)
    }

    /**
     * Use to check the approximate difference between two floating point numbers
     * @param testedElement {number} - Tested element
     * @param number {number} - Compared number
     * @param numDigits {number} - Number of digit after comma, comparison precision
     */
    protected assertNotCloseTo (testedElement: number, number: number, numDigits?: number): void {
        expect(testedElement).not.toBeCloseTo(number, numDigits)
    }

    /**
     * Ensure that a variable is not undefined.
     * @param testedElement {any} - Tested element
     */
    protected assertDefined (testedElement: any): void {
        expect(testedElement).toBeDefined()
    }

    /**
     * Use to check that an element is false (cast element if not boolean)
     * @param testedElement {any} - Tested element
     */
    protected assertFalse (testedElement: any): void {
        expect(testedElement).toBeFalsy()
    }

    /**
     * Use to check that an element is upper than given number
     * @param testedElement {any} - Tested element
     * @param number {number} - Compared number
     */
    protected assertGreaterThan (testedElement: any, number: number): void {
        expect(testedElement).toBeGreaterThan(number)
    }

    /**
     * Use to check that an element is upper or equal than given number
     * @param testedElement {any} - Tested element
     * @param number {number} - Compared number
     */
    protected assertGreaterThanOrEqual (testedElement: any, number: number): void {
        expect(testedElement).toBeGreaterThanOrEqual(number)
    }

    /**
     * Use to check that an element is lower than given number
     * @param testedElement {any} - Tested element
     * @param number {number} - Compared number
     */
    protected assertLessThan (testedElement: any, number: number): void {
        expect(testedElement).toBeLessThan(number)
    }

    /**
     * Use to check that an element is lower or equal than given number
     * @param testedElement {any} - Tested element
     * @param number {number} - Compared number
     */
    protected assertLessThanOrEqual (testedElement: any, number: number): void {
        expect(testedElement).toBeLessThanOrEqual(number)
    }

    /**
     * Use to check that an element is an instance of a given class
     * @param testedElement {any} - Tested element
     * @param className {class} - Expected class
     */
    protected assertInstanceOf (testedElement: any, objectClass: any): void {
        expect(testedElement).toBeInstanceOf(objectClass)
    }

    /**
     * Use to check that an element is not an instance of a given class
     * @param testedElement {any} - Tested element
     * @param className {class} - Unexpected class
     */
    protected assertNotInstanceOf (testedElement: any, objectClass: any): void {
        expect(testedElement).not.toBeInstanceOf(objectClass)
    }

    /**
     * Use to check that an element is null
     * @param testedElement {any} - Tested element
     */
    protected assertNull (testedElement: any): void {
        expect(testedElement).toBeNull()
    }

    /**
     * Use to check that an element is not null
     * @param testedElement {any} - Tested element
     */
    protected assertNotNull (testedElement: any): void {
        expect(testedElement).not.toBeNull()
    }

    /**
     * Use to check that an element is true (cast element if not boolean)
     * @param testedElement {any} - Tested element
     */
    protected assertTrue (testedElement: any): void {
        expect(testedElement).toBeTruthy()
    }

    /**
     * Use to check that an element is undefined
     * @param testedElement {any} - Tested element
     */
    protected assertUndefined (testedElement: any): void {
        expect(testedElement).toBeUndefined()
    }

    /**
     * Use to check that an element is NaN
     * @param testedElement {any} - Tested element
     */
    protected assertNaN (testedElement: any): void {
        expect(testedElement).toBeNaN()
    }

    /**
     * Use to check that an element is not NaN
     * @param testedElement {any} - Tested element
     */
    protected assertNotNaN (testedElement: any): void {
        expect(testedElement).not.toBeNaN()
    }

    /**
     * Use to check that an element contains a given item
     * @param testedElement {any} - Tested element
     * @param item {any} - Expected item
     */
    protected assertContain (testedElement: any, item: any): void {
        expect(testedElement).toContain(item)
    }

    /**
     * Use to check that an element not contains a given item
     * @param testedElement {any} - Tested element
     * @param item {any} - Unexpected item
     */
    protected assertNotContain (testedElement: any, item: any): void {
        expect(testedElement).not.toContain(item)
    }

    /**
     * Use to check that an element contains a given item
     * @param testedElement {any} - Tested element
     * @param item {any} - Expected item
     */
    protected assertContainEqual (testedElement: any, item: any): void {
        expect(testedElement).toContainEqual(item)
    }

    /**
     * Use to check that an element not contains a given item
     * @param testedElement {any} - Tested element
     * @param item {any} - Expected item
     */
    protected assertNotContainEqual (testedElement: any, item: any): void {
        expect(testedElement).not.toContainEqual(item)
    }

    /**
     * Use to check that two objects have the same value
     * @param testedElement {any} - Tested element
     * @param value {any} - Comparison value
     */
    protected assertEqual (testedElement: any, value: any): void {
        expect(testedElement).toEqual(value)
    }

    /**
     * Use to check that two objects have not the same value
     * @param testedElement {any} - Tested element
     * @param value {any} - Comparison value
     */
    protected assertNotEqual (testedElement: any, value: any): void {
        expect(testedElement).not.toEqual(value)
    }

    /**
     * Use to check that a string matches a regular expression or a string
     * @param testedElement {any} - Tested element
     * @param reg {RegExp|string} - Expected expression
     */
    protected assertMatch (testedElement: any, reg: RegExp | string): void {
        expect(testedElement).toMatch(reg)
    }

    /**
     * Use to check that a string not matches a regular expression or a string
     * @param testedElement {any} - Tested element
     * @param reg {RegExp|string} - Unexpected expression
     */
    protected assertNotMatch (testedElement: any, reg: RegExp | string): void {
        expect(testedElement).not.toMatch(reg)
    }

    /**
     * Use to test that objects have the same types as well as structure
     * @param testedElement {any} - Tested element
     * @param value {any} - Comparison value
     */
    protected assertStrictEqual (testedElement: any, value: any): void {
        expect(testedElement).toStrictEqual(value)
    }

    /**
     * Use to test that objects have not the same types as well as structure
     * @param testedElement {any} - Tested element
     * @param value {any} - Comparison value
     */
    protected assertNotStrictEqual (testedElement: any, value: any): void {
        expect(testedElement).not.toStrictEqual(value)
    }

    /**
     * Used to test that a function throws when it is called
     * @param testedElement {any} - Tested function
     * @param error {string | Constructable | RegExp | Error} - Error
     */
    protected assertThrow (testedElement: any, error?: any): void {
        expect(testedElement).toThrow(error)
    }

    /**
     * Used to test that a function not throws when it is called
     * @param testedElement {any} - Tested function
     * @param error {string | Constructable | RegExp | Error} - Error
     */
    protected assertNotThrow (testedElement: any, error?: any): void {
        expect(testedElement).not.toThrow(error)
    }

    /**
     * Flushes current Promises by adding a new one "awaitable" in the execution stack
     *
     * @return {Promise<void>}
     */
    protected async flushPromises (): Promise<void> {
        await this.wait(0)
    }
}

export async function flushPromises (): Promise<void> {
    await new Promise((resolve) => {
        setTimeout(resolve, 0)
    })
}

/**
 * Add modules given is param in active modules list in test env
 */
export function addActiveModules (...moduleNames: Array<string>) {
    const activeModules = moduleNames.map((moduleName) => {
        return new Module(moduleName)
    })

    const navStore = useNavStore()
    navStore.modules = [...navStore.modules, ...activeModules]
}

/**
 * Set modules given is param as active in test env
 */
export function setActiveModules (...moduleNames: Array<string>) {
    const activeModules = moduleNames.map((moduleName) => {
        return new Module(moduleName)
    })

    const navStore = useNavStore()
    navStore.modules = activeModules
}

/**
 * Remove modules given is param in active modules list in test env
 */
export function removeActiveModules (...moduleNames: Array<string>) {
    const navStore = useNavStore()
    navStore.modules = navStore.modules.filter((module) => {
        return !moduleNames.includes(module.name)
    })
}
