/**
 * @package Oxify\utils
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

// Colors also define in style/variablesStyles.scss

const primaryLight = Object.freeze({
    default: "#3F51B5",
    900: "#1A237E",
    800: "#283593",
    700: "#303F9F",
    600: "#3949AB",
    500: "#3F51B5",
    400: "#5C6BC0",
    300: "#7986CB",
    200: "#9FA8DA",
    100: "#CFD4F3",
    50: "#E8EAF6"
})
const secondaryLight = Object.freeze({
    default: "#03A9F4",
    900: "#01579B",
    800: "#0277BD",
    700: "#0277BD",
    600: "#039BE5",
    500: "#03A9F4",
    400: "#29B6F6",
    300: "#4FC3F7",
    200: "#81D4FA",
    100: "#B3E5FC",
    50: "#E1F5FE"
})
const errorLight = Object.freeze({
    surfaceDefault: "#F44336",
    surfaceDarken: "#D94848",
    textDefault: "#FF3100",
    textOnPrimary: "#FF8B70"
})
const infoLight = Object.freeze({
    surfaceDefault: "#1976D2",
    textDefault: "#1E88E5"
})
const successLight = Object.freeze({
    surfaceDefault: "#388E3C",
    textDefault: "#43A047"
})
const warningLight = Object.freeze({
    surfaceDefault: "#E65100",
    textDefault: "#FF9800"
})

const primaryDark = Object.freeze({
    default: "#9FA8DA",
    900: "#BAC0E5",
    800: "#B6BDE3",
    700: "#B0B8E1",
    600: "#A9B1DE",
    500: "#9FA8DA",
    400: "#8994D1",
    300: "#7380C9",
    200: "#5362B6",
    100: "#303E8F",
    50: "#242F72"
})
const secondaryDark = Object.freeze({
    default: "#4FC3F7",
    900: "#A9E2FB",
    800: "#96DBFA",
    700: "#84D5F9",
    600: "#69CCF8",
    500: "#4FC3F7",
    400: "#48BDF6",
    300: "#3FB5F5",
    200: "#35A0DD",
    100: "#308BC5",
    50: "#316C96"
})
const errorDark = Object.freeze({
    surfaceDefault: "#F44336",
    surfaceDarken: "#D94848",
    textDefault: "#FF3100",
    textOnPrimary: "#FF8B70"
})
const infoDark = Object.freeze({
    surfaceDefault: "#1976D2",
    textDefault: "#1E88E5"
})
const successDark = Object.freeze({
    surfaceDefault: "#388E3C",
    textDefault: "#43A047"
})
const warningDark = Object.freeze({
    surfaceDefault: "#E65100",
    textDefault: "#FF9800"
})

const lightTheme = Object.freeze({
    primary: primaryLight,
    secondary: secondaryLight,
    error: errorLight,
    info: infoLight,
    success: successLight,
    warning: warningLight
})
const darkTheme = Object.freeze({
    primary: primaryDark,
    secondary: secondaryDark,
    error: errorDark,
    info: infoDark,
    success: successDark,
    warning: warningDark
})
export default Object.freeze({
    lightTheme,
    darkTheme
})
