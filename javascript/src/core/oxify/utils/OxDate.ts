/**
 * @package Oxify\OxTest
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */
import { tr } from "@/core/utils/OxTranslator"

export default class OxDate {
    /**
     * Complete string of zeros
     * @param {string} char - base text
     * @param {number} nb - Target character number
     *
     * @return {string}
     */
    static completeNum (char: number, nb: number): string {
        let charString = char.toString()
        while (charString.length < nb) {
            charString = "0" + charString
        }
        return charString
    }

    /**
     * Return date at YYYY-MM-DD format
     * @param {Date} date - Date to format
     *
     * @return {string}
     */
    static getYMD (date: Date = new Date()): string {
        return OxDate.completeNum(date.getFullYear(), 4) + "-" +
            OxDate.completeNum((date.getMonth() + 1), 2) + "-" +
            OxDate.completeNum(date.getDate(), 2)
    }

    /**
     * Return date at HH:ii format
     * @param {Date} date - Date to format
     *
     * @return {string}
     */
    static getHm (date: Date): string {
        return OxDate.completeNum(date.getHours(), 2) + ":" +
            OxDate.completeNum(date.getMinutes(), 2)
    }

    /**
     * Return date at HH:ii:ss format
     * @param {Date} date - Date to format
     *
     * @return {string}
     */
    static getHms (date: Date): string {
        return OxDate.completeNum(date.getHours(), 2) + ":" +
            OxDate.completeNum(date.getMinutes(), 2) + ":" +
            OxDate.completeNum(date.getSeconds(), 2)
    }

    /**
     * Return date at YYYY-MM-DD HH:ii format
     * @param {Date} date - Date to format
     *
     * @return {string}
     */
    static getYMDHm (date: Date): string {
        return OxDate.getYMD(date) + " " + OxDate.getHm(date)
    }

    /**
     * Return date at YYYY-MM-DD HH:ii:ss format
     * @param {Date} date - Date to format
     *
     * @return {string}
     */
    static getYMDHms (date: Date): string {
        return OxDate.getYMD(date) + " " + OxDate.getHms(date)
    }

    /**
     * static method for dateFormat
     * @param {Date} date - Date to format
     * @param {string} mode - Date format
     *
     * @return {string}
     */
    static formatStatic (date: Date, mode: "month" | "day" | "date" | "datetime" | "time" | "completeday"): string {
        let formatedDate = ""
        if (!date) {
            return formatedDate
        }
        if (mode === "day" || mode === "completeday") {
            formatedDate += ["L", "M", "M", "J", "V", "S", "D"][(date.getDay() - 1)] + " " + OxDate.completeNum(date.getDate(), 2) + " "
        }
        if (mode === "month" || mode === "completeday") {
            formatedDate += [
                tr("OxDate-ShortMonth-Janvier"),
                tr("OxDate-ShortMonth-Fevrier"),
                tr("OxDate-ShortMonth-Mars"),
                tr("OxDate-ShortMonth-Avril"),
                tr("OxDate-ShortMonth-Mai"),
                tr("OxDate-ShortMonth-Juin"),
                tr("OxDate-ShortMonth-Juillet"),
                tr("OxDate-ShortMonth-Aout"),
                tr("OxDate-ShortMonth-Septembre"),
                tr("OxDate-ShortMonth-Octobre"),
                tr("OxDate-ShortMonth-Novembre"),
                tr("OxDate-ShortMonth-Decembre")][date.getMonth()] + ". "
        }
        if (mode === "date" || mode === "datetime") {
            formatedDate += OxDate.completeNum(date.getDate(), 2) + "/" + OxDate.completeNum(date.getMonth() + 1, 2) + "/"
        }
        if (mode === "date" || mode === "datetime" || mode === "month" || mode === "completeday") {
            formatedDate += OxDate.completeNum(date.getFullYear(), 4)
        }
        if (mode === "datetime") {
            formatedDate += " "
        }
        if (mode === "time" || mode === "datetime") {
            formatedDate += OxDate.completeNum(date.getHours(), 2) + ":" + OxDate.completeNum(date.getMinutes(), 2)
        }
        return formatedDate
    }

    /**
     * Difference between two dates
     * @param {Date} from - Minimum date
     * @param {Date} to - Maximum date
     *
     * @return {Object}
     */
    static diff (from: Date, to: Date):
        {
            sec: number
            min: number
            hou: number
            day: number
            string: string
        } {
        /* eslint-disable  @typescript-eslint/no-explicit-any */
        let dateDiff = ((to as any) - (from as any)) / 1000
        const diff = {
            sec: 0,
            min: 0,
            hou: 0,
            day: 0,
            string: ""
        }

        diff.day = Math.floor(dateDiff / (60 * 60 * 24))
        dateDiff -= diff.day * (60 * 60 * 24)

        diff.hou = Math.floor(dateDiff / (60 * 60))
        dateDiff -= diff.hou * (60 * 60)

        diff.min = Math.floor(dateDiff / (60))
        dateDiff -= diff.min * (60)

        diff.sec = dateDiff

        // Day display
        if (diff.day > 0) {
            diff.string += diff.day + " " + tr("day") + " "
        }
        // Hours display
        if (diff.hou > 0) {
            diff.string += ((diff.hou < 10) ? "0" : "") + diff.hou + "h"
        }
        // Minutes display
        diff.string += ((diff.hou !== 0 && diff.min < 10) ? "0" : "") + diff.min + ((diff.hou === 0) ? "min" : "")

        return diff
    }

    /**
     * Test on the viability of a string to be a date
     * Test sur la viabilit? d'une chaine de caract?re en date
     * @param {string} date - Text to test
     *
     * @return {boolean}
     */
    static isDate (date: string): boolean {
        return ((!!date.match(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/)) ||
                !!date.match(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])) [012][0-9](:[0-5][0-9]){2}$/)) &&
            (new Date(date)).toString() !== tr("Date-invalid")
    }

    /**
     * Test on the viability of a string to be a time
     * @param {string} time - Text to test
     *
     * @return {boolean}
     */
    static isTime (time: string): boolean {
        return !!time.match(/^[0-9]{2}:[0-5][0-9](:[0-9]{2})?$/)
    }

    /**
     * Automatically determines the date format from a string
     * @param {string} date - Date label to test
     *
     * @return {string} - Date format ("date", "time" or "datetime")
     */
    static getAutoFormat (date: string): "month" | "day" | "date" | "datetime" | "time" | "completeday" {
        if (date.length === 10) {
            return "date"
        }
        if (date.length === 5 || date.length === 8) {
            return "time"
        }
        return "datetime"
    }

    /**
     * Return a HHhii format date to a HH:ii:ss format date
     * @param {string} date - Time to format
     *
     * @return {string}
     */
    static beautifyTime (date: string): string {
        if (date.length === 8 || date.length === 19) {
            date = date.slice(0, -3)
        }
        return date.replace(":", "h")
    }
}
