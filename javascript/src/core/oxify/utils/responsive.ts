/**
 * @package Oxify\Utils
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

// Screen sizes also defined in style/utils.scss

const screenWide = Object.freeze({
    900: 2100,
    800: 2000,
    700: 1900,
    600: 1800,
    500: 1700,
    400: 1600,
    300: 1500,
    200: 1400,
    100: 1300
})

const screenSmall = Object.freeze({
    900: 1200,
    800: 1100,
    700: 1000,
    600: 900,
    500: 800,
    400: 700,
    300: 600,
    200: 500,
    100: 400
})

export {
    screenWide,
    screenSmall
}
