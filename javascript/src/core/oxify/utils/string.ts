/**
 * @package Oxify\Utils
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

/**
 * Capitalize the first letter of a String
 *
 * @param value
 */
export function capitalize (value: string): string {
    return value.charAt(0).toUpperCase() + value.slice(1)
}
