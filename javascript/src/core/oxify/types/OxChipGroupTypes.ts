export interface OxChipGroupChoice {
    label: string
    value: string
}
