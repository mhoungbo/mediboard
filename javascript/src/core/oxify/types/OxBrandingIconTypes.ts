export interface OxBrandingIconPath {
    d: string
    fill: string
}

export type OxBrandingIconType = "prosante" | "gitlab" | "google" | "cpx" | "dmp" | "aati" | "adri" | "aldi" | "cdri"
    | "hri" | "insi" | "dmti" | "imti" | "appfine"
