export default interface OxSorterChoice {
    value: string
    label: string
}
