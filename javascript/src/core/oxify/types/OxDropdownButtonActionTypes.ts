export interface OxDropdownButtonActionTypes {
    label: string
    eventName: string
    icon?: string
    iconColor?: string
    disabled?: boolean
    hide?: boolean
    customIcon?: boolean
}

export interface OxDropdownButtonActionsGroups {
    label: string
    actions: OxDropdownButtonActionTypes[]
    hide?: boolean
}
