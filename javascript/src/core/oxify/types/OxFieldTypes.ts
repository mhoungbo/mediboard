export interface OxSelectElement {
    view?: string
    _id?: string
    [key: string]: any
}
