/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { defineStore } from "pinia"
import Configurations from "@modules/system/vue/models/Configurations"
import Function from "@modules/mediusers/vue/Models/Function"
import Group from "@modules/dPetablissement/vue/models/Group"
import Vue from "vue"

/**
 * Store containing navigation context data
 */
export const useContextStore = defineStore("context", {
    state: () => ({
        functions: [] as Array<Function>,
        currentGroup: new Group(),
        configurations: new Configurations()
    }),
    getters: {
        hasCurrentGroup (): boolean {
            return !!this.currentGroup.id
        },
        currentFunction (): Function {
            const currentFunction = this.functions.find((func) => {
                return func.groupId?.toString() === this.currentGroup.id.toString()
            })

            return currentFunction as Function ?? new Function()
        }
    },
    actions: {
        addConfiguration (key: string, value) {
            Vue.set(this.configurations.attributes, key, value)
        }
    }
})
