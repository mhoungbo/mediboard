/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { defineStore } from "pinia"
import Mediuser from "@modules/mediusers/vue/Models/Mediuser"
import Preferences from "@modules/system/vue/models/Preferences"
import { OxObjectAttributes, OxObjectLinks } from "@/core/types/OxObjectTypes"
import Vue from "vue"
import Group from "@modules/dPetablissement/vue/models/Group"
import Function from "@modules/mediusers/vue/Models/Function"

/**
 * Store containing connected user's data
 */
export const useUserStore = defineStore("user", {
    state: () => ({
        user: new Mediuser(),
        sessionHash: "",
        preferences: new Preferences(),
        userGroup: new Group(),
        userFunction: new Function()
    }),
    getters: {
        hasSessionHash (): boolean {
            return !!this.sessionHash
        },
        hasCurrentUser (): boolean {
            return !!this.user.id
        },
        hasUserGroup (): boolean {
            return !!this.userGroup.id
        },
        hasUserFunction (): boolean {
            return !!this.userFunction.id
        },
        // Get for ignore type bug with unwrap ref, see https://github.com/vuejs/core/issues/2981
        getUser (): Mediuser {
            return this.user as Mediuser
        }
    },
    actions: {
        addPreference (key: string, value) {
            Vue.set(this.preferences.attributes, key, value)
        },
        addUserAttributes (attributes: OxObjectAttributes) {
            this.user.attributes = { ...this.user.attributes, ...attributes }
        },
        addUserLinks (links: OxObjectLinks) {
            this.user.links = { ...this.user.links, ...links }
        }
    }
})
