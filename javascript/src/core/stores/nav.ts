/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { defineStore } from "pinia"
import Module from "@/core/models/Module"
import { TabBadgeModel } from "@/components/Appbar/types/AppbarTypes"
import { tr } from "@/core/utils/OxTranslator"

/**
 * Store containing navigation context data
 */
export const useNavStore = defineStore("nav", {
    state: () => ({
        currentModuleName: undefined as undefined | string,
        tabActive: undefined as undefined | string,
        tabBadges: [] as Array<TabBadgeModel>,
        // Active modules
        modules: [] as Array<Module>
    }),
    getters: {
        currentModule (): Module {
            const currentModule = this.modules.find((module) => {
                return module.name === this.currentModuleName
            })

            if (currentModule === undefined) {
                return new Module(this.currentModuleName)
            }

            return currentModule as Module
        },
        tabName (): string {
            if (!this.currentModuleName || !this.tabActive) {
                throw new Error("Impossible to get current module nor active tab")
            }
            return tr("mod-" + this.currentModuleName + "-tab-" + this.tabActive)
        },
        getTabBadge: (state) => (moduleName: string, tabName: string) => {
            return state.tabBadges.find((tabBadge: TabBadgeModel) => {
                return tabBadge.module_name === moduleName && tabBadge.tab_name === tabName
            }) as TabBadgeModel | undefined
        },
        currentTabIsParam (): boolean {
            return this.currentModule.paramTabs.some((tab) => {
                return tab.name === this.tabActive
            })
        },
        currentTabIsConfig (): boolean {
            return this.currentModule.configureTabs.some((tab) => {
                return tab.name === this.tabActive
            })
        },
        modulesVisible (): Array<Module> {
            return this.modules.filter((module) => {
                return module.visible
            }) as Array<Module> // Mandatory type redeclaration, see https://github.com/vuejs/core/issues/2981
        }
    },
    actions: {
        updateTabBadge (tabBadge: TabBadgeModel) {
            if (this.getTabBadge(tabBadge.module_name, tabBadge.tab_name) !== undefined) {
                this.tabBadges = this.tabBadges.filter((_tabBadge) => {
                    return _tabBadge.module_name !== tabBadge.module_name || _tabBadge.tab_name !== tabBadge.tab_name
                })
            }
            this.tabBadges.push(tabBadge)
        }
    }
})
