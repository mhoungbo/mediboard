import OxObject from "@/core/models/OxObject"
import { isModuleActive } from "@/core/utils/OxModulesManager"
import User from "@modules/admin/vue/models/User"
import MedicalCode from "@modules/system/vue/models/MedicalCode"

const modulesModelsName = "dPcompteRendu"
let OxHelpedText = OxObject
let helpedText = new OxObject()

if (isModuleActive(modulesModelsName)) {
    // @ts-ignore
    import("@modules/dPcompteRendu/vue/models/OxHelpedText").then((mod) => {
        OxHelpedText = mod.default
        helpedText = new OxHelpedText()
    })
}

const modulesPatientName = "dPpatients"
let Patient = OxObject
let patient = new OxObject()

if (isModuleActive(modulesPatientName)) {
    // @ts-ignore
    import("@modules/dPpatients/vue/models/Patient").then((mod) => {
        Patient = mod.default
        patient = new Patient()
    })
}

export const OxRichTextFieldLineHeight = 20
export const OxRichTextFieldAutocompleteArrowUp = "ArrowUp"
export const OxRichTextFieldAutocompleteArrowDown = "ArrowDown"
export const OxRichTextFieldAutocompleteEnter = "Enter"
export const OxRichTextFieldAutocompleteEscape = "Escape"
export const OxRichTextFieldAutocompleteShortcuts = [
    OxRichTextFieldAutocompleteArrowUp,
    OxRichTextFieldAutocompleteArrowDown,
    OxRichTextFieldAutocompleteEnter,
    OxRichTextFieldAutocompleteEscape
]

export interface OxRichTextOptions {
    objectType: string
    field: string
    dependValue1?: string | null
    dependValue2?: string | null
    classDependsValue1?: string | null
    classDependsValue2?: string | null
}

export interface OxQuickActionsOptions {
    usersQuickAction?: boolean
    codesQuickAction?: boolean
    patientsQuickAction?: boolean
    authorizedCodes?: Array<"ccam" | "cim10" | "loinc" | "snomed" | "ngap">
}

export interface OxCursorNodes {
    startNode: Node
    endNode: Node
    startPos: number
    endPos: number
}

export interface OxQuickActionEmits {
    type: "user" | "code" | "patient"
    word: string
}

export interface OxRichTextFieldAutocompletePosition {
    top?: string
    left?: string
    right?: string
    bottom?: string
    maxWidth?: string
    maxHeight?: string
}

export interface OxRichTextExecutedActions {
    helpedTexts: typeof helpedText[]
    users: User[]
    patients: typeof patient[]
    medicalCodes: MedicalCode[]
}
