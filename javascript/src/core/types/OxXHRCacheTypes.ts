/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { OxJsonApi } from "@/core/types/OxApiTypes"

export interface OxXHRCacheConfig {
    useCache?: boolean
    freshnessTime?: number
    useEtag?: boolean
}

export interface OxXHRCacheItem {
    timestamp: number
    data: OxJsonApi
    etag: string
}
