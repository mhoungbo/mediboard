export type OxMomentInput = Date | string | number | (number | string)[] | null | undefined;
export type OxUnitOfTime = "years" | "quarters" | "months" | "weeks" | "days" | "day" | "hours" | "minutes" | "seconds"
