/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { OxObjectAttributes } from "@/core/types/OxObjectTypes"
import OxObject from "@/core/models/OxObject"
import OxCollection from "@/core/models/OxCollection"

export interface OxJsonApiDataLinks {
    self?: string
    schema?: string
    history?: string
    [key: string]: string | undefined
}

export interface OxJsonApiRelation {
    type: string,
    id: string,
    relation?: string,
    attributes?: OxObjectAttributes
}

export interface OxJsonApiRelationships {
    [key: string]: {
        data: OxJsonApiRelation | OxJsonApiRelation[]
    }
}

export interface OxJsonApiMeta {
    [key: string]: string | Object | number | undefined
    permissions?: {[key: string]: string}
}

export interface OxJsonApiData {
    id: string
    type: string
    links?: OxJsonApiDataLinks
    attributes: OxObjectAttributes,
    relationships?: OxJsonApiRelationships,
    meta?: OxJsonApiMeta
}

export interface OxJsonApiLinks {
    self?: string
    first?: string
    last?: string
    next?: string
    prev?: string
}

export interface OxApiSchema {
    data: any
}

export interface OxApiMeta {
    authors?: string
    count?: number
    date: string,
    copyright: string,
    schema?: OxApiSchema,
    total?: number
}

export interface OxApiError {
    code: number
    message: string
    type: string
}

export interface OxJsonApi {
    data: OxJsonApiData | OxJsonApiData[]
    links: OxJsonApiLinks
    included: OxJsonApiData[]
    meta?: OxApiMeta
    errors?: OxApiError
}

export interface OxJsonApiBulkOperation {
    id: string
    method: "GET" | "POST" | "PATCH" | "DELETE"
    path: string
    parameters?: {
        [key: string]: string
    }
    body?: Partial<OxJsonApi>
}

export interface OxJsonApiBulkResponse {
    id: string
    status: number
    body: OxJsonApi
}

export interface OxJsonResponseError {
    config: Object
    data: {
        errors: OxApiError
    }
    headers: Object,
    status: number,
    statusText: string
}

export interface OxJsonApiBulkResponseChunk {
    status: "error" | "success",
    body: OxObject | OxCollection<OxObject> | string
}

export interface OxJsonApiBulkResult {
    [key: string]: OxJsonApiBulkResponseChunk
}
