/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

// Ignore camelcase because attributes are given by back-end
/* eslint-disable camelcase */

export interface OxEntryPointLinks {
    [key: string]: string
}

export interface OxEntryPointConfigs {
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    [key: string]: any
    date_format: string
    is_distinct_groups: boolean
    is_qualif: boolean
}

export interface OxEntryPointPrefs {
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    [key: string]: any
    is_dark: "0" | "1"
}

export interface OxEntryPointMeta {
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    [key: string]: any
}

export interface OxEntryPointLocales {
    [key: string]: string
}

export interface OxEntryPointTokens {
    [key: string]: string
}

export interface OxEntryPointCurrentUser {
    id: string,
    guid: string,
    view: string,
    login: string,
    type: string,
    last_name: string,
    first_name: string,
    color: string
}

export interface OxEntryPointCurrentFunction {
    id: string,
    guid: string,
    view: string,
}

export interface OxEntryPointCurrentGroup {
    id: string,
    guid: string,
    view: string,
}
