/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import moment from "moment"
import { OxMomentInput, OxUnitOfTime } from "@/core/types/OxMomentTypes"

export default class OxMoment {
    private moment

    constructor (momentParams?: OxMomentInput, momentFormat?: string, language = "fr", strict = true) {
        this.moment = moment(momentParams, momentFormat, language, strict)
    }

    public format (format?: string): string {
        return this.moment.format(format)
    }

    public fromNow (withoutSuffix?: boolean): string {
        return this.moment.fromNow(withoutSuffix)
    }

    public add (amount: number, unit: OxUnitOfTime): string {
        return this.moment.add(amount, unit)
    }

    public subtract (amount: number, unit: OxUnitOfTime): string {
        return this.moment.subtract(amount, unit)
    }

    public isSame (inp: Date | string | number, granularity: OxUnitOfTime): boolean {
        return this.moment.isSame(inp, granularity)
    }

    public isSameOrAfter (inp: Date | string | number, granularity: OxUnitOfTime): boolean {
        return this.moment.isSameOrAfter(inp, granularity)
    }
}
