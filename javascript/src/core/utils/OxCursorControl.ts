/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import {
    OxCursorNodes,
    OxRichTextFieldAutocompletePosition,
    OxRichTextFieldLineHeight
} from "@/core/types/OxRichTextTypes"

/**
 * Get the cursor nodes and positions
 */
export function getCursorNodes (): OxCursorNodes | undefined {
    const selection = window.getSelection()

    if (selection !== null && selection.rangeCount !== 0) {
        const range = selection.getRangeAt(0)
        return {
            startNode: range.startContainer,
            endNode: range.endContainer,
            startPos: range.startOffset,
            endPos: range.endOffset
        }
    }
    return undefined
}

/**
 * Get the cursor position as X and Y and remove the offset of the written word
 * @param word
 * @param element
 */
export function getAutocompletePosition (word, element: HTMLDivElement): OxRichTextFieldAutocompletePosition | undefined {
    const selection = window.getSelection()

    // If we don't have any selection
    if (selection === null || selection.rangeCount <= 0) {
        return
    }

    const range = selection.getRangeAt(0).cloneRange()
    // If the selection is outside the container
    if (!element.contains(range.commonAncestorContainer)) {
        return
    }

    // Move the cursor at the start of the first character of the word
    if (selection.anchorNode && selection.focusNode) {
        const anchorPos = selection.anchorOffset - word.length < 0 ? 0 : selection.anchorOffset - word.length
        range.setStart(selection.anchorNode, anchorPos)
        range.setEnd(selection.focusNode, selection.focusOffset)
    }

    // We get the bounding box of the range
    const rect = range.getBoundingClientRect()
    const scrollTop = window.scrollY || document.documentElement.scrollTop
    const scrollLeft = window.scrollX || document.documentElement.scrollLeft
    const { offsetTop, offsetLeft } = calculateParentsOffset(element)

    // Initiate finals values for the placement
    const top = rect.top + scrollTop - offsetTop + OxRichTextFieldLineHeight
    const left = rect.left + scrollLeft - offsetLeft
    const right = element.offsetWidth - (rect.right + scrollLeft - offsetLeft)
    const bottom = element.offsetHeight - (rect.bottom + scrollTop - offsetTop - OxRichTextFieldLineHeight)

    // If top has been miscalculated place it at the default position
    if (top < 0) {
        return { top: `${OxRichTextFieldLineHeight + 48}px`, left: "8px" }
    }

    return calculatePositionForAutocomplete(
        top,
        left,
        bottom,
        right,
        offsetTop - scrollTop,
        offsetLeft - scrollLeft,
        (window.innerHeight - element.offsetHeight - offsetTop - scrollTop),
        (window.innerWidth - element.offsetWidth - offsetLeft - scrollLeft)
    )
}

function calculateParentsOffset (element: Element): {offsetTop: number, offsetLeft: number} {
    let offsetTop = 0
    let offsetLeft = 0

    /** @var parent HTMLDivElement */
    let parent = element as Element | null
    /*
     * Then we climb up all the parent until there is no parent or the parent isn't a HTMLElement
     * We need to do that to consider all the different offset of all the parents
     * Without this part, the placement will be right on some pages and wrong on other pages
     */
    while (parent && parent instanceof HTMLElement) {
        offsetTop += parent.offsetTop
        offsetLeft += parent.offsetLeft
        parent = parent.offsetParent
    }

    return { offsetTop, offsetLeft }
}

function calculatePositionForAutocomplete (
    top: number,
    left: number,
    bottom: number,
    right: number,
    offsetTop: number,
    offsetLeft: number,
    offsetBottom: number,
    offsetRight: number
): OxRichTextFieldAutocompletePosition {
    // Build up the final value
    const returnValue = {} as OxRichTextFieldAutocompletePosition

    const halfWidthScreen = window.innerWidth / 2
    if ((left + offsetLeft) > halfWidthScreen) {
        returnValue.right = `${right}px`
        returnValue.maxWidth = `${window.innerWidth - offsetRight - right - 16}px`
    }
    else {
        returnValue.left = `${left}px`
        returnValue.maxWidth = `${window.innerWidth - offsetLeft - left - 16}px`
    }

    const threeFifthOfScreenHeight = (window.innerHeight / 5) * 3
    if ((top + offsetTop) > threeFifthOfScreenHeight) {
        returnValue.bottom = `${bottom}px`
        returnValue.maxHeight = `${window.innerHeight - offsetBottom - bottom - 16}px`
    }
    else {
        returnValue.top = `${top}px`
        returnValue.maxHeight = `${window.innerHeight - offsetTop - top - 16}px`
    }

    return returnValue
}

/**
 * Place the cursor to a specific child node and at a specific index
 * @param child
 * @param index
 */
export function setCursorToAChild (child: Node, index: number) {
    setCursor(child, index, child, index)
}

/**
 * Place the cursor to the selected children and positions
 * @param startChild
 * @param startIndex
 * @param endChild
 * @param endIndex
 */
export function setCursor (startChild: Node, startIndex: number, endChild: Node, endIndex: number) {
    const range = document.createRange()
    const sel = window.getSelection()
    if (sel !== null) {
        range.setStart(startChild, startIndex)
        range.setEnd(endChild, endIndex)
        sel.removeAllRanges()
        sel.addRange(range)
    }
}

if (process.env.NODE_ENV === "test") {
    module.exports.calculateParentsOffset = calculateParentsOffset
    module.exports.calculatePositionForAutocomplete = calculatePositionForAutocomplete
}
