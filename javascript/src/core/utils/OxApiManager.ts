/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import OxObject from "@/core/models/OxObject"
import OxCollection from "@/core/models/OxCollection"
import { OxApiError, OxJsonApi } from "@/core/types/OxApiTypes"
import {
    createJsonApiPostSkeleton,
    createJsonApiUpdateFieldsSkeleton,
    createJsonApiUpdateSkeleton,
    dataTransformer,
    extractSchemasFromJsonApi,
    includedTransformer
} from "@/core/utils/OxJsonApiTransformer"
import { storeObjects, storeSchemas } from "@/core/utils/OxStorage"
import oxApiService from "@/core/utils/OxApiService"
import { OxCollectionMeta } from "@/core/types/OxCollectionTypes"
import { OxObjectAttributes } from "@/core/types/OxObjectTypes"
import { OxXHRCacheConfig } from "@/core/types/OxXHRCacheTypes"
import { getCachedData, xhrCacheEnabled } from "@/core/utils/OxXHRCache"
import axios, { AxiosResponse } from "axios"
import { useUserStore } from "@/core/stores/user"

/**
 * Return an OxObject from an url
 * @param {OxObject} ObjectType - Type of OxObject that should be returned
 * @param {string} url - Resource url
 * @param {OxXHRCacheConfig} cacheConfig - Cache configuration for api call
 *
 * @returns {OxObject}
 */
export async function getObjectFromJsonApiRequest<DataType extends OxObject> (
    ObjectType: new() => DataType,
    url: string,
    cacheConfig?: OxXHRCacheConfig
): Promise<DataType> {
    let dataJson: OxJsonApi
    if (cacheConfig?.useCache && xhrCacheEnabled()) {
        const userStore = useUserStore()
        dataJson = await getCachedData(userStore.sessionHash, url, cacheConfig?.freshnessTime, cacheConfig?.useEtag)
    }
    else {
        dataJson = (await oxApiService.get<OxJsonApi>(url)).data
    }

    return getObjectFromJsonApi(ObjectType, dataJson)
}

/**
 * Return a OxCollection from an url
 * @param {OxObject} ObjectType - Type of OxObject that should be returned
 * @param {string} url - Resource url
 * @param {OxXHRCacheConfig} cacheConfig - Cache configuration for api call
 *
 * @returns {OxCollection}
 */
export async function getCollectionFromJsonApiRequest<DataType extends OxObject> (
    ObjectType: new() => DataType,
    url: string,
    cacheConfig?: OxXHRCacheConfig
): Promise<OxCollection<DataType>> {
    let dataJson: OxJsonApi

    if (cacheConfig?.useCache && xhrCacheEnabled()) {
        const userStore = useUserStore()
        dataJson = await getCachedData(userStore.sessionHash, url, cacheConfig?.freshnessTime, cacheConfig?.useEtag)
    }
    else {
        dataJson = (await oxApiService.get<OxJsonApi>(url)).data
    }

    return getCollectionFromJsonApi(ObjectType, dataJson)
}

/**
 * Return an OxObject from a JSON:API
 * @param {OxObject} ObjectType - Type of OxObject that should be returned
 * @param {OxJsonApi} json - The JSON:API containing the object
 *
 * @returns {OxObject}
 */
export function getObjectFromJsonApi<DataType extends OxObject> (
    ObjectType: new() => DataType,
    json: OxJsonApi
): DataType {
    let result = dataTransformer(ObjectType, json)

    if (Array.isArray(result)) {
        // If is array containing once element, get this element
        if (result.length === 1) {
            result = result[0]
        }
        else {
            throw new Error("Get array instead of object")
        }
    }

    storeIncluded(ObjectType, json)
    storeSchemas(extractSchemasFromJsonApi(json))

    return result
}

/**
 * Return a OxCollection from a JSON:API
 * @param {OxObject} ObjectType - Type of OxObject that should be returned
 * @param {OxJsonApi} json - The JSON:API containing the objects
 *
 * @returns {OxCollection}
 */
export function getCollectionFromJsonApi<DataType extends OxObject> (
    ObjectType: new() => DataType,
    json: OxJsonApi
): OxCollection<DataType> {
    const collection = new OxCollection<DataType>()
    let result = dataTransformer(ObjectType, json)

    if (!Array.isArray(result)) {
        result = [result]
    }

    collection.objects = result
    collection.links = json.links
    collection.meta = json.meta as OxCollectionMeta | undefined

    storeIncluded(ObjectType, json)
    storeSchemas(extractSchemasFromJsonApi(json))

    return collection
}

export function getErrorFromJsonApi (json: OxJsonApi): OxApiError {
    const error = json.errors

    if (error === undefined) {
        throw new Error("No error found in json")
    }

    return error
}

/**
 * Post to backend given objects in JSON:API format
 * @param objects - Object or objects to store
 * @param {string } url - Url to call
 */
export async function createJsonApiObjects<O extends OxObject> (
    objects: O,
    url: string
): Promise<O>;
export async function createJsonApiObjects<O extends OxObject> (
    objects: O[],
    url: string
): Promise<OxCollection<O>>;
export async function createJsonApiObjects (
    objects: [],
    url: string
): Promise<[]>;
export async function createJsonApiObjects<O extends OxObject> (
    objects: O | O[] | [],
    url: string
): Promise<O | OxCollection<O> | []> {
    const isArray = Array.isArray(objects)
    const response = (await oxApiService.post<OxJsonApi>(url, createJsonApiPostSkeleton(objects))).data

    if (isArray && !objects.length) {
        return []
    }
    else if (isArray) {
        return getCollectionFromJsonApi(objects[0].constructor as new() => O, response)
    }
    else {
        return getObjectFromJsonApi(objects.constructor as new() => O, response)
    }
}

/**
 * Update backend object based on modified fields between original object and mutated object
 * @param {OxObject} from - Original object
 * @param {OxObject} to - Mutated object
 *
 * @returns {OxObject} The updated object
 */
export async function updateJsonApiObject<O extends OxObject> (from: O, to: O): Promise<O> {
    if (from.constructor.name !== to.constructor.name) {
        throw new Error("No matching object type between " + from.constructor.name + " and " + to.constructor.name)
    }

    if (to.links.self === undefined) {
        throw new Error("Missing self links on object")
    }

    const response = (await oxApiService.patch<OxJsonApi>(to.links.self, createJsonApiUpdateSkeleton(from, to))).data
    return getObjectFromJsonApi(to.constructor as new() => O, response)
}

/**
 * Update backend object based on given attribute's values
 *
 * @param {OxObject} object - Object to update
 * @param {OxObjectAttributes} attributes - Attribute's values we want to update on the given object
 *
 * @returns {OxObject} The updated object
 */
export async function updateJsonApiObjectFields<O extends OxObject> (
    object: O,
    attributes: OxObjectAttributes
): Promise<O> {
    if (object.links.self === undefined) {
        throw new Error("Missing self links on object")
    }

    const response = (await oxApiService.patch<OxJsonApi>(
        object.links.self,
        createJsonApiUpdateFieldsSkeleton(object, attributes))
    ).data
    return getObjectFromJsonApi(object.constructor as new() => O, response)
}

/**
 * Delete backend object
 * @param {OxObject} object - Original object
 *
 * @returns {boolean} Success request
 */
export async function deleteJsonApiObject<O extends OxObject> (object: O): Promise<boolean> {
    if (object.links.self === undefined) {
        throw new Error("Missing self links on object")
    }

    await oxApiService.delete<OxJsonApi>(object.links.self)

    return true
}

/**
 * Check if cached resource is up to date with etag
 * @param {string} url - The url to call to receive data
 * @param {string} etag - Etag of cached resource
 *
 * @returns {Promise<AxiosResponse<OxJsonApi> | false>} The axios response if resource has changed, false otherwise
 */
export async function checkChanges (url: string, etag: string): Promise<AxiosResponse<OxJsonApi> | false> {
    const result = await axios.get(url, {
        headers: {
            "Content-Type": "application/vnd.api+json",
            "X-Requested-With": "XMLHttpRequest",
            "If-None-Match": etag
        },
        validateStatus: (status: number) => status >= 200 && status < 400
    })
    if (result.status === 200) {
        // Object updated
        return result
    }
    if (result.status === 304) {
        return false
    }
    // If back doesn't return 200 ou 304, there is a problem
    throw new Error(`Status response ${result.status} different of 200 or 304`)
}

/**
 * Store included of given JSON:API
 *
 * @param {OxObject} ObjectType - Type of OxObject that should be store
 * @param {OxJsonApi} json - The JSON:API containing the included
 */
function storeIncluded<T extends OxObject> (ObjectType: new () => T, json: OxJsonApi) {
    const included = includedTransformer(ObjectType, json)
    if (included.length) {
        storeObjects(included)
    }
}
