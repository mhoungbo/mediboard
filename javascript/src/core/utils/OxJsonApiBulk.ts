/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import {
    OxJsonApiBulkOperation,
    OxJsonApiBulkResponse,
    OxJsonApiBulkResponseChunk,
    OxJsonApiBulkResult
} from "@/core/types/OxApiTypes"
import OxObject from "@/core/models/OxObject"
import {
    createJsonApiPostSkeleton,
    createJsonApiSkeleton,
    createJsonApiUpdateFieldsSkeleton,
    createJsonApiUpdateSkeleton
} from "@/core/utils/OxJsonApiTransformer"
import { OxObjectAttributes } from "@/core/types/OxObjectTypes"
import oxApiService from "@/core/utils/OxApiService"
import { getCollectionFromJsonApi, getErrorFromJsonApi, getObjectFromJsonApi } from "@/core/utils/OxApiManager"
import { OxUrlBuilder } from "@/core/utils/OxUrlTools"
import { sanitizeHtml } from "@/core/utils/OxFunctions"
import { OxUrlDiscovery } from "@/core/utils/OxUrlDiscovery"

export class OxJsonApiBulk {
    private readonly OPERATION_LIMIT = 10

    private _operations: OxJsonApiBulkOperation[] = []
    private _objectMapping: { [key: string]: { object: new() => OxObject, isCollection: boolean } } = {}
    private _stopOnFailure = false

    constructor (stopOnFailure = false) {
        this._stopOnFailure = stopOnFailure
    }

    get operations (): OxJsonApiBulkOperation[] {
        return this._operations
    }

    delete<O extends OxObject> (key: string, object: O): this {
        if (object.self === undefined) {
            throw new Error("Missing self links on object")
        }
        const operation: OxJsonApiBulkOperation = {
            id: key,
            method: "DELETE",
            path: object.self
        }
        this.addOperation(operation)
        this._objectMapping[key] = {
            object: object.constructor as new() => OxObject,
            isCollection: false
        }

        return this
    }

    create<O extends OxObject> (
        key: string,
        objects: O | O[],
        url: string,
        parameters?: { [key: string]: string }
    ): this {
        const operation: OxJsonApiBulkOperation = {
            id: key,
            method: "POST",
            path: url,
            parameters,
            body: createJsonApiPostSkeleton(objects)
        }
        this.addOperation(operation)
        if (Array.isArray(objects)) {
            this._objectMapping[key] = {
                object: objects[0].constructor as new() => OxObject,
                isCollection: true
            }
        }
        else {
            this._objectMapping[key] = {
                object: objects.constructor as new() => OxObject,
                isCollection: false
            }
        }

        return this
    }

    update<O extends OxObject> (
        key: string,
        from: O,
        to: O,
        parameters?: { [key: string]: string }
    ): this {
        if (from.constructor.name !== to.constructor.name) {
            throw new Error("No matching object type between " + from.constructor.name + " and " + to.constructor.name)
        }

        if (to.self === undefined) {
            throw new Error("Missing self links on object")
        }

        const operation: OxJsonApiBulkOperation = {
            id: key,
            method: "PATCH",
            path: to.self,
            parameters,
            body: createJsonApiUpdateSkeleton(from, to)
        }
        this.addOperation(operation)
        this._objectMapping[key] = {
            object: to.constructor as new() => OxObject,
            isCollection: false
        }

        return this
    }

    updateFields<O extends OxObject> (
        key: string,
        object: O,
        attributes: OxObjectAttributes,
        parameters?: { [key: string]: string }
    ): this {
        if (object.self === undefined) {
            throw new Error("Missing self links on object")
        }

        const operation: OxJsonApiBulkOperation = {
            id: key,
            method: "PATCH",
            path: object.self,
            parameters,
            body: createJsonApiUpdateFieldsSkeleton(object, attributes)
        }
        this.addOperation(operation)
        this._objectMapping[key] = {
            object: object.constructor as new() => OxObject,
            isCollection: false
        }

        return this
    }

    getObject<O extends OxObject> (
        key: string,
        objectType: new() => O,
        url: string,
        parameters?: { [key: string]: string }
    ): this {
        const operation: OxJsonApiBulkOperation = {
            id: key,
            method: "GET",
            path: url,
            parameters
        }
        this.addOperation(operation)
        this._objectMapping[key] = {
            object: objectType,
            isCollection: false
        }

        return this
    }

    getCollection<O extends OxObject> (
        key: string,
        objectType: new() => O,
        url: string,
        parameters?: { [key: string]: string }
    ): this {
        const operation: OxJsonApiBulkOperation = {
            id: key,
            method: "GET",
            path: url,
            parameters
        }
        this.addOperation(operation)
        this._objectMapping[key] = {
            object: objectType,
            isCollection: true
        }

        return this
    }

    stopOnFailure (): this {
        this._stopOnFailure = true
        return this
    }

    async resolve (): Promise<OxJsonApiBulkResult> {
        const resolveResult: OxJsonApiBulkResult = {}

        const bulkUrl = new OxUrlBuilder(OxUrlDiscovery.bulk())
        if (this._stopOnFailure) {
            bulkUrl.addParameter("stopOnFailure", "true")
        }
        const response = (await oxApiService.post<Array<OxJsonApiBulkResponse>>(
            bulkUrl.toString(),
            createJsonApiSkeleton(this.operations))
        ).data
        response.forEach((bulkElement) => {
            const responseChunk: OxJsonApiBulkResponseChunk = { status: "success", body: "" }

            // Error handling
            if (bulkElement.status >= 400) {
                responseChunk.status = "error"
                responseChunk.body = sanitizeHtml(getErrorFromJsonApi(bulkElement.body).message)
            }
            else if (!bulkElement.body) {
                responseChunk.body = ""
            }
            else {
                responseChunk.body = this._objectMapping[bulkElement.id].isCollection
                    ? getCollectionFromJsonApi(this._objectMapping[bulkElement.id].object, bulkElement.body)
                    : getObjectFromJsonApi(this._objectMapping[bulkElement.id].object, bulkElement.body)
            }
            resolveResult[bulkElement.id] = responseChunk
        })

        return resolveResult
    }

    private addOperation (operation: OxJsonApiBulkOperation) {
        if (this._operations.length >= this.OPERATION_LIMIT) {
            throw new Error("Max bulk operation limit reached")
        }
        const isKeyExists = this._objectMapping[operation.id] !== undefined
        if (isKeyExists) {
            throw new Error(`Key ${operation.id} already exists on bulk`)
        }

        this._operations.push(operation)
    }
}
