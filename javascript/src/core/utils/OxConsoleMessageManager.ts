/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Show OxPlatform welcome message in console
 */
export function showWelcomeMessage () {
    const titleStyle = [
        "font-size: 24px",
        "color: #9FA8DA",
        "font-weight: 700",
        "line-height: 32px"
    ].join(" ;")
    const bodyStyle = [
        "font-size: 12px",
        "line-height: 16px",
        "letter-spacing: 0.25px"
    ].join(" ;")
    const url = location.pathname.slice(0, location.pathname.indexOf("/gui")) + "/gui/sample/readme"
    console.log(
        "%cOX Platform \n" +
        "%c\n" +
        "OX Platform offers a modular architecture, based on a set of back-end and front-end components. \n" +
        "\n" +
        "🦊 Visit our GitLab: https://gitlab.com/openxtrem \n" +
        "⚙️ Read the documentation: https://gitlab.com/openxtrem/mediboard/-/wikis/home \n" +
        "🎨 Check our Design System: https://www.figma.com/files/team/1001427632996395878 \n" +
        "🧪 Try Sample Module: " + new URL(url, location.origin).toString() + " \n" +
        "🧩 Visit our Storybook: https://openxtrem.gitlab.io/mediboard/storybook \n" +
        "❔ Ask your questions: https://erp.openxtrem.com/gui/questionsAnswers \n",
        titleStyle,
        bodyStyle
    )
}
