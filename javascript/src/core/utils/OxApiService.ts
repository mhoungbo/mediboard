/**
 * @package Openxtrem\Core
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Api calls service
 */
import axios, { AxiosError, AxiosInstance, AxiosResponse } from "axios"
import { addError } from "@/core/utils/OxNotifyManager"
import { OxJsonResponseError } from "@/core/types/OxApiTypes"
import { sanitizeHtml } from "@/core/utils/OxFunctions"
import { useAppStore } from "@/core/stores/app"
import { loadStop } from "@/core/utils/OxLoadingManager"

// Axios
const oxApiService: AxiosInstance = axios.create({
    headers: {
        "Content-Type": "application/vnd.api+json",
        "X-Requested-With": "XMLHttpRequest"
    },
    timeout: 50000,
    withCredentials: true,
    validateStatus: (status: number) => status >= 200 && status < 400
})

// request axios
oxApiService.interceptors.request.use(
    (config) => {
        return config
    },
    (error) => {
        // Do something with request error
        console.error(error) // for debug
        Promise.reject(error)
    }
)

// response axios
oxApiService.interceptors.response.use(
    (res: AxiosResponse) => {
        // Before do something with api return
        return res
    },
    (error: AxiosError) => {
        const errorResponse = error.response as unknown as OxJsonResponseError

        if (errorResponse) {
            // Authentification required
            if (errorResponse.status === 401) {
                const app = useAppStore()
                app.mustReconnect = true
            }
            else {
                // Sanitize back response
                addError(sanitizeHtml(errorResponse.data.errors.message))
                loadStop()
            }
        }

        return Promise.reject(error)
    }
)

export default oxApiService
