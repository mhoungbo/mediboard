/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Class to manipulate global data for core framework use
 */
export class OxAppContext {
    /**
     * App context data initialization
     */
    public static start () {
        if (window.__core === undefined) {
            window.__core = {
                mounted: false
            }
        }
    }

    /**
     * Return if a Vue component is mounted in window
     * @returns {boolean}
     */
    public static get mounted (): boolean {
        return !!window.__core && window.__core.mounted
    }

    /**
     * Set mounted state
     * @param {boolean} mounted
     */
    public static set mounted (mounted: boolean) {
        if (window.__core !== undefined) {
            window.__core.mounted = mounted
        }
    }

    /**
     * Return build environment
     * @returns {"development"|"production"|"test"}
     */
    public static get env (): "development" | "production" | "test" | "undefined" {
        if (process.env.NODE_ENV === undefined) {
            return "undefined"
        }
        return process.env.NODE_ENV as "development" | "production" | "test" | "undefined"
    }
}
