/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

export const keyConfigs = Symbol("configs")
export const configs = "__configs"

export const keyPrefs = Symbol("prefs")
export const prefs = "__prefs"

export const keyMeta = Symbol("meta")
export const meta = "__meta"

/**
 * @deprecated Use useContextStore instead
 * Inject configurations when used with inject Vue component option
 */
export function injectConfigs () {
    return { [configs]: { from: keyConfigs, default: {} } }
}

/**
 * @deprecated Use useUserStore instead
 * Inject preferences when used with inject Vue component option
 */
export function injectPrefs () {
    return { [prefs]: { from: keyPrefs, default: {} } }
}

/**
 * Inject meta when used with inject Vue component option
 */
export function injectMeta () {
    return { [meta]: { from: keyMeta, default: {} } }
}
