/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { Component } from "vue"
import { showWelcomeMessage } from "@/core/utils/OxConsoleMessageManager"
import mountVue from "@/core/plugins/OxVue"
import { OxAppContext } from "@/core/utils/OxAppContext"

const EXTRACTED_CORE_PROPS = ["prefs", "configs", "locales", "meta", "current-user", "user-function", "user-group"]

/**
 * VueJS Entry point creator
 * @param {string} id - The Element id to graft VueJS
 * @param {Component} component - The component to load instead of Element
 */
export function createEntryPoint (id: string, component: Component) {
    const element = document.getElementById(id)

    if (element === null) {
        throw new Error(`Element with id '${id}' not found`)
    }

    OxAppContext.start()
    let vueProps = ""
    let coreProps = ""
    let isRoot = false
    if (!OxAppContext.mounted) {
        isRoot = true
        OxAppContext.mounted = true
    }

    for (const attribute of element.attributes) {
        // Exclude non-VueJS attributes
        if ((attribute.nodeName.indexOf("vue-") !== 0 && attribute.nodeName.indexOf(":vue-") !== 0)) {
            continue
        }

        const attributeName = attribute.nodeName.replace("vue-", "")
        const attributeValue = attribute.nodeValue ?? ""

        // Check if prop is a core prop
        const isCoreProp = EXTRACTED_CORE_PROPS.includes(attributeName.replace(":", ""))

        if (isCoreProp) {
            coreProps += `${attributeName}='${attributeValue}' `
        }
        else {
            vueProps += `${attributeName}='${attributeValue}' `
        }
    }

    if (isRoot && OxAppContext.env === "development") {
        showWelcomeMessage()
    }

    mountVue(component, element, isRoot, coreProps, vueProps)
}
