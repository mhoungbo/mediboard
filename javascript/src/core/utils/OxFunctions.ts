/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Levenshtein function
 *
 * @see https://stackoverflow.com/a/35279162
 *
 * @param {String} s String 1
 * @param {String} t String 2
 *
 * @return {Number} The Levenshtein distance between s and t
 */
export function levenshtein (s: string, t: string) {
    if (s === t) {
        return 0
    }
    const n = s.length
    const m = t.length
    if (n === 0 || m === 0) {
        return n + m
    }
    let x = 0
    let y
    let a
    let b
    let c
    let d
    let g
    let h
    const p = new Uint16Array(n)
    const u = new Uint32Array(n)
    for (y = 0; y < n;) {
        u[y] = s.charCodeAt(y)
        p[y] = ++y
    }

    for (; (x + 3) < m; x += 4) {
        const e1 = t.charCodeAt(x)
        const e2 = t.charCodeAt(x + 1)
        const e3 = t.charCodeAt(x + 2)
        const e4 = t.charCodeAt(x + 3)
        c = x
        b = x + 1
        d = x + 2
        g = x + 3
        h = x + 4
        for (y = 0; y < n; y++) {
            a = p[y]
            if (a < c || b < c) {
                c = (a > b ? b + 1 : a + 1)
            }
            else {
                if (e1 !== u[y]) {
                    c++
                }
            }

            if (c < b || d < b) {
                b = (c > d ? d + 1 : c + 1)
            }
            else {
                if (e2 !== u[y]) {
                    b++
                }
            }

            if (b < d || g < d) {
                d = (b > g ? g + 1 : b + 1)
            }
            else {
                if (e3 !== u[y]) {
                    d++
                }
            }

            if (d < g || h < g) {
                g = (d > h ? h + 1 : d + 1)
            }
            else {
                if (e4 !== u[y]) {
                    g++
                }
            }
            p[y] = h = g
            g = d
            d = b
            b = c
            c = a
        }
    }

    for (; x < m;) {
        const e = t.charCodeAt(x)
        c = x
        d = ++x
        for (y = 0; y < n; y++) {
            a = p[y]
            if (a < c || d < c) {
                d = (a > d ? d + 1 : a + 1)
            }
            else {
                if (e !== u[y]) {
                    d = c + 1
                }
                else {
                    d = c
                }
            }
            p[y] = d
            c = a
        }
        h = d
    }

    return h
}

/**
 * Return text content from html string
 *
 * @param {String} content HTML Content
 *
 * @return {String} The text content without html tag
 */
export function sanitizeHtml (content?: string): string {
    const sanitize = document.createElement("div")
    sanitize.innerHTML = content ?? ""
    return sanitize.textContent || sanitize.innerText || ""
}

/**
 * Load new page
 * @param {string} url - New page url
 * @param {boolean} newTab - Open new page in new tab if true
 */
export function changePage (url: string, newTab = false) {
    if (newTab) {
        (window.open(url, "_blank") as WindowProxy).focus()
    }
    else {
        window.location.href = url
    }
}
