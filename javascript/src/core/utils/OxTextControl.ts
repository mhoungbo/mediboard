/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Based on the window selection, we determine the current word (all text between the cursor and a space or the end of the line)
 */
export function getCurrentWord () : string {
    let word = ""
    const selection = window.getSelection()
    // Check if there is a selection (i.e. cursor in place)
    if (selection !== null && selection.rangeCount !== 0) {
        const range = selection.getRangeAt(0)

        const preCaretRange = range.cloneRange()
        preCaretRange.setEnd(range.endContainer, range.endOffset)
        let start = range.endOffset - 1
        while (!word.startsWith(" ") && start >= 0) {
            preCaretRange.setStart(range.endContainer, start)
            word = preCaretRange.toString()
            start--
        }
    }
    word = word.trim()
    return word
}

/**
 * From the window selection, we get the current text node and gather the content inside if the cursor is at the end
 */
export function getCurrentNodeText () : { text: string, isMiddle: boolean } {
    let text = ""
    let isMiddle = true
    const selection = window.getSelection()
    // Check if there is a selection (i.e. cursor in place)
    if (selection !== null && selection.rangeCount !== 0) {
        const range = selection.getRangeAt(0)

        isMiddle = (range.endContainer.textContent?.length || 0) > range.endOffset

        const preCaretRange = range.cloneRange()
        preCaretRange.setEnd(range.endContainer, 0)
        preCaretRange.setEnd(range.endContainer, range.endOffset)
        text = preCaretRange.toString().trimStart()
    }
    return { text, isMiddle }
}

/**
 * This will build the helpText node
 * This simply manage the lines breaks
 * This may be exported to be used also in the paste event of richTextContent
 * @param text
 */
export function createNodeFromTextWithBrTags (text: string): HTMLSpanElement {
    // Splitting lines to create BR tags
    const lines = text.split(/\r?\n|\r|\n/g)

    // Regroup all the text node into a span and put BR for each lines except the last one
    const node = document.createElement("span")
    lines.forEach((text, index) => {
        const child = document.createTextNode(text)
        node.appendChild(child)
        if (index !== (lines.length - 1)) {
            node.appendChild(document.createElement("br"))
        }
    })

    return node
}
