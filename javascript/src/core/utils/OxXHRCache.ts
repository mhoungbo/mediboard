/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import { AxiosResponse } from "axios"
import { OxJsonApi } from "@/core/types/OxApiTypes"
import oxApiService from "@/core/utils/OxApiService"
import { OxXHRCacheItem } from "@/core/types/OxXHRCacheTypes"
import { isEmpty } from "lodash"
import { checkChanges } from "@/core/utils/OxApiManager"

/* XHRCache version number */
const VERSION = 1
/* XHRCache base name */
const NAME = "XHR-CACHE"
/* Default values */
const CACHE_FRESHNESS_TIME = 0
const USE_ETAG = false

/**
 * Get data corresponding to url from cache if possible, from api otherwise
 * @param {string} session - The current user's session key
 * @param {string} url - The url to call to receive data
 * @param {number} freshnessTime - In seconds, the maximum time allowed since the last data retrieval from backend
 * @param {boolean} useEtag - Use etag for cache data invalidation
 *
 * @returns {OxJsonApi} The response corresponding to url
 */
export async function getCachedData (
    session: string,
    url: string,
    freshnessTime = CACHE_FRESHNESS_TIME,
    useEtag = USE_ETAG
): Promise<OxJsonApi> {
    const cacheResponse = await getDataFromCache(session, url)

    // If no corresponding data in cache, get from api and update cache
    if (!cacheResponse) {
        const newResponse = await oxApiService.get<OxJsonApi>(url)
        // No need to await this function
        setDataToCache(session, url, newResponse)
        return newResponse.data
    }
    else {
        // Check freshness time
        const timeDiff = getCurrentTimestamp() - cacheResponse.timestamp

        /* If good return cache item */
        if (timeDiff <= freshnessTime) {
            return cacheResponse.data
        }
        /* Otherwise, call to back-end to get updated resource */
        if (useEtag) {
            const isChanged = await checkChanges(url, cacheResponse.etag)

            /* If item has changed, return new item */
            if (isChanged) {
                // No need to await this function
                setDataToCache(session, url, isChanged)
                return isChanged.data
            }
            /* Otherwise, update current cache item timestamp */
            // No need to await this function
            updateCacheItemTimestamp(session, url, cacheResponse)
            return cacheResponse.data
        }
        const newResponse = await oxApiService.get<OxJsonApi>(url)
        // No need to await this function
        setDataToCache(session, url, newResponse)
        return newResponse.data
    }
}

/**
 * Add response to cache for url
 * @param {string} session - The current user's session key
 * @param {string} url - The url to call to receive data
 * @param {AxiosResponse} response - The axios return after call url
 */
export async function setDataToCache (session: string, url: string, response: AxiosResponse<OxJsonApi>) {
    const cacheItem: OxXHRCacheItem = {
        timestamp: getCurrentTimestamp(),
        data: response.data,
        etag: response.headers.etag
    }
    const cache = await openCache(session)
    await cache.put(url, new Response(JSON.stringify(cacheItem)))
}

/**
 * Update timestamp to current time for a cache item
 * @param {string} session - The current user's session key
 * @param {string} url - The url to call to receive data
 * @param {OxXHRCacheItem} item - The cache item to update
 */
export async function updateCacheItemTimestamp (session: string, url: string, item: OxXHRCacheItem) {
    const cacheItem: OxXHRCacheItem = {
        timestamp: getCurrentTimestamp(),
        data: item.data,
        etag: item.etag
    }
    const cache = await openCache(session)
    await cache.put(url, new Response(JSON.stringify(cacheItem)))
}

/**
 * Get data store in cache for url
 * @param {string} session - The current user's session key
 * @param {string} url - The url to call to receive data
 *
 * @returns {Promise<OxXHRCacheItem|undefined>} The cache item if exists, undefined otherwise
 */
export async function getDataFromCache (session: string, url: string): Promise<OxXHRCacheItem | undefined> {
    const cache = await openCache(session)
    const cacheResponse = await cache.match(url)

    if (!cacheResponse || !cacheResponse.ok) {
        return undefined
    }

    return await cacheResponse.json()
}

/**
 * @returns {number} the current timestamp in seconds
 */
export function getCurrentTimestamp (): number {
    return Date.now() / 1000
}

/**
 * Return the XHRCache name corresponding to session key
 * @param {string} session - The session key
 *
 * @returns {string} The cache name
 */
export function getCacheName (session: string): string {
    return `${NAME}-${session}-${VERSION}`
}

/**
 * Return Cache object corresponding to cacheName and remove all others if it's first access
 * @param {string} session - The session key
 */
export async function openCache (session: string) {
    const cacheName = getCacheName(session)
    const cache = await caches.open(cacheName)
    // If no data in cache, considering it's first opening of cache for this session. So remove all other caches
    if (isEmpty(await cache.keys())) {
        deleteOldCaches(session)
    }
    return cache
}

/**
 * Removes all XHR caches that are different to currentCacheName
 * @param {string} session - The session key
 */
export async function deleteOldCaches (session: string) {
    const currentCacheName = getCacheName(session)
    const keys = await caches.keys()

    for (const key of keys) {
        const isXHRCache = key.startsWith(NAME)
        if (currentCacheName === key || !isXHRCache) {
            continue
        }
        caches.delete(key)
    }
}

/**
 * Return if XHRCaches is enabled on the current window
 */
export function xhrCacheEnabled () {
    return "caches" in window
}
