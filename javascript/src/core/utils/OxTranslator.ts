/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Translator string
 *
 * @param {string} key - Translator key
 * @param {boolean} plural - use plural
 * @param {string} values - values inject in translator
 *
 * @return {string}
 */
export function tr (key: string, plural = false, ...values: string[]): string {
    // @ts-ignore
    if (window.$T && !window.guiLocales) {
        // @ts-ignore
        return window.$T(key + (plural ? "|pl" : ""), ...values)
    }

    return window.guiLocales && window.guiLocales[key] ? window.guiLocales[key] : ""
}
