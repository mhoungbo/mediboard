/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * class for managing generic urls
 */
export class OxUrlDiscovery {
    private static readonly LOGIN_PAGE_LINK = "gui/login"
    private static readonly LOGOUT_LINK = "gui/logout"
    private static readonly STATUS_LINK = "api/status"
    private static readonly OPENAPI_LINK = "api/doc"
    private static readonly CONFIGURATIONS_LINK = "api/configurations"
    private static readonly PREFERENCES_LINK = "api/preferences"
    private static readonly LOCALES_LINK = "api/locales"
    private static readonly BULK_LINK = "api/bulkOperations"
    private static readonly SCHEMA_LINK = "api/schemas"
    private static readonly IDEX_LINK = "api/identifiers/idex"
    private static readonly NOTES_LINK = "api/system/notes"
    private static readonly MEDIUSERS_LINK = "api/mediuser/mediusers"

    private static readonly HELPED_TEXTS_LINK = "api/models/helped_texts"
    private static readonly HELPED_TEXTS_EDIT_LINK = "gui/models/helped_texts/vwEdit"
    private static readonly MEDICAL_CODES_LINK = "api/system/codes"
    private static readonly PATIENTS_LINK = "api/dossierpatient/patients"
    private static readonly USERS_LINK = "api/admin/users"

    public static loginPage (isReconnection = false): string {
        return this.LOGIN_PAGE_LINK + (isReconnection ? "?reconnect=1" : "")
    }

    public static logout (): string {
        return this.LOGOUT_LINK
    }

    public static status (): string {
        return this.STATUS_LINK
    }

    public static openapi (): string {
        return this.OPENAPI_LINK
    }

    public static moduleConfigurations (moduleName: string): string {
        return `${this.CONFIGURATIONS_LINK}/${moduleName}`
    }

    public static preferences (moduleName: string): string {
        return `${this.PREFERENCES_LINK}/${moduleName}`
    }

    public static userPreferences (moduleName = "common", userId: string): string {
        return `${this.PREFERENCES_LINK}/${moduleName}/${userId}`
    }

    public static locales (moduleName: string, language: "fr" | "en" | "it" | "de" | "fe-be" | "nl-be" | "core" = "core"): string {
        return `${this.LOCALES_LINK}/${language}/${moduleName}`
    }

    public static bulk (): string {
        return this.BULK_LINK
    }

    public static schema (resourceName?: string): string {
        return this.SCHEMA_LINK + (resourceName ? `/${resourceName}` : "")
    }

    public static createHelpedText (): string {
        return this.HELPED_TEXTS_LINK
    }

    public static listHelpedTexts (resourceName: string, fieldName: string): string {
        return `${this.HELPED_TEXTS_LINK}/${resourceName}/${fieldName}`
    }

    public static editHelpedTexts (): string {
        return this.HELPED_TEXTS_EDIT_LINK
    }

    public static idex (): string {
        return this.IDEX_LINK
    }

    public static notes (): string {
        return this.NOTES_LINK
    }

    public static listMediusers (): string {
        return this.MEDIUSERS_LINK
    }

    public static listUsers (): string {
        return this.USERS_LINK
    }

    public static listMedicalCodes (): string {
        return this.MEDICAL_CODES_LINK
    }

    public static listPatients (): string {
        return this.PATIENTS_LINK
    }
}
