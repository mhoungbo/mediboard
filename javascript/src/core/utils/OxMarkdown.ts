/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

import TurndownService from "turndown"
import MarkdownIt from "markdown-it"

interface MarkdownItOptions {
    html: boolean
    xhtmlOut: boolean
    breaks: boolean
    linkify: boolean
    typographer: boolean
}

/**
 * Using the Turndown lib : https://www.npmjs.com/package/turndown
 * Convert HTML to markdown in order to be sent to the back
 * @param text
 */
export function convertHTMLToMarkdown (text: string) : string {
    const ts = new TurndownService(
        {
            headingStyle: "atx",
            hr: "---",
            bulletListMarker: "-",
            codeBlockStyle: "fenced"
        }
    )

    // @ts-ignore
    ts.addRule("strikethrough", {
        filter: ["del", "s", "strike"],
        replacement: function (content) {
            return `~~${content}~~`
        }
    })

    return ts.turndown(text)
}

/**
 * Using the Markdown-It lib : https://www.npmjs.com/package/markdown-it
 * Convert markdown to HTML in order to be rendered to the DOM
 * We use the commonmark format
 * @param {string} text
 * @param {MarkdownItOptions} options
 */
export function convertMarkdownToHTML (
    text: string,
    options: MarkdownItOptions = {
        html: false,
        xhtmlOut: false,
        breaks: false,
        linkify: false,
        typographer: false
    }
): string {
    const md = new MarkdownIt(options)
    return md.render(text)
}
