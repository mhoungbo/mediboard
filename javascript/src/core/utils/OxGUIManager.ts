/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

const GUI_IFRAME_NAME = "ox-core-iframe"

/**
 * Load a GUI endpoint into the given element in an iframe
 * @param {string} url - Url corresponding to GUI route
 * @param {HTMLElement} target - Element that receive the iframe
 */
export async function loadGUIEntrypointIframe (url: string, target: HTMLElement) {
    // reset modal content
    target.innerHTML = ""

    // Load script to load Vue in entrypoint
    const iframe = document.createElement("iframe")
    iframe.src = url
    iframe.name = GUI_IFRAME_NAME
    iframe.width = "100%"
    iframe.height = "100%"
    iframe.frameBorder = "0"
    target.appendChild(iframe)

    return true
}

/**
 * Determines if current modal is a GUI context
 */
export function isGUIModalContext (): boolean {
    return window.name === GUI_IFRAME_NAME
}
