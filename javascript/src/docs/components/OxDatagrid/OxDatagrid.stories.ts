import type { Meta, StoryObj } from "@storybook/vue"
import OxDatagrid from "@/core/components/OxDatagrid/OxDatagrid.vue"
import OxCollection from "@/core/models/OxCollection"
import OxObject from "@/core/models/OxObject"

const meta = {
    title: "Core/Visual Components/Datagrid",
    component: OxDatagrid,
    tags: ["autodocs"],
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDatagrid },
        template: '<ox-datagrid v-bind="$props" />'
    })
} satisfies Meta<typeof OxDatagrid>

export default meta
type Story = StoryObj<typeof OxDatagrid>

const datas: OxCollection<OxObject> = new OxCollection()
const glace = new OxObject()
glace.id = "1"
glace.attributes.name = "Glace"
glace.attributes.calories = 321
glace.attributes.fat = 32
glace.attributes.carbs = 230
glace.attributes.iron = 12
const brownie = new OxObject()
brownie.id = "2"
brownie.attributes.name = "Brownie"
brownie.attributes.calories = 241
brownie.attributes.fat = 73
brownie.attributes.carbs = 212
brownie.attributes.iron = 19
const parisBrest = new OxObject()
parisBrest.id = "3"
parisBrest.attributes.name = "Paris Brest"
parisBrest.attributes.calories = 383
parisBrest.attributes.fat = 32
parisBrest.attributes.carbs = 212
parisBrest.attributes.iron = 19
datas.objects = [
    glace,
    brownie,
    parisBrest
]
datas.links = {}
export const Default: Story = {
    args: {
        columns: [
            {
                text: "Dessert (pour 100g)",
                sortable: false,
                value: "attributes.name"
            },
            { text: "Calories", value: "attributes.calories" },
            { text: "Lipides (g)", value: "attributes.fat" },
            { text: "Glucides (g)", value: "attributes.carbs" },
            { text: "Fer (%)", value: "attributes.iron" }
        ],
        oxObject: OxObject,
        value: datas
    }
}
