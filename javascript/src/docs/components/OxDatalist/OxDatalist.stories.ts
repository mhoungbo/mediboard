import type { Meta, StoryObj } from "@storybook/vue"
import OxDatalist from "@/core/components/OxDatalist/OxDatalist.vue"
import OxCollection from "@/core/models/OxCollection"
import OxObject from "@/core/models/OxObject"
import { VSimpleCheckbox } from "vuetify/lib/components"

const meta = {
    title: "Core/Visual Components/Datalist",
    component: OxDatalist,
    tags: ["autodocs"],
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDatalist, VSimpleCheckbox },
        template: '<ox-datalist v-bind="$props" />'
    })
} satisfies Meta<typeof OxDatalist>

export default meta
type Story = StoryObj<typeof OxDatalist>

const datas: OxCollection<OxObject> = new OxCollection()
const icecream = new OxObject()
icecream.id = "1"
icecream.attributes.name = "Icecream"
icecream.attributes.calories = 321
icecream.attributes.fat = 32
icecream.attributes.carbs = 230
icecream.attributes.iron = 12
const brownie = new OxObject()
brownie.id = "2"
brownie.attributes.name = "Brownie"
brownie.attributes.calories = 241
brownie.attributes.fat = 73
brownie.attributes.carbs = 212
brownie.attributes.iron = 19
const parisBrest = new OxObject()
parisBrest.id = "3"
parisBrest.attributes.name = "Paris Brest"
parisBrest.attributes.calories = 383
parisBrest.attributes.fat = 32
parisBrest.attributes.carbs = 212
parisBrest.attributes.iron = 19
const lolipop = new OxObject()
lolipop.id = "4"
lolipop.attributes.name = "Lolipop"
lolipop.attributes.calories = 392
lolipop.attributes.fat = 2
lolipop.attributes.carbs = 98
lolipop.attributes.iron = 2
const donut = new OxObject()
donut.id = "4"
donut.attributes.name = "Donut"
donut.attributes.calories = 452
donut.attributes.fat = 25
donut.attributes.carbs = 51
donut.attributes.iron = 22
datas.objects = [
    icecream,
    brownie,
    parisBrest,
    lolipop,
    donut
]
datas.links = {}
datas.meta = { total: 3, count: 3 }
export const Default: Story = {
    args: {
        columns: [
            {
                text: "Dessert (pour 100g)",
                sortable: false,
                value: "attributes.name"
            },
            { text: "Calories", value: "attributes.calories" },
            { text: "Lipides (g)", value: "attributes.fat" },
            { text: "Glucides (g)", value: "attributes.carbs" },
            { text: "Fer (%)", value: "attributes.iron" }
        ],
        oxObject: OxObject,
        value: datas
    }
}
