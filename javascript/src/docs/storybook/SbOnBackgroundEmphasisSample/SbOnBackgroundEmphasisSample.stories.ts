import type { Meta, StoryObj } from "@storybook/vue"
import SbOnBackgroundEmphasisSample
    from "@/docs/storybook/SbOnBackgroundEmphasisSample/SbOnBackgroundEmphasisSample.vue"

const meta = {
    title: "SbOnBackgroundEmphasisSample",
    component: SbOnBackgroundEmphasisSample
} satisfies Meta<typeof SbOnBackgroundEmphasisSample>

export default meta
type Story = StoryObj<typeof SbOnBackgroundEmphasisSample>

export const Default: Story = {
    render: (args, { argTypes }) => ({
        components: { SbOnBackgroundEmphasisSample },
        template: "<sb-on-background-emphasis-sample />"
    })
}
