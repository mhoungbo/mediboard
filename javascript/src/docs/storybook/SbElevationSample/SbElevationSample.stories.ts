import type { Meta, StoryObj } from "@storybook/vue"
import SbElevationSample from "@/docs/storybook/SbElevationSample/SbElevationSample.vue"

const meta = {
    title: "SbElevationSample",
    component: SbElevationSample
} satisfies Meta<typeof SbElevationSample>

export default meta
type Story = StoryObj<typeof SbElevationSample>

export const Default: Story = {
    render: (args, { argTypes }) => ({
        components: { SbElevationSample },
        template: "<sb-elevation-sample />"
    })
}
