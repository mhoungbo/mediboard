import type { Meta, StoryObj } from "@storybook/vue"
import SbOnPrimaryEmphasisSample from "@/docs/storybook/SbOnPrimaryEmphasisSample/SbOnPrimaryEmphasisSample.vue"

const meta = {
    title: "SbOnPrimaryEmphasisSample",
    component: SbOnPrimaryEmphasisSample
} satisfies Meta<typeof SbOnPrimaryEmphasisSample>

export default meta
type Story = StoryObj<typeof SbOnPrimaryEmphasisSample>

export const Default: Story = {
    render: (args, { argTypes }) => ({
        components: { SbOnPrimaryEmphasisSample },
        template: "<sb-on-primary-emphasis-sample />"
    })
}
