import type { Meta, StoryObj } from "@storybook/vue"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"
import { VScaleTransition } from "vuetify/lib/components"
import { customIcons, icons } from "@oxify/components/OxIcon/OxIconCore"

const meta = {
    title: "SbIconGalery"
}

export default meta

export const Default: StoryObj = {
    render: (args, { argTypes }) => ({
        components: { OxIcon, VScaleTransition },
        props: Object.keys(argTypes),
        data: () => {
            return {
                showNotify: false,
                timeout: 0
            }
        },
        methods: {
            copy (iconName, data) {
                navigator.clipboard.writeText(iconName)
                data.showNotify = false
                data.showNotify = true
                clearTimeout(data.timeout)
                data.timeout = setTimeout(() => {
                    data.showNotify = false
                }, 1000)
            }
        },
        template: `
          <div style="display: flex; flex-direction: row; flex-wrap: wrap">
          <div
              v-for="(iconKey, iconName) in $props.icons"
              :key="iconName"
              style="display: flex; flex-direction: row; flex-wrap: nowrap; padding: 4px; margin: 4px;
           justify-content: left; align-items: center; box-sizing: border-box;"
              :title="iconName"
          >
            <div class="pres-icon"
                 style="flex: 1; gap: 4px; border: 1px solid var(--grey-100); border-radius: 6px; margin: 8px; transition: all 0.08s linear;
                padding: 28px 8px 8px 8px; display: flex; flex-flow: column nowrap; align-items: center; width: 120px;
                height: 120px; color: var(--grey-600); background-color: var(--background-elevation-1); cursor: pointer"
                 onmouseover="this.style.backgroundColor = 'rgba(var(--secondary-500-rgb), 0.16)'"
                 onmouseout="this.style.backgroundColor = 'var(--background-elevation-1)'"
                 @click="copy(iconName, $data)"
            >
              <ox-icon :icon="iconName" :size="38"/>
              <div style="font-size: 12px; font-weight: 500; word-break: break-all; text-align: center">
                {{ iconName }}
              </div>
            </div>
          </div>
          <v-scale-transition>
            <div
                v-if="showNotify"
                style="position: fixed; top: 15vh; left: 50%; transform: translateX(-50%); padding: 16px 40px;
        background-color: var(--secondary-100);
        color: var(--secondary-800);
        box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.14), 0px 1px 18px rgba(0, 0, 0, 0.12), 0px 3px 5px rgba(0, 0, 0, 0.2);
        border-radius: 4px;"
            >
              Copied to clipboard
            </div>
          </v-scale-transition>
          </div>
        `
    }),
    args: {
        icons: Object.keys({ ...icons, ...customIcons }).sort().reduce(
            (obj, key) => {
                obj[key] = { ...icons, ...customIcons }[key]
                return obj
            },
            {}
        )
    }
}
