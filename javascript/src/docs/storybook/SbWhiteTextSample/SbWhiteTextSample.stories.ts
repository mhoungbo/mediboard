import type { Meta, StoryObj } from "@storybook/vue"
import SbWhiteTextSample from "@/docs/storybook/SbWhiteTextSample/SbWhiteTextSample.vue"

const meta = {
    title: "SbWhiteTextSample",
    component: SbWhiteTextSample
} satisfies Meta<typeof SbWhiteTextSample>

export default meta
type Story = StoryObj<typeof SbWhiteTextSample>

export const Default: Story = {
    render: (args, { argTypes }) => ({
        components: { SbWhiteTextSample },
        template: "<sb-white-text-sample />"
    })
}
