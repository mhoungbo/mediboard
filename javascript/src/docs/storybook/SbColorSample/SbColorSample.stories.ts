import type { Meta, StoryObj } from "@storybook/vue"
import SbColorSample from "./SbColorSample.vue"

const meta = {
    title: "SbColorSample",
    component: SbColorSample
} satisfies Meta<typeof SbColorSample>

export default meta
type Story = StoryObj<typeof SbColorSample>

export const Primary: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: "<sb-color-sample />"
    })
}

export const Secondary: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: "<sb-color-sample color='secondary' />"
    })
}

export const Grey: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: "<sb-color-sample color='grey' :use-default='false' />"
    })
}

export const Error: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: `<sb-color-sample color='error'
                                    :use-default='false'
                                    :variations="['surface-default', 'surface-light', 'surface-darken', 'text-default']" />`
    })
}

export const Warning: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: `<sb-color-sample color='warning'
                                    :use-default='false'
                                    :variations="['surface-default', 'surface-light', 'text-default']" />`
    })
}

export const Success: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: `<sb-color-sample color='success'
                                    :use-default='false'
                                    :variations="['surface-default', 'surface-light', 'text-default']" />`
    })
}

export const Info: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: `<sb-color-sample color='info'
                                    :use-default='false'
                                    :variations="['surface-default', 'surface-light', 'text-default']" />`
    })
}

export const Backgrounds: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: `<sb-color-sample color='background'
                                    :use-default='false'
                                    :variations="['default', 'default-light', 'light', 'lighter', 'dark', 'darker', 'info', 'hover', 'input-default', 'input-hover']" />`
    })
}

export const Surfaces: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: `<sb-color-sample color='surface'
                                    :use-default='false'
                                    :variations="['primary', 'primary-800', 'primary-700', 'primary-600', 'primary-500', 'primary-300', 'primary-100', 'secondary-900', 'secondary-800']" />`
    })
}

export const CategoricalPalette: Story = {
    render: (args, { argTypes }) => ({
        components: { SbColorSample },
        template: `<sb-color-sample color='color'
                                    :use-default='false'
                                    :variations="[
                                      'pink',
                                      'purple',
                                      'deep-purple',
                                      'blue',
                                      'cyan',
                                      'teal',
                                      'green',
                                      'light-green',
                                      'lime',
                                      'yellow',
                                      'orange'
                                    ]" />`
    })
}
