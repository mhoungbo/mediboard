import { Meta, StoryObj } from "@storybook/vue"
import OxChipGroup from "@oxify/components/OxChipGroup/OxChipGroup.vue"

const meta = {
    title: "Core/Oxify/ChipGroup",
    component: OxChipGroup,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxChipGroup },
        template: `
          <ox-chip-group
              v-model="$props.value"
              :choices="$props.choices"
              :mandatory="$props.mandatory"
              :max="$props.max"
              :multiple="$props.multiple"
              :small="$props.small"
              :wrap="$props.wrap"
          />
        `
    }),
    argTypes: {
        value: {
            name: "value",
            description: "The list of values corresponding to the selected chips.",
            table: {
                category: "Props",
                type: {
                    summary: "string[] | string"
                }
            },
            control: false
        },
        choices: {
            name: "choices",
            description: "The list of available choices. Each choice will be displayed in an chip.",
            control: {
                type: "object"
            },
            table: {
                category: "Props",
                type: {
                    summary: "OxChipGroupChoiceModel[]",
                    detail: "{ \n" +
                        "   label: string \n" +
                        "   value: string \n" +
                        "}"
                }
            }
        },
        wrap: {
            name: "wrap",
            description: "Remove horizontal pagination and wrap chips as needed.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        mandatory: {
            name: "mandatory",
            description: "Forces a value to always be selected (if available).",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        max: {
            name: "max",
            description: "Sets a maximum number of selections that can be made.",
            control: {
                type: "number"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: undefined
                },
                type: {
                    summary: "number"
                }
            }
        },
        multiple: {
            name: "multiple",
            description: "Allow multiple selections. The choices prop must be an array.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        small: {
            name: "small",
            description: "Makes the chips small.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        // @ts-ignore
        change: {
            action: "change",
            description: "Emit `change` event when the selected chips change.",
            table: {
                category: "Events"
            }
        },
        input: {
            name: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        }
    }
} satisfies Meta<typeof OxChipGroup>

const choices = [
    {
        label: "Extra Soft",
        value: "extra-soft"
    },
    {
        label: "Soft",
        value: "soft"
    },
    {
        label: "Medium",
        value: "medium"
    },
    {
        label: "Hard",
        value: "hard"
    },
    {
        label: "Extra hard",
        value: "extra-hard"
    }
]

export default meta
type Story = StoryObj<typeof OxChipGroup>

export const Default: Story = {
    args: {
        choices
    }
}

export const Model: Story = {
    parameters: {
        docs: {
            source: {
                code: `

export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            selected: undefined as Array<string> | string | undefined,
            choices: [
              {
                label: "Extra Soft",
                value: "extra-soft"
              },
              {
                label: "Soft",
                value: "soft"
              },
              {
                label: "Medium",
                value: "medium"
              },
              {
                label: "Hard",
                value: "hard"
              },
              {
                label: "Extra hard",
                value: "extra-hard"
              }
            ] as OxChipGroupChoice[]
        }
    }
})

<ox-chip-group
  v-model="selected"
  :choices="choices"
  multiple
/>
<div>
  Selected : {{ selected }}
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxChipGroup },
        data () {
            return {
                selected: undefined as Array<string> | string | undefined,
                choices
            }
        },
        template: `
          <div>
              <ox-chip-group
                  v-model="selected"
                  :choices="choices"
                  multiple
              />
              <div>
                Selected : {{ selected }}
              </div>
          </div>
        `
    })
}

export const Multiple: Story = {
    parameters: {
        docs: {
            source: {
                code: `

export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            selectedSingle: undefined as Array<string> | string | undefined,
            selectedMultiple: undefined as Array<string> | string | undefined,
            choices: [
              {
                label: "Extra Soft",
                value: "extra-soft"
              },
              {
                label: "Soft",
                value: "soft"
              },
              {
                label: "Medium",
                value: "medium"
              },
              {
                label: "Hard",
                value: "hard"
              },
              {
                label: "Extra hard",
                value: "extra-hard"
              }
            ] as OxChipGroupChoice[]
        }
    }
})

<ox-chip-group
  v-model="selectedSingle"
  :choices="choices"
/>
<div>
  Single - Selected : {{ selectedSingle }}
</div>
<ox-chip-group
  v-model="selectedMultiple"
  :choices="choices"
  multiple
/>
<div>
  Multiple - Selected : {{ selectedMultiple }}
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxChipGroup },
        data () {
            return {
                selectedSingle: undefined as Array<string> | string | undefined,
                selectedMultiple: undefined as Array<string> | string | undefined,
                choices
            }
        },
        template: `
          <div>
          <ox-chip-group
              v-model="selectedSingle"
              :choices="choices"
          />
          <div>
            Single - Selected : {{ selectedSingle }}
          </div>
              <ox-chip-group
                  v-model="selectedMultiple"
                  :choices="choices"
                  multiple
              />
              <div>
                Multiple - Selected : {{ selectedMultiple }}
              </div>
          </div>
        `
    })
}

export const Wrap: Story = {
    parameters: {
        docs: {
            source: {
                code: `

export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            choices: [
              {
                label: "Extra Soft",
                value: "extra-soft"
              },
              {
                label: "Soft",
                value: "soft"
              },
              {
                label: "Medium",
                value: "medium"
              },
              {
                label: "Hard",
                value: "hard"
              },
              {
                label: "Extra hard",
                value: "extra-hard"
              }
            ] as OxChipGroupChoice[]
        }
    }
})

<div style="width: 300px">
  <ox-chip-group
    :choices="choices"
  />
</div>
<div style="width: 300px">
  <ox-chip-group
    :choices="choices"
    wrap
  />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxChipGroup },
        data () {
            return {
                choices
            }
        },
        template: `
          <div>
            <div style="width: 300px">
              <ox-chip-group
                  :choices="choices"
              />
            </div>
            <div style="width: 300px">
              <ox-chip-group
                  :choices="choices"
                  wrap
              />
            </div>
          </div>
        `
    })
}

export const Mandatory: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            choices: [
              {
                label: "Extra Soft",
                value: "extra-soft"
              },
              {
                label: "Soft",
                value: "soft"
              },
              {
                label: "Medium",
                value: "medium"
              },
              {
                label: "Hard",
                value: "hard"
              },
              {
                label: "Extra hard",
                value: "extra-hard"
              }
            ] as OxChipGroupChoice[]
        }
    }
})
<ox-chip-group 
  :choices="choices"
  mandatory
/>
                `
            }
        }
    },
    args: {
        choices,
        mandatory: true
    }
}

export const Max: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            choices: [
              {
                label: "Extra Soft",
                value: "extra-soft"
              },
              {
                label: "Soft",
                value: "soft"
              },
              {
                label: "Medium",
                value: "medium"
              },
              {
                label: "Hard",
                value: "hard"
              },
              {
                label: "Extra hard",
                value: "extra-hard"
              }
            ] as OxChipGroupChoice[]
        }
    }
})
<ox-chip-group 
  :choices="choices"
  :max="3"
  multiple
/>
                `
            }
        }
    },
    args: {
        choices,
        max: 3,
        multiple: true
    }
}
