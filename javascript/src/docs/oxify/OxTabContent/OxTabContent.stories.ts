import { Meta, StoryObj } from "@storybook/vue"
import OxTabs from "@oxify/components/OxTabs/OxTabs.vue"
import OxTab from "@oxify/components/OxTab/OxTab.vue"
import OxTabContent from "@oxify/components/OxTabContent/OxTabContent.vue"

const meta = {
    title: "Core/Oxify/TabContent",
    component: OxTabContent,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxTabs, OxTab, OxTabContent },
        data () {
            return {
                tab: null
            }
        },
        template: `
          <ox-tabs v-model="tab">
              <ox-tab title="Tab 1"/>
    
              <template #contents>
                <ox-tab-content v-bind="$props">1</ox-tab-content>
              </template>
          </ox-tabs>
        `
    })
} satisfies Meta<typeof OxTabContent>

export default meta
type Story = StoryObj<typeof OxTabContent>

export const Default: Story = {
    args: {}
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            tab: null
        }
    }
})

<ox-tabs v-model="tab">
  <ox-tab title="Tab 1" />

  <template #contents>
    <ox-tab-content>1</ox-tab-content>
  </template>
</ox-tabs>
                `
            }
        }
    },
    args: {}
}
