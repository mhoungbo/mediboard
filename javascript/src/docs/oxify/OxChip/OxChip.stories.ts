import { Meta, StoryObj } from "@storybook/vue"
import OxChip from "@oxify/components/OxChip/OxChip.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"

const meta = {
    title: "Core/Oxify/Chip",
    component: OxChip,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxChip },
        template: `
          <ox-chip
              :chipActive="$props.chipActive"
              :close="$props.close"
              :ripple="$props.ripple"
              :small="$props.small"
              :link="$props.link"
              :disabled="$props.disabled"
              :draggable="$props.draggable"
              :color="$props.color"
          >
          {{ $props.default }}
          </ox-chip>
        `
    }),
    argTypes: {
        chipActive: {
            name: "chip-active",
            description: "Determines whether the chip is visible or not.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: true
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        close: {
            name: "close",
            description: "Adds remove button.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        ripple: {
            name: "ripple",
            description: "Applies the ripple effect on click.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        small: {
            name: "small",
            description: "Makes the chip small.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        link: {
            name: "link",
            description: "Explicitly define the chip as a link.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        disabled: {
            name: "disabled",
            description: "Disables the chip, making it un-selectable.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        draggable: {
            name: "draggable",
            description: "Makes the chip draggable.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        color: {
            name: "color",
            description: "Define the color of the component.",
            control: {
                type: "text"
            },
            table: {
                category: "Props",
                type: {
                    summary: "string",
                    detail: ' "primary" | "secondary" | "success" | "warning" | "error" | string '
                }
            }
        },
        // @ts-ignore
        click: {
            action: "click",
            description: "Emit `click` event when Chip is clicked.",
            table: {
                category: "Events"
            },
            control: false
        },
        "click:close": {
            action: "click:close",
            description: "Emit `click:close` event when close icon is clicked",
            table: {
                category: "Events"
            },
            control: false
        },
        default: {
            name: "default",
            description: "Default slot",
            control: {
                type: "text"
            },
            table: {
                category: "Slots",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            }
        }
    }
} satisfies Meta<typeof OxChip>

export default meta
type Story = StoryObj<typeof OxChip>

export const Default: Story = {
    args: {
        // @ts-ignore
        default: "Default chip"
    }
}

export const Closable: Story = {
    parameters: {
        docs: {
            source: {
                code: `

export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            chip1: true,
            chip2: true,
            chip3: true
        }
    },
    computed: {
        showButton ():boolean {
            return !this.chip1 && !this.chip2 && !this.chip3
        }
    },
    methods: {
        resetChips () {
            this.chip1 = true
            this.chip2 = true
            this.chip3 = true
        },
        removeChip1 () {
            this.chip1 = false
        },
        removeChip2 () {
            this.chip2 = false
        },
        removeChip3 () {
            this.chip3 = false
        }
    }
})

<ox-button
    v-if="showButton"
    button-style="primary"
    label="Reset"
    @click="resetChips"
></ox-button>
<ox-chip
    v-if="chip1"
    :close="true"
    @click:close="removeChip1"
>
  Closable
</ox-chip>
<ox-chip
    v-if="chip2"
    :close="true"
    color="error"
    @click:close="removeChip2"
>
  Closable error
</ox-chip>
<ox-chip
    v-if="chip3"
    :close="true"
    color="primary"
    @click:close="removeChip3"
>
  Closable primary
</ox-chip>
                `
            }
        }
    },
    render: () => ({
        components: { OxChip, OxButton },
        data () {
            return {
                chip1: true,
                chip2: true,
                chip3: true
            }
        },
        computed: {
            showButton ():boolean {
                // @ts-ignore
                return !this.chip1 && !this.chip2 && !this.chip3
            }
        },
        methods: {
            resetChips () {
                this.chip1 = true
                this.chip2 = true
                this.chip3 = true
            },
            removeChip1 () {
                this.chip1 = false
            },
            removeChip2 () {
                this.chip2 = false
            },
            removeChip3 () {
                this.chip3 = false
            }
        },
        template: `
          <div>
              <ox-button
                  v-if="showButton"
                  button-style="primary"
                  label="Reset"
                  @click="resetChips"
              ></ox-button>
              <ox-chip
                  v-if="chip1"
                  :close="true"
                  @click:close="removeChip1"
              >
              Closable
              </ox-chip>
              <ox-chip
                  v-if="chip2"
                  :close="true"
                  color="error"
                  @click:close="removeChip2"
              >
              Closable error
              </ox-chip>
              <ox-chip
                  v-if="chip3"
                  :close="true"
                  color="primary"
                  @click:close="removeChip3"
              >
                Closable primary
              </ox-chip>
          </div>
        `
    })
}

export const Sizing: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-chip small>
  Small chip
</ox-chip>
<ox-chip 
  small
  close
>
  Small closable chip
</ox-chip>
<ox-chip
  small
  color="primary"
>
  Small primary chip
</ox-chip>
<ox-chip
  small
  close
  color="primary"
>
  Small closable primary chip
</ox-chip>

<ox-chip>
  Default chip
</ox-chip>
<ox-chip close>
  Default closable chip
</ox-chip>
<ox-chip color="primary">
  Default primary chip
</ox-chip>
<ox-chip 
  close
  color="primary"
>
  Default closable primary chip
</ox-chip>
                `
            }
        }
    },
    render: () => ({
        components: { OxChip },
        template: `
          <div style="display: flex; flex-direction: column; gap: 16px;">
          <div>
            <ox-chip small>
              Small chip
            </ox-chip>
            <ox-chip 
                small
                close
            >
              Small closable chip
            </ox-chip>
            <ox-chip
                small
                color="primary"
            >
              Small primary chip
            </ox-chip>
            <ox-chip
                small
                close
                color="primary"
            >
              Small closable primary chip
            </ox-chip>
          </div>
          <div>
            <ox-chip>
              Default chip
            </ox-chip>
            <ox-chip close>
              Default closable chip
            </ox-chip>
            <ox-chip color="primary">
              Default primary chip
            </ox-chip>
            <ox-chip 
                close
                color="primary"
            >
              Default closable primary chip
            </ox-chip>
          </div>
          
          </div>
        `
    })
}

export const States: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-chip>
  Default chip
</ox-chip>
<ox-chip color="primary">
  Primary chip
</ox-chip>
<ox-chip color="secondary">
  Secondary chip
</ox-chip>
<ox-chip color="success">
  Success chip
</ox-chip>
<ox-chip color="warning">
  Warning chip
</ox-chip>
<ox-chip color="error">
  Error chip
</ox-chip>
<ox-chip color="#FF00FF">
  #FF00FF chip
</ox-chip>
<ox-chip color="purple">
  Purple chip
</ox-chip>
                `
            }
        }
    },
    render: () => ({
        components: { OxChip },
        template: `
          <div>
            <ox-chip>
              Default chip
            </ox-chip>
            <ox-chip color="primary">
              Primary chip
            </ox-chip>
            <ox-chip color="secondary">
              Secondary chip
            </ox-chip>
            <ox-chip color="success">
              Success chip
            </ox-chip>
            <ox-chip color="warning">
              Warning chip
            </ox-chip>
            <ox-chip color="error">
              Error chip
            </ox-chip>
            <ox-chip color="#FF00FF">
              #FF00FF chip
            </ox-chip>
            <ox-chip color="purple">
              Purple chip
            </ox-chip>
          </div>
        `
    })
}
