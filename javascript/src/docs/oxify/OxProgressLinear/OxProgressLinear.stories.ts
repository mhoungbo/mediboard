import type { Meta, StoryObj } from "@storybook/vue"
import OxProgressLinear from "@oxify/components/OxProgressLinear/OxProgressLinear.vue"
import OxThemeCore from "@/core/oxify/utils/OxThemeCore"

const meta = {
    title: "Core/Oxify/Progress Linear",
    component: OxProgressLinear,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxProgressLinear },
        template: `
          <ox-progress-linear
            :buffer-color="$props.bufferColor"
            :buffer-value="$props.bufferValue"
            :color="$props.color"
            :height="$props.height"
            :value="$props.value"
          />
        `
    }),
    argTypes: {
        bufferColor: {
            name: "buffer-color",
            description: "Buffer background color, set to component's color if null.",
            control: {
                type: "color"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            }
        },
        bufferValue: {
            name: "buffer-value",
            description: "The percentage value for the buffer.",
            control: {
                type: "range",
                min: 0,
                max: 100,
                step: 1
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: 0
                },
                type: {
                    summary: "number | string",
                    detail: "Number between 0 and 100"
                }
            }
        },
        color: {
            name: "color",
            description: "Applies specified color to the control.",
            control: {
                type: "color"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: "OxThemeCore.primary"
                },
                type: {
                    summary: "string"
                }
            }
        },
        height: {
            name: "height",
            description: "Sets the height for the component.",
            control: {
                type: "number"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: 4
                },
                type: {
                    summary: "number | string"
                }
            }
        },
        value: {
            name: "value",
            description: "The designated model value for the component.",
            control: {
                type: "range",
                min: 0,
                max: 100,
                step: 1
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: 0
                },
                type: {
                    summary: "number | string"
                }
            }
        }
    }
} satisfies Meta<typeof OxProgressLinear>

export default meta
type Story = StoryObj<typeof OxProgressLinear>

export const Default: Story = {
    args: {
        value: 42,
        bufferValue: 0
    }
}

export const Buffer: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
      return {
          buffer: 0
        }
    },
    methods: {
        changeBuffer (): void {
          this.buffer = this.buffer === 80 ? 0 : 80
        }
    }
}

<div
  v-on:click="changeBuffer"
  class="cardSample"
>
    Click me !
    <ox-progress-linear
      :buffer-value="buffer"
      :color="OxThemeCore.secondary"
      :value="38"
    />
</div>
<div class="cardSample">
    Buffer color
    <ox-progress-linear
      :buffer-color="OxThemeCore.pinkText"
      :buffer-value="80"
      :color="OxThemeCore.blueText"
      :value="30"
    />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxProgressLinear },
        data () {
            return {
                buffer1: 0,
                theme: OxThemeCore
            }
        },
        template: `
          <div>
            <div v-on:click="buffer1 = buffer1 === 80 ? 0 : 80" style="margin: 16px; user-select: none; cursor: pointer; padding: 16px; border: 1px solid var(--background-light); background-color: var(--background-light); border-radius: 4px;">
              Click me !
              <ox-progress-linear :value="38" :color="theme.secondary" :buffer-value="buffer1" />
            </div>
            <div style="margin: 16px; user-select: none; padding: 16px; border: 1px solid var(--background-light); background-color: var(--background-light); border-radius: 4px;">
              Buffer color
              <ox-progress-linear :value="30" :color="theme.blueText" :buffer-value="80" :buffer-color="theme.pinkText" />
            </div>
          </div>
        `
    })
}
