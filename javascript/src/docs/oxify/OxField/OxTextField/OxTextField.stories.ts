import type { Meta, StoryObj } from "@storybook/vue"
import OxTextField from "@oxify/components/OxField/OxTextField/OxTextField.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"
import OxForm from "@oxify/components/OxForm/OxForm.vue"

const emptyFunction = () => {}
const escapeKeyPressed = () => {
    alert("Escape key is pressed")
}

const meta = {
    title: "Core/Form/TextField",
    component: OxTextField,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxTextField },
        template: `<ox-text-field
            :autofocus="$props.autofocus"
            :label="$props.label"
            :counter="$props.counter"
            :dense="$props.dense"
            :rounded="$props.rounded"
            :placeholder="$props.placeholder"
            :disabled="$props.disabled"
            :icon="$props.icon"
            :notNull="$props.notNull"
            :number="$props.number"
            :min="$props.min"
            :max="$props.max"
            :maxlength="$props.maxlength"
            :mask="$props.mask"
            :message="$props.message"
            :name="$props.name"
            :rules="$props.rules"
            :showLoading="$props.showLoading"
            v-model="$props.value"
            @keydown.esc="$props.keydownEscFunction"
        />`
    }),
    argTypes: {
        // @ts-ignore
        list: { table: { disable: true } },
        onPrimary: { table: { disable: true } },
        changeFunction: { table: { disable: true } },
        clickFunction: { table: { disable: true } },
        keydownEscFunction: { table: { disable: true } },
        autofocus: {
            name: "autofocus",
            description: "Automatically focus the field.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        label: {
            name: "label",
            description: "The field label.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        disabled: {
            name: "disabled",
            description: "Disable the field edition.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        dense: {
            name: "dense",
            description: "Reduces the input height. Works only with no label input.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        rounded: {
            name: "rounded",
            description: "Adds a border radius to the input.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        placeholder: {
            name: "placeholder",
            description: "Sets the input's placeholder text.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        icon: {
            name: "icon",
            description: "Set an input icon.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "select",
                options: ["", "add", "cancel", "check", "refresh", "remove"]
            }
        },
        message: {
            name: "message",
            description: "Add a field hint message.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        notNull: {
            name: "not-null",
            description: "Define the input as a mandatory field.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        number: {
            name: "number",
            description: "Set the input as a number-picker.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        counter: {
            name: "counter",
            description: "Creates counter for input length. If the value is true, the maxlength prop must be filled in. Does not apply any validation.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "number | boolean"
                }
            },
            control: {
                type: "number"
            }
        },
        maxlength: {
            name: "maxlength",
            description: "Input maxlength attribute.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "number"
                }
            },
            control: {
                type: "number"
            }
        },
        min: {
            name: "min",
            description: "Number input minimum value.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "number | undefined"
                }
            },
            control: {
                type: "number"
            }
        },
        max: {
            name: "max",
            description: "Number input maximum value.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "number | undefined"
                }
            },
            control: {
                type: "number"
            }
        },
        mask: {
            name: "mask",
            description: "Input mask value.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "string | undefined",
                    detail: '"#": Number (0-9) \n"A": Letter (a-z,A-Z) \n"N": Number or letter (a-z,A-Z,0-9) \n"X": Any symbol \n"?": Optional (next character)'
                }
            },
            control: {
                type: "text"
            }
        },
        name: {
            name: "name",
            description: "Defines the input name.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        rules: {
            name: "rules",
            description: "Functions array defining the mandatory strategy for the field.",
            table: {
                category: "Props",
                type: {
                    summary: "array"
                },
                defaultValue: {
                    summary: "[]"
                }
            }
        },
        showLoading: {
            name: "show-loading",
            description: "Set a loading indicator in the field.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        value: {
            name: "value",
            description: "The base field value.",
            table: {
                category: "Props",
                type: {
                    summary: "string | number"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "text"
            }
        },
        validator: {
            name: "validator",
            description: "The form validator object.",
            table: {
                category: "Props",
                type: {
                    summary: "OxFormValidator"
                }
            }
        },
        change: {
            action: "change",
            description: "Emit `change` when the input change with the `value` as parameter.",
            table: {
                category: "Events"
            },
            control: false
        },
        input: {
            action: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        },
        click: {
            action: "click",
            description: "Emit `click` event when the field is clicked.",
            table: {
                category: "Events"
            },
            control: false
        },
        "click:append": {
            action: "click:append",
            description: "Emit `click:append` event when appended icon is clicked.",
            table: {
                category: "Events"
            },
            control: false
        },
        keydown: {
            action: "keydown",
            description: "Emit `keydown` event when a key is pressed when the field is focused.",
            table: {
                category: "Events"
            },
            control: false
        },
        blur: {
            action: "blur",
            description: "Emit `blur` event when the field is blured.",
            table: {
                category: "Events"
            },
            control: false
        },
        focus: {
            action: "focus",
            description: "Emit `focus` event when the field is focused.",
            table: {
                category: "Events"
            },
            control: false
        }
    }
} satisfies Meta<typeof OxTextField>

export default meta
type Story = StoryObj<typeof OxTextField>

export const Default: Story = {
    args: {
        // @ts-ignore
        label: "Field label",
        value: "Some value",
        clickFunction: emptyFunction,
        changeFunction: emptyFunction,
        keydowEscFunction: emptyFunction
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-text-field
    label="Some field"
    v-model="Hello World"
/>
                        `
            }
        }
    },
    args: {
        // @ts-ignore
        label: "Some field",
        value: "Hello world",
        clickFunction: emptyFunction,
        changeFunction: emptyFunction,
        keydowEscFunction: emptyFunction
    }
}

export const Message: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            text: "Hello",
            message: ""
        }
    },
    methods: {
        updateMessage (value) {
            this.message = value.length > 3 ? "Strong length field" : "Weak length field"
        }
    },
    mounted () {
        this.updateMessage(this.text)
    }
}

<ox-text-field
    label="Some field"
    v-model="text"
    :message="message"
    v-on:input="updateMessage"
/>
                        `
            }
        }
    },
    render: () => ({
        components: { OxTextField },
        data () {
            return {
                text: "Hello",
                message: ""
            }
        },
        methods: {
            updateMessage (value) {
                this.message = value.length > 3 ? "Strong length field" : "Weak length field"
            }
        },
        mounted () {
            // @ts-ignore
            this.updateMessage(this.text)
        },
        template: `
          <ox-text-field
              label="Some field"
              v-model="text"
              :message="message"
              v-on:input="updateMessage"
          />
        `
    })
}

export const KeydownEscape: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    methods: {
        keydownEscape () {
            alert("Escape key is pressed")
        }
    }
}

<ox-text-field
    label="Some Field"
    v-model="Detect the escape key"
    v-on:keydown.esc="keydownEscape"
/>
                        `
            }
        }
    },
    args: {
        // @ts-ignore
        label: "Some field",
        value: "Detect the escape key",
        clickFunction: emptyFunction,
        changeFunction: emptyFunction,
        keydownEscFunction: escapeKeyPressed
    }
}

export const Rules: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data: () => {
     return {
       fieldRules: [
         (v) => v.toString().length >= 3 || "Need at least 3 char",
         (v) => v.toString().indexOf("!") === -1 || "The char '!' can't be used in this field"
       ]
     }
   }
}

<ox-text-field
    label="Form field"
    :rules="fieldRules"
/>
                        `
            }
        }
    },
    render: () => ({
        components: { OxTextField, OxButton, OxForm },
        data: () => {
            return {
                text: "",
                fieldRules: [
                    (v) => v.toString().length >= 3 || "Need at least 3 char",
                    (v) => v.toString().indexOf("!") === -1 || "The char '!' can't be used in this field"
                ]
            }
        },
        template: `
              <ox-text-field
                  label="Form field"
                  :rules="fieldRules"
                  v-model="text"
              />
        `
    })
}

export const Counter: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <ox-text-field
    label="Non-blocking limit"
    :counter="20"
  />
  <ox-text-field
    label="Blocking limit"
    :counter="20"
    :maxlength="20"
  />
  <ox-text-field
    label="Blocking limit without indicator"
    :maxlength="20"
  />
</div>
                        `
            }
        }
    },
    render: () => ({
        components: { OxTextField },
        template: `
          <div>
            <ox-text-field
              label="Non-blocking limit"
              :counter="20"
            />
            <ox-text-field
              label="Blocking limit"
              :counter="20"
              :maxlength="20"
            />
            <ox-text-field
              label="Blocking limit without indicator"
              :maxlength="20"
            />
          </div>
        `
    })
}
