import type { Meta, StoryObj } from "@storybook/vue"
import OxRadioGroup from "@oxify/components/OxField/OxRadioGroup/OxRadioGroup.vue"
import OxRadio from "@oxify/components/OxField/OxRadio/OxRadio.vue"

const meta = {
    title: "Core/Form/RadioGroup",
    component: OxRadioGroup,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxRadioGroup, OxRadio },
        template: `
          <ox-radio-group
              v-model="$props.value"
              :column="$props.column"
              :label="$props.label"
              :row="$props.row"
          >
          <ox-radio 
            label="Label 1"
            value="label_1"
          />
          <ox-radio
              label="Label 2"
              value="label_2"
          />
          <ox-radio
              label="Label 3"
              value="label_3"
          />
          </ox-radio-group>
        `
    }),
    argTypes: {
        // @ts-ignore
        icon: { table: { disable: true } },
        list: { table: { disable: true } },
        message: { table: { disable: true } },
        notNull: { table: { disable: true } },
        onPrimary: { table: { disable: true } },
        showLoading: { table: { disable: true } },
        dense: { table: { disable: true } },
        placeholder: { table: { disable: true } },
        disabled: { table: { disable: true } },
        label: {
            name: "label",
            description: "The radio group label.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        value: {
            name: "value",
            description: "The value of the selected radio in the group.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: ""
                }
            },
            control: {
                type: "text"
            }
        },
        column: {
            name: "column",
            description: "Displays radio buttons in column.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: true
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        row: {
            name: "row",
            description: "Displays radio buttons in row.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        default: {
            name: "default",
            description: "Radio group content.",
            control: {
                type: "text"
            },
            table: {
                category: "Slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        change: {
            action: "change",
            description: "Emit `change` when the input change with the `value` as parameter.",
            table: {
                category: "Events"
            },
            control: false
        },
        input: {
            action: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        },
        validator: {
            name: "validator",
            description: "The form validator object.",
            table: {
                category: "Props",
                type: {
                    summary: "OxFormValidator"
                }
            }
        },
        rules: {
            name: "rules",
            description: "Functions array defining the mandatory strategy for the field.",
            table: {
                category: "Props",
                type: {
                    summary: "array"
                },
                defaultValue: {
                    summary: "[]"
                }
            }
        },
        name: {
            name: "name",
            description: "Defines the input name.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        }
    }
} satisfies Meta<typeof OxRadioGroup>

export default meta
type Story = StoryObj<typeof OxRadioGroup>

export const Default: Story = {
    args: {
        // @ts-ignore
        label: "Radio group label",
        value: "label_1"
    }
}

export const Row: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            radioValue: ""
        }
    }
}

<div>
  <ox-radio-group 
      v-model="radioValue"
      row
  >
    <ox-radio
        label="Field label 1"
        value="Value 1"
    />
    <ox-radio
        label="Field label 2"
        value="Value 2"
    />
    <ox-radio
        label="Field label 3"
        value="Value 3"
    />
  </ox-radio-group>
  <div>Value = {{ radioValue }}</div>
</div>
                        `
            }
        }
    },
    render: () => ({
        components: { OxRadio, OxRadioGroup },
        data: () => {
            return {
                radioValue: ""
            }
        },
        template: `
          <div>
              <ox-radio-group 
                  v-model="radioValue"
                  row
              >
                <ox-radio
                    label="Field label 1"
                    value="Value 1"
                />
                <ox-radio
                    label="Field label 2"
                    value="Value 2"
                />
                <ox-radio
                    label="Field label 3"
                    value="Value 3"
                />
              </ox-radio-group>
              <div>Value = {{ radioValue }}</div>
          </div>
        `
    })
}
