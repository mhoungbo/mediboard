import type { Meta, StoryObj } from "@storybook/vue"
import OxSelect from "@oxify/components/OxField/OxSelect/OxSelect.vue"

const meta = {
    title: "Core/Form/Select",
    component: OxSelect,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxSelect },
        template: `<ox-select
            :label="$props.label"
            :disabled="$props.disabled"
            :icon="$props.icon"
            :message="$props.message"
            :multiple="$props.multiple"
            :placeholder="$props.placeholder"
            :name="$props.name"
            :list="$props.list"
            :notNull="$props.notNull"
            :showLoading="$props.showLoading"
            :value="$props.value"
            :option-id="$props.optionId"
            :option-view="$props.optionView"
            :small="$props.small"
        />`
    }),
    argTypes: {
        // @ts-ignore
        onPrimary: { table: { disable: true } },
        dense: { table: { disable: true } },
        rounded: { table: { disable: true } },
        counter: { table: { disable: true } },
        maxlength: { table: { disable: true } },
        label: {
            name: "label",
            description: "The field label.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        disabled: {
            name: "disabled",
            description: "Disable the field edition.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        icon: {
            name: "icon",
            description: "Set an input icon.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "select",
                options: ["", "add", "cancel", "check", "refresh", "remove"]
            }
        },
        message: {
            name: "message",
            description: "Add a field hint message.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        multiple: {
            name: "multiple",
            description: "Changes select to multiple. Accepts array for value.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        name: {
            name: "name",
            description: "Defines the input name.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        placeholder: {
            name: "placeholder",
            description: "Sets the input's placeholder text.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        rules: {
            name: "rules",
            description: "Functions array defining the mandatory strategy for the field.",
            table: {
                category: "Props",
                type: {
                    summary: "array"
                },
                defaultValue: {
                    summary: "[]"
                }
            }
        },
        notNull: {
            name: "not-null",
            description: "Define the input as a mandatory field.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        list: {
            name: "list",
            description: "Options.",
            table: {
                category: "Props",
                type: {
                    summary: "Object[] | string[]"
                },
                defaultValue: {
                    summary: "[]"
                }
            }
        },
        optionId: {
            name: "option-id",
            description: "Field name for the given list that refer to the objet id.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: "_id"
                }
            }
        },
        optionView: {
            name: "option-view",
            description: "Field name for the given list that refer to the objet view.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: "view"
                }
            }
        },
        small: {
            name: "small",
            description: "Displays the select in small mode.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        showLoading: {
            name: "show-loading",
            description: "Set a loading indicator in the field.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        value: {
            name: "value",
            description: "The base field value.",
            table: {
                category: "Props",
                type: {
                    summary: "string | number"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "text"
            }
        },
        validator: {
            name: "validator",
            description: "The form validator object.",
            table: {
                category: "Props",
                type: {
                    summary: "OxFormValidator"
                }
            }
        },
        change: {
            action: "change",
            description: "Emit `change` when the input change with the `value` as parameter (corresponding to the `option-id` of the selected item).",
            table: {
                category: "Events"
            },
            control: false
        },
        input: {
            action: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        }
    }
} satisfies Meta<typeof OxSelect>

export default meta
type Story = StoryObj<typeof OxSelect>

export const Default: Story = {
    args: {
        // @ts-ignore
        label: "Field label",
        list: [{ _id: "1", view: "Option 1" }, { _id: "2", view: "Option 2" }, { _id: "3", view: "Option 3" }],
        value: "2"
    }
}

export const List: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            label: "String list",
            label2: "Object list",
            list: ["Lorem", "ipsum", "dolor", "sit", "amet"],
            list2: [{ _id: "17", view: "consectetur" }, { _id: "23", view: "adipiscing" }, { _id: "32", view: "elit" }]
        }
    },
    methods: {
        log (value) {
            console.log(value)
        }
    }
}

<div>
  <ox-select
      :label="label"
      :list="list"
      @change="log"
  />
  <ox-select
      :label="label2"
      :list="list2"
      @change="log"
  />
</div>
                        `
            }
        }
    },
    render: () => ({
        components: { OxSelect },
        data () {
            return {
                label: "String list",
                label2: "Object list",
                list: ["Lorem", "ipsum", "dolor", "sit", "amet"],
                list2: [{ _id: "17", view: "consectetur" }, { _id: "23", view: "adipiscing" }, { _id: "32", view: "elit" }]
            }
        },
        methods: {
            log (value) {
                console.log(value)
            }
        },
        template: `
          <div>
              <ox-select
                  class="pb-4"
                  :label="label"
                  :list="list"
                  @change="log"
              />
              <ox-select
                  :label="label2"
                  :list="list2"
                  @change="log"
              />
          </div>
        `
    })
}
