import type { Meta, StoryObj } from "@storybook/vue"
import OxPassword from "@oxify/components/OxField/OxPassword/OxPassword.vue"

const meta = {
    title: "Core/Form/Password",
    component: OxPassword,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxPassword },
        template: `<ox-password
            :autofocus="$props.autofocus"
            :label="$props.label"
            :disabled="$props.disabled"
            :message="$props.message"
            :name="$props.name"
            :notNull="$props.notNull"
            :rules="$props.rules"
            :showLoading="$props.showLoading"
            :value="$props.value"
        />`
    }),
    argTypes: {
        // @ts-ignore
        icon: { table: { disable: true } },
        list: { table: { disable: true } },
        onPrimary: { table: { disable: true } },
        clickFunction: { table: { disable: true } },
        changeFunction: { table: { disable: true } },
        dense: { table: { disable: true } },
        rounded: { table: { disable: true } },
        placeholder: { table: { disable: true } },
        autofocus: {
            name: "autofocus",
            description: "Automatically focus the field.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        label: {
            name: "label",
            description: "The password label.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        disabled: {
            name: "disabled",
            description: "Disable the field edition.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        counter: {
            name: "counter",
            description: "Creates counter for input length. If the value is true, the maxlength prop must be filled in. Does not apply any validation.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "number | boolean"
                }
            },
            control: {
                type: "number"
            }
        },
        maxlength: {
            name: "maxlength",
            description: "Input maxlength attribute.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "number"
                }
            },
            control: {
                type: "number"
            }
        },
        message: {
            name: "message",
            description: "Add a field hint message.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        name: {
            name: "name",
            description: "Defines the input name.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        notNull: {
            name: "not-null",
            description: "Define the input as a mandatory field.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        rules: {
            name: "rules",
            description: "Functions array defining the mandatory strategy for the field.",
            table: {
                category: "Props",
                type: {
                    summary: "array"
                },
                defaultValue: {
                    summary: "[]"
                }
            }
        },
        showLoading: {
            name: "show-loading",
            description: "Set a loading indicator in the field.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        value: {
            name: "value",
            description: "The base field value.",
            table: {
                category: "Props",
                type: {
                    summary: "string | number"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "text"
            }
        },
        validator: {
            name: "validator",
            description: "The form validator object.",
            table: {
                category: "Props",
                type: {
                    summary: "OxFormValidator"
                }
            }
        },
        change: {
            action: "change",
            description: "Emit `change` when the input change with the `value` as parameter.",
            table: {
                category: "Events"
            },
            control: false
        },
        input: {
            action: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        },
        click: {
            action: "click",
            description: "Emit `click` event when the field is clicked.",
            table: {
                category: "Events"
            },
            control: false
        },
        keydown: {
            action: "keydown",
            description: "Emit `keydown` event when a key is pressed when the field is focused.",
            table: {
                category: "Events"
            },
            control: false
        },
        blur: {
            action: "blur",
            description: "Emit `blur` event when the field is blured.",
            table: {
                category: "Events"
            },
            control: false
        },
        focus: {
            action: "focus",
            description: "Emit `focus` event when the field is focused.",
            table: {
                category: "Events"
            },
            control: false
        }
    }
} satisfies Meta<typeof OxPassword>

export default meta
type Story = StoryObj<typeof OxPassword>

export const Default: Story = {
    args: {
        // @ts-ignore
        label: "Field label",
        value: "Some value"
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            password: "Hello World",
        }
    }
}

<ox-password
    label="Some password"
    v-model="password"
/>
                        `
            }
        }
    },
    args: {
        // @ts-ignore
        label: "Some Password",
        value: "Hello World"
    }
}
