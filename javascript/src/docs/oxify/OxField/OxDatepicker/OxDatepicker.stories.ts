import type { Meta, StoryObj } from "@storybook/vue"
import OxDatepicker from "@oxify/components/OxField/OxDatepicker/OxDatepicker.vue"

const meta = {
    title: "Core/Form/Datepicker",
    component: OxDatepicker,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDatepicker },
        template: `<ox-datepicker
            v-model="$props.value"
            :format="$props.format"
            :default-picker-date="$props.defaultPickerDate"
            :default-picker-time="$props.defaultPickerTime"
            :show-now="$props.showNow"
            :show-today="$props.showToday"
            :min-date="$props.minDate"
            :max-date="$props.maxDate"
            :allowed-dates="$props.allowedDates"
            :min-time="$props.minTime"
            :max-time="$props.maxTime"
            :allowed-hours="$props.allowedHours"
            :allowed-minutes="$props.allowedMinutes"
            :allowed-seconds="$props.allowedSeconds"
        />`
    }),
    argTypes: {
        allowedDates: {
            name: "allowed-dates",
            description: "A function that can restrict the dates. Works on `date`, `datetime` and `month`. [See example](#allowed-dates-and-times)",
            table: {
                category: "Props",
                type: {
                    summary: "Function"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "Function"
            }
        },
        allowedHours: {
            name: "allowed-hours",
            description: "A function that can restrict the hours. Works on `time` and `datetime`.  [See example](#allowed-dates-and-times)",
            table: {
                category: "Props",
                type: {
                    summary: "Function | Number[]"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "Function"
            }
        },
        allowedMinutes: {
            name: "allowed-minutes",
            description: "A function that can restrict the minutes. Works on `time` and `datetime`.  [See example](#allowed-dates-and-times)",
            table: {
                category: "Props",
                type: {
                    summary: "Function | Number[]"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "Function"
            }
        },
        allowedSeconds: {
            name: "allowed-seconds",
            description: "A function that can restrict the seconds. Works on `time` and `datetime`.  [See example](#allowed-dates-and-times)",
            table: {
                category: "Props",
                type: {
                    summary: "Function | Number[]"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "Function"
            }
        },
        autofocus: { table: { disable: true } },
        counter: { table: { disable: true } },
        defaultPickerDate: {
            name: "default-picker-date",
            description: "Set the default selected date when the datepicker is opened.",
            table: {
                category: "Props",
                type: {
                    summary: "String"
                },
                defaultValue: {
                    summary: undefined
                }
            }
        },
        defaultPickerTime: {
            name: "default-picker-time",
            description: "Set the default selected time when the timepicker is opened.",
            table: {
                category: "Props",
                type: {
                    summary: "String"
                },
                defaultValue: {
                    summary: undefined
                }
            }
        },
        dense: { table: { disable: true } },
        disabled: {
            name: "disabled",
            description: "Disable the field edition.",
            table: {
                category: "Props",
                type: {
                    summary: "Boolean"
                },
                defaultValue: {
                    summary: false
                }
            }
        },
        format: {
            name: "format",
            description: "Define the time of the picker, it can be `date`, `time`, `datetime` or `month`.",
            table: {
                category: "Props",
                type: {
                    summary: "String",
                    detail: ' "date" | "time" | "datetime" | "month" '
                },
                defaultValue: {
                    summary: "date"
                }
            },
            control: {
                type: "select",
                options: ["date", "time", "datetime", "month"]
            }
        },
        icon: { table: { disable: true } },
        label: {
            name: "label",
            description: "",
            table: {
                category: "Props",
                type: {
                    summary: "String"
                },
                defaultValue: {
                    summary: ""
                }
            }
        },
        list: { table: { disable: true } },
        mask: { table: { disable: true } },
        max: { table: { disable: true } },
        maxDate: {
            name: "max-date",
            description: "This locks the max date to be select. Works on `date`, `datetime` and `month`. [See example](#allowed-dates-and-times)",
            table: {
                category: "Props",
                type: {
                    summary: "String"
                }
            }
        },
        maxlength: { table: { disable: true } },
        maxTime: {
            name: "max-time",
            description: "This locks the max time to be select. Works on `time` and `datetime`. [See example](#allowed-dates-and-times)",
            table: {
                category: "Props",
                type: {
                    summary: "String"
                }
            }
        },
        message: { table: { disable: true } },
        min: { table: { disable: true } },
        minDate: {
            name: "min-date",
            description: "This locks the min date to be select. Works on `date`, `datetime` and `month`. [See example](#allowed-dates-and-times)",
            table: {
                category: "Props",
                type: {
                    summary: "String"
                }
            }
        },
        minTime: {
            name: "min-time",
            description: "This locks the min time to be select. Works on `time` and `datetime`. [See example](#allowed-dates-and-times)",
            table: {
                category: "Props",
                type: {
                    summary: "String"
                }
            }
        },
        name: { table: { disable: true } },
        notNull: {
            name: "not-null",
            description: "",
            table: {
                category: "Props",
                type: {
                    summary: "Boolean"
                }
            }
        },
        number: { table: { disable: true } },
        onPrimary: {
            name: "on-primary",
            description: "",
            table: {
                category: "Props",
                type: {
                    summary: "Boolean"
                }
            }
        },
        placeholder: { table: { disable: true } },
        rounded: { table: { disable: true } },
        rules: {
            name: "rules",
            description: "",
            table: {
                category: "Props",
                type: {
                    summary: "Array"
                }
            }
        },
        showLoading: {
            name: "show-loading",
            description: "",
            table: {
                category: "Props",
                type: {
                    summary: "Boolean"
                }
            }
        },
        showNow: {
            name: "show-now",
            description: "Display the button now in the `time` or `datetime` picker.",
            table: {
                category: "Props",
                type: {
                    summary: "Boolean"
                }
            }
        },
        showToday: {
            name: "show-today",
            description: "Display the button today in the `date`, `datetime` or `month` picker.",
            table: {
                category: "Props",
                type: {
                    summary: "Boolean"
                }
            }
        },
        validator: {
            name: "validator",
            description: "",
            table: {
                category: "Props",
                type: {
                    summary: "OxFormValidator"
                }
            }
        },
        value: {
            name: "value",
            description: "The default value of the field.",
            table: {
                category: "Props",
                type: {
                    summary: "String",
                    detail: "2023-02-23 10:30"
                }
            },
            control: {
                type: "text"
            }
        },
        blur: { table: { disable: true } },
        change: {
            name: "change",
            description: "Emit `change` event when clicked with the new `value` as event parameter.",
            table: {
                category: "Events"
            }
        },
        click: { table: { disable: true } },
        "click:append": { table: { disable: true } },
        focus: { table: { disable: true } },
        input: {
            name: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            }
        },
        keydown: { table: { disable: true } }
    }
} satisfies Meta<typeof OxDatepicker>

export default meta
type Story = StoryObj<typeof OxDatepicker>

export const Default: Story = {
    args: {
        format: "datetime"
    }
}

export const LimitDateTime: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    methods: {
        allowedDates (val) {
          const day = parseInt(val.split('-')[2], 10)
          return day >= 10 && day <= 20
        },
        allowedMinutes (val) {
          return val >= 15 && val <= 45
        }
    }
}

<ox-datepicker
  :allowed-dates="allowedDates"
  :allowed-minutes="allowedMinutes"
  format="datetime"
  min-date="2022-10-13"
  max-time="20:30"
  min-time="10:30"
/>
                        `
            }
        }
    },
    args: {
        format: "datetime",
        minDate: "2022-10-13",
        minTime: "10:30",
        maxTime: "20:30",
        allowedDates: (val) => {
            const day = parseInt(val.split("-")[2], 10)
            return day >= 10 && day <= 20
        },
        allowedMinutes: (val) => {
            return val >= 15 && val <= 45
        }
    }
}
