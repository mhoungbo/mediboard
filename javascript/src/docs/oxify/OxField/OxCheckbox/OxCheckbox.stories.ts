import type { Meta, StoryObj } from "@storybook/vue"
import OxCheckbox from "@oxify/components/OxField/OxCheckbox/OxCheckbox.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"
import OxForm from "@oxify/components/OxForm/OxForm.vue"
import OxFormValidator from "@oxify/components/OxForm/OxFormValidator"

const meta = {
    title: "Core/Form/Checkbox",
    component: OxCheckbox,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxCheckbox },
        template: `<ox-checkbox
            :emphasis="$props.emphasis"
            :disabled="$props.disabled"
            :label="$props.label"
            :name="$props.name"
            :rules="$props.rules"
            :value="$props.value"
            :checkboxValue="$props.checkboxValue" 
        />`
    }),
    argTypes: {
        // @ts-ignore
        icon: { table: { disable: true } },
        list: { table: { disable: true } },
        message: { table: { disable: true } },
        notNull: { table: { disable: true } },
        onPrimary: { table: { disable: true } },
        showLoading: { table: { disable: true } },
        dense: { table: { disable: true } },
        rounded: { table: { disable: true } },
        placeholder: { table: { disable: true } },
        emphasis: {
            name: "emphasis",
            description: "Label's emphasis.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "high"
                },
                type: {
                    summary: "string",
                    detail: ' "low" | "medium" | "high" '
                }
            },
            control: {
                type: "radio",
                options: ["low", "medium", "high"]
            }
        },
        label: {
            name: "label",
            description: "The field label.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        checkboxValue: {
            name: "checkbox-value",
            description: "Value of the checkbox.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: ""
                }
            },
            control: {
                type: "text"
            }
        },
        disabled: {
            name: "disabled",
            description: "Disable the field edition.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        name: {
            name: "name",
            description: "Defines the input name.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        value: {
            name: "value",
            description: "The checked state of the checkbox.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        validator: {
            name: "validator",
            description: "The form validator object.",
            table: {
                category: "Props",
                type: {
                    summary: "OxFormValidator"
                }
            }
        },
        change: {
            action: "change",
            description: "Emit `change` event when clicked with the new `value` as event parameter",
            table: {
                category: "Events"
            },
            control: false
        },
        input: {
            action: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        },
        rules: {
            name: "rules",
            description: "Functions array defining the mandatory strategy for the field.",
            table: {
                category: "Props",
                type: {
                    summary: "array"
                },
                defaultValue: {
                    summary: "[]"
                }
            }
        },
        slotLabel: {
            name: "label",
            description: "Replaces the default label",
            control: {
                type: "text"
            },
            table: {
                category: "Slots",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxCheckbox>

export default meta
type Story = StoryObj<typeof OxCheckbox>

export const Default: Story = {
    args: {
        // @ts-ignore
        label: "Field label",
        value: false
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-checkbox
  label="Some field"
  v-model="true"
/>
                        `
            }
        }
    },
    args: {
        // @ts-ignore
        label: "Some field",
        value: true
    }
}

export const Mandatory: Story = {
    parameters: {
        docs: {
            source: {
                code: `

export default {
    name: "MyComponent",
    data () {
        return {
            checkbox: false,
            validator: new OxFormValidator()
        }
    },
    mounted () {
        this.validator.rules = {
            checkbox: [
                (v) => v === true || "You must agree to continue!"
            ]
        }
    }
}

<ox-form 
  ref="form" 
  :validator="validator"
>
  <ox-checkbox
      label="Do you agree"
      v-model="checkbox"
      name="checkbox"
      :validator="validator"
  />
  <ox-button
      label="Validate"
      v-on:click="$refs.form.validate()"
  />
</ox-form>
                        `
            }
        }
    },
    render: () => ({
        components: { OxCheckbox, OxButton, OxForm },
        data () {
            return {
                checkbox: false,
                validator: new OxFormValidator()
            }
        },
        mounted () {
            // @ts-ignore
            this.validator.rules = {
                checkbox: [
                    (v) => v === true || "You must agree to continue!"
                ]
            }
        },
        template: `
          <ox-form ref="form" :validator="validator">
              <ox-checkbox
                  label="Do you agree"
                  v-model="checkbox"
                  name="checkbox"
                  :validator="validator"
              />
              <ox-button
                  label="Validate"
                  v-on:click="$refs.form.validate()"
              />
          </ox-form>
        `
    })
}

export const Multiple: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            checkBoxesChecked: [],
        }
    }
}

<div>
  <ox-checkbox checkbox-value="John" v-model="checkBoxesChecked" label="John"></ox-checkbox>
  <ox-checkbox checkbox-value="Doe" v-model="checkBoxesChecked" label="Doe"></ox-checkbox>
  <ox-checkbox checkbox-value="Foo" v-model="checkBoxesChecked" label="Foo"></ox-checkbox>
  <ox-checkbox checkbox-value="Bar" v-model="checkBoxesChecked" label="Bar"></ox-checkbox>
  <div>{{ checkBoxesChecked }}</div>
</div>
                        `
            }
        }
    },
    render: () => ({
        components: { OxCheckbox, OxButton, OxForm },
        data () {
            return {
                checkBoxesChecked: []
            }
        },
        template: `
          <div>
              <ox-checkbox checkbox-value="John" v-model="checkBoxesChecked" label="John"></ox-checkbox>
              <ox-checkbox checkbox-value="Doe" v-model="checkBoxesChecked" label="Doe"></ox-checkbox>
              <ox-checkbox checkbox-value="Foo" v-model="checkBoxesChecked" label="Foo"></ox-checkbox>
              <ox-checkbox checkbox-value="Bar" v-model="checkBoxesChecked" label="Bar"></ox-checkbox>
              <div>{{ checkBoxesChecked }}</div>
          </div>
        `
    })
}
