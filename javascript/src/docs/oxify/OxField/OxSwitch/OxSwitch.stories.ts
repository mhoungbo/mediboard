import type { Meta, StoryObj } from "@storybook/vue"
import OxSwitch from "@oxify/components/OxField/OxSwitch/OxSwitch.vue"

const meta = {
    title: "Core/Form/Switch",
    component: OxSwitch,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxSwitch },
        template: `<ox-switch 
            :disabled="$props.disabled" 
            :label="$props.label"
            :label-position="$props.labelPosition"
            :name="$props.name" 
            :switch-value="$props.switchValue" 
            :value="$props.value"
        />`
    }),
    argTypes: {
        // @ts-ignore
        label: {
            name: "label",
            description: "The field label.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        disabled: {
            name: "disabled",
            description: "Disable the field edition.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        name: {
            name: "name",
            description: "Defines the input name.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        labelPosition: {
            name: "label-position",
            description: "Defines the label position compared to the input.",
            table: {
                category: "Props",
                type: {
                    summary: "string",
                    detail: ' "left" | "right" '
                },
                defaultValue: {
                    summary: "right"
                }
            },
            control: {
                type: "radio",
                options: ["left", "right"]
            }
        },
        switchValue: {
            name: "switch-value",
            description: "The base input's value.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: ""
                }
            },
            control: {
                type: "text"
            }
        },
        value: {
            name: "value",
            description: "The v-model value.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        change: {
            action: "change",
            description: "Emit `change` event when clicked with the new `value` as event parameter",
            table: {
                category: "Events"
            },
            control: false
        },
        input: {
            action: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        },
        icon: { table: { disable: true } },
        list: { table: { disable: true } },
        message: { table: { disable: true } },
        notNull: { table: { disable: true } },
        onPrimary: { table: { disable: true } },
        rules: { table: { disable: true } },
        showLoading: { table: { disable: true } },
        dense: { table: { disable: true } },
        rounded: { table: { disable: true } },
        placeholder: { table: { disable: true } },
        validator: { table: { disable: true } }
    }
} satisfies Meta<typeof OxSwitch>

export default meta
type Story = StoryObj<typeof OxSwitch>

export const Default: Story = {
    args: {
        // @ts-ignore
        label: "Field label",
        value: false
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-switch
  label="Some field"
  v-model="true"
/>
                        `
            }
        }
    },
    args: {
        // @ts-ignore
        label: "Field label",
        value: true
    }
}
