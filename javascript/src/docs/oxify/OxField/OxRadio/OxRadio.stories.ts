import type { Meta, StoryObj } from "@storybook/vue"
import OxRadio from "@oxify/components/OxField/OxRadio/OxRadio.vue"
import OxRadioGroup from "@oxify/components/OxField/OxRadioGroup/OxRadioGroup.vue"

const meta = {
    title: "Core/Form/Radio",
    component: OxRadio,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxRadio, OxRadioGroup },
        template: `
          <ox-radio-group 
              v-model="$data.radioValue" 
              :row="true"
          >
            <ox-radio
              :disabled="$props.disabled"
              :emphasis="$props.emphasis"
              :expand="$props.expand"
              :label="$props.label"
              :value="$props.value"
            />
          </ox-radio-group>
        `
    }),
    argTypes: {
        // @ts-ignore
        icon: { table: { disable: true } },
        list: { table: { disable: true } },
        message: { table: { disable: true } },
        notNull: { table: { disable: true } },
        onPrimary: { table: { disable: true } },
        showLoading: { table: { disable: true } },
        dense: { table: { disable: true } },
        placeholder: { table: { disable: true } },
        label: {
            name: "label",
            description: "The field label. Displayed if the label slot is not defined.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        disabled: {
            name: "disabled",
            description: "Disable the field edition.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        value: {
            name: "value",
            description: "The value used when the component is selected in a group. If not provided, the index will be used.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: ""
                }
            },
            control: {
                type: "text"
            }
        },
        emphasis: {
            name: "emphasis",
            description: "Label's emphasis.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "high"
                },
                type: {
                    summary: "string",
                    detail: ' "low" | "medium" | "high" '
                }
            },
            control: {
                type: "radio",
                options: ["low", "medium", "high"]
            }
        },
        expand: {
            name: "expand",
            description: "Displays the radio in expand mode.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        slotLabel: {
            name: "label",
            description: "Replaces the default label",
            control: {
                type: "text"
            },
            table: {
                category: "Slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        change: {
            action: "change",
            description: "Emit `change` when the input change with the `value` as parameter.",
            table: {
                category: "Events"
            },
            control: false
        },
        input: {
            action: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        },
        validator: {
            name: "validator",
            description: "The form validator object.",
            table: {
                category: "Props",
                type: {
                    summary: "OxFormValidator"
                }
            }
        },
        rules: {
            name: "rules",
            description: "Functions array defining the mandatory strategy for the field.",
            table: {
                category: "Props",
                type: {
                    summary: "array"
                },
                defaultValue: {
                    summary: "[]"
                }
            }
        },
        name: {
            name: "name",
            description: "Defines the input name.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        }
    }
} satisfies Meta<typeof OxRadio>

export default meta
type Story = StoryObj<typeof OxRadio>

export const Default: Story = {
    args: {
        // @ts-ignore
        label: "Field label",
        value: ""
    }
}

export const Expand: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            radioValue: ""
        }
    }
}

<div>
  <ox-radio-group v-model="radioValue">
    <ox-radio
        expand
        label="Field label 1"
        value="Value 1"
    />
    <ox-radio
        expand
        label="Field label 2"
        value="Value 2"
    />
    <ox-radio
        expand
        label="Field label 3"
        value="Value 3"
    />
  </ox-radio-group>
  <div>Value = {{ radioValue }}</div>
</div>
                        `
            }
        }
    },
    render: () => ({
        components: { OxRadio, OxRadioGroup },
        data: () => {
            return {
                radioValue: ""
            }
        },
        template: `
          <div>
              <ox-radio-group v-model="radioValue">
                <ox-radio
                    expand
                    label="Field label 1"
                    value="Value 1"
                />
                <ox-radio
                    expand
                    label="Field label 2"
                    value="Value 2"
                />
                <ox-radio
                    expand
                    label="Field label 3"
                    value="Value 3"
                />
              </ox-radio-group>
              <div>Value = {{ radioValue }}</div>
          </div>
        `
    })
}
