import type { Meta, StoryObj } from "@storybook/vue"
import OxDropdownButton from "@oxify/components/OxDropdownButton/OxDropdownButton.vue"
import { OxDropdownButtonActionsGroups, OxDropdownButtonActionTypes } from "@oxify/types/OxDropdownButtonActionTypes"
import OxButton from "@oxify/components/OxButton/OxButton.vue"

const dropdownActionsWithIcon: OxDropdownButtonActionTypes[] = [
    {
        label: "Action add",
        eventName: "actionadd",
        icon: "add"
    },
    {
        label: "Alternative action",
        eventName: "alternativeaction"
    },
    {
        label: "Disabled action",
        eventName: "disabledaction",
        icon: "account",
        disabled: true
    }
]

// eslint-disable-next-line @typescript-eslint/no-empty-function
const emptyFunction = () => {}

const meta = {
    title: "Core/Oxify/Dropdown Button",
    component: OxDropdownButton,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDropdownButton },
        template: `
          <ox-dropdown-button
            :alt-actions="$props.altActions"
            :bottom="$props.bottom"
            :button-style="$props.buttonStyle"
            :disabled="$props.disabled"
            :label="$props.label"
            :left="$props.left"
            :right="$props.right"
            :split="$props.split"
            :title="$props.title"
            :top="$props.top"
            :separated="$props.separated"
            @click="$props.mainClickFunction"
            @altclick="$props.altActionFunction"
            @actionadd="$props.addClickFunction"
            @alternativeaction="$props.altClickFunction"
          >
          </ox-dropdown-button>
        `
    }),
    argTypes: {
        // @ts-ignore
        mainClickFunction: { table: { disable: true } },
        addClickFunction: { table: { disable: true } },
        altClickFunction: { table: { disable: true } },
        altActionFunction: { table: { disable: true } },
        default: { table: { disable: true } },
        attach: {
            name: "attach",
            description: "Specifies which `DOM` element that this component should detach to. String can be any valid querySelector and Object can be any valid Node. This will attach at the end of the entrypoint by default.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "false"
                },
                type: {
                    summary: "any"
                }
            },
            control: {
                type: "text"
            }
        },
        label: {
            name: "label",
            description: "Dropdown button's label. Set empty string to create an icon-only dropdown button (with vertical 3 dots icon).",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        buttonStyle: {
            name: "button-style",
            description: "Main style of dropdown button. If its value is `tertiary`  or `tertiary-dark`, the `split` property must be `false`.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "secondary"
                },
                type: {
                    summary: "string",
                    detail: ' "primary" | "secondary" | "tertiary" | "tertiary-dark" '
                }
            },
            control: {
                type: "radio",
                options: ["primary", "secondary", "tertiary", "tertiary-dark"]
            }
        },
        title: {
            name: "title",
            description: "Dropdown button's title display on hover.",
            control: {
                type: "text"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            }
        },
        split: {
            name: "split",
            description: "Separates the main action from the rest of the actions.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        left: {
            name: "left",
            description: "Aligns the dropdown menu towards the left.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        right: {
            name: "right",
            description: "Aligns the dropdown menu towards the right.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        top: {
            name: "top",
            description: "Aligns the dropdown menu towards the top.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        bottom: {
            name: "bottom",
            description: "Aligns the dropdown menu towards the bottom.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        altActions: {
            name: "alt-actions",
            description: "List of actions to display in the dropdown.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: []
                },
                type: {
                    summary: "OxDropdownButtonActionTypes[] | OxDropdownButtonActionsGroups[]",
                    detail: "{ \n" +
                        "   label: string \n" +
                        "   eventName: string \n" +
                        "   icon?: string \n" +
                        "   iconColor?: string \n" +
                        "   disabled?: boolean \n" +
                        "   hide?: boolean \n" +
                        "   customIcon?: boolean \n" +
                        "} \n" +
                        "OR \n" +
                        "{ \n" +
                        "   label: string \n" +
                        "   actions: OxDropdownButtonActionTypes[] \n" +
                        "   hide?: boolean \n" +
                        "}"
                }
            },
            control: {
                type: "object"
            }
        },
        disabled: {
            title: "disabled",
            description: "Display the dropdown button in an inactive state.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        separated: {
            title: "separated",
            description: "Create a divider between groups",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        click: {
            action: "click",
            description: "Emit `click` event when clicked on main button in `split` form.",
            table: {
                category: "Events"
            },
            control: false
        },
        customEvents: {
            action: "customevent",
            description: "Emit the custom event for each action defined in the `alt-actions` property. See examples below for more details.",
            table: {
                category: "Events",
                type: {
                    summary: "custom",
                    detail: "Name of the event defined by the eventName key in the alt-actions property."
                }
            },
            control: false
        },
        altclick: {
            action: "altclick",
            description: "Emit `altclick` event when an alt-action (display in dropdown menu) is clicked. This event provides in arguments the eventName of the alt-action clicked",
            table: {
                category: "Events"
            },
            control: false
        },
        change: {
            action: "change",
            description: "Emit `change` event actions display change with `true` if actions are visible, `false` otherwise",
            table: {
                category: "Events"
            },
            control: false
        },
        "'custom-icon-' + action.eventName": {
            name: "custom-icon-{{eventName}}",
            description: "This slot is displayed only if the alt action has the icon property set. {{eventName}} is the event name linked to the action. By default, it displays the requested icon.",
            control: false,
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        dropdown: {
            name: "dropdown",
            description: "Slot for custom dropdown. Replace default alt-actions list. See example below.",
            control: false,
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxDropdownButton>

export default meta
type Story = StoryObj<typeof OxDropdownButton>

export const Default: Story = {
    args: {
        altActions: dropdownActionsWithIcon,
        buttonStyle: "primary",
        label: "Dropdown button",
        // @ts-ignore
        mainClickFunction: emptyFunction,
        addClickFunction: emptyFunction,
        altClickFunction: emptyFunction,
        altActionFunction: emptyFunction
    }
}

export const Split: Story = {
    parameters: {
        docs: {
            source: {
                code: `
                
export default {
    name: "MyComponent",
    data () {
        return {
            altActions: [
              {
                label: "Action add",
                eventName: "actionadd",
                icon: "add"
              },
              {
                label: "Alternative action",
                eventName: "altaction"
              },
              {
                label: "Disabled action",
                eventName: "disabledaction",
                icon: "account",
                disabled: true
              }
            ] as OxDropdownButtonActionTypes[]
        }
    },
    methods: {
        mainAction () {
            alert("Main Action")
        },
        addFunction () {
            alert("Add Action")
        },
        altFunction () {
            alert("Alternative Action")
        }
    }
}
<ox-dropdown-button
  :alt-actions="altActions"
  label="Main action"
  split
  @actionadd="addFunction"
  @altaction="altFunction"
  @click="mainAction"
/>
                `
            }
        }
    },
    render: () => ({
        components: { OxDropdownButton },
        data () {
            return {
                altActions: [
                    {
                        label: "Action add",
                        eventName: "actionadd",
                        icon: "add"
                    },
                    {
                        label: "Alternative action",
                        eventName: "altaction"
                    },
                    {
                        label: "Disabled action",
                        eventName: "disabledaction",
                        icon: "account",
                        disabled: true
                    }
                ] as OxDropdownButtonActionTypes[]
            }
        },
        methods: {
            mainAction () {
                alert("Main Action")
            },
            addFunction () {
                alert("Add Action")
            },
            altFunction () {
                alert("Alternative Action")
            }
        },
        template: `
          <ox-dropdown-button
              :alt-actions="altActions"
              label="Main action"
              split
              @actionadd="addFunction"
              @altaction="altFunction"
              @click="mainAction"
          />
        `
    })
}

export const AltClick: Story = {
    parameters: {
        docs: {
            source: {
                code: `
                
export default {
    name: "MyComponent",
    data () {
        return {
            altActions: [
              {
                label: "Action add",
                eventName: "actionadd",
                icon: "add"
              },
              {
                label: "Alternative action",
                eventName: "altaction"
              },
              {
                label: "Disabled action",
                eventName: "disabledaction",
                icon: "account",
                disabled: true
              }
            ] as OxDropdownButtonActionTypes[]
        }
    },
    methods: {
        mainAction () {
            alert("Main Action")
        },
        altFunction (event: string) {
            alert("Alt action clicked for event : " + event)
        }
    }
}
<ox-dropdown-button
  :alt-actions="altActions"
  label="Main action"
  split
  @altclick="altFunction"
  @click="mainAction"
/>
                `
            }
        }
    },
    render: () => ({
        components: { OxDropdownButton },
        data () {
            return {
                altActions: [
                    {
                        label: "Action add",
                        eventName: "actionadd",
                        icon: "add"
                    },
                    {
                        label: "Alternative action",
                        eventName: "altaction"
                    },
                    {
                        label: "Disabled action",
                        eventName: "disabledaction",
                        icon: "account",
                        disabled: true
                    }
                ] as OxDropdownButtonActionTypes[]
            }
        },
        methods: {
            mainAction () {
                alert("Main Action")
            },
            altFunction (event: string) {
                alert("Alt action clicked for event : " + event)
            }
        },
        template: `
          <ox-dropdown-button
              :alt-actions="altActions"
              label="Main action"
              split
              @altclick="altFunction"
              @click="mainAction"
          />
        `
    })
}

export const Icon: Story = {
    parameters: {
        docs: {
            source: {
                code: `
                
export default {
    name: "MyComponent",
    data () {
        return {
            altActions: [
              {
                label: "Action add",
                eventName: "actionadd",
                icon: "add"
              },
              {
                label: "Alternative action",
                eventName: "altaction"
              },
              {
                label: "Disabled action",
                eventName: "disabledaction",
                icon: "account",
                disabled: true
              }
            ] as OxDropdownButtonActionTypes[]
        }
    },
    methods: {
        addFunction () {
            alert("Add Action")
        },
        altFunction () {
            alert("Alternative Action")
        }
    }
}
<ox-dropdown-button
  :alt-actions="altActions"
  button-style="tertiary"
  @actionadd="addFunction"
  @altaction="altFunction"
/>
                `
            }
        }
    },
    render: () => ({
        components: { OxDropdownButton },
        data () {
            return {
                altActions: [
                    {
                        label: "Action add",
                        eventName: "actionadd",
                        icon: "add",
                        iconColor: "primary"
                    },
                    {
                        label: "Alternative action",
                        eventName: "altaction"
                    },
                    {
                        label: "Disabled action",
                        eventName: "disabledaction",
                        icon: "account",
                        disabled: true
                    }
                ] as OxDropdownButtonActionTypes[]
            }
        },
        methods: {
            addFunction () {
                alert("Add Action")
            },
            altFunction () {
                alert("Alternative Action")
            }
        },
        template: `
          <ox-dropdown-button
              :alt-actions="altActions"
              button-style="tertiary"
              @actionadd="addFunction"
              @altaction="altFunction"
          />
        `
    })
}

export const Groups: Story = {
    parameters: {
        docs: {
            source: {
                code: `
                
export default {
    name: "MyComponent",
    data () {
        return {
            altActions: [
            {
                label: "Actions",
                actions: [
                    {
                        label: "Action add",
                        eventName: "actionadd",
                        icon: "add"
                    },
                    {
                        label: "Alternative action",
                        eventName: "altaction"
                    }
                ]
            },
                {
                    label: "Actions",
                    actions: [
                        {
                            label: "Disabled action",
                            eventName: "disabledaction",
                            icon: "account",
                            disabled: true
                        }
                    ]
                }
            ] as OxDropdownButtonActionsGroups[]
        }
    },
    methods: {
        addFunction () {
            alert("Add Action")
        },
        altFunction () {
            alert("Alternative Action")
        }
    }
}
<ox-dropdown-button
  :alt-actions="altActions"
  button-style="primary"
  label="Dropdown button"
  @actionadd="addFunction"
  @altaction="altFunction"
  separated
/>
                `
            }
        }
    },
    render: () => ({
        components: { OxDropdownButton },
        data () {
            return {
                altActions: [
                    {
                        label: "Actions",
                        actions: [
                            {
                                label: "Action add",
                                eventName: "actionadd",
                                icon: "add"
                            },
                            {
                                label: "Alternative action",
                                eventName: "altaction"
                            }
                        ]
                    },
                    {
                        label: "diabledActions",
                        actions: [
                            {
                                label: "Disabled action",
                                eventName: "disabledaction",
                                icon: "account",
                                disabled: true
                            }
                        ]
                    }
                ] as OxDropdownButtonActionsGroups[]
            }
        },
        methods: {
            addFunction () {
                alert("Add Action")
            },
            altFunction () {
                alert("Alternative Action")
            }
        },
        template: `
          <ox-dropdown-button
              :alt-actions="altActions"
              button-style="primary"
              label="Dropdown button"
              @actionadd="addFunction"
              @altaction="altFunction"
              separated
          />
        `
    })
}

export const Custom: Story = {
    parameters: {
        docs: {
            source: {
                code: `
                
export default {
    name: "MyComponent",
    data () {
        return {
            altActions: [
              {
                label: "Action add",
                eventName: "actionadd",
                icon: "add"
              },
              {
                label: "Alternative action",
                eventName: "altaction"
              },
              {
                label: "Disabled action",
                eventName: "disabledaction",
                icon: "account",
                disabled: true
              }
            ] as OxDropdownButtonActionTypes[]
        }
    },
    methods: {
        mainAction () {
          alert("Main Action")
        },
        altFunction (event: string) {
          alert("Alt action clicked for event : " + event)
        }
    }
}
<ox-dropdown-button
  :alt-actions="altActions"
  label="Main action"
  split
  @click="mainAction"
  @altclick="altFunction"
>
  <template v-slot:dropdown="{ actions, dispatch }">
    <div class="Dropdown">
        <div class="Dropdown-text">
          <div class="Dropdown-title">
            Custom dropdown
          </div>
          <div class="Dropdown-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
          </div>
        </div>
        <div class="Dropdown-actions">
          <ox-button
              v-for="action in actions"
              :key="action.eventName"
              button-style="tertiary"
              :disabled="action.disabled"
              :icon="action.icon"
              :label="action.label"
              v-on:click="dispatch(action.eventName)"
          >
            {{ action.label }}
          </ox-button>
        </div>
    </div>
  </template>
</ox-dropdown-button>
                `
            }
        }
    },
    render: () => ({
        components: { OxDropdownButton, OxButton },
        data () {
            return {
                altActions: [
                    {
                        label: "Action add",
                        eventName: "actionadd",
                        icon: "add"
                    },
                    {
                        label: "Alternative action",
                        eventName: "altaction"
                    },
                    {
                        label: "Disabled action",
                        eventName: "disabledaction",
                        icon: "account",
                        disabled: true
                    }
                ] as OxDropdownButtonActionTypes[]
            }
        },
        methods: {
            mainAction () {
                alert("Main Action")
            },
            altFunction (event: string) {
                alert("Alt action clicked for event : " + event)
            }
        },
        template: `
          <ox-dropdown-button
              :alt-actions="altActions"
              label="Main action"
              split
              @click="mainAction"
              @altclick="altFunction"
          >
          <template v-slot:dropdown="{ actions, dispatch }">
            <div style="background: var(--background-elevation-8); display: flex; flex-direction: column; padding: 16px; border-radius: 6px; gap: 12px; width: 528px;">
              <div style="display: flex; flex-direction: column; gap: 4px">
                <div style="font-family: 'Roboto', 'Nunito', sans-serif; font-size: 20px; font-style: normal; font-weight: 500; letter-spacing: 0.15px; line-height: 30px; color: var(--color-on-background-high)">
                  Custom dropdown
                </div>
                <div style="font-family: 'Roboto', 'Nunito', sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 20px; letter-spacing: 0.25px; color: var(--color-on-background-medium)">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                </div>
              </div>
              <div style="display: flex; gap: 8px;">
                <ox-button
                    v-for="action in actions"
                    :key="action.eventName"
                    button-style="tertiary"
                    :disabled="action.disabled"
                    :icon="action.icon"
                    :label="action.label"
                    v-on:click="dispatch(action.eventName)"
                >
                  {{ action.label }}
                </ox-button>
              </div>
            </div>
          </template>
          </ox-dropdown-button>
        `
    })
}
