import type { Meta, StoryObj } from "@storybook/vue"
import OxTooltip from "@oxify/components/OxTooltip/OxTooltip.vue"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"
import OxTextField from "@oxify/components/OxField/OxTextField/OxTextField.vue"
import OxCheckbox from "@oxify/components/OxField/OxCheckbox/OxCheckbox.vue"

const meta = {
    title: "Core/Oxify/Tooltip",
    component: OxTooltip,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxTooltip, OxIcon },
        template: `
          <div style="display: flex; justify-content: center;">
              <ox-tooltip 
                  :delay="$props.delay" 
                  :disabled="$props.disabled" 
                  :position="$props.position"
              >
                <template v-slot:content="{ on }">
                  <div v-on="on">
                    <ox-icon icon="eye"/>
                  </div>
                </template>
                {{ $props.default }}
              </ox-tooltip>
          </div>
        `
    }),
    argTypes: {
        position: {
            name: "position",
            description: "Where the message is displayed (relative to the target element).",
            table: {
                category: "Props",
                type: {
                    summary: '"bottom" | "right" | "top" | "left"'
                }
            },
            control: {
                type: "select",
                options: ["bottom", "right", "top", "left"]
            }
        },
        delay: {
            name: "delay",
            description: "Delay (in ms) to display the message when the target element is hovered.",
            table: {
                category: "Props",
                type: {
                    summary: "number"
                },
                defaultValue: {
                    summary: 250
                }
            },
            control: {
                type: "number"
            }
        },
        disabled: {
            name: "disabled",
            description: "Disable the tooltip.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        },
        // @ts-ignore
        content: {
            name: "content",
            description: "Target element triggering the message display.",
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        default: {
            name: "default",
            description: "Message to display on the target hover.",
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            },
            control: {
                type: "text"
            }
        }
    }
} satisfies Meta<typeof OxTooltip>

export default meta
type Story = StoryObj<typeof OxTooltip>

export const Default: Story = {
    args: {
        // @ts-ignore
        default: "Tooltip message"
    }
}

export const MessageTarget: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-tooltip position="right" >
  <template v-slot:content="{ on }">
    <div v-on="on">
      <ox-vicon icon="eye"/>
    </div>
  </template>
  <div>The message !</div>
</ox-tooltip>
                        `
            }
        }
    },
    args: {
        // @ts-ignore
        default: "The message !",
        position: "right"
    }
}

export const Targets: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-tooltip position="bottom">
  <template v-slot:content="{ on }">
    <div v-on="on">
      <ox-button label="Button"/>
    </div>
  </template>
  <div>Button tooltip !</div>
</ox-tooltip>
<ox-tooltip position="bottom">
  <template v-slot:content="{ on }">
    <div v-on="on">
      <ox-text-field label="Input"/>
    </div>
  </template>
  <div>Textfield tooltip !</div>
</ox-tooltip>
<ox-tooltip position="bottom">
  <template v-slot:content="{ on }">
    <div v-on="on">
      <ox-checkbox label="Checkbox"/>
    </div>
  </template>
  <div>Checkbox tooltip !</div>
</ox-tooltip>
                        `
            }
        }
    },
    render: () => ({
        components: { OxTooltip, OxButton, OxTextField, OxCheckbox },
        template: `
          <div style="display: flex; justify-content: center; gap: 64px;">
              <ox-tooltip position="bottom">
                <template v-slot:content="{ on }">
                  <div v-on="on">
                    <ox-button label="Button"/>
                  </div>
                </template>
                <div>Button tooltip !</div>
              </ox-tooltip>
              <ox-tooltip position="bottom">
                <template v-slot:content="{ on }">
                  <div v-on="on">
                    <ox-text-field label="Input"/>
                  </div>
                </template>
                <div>Textfield tooltip !</div>
              </ox-tooltip>
              <ox-tooltip position="bottom">
                <template v-slot:content="{ on }">
                  <div v-on="on">
                    <ox-checkbox label="Checkbox"/>
                  </div>
                </template>
                <div>Checkbox tooltip !</div>
              </ox-tooltip>
          </div>
        `
    })
}
