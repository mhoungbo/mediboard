import type { Meta, StoryObj } from "@storybook/vue"
import OxExpansionPanel from "@oxify/components/OxExpansionPanel/OxExpansionPanel.vue"
import OxExpansionPanels from "@oxify/components/OxExpansionPanels/OxExpansionPanels.vue"

const meta = {
    title: "Core/Oxify/Expansion Panel",
    component: OxExpansionPanel,
    data () {
        return {
            panel: []
        }
    },
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxExpansionPanel, OxExpansionPanels },
        template: `
          <div>
              <ox-expansion-panels :v-model="$data.panel">
                <ox-expansion-panel
                    :title="$props.title"
                    :description="$props.description"
                    :readonly="$props.readonly"
                    :sticky="$props.sticky"
                    :top="$props.top"
                    :show-sides="$props.showSides"
                >
                  <template v-slot:content>{{ $props.content }}</template>
                </ox-expansion-panel>
              </ox-expansion-panels>
          </div>
        `
    }),
    argTypes: {
        title: {
            name: "title",
            description: "Title of the expansion panel.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "text"
            }
        },
        description: {
            name: "description",
            description: "Description of the expansion panel.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "text"
            }
        },
        readonly: {
            name: "readonly",
            description: "Makes the expansion panel content read only.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: false
                }
            },
            control: {
                type: "boolean"
            }
        },
        sticky: {
            name: "sticky",
            description: "Makes the expansion panel's header sticky.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: false
                }
            },
            control: {
                type: "boolean"
            }
        },
        top: {
            name: "top",
            description: "The top position for expansion panel's header if it's sticky.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: "0"
                }
            },
            control: {
                type: "text"
            }
        },
        showSides: {
            name: "show-sides",
            description: "Defines which sides slot to display.",
            table: {
                category: "Props",
                type: {
                    summary: '"left" | "right" | "both" | "none"'
                },
                defaultValue: {
                    summary: "right"
                }
            },
            control: "select",
            options: ["left", "right", "both", "none"]
        },
        // @ts-ignore
        left: {
            name: "left",
            description: 'Left part of the expansion panel. Only appears if "left" or "both" values are set to "show-sides" prop.',
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            },
            control: {
                type: "text"
            }
        },
        header: {
            name: "header",
            description: "Header content part of the expansion panel. By default it displays the title and the description.",
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            },
            control: {
                type: "text"
            }
        },
        right: {
            name: "right",
            description: 'Right part of the expansion panel. Only appears if "right" or "both" values are set to "show-sides" prop.',
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            },
            control: {
                type: "text"
            }
        },
        content: {
            name: "content",
            description: "Content part of the expansion panel.",
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            },
            control: {
                type: "text"
            }
        }
    }
} satisfies Meta<typeof OxExpansionPanel>

export default meta
type Story = StoryObj<typeof OxExpansionPanel>

export const Default: Story = {
    args: {
        title: "Title",
        description: "Description",
        // @ts-ignore
        content: "Slot content"
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            panels: [],
        }
    }
}

<ox-expansion-panels :v-model="panels">
  <ox-expansion-panel
    description="Description"
    title="Title"
  >
    <template v-slot:content>Content</template>
  </ox-expansion-panel>
</ox-expansion-panels>
                `
            }
        }
    },
    args: {
        title: "Title",
        description: "Description",
        // @ts-ignore
        content: "Slot content"
    }
}
