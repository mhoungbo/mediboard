import { Meta, StoryObj } from "@storybook/vue"
import OxTabs from "@oxify/components/OxTabs/OxTabs.vue"
import OxTab from "@oxify/components/OxTab/OxTab.vue"
import OxTabContent from "@oxify/components/OxTabContent/OxTabContent.vue"

const meta = {
    title: "Core/Oxify/Tabs",
    component: OxTabs,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxTabs, OxTab, OxTabContent },
        data () {
            return {
                tab: null
            }
        },
        template: `
          <ox-tabs v-bind="$props" v-model="tab">
              <ox-tab title="Lorem" />
              <ox-tab title="ipsum" />
              <ox-tab title="dolor" />
              <ox-tab title="sit" />
              <ox-tab title="amet" />
    
              <template #contents>
                <ox-tab-content>Lorem</ox-tab-content>
                <ox-tab-content>ipsum</ox-tab-content>
                <ox-tab-content>dolor</ox-tab-content>
                <ox-tab-content>sit</ox-tab-content>
                <ox-tab-content>amet</ox-tab-content>
              </template>
          </ox-tabs>
        `
    })
} satisfies Meta<typeof OxTabs>

export default meta
type Story = StoryObj<typeof OxTabs>

export const Default: Story = {
    args: {
        value: null
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            tab: null
        }
    }
})

<ox-tabs v-model="tab">
  <ox-tab title="Lorem" />
  <ox-tab title="ipsum" />
  <ox-tab title="dolor" />
  <ox-tab title="sit" />
  <ox-tab title="amet" />

  <template #contents>
    <ox-tab-content>Lorem</ox-tab-content>
    <ox-tab-content>ipsum</ox-tab-content>
    <ox-tab-content>dolor</ox-tab-content>
    <ox-tab-content>sit</ox-tab-content>
    <ox-tab-content>amet</ox-tab-content>
  </template>
</ox-tabs>
                `
            }
        }
    },
    args: {
        value: null
    }
}

export const CenterActive: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            tab: null,
            tabs: [
                { title: "Onglet 1" },
                { title: "Onglet 2" },
                { title: "Onglet 3" },
                { title: "Onglet 4" },
                { title: "Onglet 5" },
                { title: "Onglet 6" },
                { title: "Onglet 7" },
                { title: "Onglet 8" },
                { title: "Onglet 9" },
                { title: "Onglet 10" },
                { title: "Onglet 11" },
                { title: "Onglet 12" },
                { title: "Onglet 13" },
                { title: "Onglet 14" },
                { title: "Onglet 15" }
            ]
        }
    }
})

<ox-tabs v-model="tab" center-active show-arrows>
  <ox-tab v-for="(t, i) in tabs" :key="i" :title="t.title" />

  <template #contents>
    <ox-tab-content v-for="(t, i) in tabs" :key="i">{{ t.title }}</ox-tab-content>
  </template>
</ox-tabs>
                `
            }
        }
    },
    render: () => ({
        components: { OxTabs, OxTab, OxTabContent },
        data () {
            return {
                tab: null,
                tabs: [
                    { title: "Onglet 1" },
                    { title: "Onglet 2" },
                    { title: "Onglet 3" },
                    { title: "Onglet 4" },
                    { title: "Onglet 5" },
                    { title: "Onglet 6" },
                    { title: "Onglet 7" },
                    { title: "Onglet 8" },
                    { title: "Onglet 9" },
                    { title: "Onglet 10" },
                    { title: "Onglet 11" },
                    { title: "Onglet 12" },
                    { title: "Onglet 13" },
                    { title: "Onglet 14" },
                    { title: "Onglet 15" }
                ]
            }
        },
        template: `
          <ox-tabs v-model="tab" center-active show-arrows>
              <ox-tab v-for="(t, i) in tabs" :key="i" :title="t.title" />
    
              <template #contents>
                <ox-tab-content v-for="(t, i) in tabs" :key="i">{{ t.title }}</ox-tab-content>
              </template>
          </ox-tabs>
        `
    })
}

export const Vertical: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            tab: null
        }
    }
})

<ox-tabs v-model="tab" vertical>
  <ox-tab title="Lorem" />
  <ox-tab title="ipsum" />
  <ox-tab title="dolor" />
  <ox-tab title="sit" />
  <ox-tab title="amet" />

  <template #contents>
    <ox-tab-content>Lorem</ox-tab-content>
    <ox-tab-content>ipsum</ox-tab-content>
    <ox-tab-content>dolor</ox-tab-content>
    <ox-tab-content>sit</ox-tab-content>
    <ox-tab-content>amet</ox-tab-content>
  </template>
</ox-tabs>
                `
            }
        }
    },
    args: {
        value: null,
        vertical: true
    }
}
