import type { Meta, StoryObj } from "@storybook/vue"
import OxDivider from "@oxify/components/OxDivider/OxDivider.vue"

const meta = {
    title: "Core/Oxify/Divider",
    component: OxDivider,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDivider },
        template: `
          <ox-divider
            :color="$props.color"
            :insetAxial="$props.insetAxial"
            :insetRadial="$props.insetRadial"
            :vertical="$props.vertical"
          />
        `
    }),
    argTypes: {
        color: {
            name: "color",
            description: "Divider color.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "dark"
                },
                type: {
                    summary: "string",
                    detail: ' "dark" | "light" '
                }
            },
            control: {
                type: "radio",
                options: ["dark", "light"]
            }
        },
        insetAxial: {
            name: "inset-axial",
            description: "Adds appropriate axial margin. Also reduces max width for normal dividers, or reduces max height for vertical. The value defines the margin size in pixel.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: 0
                },
                type: {
                    summary: "number"
                }
            },
            control: {
                type: "number"
            }
        },
        insetRadial: {
            name: "inset-radial",
            description: "Adds top/bottom margin for normal dividers, or left/right margin for vertical. The value defines the margin size in pixel.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: 0
                },
                type: {
                    summary: "number"
                }
            },
            control: {
                type: "number"
            }
        },
        vertical: {
            name: "vertical",
            description: "Displays divider vertically",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "false"
                },
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        }
    }
} satisfies Meta<typeof OxDivider>

export default meta
type Story = StoryObj<typeof OxDivider>

export const Default: Story = {}

export const Color: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <div style="background-color: #eee; border-radius: 4px; margin-bottom: 10px; padding: 20px 0;">
    <ox-divider color="dark" />
  </div>
  <div style="background-color: rgba(0, 0, 0, 0.9); border-radius: 4px; padding: 20px 0;">
    <ox-divider color="light" />
  </div>
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxDivider },
        template: `
          <div>
            <div style="background-color: #eee; border-radius: 4px; margin-bottom: 10px; padding: 20px 0;">
              <ox-divider color="dark" />
            </div>
            <div style="background-color: rgba(0, 0, 0, 0.9); border-radius: 4px; padding: 20px 0;">
              <ox-divider color="light" />
            </div>
          </div>
        `
    })
}

export const AxialInset: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <div style="background-color: #eee; border-radius: 4px; margin-bottom: 10px; padding: 20px 0;">
    <ox-divider 
      :insetAxial="50"
      color="dark"
    />
  </div>
  <div style="background-color: #eee; border-radius: 4px; height: 300px; padding: 5px 20px; display: flex; justify-content: center;">
    <ox-divider
      :insetAxial="50" 
      color="dark"
      :vertical="true"
    />
  </div>
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxDivider },
        template: `
          <div>
          <div style="background-color: #eee; border-radius: 4px; margin-bottom: 10px; padding: 20px 0;">
            <ox-divider
                :insetAxial="50"
                color="dark"
            />
          </div>
          <div style="background-color: #eee; border-radius: 4px; height: 300px; padding: 5px 20px; display: flex; justify-content: center;">
            <ox-divider
                :insetAxial="50"
                color="dark"
                :vertical="true"
            />
          </div>
          </div>
        `
    })
}

export const RadialInset: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <div style="background-color: #eee; border-radius: 4px; display: flex; flex-direction: column; height: 200px; margin-bottom: 30px; padding: 5px;">
    <div style="background-color: #333; height: 100%;" />
    <ox-divider 
      color="dark" 
      :insetRadial="10" 
    />
    <div style="background-color: #333; height: 100%;" />
  </div>
  <div style="background-color: #eee; border-radius: 4px; display: flex; flex-direction: row; height: 200px; padding: 5px;">
    <div style="background-color: #333; width: 100%;" />
    <ox-divider 
      color="dark"
      :insetRadial="10"
      :vertical="true"
    />
    <div style="background-color: #333; width: 100%;" />
  </div>
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxDivider },
        template: `
          <div>
              <div style="background-color: #eee; border-radius: 4px; display: flex; flex-direction: column; height: 200px; margin-bottom: 30px; padding: 5px;">
                <div style="background-color: #333; height: 100%;" />
                <ox-divider 
                  color="dark" 
                  :insetRadial="10" 
                />
                <div style="background-color: #333; height: 100%;" />
              </div>
              <div style="background-color: #eee; border-radius: 4px; display: flex; flex-direction: row; height: 200px; padding: 5px;">
                <div style="background-color: #333; width: 100%;" />
                <ox-divider 
                  color="dark"
                  :insetRadial="10"
                  :vertical="true"
                />
                <div style="background-color: #333; width: 100%;" />
              </div>
          </div>
        `
    })
}

export const AxialRadialInset: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <div style="background-color: #eee; border-radius: 4px; display: flex; flex-direction: column; height: 200px; margin-bottom: 30px; padding: 5px;">
    <div style="background-color: #333; height: 100%;" />
    <ox-divider 
      color="dark" 
      :insetAxial="50" 
      :insetRadial="10" 
    />
    <div style="background-color: #333; height: 100%;" />
  </div>
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxDivider },
        template: `
          <div>
            <div style="background-color: #eee; border-radius: 4px; display: flex; flex-direction: column; height: 200px; margin-bottom: 30px; padding: 5px;">
              <div style="background-color: #333; height: 100%;" />
              <ox-divider 
                color="dark" 
                :insetAxial="50" 
                :insetRadial="10" 
              />
              <div style="background-color: #333; height: 100%;" />
            </div>
          </div>
        `
    })
}
