import { Meta, StoryObj } from "@storybook/vue"
import OxLoading from "@oxify/components/OxLoading/OxLoading.vue"

const meta = {
    title: "Core/Oxify/Loading",
    component: OxLoading,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxLoading },
        template: `
          <ox-loading v-bind="$props"/>
        `
    }),
    argTypes: {
        color: {
            name: "color",
            description: "Defines the color",
            control: {
                type: "text"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: "secondary"
                },
                type: {
                    summary: "string"
                }
            }
        },
        height: {
            name: "height",
            description: "For the linear loader set the height in px",
            control: {
                type: "text"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: 4
                },
                type: {
                    summary: "string | number"
                }
            }
        },
        width: {
            name: "width",
            description: "For the circular loader set the stroke width in px",
            control: {
                type: "text"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: 2
                },
                type: {
                    summary: "string | number"
                }
            }
        },
        size: {
            name: "size",
            description: "Defines the size of the circular loader",
            control: {
                type: "text"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: 32
                },
                type: {
                    summary: "string | number"
                }
            }
        },
        circular: {
            name: "circular",
            description: "Choose between linear loader and circular loader",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        }
    }
} satisfies Meta<typeof OxLoading>

export default meta
type Story = StoryObj<typeof OxLoading>

export const Default: Story = {}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: "<ox-loading />"
            }
        }
    }
}
