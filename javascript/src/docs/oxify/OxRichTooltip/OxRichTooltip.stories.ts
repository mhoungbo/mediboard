import type { Meta, StoryObj } from "@storybook/vue"
import OxRichTooltip from "@oxify/components/OxRichTooltip/OxRichTooltip.vue"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"

const meta = {
    title: "Core/Oxify/Rich Tooltip",
    component: OxRichTooltip,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxRichTooltip, OxIcon },
        template: `
          <div style="display: flex; justify-content: center;">
            <ox-rich-tooltip
                :delay-after-leave="$props.delayAfterLeave"
                :delay-before-call="$props.delayBeforeCall"
                :delay-before-show="$props.delayBeforeShow"
                :force-close="$props.forceClose"
                :max-height="$props.maxHeight"
                :position="$props.position"
                :tooltip-class="$props.tooltipClass"
                :width="$props.width"
            >
              <template v-slot:activator>
                <div style="display: inline-block; width: auto">
                  <ox-icon icon="eye"/>
                </div>
              </template>
              <template v-if="$props.tooltip" v-slot:tooltip>{{ $props.tooltip }}</template>
            </ox-rich-tooltip>
          </div>
        `
    }),
    argTypes: {
        delayAfterLeave: {
            name: "delay-after-leave",
            description: "Delay (in ms) before hide the tooltip after leaving the target element.",
            table: {
                category: "Props",
                type: {
                    summary: "number"
                },
                defaultValue: {
                    summary: 200
                }
            },
            control: {
                type: "number"
            }
        },
        delayBeforeCall: {
            name: "delay-before-call",
            description: 'Delay (in ms) to emit "beforeCall" event when the target element is hovered.',
            table: {
                category: "Props",
                type: {
                    summary: "number"
                },
                defaultValue: {
                    summary: 300
                }
            },
            control: {
                type: "number"
            }
        },
        delayBeforeShow: {
            name: "delay-before-show",
            description: "Delay (in ms) to display the message when the target element is hovered.",
            table: {
                category: "Props",
                type: {
                    summary: "number"
                },
                defaultValue: {
                    summary: 500
                }
            },
            control: {
                type: "number"
            }
        },
        forceClose: {
            name: "force-close",
            description: "Prevents the tooltip to open or forces her to close if opened.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        maxHeight: {
            name: "max-height",
            description: "Max height.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "",
                    detail: "Required"
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        width: {
            name: "width",
            description: "Tooltip width.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "",
                    detail: "Required"
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        tooltipClass: {
            name: "tooltip-class",
            description: "Tooltip class.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: undefined
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        position: {
            name: "position",
            description: "Where the message is displayed (relative to the target element).",
            table: {
                category: "Props",
                type: {
                    summary: '"bottom" | "right" | "top" | "left"'
                },
                defaultValue: {
                    summary: "bottom"
                }
            },
            control: {
                type: "select",
                options: ["bottom", "right", "top", "left"]
            }
        },
        // @ts-ignore
        activator: {
            name: "activator",
            description: "Target element triggering the message display.",
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            },
            control: false
        },
        tooltip: {
            name: "tooltip",
            description: "Message to display on the target hover.",
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            },
            control: {
                type: "text"
            }
        },
        beforeDisplay: {
            action: "beforeDisplay",
            description: "Emit `beforeDisplay` event when the delayBeforeCall is finished after activator.",
            table: {
                category: "Events"
            },
            control: false
        },
        show: {
            action: "show",
            description: "Emit `show` event when the delayBeforeShow is finished after activator.",
            table: {
                category: "Events"
            },
            control: false
        },
        click: {
            action: "click",
            description: "Emit `click` event when the activator is clicked.",
            table: {
                category: "Events"
            },
            control: false
        }
    }
} satisfies Meta<typeof OxRichTooltip>

export default meta
type Story = StoryObj<typeof OxRichTooltip>

export const Default: Story = {
    args: {
        // @ts-ignore
        tooltip: "Tooltip message",
        width: "300",
        maxHeight: "150"
    }
}

export const Tooltip: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            slotContent: ""
        }
    }
    computed: {
        hasData (): boolean {
            return this.slotContent !== ""
        }
    }
    methods: {
        eventAction (): void {
            if (!this.hasData) {
                this.slotContent = "The message !"
            }
        }
    }
}

<ox-rich-tooltip 
  v-on:beforeDisplay="eventAction" 
  width="300" 
  max-height="150"
>
  <template #activator>
    <ox-icon icon="eye" />
  </template>
  <template
    v-if="hasData"
    #tooltip
  >
    {{ slotContent }}
  </template>
</ox-rich-tooltip>
                `
            }
        }
    },
    args: {
        // @ts-ignore
        tooltip: "The message !",
        width: "300",
        maxHeight: "150"
    }
}
