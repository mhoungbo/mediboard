import type { Meta, StoryObj } from "@storybook/vue"
import OxExpansionPanel from "@oxify/components/OxExpansionPanel/OxExpansionPanel.vue"
import OxExpansionPanels from "@oxify/components/OxExpansionPanels/OxExpansionPanels.vue"

const meta = {
    title: "Core/Oxify/Expansion Panels",
    component: OxExpansionPanels,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        data () {
            return {
                content: "Slot content"
            }
        },
        components: { OxExpansionPanel, OxExpansionPanels },
        template: `
          <div>
              <ox-expansion-panels
                  v-model="$props.value"
                  :accordion="$props.accordion"
                  :focusable="$props.focusable"
                  :multiple="$props.multiple"
              >
                {{ $props.default }}
                <ox-expansion-panel
                    description="Description 1"
                    title="Title 1"
                >
                  <template #content>{{ content }}</template>
                </ox-expansion-panel>
                <ox-expansion-panel
                    description="Description 2"
                    title="Title 2"
                >
                  <template #content>{{ content }}</template>
                </ox-expansion-panel>
                <ox-expansion-panel
                    description="Description 3"
                    title="Title 3"
                >
                  <template #content>{{ content }}</template>
                </ox-expansion-panel>
              </ox-expansion-panels>
          </div>
        `
    }),
    argTypes: {
        accordion: {
            name: "accordion",
            description: "Removes the margin around open panels.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: true
                }
            },
            control: {
                type: "boolean"
            }
        },
        focusable: {
            name: "focusable",
            description: "Makes the expansion panel headers focusable.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: false
                }
            },
            control: {
                type: "boolean"
            }
        },
        multiple: {
            name: "multiple",
            description: "Allows multiple selections. Requires the value prop to be an array.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: false
                }
            },
            control: {
                type: "boolean"
            }
        },
        transparent: {
            name: "transparent",
            description: "Removes the expansion panel's background.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: false
                }
            },
            control: {
                type: "boolean"
            }
        },
        value: {
            name: "value",
            description: "The opened/closed state of content in the expansion panel. If the multiple prop is used, then it has to be an array of numbers where each entry corresponds to the index of the opened content.",
            table: {
                category: "Props",
                type: {
                    summary: "number | number[]"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "text"
            }
        },
        // @ts-ignore
        input: {
            action: "input",
            description: "Event binding for v-model",
            table: {
                category: "Events"
            },
            control: false
        },
        default: {
            name: "default",
            description: "Expansion panels content. Contains all OxExpansionPanel childs.",
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxExpansionPanels>

export default meta
type Story = StoryObj<typeof OxExpansionPanels>

export const Default: Story = {}
export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: ` 
export default {
    name: "MyComponent",
    data () {
        return {
            panels: [1],
        }
    }
}

<ox-expansion-panels
  v-model="panels"
  multiple
>
  <ox-expansion-panel
    description="Description 1"
    title="Title 1"
  >
    <template #content>Slot content</template>
  </ox-expansion-panel>
  <ox-expansion-panel
    description="Description 2"
    title="Title 2"
  >
    <template #content>Slot content</template>
  </ox-expansion-panel>
  <ox-expansion-panel
    description="Description 3"
    title="Title 3"
  >
    <template #content>Slot content</template>
  </ox-expansion-panel>
</ox-expansion-panels>
                `
            }
        }
    },
    args: {
        multiple: true,
        value: [1]
    }
}
