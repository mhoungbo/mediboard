import type { Meta, StoryObj } from "@storybook/vue"
import OxBeautify from "@oxify/components/OxBeautify/OxBeautify.vue"

const meta = {
    title: "Core/Oxify/Beautify",
    component: OxBeautify,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxBeautify },
        template: `
          <ox-beautify 
              :value="$props.value" 
              :void-label="$props.voidLabel" 
              :inline="$props.inline" 
          />
        `
    }),
    argTypes: {
        value: {
            name: "value",
            description: "The value to display.",
            table: {
                category: "Props",
                type: {
                    summary: "any",
                    detail: "Can be string (eventually formatted in date or time), boolean or array"
                }
            },
            control: "select",
            options: ["", "Test", "2020-01-01", "10:15:00", true, false, 77],
            labels: {
                Test: '(String) => "Test"',
                "2020-01-01": '(Date) => "2020-01-01"',
                "10:15:00": '(Time) => "10:15:00"',
                true: "(Boolean) => true",
                false: "(Boolean) => false",
                77: "(Number) => 77"
            }
        },
        voidLabel: {
            name: "void-label",
            description: "label to display when `value` is void.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "-"
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        inline: {
            title: "inline",
            description: "",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        }
    }
} satisfies Meta<typeof OxBeautify>

export default meta
type Story = StoryObj<typeof OxBeautify>

export const Default: Story = {}

export const String: Story = {
    parameters: {
        docs: {
            source: {
                code: "<ox-beautify value='Foo, bar' />"
            }
        }
    },
    args: {
        value: "Foo, bar"
    }
}

export const Boolean: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <ox-beautify :value=true />
  <ox-beautify :value=false />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxBeautify },
        template: `
          <div>
            <ox-beautify :value=true />
            <ox-beautify :value=false />
          </div>
        `
    })
}

export const Number: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <ox-beautify :value=77 />
  <ox-beautify :value=667.22 />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxBeautify },
        template: `
          <div>
            <ox-beautify :value=77 />
            <ox-beautify :value=667.22 />
          </div>
        `
    })
}

export const Date: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <ox-beautify value="2021-05-07" />
  <ox-beautify value="2021-05-07 10:15:00" />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxBeautify },
        template: `
          <div>
            <ox-beautify value="2021-05-07" />
            <ox-beautify value="2021-05-07 10:15:00" />
          </div>
        `
    })
}

export const Time: Story = {
    parameters: {
        docs: {
            source: {
                code: "<ox-beautify value='10:15:00' />"
            }
        }
    },
    args: {
        value: "10:15:00"
    }
}

export const Array: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <ox-beautify :value="['foo', 'bar', 12]" />
  <ox-beautify :value="[]" />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxBeautify },
        template: `
          <div>
            <ox-beautify :value="['foo', 'bar', 12]" />
            <ox-beautify :value="[]" />
          </div>
        `
    })
}

export const Void: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<div>
  <ox-beautify value="" void-label="foo" />
  <ox-beautify void-label="bar"/>
  <ox-beautify />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxBeautify },
        template: `
          <div>
            <ox-beautify value="" void-label="foo" />
            <ox-beautify void-label="bar"/>
            <ox-beautify />
          </div>
        `
    })
}
