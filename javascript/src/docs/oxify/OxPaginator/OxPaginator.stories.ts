import type { Meta, StoryObj } from "@storybook/vue"
import OxPaginator from "@oxify/components/OxPaginator/OxPaginator.vue"

const meta = {
    title: "Core/Oxify/Paginator",
    component: OxPaginator,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxPaginator },
        template: `
          <ox-paginator
              :value="$props.value"
              :total="$props.total"
              :perPage="$props.perPage"
              :total-visible="$props.totalVisible"
              :disabled="$props.disabled"
          />
        `
    }),
    argTypes: {
        total: {
            name: "total",
            description: "Sets the total number of paginated items.",
            control: {
                type: "number"
            },
            table: {
                category: "Props",
                type: {
                    summary: "Number",
                    detail: "Required"
                }
            }
        },
        perPage: {
            name: "per-page",
            description: "Defines the number of items displayed per page.",
            control: {
                type: "range",
                min: 1,
                max: 100,
                step: 1
            },
            table: {
                category: "Props",
                type: {
                    summary: "Number",
                    detail: "Required"
                }
            }
        },
        totalVisible: {
            name: "total-visible",
            description: "Specifies how many pagination numbers are visible at max.",
            control: {
                type: "number"
            },
            table: {
                category: "Props",
                type: {
                    summary: "Number"
                }
            }
        },
        disabled: {
            name: "disabled",
            description: "Disables component.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        value: {
            name: "value",
            description: "The designated model value for the component.",
            control: {
                type: "number"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: 1
                },
                type: {
                    summary: "number",
                    detail: "The current page number"
                }
            }
        },
        // @ts-ignore
        input: {
            action: "input",
            description: "Event binding for v-model.",
            table: {
                category: "Events"
            },
            control: false
        }
    }
} satisfies Meta<typeof OxPaginator>

export default meta
type Story = StoryObj<typeof OxPaginator>

export const Default: Story = {
    args: {
        total: 122,
        perPage: 20
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            totalItems: 77,
            perPage: 6,
            currentPage: 1
        }
    },
    computed: {
        currentPagination () {
            const items = [] as Array<number>
            for (let i = 1; i <= this.perPage; i++) {
                const itemNumber = i + ((this.currentPage - 1) * this.perPage)
                if (itemNumber <= this.totalItems) {
                    items.push(itemNumber)
                }
            }
            return items
        }
    }
}

<div style="display: flex; flex-direction: column; align-items: flex-start;">
  <div style="display: flex; gap: 8px; margin-bottom: 20px;">
    <div
        v-for="n in currentPagination"
        :key="n"
        style="padding: 8px; border-radius: 4px; border: 1px solid lightgrey; width: 100px; height: 100px;"
    >
      Card {{n}}
    </div>
  </div>
  <ox-paginator
      :value="currentPage"
      :total="totalItems"
      :perPage="perPage"
      :total-visible="7"
      v-on:input="(value) => currentPage = value"
  />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxPaginator },
        data () {
            return {
                totalItems: 77,
                perPage: 6,
                currentPage: 1
            }
        },
        computed: {
            currentPagination () {
                const items = [] as Array<number>
                // @ts-ignore
                for (let i = 1; i <= this.perPage; i++) {
                    // @ts-ignore
                    const itemNumber = i + ((this.currentPage - 1) * this.perPage)
                    // @ts-ignore
                    if (itemNumber <= this.totalItems) {
                        items.push(itemNumber)
                    }
                }
                return items
            }
        },
        template: `
          <div style="display: flex; flex-direction: column; align-items: flex-start;">
              <div style="display: flex; gap: 8px; margin-bottom: 20px;">
                <div
                    v-for="n in currentPagination"
                    :key="n"
                    style="padding: 8px; border-radius: 4px; border: 1px solid lightgrey; width: 100px; height: 100px;"
                >
                  Card {{n}}
                </div>
              </div>
              <ox-paginator
                  :value="currentPage"
                  :total="totalItems"
                  :perPage="perPage"
                  :total-visible="7"
                  @input="(value) => currentPage = value"
              />
          </div>
        `
    })
}
