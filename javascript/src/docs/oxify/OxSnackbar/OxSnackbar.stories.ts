import type { Meta, StoryObj } from "@storybook/vue"
import OxSnackbar from "@oxify/components/OxSnackbar/OxSnackbar.vue"

const meta = {
    title: "Core/Oxify/Snackbar",
    component: OxSnackbar,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxSnackbar },
        template: `
          <ox-snackbar
            :closable="$props.closable"
            :error="$props.error"
            :multi-line="$props.multiLine"
            :text="$props.text"
            :timeout="$props.timeout"
            :value="$props.value"
          />
        `
    }),
    argTypes: {
        closable: {
            name: "closable",
            description: "Displays the button to manually close the snackbar.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: true
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        text: {
            name: "text",
            description: "Snackbar's content text.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        error: {
            name: "error",
            description: 'Defines the snackbar as "error" type. It changes the background color and removes the timeout.',
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        timeout: {
            name: "timeout",
            description: "Snackbar's display duration.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: 5000
                },
                type: {
                    summary: "number"
                }
            },
            control: {
                type: "number"
            }
        },
        // @ts-ignore
        "click:close": {
            action: "click:close",
            description: "Emit `click:close` event when close icon is clicked",
            table: {
                category: "Events"
            },
            control: false
        }
    }
} satisfies Meta<typeof OxSnackbar>

export default meta
type Story = StoryObj<typeof OxSnackbar>

export const Default: Story = {
    args: {
        text: "The content inside the snackbar"
    }
}

export const Error: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-snackbar
  error
  text="Snackbar content"
/>
                `
            }
        }
    },
    args: {
        text: "Snackbar content",
        error: true
    }
}

export const Closable: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-snackbar
  :closable="false"
  text="Snackbar content"
  :timeout="50000"
/>
                `
            }
        }
    },
    args: {
        text: "Snackbar content",
        closable: false,
        timeout: 50000
    }
}
