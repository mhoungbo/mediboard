import type { Meta, StoryObj } from "@storybook/vue"
import { VList, VListItem, VListItemContent, VListItemGroup, VListItemIcon, VListItemTitle } from "vuetify/lib/components"

import OxContextMenu from "@oxify/components/OxContextMenu/OxContextMenu.vue"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"

const meta = {
    title: "Core/Oxify/ContextMenu",
    component: OxContextMenu,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxContextMenu, OxIcon, VList, VListItem, VListItemContent, VListItemGroup, VListItemIcon, VListItemTitle },
        template: `
          <div>
              <ox-context-menu>
                  <template #activator="{ on }">
                    <div
                        style="height: 30px; background-color: var(--primary-500); color: var(--color-on-primary-high); padding: 5px;"
                        v-on="on"
                    >
                      Right click on me
                    </div>
                  </template>
                  <template #menu>
                    <v-list>
                      <v-list-item-group>
                        <v-list-item>
                          <v-list-item-icon><ox-icon icon="edit"/></v-list-item-icon>
                          <v-list-item-content><v-list-item-title>Edit</v-list-item-title></v-list-item-content>
                        </v-list-item>
                        <v-list-item>
                          <v-list-item-icon><ox-icon icon="delete"/></v-list-item-icon>
                          <v-list-item-content><v-list-item-title>Delete</v-list-item-title></v-list-item-content>
                        </v-list-item>
                      </v-list-item-group>
                    </v-list>
                  </template>
              </ox-context-menu>
          </div>
        `
    }),
    argTypes: {
        menu: {
            name: "menu",
            description: "Context menu content",
            control: false,
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        activator: {
            name: "activator",
            description: "Context menu activator (to enable the menu).",
            control: false,
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxContextMenu>

export default meta
type Story = StoryObj<typeof OxContextMenu>

export const Default: Story = {
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-context-menu>
  <template #activator="{ on }">
    <div
        style="height: 30px; background-color: OxThemeCore.primary; color: OxThemeCore.onPrimaryHighEmphasis; padding: 5px;"
        v-on="on"
    >
      Right click on me
    </div>
  </template>
  <template #menu>
    <v-list>
      <v-list-item-group>
        <v-list-item>
          <v-list-item-icon><ox-icon icon="edit"/></v-list-item-icon>
          <v-list-item-content><v-list-item-title>Edit</v-list-item-title></v-list-item-content>
        </v-list-item>
        <v-list-item>
          <v-list-item-icon><ox-icon icon="delete"/></v-list-item-icon>
          <v-list-item-content><v-list-item-title>Delete</v-list-item-title></v-list-item-content>
        </v-list-item>
      </v-list-item-group>
    </v-list>
  </template>
</ox-context-menu>
                `
            }
        }
    }
}
