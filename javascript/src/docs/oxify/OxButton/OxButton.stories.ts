import type { Meta, StoryObj } from "@storybook/vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"

const meta = {
    title: "Core/Oxify/Button",
    component: OxButton,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxButton },
        template: '<ox-button v-bind="$props" />'
    }),
    argTypes: {
        iconButton: { table: { disable: true } },
        smallPrimaryIconOnly: { table: { disable: true } },
        label: {
            name: "label",
            description: "Button's label. Set empty string to create an icon-only button.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        buttonStyle: {
            name: "button-style",
            description: "Main style of button.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "secondary"
                },
                type: {
                    summary: "string",
                    detail: ' "primary" | "secondary" | "tertiary" | "tertiary-dark" '
                }
            },
            control: "radio",
            options: ["primary", "secondary", "tertiary", "tertiary-dark"]
        },
        icon: {
            name: "icon",
            description: "Icon's name associated with the button.",
            control: "select",
            options: ["", "add", "cancel", "check", "refresh", "remove"],
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            }
        },
        title: {
            name: "title",
            description: "Button's title display on hover.",
            control: {
                type: "text"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            }
        },
        customClass: {
            name: "custom-class",
            description: "Additional classes added to the button.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        href: {
            name: "href",
            description: "Designates the component as anchor and applies the href attribute.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        iconSide: {
            name: "icon-side",
            description: "Position of the icon in the button.",
            control: "radio",
            options: ["left", "right"],
            table: {
                category: "Props",
                defaultValue: {
                    summary: "left"
                },
                type: {
                    summary: "string",
                    detail: '"left" | "right"'
                }
            }
        },
        small: {
            name: "small",
            description: "Display the button in small size.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        depressed: {
            name: "depressed",
            description: "Removes the button box shadow.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        block: {
            name: "block",
            description: "Expands the button to 100% of available space.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        disabled: {
            name: "disabled",
            description: "Display the button in an inactive state.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        loading: {
            name: "loading",
            description: "Add a loading effect to the button.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        color: {
            name: "color",
            description: "Sets a custom color on button. Specifies 'currentColor' for css currentColor implementation.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: undefined
                },
                type: {
                    summary: "string"
                }
            }
        },
        // @ts-ignore
        click: {
            action: "click",
            description: "Emit `click` event when clicked",
            table: {
                category: "Events"
            },
            control: false
        },
        default: {
            name: "default",
            description: "Equivalent to label prop.",
            table: {
                category: "Slots",
                defaultValue: {
                    summary: undefined
                },
                type: {
                    summary: "string"
                }
            },
            control: false
        }
    }
} satisfies Meta<typeof OxButton>

export default meta
type Story = StoryObj<typeof OxButton>

export const Default: Story = {
    args: {
        buttonStyle: "primary",
        label: "Button"
    }
}

export const Primary: Story = {
    args: {
        buttonStyle: "primary",
        label: "Main action"
    }
}

export const Secondary: Story = {
    args: {
        buttonStyle: "secondary",
        label: "Secondary action"
    },
    parameters: {
        docs: {
            source: {
                code: `
<ox-button
  button-style="secondary"
  label="Secondary action"
/>
                `
            }
        }
    }
}

export const Tertiary: Story = {
    args: {
        buttonStyle: "tertiary",
        label: "Tertiary action"
    }
}

export const Sizing: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-button
  button-style="primary"
  label="Primary small"
  small
/>
<ox-button
  button-style="secondary"
  icon="account"
  label="Account"
  small
/>
<ox-button
  button-style="tertiary"
  icon="add"
  small
/>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        components: { OxButton },
        template: `
          <div>
              <ox-button button-style="primary" custom-class="mr-2"  label="Primary small" small />
              <ox-button button-style="secondary" icon="account" label="Account" small />
              <ox-button button-style="tertiary" custom-class="mr-2" icon="add" small />
          </div>
        `
    })
}

export const Icons: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-button
  button-style="primary"
  icon="add"
  label="Add object"
/>
<ox-button
  button-style="secondary"
  icon="add"
/>
<ox-button
  button-style="secondary"
  icon="refresh"
  icon-side="right"
  label="Reload"
/>
<ox-button
  button-style="tertiary"
  icon="vDots"
/>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        components: { OxButton },
        template: `
          <div>
              <ox-button button-style="primary" custom-class="mr-2" icon="add" label="Add object" />
              <ox-button button-style="secondary" custom-class="mr-2" icon="add" />
              <ox-button button-style="secondary" custom-class="mr-2" icon="refresh" icon-side="right" label="Reload" />
              <ox-button button-style="tertiary" icon="vDots" />
          </div>
        `
    })
}
