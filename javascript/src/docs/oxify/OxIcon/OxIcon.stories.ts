import type { Meta, StoryObj } from "@storybook/vue"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"
import OxThemeCore from "@/core/oxify/utils/OxThemeCore"

const meta = {
    title: "Core/Oxify/Icon",
    component: OxIcon,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxIcon },
        template: '<ox-icon v-bind="$props" />'
    }),
    argTypes: {
        dark: { table: { disable: true } },
        right: { table: { disable: true } },
        left: { table: { disable: true } },
        icon: {
            name: "icon",
            description: "Icon's name.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        className: {
            name: "class-name",
            description: "Additional classes added to the icon.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        size: {
            name: "size",
            description: "Specifies a custom font size for the icon.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "number | undefined"
                }
            },
            control: {
                type: "number"
            }
        },
        color: {
            name: "color",
            description: "Applies specified color to the control - it can be the name of material color (for example success or purple), a OxThemeCore color (see Color documentation) or css color (#033 or rgba(255, 0, 0, 0.5)).",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "undefined"
                },
                type: {
                    summary: "string | undefined"
                }
            }
        }
    }
} satisfies Meta<typeof OxIcon>

export default meta
type Story = StoryObj<typeof OxIcon>

export const Default: Story = {
    args: {
        icon: "add"
    }
}

export const Colors: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-icon
  :color="OxThemeCore.primary"
  icon="eye"
/>
<ox-icon
  :color="OxThemeCore.secondary"
  icon="calendar"
/>
<ox-icon
  :color="OxThemeCore.errorTextDefault"
  icon="refresh"
/>
<ox-icon
  :color="OxThemeCore.grey300"
  icon="lock"
/>
<ox-icon
  :color="OxThemeCore.onBackgroundHighEmphasis"
  icon="search"
/>
<ox-icon
  :color="OxThemeCore.onBackgroundMediumEmphasis"
  icon="timerSand"
/>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxIcon },
        data () {
            return {
                theme: OxThemeCore
            }
        },
        template: `
          <div>
              <ox-icon class-name="mr-4" :color="theme.primary" icon="eye"/>
              <ox-icon class-name="mr-4" :color="theme.secondary" icon="calendar"/>
              <ox-icon class-name="mr-4" :color="theme.errorTextDefault" icon="refresh"/>
              <ox-icon class-name="mr-4" :color="theme.grey300" icon="lock"/>
              <ox-icon class-name="mr-4" :color="theme.onBackgroundHighEmphasis" icon="search"/>
              <ox-icon :color="theme.onBackgroundMediumEmphasis" icon="timerSand"/>
          </div>
        `
    })
}

export const Sizes: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-icon
  icon="timerSand"
  :size="12"
/>
<ox-icon
  icon="timerSand"
  :size="24"
/>
<ox-icon
  icon="timerSand"
  :size="36"
/>
<ox-icon
  icon="timerSand"
  :size="48"
/>
<ox-icon
  icon="timerSand"
  :size="64"
/>
<ox-icon
  icon="timerSand"
  :size="128"
/>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxIcon },
        template: `
          <div>
              <ox-icon class-name="mr-4" icon="timerSand" :size="12" />
              <ox-icon class-name="mr-4" icon="timerSand" :size="24" />
              <ox-icon class-name="mr-4" icon="timerSand" :size="36" />
              <ox-icon class-name="mr-4" icon="timerSand" :size="48" />
              <ox-icon class-name="mr-4" icon="timerSand" :size="64" />
              <ox-icon class-name="mr-4" icon="timerSand" :size="128" />
          </div>
        `
    })
}
