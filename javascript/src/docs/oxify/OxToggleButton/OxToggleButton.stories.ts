import type { Meta, StoryObj } from "@storybook/vue"
import OxToggleButton from "@oxify/components/OxToggleButton/OxToggleButton.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"
import { VBtn } from "vuetify/lib/components"
import OxRadio from "@oxify/components/OxField/OxRadio/OxRadio.vue"
import OxRadioGroup from "@oxify/components/OxField/OxRadioGroup/OxRadioGroup.vue"

const meta = {
    title: "Core/Oxify/ToggleButton",
    component: OxToggleButton,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxToggleButton, OxButton, VBtn },
        template: `
          <ox-toggle-button
            :borderless="$props.borderless"
            :color="$props.color"
            :group="$props.group"
            :mandatory="$props.mandatory"
            :multiple="$props.multiple"
            :rounded="$props.rounded"
            :tile="$props.tile"
            :value="$props.value"
          >
            <v-btn>
              Label 1
            </v-btn>
            <v-btn>
              Label 2
            </v-btn>
            <v-btn>
              Label 3
            </v-btn>
            <v-btn>
              Label 4
            </v-btn>
          </ox-toggle-button>
        `
    }),
    argTypes: {
        borderless: {
            name: "borderless",
            description: "Removes the group's border.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        color: {
            name: "color",
            description: "The button's color.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "primary"
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        group: {
            name: "group",
            description: "Removes background-color, border and increases space between the buttons.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        mandatory: {
            name: "mandatory",
            description: "A value has to be set in the group. If not, a default value is automatically set.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        multiple: {
            name: "multiple",
            description: "The group can have multiple values.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        rounded: {
            name: "rounded",
            description: "Round edge buttons.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        tile: {
            name: "tile",
            description: "Removes the component border-radius.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        // @ts-ignore
        input: {
            action: "input",
            description: "Event binding for v-model",
            table: {
                category: "Events"
            },
            control: false
        },
        default: {
            name: "default",
            description: "Toggle button content. Contains all VBtn childs.",
            control: {
                type: "text"
            },
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxToggleButton>

export default meta
type Story = StoryObj<typeof OxToggleButton>

export const Default: Story = {}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: ` 
export default {
    name: "MyComponent",
    data () {
        return {
            toggle: "label2",
        }
    }
}

<div>
  <ox-toggle-button
    v-model="toggle"
    color="secondary"
  >
    <v-btn value="label1">
      Label 1
    </v-btn>
    <v-btn value="label2">
      Label 2
    </v-btn>
    <v-btn value="label3">
      Label 3
    </v-btn>
    <v-btn value="label4">
      Label 4
    </v-btn>
  </ox-toggle-button>
  <div>Value = {{ toggle }}</div>
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxToggleButton, OxButton, VBtn },
        data: () => {
            return {
                toggle: "label2"
            }
        },
        template: `
          <div>
            <ox-toggle-button
              v-model="toggle"
              color="secondary"
            >
              <v-btn value="label1">
                Label 1
              </v-btn>
              <v-btn value="label2">
                Label 2
              </v-btn>
              <v-btn value="label3">
                Label 3
              </v-btn>
              <v-btn value="label4">
                Label 4
              </v-btn>
            </ox-toggle-button>
            <div>Value = {{ toggle }}</div>
          </div>
        `
    })
}
