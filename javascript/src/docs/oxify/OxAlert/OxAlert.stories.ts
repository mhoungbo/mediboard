import type { Meta, StoryObj } from "@storybook/vue"
import OxAlert from "@oxify/components/OxAlert/OxAlert.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"

const meta = {
    title: "Core/Oxify/Alert",
    components: { OxAlert, OxButton },
    render: (args, { argTypes }) => ({
        components: { OxAlert, OxButton },
        props: Object.keys(argTypes),
        data: () => {
            return {
                modal: false
            }
        },
        template: `
            <div>
                <ox-button label="Show alert" @click="() => $data.modal = true" />
                <ox-alert 
                    v-model="$data.modal"
                    :label-accept="$props.labelAccept"
                    :label-cancel="$props.labelCancel"
                    :title="$props.title"
                    :width="$props.width"
                    @accept="$props.acceptFunction"
                    @cancel="$props.cancelFunction"
                >
                  {{$props.default}}
                </ox-alert>
            </div>
        `
    }),
    argTypes: {
        // @ts-ignore
        acceptFunction: { table: { disable: true } },
        cancelFunction: { table: { disable: true } },
        labelAccept: {
            name: "label-accept",
            description: "Label of accept button.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "text"
            }
        },
        labelCancel: {
            name: "label-cancel",
            description: "Label of cancel button.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "text"
            }
        },
        title: {
            name: "title",
            description: "Alert title.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "text"
            }
        },
        value: {
            name: "value",
            description: "Controls whether the component is visible or hidden.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                }
            },
            control: false
        },
        width: {
            name: "width",
            description: "Specifies a custom width for alert.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: 508
                },
                type: {
                    summary: "number"
                }
            },
            control: {
                type: "number"
            }
        },
        // @ts-ignore
        accept: {
            action: "accept",
            description: "Emit `accept` event when accept button is clicked",
            table: {
                category: "Events"
            },
            control: false
        },
        cancel: {
            action: "cancel",
            description: "Emit `cancel` event when cancel button is clicked",
            table: {
                category: "Events"
            },
            control: false
        },
        input: {
            action: "input",
            description: "Event binding for v-model",
            table: {
                category: "Events"
            },
            control: false
        },
        close: {
            action: "close",
            description: "Emit `close` event the alert is closed",
            table: {
                category: "Events"
            },
            control: false
        },
        default: {
            name: "default",
            description: "Alert content",
            control: {
                type: "text"
            },
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxAlert>

export default meta
type Story = StoryObj<Partial<typeof OxAlert>>

const voidFunction = () => {}

export const Default: Story = {
    args: {
        title: "Alert title",
        labelAccept: "Accept label",
        labelCancel: "Cancel label",
        default: "Alert content",
        acceptFunction: voidFunction,
        cancelFunction: voidFunction
    }
}

const acceptFunction = () => {
    alert("Accept Action")
}

const cancelFunction = () => {
    alert("Cancel Action")
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            showModal: false,
        }
    },
    methods: {
        acceptFunction () {
            alert("Accept Action")
        },
        cancelFunction () {
            alert("Cancel Action")
        }
    }
}

<ox-alert
  v-model="showModal"
  label-accept="Accept label"
  label-cancel="Cancel label"
  title="Alert title"
  v-on:accept="acceptFunction"
  v-on:cancel="cancelFunction"
>
  Each action will trigger alert
</ox-alert>
                `
            }
        }
    },
    args: {
        title: "Basic Alert",
        labelAccept: "Accept label",
        labelCancel: "Cancel label",
        default: "Each action will trigger alert",
        acceptFunction,
        cancelFunction
    }
}
