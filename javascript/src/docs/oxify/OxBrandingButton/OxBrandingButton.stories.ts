import type { Meta, StoryObj } from "@storybook/vue"
import OxBrandingButton from "@oxify/components/OxBrandingButton/OxBrandingButton.vue"
import { OxBrandingIconPath } from "@oxify/types/OxBrandingIconTypes"

const googleSvg: OxBrandingIconPath[] = [
    {
        d: "m23.745 12.27c0-0.79-0.07-1.54-0.19-2.27h-11.3v4.51h6.47c-0.29 1.48-1.14 2.73-2.4 3.58v3h3.86c2.26-2.09 3.56-5.17 3.56-8.82z",
        fill: "#aaa"
    },
    {
        d: "m12.255 24c3.24 0 5.95-1.08 7.93-2.91l-3.86-3c-1.08 0.72-2.45 1.16-4.07 1.16-3.13 0-5.78-2.11-6.73-4.96h-3.98v3.09c1.97 3.92 6.02 6.62 10.71 6.62z",
        fill: "#aaa"
    },
    {
        d: "m5.525 14.29c-0.25-0.72-0.38-1.49-0.38-2.29s0.14-1.57 0.38-2.29v-3.09h-3.98c-0.82 1.62-1.29 3.44-1.29 5.38s0.47 3.76 1.29 5.38z",
        fill: "#aaa"
    },
    {
        d: "m12.255 4.75c1.77 0 3.35 0.61 4.6 1.8l3.42-3.42c-2.07-1.94-4.78-3.13-8.02-3.13-4.69 0-8.74 2.7-10.71 6.62l3.98 3.09c0.95-2.85 3.6-4.96 6.73-4.96z",
        fill: "#aaa"
    }
]

const oxSvg: OxBrandingIconPath[] = [
    {
        d: "m3.9378 1.3583c-3.9376 0-3.9378 3.8629-3.9378 3.8629v13.533c0.074766 3.8878 3.9378 3.8381 3.9378 3.8381l16 0.04964s4.0874 9.7e-5 4.0624-3.8877v-10.018h-0.02481v-3.5888c-4e-6 -3.7881-3.7884-3.7884-3.7884-3.7884zm0 1.0222h16.224s2.8908 0 2.8908 2.866v3.5387h0.02481v9.9935c-0.02493 2.9158-3.14 2.9161-3.14 2.9161h-16s-2.8908-0.05013-2.8908-2.9161v-13.532s-0.025019-2.866 2.8908-2.866zm4.3861 4.8846c-1.3956 0-2.4674 0.39885-3.215 1.2213-0.74765 0.82242-1.1215 1.9936-1.1215 3.5139 0 1.5202 0.37383 2.7163 1.1215 3.5387 0.74765 0.82242 1.8194 1.2461 3.215 1.2461 1.3707 0 2.4425-0.39875 3.1902-1.2461 0.74766-0.84734 1.1215-2.0186 1.1215-3.5139 0-1.5202-0.37383-2.6665-1.1215-3.5139-0.74765-0.82242-1.8195-1.2461-3.1902-1.2461zm3.7383 0.14943 2.866 4.4611-3.0656 4.7848h1.6199l2.3427-3.7879 2.3179 3.7879h1.7445l-3.0904-4.76 2.8908-4.4859h-1.6447l-2.1432 3.489-2.1432-3.489zm-3.7134 1.1463c0.89719 0 1.57 0.27414 2.0186 0.87226 0.44859 0.59812 0.67318 1.4456 0.67318 2.592 0 1.1464-0.24941 2.0188-0.698 2.592-0.47351 0.5732-1.1462 0.87226-2.0434 0.87226-0.89719 0-1.5704-0.27414-2.0439-0.87226-0.47351-0.59812-0.69752-1.4456-0.69752-2.592 0-1.1215 0.24922-1.9938 0.74765-2.592 0.44859-0.5732 1.1462-0.87226 2.0434-0.87226z",
        fill: "#437dbc"
    }
]

const meta = {
    title: "Core/Oxify/BrandingButton",
    component: OxBrandingButton,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxBrandingButton },
        template: `
          <ox-branding-button
            :block="$props.block"
            :color="$props.color"
            :disabled="$props.disabled"
            :label="$props.label"
            :loading="$props.loading"
            :paths="$props.paths"
            :title="$props.title"
            :type="$props.type"
          />
        `
    }),
    argTypes: {
        icon: { table: { disable: true } },
        block: {
            name: "block",
            description: "Expands the button to 100% of available space.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        },
        color: {
            name: "color",
            description: "Applies specified color to the branding button.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "color"
            }
        },
        type: {
            name: "type",
            description: "Button's type. Defines a default color and a default icon (if a predefined icon exists) for the button.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "OxBrandingIconType",
                    detail: ' "prosante" | "gitlab" | "google" | "cpx" | "dmp" | "aati" | "adri" | "aldi" | "cdri"' +
                        ' | "hri" | "insi" | "dmti" | "imti" '
                }
            },
            control: "radio",
            options: ["prosante", "gitlab", "google", "cpx", "dmp", "insi"]
        },
        label: {
            name: "label",
            description: "Button's label. Set empty string to create an icon-only button.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        loading: {
            name: "loading",
            description: "Add a loading effect to the button.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        disabled: {
            name: "disabled",
            description: "Display the button in an inactive state.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        paths: {
            name: "paths",
            description: "List of paths to display the SVG icon. This paths will be used even if Oxify stores a predefined icon for the type chosen. Paths must be defined for a viewbox with 24 in height and 24 in width.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: []
                },
                type: {
                    summary: "OxBrandingIconPath[]",
                    detail: "{ \n" +
                        "   d: string \n" +
                        "   fill: string \n" +
                        "}"
                }
            },
            control: {
                type: "object"
            }
        },
        title: {
            name: "title",
            description: "Button's title display on hover.",
            control: {
                type: "text"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            }
        },
        // @ts-ignore
        click: {
            action: "click",
            description: "Emit `click` event when clicked",
            table: {
                category: "Events"
            },
            control: false
        }
    }
} satisfies Meta<typeof OxBrandingButton>

export default meta
type Story = StoryObj<typeof OxBrandingButton>

export const Default: Story = {
    args: {
        label: "Label"
    }
}

export const PredefinedIcons: Story = {
    parameters: {
        docs: {
            source: {
                code: `
private paths: OxBrandingIconPath[] = [
  {
    d: "m23.745 12.27c0-0.79-0.07-1.54-0.19-2.27h-11.3v4.51h6.47c-0.29 1.48-1.14 2.73-2.4 3.58v3h3.86c2.26-2.09 3.56-5.17 3.56-8.82z",
    fill: "#aaa"
  },
  {
    d: "m12.255 24c3.24 0 5.95-1.08 7.93-2.91l-3.86-3c-1.08 0.72-2.45 1.16-4.07 1.16-3.13 0-5.78-2.11-6.73-4.96h-3.98v3.09c1.97 3.92 6.02 6.62 10.71 6.62z",
    fill: "#aaa"
  },
  {
    d: "m5.525 14.29c-0.25-0.72-0.38-1.49-0.38-2.29s0.14-1.57 0.38-2.29v-3.09h-3.98c-0.82 1.62-1.29 3.44-1.29 5.38s0.47 3.76 1.29 5.38z",
    fill: "#aaa"
  },
  {
    d: "m12.255 4.75c1.77 0 3.35 0.61 4.6 1.8l3.42-3.42c-2.07-1.94-4.78-3.13-8.02-3.13-4.69 0-8.74 2.7-10.71 6.62l3.98 3.09c0.95-2.85 3.6-4.96 6.73-4.96z",
    fill: "#aaa"
  }
]

<div>
  Default version with predefined icon
  <div>
    <ox-branding-button
      label="S'identifier avec Pro Sante Connect"
      type="prosante"
    />
    <ox-branding-button
      label="S'identifier avec GitLab"
      type="gitlab"
    />
    <ox-branding-button
      label="S'identifier avec Google"
      type="google"
    />
  </div>
  Custom icon version
  <div>
    <ox-branding-button
      label="S'identifier avec Google"
      :paths="paths"
    />
  </div>
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxBrandingButton },
        data () {
            return {
                paths: googleSvg
            }
        },
        template: `
          <div>
              Default version with predefined icon
              <div>
                <ox-branding-button
                    label="S'identifier avec Pro Sante Connect"
                    type="prosante"
                />
                <ox-branding-button
                    label="S'identifier avec GitLab"
                    type="gitlab"
                />
                <ox-branding-button
                    label="S'identifier avec Google"
                    type="google"
                />
              </div>
              Custom icon version
              <div>
                <ox-branding-button
                    label="S'identifier avec Google"
                    :paths="paths"
                />
              </div>
          </div>
        `
    })
}

export const UndefinedIcons: Story = {
    parameters: {
        docs: {
            source: {
                code: `
private paths: OxBrandingIconPath[] = [
  {
    d: "m23.745 12.27c0-0.79-0.07-1.54-0.19-2.27h-11.3v4.51h6.47c-0.29 1.48-1.14 2.73-2.4 3.58v3h3.86c2.26-2.09 3.56-5.17 3.56-8.82z",
    fill: "#aaa"
  },
  {
    d: "m12.255 24c3.24 0 5.95-1.08 7.93-2.91l-3.86-3c-1.08 0.72-2.45 1.16-4.07 1.16-3.13 0-5.78-2.11-6.73-4.96h-3.98v3.09c1.97 3.92 6.02 6.62 10.71 6.62z",
    fill: "#aaa"
  },
  {
    d: "m5.525 14.29c-0.25-0.72-0.38-1.49-0.38-2.29s0.14-1.57 0.38-2.29v-3.09h-3.98c-0.82 1.62-1.29 3.44-1.29 5.38s0.47 3.76 1.29 5.38z",
    fill: "#aaa"
  },
  {
    d: "m12.255 4.75c1.77 0 3.35 0.61 4.6 1.8l3.42-3.42c-2.07-1.94-4.78-3.13-8.02-3.13-4.69 0-8.74 2.7-10.71 6.62l3.98 3.09c0.95-2.85 3.6-4.96 6.73-4.96z",
    fill: "#aaa"
  }
]

<div>
  Default version (without icon)
  <div>
    <ox-branding-button
      label="Authentification CPX"
      type="cpx"
    />
  </div>
  Custom version (with icon)
  <div>
    <ox-branding-button
      label="Authentification CPX"
      :paths="paths"
      type="cpx"
    />
  </div>
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxBrandingButton },
        data () {
            return {
                paths: googleSvg
            }
        },
        template: `
          <div>
              Default version (without icon)
              <div>
                <ox-branding-button
                    label="Authentification CPX"
                    type="cpx"
                />
              </div>
              Custom version (with icon)
              <div>
                <ox-branding-button
                    label="Authentification CPX"
                    :paths="paths"
                    type="cpx"
                />
              </div>
          </div>
        `
    })
}

export const Color: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-branding-button
  color="#555555"
  label="S'identifier avec Pro Sante Connect"
  type="prosante"
/>
<ox-branding-button
  color="#555555"
  label="Authentification CPX"
  type="cpx"
/>
                `
            }
        }
    },
    render: () => ({
        components: { OxBrandingButton },
        template: `
          <div>
              <ox-branding-button
                  color="#555555"
                  label="S'identifier avec Pro Sante Connect"
                  type="prosante"
              />
              <ox-branding-button
                  color="#555555"
                  label="Authentification CPX"
                  type="cpx"
              />
          </div>
        `
    })
}

export const FullCustom: Story = {
    parameters: {
        docs: {
            source: {
                code: `
private paths: OxBrandingIconPath[] = [
  {
    d: "m3.9378 1.3583c-3.9376 0-3.9378 3.8629-3.9378 3.8629v13.533c0.074766 3.8878 3.9378 3.8381 3.9378 3.8381l16 0.04964s4.0874 9.7e-5 4.0624-3.8877v-10.018h-0.02481v-3.5888c-4e-6 -3.7881-3.7884-3.7884-3.7884-3.7884zm0 1.0222h16.224s2.8908 0 2.8908 2.866v3.5387h0.02481v9.9935c-0.02493 2.9158-3.14 2.9161-3.14 2.9161h-16s-2.8908-0.05013-2.8908-2.9161v-13.532s-0.025019-2.866 2.8908-2.866zm4.3861 4.8846c-1.3956 0-2.4674 0.39885-3.215 1.2213-0.74765 0.82242-1.1215 1.9936-1.1215 3.5139 0 1.5202 0.37383 2.7163 1.1215 3.5387 0.74765 0.82242 1.8194 1.2461 3.215 1.2461 1.3707 0 2.4425-0.39875 3.1902-1.2461 0.74766-0.84734 1.1215-2.0186 1.1215-3.5139 0-1.5202-0.37383-2.6665-1.1215-3.5139-0.74765-0.82242-1.8195-1.2461-3.1902-1.2461zm3.7383 0.14943 2.866 4.4611-3.0656 4.7848h1.6199l2.3427-3.7879 2.3179 3.7879h1.7445l-3.0904-4.76 2.8908-4.4859h-1.6447l-2.1432 3.489-2.1432-3.489zm-3.7134 1.1463c0.89719 0 1.57 0.27414 2.0186 0.87226 0.44859 0.59812 0.67318 1.4456 0.67318 2.592 0 1.1464-0.24941 2.0188-0.698 2.592-0.47351 0.5732-1.1462 0.87226-2.0434 0.87226-0.89719 0-1.5704-0.27414-2.0439-0.87226-0.47351-0.59812-0.69752-1.4456-0.69752-2.592 0-1.1215 0.24922-1.9938 0.74765-2.592 0.44859-0.5732 1.1462-0.87226 2.0434-0.87226z",
    fill: "#437dbc"
  }
]

<ox-branding-button
  color="#437dbc"
  label="Authentification OpenXtrem"
  :paths="paths"
  type="ox"
/>
                `
            }
        }
    },
    render: () => ({
        components: { OxBrandingButton },
        data () {
            return {
                paths: oxSvg
            }
        },
        template: `
          <ox-branding-button
              color="#437dbc"
              label="Authentification OpenXtrem"
              :paths="paths"
              type="ox"
          />
        `
    })
}
