import type { Meta, StoryObj } from "@storybook/vue"
import OxDate from "@oxify/components/OxDate/OxDate.vue"

const dateObject = new Date("2021-05-25 14:22:00")
const dateString = "2021-05-25 14:22:00"

const meta = {
    title: "Core/Oxify/Date",
    component: OxDate,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDate },
        template: `
          <ox-date
            :date="$props.date" 
            :date-string="$props.dateString" 
            :mode="$props.mode"
          />`
    }),
    argTypes: {
        date: {
            name: "date",
            description: "The date to display (date format).",
            table: {
                category: "Props",
                type: {
                    summary: "Date"
                }
            },
            control: {
                type: "none"
            }
        },
        dateString: {
            name: "date-string",
            description: "The date to display (string format).",
            table: {
                category: "Props",
                type: {
                    summary: "String"
                }
            },
            control: {
                type: "text"
            }
        },
        mode: {
            name: "mode",
            description: "Display mode",
            table: {
                category: "Props",
                type: {
                    summary: '"month" | "day" | "date" | "datetime" | "time" | "completeday"'
                }
            },
            control: {
                type: "select",
                options: ["month", "day", "date", "datetime", "time", "completeday"]
            }
        }
    }
} satisfies Meta<typeof OxDate>

export default meta
type Story = StoryObj<typeof OxDate>

export const Default: Story = {
    args: {
        dateString: "2021-05-25 14:22:00"
    }
}

export const DateString: Story = {
    parameters: {
        docs: {
            source: {
                code: `
const dateObject = new Date("2021-05-25 14:22:00")
const dateString = "2021-05-25 14:22:00"
<div>
    <ox-date :date="dateObject" />
    and
    <ox-date :date-string="dateString" />
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxDate },
        data () {
            return {
                dateObject,
                dateString
            }
        },
        template: `
          <div>
              <ox-date :date="dateObject" />
              and
              <ox-date :date-string="dateString" />
          </div>
        `
    })
}

export const DisplayMode: Story = {
    parameters: {
        docs: {
            source: {
                code: `
const dateObject = new Date("2021-05-25 14:22:00")
const dateString = "2021-05-25 14:22:00"
<div>
  <ul>
    <li>
      <ox-date 
        :date-string="dateString"
        mode="month"
      />
    </li>
    <li>
      <ox-date 
        :date-string="dateString"
        mode="day"
      />
    </li>
    <li>
      <ox-date 
        :date-string="dateString"
        mode="date"
      />
    </li>
    <li>
      <ox-date 
        :date-string="dateString"
        mode="datetime"
      />
    </li>
    <li>
      <ox-date 
        :date-string="dateString"
        mode="completeday"
      />
    </li>
  </ul>
</div>
                `
            }
        }
    },
    render: () => ({
        components: { OxDate },
        data () {
            return {
                dateString
            }
        },
        template: `
          <div>
          <ul>
            <li>
              <ox-date
                  :date-string="dateString"
                  mode="month"
              />
            </li>
            <li>
              <ox-date
                  :date-string="dateString"
                  mode="day"
              />
            </li>
            <li>
              <ox-date
                  :date-string="dateString"
                  mode="date"
              />
            </li>
            <li>
              <ox-date
                  :date-string="dateString"
                  mode="datetime"
              />
            </li>
            <li>
              <ox-date
                  :date-string="dateString"
                  mode="completeday"
              />
            </li>
          </ul>
          </div>
        `
    })
}
