import type { Meta, StoryObj } from "@storybook/vue"
import OxSections from "@oxify/components/OxSections/OxSections.vue"

const sections = [
    {
        title: "Section 1",
        name: "section_1"
    },
    {
        title: "Section 2",
        name: "section_2"
    },
    {
        title: "Section 3",
        name: "section_3"
    }
]

const meta = {
    title: "Core/Oxify/Sections",
    component: OxSections,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxSections },
        template: `
          <ox-sections :sections="$props.sections">
            <template #section_1><div>Section 1</div></template>
            <template #section_2><div>Section 2</div></template>
            <template #section_3><div>Section 3</div></template>
          </ox-sections>
        `
    }),
    argTypes: {
        sections: {
            name: "sections",
            description: "The list of visible sections.",
            control: {
                type: "object"
            },
            table: {
                category: "Props",
                type: {
                    summary: "OxSectionModel[]",
                    detail: "{ \n" +
                        "   title: string \n" +
                        "   name: string \n" +
                        "}"
                }
            }
        },
        // @ts-ignore
        section_x: {
            name: "section_x",
            description: 'The component generates slots with his given section names. For example, if given section names are "section_1" and "section_2", the slots will have the same names.',
            table: {
                category: "Slots",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxSections>

export default meta
type Story = StoryObj<typeof OxSections>

export const Default: Story = {
    args: {
        sections
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            sections: [
                {
                    title: "Fruits",
                    name: "fruits"
                },
                {
                    title: "Vegetables",
                    name: "vegetables"
                },
                {
                    title: "Drinks",
                    name: "drinks"
                }
            ]
        }
    }
}

<ox-sections :sections="sections">
  <template #fruits>
    <div>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat turpis, tincidunt et maximus vitae, blandit 
      non orci. Ut placerat nisl turpis, quis accumsan diam tincidunt sit amet. Curabitur at massa id augue molestie 
      interdum finibus vitae mi. Suspendisse purus arcu, molestie vel fringilla in, iaculis in velit. Quisque et libero 
      id libero laoreet convallis ut vel urna. Nulla eu dolor quam. Aliquam commodo, mi venenatis cursus maximus, enim 
      lacus maximus quam, ut tempus risus turpis facilisis tortor.
      Cras gravida eros dolor, at ultrices tortor hendrerit ac. Praesent at nunc nec nulla dictum sagittis. Nam 
      convallis enim eget risus feugiat varius. Maecenas non hendrerit nulla. Vivamus at est mi. Duis dapibus ligula 
      quis eros consectetur, eget ultrices neque congue. Vestibulum a tortor ut enim interdum cursus quis eget dui.
    </div>
  </template>
  <template #vegetables>
    <div>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat turpis, tincidunt et maximus vitae, blandit 
      non orci. Ut placerat nisl turpis, quis accumsan diam tincidunt sit amet. Curabitur at massa id augue molestie 
      interdum finibus vitae mi. Suspendisse purus arcu, molestie vel fringilla in, iaculis in velit. Quisque et libero 
      id libero laoreet convallis ut vel urna. Nulla eu dolor quam. Aliquam commodo, mi venenatis cursus maximus, enim 
      lacus maximus quam, ut tempus risus turpis facilisis tortor.
      Cras gravida eros dolor, at ultrices tortor hendrerit ac. Praesent at nunc nec nulla dictum sagittis. Nam 
      convallis enim eget risus feugiat varius. Maecenas non hendrerit nulla. Vivamus at est mi. Duis dapibus ligula 
      quis eros consectetur, eget ultrices neque congue. Vestibulum a tortor ut enim interdum cursus quis eget dui.
    </div>
  </template>
  <template #drinks>
    <div>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat turpis, tincidunt et maximus vitae, blandit 
      non orci. Ut placerat nisl turpis, quis accumsan diam tincidunt sit amet. Curabitur at massa id augue molestie 
      interdum finibus vitae mi. Suspendisse purus arcu, molestie vel fringilla in, iaculis in velit. Quisque et libero 
      id libero laoreet convallis ut vel urna. Nulla eu dolor quam. Aliquam commodo, mi venenatis cursus maximus, enim 
      lacus maximus quam, ut tempus risus turpis facilisis tortor.
      Cras gravida eros dolor, at ultrices tortor hendrerit ac. Praesent at nunc nec nulla dictum sagittis. Nam 
      convallis enim eget risus feugiat varius. Maecenas non hendrerit nulla. Vivamus at est mi. Duis dapibus ligula 
      quis eros consectetur, eget ultrices neque congue. Vestibulum a tortor ut enim interdum cursus quis eget dui.
    </div>
  </template>
</ox-sections>
                `
            }
        }
    },
    render: () => ({
        components: { OxSections },
        data () {
            return {
                sections: [
                    {
                        title: "Fruits",
                        name: "fruits"
                    },
                    {
                        title: "Vegetables",
                        name: "vegetables"
                    },
                    {
                        title: "Drinks",
                        name: "drinks"
                    }
                ]
            }
        },
        template: `
          <div style="height: 400px;">
          <ox-sections :sections="sections">
            <template #fruits>
              <div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat turpis, tincidunt et maximus vitae, blandit
                non orci. Ut placerat nisl turpis, quis accumsan diam tincidunt sit amet. Curabitur at massa id augue molestie
                interdum finibus vitae mi. Suspendisse purus arcu, molestie vel fringilla in, iaculis in velit. Quisque et libero
                id libero laoreet convallis ut vel urna. Nulla eu dolor quam. Aliquam commodo, mi venenatis cursus maximus, enim
                lacus maximus quam, ut tempus risus turpis facilisis tortor.
                Cras gravida eros dolor, at ultrices tortor hendrerit ac. Praesent at nunc nec nulla dictum sagittis. Nam
                convallis enim eget risus feugiat varius. Maecenas non hendrerit nulla. Vivamus at est mi. Duis dapibus ligula
                quis eros consectetur, eget ultrices neque congue. Vestibulum a tortor ut enim interdum cursus quis eget dui.
              </div>
            </template>
            <template #vegetables>
              <div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat turpis, tincidunt et maximus vitae, blandit
                non orci. Ut placerat nisl turpis, quis accumsan diam tincidunt sit amet. Curabitur at massa id augue molestie
                interdum finibus vitae mi. Suspendisse purus arcu, molestie vel fringilla in, iaculis in velit. Quisque et libero
                id libero laoreet convallis ut vel urna. Nulla eu dolor quam. Aliquam commodo, mi venenatis cursus maximus, enim
                lacus maximus quam, ut tempus risus turpis facilisis tortor.
                Cras gravida eros dolor, at ultrices tortor hendrerit ac. Praesent at nunc nec nulla dictum sagittis. Nam
                convallis enim eget risus feugiat varius. Maecenas non hendrerit nulla. Vivamus at est mi. Duis dapibus ligula
                quis eros consectetur, eget ultrices neque congue. Vestibulum a tortor ut enim interdum cursus quis eget dui.
              </div>
            </template>
            <template #drinks>
              <div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat turpis, tincidunt et maximus vitae, blandit
                non orci. Ut placerat nisl turpis, quis accumsan diam tincidunt sit amet. Curabitur at massa id augue molestie
                interdum finibus vitae mi. Suspendisse purus arcu, molestie vel fringilla in, iaculis in velit. Quisque et libero
                id libero laoreet convallis ut vel urna. Nulla eu dolor quam. Aliquam commodo, mi venenatis cursus maximus, enim
                lacus maximus quam, ut tempus risus turpis facilisis tortor.
                Cras gravida eros dolor, at ultrices tortor hendrerit ac. Praesent at nunc nec nulla dictum sagittis. Nam
                convallis enim eget risus feugiat varius. Maecenas non hendrerit nulla. Vivamus at est mi. Duis dapibus ligula
                quis eros consectetur, eget ultrices neque congue. Vestibulum a tortor ut enim interdum cursus quis eget dui.
              </div>
            </template>
          </ox-sections>
          </div>
        `
    })
}
