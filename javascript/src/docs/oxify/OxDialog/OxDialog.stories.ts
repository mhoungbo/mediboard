import type { Meta, StoryObj } from "@storybook/vue"
import OxDialog from "@oxify/components/OxDialog/OxDialog.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"

const meta = {
    title: "Core/Oxify/Dialog",
    component: OxDialog,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDialog, OxButton },
        template: `
          <div>
            <ox-dialog
              :button-label="$props.buttonLabel"
              :compact="$props.compact"
              :eager="$props.eager"
              :full-height="$props.fullHeight"
              :fullscreen="$props.fullscreen"
              :hide-header="$props.hideHeader"
              :max-width="$props.maxWidth"
              :persistent="$props.persistent"
              :scrollable="$props.scrollable"
              :separated="$props.separated"
              :show-button="$props.showButton"
              :title="$props.title"
              :width="$props.width"
              @click="$props.clickFunction"
            >
              <template #activator="{ on, attrs }">
                <ox-button label="Show dialog" title="Show dialog" v-bind="attrs" v-on="on" />
              </template>
              {{ $props.default }}
              <template #actions>
                <ox-button button-style="tertiary" label="Delete" title="Delete" />
                <ox-button button-style="primary" label="Add" title="Add" />
              </template>
            </ox-dialog>
          </div>
        `
    }),
    argTypes: {
        // @ts-ignore
        clickFunction: { table: { disable: true } },
        buttonLabel: {
            name: "button-label",
            description: "Label of the topbar button if fullscreen mode is on.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "text"
            }
        },
        fullscreen: {
            name: "fullscreen",
            description: "Enables fullscreen mode.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        },
        hideHeader: {
            name: "hide-header",
            description: "Hide the header (containing the dialog title).",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        separated: {
            name: "separated",
            description: "Adds separator lines between the title, the content and actions slot.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        scrollable: {
            name: "scrollable",
            description: "When set to true, expects a v-card and a v-card-text component with a designated height. Allows to scroll in the content.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        showButton: {
            name: "show-button",
            description: "Displays the topbar button if the modal is in fullscreen mode.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: true
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        title: {
            name: "title",
            description: "Dialog title.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                },
                defaultValue: {
                    summary: undefined
                }
            },
            control: {
                type: "text"
            }
        },
        value: {
            name: "value",
            description: "Controls whether the component is visible or hidden.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                }
            },
            control: false
        },
        width: {
            name: "width",
            description: "Specifies a custom width for dialog.",
            table: {
                category: "Props",
                type: {
                    summary: "number"
                }
            },
            control: {
                type: "number"
            }
        },
        maxWidth: {
            name: "max-width",
            description: "Sets the maximum width for the dialog.",
            table: {
                category: "Props",
                type: {
                    summary: "number | string"
                }
            },
            control: {
                type: "number"
            }
        },
        compact: {
            name: "compact",
            description: "Removes sides and top paddings in the dialog content.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        },
        fullHeight: {
            name: "full-height",
            description: "In fullscreen mode, it forces the content to have a height that completely covers the modal.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        },
        eager: {
            name: "eager",
            description: "Will force the components content to render on mounted. This is useful if you have content that will not be rendered in the DOM that you want crawled for SEO.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        },
        persistent: {
            name: "persistent",
            description: "Avoid closing when clicking outside of the element or pressing esc key.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        },
        input: {
            action: "input",
            description: "Event binding for v-model",
            table: {
                category: "Events"
            },
            control: false
        },
        click: {
            action: "click",
            description: "Emit `click` event when the topbar button is clicked in fullscreen mode.",
            table: {
                category: "Events"
            },
            control: false
        },
        default: {
            name: "default",
            description: "Dialog content",
            control: {
                type: "text"
            },
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        activator: {
            name: "activator",
            description: "Dialog activator (to enable the modal).",
            control: {
                type: "text"
            },
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        actions: {
            name: "actions",
            description: "Dialog actions",
            control: {
                type: "text"
            },
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        opened: {
            name: "opened",
            description: "Return the generated `div`, this div is the modal's container, on the dialog opening.",
            table: {
                category: "events",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxDialog>

export default meta
type Story = StoryObj<typeof OxDialog>

export const Default: Story = {
    args: {
        separated: true,
        title: "Dialog title",
        // @ts-ignore
        default: "Content",
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        clickFunction: () => {},
        activator: `
<ox-button label="Show dialog" title="Show dialog" v-bind="attrs" v-on="on" />
        `,
        actions: `
<ox-button button-style="tertiary" label="Delete" title="Delete" />
<ox-button button-style="primary" label="Add" title="Add" />
        `
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            showDialog: false,
        }
    }
}

<ox-dialog
  v-model="showDialog"
  separated
  title="Dialog title"
>
  <template #activator="{ on, attrs }">
    <ox-button
      label="Show dialog"
      title="Show dialog"
      v-bind="attrs"
      v-on="on"
    />
  </template>
  Content
  <template #actions>
    <ox-button
      button-style="tertiary"
      label="Delete"
      title="Delete"
    />
    <ox-button
      button-style="primary"
      label="Add"
      title="Add"
    />
  </template>
</ox-dialog>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDialog, OxButton },
        data () {
            return {
                showDialog: false
            }
        },
        template: `
          <ox-dialog
              v-model="showDialog"
              separated
              title="Dialog title"
          >
          <template #activator="{ on, attrs }">
            <ox-button
                label="Show dialog"
                title="Show dialog"
                v-bind="attrs"
                v-on="on"
            />
          </template>
          Content
          <template #actions>
            <ox-button
                button-style="tertiary"
                label="Delete"
                title="Delete"
            />
            <ox-button
                button-style="primary"
                label="Add"
                title="Add"
            />
          </template>
          </ox-dialog>
        `
    })
}

export const FullScreen: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            showDialog: false,
        }
    },
    methods: {
        click () {
            alert("Click Action")
        }
    }
}


<ox-dialog
  v-model="showDialog"
  button-label="Click me"
  fullscreen
  show-button
  title="Dialog title"
  @click="click"
>
  <template #activator="{ on, attrs }">
    <ox-button
      label="Show dialog"
      title="Show dialog"
      v-bind="attrs"
      v-on="on"
    />
  </template>
  Content
  <template #actions>
    <ox-button
      button-style="tertiary"
      label="Delete"
      title="Delete"
    />
    <ox-button
      button-style="primary"
      label="Add"
      title="Add"
    />
  </template>
</ox-dialog>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxDialog, OxButton },
        data () {
            return {
                showDialog: false
            }
        },
        methods: {
            clickFunction () {
                alert("Click Action")
            }
        },
        template: `
          <ox-dialog
              v-model="showDialog"
              button-label="Click me"
              fullscreen
              show-button
              title="Dialog title"
              @click="clickFunction"
          >
          <template #activator="{ on, attrs }">
            <ox-button
                label="Show dialog"
                title="Show dialog"
                v-bind="attrs"
                v-on="on"
            />
          </template>
          Content
          <template #actions>
            <ox-button
                button-style="tertiary"
                label="Delete"
                title="Delete"
            />
            <ox-button
                button-style="primary"
                label="Add"
                title="Add"
            />
          </template>
          </ox-dialog>
        `
    })
}
