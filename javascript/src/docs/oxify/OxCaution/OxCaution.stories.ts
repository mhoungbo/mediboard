import type { Meta, StoryObj } from "@storybook/vue"
import OxCaution from "@oxify/components/OxCaution/OxCaution.vue"

const meta = {
    title: "Core/Oxify/Caution",
    component: OxCaution,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxCaution },
        template: `
          <ox-caution
              :icon="$props.icon"
              :plain="$props.plain"
              :prominent="$props.prominent"
              :show-divider="$props.showDivider"
              :show-icon="$props.showIcon"
              :title="$props.title"
              :type="$props.type"
              >
          {{ $props.default }}
          <template #subcontent>
            {{ $props.subcontent }}
          </template>
          </ox-caution>
        `
    }),
    argTypes: {
        icon: {
            name: "icon",
            description: "Custom icon's name.",
            control: "select",
            options: ["", "add", "cancel", "check", "refresh", "remove"],
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            }
        },
        plain: {
            name: "plain",
            description: "Displays the button in a plain state.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        prominent: {
            name: "prominent",
            description: "Displays the button in a prominent state.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        showDivider: {
            name: "show-divider",
            description: "Displays the divider between slots.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        showIcon: {
            name: "show-icon",
            description: "Displays the icon.",
            control: {
                type: "boolean"
            },
            table: {
                category: "Props",
                defaultValue: {
                    summary: false
                },
                type: {
                    summary: "boolean"
                }
            }
        },
        title: {
            name: "title",
            description: "Caution's title. Will be wrapped into h6 HTML element.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        type: {
            name: "type",
            description: "Caution's type. Defines a default color and a default icon (if a predefined icon exists), which help represent different actions each type portrays.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string",
                    detail: ' "success" | "error" | "info" | "warning" '
                }
            },
            control: "radio",
            options: ["success", "error", "info", "warning"]
        },
        // @ts-ignore
        default: {
            name: "default",
            description: "Default content slot",
            control: {
                type: "text"
            },
            table: {
                category: "Slots",
                type: {
                    summary: "DOM"
                }
            }
        },
        subcontent: {
            name: "subcontent",
            description: "Subcontent slot",
            control: {
                type: "text"
            },
            table: {
                category: "Slots",
                type: {
                    summary: "DOM"
                }
            }
        }
    }
} satisfies Meta<typeof OxCaution>

export default meta
type Story = StoryObj<typeof OxCaution>

export const Default: Story = {
    args: {
        // @ts-ignore
        default: "This is my default content Slot",
        subcontent: ""
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-caution
  icon="add"
  :show-divider="true"
  :show-icon="true"
  title="Title"
  type="warning"
>
  This is my default content Slot
  <template #subcontent>
    This is the subcontent
  </template>
</ox-caution>
                `
            }
        }
    },
    render: () => ({
        components: { OxCaution },
        template: `
          <ox-caution
              icon="add"
              :show-divider="true"
              :show-icon="true"
              title="Title"
              type="warning"
          >
              This is my default content Slot
              <template #subcontent>
                This is the subcontent
              </template>
          </ox-caution>
        `
    })
}
