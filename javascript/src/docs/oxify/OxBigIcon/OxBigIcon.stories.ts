import type { Meta, StoryObj } from "@storybook/vue"
import OxBigIcon from "@oxify/components/OxBigIcon/OxBigIcon.vue"

const meta = {
    title: "Core/Oxify/BigIcon",
    component: OxBigIcon,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxBigIcon },
        template: '<ox-big-icon v-bind="$props" />'
    }),
    argTypes: {
        icon: {
            name: "icon",
            description: "Icon's name.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        context: {
            name: "context",
            description: "Icon display context.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "onBackground"
                },
                type: {
                    summary: "string",
                    detail: ' "onBackground" | "onPrimary" '
                }
            },
            control: {
                type: "radio",
                options: ["onBackground", "onPrimary"]
            }
        }
    }
} satisfies Meta<typeof OxBigIcon>

export default meta
type Story = StoryObj<typeof OxBigIcon>

export const Default: Story = {
    args: {
        icon: "add"
    }
}

export const BasicUsage :Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-big-icon
  icon="calendar"
/>
<ox-big-icon
  context="onPrimary"
  icon="lock"
/>
                `
            }
        }
    },
    render: () => ({
        components: { OxBigIcon },
        template: `
            <div>
                <ox-big-icon
                    icon="calendar"
                />
                <ox-big-icon
                    context="onPrimary"
                    icon="lock"
                />
            </div>
        `
    })
}
