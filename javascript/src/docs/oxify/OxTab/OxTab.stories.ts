import { Meta, StoryObj } from "@storybook/vue"
import OxTabs from "@oxify/components/OxTabs/OxTabs.vue"
import OxTab from "@oxify/components/OxTab/OxTab.vue"
import OxTabContent from "@oxify/components/OxTabContent/OxTabContent.vue"

const meta = {
    title: "Core/Oxify/Tab",
    component: OxTab,
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            tab: null
        }
    }
})

<ox-tabs v-model="tab">
  <ox-tab v-bind="$props"/>
  <ox-tab v-bind="$props"/>

  <template #contents>
    <ox-tab-content>1</ox-tab-content>
    <ox-tab-content>2</ox-tab-content>
  </template>
</ox-tabs>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxTabs, OxTab, OxTabContent },
        data () {
            return {
                tab: null
            }
        },
        template: `
          <ox-tabs v-model="tab">
              <ox-tab v-bind="$props"/>
              <ox-tab v-bind="$props"/>
    
              <template #contents>
                <ox-tab-content>1</ox-tab-content>
                <ox-tab-content>2</ox-tab-content>
              </template>
          </ox-tabs>
        `
    })
} satisfies Meta<typeof OxTab>

export default meta
type Story = StoryObj<typeof OxTab>

export const Default: Story = {
    args: {
        title: "Lorem"
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            tab: null
        }
    }
})

<ox-tabs v-model="tab">
  <ox-tab title="Onglet" />
  <ox-tab title="Onglet" />

  <template #contents>
    <ox-tab-content>1</ox-tab-content>
    <ox-tab-content>2</ox-tab-content>
  </template>
</ox-tabs>
                `
            }
        }
    },
    args: {
        title: "Onglet"
    }
}

export const WithIcon: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default Vue.extend({
    name: "MyComponent",
    data () {
        return {
            tab: null
        }
    }
})

<ox-tabs v-model="tab">
  <ox-tab title="Onglet" icon-name="caretDown" />
  <ox-tab title="Onglet" icon-name="caretDown" />

  <template #contents>
    <ox-tab-content>1</ox-tab-content>
    <ox-tab-content>2</ox-tab-content>
  </template>
</ox-tabs>
                `
            }
        }
    },
    args: {
        title: "Onglet",
        iconName: "caretDown"
    }
}
