import type { Meta, StoryObj } from "@storybook/vue"
import OxSorter from "@oxify/components/OxSorter/OxSorter.vue"

export const choices = [
    {
        label: "First criteria",
        value: "first_criteria"
    },
    {
        label: "Second criteria",
        value: "second_criteria"
    },
    {
        label: "Third criteria",
        value: "third_criteria"
    }
]

const meta = {
    title: "Core/Oxify/Sorter",
    component: OxSorter,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxSorter },
        template: `
          <ox-sorter
            :aspect="$props.aspect"
            :choices="$props.choices"
            :default-choice="$props.defaultChoice"
            :default-sort="$props.defaultSort"
          />
        `
    }),
    argTypes: {
        aspect: {
            name: "aspect",
            description: "Aspect style of the sorter.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "select"
                },
                type: {
                    summary: "string",
                    detail: ' "select" | "text" '
                }
            },
            control: "radio",
            options: ["select", "text"]
        },
        choices: {
            name: "choices",
            description: "The list of available choices.",
            control: {
                type: "object"
            },
            table: {
                category: "Props",
                type: {
                    summary: "OxSorterChoiceModel[]",
                    detail: "{ \n" +
                        "   label: string \n" +
                        "   value: string \n" +
                        "}"
                }
            }
        },
        defaultChoice: {
            name: "default-choice",
            description: "The default selected choice value.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        defaultSort: {
            name: "default-sort",
            description: "The default selected sort.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "ASC"
                },
                type: {
                    summary: "string"
                }
            },
            control: "radio",
            options: ["ASC", "DESC"]
        },
        // @ts-ignore
        update: {
            action: "update",
            description: "Emit `update` event with when the sort value (ASC / DESC) or the choice value changes. Sends in arguments the new selected choice and sort values.",
            table: {
                category: "Events",
                type: {
                    summary: "selectedChoiceSort",
                    detail: "{ \n" +
                        "   choice: string \n" +
                        "   sort: string \n" +
                        "}"
                }
            },
            control: false
        }
    }
} satisfies Meta<typeof OxSorter>

export default meta
type Story = StoryObj<typeof OxSorter>

export const Default: Story = {
    args: {
        choices
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
export default {
    name: "MyComponent",
    data () {
        return {
            choices: [
                {
                    label: 'First criteria',
                    value: 'first_criteria'
                },
                {
                    label: 'Second criteria',
                    value: 'second_criteria'
                },
                {
                    label: 'Third criteria',
                    value: 'third_criteria'
                }
            ] as OxSorterChoiceModel[]
        }
    }
}

<ox-sorter
  :choices="choices"
  :default-choice="second_criteria"
  :default-sort="DESC"
/>
                        `
            }
        }
    },
    args: {
        choices,
        defaultChoice: "second_criteria",
        defaultSort: "DESC"
    }
}
