import type { Meta, StoryObj } from "@storybook/vue"
import OxBadge from "@oxify/components/OxBadge/OxBadge.vue"
import OxIcon from "@oxify/components/OxIcon/OxIcon.vue"
import OxButton from "@oxify/components/OxButton/OxButton.vue"

const meta = {
    title: "Core/Oxify/Badge",
    component: OxBadge,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxBadge, OxIcon },
        template: '<ox-badge v-bind="$props"><ox-icon icon="practitioner"/></ox-badge>'
    }),
    argTypes: {
        bottom: {
            name: "bottom",
            description: "Set the badge on the bottom of the element.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        color: {
            name: "color",
            description: "Badge color.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: "primary"
                },
                type: {
                    summary: "string",
                    detail: ' "primary" | "secondary" | "error" '
                }
            },
            control: {
                type: "radio",
                options: ["primary", "secondary", "error"]
            }
        },
        content: {
            name: "content",
            description: "String displayed inside the badge.",
            table: {
                category: "Props",
                type: {
                    summary: "string"
                }
            },
            control: {
                type: "text"
            }
        },
        displayed: {
            name: "displayed",
            description: "Display the badge.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                }
            },
            control: {
                type: "boolean"
            }
        },
        dot: {
            name: "dot",
            description: "Display the badge as a small dot (disable the content).",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        left: {
            name: "left",
            description: "Set the button on the left of the element.",
            table: {
                category: "Props",
                type: {
                    summary: "boolean"
                },
                defaultValue: {
                    summary: "false"
                }
            },
            control: {
                type: "boolean"
            }
        },
        // @ts-ignore
        default: {
            name: "default",
            description: "Element fixed by the badge.",
            table: {
                category: "slots",
                type: {
                    summary: "DOM"
                }
            },
            control: false
        }
    }
} satisfies Meta<typeof OxBadge>

export default meta
type Story = StoryObj<typeof OxBadge>

export const Default: Story = {
    args: {
        color: "primary",
        content: "7"
    }
}

export const TargetElement: Story = {
    parameters: {
        docs: {
            source: {
                code: `
<ox-badge dot>
  <ox-icon icon="star" />
</ox-badge>

<ox-badge content="!">
  <ox-button label="New action"/>
</ox-badge>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxBadge, OxIcon, OxButton },
        template: `
          <div style="display: flex; justify-content: space-evenly;">
            <ox-badge dot>
              <ox-icon icon="star"/>
            </ox-badge>
            <ox-badge content="!">
              <ox-button label="New action"/>
            </ox-badge>
          </div>
        `
    })
}

export const ConditionalDisplay: Story = {
    parameters: {
        docs: {
            source: {
                code: `
private displayBadge = true

private toggleDisplay (): void {
  this.displayBadge = !this.displayBadge
}

<ox-badge
  :displayed="displayBadge"
  content="!"
>
  <ox-button
    label="Toggle display"
    v-on:click="toggleDisplay"
  />
</ox-badge>
                `
            }
        }
    },
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxBadge, OxIcon, OxButton },
        methods: {
            toggleDisplay: (data) => {
                data.display = !data.display
            }
        },
        data () {
            return {
                display: true
            }
        },
        template: '<ox-badge :displayed="$data.display" content="!"><ox-button label="Toggle display" @click="() => { toggleDisplay($data) }"/></ox-badge>'
    })
}
