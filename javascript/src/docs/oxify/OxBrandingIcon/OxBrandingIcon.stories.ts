import type { Meta, StoryObj } from "@storybook/vue"
import OxBrandingIcon from "@oxify/components/OxBrandingIcon/OxBrandingIcon.vue"
import { OxBrandingIconPath } from "@oxify/types/OxBrandingIconTypes"

const paths: OxBrandingIconPath[] = [
    {
        d: "m3.9378 1.3583c-3.9376 0-3.9378 3.8629-3.9378 3.8629v13.533c0.074766 3.8878 3.9378 3.8381 3.9378 3.8381l16 0.04964s4.0874 9.7e-5 4.0624-3.8877v-10.018h-0.02481v-3.5888c-4e-6 -3.7881-3.7884-3.7884-3.7884-3.7884zm0 1.0222h16.224s2.8908 0 2.8908 2.866v3.5387h0.02481v9.9935c-0.02493 2.9158-3.14 2.9161-3.14 2.9161h-16s-2.8908-0.05013-2.8908-2.9161v-13.532s-0.025019-2.866 2.8908-2.866zm4.3861 4.8846c-1.3956 0-2.4674 0.39885-3.215 1.2213-0.74765 0.82242-1.1215 1.9936-1.1215 3.5139 0 1.5202 0.37383 2.7163 1.1215 3.5387 0.74765 0.82242 1.8194 1.2461 3.215 1.2461 1.3707 0 2.4425-0.39875 3.1902-1.2461 0.74766-0.84734 1.1215-2.0186 1.1215-3.5139 0-1.5202-0.37383-2.6665-1.1215-3.5139-0.74765-0.82242-1.8195-1.2461-3.1902-1.2461zm3.7383 0.14943 2.866 4.4611-3.0656 4.7848h1.6199l2.3427-3.7879 2.3179 3.7879h1.7445l-3.0904-4.76 2.8908-4.4859h-1.6447l-2.1432 3.489-2.1432-3.489zm-3.7134 1.1463c0.89719 0 1.57 0.27414 2.0186 0.87226 0.44859 0.59812 0.67318 1.4456 0.67318 2.592 0 1.1464-0.24941 2.0188-0.698 2.592-0.47351 0.5732-1.1462 0.87226-2.0434 0.87226-0.89719 0-1.5704-0.27414-2.0439-0.87226-0.47351-0.59812-0.69752-1.4456-0.69752-2.592 0-1.1215 0.24922-1.9938 0.74765-2.592 0.44859-0.5732 1.1462-0.87226 2.0434-0.87226z",
        fill: "#437dbc"
    }
]

const meta = {
    title: "Core/Oxify/BrandingIcon",
    component: OxBrandingIcon,
    render: (args, { argTypes }) => ({
        props: Object.keys(argTypes),
        components: { OxBrandingIcon },
        template: `
          <ox-branding-icon
              :paths="$props.paths"
              :size="$props.size"
              :type="$props.type"
          />
        `
    }),
    argTypes: {
        type: {
            name: "type",
            description: "Icon's type. Defines a default icon (if a predefined icon exists).",
            table: {
                category: "Props",
                defaultValue: {
                    summary: ""
                },
                type: {
                    summary: "OxBrandingIconType",
                    detail: ' "prosante" | "gitlab" | "google" | "cpx" | "dmp" | "aati" | "adri" | "aldi" | "cdri"' +
                        ' | "hri" | "insi" | "dmti" | "imti" '
                }
            },
            control: "radio",
            options: [
                "prosante",
                "gitlab",
                "google",
                "dmp",
                "aati",
                "adri",
                "aldi",
                "cdri",
                "hri",
                "insi",
                "dmti",
                "imti"
            ]
        },
        size: {
            name: "size",
            description: "Icon's size.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: 24
                },
                type: {
                    summary: "number"
                }
            },
            control: {
                type: "number"
            }
        },
        paths: {
            name: "paths",
            description: "List of paths to display the SVG icon. This paths will be used even if Oxify stores a predefined icon for the type chosen. Paths must be defined for a viewbox with 24 in height and 24 in width.",
            table: {
                category: "Props",
                defaultValue: {
                    summary: []
                },
                type: {
                    summary: "OxBrandingIconPath[]",
                    detail: "{ \n" +
                        "   d: string \n" +
                        "   fill: string \n" +
                        "}"
                }
            },
            control: {
                type: "object"
            }
        }
    }
} satisfies Meta<typeof OxBrandingIcon>

export default meta
type Story = StoryObj<typeof OxBrandingIcon>

export const Default: Story = {
    args: {
        type: "prosante"
    }
}

export const BasicUsage: Story = {
    parameters: {
        docs: {
            source: {
                code: `
private paths: OxBrandingIconPath[] = [
  {
    d: "m3.9378 1.3583c-3.9376 0-3.9378 3.8629-3.9378 3.8629v13.533c0.074766 3.8878 3.9378 3.8381 3.9378 3.8381l16 0.04964s4.0874 9.7e-5 4.0624-3.8877v-10.018h-0.02481v-3.5888c-4e-6 -3.7881-3.7884-3.7884-3.7884-3.7884zm0 1.0222h16.224s2.8908 0 2.8908 2.866v3.5387h0.02481v9.9935c-0.02493 2.9158-3.14 2.9161-3.14 2.9161h-16s-2.8908-0.05013-2.8908-2.9161v-13.532s-0.025019-2.866 2.8908-2.866zm4.3861 4.8846c-1.3956 0-2.4674 0.39885-3.215 1.2213-0.74765 0.82242-1.1215 1.9936-1.1215 3.5139 0 1.5202 0.37383 2.7163 1.1215 3.5387 0.74765 0.82242 1.8194 1.2461 3.215 1.2461 1.3707 0 2.4425-0.39875 3.1902-1.2461 0.74766-0.84734 1.1215-2.0186 1.1215-3.5139 0-1.5202-0.37383-2.6665-1.1215-3.5139-0.74765-0.82242-1.8195-1.2461-3.1902-1.2461zm3.7383 0.14943 2.866 4.4611-3.0656 4.7848h1.6199l2.3427-3.7879 2.3179 3.7879h1.7445l-3.0904-4.76 2.8908-4.4859h-1.6447l-2.1432 3.489-2.1432-3.489zm-3.7134 1.1463c0.89719 0 1.57 0.27414 2.0186 0.87226 0.44859 0.59812 0.67318 1.4456 0.67318 2.592 0 1.1464-0.24941 2.0188-0.698 2.592-0.47351 0.5732-1.1462 0.87226-2.0434 0.87226-0.89719 0-1.5704-0.27414-2.0439-0.87226-0.47351-0.59812-0.69752-1.4456-0.69752-2.592 0-1.1215 0.24922-1.9938 0.74765-2.592 0.44859-0.5732 1.1462-0.87226 2.0434-0.87226z",
    fill: "#437dbc"
  }
]

<ox-branding-icon 
  type="gitlab" 
  :size="42"
/>
<ox-branding-icon :paths="paths" />
                `
            }
        }
    },
    render: () => ({
        components: { OxBrandingIcon },
        data () {
            return {
                paths
            }
        },
        template: `
          <div>
              <ox-branding-icon
                  type="gitlab"
                  :size="42"
              />
              <ox-branding-icon :paths="paths" />
          </div>
        `
    })
}
