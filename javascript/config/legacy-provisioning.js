const path = require("path")
const fs = require("fs-extra")
const now = require("performance-now")

const startParamCopy = now()
const startParamDelete = now()

const source = "node_modules"
const destination = "lib"

const librariesList = {
    "@openxtrem/datepicker": "datepicker",
    "@openxtrem/flot": "flot",
    "@openxtrem/flotr": "flotr",
    "@openxtrem/scriptaculous": "scriptaculous",
    "@openxtrem/visjs": "visjs",
    "@openxtrem/favico": "favico",
    "@openxtrem/ckeditor": "ckeditor",
    "@openxtrem/livepipe": "livepipe",
    "@openxtrem/jquery": "jquery-3.6.0",
    "@openxtrem/expr-eval": "jsExpressionEval",
    "@openxtrem/jitsi-meet": "lib-jitsi-meet",
    bowser: "bowser",
    crossfilter: "crossfilter",
    d3: "d3-3.5.16",
    dc: "dc",
    fabric: "fabricjs",
    papaparse: "PapaParse",
    "paste.js": "paste.js",
    prismjs: "prismjs",
    requirejs: "requirejs",
    "spectrum-colorpicker": "spectrum",
    store: "store.js"
}

let nbLibrariesCopied = 0
let nbLibrariesDeleted = 0

try {
    if (fs.pathExistsSync(destination)) {
        console.log("\x1b[36m%s\x1b[0m", "Checking for legacy libraries to delete...")

        const libraries = fs.readdirSync(destination)
        libraries.forEach(lib => {
            fs.rmSync(path.join(destination, lib), { recursive: true })
            if (!Object.values(librariesList).includes(lib)) {
                nbLibrariesDeleted++
            }
        })

        if (nbLibrariesDeleted > 0) {
            const endParamDelete = now()
            console.log("\x1b[32m%s\x1b[0m", `${nbLibrariesDeleted} libraries deleted in ${parseFloat(endParamDelete - startParamDelete).toFixed(2)} milliseconds`)
        }
        else {
            console.log("\x1b[32m%s\x1b[0m", "No libraries to delete")
        }
    }

    console.log("\x1b[36m%s\x1b[0m", "Copying legacy libraries processing...")

    copyLegacyLibraries(librariesList)
}
catch (err) {
    console.error(err)
}

const endParamCopy = now()
console.log("\x1b[32m%s\x1b[0m", `${nbLibrariesCopied} libraries copied from node_modules/ to lib/ in ${parseFloat(endParamCopy - startParamCopy).toFixed(2)} milliseconds`)

function copyLegacyLibraries (libraries) {
    for (const [key, value] of Object.entries(libraries)) {
        const isExists = fs.pathExistsSync(path.join(source, key))

        if (isExists) {
            fs.copySync(path.join(source, key), path.join(destination, value), { overwrite: true })
            nbLibrariesCopied++
        }
        else {
            console.log("\x1b[31m%s\x1b[0m", `Missing library ${key} from node_modules/`)
        }
    }
}
