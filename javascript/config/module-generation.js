const fs = require("fs")
const now = require("performance-now")

/**
 * Modules list generation process => generated in javascript/src/core/utils/modulesList.json file
 */
module.exports = function buildModules () {
    console.log("\x1b[36m%s\x1b[0m", "Modules list generation processing...")
    const startModulesListGeneration = now()
    const modulesListFile = "javascript/src/core/utils/modulesList.json"
    const modulesList = []

    try {
        fs.writeFileSync(modulesListFile, "{\n    \"modules\": [\n")

        fs.readdirSync("modules/").forEach((file, idx, array) => {
            modulesList.push(file)
            fs.appendFileSync(modulesListFile, "\"" + file)

            idx === array.length - 1
                ? fs.appendFileSync(modulesListFile, "\"\n")
                : fs.appendFileSync(modulesListFile, "\",\n")
        })

        fs.appendFileSync(modulesListFile, "    ]\n}\n")
        console.log("\x1b[32m%s\x1b[0m", "File modulesList.json created successfully")
    }
    catch (err) {
        console.error(err)
    }

    const endModulesListGeneration = now()
    console.log("\x1b[32m%s\x1b[0m", `Modules list generated in ${parseFloat(endModulesListGeneration - startModulesListGeneration).toFixed(2)} milliseconds`)
    return modulesList
}
