import type { Preview } from "@storybook/vue"
import { VApp } from "vuetify/lib"
import Vuetify from "../../src/core/plugins/OxVuetifyCore"
import SbThemeSwitcher from "../storybook/SbThemeSwitcher/SbThemeSwitcher.vue"
import Vue from "vue"
import SbTranslator from "./plugins/SbTranslator"

Vue.use(SbTranslator)

const preview: Preview = {
    decorators: [
        (story) => ({
            vuetify: Vuetify,
            components: { VApp, SbThemeSwitcher },
            template: `
              <v-app style="border-radius: 6px;">
                  <div style="padding: 24px;">
                    <story />
                    <sb-theme-switcher />
                  </div>
              </v-app>
            `
        })
    ],
    parameters: {
        actions: { argTypesRegex: "^on[A-Z].*" },
        options: {
            storySort: {
                method: "alphabetical",
                order: [
                    "Intro",
                    "Colors",
                    "Elevation",
                    "Typography",
                    "Icons",
                    "Responsive",
                    "Oxify",
                    "Core"
                ]
            }
        },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/
            },
            sort: "requiredFirst"
        },
        docs: {
            controls: {
                sort: "requiredFirst"
            },
            source: {
                code: null
            }
        }
    }
}

export default preview
