import type { StorybookConfig } from "@storybook/vue-webpack5"
import path from "path"
import VuetifyLoaderPlugin from "vuetify-loader/lib/plugin"

const config: StorybookConfig = {
    stories: ["../../src/docs/**/*.mdx", "../../src/docs/**/*.stories.@(js|jsx|ts|tsx)"],
    addons: [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        "@storybook/addon-interactions"
    ],
    framework: {
        name: "@storybook/vue-webpack5",
        options: {}
    },
    docs: {
        autodocs: "tag",
        defaultName: "Documentation"
    },
    webpackFinal: async (config) => {
        if (config.resolve) {
            config.resolve.alias = {
                ...config.resolve.alias,
                "@": path.resolve("javascript/src/"),
                "@oxify": path.resolve("javascript/src/core/oxify/"),
                assets: path.resolve("images/assets_vue"),
                "@modules": path.resolve("modules/")
            }
        }
        if (config.plugins) {
            config.plugins.push(new VuetifyLoaderPlugin())
        }
        // @ts-ignore
        config.module.rules.push(
            {
                test: /\.sass$/,
                use: [
                    "vue-style-loader",
                    "css-loader",
                    {
                        loader: "sass-loader",
                        options: {
                            // This is the path to your variables
                            additionalData: "@import '@/core/styles/base.scss'"
                        }
                    }
                ]
            },
            /*
             * SCSS has different line endings than SASS
             * and needs a semicolon after the import.
             */
            {
                test: /\.scss$/,
                use: [
                    "vue-style-loader",
                    "css-loader",
                    {
                        loader: "sass-loader",
                        // Requires sass-loader@^9.0.0
                        options: {
                            // This is the path to your variables
                            additionalData: "@import '@/core/styles/base.scss';"
                        }
                    }
                ]
            }
        )
        return config
    }
}
export default config
