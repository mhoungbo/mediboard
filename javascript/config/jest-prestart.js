// JEST global setup file
// This script is executed before each test file
import Vue from "vue"
import Vuetify from "vuetify"
import { OxAppContext } from "@/core/utils/OxAppContext"

Vue.use(Vuetify)

Vue.config.productionTip = false;

OxAppContext.start()
OxAppContext.sessionHash = "sessionHash"

window.$T = function (trad) { return trad }
