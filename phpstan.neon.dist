includes:
  - phpstan-baseline.neon

parameters:
  # phpVersion prevents reporting different errors with the current php version
  phpVersion: 80100
  level: 2
  tmpDir: tmp/phpstan
  paths:
    - core/classes
    - cli/classes
  excludePaths:
    # The extension might not be present and the error cannot be ignored.
    - core/classes/COracleDataSource.php
  ignoreErrors:
    # Ignore errors container @throws Psr\SimpleCache\InvalidArgumentException because the PSR was created before the Throwable interface
    - '#^PHPDoc tag @throws with type\s*([\w\|\\]+\|)?\s*Psr\\SimpleCache\\InvalidArgumentException\s*(\|[\w\|\\]+)?\s*is not subtype of Throwable$#'
    # Ignore errors container @throws Elasticsearch\Common\Exceptions\ElasticsearchException because the interface was created before Throwable interface
    - '#^PHPDoc tag @throws with type\s*([\w\|\\]+\|)?\s*Elasticsearch\\Common\\Exceptions\\ElasticsearchException\s*(\|[\w\|\\]+)?\s*is not subtype of Throwable$#'
    # Cannot make CStoredObject::__construct final but no CStoredObject's class' __construct take arguments
    - '#^Unsafe usage of new static\(\)\.$#'
    # This attribute is defined by PHPStorm for array autocompletion
    - '#^Attribute class JetBrains\\PhpStorm\\ArrayShape does not exist\.$#'
  forbiddenFunctionsExtension:
          data:
            -
              rules:
                'Ox\Core\CValue::session': 'Usage of session is forbidden'
                'Ox\Core\CValue::sessionAbs': 'Usage of session is forbidden'
                'Ox\Core\CValue::getOrSession': 'Usage of session is forbidden'
                'Ox\Core\CValue::postOrSession': 'Usage of session is forbidden'
                'Ox\Core\CValue::getOrSessionAbs': 'Usage of session is forbidden'
                'Ox\Core\CValue::postOrSessionAbs': 'Usage of session is forbidden'
                'Ox\Core\CValue::setSession': 'Usage of session is forbidden'
                'Ox\Core\CValue::setSessionAbs': 'Usage of session is forbidden'
                'dd': 'use only for debug purposes'
                'phpinfo': ''
                'utf8_decode': 'Use mb_convert_encoding($str, "ISO-8859-1", "UTF-8") or CMbString::utf8Decode($str) instead'
                'utf8_encode': 'Use mb_convert_encoding($str, "UTF-8", "ISO-8859-1") or CMbString::utf8Encode($str) instead'
                'session_abort': 'Do not use session_* functions. Use the session object from the Request.'
                'session_cache_expire': 'Do not use session_* functions. Use the session object from the Request.'
                'session_cache_limiter': 'Do not use session_* functions. Use the session object from the Request.'
                'session_commit': 'Do not use session_* functions. Use the session object from the Request.'
                'session_create_id': 'Do not use session_* functions. Use the session object from the Request.'
                'session_decode': 'Do not use session_* functions. Use the session object from the Request.'
                'session_destroy': 'Do not use session_* functions. Use the session object from the Request.'
                'session_encode': 'Do not use session_* functions. Use the session object from the Request.'
                'session_gc': 'Do not use session_* functions. Use the session object from the Request.'
                'session_gc_cookie_params': 'Do not use session_* functions. Use the session object from the Request.'
                'session_id': 'Do not use session_* functions. Use the session object from the Request.'
                'session_module_name': 'Do not use session_* functions. Use the session object from the Request.'
                'session_name': 'Do not use session_* functions. Use the session object from the Request.'
                'session_regenerate_id': 'Do not use session_* functions. Use the session object from the Request.'
                'session_register_shutdown': 'Do not use session_* functions. Use the session object from the Request.'
                'session_reset': 'Do not use session_* functions. Use the session object from the Request.'
                'session_save_path': 'Do not use session_* functions. Use the session object from the Request.'
                'session_set_cookie_params': 'Do not use session_* functions. Use the session object from the Request.'
                'session_set_save_handler': 'Do not use session_* functions. Use the session object from the Request.'
                'session_start': 'Do not use session_* functions. Use the session object from the Request.'
                'session_status': 'Do not use session_* functions. Use the session object from the Request.'
                'session_unset': 'Do not use session_* functions. Use the session object from the Request.'
                'session_write_close': 'Do not use session_* functions. Use the session object from the Request.'

parametersSchema:
    forbiddenFunctionsExtension: structure([
        data: arrayOf(arrayOf(arrayOf(string())))
    ])
services:
    -
        class: Ox\Tests\PHPStan\Rule\ForbiddenFunctionsRule
        tags:
            - phpstan.rules.rule
        arguments:
            data: %forbiddenFunctionsExtension.data%
