<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Exception;
use InvalidArgumentException;
use Ox\Core\Api\Resources\Item;
use Ox\Core\CAppUI;
use Ox\Core\Controller;
use Ox\Core\CModelObjectCollection;
use Ox\Core\CSQLDataSource;
use Ox\Core\Kernel\Exception\ControllerException;
use Ox\Core\Kernel\Exception\UnreachableDatabaseException;
use Ox\Interop\Fhir\Controllers\CFHIRController;
use Ox\Mediboard\Admin\Controllers\PermissionController;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\System\Controllers\SystemController;
use Ox\Mediboard\System\CSourceHTTP;
use Ox\Mediboard\System\Tests\Classes\SourceTestTrait;
use Ox\Tests\OxUnitTestCase;
use ReflectionClass;
use ReflectionMethod;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class ControllerTest extends OxUnitTestCase
{
    use SourceTestTrait;

    private const CONTROLLER      = CFHIRController::class;
    private const FUNCTION        = 'initBlink1';
    private const FUNCTION_FAILED = 'failedTestFunction';
    private const MODULE          = 'fhir';
    private const ROUTE           = 'fhir_metadata';

    /**
     * RenderResponse
     */
    public function testRenderResponse(): void
    {
        $controller = $this->getController();
        $content    = 'Lorem ipsum';
        $response   = $controller->renderResponse($content);
        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals($response->getContent(), $content);
    }

    /**
     * GetModule
     *
     * @throws Exception
     */
    public function testGetModule(): void
    {
        $controller = $this->getController();
        $module     = $controller->getModuleName();
        $this->assertEquals($module, self::MODULE);
    }

    /**
     * GetControllerFromRequest
     *
     * @throws ControllerException
     */
    public function testGetControllerFromRequest(): void
    {
        $route = new Route('/lorem/ipsum');
        $route->setDefault('_controller', self::CONTROLLER);
        $collection = new RouteCollection();
        $collection->add('a', $route);

        $request    = Request::create('/lorem/ipsum');
        $matcher    = new UrlMatcher($collection, new RequestContext());
        $parameters = $matcher->matchRequest($request);
        $request->attributes->add($parameters);

        $controller = Controller::getControllerFromRequest($request);
        $this->assertEquals($controller, $this->getController());
    }

    public function testGetReflectionMethod(): void
    {
        $controller = self::CONTROLLER;
        /** @var Controller $controller */
        $controller = new $controller();
        $this->assertInstanceOf(ReflectionMethod::class, $controller->getReflectionMethod(self::FUNCTION));
    }

    public function testGetReflectionClass(): void
    {
        $controller = self::CONTROLLER;
        /** @var Controller $controller */
        $controller = new $controller();
        $this->assertInstanceOf(ReflectionClass::class, $controller->getReflectionClass());
    }

    /**
     * RenderJsonRespons
     */
    public function testRenderJsonResponse(): void
    {
        $controller = $this->getController();
        $content    = ['foo' => 'bar'];
        $response   = $controller->renderJsonResponse($content, 200, [], false);
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals($response->getContent(), json_encode($content));
    }

    /**
     * GetControllerFromRouteOk
     */
    public function testGetControllerFromRouteOk(): void
    {
        $this->assertInstanceOf(Controller::class, Controller::getControllerFromRoute($this->getRoute()));
    }

    /**
     * GetControllerFromRouteKo
     */
    public function testGetControllerFromRouteKo(): void
    {
        $this->expectException(ControllerException::class);
        Controller::getControllerFromRoute(new Route('api', []));
    }

    /**
     * GetMethodFromRouteOk
     */
    public function testGetMethodFromRouteOk(): void
    {
        $this->assertEquals(self::FUNCTION, Controller::getMethodFromRoute($this->getRoute()));
    }

    /**
     * GetMethodFromRouteKo
     */
    public function testGetMethodFromRouteKo(): void
    {
        $this->expectException(ControllerException::class);
        Controller::getMethodFromRoute($this->getRouteFailed());
    }

    /**
     * @return Controller
     */
    private function getController(): Controller
    {
        $controller = self::CONTROLLER;

        return new $controller();
    }

    /**
     * @return Route
     */
    private function getRoute(): Route
    {
        return new Route('http::localhost', ['_controller' => self::CONTROLLER . '::' . self::FUNCTION]);
    }

    /**
     * @return Route
     */
    private function getRouteFailed(): Route
    {
        return new Route('http::localhost', ['_controller' => self::CONTROLLER . '::' . self::FUNCTION_FAILED]);
    }

    public function testRedirect(): void
    {
        $controller = new PermissionController();
        $target_url = 'http://www.loremi-ipsum.fr';
        /** @var RedirectResponse $response */
        $response = $this->invokePrivateMethod($controller, 'redirect', $target_url);
        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals($target_url, $response->getTargetUrl());
    }

    public function testStore(): void
    {
        $controller = new SystemController();
        /** @var CUser $model */
        $model                = $this->getObjectFromFixturesReference(
            CUser::class,
            UsersFixtures::REF_USER_LOREM_IPSUM
        );
        $address1             = uniqid('address');
        $model->user_address1 = $address1;

        $collection = new CModelObjectCollection();
        $collection->add($model);

        /** @var Item $item */
        $item = $this->invokePrivateMethod($controller, 'storeCollection', $collection);
        $this->assertEquals($item->getDatas()[0]->user_address1, $address1);
    }

    public function testSelfRequestSourceDisabled(): void
    {
        $source         = new CSourceHTTP();
        $source->active = 0;
        $controller     = new SystemController();

        $this->expectExceptionObject(
            new ControllerException(Response::HTTP_INTERNAL_SERVER_ERROR, CAppUI::tr('access-forbidden'))
        );
        $this->invokePrivateMethod($controller, 'selfRequest', $source);
    }

    public function testSelfRequestUnautorized(): void
    {
        $source         = $this->getMockBuilder(CSourceHTTP::class)
                               ->disableOriginalConstructor()
                               ->getMock();
        $source->host   = '127.0.0.1';
        $source->active = 1;

        $response = new \Nyholm\Psr7\Response(Response::HTTP_UNAUTHORIZED, [], 'loremp ipsum');
        $this->mockSourceForResponse($response, $source);

        $controller = new SystemController();
        $this->expectExceptionObject(
            new ControllerException(Response::HTTP_INTERNAL_SERVER_ERROR, CAppUI::tr('access-forbidden'))
        );

        $this->invokePrivateMethod($controller, 'selfRequest', $source);
    }

    public function testPrepareHeaderFromResponse(): void
    {
        $response_headers = [
            'Server'            => "test",
            'X-Powered-By'      => "test",
            'X-Mb-RequestInfo'  => "test",
            'Set-Cookie'        => "test",
            'Transfer-Encoding' => "test",
        ];

        $this->assertEmpty(
            $this->invokePrivateMethod($this->getController(), 'prepareHeaderFromResponse', $response_headers)
        );
    }

    public function testRenderXmlResponse(): void
    {
        $controller = $this->getController();
        $content    = $controller->renderXmlResponse(['lorem' => "ipsum"])->getContent();

        $this->assertStringContainsString('<lorem>ipsum</lorem>', $content);
    }

    /**
     * @config enslaving_active 0
     */
    public function testEnforceSlaveNotActive(): void
    {
        $this->assertFalse(CSQLDataSource::isSlaveState());
        $controller = new SystemController();
        $this->invokePrivateMethod($controller, 'enforceSlave');
        $this->assertFalse(CSQLDataSource::isSlaveState());
    }

    /**
     * @config db slave dbhost 0
     */
    public function testEnforceSlaveNoSlave(): void
    {
        $this->assertFalse(CSQLDataSource::isSlaveState());
        $controller = new SystemController();
        $this->invokePrivateMethod($controller, 'enforceSlave');
        $this->assertFalse(CSQLDataSource::isSlaveState());
    }

    /**
     * @config enslaving_active 1
     */
    public function testEnforceSlave(): Controller
    {
        $this->assertFalse(CSQLDataSource::isSlaveState());
        $controller = new SystemController();
        $this->invokePrivateMethod($controller, 'enforceSlave');
        $this->assertTrue(CSQLDataSource::isSlaveState());

        return $controller;
    }

    /**
     * @depends testEnforceSlave
     */
    public function testDisableSlave(Controller $controller): void
    {
        $this->invokePrivateMethod($controller, 'disableSlave');

        $this->assertFalse(CSQLDataSource::isSlaveState());
    }

    /**
     * @config db slave dbname foo
     * @config enslaving_active 1
     *
     * @runInSeparateProcess
     */
    public function testEnforceSlaveDieError(): void
    {
        $controller = new SystemController();

        unset(CSQLDataSource::$dataSources['slave']);

        $this->expectException(UnreachableDatabaseException::class);
        $this->invokePrivateMethod($controller, 'enforceSlave');
    }

    /**
     * @config db slave dbname foo
     * @config enslaving_active 1
     *
     * @runInSeparateProcess
     */
    public function testEnforceSlaveNoDieError(): void
    {
        $controller = new SystemController();

        unset(CSQLDataSource::$dataSources['slave']);

        $this->invokePrivateMethod($controller, 'enforceSlave', false);

        $this->assertFalse(CSQLDataSource::isSlaveState());
    }

    public function testAddUiMsg(): void
    {
        // Reset msg and scripts
        CAppUI::getMsg();
        CAppUI::$instance->scripts = [];

        $controller = new SystemController();

        $this->invokePrivateMethod($controller, 'addUiMsgOk', 'ok_normal');
        $this->invokePrivateMethod($controller, 'addUiMsgOk', 'ok_normal');
        $this->invokePrivateMethod($controller, 'addUiMsgOk', 'ok_notif', true);
        $this->invokePrivateMethod($controller, 'addUiMsgAlert', 'alert_normal');
        $this->invokePrivateMethod($controller, 'addUiMsgAlert', 'alert_notif', true);
        $this->invokePrivateMethod($controller, 'addUiMsgWarning', 'warning_normal');
        $this->invokePrivateMethod($controller, 'addUiMsgWarning', 'warning_notif', true);
        $this->invokePrivateMethod($controller, 'addUiMsgError', 'error_normal');
        $this->invokePrivateMethod($controller, 'addUiMsgError', 'error_notif', true);

        $this->assertEquals(2, CAppUI::$instance->messages[UI_MSG_OK]['ok_normal']);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_ALERT]['alert_normal']);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_WARNING]['warning_normal']);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_ERROR]['error_normal']);

        $this->assertStringContainsString('ok_notif', CAppUI::$instance->scripts[0]);
        $this->assertStringContainsString('alert_notif', CAppUI::$instance->scripts[1]);
        $this->assertStringContainsString('warning_notif', CAppUI::$instance->scripts[2]);
        $this->assertStringContainsString('error_notif', CAppUI::$instance->scripts[3]);
    }
}
