<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Ox\Core\CApp;
use Ox\Core\CCanDo;
use Ox\Core\Kernel\Exception\PublicEnvironmentException;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\HttpFoundation\Request;

class CCanDoTest extends OxUnitTestCase
{
    public function testPublicEnvironmentPreventsUsage(): void
    {
        CApp::getInstance()->setPublic(true);

        $can = new CCanDo();

        $exception_throwned = 0;
        try {
            $can->denied();
            $this->fail('$can->denied() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }

        try {
            $can->needsRead();
            $this->fail('$can->needsRead() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }

        try {
            $can->needsEdit();
            $this->fail('$can->needsEdit() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }

        try {
            $can->needsAdmin();
            $this->fail('$can->needsAdmin() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }

        try {
            $can::check();
            $this->fail('$can::check() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }
        try {
            $can::checkRead();
            $this->fail('$can::checkRead() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }
        try {
            $can::checkEdit();
            $this->fail('$can::checkEdit() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }
        try {
            $can::checkAdmin();
            $this->fail('$can::checkAdmin() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }

        try {
            $can::read();
            $this->fail('$can::read() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }
        try {
            $can::edit();
            $this->fail('$can::edit() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }
        try {
            $can::admin();
            $this->fail('$can::admin() did not throw exception');
        } catch (PublicEnvironmentException) {
            $exception_throwned++;
        }

        $this->assertEquals(11, $exception_throwned);
    }

    /**
     * Preventing unit test handlers to fail when in public environment
     * (because of CApp global state internally modified).
     */
    public function tearDown(): void
    {
        parent::tearDown();

        CApp::getInstance()->setPublic(false);
    }
}
