<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Controllers;

use Ox\Core\Controller;

class CRouteManagerControllerTest extends Controller
{
    public function without_doc()
    {
        return;
    }

    /**
     * @api
     */
    public function with_false_doc()
    {
        return;
    }

    /**
     * @api public
     */
    public function succes()
    {
        return;
    }
}
