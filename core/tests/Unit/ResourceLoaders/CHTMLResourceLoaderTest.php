<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\ResourceLoaders;

use Ox\Core\Kernel\Event\MainControllerListener;
use Ox\Core\ResourceLoaders\CHTMLResourceLoader;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CHTMLResourceLoaderTest extends OxUnitTestCase
{
    public function testMakeAllInOneResponse(): void
    {
        $content     = $this->getContentIn();
        $response_in = new Response($content);

        $response_out = CHTMLResourceLoader::makeAllInOneResponse($response_in, true, false, null);
        $this->assertEquals($this->getContentOut(), $response_out->getContent());
    }

    public function testMakeAllInOneResponseWithSaveFile(): void
    {
        $content      = $this->getContentIn();
        $response_in  = new Response($content);
        $response_out = CHTMLResourceLoader::makeAllInOneResponse(
            $response_in,
            CHTMLResourceLoader::MODE_SAVE_FILE,
            false,
            null
        );

        $this->assertInstanceOf(StreamedResponse::class, $response_out);

        ob_start();
        $response_out->sendContent();
        $content = ob_get_contents();
        ob_end_clean();

        // Content is the binary of the zip file generated
        $this->assertIsString($content);
    }

    private function getContentIn(): string
    {
        $resource_dir = dirname(__DIR__, 2) . '/Resources/ResourceLoaders';

        return <<< EOD
<base href="test/../toto">
<div>Simple text for display</div>
<img src="$resource_dir/logo.svg">
<script type="text/javascript" src="$resource_dir/script.js"></script>
<span>More text !</span>
<link rel="stylesheet" href="$resource_dir/css.css"/>
EOD;
    }

    private function getContentOut(): string
    {
        return <<< EOD

<div>Simple text for display</div>
<img   src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzYiIGhlaWdodD0iMzYiIHZpZXdCb3g9IjAgMCAzNiAzNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICA8cGF0aCBkPSJNMTYuMjM3NSAyNC4zQzExLjcgMjYuNzc1IDguMTM3NSAyNC4yNjI1IDcuOTEyNSAyMS4zMzc1QzcuNSAxNi42MTI1IDEzLjUgNy4xOTk5OSAyMy40NzUgMC44OTk5OTRDMjIuMiAyLjEzNzQ5IDIuMDYyNSAyMy4wMjUgMTYuMjM3NSAyNC4zWiIgZmlsbD0iIzNGNTFCNSI+PC9wYXRoPgogIDxwYXRoIGQ9Ik0xOS44NzUgMTEuNTVDMjQuNDEyNSA5LjA3NDk4IDI3Ljk3NSAxMS41ODc1IDI4LjIgMTQuNTEyNUMyOC41NzUgMTkuMjM3NSAyMi41NzUgMjguNjUgMTIuNjM3NSAzNC45ODc1QzEzLjkxMjUgMzMuNzEyNSAzNC4wMTI1IDEyLjgyNSAxOS44NzUgMTEuNTVaIiBmaWxsPSIjMDNBOUY0Ij48L3BhdGg+Cjwvc3ZnPgo="  />
<script type="text/javascript">Script = {
  test: function () {
    console.log('Hello');
  }
}
</script>
<span>More text !</span>
<style type="text/css" >div.test {
  color: orange;
}
</style>

EOD;
    }
}
