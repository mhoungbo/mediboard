<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Ox\Core\Composer\CComposer;
use Ox\Tests\OxUnitTestCase;

/**
 * @group schedules
 */
class CComposerTest extends OxUnitTestCase
{
    public function testVersion(): void
    {
        $composer = new CComposer();
        $this->assertStringStartsWith('2.', $composer->getVersion());
    }

    public function testJson(): void
    {
        $composer = new CComposer();
        $this->assertJson($composer->getJson());
    }

    public function testPrefix(): void
    {
        $composer = new CComposer();
        $this->assertNotEmpty($composer->getPrefixPsr4());
    }

    public function testCheckAll(): void
    {
        $composer = new CComposer();
        $this->assertTrue($composer->checkAll());
    }

    public function testCount(): void
    {
        $composer = new CComposer();
        $this->assertGreaterThan(0, $composer->countPackages());
        $this->assertGreaterThan(0, $composer->countPackagesInstalled());
    }

    public function testLicense(): void
    {
        $composer = new CComposer();
        $this->assertNotEmpty($composer->licenses());
    }
}
