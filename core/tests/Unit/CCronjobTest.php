<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use DateInterval;
use DateTime;
use Exception;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\System\Cron\CCronJob;
use Ox\Tests\OxUnitTestCase;

class CCronjobTest extends OxUnitTestCase
{
    /**
     * Tests some cronjob expressions
     * Todo: Test more complex expressions (complicated because of time dependency, consider using a DateTime mockup
     * interface)
     *
     * @throws Exception
     */
    public function testGetNextDate(): void
    {
        $date = new DateTime('now');

        $expressions = [
            '0 * * * * *' => [
                $date->format('Y-m-d H:i:00'),
                $date->add(new DateInterval('PT1M'))->format('Y-m-d H:i:00'),
                $date->add(new DateInterval('PT1M'))->format('Y-m-d H:i:00'),
                $date->add(new DateInterval('PT1M'))->format('Y-m-d H:i:00'),
                $date->add(new DateInterval('PT1M'))->format('Y-m-d H:i:00'),
            ],
        ];

        foreach ($expressions as $_expression => $_expected_results) {
            $_cronjob            = new CCronJob();
            $_cronjob->execution = $_expression;
            [, $_cronjob->_minute, $_cronjob->_hour, $_cronjob->_day, $_cronjob->_month, $_cronjob->_week] = explode(
                ' ',
                $_expression
            );

            $_results = $_cronjob->getNextDate(5);

            foreach ($_results as $_k => $_result) {
                $this->assertEquals($_expected_results[$_k], $_result);
            }
        }
    }

    public function testStoreAndGenerateToken(): void
    {
        $params = "m=system\ntab=about";

        $cron                  = new CCronJob();
        $cron->name            = 'test';
        $cron->params          = $params;
        $cron->active          = '1';
        $cron->_generate_token = true;

        $this->assertNull($cron->store());
        $this->assertNull($cron->params);

        /** @var CViewAccessToken $token */
        $token = $cron->loadRefToken();
        $this->assertInstanceOf(CViewAccessToken::class, $token);
        $this->assertEquals($params, $token->params);
    }

    public function testStoreAndReuseToken(): void
    {
        $token          = new CViewAccessToken();
        $token->params  = "m=system\ntab=about";
        $token->label   = 'test';
        $token->user_id = CUser::get()->_id;
        $this->assertNull($token->store());

        $cron                  = new CCronJob();
        $cron->name            = 'test';
        $cron->params          = "token={$token->hash}";
        $cron->active          = '1';
        $cron->_generate_token = true;

        $this->assertNull($cron->store());
        $this->assertNull($cron->params);
        $this->assertEquals($token->_id, $cron->token_id);
    }

    /**
     * @dataProvider makeUrlProvider
     */
    public function testMakeUrl(CCronJob $cron, string $expected): void
    {
        try {
            $this->assertEquals($expected, $cron->makeUrl('/'));
        } finally {
            if ($cron->_id) {
                $cron->purge();
            }
        }
    }

    public function makeUrlProvider(): array
    {
        $token = new CViewAccessToken();
        $token->_id = 1;
        $token->hash = 'aaaaaaaaaa';

        $cron_token = new CCronJob();
        $cron_token->token_id = 1;
        $cron_token->_fwd['token_id'] = $token;
        $cron_token->_params = [
            'execute_cron_log_id' => 10,
            'foo' => 'bar'
        ];

        $cron_params = new CCronJob();
        $cron_params->_params = ['m' => 'system', 'tab' => 'about'];

        $cron_route = new CCronJob();
        $cron_route->route_name = 'system_gui_about';
        $cron_route->_params = ['foo' => 'bar'];

        return [
            'with_token'  => [$cron_token, '/?token=aaaaaaaaaa&execute_cron_log_id=10'],
            'with_route'  => [$cron_route, '/gui/system/about?foo=bar'],
            'with_params' => [$cron_params, '/?m=system&tab=about'],
        ];
    }
}
