<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\FieldSpecs;

use Exception;
use Ox\Core\FieldSpecs\BinarySpec;
use Ox\Tests\OxUnitTestCase;

class BinarySpecTest extends OxUnitTestCase
{
    /**
     * @dataProvider binaryValueProvider
     *
     * @param string|null $value
     *
     * @return void
     * @throws Exception
     */
    public function testValueIsNeverAltered(?string $value): void
    {
        $class = new class {
            public ?string $property = null;
        };

        $spec = new BinarySpec($class, 'property', 'binary');

        $this->assertEquals($value, $spec->filter($value));
    }

    public function binaryValueProvider(): array
    {
        return [
            'null'                        => [null],
            'string'                      => ['abc'],
            'binary'                      => [b'01100001 01100010 01100011'],
            'binary with carriage return' => [hex2bin('38084cf1743dcf2500671e705cc6c1288b6d13381b9ed733cff90a'),],
        ];
    }
}
