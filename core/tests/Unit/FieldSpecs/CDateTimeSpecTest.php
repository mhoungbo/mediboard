<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\FieldSpecs;

use DateTimeImmutable;
use Exception;
use Ox\Core\FieldSpecs\CDateTimeSpec;
use Ox\Tests\OxUnitTestCase;
use stdClass;

class CDateTimeSpecTest extends OxUnitTestCase
{
    /**
     * @return void
     * @throws Exception
     */
    public function testGetTypedValue(): void
    {
        $spec = new CDateTimeSpec('foo', 'bar');

        $dataset = [
            ['now', new DateTimeImmutable()],
            ['2023-01-01 12:30:15', new DateTimeImmutable('2023-01-01 12:30:15')],
            [null, null],
        ];

        foreach ($dataset as [$value, $expected]) {
            $value = $spec->getTypedValue($value);
            if ($expected instanceof DateTimeImmutable && $value instanceof DateTimeImmutable) {
                /* Assert that the datetime objects are similar, with a delta accounting the potential delay due to the test execution */
                $this->assertEqualsWithDelta($expected->getTimestamp(), $value->getTimestamp(), 5);
            } else {
                $this->assertEquals($expected, $value);
            }
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testGetTypedValueInvalidDatetimeStringException(): void
    {
        $spec = new CDateTimeSpec('foo', 'bar');

        $this->expectExceptionMessage('Failed to parse time string');
        $spec->getTypedValue('string');
    }

    /**
     * @dataProvider getTypedValueExceptionProvider
     *
     * @param mixed $value
     *
     * @return void
     * @throws Exception
     */
    public function testGetTypedValueException(mixed $value): void
    {
        $spec = new CDateTimeSpec('foo', 'bar');

        $this->expectException('InvalidArgumentException');
        $spec->getTypedValue($value);
    }

    public function getTypedValueExceptionProvider(): array
    {
        return [
            [[]],
            [new stdClass()],
            [false],
            [15]
        ];
    }
}
