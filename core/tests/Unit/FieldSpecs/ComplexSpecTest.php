<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\FieldSpecs;

use LogicException;
use Ox\Core\FieldSpecs\ComplexSpec;
use Ox\Tests\OxUnitTestCase;
use stdClass;

class ComplexSpecTest extends OxUnitTestCase
{
    public function testOnlyFormFieldsAreAllowed(): void
    {
        new ComplexSpec(stdClass::class, '_form_field');

        $this->expectException(LogicException::class);
        new ComplexSpec(stdClass::class, 'plain_field');
    }

    public function testCallableIsCalled(): void
    {
        $class = new class {
            public ?string $_field = null;

            public function method(): string
            {
                return 'valued';
            }
        };

        $spec = new ComplexSpec($class, '_field', 'complex callable|method');

        $this->assertEquals('valued', $spec->getValue(new $class()));
    }

    public function testCallableIsRestrictedToObjectClassScope(): void
    {
        $class = new class {
            public ?string $_field = null;

            public function method(): string
            {
                return 'valued';
            }
        };

        $spec = new ComplexSpec($class, '_field', 'complex callable|not_my_method');

        $this->assertEquals('', $spec->getValue(new $class()));
    }

    public function testStaticCallableWorks(): void
    {
        $class = new class {
            public ?string $_field = null;

            public static function method(): string
            {
                return 'valued';
            }
        };

        $spec = new ComplexSpec($class, '_field', 'complex callable|method');

        $this->assertEquals('valued', $spec->getValue(new $class()));
    }

    public function testCallableCanAccessPrivateProperties(): void
    {
        $class = new class {
            public ?string $_field = null;
            private string $value  = 'private value';

            public function method(): string
            {
                return $this->value;
            }
        };

        $spec = new ComplexSpec($class, '_field', 'complex callable|method');

        $this->assertEquals('private value', $spec->getValue(new $class()));
    }

    public function testPrivateCallableDoesNothing(): void
    {
        $class = new class {
            public ?string $_field = null;

            private function method(): string
            {
                return 'valued';
            }
        };

        $spec = new ComplexSpec($class, '_field', 'complex callable|method');

        $this->assertEquals('', $spec->getValue(new $class()));
    }

    public function testCallableIsNotCalledWhenFieldIsValued(): void
    {
        $class = new class {
            public ?string $_field = 'default value';

            public function method(): string
            {
                return 'valued';
            }
        };

        $spec = new ComplexSpec($class, '_field', 'complex callable|method');

        $this->assertEquals('default value', $spec->getValue(new $class()));
    }
}
