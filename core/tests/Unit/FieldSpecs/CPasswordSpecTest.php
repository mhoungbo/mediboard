<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\FieldSpecs;

use LogicException;
use Ox\Core\FieldSpecs\CPasswordSpec;
use Ox\Tests\OxUnitTestCase;
use stdClass;

class CPasswordSpecTest extends OxUnitTestCase
{
    public function testStandardPassword(): void
    {
        $spec = new CPasswordSpec(stdClass::class, 'field');

        // Asserting that "default" password spec is not considered as an encrypted password
        $this->assertFalse($spec->isEncrypted());
        $this->assertEquals('VARCHAR(50)', $spec->getDBSpec());
    }

    public function testEncryptedPassword(): void
    {
        $spec = new CPasswordSpec(stdClass::class, 'field', 'password key|test formField|_field');

        $this->assertTrue($spec->isEncrypted());
        $this->assertEquals('INT(11) UNSIGNED', $spec->getDBSpec());
    }


    /**
     * @dataProvider invalidEncryptedSpecProvider
     *
     * @param string $prop
     *
     * @return void
     */
    public function testEncryptedPasswordFailsIfInvalidProp(string $prop): void
    {
        $this->expectException(LogicException::class);
        new CPasswordSpec(stdClass::class, 'field', $prop);
    }

    public function invalidEncryptedSpecProvider(): array
    {
        return [
            'no formField'               => ['password key|test',],
            'formField not a form field' => ['password key|test formField|field'],
            'formField but no key'       => ['password formField|_field'],
        ];
    }
}
