<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\FieldSpecs;

use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Tests\OxUnitTestCase;
use stdClass;

class CRefSpecTest extends OxUnitTestCase
{
    /**
     * @dataProvider getTypedValueProvider
     *
     * @param mixed $value
     * @param mixed $expected
     *
     * @return void
     */
    public function testGetTypedValue(mixed $value, mixed $expected): void
    {
        $spec = new CRefSpec('foo', 'bar');

        $this->assertEquals($expected, $spec->getTypedValue($value));
    }

    public function getTypedValueProvider(): array
    {
        $object = new stdClass();

        return [
            ['1', 1],
            ['0', 0],
            ['string', 0],
            ['12string', 12],
            [null, null],
        ];
    }

    /**
     * @dataProvider getTypedValueExceptionProvider
     *
     * @param mixed $value
     *
     * @return void
     */
    public function testGetTypedValueException(mixed $value): void
    {
        $spec = new CRefSpec('foo', 'bar');

        $this->expectException('InvalidArgumentException');
        $spec->getTypedValue($value);
    }

    public function getTypedValueExceptionProvider(): array
    {
        return [
            [[]],
            [new stdClass()],
        ];
    }
}
