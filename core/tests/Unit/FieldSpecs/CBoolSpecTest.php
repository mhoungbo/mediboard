<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\FieldSpecs;

use Ox\Core\FieldSpecs\CBoolSpec;
use Ox\Tests\OxUnitTestCase;
use stdClass;

class CBoolSpecTest extends OxUnitTestCase
{
    /**
     * @dataProvider getTypedValueProvider
     *
     * @param mixed $value
     * @param mixed $expected
     *
     * @return void
     */
    public function testGetTypedValue(mixed $value, mixed $expected): void
    {
        $spec = new CBoolSpec('foo', 'bar');

        $this->assertEquals($expected, $spec->getTypedValue($value));
    }

    public function getTypedValueProvider(): array
    {
        return [
            ['1', true],
            ['0', false],
            ['string', true],
            [null, null],
        ];
    }

    /**
     * @dataProvider getTypedValueExceptionProvider
     *
     * @param mixed $value
     *
     * @return void
     */
    public function testGetTypedValueException(mixed $value): void
    {
        $spec = new CBoolSpec('foo', 'bar');

        $this->expectException('InvalidArgumentException');
        $spec->getTypedValue($value);
    }

    public function getTypedValueExceptionProvider(): array
    {
        return [
            [[]],
            [new stdClass()],
        ];
    }
}
