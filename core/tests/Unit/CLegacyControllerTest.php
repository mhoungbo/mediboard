<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Exception;
use Ox\Core\CClassMap;
use Ox\Core\CLegacyController;
use Ox\Core\CMbArray;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use ReflectionException;
use stdClass;

class CLegacyControllerTest extends OxUnitTestCase
{
    /**
     * @param mixed $data
     *
     * @dataProvider renderJsonExceptionProvider
     * @throws TestsException|ReflectionException
     */
    public function testRenderJsonExpectExceptions($data): void
    {
        $this->expectException(Exception::class);
        $this->invokePrivateMethod(CLegacyController::class, "renderJson", $data);
    }

    public function renderJsonExceptionProvider(): array
    {
        return [[new stdClass()], [null]];
    }

    /**
     * Restrict adding legacy controllers
     *
     * This test will pass on :
     * - Deleting legacy controller
     *
     * And it will fail on :
     * - Adding legacy controller
     * - Renaming legacy controller
     *
     * Please use GUI controllers instead of legacy controllers
     *
     * Actual version of legacy_controllers whitelist is 2023-02-28 (334 controllers)
     * 2023-01-27 : 343 controllers
     *
     * @throws Exception
     */
    public function testRestrictLegacyControllersWithWhitelist(): void
    {
        $path_whitelist_legacy_controllers = dirname(__DIR__) . '/Resources/Controllers/Legacy/legacy_controllers.php';
        $whitelist_legacy_controllers      = include $path_whitelist_legacy_controllers;
        $actual_legacy_controllers         = CClassMap::getInstance()->getClassChildren(CLegacyController::class);

        // Check difference between actual and whitelisted legacy controllers
        $diff = array_diff($actual_legacy_controllers, $whitelist_legacy_controllers);

        if (is_array($diff) && (count($diff) > 0)) {
            $this->assertCount(
                0,
                $diff,
                sprintf(
                    "The following legacy controllers have been renamed or added, please remove them and use GUI controllers : %s ",
                    json_encode(array_values($diff), JSON_PRETTY_PRINT)
                )
            );
        }

        $this->assertLessThanOrEqual(count($whitelist_legacy_controllers), count($actual_legacy_controllers));
    }

    /**
     * Restrict adding legacy actions
     *
     * This test will pass on :
     * - Deleting legacy action
     *
     * And it will fail on :
     * - Adding legacy action
     * - Renaming legacy action
     *
     * Please use GUI controllers instead of legacy actions
     *
     * Actual version of legacy_actions whitelist is 2023-02-28 (2150 actions)
     * 2023-01-27 : 2193 actions
     *
     * @throws Exception
     */
    public function testRestrictLegacyActionsWithWhitelist(): void
    {
        $path_whitelist_legacy_actions = dirname(__DIR__) . '/Resources/Controllers/Legacy/legacy_actions.php';
        $whitelist_legacy_actions      = include $path_whitelist_legacy_actions;
        $actual_legacy_actions         = CClassMap::getInstance()->getLegacyActions();

        // Unset useless keys defined by unit tests...
        unset($actual_legacy_actions['core']);
        unset($actual_legacy_actions['dPpatients']['action_foo']);

        // Check difference between actual and whitelisted legacy actions
        $diff = CMbArray::arrayKeyRecursiveCompare($actual_legacy_actions, $whitelist_legacy_actions);

        if (isset($diff[0]) && is_array($diff[0])) {
            $this->assertCount(
                0,
                $diff[0],
                sprintf(
                    "The following legacy actions have been renamed or added, please remove them and use GUI controllers : %s ",
                    json_encode(array_values($diff[0]), JSON_PRETTY_PRINT)
                )
            );
        }

        $this->assertLessThanOrEqual(
            count($whitelist_legacy_actions, COUNT_RECURSIVE),
            count($actual_legacy_actions, COUNT_RECURSIVE)
        );
    }
}
