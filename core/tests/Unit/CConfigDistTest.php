<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Ox\Core\CMbArray;
use Ox\Core\CMbConfig;
use Ox\Core\Config\CConfigDist;
use Ox\Tests\OxUnitTestCase;

class CConfigDistTest extends OxUnitTestCase
{
    public function testBuild(): void
    {
        $config_dist = new CConfigDist();
        $msg         = $config_dist->build();
        $this->assertStringStartsWith('Generated config_dist file', $msg);
        $path = dirname(__DIR__, 3) . '/includes/config_dist.php';
        $this->assertFileExists($path);
    }

    /**
     * Restrict adding configuration to config_dist.php (config_core.php + modules/ * /config.php)
     *
     * This test will pass on :
     * - Deleting configuration
     * - Changing configuration values
     *
     * And it will fail on :
     * - Adding configuration
     * - Renaming configuration
     *
     * Please convert the config_dist configs to contextual configs
     * (convert modules/ * /config.php to modules/ * /classes/CConfiguration*.php)
     *
     * Actual version of config_dist whitelist is 2023-05-26 09:30:00 (1318 configs)
     * 2022-12-22 : 1710
     */
    public function testRestrictConfigDistWithWhitelist(): void
    {
        // Read whitelist configs file
        $path_configs_whitelist  = dirname(__DIR__) . '/Resources/Configs/config_dist_whitelist.php';
        $configs_whitelist       = include $path_configs_whitelist;
        $count_configs_whitelist = count($configs_whitelist, COUNT_RECURSIVE);

        // Keep old configs values and reset dPconfig
        $old_configs         = $GLOBALS['dPconfig'];
        $GLOBALS['dPconfig'] = [];

        // Read config_dist.php
        $path_config_dist = $old_configs['root_dir'] . DIRECTORY_SEPARATOR . CMbConfig::CONFIG_DIST_FILE;
        include $path_config_dist;
        $count_config_dist = count($GLOBALS['dPconfig'], COUNT_RECURSIVE);

        // Check difference between config_dist and whitelist
        $diff = CMbArray::arrayKeyRecursiveCompare($GLOBALS['dPconfig'], $configs_whitelist);

        // Restore old configs
        $GLOBALS['dPconfig'] = $old_configs;

        if (isset($diff[0]) && is_array($diff[0])) {
            $msg = sprintf(
                "The following configurations have been renamed or added to config_dist, please remove them: %s",
                json_encode($diff[0], JSON_PRETTY_PRINT)
            );

            $this->assertCount(0, $diff[0], $msg);
        }

        $this->assertLessThanOrEqual($count_configs_whitelist, $count_config_dist);
    }
}
