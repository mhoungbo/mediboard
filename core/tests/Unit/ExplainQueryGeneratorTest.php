<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Ox\Core\CSQLDataSource;
use Ox\Core\ExplainQueryGenerator;
use Ox\Tests\OxUnitTestCase;

class ExplainQueryGeneratorTest extends OxUnitTestCase
{
    /**
     * @dataProvider validQueryProvider
     *
     * @param string $query
     * @param int    $nb_queries
     *
     * @return void
     */
    public function testSupportedQueriesAreExplained(string $query, int $nb_queries = 1): void
    {
        $generator = new ExplainQueryGenerator();
        $explain   = $generator->explainQuery($query, 'std');

        $this->assertIsArray($explain);
        $this->assertNotEmpty($explain);
        $this->assertCount($nb_queries, $explain);

        foreach ($explain as $_explain) {
            $this->assertArrayHasKey('possible_keys', $_explain); // Should be enough
        }
    }

    /**
     * @dataProvider invalidQueryProvider
     *
     * @param string $query
     *
     * @return void
     */
    public function testUnsupportedQueriesAreNotExplained(string $query): void
    {
        $generator = new ExplainQueryGenerator();
        $explain   = $generator->explainQuery($query, 'std');

        $this->assertNull($explain);
    }

    /**
     * @dataProvider validQueryProvider
     *
     * @param string $query
     *
     * @return void
     */
    public function testTokenIsGenerated(string $query): void
    {
        $generator = $this->getMockBuilder(ExplainQueryGenerator::class)
                          ->onlyMethods(['buildToken'])
                          ->getMock();

        $generator->expects($this->once())->method('buildToken');

        $generator->addQuery($query, 'std');
        $generator->generateToken();
    }

    /**
     * @dataProvider invalidQueryProvider
     *
     * @param string $query
     *
     * @return void
     */
    public function testTokenIsNotGenerated(string $query): void
    {
        $generator = $this->getMockBuilder(ExplainQueryGenerator::class)
                          ->onlyMethods(['buildToken'])
                          ->getMock();

        $generator->expects($this->never())->method('buildToken');

        $generator->addQuery($query, 'std');
        $generator->generateToken();
    }

    /**
     * @dataProvider validQueryProvider
     *
     * @param string $query
     *
     * @return void
     */
    public function testReplayIsExecuted(string $query): void
    {
        $ds = $this->createMock(CSQLDataSource::class);
        $ds->method('loadList')->willReturn([1, 2, 3]);

        $generator = $this->getMockBuilder(ExplainQueryGenerator::class)
                          ->onlyMethods(['getDS'])
                          ->getMock();

        $generator->method('getDS')->willReturn($ds);

        $result = $generator->replayNormalizedQuery($generator->normalizeQuery($query, 'std'));

        $this->assertCount(2, $result);
        $this->assertIsFloat($result[0]);
        $this->assertEquals(3, $result[1]);
    }

    /**
     * @dataProvider validQueryProvider
     * @dataProvider invalidQueryProvider
     *
     * @param string $query
     *
     * @return void
     */
    public function testReplayWhenAnErrorOccurs(string $query): void
    {
        $ds = $this->createMock(CSQLDataSource::class);
        $ds->method('loadList')->willReturn(false);

        $generator = $this->getMockBuilder(ExplainQueryGenerator::class)
                          ->onlyMethods(['getDS'])
                          ->getMock();

        $generator->method('getDS')->willReturn($ds);

        $result = $generator->replayNormalizedQuery($generator->normalizeQuery($query, 'std'));

        $this->assertCount(2, $result);

        // [null, null] when query is not supported, but also when DS returns an error
        $this->assertNull($result[0]);
        $this->assertNull($result[1]);
    }

    public function validQueryProvider(): array
    {
        return [
            'simple select'         => ['SELECT * from `users`'],
            'select with join'      => [
                'SELECT * from `users` LEFT JOIN `users_mediboard` ON `users`.`user_id` = `users_mediboard`.`user_id`',
                2,
            ],
            'select with two joins' => [
                'SELECT * from `users` LEFT JOIN `users_mediboard` ON `users`.`user_id` = `users_mediboard`.`user_id` LEFT JOIN `functions_mediboard` ON `users_mediboard`.`function_id` = `functions_mediboard`.`function_id`',
                3,
            ],
            'select with sub-query' => [
                'SELECT * from `users` WHERE `user_id` IN (SELECT `user_id` FROM `users` WHERE `user_id` = 1);',
                2,
            ],
            'only some columns'     => ['SELECT `user_id` FROM `users`;', 1,],
        ];
    }

    public function invalidQueryProvider(): array
    {
        return [
            'insert' => ['INSERT INTO `users` VALUES()'],
            'delete' => ['DELETE `users` WHERE `user_id` = ?'],
            'update' => ['UPDATE `users` SET `user_id` = ? WHERE `user_id` = ?'],
            'show'   => ['SHOW TABLES'],
            'alter'  => ['ALTER TABLE `users` ADD COLUMN `test` VARCHAR(255);'],
        ];
    }
}
