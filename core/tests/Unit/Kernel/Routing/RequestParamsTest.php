<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Kernel\Routing;

use DateTimeImmutable;
use Exception;
use InvalidArgumentException;
use Monolog\Handler\TestHandler;
use Monolog\Logger;
use Ox\Core\CMbFieldSpecFact;
use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Routing\RequestParams;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class RequestParamsTest extends OxUnitTestCase
{
    public function testGetFromRequest(): void
    {
        $request = new Request(
            ['test' => 'query', 'test3' => 'request3'],
            ['test' => 'request', 'test2' => 'request2']
        );

        $stack = new RequestStack();
        $stack->push($request);

        $params = new RequestParams($stack, new Translator(), new Logger('test'));
        $this->assertEquals('query', $this->invokePrivateMethod($params, 'getFromRequest', 'get', 'test'));
        $this->assertEquals('request', $this->invokePrivateMethod($params, 'getFromRequest', 'post', 'test'));
        $this->assertEquals('query', $this->invokePrivateMethod($params, 'getFromRequest', 'request', 'test'));
        $this->assertEquals('request3', $this->invokePrivateMethod($params, 'getFromRequest', '', 'test3'));
        $this->assertEquals('request2', $this->invokePrivateMethod($params, 'getFromRequest', '', 'test2'));
        $this->assertNull($this->invokePrivateMethod($params, 'getFromRequest', 'get', 'test2'));
        $this->assertNull($this->invokePrivateMethod($params, 'getFromRequest', 'post', 'test3'));
    }

    /**
     * @dataProvider getFromRequestForbiddenParamProvider
     */
    public function testGetFromRequestForbiddenParam(RequestParams $params, string $param_name): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->invokePrivateMethod($params, 'getFromRequest', 'get', $param_name);
    }

    public function testValidateValueFromPropEmptyValue(): void
    {
        $stack = new RequestStack();
        $stack->push(new Request());
        $params = new RequestParams($stack, new Translator(), new Logger('test'));
        $this->assertEquals('', $this->invokePrivateMethod($params, 'validateValueFromProp', 'foo', '', 'str', 0));
        $this->assertNull($this->invokePrivateMethod($params, 'validateValueFromProp', 'foo', null, 'str', 0));
        $this->assertEquals(
            'default',
            $this->invokePrivateMethod($params, 'validateValueFromProp', 'foo', null, 'str default|default', 0)
        );
    }

    public function testValidateValueFromPropFailed(): void
    {
        $stack = new RequestStack();
        $stack->push(new Request());
        $params = new RequestParams($stack, new Translator(), new Logger('test'));

        $this->expectException(InvalidArgumentException::class);

        $this->invokePrivateMethod($params, 'validateValueFromProp', 'foo', 'bar', 'dateTime', 0);
    }

    public function testValidateValueFromPropOk(): void
    {
        $stack = new RequestStack();
        $stack->push(new Request());
        $params = new RequestParams($stack, new Translator(), new Logger('test'));

        $this->assertEquals(
            'bar',
            $this->invokePrivateMethod($params, 'validateValueFromProp', 'foo', 'bar', 'str default|test', 0)
        );
    }

    /**
     * @runInSeparateProcess
     */
    public function testValidateValueFromPropRefNoPerm(): void
    {
        $spec = $this->getMockBuilder(CRefSpec::class)
                     ->onlyMethods(['checkPermission', 'checkPropertyValue'])
                     ->setConstructorArgs(['stdClass', 'test'])
                     ->getMock();
        $spec->method('checkPermission')->willReturn(false);
        $spec->method('checkPropertyValue')->willReturn(null);

        CMbFieldSpecFact::$classes['foo'] = get_class($spec);

        $stack = new RequestStack();
        $stack->push(new Request());
        $params = new RequestParams($stack, new Translator(), new Logger('test'));

        $this->expectException(AccessDeniedException::class);

        $this->invokePrivateMethod($params, 'validateValueFromProp', 'foo', 'bar', 'foo class|CUser', 1);
    }

    public function testValidatevalueFromPropRefOk(): void
    {
        $current_user = CMediusers::get();

        $stack = new RequestStack();
        $stack->push(new Request());
        $params = new RequestParams($stack, new Translator(), new Logger('test'));

        $user_id = $this->invokePrivateMethod(
            $params,
            'validateValueFromProp',
            'foo',
            $current_user->_id,
            'ref class|CMediusers notNull',
            1
        );

        $this->assertEquals($current_user->_id, $user_id);
    }

    public function testGet(): void
    {
        $stack = new RequestStack();
        $stack->push(new Request(['test' => 'query'], ['test' => 'request']));
        $params = new RequestParams($stack, new Translator(), new Logger('test'));

        $this->assertEquals('query', $params->get('test', 'str'));
    }

    public function testPost(): void
    {
        $stack = new RequestStack();
        $stack->push(new Request(['test' => 'query'], ['test' => 'request']));
        $params = new RequestParams($stack, new Translator(), new Logger('test'));

        $this->assertEquals('request', $params->post('test', 'str'));
    }

    public function testRequest(): void
    {
        $stack = new RequestStack();
        $stack->push(new Request(['test' => 'query'], ['test' => 'request']));
        $params = new RequestParams($stack, new Translator(), new Logger('test'));

        $this->assertEquals('query', $params->request('test', 'str'));
    }

    public function testCheckParamPutLog(): void
    {
        $logger  = new Logger('test');
        $handler = new TestHandler();
        $logger->setHandlers([$handler]);

        $stack = new RequestStack();
        $stack->push(new Request(['test' => 'query'], ['test' => 'request'], [], [], [], ['APP_DEBUG' => true]));
        $params = new RequestParams($stack, new Translator(), $logger);
        $params->get('test', 'str');

        $this->assertTrue($handler->hasInfoThatContains("RequestParams check GET parameter \"test\" using"));
    }

    public function getFromRequestForbiddenParamProvider(): array
    {
        $stack = new RequestStack();
        $stack->push(new Request());
        $params = new RequestParams($stack, new Translator(), new Logger('test'));

        $return = [];
        foreach (RequestParams::FORBIDDEN_PARAMS as $param_name) {
            $return[$param_name] = [$params, $param_name];
        }

        return $return;
    }

    /**
     * @param string $request_value
     * @param mixed  $expected_value
     * @param string $prop
     *
     * @dataProvider typedValuesProvider
     *
     * @return void
     * @throws Exception
     */
    public function testTypedValues(string $request_value, mixed $expected_value, string $prop): void
    {
        $stack = new RequestStack();
        $stack->push(new Request(['test' => $request_value]));
        $params = new RequestParams($stack, new Translator(), new Logger('test'), true);

        $this->assertEquals($expected_value, $params->get('test', $prop));
    }

    public function typedValuesProvider(): array
    {
        return [
            ['string', 'string', 'str'],
            ['28', 28, 'num'],
            ['28', 28.0, 'float'],
            ['1', true, 'bool'],
            ['0', false, 'bool'],
            ['2023-01-01', new DateTimeImmutable('2023-01-01'), 'date'],
            ['2023-01-01 12:00:00', new DateTimeImmutable('2023-01-01 12:00:00'), 'dateTime'],
        ];
    }
}
