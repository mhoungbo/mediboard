<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Kernel\Routing;

use Ox\Core\CMbException;
use Ox\Core\Kernel\Exception\RouteException;
use Ox\Core\Kernel\Routing\RouteManager;
use Ox\Core\Tests\Unit\CComposerTest;
use Ox\Tests\OxUnitTestCase;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * TUs for the generation of the all_routes file and the manipulation of routes
 */
class RouteManagerTest extends OxUnitTestCase
{
    /**
     * @throws RouteException
     */
    public function setUp(): void
    {
        $manager = new RouteManager();
        if (!file_exists($manager->getAllRoutesPath())) {
            $manager->loadAllRoutes()->buildAllRoutes();
        }
    }

    public function testConstruct(): void
    {
        $manager = new RouteManager();
        $this->assertInstanceOf(RouteManager::class, $manager);
        $this->assertFileExists($manager->getRoot());
        $this->assertInstanceOf(RouteCollection::class, $manager->getRouteCollection());
    }

    /**
     * @throws RouteException
     */
    public function testloadAllRoutes(): void
    {
        $manager = new RouteManager();
        $routes  = $manager->loadAllRoutes(false)->getRouteCollection();
        $this->assertInstanceOf(RouteCollection::class, $routes);
        $this->assertNotEmpty($routes);
    }

    /**
     * @throws RouteException
     * @group schedules
     */
    public function testloadAllRoutesGlob(): void
    {
        $manager = new RouteManager();
        $routes  = $manager->loadAllRoutes(true)->getRouteCollection();
        $this->assertInstanceOf(RouteCollection::class, $routes);
        $this->assertNotEmpty($routes);
    }

    /**
     * @return void
     * @throws RouteException
     */
    public function testFilterRoutes(): void
    {
        $manager = new RouteManager();
        $manager->loadAllRoutes(false);
        $count_all = count($manager->getRouteCollection());
        $count_gui = count($manager->filterRoutesCollectionByPrefix('gui'));
        $count_api = count($manager->filterRoutesCollectionByPrefix('api'));

        // The "/" and "/token" routes
        $count_special_routes = 2;

        // Add one for the / route
        $this->assertEquals($count_all, $count_gui + $count_api + $count_special_routes);
    }

    /**
     * @throws RouteException
     */
    public function testBuildAllRoutes(): RouteManager
    {
        $manager = new RouteManager();
        $manager->loadAllRoutes(true);
        $msg = $manager->buildAllRoutes();
        $this->assertStringStartsWith('Generated routing file in', $msg);

        return $manager;
    }

    public function testGetRessources(): void
    {
        $manager    = new RouteManager();
        $ressources = $manager->getRessources();
        $this->assertNotEmpty($ressources);
    }

    public function testConvertRouteToArray(): void
    {
        $manager = new RouteManager();
        $route   = new Route('/api/lorem/ipsum', ['permission' => 'read', [], ['toto' => 'tata']]);
        $array   = $manager->convertRouteToArray('lorem_ipsum', $route);
        $this->assertArrayHasKey('defaults', $array['lorem_ipsum']);
        $this->assertArrayNotHasKey('options', $array['lorem_ipsum']);
    }

    /**
     * @param string $message the message excetpion
     * @param Route  $route   the route to check
     *
     * @throws ReflectionException
     * @dataProvider routes
     */
    public function testCheckRoute(string $message, Route $route, ?string $route_name): void
    {
        $manager         = new RouteManager();
        $reflexion_class = new ReflectionClass($manager);

        $property_routes_name = $reflexion_class->getProperty('routes_name');
        $property_routes_name->setAccessible(true);

        $property_routes_prefix = $reflexion_class->getProperty('routes_prefix');
        $property_routes_prefix->setAccessible(true);

        // specific case
        switch ($route_name) {
            case 'duplicate_name':
                $property_routes_name->setValue($manager, ['duplicate_name']);
                break;
            case 'duplicate_prefix':
                $property_routes_prefix->setValue($manager, ['dirname' => 'duplicate']);
                break;
            default:
                // reset previous pass
                $property_routes_name->setValue($manager, []);
                $property_routes_prefix->setValue($manager, []);
                break;
        }

        $method = new ReflectionMethod(RouteManager::class, 'checkRoute');
        $method->setAccessible(true);
        try {
            $retour = $method->invoke($manager, $route_name, $route);
            $this->assertTrue($retour);
        } catch (RouteException $exception) {
            $this->assertStringStartsWith($message, $exception->getMessage());
        }
    }

    /**
     * @return array
     */
    public function routes(): array
    {
        $path   = '/api/test/unit';
        $routes = [];

        $route        = new Route($path);
        $routes[]     = [
            "[duplicate_name] Duplicate route name",
            $route,
            "duplicate_name"
        ];

        $route        = new Route($path);
        $routes[]     = [
            "[php] Invalid route name, missing prefix",
            $route,
            'php'
        ];

        $route        = new Route($path);
        $routes[]     = [
            "[duplicate_prefix] Duplicate route prefix",
            $route,
            'duplicate_prefix'
        ];

        $route        = new Route($path);
        $routes[]     = [
            "[php_unit] Empty methods",
            $route,
            'php_unit'
        ];

        $route        = new Route($path);
        $route->setMethods('TOTO');
        $routes[] = [
            "[php_unit] Invalid method TOTO",
            $route,
            'php_unit'
        ];

        return $routes;
    }

    /**
     * @throws RouteException
     */
    public function testGetRouteByName(): void
    {
        $manager = new RouteManager();
        $manager->loadAllRoutes(false);
        $route = $manager->getRouteByName('admin_identicate');
        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals('/api/identicate', $route->getPath());
    }

    /**
     * @throws RouteException
     */
    public function testGetRouteByPath(): void
    {
        $manager = new RouteManager();
        $manager->loadAllRoutes(false);
        $routes = $manager->getRoutesByPath('/api/identicate');
        $this->assertInstanceOf(Route::class, $routes['admin_identicate']);
    }

    public function testGetAllRoutesPath(): void
    {
        $manager = new RouteManager();
        $path    = $manager->getAllRoutesPath();
        $this->assertFileExists($path);
    }

    /**
     * @throws CMbException
     */
    public function testCreateRouteFromRequest(): void
    {
        $manager = new RouteManager();
        $args    = [
            'path'           => null,
            'req_names'      => [],
            'route_name'     => 'lorem_ipsum',
            'controller'     => CComposerTest::class,
            'methods'        => [],
            'openapi'        => [],
            'accept'         => [],
            'description'    => [],
            'param_names'    => [],
            'content_type'   => [],
            'body_required'  => [],
            'response_names' => [],
            'security'       => null,
            'permission'     => null,
            'bulk'           => null,
        ];
        $route   = $manager->createRouteFromRequest($args);
        $this->assertInstanceOf(Route::class, $route);
    }
}
