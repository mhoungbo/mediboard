<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Kernel\Routing;

use Ox\Core\AppUiBridge;
use Ox\Core\Kernel\Routing\RequestContext;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Admin\Tests\Fixtures\UsersFixtures;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\Controllers\SystemController;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class RequestContextTest extends OxUnitTestCase
{
    private static ?CGroups $group = null;

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        self::$group?->delete();
    }

    /**
     * @dataProvider extractGroupIdProvider
     */
    public function testExtractGroupId(Request $request, AppUiBridge $app_ui, int $expected): void
    {
        $context = new RequestContext($request, $app_ui);
        $this->assertEquals($expected, $context->getGroupId());

        $this->assertEquals($expected, $request->getSession()->get('g'));
    }

    /**
     * @dataProvider extractFunctionIdProvider
     */
    public function testExtractFunctionId(Request $request, AppUiBridge $app_ui, int $expected): void
    {
        $context = new RequestContext($request, $app_ui);
        $this->assertEquals($expected, $context->getFunctionId());

        $this->assertEquals($expected, $request->getSession()->get('f'));
    }

    public function extractGroupIdProvider(): array
    {
        $group = $this->getObjectFromFixturesReference(CGroups::class, UsersFixtures::REF_FIXTURES_GROUP);

        $current_group = CGroups::loadCurrent();

        $app_ui = $this->getAppUIMock($current_group->_id);

        $request_get = new Request(
            ['g' => $group->_id],
            ['g' => $current_group->_id],
            ['_controller' => SystemController::class . '::about']
        );
        $session = new Session(new MockArraySessionStorage());
        $session->set('g', $current_group->_id);
        $request_get->setSession($session);

        $request_post = new Request([], ['g' => $group->_id], ['_controller' => SystemController::class . '::about']);
        $session = new Session(new MockArraySessionStorage());
        $session->set('g', $current_group->_id);
        $request_post->setSession($session);

        $request_session = new Request([], [], ['_controller' => SystemController::class . '::about']);
        $session = new Session(new MockArraySessionStorage());
        $session->set('g', $group->_id);
        $request_session->setSession($session);

        $request_app_ui = new Request([], [], ['_controller' => SystemController::class . '::about']);
        $request_app_ui->setSession(new Session(new MockArraySessionStorage()));

        $group_no_read = $this->createGroupWithtoutRead();

        $request_without_read = new Request(
            ['g' => $group_no_read->_id],
            [],
            ['_controller' => SystemController::class . '::about']
        );
        $request_without_read->setSession(new Session(new MockArraySessionStorage()));

        return [
            'extract_from_get'           => [$request_get, $app_ui, (int)$group->_id],
            'extract_from_post'          => [$request_get, $app_ui, (int)$group->_id],
            'extract_from_session'       => [$request_session, $app_ui, (int)$group->_id],
            'extract_from_app_ui'        => [$request_get, $this->getAppUIMock($group->_id), (int)$group->_id],
            'extract_group_without_read' => [
                $request_without_read,
                $this->getAppUIMock($group->_id),
                (int)$group->_id,
            ],
        ];
    }

    public function extractFunctionIdProvider(): array
    {
        $function = $this->getObjectFromFixturesReference(CFunctions::class, UsersFixtures::REF_FIXTURES_FUNCTION);

        $current_function = CFunctions::getCurrent();

        $app_ui = $this->getAppUIMock(1, $current_function->_id);

        $request_get = new Request(
            ['f' => $function->_id],
            ['f' => $current_function->_id],
            ['_controller' => SystemController::class . '::about']
        );
        $session = new Session(new MockArraySessionStorage());
        $session->set('f', $current_function->_id);
        $request_get->setSession($session);

        $request_post = new Request([], ['f' => $function->_id], ['_controller' => SystemController::class . '::about']);
        $session = new Session(new MockArraySessionStorage());
        $session->set('f', $current_function->_id);
        $request_post->setSession($session);

        $session_bag = new AttributeBag();
        $session_bag->set('f', $function->_id);
        $request_session = new Request([], [], ['_controller' => SystemController::class . '::about']);
        $session = new Session(new MockArraySessionStorage());
        $session->set('f', $function->_id);
        $request_session->setSession($session);

        $request_app_ui = new Request([], [], ['_controller' => SystemController::class . '::about']);
        $request_app_ui->setSession(new Session(new MockArraySessionStorage()));

        return [
            'extract_from_get'           => [$request_get, $app_ui, (int)$function->_id],
            'extract_from_post'          => [$request_get, $app_ui, (int)$function->_id],
            'extract_from_session'       => [$request_session, $app_ui, (int)$function->_id],
            'extract_from_app_ui'        => [$request_get, $this->getAppUIMock(1, $function->_id), (int)$function->_id],
        ];
    }

    private function getAppUIMock(int $group_id, ?int $function_id = null): AppUiBridge
    {
        $app = $this->getMockBuilder(AppUiBridge::class)
                    ->onlyMethods(['getCurrentUserGroup', 'getCurrentUserFunction'])
                    ->getMock();
        $app->method('getCurrentUserGroup')->willReturn($group_id);
        $app->method('getCurrentUserFunction')->willReturn($function_id);

        return $app;
    }

    private function createGroupWithtoutRead(): CGroups
    {
        $group = CGroups::getSampleObject();
        $this->storeOrFailed($group);

        $user = CMediusers::get();
        CPermObject::$users_cache[$user->_id]['CGroups'][$group->_id] = PERM_DENY;

        return self::$group = $group;
    }
}
