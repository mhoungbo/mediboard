<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Kernel\Services;

use Ox\Core\Kernel\Services\CsrfTokenDomManipulator;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CsrfTokenDomManipulatorTest extends OxUnitTestCase
{
    /**
     * @dataProvider addCsrfTokensProvider
     */
    public function testAddCsrfTokens(string $content, ?string $expected, CsrfTokenManagerInterface $manager): void
    {
        $service = new CsrfTokenDomManipulator($manager);
        $this->assertEquals($expected, $service->addCsrfTokens($content));
    }

    public function addCsrfTokensProvider(): array
    {
        $token_manager = $this->getMockBuilder(CsrfTokenManagerInterface::class)->getMock();
        $token_manager->method('getToken')->willReturn('fooBar');

        return [
            'empty_content'    => ['', null, $token_manager],
            'not_html_content' => ['foo bar', null, $token_manager],
            'post_form_without_token' => [
                '<form name="foo" method="post"></form>',
                '<html><body><form name="foo" method="post"><input name="_form" type="hidden" value="foo"><input name="_token" type="hidden" value="fooBar"></form></body></html>' . "\n",
                $token_manager
            ],
            'get_form_without_token' => [
                '<form name="foo" method="get"></form>',
                null,
                $token_manager
            ],
            'get_post_with_token' => [
                '<form name="fooGet" method="get"></form>'
                . '<form name="fooPost" method="post"></form>'
                . '<form name="foo@token" method="post"><input type="hidden" name="@token" value="tokenized"/></form>'
                . '<form name="fooToken" method="post"><input type="hidden" name="token" value="tokenized"/></form>',
                '<html><body><form name="fooGet" method="get"></form>'
                . '<form name="fooPost" method="post"><input name="_form" type="hidden" value="fooPost"><input name="_token" type="hidden" value="fooBar"></form>'
                . '<form name="foo@token" method="post"><input type="hidden" name="@token" value="tokenized"></form>'
                . '<form name="fooToken" method="post"><input type="hidden" name="token" value="tokenized"></form></body></html>' . "\n",
                $token_manager
            ],
        ];
    }
}
