<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\FileUtil;

use Ox\Core\Cache;
use Ox\Core\FileUtil\DirectoryAccessChecker;
use Ox\Tests\OxUnitTestCase;
use Psr\SimpleCache\CacheInterface;

class DirectoryAccessCheckerTest extends OxUnitTestCase
{
    public function testIsDirectoryAccessibleWithoutTimeout(): void
    {
        $access = $this->getMockBuilder(DirectoryAccessChecker::class)
            ->onlyMethods(['executeCommand'])
            ->setConstructorArgs([$this->getCache()])
            ->getMock();
        $access->method('executeCommand')->willReturn(127);

        $this->assertTrue($access->isDirectoryAccessible('/foo/bar', 10));
    }

    public function testIsDirectoryAccessibleWithTimeoutReached(): void
    {
        $access = $this->getMockBuilder(DirectoryAccessChecker::class)
                       ->onlyMethods(['executeCommand'])
                       ->setConstructorArgs([$this->getCache()])
                       ->getMock();
        $access->method('executeCommand')->willReturn(0, 124);

        $this->assertFalse($access->isDirectoryAccessible('/foo/bar', 10));
    }

    public function testIsDirectoryAccessibleOk(): void
    {
        $access = $this->getMockBuilder(DirectoryAccessChecker::class)
                       ->onlyMethods(['executeCommand'])
                       ->setConstructorArgs([$this->getCache()])
                       ->getMock();
        $access->method('executeCommand')->willReturn(0);

        $this->assertTrue($access->isDirectoryAccessible('/foo/bar', 10));
    }

    private function getCache(): CacheInterface
    {
        return Cache::getCache(Cache::NONE);
    }
}
