<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Module\Requirements;

use Ox\Tests\OxUnitTestCase;

class CRequirementsDescriptionTest extends OxUnitTestCase
{
    public function testHasDescription(): void
    {
        $requirements_test = new CRequirementsDummy();

        $description = $requirements_test->getDescription();

        $this->assertTrue($description->hasDescription());
    }

    public function testContentDescription(): void
    {
        $requirements_test = new CRequirementsDummy();
        $description       = $requirements_test->getDescription();
        $text              = "# title # \n*** \ntest \n* test list \n* test list 2 \n";
        $this->assertEquals($text, $description->render());
    }

    public function testSetDescription(): void
    {
        $requirements_test = new CRequirementsDummy();
        $description       = $requirements_test->getDescription();
        $text              = "# title";
        $description->setDescription($text);
        $this->assertEquals($text, $description->render());
    }
}

