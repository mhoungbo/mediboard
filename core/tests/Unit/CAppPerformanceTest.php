<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Ox\Core\CApp;
use Ox\Core\CAppPerformance;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\HttpFoundation\Response;

class CAppPerformanceTest extends OxUnitTestCase
{
    public function testPreparePerformance(): void
    {
        CApp::$chrono->total           = 10;
        CStoredObject::$cachableCounts = ['CUser' => 5, 'CMediusers' => 10];
        CStoredObject::$objectCounts   = ['CUser' => 20, 'CMediusers' => 100];

        $std                       = CSQLDataSource::get('std');
        $std->chrono->total        = 20;
        $std->chrono->nbSteps      = 5;
        $std->chronoFetch->total   = 10;
        $std->chronoFetch->nbSteps = 100;
        $std->latency              = 20;
        $std->connection_time      = 5;
        $sae                       = CSQLDataSource::get('sae');
        $sae->chrono->total        = 10;
        $sae->chrono->nbSteps      = 1;
        $sae->chronoFetch->total   = 5;
        $sae->chronoFetch->nbSteps = 2;
        $sae->latency              = 200;
        $sae->connection_time      = 10;

        CSQLDataSource::$dataSources = [
            'std' => $std,
            'sae' => $sae,
            'foo' => null,
            'bar' => 0,
        ];


        $app = new CAppPerformance();
        $app->preparePerformance(new Response());

        $data = $app->toArray();

        $this->assertEquals(10, $data['genere']);
        $this->assertArrayHasKey('memoire', $data);
        $this->assertEquals(120, $data['objets']);
        $this->assertEquals(['CMediusers' => 100, 'CUser' => 20], $data['objectCounts']);
        $this->assertEquals(15, $data['cachableCount']);
        $this->assertEquals(['CMediusers' => 10, 'CUser' => 5], $data['cachableCounts']);
        $this->assertArrayHasKey('ip', $data);
        $this->assertArrayHasKey('size', $data);
        $this->assertArrayHasKey('totals', $data['cache']);
        $this->assertArrayHasKey('total', $data['cache']);
        $this->assertArrayHasKey('enslaved', $data);
        $this->assertEquals(
            [
                'std' => [
                    'latency'    => 20,
                    'ct'         => 5,
                    'count'      => 5,
                    'time'       => 20,
                    'countFetch' => 100,
                    'timeFetch'  => 10,
                ],
                'sae' => [
                    'latency'    => 200,
                    'ct'         => 10,
                    'count'      => 1,
                    'time'       => 10,
                    'countFetch' => 2,
                    'timeFetch'  => 5,
                ],
            ],
            $data['dataSources']
        );
        $this->assertEquals(45, $data['dataSourceTime']);
        $this->assertEquals(6, $data['dataSourceCount']);
    }
}
