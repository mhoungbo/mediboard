<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Ox\Core\CSQLDataSource;
use Ox\Core\CView;
use Ox\Core\Kernel\Exception\ControllerStoppedException;
use Ox\Mediboard\System\Controllers\SystemController;
use Ox\Tests\OxUnitTestCase;

class CViewTest extends OxUnitTestCase
{
    /**
     * @config enslaving_active 0
     */
    public function testEnforceSlaveNotActive(): void
    {
        $this->assertFalse(CSQLDataSource::isSlaveState());
        CView::enforceSlave();
        $this->assertFalse(CSQLDataSource::isSlaveState());
    }

    /**
     * @config db slave dbhost 0
     */
    public function testEnforceSlaveNoSlave(): void
    {
        $this->assertFalse(CSQLDataSource::isSlaveState());
        CView::enforceSlave();
        $this->assertFalse(CSQLDataSource::isSlaveState());
    }

    /**
     * @config enslaving_active 1
     */
    public function testEnforceSlave(): void
    {
        $this->assertFalse(CSQLDataSource::isSlaveState());
        CView::enforceSlave();
        $this->assertTrue(CSQLDataSource::isSlaveState());
    }

    /**
     * @depends testEnforceSlave
     */
    public function testDisableSlave(): void
    {
        CView::disableSlave();

        $this->assertFalse(CSQLDataSource::isSlaveState());
    }

    /**
     * @config db slave dbname foo
     * @config enslaving_active 1
     *
     * @runInSeparateProcess
     */
    public function testEnforceSlaveDieError(): void
    {
        unset(CSQLDataSource::$dataSources['slave']);

        // Exception thrown is ControllerStoppedException from CAppUI::stepAjax
        $this->expectException(ControllerStoppedException::class);
        CView::enforceSlave();
    }

    /**
     * @config db slave dbname foo
     * @config enslaving_active 1
     *
     * @runInSeparateProcess
     */
    public function testEnforceSlaveNoDieError(): void
    {
        $controller = new SystemController();

        unset(CSQLDataSource::$dataSources['slave']);

        CView::enforceSlave(false);

        $this->assertFalse(CSQLDataSource::isSlaveState());
    }
}
