<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit;

use Ox\Core\CAppUI;
use Ox\Core\Kernel\Exception\ControllerStoppedException;
use Ox\Tests\OxUnitTestCase;

class CAppUITest extends OxUnitTestCase
{
    public function testSetMsg(): void
    {
        // Reset msg
        CAppUI::getMsg();

        $msg  = uniqid();
        $msg2 = uniqid();

        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_OK][$msg]));
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_WARNING][$msg]));
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_ERROR][$msg]));

        CAppUI::setMsg($msg, UI_MSG_OK);

        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_WARNING][$msg]));
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_ERROR][$msg]));

        CAppUI::setMsg($msg, UI_MSG_WARNING);
        CAppUI::setMsg($msg, UI_MSG_OK);

        $this->assertEquals(2, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_WARNING][$msg]);
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_ERROR][$msg]));

        CAppUI::setMsg($msg2, UI_MSG_WARNING);
        CAppUI::setMsg($msg, UI_MSG_OK);
        CAppUI::setMsg($msg, UI_MSG_ERROR);

        $this->assertEquals(3, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_WARNING][$msg]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_WARNING][$msg2]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_ERROR][$msg]);
    }

    /**
     * @config ui_locale_warn 0
     */
    public function testDisplayMsg(): void
    {
        // Reset msg
        CAppUI::getMsg();

        $msg = uniqid();
        $action = uniqid();

        $msg_action = "$action: $msg";

        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_OK][$msg]));
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_ERROR][$msg_action]));

        CAppUI::displayMsg('', $msg);

        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_ERROR][$msg_action]));

        CAppUI::displayMsg($msg, $action);

        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_ERROR][$msg_action]);

        CAppUI::displayMsg('', $msg);
        CAppUI::displayMsg($msg, $action);

        $this->assertEquals(2, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertEquals(2, CAppUI::$instance->messages[UI_MSG_ERROR][$msg_action]);
    }

    public function testDisplayAjaxMsg(): void
    {
        // Reset scripts
        CAppUI::$instance->scripts = [];

        $msg = uniqid();

        CAppUI::displayAjaxMsg($msg, UI_MSG_OK);
        CAppUI::displayAjaxMsg($msg, UI_MSG_WARNING);
        CAppUI::displayAjaxMsg($msg, UI_MSG_ERROR);
        CAppUI::displayAjaxMsg($msg, UI_MSG_OK);

        $this->assertStringContainsString("<div class='info'>$msg<\/div>", CAppUI::$instance->scripts[0]);
        $this->assertStringContainsString("<div class='warning'>$msg<\/div>", CAppUI::$instance->scripts[1]);
        $this->assertStringContainsString("<div class='error'>$msg<\/div>", CAppUI::$instance->scripts[2]);
        $this->assertStringContainsString("<div class='info'>$msg<\/div>", CAppUI::$instance->scripts[3]);
    }

    public function testStepMessage(): void
    {
        // Reset msg
        CAppUI::getMsg();

        $msg  = uniqid();
        $msg2 = uniqid();

        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_OK][$msg]));
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_WARNING][$msg]));
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_ERROR][$msg]));

        CAppUI::stepMessage(UI_MSG_OK, $msg);

        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_WARNING][$msg]));
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_ERROR][$msg]));

        CAppUI::stepMessage(UI_MSG_WARNING, $msg);
        CAppUI::stepMessage(UI_MSG_OK, $msg);

        $this->assertEquals(2, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_WARNING][$msg]);
        $this->assertFalse(isset(CAppUI::$instance->messages[UI_MSG_ERROR][$msg]));

        CAppUI::stepMessage(UI_MSG_WARNING, $msg2);
        CAppUI::stepMessage(UI_MSG_OK, $msg);
        CAppUI::stepMessage(UI_MSG_ERROR, $msg);

        $this->assertEquals(3, CAppUI::$instance->messages[UI_MSG_OK][$msg]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_WARNING][$msg]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_WARNING][$msg2]);
        $this->assertEquals(1, CAppUI::$instance->messages[UI_MSG_ERROR][$msg]);
    }

    public function testStepAjax(): void
    {
        $this->expectException(ControllerStoppedException::class);
        CAppUI::stepAjax('foo', UI_MSG_ERROR);
    }

    public function testGetMsg(): void
    {
        // Reset msg
        CAppUI::getMsg();

        CAppUI::setMsg('ok', UI_MSG_OK);
        CAppUI::setMsg('ok', UI_MSG_OK);
        CAppUI::setMsg('warning', UI_MSG_WARNING);
        CAppUI::setMsg('error', UI_MSG_ERROR);

        $this->assertEquals(
            "<div class=\"info\">ok x 2</div><div class=\"warning\">warning</div><div class=\"error\">error</div>",
            CAppUI::getMsg()
        );
    }
}
