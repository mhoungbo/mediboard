<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Api\Transformers;

use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Api\Transformers\ModelObjectTransformer;
use Ox\Core\CMbFieldSpec;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CUserLog;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\RouterTest;
use stdClass;

/**
 * Test the conversion of types.
 */
class ModelObjectTransformerTest extends OxUnitTestCase
{
    /**
     * @param mixed $value
     * @param mixed $expected_value
     *
     * @dataProvider convertTypeProvider
     */
    public function testConvertType($value, string $spec, $expected_value): void
    {
        $transformer = new ModelObjectTransformer(new Item([]));
        // Use assertTrue to allow type comparison
        $this->assertTrue(
            $expected_value === $this->invokePrivateMethod($transformer, 'convertType', $value, $spec)
        );
    }

    /**
     * @param mixed $resource
     *
     * @dataProvider isValidResourceProvider
     */
    public function testIsValidResource($resource, bool $valid): void
    {
        $transformer = new ModelObjectTransformer(new Item([]));
        if ($valid) {
            $this->assertTrue($this->invokePrivateMethod($transformer, 'isValidResource', $resource));
        } else {
            $this->assertFalse($this->invokePrivateMethod($transformer, 'isValidResource', $resource));
        }
    }

    /**
     * @dataProvider addSpecialLinksProvider
     */
    public function testAddSpecialLinks(Item $item, array $links_presents, array $links_no_presents): void
    {
        $transformer = new ModelObjectTransformer($item);
        $data        = $transformer->createDatas();

        if (empty($links_presents)) {
            $this->assertArrayNotHasKey('links', $data);

            return;
        }

        foreach ($links_presents as $link) {
            $this->assertArrayHasKey($link, $data['links']);
        }

        foreach ($links_no_presents as $link) {
            $this->assertArrayNotHasKey($link, $data['links']);
        }
    }

    public function convertTypeProvider(): array
    {
        return [
            'convert_type_string'      => ['toto', CMbFieldSpec::PHP_TYPE_STRING, 'toto'],
            'convert_type_int'         => ['1234titi', CMbFieldSpec::PHP_TYPE_INT, 1234],
            'convert_type_float'       => ['1234.155', CMbFieldSpec::PHP_TYPE_FLOAT, 1234.155],
            'convert_type_bool_true'   => [1, CMbFieldSpec::PHP_TYPE_BOOL, true],
            'convert_type_bool_false'  => [0, CMbFieldSpec::PHP_TYPE_BOOL, false],
            'convert_type_bool_string' => ['false', CMbFieldSpec::PHP_TYPE_BOOL, true],
            'convert_null_value'       => [null, CMbFieldSpec::PHP_TYPE_BOOL, null],
        ];
    }

    public function isValidResourceProvider(): array
    {
        return [
            'null'            => [null, true],
            'empty_array'     => [[], true],
            'item'            => [new Item([]), true],
            'collection'      => [new Collection([]), true],
            'non_empty_array' => ['foo', false],
            '0'               => [0, false],
            'string null'     => ['null', false],
            'other_object'    => [new stdClass(), false],
        ];
    }

    public function addSpecialLinksProvider(): array
    {
        $router = RouterTest::getInstance();

        $current_user = CMediusers::get();

        $no_router = new Item($current_user);

        $stored_object = new Item(CModule::getActive('system'));
        $stored_object->setRouter($router);

        $log               = new CUserLog();
        $log->_id          = 1;
        $log->object_class = $current_user->_class;
        $log->object_id    = $current_user->_id;

        $not_loggable = new Item($log);
        $not_loggable->setRouter($router);

        $cmb_object = new Item(CMediusers::get());
        $cmb_object->setRouter($router);

        return [
            'no_router'     => [$no_router, [], []],
            'stored_object' => [$stored_object, ['self', 'schema', 'history', 'identifiers'], ['notes']],
            'not_loggable'  => [$not_loggable, ['self', 'schema', 'identifiers'], ['notes', 'history']],
            'cmb_object'    => [$cmb_object, ['self', 'schema', 'notes', 'history', 'identifiers'], []],
        ];
    }
}
