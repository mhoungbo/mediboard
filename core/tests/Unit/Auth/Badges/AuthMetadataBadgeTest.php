<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Badges;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\OAuth2\OIDC\TokenSet;
use Ox\Tests\OxUnitTestCase;

class AuthMetadataBadgeTest extends OxUnitTestCase
{
    public function testDefaultValues(): void
    {
        $badge = new AuthMetadataBadge('test');

        $this->assertEquals('test', $badge->getAuth());
        $this->assertFalse($badge->isIncrementAttempts());
        $this->assertFalse($badge->isResetLoginAttempts());
        $this->assertFalse($badge->isStateful());
        $this->assertFalse($badge->isWeakPassword());
        $this->assertNull($badge->getOIDCTokens());
    }

    /**
     * @depends testDefaultValues
     *
     * @return void
     */
    public function testMutators(): void
    {
        $badge = new AuthMetadataBadge('test');
        $badge->setAuthMethod('mutated test');
        $badge->setIncrementAttempts(true);
        $badge->setResetLoginAttempts(true);
        $badge->setStateful(true);
        $badge->setWeakPassword(true);

        $tokens = new TokenSet('', '', '', '');
        $badge->setOIDCTokens($tokens);

        $this->assertEquals('mutated test', $badge->getAuth());
        $this->assertTrue($badge->isIncrementAttempts());
        $this->assertTrue($badge->isResetLoginAttempts());
        $this->assertTrue($badge->isStateful());
        $this->assertTrue($badge->isWeakPassword());
        $this->assertSame($tokens, $badge->getOIDCTokens());
    }

    public function testBadgeIsAlwaysResolved(): void
    {
        $badge = new AuthMetadataBadge('test');
        $this->assertTrue($badge->isResolved());
    }
}
