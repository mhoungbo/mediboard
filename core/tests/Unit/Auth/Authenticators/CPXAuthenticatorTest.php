<?php

namespace Ox\Core\Tests\Unit\Auth\Authenticators;

use Ox\Core\Auth\Authenticators\CPXAuthenticator;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CPXAuthenticatorTest extends AbstractAuthenticatorTest
{
    protected function getNormalAuthenticator(bool $is_already_authenticated = false): CPXAuthenticator
    {
        $security = $this->createMock(Security::class);
        $security->method('isGranted')->willReturnMap(
            [
                ['IS_AUTHENTICATED_FULLY', null, $is_already_authenticated],
            ]
        );

        // Manually adding loadUserByIdentifier because of it does not exist per se.
        $provider = $this->getMockBuilder(UserProviderInterface::class)
                         ->addMethods(['loadUserByIdentifier'])
                         ->onlyMethods(['loadUserByUsername', 'refreshUser', 'supportsClass'])
                         ->getMock();

        return new CPXAuthenticator(
            $provider,
            $this->createMock(CredentialsCheckerInterface::class),
            $security
        );
    }

    public function supportedRequestsProvider(): array
    {
        return [
            'request with signature and certificate signature' => [$this->getValidRequest()],
        ];
    }

    public function unsupportedRequestsProvider(): array
    {
        $sign_wo_cert = new Request();
        $sign_wo_cert->request->set('signature', 'signature');

        $cert_wo_sign = new Request();
        $cert_wo_sign->request->set('certificat_signature', 'cert_signature');

        $sign_and_cert = new Request();
        $sign_and_cert->query->set('signature', 'signature');
        $sign_and_cert->query->set('certificat_signature', 'cert_signature');

        $public = $this->getValidRequest();
        $public->attributes->set('public', true);

        return [
            'empty request'                                                           => [new Request()],
            'request with signature, but without certificate signature'               => [$sign_wo_cert],
            'request with certificate signature, but without signature'               => [$cert_wo_sign],
            'request with signature and certificate signature, but not in parameters' => [$sign_and_cert],
            'valid request, but public'                                               => [$public],
        ];
    }

    private function getValidRequest(): Request
    {
        $request = new Request();
        $request->request->set('signature', 'signature');
        $request->request->set('certificat_signature', 'cert_signature');

        return $request;
    }

    public function validRequestsProvider(): array
    {
        return [
            [
                $this->getValidRequest(),
            ],
        ];
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testAuthenticateDoesNotFail(Request $request): void
    {
        $this->expectNotToPerformAssertions();

        $auth = $this->getNormalAuthenticator();
        // AuthenticatorInterface::supports must be called first (setting the $request property).
        $auth->supports($request);

        $auth->authenticate($request);
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testIsInteractive(Request $request): void
    {
        $auth = $this->getNormalAuthenticator();
        $auth->supports($request);
        $auth->authenticate($request);

        $this->assertTrue($auth->isInteractive());
    }
}
