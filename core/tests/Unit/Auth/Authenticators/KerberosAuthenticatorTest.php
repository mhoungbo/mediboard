<?php

namespace Ox\Core\Tests\Unit\Auth\Authenticators;

use Ox\Core\Auth\Authenticators\KerberosAuthenticator;
use Ox\Core\Config\Conf;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class KerberosAuthenticatorTest extends AbstractAuthenticatorTest
{
    protected function getNormalAuthenticator(bool $is_already_authenticated = false): KerberosAuthenticator
    {
        return $this->getAuthenticator($is_already_authenticated, true);
    }

    private function getAuthenticator(bool $is_already_authenticated, bool $enabled): KerberosAuthenticator
    {
        $security = $this->createMock(Security::class);
        $security->method('isGranted')->willReturnMap(
            [
                ['IS_AUTHENTICATED_FULLY', null, $is_already_authenticated],
            ]
        );

        // Manually adding loadUserByIdentifier because of it does not exist per se.
        $provider = $this->getMockBuilder(UserProviderInterface::class)
                         ->addMethods(['loadUserByIdentifier'])
                         ->onlyMethods(['loadUserByUsername', 'refreshUser', 'supportsClass'])
                         ->getMock();

        $conf = $this->createMock(Conf::class);
        $conf->method('get')->willReturnMap(
            [
                ['admin CKerberosLdapIdentifier enable_kerberos_authentication', $enabled],
            ]
        );

        return new KerberosAuthenticator($provider, $security, $conf);
    }

    public function supportedRequestsProvider(): array
    {
        return [
            'request with config, auth type and remote user' => [$this->getValidRequest()],
        ];
    }

    public function unsupportedRequestsProvider(): array
    {
        $wo_remote_user = new Request();
        $wo_remote_user->server->set('AUTH_TYPE', 'Negotiate');

        $wo_auth_type = new Request();
        $wo_auth_type->server->set('REMOTE_USER', 'identifier');

        $not_in_server = new Request();
        $not_in_server->request->set('AUTH_TYPE', 'Negotiate');
        $not_in_server->request->set('REMOTE_USER', 'identifier');

        $invalid_auth_type = new Request();
        $invalid_auth_type->server->set('AUTH_TYPE', 'Not Negotiate');

        $public = $this->getValidRequest();
        $public->attributes->set('public', true);

        return [
            'empty request'                                             => [new Request()],
            'request with invalid auth type,'                           => [$invalid_auth_type],
            'request with auth type, but without remote user'           => [$wo_remote_user],
            'request with remote user signature, but without auth type' => [$wo_auth_type],
            'request with auth type and remote user, but not in server' => [$not_in_server],
            'valid request, but public'                                 => [$public],
        ];
    }

    private function getValidRequest(): Request
    {
        $request = new Request();
        $request->server->set('AUTH_TYPE', 'Negotiate');
        $request->server->set('REMOTE_USER', 'identifier');

        return $request;
    }

    public function validRequestsProvider(): array
    {
        return [
            [
                $this->getValidRequest(),
            ],
        ];
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testDoesNotSupportRequestIfDisabled(Request $request): void
    {
        $auth = $this->getAuthenticator(false, false);
        $this->assertFalse($auth->supports($request));
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testAuthenticateDoesNotFail(Request $request): void
    {
        $this->expectNotToPerformAssertions();

        $auth = $this->getNormalAuthenticator();
        // AuthenticatorInterface::supports must be called first (setting the $request property).
        $auth->supports($request);

        $auth->authenticate($request);
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testIsInteractive(Request $request): void
    {
        $auth = $this->getNormalAuthenticator();
        $auth->supports($request);
        $auth->authenticate($request);

        $this->assertTrue($auth->isInteractive());
    }
}
