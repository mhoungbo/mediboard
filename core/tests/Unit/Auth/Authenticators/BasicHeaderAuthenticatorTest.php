<?php

namespace Ox\Core\Tests\Unit\Auth\Authenticators;

use Ox\Core\Auth\Authenticators\BasicHeaderAuthenticator;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class BasicHeaderAuthenticatorTest extends AbstractAuthenticatorTest
{
    private const B64_HEADER = 'Basic dGVzdDp0ZXN0'; // test:test

    protected function getNormalAuthenticator(bool $is_already_authenticated = false): BasicHeaderAuthenticator
    {
        $security = $this->createMock(Security::class);
        $security->method('isGranted')->willReturnMap(
            [
                ['IS_AUTHENTICATED_FULLY', null, $is_already_authenticated],
            ]
        );

        // Manually adding loadUserByIdentifier because of it does not exist per se.
        $provider = $this->getMockBuilder(UserProviderInterface::class)
                         ->addMethods(['loadUserByIdentifier'])
                         ->onlyMethods(['loadUserByUsername', 'refreshUser', 'supportsClass'])
                         ->getMock();

        return new BasicHeaderAuthenticator(
            $provider,
            $this->createMock(CredentialsCheckerInterface::class),
            $security
        );
    }

    public function supportedRequestsProvider(): array
    {
        return [
            'request with basic header' => [$this->getValidRequest()],
        ];
    }

    public function unsupportedRequestsProvider(): array
    {
        $public = $this->getValidRequest();
        $public->attributes->set('public', true);

        return [
            'request without basic header'              => [new Request()],
            'request with basic header, but null'       => [$this->getValidRequest(null)],
            'request with basic header, but empty'      => [$this->getValidRequest('')],
            'request with basic header, but not basic'  => [$this->getValidRequest('not basic')],
            'request with basic header, but not base64' => [$this->getValidRequest('Basic test@')],
            'request with basic header, but public'     => [$public],
        ];
    }

    private function getValidRequest(?string $value = self::B64_HEADER): Request
    {
        $request = new Request();
        $request->headers->set('Authorization', $value);

        return $request;
    }

    public function validRequestsProvider(): array
    {
        return [
            [
                $this->getValidRequest(),
            ],
        ];
    }

    public function testInvalidCredentialsFormatThrowsAnException(): void
    {
        $request = $this->getValidRequest('Basic ' . base64_encode('test:'));

        $auth = $this->getNormalAuthenticator();
        $this->assertTrue($auth->supports($request));

        $this->expectException(AuthenticationException::class);
        $auth->authenticate($request);
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testAuthenticateDoesNotFail(Request $request): void
    {
        $this->expectNotToPerformAssertions();

        $auth = $this->getNormalAuthenticator();
        // AuthenticatorInterface::supports must be called first (setting the $request property).
        $auth->supports($request);

        $auth->authenticate($request);
    }
}
