<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Authenticators;

use Ox\Tests\OxUnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;

abstract class AbstractAuthenticatorTest extends OxUnitTestCase
{
    abstract protected function getNormalAuthenticator(bool $is_already_authenticated = false): AuthenticatorInterface;

    abstract public function supportedRequestsProvider(): array;

    abstract public function unsupportedRequestsProvider(): array;

    abstract public function validRequestsProvider(): array;

    /**
     * @dataProvider supportedRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testSupportsRequests(Request $request): void
    {
        $authenticator = $this->getNormalAuthenticator();
        $this->assertTrue($authenticator->supports($request));
    }

    /**
     * @depends      testSupportsRequests
     * @dataProvider unsupportedRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testDoesNotSupportRequests(Request $request): void
    {
        $authenticator = $this->getNormalAuthenticator();
        $this->assertFalse($authenticator->supports($request));
    }

    /**
     * @depends      testSupportsRequests
     * @dataProvider supportedRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testDoesNotSupportRequestIfAlreadyAuthenticated(Request $request): void
    {
        $authenticator = $this->getNormalAuthenticator(true);
        $this->assertFalse($authenticator->supports($request));
    }

    /**
     * @return void
     */
    public function testOnAuthenticationSuccessReturnsNull(): void
    {
        $auth = $this->getNormalAuthenticator();

        $this->assertNull(
            $auth->onAuthenticationSuccess(new Request(), $this->createMock(TokenInterface::class), 'test')
        );
    }

    /**
     * @return void
     */
    public function testOnAuthenticationFailure(): void
    {
        $auth = $this->getNormalAuthenticator();

        $this->assertNull($auth->onAuthenticationFailure(new Request(), new AuthenticationException()));
    }
}
