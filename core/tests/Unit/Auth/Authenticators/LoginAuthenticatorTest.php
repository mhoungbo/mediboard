<?php

namespace Ox\Core\Tests\Unit\Auth\Authenticators;

use Ox\Core\Auth\Authenticators\LoginAuthenticator;
use Ox\Core\Auth\Badges\CsrfBadgeBuilder;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class LoginAuthenticatorTest extends AbstractAuthenticatorTest
{
    protected function getNormalAuthenticator(bool $is_already_authenticated = false): LoginAuthenticator
    {
        $security = $this->createMock(Security::class);
        $security->method('isGranted')->willReturnMap(
            [
                ['IS_AUTHENTICATED_FULLY', null, $is_already_authenticated],
            ]
        );

        // Manually adding loadUserByIdentifier because of it does not exist per se.
        $provider = $this->getMockBuilder(UserProviderInterface::class)
                         ->addMethods(['loadUserByIdentifier'])
                         ->onlyMethods(['loadUserByUsername', 'refreshUser', 'supportsClass'])
                         ->getMock();

        return new LoginAuthenticator(
            $provider,
            $this->createMock(CredentialsCheckerInterface::class),
            $security,
            $this->createMock(CsrfBadgeBuilder::class)
        );
    }

    public function supportedRequestsProvider(): array
    {
        return [
            'request with username and password' => [$this->getValidRequest()],
        ];
    }

    public function unsupportedRequestsProvider(): array
    {
        $login_wo_pwd = new Request();
        $login_wo_pwd->request->set('username', 'username');

        $pwd_wo_login = new Request();
        $pwd_wo_login->request->set('password', 'password');

        $pwd_and_login = new Request();
        $pwd_and_login->query->set('username', 'username');
        $pwd_and_login->query->set('password', 'password');

        $public = $this->getValidRequest();
        $public->attributes->set('public', true);

        return [
            'empty request'                                             => [new Request()],
            'request with username, but without password'               => [$login_wo_pwd],
            'request with password, but without username'               => [$pwd_wo_login],
            'request with username and password, but not in parameters' => [$pwd_and_login],
            'valid request, but public'                                 => [$public],
        ];
    }

    private function getValidRequest(?string $username = 'username', ?string $password = 'password'): Request
    {
        $request = new Request();
        $request->request->set('username', $username);
        $request->request->set('password', $password);

        return $request;
    }

    public function validRequestsProvider(): array
    {
        return [
            [
                $this->getValidRequest(),
            ],
        ];
    }

    public function invalidRequestsProvider(): array
    {
        return [
            'username null'              => [$this->getValidRequest(null, 'password')],
            'password null'              => [$this->getValidRequest('username', null)],
            'username and password null' => [$this->getValidRequest(null, null)],
        ];
    }

    /**
     * @dataProvider invalidRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testInvalidCredentialsFormatThrowsAnException(Request $request): void
    {
        $auth = $this->getNormalAuthenticator();
        $this->assertTrue($auth->supports($request));

        $this->expectException(AuthenticationException::class);
        $auth->authenticate($request);
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testAuthenticateDoesNotFail(Request $request): void
    {
        $this->expectNotToPerformAssertions();

        $auth = $this->getNormalAuthenticator();
        $auth->authenticate($request);
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testIsInteractive(Request $request): void
    {
        $auth = $this->getNormalAuthenticator();
        $auth->authenticate($request);

        $this->assertTrue($auth->isInteractive());
    }
}
