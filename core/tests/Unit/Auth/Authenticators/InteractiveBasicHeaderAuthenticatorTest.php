<?php

namespace Ox\Core\Tests\Unit\Auth\Authenticators;

use Ox\Core\Auth\Authenticators\InteractiveBasicHeaderAuthenticator;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class InteractiveBasicHeaderAuthenticatorTest extends BasicHeaderAuthenticatorTest
{
    protected function getNormalAuthenticator(
        bool $is_already_authenticated = false
    ): InteractiveBasicHeaderAuthenticator {
        $security = $this->createMock(Security::class);
        $security->method('isGranted')->willReturnMap(
            [
                ['IS_AUTHENTICATED_FULLY', null, $is_already_authenticated],
            ]
        );

        // Manually adding loadUserByIdentifier because of it does not exist per se.
        $provider = $this->getMockBuilder(UserProviderInterface::class)
                         ->addMethods(['loadUserByIdentifier'])
                         ->onlyMethods(['loadUserByUsername', 'refreshUser', 'supportsClass'])
                         ->getMock();

        return new InteractiveBasicHeaderAuthenticator(
            $provider,
            $this->createMock(CredentialsCheckerInterface::class),
            $security
        );
    }
}
