<?php

namespace Ox\Core\Tests\Unit\Auth\Authenticators;

use Exception;
use Ox\Core\Auth\Authenticators\FCAuthenticator;
use Ox\Core\Auth\Exception\ExternalAuthenticationException;
use Ox\Core\OAuth2\OIDC\FC\Client;
use Ox\Core\OAuth2\OIDC\FC\UserInfo;
use Ox\Core\OAuth2\OIDC\TokenSet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;

// Todo: Test when UserInfo Sub/Id is null
class FCAuthenticatorTest extends AbstractAuthenticatorTest
{
    protected function getNormalAuthenticator(bool $is_already_authenticated = false): FCAuthenticator
    {
        return $this->getAuthenticator($is_already_authenticated, false, false, false);
    }

    private function getAuthenticator(
        bool $is_already_authenticated,
        bool $request_tokens_exception,
        bool $user_info_exception,
        bool $get_sub_null
    ): FCAuthenticator {
        $security = $this->createMock(Security::class);
        $security->method('isGranted')->willReturnMap(
            [
                ['IS_AUTHENTICATED_FULLY', null, $is_already_authenticated],
            ]
        );

        // Manually adding loadUserByIdentifier because of it does not exist per se.
        $provider = $this->getMockBuilder(UserProviderInterface::class)
                         ->addMethods(['loadUserByIdentifier'])
                         ->onlyMethods(['loadUserByUsername', 'refreshUser', 'supportsClass'])
                         ->getMock();

        $user_info = $this->createMock(UserInfo::class);
        $user_info->method('getId')->willReturn('test@example.com');

        if (!$get_sub_null) {
            $user_info->method('getSub')->willReturn('someSub');
        }

        $token_set = $this->createMock(TokenSet::class);

        $client = $this->createMock(Client::class);

        if ($request_tokens_exception) {
            $client->expects($this->once())->method('requestTokens')->willThrowException(new Exception());
        }

        if ($user_info_exception) {
            $client->expects($this->once())->method('getUserInfo')->willThrowException(new Exception());
        }

        $client->method('getUserInfo')->willReturn($user_info);
        $client->method('getTokenSet')->willReturn($token_set);

        return new FCAuthenticator($client, $provider, $security);
    }

    public function supportedRequestsProvider(): array
    {
        return [
            'request with code and state' => [$this->getValidRequest()],
        ];
    }

    public function unsupportedRequestsProvider(): array
    {
        $only_code = new Request();
        $only_code->query->set('code', 'code');

        $only_state = new Request();
        $only_state->query->set('state', 'state');

        $only_nonce = new Request();
        $only_nonce->query->set('nonce', 'nonce');

        $code_state = new Request();
        $code_state->query->set('code', 'code');
        $code_state->query->set('state', 'state');

        $code_nonce = new Request();
        $code_nonce->query->set('code', 'code');
        $code_nonce->query->set('nonce', 'nonce');

        $state_nonce = new Request();
        $state_nonce->query->set('state', 'state');
        $state_nonce->query->set('nonce', 'nonce');

        $not_in_query = new Request();
        $not_in_query->request->set('code', 'code');
        $not_in_query->request->set('state', 'state');
        $not_in_query->request->set('nonce', 'nonce');

        $public = $this->getValidRequest();
        $public->attributes->set('public', true);

        return [
            'empty request'                                        => [new Request()],
            'request with only code'                               => [$only_code],
            'request with only state'                              => [$only_state],
            'request with only nonce'                              => [$only_nonce],
            'request with code and state, but without nonce'       => [$code_state],
            'request with code and nonce, but without state'       => [$code_nonce],
            'request with state and nonce, but without code'       => [$state_nonce],
            'request with code, state and nonce, but not in query' => [$not_in_query],
            'valid request, but public'                            => [$public],
        ];
    }

    private function getValidRequest(): Request
    {
        $request = new Request();
        $request->query->set('code', 'code');
        $request->query->set('state', 'state');
        $request->query->set('nonce', 'nonce');

        return $request;
    }

    public function validRequestsProvider(): array
    {
        return [
            [
                $this->getValidRequest(),
            ],
        ];
    }

    public function clientErrorsProvider(): array
    {
        return [
            'request tokens exception' => [$this->getAuthenticator(false, true, false, false)],
            'get user info exception'  => [$this->getAuthenticator(false, false, true, false)],
            'user sub is null'         => [$this->getAuthenticator(false, false, false, true)],
        ];
    }

    /**
     * @dataProvider clientErrorsProvider
     *
     * @param FCAuthenticator $authenticator
     *
     * @return void
     */
    public function testClientExceptionsAreCaughtAndExternal(FCAuthenticator $authenticator): void
    {
        $this->expectException(ExternalAuthenticationException::class);

        $valid = $this->getValidRequest();

        $this->assertTrue($authenticator->supports($valid));
        $authenticator->authenticate($valid);
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testAuthenticateDoesNotFail(Request $request): void
    {
        $this->expectNotToPerformAssertions();

        $auth = $this->getNormalAuthenticator();
        // AuthenticatorInterface::supports must be called first (setting the $request property).
        $auth->supports($request);

        $auth->authenticate($request);
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testIsInteractive(Request $request): void
    {
        $auth = $this->getNormalAuthenticator();
        $auth->supports($request);
        $auth->authenticate($request);

        $this->assertTrue($auth->isInteractive());
    }
}
