<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Authenticators;

use Exception;
use Ox\Core\Auth\Authenticators\AbstractAuthenticator;
use Ox\Core\Auth\Authenticators\LegacyTokenAuthenticator;
use Ox\Core\Auth\Providers\TokenUserProvider;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\Admin\Repositories\AccessTokenRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;

/**
 * Todo: Test that non-legacy tokens are forbidden.
 */
class LegacyTokenAuthenticatorTest extends AbstractAuthenticatorTest
{
    protected function getNormalAuthenticator(bool $is_already_authenticated = false): AuthenticatorInterface
    {
        $repository = new AccessTokenRepository();

        $security = $this->createMock(Security::class);
        $security->method('isGranted')->willReturnMap(
            [
                ['IS_AUTHENTICATED_FULLY', null, $is_already_authenticated],
            ]
        );

        return new LegacyTokenAuthenticator(
            $repository,
            new TokenUserProvider($repository),
            $security,
        );
    }

    public function supportedRequestsProvider(): array
    {
        return [
            'request with token' => [$this->getValidRequest()],
        ];
    }

    public function unsupportedRequestsProvider(): array
    {
        $public = $this->getValidRequest();
        $public->attributes->set('public', true);

        return [
            'request with token, but public' => [$public],
        ];
    }

    public function validRequestsProvider(): array
    {
        return [
            'legacy token' => [
                $this->mockRepository(true, true, false),
                $this->getValidRequest(),
            ],
        ];
    }

    protected function getAuthenticator(?AccessTokenRepository $repository): AbstractAuthenticator
    {
        $repository = $repository ?: new AccessTokenRepository();
        $provider   = new TokenUserProvider($repository);
        $security   = $this->createMock(Security::class);

        return new LegacyTokenAuthenticator($repository, $provider, $security);
    }

    /**
     * @dataProvider validRequestsProvider
     *
     * @param AccessTokenRepository $repository
     * @param Request               $request
     *
     * @return void
     */
    public function testAuthenticateDoesNotFail(AccessTokenRepository $repository, Request $request): void
    {
        $this->expectNotToPerformAssertions();

        $auth = $this->getAuthenticator($repository);
        $auth->authenticate($request);
    }

    /**
     * @dataProvider emptyTokenProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testDoesNotPermitEmptyToken(Request $request): void
    {
        $this->expectException(CustomUserMessageAuthenticationException::class);
        $auth = $this->getNormalAuthenticator();

        $auth->authenticate($request);
    }

    /**
     * @dataProvider invalidTokenProvider
     *
     * @param AccessTokenRepository $repository
     *
     * @return void
     */
    public function testDoesNotPermitInvalidToken(AccessTokenRepository $repository): void
    {
        $this->expectException(CustomUserMessageAuthenticationException::class);
        $auth = $this->getAuthenticator($repository);

        $auth->authenticate($this->getValidRequest());
    }

    public function testErrorUsingTokenThrowsAnException(): void
    {
        $this->expectException(CustomUserMessageAuthenticationException::class);

        $auth = $this->getAuthenticator($this->mockRepository(true, true, true));
        $auth->authenticate($this->getValidRequest());
    }

    /**
     * @depends      testSupportsRequests
     * @dataProvider supportedRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testDoesNotSupportRequestIfAlreadyAuthenticated(Request $request): void
    {
        $this->expectNotToPerformAssertions(); // Not implemented - quite the contrary
    }

    /**
     * @depends      testSupportsRequests
     * @dataProvider supportedRequestsProvider
     *
     * @param Request $request
     *
     * @return void
     */
    public function testSupportsRequestIfAlreadyAuthenticated(Request $request): void
    {
        $authenticator = $this->getNormalAuthenticator(true);
        $this->assertTrue($authenticator->supports($request));
    }

    private function getValidRequest(?string $value = 'test'): Request
    {
        $request = new Request();
        $request->query->set('token', $value);

        return $request;
    }

    public function requestProvider(): array
    {
        $public = $this->getValidRequest();
        $public->attributes->set('public', true);

        return [
            'request with token'            => [$this->getValidRequest(), true],
            'request with token but public' => [$public, false],
        ];
    }

    public function emptyTokenProvider(): array
    {
        return [
            'null token'  => [$this->getValidRequest(null)],
            'empty token' => [$this->getValidRequest('')],
        ];
    }

    public function invalidTokenProvider(): array
    {
        return [
            'not found token' => [$this->mockRepository(false, false, false)],
            'invalid token'   => [$this->mockRepository(true, false, false)],
        ];
    }

    private function mockRepository(
        bool $returns_token,
        bool $is_valid,
        bool $use_it_exception,
    ): AccessTokenRepository {
        $repository = $this->getMockBuilder(AccessTokenRepository::class)
                           ->getMock();

        if ($returns_token) {
            $token = $this->getMockBuilder(CViewAccessToken::class)
                          ->disableOriginalConstructor()
                          ->getMock();

            $token->hash = 'some hash';

            $token->expects($this->any())->method('isValidForLegacy')->willReturn($is_valid);

            if ($use_it_exception) {
                $token->expects($this->any())->method('useIt')->willThrowException(new Exception());
            }

            $repository->expects($this->any())->method('findByHash')->willReturn($token);
        } else {
            $repository->expects($this->any())->method('findByHash')->willReturn(null);
        }

        return $repository;
    }
}
