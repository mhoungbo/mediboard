<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Voters;

use Ox\Core\Auth\User;
use Ox\Core\Auth\Voters\CanSwitchUserVoter;
use Ox\Core\Auth\WeakPassword;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CanSwitchUserVoterTest extends AbstractVoterTest
{
    public function subjectProvider(): array
    {
        return [
            'User class (success)'        => [User::class, true],
            'CUser class (failure)'       => [CUser::class, false],
            'string (failure)'            => [get_debug_type('test'), false],
            'int (failure)'               => [get_debug_type(1), false],
            'null (failure)'              => [get_debug_type(null), false],
            'bool (failure)'              => [get_debug_type(true), false],
            'float (failure)'             => [get_debug_type(1.0), false],
            'array (failure)'             => [get_debug_type([]), false],
            'resource (failure)'          => ['resource (stream)', false],
            'resource (closed) (failure)' => ['resource (closed)', false],
            'anonymous class (failure)'   => [
                get_debug_type(
                    new class {
                    }
                ),
                false,
            ],
        ];
    }

    public function attributeProvider(): array
    {
        return [
            'CAN_SWITCH_USER (success)' => ['CAN_SWITCH_USER', true],
            'can_switch_user (failure)' => ['can_switch_user', false],
            'ROLE_USER (failure)'       => ['ROLE_USER', false],
            'role_user (failure)'       => ['role_user', false],
            'empty (failure)'           => ['', false],
        ];
    }

    public function voteProvider(): array
    {
        $invalid_user_token = $this->createMock(TokenInterface::class);
        $invalid_user_token->method('getUser')->willReturn($this->createMock(UserInterface::class));

        $non_admin_user_token   = $this->mockToken(false);
        $admin_user_token       = $this->mockToken(true);
        $super_admin_user_token = $this->mockToken(true, true);

        $valid_subject   = $this->createMock(User::class);
        $invalid_subject = null;

        return [
            "token's user is not a User (failure)"  => [
                $invalid_user_token,
                $valid_subject,
                VoterInterface::ACCESS_DENIED,
            ],
            "subject is not a User (failure)"       => [
                $admin_user_token,
                $invalid_subject,
                VoterInterface::ACCESS_ABSTAIN, // Because of not supporting the subject
            ],
            "token's user is admin (success)"       => [
                $admin_user_token,
                $valid_subject,
                VoterInterface::ACCESS_GRANTED,
            ],
            "token's user is super admin (success)" => [
                $super_admin_user_token,
                $valid_subject,
                VoterInterface::ACCESS_GRANTED,
            ],
            "token's user is not admin (failure)"   => [
                $non_admin_user_token,
                $valid_subject,
                VoterInterface::ACCESS_DENIED,
            ],
        ];
    }

    public function testUserWithWeakPasswordCannotSwitch(): void
    {
        $voter = new CanSwitchUserVoter($this->getWeakPassword());

        $admin_user_token     = $this->mockToken(true);
        $non_admin_user_token = $this->mockToken(false);
        $valid_subject        = $this->createMock(User::class);

        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $voter->vote($admin_user_token, $valid_subject, $this->getSupportedAttributes())
        );

        $this->assertEquals(
            VoterInterface::ACCESS_DENIED,
            $voter->vote($non_admin_user_token, $valid_subject, $this->getSupportedAttributes())
        );
    }

    private function getWeakPassword(): WeakPassword
    {
        $weak_password = $this->createMock(WeakPassword::class);
        $weak_password->expects($this->exactly(2))->method('check')->willReturn(true);

        return $weak_password;
    }

    public function getSupportedAttributes(): array
    {
        return ['CAN_SWITCH_USER'];
    }

    public function getVoter(): VoterInterface
    {
        return new CanSwitchUserVoter($this->createMock(WeakPassword::class));
    }

    private function mockToken(bool $admin, bool $super_admin = false): TokenInterface
    {
        $ox_user = $this->createMock(CUser::class);

        if ($super_admin) {
            $ox_user->method('isSuperAdmin')->willReturn($super_admin);
        } else {
            $ox_user->method('isTypeAdmin')->willReturn($admin);
        }

        $user = $this->createMock(User::class);
        $user->method('getOxUser')->willReturn($ox_user);

        $token = $this->createMock(TokenInterface::class);
        $token->method('getUser')->willReturn($user);

        return $token;
    }
}
