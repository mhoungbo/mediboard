<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Voters;

use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

abstract class AbstractVoterTest extends OxUnitTestCase
{
    abstract public function getVoter(): VoterInterface;

    abstract public function subjectProvider(): array;

    abstract public function attributeProvider(): array;

    abstract public function voteProvider(): array;

    abstract public function getSupportedAttributes(): array;

    /**
     * @dataProvider subjectProvider
     *
     * @param string $subject
     * @param bool   $expected
     *
     * @return void
     */
    public function testVoterOnlySupportsWantedObjects(string $subject, bool $expected): void
    {
        $voter = $this->getVoter();
        $this->assertEquals($expected, $voter->supportsType($subject));
    }

    /**
     * @dataProvider attributeProvider
     *
     * @param string $attribute
     * @param bool   $expected
     *
     * @return void
     */
    public function testVoterOnlySupportsWantedAttributes(string $attribute, bool $expected): void
    {
        $voter = $this->getVoter();
        $this->assertEquals($expected, $voter->supportsAttribute($attribute));
    }

    /**
     * @dataProvider voteProvider
     *
     * @param TokenInterface $token
     * @param mixed          $subject
     * @param int            $expected
     *
     * @return void
     */
    public function testVoter(TokenInterface $token, $subject, int $expected): void
    {
        $voter = $this->getVoter();
        $this->assertEquals($expected, $voter->vote($token, $subject, $this->getSupportedAttributes()));
    }
}
