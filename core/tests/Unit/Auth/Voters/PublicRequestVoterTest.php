<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Voters;

use Ox\Core\Api\Request\RequestApi;
use Ox\Core\Auth\Voters\PublicRequestVoter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class PublicRequestVoterTest extends AbstractVoterTest
{
    public function subjectProvider(): array
    {
        return [
            'SF request class (success)'  => [Request::class, true],
            'OX request class (failure)'  => [RequestApi::class, false],
            'string (failure)'            => [get_debug_type('test'), false],
            'int (failure)'               => [get_debug_type(1), false],
            'null (failure)'              => [get_debug_type(null), false],
            'bool (failure)'              => [get_debug_type(true), false],
            'float (failure)'             => [get_debug_type(1.0), false],
            'array (failure)'             => [get_debug_type([]), false],
            'resource (failure)'          => ['resource (stream)', false],
            'resource (closed) (failure)' => ['resource (closed)', false],
            'anonymous class (failure)'   => [
                get_debug_type(
                    new class {
                    }
                ),
                false,
            ],
        ];
    }

    public function attributeProvider(): array
    {
        return [
            'ROLE_USER (success)'     => ['ROLE_USER', true],
            'role_user (failure)'     => ['role_user', false],
            'ROLE_API_USER (failure)' => ['ROLE_API_USER', false],
            'role_api_user (failure)' => ['role_api_user', false],
            'empty (failure)'         => ['', false],
        ];
    }

    public function voteProvider(): array
    {
        $token = $this->createMock(TokenInterface::class);

        $public = new Request();
        $public->attributes->set('public', true);

        $std = new Request();

        $null = new Request();
        $null->attributes->set('public', null);

        $non_empty = new Request();
        $non_empty->attributes->set('public', [1, 2, 3]);

        return [
            'public'    => [$token, $public, VoterInterface::ACCESS_GRANTED],
            'standard'  => [$token, $std, VoterInterface::ACCESS_DENIED],
            'null'      => [$token, $null, VoterInterface::ACCESS_DENIED],
            'non-empty' => [$token, $non_empty, VoterInterface::ACCESS_DENIED],
        ];
    }

    public function getSupportedAttributes(): array
    {
        return ['ROLE_USER'];
    }

    public function getVoter(): VoterInterface
    {
        return new PublicRequestVoter();
    }
}
