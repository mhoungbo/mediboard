<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Checkers;

use Ox\Core\Auth\Checkers\BasicCredentialsChecker;
use Ox\Core\Config\Conf;
use Ox\Core\Security\Crypt\Hasher;

class BasicCredentialsCheckerTest extends AbstractCredentialsCheckerTest
{
    public function getChecker(Conf $conf = null): BasicCredentialsChecker
    {
        return new BasicCredentialsChecker(new Hasher(), ($conf instanceof Conf) ? $conf : new Conf());
    }
}
