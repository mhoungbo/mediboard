<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Checkers;

use Ox\Core\Auth\Checkers\CPXCertSignatureChecker;
use Ox\Core\Auth\User;
use Ox\Core\Security\Crypt\Crypto;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Files\Repositories\FileRepository;
use Ox\Tests\OxUnitTestCase;
use Ox\Tests\TestsException;
use stdClass;
use Symfony\Component\Security\Core\User\UserInterface;

// Todo: More exhaustive (signature mismatch, etc)
class CPXCertSignatureCheckerTest extends OxUnitTestCase
{
    public function getNormalChecker(): CPXCertSignatureChecker
    {
        return new CPXCertSignatureChecker(new Crypto(), new FileRepository());
    }

    /**
     * @dataProvider validCredentialsProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     */
    public function testValidCredentials($credentials, UserInterface $user): void
    {
        $checker = $this->getNormalChecker();

        $this->assertTrue($checker->check($credentials, $user));
    }

    /**
     * @dataProvider invalidCredentialsProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     */
    public function testInvalidCredentials($credentials, UserInterface $user): void
    {
        $checker = $this->getNormalChecker();

        $this->assertFalse($checker->check($credentials, $user));
    }

    /**
     * @dataProvider invalidUsersProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     */
    public function testCheckerOnlyAcceptsUser($credentials, UserInterface $user): void
    {
        $checker = $this->getNormalChecker();

        $this->assertFalse($checker->check($credentials, $user));
    }

    /**
     * @dataProvider invalidTypeCredentialsProvider
     *
     * @param mixed $credentials
     *
     * @return void
     */
    public function testCheckerOnlyAcceptValidCredentialsType($credentials): void
    {
        $checker = $this->getNormalChecker();
        $this->assertFalse($checker->check($credentials, $this->createMock(User::class)));
    }

    protected function getValidCredentials(): array
    {
        return [
            'signature'      => AuthenticationFixtures::CPX_SIGNATURE,
            'cert_signature' => AuthenticationFixtures::CPX_CERT_SIGNATURE,
        ];
    }

    protected function getInvalidCredentials(): array
    {
        return [
            'signature'      => 'invalid',
            'cert_signature' => 'invalid',
        ];
    }

    public function invalidTypeCredentialsProvider(): array
    {
        return [
            'string' => ['str'],
            'null'   => [null],
            'int'    => [123],
            'object' => [new stdClass()],
        ];
    }

    public function validCredentialsProvider(): array
    {
        return $this->getValidProviderData(User::class);
    }

    private function getValidProviderData(string $user_class): array
    {
        return [
            'valid password' => [
                $this->getValidCredentials(),
                $this->mockUser(
                    $user_class,
                    true
                ),
            ],
        ];
    }

    public function invalidCredentialsProvider(): array
    {
        return $this->getInvalidProviderData(User::class);
    }

    private function getInvalidProviderData(string $user_class): array
    {
        return [
            'invalid signature' => [
                $this->getInvalidCredentials(),
                $this->mockUser($user_class, false),
            ],
        ];
    }

    public function invalidUsersProvider(): array
    {
        return $this->getValidProviderData(UserInterface::class)
            + $this->getInvalidProviderData(UserInterface::class);
    }

    /**
     * @param string $user_class
     * @param bool   $success
     *
     * @return UserInterface
     * @throws TestsException
     */
    protected function mockUser(
        string $user_class,
        bool   $success
    ): UserInterface {
        $ox_user = $this->getMockBuilder(CUser::class)
                        ->disableOriginalConstructor()
                        ->getMock();

        if ($success) {
            /** @var CFile $file */
            $file = $this->getObjectFromFixturesReference(
                CFile::class,
                AuthenticationFixtures::CPX_SIGNATURE_FILE
            );

            $ox_user->_id = $file->object_id;
        } else {
            $ox_user->_id = -1;
        }

        if ($user_class === UserInterface::class) {
            $user = $this->getMockBuilder($user_class)
                         ->disableOriginalConstructor()
                         ->getMock();
        } else {
            $user = $this->getMockBuilder($user_class)
                         ->disableOriginalConstructor()
                         ->onlyMethods(['getOxUser'])
                         ->getMock();

            $user->expects($this->once())->method('getOxUser')->willReturn($ox_user);
        }

        return $user;
    }
}
