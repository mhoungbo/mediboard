<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Checkers;

use Exception;
use Ox\Core\AppUiBridge;
use Ox\Core\Auth\Checkers\UserChecker;
use Ox\Core\Auth\Exception\CouldNotValidatePreAuthentication;
use Ox\Core\Auth\User;
use Ox\Core\Config\Conf;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractUserCheckerTest extends OxUnitTestCase
{
    abstract public function getChecker(Conf $conf = null): UserCheckerInterface;

    abstract public function getCheckerClass(): string;

    public function usersProvider(): array
    {
        return [
            'user accepted'            => [$this->mockUser(User::class, false, false, false, false), false],
            'user interface forbidden' => [$this->mockUser(UserInterface::class, false, false, false, false), true],
        ];
    }

    public function allowedUsersProvider(): array
    {
        return [
            'normal user'                               => [
                $this->mockUser(User::class, false, false, false, false),
                new Conf(),
            ],
            'admin user'                                => [
                $this->mockUser(User::class, false, false, false, true),
                new Conf(),
            ],
            'with mediuser'                             => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(new Conf(), true, $this->mockMediuser(true, false), true, ''),
            ],
            'without mediuser module when not intranet' => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(new Conf(), false, null, false, ''),
            ],
            'without mediuser when not intranet'        => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(new Conf(), true, null, false, ''),
            ],
            'with remote access allowed'                => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(
                    new Conf(),
                    true,
                    $this->mockMediuser(true, true),
                    false,
                    ''
                ),
            ],
            'without remote access but is admin'        => [
                $this->mockUser(User::class, false, false, false, true),
                $this->mockChecker(
                    new Conf(),
                    true,
                    $this->mockMediuser(true, false),
                    false,
                    ''
                ),
            ],
            'access from location'                      => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(
                    $this->mockConf('127.0.0.1'),
                    true,
                    $this->mockMediuser(true, false),
                    true,
                    '127.0.0.1'
                ),
            ],
            'without access from location but is bot'   => [
                $this->mockUser(User::class, false, false, false, false, true),
                $this->mockChecker(
                    $this->mockConf('127.0.0.1'),
                    true,
                    $this->mockMediuser(true, false),
                    true,
                    '127.0.0.1'
                ),
            ],
            'without access from location but is admin' => [
                $this->mockUser(User::class, false, false, false, true),
                $this->mockChecker(
                    $this->mockConf('127.0.0.1'),
                    true,
                    $this->mockMediuser(true, false),
                    true,
                    '127.0.0.1'
                ),
            ],
        ];
    }

    abstract public function forbiddenPreAuthUsersProvider(): array;

    abstract public function forbiddenPostAuthUsersProvider(): array;

    /**
     * @dataProvider usersProvider
     *
     * @param UserInterface  $user
     * @param Exception|null $e
     *
     * @return void
     * @throws Exception
     */
    public function testCheckerOnlyAcceptsUserDuringPreAuth(UserInterface $user, bool $exception): void
    {
        if ($exception) {
            $this->expectExceptionObject(CouldNotValidatePreAuthentication::userIsNotSupported());
        }

        $checker = $this->getChecker();
        $checker->checkPreAuth($user);
    }

    /**
     * @dataProvider usersProvider
     *
     * @param UserInterface  $user
     * @param Exception|null $e
     *
     * @return void
     * @throws Exception
     */
    public function testCheckerOnlyAcceptsUserDuringPostAuth(UserInterface $user, bool $exception): void
    {
        if ($exception) {
            $this->expectExceptionObject(CouldNotValidatePreAuthentication::userIsNotSupported());
        }

        $checker = $this->getChecker();
        $checker->checkPostAuth($user);
    }

    /**
     * @dataProvider allowedUsersProvider
     *
     * @param UserInterface    $user
     * @param Conf|UserChecker $conf
     * @param Exception        $e
     *
     * @return void
     * @throws Exception
     */
    public function testUserIsAllowed(UserInterface $user, $conf): void
    {
        $checker = ($conf instanceof Conf) ? $this->getChecker($conf) : $conf;
        $checker->checkPreAuth($user);
        $checker->checkPostAuth($user);
    }

    protected function mockChecker(
        Conf        $conf,
        bool        $mediuser_module,
        ?CMediusers $mediusers,
        bool        $intranet,
        string      $ip
    ): UserCheckerInterface {
        $user_checker = $this->getMockBuilder($this->getCheckerClass())
                             ->setConstructorArgs([$conf, new AppUiBridge()])
                             ->onlyMethods(['isMediusersModuleActive', 'loadMediuser', 'isIntranet', 'getIp'])
                             ->getMock();

        $user_checker->expects($this->any())->method('isMediusersModuleActive')->willReturn($mediuser_module);
        $user_checker->expects($this->any())->method('loadMediuser')->willReturn($mediusers);
        $user_checker->expects($this->any())->method('isIntranet')->willReturn($intranet);
        $user_checker->expects($this->any())->method('getIp')->willReturn($ip);

        return $user_checker;
    }

    protected function mockMediuser(
        bool   $actif,
        bool   $remote,
        string $deb_activite = null,
        string $fin_activite = null
    ): CMediusers {
        $user = $this->getMockBuilder(CMediusers::class)
                     ->disableOriginalConstructor()
                     ->getMock();

        $user->_id   = 'test';
        $user->actif = $actif;
        // Remote=1 means NO remote is allowed...
        $user->remote       = $remote ? 0 : 1;
        $user->deb_activite = $deb_activite;
        $user->fin_activite = $fin_activite;

        $user->expects($this->any())->method('loadRefFunction')->willReturnCallback(function () {
            $function           = new CFunctions();
            $function->group_id = 1;

            return $function;
        });

        return $user;
    }

    protected function mockConf(string $network_whitelist): Conf
    {
        $conf = $this->createMock(Conf::class);

        $conf->method('getForGroupId')->willReturnMap(
            [
                ['system network ip_address_range_whitelist', 1, $network_whitelist,],
            ]
        );

        return $conf;
    }

    protected function mockUser(
        string $user_class,
        bool   $template,
        bool   $secondary,
        bool   $locked,
        bool   $admin,
        bool   $bot = false
    ): UserInterface {
        $ox_user = $this->getMockBuilder(CUser::class)
                        ->disableOriginalConstructor()
                        ->getMock();

        $ox_user->user_username = 'username';
        $ox_user->template      = $template;

        $ox_user->method('isSecondary')->willReturn($secondary);
        $ox_user->method('isLocked')->willReturn($locked);
        $ox_user->method('isTypeAdmin')->willReturn($admin);
        $ox_user->method('isRobot')->willReturn($bot);

        if ($user_class === UserInterface::class) {
            $user = $this->getMockBuilder($user_class)
                         ->disableOriginalConstructor()
                         ->getMock();
        } else {
            $user = $this->getMockBuilder($user_class)
                         ->disableOriginalConstructor()
                         ->onlyMethods(['getOxUser'])
                         ->getMock();

            $user->expects($this->atMost(2))->method('getOxUser')->willReturn($ox_user);
        }

        return $user;
    }
}
