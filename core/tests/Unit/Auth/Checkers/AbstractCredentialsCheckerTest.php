<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Checkers;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Ox\Core\Auth\Exception\CredentialsCheckException;
use Ox\Core\Auth\User;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Ox\Core\Security\Crypt\Hash;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Core\Security\Exception\CouldNotHash;
use Ox\Mediboard\Admin\CUser;
use Ox\Tests\OxUnitTestCase;
use stdClass;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractCredentialsCheckerTest extends OxUnitTestCase
{
    abstract public function getChecker(): CredentialsCheckerInterface;

    /**
     * @return mixed
     */
    protected function getValidCredentials()
    {
        return 'password';
    }

    /**
     * @return mixed
     */
    protected function getInvalidCredentials()
    {
        return 'invalid';
    }

    /**
     * @dataProvider validCredentialsProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testValidCredentials($credentials, UserInterface $user): void
    {
        $checker = $this->getChecker();

        $this->assertTrue($checker->check($credentials, $user));
    }

    /**
     * @dataProvider invalidCredentialsProvider
     *
     * @param string        $credentials
     * @param UserInterface $user
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testInvalidCredentials(string $credentials, UserInterface $user): void
    {
        $checker = $this->getChecker();

        $this->assertFalse($checker->check($credentials, $user));
    }

    /**
     * @dataProvider invalidUsersProvider
     *
     * @param string        $credentials
     * @param UserInterface $user
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testCheckerOnlyAcceptsUser(string $credentials, UserInterface $user): void
    {
        $checker = $this->getChecker();

        $this->assertFalse($checker->check($credentials, $user));
    }

    /**
     * @dataProvider invalidTypeCredentialsProvider
     *
     * @param mixed $credentials
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testCheckerOnlyAcceptValidCredentialsType($credentials): void
    {
        $checker = $this->getChecker();
        $this->assertFalse($checker->check($credentials, $this->createMock(User::class)));
    }

    public function invalidTypeCredentialsProvider(): array
    {
        return [
            'array'  => [[]],
            'null'   => [null],
            'int'    => [123],
            'object' => [new stdClass()],
        ];
    }

    /**
     * @dataProvider weakPasswordProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testPasswordIsMarkedAsWeak($credentials, UserInterface $user): void
    {
        $auth_metadata = new AuthMetadataBadge('test');
        $this->assertFalse($auth_metadata->isWeakPassword());

        $checker = $this->getChecker();

        $checker->setAuthMetadataBadge($auth_metadata);

        $checker->check($credentials, $user);

        $this->assertTrue($auth_metadata->isWeakPassword());
    }

    /**
     * @dataProvider nonWeakPasswordProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testPasswordBadgeIsNotMarkedAsWeak($credentials, UserInterface $user): void
    {
        $auth_metadata = new AuthMetadataBadge('test');
        $this->assertFalse($auth_metadata->isWeakPassword());

        $checker = $this->getChecker();

        $checker->setAuthMetadataBadge($auth_metadata);

        $checker->check($credentials, $user);

        $this->assertFalse($auth_metadata->isWeakPassword());
    }

    /**
     * @dataProvider validCredentialsProvider
     * @dataProvider invalidCredentialsProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testLogAuthIsSet($credentials, UserInterface $user): void
    {
        $auth_metadata = new AuthMetadataBadge('TEST');
        $this->assertEquals('TEST', $auth_metadata->getAuth());

        $checker = $this->getChecker();

        $checker->setAuthMetadataBadge($auth_metadata);

        $checker->check($credentials, $user);

        $this->assertEquals($checker->getMethod(), $auth_metadata->getAuth());
    }

    /**
     * @dataProvider weakPasswordBotProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testWeakPasswordIsNotCheckWhenUserIsABot($credentials, UserInterface $user): void
    {
        $auth_metadata = new AuthMetadataBadge('test');
        $this->assertFalse($auth_metadata->isWeakPassword());

        $checker = $this->getChecker();

        $checker->setAuthMetadataBadge($auth_metadata);

        $checker->check($credentials, $user);

        $this->assertFalse($auth_metadata->isWeakPassword());
    }

    /**
     * Todo: This test should be more global (used in all data providers here)
     *
     * @dataProvider nonWeakPasswordProvider
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return void
     * @throws CredentialsCheckException
     */
    public function testPasswordRotationIsChecked($credentials, UserInterface $user): void
    {
        $auth_metadata = new AuthMetadataBadge('test');
        $this->assertFalse($auth_metadata->isWeakPassword());

        $conf = $this->createMock(Conf::class);
        $conf->expects($this->exactly(2))->method('get')->willReturnMap(
            [
                ['admin CUser force_changing_password', true,],
                ['admin CUser password_life_duration', '3 minutes',],
            ]
        );

        $checker = $this->getChecker($conf);

        $checker->setAuthMetadataBadge($auth_metadata);

        $checker->check($credentials, $user);

        $this->assertTrue($auth_metadata->isWeakPassword());
    }

    public function validCredentialsProvider(): array
    {
        return $this->getValidProviderData(User::class, false);
    }

    private function getValidProviderData(string $user_class, bool $weak_password, bool $bot = false): array
    {
        return [
            'valid password'  => [
                $this->getValidCredentials(),
                $this->mockUser(
                    $user_class,
                    $this->getValidCredentials(),
                    'salt',
                    $weak_password,
                    $bot
                ),
            ],
            'null salt in db' => [
                $this->getValidCredentials(),
                $this->mockUser($user_class, $this->getValidCredentials(), null, $weak_password, $bot),
            ],
        ];
    }

    public function invalidCredentialsProvider(): array
    {
        return $this->getInvalidProviderData(User::class);
    }

    private function getInvalidProviderData(string $user_class): array
    {
        return [
            'invalid password'        => [
                $this->getInvalidCredentials(),
                $this->mockUser($user_class, $this->getValidCredentials(), 'salt', false),
            ],
            'null password in db'     => [
                $this->getInvalidCredentials(),
                $this->mockUser($user_class, null, 'salt', false),
            ],
            'null salt and pwd in db' => [
                $this->getInvalidCredentials(),
                $this->mockUser($user_class, null, null, false),
            ],
        ];
    }

    public function invalidUsersProvider(): array
    {
        return $this->getValidProviderData(UserInterface::class, false)
            + $this->getInvalidProviderData(UserInterface::class);
    }

    public function weakPasswordProvider(): array
    {
        return $this->getValidProviderData(User::class, true);
    }

    public function nonWeakPasswordProvider(): array
    {
        return $this->getValidProviderData(User::class, false);
    }

    public function weakPasswordBotProvider(): array
    {
        return $this->getValidProviderData(User::class, true, true);
    }

    /**
     * @param string      $user_class
     * @param string|null $user_password
     * @param string|null $user_salt
     * @param bool        $check_password_weakness
     * @param bool        $bot
     *
     * @return UserInterface
     * @throws CouldNotHash
     */
    protected function mockUser(
        string  $user_class,
        ?string $user_password,
        ?string $user_salt,
        bool    $check_password_weakness,
        bool    $bot = false
    ): UserInterface {
        $ox_user = $this->getMockBuilder(CUser::class)
                        ->disableOriginalConstructor()
                        ->onlyMethods(['checkPasswordWeakness', 'isRobot'])
                        ->getMock();

        $ox_user->expects($this->atMost(1))->method('isRobot')->willReturn($bot);
        $ox_user->expects($this->once())->method('checkPasswordWeakness')->willReturn($check_password_weakness);
        $ox_user->user_password             = (new Hasher())->hash(Hash::SHA256, $user_salt . $user_password);
        $ox_user->user_salt                 = $user_salt;
        $ox_user->user_password_last_change = CMbDT::dateTime('-5 minutes');

        if ($user_class === UserInterface::class) {
            $user = $this->getMockBuilder($user_class)
                         ->disableOriginalConstructor()
                         ->getMock();
        } else {
            $user = $this->getMockBuilder($user_class)
                         ->disableOriginalConstructor()
                         ->onlyMethods(['getOxUser'])
                         ->getMock();

            $user->expects($this->once())->method('getOxUser')->willReturn($ox_user);
        }

        return $user;
    }
}
