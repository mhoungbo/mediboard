<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Checkers;

use Exception;
use Ox\Core\AppUiBridge;
use Ox\Core\Auth\Checkers\SwitchingUserChecker;
use Ox\Core\Auth\Checkers\UserChecker;
use Ox\Core\Auth\Exception\CouldNotValidatePostAuthentication;
use Ox\Core\Auth\User;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Throwable;

// Todo: Should explicitly test differences from UserCheckerTest (no remote check neither location)
class SwitchingUserCheckerTest extends AbstractUserCheckerTest
{
    public function getChecker(Conf $conf = null): UserCheckerInterface
    {
        return new SwitchingUserChecker(($conf instanceof Conf) ? $conf : new Conf(), new AppUiBridge());
    }

    public function getCheckerClass(): string
    {
        return SwitchingUserChecker::class;
    }

    /**
     * @dataProvider forbiddenPostAuthUsersProvider
     *
     * @param UserInterface    $user
     * @param Conf|UserChecker $conf
     * @param Exception        $e
     *
     * @return void
     * @throws Exception
     */
    public function testUserIsNotAllowedDuringPostAuth(UserInterface $user, $conf, Exception $e): void
    {
        $this->expectExceptionObject($e);

        $checker = ($conf instanceof Conf) ? $this->getChecker($conf) : $conf;
        $checker->checkPostAuth($user);
    }

    /**
     * @depends      testUserIsNotAllowedDuringPostAuth
     * @dataProvider forbiddenPostAuthUsersProvider
     *
     * @param UserInterface    $user
     * @param Conf|UserChecker $conf
     * @param Exception        $e
     *
     * @return void
     */
    public function testPostAuthErrorsAreVisibleByEndUser(UserInterface $user, $conf, Exception $e): void
    {
        try {
            $checker = ($conf instanceof Conf) ? $this->getChecker($conf) : $conf;
            $checker->checkPostAuth($user);

            $this->fail('This test should throw an exception');
        } catch (Throwable $t) {
            $this->assertInstanceOf(CustomUserMessageAccountStatusException::class, $t);
        }
    }

    public function forbiddenPreAuthUsersProvider(): array
    {
        return [];
    }

    public function forbiddenPostAuthUsersProvider(): array
    {
        return [
            'template'                => [
                $this->mockUser(User::class, true, false, false, false),
                new Conf(),
                CouldNotValidatePostAuthentication::userIsATemplate(),
            ],
            'secondary'               => [
                $this->mockUser(User::class, false, true, false, false),
                new Conf(),
                CouldNotValidatePostAuthentication::userIsSecondary(),
            ],
            'locked'                  => [
                $this->mockUser(User::class, false, false, true, false),
                new Conf(),
                CouldNotValidatePostAuthentication::userIsLocked(),
            ],
            'mediuser is deactivated' => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(new Conf(), true, $this->mockMediuser(false, false), false, ''),
                CouldNotValidatePostAuthentication::userIsDeactivated(),
            ],
            'mediuser has expired'    => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(
                    new Conf(),
                    true,
                    $this->mockMediuser(true, false, CMbDT::date('-2 day'), CMbDT::date('-1 day')),
                    false,
                    ''
                ),
                CouldNotValidatePostAuthentication::userAccountHasExpired(),
            ],
        ];
    }
}
