<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Checkers;

use Exception;
use Ox\Core\AppUiBridge;
use Ox\Core\Auth\Checkers\UserChecker;
use Ox\Core\Auth\Exception\CouldNotValidatePostAuthentication;
use Ox\Core\Auth\Exception\CouldNotValidatePreAuthentication;
use Ox\Core\Auth\User;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Throwable;

class UserCheckerTest extends AbstractUserCheckerTest
{
    public function getChecker(Conf $conf = null): UserCheckerInterface
    {
        return new UserChecker(($conf instanceof Conf) ? $conf : new Conf(), new AppUiBridge());
    }

    public function getCheckerClass(): string
    {
        return UserChecker::class;
    }

    /**
     * @dataProvider forbiddenPreAuthUsersProvider
     *
     * @param UserInterface    $user
     * @param Conf|UserChecker $conf
     * @param Exception        $e
     *
     * @return void
     * @throws Exception
     */
    public function testUserIsNotAllowedDuringPreAuth(UserInterface $user, $conf, Exception $e): void
    {
        $this->expectExceptionObject($e);

        $checker = ($conf instanceof Conf) ? $this->getChecker($conf) : $conf;
        $checker->checkPreAuth($user);
    }

    /**
     * @dataProvider forbiddenPostAuthUsersProvider
     *
     * @param UserInterface    $user
     * @param Conf|UserChecker $conf
     * @param Exception        $e
     *
     * @return void
     * @throws Exception
     */
    public function testUserIsNotAllowedDuringPostAuth(UserInterface $user, $conf, Exception $e): void
    {
        $this->expectExceptionObject($e);

        $checker = ($conf instanceof Conf) ? $this->getChecker($conf) : $conf;
        $checker->checkPostAuth($user);
    }

    /**
     * @depends      testUserIsNotAllowedDuringPreAuth
     * @dataProvider forbiddenPreAuthUsersProvider
     *
     * @param UserInterface    $user
     * @param Conf|UserChecker $conf
     * @param Exception        $e
     *
     * @return void
     * @throws Exception
     */
    public function testPreAuthErrorsAreNotVisibleByEndUser(UserInterface $user, $conf, Exception $e): void
    {
        try {
            $checker = ($conf instanceof Conf) ? $this->getChecker($conf) : $conf;
            $checker->checkPreAuth($user);

            $this->fail('This test should throw an exception');
        } catch (Throwable $t) {
            $this->assertInstanceOf(AccountStatusException::class, $t);
        }
    }

    /**
     * @depends      testUserIsNotAllowedDuringPostAuth
     * @dataProvider forbiddenPostAuthUsersProvider
     *
     * @param UserInterface    $user
     * @param Conf|UserChecker $conf
     * @param Exception        $e
     *
     * @return void
     */
    public function testPostAuthErrorsAreVisibleByEndUser(UserInterface $user, $conf, Exception $e): void
    {
        try {
            $checker = ($conf instanceof Conf) ? $this->getChecker($conf) : $conf;
            $checker->checkPostAuth($user);

            $this->fail('This test should throw an exception');
        } catch (Throwable $t) {
            $this->assertInstanceOf(CustomUserMessageAccountStatusException::class, $t);
        }
    }

    public function forbiddenPreAuthUsersProvider(): array
    {
        return [
            'template'  => [
                $this->mockUser(User::class, true, false, false, false),
                new Conf(),
                CouldNotValidatePreAuthentication::userIsATemplate(),
            ],
            'secondary' => [
                $this->mockUser(User::class, false, true, false, false),
                new Conf(),
                CouldNotValidatePreAuthentication::userIsSecondary(),
            ],
        ];
    }

    public function forbiddenPostAuthUsersProvider(): array
    {
        return [
            'locked'                                           => [
                $this->mockUser(User::class, false, false, true, false),
                new Conf(),
                CouldNotValidatePostAuthentication::userIsLocked(),
            ],
            'mediuser is deactivated'                          => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(new Conf(), true, $this->mockMediuser(false, false), false, ''),
                CouldNotValidatePostAuthentication::userIsDeactivated(),
            ],
            'mediuser has expired'                             => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(
                    new Conf(),
                    true,
                    $this->mockMediuser(true, false, CMbDT::date('-2 day'), CMbDT::date('-1 day')),
                    false,
                    ''
                ),
                CouldNotValidatePostAuthentication::userAccountHasExpired(),
            ],
            'mediuser (non-admin) has no remote access'        => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(
                    new Conf(),
                    true,
                    $this->mockMediuser(true, false),
                    false,
                    ''
                ),
                CouldNotValidatePostAuthentication::userHasNoRemoteAccess(),
            ],
            'mediuser (non-robot) has no access from location' => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(
                    $this->mockConf('127.0.0.1'),
                    true,
                    $this->mockMediuser(true, false),
                    true,
                    '127.0.0.2'
                ),
                CouldNotValidatePostAuthentication::userHasNoAccessFromThisLocation(),
            ],
            'mediuser (non-admin) has no access from location' => [
                $this->mockUser(User::class, false, false, false, false),
                $this->mockChecker(
                    $this->mockConf('127.0.0.1'),
                    true,
                    $this->mockMediuser(true, false),
                    true,
                    '127.0.0.2'
                ),
                CouldNotValidatePostAuthentication::userHasNoAccessFromThisLocation(),
            ],
        ];
    }
}
