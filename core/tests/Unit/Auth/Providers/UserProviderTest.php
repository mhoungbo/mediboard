<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Providers;

use Ox\Core\Auth\Providers\UserProvider;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProviderTest extends AbstractProviderTest
{
    protected function getProvider(): UserProviderInterface
    {
        return new UserProvider();
    }

    protected function getValidIdentifier(): string
    {
        return CUser::USER_PHPUNIT;
    }

    protected function getInvalidIdentifier(): string
    {
        return 'highly_unexpected_user_to_exist';
    }
}
