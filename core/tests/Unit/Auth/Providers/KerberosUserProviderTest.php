<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Providers;

use Ox\Core\Auth\Providers\KerberosUserProvider;
use Ox\Mediboard\Admin\CKerberosLdapIdentifier;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Repositories\KerberosLdapIdentifierRepository;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class KerberosUserProviderTest extends AbstractProviderTest
{
    protected function getProvider(): UserProviderInterface
    {
        return new KerberosUserProvider(new KerberosLdapIdentifierRepository());
    }

    protected function getValidIdentifier(): string
    {
        return AuthenticationFixtures::TEST_KRB_NAME;
    }

    protected function getInvalidIdentifier(): string
    {
        return 'invalid krb';
    }

    protected function getValidCUser(): CUser
    {
        return CKerberosLdapIdentifier::findUserByName($this->getValidIdentifier());
    }
}
