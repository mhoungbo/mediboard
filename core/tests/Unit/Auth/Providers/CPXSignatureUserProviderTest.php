<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Providers;

use Ox\Core\Auth\Providers\CPXSignatureUserProvider;
use Ox\Core\Auth\User;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Files\Repositories\FileRepository;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CPXSignatureUserProviderTest extends AbstractProviderTest
{
    protected function getProvider(bool $mock = false): UserProviderInterface
    {
        if (!$mock) {
            return new CPXSignatureUserProvider(new FileRepository(), new Hasher());
        }

        /** @var CFile $fixture */
        $fixture = $this->getObjectFromFixturesReference(CFile::class, AuthenticationFixtures::CPX_SIGNATURE_FILE);

        // Mocking here because of CFile not working in CLI SAPI (realpath error)
        $file = $this->getMockBuilder(CFile::class)->disableOriginalConstructor()->getMock();
        $file->method('getBinaryContent')->willReturn(AuthenticationFixtures::CPX_CERT_SIGNATURE);
        $file->object_id = $fixture->object_id;

        $repository = $this->createMock(FileRepository::class);
        $repository->method('findCertificateRelatedFiles')->willReturn([$file]);

        return new CPXSignatureUserProvider($repository, new Hasher());
    }

    protected function getValidIdentifier(): string
    {
        return AuthenticationFixtures::CPX_CERT_SIGNATURE;
    }

    protected function getInvalidIdentifier(): string
    {
        return 'invalid cert signature';
    }

    protected function getValidCUser(): CUser
    {
        /** @var CFile $file */
        $file = $this->getObjectFromFixturesReference(CFile::class, AuthenticationFixtures::CPX_SIGNATURE_FILE);

        return $file->loadTargetObject()->loadRefUser();
    }

    /**
     * @return void
     */
    public function testLoadByUsername(): void
    {
        $this->expectNotToPerformAssertions();

        $provider = $this->getProvider(true);
        $provider->loadUserByUsername($this->getValidIdentifier());
    }

    public function testLoadUserByIdentifier(): void
    {
        $this->expectNotToPerformAssertions();

        $provider = $this->getProvider(true);
        $provider->loadUserByIdentifier($this->getValidIdentifier());
    }

    public function testRefreshValidUser(): void
    {
        $this->expectNotToPerformAssertions();

        $provider = $this->getProvider(true);

        $valid_user = new User($this->getValidCUser());

        $provider->refreshUser($valid_user);
    }
}
