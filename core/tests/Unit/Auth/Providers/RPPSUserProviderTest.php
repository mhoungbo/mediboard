<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Providers;

use Ox\Core\Auth\Providers\RPPSUserProvider;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Ox\Mediboard\Mediusers\CMediusers;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class RPPSUserProviderTest extends AbstractProviderTest
{
    protected function getProvider(): UserProviderInterface
    {
        return new RPPSUserProvider();
    }

    protected function getValidIdentifier(): string
    {
        return AuthenticationFixtures::TEST_VALID_RPPS;
    }

    protected function getInvalidIdentifier(): string
    {
        return 'invalid rpps';
    }

    protected function getValidCUser(): CUser
    {
        $user       = new CMediusers();
        $user->rpps = $this->getValidIdentifier();
        $user->loadMatchingObjectEsc();

        return $user->_ref_user;
    }
}
