<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Providers;

use Ox\Core\Auth\Providers\ChainUserProvider;
use Ox\Core\Auth\Providers\RPPSUserProvider;
use Ox\Core\Auth\Providers\UserProvider;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ChainUserProviderTest extends AbstractProviderTest
{
    protected function getProvider(): UserProviderInterface
    {
        return new ChainUserProvider(
            new UserProvider(),
            new RPPSUserProvider()
        );
    }

    protected function getValidIdentifier(): string
    {
        return CUser::USER_PHPUNIT;
    }

    protected function getInvalidIdentifier(): string
    {
        return 'some user that does not exist';
    }

    /**
     * This test is suited for ChainUserProvider specificity: multiple identifiers.
     * The nominal tests (1 identifier, 1 provider or first matching provider) are automatically tested by parent.
     *
     * @dataProvider multipleIdentifiersProvider
     *
     * @param string $identifier
     * @param string $expected_identifier
     *
     * @return void
     */
    public function testMultipleIdentifiers(string $identifier, string $expected_identifier): void
    {
        $provider = $this->getProvider();

        $user = $provider->loadUserByIdentifier($identifier);

        $this->assertIsObject($user);
        $this->assertTrue($provider->supportsClass(get_class($user)));

        $this->assertEquals($expected_identifier, $user->getUserIdentifier());
        $this->assertNull($user->getPassword());

        $this->assertContains('ROLE_USER', $user->getRoles());
    }

    public function multipleIdentifiersProvider(): array
    {
        return [
            '1 identifier / matching is 2nd provider'  => [
                AuthenticationFixtures::TEST_VALID_RPPS,
                AuthenticationFixtures::TEST_VALID_RPPS,
            ],
            '2 identifiers / matching is 2nd provider' => [
                'somebody_that_does_not_exist#' . AuthenticationFixtures::TEST_VALID_RPPS,
                AuthenticationFixtures::TEST_VALID_RPPS,
            ],
            '2 identifiers / matching is 1st provider' => [
                CUser::USER_PHPUNIT . '#' . AuthenticationFixtures::TEST_VALID_RPPS,
                CUser::USER_PHPUNIT,
            ],
        ];
    }
}
