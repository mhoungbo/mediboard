<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Providers;

use Ox\Core\Auth\Providers\FCSubUserProvider;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class FcSubUserProviderTest extends AbstractProviderTest
{
    protected function getProvider(): UserProviderInterface
    {
        return new FCSubUserProvider();
    }

    protected function getValidIdentifier(): string
    {
        return AuthenticationFixtures::TEST_VALID_FC_SUB;
    }

    protected function getInvalidIdentifier(): string
    {
        return 'invalid fc sub';
    }

    protected function getValidCUser(): CUser
    {
        $user         = new CUser();
        $user->sub_fc = $this->getValidIdentifier();
        $user->loadMatchingObjectEsc();

        return $user;
    }
}
