<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Providers;

use Ox\Core\Auth\Providers\TokenUserProvider;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\Admin\Repositories\AccessTokenRepository;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class TokenUserProviderTest extends AbstractProviderTest
{
    protected function getProvider(): UserProviderInterface
    {
        return new TokenUserProvider(new AccessTokenRepository());
    }

    protected function getValidIdentifier(): string
    {
        /** @var CViewAccessToken $token */
        $token = $this->getObjectFromFixturesReference(CViewAccessToken::class, AuthenticationFixtures::API_TOKEN);

        return $token->hash;
    }

    protected function getInvalidIdentifier(): string
    {
        return 'invalid token';
    }

    protected function getValidCUser(): CUser
    {
        /** @var CViewAccessToken $token */
        $token = $this->getObjectFromFixturesReference(CViewAccessToken::class, AuthenticationFixtures::API_TOKEN);

        return $token->loadRefUser();
    }
}
