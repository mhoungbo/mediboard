<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Unit\Auth\Providers;

use Ox\Core\Auth\User;
use Ox\Mediboard\Admin\CUser;
use Ox\Tests\OxUnitTestCase;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

abstract class AbstractProviderTest extends OxUnitTestCase
{
    abstract protected function getProvider(): UserProviderInterface;

    abstract protected function getValidIdentifier(): string;

    abstract protected function getInvalidIdentifier(): string;

    /**
     * @dataProvider userClassesProvider
     *
     * @param string $class
     * @param bool   $expected
     *
     * @return void
     */
    public function testSupportsOnlyUser(string $class, bool $expected): void
    {
        $provider = $this->getProvider();

        $this->assertEquals($expected, $provider->supportsClass($class));
    }

    /**
     * @return void
     */
    public function testLoadByUsername(): void
    {
        $provider = $this->getProvider();

        $user = $provider->loadUserByUsername($this->getValidIdentifier());

        $this->assertUserIsValid($provider, $user);
    }

    public function testLoadUserByIdentifier(): void
    {
        $provider = $this->getProvider();

        $user = $provider->loadUserByIdentifier($this->getValidIdentifier());

        $this->assertUserIsValid($provider, $user);
    }

    /**
     * @dataProvider notFoundUserProvider
     *
     * @param string $username
     *
     * @return void
     */
    public function testLoadUserByUsernameWhenNotFoundUserThrowsAnException(string $username): void
    {
        $this->expectException(UserNotFoundException::class);

        $provider = $this->getProvider();
        $provider->loadUserByUsername($username);
    }

    /**
     * @dataProvider notFoundUserProvider
     *
     * @param string $username
     *
     * @return void
     */
    public function testLoadUserByIdentifierWhenNotFoundUserThrowsAnException(string $username): void
    {
        $this->expectException(UserNotFoundException::class);

        $provider = $this->getProvider();
        $provider->loadUserByIdentifier($username);
    }

    public function testRefreshValidUser(): void
    {
        $provider = $this->getProvider();

        $valid_user = new User($this->getValidCUser());

        $refreshed_user = $provider->refreshUser($valid_user);

        $this->assertUserIsValid($provider, $refreshed_user);
        $this->assertObjectEquals($valid_user, $refreshed_user);
    }

    /**
     * @dataProvider invalidUsersProvider
     *
     * @param UserInterface $user
     *
     * @return void
     */
    public function testRefreshInvalidUserThrowsAnException(UserInterface $user): void
    {
        $this->expectException(UnsupportedUserException::class);

        $provider = $this->getProvider();
        $provider->refreshUser($user);
    }

    public function testLoadValidOxUser(): void
    {
        $provider = $this->getProvider();

        $ox_user    = $this->getValidCUser();
        $valid_user = new User($ox_user);

        $actual_user = $provider->loadOxUser($valid_user);

        $this->assertEquals($ox_user->_id, $actual_user->_id);
    }

    /**
     * @dataProvider invalidUsersProvider
     *
     * @param UserInterface $user
     *
     * @return void
     */
    public function testLoadInvalidOxUserThrowsAnException(UserInterface $user): void
    {
        $this->expectException(UnsupportedUserException::class);

        $provider = $this->getProvider();
        $provider->loadOxUser($user);
    }

    /**
     * @param UserProviderInterface $provider
     * @param mixed                 $user
     *
     * @return void
     */
    protected function assertUserIsValid(UserProviderInterface $provider, $user): void
    {
        $this->assertIsObject($user);
        $this->assertTrue($provider->supportsClass(get_class($user)));

        $this->assertEquals($this->getValidIdentifier(), $user->getUserIdentifier());
        $this->assertNull($user->getPassword());

        $this->assertContains('ROLE_USER', $user->getRoles());
    }

    protected function getValidCUser(): CUser
    {
        $user                = new CUser();
        $user->user_username = $this->getValidIdentifier();
        $user->loadMatchingObjectEsc();

        return $user;
    }

    public function userClassesProvider(): array
    {
        return [
            'User Interface - not supported' => [UserInterface::class, false],
            'User - supported'               => [User::class, true],
            'CUser - not supported'          => [CUser::class, false],
            'Empty class - not supported'    => ['', false],
        ];
    }

    public function invalidUsersProvider(): array
    {
        return [
            'User Interface' => [$this->getMockBuilder(UserInterface::class)->getMock()],
        ];
    }

    public function notFoundUserProvider(): array
    {
        return [
            'Not found identifier' => [$this->getInvalidIdentifier()],
            'Empty identifier'     => [$this->getInvalidIdentifier()],
        ];
    }
}
