<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Resources\Controllers\Legacy;

class CLoremIpsumInheritLegacyController extends AbstractCLoremIpsumLegacyController
{
    public function action_inherit()
    {
    }
}
