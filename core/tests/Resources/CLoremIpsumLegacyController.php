<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Resources;

use Ox\Core\CLegacyController;

class CLoremIpsumLegacyController extends CLegacyController
{
    public function action_foo()
    {
    }

    private function action_bar()
    {
    }
}
