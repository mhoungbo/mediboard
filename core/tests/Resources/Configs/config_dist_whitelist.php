<?php

return [
    'root_dir'                              => '/var/www/mediboard',
    'product_name'                          => 'Produit',
    'company_name'                          => 'OpenXtrem',
    'page_title'                            => 'Mediboard SIH',
    'base_url'                              => 'http://localhost/mediboard/',
    'external_url'                          => 'http://localhost/mediboard/',
    'master_key_filepath'                   => '',
    'intercept_database_engine_instruction' => '0',
    'offline'                               => '0',
    'offline_non_admin'                     => '0',
    'instance_role'                         => 'qualif',
    'mb_id'                                 => '',
    'mb_oid'                                => '',
    'servers_ip'                            => '',
    'minify_javascript'                     => '1',
    'minify_css'                            => '1',
    'currency_symbol'                       => '&euro;',
    'ref_pays'                              => '1',
    'hide_confidential'                     => '0',
    'modal_windows_draggable'               => '0',
    'locale_warn'                           => '0',
    'locale_alert'                          => '^',
    'login_browser_check'                   => '0',
    'debug'                                 => '1',
    'readonly'                              => '0',
    'check_server_connectivity'             => '0',
    'dir_log_mediboard'                     => '',
    'log_datasource_metrics'                => '1',
    'log_access'                            => '1',
    'access_log_buffer_lifetime'            => '100',
    'aggregate_lifetime'                    => '100',
    'human_long_request_level'              => '10',
    'bot_long_request_level'                => '100',
    'log_all_queries'                       => '',
    'long_request_whitelist'                => '',
    'logged_handler_calls_list'             => '',
    'application_log_using_nosql'           => '0',
    'error_log_using_nosql'                 => '0',
    'activer_migration_log_to_action'       => '1',
    'activer_compression_diff'              => '1',
    'migration_log_to_action_probably'      => '100',
    'migration_log_to_action_nbr'           => '1000',
    'shared_memory'                         => 'none',
    'shared_memory_distributed'             => '',
    'shared_memory_params'                  => '',
    'session_handler'                       => 'files',
    'session_handler_mutex_type'            => 'files',
    'CAlert_purge_lifetime'                 => '100',
    'CAlert_purge_delay'                    => '90',
    'CViewAccessToken_purge_delay'          => '7',
    'CCronJobLog_purge_probability'         => '1000',
    'CCronJobLog_purge_delay'               => '30',
    'mutex_drivers'                         =>
        [
            'CMbRedisMutex' => '0',
            'CMbAPCMutex'   => '0',
            'CMbFileMutex'  => '1',
        ],
    'mutex_drivers_params'                  =>
        [
            'CMbRedisMutex' => '127.0.0.1:6379',
            'CMbFileMutex'  => '',
        ],
    'weinre_debug_host'                     => '',
    'offline_time_start'                    => '',
    'offline_time_end'                      => '',
    'issue_tracker_url'                     => '',
    'help_page_url'                         => '',
    'purify_text_input'                     => '0',
    'app_private_key_filepath'              => '',
    'app_public_key_filepath'               => '',
    'app_master_key_filepath'               => '',
    'anti_csrf_enable'                      => '1',
    'dataminer_limit'                       => '20',
    'merge_prevent_base_without_idex'       => '1',
    'aio_output_path'                       => '',
    'object_handlers'                       =>
        [
            'C3333TelHandler'      => '0',
            'CPassageHandler'      => '1',
            'CDoctolibHandler'     => '0',
            'CNotificationHandler' => '0',
            'CMbSyncHandler'       => '1',
        ],
    'index_handlers'                        =>
        [
        ],
    'eai_handlers'                          =>
        [
            'CSisraITI41Handler' => '1',
        ],
    'appbar_shortcuts'                      =>
        [
        ],
    'enslaving_active'                      => '1',
    'enslaving_ratio'                       => '100',
    'date'                                  => '%d/%m/%Y',
    'time'                                  => '%Hh%M',
    'datetime'                              => '%d/%m/%Y %Hh%M',
    'longdate'                              => '%A %d %B %Y',
    'longtime'                              => '%H heures %M minutes',
    'timezone'                              => 'Europe/Paris',
    'php'                                   =>
        [
        ],
    'db'                                    =>
        [
            'genericImport1'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport2'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport3'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport4'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport5'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport6'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport7'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport8'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport9'      =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'genericImport10'     =>
                [
                    'dbtype' => '',
                    'dbhost' => '',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'hl7v2'               =>
                [
                    'dbtype' => 'mysql',
                    'dbhost' => 'localhost',
                    'dbname' => 'hl7v2',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'osoft_import'        =>
                [
                    'dbtype' => 'oracle',
                    'dbhost' => 'localhost',
                    'dbname' => 'osoft',
                    'dbuser' => 'osoft',
                    'dbpass' => '',
                ],
            'oxLaboServer_import' =>
                [
                    'dbtype' => 'mysql',
                    'dbhost' => 'localhost',
                    'dbname' => 'oxLaboServer_import',
                    'dbuser' => 'oxLaboServer_import',
                    'dbpass' => '',
                ],
            'rpps_import'         =>
                [
                    'dbtype' => 'pdo_mysql',
                    'dbhost' => '127.0.0.1',
                    'dbname' => 'rpps',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'presta_ssr'          =>
                [
                    'dbtype' => 'mysql',
                    'dbhost' => 'localhost',
                    'dbname' => 'presta_ssr',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'surgica_import'      =>
                [
                    'dbtype' => 'pdo_mysql',
                    'dbhost' => 'localhost',
                    'dbname' => 'surgica',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
            'web100T'             =>
                [
                    'dbtype' => 'oracle',
                    'dbhost' => 'localhost',
                    'dbname' => '',
                    'dbuser' => '',
                    'dbpass' => '',
                ],
        ],
    'interop'                               =>
        [
            'mode_compat' => 'default',
        ],
    'ft'                                    =>
        [
            'default'            => '/usr/bin/strings',
            'application/msword' => '/usr/bin/strings',
            'text/html'          => '/usr/bin/strings',
            'application/pdf'    => '/usr/bin/pdftotext',
        ],
    'other_databases'                       => '',
    'achilles'                              =>
        [
            'mediuser_id'     => '',
            'document_path'   => '',
            'data_path'       => '',
            'import_tag_name' => 'achilles_import',
            'fixtext_mapping' =>
                [
                ],
        ],
    'admin'                                 =>
        [
            'CUser'                   =>
                [
                    'strong_password'                                 => '1',
                    'apply_all_users'                                 => '0',
                    'enable_ldap_specific_strong_password'            => '0',
                    'enable_admin_specific_strong_password'           => '0',
                    'max_login_attempts'                              => '5',
                    'lock_expiration_time'                            => '60',
                    'allow_change_password'                           => '1',
                    'force_changing_password'                         => '0',
                    'password_life_duration'                          => '3 month',
                    'reuse_password_probation_period'                 => 'none',
                    'coming_password_expiration_threshold'            => '',
                    'custom_password_recommendations'                 => '',
                    'force_inactive_old_authentification'             => 180,
                    'probability_force_inactive_old_authentification' => 100,
                ],
            'LDAP'                    =>
                [
                    'ldap_connection'                => '0',
                    'ldap_tag'                       => 'ldap',
                    'object_guid_mode'               => 'hexa',
                    'allow_change_password'          => '0',
                    'allow_login_as_admin'           => '0',
                    'check_ldap_password_expiration' => '1',
                ],
            'CPasswordSpec'           =>
                [
                    'strong_password_min_length'          => '6',
                    'strong_password_alpha_chars'         => '1',
                    'strong_password_upper_chars'         => '0',
                    'strong_password_num_chars'           => '1',
                    'strong_password_special_chars'       => '0',
                    'ldap_strong_password_min_length'     => '6',
                    'ldap_strong_password_alpha_chars'    => '1',
                    'ldap_strong_password_upper_chars'    => '0',
                    'ldap_strong_password_num_chars'      => '1',
                    'ldap_strong_password_special_chars'  => '0',
                    'admin_strong_password_min_length'    => '6',
                    'admin_strong_password_alpha_chars'   => '1',
                    'admin_strong_password_upper_chars'   => '0',
                    'admin_strong_password_num_chars'     => '1',
                    'admin_strong_password_special_chars' => '0',
                ],
            'CKerberosLdapIdentifier' =>
                [
                    'enable_kerberos_authentication' => '0',
                    'enable_login_button'            => '0',
                    'enable_automapping'             => '0',
                ],
            'ProSanteConnect'         =>
                [
                    'enable_psc_authentication' => '0',
                    'enable_login_button'       => '0',
                    'enable_automapping'        => '0',
                    'session_mode'              => '0',
                    'client_id'                 => '',
                    'client_secret'             => '',
                    'redirect_uri'              => '',
                ],
            'FranceConnect'           =>
                [
                    'enable_fc_authentication' => '0',
                    'enable_login_button'      => '0',
                    'client_id'                => '',
                    'client_secret'            => '',
                    'redirect_uri'             => '',
                    'logout_redirect_uri'      => '',
                ],
            'CRGPDConsent'            =>
                [
                    'user_id' => '',
                ],
            'CViewAccessToken'        =>
                [
                    'cron_name' => 'cronmajauto',
                    'modules'   => 'monitorClient',
                ],
        ],
    'apicrypt'                              =>
        [
            'keys_folder'            => '/var/apicrypt/',
            'ldap_checkout_interval' => '+1 MONTH',
        ],
    'appDeploy'                             =>
        [
        ],
    'appFine'                               =>
        [
            'tag'                        => 'appFine group:$g',
            'tag_evenement_medical'      => 'evenement_medical group:$g',
            'google_maps_key'            => 'AIzaSyBoZHYNI0qByGY1_oCWn0NbWsDE7vFaQ2o',
            'address_appFine'            => 'https://qualif.appfine.fr',
            'cache_duration_travel_time' => '15',
            'max_send_notification'      => '5',
            'reminder_email_patient'     => 4,
            'CViewAccessToken'           =>
                [
                    'token_lifetime'  => 4,
                    'token_max_usage' => 3,
                ],
            'CPatientUser'               =>
                [
                    'activation_max_attempts' => 3,
                ],
            'CAppFineNotif'              =>
                [
                    'thresold_date'  => 7,
                    'thresold_purge' => 100,
                    'mail_limit'     => 100,
                ],
            'CAppFineSync'               =>
                [
                    'enable_purge'   => '',
                    'purge_lifetime' => '100',
                    'months_to_keep' => '',
                    'token_lifetime' => '18',
                ],
            'CAppFineNotifDevice'        =>
                [
                    'api_access_key' => '',
                ],
            'CRGPDConsent'               =>
                [
                    'group_id_global_consent' => '',
                ],
            'Interop'                    =>
                [
                    'create_doctor' => 0,
                ],
            'locales'                    =>
                [
                    'locales_active' => '',
                ],
            'chat'                       =>
                [
                    'active'            => false,
                    'host'              => '',
                    'token'             => '',
                    'accelerator_host'  => '',
                    'accelerator_token' => '',
                ],
            'CAppFineAppointment'        =>
                [
                    'show_button_appointment' => 0,
                ],
        ],
    'appFineClient'                         =>
        [
            'tag'                           => 'appFine group:$g',
            'tag_responsable_count_appFine' => 'responsable_count_appFine',
            'age_minimal_acces_appFine'     => 16,
            'intervalle_dashboard'          => 7,
            'OID_appFine'                   => '',
            'address_portail_appfine'       => 'https://www.appfine.fr',
            'reminder_email_order_patient'  => 2,
        ],
    'atih'                                  =>
        [
        ],
    'axisante'                              =>
        [
            'function'           => '',
            'import_tag_name'    => 'axisante_import',
            'default_user'       => '',
            'dif_path'           => '',
            'sql_path'           => '',
            'csv_path'           => '',
            'doc_path'           => '',
            'doc_path_replace'   => '',
            'praticiens'         => '',
            'types_atcd'         => '',
            'file_date_min'      => '1970-01-01',
            'file_date_max'      => '2020-12-31',
            'xml_path'           => '',
            'import_file_path'   => '',
            'files_dir_path'     => '',
            'file_cat_ordo'      => '',
            'file_cat_courrier'  => '',
            'file_cat_doc_ext'   => '',
            'file_cat_exam_comp' => '',
        ],
    'barcodeDoc'                            =>
        [
        ],
    'bcb'                                   =>
        [
            'CBcbObject'              =>
                [
                    'dsn' => 'bcb',
                ],
            'CBcbClasseATC'           =>
                [
                    'niveauATC' => '2',
                ],
            'CBcbClasseTherapeutique' =>
                [
                    'niveauBCB' => '2',
                ],
            'CBcbProduit'             =>
                [
                    'unite_up'          => '0',
                    'activate_map_unit' => '0',
                ],
        ],
    'besco'                                 =>
        [
            'CBesco' =>
                [
                    'dsn' => 'besco',
                ],
        ],
    'bioserveur'                            =>
        [
            'url_service_web' => 'https://secure.bioserveur.com/bioserveur-access/ws_download.pl',
            'keep_zip_file'   => 0,
            'old_limit_cron'  => 10,
        ],
    'cda'                                   =>
        [
            'OID_root'         => '',
            'path_ghostscript' => '',
        ],
    'cegi'                                  =>
        [
            'emetteur'           => '',
            'tag_cegi'           => '',
            'import_prat_connu'  => 1,
            'functionPratImport' => 'Import',
            'StockFirst'         =>
                [
                    'tag'                   => 'CEGI StockFirst',
                    'mode_recover_products' => 'manuel',
                ],
            'imports'            =>
                [
                    'pat_csv_path'    => '',
                    'pat_start'       => 0,
                    'pat_count'       => 20,
                    'sejour_csv_path' => '',
                    'sejour_start'    => 0,
                    'sejour_count'    => 20,
                ],
        ],
    'cerfa'                                 =>
        [
        ],
    'cerveau'                               =>
        [
        ],
    'computerEngineering'                   =>
        [
        ],
    'connectathon'                          =>
        [
        ],
    'context'                               =>
        [
            'token_lifetime' => '',
        ],
    'dPImeds'                               =>
        [
            'tag' => 'imeds group:$g',
        ],
    'dPcim10'                               =>
        [
            'cim10_version' => 'oms',
        ],
    'dPfiles'                               =>
        [
            'CFile'              =>
                [
                    'upload_directory'         => 'files',
                    'upload_directory_private' => '',
                    'signature_filename'       => '',
                    'ooo_active'               => '0',
                    'python_path'              => '',
                    'migration_limit'          => '100',
                    'migration_ratio'          => '10',
                    'migration_finished'       => '0',
                    'migration_started'        => '0',
                    'prefix_format'            => '',
                    'prefix_format_qualif'     => '',
                    'hierarchy'                => '2,2,2',
                ],
            'CThumbnail'         =>
                [
                    'gs_alias' => 'gs',
                ],
            'import_dir'         => '',
            'import_mediuser_id' => '',
        ],
    'dPmedicament'                          =>
        [
            'prefix_crc'                     => '',
            'CBcbProduitLivretTherapeutique' =>
                [
                    'product_category_id' => '',
                ],
        ],
    'dPpatients'                            =>
        [
            'CPatient'             =>
                [
                    'function_distinct'  => '0',
                    'tag_ipp'            => '',
                    'tag_ipp_group_idex' => '',
                    'tag_ipp_trash'      => 'trash_',
                    'tag_conflict_ipp'   => 'conflict_',
                ],
            'CConstantesMedicales' =>
                [
                    'unite_ta'        => 'cmHg',
                    'unite_glycemie'  => 'g/l',
                    'unite_cetonemie' => 'g/l',
                ],
            'CAntecedent'          =>
                [
                    'types'           => 'med|alle|trans|obst|chir|fam|anesth|gyn',
                    'mandatory_types' => '',
                    'appareils'       => 'cardiovasculaire|digestif|endocrinien|neuro_psychiatrique|pulmonaire|uro_nephrologique',
                ],
            'CTraitement'          =>
                [
                    'enabled' => '1',
                ],
            'CDossierMedical'      =>
                [
                ],
            'imports'              =>
                [
                    'pat_csv_path'    => '',
                    'pat_start'       => 0,
                    'pat_count'       => 20,
                    'sejour_csv_path' => '',
                ],
            'INSEE'                =>
                [
                    'france'    => '1',
                    'suisse'    => '0',
                    'allemagne' => '0',
                    'espagne'   => '0',
                    'portugal'  => '0',
                    'gb'        => '0',
                    'belgique'  => '0',
                ],
            'import_tag'           => '',
            'file_date_min'        => '1970-01-01',
            'file_date_max'        => '2020-12-31',
        ],
    'dPplanningOp'                          =>
        [
            'COperation'          =>
                [
                    'mode_easy'                 => '1col',
                    'easy_materiel'             => '0',
                    'easy_remarques'            => '0',
                    'easy_regime'               => '1',
                    'easy_accident'             => '0',
                    'easy_assurances'           => '0',
                    'easy_type_anesth'          => '0',
                    'easy_length_input_label'   => '50',
                    'easy_rrac'                 => '0',
                    'duree_deb'                 => '0',
                    'duree_fin'                 => '10',
                    'hour_urgence_deb'          => '0',
                    'hour_urgence_fin'          => '23',
                    'min_intervalle'            => '15',
                    'locked'                    => '0',
                    'horaire_voulu'             => '0',
                    'use_ccam'                  => '1',
                    'verif_cote'                => '0',
                    'delete_only_admin'         => '1',
                    'duree_preop_adulte'        => '45',
                    'duree_preop_enfant'        => '15',
                    'show_duree_preop'          => '0',
                    'show_duree_uscpo'          => '0',
                    'save_rank_annulee_validee' => '0',
                    'cancel_only_for_resp_bloc' => '0',
                    'fiche_examen'              => '0',
                    'fiche_materiel'            => '0',
                    'fiche_rques'               => '0',
                    'nb_jours_urgence'          => '1',
                    'use_poste'                 => '0',
                    'show_secondary_function'   => '0',
                    'show_presence_op'          => '1',
                    'show_remarques'            => '1',
                    'show_montant_dp'           => '1',
                    'show_asa_position'         => '1',
                    'show_print_dhe_info'       => '1',
                    'default_week_stat_uscpo'   => 'last',
                    'use_session_praticien'     => true,
                ],
            'CSejour'             =>
                [
                    'easy_cim10'                       => '0',
                    'easy_service'                     => '0',
                    'easy_uf_soins'                    => '0',
                    'easy_chambre_simple'              => '1',
                    'easy_ald_c2s'                     => '0',
                    'easy_atnc'                        => '0',
                    'easy_mode_sortie'                 => '0',
                    'easy_entree_sortie'               => '0',
                    'patient_id'                       => '1',
                    'entree_modifiee'                  => '1',
                    'heure_deb'                        => '0',
                    'heure_fin'                        => '23',
                    'min_intervalle'                   => '15',
                    'locked'                           => '0',
                    'tag_dossier'                      => '',
                    'tag_dossier_group_idex'           => '',
                    'tag_dossier_pa'                   => 'pa_',
                    'tag_dossier_cancel'               => 'cancel_',
                    'tag_dossier_trash'                => 'trash_',
                    'tag_dossier_rang'                 => '',
                    'use_dossier_rang'                 => '0',
                    'blocage_occupation'               => '0',
                    'service_id_notNull'               => '0',
                    'consult_accomp'                   => '0',
                    'delete_only_admin'                => '1',
                    'max_cancel_time'                  => '0',
                    'hours_sejour_proche'              => '48',
                    'show_modal_identifiant'           => '0',
                    'show_discipline_tarifaire'        => '0',
                    'show_type_pec'                    => '0',
                    'create_anonymous_pat'             => '0',
                    'anonymous_sexe'                   => 'm',
                    'anonymous_naissance'              => '1970-12-31',
                    'use_recuse'                       => '0',
                    'systeme_isolement'                => 'standard',
                    'easy_isolement'                   => '0',
                    'show_only_charge_price_indicator' => '0',
                    'use_custom_mode_entree'           => '0',
                    'use_custom_mode_sortie'           => '0',
                    'specified_output_mode'            => '1',
                    'show_confirm_change_patient'      => '1',
                    'use_session_praticien'            => '0',
                ],
            'CPatient'            =>
                [
                    'easy_correspondant'  => '0',
                    'easy_tutelle'        => '0',
                    'easy_handicap'       => '0',
                    'easy_aide_organisee' => '0',
                ],
            'CRegleSectorisation' =>
                [
                    'use_sectorisation' => '0',
                ],
        ],
    'dPpmsi'                                =>
        [
            'systeme_facturation' => '',
            'server'              => '0',
            'transmission_actes'  => 'pmsi',
            'passage_facture'     => 'envoi',
            'use_cim_pmsi'        => '0',
        ],
    'dPprescription'                        =>
        [
            'CPrescription'         =>
                [
                    'scores'         =>
                        [
                            'interaction' =>
                                [
                                    'niv1' => '1',
                                    'niv2' => '1',
                                    'niv3' => '2',
                                    'niv4' => '2',
                                ],
                            'posoqte'     =>
                                [
                                    'niv10' => '0',
                                    'niv11' => '1',
                                    'niv12' => '2',
                                ],
                            'posoduree'   =>
                                [
                                    'niv20' => '0',
                                    'niv21' => '1',
                                    'niv22' => '2',
                                ],
                            'profil'      =>
                                [
                                    'niv0'  => '1',
                                    'niv1'  => '1',
                                    'niv2'  => '1',
                                    'niv9'  => '1',
                                    'niv30' => '1',
                                    'niv39' => '1',
                                ],
                            'allergie'    => '2',
                            'IPC'         => '2',
                            'hors_livret' => '1',
                        ],
                    'manual_planif'  => '0',
                    'duree_ref_perf' => '24',
                ],
            'CCategoryPrescription' =>
                [
                    'dmi'      =>
                        [
                            'phrase'      => 'Bon pour',
                            'unite_prise' => 'dispositif (s)',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'anapath'  =>
                        [
                            'phrase'      => 'Faire pratiquer',
                            'unite_prise' => 'examen(s)',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'biologie' =>
                        [
                            'phrase'      => 'Faire pratiquer',
                            'unite_prise' => 'examen(s)',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'imagerie' =>
                        [
                            'phrase'      => 'Faire pratiquer',
                            'unite_prise' => 'clich�(s)',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'consult'  =>
                        [
                            'phrase'      => '',
                            'unite_prise' => 'consultation(s)',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'kine'     =>
                        [
                            'phrase'      => 'Faire pratiquer',
                            'unite_prise' => 's�ance(s)',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'soin'     =>
                        [
                            'phrase'      => 'Pratiquer',
                            'unite_prise' => 'soin(s)',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'dm'       =>
                        [
                            'phrase'      => 'D�livrer',
                            'unite_prise' => 'dispositif (s)',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'ds'       =>
                        [
                            'phrase'      => '',
                            'unite_prise' => '',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'med_elt'  =>
                        [
                            'phrase'      => '',
                            'unite_prise' => '',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'mc'       =>
                        [
                            'phrase'      => '',
                            'unite_prise' => '',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                    'psy'      =>
                        [
                            'phrase'      => '',
                            'unite_prise' => '',
                            'fin_sejour'  => '0',
                            'cible_trans' => '1',
                        ],
                ],
        ],
    'dPqualite'                             =>
        [
            'CFicheEi'     =>
                [
                    'mode_anonyme' => 0,
                ],
            'CDocGed'      =>
                [
                    '_reference_doc' => 0,
                ],
            'CChapitreDoc' =>
                [
                    'profondeur' => 1,
                ],
        ],
    'dPsante400'                            =>
        [
            'nb_rows'      => '5',
            'mark_row'     => '0',
            'cache_hours'  => '1',
            'prefix'       => 'odbc',
            'dsn'          => '',
            'other_dsn'    => '',
            'user'         => '',
            'pass'         => '',
            'group_id'     => '',
            'fix_encoding' => '0',
            'CSejour'      =>
                [
                    'sibling_hours' => 1,
                ],
            'CIncrementer' =>
                [
                    'cluster_count'    => 1,
                    'cluster_position' => 0,
                ],
        ],
    'dPstock'                               =>
        [
            'host_group_id' => '',
            'general'       =>
                [
                    'unite_disp' => '0',
                ],
            'CProductOrder' =>
                [
                    'order_number_format'     => '%y%m%d%H%M%id',
                    'order_number_contextual' => '0',
                ],
        ],
    'dPurgences'                            =>
        [
            'date_tolerance'                       => '2',
            'old_rpu'                              => '0',
            'rpu_warning_time'                     => '00:20:00',
            'rpu_alert_time'                       => '01:00:00',
            'default_view'                         => 'tous',
            'allow_change_patient'                 => '1',
            'age_patient_rpu_view'                 => '0',
            'responsable_rpu_view'                 => '1',
            'sortie_prevue'                        => 'sameday',
            'only_prat_responsable'                => '0',
            'gerer_reconvoc'                       => '1',
            'sibling_hours'                        => '0',
            'pec_change_prat'                      => '1',
            'pec_after_sortie'                     => '0',
            'create_sejour_hospit'                 => '0',
            'hide_reconvoc_sans_sortie'            => '0',
            'show_statut'                          => '0',
            'attente_first_part'                   => '00:30:00',
            'attente_second_part'                  => '02:00:00',
            'attente_third_part'                   => '04:00:00',
            'gerer_circonstance'                   => '0',
            'valid_cotation_sortie_reelle'         => '1',
            'display_regule_par'                   => '0',
            'view_rpu_uhcd'                        => '0',
            'use_blocage_lit'                      => '0',
            'create_affectation'                   => '1',
            'main_courante_refresh_frequency'      => '90',
            'uhcd_refresh_frequency'               => '180',
            'imagerie_refresh_frequency'           => '180',
            'identito_vigilance_refresh_frequency' => '300',
            'avis_maternite_refresh_frequency'     => '180',
            'use_vue_topologique'                  => '1',
            'vue_topo_refresh_frequency'           => '90',
            'CExtractPassages'                     =>
                [
                    'purge_probability'      => '100',
                    'purge_empty_threshold'  => '28',
                    'purge_delete_threshold' => '168',
                ],
        ],
    'dicom'                                 =>
        [
            'implementation_version'   => 'Mediboard',
            'implementation_sop_class' => '1.2.17.000.4',
        ],
    'directConsult'                         =>
        [
            'tag'                  => 'directConsult group:$g',
            'max_files_to_process' => '20',
        ],
    'dispensation'                          =>
        [
        ],
    'dmp'                                   =>
        [
            'LPS_Nom'                               => '',
            'LPS_Version'                           => '',
            'LPS_ID_HOMOLOGATION_DMP'               => '',
            'LPS_ID'                                => '',
            'URL_DMP'                               => 'https://dev.lps.dmp.gouv.fr/si-dmp-server/v2/services',
            'NIR_OID'                               => '1.2.250.1.213.1.4.10',
            'Fonction_Gestion_Mineur'               => 'false',
            'Age_Majorite'                          => '18',
            'Cumul_Invisible_Patient_Masque_PS'     => 'false',
            'URL_formulaire'                        => 'http://www.dmp.gouv.fr/web/dmp/documents/param-lps',
            'MAJ_formulaire_DocumentAccuseCreation' => '',
            'MAJ_formulaire_DocumentSecret'         => '',
            'URL_webPS'                             => '',
        ],
    'docapost'                              =>
        [
            'docapost_login'    => '',
            'docapost_password' => '',
        ],
    'doctolib'                              =>
        [
        ],
    'dpe'                                   =>
        [
            'col_patient_ipp'            => '',
            'col_patient_prenom'         => '',
            'col_patient_nom'            => '',
            'col_patient_nom_naissance'  => '',
            'col_patient_date_naissance' => '',
            'col_patient_sexe'           => '',
            'col_patient_numero_secu'    => '',
            'col_patient_civilite'       => '',
            'col_patient_adresse1'       => '',
            'col_patient_adresse2'       => '',
            'col_patient_cp'             => '',
            'col_patient_ville'          => '',
            'col_sejour_nda'             => '',
            'col_sejour_date_entree'     => '',
            'col_sejour_date_sortie'     => '',
            'col_sejour_service'         => '',
            'col_sejour_diagnostic'      => '',
            'col_sejour_pathologie'      => '',
            'col_responsable_document'   => '',
            'import_segment'             => '100',
            'import_id_min'              => '',
            'import_id_max'              => '',
        ],
    'ePrescription'                         =>
        [
        ],
    'eai'                                   =>
        [
            'exchange_format_delayed'        => '30',
            'max_files_to_process'           => '50',
            'max_reprocess_retries'          => '5',
            'use_domain'                     => '0',
            'use_routers'                    => '0',
            'send_messages_with_same_group'  => '0',
            'tunnel_pass'                    => '0',
            'message_supported'              => '0',
            'nb_max_export_csv'              => '100',
            'nb_files_retention_mb_excludes' => '30',
            'CExchangeDataFormat'            =>
                [
                    'purge_probability'      => '100',
                    'purge_empty_threshold'  => '28',
                    'purge_delete_threshold' => '168',
                ],
            'CExchangeTransportLayer'        =>
                [
                    'purge_probability'      => '100',
                    'purge_empty_threshold'  => '28',
                    'purge_delete_threshold' => '168',
                ],
        ],
    'ecap'                                  =>
        [
            'WebServices' =>
                [
                    'user_login_prefix' => '',
                ],
        ],
    'eds'                                   =>
        [
        ],
    'enovacom'                              =>
        [
            'tag_user' => 'cpsure group:$g',
        ],
    'fhir'                                  =>
        [
            'tag_default' => '',
            'version'     => '3.0',
        ],
    'forms'                                 =>
        [
            'CExClassField' =>
                [
                    'force_concept'            => 1,
                    'doc_template_integration' => 0,
                ],
            'CExConcept'    =>
                [
                    'force_list'   => 1,
                    'native_field' => 0,
                ],
            'CExClass'      =>
                [
                    'pixel_positionning'              => 0,
                    'pixel_layout_delimiter'          => '1',
                    'show_color_score_form'           => 1,
                    'check_modification_before_close' => 1,
                    'display_list_readonly'           => 0,
                    'allowing_additional_columns'     => 0,
                ],
        ],
    'fse'                                   =>
        [
            'oxPyxvital' =>
                [
                    'instance_key' => '',
                ],
        ],
    'galaxie'                               =>
        [
            'tag_agenda' => 'galaxie_agenda group:$g',
            'tag_token'  => 'galaxie_token group:$g',
        ],
    'Generix'                               =>
        [
        ],
    'groupSplitter'                         =>
        [
        ],
    'hl7'                                   =>
        [
            'assigning_authority_namespace_id'      => 'Mediboard',
            'assigning_authority_universal_id'      => '1.2.250.1.2.3.4',
            'assigning_authority_universal_type_id' => 'OX',
            'sending_application'                   => 'Mediboard',
            'sending_facility'                      => 'Mediboard',
            'strictSejourMatch'                     => '1',
            'indeterminateDoctor'                   => 'Medecin ind�termin�',
            'doctorActif'                           => '0',
            'importFunctionName'                    => 'Import',
            'type_antecedents_adt_a60'              => 'alle',
            'appareil_antecedents_adt_a60'          => '',
            'default_version'                       => '2.5',
            'default_fr_version'                    => 'FRA_2.5',
            'CHL7v2Segment'                         =>
                [
                    'PV1_3_2'                     => '',
                    'PV1_3_3'                     => '',
                    'ignore_unexpected_z_segment' => '0',
                ],
            'tag_default'                           => '',
        ],
    'hospitalis'                            =>
        [
        ],
    'hprim21'                               =>
        [
            'CHprim21Reader'            =>
                [
                    'hostname'      => '',
                    'username'      => '',
                    'userpass'      => '',
                    'fileextension' => 'hpr',
                ],
            'mandatory_num_dos_ipp_adm' => '1',
            'tag'                       => '',
        ],
    'hprimsante'                            =>
        [
            'default_version'           => 'H2.3',
            'mandatory_num_dos_ipp_adm' => '1',
            'tag'                       => '',
            'sending_application'       => '',
            'importFunctionName'        => 'Import',
            'doctorActif'               => '0',
        ],
    'hprimxml'                              =>
        [
            'evt_serveuractes'           =>
                [
                    'validation' => '0',
                    'version'    => '1.01',
                    'send_ack'   => '1',
                ],
            'evt_pmsi'                   =>
                [
                    'validation' => '0',
                    'version'    => '1.01',
                    'send_ack'   => '1',
                ],
            'evt_serveuretatspatient'    =>
                [
                    'validation' => '0',
                    'version'    => '1.05',
                    'send_ack'   => '1',
                ],
            'evt_frais_divers'           =>
                [
                    'validation' => '0',
                    'version'    => '1.05',
                    'send_ack'   => '1',
                ],
            'evt_serveurintervention'    =>
                [
                    'validation' => '0',
                    'version'    => '1.072',
                    'send_ack'   => '1',
                ],
            'evt_patients'               =>
                [
                    'validation' => '0',
                    'version'    => '1.05',
                    'send_ack'   => '1',
                ],
            'evt_mvtStock'               =>
                [
                    'validation' => '0',
                    'version'    => '1.01',
                    'send_ack'   => '1',
                ],
            'functionPratImport'         => 'Import',
            'medecinIndetermine'         => 'Medecin Indetermin�',
            'medecinActif'               => '0',
            'user_type'                  => '13',
            'strictSejourMatch'          => '1',
            'notifier_sortie_reelle'     => '1',
            'notifier_entree_reelle'     => '1',
            'trash_numdos_sejour_cancel' => '0',
            'code_transmitter_sender'    => 'mb_id',
            'code_receiver_sender'       => 'dest',
            'date_heure_acte'            => 'operation',
            'concatenate_xsd'            => '0',
            'mvtComplet'                 => '0',
            'send_diagnostic'            => 'evt_pmsi',
            'actes_ngap_excludes'        => '',
            'tag_default'                => '',
            'send_only_das_diags'        => '0',
            'use_recueil'                => '0',
        ],
    'HubDocumentaire'                       =>
        [
        ],
    'ihe'                                   =>
        [
            'RAD-3' =>
                [
                    'function_ids' => '',
                ],
        ],
    'jfse'                                  =>
        [
        ],
    'lifeline'                              =>
        [
            'api_url' => '',
        ],
    'livi'                                  =>
        [
        ],
    'mediusers'                             =>
        [
            'import_tag' => 'migration',
        ],
    'messagerie'                            =>
        [
            'CronJob_nbMail'                  => '5',
            'CronJob_schedule'                => '3',
            'CronJob_olderThan'               => '5',
            'hprimnet_key_directory'          => '',
            'hprimnet_certificates_directory' => '',
        ],
    'mondialSante'                          =>
        [
            'service_url'  => 'https://87.98.146.220/HPS/ws/webservice.php5',
            'username'     => '',
            'password'     => '',
            'debug_traces' => 0,
        ],
    'monitorClient'                         =>
        [
            'instance_uuid'           => '',
            'enable_changelog_viewer' => '',
        ],
    'monitorServer'                         =>
        [
            'enable_last_probe_alerts'                              => '0',
            'enable_notifications'                                  => '0',
            'recalls_period'                                        => '1',
            'recalls_frequency'                                     => '15',
            'CMonitorSystemInfo_notification_validation_period'     => '3',
            'CMonitorApacheInfo_notification_validation_period'     => '3',
            'CMonitorMysqlInfo_notification_validation_period'      => '3',
            'CMonitorPartitionsInfo_notification_validation_period' => '12',
            'CMonitorBackupInfo_notification_validation_period'     => '3',
            'CMonitorMountPointInfo_notification_validation_period' => '12',
            'CMonitorInstanceInfo_notification_validation_period'   => '12',
            'CMonitorSources_notification_validation_period'        => '12',
            'CMonitorDataSourceInfo_notification_validation_period' => '3',
            'CMonitorMBDataItem_notification_validation_period'     => '12',
            'CMonitorAccessLogs_notification_validation_period'     => '12',
            'notification_type'                                     => 'email',
            'notification_email_recipient'                          => '',
            'aggregation_lifetime'                                  => '100',
            'log_aggregation'                                       => '1',
            'audio_alarm_music'                                     => 'half-life',
            'OID_root'                                              => '',
        ],
    'monitoringBloc'                        =>
        [
        ],
    'monitoringMaternite'                   =>
        [
        ],
    'MonitoringPatient'                     =>
        [
        ],
    'mssante'                               =>
        [
            'idp_otp_url'            => 'https://mss-idp.mssante.fr/openam/SSOSoap/metaAlias/asip/idp',
            'folder_service_url'     => 'https://mss-msg.mssante.fr/mss-msg-services/services/Folder/soap/v1?wsdl',
            'item_service_url'       => 'https://mss-msg.mssante.fr/mss-msg-services/services/Item/soap/v1?wsdl',
            'attachment_service_url' => 'https://mss-msg.mssante.fr/mss-msg-services/services/Attachment/soap/v1?wsdl',
            'directory_service_url'  => 'https://mss-msg.mssante.fr/mss-msg-services/services/Annuaire/soap/v1?wsdl',
            'ldap_url'               => 'ldap.annuaire.mssante.fr',
            'ldap_port'              => '389',
            'debug_traces'           => '0',
        ],
    'novxtelHospitality'                    =>
        [
        ],
    'orthomax'                              =>
        [
            'import_tag' => 'orthomax_import',
            'xml_path'   => '',
        ],
    'orutsbn'                               =>
        [
        ],
    'oscour'                                =>
        [
        ],
    'osoft'                                 =>
        [
            'version'              => 'A',
            'patient_nom_jf_mode'  => 'marital_usuel',
            'pat_anteced_to_rques' => '0',
            'fix_encoding'         => 'macroman',
            'medecin_traitant'     => '1',
            'img_mount_point'      => '/mnt/echange',
            'img_paths'            => '',
            'import_tag'           => 'osoft_import',
            'csv_path'             => '',
            'xml_path'             => '',
            'file_date_min'        => '1970-01-01',
            'file_date_max'        => '2020-12-31',
            'link_to_user'         => '0',
            'import_ipp'           => '0',
            'import_nda'           => '0',
            'old_ipp_tag'          => 'osoft_ipp',
            'old_nda_tag'          => 'osoft_nda',
        ],
    'oural'                                 =>
        [
        ],
    'oxCRM'                                 =>
        [
        ],
    'oxCabinet'                             =>
        [
        ],
    'oxContract'                            =>
        [
            'COXModuleTechnique' =>
                [
                    'GPL_path'  => '',
                    'OXOL_path' => '',
                ],
            'COXContrat'         =>
                [
                    'check_line_integrity' => '',
                ],
        ],
    'oxDeploy'                              =>
        [
            'ssh_user'        => '',
            'ssh_host'        => '',
            'ssh_private_key' => '',
            'release_dir'     => '',
            'bundles_dir'     => '',
        ],
    'oxEDM'                                 =>
        [
            'COXDocument' =>
                [
                    'function_id_access_document_tamm' => '',
                ],
        ],
    'oxERP'                                 =>
        [
            'ox_function_id'         => '',
            'kh_function_id'         => '',
            'idem_sante_function_id' => '',
        ],
    'oxExploitation'                        =>
        [
            'oxExploitation_team_id' => '',
            'oxInterop_team_id'      => '',
            'function_id_ox'         => '',
            'interop_team'           => '',
            'interop_lead'           => '',
            'exploitation_team'      => '',
            'exploitation_lead'      => '',
            'internal_email'         => '',
            'notifications'          =>
                [
                    'COXOperation'        =>
                        [
                            'plan'   => '',
                            'replan' => '',
                            'cancel' => '',
                            'start'  => '',
                            'end'    => '',
                        ],
                    'COXUpdate'           =>
                        [
                            'plan'   => '',
                            'replan' => '',
                            'cancel' => '',
                            'start'  => '',
                            'end'    => '',
                        ],
                    'COXProdUpdate'       =>
                        [
                            'plan'   => '',
                            'replan' => '',
                            'cancel' => '',
                            'start'  => '',
                            'end'    => '',
                        ],
                    'COXTrainingUpdate'   =>
                        [
                            'plan'   => '',
                            'replan' => '',
                            'cancel' => '',
                            'start'  => '',
                            'end'    => '',
                        ],
                    'COXTestingUpdate'    =>
                        [
                            'plan'   => '',
                            'replan' => '',
                            'cancel' => '',
                            'start'  => '',
                            'end'    => '',
                        ],
                    'COXInteropOperation' =>
                        [
                            'plan'   => '',
                            'replan' => '',
                            'cancel' => '',
                            'start'  => '',
                            'end'    => '',
                        ],
                ],
        ],
    'oxFAC'                                 =>
        [
            'oxFAC_team_id'                 => '',
            'oxFAC_team_additional_members' => '',
            'convention_id'                 => '',
            'attestation_id'                => '',
            'convocation_id'                => '',
            'emargement_id'                 => '',
            'ordre_mission_id'              => '',
            'qsc_id'                        => '',
            'evaluation_id'                 => '',
            'heures_par_jour'               => 7,
            'seuil_avertissement'           => 75,
            'seuil_1_semaine'               => 3,
            'seuil_4_semaines'              => 8,
            'seuil_8_semaines'              => 15,
        ],
    'oxFacturation'                         =>
        [
            'OX'      =>
                [
                    'type_societe'  => '',
                    'title'         => '',
                    'adresse'       => '29 rue Nicolas Denys de Fronsac',
                    'ville'         => 'La Rochelle',
                    'cp'            => '17000',
                    'tel'           => '',
                    'SIRET'         => '',
                    'SIREN'         => '',
                    'APE'           => '',
                    'num_tva'       => '',
                    'domiciliation' => '',
                    'prefix_iban'   => '',
                    'bic'           => '',
                    'iban'          => '',
                    'code_banque'   => '',
                    'code_guichet'  => '',
                    'num_compte'    => '',
                    'cle_rib'       => '',
                    'ics'           => '',
                    'capital'       => '',
                ],
            'Other'   =>
                [
                    'nom_pole'         => '',
                    'type_pole'        => 'test',
                    'nb_min_use_bill'  => '10',
                    'vw_emprunte'      => '0',
                    'default_taux_tva' => '0|5.5|7|7.5|10|19.6|20',
                    'type_use'         => 'package_prat',
                    'create_auto_num'  => '0',
                    'nb_mois_parraine' => '3',
                    'pct_parraine'     => '50',
                    'nb_mois_parrain'  => '3',
                    'pct_parrain'      => '100',
                ],
            'Message' =>
                [
                    'default_email_from' => '',
                    'text_email'         => '',
                ],
        ],
    'oxHotline'                             =>
        [
            'ox_function_id' => '',
        ],
    'oxImport'                              =>
        [
            'oxImport_team_id' => '',
        ],
    'oxLaboClient'                          =>
        [
        ],
    'oxPipeline'                            =>
        [
        ],
    'oxPresta'                              =>
        [
            'hours_per_day'        => 8,
            'known_executors'      => '',
            'COXLignePrestation'   =>
                [
                    'round_upper'         => 15,
                    'expense_category_id' => '',
                ],
            'OXForfaitDeplacement' =>
                [
                    'zones_list' => '',
                ],
        ],
    'oxProject'                             =>
        [
        ],
    'oxProspect'                            =>
        [
        ],
    'oxSupport'                             =>
        [
            'oxSupport_team_id'      => '',
            'COXDemandeAssistance'   =>
                [
                    'enable_assistance_reminder' => '1',
                ],
            'COXDemandeExploitation' =>
                [
                    'enable_notifications' => '1',
                ],
            'COXAssistanceMessage'   =>
                [
                    'enable_revert_time' => '',
                    'revert_time'        => 20,
                ],
        ],
    'phast'                                 =>
        [
            'tag_phast'         => '',
            'STS_prefix'        => 'STS_2_2_',
            'STS_Base_id'       => '2.16.840.1.113883.3.81.1.1.34.0',
            'STS_User_id'       => '',
            'STS_Pin_code'      => '',
            'STS_Time_limit'    => '0',
            'STS_namespace'     => '',
            'PN13'              =>
                [
                    'validation'            => '1',
                    'version'               => '2.3.12',
                    'application_interface' => '',
                    'ack_version'           => '0.1',
                ],
            'CPhastXMLDocument' =>
                [
                    'type_elt_livr_group_idex' => '',
                    'type_compo_group_idex'    => '',
                    'code_compo_group_idex'    => '',
                    'code_compo_required'      => '1',
                ],
        ],
    'planSoins'                             =>
        [
            'CPlanificationSysteme' =>
                [
                    'purge_lifetime'        => '1000',
                    'purge_limit'           => '100',
                    'purge_delay'           => '90',
                    'purge_lifetime_planif' => '1000',
                    'purge_limit_planif'    => '20',
                ],
        ],
    'populate'                              =>
        [
            'CGroups_max_count'         => '3',
            'CFunctions_max_count'      => '10',
            'CMediusers_max_count'      => '100',
            'CPatient_pct_create'       => '97',
            'zip_codes'                 => '',
            'CPatient_max_years_old'    => '100',
            'CPatient_age_min'          => '10',
            'CPatient_atcd_max_count'   => '4',
            'CPatient_atcd_min_count'   => '0',
            'CSejour_pct_ambu'          => '75',
            'CSejour_max_days_hospi'    => '4',
            'CSejour_max_years_start'   => '5',
            'CSejour_max_years_end'     => '1',
            'CSejour_min_file_count'    => '1',
            'CSejour_max_file_count'    => '5',
            'CSejour_min_mono_count'    => '1',
            'CSejour_max_mono_count'    => '1',
            'CTransmission_min_count'   => '0',
            'CTransmission_max_count'   => '10',
            'CObservation_min_count'    => '0',
            'CObservation_max_count'    => '3',
            'CService_max_count'        => '10',
            'CChambre_max_count'        => '20',
            'CLit_max_count'            => '2',
            'CBlocOperatoire_max_count' => '4',
            'CSalle_max_count'          => '10',
            'CPlageOp_date_max'         => '1',
            'CPlageOp_date_min'         => '1',
        ],
    'pyxvitalManager'                       =>
        [
            'instance_service_url' => '',
            'CLicence'             =>
                [
                    'recipient'                 => 'com@pyxistem.com',
                    'cc_recipients'             => 'fse@openxtrem.com',
                    'command_subject'           => 'Commande de licences Pyxvital',
                    'renew_subject'             => 'Renouvellement de licences Pyxvital',
                    'modify_subject'            => 'Modification de num�ro de facturation',
                    'cancel_subject'            => 'Annulation de licence',
                    'user_notification_subject' => 'Cl� d\'activation Pyxvital',
                    'send_user_notification'    => '1',
                    'licence_price'             => '100',
                    'transmission_domain'       => 'oxfse.com',
                ],
            'CLicenceOrder'        =>
                [
                    'order_number' => '92',
                ],
        ],
    'radiologie'                            =>
        [
        ],
    'recipe'                                =>
        [
        ],
    'releaseNotes'                          =>
        [
        ],
    'ror'                                   =>
        [
            'pattern_keyinfo'         => '',
            'gnupg_path'              => '',
            'affected_region'         => '',
            'version_complete'        => '',
            'position_encryption_key' => '',
            'index_encryption_key'    => '0',
            'rpu_xml_validation'      => '1',
        ],
    'rpps'                                  =>
        [
            'download_directory'           => '',
            'sync_step'                    => '50',
            'disable_days_withtout_update' => '30',
        ],
    'sa'                                    =>
        [
            'server' => '0',
        ],
    'sas'                                   =>
        [
            'nb_attempt_send' => '3',
        ],
    'search'                                =>
        [
            'CConfigEtab'               =>
                [
                    'active_indexing'       => '0',
                    'active_handler_search' => '0',
                    'active_search_history' => '1',
                ],
            'nb_replicas'               => '1',
            'interval_indexing'         => '100',
            'history_purge_probability' => '100',
            'history_purge_day'         => '14',
            'obfuscation_body'          => '0',
        ],
    'sip'                                   =>
        [
            'wsdl_mode'       => '1',
            'export_dest'     => '',
            'export_segment'  => '100',
            'export_id_min'   => '',
            'export_id_max'   => '',
            'export_date_min' => '',
            'export_date_max' => '',
            'batch_count'     => '10',
            'pat_no_ipp'      => '0',
            'tag_ipp'         => 'mb_sip',
        ],
    'smp'                                   =>
        [
            'export_dest'         => '',
            'export_segment'      => '100',
            'export_id_min'       => '',
            'export_id_max'       => '',
            'export_date_min'     => '',
            'export_date_max'     => '',
            'batch_count'         => '10',
            'sej_no_numdos'       => '0',
            'send_sej_pa'         => '1',
            'verify_repair'       => '1',
            'repair_segment'      => '100',
            'repair_date_min'     => '',
            'repair_date_max'     => '',
            'tag_nda'             => 'mb_smp',
            'tag_visit_number'    => 'vn group:$g',
            'send_mvt'            => '0',
            'create_object_by_vn' => '0',
        ],
    'sms'                                   =>
        [
            'server' => '0',
        ],
    'softway'                               =>
        [
            'tag' => 'softway',
        ],
    'sourceCode'                            =>
        [
            'phpunit_user_password' => '',
            'fhir'                  =>
                [
                    'fhir_validator_path' => '',
                ],
        ],
    'ssr'                                   =>
        [
            'occupation_surveillance' =>
                [
                    'faible' => '200',
                    'eleve'  => '800',
                ],
            'occupation_technicien'   =>
                [
                    'faible' => '50',
                    'eleve'  => '200',
                ],
            'recusation'              =>
                [
                    'sejour_readonly'        => '0',
                    'use_recuse'             => '1',
                    'view_services_inactifs' => '1',
                ],
            'repartition'             =>
                [
                    'show_tabs' => '1',
                ],
            'CBilanSSR'               =>
                [
                    'tolerance_sejour_demandeur' => '2',
                ],
            'CFicheAutonomie'         =>
                [
                    'use_ex_form' => '0',
                ],
            'CPrescription'           =>
                [
                    'show_dossier_soins' => '0',
                ],
        ],
    'supportClient'                         =>
        [
        ],
    'surgica'                               =>
        [
            'import_function_name' => '',
            'file_category_name'   => '',
            'document_mount_point' => '',
            'document_keyword'     => 'DOCEXT',
            'import_tag'           => 'surgica_import',
            'import_ipp'           => '0',
            'default_naissance'    => '',
            'type_consult'         => '1',
            'type_intervention'    => '2',
            'type_sejour'          => '3',
            'date_min'             => '1970-01-01',
        ],
    'sync'                                  =>
        [
            'handled'        => '',
            'enable_purge'   => '',
            'purge_lifetime' => '100',
            'months_to_keep' => '',
        ],
    'system'                                =>
        [
            'phone_number_format' => '99 99 99 99 99',
            'phone_min_length'    => '10',
            'phone_area_code'     => '33',
            'reverse_proxy'       => '0.0.0.0',
            'website_url'         => 'http://www.mediboard.org',
            'max_log_duration'    => '0',
            'CMessage'            =>
                [
                    'default_email_from' => '',
                    'default_email_to'   => '',
                ],
        ],
    'tammDeploy'                            =>
        [
        ],
    'tasking'                               =>
        [
            'function_id_ox'                    => '',
            'oxDev_team_id'                     => '',
            'training_team'                     => '',
            'delegation_tag_id'                 => '',
            'minutes_allowing_comment_deletion' => '20',
            'minutes_reverting_an_action'       => '2',
            'digest_purge_lifetime'             => '100',
            'digest_purge_delay'                => '2-months',
        ],
    'teleconsultation'                      =>
        [
        ],
    'terreSante'                            =>
        [
        ],
    'ucum'                                  =>
        [
        ],
    'vivalto'                               =>
        [
            'file_directory' => '',
            'VSMobile'       =>
                [
                    'tag' => 'vivalto group:$g',
                ],
        ],
    'webservices'                           =>
        [
            'connection_timeout'   => 5,
            'response_timeout'     => 120,
            'trace'                => 0,
            'wsdl_root_url'        => '',
            'soap_server_encoding' => 'UTF-8',
        ],
    'weda'                                  =>
        [
            'root_path' => '',
        ],
];
