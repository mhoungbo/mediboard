<?php

return [
    'core'       => [
        'action_foo'     => 'Ox\Core\Tests\Resources\Controllers\Legacy\CLoremIpsumLegacyController',
        'action_inherit' => 'Ox\Core\Tests\Resources\Controllers\Legacy\CLoremIpsumInheritLegacyController',
    ],
    'dPpatients' => [
        'action_foo' => 'Ox\Core\Tests\Resources\Controllers\Legacy\CLoremIpsumSecondLegacyController',
    ],
];
