<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Functional\Event;

use Ox\Core\CAppUI;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Tests\OxWebTestCase;
use ReflectionProperty;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CSRFListenerTest extends OxWebTestCase
{
    //    private static CViewSender $sender;

    private static CIdSante400 $idx;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        /*self::$sender         = new CViewSender();
        self::$sender->name   = 'csrf-' . uniqid();
        self::$sender->params = "m=system\ntab=about";
        self::$sender->period = 60;
        self::$sender->every  = 1;
        self::$sender->store();*/

        self::$idx               = new CIdSante400();
        self::$idx->tag          = 'CSRFListenerTest';
        self::$idx->id400        = uniqid();
        self::$idx->object_class = 'CUser';
        self::$idx->object_id    = CUser::get()->_id;
        self::$idx->store();
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        //        self::$sender->purge();

        self::$idx->purge();
    }

    /**
     * @config [CConfiguration] [static] system csrf auto 1
     */
    public function testGetIsNotCheckedLegacy(): void
    {
        self::createClient()->request('GET', '/?m=system&a=about');

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * @config [CConfiguration] [static] system csrf auto 0
     */
    public function testPostIsNotCheckedWithoutConfigLegacy(): void
    {
        self::createClient()->request(
            'POST',
            '/',
            [
                'm'                 => 'dPsante400',
                'do_idsante400_aed' => 'CIdSante400',
                'id_sante400_id'    => self::$idx->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * @config [CConfiguration] [static] system csrf auto 1
     */
    public function testPostIsNotCheckedWithOxTokenLegacy(): void
    {
        self::createClient()->request(
            'POST',
            '/',
            [
                '@token'            => 'dummy',
                'm'                 => 'dPsante400',
                'do_idsante400_aed' => 'CIdSante400',
                'id_sante400_id'    => self::$idx->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * @config [CConfiguration] [static] system csrf auto 1
     */
    public function testOnRequestThrowExceptionIfNoFormNameLegacy(): void
    {
        self::createClient()->request(
            'POST',
            '/',
            [
                'm'                 => 'dPsante400',
                'do_idsante400_aed' => 'CIdSante400',
                'id_sante400_id'    => self::$idx->_id,
                '_token'            => 'foo',
            ]
        );

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * @config [CConfiguration] [static] system csrf auto 1
     */
    public function testOnRequestThrowExceptionIfNoTokenLegacy(): void
    {
        self::createClient()->request(
            'POST',
            '/',
            [
                '_form'             => 'bar',
                'm'                 => 'dPsante400',
                'do_idsante400_aed' => 'CIdSante400',
                'id_sante400_id'    => self::$idx->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * @config [CConfiguration] [static] system csrf auto 1
     */
    public function testOnRequestThrowExceptionIfTokenIsNotValidLegacy(): void
    {
        $token_manager = $this->getMockBuilder(CsrfTokenManagerInterface::class)->getMock();
        $token_manager->method('isTokenValid')->willReturn(false);

        $client = self::createClient();
        $client->getContainer()->set('security.csrf.token_manager', $token_manager);

        $client->request(
            'POST',
            '/',
            [
                '_form'             => 'foo',
                '_token'            => 'bar',
                'm'                 => 'dPsante400',
                'do_idsante400_aed' => 'CIdSante400',
                'id_sante400_id'    => self::$idx->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(403);
    }

    /**
     * @config [CConfiguration] [static] system csrf auto 1
     */
    public function testOnRequestWithValidTokenLegacy(): void
    {
        $token_manager = $this->getMockBuilder(CsrfTokenManagerInterface::class)->getMock();
        $token_manager->method('isTokenValid')->willReturn(true);

        $client = self::createClient();
        $client->getContainer()->set('security.csrf.token_manager', $token_manager);

        $client->request(
            'POST',
            '/',
            [
                '_token'            => 'foo',
                '_form'             => 'bar',
                'm'                 => 'dPsante400',
                'do_idsante400_aed' => 'CIdSante400',
                'id_sante400_id'    => self::$idx->_id,
            ]
        );

        $this->assertResponseStatusCodeSame(200);
    }

    public function testOnRequestGuiForGet(): void
    {
        self::createClient()->request('GET', '/gui/system/about');

        $this->assertResponseStatusCodeSame(200);
    }

    public function testOnRequestGuiWithoutToken(): void
    {
        self::createClient()->request('POST', '/gui/system/view_senders/edit', server: ['HTTP_USER_AGENT' => 'nothing']);

        $this->assertResponseStatusCodeSame(403);
    }
}
