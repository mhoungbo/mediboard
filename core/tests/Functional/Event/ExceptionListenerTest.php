<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Functional\Event;

use Exception;
use Monolog\Logger;
use Ox\Core\AppUiBridge;
use Ox\Core\Kernel\Event\ExceptionListener;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class ExceptionListenerTest extends OxWebTestCase
{
    public function testExceptionListenerForGui(): void
    {
        $client = self::createClient();
        $client->request('GET', '/gui/lorem/ipsum');

        $this->assertResponseStatusCodeSame(404);
        $this->assertInstanceOf(Response::class, $client->getResponse());
        $this->assertFalse($client->getResponse() instanceof JsonResponse);
    }

    public function testExceptionListenerForApi(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/foo/bar');

        $this->assertResponseStatusCodeSame(404);
        $this->assertInstanceOf(JsonResponse::class, $client->getResponse());
    }
}
