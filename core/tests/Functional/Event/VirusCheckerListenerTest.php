<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Functional\Event;

use DateTimeImmutable;
use Ox\Core\Security\Antivirus\VirusCheckerInterface;
use Ox\Tests\OxWebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class VirusCheckerListenerTest extends OxWebTestCase
{
    public function testOnControllerWithoutFile(): void
    {
        $checker = $this->getCheckerMock(available_count: 0);

        $client = self::createClient();
        $client->getKernel()->getContainer()->set('ox.clamav.checker', $checker);

        $client->request('GET', '/gui/system/about');

        $this->assertResponseStatusCodeSame(200);
    }

    public function testOnControllerWithInfectedFileAndCheckerNotAvailable(): void
    {
        $checker = $this->getCheckerMock(false, 1, false, 0);

        $client = self::createClient();
        $client->getKernel()->getContainer()->set('ox.clamav.checker', $checker);

        $client->request('GET', '/gui/system/about', files: [[$this->getTestFile()]]);

        $this->assertResponseStatusCodeSame(200);
    }

    public function testOnControllerWithInfectedFile(): void
    {
        $checker = $this->getCheckerMock(true, 1, false, 1);

        $client = self::createClient();
        $client->getKernel()->getContainer()->set('ox.clamav.checker', $checker);

        $client->request('GET', '/gui/system/about', files: [[$this->getTestFile()]]);

        $this->assertResponseStatusCodeSame(415);
    }

    public function testOnControllerWithoutInfectedFile(): void
    {
        $checker = $this->getCheckerMock(available_count: 1, is_clean_count: 2);

        $client = self::createClient();
        $client->getKernel()->getContainer()->set('ox.clamav.checker', $checker);

        $client->request('GET', '/gui/system/about', files: [[$this->getTestFile(), $this->getTestFile()]]);

        $this->assertResponseStatusCodeSame(200);
    }

    public function testOnResponseWithoutFileResponse(): void
    {
        $checker = $this->getCheckerMock(available_count: 0, is_clean_count: 0);

        $client = self::createClient();
        $client->getKernel()->getContainer()->set('ox.clamav.checker', $checker);

        $client->request('GET', '/gui/system/about');

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * @config [CConfiguration] [static] system antivirus freshness 1
     */
    public function testOnControllerWithOutOfDateSignatureDatabase(): void
    {
        $old_date = new DateTimeImmutable('-5 DAYS');
        $checker = $this->getCheckerMock(fresh_date: $old_date);

        $client = self::createClient();
        $client->getKernel()->getContainer()->set('ox.clamav.checker', $checker);

        $client->request(
            'GET',
            '/gui/system/about',
            files: [[$this->getTestFile(), $this->getTestFile()]],
            server: ['HTTP_X-Requested-With' => 'XMLHttpRequest']
        );

        $this->assertResponseStatusCodeSame(500);

        $this->assertStringContainsString(
            $this->translator->tr(
                'VirusCheckerListener-error-Signature database is too old',
                $old_date->format('Y-m-d H:i:s')
            ),
            html_entity_decode($client->getResponse()->getContent())
        );
    }

    /**
     * @config [CConfiguration] [static] system antivirus freshness 0
     */
    public function testOnControllerWithoutDateCheck(): void
    {
        $old_date = new DateTimeImmutable('-5 DAYS');
        $checker = $this->getCheckerMock(fresh_date: $old_date);

        $client = self::createClient();
        $client->getKernel()->getContainer()->set('ox.clamav.checker', $checker);

        $client->request(
                    'GET',
                    '/gui/system/about',
            files: [$this->getTestFile(), $this->getTestFile()],
            server: ['HTTP_X-Requested-With' => 'XMLHttpRequest']
        );

        $this->assertResponseStatusCodeSame(200);
    }

    private function getCheckerMock(
        bool     $available = true,
        int      $available_count = null,
        bool     $is_clean = true,
        int      $is_clean_count = null,
        DateTimeImmutable $fresh_date = new DateTimeImmutable(),
    ): VirusCheckerInterface {
        $checker = $this->getMockBuilder(VirusCheckerInterface::class)
                        ->getMock();

        if (null === $available_count) {
            $checker->method('isAvailable')->willReturn($available);
        } else {
            $checker->expects($this->exactly($available_count))->method('isAvailable')->willReturn($available);
        }

        if (null === $is_clean_count) {
            $checker->method('isFileClean')->willReturn($is_clean);
        } else {
            $checker->expects($this->exactly($is_clean_count))->method('isFileClean')->willReturn($is_clean);
        }

        $checker->method('getLastVirusDatabaseUpdate')->willReturn($fresh_date);

        return $checker;
    }

    private function getTestFile(): UploadedFile
    {
        return new UploadedFile('/foo/bar', 'foo_bar', error: UPLOAD_ERR_NO_FILE, test: true);
    }
}
