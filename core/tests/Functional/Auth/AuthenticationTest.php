<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Tests\Functional\Auth;

use DateTimeImmutable;
use Exception;
use Ox\Core\AppUiBridge;
use Ox\Core\Auth\Authenticators\ApiTokenAuthenticator;
use Ox\Core\Auth\Badges\CsrfBadgeBuilder;
use Ox\Core\Auth\Checkers\UserChecker;
use Ox\Core\Auth\Exception\AuthenticationFailedException;
use Ox\Core\Auth\Exception\CouldNotValidatePostAuthentication;
use Ox\Core\Auth\Exception\CouldNotValidatePreAuthentication;
use Ox\Core\CAppUI;
use Ox\Core\Config\Conf;
use Ox\Core\OAuth2\OIDC\FC\Client as FCClient;
use Ox\Core\OAuth2\OIDC\FC\UserInfo as FCUserInfo;
use Ox\Core\OAuth2\OIDC\PSC\Client as PSCClient;
use Ox\Core\OAuth2\OIDC\PSC\PscOptions;
use Ox\Core\OAuth2\OIDC\PSC\UserInfo as PSCUserInfo;
use Ox\Core\OAuth2\OIDC\TokenSet;
use Ox\Core\Sessions\SessionHelper;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\Admin\Tests\Fixtures\AuthenticationFixtures;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Files\Repositories\FileRepository;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CUserAuthentication;
use Ox\Mediboard\System\CUserAuthenticationError;
use Ox\Tests\OxWebTestCase;
use ReflectionProperty;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\Security\Core\User\UserCheckerInterface;

/**
 * Todo: Make tests targeting autowiring and argument resolving with gui tokens.
 * Todo: Test tokens (gui & legacy)
 *
 * @group schedules
 *
 * @runTestsInSeparateProcesses
 *
 * These tests authenticate another user
 * So we have to run it in separate process
 */
class AuthenticationTest extends OxWebTestCase
{
    private const LOGIN     = '/gui/login';
    private const KRB_LOGIN = '/gui/krb/login';
    private const PSC_LOGIN = '/gui/oauth2/psc/callback';
    private const FC_LOGIN  = '/gui/oauth2/fc/callback';
    private const CPX_LOGIN = '/gui/cpx/login';

    private const API_ROUTE = '/api/tokens';

    private function getAuthClient(?array $options = []): KernelBrowser
    {
        $options = array_merge(
            [
                'enable_ldap'        => false, // Todo: Enable when CI-ready
                'enable_krb'         => true,
                'enable_psc'         => false,
                'enable_fc'          => false,
                'enable_cpx'         => false,
                'limit_remote_users' => false,
                'ip_whitelist'       => false,
            ],
            $options
        );

        $client = static::createClient([], ['X-AUTH_TYPE' => 'OX-Web-Test'], null, false);

        static::getContainer()->set(CsrfBadgeBuilder::class, $this->createMock(CsrfBadgeBuilder::class));

        if ($options['enable_psc']) {
            static::getContainer()->set(PSCClient::class, $this->getPSCClientMock());
        }

        if ($options['enable_fc']) {
            static::getContainer()->set(FCClient::class, $this->getFCClientMock());
        }

        if ($options['enable_cpx']) {
            static::getContainer()->set(FileRepository::class, $this->getCPXMock());
        }

        $conf_mock = $this->getConfMock($options);
        static::getContainer()->set(Conf::class, $conf_mock);

        if ($options['limit_remote_users'] || $options['ip_whitelist']) {
            static::getContainer()->set(UserCheckerInterface::class, $this->getUserCheckerMock($conf_mock));
        }

        return $client;
    }

    private function getPSCClientMock(): PSCClient
    {
        $token_set = $this->createMock(TokenSet::class);

        $psc_user_info = $this->createMock(PSCUserInfo::class);
        $psc_user_info->method('getId')->willReturn(AuthenticationFixtures::TEST_VALID_RPPS);
        $psc_user_info->method('getSub')->willReturn('someSub');

        $psc = $this->getMockBuilder(PSCClient::class)->disableOriginalConstructor()->getMock();

        $ref_property = new ReflectionProperty($psc, 'options');
        $ref_property->setAccessible(true);
        $ref_property->setValue($psc, (new PscOptions())->getOptions());
        $psc->method('getUserInfo')->willReturn($psc_user_info);
        $psc->method('getTokenSet')->willReturn($token_set);

        return $psc;
    }

    private function getFCClientMock(): FCClient
    {
        $token_set = $this->createMock(TokenSet::class);

        $fc_user_info = $this->createMock(FCUserInfo::class);
        $fc_user_info->method('getId')->willReturn('test@example.com');
        $fc_user_info->method('getSub')->willReturn(AuthenticationFixtures::TEST_VALID_FC_SUB);

        $fc = $this->getMockBuilder(FCClient::class)->disableOriginalConstructor()->getMock();
        $fc->method('getUserInfo')->willReturn($fc_user_info);
        $fc->method('getTokenSet')->willReturn($token_set);

        return $fc;
    }

    private function getCPXMock(): FileRepository
    {
        /** @var CFile $fixture */
        $fixture = $this->getObjectFromFixturesReference(CFile::class, AuthenticationFixtures::CPX_SIGNATURE_FILE);

        // Mocking here because of CFile not working in CLI SAPI (realpath error)
        $file = $this->getMockBuilder(CFile::class)->disableOriginalConstructor()->getMock();
        $file->method('getBinaryContent')->willReturn(AuthenticationFixtures::CPX_CERT_SIGNATURE);
        $file->object_id = $fixture->object_id;

        $repository = $this->createMock(FileRepository::class);
        $repository->method('findCertificateRelatedFiles')->willReturn([$file]);
        $repository->method('findCertificateFile')->willReturn(
            $this->getObjectFromFixturesReference(CFile::class, AuthenticationFixtures::CPX_CERT_SIGNATURE_FILE)
        );

        return $repository;
    }

    private function getUserCheckerMock(Conf $conf): UserCheckerInterface
    {
        $app_ui = $this->createMock(AppUiBridge::class);
        $app_ui->method('isIntranet')->willReturn(false);
        $app_ui->method('getIp')->willReturn('127.0.0.1');

        return new UserChecker($conf, $app_ui);
    }

    private function getConfMock(array $options): Conf
    {
        $conf = $this->getMockBuilder(Conf::class)
                     ->onlyMethods(['get', 'getForGroupId'])
                     ->getMock();

        $conf->method('get')->willReturnCallback(function (string $name) use ($options) {
            switch ($name) {
                case 'admin LDAP ldap_connection':
                    return ($options['enable_ldap']) ? '1' : '0';

                case 'admin CKerberosLdapIdentifier enable_kerberos_authentication':
                    return ($options['enable_krb']) ? '1' : '0';

                default:
                    return CAppUI::conf($name);
            }
        });

        $conf->method('getForGroupId')->willReturnCallback(function (string $name, int $group_id) use ($options) {
            switch ($name) {
                case 'system network ip_address_range_whitelist':
                    return ($options['ip_whitelist']) ? '127.0.0.2' : null;

                default:
                    return CAppUI::gconf($name, $group_id);
            }
        });

        return $conf;
    }

    /**
     * @dataProvider successfulStatefulAuthProvider
     *
     * @param array  $args
     * @param CUser  $user
     * @param string $auth_method
     * @param array  $options
     *
     * @return void
     */
    public function testSuccessfulStatefulAuthLogging(
        array  $args,
        CUser  $user,
        string $auth_method,
        array  $options
    ): void {
        $client = $this->getAuthClient($options);

        $now = new DateTimeImmutable();
        $client->request(...$args);

        $this->assertLastStatefulAuth($user, $now, $auth_method);
    }

    /**
     * @dataProvider successfulStatelessAuthProvider
     *
     * @param array  $args
     * @param CUser  $user
     * @param string $auth_method
     * @param array  $options
     *
     * @return void
     */
    public function testFirstSuccessfulStatelessAuthLogging(
        array  $args,
        CUser  $user,
        string $auth_method,
        array  $options
    ): void {
        $client = $this->getAuthClient($options);

        $now = new DateTimeImmutable();
        $client->request(...$args);

        $this->assertFirstStatelessAuth($user, $now, $auth_method);
    }

    /**
     * @dataProvider successfulStatelessAuthProvider
     * @depends      testFirstSuccessfulStatelessAuthLogging
     *
     * @param array  $args
     * @param CUser  $user
     * @param string $auth_method
     *
     * @return void
     */
    public function testSuccessfulStatelessAuthLogging(array $args, CUser $user, string $auth_method): void
    {
        $previous_auth = $user->loadRefLastAuth();

        $client = $this->getAuthClient();

        $now = new DateTimeImmutable();
        $client->request(...$args);

        $this->assertLastStatelessAuth($user, $previous_auth, $now, $auth_method);
    }

    /**
     * @dataProvider failedStatefulAuthProvider
     *
     * @param array       $args
     * @param CUser       $user
     * @param string      $auth_method
     * @param array       $options
     * @param string|null $message
     *
     * @return void
     */
    public function testFailedStatefulAuthLogging(
        array   $args,
        CUser   $user,
        string  $auth_method,
        array   $options,
        ?string $message
    ): void {
        $client = $this->getAuthClient($options);

        $now = new DateTimeImmutable();
        $client->request(...$args);

        $this->assertLastErrorAuth($user, $now, $auth_method, $message);
    }

    /**
     * @dataProvider validTokenProvider
     *
     * @param CViewAccessToken $token
     *
     * @return void
     * @throws Exception
     */
    public function testValidGuiToken(CViewAccessToken $token): void
    {
        $client = $this->getAuthClient();
        $client->request('GET', "/token/{$token->hash}");

        $this->assertResponseIsSuccessful();
        $this->assertResponseNotHasCookie(SessionHelper::getSessionName());
    }

    /**
     * @dataProvider invalidTokenProvider
     *
     * @param CViewAccessToken $token
     *
     * @return void
     * @throws Exception
     */
    public function testInvalidGuiToken(CViewAccessToken $token): void
    {
        $client = $this->getAuthClient();

        $client->request('GET', "/token/{$token->hash}");

        $this->assertResponseRedirects();

        // Seems to make session start twice
        $client->followRedirect();

        $this->assertResponseIsSuccessful();
    }

    public function validTokenProvider(): array
    {
        return [
            'gui token / 1 route / non restricted' => [
                $this->getObjectFromFixturesReference(
                    CViewAccessToken::class,
                    AuthenticationFixtures::GUI_TOKEN
                ),
            ],
            'gui token / 1 route / restricted'     => [
                $this->getObjectFromFixturesReference(
                    CViewAccessToken::class,
                    AuthenticationFixtures::GUI_RESTRICTED_TOKEN
                ),
            ],
        ];
    }

    public function invalidTokenProvider(): array
    {
        return [
            'gui token / 2 routes' => [
                $this->getObjectFromFixturesReference(
                    CViewAccessToken::class,
                    AuthenticationFixtures::GUI_MULTI_ROUTE_TOKEN
                ),
            ],
            'gui token / no route' => [
                $this->getObjectFromFixturesReference(
                    CViewAccessToken::class,
                    AuthenticationFixtures::GUI_NO_ROUTE_TOKEN
                ),
            ],
        ];
    }

    private function getUser(string $fixture_tag = AuthenticationFixtures::STANDARD_USER): CUser
    {
        /** @var CUser $user */
        $user = $this->getObjectFromFixturesReference(CUser::class, $fixture_tag);

        return $user;
    }

    private function assertLastStatefulAuth(CUser $user, DateTimeImmutable $datetime, string $auth_method): void
    {
        // Todo: Clearly not robust enough
        $last_auth = $user->loadRefLastAuth();
        $this->assertInstanceOf(CUserAuthentication::class, $last_auth);
        $this->assertNotNull($last_auth->_id);
        $this->assertGreaterThanOrEqual($datetime->format('Y-m-d H:i:s'), $last_auth->datetime_login);
        $this->assertEquals($auth_method, $last_auth->auth_method);
        $this->assertEquals($last_auth->datetime_login, $last_auth->last_session_update);
        $this->assertEquals(0, $last_auth->nb_update);
        $this->assertFalse(str_starts_with($last_auth->session_id, 'stateless'));
        $this->assertGreaterThan(0, $last_auth->session_lifetime);
    }

    private function assertFirstStatelessAuth(CUser $user, DateTimeImmutable $datetime, string $auth_method): void
    {
        // Todo: Clearly not robust enough
        $last_auth = $user->loadRefLastAuth();
        $this->assertInstanceOf(CUserAuthentication::class, $last_auth);
        $this->assertNotNull($last_auth->_id);
        $this->assertGreaterThanOrEqual($datetime->format('Y-m-d H:i:s'), $last_auth->datetime_login);
        $this->assertEquals($auth_method, $last_auth->auth_method);
        $this->assertEquals($last_auth->datetime_login, $last_auth->last_session_update);
        $this->assertEquals(0, $last_auth->nb_update);
        $this->assertTrue(str_starts_with($last_auth->session_id, 'stateless'));
    }

    private function assertLastStatelessAuth(
        CUser               $user,
        CUserAuthentication $previous_auth,
        DateTimeImmutable   $datetime,
        string              $auth_method
    ): void {
        // Todo: Clearly not robust enough
        $last_auth = $user->loadRefLastAuth();
        $this->assertInstanceOf(CUserAuthentication::class, $last_auth);
        $this->assertEquals($previous_auth->_id, $last_auth->_id);
        $this->assertEquals($previous_auth->datetime_login, $last_auth->datetime_login);
        $this->assertEquals($auth_method, $last_auth->auth_method);
        $this->assertGreaterThanOrEqual($datetime->format('Y-m-d H:i:s'), $last_auth->last_session_update);
        $this->assertEquals(($previous_auth->nb_update + 1), $last_auth->nb_update);
        $this->assertEquals(0, $last_auth->session_lifetime);
        $this->assertTrue(str_starts_with($last_auth->session_id, 'stateless'));
    }

    private function assertLastErrorAuth(
        CUser             $user,
        DateTimeImmutable $datetime,
        string            $auth_method,
        ?string           $message
    ): void {
        // Todo: Clearly not robust enough

        if ($user->_id) {
            $last_error = $user->loadUniqueBackRef('authentication_errors', 'datetime DESC', 1);
        } else {
            $last_error              = new CUserAuthenticationError();
            $last_error->login_value = $user->user_username;
            $last_error->loadMatchingObjectEsc('datetime DESC');

            if (!$last_error->_id) {
                $this->fail('Unable to retrieve last authentication error log');
            }
        }

        $this->assertInstanceOf(CUserAuthenticationError::class, $last_error);
        $this->assertNotNull($last_error->_id);
        $this->assertGreaterThanOrEqual($datetime->format('Y-m-d H:i:s'), $last_error->datetime);
        $this->assertEquals($auth_method, $last_error->auth_method);
        $this->assertEquals($last_error->login_value, $user->user_username);
        $this->assertEquals($last_error->user_id, $user->_id);
        $this->assertEquals($message, $last_error->message);
    }

    public function successfulStatefulAuthProvider(): array
    {
        $std        = $this->getUser();
        $ldap       = $this->getUser(AuthenticationFixtures::LDAP_USER);
        $basic      = $this->getUser();
        $basic_ldap = $this->getUser(AuthenticationFixtures::LDAP_USER);
        $krb        = $this->getUser(AuthenticationFixtures::KRB_USER);

        /** @var CMediusers $psc */
        $psc = $this->getObjectFromFixturesReference(CMediusers::class, AuthenticationFixtures::RPPS_USER);
        $fc  = $this->getUser(AuthenticationFixtures::FC_USER);

        /** @var CFile $cpx */
        $cpx = $this->getObjectFromFixturesReference(CFile::class, AuthenticationFixtures::CPX_SIGNATURE_FILE);

        return [
            'gui / standard'        => [
                [
                    'POST',
                    self::LOGIN,
                    [
                        'username' => $std->user_username,
                        'password' => AuthenticationFixtures::PASSWORD,
                    ],
                    [],
                    [],
                ],
                $std,
                CUserAuthentication::AUTH_METHOD_STANDARD,
                [],
            ],
            'gui / ldap (standard)' => [
                [
                    'POST',
                    self::LOGIN,
                    [
                        'username' => $ldap->user_username,
                        'password' => AuthenticationFixtures::PASSWORD,
                    ],
                    [],
                    [],
                ],
                $ldap,
                CUserAuthentication::AUTH_METHOD_STANDARD, // Todo: Change this to LDAP when it will be available.
                [],
            ],
            'gui / basic'           => [
                [
                    'POST',
                    self::LOGIN,
                    [],
                    [],
                    [
                        'HTTP_Authorization' => $this->makeHttpBasicHeader($basic),
                    ],
                ],
                $basic,
                CUserAuthentication::AUTH_METHOD_BASIC,
                [],
            ],
            'gui / ldap (basic)'    => [
                [
                    'POST',
                    self::LOGIN,
                    [],
                    [],
                    [
                        'HTTP_Authorization' => $this->makeHttpBasicHeader($basic_ldap),
                    ],
                ],
                $basic_ldap,
                CUserAuthentication::AUTH_METHOD_BASIC, // Todo: Change this to LDAP when it will be available.
                [],
            ],
            'gui / krb'             => [
                [
                    'POST',
                    self::KRB_LOGIN,
                    [],
                    [],
                    [
                        'AUTH_TYPE'   => 'Negotiate',
                        'REMOTE_USER' => $krb->user_username,
                    ],
                ],
                $krb,
                CUserAuthentication::AUTH_METHOD_SSO,
                [],
            ],
            'gui / psc'             => [
                [
                    'GET',
                    self::PSC_LOGIN,
                    [
                        'code'  => 'some code',
                        'state' => 'some state',
                    ],
                    [],
                    [],
                ],
                $psc->loadRefUser(),
                CUserAuthentication::AUTH_METHOD_SSO,
                ['enable_psc' => true],
            ],
            'gui / fc'              => [
                [
                    'GET',
                    self::FC_LOGIN,
                    [
                        'code'  => 'some code',
                        'state' => 'some state',
                        'nonce' => 'some nonce',
                    ],
                    [],
                    [],
                ],
                $fc,
                CUserAuthentication::AUTH_METHOD_SSO,
                ['enable_fc' => true],
            ],
            'gui / cpx'             => [
                [
                    'POST',
                    self::CPX_LOGIN,
                    [
                        'signature'            => AuthenticationFixtures::CPX_SIGNATURE,
                        'certificat_signature' => AuthenticationFixtures::CPX_CERT_SIGNATURE,
                    ],
                    [],
                    [],
                ],
                $cpx->loadTargetObject()->loadRefUser(),
                CUserAuthentication::AUTH_METHOD_CARD,
                ['enable_cpx' => true],
            ],
        ];
    }

    public function successfulStatelessAuthProvider(): array
    {
        /** @var CViewAccessToken $token */
        $token      = $this->getObjectFromFixturesReference(CViewAccessToken::class, AuthenticationFixtures::API_TOKEN);
        $basic      = $this->getUser();
        $basic_ldap = $this->getUser(AuthenticationFixtures::LDAP_USER);

        return [
            'api / token'        => [
                [
                    'GET',
                    self::API_ROUTE,
                    [],
                    [],
                    [
                        'HTTP_' . ApiTokenAuthenticator::TOKEN_HEADER_KEY => $token->hash,
                    ],
                ],
                $token->loadRefUser(),
                CUserAuthentication::AUTH_METHOD_TOKEN,
                [],
            ],
            'api / basic'        => [
                [
                    'GET',
                    self::API_ROUTE,
                    [],
                    [],
                    [
                        'HTTP_Authorization' => $this->makeHttpBasicHeader($basic),
                    ],
                ],
                $basic,
                CUserAuthentication::AUTH_METHOD_BASIC,
                [],
            ],
            'api / ldap (basic)' => [
                [
                    'GET',
                    self::API_ROUTE,
                    [],
                    [],
                    [
                        'HTTP_Authorization' => $this->makeHttpBasicHeader($basic_ldap),
                    ],
                ],
                $basic_ldap,
                CUserAuthentication::AUTH_METHOD_BASIC, // Todo: Change this to LDAP when it will be available.
                [],
            ],
        ];
    }

    public function failedStatefulAuthProvider(): array
    {
        $unknown                = new CUser();
        $unknown->user_username = 'Je suis Fant�mas...';

        $std      = $this->getUser();
        $template = $this->getUser(AuthenticationFixtures::TEMPLATE_USER);

        /** @var CMediusers $secondary */
        $secondary = $this->getObjectFromFixturesReference(CMediusers::class, AuthenticationFixtures::SECONDARY_USER);

        // Todo: Heavily relies on "admin CUser max_login_attempts" and "admin CUser lock_expiration_time" confs
        $locked = $this->getUser(AuthenticationFixtures::LOCKED_USER);

        /** @var CMediusers $inactive */
        $inactive = $this->getObjectFromFixturesReference(CMediusers::class, AuthenticationFixtures::INACTIVE_USER);

        /** @var CMediusers $local */
        $local = $this->getObjectFromFixturesReference(CMediusers::class, AuthenticationFixtures::LOCAL_USER);

        /** @var CMediusers $ip_not_allowed */
        $ip_not_allowed = $this->getObjectFromFixturesReference(CMediusers::class, AuthenticationFixtures::IP_USER);

        $users = [
            'unknown'   => $unknown,
            'std'       => $std,
            'template'  => $template,
            'secondary' => $secondary->loadRefUser(),
            'locked'    => $locked,
            'inactive'  => $inactive->loadRefUser(),
            'local'     => $local->loadRefUser(),
            'ip'        => $ip_not_allowed->loadRefUser(),
        ];

        return
            $this->buildSeries(
                'gui / standard',
                $users,
                CUserAuthentication::AUTH_METHOD_STANDARD,
                [$this, 'makeStandardAuthRequest']
            )
            + $this->buildSeries(
                'gui / basic',
                $users,
                CUserAuthentication::AUTH_METHOD_BASIC,
                [$this, 'makeGuiBasicAuthRequest']
            )
            + $this->buildSeries(
                'gui / krb',
                $users,
                CUserAuthentication::AUTH_METHOD_SSO,
                [$this, 'makeKrbAuthRequest']
            )
            + $this->buildSeries(
                'api / basic',
                $users,
                CUserAuthentication::AUTH_METHOD_BASIC,
                [$this, 'makeApiBasicAuthRequest']
            );
    }

    private function buildSeries(string $name, array $users, string $auth_method, callable $auth_callable): array
    {
        $series = [
            "{$name} / unknown"         => [
                $auth_callable($users['unknown'], 'anyway'),
                $users['unknown'],
                $auth_method,
                [],
                "User {$users['unknown']->user_username} not found",
            ],
            "{$name} / bad credentials" => [
                $auth_callable($users['std'], 'bad credentials'),
                $users['std'],
                $auth_method,
                [],
                AuthenticationFailedException::invalidCredentials()->getMessage(),
            ],
            "{$name} / template"        => [
                $auth_callable($users['template']),
                $users['template'],
                $auth_method,
                [],
                CouldNotValidatePreAuthentication::userIsATemplate()->getMessage(),
            ],
            "{$name} / secondary"       => [
                $auth_callable($users['secondary']),
                $users['secondary'],
                $auth_method,
                [],
                CouldNotValidatePreAuthentication::userIsSecondary()->getMessage(),
            ],
            "{$name} / locked"          => [
                $auth_callable($users['locked']),
                $users['locked'],
                $auth_method,
                [],
                CouldNotValidatePostAuthentication::userIsLocked()->getMessage(),
            ],
            "{$name} / inactive"        => [
                $auth_callable($users['inactive']),
                $users['inactive'],
                $auth_method,
                [],
                CouldNotValidatePostAuthentication::userIsDeactivated()->getMessage(),
            ],
            "{$name} / local only"      => [
                $auth_callable($users['local']),
                $users['local'],
                $auth_method,
                ['limit_remote_users' => true],
                CouldNotValidatePostAuthentication::userHasNoRemoteAccess()->getMessage(),
            ],
            "{$name} / ip not allowed"  => [
                $auth_callable($users['ip']),
                $users['ip'],
                $auth_method,
                ['ip_whitelist' => true],
                CouldNotValidatePostAuthentication::userHasNoAccessFromThisLocation()->getMessage(),
            ],
        ];

        // Meh...
        if ($name === 'gui / krb') {
            unset($series['gui / krb / bad credentials']);
        }

        return $series;
    }

    private function makeHttpBasicHeader(CUser $user, string $password = AuthenticationFixtures::PASSWORD): string
    {
        return 'Basic ' . base64_encode("{$user->user_username}:{$password}");
    }

    private function makeStandardAuthRequest(CUser $user, string $password = AuthenticationFixtures::PASSWORD): array
    {
        return [
            'POST',
            self::LOGIN,
            [
                'username' => $user->user_username,
                'password' => $password,
            ],
            [],
            [],
        ];
    }

    private function makeGuiBasicAuthRequest(CUser $user, string $password = AuthenticationFixtures::PASSWORD): array
    {
        return [
            'POST',
            self::LOGIN,
            [],
            [],
            [
                'HTTP_Authorization' => $this->makeHttpBasicHeader($user, $password),
            ],
        ];
    }

    private function makeApiBasicAuthRequest(CUser $user, string $password = AuthenticationFixtures::PASSWORD): array
    {
        return [
            'GET',
            self::API_ROUTE,
            [],
            [],
            [
                'HTTP_Authorization' => $this->makeHttpBasicHeader($user, $password),
            ],
        ];
    }

    private function makeKrbAuthRequest(CUser $user, string $password = AuthenticationFixtures::PASSWORD): array
    {
        return [
            'POST',
            self::KRB_LOGIN,
            [],
            [],
            [
                'AUTH_TYPE'   => 'Negotiate',
                'REMOTE_USER' => $user->user_username,
            ],
        ];
    }
}
