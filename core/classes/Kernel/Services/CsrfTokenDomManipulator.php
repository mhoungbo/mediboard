<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Services;

use DOMDocument;
use DOMElement;
use DOMException;
use DOMXPath;
use Ox\Core\Kernel\Event\CSRFListener;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Add CSRF tokens to forms that need it.
 */
class CsrfTokenDomManipulator
{
    private CsrfTokenManagerInterface $csrf_token_manager;

    public function __construct(CsrfTokenManagerInterface $csrf_token_manager)
    {
        $this->csrf_token_manager = $csrf_token_manager;
    }

    /**
     * Add anti-csrf token to forms without an input named "@token" or "token".
     *
     * @throws DOMException
     */
    public function addCsrfTokens(string $content): ?string
    {
        if ($content === '') {
            return null;
        }

        $dom = new DOMDocument();
        // Avoid automatically adding doctype.
        if (@$dom->loadHTML($content, LIBXML_HTML_NODEFDTD)) {
            $forms = (new DOMXPath($dom))
                ->query('//form[@method="post" and not(input[@type="hidden" and (@name="@token" or @name="token")])]');

            if (!$forms || $forms->length === 0) {
                return null;
            }

            /** @var DOMElement $form */
            foreach ($forms as $form) {
                $form_name = $form->getAttribute('name');

                // Add form name (will be token key)
                $input = $dom->createElement("input");
                $input->setAttribute("name", CSRFListener::INPUT_FORM_NAME);
                $input->setAttribute("type", "hidden");
                $input->setAttribute("value", $form_name);
                $form->appendChild($input);

                // Add token
                $input = $dom->createElement("input");
                $input->setAttribute("name", CSRFListener::INPUT_TOKEN);
                $input->setAttribute("type", "hidden");
                $input->setAttribute("value", $this->csrf_token_manager->getToken($form_name));
                $form->appendChild($input);
            }
        }

        return $dom->saveHTML();
    }
}
