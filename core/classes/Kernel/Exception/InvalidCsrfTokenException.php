<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Exception;

use Ox\Core\Locales\Translator;

class InvalidCsrfTokenException extends AccessDeniedException
{
    /** @var bool */
    protected bool $is_loggable = true;

    public function __construct($message = "common-csrf-invalid", array $headers = [], $code = 0)
    {
        parent::__construct((new Translator())->tr($message), $headers, $code);
    }
}
