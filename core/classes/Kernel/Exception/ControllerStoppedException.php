<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Exception;

/**
 * Exception used to replace the legacy CApp::rip in CAppUI::stepAjax with UI_MSG_ERROR.
 */
class ControllerStoppedException extends HttpException
{
    public bool $is_loggable = false;
}
