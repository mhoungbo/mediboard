<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Exception;

use Ox\Core\Locales\Translator;
use Symfony\Component\HttpFoundation\Response;

class ObjectNotFoundException extends HttpException
{
    /** @var bool */
    protected bool $is_loggable = false;

    public function __construct(string $object_guid, array $headers = [], $code = 0)
    {
        $translator = new Translator();
        [$class, $id] = explode('-', $object_guid);

        $message = $translator->tr(
            "common-msg-Object of class %s and ID %d was not found",
            $translator->tr($class),
            $id
        );

        parent::__construct(Response::HTTP_NOT_FOUND, $message, $headers, $code);
    }
}
