<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Routing;

use Symfony\Component\HttpFoundation\Request;

/**
 * This trait is simple helper to provide accessor on request
 * And to return custom info on request (public, api/gui ... )
 */
trait RequestHelperTrait
{
    /**
     * Tell whether the given request is in a API context.
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function isRequestApi(Request $request): bool
    {
        return $this->isRequestPrefixed($request, 'api');
    }

    /**
     * Tell whether the given request is in a Gui context.
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function isRequestGui(Request $request): bool
    {
        return $this->isRequestPrefixed($request, 'gui') || $this->isRequestPrefixed($request, 'token');
    }

    /**
     * Tell whether the given request is in a prefixed context.
     *
     * @param Request $request
     * @param string  $prefix
     *
     * @return bool
     */
    private function isRequestPrefixed(Request $request, string $prefix): bool
    {
        return strpos($request->getPathInfo(), $prefix) === 1;
    }

    /**
     * Tell if the request is made for the root of the webserver.
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function isRequestLegacy(Request $request): bool
    {
        return $request->getPathInfo() === '/';
    }

    protected function isEnvironmentDev(Request $request): bool
    {
        return $request->server->get('APP_ENV') === "dev";
    }

    protected function isEnvironmentTest(Request $request): bool
    {
        return $request->server->get('APP_ENV') === "test";
    }

    protected function isAppDebug(Request $request): bool
    {
        return (bool)$request->server->get('APP_DEBUG');
    }

    /**
     * Tell whether the given request is in a profiler context
     */
    protected function isRequestProfiler(Request $request): bool
    {
        return $this->isRequestPrefixed($request, '_profiler') || $this->isRequestPrefixed($request, '_wdt');
    }

    /**
     * Tell whether the given request is in a public environment.
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function isRequestPublic(Request $request): bool
    {
        return $request->attributes->get('public') === true;
    }

    /**
     * Get the controller attributes
     *
     * @param Request $request
     *
     * @return string|null
     */
    protected function getController(Request $request): ?string
    {
        $_controller = $request->attributes->get('_controller', '');
        if (strpos($_controller, '::') !== false) {
            return explode('::', $_controller)[0];
        }

        return $_controller;
    }

    protected function isRequestWebTestCase(Request $request): bool
    {
        return ($request->headers->get('user-agent') === 'Symfony BrowserKit') && $this->isEnvironmentTest($request);
    }

    protected function isAuthenticationWebTestCaseRequest(Request $request): bool
    {
        $auth_type = $request->server->get('X-AUTH_TYPE');

        return ($auth_type === 'OX-Web-Test');
    }
}
