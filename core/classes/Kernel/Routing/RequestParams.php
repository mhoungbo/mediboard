<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Routing;

use Exception;
use InvalidArgumentException;
use Ox\Core\CMbFieldSpecFact;
use Ox\Core\CMbString;
use Ox\Core\FieldSpecs\CBoolSpec;
use Ox\Core\FieldSpecs\CRefSpec;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Locales\Translator;
use Psr\Log\LoggerInterface;
use stdClass;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Access to request parameters ($_GET, $_POST, $_FILES, $_COOKIES).
 * Validate parameters before returning them
 */
class RequestParams
{
    public const FORBIDDEN_PARAMS = ["m", "a", "tab", "dialog", "raw", "ajax", "info", "enslave"];
    public const SPEC_OPTIONS     = ["allow_zero" => true, "allowed_values" => null,];

    private Request    $request;

    private stdClass        $params;

    public function __construct(
        RequestStack $stack,
        private Translator $translator,
        private LoggerInterface $logger,
        private bool $typed_values = false
    ) {
        $this->request    = $stack->getCurrentRequest();
        $this->params     = new stdClass();
    }

    /**
     * If true, the values returned by the methods get(), post() or request() will be typed according to the props.
     *
     * The specs num will return an int, bool a boolean, etc.
     * The specs date and dateTime will return a DateTimeImmutable object.
     *
     * This method can be used instead of naming the RequestParams parameter to $typed_request
     * in the methods of a Controller.
     *
     * It can also be used inside a method to disable the typed returns for a few params if necessary
     *
     * @param bool $value
     *
     * @return void
     */
    public function setTypedValues(bool $value): void
    {
        $this->typed_values = $value;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param string       $param_name
     * @param string|array $prop
     * @param int          $perm
     *
     * @return mixed
     * @throws Exception
     */
    public function get(string $param_name, string|array $prop, int $perm = 0): mixed
    {
        return $this->checkParam('get', $param_name, $prop, $perm);
    }

    /**
     * @param string       $param_name
     * @param string|array $prop
     * @param int          $perm
     *
     * @return mixed
     * @throws Exception
     */
    public function post(string $param_name, string|array $prop, int $perm = 0): mixed
    {
        return $this->checkParam('post', $param_name, $prop, $perm);
    }

    /**
     * @param string       $param_name
     * @param string|array $prop
     * @param int          $perm
     *
     * @return mixed
     * @throws Exception
     */
    public function session(string $param_name, string|array $prop, int $perm = 0): mixed
    {
        return $this->checkParam('session', $param_name, $prop, $perm);
    }

    /**
     * @param string       $param_name
     * @param string|array $prop
     * @param int          $perm
     *
     * @return mixed
     * @throws Exception
     */
    public function request(string $param_name, string|array $prop, int $perm = 0): mixed
    {
        return $this->checkParam('request', $param_name, $prop, $perm);
    }

    /**
     * @param string       $type
     * @param string       $param_name
     * @param string|array $prop
     * @param int          $perm
     *
     * @return mixed|string|null
     * @throws Exception
     */
    private function checkParam(string $type, string $param_name, string|array $prop, int $perm = 0): mixed
    {
        // Get the value from the container bag
        $value = $this->getFromRequest($type, $param_name);

        // Validate value
        $value = $this->validateValueFromProp($param_name, $value, $prop, $perm);

        // Log
        if ($this->isDebug()) {
            $upper_type = strtoupper($type);
            $this->logger->info(
                "RequestParams check $upper_type parameter \"$param_name\" using {prop}",
                [
                    'type'       => $upper_type,
                    'param_name' => $param_name,
                    'prop'       => is_array($prop) ? 'Array' : $prop,
                    'value'      => $value,
                    'perm'       => $perm,
                ]
            );
        }

        return $value;
    }

    /**
     * @param string $type
     * @param string $param_name
     *
     * @return mixed
     * @throws InvalidArgumentException
     */
    private function getFromRequest(string $type, string $param_name): mixed
    {
        if (in_array($param_name, self::FORBIDDEN_PARAMS)) {
            throw new InvalidArgumentException(
                $this->translator->tr('RequestParams-Error-Parameter name is forbidden', $param_name)
            );
        }

        return match ($type) {
            'get'     => $this->request->query->get($param_name),
            'post'    => $this->request->request->get($param_name),
            'session' => $this->request->getSession()->get($param_name),
            default   => $this->request->get($param_name),
        };
    }

    /**
     * @param string       $param_name
     * @param mixed        $value
     * @param string|array $prop
     * @param int          $perm
     *
     * @return mixed|string|null
     * @throws Exception
     */
    private function validateValueFromProp(string $param_name, mixed $value, string|array $prop, int $perm): mixed
    {
        // Avoid setting a default value when a bool spec is used without default
        CBoolSpec::$_default_no = false;
        if (is_array($prop)) {
            $spec = CMbFieldSpecFact::getComplexSpecWithClassName("stdClass", $param_name, $prop, self::SPEC_OPTIONS);
            $prop = array_shift($prop);
        } else {
            $spec = CMbFieldSpecFact::getSpecWithClassName("stdClass", $param_name, $prop, self::SPEC_OPTIONS);
        }
        CBoolSpec::$_default_no = true;

        // Defaults the value when available
        if ($value === null && $spec->default !== null) {
            $value = $spec->default;
        }

        // Could be null
        if ($value === "" || $value === null) {
            if (!$spec->notNull) {
                return $value;
            }
        }

        $this->params->$param_name = $value;

        // Check the value
        if ($msg = $spec->checkPropertyValue($this->params)) {
            $truncated = CMbString::truncate($value);
            throw new InvalidArgumentException(
                $this->translator->tr(
                    'RequestParams-Error-View parameter does not validate the prop',
                    $param_name,
                    $prop,
                    $truncated,
                    $msg
                )
            );
        }

        if ($spec instanceof CRefSpec && ($perm > 0)) {
            if (!$spec->checkPermission($this->params, $perm)) {
                throw new AccessDeniedException();
            }
        }

        if ($this->typed_values) {
            $value = $spec->getTypedValue($value);
        }

        return $value;
    }

    public function isDebug(): bool
    {
        return (bool)$this->request->server->get('APP_DEBUG');
    }
}
