<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Routing;

use Exception;
use Ox\Core\AppUiBridge;
use Ox\Core\CApp;
use Ox\Core\CCanDo;
use Ox\Core\CClassMap;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\System\Log\AccessLogManager;
use Symfony\Component\HttpFoundation\Request;

/**
 * Legacy bridge vue modes and globals with SF controller
 */
class RequestContext
{
    use RequestHelperTrait;

    private const MODE_SUPPRESS_HEADERS = 'suppressHeaders';
    private const MODE_DIALOG           = 'dialog';
    private const MODE_AJAX             = 'ajax';

    private const PARAM_GROUP    = 'g';
    private const PARAM_FUNCTION = 'f';

    private bool $dialog           = false; // full dom without app bar
    private bool $suppress_headers = false; // only tpl dom
    private bool $ajax             = false; // only tpl dom + localize + ajax error

    private ?string $group_id    = null;
    private ?string $function_id = null;

    private ?string $controller;
    private ?string $module;
    private ?string $action;
    private ?string $route;

    private ?CCanDo $can = null;

    private Request $request;

    public function __construct(Request $request, AppUiBridge $app, string $module = null, string $route = null)
    {
        $public = CApp::getInstance()->isPublic();

        $this->request          = $request;
        $this->dialog           = $request->get(self::MODE_DIALOG, false);
        $this->ajax             = $request->get(self::MODE_AJAX, false);
        $this->suppress_headers = $this->ajax || $request->get(self::MODE_SUPPRESS_HEADERS, false);

        $request->attributes->set(
            AccessLogManager::REGISTER_HIT,
            !$this->dialog && !$this->suppress_headers && !$public
        );

        [$controller, $action] = explode('::', $request->attributes->get('_controller'));
        $this->action     = $action;
        $this->controller = $controller;
        $this->route      = $route ?? $request->attributes->get('_route');
        $this->module     = $module ?? CClassMap::getInstance()->getClassMap($this->controller)->module;

        if (!$public) {
            $this->group_id    = $this->extractGroupId($request, $app);
            $this->function_id = $this->extractFunctionId($request, $app);
            $this->can = CModule::getActive($this->module)->canDo();
        }

        $this->defineGlobals();
    }

    /**
     * Selection of group_id to use :
     *   - Extract the group_id to use from GET parameter "g".
     *   - If the GET parameter "g" is not set, check the "g" value in the session.
     *   - If "g" is still not set use the current user default group.
     *
     * Once the "g" parameter is set, load the group and check if the user have read permission on it.
     * If the user have not the read permission on it use the user's default group.
     * Else use the loaded group to value group_id (if an invalid group_id has been asked the first group with the read
     * permission on it will be returned).
     *
     * Finally value the "g" parameter in session for next calls.
     *
     * @param Request     $request
     * @param AppUiBridge $app
     *
     * @return int The selected group_id
     * @throws Exception
     */
    private function extractGroupId(Request $request, AppUiBridge $app): ?int
    {
        $group_id = $request->get(self::PARAM_GROUP, $request->getSession()->get('g'));
        if (!$group_id) {
            $group_id = $app->getCurrentUserGroup();
        }

        $group = CGroups::get($group_id);
        if ($group && !$group->getPerm(PERM_READ)) {
            $group_id = $app->getCurrentUserGroup();
        } else {
            $group_id = $group->_id;
        }

        $request->getSession()->set('g', $group_id);

        return $group_id;
    }

    /**
     * Selection of the function_id to use :
     *   - Extract the function_id from the GET parameter "f".
     *   - If the GET parameter "f" is not set use the "f" value from the session.
     *   - If function_id is still not set use the default user's function.
     *
     * Finally set the "f" parameter in session for next calls.
     *
     * @param Request     $request
     * @param AppUiBridge $app
     *
     * @return int|null The selected function id
     */
    private function extractFunctionId(Request $request, AppUiBridge $app): ?int
    {
        $session = $request->getSession();

        $function_id = $request->get(self::PARAM_FUNCTION, $session->get('f'));

        if (!$function_id) {
            $function_id = $app->getCurrentUserFunction();
        }

        $session->set('f', $function_id);

        return $function_id;
    }

    /**
     * @return string|null
     */
    public function getGroupId(): ?string
    {
        return $this->group_id;
    }

    /**
     * @return string|null
     */
    public function getFunctionId(): ?string
    {
        return $this->function_id;
    }

    /**
     * @return bool
     */
    public function isDialog(): bool
    {
        return $this->dialog;
    }

    /**
     * @return bool
     */
    public function isSuppressHeaders(): bool
    {
        return $this->suppress_headers;
    }

    /**
     * @return bool
     */
    public function isAjax(): bool
    {
        return $this->ajax;
    }

    /**
     * @return string|null
     */
    public function getController(): ?string
    {
        return $this->controller;
    }

    /**
     * @return string|null
     */
    public function getModule(): ?string
    {
        return $this->module;
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @return string|null
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @return CCanDo
     */
    public function getCan(): CCanDo
    {
        return $this->can;
    }


    /**
     * Legacy front-end compatibility with smarty global scope
     * @return void
     */
    private function defineGlobals(): void
    {
        global $m, $a, $tab, $g, $f, $action, $dialog, $ajax, $suppressHeaders, $can;
        $m               = $this->module;
        $a               = $action = $this->action;
        $g               = $this->group_id;
        $f               = $this->function_id;
        $action          = $this->module;
        $dialog          = $this->dialog;
        $ajax            = $this->ajax;
        $suppressHeaders = $this->suppress_headers;
        $can             = $this->can;
        $tab             = $this->route;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }
}
