<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use Exception;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestFormats;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Api\Serializers\ErrorSerializer;
use Ox\Core\AppUiBridge;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestContext;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\Security\Antivirus\Exception\InfectedFileException;
use Ox\Mediboard\System\Controllers\SystemController;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Exception listener for V2 calls
 * Keep exception and make api/gui response (or let sf debuger)
 */
class ExceptionListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    private LoggerInterface    $logger;
    private ContainerInterface $container;
    private AppUiBridge        $app_ui;

    /**
     * @param LoggerInterface $requestLogger Get the requestLogger because exceptions are on the request channel.
     */
    public function __construct(LoggerInterface $requestLogger, ContainerInterface $container, AppUiBridge $app_ui)
    {
        $this->logger    = $requestLogger;
        $this->container = $container;
        $this->app_ui    = $app_ui;
    }

    /**
     * @inheritdoc
     *
     * Do not put more than -1 on the KernelEvents::EXCEPTION event to let SF log the exception with a 0 priority.
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onException', -1],
        ];
    }

    /**
     * On exception check if the event is supported (not in _profiler).
     * And if the app is not in debug make a API/GUI response using the exception.
     *
     * @throws Exception
     */
    public function onException(ExceptionEvent $event): void
    {
        $throwable = $event->getThrowable();
        $request   = $event->getRequest();

        if (!$this->supports($event)) {
            $this->emptyBuffer();

            return;
        }

        // Make Response
        $status_code = $throwable instanceof HttpExceptionInterface ?
            $throwable->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;

        $headers = $throwable instanceof HttpExceptionInterface ?
            $throwable->getHeaders() : [];

        if ($this->isRequestApi($request)) {
            $response = $this->makeApiResponse($event, $status_code, $headers);
        } else {
            $response = $this->makeGuiResponse($event, $status_code, $headers);
        }

        $event->setResponse($response);
        $event->stopPropagation();
    }

    /**
     * @param ExceptionEvent $event
     * @param string         $status_code
     * @param array          $headers
     *
     * @return Response
     * @throws ApiException
     */
    protected function makeApiResponse(ExceptionEvent $event, string $status_code, array $headers = []): Response
    {
        $e = $event->getThrowable();

        $format = (new RequestFormats($event->getRequest()))->getExpected();
        $datas  = [
            'type'    => $this->encodeType($e),
            'code'    => $e->getCode(),
            'message' => $e->getMessage(),
        ];

        $resource = new Item($datas);
        $resource->setSerializer(ErrorSerializer::class);
        $resource->setFormat($format);

        return (new Controller())->renderApiResponse($resource, $status_code, $headers);
    }

    /**
     * Obfuscated exception's class
     *
     * @param object $e
     *
     * @return string
     */
    private function encodeType($e): string
    {
        return base64_encode(get_class($e));
    }

    /**
     * @throws RuntimeError
     * @throws LoaderError
     * @throws SyntaxError
     */
    protected function makeGuiResponse(
        ExceptionEvent $event,
        string         $status_code,
        array          $headers = []
    ): Response {
        // Clear the output to only display the error.
        if ($this->isRequestLegacy($event->getRequest())) {
            $this->emptyBuffer();
        }

        /** @var SystemController $controller */
        $controller = $this->container->get(SystemController::class);
        switch ($status_code) {
            case Response::HTTP_SERVICE_UNAVAILABLE:
                $response = $controller->offline(
                    $event->getThrowable()->getMessage(),
                    $event->getRequest()->isXmlHttpRequest()
                );
                break;
            case Response::HTTP_NOT_FOUND:
                $response = $controller->notFound($event->getThrowable(), $event->getRequest()->isXmlHttpRequest());
                break;
            default:
                if ($this->isRequestLegacy($event->getRequest()) && !$event->getRequest()->isXmlHttpRequest()) {
                    // Must force the module and action for tab translation purpose.
                    global $m, $tab, $a;
                    $request_context = new RequestContext($event->getRequest(), $this->app_ui, $m, $tab ?? $a);
                } else {
                    $request_context = new RequestContext($event->getRequest(), $this->app_ui);
                }

                $controller->setRequestContext($request_context);

                $response = $controller->errorOccured(
                    $event->getThrowable(),
                    $status_code,
                    $event->getRequest()->isXmlHttpRequest()
                );
                break;
        }

        $response->headers->add($headers);

        return $response;
    }

    /**
     * Do not use custom exception handler for profiler requests.
     * In profiler AppListener is not supported and CAppUI::conf is not initialized.
     */
    private function supports(KernelEvent $event): bool
    {
        $request = $event->getRequest();

        if (PHP_SAPI !== 'cli' && headers_sent()) {
            return false;
        }

        return !$this->isRequestProfiler($request) && $this->handleResponse($request);
    }

    /**
     * If not APP_DEBUG handle the response and do not let SF do it.
     * If the request is API and an ajax call handle the response to display the JSON error.
     */
    private function handleResponse(Request $request): bool
    {
        if (!$this->isAppDebug($request)) {
            return true;
        }

        if ($request->isXmlHttpRequest()) {
            return true;
        }

        return false;
    }

    /**
     * If output buffers exists clean them to have only the error display
     *
     * @return void
     */
    private function emptyBuffer(): void
    {
        while (ob_get_level() > 1) {
            ob_end_clean();
        }

        if (ob_get_level() > 0) {
            ob_clean();
        }
    }
}
