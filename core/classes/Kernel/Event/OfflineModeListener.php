<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use Ox\Core\Auth\SecurityHelperTrait;
use Ox\Core\Config\Conf;
use Ox\Core\Kernel\Exception\UnavailableApplicationException;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class OfflineModeListener implements EventSubscriberInterface
{
    use RequestHelperTrait;
    use SecurityHelperTrait;

    public const MODE_OFFLINE = 'offline';

    private Conf $conf;

    /**
     * @param Security $security
     * @param Conf     $conf
     */
    public function __construct(Security $security, Conf $conf)
    {
        $this->security = $security;
        $this->conf     = $conf;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => ['beforeController', 99],
        ];
    }

    public function beforeController(ControllerEvent $event): void
    {
        $request = $event->getRequest();
        if (!$this->supports($request)) {
            return;
        }

        $user = $this->getCUser();

        // Offline mode for non-admins
        if ($this->conf->get('offline_non_admin') && !$user->isTypeAdmin()) {
            $request->getSession()->invalidate();

            throw UnavailableApplicationException::applicationIsDisabledBecauseOfMaintenance();
        }
    }

    private function supports(Request $request): bool
    {
        $firewall_context = $request->attributes->get('_firewall_context', 'security.firewall.map.context.dev');
        if ($firewall_context === 'security.firewall.map.context.dev') {
            return false;
        }

        return !$this->isRequestPublic($request);
    }
}
