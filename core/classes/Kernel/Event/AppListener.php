<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use Exception;
use Ox\Core\CApp;
use Ox\Core\Config\Conf;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class AppListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    private CApp            $app;
    private Conf            $conf;
    private LoggerInterface $logger;
    private Security        $security;

    public function __construct(
        Conf            $conf,
        RouterInterface $router,
        LoggerInterface $logger,
        LoggerInterface $phpLogger,
        Security        $security
    ) {
        $this->app = CApp::getInstance();
        $this->app->setRouter($router);
        $this->app->setLogger($logger);
        $this->conf     = $conf;
        $this->logger   = $phpLogger;
        $this->security = $security;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST   => [
                ['onRequest', 100],
                ['initPublicEnvironment', 0], // SF Route listener is 32, auth is around 8
            ],
            KernelEvents::RESPONSE  => ['onResponse', 100],
            KernelEvents::TERMINATE => [
                ['disableReadonly', 100],
                ['triggerCallbacks', 80]
            ],
        ];
    }

    /**
     * @param RequestEvent $event
     *
     * @return void
     * @throws Exception
     */
    public function onRequest(RequestEvent $event)
    {
        if (!$event->isMainRequest()) {
            return;
        }

        if ($this->app->isStarted()) {
            return;
        }

        $this->app->start();
    }

    /**
     * @param RequestEvent $event
     *
     * @throws Exception
     */
    public function initPublicEnvironment(RequestEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $this->app->setPublic(
            $this->isRequestPublic($event->getRequest())
            && !$this->security->isGranted('IS_AUTHENTICATED_FULLY')
        );
    }

    /**
     * Disable the readonly state to log access and potential errors / long request.
     *
     * @param TerminateEvent $event
     *
     * @return void
     */
    public function disableReadonly(TerminateEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $this->app->setPublic(false);
    }

    /**
     * @param ResponseEvent $event
     *
     * @return void
     * @throws Exception
     */
    public function onResponse(ResponseEvent $event)
    {
        if (!$event->isMainRequest()) {
            return;
        }

        if (!$this->app->isStarted()) {
            $event->stopPropagation();

            return;
        }

        $request = $event->getRequest();
        $this->app->stop($request);
    }

    public function triggerCallbacks(TerminateEvent $event): void
    {
        if (!$this->isTerminateEventSupported($event)) {
            return;
        }

        $this->app::triggerCallbacksFunc($this->logger);
    }

    private function isTerminateEventSupported(TerminateEvent $event): bool
    {
        if (!$this->app->isStarted()) {
            $event->stopPropagation();

            return false;
        }

        if ($this->isRequestWebTestCase($event->getRequest())) {
            return false;
        }

        // Do not trigger callbacks for non admin user in offline_non_admin mode
        if ($this->conf->get('offline_non_admin')) {
            return false;
        }

        return true;
    }
}
