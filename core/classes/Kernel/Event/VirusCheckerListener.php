<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use Exception;
use Ox\Core\Security\Antivirus\Exception\InfectedFileException;
use Ox\Core\Security\Antivirus\Exception\UnavailableCheckerException;
use Ox\Core\Security\Antivirus\AntivirusService;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Check if the uploaded files or the files sent to the user contains a virus.
 */
class VirusCheckerListener implements EventSubscriberInterface
{
    public function __construct(
        private readonly AntivirusService $antivirus,
        private readonly LoggerInterface  $logger,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => ['onController', 50],
            KernelEvents::RESPONSE   => ['onResponse', 90],
        ];
    }

    /**
     * Check if uploaded files contains a virus.
     *
     * @param ControllerEvent $event
     *
     * @return void
     * @throws UnavailableCheckerException
     * @throws Exception
     */
    public function onController(ControllerEvent $event): void
    {
        if (!count($event->getRequest()->files) || !$this->support($event)) {
            return;
        }

        $this->antivirus->checkAntiVirusSignatureDatabaseDate();
        try {
            foreach ($event->getRequest()->files->all() as $files) {
                $this->antivirus->analyseFiles($files);
            }
        } catch (InfectedFileException $e) {
            $this->logger->critical($e->getMessage());
            throw $e;
        }
    }

    /**
     * TODO : Find a way to check StreamedResponse for virus. The problem is the callback is not executed yet.
     *
     * @param ResponseEvent $event
     *
     * @return void
     * @throws UnavailableCheckerException
     * @throws Exception
     */
    public function onResponse(ResponseEvent $event): void
    {
        $response = $event->getResponse();
        if (!($response instanceof BinaryFileResponse) || !$this->support($event)) {
            return;
        }

        $this->antivirus->checkAntiVirusSignatureDatabaseDate();
        try {
            $this->antivirus->analyseFiles($response->getFile());
        } catch (InfectedFileException $e) {
            $this->logger->critical($e->getMessage());
            throw $e;
        }
    }


    private function support(KernelEvent $event): bool
    {
        if (!$event->isMainRequest() || !$this->antivirus->isAvailable()) {
            return false;
        }

        return true;
    }
}
