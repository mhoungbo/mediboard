<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use DateTime;
use Ox\Core\CApp;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class HeadersListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    public const PRAGMA            = 'no-cache';
    public const COMPATIBILITY     = 'IE=edge';
    public const CONTENT_TYPE_HTML    = 'text/html;charset=';
    public const CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded';

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => ['onResponse', 90],
        ];
    }

    /**
     * Set custom headers if not already set
     *
     * @param ResponseEvent $event
     *
     * @return void
     */
    public function onResponse(ResponseEvent $event)
    {
        $response = $event->getResponse();

        if ($this->isRequestLegacy($event->getRequest())) {
            foreach (headers_list() as $header) {
                [$header_name, $header_value] = explode(':', $header, 2);
                $response->headers->set($header_name, $header_value);

                header_remove($header_name);
            }
        }

        $response_headers = $response->headers;

        if (!$this->isRequestApi($event->getRequest())) {
            // Override the default Cache-Control SF headers.
            $response_headers->addCacheControlDirective('no-cache, no-store, must-revalidate');
        }

        foreach ($this->getHeaders() as $header_name => $header_value) {
            if (!$response_headers->has($header_name)) {
                $response_headers->set($header_name, $header_value);
            }
        }
    }

    /**
     * @return array
     */
    private function getHeaders(): array
    {
        return [
            'Last-Modified'   => gmdate('D, d M Y H:i:s') . ' GMT',  // always modified
            'Pragma'          => self::PRAGMA,  // HTTP/1.0
            'X-UA-Compatible' => self::COMPATIBILITY,  // Force IE document mode
            'X-Request-ID'    => CApp::getRequestUID(),  // Correlates HTTP requests between a client and server
            'Content-Type'    => self::CONTENT_TYPE_HTML . CApp::$encoding, // Force charset depends on locales
        ];
    }
}
