<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use Exception;
use Ox\Core\AppUiBridge;
use Ox\Core\CApp;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Ox\Core\Controller;
use Ox\Core\Kernel\Routing\RequestContext;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\ResourceLoaders\CHTMLResourceLoader;
use Ox\Mediboard\Etablissement\CGroups;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Create RequestContext and hydrate it on Controller
 */
final class MainControllerListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    private const PARAM_AIO            = '_aio';
    private const PARAM_IGNORE_SCRIPTS = '_aio_ignore_scripts';

    private readonly Conf        $conf;
    private readonly AppUiBridge $app_ui;

    private readonly CApp $app;

    public function __construct(
        Conf        $conf = new Conf(),
        AppUiBridge $app_ui = new AppUiBridge(),
        CApp        $app = null,
    ) {
        $this->app    = $app ?? CApp::getInstance();
        $this->conf   = $conf;
        $this->app_ui = $app_ui;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            // onController must be just after PermissionListener (90)
            KernelEvents::CONTROLLER => ['onController', 89],
            KernelEvents::RESPONSE   => ['onResponse', 80],
        ];
    }

    /**
     * On controller event fix the date if needed.
     * For GUI only prepare the RequestContext object.
     *
     * @param ControllerEvent $event
     *
     * @throws InvalidArgumentException
     */
    public function onController(ControllerEvent $event)
    {
        $controller = $event->getController();
        $controller = is_array($controller) ? $controller[0] : $controller;

        if (!($controller instanceof Controller)) {
            return;
        }

        if (!$this->app->isPublic()) {
            // Force the current group system date if configured
            CMbDT::setSystemDate($this->conf->getForGroupId('system General system_date', CGroups::get()->_id));
        }

        if (!$this->isRequestGui($event->getRequest())) {
            return;
        }

        // Create the RequestContext and define the globals vars for legacy compatibility.
        $request_context = new RequestContext($event->getRequest(), $this->app_ui);
        $controller->setRequestContext($request_context);
    }


    /**
     * On response check if the request have the _aio parameter.
     * If so use the CHTMLResourceLoader to prepare a response with all the resources (img, stylesheet, ...).
     * If _aio = savefile make a StreamResponse to download a zip file containing all the resources of the response.
     *
     * @throws Exception
     */
    public function onResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        if (
            (!$this->isRequestGui($request) && !$this->isRequestLegacy($request))
            || !($aio = $request->get(self::PARAM_AIO))
        ) {
            return;
        }

        $response = $event->getResponse();

        $event->setResponse(
            CHTMLResourceLoader::makeAllInOneResponse($response, $aio, (bool)$request->get(self::PARAM_IGNORE_SCRIPTS))
        );
    }

    /**
     * Supports only request GUI
     */
    private function supports(KernelEvent $event): bool
    {
        return $this->isRequestGui($event->getRequest());
    }
}
