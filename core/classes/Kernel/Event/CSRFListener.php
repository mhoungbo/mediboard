<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use DOMException;
use Exception;
use Ox\Core\Config\Conf;
use Ox\Core\Kernel\Exception\InvalidCsrfTokenException;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\Kernel\Services\CsrfTokenDomManipulator;
use Ox\Core\Locales\Translator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * This listener generate & control anti-csrf token on request (gui/legacy) x-www-form-urlencoded
 */
final class CSRFListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    public const INPUT_FORM_NAME = '_form';
    public const INPUT_TOKEN     = '_token';

    private Translator $translator;

    private Conf $conf;

    private CsrfTokenManagerInterface $csrf_token_manager;

    private CsrfTokenDomManipulator $csrf_manipulator;

    public function __construct(
        CsrfTokenManagerInterface $csrf_token_manager,
        CsrfTokenDomManipulator $csrf_manipulator,
        Conf $conf,
        Translator $tr
    ) {
        $this->csrf_token_manager = $csrf_token_manager;
        $this->csrf_manipulator   = $csrf_manipulator;
        $this->conf               = $conf;
        $this->translator         = $tr;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            // Must be after the SF RouterListener::onKernelRequest (32).
            KernelEvents::REQUEST  => [
                ['onRequestGui', 30],
                ['onRequestLegacy', 30],
            ],
            KernelEvents::RESPONSE => ['onResponse', 85],
        ];
    }

    public function onRequestGui(RequestEvent $event)
    {
        $request = $event->getRequest();

        if (!$event->isMainRequest() || !$this->isRequestGui($request) || $this->isRequestWebTestCase($request)) {
            return;
        }

        // only post request
        if ($request->getMethod() !== Request::METHOD_POST) {
            return;
        }

        // Only x-www-form-urlencoded
        if (
            $request->headers->has("content-type")
            && !str_starts_with($request->headers->get("content-type"), HeadersListener::CONTENT_TYPE_FORM)
        ) {
            return;
        }

        // Check token exist (controller will check if token is valid)
        if (!$request->request->has('@token') && !$request->request->has('token')) {
            throw new InvalidCsrfTokenException("common-csrf-missing");
        }
    }

    /**
     * @param RequestEvent $event
     *
     * @return void
     * @throws Exception
     */
    public function onRequestLegacy(RequestEvent $event): void
    {
        if (!$this->supportsLegacy($event)) {
            return;
        }

        $request = $event->getRequest();

        // only post request
        if ($request->getMethod() !== Request::METHOD_POST) {
            return;
        }

        // Only x-www-form-urlencoded
        if (
            $request->headers->has("content-type")
            && !str_starts_with($request->headers->get("content-type"), HeadersListener::CONTENT_TYPE_FORM)
        ) {
            return;
        }

        // Ox or Sf token (not auto token)
        if ($request->request->has('@token') || $request->request->has('token')) {
            return;
        }

        // Check token
        if (!$request->request->has(self::INPUT_TOKEN) || !$request->request->has(self::INPUT_FORM_NAME)) {
            throw new InvalidCsrfTokenException("common-csrf-missing");
        }

        $token = new CsrfToken(
            $request->request->get(self::INPUT_FORM_NAME), $request->request->get(self::INPUT_TOKEN)
        );
        if (!$this->csrf_token_manager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $this->csrf_token_manager->removeToken($token->getId());
    }

    /**
     * @param ResponseEvent $event
     *
     * @return void
     * @throws DOMException
     * @throws Exception
     */
    public function onResponse(ResponseEvent $event): void
    {
        if (!$this->supportsLegacy($event)) {
            return;
        }

        $response = $event->getResponse();

        // Only text html
        if (!str_starts_with($response->headers->get("Content-type"), HeadersListener::CONTENT_TYPE_HTML)) {
            return;
        }

        // Check if we have form
        $content = $response->getContent();

        if (!preg_match('/<form\b[^>]*\bmethod=["\']post["\'][^>]*>/i', $content)) {
            return;
        }

        if (null !== $updated_content = $this->csrf_manipulator->addCsrfTokens($content)) {
            $response->setContent($updated_content);
        }
    }

    /**
     * @throws Exception
     */
    private function supportsLegacy(KernelEvent $event): bool
    {
        $req  = $event->getRequest();
        $conf = (bool)$this->conf->getStatic('system csrf auto');

        return $conf && $event->isMainRequest() && $this->isRequestLegacy($req);
    }
}
