<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use LogicException;
use Ox\Core\Auth\User;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\Controller;
use Ox\Core\CPermission;
use Ox\Core\CValue;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Mediboard\Admin\CPermModule;
use Ox\Mediboard\Admin\CPermObject;
use Ox\Mediboard\Etablissement\CGroups;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;

class PermissionListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    private CApp                     $app;
    private Security                 $security;
    private EventDispatcherInterface $dispatcher;

    /**
     * @param CApp                     $app
     * @param Security                 $security
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(CApp $app, Security $security, EventDispatcherInterface $dispatcher)
    {
        $this->app        = $app;
        $this->security   = $security;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => [['beforeController', 100], ['onController', 90]],
            SwitchUserEvent::class   => ['onSwitchUser', 80], // After WeakPasswordListener::onSwitchUser
        ];
    }

    public function beforeController(ControllerEvent $event): void
    {
        if (!$this->supportsController($event)) {
            return;
        }

        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->security->getUser();

            if (!($user instanceof User)) {
                throw new LogicException('Null or not supported user');
            }

            $this->app->beforeController();

            // Must set the global $g variable for API calls.
            if ($this->isRequestApi($event->getRequest())) {
                // Set the GLOBAL $g from CAppUI::$instance->_user_group in order to be used by RequestGroup.
                $GLOBALS['g'] = CAppUI::$instance->user_group;
                // Force the current group system date if configured
                CMbDT::setSystemDate(CAppUI::gconf('system General system_date'));
            }
        }

        $this->dispatcher->dispatch(
            new FrameworkInitializedEvent($event->getRequest()),
            FrameworkInitializedEvent::NAME
        );
    }

    /**
     * @param ControllerEvent $event
     *
     * @return void
     */
    public function onController(ControllerEvent $event): void
    {
        if (!$this->supportsController($event)) {
            return;
        }

        $request = $event->getRequest();

        $permission = new CPermission($this->extractController($event), $event->getRequest());

        $permission->checkModuleExists();

        // TODO [public] Check when removing public
        if ($this->isRequestPublic($request)) {
            return;
        }

        $permission->checkModulePerm();
    }

    /**
     * @param ControllerEvent $event
     *
     * @return bool
     */
    private function supportsController(ControllerEvent $event): bool
    {
        $controller = $this->extractController($event);

        // Only handle Controller subclasses
        return $this->supports($controller);
    }

    /**
     * @param ControllerEvent $event
     *
     * @return mixed
     */
    private function extractController(ControllerEvent $event)
    {
        $controller = $event->getController();

        return is_array($controller) ? $controller[0] : $controller;
    }

    /**
     * @param mixed $controller
     *
     * @return bool
     */
    private function supports($controller): bool
    {
        return is_subclass_of($controller, Controller::class);
    }

    public function onSwitchUser(SwitchUserEvent $event): void
    {
        // Substitution can't target current group if the substituted user hasn't write access on it
        $indexGroup = CGroups::get();

        // Build perms again (old user perms in cache)
        CPermObject::loadUserPerms();
        CPermModule::loadUserPerms();

        if (!$indexGroup->getPerm(PERM_EDIT)) {
            CValue::setSessionAbs("g", CAppUI::$instance->user_group);
        }
    }
}
