<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Kernel\Event;

use Ox\Core\Config\Conf;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Mediboard\System\Log\AccessLogManager;
use Ox\Mediboard\System\Log\LongRequestLogger;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Throwable;

class LogListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    private LongRequestLogger $long_request_logger;
    private Conf              $conf;
    private LoggerInterface   $logger;

    /**
     * @param LongRequestLogger $long_request_logger
     * @param Conf              $conf
     * @param LoggerInterface   $phpLogger
     */
    public function __construct(LongRequestLogger $long_request_logger, Conf $conf, LoggerInterface $phpLogger)
    {
        $this->long_request_logger = $long_request_logger;
        $this->conf                = $conf;
        $this->logger              = $phpLogger;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            // Between AppListener::terminate and AppListener::triggerCallbacks
            KernelEvents::TERMINATE => ['logHit', 85],
        ];
    }

    public function logHit(TerminateEvent $event): void
    {
        if (!$this->supports($event)) {
            return;
        }

        $this->long_request_logger->setResponse($event->getResponse());

        try {
            if ($this->isRequestLegacy($event->getRequest())) {
                $access_log_manager = AccessLogManager::createFromGlobals();
                $this->long_request_logger->logFromGlobals();
            } else {
                $access_log_manager = AccessLogManager::createFromRequest($event->getRequest());
                $this->long_request_logger->logFromRequest($event->getRequest());
            }

            // Access log
            $access_log_manager->log();
        } catch (Throwable $e) {
            $this->logger->log($e->getCode(), $e->getMessage(), ['exception' => $e]);
        }
    }

    private function supports(TerminateEvent $event): bool
    {
        $request = $event->getRequest();

        // Do not log for non admin user in offline mode
        if ($this->isRequestProfiler($request) || $this->conf->get('offline_non_admin')) {
            return false;
        }

        return (
            !$this->isRequestWebTestCase($request) ||
            $this->isAuthenticationWebTestCaseRequest($request)
        );
    }
}
