<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use DomainException;
use Exception;
use LogicException;
use Ox\Core\Api\Exceptions\ApiException;
use Ox\Core\Api\Request\RequestFormats;
use Ox\Core\Api\Resources\AbstractResource;
use Ox\Core\Api\Resources\Collection;
use Ox\Core\Api\Resources\Item;
use Ox\Core\Api\Serializers\JsonApiSerializer;
use Ox\Core\Auth\SecurityHelperTrait;
use Ox\Core\Config\Conf;
use Ox\Core\Kernel\Exception\ControllerException;
use Ox\Core\Kernel\Exception\HttpException;
use Ox\Core\Kernel\Exception\UnreachableDatabaseException;
use Ox\Core\Kernel\Routing\RequestContext;
use Ox\Core\Locales\Translator;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\Controllers\MainController;
use Ox\Mediboard\System\CSourceHTTP;
use Ox\Mediboard\System\Forms\CExObject;
use Ox\Mediboard\System\Pref;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Spatie\ArrayToXml\ArrayToXml;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Route;
use Throwable;

class Controller extends AbstractController
{
    use SecurityHelperTrait {
        getCUser as getCUserFromTrait;
        getCMediuser as getCMediuserFromTrait;
    }

    protected RequestContext $request_context;
    protected ?CApp          $app;
    protected Conf           $conf;
    protected Pref           $pref;
    protected Translator     $translator;

    public function __construct(
        ?CApp      $app = null,
        Conf       $conf = new Conf(),
        Pref       $pref = new Pref(),
        Translator $translator = new Translator(),
    ) {
        $this->app        = $app ?? CApp::getInstance();
        $this->conf       = $conf;
        $this->pref       = $pref;
        $this->translator = $translator;
    }

    /**
     * @return RequestContext
     */
    public function getRequestContext(): RequestContext
    {
        return $this->request_context;
    }

    /**
     * @param RequestContext $request_context
     */
    public function setRequestContext(RequestContext $request_context): void
    {
        $this->request_context = $request_context;
    }


    /**
     * Return empty response with UI MSG
     * @return Response
     */
    protected function renderEmptyResponse(int $status = Response::HTTP_OK): Response
    {
        $content = '';

        // Flush messages
        if ($msg = CAppUI::getMsg()) {
            $content .= $msg;
        }

        // Flush scripts
        if ($scripts = CAppUI::getScripts()) {
            $content .= implode('', $scripts);
        }

        return new Response($content, $status);
    }

    /**
     * @warning work in progress
     * Renders a smarty view.
     */
    protected function renderSmarty(
        string  $tpl,
        array   $parameters = [],
        ?string $template_module = null,
                $status = 200,
                $headers = []
    ): Response {
        $main = new MainController();

        CSmartyMB::setTokenManager($this->container->get('security.csrf.token_manager'));

        $content = null;

        // Header
        if (!$this->request_context->isSuppressHeaders()) {
            $module    = $this->request_context->getModule();
            $tab       = $this->request_context->getRoute();
            $is_dialog = $this->request_context->isDialog();
            $content   = $main->header(
                $module,
                $this->request_context->getRequest()->getSession(),
                $tab,
                $is_dialog,
                true
            );
        }

        // Tabbox open
        if (!$this->request_context->isDialog() && !$this->request_context->isSuppressHeaders()) {
            $content .= $main->tabboxOpen($this->getModule()->_tabs, $this->request_context->getRoute(), true);
        }

        // Template
        if ($template_module) {
            $template_module = "modules/$template_module";
        }

        $smarty = new CSmartyDP($template_module);
        $smarty->setRouter($this->container->get('router'));
        $smarty->assign($parameters);
        $content .= $smarty->fetch($tpl);

        // Tabbox close
        if (!$this->request_context->isDialog() && !$this->request_context->isSuppressHeaders()) {
            $content .= $main->tabboxClose(true);
        }

        // Flush messages
        if ($msg = CAppUI::getMsg()) {
            $content .= $msg;
        }

        // Flush scripts
        if ($scripts = CAppUI::getScripts()) {
            $content .= implode('', $scripts);
        }

        // Unlocalized
        if (!$this->request_context->isSuppressHeaders() || $this->request_context->isAjax()) {
            $content .= $main->unlocalized(true);
        }

        // Footer
        if (!$this->request_context->isSuppressHeaders()) {
            $content .= $main->footer(true);
        }

        // Ajax error
        if ($this->request_context->isAjax()) {
            $content .= $main->ajaxErrors(true);
        }

        return new Response($content, $status, $headers);
    }

    /**
     * @param string $content
     * @param int    $status  The response status code
     * @param array  $headers An array of response headers
     *
     * @return Response
     */
    public function renderResponse($content, $status = 200, $headers = []): Response
    {
        return new Response($content, $status, $headers);
    }

    /**
     * @param mixed $data    The response data
     * @param int   $status  The response status code
     * @param array $headers An array of response headers
     * @param bool  $json    If the data is already a JSON string
     *
     * @return JsonResponse
     */
    public function renderJsonResponse($data, $status = 200, $headers = [], $json = true): JsonResponse
    {
        return new JsonResponse($data, $status, $headers, $json);
    }

    /**
     * @param array $data
     * @param int   $status
     * @param array $headers
     * @param bool  $convert
     *
     * @return Response
     */
    public function renderXmlResponse(array $data, $status = 200, $headers = [], $convert = true): Response
    {
        if ($convert) {
            $data = ArrayToXml::convert($data, '', true, '');
        }

        $response = new Response($data, $status, $headers);
        $response->headers->set('content-type', RequestFormats::FORMAT_XML);

        return $response;
    }

    /**
     * @param AbstractResource $resource
     *
     * @param int              $status
     * @param array            $headers
     *
     * @return Response
     */
    public function renderApiResponse(
        AbstractResource $resource,
        int              $status = 200,
        array            $headers = [],
        bool             $is_etaggable = true
    ): Response {
        // Set resource router to inject him in when transform CModelObject::getApiLink
        if ($resource->needRouter()) {
            $resource->setRouter($this->container->get('router'));
        }

        switch ($resource->getFormat()) {
            // XML
            case RequestFormats::FORMAT_XML:
                $datas    = $resource->xmlSerialize();
                $response = $this->renderXmlResponse($datas, $status, $headers);
                break;
            // JSON
            case RequestFormats::FORMAT_JSON:
            default:
                $response = $this->renderJsonResponse($resource, $status, $headers, false);
                $response->setEncodingOptions($response->getEncodingOptions() | JSON_PRETTY_PRINT);
                if ($resource->getSerializer() === JsonApiSerializer::class) {
                    $response->headers->set('content-type', RequestFormats::FORMAT_JSON_API);
                }
        }

        if ($is_etaggable) {
            $etag = $this->createEtag($resource->getDatasTransformed(), $resource->getRequestUrl());
            $response->setEtag($etag);
        }

        return $response;
    }

    private function createEtag(array $datas, $url): string
    {
        return md5(serialize($datas) . $url);
    }

    /**
     * Return a streamed file response.
     *
     * @param string $content      Content of the file to stream
     * @param string $file_name    Name of the exported file with extension
     * @param string $content_type Content type (ex: application/csv)
     * @param string $disposition  Disposition (default: attachment)
     */
    public function renderFileResponse(
        string $content,
        string $file_name,
        string $content_type,
        string $disposition = 'attachment'
    ): StreamedResponse {
        return new StreamedResponse(
            function () use ($content): void {
                echo $content;
            },
            Response::HTTP_OK,
            [
                'Content-Type'              => $content_type,
                'Content-Disposition'       => $disposition . '; filename="' . $file_name . '";',
                'Content-Transfer-Encoding' => 'binary',
                'Content-Length'            => mb_strlen($content),
            ]
        );
    }


    protected function selfRequest(CSourceHTTP $source, string $method = 'GET', array $options = []): Response
    {
        if (!$source->active) {
            throw new ControllerException(Response::HTTP_INTERNAL_SERVER_ERROR, CAppUI::tr('access-forbidden'));
        }

        $client   = $source->getClient();
        $response = $client->request(
            $method,
            $source->host,
            array_merge(['ox-token' => $source->getToken(), $options])
        );

        $response_header = $response->getHeaders();
        $http_code       = $response->getStatusCode();
        $acq             = $response->getBody()->__toString();

        // For public routes do not send a HTTP_UNAUTHORIZED response
        if ($http_code == Response::HTTP_UNAUTHORIZED) {
            throw new ControllerException(Response::HTTP_INTERNAL_SERVER_ERROR, CAppUI::tr('access-forbidden'));
        }

        return new Response(
            $acq,
            $http_code,
            $this->prepareHeaderFromResponse($response_header)
        );
    }

    /**
     * @return string|null The module name
     * @throws Exception
     */
    public function getModuleName(): ?string
    {
        return CClassMap::getInstance()->getClassMap(static::class)->module;
    }

    /**
     * @return ReflectionClass
     * @throws ControllerException
     */
    public function getReflectionClass(): ?ReflectionClass
    {
        try {
            return new ReflectionClass($this);
        } catch (ReflectionException $e) {
            throw new ControllerException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                'Unable to construct the ReflectionClass'
            );
        }
    }

    /**
     * @param string $method_name
     *
     * @return ReflectionMethod
     * @throws ControllerException
     */
    public function getReflectionMethod($method_name): ?ReflectionMethod
    {
        try {
            return ($this->getReflectionClass())->getMethod($method_name);
        } catch (ReflectionException $e) {
            throw new ControllerException(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    /**
     * Get Controller onstance from attributes _controller in Request
     *
     * @param Request $request
     *
     * @return mixed
     * @throws ControllerException
     */
    public static function getControllerFromRequest(Request $request)
    {
        $_controller = $request->attributes->get('_controller');
        $_controller = explode('::', $_controller);
        $_controller = $_controller[0];
        if (!class_exists($_controller)) {
            throw new ControllerException(Response::HTTP_INTERNAL_SERVER_ERROR, "Invalid controller {$_controller}.");
        }

        return new $_controller();
    }

    /**
     * @param Route $route
     *
     * @return Controller
     * @throws ControllerException
     */
    public static function getControllerFromRoute(Route $route)
    {
        $controller_name = $route->getDefault('_controller');

        $_controller = explode('::', $controller_name ?? '');
        $_controller = $_controller[0];
        if (!class_exists($_controller)) {
            throw new ControllerException(Response::HTTP_INTERNAL_SERVER_ERROR, "Invalid controller {$_controller}.");
        }

        return new $_controller();
    }

    /**
     * Get method
     *
     * @param Route $route
     *
     * @return string
     * @throws ControllerException
     */
    public static function getMethodFromRoute(Route $route)
    {
        $controller_name = $route->getDefault('_controller') ?? "";

        $_controller = explode('::', $controller_name);
        $method      = $_controller[1];
        $controller  = $_controller[0];

        if (!method_exists(new $controller(), $method)) {
            throw new ControllerException(Response::HTTP_INTERNAL_SERVER_ERROR, "Invalid method {$method}.");
        }

        return $method;
    }

    public function getRootDir(): string
    {
        return dirname(__DIR__, 2);
    }

    final protected function getApplicationUrl(): string
    {
        return rtrim($this->conf->get('external_url'), '/');
    }

    public function checkPermEdit(CStoredObject $object, Request $request = null): bool
    {
        return (bool)($object->canDo())->edit;
    }

    public function checkPermRead(CStoredObject $object, Request $request = null): bool
    {
        return (bool)($object->canDo())->read;
    }

    public function checkPermAdmin(CStoredObject $object, Request $request = null): bool
    {
        return (bool)($object->canDo())->admin;
    }

    /**
     * Return TRUE if current user as READ permission on controller's module.
     *
     * @return bool
     */
    public function checkModulePermRead(): bool
    {
        return $this->checkPermRead($this->getModule());
    }

    /**
     * Return TRUE if current user as EDIT permission on controller's module.
     *
     * @return bool
     */
    public function checkModulePermEdit(): bool
    {
        return $this->checkPermEdit($this->getModule());
    }

    /**
     * Return TRUE if current user as ADMIN permission on controller's module.
     *
     * @return bool
     */
    public function checkModulePermAdmin(): bool
    {
        return $this->checkPermAdmin($this->getModule());
    }

    /**
     * Return the controller's module.
     *
     * @return CModule
     * @throws DomainException|LogicException
     */
    protected function getModule(): CModule
    {
        try {
            $module_name = $this->getModuleName();

            if ($module_name === null) {
                throw new Exception();
            }
        } catch (Throwable $t) {
            throw new DomainException('Controller does not have a related module.');
        }

        $module = CModule::getActive($module_name);

        if (!($module instanceof CModule) || ($module->mod_name !== $module_name)) {
            throw new LogicException(sprintf("Unable to find controller's module: %s.", $module_name));
        }

        return $module;
    }

    /**
     * Get and check if module is active
     *
     * @param string $mod_name
     *
     * @return string
     * @throws HttpException
     */
    protected function getActiveModule(string $mod_name): string
    {
        // dP add super hack
        if (CModule::getActive($mod_name) === null) {
            $mod_name = 'dP' . $mod_name;

            if (CModule::getActive($mod_name) === null) {
                throw new HttpException(
                    Response::HTTP_NOT_FOUND,
                    "Module '{$mod_name}' does not exists or is not active",
                );
            }
        }

        return $mod_name;
    }

    /**
     * Store item and return an API response with item in content
     *
     * @param CStoredObject $object
     * @param int           $status
     * @param array         $headers
     *
     * @return Response|null
     * @throws ApiException
     * @throws CMbException
     */
    public function storeObjectAndRenderApiResponse(
        CStoredObject $object,
        int           $status = Response::HTTP_CREATED,
        array         $headers = []
    ): ?Response {
        if (!($item = $this->storeObject($object))) {
            return null;
        }

        return $this->renderApiResponse($item, $status, $headers);
    }

    /**
     * Store collection and return an API response with items in content
     *
     * @param CModelObjectCollection $object_collection
     * @param int                    $status
     * @param array                  $headers
     *
     * @return Response|null
     * @throws ApiException
     * @throws CMbException
     */
    public function storeCollectionAndRenderApiResponse(
        CModelObjectCollection $object_collection,
        int                    $status = Response::HTTP_CREATED,
        array                  $headers = []
    ): ?Response {
        if (!($collection = $this->storeCollection($object_collection))) {
            return null;
        }

        return $this->renderApiResponse($collection, $status, $headers);
    }

    /**
     * @throws ApiException
     * @throws CMbException
     */
    protected function storeObject(CStoredObject $object, bool $return_item = true)
    {
        if (!$this->checkPermEdit($object)) {
            throw new CMbException('Access denied');
        }

        if ($msg = $object->store()) {
            throw new CMbException($msg);
        }

        return ($return_item) ? new Item($object) : $object;
    }

    protected function storeObjectAndReturnEmptyResponse(
        CStoredObject $object,
        bool          $new,
        bool          $notification = true
    ): Response {
        try {
            $this->storeObject($object, false);
        } catch (CMbException $e) {
            $this->addUiMsgError($e->getMessage(), $notification);

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }

        $this->addUiMsgOk("$object->_class-msg-" . ($new ? 'create' : 'modify'), $notification);

        return $this->renderEmptyResponse();
    }

    /**
     * Delete an object, throw an exception if a failure occure.
     *
     * @throws CMbException
     */
    protected function deleteObject(CStoredObject $object): void
    {
        if (!$this->checkPermEdit($object)) {
            throw new CMbException('Access denied');
        }

        if ($msg = $object->delete()) {
            throw new CMbException($msg);
        }
    }

    protected function deleteObjectAndReturnEmptyResponse(CStoredObject $object, bool $notification = true): Response
    {
        try {
            $this->deleteObject($object);
        } catch (CMbException $e) {
            $this->addUiMsgError($e->getMessage(), $notification);

            return $this->renderEmptyResponse(Response::HTTP_BAD_REQUEST);
        }

        $this->addUiMsgOk($object->_class . '-msg-delete', $notification);

        return $this->renderEmptyResponse();
    }

    /**
     * @return Collection|array
     * @throws ApiException|CMbException
     *
     */
    protected function storeCollection(CModelObjectCollection $collection, bool $return_coll = true)
    {
        $objects = [];

        foreach ($collection as $object) {
            $objects[] = $this->storeObject($object, false);
        }

        return $return_coll ? new Collection($objects) : $objects;
    }

    protected function prepareHeaderFromResponse(array $response_headers): array
    {
        unset($response_headers['Server']);
        unset($response_headers['X-Powered-By']);
        unset($response_headers['X-Mb-RequestInfo']);
        unset($response_headers['Set-Cookie']);
        unset($response_headers['Transfer-Encoding']);

        return $response_headers;
    }

    /**
     * @return CUser
     *
     * @throws LogicException
     */
    protected function getCUser(): CUser
    {
        return $this->getCUserFromTrait(null, parent::getUser());
    }

    /**
     * @return CMediusers
     *
     * @throws LogicException
     */
    protected function getCMediuser(): CMediusers
    {
        return $this->getCMediuserFromTrait(null, parent::getUser());
    }

    /**
     * Todo: Since these messages are supposed to be sent to the user, we should automatically translate them here.
     *
     * Add an info-type Flash Message.
     *
     * @param string $message
     *
     * @return void
     */
    protected function info(string $message): void
    {
        $this->addFlash('info', $message);
    }

    /**
     * Todo: Since these messages are supposed to be sent to the user, we should automatically translate them here.
     *
     * Add an error-type Flash Message.
     *
     * @param string $message
     *
     * @return void
     */
    protected function error(string $message): void
    {
        $this->addFlash('error', $message);
    }

    private function addUiMsg(int $type, string $msg, bool $notification, array $args = []): void
    {
        if ($notification) {
            CAppUI::displayAjaxMsg($msg, $type, ...$args);
        } else {
            CAppUI::setMsg($msg, $type, ...$args);
        }
    }

    protected function addUiMsgOk(string $message, bool $notification = false, ...$args): void
    {
        $this->addUiMsg(CAppUI::UI_MSG_OK, $message, $notification, $args);
    }

    protected function addUiMsgAlert(string $message, bool $notification = false, ...$args): void
    {
        $this->addUiMsg(CAppUI::UI_MSG_ALERT, $message, $notification, $args);
    }

    protected function addUiMsgWarning(string $message, bool $notification = false, ...$args): void
    {
        $this->addUiMsg(CAppUI::UI_MSG_WARNING, $message, $notification, $args);
    }

    protected function addUiMsgError(string $message, bool $notification = false, ...$args): void
    {
        $this->addUiMsg(CAppUI::UI_MSG_ERROR, $message, $notification, $args);
    }

    protected function enforceSlave(bool $die_error = true): void
    {
        if (!$this->conf->get("enslaving_active")) {
            return;
        }

        if (CSQLDataSource::isSlaveState()) {
            return;
        }

        // Test wether a slave datasource has been configured
        if (!$this->conf->get("db slave dbhost")) {
            return;
        }

        // Check connection to the slave datasource
        if (!CSQLDataSource::get("slave", true)) {
            if ($die_error) {
                throw new UnreachableDatabaseException(500, (new Translator())->tr('common-Error-Slave not reachable'));
            } else {
                return;
            }
        }

        CSQLDataSource::setSlaveState(true);
    }

    protected function disableSlave(): void
    {
        if (CSQLDataSource::isEnslaved() && !$this->app->isPublic()) {
            CSQLDataSource::setSlaveState(false);
        }
    }

    /**
     * Load an object from it's API resource type and id.
     *
     * @throws InvalidArgumentException
     * @throws Exception
     */
    protected function getObjectFromResourceTypeAndId(
        string $resource_type,
        string $resource_id,
        ?int   $ex_class_id = null
    ): CStoredObject {
        $object_class = CModelObject::getClassNameByResourceType($resource_type);

        if (!is_subclass_of($object_class, CStoredObject::class)) {
            throw new ControllerException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $this->translator->tr('Controller-Error-Resource type must be storable', $resource_type)
            );
        }

        if ($object_class === CExObject::class && $ex_class_id) {
            $object = new CExObject($ex_class_id);
        } else {
            $object = new $object_class();
        }

        $object->load($resource_id);

        if (!$object->_id) {
            throw new ControllerException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $this->translator->tr('Controller-Error-Invalide identifier'),
                [],
                1
            );
        }

        return $object;
    }
}
