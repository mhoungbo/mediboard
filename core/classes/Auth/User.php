<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth;

use InvalidArgumentException;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Acts as a bridge between UserInterface and the legacy CUser class
 */
class User implements UserInterface
{
    private const USER_ROLE = 'ROLE_USER';

    /** @var string */
    private string $username;

    /** @var array */
    private array $roles = [];

    /** @var CUser */
    private CUser $ox_user;

    /**
     * @param CUser $user
     *
     * @throws InvalidArgumentException
     */
    public function __construct(CUser $user)
    {
        if (!$user->_id || !$user->user_username) {
            throw new InvalidArgumentException('Invalid user provided');
        }

        // Encoding username here because SF wants an UTF-8 encoded string when displaying the username in the WDT.
        $this->username = mb_convert_encoding($user->user_username, 'UTF-8', 'ISO-8859-1');
        $this->ox_user  = $user;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = self::USER_ROLE;

        return array_unique($roles);
    }

    /**
     * @inheritDoc
     */
    public function getPassword(): ?string
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->getUserIdentifier();
    }

    /**
     * @inheritDoc
     */
    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function getOxUser(): CUser
    {
        return $this->ox_user;
    }

    public function equals(self $other): bool
    {
        return ($this->username === $other->username)
            && ($this->getRoles() === $other->getRoles())
            && ($this->ox_user->_id === $other->ox_user->_id);
    }

    /**
     * @return string[]
     * @internal DO NOT serialize this object.
     */
    public function __serialize(): array
    {
        return [
            'username' => $this->username,
        ];
    }

    /**
     * @param array $data
     *
     * @return void
     * @internal DO NOT serialize this object.
     */
    public function __unserialize(array $data): void
    {
        // Unserialization is voluntarily incomplete here because we are refreshing the user
        // from the database in the ContextListener.
        $this->username = $data['username'];
    }
}
