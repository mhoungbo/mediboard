<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Exception;

use Ox\Core\Locales\Translator;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Throwable;

final class CouldNotValidatePreAuthentication extends AccountStatusException
{
    /**
     * @inheritDoc
     */
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        $translator = new Translator();

        parent::__construct($translator->tr($message), $code, $previous);
    }

    /**
     * @return self
     */
    public static function userIsNotSupported(): self
    {
        return new self('Auth-error-User not supported');
    }

    /**
     * @return self
     */
    public static function userIsATemplate(): self
    {
        return new self('Auth-failed-template');
    }

    /**
     * @return self
     */
    public static function userIsSecondary(): self
    {
        return new self('CUserAuthentication-error-Connection of secondary user is not permitted.');
    }
}
