<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Exception;

use Ox\Core\Locales\Translator;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;

final class CouldNotValidatePostAuthentication extends CustomUserMessageAccountStatusException
{
    /**
     * @inheritDoc
     */
    private function __construct(string $message = '')
    {
        $translator = new Translator();

        parent::__construct($translator->tr($message));
    }

    /**
     * @return self
     */
    public static function userIsATemplate(): self
    {
        return new self('Auth-failed-template');
    }

    /**
     * @return self
     */
    public static function userIsSecondary(): self
    {
        return new self('CUserAuthentication-error-Connection of secondary user is not permitted.');
    }

    /**
     * @return self
     */
    public static function userIsDeactivated(): self
    {
        return new self('Auth-failed-user-deactivated');
    }

    /**
     * @return self
     */
    public static function userAccountHasExpired(): self
    {
        return new self('Auth-failed-user-deactivated');
    }

    /**
     * @return self
     */
    public static function userIsLocked(): self
    {
        return new self('Auth-failed-user-locked');
    }

    /**
     * @return self
     */
    public static function userHasNoRemoteAccess(): self
    {
        return new self('Auth-failed-user-noremoteaccess');
    }

    /**
     * @return self
     */
    public static function userHasNoAccessFromThisLocation(): self
    {
        return new self('Auth-failed-Authentication is not allowed from your location.');
    }
}
