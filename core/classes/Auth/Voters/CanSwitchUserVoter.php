<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Voters;

use Ox\Core\Auth\User;
use Ox\Core\Auth\WeakPassword;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CanSwitchUserVoter extends Voter
{
    private const SUPPORTED_ATTRIBUTE = 'CAN_SWITCH_USER';

    private WeakPassword $weak_password;

    public function __construct(WeakPassword $weak_password)
    {
        $this->weak_password = $weak_password;
    }

    /**
     * @inheritDoc
     */
    protected function supports(string $attribute, $subject): bool
    {
        return $this->supportsAttribute($attribute) && ($subject instanceof User);
    }

    /**
     * @inheritDoc
     */
    public function supportsType(string $subjectType): bool
    {
        return parent::supportsType($subjectType)
            && (
                ($subjectType === User::class)
                || is_subclass_of($subjectType, User::class)
            );
    }

    public function supportsAttribute(string $attribute): bool
    {
        return $attribute === self::SUPPORTED_ATTRIBUTE;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!($user instanceof User) || !($subject instanceof User)) {
            return false;
        }

        // We cannot impersonate if currently obliged to update our password.
        if ($this->weak_password->check()) {
            return false;
        }

        $ox_user = $user->getOxUser();

        return ($ox_user->isSuperAdmin() || $ox_user->isTypeAdmin());
    }
}
