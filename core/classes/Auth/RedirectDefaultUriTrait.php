<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth;

use LogicException;
use Ox\Core\Module\CModule;
use Ox\Mediboard\System\Pref;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

trait RedirectDefaultUriTrait
{
    /**
     * Get a RedirectResponse according to given parameter in the Request or according to the default preference.
     *
     * @param Request     $request
     * @param int         $user_id
     * @param string|null $name The name of the SESSION parameter to check for. Keep NULL if no check is asked.
     *
     * @return RedirectResponse
     */
    public function getRedirectResponse(
        Request $request,
        int     $user_id,
        string  $name = null
    ): RedirectResponse {
        if ($name !== null) {
            $redirect_uri = $request->getSession()->get($name);

            if (($redirect_uri !== null) && ($redirect_uri !== '') && (strpos($redirect_uri, 'logout=') === false)) {
                // Removing the redirect URI from the session
                $request->getSession()->remove($name);

                return new RedirectResponse($redirect_uri);
            }
        }

        $path = dirname($_SERVER["SCRIPT_NAME"]);
        $path = rtrim($path, '/');

        $redirect_uri = (new Pref())->load('DEFMODULE', $user_id);

        if (($redirect_uri === null) || ($redirect_uri === '')) {
            return new RedirectResponse("{$path}/");
        }

        $parts = explode('-', $redirect_uri, 2);

        $module = CModule::getActive($parts[0]);
        $redirect_uri = $module->generateModuleUrl($path, $parts[1] ?? null);

        // Handle case of redirection to legacy
        if (str_starts_with($redirect_uri, '?')) {
            $redirect_uri = '/' . $redirect_uri;
        }

        return new RedirectResponse($redirect_uri);
    }

    public function getRedirectIfLogged(Request $request, Security $security): ?Response
    {
        if (!$security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return null;
        }

        $user = $security->getUser();

        if (!($user instanceof User)) {
            throw new LogicException('No user in Security');
        }

        return $this->getRedirectResponse($request, $user->getOxUser()->_id);
    }
}
