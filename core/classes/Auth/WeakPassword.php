<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth;

use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class WeakPassword
{
    use RequestHelperTrait;

    public const WEAK_PWD            = 'weak-password';
    public const LDAP_REMAINING_DAYS = 'ldap_pwd_remaining_days';

    private Request $current_request;

    /**
     * @param RequestStack $request_stack
     */
    public function __construct(RequestStack $request_stack)
    {
        $this->current_request = $request_stack->getCurrentRequest();
    }

    public function check(): bool
    {
        return (bool)$this->current_request->getSession()->get(self::WEAK_PWD, false);
    }

    public function set(): void
    {
        $this->mutate(true);
    }

    public function unset(): void
    {
        $this->mutate(false);
    }

    public function getRemainingDays(): int
    {
        return (int)$this->current_request->getSession()->get(self::LDAP_REMAINING_DAYS, 0);
    }

    public function resetRemainingDays(): void
    {
        $this->current_request->getSession()->remove(self::LDAP_REMAINING_DAYS);
    }

    private function mutate(bool $value): void
    {
        $this->current_request->getSession()->set(self::WEAK_PWD, $value);
    }
}
