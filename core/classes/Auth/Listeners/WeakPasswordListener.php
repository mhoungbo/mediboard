<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Listeners;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\User;
use Ox\Core\Auth\WeakPassword;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\Locales\Translator;
use Ox\Mediboard\Admin\Controllers\AccountController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;

class WeakPasswordListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    private Security              $security;
    private UrlGeneratorInterface $router;
    private WeakPassword          $weak_password;
    private Translator            $translator;

    /**
     * @param Security              $security
     * @param UrlGeneratorInterface $router
     * @param WeakPassword          $weak_password
     * @param Translator            $translator
     */
    public function __construct(
        Security              $security,
        UrlGeneratorInterface $router,
        WeakPassword          $weak_password,
        Translator            $translator
    ) {
        $this->security      = $security;
        $this->router        = $router;
        $this->weak_password = $weak_password;
        $this->translator    = $translator;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            LoginSuccessEvent::class => ['onLoginSuccess', 90],
            SwitchUserEvent::class   => ['onSwitchUser', 90], // After AuthenticationListener::onSwitchUser
            RequestEvent::class      => ['onRequest', 0],
        ];
    }

    public function onLoginSuccess(LoginSuccessEvent $event): void
    {
        $passport = $event->getPassport();

        /** @var AuthMetadataBadge $metadata */
        $metadata = $passport->getBadge(AuthMetadataBadge::class);

        if ($metadata === null) {
            return;
        }

        if ($metadata->isWeakPassword()) {
            $this->weak_password->set();
        }

        /** @var User $user */
        $user    = $event->getUser();
        $ox_user = $user->getOxUser();

        if ($ox_user->force_change_password) {
            $this->weak_password->set();
        }
    }

    public function onSwitchUser(SwitchUserEvent $event): void
    {
        // Resetting weak password flag when impersonating
        $this->weak_password->unset();
        $this->weak_password->resetRemainingDays();
    }

    public function onRequest(RequestEvent $event): void
    {
        if (!$this->supports($event)) {
            return;
        }

        $request = $event->getRequest();

        if (
            $this->weak_password->check()
            && !in_array(
                $request->attributes->get('_route'),
                AccountController::CHANGE_PASSWORD_ROUTES
            )
            && !$this->isActivatingAccount($request)
        ) {
            $response = new RedirectResponse(
                $this->router->generate(
                    AccountController::CHANGE_PASSWORD_VIEW,
                    ['action' => 'change_password']
                )
            );

            $event->setResponse($response);
        }
    }

    private function supports(RequestEvent $event): bool
    {
        // isRequestProfiler actually not needed, but... we dont know.
        return $event->isMainRequest()
            && !$this->isRequestApi($event->getRequest()) // Todo: Handle this with AuthMetadata instead
            && !$this->isRequestProfiler($event->getRequest())
            && $this->security->isGranted('IS_AUTHENTICATED_FULLY');
    }

    /**
     * An user is activating is account if he use a token (route "admin_token") and the route_controller (argument set
     * by the token) is AccountController::show (the route to display the password change.
     *
     * @param Request $request
     *
     * @return bool
     */
    private function isActivatingAccount(Request $request): bool
    {
        if (!($route_controller = $request->attributes->get('route_controller'))) {
            return false;
        }

        return AccountController::class . '::show' === $route_controller
            && 'admin_token' === $request->attributes->get('_route');
    }
}
