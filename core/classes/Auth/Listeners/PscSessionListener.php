<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Listeners;

use Exception;
use Ox\Core\Config\Conf;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\OAuth2\OIDC\PSC\Client;
use Ox\Core\OAuth2\OIDC\TokenSet;
use Ox\Mediboard\Admin\Controllers\LoginController;
use Ox\Core\OAuth2\OIDC\PSC\PscOptions;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Throwable;

// Todo: We should have an OIDCToken Authenticator instead, but would be difficult to use when in legacy app
//   (same as KRB in fact).
class PscSessionListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    private Security              $security;
    private UrlGeneratorInterface $router;
    private Conf                  $conf;
    private PscOptions            $options;

    /**
     * @param Security              $security
     * @param UrlGeneratorInterface $router
     * @param Conf                  $conf
     */
    public function __construct(Security $security, UrlGeneratorInterface $router, Conf $conf, PscOptions $options)
    {
        $this->security = $security;
        $this->router   = $router;
        $this->conf     = $conf;
        $this->options  = $options;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            SwitchUserEvent::class => ['onSwitchUser', 90],
            RequestEvent::class    => ['onRequest', -100],
            LogoutEvent::class     => ['onLogout', 10], // Before SessionListener::onLogout
        ];
    }

    private function getOIDCTokens(Request $request): ?TokenSet
    {
        $oidc_tokens   = $request->getSession()->get('oidc_tokens');

        if (($oidc_tokens instanceof TokenSet) && $oidc_tokens->isPSC()) {
            return $oidc_tokens;
        }

        return null;
    }

    // Todo: What to do when a PSC-connected user impersonate someone else and then logout?
    public function onSwitchUser(SwitchUserEvent $event): void
    {
    }

    public function onRequest(RequestEvent $event): void
    {
        if (!$this->supports($event)) {
            return;
        }

        $oidc_tokens = $this->getOIDCTokens($event->getRequest());

        if ($this->options->getSessionMode() && ($oidc_tokens !== null)) {
            // Cannot autowire legacy Client because it's not a full service
            $client = new Client($this->options);

            // Todo: I don't like that but didn't find how to disconnect a user with just a controller
            if ($client->hasRefreshTokenExpired()) {
                $response = new RedirectResponse($this->router->generate(LoginController::LOGOUT_ROUTE_NAME));
                $event->setResponse($response);

                return;
            }

            // Todo: Make Exceptions more distinct (notably Expired Token from An Error Occurred)
            // Todo: Should disconnect if Network Exception
            try {
                $client->refreshToken($oidc_tokens->getRefreshToken(), $oidc_tokens);
            } catch (Exception $e) {
                $event->getRequest()->getSession()->remove('oidc_tokens');

                $response = new RedirectResponse($this->router->generate(LoginController::LOGOUT_ROUTE_NAME));
                $event->setResponse($response);
            }
        }
    }

    public function onLogout(LogoutEvent $event): void
    {
        $oidc_tokens = $this->getOIDCTokens($event->getRequest());

        if ($oidc_tokens === null) {
            return;
        }

        try {
            $client   = new Client($this->options);
            $endpoint = $client->signOut($oidc_tokens->getIdToken(), true);

            $event->setResponse(new RedirectResponse($endpoint));
        } catch (Throwable $t) {
            // Todo: Nothing?
        }
    }

    private function supports(RequestEvent $event): bool
    {
        // isRequestProfiler actually not needed, but... we dont know.
        return $event->isMainRequest()
            && !$this->isRequestProfiler($event->getRequest())
            && $this->security->isGranted('IS_AUTHENTICATED_FULLY');
    }
}
