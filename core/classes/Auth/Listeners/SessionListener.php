<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Listeners;

use LogicException;
use Ox\Core\AppUiBridge;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\Sessions\Handler\SessionHandlerInterface;
use Ox\Core\Sessions\SessionHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

/**
 * Subscriber handling session-related events.
 */
class SessionListener implements EventSubscriberInterface
{
    use RequestHelperTrait;

    private Security    $security;
    private AppUiBridge $app_ui;

    private SessionHandlerInterface $session_handler;
    private CApp                    $app;

    public function __construct(
        Security                $security,
        AppUiBridge             $app_ui,
        SessionHandlerInterface $session_handler,
        CApp                    $app,
    ) {
        $this->security        = $security;
        $this->app_ui          = $app_ui;
        $this->session_handler = $session_handler;
        $this->app             = $app;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST    => ['startSession', 99], // Just after AppListener::onRequest
            KernelEvents::CONTROLLER => ['closeSession', 55], // Before actually calling the controller
            KernelEvents::RESPONSE   => ['onResponse', -999],
        ];
    }

    public function startSession(RequestEvent $event): void
    {
        if (!$this->supports($event)) {
            return;
        }

        // Don't ignore user abort as long as session is still locked
        ignore_user_abort(false);

        $session = $event->getRequest()->getSession();

        // DO NOT use session ID from here, as it will be regenerated after login.

        // Default bag
        $attributes_bag = new AttributeBag();
        $session->registerBag($attributes_bag);

        // Setting the session's custom name.
        $session->setName(SessionHelper::getSessionName());

        // Explicit start for traceability purpose (SF does not seem to call our SessionStorage but the native one).
        $session->start();

        // --- Session is ready to be used from here ---

        // Technically the first use of the session
        // Legacy AppUi
        $appui = $session->get('AppUi');
        if ($appui !== null) {
            CAppUI::$instance = $appui;
        }

        $this->app_ui->setSessionRevive($event->getRequest()->query->getBoolean('session_no_revive'));

        // Ignore aborted HTTP request, so that PHP finishes the current script
        ignore_user_abort(true);

        // If the user is not logged load default preferences
        if (!CAppUI::$instance || !CAppUI::$instance->user_id) {
            CAppUI::loadPrefs();
        }

        $lifetime         = (int)(CAppUI::$instance->user_prefs['sessionLifetime'] ?? 0);
        $session_lifetime = $lifetime * 60;

        if ($session_lifetime > 0) {
            $this->session_handler->setLifetime($session_lifetime);
        }

        // --- REVIVE
        $auth = CAppUI::$instance->_ref_last_auth;
        $now  = CMbDT::dateTime();

        if ($this->app_ui->canReviveSession()) {
            if (
                $auth
                && ($now > CMbDT::dateTime("+ 60 second", $auth->last_session_update))
                && ($now < $auth->expiration_datetime)
            ) {
                $auth->expiration_datetime = CMbDT::dateTime("+ {$auth->session_lifetime} second", $now);
                $auth->last_session_update = $now;
                $auth->nb_update++;

                $auth->store();
            }
        }

        // Legacy helper
        $this->app->setSessionHelper(new SessionHelper($this->session_handler, $session));
    }

    public function onResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        if (!$request->hasSession()) {
            return;
        }

        $session = $request->getSession();

        if (function_exists('apache_setenv')) {
            apache_setenv('PHP_SESS_ID', $session->getId());
        }

        try {
            // Re-storing CAppUI in session in case of modification of the object during script execution.
            $session->set('AppUi', CAppUI::$instance);
        } catch (LogicException) {
            // If session is not ready to use here, we encountered an error before calling SessionListener::startSession
            return;
        }

        // Preserving session data before sending response in case of a latter function trying to get
        // the session content (ie. log hit) and so triggering session start which will fail.
        $this->app::getSessionHelper()?->preserveData();

        if ($this->app::getSessionHelper()?->isInvalidated()) {
            if (!$session->isStarted()) {
                $session->start();
            }

            $session->invalidate();
        }

        // Destroy session if :
        //   - Request is API AND :
        //       - No auth method is provided (public routes) that has not been called from the application
        //       - OR the session does not have the session_auth attribute (API call not using legacy session)
        if (
            $event->isMainRequest()
            && !$this->isRequestWebTestCase($request)
            && $this->isRequestApi($request)
            && (
                ($this->app_ui->getAuthMethod() === null)
                || $request->attributes->get(AuthenticationListener::STATELESS_REQUEST_ATTRIBUTE)
            )
        ) {
            if (!$session->isStarted()) {
                $session->start();
            }

            $session->invalidate();
        } elseif ($request->attributes->get(AuthenticationListener::STATELESS_REQUEST_ATTRIBUTE)) {
            if (!$session->isStarted()) {
                $session->start();
            }

            $session->invalidate();
        }
    }

    private function supports(KernelEvent $event): bool
    {
        if (!$event->isMainRequest()) {
            return false;
        }

        return true;
    }

    /**
     * Must close the session from SF before to write SF keys.
     *
     * @param KernelEvent $event
     *
     * @return void
     */
    public function closeSession(KernelEvent $event): void
    {
        $request = $event->getRequest();

        // We do NOT close the session if target is a legacy dosql script or the generic controller.
        if (
            $this->supports($event)
            && $this->security->isGranted('IS_AUTHENTICATED_FULLY')
            && !(
            ($this->isRequestLegacy($request))
                //                && (
                //                    $request->request->has('dosql')
                //                    || $request->request->has('@class')
                //                )
            )
        ) {
            $request->getSession()->save();
        }
    }
}
