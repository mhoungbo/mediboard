<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Listeners;

use Exception;
use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\Exception\AuthenticationFailedException;
use Ox\Core\Auth\Exception\ExternalAuthenticationException;
use Ox\Core\Auth\Exception\SilentAuthenticationException;
use Ox\Core\Auth\SecurityHelperTrait;
use Ox\Core\Auth\User;
use Ox\Core\CAppUI;
use Ox\Core\CMbDT;
use Ox\Core\Kernel\Event\FrameworkInitializedEvent;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\System\CUserAuthentication;
use Ox\Mediboard\System\Factories\UserAuthenticationFactory;
use Ox\Mediboard\System\Pref;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\SwitchUserToken;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Event\LoginFailureEvent;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;

/**
 * Subscriber allowing us to log successful and failed connections
 * and to process the last loadings required for complete authentication.
 *
 * Todo: Ref authentication checks with CheckPassportEvent listener,
 *   see https://symfony.com/doc/5.4/security.html#authentication-events
 *
 * Todo: Use the Security Events according to the firewall
 *   see https://symfony.com/doc/5.4/security.html#security-events
 */
class AuthenticationListener implements EventSubscriberInterface
{
    use RequestHelperTrait;
    use SecurityHelperTrait;

    private const AUTHENTICATION_REQUEST_ATTRIBUTE = 'authentication-request';
    public const  STATELESS_REQUEST_ATTRIBUTE      = 'stateless-request';

    private UserAuthenticationFactory $auth_factory;
    private UrlGeneratorInterface     $router;
    private LoggerInterface           $logger;

    public function __construct(
        UserAuthenticationFactory $auth_factory,
        Security                  $security,
        UrlGeneratorInterface     $router,
        LoggerInterface           $logger
    ) {
        $this->auth_factory = $auth_factory;
        $this->security     = $security;
        $this->router       = $router;
        $this->logger       = $logger;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            // After SessionStrategyListener (which regenerates the session ID)
            LoginSuccessEvent::class        => ['onSuccessfulAuth', -10],
            LoginFailureEvent::class        => ['onFailedAuth', -10],
            SwitchUserEvent::class          => ['onSwitchUser', 100],
            // Before SessionListener::onLogout
            LogoutEvent::class              => ['onLogout', -5],
            FrameworkInitializedEvent::NAME => ['onFrameworkInitialized', 100],
        ];
    }

    /**
     * Todo: Should move some authenticator/provider specific logic into other listener (PscSessionListener, etc.)
     *
     * @param LoginSuccessEvent $event
     *
     * @return void
     * @throws Exception|InvalidArgumentException
     */
    public function onSuccessfulAuth(LoginSuccessEvent $event): void
    {
        /** @var Passport $passport */
        $passport = $event->getPassport();

        /** @var AuthMetadataBadge $metadata */
        $metadata = $passport->getBadge(AuthMetadataBadge::class);

        if ($metadata === null) {
            // Legacy session mode
            return;
        }

        /** @var User $user */
        $user    = $event->getUser();
        $ox_user = $user->getOxUser();

        $auth = $this->auth_factory->createSuccess($ox_user, $metadata, $event->getRequest()->request->get('uainfo'));
        $auth?->store();

        if ($metadata->isResetLoginAttempts()) {
            $ox_user->resetLoginErrorsCounter(true);
        }

        $request = $event->getRequest();

        $request->attributes->set(self::AUTHENTICATION_REQUEST_ATTRIBUTE, true);
        $request->attributes->set(self::STATELESS_REQUEST_ATTRIBUTE, !$metadata->isStateful());

        if ($tokens = $metadata->getOIDCTokens()) {
            $request->getSession()->set('oidc_tokens', $tokens);

            // Storing PSC sub
            // Todo: Do the same for FC
            if ($tokens->isPSC() && ($info = $metadata->getOIDCUserInfo()) && !$ox_user->_ref_mediuser->sub_psc) {
                $med                 = $ox_user->_ref_mediuser;
                $med->_user_password = null; # /!\ DO NOT REMOVE THIS
                $med->sub_psc        = $info->getSub();
                $med->store();
            }
        }

        $this->prepareLegacySession($request->getSession(), $ox_user, $metadata, $auth);
    }

    /**
     * Todo: Message handling is too complex
     *
     * @param LoginFailureEvent $event
     *
     * @return void
     * @throws AuthenticationFailedException
     * @throws Exception
     */
    public function onFailedAuth(LoginFailureEvent $event): void
    {
        // If a response is set by an Authenticator, we simply redirect without further treatment.
        if ($event->getResponse() !== null) {
            return;
        }

        $this->setAuthMessageFromException($event);

        $auth = $event->getAuthenticator();

        /** @var Passport $passport */
        $passport = $event->getPassport();

        // No Passport, the Authenticator probably threw an Exception itself.
        if ($passport === null) {
            // Will be redirected by the entrypoint
            if (($auth instanceof InteractiveAuthenticatorInterface) && $auth->isInteractive()) {
                return;
            }

            // Typically Legacy Session mode with API firewall
            throw AuthenticationFailedException::invalidCredentials();
        }

        /** @var AuthMetadataBadge $metadata */
        $metadata = $passport->getBadge(AuthMetadataBadge::class);

        if ($metadata === null) {
            // No known case
            throw AuthenticationFailedException::invalidCredentials();
        }

        try {
            /** @var User $user */
            $user       = $passport->getUser();
            $identifier = $user->getUserIdentifier();

            $ox_user = $user->getOxUser();

            // Do not increment attempts if exception from a UserCheckerInterface
            if (!($event->getException() instanceof AccountStatusException) && $metadata->isIncrementAttempts()) {
                $ox_user->tryIncrementLoginAttempts();
            }

            $user_id = $ox_user->_id;
        } catch (UserNotFoundException $e) {
            $identifier = $e->getUserIdentifier();
            $user_id    = null;
        }

        $exception = $event->getException();

        $real_message = $exception->getMessageKey();
        if ($exception instanceof BadCredentialsException) {
            if ($previous = $exception->getPrevious()) {
                $real_message = $previous->getMessage();
            } else {
                $real_message = AuthenticationFailedException::invalidCredentials()->getMessage();
            }
        }

        $error = $this->auth_factory->createError(
            $identifier,
            $metadata->getAuth(),
            $user_id,
            $real_message
        );

        $error?->store();

        if (!$metadata->isStateful()) {
            $event->getRequest()->attributes->set(self::STATELESS_REQUEST_ATTRIBUTE, true);
        }

        // No response set by the event and authenticator is not interactive so no redirection towards login page.
        if (
            ($event->getResponse() === null)
            && !(
                ($auth instanceof InteractiveAuthenticatorInterface)
                && $auth->isInteractive()
            )
        ) {
            throw AuthenticationFailedException::invalidCredentials();
        }
    }

    /**
     * @param LoginFailureEvent $event
     *
     * @return void
     */
    private function setAuthMessageFromException(LoginFailureEvent $event): void
    {
        $exception = $event->getException();

        if (!($exception instanceof SilentAuthenticationException)) {
            $flash_key = ($exception instanceof ExternalAuthenticationException) ? 'auth-external' : 'auth-internal';

            switch (true) {
                case $exception instanceof InvalidCsrfTokenException:
                    $this->logger->alert($exception->getMessageKey());
                    $msg = AuthenticationFailedException::invalidCredentials()->getMessage();
                    break;

                case $exception instanceof BadCredentialsException:
                    $msg = AuthenticationFailedException::invalidCredentials()->getMessage();
                    break;

                default:
                    $msg = $exception->getMessageKey();
            }

            $session = $event->getRequest()->getSession();
            if ($session instanceof Session) {
                $session->getFlashBag()->add($flash_key, $msg);
            }
        }
    }

    /**
     * @param SwitchUserEvent $event
     *
     * @return void
     * @throws Exception|InvalidArgumentException
     */
    public function onSwitchUser(SwitchUserEvent $event): void
    {
        /** @var User $user */
        $user    = $event->getTargetUser();
        $ox_user = $user->getOxUser();

        // Session should be already initialized
        $auth = $this->auth_factory->createSwitchLog($ox_user);
        $auth?->store();

        $token   = $this->security->getToken();
        $session = $event->getRequest()->getSession();

        if ($token instanceof SwitchUserToken) {
            $og = $token->getOriginalToken()->getUser();

            // Removing the trace that the user impersonated
            if ($og->getUserIdentifier() === $user->getUserIdentifier()) {
                $session->remove('_impersonator');
            } elseif ($og instanceof User) {
                $og_user = $og->getOxUser();
                // Not the first impersonation, but for the A -> B -> C scheme.
                $session->set(
                    '_impersonator',
                    ['firstname' => $og_user->user_first_name, 'lastname' => $og_user->user_last_name]
                );
            }
        } else {
            // This is the first impersonation
            $og = $this->getCUser();

            $session->set('_impersonator', ['firstname' => $og->user_first_name, 'lastname' => $og->user_last_name]);
        }

        $metadata = new AuthMetadataBadge($auth->auth_method);
        $metadata->setStateful(true);
        $metadata->setWeakPassword(false);

        // Meh..
        CAppUI::$instance->_is_ldap_linked = null;

        $this->prepareLegacySession($session, $ox_user, $metadata, $auth);
    }

    public function onLogout(LogoutEvent $event): void
    {
        $last_auth = CAppUI::$instance->_ref_last_auth;

        if ($last_auth && $last_auth->_id) {
            $dtnow = CMbDT::dateTime();

            $last_auth->expiration_datetime = $dtnow;
            $last_auth->last_session_update = $dtnow;
            $last_auth->nb_update++;

            $last_auth->store();
        }
    }

    /**
     * @param FrameworkInitializedEvent $event
     *
     * @return void
     * @throws Exception
     */
    public function onFrameworkInitialized(FrameworkInitializedEvent $event): void
    {
        if ($this->isRequestAuthenticated($event->getRequest()) && !$this->security->isGranted('IS_IMPERSONATOR')) {
            $disconnect_pref = (bool)(new Pref())->get('admin_unique_session');
            $auth            = $this->getCUser()->loadRefLastAuth();

            if (($auth !== null) && $disconnect_pref) {
                $auth->disconnectSimilarOnes();
            }
        }
    }

    /**
     * @param CUser                    $ox_user
     * @param AuthMetadataBadge        $metadata_badge
     * @param CUserAuthentication|null $auth
     *
     * @return void
     * @throws Exception
     */
    private function prepareLegacySession(
        SessionInterface     $session,
        CUser                $ox_user,
        AuthMetadataBadge    $metadata_badge,
        ?CUserAuthentication $auth
    ): void {
        // Set the CAppUI::$instance->_user_group
        $appui = CAppUI::create($ox_user, $metadata_badge->getAuth(), $auth);

        $session->set('AppUi', $appui);

        // Todo: Care, it seems that if we do not log authentication, preferences would not been built.
        CAppUI::buildPrefs();
    }

    private function isRequestAuthenticated(Request $request): bool
    {
        return (bool)$request->attributes->get(self::AUTHENTICATION_REQUEST_ATTRIBUTE, false);
    }
}
