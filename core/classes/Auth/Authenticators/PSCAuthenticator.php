<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Exception;
use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\Exception\ExternalAuthenticationException;
use Ox\Core\OAuth2\OIDC\PSC\Client;
use Ox\Mediboard\System\CUserAuthentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Throwable;

/**
 * Authentication with Pro Sante Connect.
 */
class PSCAuthenticator extends AbstractAuthenticator implements InteractiveAuthenticatorInterface
{
    private Client                $client;
    private UserProviderInterface $user_provider;

    /**
     * @param Client                $client
     * @param UserProviderInterface $user_provider
     * @param Security              $security
     */
    public function __construct(
        Client                $client,
        UserProviderInterface $user_provider,
        Security              $security
    ) {
        parent::__construct($security);

        $this->client        = $client;
        $this->user_provider = $user_provider;
    }

    /**
     * @inheritDoc
     */
    public function isInteractive(): bool
    {
        return true;
    }

    public function supports(Request $request): ?bool
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return false;
        }

        return !$this->isRequestPublic($request)
            && $request->query->has('code')
            && $request->query->has('state');
    }

    /**
     * @inheritDoc
     */
    public function authenticate(Request $request): Passport
    {
        try {
            $this->client->requestTokens($request->query->get('code'), $request->query->get('state'));
            $info = $this->client->getUserInfo();

            if (($info->getSub() === null) || ($info->getId() === null)) {
                throw new Exception('No sub or rpps');
            }
        } catch (Throwable $t) {
            throw new ExternalAuthenticationException('An error occurred with PSC service.');
        }

        $metadata = new AuthMetadataBadge(CUserAuthentication::AUTH_METHOD_SSO);
        $metadata->setStateful(true);
        $metadata->setOIDCTokens($this->client->getTokenSet());
        $metadata->setOIDCUserInfo($info);

        // Concatenating Sub and Id (RPPS) with # because we use a ChainUserProvider which will explode $userIdentifier.
        return new SelfValidatingPassport(
            new UserBadge($info->getSub() . '#' . $info->getId(), [$this->user_provider, 'loadUserByIdentifier']),
            [$metadata]
        );
    }
}
