<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Ox\Core\Auth\Badges\CsrfBadgeBuilder;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Ox\Mediboard\System\CUserAuthentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;

class LoginAuthenticator extends AbstractLoginAuthenticator implements InteractiveAuthenticatorInterface
{
    private CsrfBadgeBuilder $csrf_badge_builder;

    public function __construct(
        UserProviderInterface       $user_provider,
        CredentialsCheckerInterface $credentials_checker,
        Security                    $security,
        CsrfBadgeBuilder            $badge_builder
    ) {
        parent::__construct($user_provider, $credentials_checker, $security);

        $this->csrf_badge_builder = $badge_builder;
    }

    /**
     * @inheritDoc
     */
    public function isInteractive(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request): ?bool
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return false;
        }

        return (
            !$this->isRequestPublic($request)
            && $request->request->has('username')
            && $request->request->has('password')
        );
    }

    protected function extractCredentials(Request $request): array
    {
        $username = $request->request->get('username');
        $password = $request->request->get('password');

        if (($username === null) || ($password === null)) {
            throw new AuthenticationException('Missing parameter');
        }

        return [$username, $password];
    }

    protected function getAuthMethod(): string
    {
        return CUserAuthentication::AUTH_METHOD_STANDARD;
    }

    protected function isStateless(): bool
    {
        return false;
    }

    protected function getAdditionalBadges(Request $request): array
    {
        $badge = $this->csrf_badge_builder->getBadge('login', $request->request->get('token'));

        if ($badge !== null) {
            return [$badge];
        }

        return [];
    }
}
