<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;

/**
 * Authentication with Basic headers.
 */
class InteractiveBasicHeaderAuthenticator extends BasicHeaderAuthenticator implements InteractiveAuthenticatorInterface
{
    public function isInteractive(): bool
    {
        return true;
    }
}
