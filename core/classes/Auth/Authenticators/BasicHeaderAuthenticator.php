<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Ox\Mediboard\System\CUserAuthentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Authentication with Basic headers.
 */
class BasicHeaderAuthenticator extends AbstractLoginAuthenticator
{
    private const BASIC_REGEXP = '/^Basic\s(?:[A-Za-z\d+\/]{4})*(?:[A-Za-z\d+\/]{2}==|[A-Za-z\d+\/]{3}=)?$/';
    private const LOGIN_REGEXP = '/^(?<username>[^:]+):(?<password>.+)$/';

    /** @var Request */
    private $request;

    /**
     * @inheritDoc
     */
    public function supports(Request $request): ?bool
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return false;
        }

        $basic = $request->headers->get('Authorization');

        $match = [];

        $supports = !$this->isRequestPublic($request)
            && $basic
            && (preg_match(self::BASIC_REGEXP, $basic, $match) === 1);

        if ($supports) {
            $this->setRequest($request);
        }

        return $supports;
    }

    protected function extractCredentials(Request $request): array
    {
        $basic       = $request->headers->get('Authorization');
        $b64         = explode(' ', $basic)[1];
        $credentials = base64_decode($b64);

        $match = [];
        if (preg_match(self::LOGIN_REGEXP, $credentials, $match)) {
            $username = $match['username'];
            $password = $match['password'];
        } else {
            throw new AuthenticationException('Invalid header format');
        }

        return [$username, $password];
    }

    protected function getAuthMethod(): string
    {
        return CUserAuthentication::AUTH_METHOD_BASIC;
    }

    protected function isStateless(): bool
    {
        return $this->isRequestApi($this->request);
    }

    private function setRequest(Request $request): void
    {
        $this->request = $request;
    }
}
