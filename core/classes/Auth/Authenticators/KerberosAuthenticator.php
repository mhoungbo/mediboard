<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Config\Conf;
use Ox\Mediboard\System\CUserAuthentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

/**
 * Authentication with KRB.
 */
class KerberosAuthenticator extends AbstractAuthenticator implements InteractiveAuthenticatorInterface
{
    private UserProviderInterface $user_provider;
    private Conf                  $conf;

    /**
     * @param UserProviderInterface $user_provider
     * @param Security              $security
     * @param Conf                  $conf
     */
    public function __construct(
        UserProviderInterface $user_provider,
        Security              $security,
        Conf                  $conf
    ) {
        parent::__construct($security);

        $this->user_provider = $user_provider;
        $this->conf          = $conf;
    }

    /**
     * @inheritDoc
     */
    public function isInteractive(): bool
    {
        return true;
    }

    public function supports(Request $request): ?bool
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return false;
        }

        $krb_auth_type = $request->server->get('AUTH_TYPE');

        return !$this->isRequestPublic($request)
            && $this->conf->get('admin CKerberosLdapIdentifier enable_kerberos_authentication')
            && $request->server->has('REMOTE_USER')
            && ($krb_auth_type === 'Negotiate');
    }

    /**
     * @inheritDoc
     */
    public function authenticate(Request $request): Passport
    {
        $identifier = $request->server->get('REMOTE_USER');

        $metadata = new AuthMetadataBadge(CUserAuthentication::AUTH_METHOD_SSO);
        $metadata->setStateful(true);

        return new SelfValidatingPassport(
            new UserBadge($identifier, [$this->user_provider, 'loadUserByIdentifier']),
            [$metadata]
        );
    }
}
