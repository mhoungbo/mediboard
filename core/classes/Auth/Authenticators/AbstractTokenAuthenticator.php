<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Exception;
use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\Providers\UserProvider;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\Admin\Repositories\AccessTokenRepository;
use Ox\Mediboard\System\CUserAuthentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

/**
 * Authentication with an AccessToken.
 * Extracting the token from the request and using it.
 */
abstract class AbstractTokenAuthenticator extends AbstractAuthenticator
{
    protected AccessTokenRepository $repository;
    protected UserProviderInterface $user_provider;

    /**
     * @param AccessTokenRepository $repository
     * @param UserProviderInterface $user_provider
     * @param Security              $security
     */
    public function __construct(
        AccessTokenRepository $repository,
        UserProviderInterface $user_provider,
        Security              $security
    ) {
        parent::__construct($security);

        $this->repository    = $repository;
        $this->user_provider = $user_provider;
    }

    abstract protected function extractToken(Request $request): ?string;

    abstract protected function checkTokenContextValidity(?CViewAccessToken $token): void;

    abstract protected function beforeUsingToken(CViewAccessToken $token, Request $request): void;

    abstract protected function afterUsingToken(CViewAccessToken $token, Request $request): void;

    protected function getPassport(CViewAccessToken $token, Request $request): Passport
    {
        $metadata = new AuthMetadataBadge(CUserAuthentication::AUTH_METHOD_TOKEN);

        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            // User is currently already authenticated, we apply the token without switching the current user
            $user_badge = new UserBadge(
                $this->security->getUser()->getUserIdentifier(),
                [new UserProvider(), 'loadUserByIdentifier']
            );

            $metadata->setStateful(true);
        } else {
            // User is currently not authenticated: we load it from the token
            $user_badge = new UserBadge($token->hash, [$this->user_provider, 'loadUserByIdentifier']);

            $metadata->setStateful(!$token->isRestricted());
        }

        return new SelfValidatingPassport($user_badge, [$metadata]);
    }

    /**
     * @inheritDoc
     */
    public function authenticate(Request $request): Passport
    {
        $token = $this->extractToken($request);

        if ((null === $token) || ('' === $token)) {
            // The token header was empty, authentication fails with HTTP Status
            throw new CustomUserMessageAuthenticationException('No token provided');
        }

        // Use the token here because of TokenProvider may get called several times.
        $token = $this->repository->findByHash($token);

        $this->checkTokenContextValidity($token);

        $this->beforeUsingToken($token, $request);

        try {
            $token->useIt();
        } catch (Exception) {
            throw new CustomUserMessageAuthenticationException('An error occurred');
        }

        $this->afterUsingToken($token, $request);

        return $this->getPassport($token, $request);
    }
}
