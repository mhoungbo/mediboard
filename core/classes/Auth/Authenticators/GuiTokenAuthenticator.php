<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\Admin\Repositories\AccessTokenRepository;
use Ox\Mediboard\Developpement\Index\RouteIndexer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;

/**
 * This authenticator is declared as "interactive" because we redirect the user to the login page in case of error.
 */
class GuiTokenAuthenticator extends AbstractTokenAuthenticator implements InteractiveAuthenticatorInterface
{
    private RouteIndexer $index;

    /**
     * @param AccessTokenRepository $repository
     * @param UserProviderInterface $user_provider
     * @param Security              $security
     * @param RouteIndexer          $index
     */
    public function __construct(
        AccessTokenRepository $repository,
        UserProviderInterface $user_provider,
        Security              $security,
        RouteIndexer          $index
    ) {
        parent::__construct($repository, $user_provider, $security);

        $this->index = $index;
    }

    public function supports(Request $request): ?bool
    {
        return !$this->isRequestPublic($request);
    }

    public function isInteractive(): bool
    {
        return true;
    }

    protected function extractToken(Request $request): ?string
    {
        return $request->attributes->get('token');
    }

    protected function checkTokenContextValidity(?CViewAccessToken $token): void
    {
        if (($token === null) || !$token->isValidForGui()) {
            throw new CustomUserMessageAuthenticationException('Invalid token provided');
        }
    }

    protected function beforeUsingToken(CViewAccessToken $token, Request $request): void
    {
        $route_metadata = $this->index->findByName(reset($token->_routes_names));

        if ($route_metadata === null) {
            throw new CustomUserMessageAuthenticationException('Route does not exist anymore');
        } else {
            $request->attributes->set(
                'route_controller',
                "{$route_metadata->getController()}::{$route_metadata->getAction()}"
            );
            $request->attributes->set('route_permission', $route_metadata->getPermission());
        }
    }

    protected function afterUsingToken(CViewAccessToken $token, Request $request): void
    {
        $token->prepareAppForToken();
    }
}
