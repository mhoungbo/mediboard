<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

abstract class AbstractLoginAuthenticator extends AbstractAuthenticator
{
    private UserProviderInterface       $user_provider;
    private CredentialsCheckerInterface $credentials_checker;

    /**
     * @param UserProviderInterface       $user_provider
     * @param CredentialsCheckerInterface $credentials_checker
     * @param Security                    $security
     */
    public function __construct(
        UserProviderInterface       $user_provider,
        CredentialsCheckerInterface $credentials_checker,
        Security                    $security
    ) {
        parent::__construct($security);

        $this->user_provider       = $user_provider;
        $this->credentials_checker = $credentials_checker;
    }

    abstract protected function extractCredentials(Request $request): array;

    abstract protected function getAuthMethod(): string;

    abstract protected function isStateless(): bool;

    /**
     * Return additional badges (other than AuthMetadata) if required.
     *
     * @param Request $request
     *
     * @return array
     */
    protected function getAdditionalBadges(Request $request): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function authenticate(Request $request): Passport
    {
        [$username, $password] = $this->extractCredentials($request);

        // The same badge is passed by reference to the credentials checker and to the passport
        // in order to toggle the failure increment, the log method and the weak password.
        $metadata = new AuthMetadataBadge($this->getAuthMethod());
        $metadata->setIncrementAttempts(true);
        $metadata->setResetLoginAttempts(true);
        $metadata->setStateful(!$this->isStateless());
        // Weak password will be set by credentials checker

        $this->credentials_checker->setAuthMetadataBadge($metadata);

        return new Passport(
            new UserBadge($username, [$this->user_provider, 'loadUserByIdentifier']),
            new CustomCredentials([$this->credentials_checker, 'check'], $password),
            array_merge([$metadata], $this->getAdditionalBadges($request))
        );
    }
}
