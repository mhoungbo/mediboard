<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\Checkers\CredentialsCheckerInterface;
use Ox\Mediboard\System\CUserAuthentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

/**
 * Authentication with CPX.
 * Todo: A recetter avec quelqu'un (chgt routes)
 */
class CPXAuthenticator extends AbstractAuthenticator implements InteractiveAuthenticatorInterface
{
    private CredentialsCheckerInterface $credentials_checker;
    private UserProviderInterface       $user_provider;

    /**
     * @param UserProviderInterface       $user_provider
     * @param CredentialsCheckerInterface $credentials_checker
     * @param Security                    $security
     */
    public function __construct(
        UserProviderInterface       $user_provider,
        CredentialsCheckerInterface $credentials_checker,
        Security                    $security
    ) {
        parent::__construct($security);

        $this->user_provider       = $user_provider;
        $this->credentials_checker = $credentials_checker;
    }

    /**
     * @inheritDoc
     */
    public function isInteractive(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request): ?bool
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return false;
        }

        return !$this->isRequestPublic($request)
            && $request->request->has('signature')
            && $request->request->has('certificat_signature');
    }

    /**
     * @inheritDoc
     */
    public function authenticate(Request $request): Passport
    {
        $credentials = [
            'signature'      => $request->request->get('signature'),
            'cert_signature' => $request->request->get('certificat_signature'),
        ];

        $metadata = new AuthMetadataBadge(CUserAuthentication::AUTH_METHOD_CARD);
        $metadata->setStateful(true);

        return new Passport(
            new UserBadge($credentials['cert_signature'], [$this->user_provider, 'loadUserByIdentifier']),
            new CustomCredentials([$this->credentials_checker, 'check'], $credentials),
            [$metadata]
        );
    }
}
