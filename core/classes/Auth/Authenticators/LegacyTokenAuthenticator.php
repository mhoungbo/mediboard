<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Ox\Core\CMbString;
use Ox\Mediboard\Admin\CViewAccessToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;

/**
 * This authenticator is declared as "interactive" because we redirect the user to the login page in case of error.
 */
class LegacyTokenAuthenticator extends AbstractTokenAuthenticator implements InteractiveAuthenticatorInterface
{
    public function supports(Request $request): ?bool
    {
        return !$this->isRequestPublic($request)
            && (
                $request->query->has('token')
                || ($request->query->count() === 1)
            );
    }

    public function isInteractive(): bool
    {
        return true;
    }

    protected function extractToken(Request $request): ?string
    {
        return $request->query->get('token') ?? $this->extractTokenFromShortUrl($request);
    }

    protected function checkTokenContextValidity(?CViewAccessToken $token): void
    {
        if (($token === null) || !$token->isValidForLegacy()) {
            throw new CustomUserMessageAuthenticationException('Invalid token provided');
        }
    }

    protected function beforeUsingToken(CViewAccessToken $token, Request $request): void
    {
        // Nothing
    }

    protected function afterUsingToken(CViewAccessToken $token, Request $request): void
    {
        // Applying parameters to superglobals because of legacy scripts still using them.
        $token->applyParams();
        $token->applyParamsToRequest($request);
    }

    private function extractTokenFromShortUrl(Request $request): ?string
    {
        $possibly_token_hash = array_key_first($request->query->all());

        if (!CMbString::isBase58($possibly_token_hash) && !CMbString::isBase64($possibly_token_hash)) {
            return null;
        }

        if (mb_strlen($possibly_token_hash) < CViewAccessToken::MINIMUM_HASH_LENGTH) {
            return null;
        }

        return $possibly_token_hash;
    }
}
