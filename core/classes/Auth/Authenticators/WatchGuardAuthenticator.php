<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

class WatchGuardAuthenticator extends AbstractAuthenticator
{
    public function __construct(Security $security)
    {
        parent::__construct($security);
    }

    public function supports(Request $request): ?bool
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return false;
        }

        return !$this->isRequestPublic($request)
            && (
                $request->query->has('login')
                || $request->request->has('login')
            );
    }

    /**
     * @inheritDoc
     */
    public function authenticate(Request $request): Passport
    {
        throw new AuthenticationException('Login parameter is obsolete');
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new Response('', Response::HTTP_NOT_IMPLEMENTED);
    }
}
