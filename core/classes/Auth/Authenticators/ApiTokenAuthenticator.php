<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Authenticators;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Mediboard\Admin\CViewAccessToken;
use Ox\Mediboard\System\CUserAuthentication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

/**
 * Authentication with an AccessToken.
 * Extracting the token from the request and using it.
 */
class ApiTokenAuthenticator extends AbstractTokenAuthenticator
{
    public const TOKEN_HEADER_KEY = 'X-OXAPI-KEY';

    public function supports(Request $request): ?bool
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY')) {
            return false;
        }

        return !$this->isRequestPublic($request) && $request->headers->has(self::TOKEN_HEADER_KEY);
    }

    protected function extractToken(Request $request): ?string
    {
        return $request->headers->get(self::TOKEN_HEADER_KEY);
    }

    protected function checkTokenContextValidity(?CViewAccessToken $token): void
    {
        if (($token === null) || !$token->isValidForApi()) {
            throw new CustomUserMessageAuthenticationException('Invalid token provided');
        }
    }

    protected function beforeUsingToken(CViewAccessToken $token, Request $request): void
    {
        // Nothing
    }

    protected function afterUsingToken(CViewAccessToken $token, Request $request): void
    {
        $token->prepareAppForToken();
    }

    protected function getPassport(CViewAccessToken $token, Request $request): Passport
    {
        $metadata = new AuthMetadataBadge(CUserAuthentication::AUTH_METHOD_TOKEN);
        $metadata->setStateful(false);

        return new SelfValidatingPassport(
            new UserBadge($token->hash, [$this->user_provider, 'loadUserByIdentifier']),
            [$metadata]
        );
    }
}
