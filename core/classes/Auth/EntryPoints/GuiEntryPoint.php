<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\EntryPoints;

use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Mediboard\Admin\Controllers\LoginController;
use Ox\Mediboard\System\Controllers\MainController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;

class GuiEntryPoint implements AuthenticationEntryPointInterface
{
    private UrlGeneratorInterface $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @inheritDoc
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        // Ajax call, show the reconnection modal.
        if ($request->query->get('ajax')) {
            CAppUI::$instance->user_id = null;
            return new Response((new MainController())->ajaxErrors(true), Response::HTTP_UNAUTHORIZED);
        }

        $login_path = $this->router->generate(LoginController::LOGIN_ROUTE_NAME, $request->query->all());

        // Small hack because of various vhosts/base href settings.
        $login_path = str_replace('/index.php', '', $login_path);

        $response = new RedirectResponse($login_path);

        // Setting the target path into the session for further redirect
        if ($request->isMethod(Request::METHOD_GET)) {
            $request->getSession()->set('redirect_uri', $request->server->get('REQUEST_URI'));
        }

        return $response;
    }
}
