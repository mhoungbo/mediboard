<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Badges;

use Ox\Core\OAuth2\OIDC\TokenSet;
use Ox\Core\OAuth2\OIDC\UserInfoInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\BadgeInterface;

class AuthMetadataBadge implements BadgeInterface
{
    private string             $auth_method;
    private bool               $increment_attempts   = false;
    private bool               $reset_login_attempts = false;
    private bool               $stateful             = false;
    private bool               $weak_password        = false;
    private ?TokenSet          $oidc_tokens          = null;
    private ?UserInfoInterface $oidc_user_info       = null;

    public function __construct(string $auth_method)
    {
        $this->auth_method = $auth_method;
    }

    /**
     * @return string
     */
    public function getAuth(): string
    {
        return $this->auth_method;
    }

    /**
     * @param string $auth_method
     */
    public function setAuthMethod(string $auth_method): void
    {
        $this->auth_method = $auth_method;
    }

    /**
     * @return bool
     */
    public function isIncrementAttempts(): bool
    {
        return $this->increment_attempts;
    }

    /**
     * @param bool $increment_attempts
     */
    public function setIncrementAttempts(bool $increment_attempts): void
    {
        $this->increment_attempts = $increment_attempts;
    }

    /**
     * @return bool
     */
    public function isResetLoginAttempts(): bool
    {
        return $this->reset_login_attempts;
    }

    /**
     * @param bool $reset_login_attempts
     */
    public function setResetLoginAttempts(bool $reset_login_attempts): void
    {
        $this->reset_login_attempts = $reset_login_attempts;
    }

    /**
     * @return bool
     */
    public function isStateful(): bool
    {
        return $this->stateful;
    }

    /**
     * @param bool $stateful
     */
    public function setStateful(bool $stateful): void
    {
        $this->stateful = $stateful;
    }

    /**
     * @return bool
     */
    public function isWeakPassword(): bool
    {
        return $this->weak_password;
    }

    /**
     * @param bool $weak_password
     */
    public function setWeakPassword(bool $weak_password): void
    {
        $this->weak_password = $weak_password;
    }

    public function getOIDCTokens(): ?TokenSet
    {
        return $this->oidc_tokens;
    }

    public function getOIDCUserInfo(): ?UserInfoInterface
    {
        return $this->oidc_user_info;
    }

    public function setOIDCTokens(TokenSet $token_set): void
    {
        $this->oidc_tokens = $token_set;
    }

    public function setOIDCUserInfo(UserInfoInterface $info): void
    {
        $this->oidc_user_info = $info;
    }

    public function isResolved(): bool
    {
        return true;
    }
}
