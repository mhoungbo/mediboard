<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Badges;

use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;

class CsrfBadgeBuilder
{
    public function getBadge(string $name, ?string $value): ?CsrfTokenBadge
    {
        return new CsrfTokenBadge($name, $value);
    }
}
