<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Providers;

use Exception;
use Ox\Core\Auth\User;
use Ox\Core\Security\Crypt\Hash;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Files\Repositories\FileRepository;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Throwable;

class CPXSignatureUserProvider extends UserProvider
{
    private FileRepository $repository;
    private Hasher         $hasher;

    /**
     * @param FileRepository $repository
     * @param Hasher         $hasher
     */
    public function __construct(FileRepository $repository, Hasher $hasher)
    {
        $this->repository = $repository;
        $this->hasher     = $hasher;
    }

    /**
     * @param string $identifier Certificate signature
     *
     * @return UserInterface
     * @throws UserNotFoundException
     */
    protected function getUser(string $identifier): UserInterface
    {
        try {
            $files     = $this->repository->findCertificateRelatedFiles();
            $cert_hash = $this->hasher->hash(Hash::MD5, $identifier);

            foreach ($files as $file) {
                if (
                    $this->hasher->equals(
                        $cert_hash,
                        $this->hasher->hash(Hash::MD5, $file->getBinaryContent())
                    )
                ) {
                    return new User(CUser::findOrFail($file->object_id));
                }
            }

            throw new Exception('CUserAuthentication-error-Authentication card mismatch');
        } catch (Throwable $t) {
            $this->throwException(/*$identifier*/ 'cpx'); // Todo: Do not log this ($identifier)
        }
    }
}
