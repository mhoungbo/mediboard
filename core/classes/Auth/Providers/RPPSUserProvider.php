<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Providers;

use Ox\Core\Auth\User;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Symfony\Component\Security\Core\User\UserInterface;

class RPPSUserProvider extends UserProvider
{
    protected function getUser(string $identifier): UserInterface
    {
        /** @var CMediusers $user */
        $user = CMediusers::loadFromRPPS($identifier);

        if (!$user->_id) {
            $this->throwException($identifier);
        }

        // Todo: Find why CMediusers::loadRefUser make SF unserialization (from Session) "incomplete"
        return new User(CUser::findOrFail($user->_id));
    }
}
