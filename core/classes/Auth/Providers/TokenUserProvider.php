<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Providers;

use Exception;
use Ox\Core\Auth\User;
use Ox\Mediboard\Admin\Repositories\AccessTokenRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Throwable;

class TokenUserProvider extends UserProvider
{
    private AccessTokenRepository $repository;

    public function __construct(AccessTokenRepository $repository)
    {
        $this->repository = $repository;
    }

    protected function getUser(string $identifier): UserInterface
    {
        try {
            $token = $this->repository->findByHash($identifier);
            $user  = $token->loadRefUser();
            if (!$user || !$user->_id) {
                throw new Exception();
            }
            return new User($user);
        } catch (Throwable $t) {
            $this->throwException($identifier);
        }
    }
}
