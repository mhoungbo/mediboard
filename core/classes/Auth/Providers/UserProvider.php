<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Providers;

use Ox\Core\Auth\User;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Enable us to get the User during Authentication.
 */
class UserProvider implements UserProviderInterface
{
    /** @var bool Used when username is UTF-8, typically when used with impersonation which send an UTF-8 string via the URL. */
    private bool $utf8_decode = false;

    public function __construct(bool $utf8_decode = false)
    {
        $this->utf8_decode = $utf8_decode;
    }

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        /** @var User $user */
        $this->checkSupport($user);

        // Here (call from ContextListener), $user is temporary "incomplete" (without a CUser) as it is not serialized.

        // Refreshing is done without initially used identifier because we share firewalls
        // and ALL the providers declared in security.yaml are called in chain...
        // https://github.com/symfony/symfony/issues/4498
        return $this->refreshUserByUsername($user->getUserIdentifier());
    }

    /**
     * @inheritDoc
     */
    public function supportsClass(string $class): bool
    {
        return ((User::class === $class) || is_subclass_of($class, User::class));
    }

    private function refreshUserByUsername(string $username): UserInterface
    {
        $ox_user = $this->loadOxUserByUsername($username);

        return new User($ox_user);
    }

    /**
     * @inheritDoc
     */
    public function loadUserByUsername(string $username): UserInterface
    {
        return $this->loadUserByIdentifier($username);
    }

    /**
     * @inheritDoc
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        return $this->getUser($identifier);
    }

    protected function getUser(string $identifier): UserInterface
    {
        $ox_user = $this->loadOxUserByUsername($identifier);

        return new User($ox_user);
    }

    /**
     * @param UserInterface $user
     *
     * @return CUser
     */
    public function loadOxUser(UserInterface $user): CUser
    {
        $this->checkSupport($user);

        return $this->loadOxUserByUsername($user->getUserIdentifier());
    }

    /**
     * @param string $username
     *
     * @return CUser
     * @throws UserNotFoundException
     */
    protected function loadOxUserByUsername(string $username): CUser
    {
        $username = ($this->utf8_decode) ? mb_convert_encoding($username, 'ISO-8859-1', 'UTF-8') : $username;

        $user                = new CUser();
        $user->user_username = $username;
        $user->loadMatchingObjectEsc();

        if (!$username || !$user->_id) {
            $this->throwException($username);
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     *
     * @return void
     */
    protected function checkSupport(UserInterface $user): void
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
    }

    /**
     * @return never
     * @throws UserNotFoundException
     *
     */
    protected function throwException(string $identifier): void
    {
        $identifier = ($this->utf8_decode) ? mb_convert_encoding($identifier, 'ISO-8859-1', 'UTF-8') : $identifier;

        $exception = new UserNotFoundException(sprintf('User %s not found', $identifier));
        $exception->setUserIdentifier($identifier);

        throw $exception;
    }
}
