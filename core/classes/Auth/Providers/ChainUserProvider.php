<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Providers;

use LogicException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

// Todo: Should implement (and test) supportsClass too.
class ChainUserProvider extends UserProvider
{
    /** @var UserProvider[] */
    private array $providers;

    public function __construct(UserProvider ...$providers)
    {
        $this->providers = $providers;
    }

    protected function getUser(string $identifier): UserInterface
    {
        // Exploding identifiers by # (@see PSCAuthenticator)
        $identifiers = explode('#', $identifier);

        $count           = count($identifiers);
        $count_providers = count($this->providers);

        if (($count > 1) && ($count !== $count_providers)) {
            throw new LogicException('Invalid count of identifiers according to given providers');
        }

        // If only one identifier and several providers, we initialize an array filled with the identifier for each one.
        if (($count === 1) && ($count_providers > 1)) {
            $identifiers = array_fill(0, $count_providers, reset($identifiers));
        }

        $i = 0;
        foreach ($this->providers as $provider) {
            try {
                // Using the first identifier with the first provider, and so on...
                return $provider->getUser($identifiers[$i]);
            } catch (UserNotFoundException $e) {
                $i++;
                continue;
            }
        }

        $this->throwException($identifier);
    }
}
