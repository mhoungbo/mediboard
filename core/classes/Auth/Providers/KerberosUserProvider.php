<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Providers;

use Ox\Core\Auth\User;
use Ox\Mediboard\Admin\Repositories\KerberosLdapIdentifierRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class KerberosUserProvider extends UserProvider
{
    private KerberosLdapIdentifierRepository $repository;

    public function __construct(KerberosLdapIdentifierRepository $repository)
    {
        $this->repository = $repository;
    }

    protected function getUser(string $identifier): UserInterface
    {
        $user = $this->repository->findUserByIdentifier($identifier);

        if (($user === null) || !$user->_id) {
            $this->throwException($identifier);
        }

        return new User($user);
    }
}
