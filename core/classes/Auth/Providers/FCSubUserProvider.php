<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Providers;

use Ox\Core\Auth\User;
use Ox\Mediboard\Admin\CUser;
use Symfony\Component\Security\Core\User\UserInterface;

class FCSubUserProvider extends UserProvider
{
    protected function getUser(string $identifier): UserInterface
    {
        $user = CUser::findFromFCSub($identifier);

        if (!$user || !$user->_id) {
            $this->throwException($identifier);
        }

        return new User($user);
    }
}
