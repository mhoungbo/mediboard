<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth;

use LogicException;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

trait SecurityHelperTrait
{
    private ?Security $security = null;

    /**
     * @param Security|null      $security
     * @param UserInterface|null $user
     *
     * @return CUser
     * @throws LogicException
     */
    private function getCurrentCUser(?Security $security = null, ?UserInterface $user = null): CUser
    {
        $security = ($security) ?: $this->security;

        if (($security === null) && ($user === null)) {
            throw new LogicException('Security and UserInterface objects must be available');
        }

        $user = ($user) ?: $security->getUser();

        if (!($user instanceof User)) {
            throw new LogicException('Null or not supported user');
        }

        $cuser = $user->getOxUser();

        if (($cuser === null) || !$cuser->_id) {
            throw new LogicException('Unable to retrieve CUser');
        }

        return $cuser;
    }

    /**
     * @param Security|null      $security
     * @param UserInterface|null $user
     *
     * @return CUser
     * @throws LogicException
     */
    protected function getCUser(?Security $security = null, ?UserInterface $user = null): CUser
    {
        return $this->getCurrentCUser($security, $user);
    }

    /**
     * @param Security|null      $security
     * @param UserInterface|null $user
     *
     * @return CMediusers
     * @throws LogicException
     */
    protected function getCMediuser(Security $security = null, UserInterface $user = null): CMediusers
    {
        $user     = $this->getCurrentCUser($security, $user);
        $mediuser = $user->loadRefMediuser();

        if (($mediuser === null) || !$mediuser->_id) {
            throw new LogicException('Unable to retrieve CMediuser');
        }

        return $mediuser;
    }
}
