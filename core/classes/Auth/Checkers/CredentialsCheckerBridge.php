<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Ox\Core\Auth\User;
use Ox\Core\Config\Conf;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Mediboard\Admin\CLDAP;
use Ox\Mediboard\Admin\CUser;
use Throwable;

/**
 * Legacy bridge for checking user's credentials.
 */
class CredentialsCheckerBridge
{
    /**
     * @param string|null $username
     * @param string|null $password
     *
     * @return bool Returns TRUE if the password matches, FALSE otherwise or if the user does not exist.
     */
    public static function checkPassword(?string $username, ?string $password): bool
    {
        if (!$username || !$password) {
            return false;
        }

        $user                = new CUser();
        $user->user_username = $username;
        $user->loadMatchingObjectEsc();

        if (!$user->_id) {
            return false;
        }

        $checker = new ChainCredentialsChecker(
            new LdapCredentialsChecker(new Conf(), new CLDAP()),
            new StandardCredentialsChecker(new Hasher(), new Conf())
        );

        try {
            return $checker->check($password, new User($user));
        } catch (Throwable $t) {
            return false;
        }
    }
}
