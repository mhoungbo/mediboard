<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\Exception\CredentialsCheckException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Chaining the credentials checkers.
 */
class ChainCredentialsChecker implements CredentialsCheckerInterface
{
    use CredentialsCheckerTrait {
        setAuthMetadataBadge as setBadge;
    }

    /** @var CredentialsCheckerInterface[] */
    private array $checkers = [];

    /**
     * @param CredentialsCheckerInterface ...$checkers
     */
    public function __construct(CredentialsCheckerInterface ...$checkers)
    {
        $this->checkers = $checkers;
    }

    public function setAuthMetadataBadge(AuthMetadataBadge $badge): void
    {
        $this->setBadge($badge);

        if ($this->auth_metadata_badge !== null) {
            foreach ($this->checkers as $checker) {
                $checker->setAuthMetadataBadge($this->auth_metadata_badge);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function check($credentials, UserInterface $user): bool
    {
        foreach ($this->checkers as $checker) {
            try {
                if ($checker->check($credentials, $user)) {
                    return true;
                }
            } catch (CredentialsCheckException $e) {
                return false;
            }
        }

        return false;
    }
}
