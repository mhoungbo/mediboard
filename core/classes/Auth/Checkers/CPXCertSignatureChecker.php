<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Ox\Core\Auth\User;
use Ox\Core\Security\Crypt\Crypto;
use Ox\Core\Security\Crypt\Hash;
use Ox\Mediboard\Files\Repositories\FileRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class CPXCertSignatureChecker implements CredentialsCheckerInterface
{
    use CredentialsCheckerTrait;

    private Crypto         $crypto;
    private FileRepository $repository;

    /**
     * @param Crypto         $crypto
     * @param FileRepository $repository
     */
    public function __construct(Crypto $crypto, FileRepository $repository)
    {
        $this->crypto     = $crypto;
        $this->repository = $repository;
    }

    /**
     * @param string|array  $credentials Signature
     * @param UserInterface $user
     *
     * @return bool
     */
    public function check($credentials, UserInterface $user): bool
    {
        if (!($user instanceof User)) {
            return false;
        }

        if (!is_array($credentials)) {
            return false;
        }

        $signature      = $credentials['signature'] ?? null;
        $cert_signature = $credentials['cert_signature'] ?? null;

        if (($signature === null) || ($cert_signature === null)) {
            return false;
        }

        $ox_user = $user->getOxUser();
        $file    = $this->repository->findCertificateFile($ox_user->_id);

        if ($file === null) {
            return false;
        }

        return $this->crypto->verifyRSASignature(
            Hash::SHA1,
            $cert_signature,
            'mediboard',
            base64_decode($signature)
        );
    }
}
