<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Exception;
use Ox\Core\Auth\User;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Ox\Core\Security\Crypt\Hash;
use Ox\Core\Security\Crypt\Hasher;
use Ox\Core\Security\Exception\CouldNotHash;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\System\CUserAuthentication;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Will check the credentials against the database.
 * Todo: Should remove the auth-related info (method and badges) from here.
 */
class StandardCredentialsChecker implements CredentialsCheckerInterface
{
    use CredentialsCheckerTrait;

    private Hasher $hasher;
    private Conf   $conf;

    /**
     * @param Hasher $hasher
     * @param Conf   $conf
     */
    public function __construct(Hasher $hasher, Conf $conf)
    {
        $this->hasher = $hasher;
        $this->conf   = $conf;
        $this->method = CUserAuthentication::AUTH_METHOD_STANDARD;
    }

    /**
     * @inheritDoc
     * @throws CouldNotHash
     * @throws Exception
     */
    public function check($credentials, UserInterface $user): bool
    {
        $this->setLogMethod();

        if (!($user instanceof User)) {
            return false;
        }

        if (!is_string($credentials)) {
            return false;
        }

        $ox_user = $user->getOxUser();
        $this->checkIfPasswordMustBeUpdated($ox_user, $credentials);

        return $this->hasher->equals(
            $this->hasher->hash(Hash::SHA256, $ox_user->user_salt . $credentials),
            $ox_user->user_password
        );
    }

    /**
     * @param CUser  $ox_user
     * @param string $password
     *
     * @return void
     * @throws Exception
     */
    private function checkIfPasswordMustBeUpdated(CUser $ox_user, string $password): void
    {
        // Only check the weak password spec with standard login if user is not a bot
        if (($this->auth_metadata_badge !== null) && !$ox_user->isRobot()) {
            // If user MUST change or if password too weak
            if ($ox_user->force_change_password || $ox_user->checkPasswordWeakness($password)) {
                $this->auth_metadata_badge->setWeakPassword(true);
            } elseif ($this->conf->get('admin CUser force_changing_password')) {
                // Else, check if password must be rotated
                $lifespan = $this->conf->get('admin CUser password_life_duration');

                if (CMbDT::dateTime("-{$lifespan}") > $ox_user->user_password_last_change) {
                    $this->auth_metadata_badge->setWeakPassword(true);
                }
            }
        }
    }
}
