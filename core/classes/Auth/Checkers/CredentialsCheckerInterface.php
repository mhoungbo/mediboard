<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Ox\Core\Auth\Badges\AuthMetadataBadge;
use Ox\Core\Auth\Exception\CredentialsCheckException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface for check a given user's credentials
 */
interface CredentialsCheckerInterface
{
    /**
     * @param string|array  $credentials Credentials, commonly a password.
     * @param UserInterface $user
     *
     * @return bool
     * @throws CredentialsCheckException
     */
    public function check($credentials, UserInterface $user): bool;

    /**
     * @param AuthMetadataBadge $badge
     *
     * @return void
     */
    public function setAuthMetadataBadge(AuthMetadataBadge $badge): void;
}
