<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Ox\Core\Auth\Badges\AuthMetadataBadge;

/**
 * Helper for some CredentialsCheckerInterface
 */
trait CredentialsCheckerTrait
{
    private ?AuthMetadataBadge $auth_metadata_badge = null;

    /** @var string */
    protected $method;

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setAuthMetadataBadge(AuthMetadataBadge $badge): void
    {
        $this->auth_metadata_badge = $badge;
    }

    private function setLogMethod(): void
    {
        if (($this->auth_metadata_badge === null) || ($this->method === null)) {
            return;
        }

        $this->auth_metadata_badge->setAuthMethod($this->method);
    }
}
