<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Exception;
use Ox\Core\Auth\User;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Checks if given User is allowed to be switched on
 */
class SwitchingUserChecker extends AbstractUserChecker
{
    /**
     * @inheritDoc
     */
    public function checkPreAuth(UserInterface $user)
    {
        $this->checkUserIsSupported($user);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function checkPostAuth(UserInterface $user)
    {
        $this->checkUserIsSupported($user);

        /** @var User $user */
        $ox_user = $user->getOxUser();

        $this->checkUserIsATemplate($ox_user, true);
        $this->checkUserIsSecondary($ox_user, true);
        $this->checkUserIsLocked($ox_user);
        $this->checkUserIsActive($ox_user);
    }
}
