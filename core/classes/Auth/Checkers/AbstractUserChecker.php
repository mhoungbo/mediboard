<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Exception;
use Ox\Core\AppUiBridge;
use Ox\Core\Auth\Exception\CouldNotValidatePostAuthentication;
use Ox\Core\Auth\Exception\CouldNotValidatePreAuthentication;
use Ox\Core\Auth\User;
use Ox\Core\CMbDT;
use Ox\Core\Config\Conf;
use Ox\Core\Module\CModule;
use Ox\Core\Network\IpRangeMatcher;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Throwable;

abstract class AbstractUserChecker implements UserCheckerInterface
{
    private Conf        $conf;
    private AppUiBridge $app_ui;

    public function __construct(Conf $conf, AppUiBridge $app_ui)
    {
        $this->conf   = $conf;
        $this->app_ui = $app_ui;
    }

    protected function checkUserIsSupported(UserInterface $user): void
    {
        if (!($user instanceof User)) {
            throw CouldNotValidatePreAuthentication::userIsNotSupported();
        }
    }

    /**
     * @param CUser $user
     * @param bool  $user_msg Whether to display error message to the final user.
     *
     * @return void
     */
    protected function checkUserIsATemplate(CUser $user, bool $user_msg): void
    {
        // User template case
        if ($user->template) {
            if ($user_msg) {
                throw CouldNotValidatePostAuthentication::userIsATemplate();
            } else {
                throw CouldNotValidatePreAuthentication::userIsATemplate();
            }
        }
    }

    /**
     * @param CUser $user
     * @param bool  $user_msg Whether to display error message to the final user.
     *
     * @return void
     */
    protected function checkUserIsSecondary(CUser $user, bool $user_msg): void
    {
        // User is a secondary user (user duplicate)
        if ($user->isSecondary()) {
            if ($user_msg) {
                throw CouldNotValidatePostAuthentication::userIsSecondary();
            } else {
                throw CouldNotValidatePreAuthentication::userIsSecondary();
            }
        }
    }

    protected function checkUserIsLocked(CUser $user): void
    {
        try {
            if ($user->isLocked()) {
                throw new Exception();
            }
        } catch (Throwable $t) {
            throw CouldNotValidatePostAuthentication::userIsLocked();
        }
    }

    protected function checkUserIsActive(CUser $user): void
    {
        if ($this->isMediusersModuleActive()) {
            $mediuser = $this->loadMediuser($user->user_username);

            if ($mediuser && $mediuser->_id) {
                if (!$mediuser->actif) {
                    throw CouldNotValidatePostAuthentication::userIsDeactivated();
                }

                $today = CMbDT::date();
                $deb   = $mediuser->deb_activite;
                $fin   = $mediuser->fin_activite;

                // Check if the user is in his activity period
                if (($deb && ($deb > $today)) || ($fin && ($fin <= $today))) {
                    throw CouldNotValidatePostAuthentication::userAccountHasExpired();
                }
            }
        }
    }

    protected function checkUserHasRemoteAccess(CUser $user): void
    {
        // CAppUI::isIntranet() hydrates the needed CAppUI::$instance properties
        if (!$this->isIntranet() && !$this->hasUserRemoteAccess($user) && !$user->isTypeAdmin()) {
            throw CouldNotValidatePostAuthentication::userHasNoRemoteAccess();
        }
    }

    protected function checkUserIsAllowedFromLocation(CUser $user): void
    {
        $group_id = $this->getUserGroupId($user);

        if (($group_id !== null) && !$user->isRobot() && !$user->isTypeAdmin()) {
            $whitelist = $this->conf->getForGroupId('system network ip_address_range_whitelist', $group_id);

            if ($whitelist) {
                $whitelist = explode("\n", $whitelist);

                try {
                    $ip_range_checker = new IpRangeMatcher($whitelist);

                    if (!$ip_range_checker->matches($this->getIp())) {
                        throw new Exception();
                    }
                } catch (Throwable $t) {
                    throw CouldNotValidatePostAuthentication::userHasNoAccessFromThisLocation();
                }
            }
        }
    }

    protected function hasUserRemoteAccess(CUser $user): bool
    {
        if ($this->isMediusersModuleActive()) {
            $mediuser = $this->loadMediuser($user->user_username);

            if ($mediuser && $mediuser->_id) {
                // Remote=1 means NO remote is allowed...
                return ($mediuser->remote != 1);
            }
        }

        return true;
    }

    protected function getUserGroupId(CUser $user): ?int
    {
        $group_id = null;
        if ($this->isMediusersModuleActive()) {
            $mediuser = $this->loadMediuser($user->user_username);

            if ($mediuser && $mediuser->_id) {
                $group_id = $mediuser->loadRefFunction()->group_id;
            }
        }

        return $group_id;
    }

    protected function isMediusersModuleActive(): bool
    {
        return (bool)CModule::getActive('mediusers');
    }

    protected function loadMediuser(string $username): ?CMediusers
    {
        if (!$username) {
            return null;
        }

        $sibling                = new CUser();
        $sibling->user_username = $username;
        $sibling->loadMatchingObjectEsc();
        $sibling->loadRefMediuser();

        if ($sibling->_ref_mediuser && $sibling->_ref_mediuser->_id) {
            return $sibling->_ref_mediuser;
        }

        return null;
    }

    protected function isIntranet(): bool
    {
        return $this->app_ui->isIntranet();
    }

    protected function getIp(): string
    {
        return $this->app_ui->getIp();
    }
}
