<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Auth\Checkers;

use Exception;
use Ox\Core\Auth\User;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Checks if given User is allowed to authenticate
 */
class UserChecker extends AbstractUserChecker
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function checkPreAuth(UserInterface $user)
    {
        $this->checkUserIsSupported($user);

        /** @var User $user */
        $ox_user = $user->getOxUser();

        $this->checkUserIsATemplate($ox_user, false);
        $this->checkUserIsSecondary($ox_user, false);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function checkPostAuth(UserInterface $user)
    {
        $this->checkUserIsSupported($user);

        /** @var User $user */
        $ox_user = $user->getOxUser();

        $this->checkUserIsLocked($ox_user);
        $this->checkUserIsActive($ox_user);
        $this->checkUserHasRemoteAccess($ox_user);
        $this->checkUserIsAllowedFromLocation($ox_user);
    }
}
