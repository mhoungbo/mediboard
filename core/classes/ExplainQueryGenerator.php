<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use Ox\Core\Security\Csrf\AntiCsrf;

/**
 * Small utility class used for manipulating SQL queries from long request log.
 * Enable us to perform EXPLAIN of theses queries, or even to replay them in SQL_NO_CACHE mode,
 * with minimal security limitations.
 */
class ExplainQueryGenerator
{
    private array $queries = [];

    public function addQuery(string $query, string $dsn): void
    {
        if (!$this->isQuerySupported($query)) {
            return;
        }

        if (!isset($this->queries[$dsn])) {
            $this->queries[$dsn] = [];
        }

        $this->queries[$dsn][] = $query;
    }

    public function isQuerySupported(string $query): bool
    {
        return (preg_match('/^SELECT/i', $query) === 1);
    }

    private function getNormalizedQueries(): array
    {
        $normalized = [];
        foreach ($this->queries as $dsn => $queries) {
            foreach ($queries as $query) {
                $normalized[] = $this->normalizeQuery($query, $dsn);
            }
        }

        return $normalized;
    }

    public function normalizeQuery(string $query, string $dsn): string
    {
        return base64_encode("{$dsn} {$query}");
    }

    public function generateToken(): ?string
    {
        $queries = $this->getNormalizedQueries();

        if (count($queries) < 1) {
            return null;
        }

        return $this->buildToken($queries);
    }

    public function denormalizeQuery(string $normalized): array
    {
        $dsn_and_query = base64_decode($normalized);

        return explode(' ', $dsn_and_query, 2);
    }

    public function explainQuery(string $query, string $dsn): ?array
    {
        if (!$this->isQuerySupported($query)) {
            return null;
        }

        return $this->getDS($dsn)->loadList(sprintf('EXPLAIN (%s)', rtrim($query, ';')));
    }

    public function replayNormalizedQuery(string $normalized): array
    {
        [$dsn, $query] = $this->denormalizeQuery($normalized);

        if (!$this->isQuerySupported($query)) {
            return [null, null];
        }

        $query = preg_replace('/^SELECT\s(?!SQL_NO_CACHE)/i', 'SELECT SQL_NO_CACHE ', $query);

        $ds = $this->getDS($dsn);

        $start = microtime(true);

        $result = $ds->loadList($query);

        if ($result === false) {
            return [null, null];
        }

        return [
            round(microtime(true) - $start, 2),
            count($result),
        ];
    }

    protected function getDS(string $dsn): CSQLDataSource
    {
        return CSQLDataSource::get($dsn);
    }

    protected function buildToken(array $queries): string
    {
        return AntiCsrf::prepare()
                       ->addParam('query_sample', $queries)
                       ->getToken();
    }
}
