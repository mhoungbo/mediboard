<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Api\Transformers;

class ObjectTransformer extends AbstractTransformer
{
    /**
     * @return array
     */
    public function createDatas(): array
    {
        $object = $this->item->getDatas();

        $attributes = get_object_vars($object);

        // id
        if (property_exists($object, 'id')) {
            $this->id = $object->id;
            unset($attributes['id']);
        } else {
            $this->id = $this->createIdFromData($object);
        }

        // type
        if (property_exists($object, 'api_type')) {
            $this->type = $object->api_type;
            unset($attributes['api_type']);
        } else {
            $this->type = $this->item->getType() ?? 'undefined';
        }

        // Attributes
        foreach ($attributes as $key => $sub_datas) {
            // todo filter
            $this->attributes[$key] = $sub_datas;
        }

        $this->links = $this->item->getLinks();

        if ($meta = $this->item->getMetas()) {
            $this->meta = $meta;
        }

        return $this->render();
    }
}
