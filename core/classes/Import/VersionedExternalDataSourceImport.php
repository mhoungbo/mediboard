<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Import;

use DateTimeImmutable;
use Exception;
use Ox\Core\CAppUI;

/**
 * An external data source import class that adds a versioning component to the process.
 *
 * This class extends the ExternalDataSourceImport class, so the import template can be extended as needed.
 *
 * For this component to work, the external database must contain a table that store the version
 * (or as many versions tables as the number of data types)
 *
 * The intended structure of the versions table is the following :
 *     - version : contain the version name, must be UNIQUE (can be a varchar or a float)
 *     - installation_date : must be a DATETIME
 *
 * The structure of the table can be different, but in that case the following methods must be overridden :
 *     - getInstalledVersion()
 *     - insertVersion()
 *     - updateVersion()
 *
 */
abstract class VersionedExternalDataSourceImport extends ExternalDataSourceImport
{
    /**
     * Returns the name of the table that contains the versions and their installation dates, for the given data type.
     *
     * @param string|null $type
     *
     * @return string
     */
    abstract protected function getVersionsTable(?string $type = null): string;

    /**
     * Returns the name (or number) of the last currently available version of the datasource.
     *
     * @param string|null $type
     *
     * @return string
     */
    abstract public function getLastAvailableVersion(?string $type = null): string;

    /**
     * Import the datasource for the given types, and updates the version of the datasource.
     *
     * If you override this method, do not forget to also update the datasource versions.
     *
     * @param array|null $types
     *
     * @return bool
     * @throws Exception
     */
    public function import(?array $types = []): bool
    {
        $result = parent::import($types);

        $this->setInstalledVersion($types);

        return $result;
    }

    /**
     * Returns the name of the last installed version of the data source.
     * If no version names are present, or if the table is empty, returns null.
     *
     * This method assume that the versions table contains two columns, version and installation_date (which is a DATETIME).
     * You can however override this method to use a different table structure.
     *
     * @param string|null $type
     *
     * @return string|null
     */
    public function getInstalledVersion(?string $type = null): ?string
    {
        $version = null;
        $table = $this->getVersionsTable($type);

        try {
            if ($this->versionsTableExists($type)) {
                $version = $this->ds->loadResult("SELECT `version` FROM `$table` ORDER BY `installation_date` DESC LIMIT 0, 1;");

                /* Set the version to null to avoid type errors */
                if ($version === false) {
                    $version = null;
                }
            }
        } catch (Exception $e) {
            $this->addMessage([$e->getMessage(), CAppUI::UI_MSG_WARNING, $this->ds->error()]);
        }


        return $version;
    }

    /**
     * Checks if the datasource version is equal to the last version available
     *
     * @param string|null $type
     *
     * @return bool
     */
    public function isDataSourceUpToDate(?string $type = null): bool
    {
        return $this->getLastAvailableVersion($type) === $this->getInstalledVersion($type);
    }

    /**
     * Returns an SQL query that check if the data source is up-to-date.
     * Can be used in the setup of a module.
     *
     * @param string|null $type
     *
     * @return string
     */
    public function getDataSourceUpToDateQuery(?string $type = null): string
    {
        if ($this->versionsTableExists($type)) {
            $table   = $this->getVersionsTable($type);
            $version = $this->getLastAvailableVersion($type);

            $query = "SELECT * FROM `$table` WHERE `version` = '$version';";
        } else {
            $query = $this->getVersionTableExistsQuery($type);
        }

        return $query;
    }

    /**
     * Checks if the versions table exists in the database, for the given data type
     *
     * @param string|null $type
     *
     * @return bool
     */
    protected function versionsTableExists(?string $type = null): bool
    {
        try {
            $this->setSource();
            $result = (bool)$this->ds->loadResult($this->getVersionTableExistsQuery($type));
        } catch (Exception $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * Returns an SQL query that checks if the version table exists in the database, for the given data type
     *
     * @param string|null $type
     *
     * @return string
     */
    protected function getVersionTableExistsQuery(?string $type = null): string
    {
        return "SHOW TABLES LIKE '" . $this->getVersionsTable($type) . "'";
    }

    /**
     * Add the last available version name in the versions table.
     *
     * If the last installed version is the last available, will only update the installation_date.
     *
     * @param array|null $types
     *
     * @return void
     */
    final protected function setInstalledVersion(?array $types = []): void
    {
        if (empty($types) && !empty($this->data_files)) {
            $types = array_keys($this->data_files);
        }

        foreach ($this->data_files as $type => $files) {
            if (!in_array($type, $types)) {
                continue;
            }

            $table   = $this->getVersionsTable($type);
            $version = $this->getLastAvailableVersion($type);

            try {
                if ($this->versionsTableExists($type)) {
                    if ($this->isDataSourceUpToDate($type)) {
                        $this->updateVersion($table, $version);
                    } else {
                        $this->insertVersion($table, $version);
                    }
                }
            } catch (Exception $e) {
                $this->addMessage([$e->getMessage(), CAppUI::UI_MSG_WARNING, $this->ds->error()]);
            }
        }
    }

    /**
     * Update the installation date for the given version.
     *
     * This method assume that the versions table contains two columns, version and installation_date (which is a DATETIME).
     * You can however override this method to use a different table structure.
     * @param string $table
     * @param string $version
     *
     * @return void
     * @throws Exception
     */
    protected function updateVersion(string $table, string $version): void
    {
        $datetime = (new DateTimeImmutable())->format('Y-m-d H:i:s');

        $this->ds->exec(
            "UPDATE `$table` SET `installation_date` = '$datetime' WHERE `version` = '$version';"
        );
    }

    /**
     * Insert the given version into the table
     *
     * This method assume that the versions table contains two columns, version and installation_date (which is a DATETIME).
     * You can however override this method to use a different table structure.
     *
     * @param string $table
     * @param string $version
     *
     * @return void
     * @throws Exception
     */
    protected function insertVersion(string $table, string $version): void
    {
        $datetime = (new DateTimeImmutable())->format('Y-m-d H:i:s');

        $this->ds->exec(
            "INSERT INTO `$table` (`version`, `installation_date`) VALUES ('$version', '$datetime');"
        );
    }
}
