<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 */

namespace Ox\Core\Import;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CMbPath;
use Ox\Core\CSQLDataSource;
use ReflectionClass;
use Throwable;

use const DIRECTORY_SEPARATOR;

/**
 * An external data source import template process.
 */
abstract class ExternalDataSourceImport
{
    private const TEMP_DIR = 'tmp';

    /** @var array */
    protected array $data_files = [];

    /** @var string */
    protected string $source_name;

    /** @var string */
    protected string $source_dir;

    /** @var string */
    protected string $data_dir;

    /** @var string */
    protected string $tmp_dir;

    /** @var ?CSQLDataSource */
    protected ?CSQLDataSource $ds;

    /** @var array */
    protected array $messages = [];

    /**
     * @param string|null $name The name of the data source
     * @param string|null $path The path to the directory containing the data source import files, relative to the directory of the class
     * @param array|null  $data The list of files to import for each data type
     */
    public function __construct(?string $name, ?string $path, ?array $data)
    {
        if (!empty($name)) {
            $this->setSourceName($name);
        }
        if (!empty($path)) {
            $this->setDataDirectory($path);
        }
        if (!empty($data)) {
            foreach ($data as $type => $files) {
                $this->addData($type, $files);
            }
        }
    }

    /**
     * Sets the data source name, and the temporary directory used for extracting the archive.
     *
     * @param string $name
     *
     * @return self
     */
    protected function setSourceName(string $name): self
    {
        $this->source_name = $name;
        $this->tmp_dir     = self::TEMP_DIR . DIRECTORY_SEPARATOR . $this->source_name . DIRECTORY_SEPARATOR;

        return $this;
    }

    /**
     * @return string
     */
    public function getSourceName(): string
    {
        return $this->source_name;
    }

    /**
     * Returns the source name in a SQL friendly format.
     *
     * @return string
     */
    public function getSourceNameForSQL(): string
    {
        return strtolower(str_replace('-', '_', $this->getSourceName()));
    }

    /**
     * @return CSQLDataSource|null
     */
    public function getSource(): ?CSQLDatasource
    {
        return $this->ds;
    }

    /**
     * Sets the CSQLDataSource object if not already set, and returns it.
     *
     * @return CSQLDataSource
     * @throws Exception
     */
    public function setSource(): CSQLDataSource
    {
        if (!isset($this->ds)) {
            $this->ds = CSQLDataSource::get($this->source_name, true);
        }

        if (!$this->ds) {
            throw new Exception($this->getLocaleClassName() . '-Error-Unable to retrieve datasource object');
        }

        return $this->ds;
    }

    /**
     * Sets the archive directory path, relative to the directory of the class.
     *
     * @param string $path
     *
     * @return $this
     */
    protected function setDataDirectory(string $path): self
    {
        $this->data_dir   = $path;
        $this->source_dir = $this->getDirectory() . DIRECTORY_SEPARATOR . $this->data_dir . DIRECTORY_SEPARATOR;

        return $this;
    }

    /**
     * Sets the files to import for the given data type
     *
     * @param string $type
     * @param array  $files
     *
     * @return $this
     */
    protected function addData(string $type, array $files): self
    {
        $this->data_files[$type] = $files;

        return $this;
    }

    /**
     * Import the datasource for the given types.
     *
     * This method can be overridden if you need extra steps in the import process
     *
     * @throws Exception
     */
    public function import(?array $types = []): bool {
        try {
            $this->setSource();
        } catch (Exception $e) {
            $this->addMessage([$e->getMessage(), CAppUI::UI_MSG_ERROR, $this->getSourceName()]);

            return false;
        }

        return $this->importDatabase($types);
    }

    /**
     * The main import method.
     * Will only import .sql files stored in an archive
     *
     * Can be overridden to import the data from a different source or file type.
     *
     * @throws Exception
     */
    protected function importDatabase(?array $types = []): bool
    {
        $return = true;

        if (empty($types) && !empty($this->data_files)) {
            $types = array_keys($this->data_files);
        }

        foreach ($this->data_files as $type => $files) {
            if (!in_array($type, $types)) {
                continue;
            }

            $archive_name = reset($files);

            if (!$this->extractData($archive_name)) {
                $return = false;
                continue;
            }

            array_shift($files);

            foreach ($files as $file) {
                $return = $return && $this->importFile($file);
            }
        }



        return $return;
    }

    /**
     * Decompress the archive file with the given name.
     *
     * @param string $archive
     *
     * @return bool
     */
    protected function extractData(string $archive): bool
    {
        try {
            $count = CMbPath::extract($this->source_dir . $archive, $this->tmp_dir);
        } catch (Throwable $e) {
            $this->addMessage([$e->getMessage(), CAppUI::UI_MSG_WARNING, $archive]);

            return false;
        }

        $this->addMessage([$this->getLocaleClassName() . '-Info-Files extracted from', CAppUI::UI_MSG_OK, $count, $archive]);

        return true;
    }

    /**
     * Imports the given SQL file in the database
     *
     * @throws Exception
     */
    protected function importFile(string $file_name): bool
    {
        try {
            if (!$this->ds) {
                return true;
            }

            if (($line_count = $this->ds->queryDump($this->tmp_dir . $file_name)) === null) {
                throw new Exception($this->getLocaleClassName() . '-Error-File, an error occured in query');
            }

            $this->addMessage(
                [$this->getLocaleClassName() . '-Info-File imported, query executed', CAppUI::UI_MSG_OK, $file_name, $line_count]
            );
        } catch (Exception $e) {
            $this->addMessage([$e->getMessage(), CAppUI::UI_MSG_WARNING, $this->ds->error()]);

            return false;
        }

        return true;
    }

    /**
     * Adds the given message in the messages stack
     *
     * @param array $message
     *
     * @return array
     */
    public function addMessage(array $message): array
    {
        return $this->messages[] = $message;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @return bool
     */
    public function hasMessages(): bool
    {
        return !empty($this->messages);
    }

    /**
     * @return string
     */
    public function getTmpDir(): string
    {
        return $this->tmp_dir;
    }

    /**
     * @return string
     */
    public function getDataDir(): string
    {
        return $this->getDirectory() . DIRECTORY_SEPARATOR . $this->data_dir;
    }

    /**
     * Returns the path to this class, depending on the context
     *
     * @return string
     */
    protected function getDirectory(): string
    {
        return dirname((new ReflectionClass(get_class($this)))->getFileName());
    }

    /**
     * Returns the class of the instance
     *
     * @return string
     */
    protected function getClassName(): string
    {
        $rc = new ReflectionClass(get_class($this));

        return $rc->getShortName();
    }

    /**
     * Returns the name of this class, used for translating the messages
     *
     * @return string
     */
    protected function getLocaleClassName(): string
    {
        return (new ReflectionClass(self::class))->getShortName();
    }
}
