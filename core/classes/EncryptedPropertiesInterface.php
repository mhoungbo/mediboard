<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use Ox\Core\FieldSpecs\CPasswordSpec;

/**
 * Indicates that an object have some properties that need to be encrypted.
 */
interface EncryptedPropertiesInterface
{
    /**
     * @param CStoredObject $object
     *
     * @return CPasswordSpec[]
     */
    public static function getEncryptedPropertiesSpecs(CStoredObject $object): array;
}
