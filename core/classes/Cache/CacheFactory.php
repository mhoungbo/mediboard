<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Cache;

use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Components\Cache\LayeredCache;
use Psr\SimpleCache\CacheInterface;

/**
 * Simple cache factory used by SF auto-wiring.
 * Should not be used directly.
 */
class CacheFactory
{
    private function __construct()
    {
    }

    /**
     * @param int  $layer
     * @param bool $compressor
     *
     * @return CacheInterface
     * @throws CouldNotGetCache
     */
    public static function create(int $layer, bool $compressor = false): CacheInterface
    {
        return ($compressor) ? LayeredCache::getCache($layer)->withCompressor() : LayeredCache::getCache($layer);
    }
}
