<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\FieldSpecs;

use Ox\Core\CMbFieldSpec;

class BinarySpec extends CMbFieldSpec
{
    private const DEFAULT_MAX_LENGTH = 255;

    public $length;
    public $maxLength;

    /**
     * @inheritdoc
     */
    function getSpecType()
    {
        return 'binary';
    }

    /**
     * @inheritDoc
     */
    public function filter($value)
    {
        // /!\ NO filtering on a binary value
        return $value;
    }

    /**
     * @inheritdoc
     */
    function getDBSpec()
    {
        if ($this->maxLength) {
            return "VARBINARY ({$this->maxLength})";
        }

        if ($this->length) {
            return "BINARY ($this->length)";
        }

        return 'VARBINARY (' . self::DEFAULT_MAX_LENGTH . ')';
    }

    /**
     * @inheritdoc
     */
    function getOptions()
    {
        return [
                'length'    => 'num',
                'maxLength' => 'num',
            ] + parent::getOptions();
    }

    /**
     * @inheritdoc
     */
    function checkProperty($object)
    {
        $propValue = $object->{$this->fieldName};

        // length
        if ($this->length) {
            if (strlen($propValue) != $this->length) {
                return "N'a pas la bonne longueur '$propValue' (longueur souhait�e : $this->length)";
            }
        }

        // maxLength
        if ($this->maxLength) {
            if (strlen($propValue) > $this->maxLength) {
                return "N'a pas la bonne longueur '$propValue' (longueur maximale souhait�e : $this->maxLength)";
            }
        }

        if (strlen($propValue) > self::DEFAULT_MAX_LENGTH) {
            return "N'a pas la bonne longueur '$propValue' (longueur maximale souhait�e : " . self::DEFAULT_MAX_LENGTH . ")";
        }

        return null;
    }
}
