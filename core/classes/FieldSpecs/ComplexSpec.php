<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\FieldSpecs;

use Closure;
use LogicException;
use Ox\Core\CMbFieldSpec;

/**
 * Spec using callable to display a value.
 *
 * /!\ Cannot be used on plain fields.
 */
class ComplexSpec extends CMbFieldSpec
{
    public ?string $callable = null;

    /**
     * @inheritDoc
     */
    public function __construct($className, $field, $prop = null, $options = [])
    {
        if (!str_starts_with($field, '_')) {
            throw new LogicException(sprintf('Spec for field %s only supports form fields', "{$className}::{$field}"));
        }

        parent::__construct($className, $field, $prop, $options);
    }

    /**
     * @inheritDoc
     */
    public function getSpecType(): string
    {
        return "complex";
    }

    /**
     * @inheritDoc
     */
    public function getOptions(): array
    {
        return [
            'callable' => 'str',
        ];
    }

    /**
     * @inheritDoc
     */
    function getValue($object, $params = [])
    {
        $value = parent::getValue($object, $params);

        if ($this->callable && !$value && is_callable([$object, $this->callable])) {
            // Do not bind any instance to this because of static methods.
            return Closure::fromCallable([$object, $this->callable])();
        }

        return $value;
    }
}
