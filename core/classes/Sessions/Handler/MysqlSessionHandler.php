<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions\Handler;

use Ox\Core\AppUiBridge;
use Ox\Core\CMbServer;
use Ox\Core\CSQLDataSource;
use Ox\Core\Mutex\IMbMutex;
use Ox\Core\Mutex\MutexFactory;
use Ox\Core\Sessions\Tracer;

/**
 * Classic MySQL session handler.
 */
class MysqlSessionHandler extends AbstractSessionHandler
{
    private const DATA_CHANGE_TIME = 5; // in seconds

    private AppUiBridge $app_ui;

    private CSQLDataSource $ds;

    private MutexFactory $mutex_factory;

    private string $lock_name;
    private int    $lock_timeout = 30;

    private int $expire;

    private string $mutex_type;

    /** @var IMbMutex|null */
    private ?IMbMutex $mutex = null;

    /** @var string|null */
    private ?string $data_hash = null;

    public function __construct(
        AppUiBridge    $app_ui,
        Tracer         $tracer,
        string         $mutex_type,
        CSQLDataSource $ds,
        MutexFactory   $mutex_factory
    ) {
        parent::__construct($tracer);

        $this->app_ui        = $app_ui;
        $this->ds            = $ds;
        $this->mutex_factory = $mutex_factory;
        $this->mutex_type    = $mutex_type;
    }

    public function isDistributed(): bool
    {
        return true;
    }

    public function exists(string $id): bool
    {
        $this->validateId($id);

        $query = $this->ds->prepare(
            "SELECT COUNT(*) FROM `session` WHERE `session_id` = ?1 AND `expire` > ?2;",
            $id,
            time()
        );

        return $this->ds->loadResult($query);
    }

    public function open(string $path, string $name): bool
    {
        return true;
    }

    public function read(string $id): string|false
    {
        $this->validateId($id);

        $this->lock_name = "session_{$id}";

        // Init the right mutex type
        $mutex = null;
        switch ($this->mutex_type) {
            case 'files':
                $mutex = $this->mutex_factory->getFileMutex($this->lock_name);
                break;

            case 'system':
                $mutex = $this->mutex_factory->getMutex($this->lock_name);
                break;

            default:
                $query = $this->ds->prepare('SELECT GET_LOCK(%1, %2)', $this->lock_name, $this->lock_timeout);
                $this->ds->query($query);
        }

        if ($mutex) {
            $mutex->acquire($this->lock_timeout);
            $this->mutex = $mutex;
        }

        $this->tracer->stop('acquire');
        $this->tracer->start('read');

        $query  = $this->ds->prepare(
            'SELECT `data`, `expire` FROM `session` WHERE `session_id` = ?1 AND `expire` > ?2;',
            $id,
            time()
        );
        $result = $this->ds->exec($query);

        if ($record = $this->ds->fetchAssoc($result)) {
            $this->expire = $record['expire'];
            $data         = $record['data'];

            $new_data = @gzuncompress($data ?? '');
            if ($new_data) {
                $data = $new_data;
            }

            $this->data_hash = md5($data);

            return $data;
        }

        $this->ds->freeResult($result);

        return '';
    }

    public function write(string $id, string $data): bool
    {
        $this->validateId($id);

        $address = CMbServer::getRemoteAddress();
        $user_id = $this->app_ui->getCurrentUserId();
        $user_ip = $address['remote'] ? inet_pton($address['remote']) : null;
        $expire  = time() + $this->lifetime;

        // If session is to be updated
        if ($this->data_hash) {
            $new_hash = md5($data);

            if ($this->data_hash !== $new_hash) {
                $compressed_data = gzcompress($data);

                // Session is to be "updated" but if a $user_id is set (instead of 0),
                // $session_id does not exist in DB because of session migration occurring during authentication.
                if ($user_id) {
                    $compressed_data = gzcompress($data);

                    $query = 'INSERT INTO `session` (`session_id`, `user_id`, `user_ip`, `expire`, `data`)
                VALUES (?1, ?2, ?3, ?4, ?5) ON DUPLICATE KEY UPDATE `data` = ?6, `expire` = ?7;';

                    $query = $this->ds->prepare(
                        $query,
                        $id,
                        $user_id,
                        $user_ip,
                        $expire,
                        $compressed_data,
                        $compressed_data,
                        $expire
                    );
                } else {
                    $query = 'UPDATE `session` SET `user_id` = ?1, `user_ip` = ?2, `expire` = ?3, `data` = ?4 
                 WHERE `session_id` = ?5;';

                    $query = $this->ds->prepare($query, $user_id, $user_ip, $expire, $compressed_data, $id);
                }
            } else {
                // If session was already written less than X seconds, don't update it once again
                if ($expire - $this->expire < self::DATA_CHANGE_TIME) {
                    return true;
                }

                $query = 'UPDATE `session` SET `user_id` = ?1, `user_ip` = ?2, `expire` = ?3 WHERE `session_id` = ?4;';
                $query = $this->ds->prepare($query, $user_id, $user_ip, $expire, $id);
            }
        } else {
            // No session yet
            $compressed_data = gzcompress($data);

            $query = 'INSERT INTO `session` (`session_id`, `user_id`, `user_ip`, `expire`, `data`)
                VALUES (?1, ?2, ?3, ?4, ?5) ON DUPLICATE KEY UPDATE `data` = ?6, `expire` = ?7;';

            $query = $this->ds->prepare(
                $query,
                $id,
                $user_id,
                $user_ip,
                $expire,
                $compressed_data,
                $compressed_data,
                $expire
            );
        }

        if (!$this->ds->query($query)) {
            return false;
        }

        return true;
    }

    public function close(): bool
    {
        if ($this->mutex) {
            $this->mutex->release();
        } else {
            $query = $this->ds->prepare('SELECT RELEASE_LOCK(%1)', $this->lock_name);

            if (!$this->ds->query($query)) {
                return false;
            }
        }

        return true;
    }

    public function destroy(string $id): bool
    {
        $this->validateId($id);

        $query = $this->ds->prepare('DELETE FROM `session` WHERE `session_id` = ?;', $id);
        $this->ds->exec($query);

        return $this->ds->affectedRows() > 0;
    }

    public function gc(int $max_lifetime): int|false
    {
        $query = $this->ds->prepare('DELETE FROM `session` WHERE `expire` < ?;', time());

        if (!$this->ds->query($query)) {
            return false;
        }

        return true;
    }
}
