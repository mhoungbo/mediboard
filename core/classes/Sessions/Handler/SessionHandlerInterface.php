<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions\Handler;

/**
 * Custom interface of a standard SessionHandlerInterface with some custom features.
 */
interface SessionHandlerInterface extends \SessionHandlerInterface
{
    public function setLifetime(int $lifetime): void;

    public function getLifetime(): int;

    public function isDistributed(): bool;

    public function exists(string $id): bool;
}
