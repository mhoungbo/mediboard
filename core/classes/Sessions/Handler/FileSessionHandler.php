<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions\Handler;

use Ox\Core\Mutex\IMbMutex;
use Ox\Core\Mutex\MutexFactory;
use Ox\Core\Sessions\Tracer;
use RuntimeException;
use Symfony\Component\Finder\Finder;

/**
 * Small fallback for sessions if others handlers are not available.
 *
 * Todo: Could use KeyStore and perform HMAC signature to prevent some malicious session modifications.
 */
class FileSessionHandler extends AbstractSessionHandler
{
    private const PATH        = '/tmp/sessions';
    private const FILE_PREFIX = 'sess_';

    private const LOCK_TIMEOUT = 30;

    private string       $path;
    private Finder       $finder;
    private MutexFactory $mutex_factory;
    private IMbMutex     $mutex;

    public function __construct(Tracer $tracer, string $save_path, Finder $finder, MutexFactory $mutex_factory)
    {
        parent::__construct($tracer);

        $this->path          = rtrim($save_path, '/') . self::PATH;
        $this->finder        = $finder;
        $this->mutex_factory = $mutex_factory;
    }

    public function isDistributed(): bool
    {
        return false;
    }

    public function open(string $path, string $name): bool
    {
        if (!is_dir($this->path)) {
            if (!mkdir($this->path)) {
                throw new RuntimeException('Failed to create session directory: ' . $this->path);
            }
        }

        return true;
    }

    public function read(string $id): string|false
    {
        $this->validateId($id);

        $this->mutex = $this->mutex_factory->getFileMutex("session_{$id}");
        $this->mutex->acquire(self::LOCK_TIMEOUT);

        $file = $this->getFilePath($id);

        $this->tracer->stop('acquire');

        $this->tracer->start('read');

        if (file_exists($file) && ((time() - filemtime($file)) <= $this->lifetime)) {
            return file_get_contents($file);
        }

        return '';
    }

    public function write(string $id, string $data): bool
    {
        $this->validateId($id);

        return (file_put_contents($this->getFilePath($id), $data) !== false);
    }

    public function close(): bool
    {
        $this->mutex->release();

        return true;
    }

    public function destroy(string $id): bool
    {
        $this->validateId($id);

        $file = $this->getFilePath($id);

        if (file_exists($file)) {
            unlink($file);
        }

        return true;
    }

    public function gc(int $max_lifetime): int|false
    {
        $deleted = 0;

        foreach (
            $this->finder
                ->name('/' . preg_quote(self::FILE_PREFIX, '/') . self::ID_REGEXP . '$/')
                ->depth('== 0')
                ->files()
                ->in([$this->path]) as $f
        ) {
            $filepath = $f->getRealPath();

            // Using filemtime should be sufficient for this purpose
            if (((filemtime($filepath) + $max_lifetime) < time())) {
                if (unlink($filepath)) {
                    $deleted++;
                }
            }
        }

        return $deleted;
    }

    public function exists(string $id): bool
    {
        $this->validateId($id);

        return file_exists($this->getFilePath($id));
    }

    private function getFilePath(string $id): string
    {
        return $this->path . '/' . self::FILE_PREFIX . $id;
    }
}
