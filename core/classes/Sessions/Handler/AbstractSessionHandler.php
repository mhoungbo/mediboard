<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions\Handler;

use InvalidArgumentException;
use Ox\Core\Sessions\Tracer;

abstract class AbstractSessionHandler implements SessionHandlerInterface
{
    protected const ID_REGEXP           = '[a-zA-Z0-9,-]+';
    protected const STATELESS_ID_REGEXP = 'stateless[0-9a-f]{14}\.\d{8}';

    protected Tracer $tracer;
    protected int    $lifetime;

    public function __construct(Tracer $tracer)
    {
        $this->tracer = $tracer;

        // Default session lifetime is the backend configuration
        $this->lifetime = (int)ini_get('session.gc_maxlifetime');
    }

    public function setLifetime(int $lifetime): void
    {
        $this->lifetime = $lifetime;
    }

    public function getLifetime(): int
    {
        return $this->lifetime;
    }

    protected function validateId(string $id): void
    {
        if (
            !preg_match('/^' . self::ID_REGEXP . '$/', $id)
            && !preg_match('/^' . self::STATELESS_ID_REGEXP . '$/', $id) // "stateless" custom fake session id.
        ) {
            throw new InvalidArgumentException('Invalid session ID format.' . $id);
        }
    }
}
