<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions\Handler;

use Ox\Core\AppUiBridge;
use Ox\Core\CMbServer;
use Ox\Core\Mutex\IMbMutex;
use Ox\Core\Mutex\MutexFactory;
use Ox\Core\Redis\ClientFactory;
use Ox\Core\Sessions\Tracer;

class RedisSessionHandler extends AbstractSessionHandler
{
    private AppUiBridge   $app_ui;
    private ClientFactory $client_factory;
    private MutexFactory  $mutex_factory;

    private ?IMbMutex     $mutex     = null;
    private ?string       $data_hash = null;
    private static string $prefix;

    public function __construct(
        AppUiBridge   $app_ui,
        Tracer        $tracer,
        string        $namespace,
        ClientFactory $client_factory,
        MutexFactory  $mutex_factory,
    ) {
        parent::__construct($tracer);

        $this->app_ui         = $app_ui;
        $this->client_factory = $client_factory;
        $this->mutex_factory  = $mutex_factory;

        // Same as CApp::getAppIdentifier
        $prefix       = preg_replace('/\W+/', '_', $namespace);
        self::$prefix = "{$prefix}-session-";
    }

    public function isDistributed(): bool
    {
        return true;
    }

    public function exists(string $id): bool
    {
        $this->validateId($id);

        return $this->client_factory->getClient()->has($this->getKey($id));
    }

    private function getKey(string $id): string
    {
        return self::$prefix . $id;
    }

    public function open(string $path, string $name): bool
    {
        return true;
    }

    public function read(string $id): string|false
    {
        $this->validateId($id);

        // Init the right mutex type
        $mutex = $this->mutex_factory->getFileMutex("session_{$id}");
        $mutex->acquire(30);

        $this->mutex = $mutex;

        $this->tracer->stop('acquire');
        $this->tracer->start('read');

        $key = $this->getKey($id);

        $client = $this->client_factory->getClient();

        if (!$client->has($key)) {
            return '';
        }

        $session = $client->get($key);

        if ($session) {
            $session = unserialize($session);
            $data    = $session['data'];

            $this->data_hash = md5($data);

            return $data;
        }

        return '';
    }

    public function write(string $id, string $data): bool
    {
        $this->validateId($id);

        $address = CMbServer::getRemoteAddress();
        $user_id = $this->app_ui->getCurrentUserId();
        $user_ip = $address['remote'] ? inet_pton($address['remote']) : null;

        $new_hash = md5($data);

        $key = $this->getKey($id);

        $client = $this->client_factory->getClient();

        // If session is to be updated
        if ($this->data_hash || ($this->data_hash !== $new_hash)) {
            $session = [
                'user_id' => $user_id,
                'user_ip' => $user_ip,
                'data'    => $data,
            ];

            $client->set($key, serialize($session), $this->lifetime);
        } else {
            $client->expire($key, $this->lifetime);
        }

        return true;
    }

    public function close(): bool
    {
        $this->mutex->release();

        return true;
    }

    public function destroy(string $id): bool
    {
        $this->validateId($id);

        $this->client_factory->getClient()->remove($this->getKey($id));

        return true;
    }

    public function gc(int $max_lifetime): int|false
    {
        // TTL is here for this ...
        return true;
    }
}
