<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions\Handler;

use LogicException;
use Ox\Core\AppUiBridge;
use Ox\Core\Config\Conf;
use Ox\Core\Database\DsFactory;
use Ox\Core\Mutex\MutexFactory;
use Ox\Core\Redis\ClientFactory;
use Ox\Core\Sessions\Tracer;
use Symfony\Component\Finder\Finder;

/**
 * Session handler acting as a proxy towards the real handler.
 *
 * Since we need to have fully started the app before determining which handler to use,
 * and since SF is instantiating the SessionHandlerInterface as soon as the request starts,
 * we cannot use configurations or database in this context, so we provide a proxy instead
 * which will fail if a SessionHandlerInterface method is called (because of null $handler property by default),
 * but will populate the property as soon as the configurations (Conf::get) will be ready.
 *
 * So, this proxy fails when sessions are called too soon for the framework to handle it correctly.
 *
 * Furthermore, it prevents writing to the session if we are in a "no revive" mode.
 */
class ProxySessionHandler implements SessionHandlerInterface
{
    private Conf          $conf;
    private AppUiBridge   $app_ui;
    private Tracer        $tracer;
    private ClientFactory $redis_client_factory;
    private DsFactory     $ds;
    private MutexFactory  $mutex_factory;

    private Finder $finder;

    /** @var SessionHandlerInterface|null The handler created via the proxy */
    private ?SessionHandlerInterface $handler = null;

    public function __construct(
        Conf          $conf,
        AppUiBridge   $app_ui,
        Tracer        $tracer,
        ClientFactory $redis_client_factory,
        DsFactory     $ds,
        MutexFactory  $mutex_factory,
        Finder        $finder,
    ) {
        $this->conf   = $conf;
        $this->app_ui = $app_ui;
        $this->tracer = $tracer;

        $this->redis_client_factory = $redis_client_factory;
        $this->ds                   = $ds;
        $this->mutex_factory        = $mutex_factory;
        $this->finder               = $finder;
    }

    private function getHandler(): SessionHandlerInterface
    {
        if ($this->handler !== null) {
            return $this->handler;
        }

        return $this->handler = match ($this->conf->get('session_handler')) {
            'redis' => new RedisSessionHandler(
                $this->app_ui,
                $this->tracer,
                $this->conf->get('root_dir'),
                $this->redis_client_factory,
                $this->mutex_factory,
            ),
            'mysql' => new MysqlSessionHandler(
                $this->app_ui,
                $this->tracer,
                $this->conf->get('session_handler_mutex_type'),
                $this->ds->getDs(),
                $this->mutex_factory,
            ),
            'files' => new FileSessionHandler(
                $this->tracer,
                $this->conf->get('root_dir'),
                $this->finder,
                $this->mutex_factory,
            ),
            default => throw new LogicException('Session is not ready'),
        };
    }

    public function open(string $path, string $name): bool
    {
        return $this->getHandler()->open($path, $name);
    }

    public function read(string $id): string|false
    {
        return $this->getHandler()->read($id);
    }

    public function write(string $id, string $data): bool
    {
        if (!$this->app_ui->canReviveSession()) {
            return true; // SF profiler does not seem to like that the session handler returns false.
        }

        return $this->getHandler()->write($id, $data);
    }

    public function close(): bool
    {
        return $this->getHandler()->close();
    }

    public function destroy(string $id): bool
    {
        return $this->getHandler()->destroy($id);
    }

    public function gc(int $max_lifetime): int|false
    {
        return $this->getHandler()->gc($max_lifetime);
    }

    public function setLifetime(int $lifetime): void
    {
        $this->getHandler()->setLifetime($lifetime);
    }

    public function getLifetime(): int
    {
        return $this->getHandler()->getLifetime();
    }

    public function isDistributed(): bool
    {
        return $this->getHandler()->isDistributed();
    }

    public function exists(string $id): bool
    {
        return $this->getHandler()->exists($id);
    }
}
