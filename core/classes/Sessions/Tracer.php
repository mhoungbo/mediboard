<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions;

/**
 * Small utility able to trace duration between two events.
 * Mainly used to measure session lock acquisition duration and session read duration.
 */
class Tracer
{
    private static array $traces = [];
    private static array $index  = [];

    public function start(string $label): void
    {
        self::$traces[] = [
            $label => [
                'start' => microtime(true),
                'stop'  => null,
            ],
        ];

        // Indexing started label
        if ($this->getIndex($label) === null) {
            // Creating the stack
            self::$index[$label] = [
                array_key_last(self::$traces),
            ];
        } else {
            // Adding the index to the stack
            self::$index[$label][] = array_key_last(self::$traces);
        }
    }

    public function stop(string $label): void
    {
        $index = $this->getIndex($label);

        // Todo: Error if not started?
        if ($index === null) {
            return;
        }

        // Getting the index of the last-started label and stopping it
        self::$traces[$index][$label]['stop'] = microtime(true);

        $this->cleanLastIndex($label);
    }

    public function trace(string $label): void
    {
        self::$traces[] = [
            $label => microtime(true),
        ];
    }

    public function getDuration(string $label): float
    {
        $total = 0.0;

        foreach (self::$traces as $trace) {
            if (key($trace) !== $label) {
                continue;
            }

            // Just a single trace, not a start/stop
            if (!is_array($trace)) {
                continue;
            }

            if (isset($trace[$label]['stop'])) {
                $total += ($trace[$label]['stop'] - $trace[$label]['start']);
            }
        }

        return $total;
    }

    private function getIndex(string $label): ?int
    {
        return isset(self::$index[$label]) ? end(self::$index[$label]) : null;
    }

    private function cleanLastIndex(string $label): void
    {
        $index_key = array_key_last(self::$index[$label]);
        unset(self::$index[$label][$index_key]);

        if (empty(self::$index[$label])) {
            unset(self::$index[$label]);
        }
    }
}
