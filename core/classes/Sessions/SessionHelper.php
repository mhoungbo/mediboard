<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions;

use LogicException;
use Ox\Core\Sessions\Handler\SessionHandlerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionHelper
{
    private const DISPLAY_FORBIDDEN_KEYS = [
        'AppUi'          => null,
        '_security_main' => null,
        'dPcompteRendu'  => ['templateManager'],
    ];

    private SessionHandlerInterface $handler;
    protected SessionInterface      $session;

    private bool  $invalidate = false;
    private array $data       = [];

    public function __construct(SessionHandlerInterface $handler, SessionInterface $session)
    {
        $this->handler = $handler;
        $this->session = $session;
    }

    public static function getSessionName(): string
    {
        $root         = basename(dirname(__DIR__, 3));
        $session_name = preg_replace('/[^a-z0-9]/i', '', $root);

        if (!preg_match('/[a-z]/i', $session_name, $matches)) {
            $session_name = "mb{$session_name}";
        }

        return $session_name;
    }

    public function sessionExists(string $session_id): bool
    {
        return $this->handler->exists($session_id);
    }

    public static function sanitize(array $session): array
    {
        $sanitized = [];

        foreach ($session as $key => $sess) {
            if (array_key_exists($key, self::DISPLAY_FORBIDDEN_KEYS)) {
                // The whole key must not be written
                if (null === self::DISPLAY_FORBIDDEN_KEYS[$key]) {
                    continue;
                }

                if (is_array($sess)) {
                    // Check for each sub key which one can be written.
                    foreach ($sess as $sub_key => $value) {
                        if (!in_array($sub_key, self::DISPLAY_FORBIDDEN_KEYS[$key])) {
                            $sanitized[$key][$sub_key] = $value;
                        }
                    }
                }
            } else {
                $sanitized[$key] = $sess;
            }
        }

        return $sanitized;
    }

    /**
     * Get the displayable session content.
     *
     * @return array
     */
    public function getSanitizedData(): array
    {
        return self::sanitize($this->getData());
    }

    public function getData(): array
    {
        return (count($this->data) > 0) ? $this->data : $this->session->all();
    }

    public function preserveData(): void
    {
        $this->data = $this->session->all();
    }

    /**
     * @deprecated
     */
    public function getName(): string
    {
        return $this->session->getName();
    }

    /**
     * @deprecated
     */
    public function getId(): string
    {
        return $this->session->getId();
    }

    /**
     * @deprecated
     */
    public function save(): void
    {
        if (!$this->session->isStarted()) {
            $this->session->start();
        }

        $this->session->save();
    }

    /**
     * @deprecated
     */
    public function invalidate(): void
    {
        $this->invalidate = true;
    }

    /**
     * @deprecated
     */
    public function isInvalidated(): bool
    {
        return $this->invalidate;
    }

    /**
     * @deprecated
     */
    public function get(string $name, mixed $default = null): mixed
    {
        return $this->session->get($name, $default);
    }

    /**
     * @deprecated
     */
    public function set(string $name, mixed $value): void
    {
        $this->session->set($name, $value);
    }

    /**
     * @deprecated
     */
    public function remove(string $name): mixed
    {
        return $this->session->remove($name);
    }

    /**
     * @deprecated
     */
    public function getSub(string $name, string $sub, mixed $default = null): mixed
    {
        return $this->get($name)[$sub] ?? $default;
    }

    /**
     * @deprecated
     */
    public function setSub(string $name, string $sub, mixed $value): void
    {
        $part = $this->get($name);

        if (is_array($part)) {
            // Hydrating the already existing array
            $part[$sub] = $value;
        } elseif ($part === null) {
            // Creating the array from scratch
            $part = [
                $sub => $value,
            ];
        } else {
            // Actual value is invalid
            throw new LogicException('Not supported');
        }

        $this->set($name, $part);
    }

    /**
     * @deprecated
     */
    public function removeSub(string $name, string $sub): mixed
    {
        $part      = $this->get($name);
        $old_value = $part[$sub] ?? null;

        if (is_array($part)) {
            // Remove the sub from the already existing array
            unset($part[$sub]);
        } elseif ($part === null) {
            // Nothing
        } else {
            // Actual value is invalid
            throw new LogicException('Not supported');
        }

        $this->set($name, $part);

        return $old_value;
    }

    /**
     * @deprecated
     */
    public function exists(string $name): bool
    {
        return $this->get($name) !== null;
    }

    /**
     * @deprecated
     */
    public function subExists(string $name, string $sub): bool
    {
        return $this->getSub($name, $sub) !== null;
    }

    public function isDistributed(): bool
    {
        return $this->handler->isDistributed();
    }

    public function getLifetime(): int
    {
        return $this->handler->getLifetime();
    }

    public function destroy(string $id): bool
    {
        return $this->handler->destroy($id);
    }
}
