<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions\Storage;

use Ox\Core\Sessions\Tracer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageFactoryInterface;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;

/**
 * SF native factory decorator, enabling us to wrap a custom SessionStorageHandler around the SF native one.
 */
class SessionStorageFactory implements SessionStorageFactoryInterface
{
    private SessionStorageFactoryInterface $factory;
    private Tracer                         $tracer;

    public function __construct(SessionStorageFactoryInterface $factory, Tracer $tracer)
    {
        $this->factory = $factory;
        $this->tracer  = $tracer;
    }

    /**
     * @inheritDoc
     */
    public function createStorage(?Request $request): SessionStorageInterface
    {
        return new SessionStorage($this->factory->createStorage($request), $this->tracer);
    }
}
