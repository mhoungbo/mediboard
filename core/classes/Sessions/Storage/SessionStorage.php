<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Sessions\Storage;

use LogicException;
use Ox\Core\Sessions\SessionHelper;
use Ox\Core\Sessions\Tracer;
use Symfony\Component\HttpFoundation\Session\SessionBagInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MetadataBag;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;

/**
 * SF native session storage decorator, enabling us to perform some custom treatments during the session lifecycle.
 *
 * Actually:
 *   - Checks if session is "used" (not all kinds of treatments) before we are really ready (ie. setting the custom
 *   session name)
 *   - Always returns the valid but perhaps not set yet session name.
 */
class SessionStorage implements SessionStorageInterface
{
    private SessionStorageInterface $storage;
    private Tracer                  $tracer;
    private bool                    $ready = false;

    public function __construct(SessionStorageInterface $storage, Tracer $tracer)
    {
        $this->storage = $storage;
        $this->tracer  = $tracer;
    }

    private function checkReady(): void
    {
        if (!$this->ready) {
            throw new LogicException('Session is not ready');
        }
    }

    /**
     * @inheritDoc
     */
    public function start(): bool
    {
        $this->checkReady();

        $this->tracer->start('acquire');

        $result = $this->storage->start();

        $this->tracer->stop('read');

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function isStarted(): bool
    {
        return $this->storage->isStarted();
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->storage->getId();
    }

    /**
     * @inheritDoc
     */
    public function setId(string $id): void
    {
        $this->storage->setId($id);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        // Always returning "official" session name (in case of SF calling this before our listener actually set it up.
        return $this->forgeSessionName();
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name): void
    {
        $this->storage->setName($name);

        // Implicitly marking the session as ready.
        $this->ready = true;
    }

    /**
     * @inheritDoc
     */
    public function regenerate(bool $destroy = false, int $lifetime = null): bool
    {
        $this->checkReady();

        return $this->storage->regenerate($destroy, $lifetime);
    }

    /**
     * @inheritDoc
     */
    public function save(): void
    {
        $this->checkReady();

        $this->storage->save();
    }

    /**
     * @inheritDoc
     */
    public function clear(): void
    {
        $this->checkReady();

        $this->storage->clear();
    }

    /**
     * @inheritDoc
     */
    public function getBag(string $name): SessionBagInterface
    {
        $this->checkReady();

        return $this->storage->getBag($name);
    }

    /**
     * @inheritDoc
     */
    public function registerBag(SessionBagInterface $bag): void
    {
        $this->storage->registerBag($bag);
    }

    /**
     * @inheritDoc
     */
    public function getMetadataBag(): MetadataBag
    {
        $this->checkReady();

        return $this->storage->getMetadataBag();
    }

    private function forgeSessionName(): string
    {
        return SessionHelper::getSessionName();
    }
}
