<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Composer;

use Composer\Composer;
use Composer\Installer\PackageEvent;
use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\UpdateOperation;
use Composer\Script\Event;
use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Autoload\CAutoloadAlias;
use Ox\Core\Cache;
use Ox\Core\CacheManager;
use Ox\Core\CClassMap;
use Ox\Core\CMbConfig;
use Ox\Core\CMbException;
use Ox\Core\Config\CConfigDist;
use Ox\Core\Exceptions\VersionException;
use Ox\Core\Kernel\Routing\RouteManager;
use Ox\Core\Kernel\Services\ServicesManager;
use Ox\Core\Version\Builder;
use Ox\Mediboard\System\ConfigurationException;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

final class CComposerScript
{
    /** @var string */
    private const AUTOLOAD_FILE = 'autoload.php';

    public static string   $vendor_dir;
    public static string   $root_dir;
    public static Composer $composer;
    public static Event    $event;
    public static bool     $is_running            = false;
    public static bool     $is_config_file_exists = false;
    private static array   $packages = [];

    /**
     * @param Event $event
     * @param bool  $require_autoload
     * @param bool  $check_configuration
     */
    private static function init(Event $event, bool $require_autoload = false, bool $check_configuration = false): void
    {
        self::$is_running = true;
        self::$event      = $event;
        self::$composer   = $event->getComposer();
        self::$vendor_dir = self::$composer->getConfig()->get('vendor-dir');
        self::$root_dir   = dirname(self::$vendor_dir);
        if ($require_autoload) {
            require self::$vendor_dir . DIRECTORY_SEPARATOR . self::AUTOLOAD_FILE;
        }

        if ($check_configuration) {
            $config = new CMbConfig(self::$root_dir);
            if (!$config->isConfigFileExists()) {
                self::$event->getIO()->write(
                    '<warning>OX configurations is missing, you should execute "composer ox-install-config"</warning>'
                );
            } else {
                self::$is_config_file_exists = true;
            }
        }
    }

    /**
     * @param Event $event
     */
    public static function preAutoloadDump(Event $event): void
    {
        self::init($event);
        self::addPrefixPsr4();
    }

    public static function postPackageInstall(PackageEvent $event): void
    {
        self::postPackageEvent($event);
    }

    public static function postPackageUpdate(PackageEvent $event): void
    {
        self::postPackageEvent($event);
    }

    private static function postPackageEvent(PackageEvent $event): void
    {
        $package_name       = self::getPackageName($event);
        self::$packages[] = $package_name;
    }

    /**
     * @param PackageEvent $event
     *
     * @return string
     */
    public static function getPackageName(PackageEvent $event): string
    {
        /** @var InstallOperation|UpdateOperation $operation */
        $operation = $event->getOperation();

        $package = method_exists($operation, 'getPackage')
            ? $operation->getPackage()
            : $operation->getInitialPackage();

        return $package->getName();
    }

    /**
     * @param Event $event
     *
     * @return void
     * @throws Exception|InvalidArgumentException
     */
    public static function postAutoloadDump(Event $event): void
    {
        self::init($event, false, true);
        self::buildOxClassMap();
        self::buildConfigDist();
        self::buildOxClassRef();
        self::buildOxLegacyActions();
        self::buildAllRoutes();
        self::buildOpenApiDocumentation();
        self::buildAllServices();
        self::buildVersion();
    }

    /**
     * @return void
     */
    private static function addPrefixPsr4(): void
    {
        $root_package = self::$composer->getPackage();
        $root         = str_replace('vendor', '', self::$vendor_dir);
        $composer     = new CComposer($root);
        $msg          = $composer->addPrefixPsr4FromModulesComposer($root_package);

        self::write($msg);
    }

    /**
     * @param Event $event
     *
     * @return void
     * @throws Exception|InvalidArgumentException
     */
    public static function updateRoutes(Event $event): void
    {
        self::init($event, true, true);
        self::buildAllRoutes();
        self::buildOpenApiDocumentation();
    }

    /**
     *
     * @return void
     * @throws Exception
     */
    private static function buildOpenApiDocumentation(): void
    {
        $time_start = microtime(true);

        $manager = new RouteManager();
        $manager->loadAllRoutes();
        $documentation = $manager->convertRoutesApiToOAS();

        // Store
        $file = self::$root_dir . '/includes/documentation.yml';
        if (file_exists($file) && is_file($file)) {
            unlink($file);
        }
        file_put_contents($file, $documentation);

        $time = round(microtime(true) - $time_start, 3);

        self::write(
            "Generated openapi documentation file in {$file} during {$time} sec"
        );
    }

    /**
     *
     * @return void
     * @throws Exception
     */
    private static function buildOxClassMap(): void
    {
        Cache::init(self::$root_dir);

        $msg = CClassMap::getInstance()->buildClassMap();
        self::write($msg);
    }

    /**
     * @warrning need classmap.php && config_dist.php
     * @return void
     * @throws Exception
     */
    private static function buildOxClassRef(): void
    {
        $msg = CClassMap::getInstance()->buildClassRef();
        self::write($msg);
    }

    /**
     * @warrning need classmap.php
     * @return void
     * @throws Exception
     */
    private static function buildOxLegacyActions(): void
    {
        $msg = CClassMap::getInstance()->buildLegacyActions();
        self::write($msg);
    }

    /**
     *
     * @return void
     * @throws Exception
     */
    private static function buildAllRoutes(): void
    {
        $manager = new RouteManager();
        $msg     = $manager->loadAllRoutes()->buildAllRoutes();
        self::write($msg);
    }

    /**
     *
     * @return void
     * @throws Exception
     */
    private static function buildAllServices(): void
    {
        $manager = new ServicesManager();
        $msg     = $manager->buildAllServices();
        self::write($msg);
    }

    /**
     *
     * @return void
     * @throws Exception
     */
    private static function buildConfigDist(): void
    {
        $config_dist = new CConfigDist();
        $msg         = $config_dist->build();
        self::write($msg);
    }

    /**
     * @throws VersionException
     */
    public static function buildVersion(): void
    {
        $msg = Builder::buildVersion();
        self::write($msg);
    }

    /**
     * @param string $msg
     * @param string $type
     *
     * @return void
     */
    private static function write(string $msg, string $type = 'info'): void
    {
        $msg = "<{$type}>openxtrem/mediboard:</{$type}> $msg";
        self::$event->getIO()->write($msg);
    }

    /**
     * @param string $msg
     *
     * @return void
     */
    private static function warning(string $msg): void
    {
        self::write($msg, 'warning');
    }
}
