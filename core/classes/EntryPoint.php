<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use Exception;
use Ox\Core\Config\Conf;
use Ox\Core\Kernel\Exception\PublicEnvironmentException;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Mediusers\CMediusers;
use Symfony\Component\Routing\RouterInterface;

/**
 * Entry point for vue scripts.
 */
class EntryPoint
{
    public const DATA_USER     = 'current-user';
    public const DATA_FUNCTION = 'user-function';
    public const DATA_GROUP    = 'user-group';

    public const CONFIG_DISTINCT_GROUP = 'is_distinct_groups';
    public const CONFIG_DATE_FORMAT    = 'date_format';
    public const CONFIG_INSTANCE_ROLE  = 'is_qualif';
    public const PREF_DARK_THEME       = 'is_dark';
    public const PREF_CAN_VIEW_HISTORY = 'can_view_history';
    public const META_SESSION_HASH     = 'session_hash';

    private string  $entry_id;
    private ?string $script_name = null;

    protected ?RouterInterface $router = null;

    protected array $data    = [];
    protected array $meta    = [];
    protected array $prefs   = [];
    protected array $configs = [];
    protected array $links   = [];
    protected array $locales = [];

    public function __construct(string $entry_id, ?RouterInterface $router = null)
    {
        $this->entry_id = $entry_id;
        $this->router   = $router;

        $this->addConfig(self::CONFIG_DATE_FORMAT, 'datetime');
        $this->addConfigValue(self::CONFIG_DISTINCT_GROUP, CAppUI::isGroup());
        $this->addConfigValue(self::CONFIG_INSTANCE_ROLE, (new Conf())->get('instance_role') === 'qualif');
        $this->addPref(self::PREF_DARK_THEME, 'mediboard_ext_dark');
        $this->addPref(self::PREF_CAN_VIEW_HISTORY, 'system_show_history');
        $this->addMeta(self::META_SESSION_HASH, $this->getSessionHash());

        if (!CApp::getInstance()->isPublic()) {
            if ($user = $this->getCurrentUser()) {
                $this->addData(self::DATA_USER, $user);
            }

            $function = $this->getUserFunction();
            if ($function) {
                $this->addData(self::DATA_FUNCTION, $function);
            }

            $group = $this->getUserGroup();
            if ($group) {
                $this->addData(self::DATA_GROUP, $group);
            }
        }
    }

    public function getId(): string
    {
        return $this->entry_id;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param mixed $data
     */
    public function addData(string $name, $data): self
    {
        $this->data[$name] = $data;

        return $this;
    }

    public function getMeta(): array
    {
        return $this->meta;
    }

    public function setMeta(array $meta): self
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * @param mixed $meta
     */
    public function addMeta(string $name, $meta): self
    {
        $this->meta[$name] = $meta;

        return $this;
    }

    public function getPrefs(): array
    {
        return $this->prefs;
    }

    /**
     * @param mixed $pref
     */
    public function addPrefValue(string $name, $pref): self
    {
        $this->prefs[$name] = $pref;

        return $this;
    }

    public function addPref(string $name, string $pref): self
    {
        $this->prefs[$name] = CAppUI::pref($pref);

        return $this;
    }

    public function getConfigs(): array
    {
        return $this->configs;
    }

    /**
     * @param mixed $config
     */
    public function addConfigValue(string $name, $config): self
    {
        $this->configs[$name] = $config;

        return $this;
    }

    /**
     * Get and add the $config to configs under the key $name.
     *
     * @param null|string|CMbObject $context
     *
     * @throws Exception
     */
    public function addConfig(string $name, string $config, $context = null): self
    {
        $this->configs[$name] = CAppUI::conf($config, $context);

        return $this;
    }

    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * Add the $link under the $name key.
     */
    public function addLinkValue(string $name, string $link): self
    {
        $this->links[$name] = $link;

        return $this;
    }

    /**
     * Generate the $link with $parameters using a Router.
     * Add the $link under the $name key.
     *
     * @throws CMbException
     */
    public function addLink(string $name, string $link, array $parameters = []): self
    {
        if ($this->router) {
            $this->links[$name] = $this->router->generate($link, $parameters);
        } else {
            $this->links[$name] = CApp::generateUrl($link, $parameters);
        }


        return $this;
    }

    public function getLocales(): array
    {
        return $this->locales;
    }

    /**
     * Replace all the locales with $locales.
     *
     * @param array $locales Strings to localise. This array can contains simple strings or arrays.
     *                       In case of arrays the first element of the array will be the string to localise and
     *                       the rest of elements will be passed as additionnal arguments to the localisation function.
     */
    public function setLocales(array $locales): self
    {
        $this->locales = [];
        foreach ($locales as $locale) {
            if (is_array($locale)) {
                $str = array_shift($locale);
                $this->addLocale($str, ...$locale);
            } else {
                $this->addLocale($locale);
            }
        }

        return $this;
    }

    /**
     * @param string $locale  The string to localise
     * @param mixed  ...$args Additionnal arguments to pass to the localisation function
     */
    public function addLocale(string $locale, ...$args): self
    {
        $this->locales[$locale] = CAppUI::tr($locale, $args);

        return $this;
    }

    public function setScriptName(string $script): self
    {
        $this->script_name = $script;

        return $this;
    }

    public function getScriptName(): ?string
    {
        return $this->script_name;
    }

    /**
     * Convert a 1-dimension array of string from ISO to UTF-8.
     *
     * @param array $data
     *
     * @return void
     */
    protected function convertArrayOfStringsEncoding(array &$data): void
    {
        array_walk($data, function (string &$v): void {
            $v = mb_convert_encoding($v, 'UTF-8', 'ISO-8859-1');
        });
    }

    private function getSessionHash(): string
    {
        if (null === ($session_id = CApp::getSessionHelper()?->getId())) {
            return '';
        }

        try {
            if (($user = CUser::get()) && $user->_id) {
                $session_id .= $user->_id;
            }
        } catch (PublicEnvironmentException) {
            // Nothing to do.
        }


        return CMbSecurity::hash(CMbSecurity::MD5, $session_id);
    }

    /**
     * @return array|null
     * @throws Exception
     */
    private function getCurrentUser(): ?array
    {
        $user = CUser::get();
        if (!$user?->_id) {
            return null;
        }

        $mediuser = $user->loadRefMediuser();
        if (!$mediuser?->_id) {
            $mediuser = null;
        }

        return [
            'id'         => $user->_id,
            'guid'       => $user->loadRefMediuser()?->_guid,
            'first_name' => CMbString::utf8Encode($user->user_first_name),
            'last_name'  => mb_convert_encoding($user->user_last_name, "UTF-8", "ISO-8859-1"),
            'view'       => $mediuser ? mb_convert_encoding($mediuser->_view, "UTF-8", "ISO-8859-1") : null,
            'login'      => mb_convert_encoding($user->user_username, "UTF-8", "ISO-8859-1"),
            'type'       => $user->user_type,
            'color'      => $mediuser?->_color,
        ];
    }

    /**
     * @return array|null
     * @throws Exception
     */
    private function getUserFunction(): ?array
    {
        $user = CMediusers::get();
        if (!$user?->_id) {
            return null;
        }
        $user->loadRefFunction();

        return [
            'id'   => $user->_ref_function->_id,
            'guid' => $user->_ref_function->_guid,
            'view' => mb_convert_encoding($user->_ref_function->_view, "UTF-8", "ISO-8859-1"),
        ];
    }

    /**
     * @return array|null
     * @throws Exception
     */
    private function getUserGroup(): ?array
    {
        $user = CMediusers::get();
        if (!$user?->_id) {
            return null;
        }
        $user->loadRefFunction()->loadRefGroup();

        return [
            'id'   => $user->_ref_function->_ref_group->_id,
            'guid' => $user->_ref_function->_ref_group->_guid,
            'view' => mb_convert_encoding($user->_ref_function->_ref_group->_view, "UTF-8", "ISO-8859-1"),
        ];
    }
}
