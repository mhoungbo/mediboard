<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;


use Ox\Core\Elastic\ElasticClient;
use Ox\Core\Redis\CRedisClient;
use Ox\Core\ResourceLoaders\CHTMLResourceLoader;
use Ox\Mediboard\System\CExchangeSource;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CAppPerformance
{
    // Performance
    private $genere  = null;
    private $memoire = null;
    private $size    = null;
    private $objets  = 0;
    private $ip      = null;

    // Cache
    private $cache          = [];
    private $cachableCount  = null;
    private $cachableCounts = null;

    // Objects
    private $objectCounts = null;

    // Data source information
    private $dataSources     = [];
    private $dataSourceTime  = null;
    private $dataSourceCount = null;
    private $enslaved;

    // Nosql
    private $nosqlTime  = 0;
    private $nosqlCount = 0;

    // transport tiers
    private $transportTiers = [];


    /**
     * Prepare performance data to be displayed
     */
    public function preparePerformance(Response $response): self
    {
        arsort(CStoredObject::$cachableCounts);
        arsort(CStoredObject::$objectCounts);

        $this->genere         = CApp::getInstance()->getDuration();
        $this->memoire        = CHTMLResourceLoader::getOutputMemory();
        $this->objets         = array_sum(CStoredObject::$objectCounts);
        $this->cachableCount  = array_sum(CStoredObject::$cachableCounts);
        $this->cachableCounts = CStoredObject::$cachableCounts;
        $this->objectCounts   = CStoredObject::$objectCounts;
        $this->ip             = $_SERVER["SERVER_ADDR"] ?? null;

        if (!$response instanceof StreamedResponse) {
            $this->size = CMbString::toDecaBinary(strlen($response->getContent()));
        }

        $this->cache = [
            "totals" => Cache::getTotals(),
            "total"  => Cache::getTotal(),
        ];

        $this->enslaved = CSQLDataSource::isEnslaved();

        $time  = 0;
        $count = 0;

        // Data sources performance
        foreach (CSQLDataSource::$dataSources as $dsn => $ds) {
            if (!$ds) {
                continue;
            }

            $chrono      = $ds->chrono;
            $chronoFetch = $ds->chronoFetch;

            $time  += $chrono->total + $chronoFetch->total;
            $count += $chrono->nbSteps;

            $this->dataSources[$dsn] = [
                "latency"    => $ds->latency,
                "ct"         => $ds->connection_time,
                "count"      => $chrono->nbSteps,
                "time"       => $chrono->total,
                "countFetch" => $chronoFetch->nbSteps,
                "timeFetch"  => $chronoFetch->total,
            ];
        }

        $this->dataSourceTime  = $time;
        $this->dataSourceCount = $count;

        $redis_chrono = CRedisClient::$chrono;
        if ($redis_chrono) {
            $this->nosqlTime  = (float)$redis_chrono->total;
            $this->nosqlCount = $redis_chrono->nbSteps;
        }

        $elastic_chrono = ElasticClient::getChrono();
        if ($elastic_chrono) {
            $this->nosqlTime  += (float)$elastic_chrono->total;
            $this->nosqlCount += $elastic_chrono->nbSteps;
        }

        // Transport tiers
        $this->transportTiers['total']   = [
            'count' => 0,
            'time'  => 0,
        ];
        $this->transportTiers['sources'] = [];

        foreach (CExchangeSource::$call_traces as $exchange_source => $chronometer) {
            $this->transportTiers['total']['count'] += $chronometer->nbSteps;
            $this->transportTiers['total']['time']  += $chronometer->total;

            $_short_name = CClassMap::getSN($exchange_source);

            $this->transportTiers['sources'][$_short_name] = [
                'count' => $chronometer->nbSteps,
                'time'  => $chronometer->total,
            ];
        }


        return $this;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
