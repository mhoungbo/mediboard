<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Exceptions;

class CanNotMerge extends MergeException
{
    public static function invalidObject(): self
    {
        return new self('mergeNotCMbObject');
    }

    public static function objectNotFound(): self
    {
        return new self('mergeNoId');
    }

    public static function differentType(): self
    {
        return new self('mergeDifferentType');
    }
}
