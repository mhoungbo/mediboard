<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Exceptions;

class CouldNotMerge extends MergeException
{
    public static function invalidNumberOfObjects(): self
    {
        return new self('mergeTooFewObjects');
    }

    public static function storeFailure(string $message): self
    {
        return new self($message);
    }

    public static function backrefsTransferFailure(string $message): self
    {
        return new self($message);
    }

    public static function deleteFailure(string $message): self
    {
        return new self($message);
    }

    public static function idexStoreFailure(string $message): self
    {
        return new self($message);
    }

    public static function domainMergeImpossible(): self
    {
        return new self('CDomain-merge_impossible');
    }

    public static function groupDomainMergeImpossible(): self
    {
        return new self('CGroupDomain-merge_impossible');
    }

    public static function mergeImpossible(): self
    {
        return new self('CMediusers-merge-impossible');
    }

    public static function baseObjectNotFound(): self
    {
        return new self('common-error-Object not found');
    }

    public static function objectNotFound(): self
    {
        return new self('common-error-Object not found');
    }

    public static function differentType(string $object_class, string $base_class): self
    {
        return new self(
            "An object from type '{$object_class}' can't be merge with an object from type '{$base_class}'"
        );
    }
}
