<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Exceptions;

use Exception;

/**
 * Exception for legacy code that will make the framework produce a redirection.
 */
class AppRedirectionException extends Exception
{
    public function __construct(private readonly string $parameters)
    {}

    public function getParameters(): string
    {
        return $this->parameters;
    }
}
