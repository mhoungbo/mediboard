<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use Ox\Mediboard\Mediusers\CMediusers;

class AppUiBridge
{
    public function getCurrentUserId(): ?int
    {
        return CAppUI::$instance->user_id;
    }

    public function getCurrentUserGroup(): ?int
    {
        return (CAppUI::$instance !== null) ? CAppUI::$instance->user_group : null;
    }

    public function getCurrentUserFunction(): ?int
    {
        return ((CAppUI::$instance->_ref_user !== null) && (CAppUI::$instance->_ref_user instanceof CMediusers)) ? CAppUI::$instance->_ref_user->function_id : null;
    }

    public function isIntranet(): bool
    {
        return CAppUI::isIntranet();
    }

    public function getIp(): ?string
    {
        return (CAppUI::$instance->proxy) ?: CAppUI::$instance->ip;
    }

    public function getAuthMethod(): ?string
    {
        return CAppUI::getAuthMethod();
    }

    /**
     * Provides session-related CViewAccessToken identifier value, through a bridge call.
     *
     * @return int|null
     */
    public function getTokenId(): ?int
    {
        return CAppUI::$token_id;
    }

    public function canReviveSession(): bool
    {
        return CAppUI::reviveSession();
    }

    public function setSessionRevive(bool $enable): void
    {
        CAppUI::$session_no_revive = $enable;
    }
}
