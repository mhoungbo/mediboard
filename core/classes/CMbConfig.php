<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use Exception;
use Ox\Core\Config\Config;

/**
 * Mediboard configuration accessor and mutator
 */
class CMbConfig
{
    public const CONFIG_FILE          = 'includes/config.php';
    public const CONFIG_DIST_FILE     = 'includes/config_dist.php';
    public const CONFIG_OVERLOAD_FILE = 'includes/config_overload.php';

    /** Parser options */
    public array $options = [
        'name' => 'dPconfig',
    ];

    /** Configuration values */
    public array $values = [];

    /** @var null|string Default configuration path */
    public ?string $sourcePath;

    /** @var string Main configuration path */
    public string $targetPath = '';

    /** @var string Overload configuration path */
    public string $overloadPath = '';

    // Ignorer également db et php
    /** Forbidden configurations to store in DB */
    public static array $forbidden_values = [
        'config_db',
        'root_dir',
        'instance_role',
        'base_url',
        'servers_ip',
        'offline_time_start',
        'offline_time_end',
    ];

    private string $rootPath;

    /**
     * CMbConfig constructor.
     *
     * @param null|string $path
     */
    public function __construct(?string $path = null)
    {
        if ($path !== null) {
            // without global
            $path               = (substr($path, -1) !== '/') ? ($path . '/') : $path;
            $this->sourcePath   = $path . self::CONFIG_DIST_FILE;
            $this->targetPath   = $path . self::CONFIG_FILE;
            $this->overloadPath = $path . self::CONFIG_OVERLOAD_FILE;
        } else {
            // legacy
            $this->rootPath     = dirname(__DIR__, 2) . DIRECTORY_SEPARATOR;
            $this->sourcePath   = $this->rootPath . self::CONFIG_DIST_FILE;
            $this->targetPath   = $this->rootPath . self::CONFIG_FILE;
            $this->overloadPath = $this->rootPath . self::CONFIG_OVERLOAD_FILE;
        }
    }

    public function isConfigFileExists(): bool
    {
        return file_exists($this->targetPath);
    }

    /**
     * Guess config values
     */
    private function guessValues(): void
    {
        $this->values['root_dir'] = strtr(realpath($this->rootPath), "\\", "/");
        $this->values['base_url'] = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["PHP_SELF"], 2);
    }

    /**
     * Load config values from path
     *
     * @param null|string $path Configuration path
     *
     * @return array
     * @throws Exception
     */
    private function loadValuesFromPath(?string $path = null)
    {
        if (!is_file($path ?? '')) {
            return [];
        }

        $config = new Config();

        try {
            $configContainer = $config->parseConfig($path, $this->options);
        } catch (Exception $e) {
            return [];
        }

        $rootConfig = $configContainer->toArray();

        return $rootConfig['root'];
    }

    /**
     * Load all config values
     * @return void
     * @throws Exception
     */
    public function load()
    {
        $this->values = [];
        $this->values = array_replace_recursive(
            $this->values,
            $this->loadValuesFromPath($this->sourcePath),
            $this->loadValuesFromPath($this->targetPath),
            $this->loadValuesFromPath($this->overloadPath)
        );

        if (!is_file($this->targetPath)) {
            $this->guessValues();
        }
    }

    /**
     * Update config values in file
     *
     * @param array $newValues New values
     * @param bool  $keepOld   Whether to keep old value or not
     *
     * @return bool|null
     * @throws Exception
     */
    public function update(array $newValues = [], bool $keepOld = true): ?bool
    {
        // Avoid passing null to stripslashes which would result on a deprecation notice
        array_walk_recursive($newValues, fn($value) => ($value !== null) ? stripslashes($value) : '');

        if ($keepOld) {
            global $dPconfig;

            $conf = $dPconfig;

            $newValues = CMbArray::mergeRecursive($conf, $newValues);
        }

        if (!count($newValues)) {
            if (is_file($this->targetPath)) {
                unlink($this->targetPath);
            }

            return null;
        }

        $this->values = $newValues;
        $dPconfig     = $this->values;

        $config = new Config();
        $config->parseConfig($this->values, $this->options);

        return $config->writeConfig($this->targetPath, $this->options);
    }

    /**
     * Set a configuration
     *
     * @param string $path  Path
     * @param mixed  $value Value
     *
     * @return void
     */
    public function set(string $path, $value)
    {
        $conf   = $this->values;
        $values = &$conf;

        $items = explode(' ', $path);
        foreach ($items as $part) {
            if (!array_key_exists($part, $conf)) {
                $conf[$part] = [];
            }

            $conf = &$conf[$part];
        }
        $conf = $value;

        $this->values = $values;
    }

    /**
     * Get a configuration from its path
     *
     * @param string $path Configuration path
     *
     * @return mixed
     */
    public function get(string $path)
    {
        $conf = $this->values;

        $items = explode(' ', $path);
        foreach ($items as $part) {
            if (!isset($conf[$part])) {
                return false;
            }
            $conf = $conf[$part];
        }

        return $conf;
    }

    /**
     * Load a configuration
     *
     * @param string $key    Key
     * @param string $value  Value
     * @param array  $config Configuration array
     *
     * @return void
     */
    public static function loadConf($key, $value, &$config)
    {
        if (count($key) > 1) {
            $firstkey = array_shift($key);
            if (!isset($config[$firstkey])) {
                $config[$firstkey] = [];
            }
            self::loadConf($key, $value, $config[$firstkey]);
        } elseif (is_array($config)) {
            $config[$key[0]] = $value;
        }
    }

    /**
     * Build configuration list from configuration array
     *
     * @param array  $list  Configuration list
     * @param array  $array Configuration array
     * @param string $_key  Key
     */
    public static function buildConf(array &$list, array $array, ?string $_key): void
    {
        foreach ($array as $key => $value) {
            $_conf_key = ($_key ? "$_key " : "") . $key;
            if (is_array($value)) {
                self::buildConf($list, $value, $_conf_key);
                continue;
            }
            $list[$_conf_key] = $value;
        }
    }

    /**
     * Load config values from database
     *
     * @throws Exception
     */
    public static function loadValuesFromDB(): void
    {
        global $dPconfig;
        $ds = CSQLDataSource::get("std");

        // If table 'config_db' does not exist, it will load configs from config_overload.php
        if ($ds->hasTable('config_db')) {
            $request = "SELECT * FROM config_db WHERE config_db.key "
                . CSQLDataSource::prepareNotIn(self::$forbidden_values);
            $configs = $ds->loadList($request);
        }

        if (!empty($configs)) {
            foreach ($configs as $_value) {
                CMbConfig::loadConf(explode(" ", $_value['key']), $_value['value'], $dPconfig);
            }
        }

        // Re-include config_overload
        if (is_file(__DIR__ . "/../../includes/config_overload.php")) {
            include __DIR__ . "/../../includes/config_overload.php";
        }
    }

    /**
     * Set a configuration in DB
     *
     * @param string      $key   Path of the configuration
     * @param null|string $value Value of the configuration
     *
     * @throws Exception
     */
    public static function setConfInDB(string $key, ?string $value): bool
    {
        $ds    = CSQLDataSource::get("std");
        $query = "INSERT INTO `config_db`
                VALUES (%1, %2)
                ON DUPLICATE KEY UPDATE `value` = %2";

        $exec = $ds->exec($ds->prepare($query, $key, $value));

        return ($exec !== false);
    }
}
