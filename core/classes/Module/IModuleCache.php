<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Module;

interface IModuleCache
{
    /**
     * Clears cache keys from patterns
     */
    public function clear(int $layer): void;

    /**
     * Specific actions to run
     */
    public function clearSpecialActions(): void;
}
