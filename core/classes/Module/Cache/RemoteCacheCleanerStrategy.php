<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Module\Cache;

use Exception;
use InvalidArgumentException;
use Ox\Core\CacheManager;
use Ox\Core\CApp;
use Ox\Core\CSmartyDP;
use Ox\Core\Locales\Translator;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Clear specific server SHM cache.
 */
class RemoteCacheCleanerStrategy implements CacheCleanerStrategyInterface
{
    private string              $cache;
    private int                 $layer;
    private HttpClientInterface $client;
    private string              $target;
    private string              $cookie;
    private string              $url_clear_cache;
    private string              $response;
    private ?string             $error = null;

    private const PROTOCOL_CLEAR_CACHE = 'http';

    /**
     * @param string              $cache           Cache key to clear
     * @param int                 $layer           SHM or DSHM layer to clear (see CacheManager)
     * @param HttpClientInterface $client          HTTClient for request execution
     * @param string              $target          Target address
     * @param string              $cookie          Cookie for authentication
     * @param string              $url_clear_cache URL used to clear cache
     *
     * @throws InvalidArgumentException
     */
    public function __construct(
        string              $cache,
        int                 $layer,
        HttpClientInterface $client,
        string              $target,
        string              $cookie,
        string              $url_clear_cache
    ) {
        $translator = new Translator();

        // Cookie is mandatory for authentication in request
        if ($cookie === '') {
            throw new InvalidArgumentException($translator->tr('system-msg-Cookie is missing'));
        }

        if ($target === '') {
            throw new InvalidArgumentException($translator->tr('system-msg-Target is missing'));
        }

        if ($url_clear_cache === '') {
            throw new InvalidArgumentException($translator->tr('system-msg-Url is missing'));
        }

        $this->cache           = $cache;
        $this->layer           = $layer;
        $this->client          = $client;
        $this->target          = $target;
        $this->cookie          = $cookie;
        $this->url_clear_cache = $url_clear_cache;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function execute(): void
    {
        $this->log();

        $options = [
            "query"        => [
                'cache'        => stripcslashes($this->cache),
                'layer_strict' => $this->layer,
                'target'       => 'local',
            ],
            "headers"      => [
                'Cookie' => $this->cookie,
            ],
            'timeout'      => 3,
            'max_duration' => 3,
        ];

        // Request in HTTP and not HTTPS because of legacy behavior
        $response = $this->client->request(
            'GET',
            self::PROTOCOL_CLEAR_CACHE . "://{$this->target}" . $this->url_clear_cache,
            $options
        );

        try {
            $this->response = $response->getContent();
        } catch (Exception $e) {
            $this->error = $e->getMessage();
        }
    }

    public function getHtmlResult(CSmartyDP $smarty = null): string
    {
        if ($this->error === null) {
            return $this->response;
        }

        if ($smarty === null) {
            throw new InvalidArgumentException();
        }

        $smarty->assign(
            [
                'outputs'     => $this->error,
                'actual_host' => $this->target,
            ]
        );

        return $smarty->fetch('cache/view_cache_header.tpl');
    }

    /**
     * @throws Exception
     */
    private function log(): void
    {
        CApp::log(
            sprintf(
                'Clearing [%s] cache [%s] for [%s]',
                CacheManager::formatLayer($this->layer),
                $this->cache,
                $this->target
            )
        );
    }
}
