<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Module\Requirements;

use Countable;
use Exception;
use Iterator;
use Ox\Core\CAppUI;
use Ox\Core\CMbObject;
use Ox\Core\CModelObject;
use Ox\Core\Module\CModule;
use Ox\Mediboard\Etablissement\CGroups;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReturnTypeWillChange;

abstract class CRequirementsManager implements Countable, Iterator
{
    public const TYPE_EXPECTED_EQUALS_OR_GREATER = 'equalsOrGreater';
    public const TYPE_EXPECTED_EQUALS_OR_LESS    = 'equalsOrLess';
    public const TYPE_EXPECTED_NOTNULL           = 'notNull';
    public const TYPE_EXPECTED_REGEX             = 'regex';
    public const TYPE_EXPECTED_BOOL              = 'bool';
    public const TYPE_EXPECTED_NOT_EQUALS        = 'notEquals';

    /** @var CRequirementsItem[] */
    private array $items = [];

    private array $tabs = [];

    private array $groups = [];

    private int $position = 0;

    private ?CRequirementsDescription $description = null;

    private int $items_index = 0;

    protected ?CGroups $establishment = null;

    public function setEstablishment(CGroups $establishment): void
    {
        $this->establishment = $establishment;
    }

    public function getEstablishment(): CGroups
    {
        return $this->establishment;
    }

    /**
     * @throws ReflectionException
     * @throws Exception
     */
    final public function checkRequirements(?CGroups $establishment = null): bool
    {
        $this->establishment = $establishment;
        if (!$this->establishment) {
            $this->establishment = CGroups::loadCurrent();
        }
        $reflection = new ReflectionClass($this);
        $methods    = $reflection->getMethods();

        // Reflection on trait
        $methodsTrait = array_merge(
               [],
            ...array_values(
                   array_map(function ($reflection_trait) {
                       /** @var ReflectionClass $reflection_trait */
                       return $reflection_trait->getMethods();
                   }, $reflection->getTraits())
               )
        );

        // methods in traits
        $methodNamesTrait = array_map(function ($method) {
            /** @var ReflectionMethod $method */
            return $method->getName();
        }, $methodsTrait);

        foreach ($methods as $method) {
            if (strpos($method->getName(), 'check') !== 0) {
                // Only checkLoremIpsum methods
                continue;
            }
            if (($method->class !== get_class($this)) || in_array($method->getName(), $methodNamesTrait)) {
                // Ignore parent methods (current!!)
                continue;
            }

            $method->invoke($this);

            // set annotation
            $doc = $method->getDocComment();

            if ($group = $this->getTag($doc, 'group')) {
                !array_key_exists($group, $this->tabs) ? ($this->groups[$group] = 1) : $this->groups[$group]++;
            }

            if ($tab = $this->getTag($doc, 'tab')) {
                !array_key_exists($tab, $this->tabs) ? ($this->tabs[$tab] = 1) : $this->tabs[$tab]++;
            }

            if (($group === null) && ($tab === null)) {
                continue;
            }

            /** @var CRequirementsItem $item */
            foreach ($this->items as $key => $item) {
                if ($key < $this->items_index) {
                    continue;
                }

                if ($tab) {
                    $item->setTab($tab);
                }

                if ($group) {
                    $item->setGroup($group);
                }
            }

            $this->items_index = count($this->items);
        }

        return $this->countErrors() === 0;
    }

    private function getTag(string $documentation, string $tag): ?string
    {
        $tag = trim($tag);
        $re  = "/(?:@$tag (?'$tag'[\w|[[:blank:]]+))/m";
        preg_match_all($re, $documentation, $matches, PREG_PATTERN_ORDER, 0);

        return empty($matches[$tag]) ? null : strtolower(trim(reset($matches[$tag])));
    }

    protected function assertModulesActived(array $modules): void
    {
        foreach ($modules as $module_name) {
            $module = CModule::getInstalled($module_name);
            $actual = ($module && $module->mod_active) ? true : false;
            $item   = new CRequirementsItem(true, $actual, $actual === true);
            $item->setDescription("module-{$module_name}-court");

            $this->addItems($item);
        }
    }

    protected function assertObjectFieldNotNull(CModelObject $object, string $field, ?string $description = null): void
    {
        $this->assertObjectFieldCheck(
            $object,
            $field,
            self::TYPE_EXPECTED_NOTNULL,
            $description,
            self::TYPE_EXPECTED_NOTNULL
        );
    }

    protected function assertObjectFieldTrue(CModelObject $object, string $field, ?string $description = null): void
    {
        $this->assertObjectFieldCheck($object, $field, true, $description, self::TYPE_EXPECTED_BOOL);
    }

    protected function assertObjectFieldFalse(CModelObject $object, string $field, ?string $description = null): void
    {
        $this->assertObjectFieldCheck($object, $field, false, $description, self::TYPE_EXPECTED_BOOL);
    }

    /**
     * @param mixed $expected
     */
    protected function assertObjectFieldEquals(
        CModelObject $object,
        string       $field,
                     $expected,
        ?string      $description = null
    ): void {
        $this->assertObjectFieldCheck($object, $field, $expected, $description, self::TYPE_EXPECTED_BOOL);
    }

    /**
     * @param mixed $expected
     */
    private function assertObjectFieldCheck(
        CModelObject $object,
        string       $field,
                     $expected,
        ?string      $description = null,
        ?string      $type = null
    ): void {
        if (($field === '_id') && !$description) {
            $field = $object->_spec->key ?? $field;
        }

        $actual = $object->{$field};
        $item   = new CRequirementsItem($expected, $actual, $this->check($actual, $expected, $type));
        if (!$description) {
            $item->setDescription("{$object->_class}-{$field}[{$object->_class}]");
        } else {
            $item->setDescription($description, false);
        }

        $this->addItems($item);
    }

    /**
     * @param mixed $actual
     */
    protected function assertNotNull($actual, string $description): void
    {
        $this->assertCheck($actual, self::TYPE_EXPECTED_NOTNULL, $description, self::TYPE_EXPECTED_NOTNULL);
    }

    /**
     * @param mixed $actual
     */
    protected function assertTrue($actual, string $description): void
    {
        $this->assertCheck($actual, true, $description, self::TYPE_EXPECTED_BOOL);
    }

    /**
     * @param mixed $actual
     */
    protected function assertFalse($actual, string $description): void
    {
        $this->assertCheck($actual, false, $description, self::TYPE_EXPECTED_BOOL);
    }

    /**
     * @param mixed $actual
     * @param mixed $expected
     */
    protected function assertEquals($actual, $expected, string $description): void
    {
        $this->assertCheck($actual, $expected, $description);
    }

    /**
     * @param mixed $actual
     * @param mixed $expected
     */
    protected function assertNotEquals($actual, $expected, string $description): void
    {
        $this->assertCheck($actual, $expected, $description, self::TYPE_EXPECTED_NOT_EQUALS);
    }

    /**
     * @param mixed $actual
     */
    protected function assertRegex($actual, string $regex, string $description): void
    {
        $this->assertCheck($actual, $regex, $description, self::TYPE_EXPECTED_REGEX);
    }

    /**
     * @param mixed $actual
     * @param mixed $expected
     */
    private function assertCheck($actual, $expected, string $description, ?string $type = null): void
    {
        $item = new CRequirementsItem($expected, $actual, $this->check($actual, $expected, $type));
        $item->setDescription($description, false);

        $this->addItems($item);
    }

    /**
     * @param mixed $expected
     */
    protected function assertObjectConfEquals(?CMbObject $object, string $field, $expected): void
    {
        $this->assertObjectConfCheck($object, $field, $expected);
    }

    protected function assertObjectConfTrue(?CMbObject $object, string $field): void
    {
        $this->assertObjectConfCheck($object, $field, true, self::TYPE_EXPECTED_BOOL);
    }

    protected function assertObjectConfFalse(?CMbObject $object, string $field): void
    {
        $this->assertObjectConfCheck($object, $field, false, self::TYPE_EXPECTED_BOOL);
    }

    protected function assertObjectConfRegex(?CMbObject $object, string $field, string $regex): void
    {
        $this->assertObjectConfCheck($object, $field, $regex, self::TYPE_EXPECTED_REGEX);
    }

    protected function assertObjectConfNotNull(?CMbObject $object, string $field): void
    {
        $this->assertObjectConfCheck($object, $field, self::TYPE_EXPECTED_NOTNULL, self::TYPE_EXPECTED_NOTNULL);
    }

    /**
     * @param mixed $expected
     */
    private function assertObjectConfCheck(CMbObject $object, string $field, $expected, ?string $type = null): void
    {
        if (!$object->_ref_object_configs) {
            $object->loadRefObjectConfigs();
        }
        $actual = $object->_ref_object_configs ? $object->_ref_object_configs->$field : null;
        $item   = new CRequirementsItem($expected, $actual, $this->check($actual, $expected, $type));
        $item->setDescription("{$object->_ref_object_configs->_class}-{$field}");
        $this->addItems($item);
    }

    /**
     * @param mixed $expected
     *
     * @throws Exception
     */
    protected function assertConfEquals(string $path, $expected): void
    {
        $this->assertConf($path, $expected);
    }

    /**
     * @throws Exception
     */
    protected function assertConfEqualsOrGreater(string $path, int $expected): void
    {
        $this->assertConf($path, $expected, self::TYPE_EXPECTED_EQUALS_OR_GREATER);
    }

    /**
     * @throws Exception
     */
    protected function assertConfEqualsOrLess(string $path, int $expected): void
    {
        $this->assertConf($path, $expected, self::TYPE_EXPECTED_EQUALS_OR_LESS);
    }

    /**
     * @throws Exception
     */
    protected function assertGConfEqualsOrGreater(string $path, int $expected): void
    {
        $this->assertConf($path, $expected, self::TYPE_EXPECTED_EQUALS_OR_GREATER, true);
    }

    /**
     * @throws Exception
     */
    protected function assertGConfEqualsOrLess(string $path, int $expected): void
    {
        $this->assertConf($path, $expected, self::TYPE_EXPECTED_EQUALS_OR_LESS, true);
    }

    /**
     * @throws Exception
     */
    protected function assertConfTrue(string $path): void
    {
        $this->assertConf($path, true, self::TYPE_EXPECTED_BOOL);
    }

    /**
     * @throws Exception
     */
    protected function assertConfFalse(string $path): void
    {
        $this->assertConf($path, false, self::TYPE_EXPECTED_BOOL);
    }

    /**
     * @throws Exception
     */
    protected function assertConfNotNull(string $path): void
    {
        $this->assertConf($path, self::TYPE_EXPECTED_NOTNULL, self::TYPE_EXPECTED_NOTNULL);
    }

    /**
     * @throws Exception
     */
    protected function assertGConfNotNull(string $path): void
    {
        $this->assertGConf($path, self::TYPE_EXPECTED_NOTNULL, self::TYPE_EXPECTED_NOTNULL);
    }

    /**
     * @param mixed $expected
     *
     * @throws Exception
     */
    protected function assertGConfEquals(string $path, $expected): void
    {
        $this->assertGConf($path, $expected);
    }

    /**
     * @throws Exception
     */
    protected function assertGConfTrue(string $path): void
    {
        $this->assertGConf($path, true, self::TYPE_EXPECTED_BOOL);
    }

    /**
     * @throws Exception
     */
    protected function assertGConfFalse(string $path): void
    {
        $this->assertGConf($path, false, self::TYPE_EXPECTED_BOOL);
    }

    /**
     * @throws Exception
     */
    protected function assertGConfRegex(string $path, string $regex): void
    {
        $this->assertGConf($path, $regex, self::TYPE_EXPECTED_REGEX);
    }

    /**
     * @param mixed $expected
     *
     * @throws Exception
     */
    private function assertGConf(string $path, $expected, ?string $type = null): void
    {
        $this->assertConf($path, $expected, $type, true);
    }

    /**
     * @param mixed $expected
     *
     * @throws Exception
     */
    private function assertConf(string $path, $expected, ?string $type = null, bool $gconf = false): void
    {
        $actual = ($gconf) ? CAppUI::gconf($path, $this->establishment->group_id) : CAppUI::conf($path);

        // item
        $item    = new CRequirementsItem($expected, $actual, $this->check($actual, $expected, $type));
        $explode = explode(' ', $path);

        $item->setDescription('config-' . implode('-', $explode));
        $item->setSection((count($explode) > 0) ? $explode[0] : 'system');
        $this->addItems($item);
    }

    /**
     * @param mixed $actual
     * @param mixed $expected
     */
    final protected function check($actual, $expected, ?string $type = null): bool
    {
        switch ($type) {
            case self::TYPE_EXPECTED_NOTNULL:
                if (is_string($actual)) {
                    return $actual !== '';
                }

                return $actual !== null;

            case self::TYPE_EXPECTED_BOOL:
                return ($actual == $expected) && ($actual !== null);

            case self::TYPE_EXPECTED_REGEX:
                return preg_match($expected, $actual) == true;

            case self::TYPE_EXPECTED_NOT_EQUALS:
                return $actual !== $expected;

            case self::TYPE_EXPECTED_EQUALS_OR_GREATER:
                return $actual >= $expected;

            case self::TYPE_EXPECTED_EQUALS_OR_LESS:
                return $actual <= $expected;

            default:
                return $actual === $expected;
        }
    }

    /**
     * Get descriptor object
     */
    public function getDescription(): CRequirementsDescription
    {
        $this->description = new CRequirementsDescription();

        return $this->description;
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function countErrors(): int
    {
        $count = 0;
        foreach ($this->items as $item) {
            if (!$item->isCheck()) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @return mixed
     */
    #[ReturnTypeWillChange]
    public function current()
    {
        return $this->items[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    /**
     * @return mixed
     */
    #[ReturnTypeWillChange]
    public function key()
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->items[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
        $this->items    = array_values($this->items);
    }

    /**
     * @param mixed $new_items
     */
    protected function addItems($new_items): void
    {
        $new_items   = is_array($new_items) ? $new_items : [$new_items];
        $this->items = array_merge($this->items, $new_items);
    }

    public function serialize(bool $is_grouped = true): array
    {
        $data = [];
        foreach ($this->items as $item) {
            if ($is_grouped) {
                // data
                $data[$item->getTab()][$item->getGroup()][$item->getSection()][] = [
                    'expected'    => $item->getExpected(),
                    'actual'      => $item->getActual(),
                    'description' => $item->getDescription(),
                    'check'       => $item->isCheck(),
                ];
            } else {
                $data[] = $item->serialize();
            }
        }

        return $data;
    }

    public function getTabs(): array
    {
        return array_keys($this->serialize());
    }

    public function getGroups(): array
    {
        return array_keys($this->groups);
    }

    /**
     * @return CRequirementsItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
