<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Module\Requirements;

use Ox\Core\CMbException;
use Ox\Core\Locales\Translator;

class CRequirementsException extends CMbException
{
    public const TOO_MUCH_REQUIREMENTS_CLASS = "1";

    /**
     * CConstantException constructor.
     *
     * @param int    $id  id of exception
     * @param string $msg optional msg
     */
    public function __construct($id, $msg = "")
    {
        $message = (new Translator())->tr("CRequirementsException-{$id}", $msg);
        parent::__construct($message, $id);
    }
}
