<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Module\Requirements;

use Ox\Core\CAppUI;
use Ox\Core\Locales\Translator;

class CRequirementsItem
{
    /** @var string */
    public const TAB_UNDEFINED = 'default';

    /** @var string */
    public const GROUP_UNDEFINED = 'default';

    /** @var string */
    public const SECTION_UNDEFINED = 'default';

    /** @var string */
    private $tab;

    /** @var string */
    private $group;

    /** @var string */
    private $section;

    /** @var string */
    private $description;

    /** @var mixed */
    private $expected;

    /** @var mixed */
    private $actual;

    /** @var bool */
    private $check;

    /**
     * CRequirementsItem constructor.
     *
     * @param mixed $expected
     * @param mixed $actual
     * @param bool  $check
     */
    public function __construct($expected, $actual, bool $check)
    {
        if ($expected === CRequirementsManager::TYPE_EXPECTED_NOTNULL) {
            $expected = (new Translator())->tr('common-error-Must not be empty');
            $actual   = ($actual === '') ? null : $actual;
        } elseif ($expected === false) {
            $expected = 0;
        } elseif ($expected === true) {
            $expected = 1;
        }
        $this->expected = $expected;
        $this->actual   = $actual;
        $this->check    = $check;
    }

    public function getTab(): string
    {
        return $this->tab ?: self::TAB_UNDEFINED;
    }

    public function setTab(string $tab): void
    {
        $this->tab = $tab;
    }

    public function getGroup(): string
    {
        return $this->group ?: self::GROUP_UNDEFINED;
    }

    public function setGroup(string $group): void
    {
        $this->group = $group;
    }

    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    public function setDescription(string $description, bool $translate = true): void
    {
        $this->description = $translate ? (new Translator())->tr($description) : $description;
    }

    /**
     * @return mixed
     */
    public function getExpected()
    {
        return $this->expected;
    }

    /**
     * @return mixed
     */
    public function getActual()
    {
        return $this->actual;
    }

    public function isCheck(): bool
    {
        return $this->check;
    }

    public function serialize(): array
    {
        return get_object_vars($this);
    }

    public function getSection(): string
    {
        return $this->section ?: self::SECTION_UNDEFINED;
    }

    public function setSection(string $section): void
    {
        $this->section = $section;
    }
}
