<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Module\Requirements;

class CRequirementsDescription
{
    private string $description = '';

    /**
     * Get description
     */
    public function render(): string
    {
        return $this->description;
    }

    /**
     * Get description
     */
    public function hasDescription(): bool
    {
        return !!$this->description;
    }

    /**
     * Set description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * Add description
     */
    public function addDescription(string $description): void
    {
        $this->description .= "$description \n";
    }

    /**
     * Add description list
     */
    public function addDescriptionList(string $description, string $limiter = "*"): void
    {
        $this->addDescription("$limiter $description");
    }

    /**
     * Make line
     */
    public function addLine(): void
    {
        $this->addDescription("***");
    }

    /**
     * Add Title
     */
    public function addTitle(string $title, int $deep = 1): void
    {
        $limiter = str_repeat("#", $deep);

        $this->addDescription("{$limiter} {$title} {$limiter}");
    }
}
