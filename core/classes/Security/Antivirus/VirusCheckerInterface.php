<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Antivirus;

use DateTimeImmutable;
use Ox\Core\Security\Antivirus\Exception\UnavailableCheckerException;

/**
 * Check is a file is infected by a virus.
 */
interface VirusCheckerInterface
{
    /**
     * Tell if the virus checker is available.
     *
     * @return bool
     */
    public function isAvailable(): bool;

    /**
     * Tell if the file from $file_path is clean (return true) or is infected (return false).
     *
     * @param string $file_path
     *
     * @return bool
     *
     * @throws UnavailableCheckerException If the checker is misconfigured of the anti-virus is not available.
     */
    public function isFileClean(string $file_path): bool;

    /**
     * Get the date of the virus database signature or null if an error occurred.
     *
     * @return DateTimeImmutable|null
     */
    public function getLastVirusDatabaseUpdate(): ?DateTimeImmutable;

    /**
     * Refresh the virus' signatures database.
     *
     * @return bool
     */
    public function refreshDatabase(): bool;
}
