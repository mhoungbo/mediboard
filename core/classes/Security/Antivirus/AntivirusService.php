<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Antivirus;

use DateInterval;
use DateTimeImmutable;
use Exception;
use Ox\Core\Config\Conf;
use Ox\Core\Locales\Translator;
use Ox\Core\Security\Antivirus\Exception\InfectedFileException;
use Ox\Core\Security\Antivirus\Exception\UnavailableCheckerException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * Allow the manipulation of VirusCheckerInterface to detect if a group of files is infected.
 */
class AntivirusService
{
    private VirusCheckerInterface $checker;
    private Conf                  $conf;
    private Translator            $translator;

    public function __construct(
        VirusCheckerInterface $checker = null,
        Conf                  $conf = new Conf(),
        Translator            $translator = new Translator()
    ) {
        // Default checker is clamAv, might use a conf if multi antivirus are handled
        $this->checker    = $checker ?? new ClamAvChecker($conf);
        $this->conf       = $conf;
        $this->translator = $translator;
    }

    /**
     * Tell if the virus checker is available.
     *
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->checker->isAvailable();
    }

    /**
     * Check if the virus signature database is up-to-date.
     *
     * @return void
     *
     * @throws InfectedFileException If the virus signature database is too old.
     * @throws Exception
     */
    public function checkAntiVirusSignatureDatabaseDate(): void
    {
        $freshness = $this->conf->getStatic('system antivirus freshness');

        if ($freshness) {
            $last_update = $this->checker->getLastVirusDatabaseUpdate();

            if (null === $last_update) {
                return;
            }

            if ($last_update->add(DateInterval::createFromDateString("+$freshness DAYS")) < new DateTimeImmutable()) {
                throw new InfectedFileException(
                    $this->translator->tr(
                        'VirusCheckerListener-error-Signature database is too old',
                        $last_update->format('Y-m-d H:i:s')
                    ),
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }
        }
    }

    /**
     * Analyse the files and stop with an exception if one contains a virus.
     *
     * @param File[]|File $files
     *
     * @return void
     * @throws UnavailableCheckerException
     * @throws InfectedFileException A virus has been found
     */
    public function analyseFiles(array|File $files): void
    {
        if (is_array($files)) {
            foreach ($files as $file) {
                if ($file instanceof UploadedFile) {
                    $this->analyseFile($file->getPathname(), $file->getClientOriginalName());
                } else {
                    $this->analyseFile($file->getPathname(), $file->getFilename());
                }
            }
        } elseif ($files instanceof UploadedFile) {
            $this->analyseFile($files->getPathname(), $files->getClientOriginalName());
        } else {
            $this->analyseFile($files->getPathname(), $files->getFilename());
        }
    }

    /**
     * @param string $file_path The path of the file to check
     * @param string $file_name The name of the file to check (used in the error message)
     *
     * @return void
     *
     * @throws UnavailableCheckerException
     * @throws InfectedFileException A virus has been found
     */
    private function analyseFile(string $file_path, string $file_name): void
    {
        try {
            if (!$this->checker->isFileClean($file_path)) {
                throw new InfectedFileException(
                    $this->translator->tr(
                        'VirusCheckerListener-error-File is infected',
                        $file_name
                    )
                );
            }
        } catch (UnavailableCheckerException $e) {
            // Do not throw exception if host or port is not set.
            if ($e->getMessage() !== UnavailableCheckerException::MISSING_HOST_OR_PORT_MESSAGE) {
                throw $e;
            }
        }
    }
}
