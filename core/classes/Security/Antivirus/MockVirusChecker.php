<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Antivirus;

use DateTimeImmutable;

/**
 * Virus checker used for tests to avoid calling a real antivirus.
 */
class MockVirusChecker implements VirusCheckerInterface
{

    public function isAvailable(): bool
    {
        return true;
    }

    public function isFileClean(string $file_path): bool
    {
        return true;
    }

    public function getLastVirusDatabaseUpdate(): ?DateTimeImmutable
    {
        return new DateTimeImmutable();
    }

    public function refreshDatabase(): bool
    {
        return true;
    }
}
