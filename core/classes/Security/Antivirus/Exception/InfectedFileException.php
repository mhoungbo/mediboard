<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Antivirus\Exception;

use Ox\Core\CMbObject;
use Ox\Core\CMbObjectSpec;
use Ox\Core\Kernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Response;

/**
 * A file is infected. This exception is caught by the ExceptionListener to display a user friendly error message.
 */
class InfectedFileException extends HttpException
{
    public function __construct(string $message, int $status_code = Response::HTTP_UNSUPPORTED_MEDIA_TYPE)
    {
        parent::__construct($status_code, $message);
    }
}
