<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Antivirus\Exception;

use Exception;

/**
 * The antivirus checker is not available (bad configuration or service not up).
 */
class UnavailableCheckerException extends Exception
{
    public const MISSING_HOST_OR_PORT_MESSAGE = 'Missing host name or port.';
    public static function missingHostOrPort(): self
    {
        return new self(self::MISSING_HOST_OR_PORT_MESSAGE);
    }

    public static function hostUnreachable(): self
    {
        return new self('Host is not reachable.');
    }
}
