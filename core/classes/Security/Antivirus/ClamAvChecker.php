<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Antivirus;

use Appwrite\ClamAV\ClamAV;
use Appwrite\ClamAV\Network;
use Appwrite\ClamAV\Pipe;
use DateTimeImmutable;
use Exception;
use Ox\Core\Config\Conf;
use Ox\Core\Security\Antivirus\Exception\UnavailableCheckerException;
use RuntimeException;
use Throwable;

/**
 * Implementation of the VirusCheckerInterface for ClamAv antivirus.
 * @link https://www.clamav.net/
 */
class ClamAvChecker implements VirusCheckerInterface
{
    private ?string $host = null;
    private ?int    $port = null;
    private ?string $socket = null;

    private ?ClamAV $clam = null;

    public function __construct(Conf $conf)
    {
        try {
            if (!$conf->getStatic('system ClamAv active')) {
                return;
            }

            $this->host   = $conf->getStatic('system ClamAv host') ?: null;
            $this->port   = $conf->getStatic('system ClamAv port') ?: null;
            $this->socket = $conf->getStatic('system ClamAv socket') ?: null;
        } catch (Throwable) {
            $this->host = $this->port = $this->socket = null;
        }
    }

    /**
     * @inheritDoc
     */
    public function isFileClean(string $file_path): bool
    {
        return $this->connect()->fileScanInStream($file_path);
    }

    /**
     * @inheritDoc
     */
    public function isAvailable(): bool
    {
        try {
            $this->connect();

            return true;
        } catch (UnavailableCheckerException|RuntimeException) {
            return false;
        }
    }

    /**
     * @inheritDoc
     *
     * @throws Exception If an error occurred in the DateTimeImmutable's creation.
     */
    public function getLastVirusDatabaseUpdate(): ?DateTimeImmutable
    {
        try {
            $version = $this->connect()->version();
        } catch (UnavailableCheckerException|RuntimeException) {
            return null;
        }

        // ClamAV 1.1.0/26931/Wed Jun  7 07:23:57 2023
        preg_match('/^ClamAV\s+\d+\.\d+\.\d+\/\d+\/(.+)$/', $version, $results);

        return isset($results[1]) ? new DateTimeImmutable($results[1]) : null;
    }

    /**
     * @inheritDoc
     *
     * @warning This function does not wait for the database refresh to be done.
     */
    public function refreshDatabase(): bool
    {
        try {
            $this->connect()->reload();
            return true;
        } catch (UnavailableCheckerException) {
            return false;
        }
    }

    /**
     * Connect to a clamAv daemon using HTTP sockets.
     *
     * @return ClamAV
     * @throws UnavailableCheckerException
     * @throws RuntimeException
     */
    private function connect(): ClamAV
    {
        if (null === $this->clam) {
            if ($this->socket) {
                $clam_av = new Pipe($this->socket);
            } elseif ($this->port && $this->host) {
                $clam_av = new Network($this->host, (int)$this->port);
            } else {
                throw UnavailableCheckerException::missingHostOrPort();
            }

            if (!$clam_av->ping()) {
                throw UnavailableCheckerException::hostUnreachable();
            }

            $this->clam = $clam_av;
        }

        return $this->clam;
    }
}
