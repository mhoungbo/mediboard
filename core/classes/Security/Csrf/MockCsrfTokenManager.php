<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Csrf;

use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Mock of CsrfTokenManager to avoid having csrf errors in tests.
 */
class MockCsrfTokenManager implements CsrfTokenManagerInterface
{
    /** @var CsrfToken[] */
    private array $tokens = [];

    public function getToken(string $tokenId): CsrfToken
    {
        if (!isset($this->tokens[$tokenId])) {
            $this->tokens[$tokenId] = new CsrfToken($tokenId, 'test');
        }

        return $this->tokens[$tokenId];
    }

    public function refreshToken(string $tokenId): CsrfToken
    {
        return $this->getToken($tokenId);
    }

    public function removeToken(string $tokenId): ?string
    {
        if (isset($this->tokens[$tokenId])) {
            $token = $this->tokens[$tokenId];
            unset($this->tokens[$tokenId]);

            return $token->getValue();
        }

        return null;
    }

    public function isTokenValid(CsrfToken $token): bool
    {
        return true;
    }
}
