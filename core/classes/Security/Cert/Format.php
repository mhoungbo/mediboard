<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Cert;

use Ox\Core\EnumTrait;

/**
 * Certificate formats enumeration
 */
enum Format: string
{
    use EnumTrait;

    case PEM = 'pem';
    case DER = 'der';
    case PKCS12 = 'pkcs12';
}
