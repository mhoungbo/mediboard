<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Exception;

use Ox\Core\CMbException;

class CouldNotEncrypt extends CMbException
{
    public static function anErrorOccurred(string $message = null): self
    {
        if ($message) {
            return new self('CouldNotEncrypt-error-Unable to encrypt: %s', $message);
        }

        return new self('CouldNotEncrypt-error-Unable to encrypt');
    }

    public static function contextDoesNotExist(?string $class): self
    {
        return new self(
            "CouldNotEncrypt-error-Unable to encrypt because of given context '%s' does not exist",
            (string)$class
        );
    }
}
