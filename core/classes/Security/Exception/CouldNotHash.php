<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Exception;

use Ox\Core\CMbException;

class CouldNotHash extends CMbException
{
    public static function anErrorOccurred(): self
    {
        return new self('CouldNotHash-error-Unable to hash');
    }
}
