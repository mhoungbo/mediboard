<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Random;

use Ox\Core\CMbException;
use Ox\Core\CMbSecurity;

/**
 * Facade for PRNG.
 */
class Random
{
    /**
     * @param int $length
     *
     * @return string A random hexadecimal string
     * @throws CMbException
     */
    public function getString(int $length): string
    {
        return CMbSecurity::getRandomString($length, false);
    }

    /**
     * @param int $length
     *
     * @return string A random binary string
     * @throws CMbException
     */
    public function getBytes(int $length): string
    {
        return CMbSecurity::getRandomString($length, true);
    }
}
