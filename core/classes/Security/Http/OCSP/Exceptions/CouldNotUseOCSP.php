<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Http\OCSP\Exceptions;

use Ox\Core\CMbException;

/**
 * Exception for inability to use OCSP - Technical issues
 */
class CouldNotUseOCSP extends CMbException
{
    public static function curlVersionNotSupported(string $actual, string $expected): self
    {
        return new self('OCSP-failed-Curl version is not supported: %s, expected at least: %s', $actual, $expected);
    }
}
