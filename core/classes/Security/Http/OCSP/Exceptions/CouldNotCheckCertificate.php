<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Http\OCSP\Exceptions;

use Ox\Core\CMbException;

/**
 * Exception for inability to check correctly an certificate - Technical issues
 */
class CouldNotCheckCertificate extends CMbException
{
    public static function cannotBuildResponse(string $message): self
    {
        return new self('OCSP-failed-Cannot build an OCSP response: %s', $message);
    }

    public static function cannotDecodeResponse(string $message): self
    {
        return new self('OCSP-failed-Cannot decode an OCSP response: %s', $message);
    }

    public static function cannotExtractInfo(string $message): self
    {
        return new self('OCSP-failed-Unable to extract information from the certificate: %s', $message);
    }

    public static function cannotExtractAuthorityUrl(): self
    {
        return new self('OCSP-failed-Unable to extract authority url');
    }
}
