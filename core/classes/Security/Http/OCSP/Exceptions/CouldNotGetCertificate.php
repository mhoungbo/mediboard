<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Http\OCSP\Exceptions;

use Ox\Core\CMbException;

/**
 * Exception for cannot get correctly a certificate
 */
class CouldNotGetCertificate extends CMbException
{
    public static function certificateNotFoundOrPartially(): self
    {
        return new self('OCSP-failed-Certificate not found or partially found');
    }

    public static function cannotReachUrl(string $url_path): self
    {
        return new self('OCSP-failed-Cannot reach url: %s', $url_path);
    }
}
