<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Crypt;

use Ox\Core\CMbSecurity;
use Ox\Core\Security\Exception\CouldNotHash;

/**
 * Object Facade of CMbSecurity, enabling us to inject it into other classes.
 */
class Hasher
{
    /**
     * @param Hash   $alg
     * @param string $text
     *
     * @return string A hexadecimal hash
     * @throws CouldNotHash
     */
    public function hash(Hash $alg, string $text): string
    {
        $hash = CMbSecurity::hash($alg->value, $text, false);

        if ($hash === false) {
            throw CouldNotHash::anErrorOccurred();
        }

        return $hash;
    }

    /**
     * @param string $known_hash
     * @param string $user_hash
     *
     * @return bool
     */
    public function equals(string $known_hash, string $user_hash): bool
    {
        return hash_equals($known_hash, $user_hash);
    }
}
