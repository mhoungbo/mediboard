<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Crypt;

use Ox\Core\CMbSecurity;
use Ox\Core\Security\Exception\CouldNotDecrypt;
use Ox\Core\Security\Exception\CouldNotEncrypt;
use phpseclib\Crypt\RSA;

/**
 * Object Facade of CMbSecurity, enabling us to inject it into other classes.
 */
class Crypto
{
    /**
     * @param Alg         $alg
     * @param Mode        $mode
     * @param string      $key
     * @param string      $text
     * @param string|null $iv
     *
     * @return string
     * @throws CouldNotEncrypt
     */
    public function encrypt(Alg $alg, Mode $mode, string $key, string $text, ?string $iv = null): string
    {
        $crypted = CMbSecurity::encrypt($alg->value, $mode->value, $key, $text, $iv);

        if ($crypted === false) {
            throw CouldNotEncrypt::anErrorOccurred();
        }

        return $crypted;
    }

    /**
     * @param Alg         $alg
     * @param Mode        $mode
     * @param string      $key
     * @param string      $text
     * @param string|null $iv
     *
     * @return string
     * @throws CouldNotDecrypt
     */
    public function decrypt(Alg $alg, Mode $mode, string $key, string $text, ?string $iv = null): string
    {
        $clear = CMbSecurity::decrypt($alg->value, $mode->value, $key, $text, $iv);

        if ($clear === false) {
            throw CouldNotDecrypt::anErrorOccurred();
        }

        return $clear;
    }

    /**
     * @param Hash   $hash
     * @param string $cert
     * @param string $message
     * @param string $signature
     *
     * @return bool
     */
    public function verifyRSASignature(Hash $hash, string $cert, string $message, string $signature): bool
    {
        $rsa = CMbSecurity::getCipher(Alg::RSA->value);
        $rsa->setHash($hash->value);
        $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1); // phpseclib2_compat only!

        $openssl_pkey = openssl_pkey_get_public($cert);
        $public_key   = openssl_pkey_get_details($openssl_pkey);

        $rsa->loadKey($public_key['key']);

        return $rsa->verify($message, $signature);
    }
}
