<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Crypt;

use Ox\Core\EnumTrait;

/**
 * Hashing algorithms enumeration
 */
enum Hash: string
{
    use EnumTrait;

    case MD5 = 'md5';
    case SHA256 = 'sha256';
    case SHA1 = 'sha1';
}
