<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Crypt;

use Ox\Core\EnumTrait;

/**
 * Encryption algorithms enumeration
 */
enum Alg: string
{
    use EnumTrait;

    case AES = 'aes';
    case RSA = 'rsa';

    public function isSymmetric(): bool
    {
        return match ($this) {
            Alg::AES => true,
            default => false,
        };
    }

    public function needsIv(): bool
    {
        return match ($this) {
            Alg::AES => true,
            default => false,
        };
    }
}
