<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Security\Crypt;

use Ox\Core\EnumTrait;

/**
 * Encryption modes enumeration
 */
enum Mode: string
{
    use EnumTrait;

    case CTR = 'ctr';
}
