<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use Exception;
use Ox\Core\Autoload\CAutoloadAlias;
use Ox\Core\Handlers\Facades\HandlerManager;
use Ox\Core\Kernel\Exception\AppException;
use Ox\Core\Kernel\Exception\PublicEnvironmentException;
use Ox\Core\Kernel\Exception\UnavailableApplicationException;
use Ox\Core\Kernel\Routing\RequestHelperTrait;
use Ox\Core\Logger\LoggerLevels;
use Ox\Core\Module\CModule;
use Ox\Core\Mutex\CMbMutex;
use Ox\Core\Profiler\BlackfireHelper;
use Ox\Core\Redis\CRedisClient;
use Ox\Core\ResourceLoaders\CHTMLResourceLoader;
use Ox\Core\Security\Csrf\AntiCsrf;
use Ox\Core\Security\Csrf\Exceptions\CouldNotUseCsrf;
use Ox\Core\Sessions\SessionHelper;
use Ox\Core\Version\Builder;
use Ox\Core\Version\Version;
use Ox\Mediboard\Files\CFile;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\System\CConfiguration;
use Ox\Mediboard\System\CExchangeHTTPClient;
use Ox\Mediboard\System\CSourceHTTP;
use Ox\Mediboard\System\CSourceSMTP;
use PHPMailer\PHPMailer\Exception as PHPMailerException;
use Psr\Log\LoggerInterface;
use ReflectionException;
use ReflectionMethod;
use SplPriorityQueue;
use Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;
use Throwable;

/**
 * The actual application class
 * Responsibilities:
 *  - application kill
 *  - class management
 *  - file inclusion
 *  - memory and performance
 */
class CApp
{
    use RequestHelperTrait;

    // Application register shutdown
    const APP_PRIORITY = 'app';

    // Framework register shutdown
    const AUTOLOAD_PRIORITY = 'autoload';
    const EVENT_PRIORITY    = 'event';
    const MUTEX_PRIORITY    = 'mutex';
    const SESSION_PRIORITY  = 'session';
    const PEACE_PRIORITY    = 'peace';
    const ERROR_PRIORITY    = 'error';
    const CRON_PRIORITY     = 'cron';

    const FRAMEWORK_PRIORITIES = [
        self::AUTOLOAD_PRIORITY => 50,
        self::EVENT_PRIORITY    => 40,
        self::MUTEX_PRIORITY    => 30,
        self::SESSION_PRIORITY  => 20,
        self::PEACE_PRIORITY    => 10,
        self::ERROR_PRIORITY    => 0,
        self::CRON_PRIORITY     => -1,
    ];

    public const APPLICATION_LOG_FILE_PATH = '/tmp/application.log';

    static $encoding = "utf-8";

    static $is_robot = false;

    /** @var callable[] A array of callbacks to be called at the end of the query */
    static $callbacks = [];

    private CAppPerformance $performance;

    /*
     * The order of the keys is important (only the first keys
     * are displayed in the short view of the Firebug console).
     */
    /**
     * @var Chronometer Main application chronometer
     */
    static $chrono;
    /** @var bool Is application in readonly mode ? */
    static $readonly;
    /** @var int Useful to log extra bandwidth use such as FTP transfers and so on */
    static $extra_bandwidth = 0;
    /** @var SplPriorityQueue A queue of callbacks to be called on shutdown */
    private static $shutdown_callbacks = [];

    private static bool $disable_callbacks = false;

    /** @var string Current request unique identifier */
    private static $requestUID = null;
    /** @var Version */
    private static $version;
    /** @var self */
    private static $instance;

    /** @var RouterInterface Only for legacy purpose */
    private RouterInterface $router;

    /** @var LoggerInterface Only for legacy purpose */
    private LoggerInterface $logger;

    /**
     * @var string
     */
    private $root_dir;
    /**
     * @var array
     */
    private $config = [];
    /** @var bool */
    private $is_started = false;
    /** @var bool */
    private $is_stoped = false;
    /** @var bool Is the current Request from a public context|environment? */
    private $is_public = false;

    private ?SessionHelper $session_helper = null;

    /** @var bool */
    private static $in_rip = false;

    /**
     * CApp constructor
     */
    private function __construct()
    {
        $this->root_dir    = dirname(__DIR__, 2) . DIRECTORY_SEPARATOR;
        $this->performance = new CAppPerformance();
    }

    /**
     * Make application die properly
     *
     * @return never
     * @deprecated only for legacy script
     */
    public static function rip(): never
    {
        throw new CAppRipException();
    }


    /**
     * Check whether we access MB in a restricted mode
     * Useful for restricted tokens and MbHost connection
     *
     * @return bool
     */
    static function isSessionRestricted()
    {
        return CAppUI::$token_restricted ;
    }

    /**
     * Callbacks function (doProbably)
     * @return void
     */
    public static function triggerCallbacksFunc(LoggerInterface $logger)
    {
        foreach (CApp::$callbacks as $_callback) {
            try {
                call_user_func($_callback);
            } catch (Throwable $e) {
                $logger->log($e->getCode(), $e->getMessage(), ['exception' => $e]);
            }
        }
    }

    /**
     * Set time limit in seconds
     *
     * @param integer $seconds The time limit in seconds
     *
     * @return string
     */
    static function setTimeLimit($seconds)
    {
        return self::setMaxPhpConfig("max_execution_time", $seconds);
    }

    /**
     * Set a php configuration limit with a minimal value
     * if the value is < actual, the old value is used
     *
     * @param string     $config the php parameter
     * @param string|int $limit  the limit required
     *
     * @return string
     */
    static function setMaxPhpConfig($config, $limit)
    {
        $actual = CMbString::fromDecaBinary(ini_get($config));
        $new    = CMbString::fromDecaBinary($limit);

        //new value is superior => change the config
        if ($new > $actual) {
            return ini_set($config, $limit);
        }

        return ini_get($config);
    }

    /**
     * Set memory limit in megabytes
     *
     * @param string $megabytes The memory limit, suffixed with K, M, G
     *
     * @return string
     */
    static function setMemoryLimit($megabytes)
    {
        return self::setMaxPhpConfig("memory_limit", $megabytes);
    }

    /**
     * Outputs JSON data after removing the Output Buffer, with a custom mime type
     *
     * @param mixed      $data     The data to output
     * @param string     $mimeType [optional] The mime type of the data, application/json by default
     * @param bool|false $prettify [optional] Use JSON_PRETTY_PRINT
     *
     * @return never
     * @throws CAppRipException
     *
     */
    static function json($data, $mimeType = "application/json", $prettify = false): never
    {
        $json = CMbArray::toJSON($data, true, $prettify ? JSON_PRETTY_PRINT : null);

        ob_clean();
        header("Content-Type: $mimeType");
        echo $json;

        self::rip();
    }

    /**
     * @param string $module    The module name or the file path
     * @param string $file      The file of the module, or null
     * @param array  $arguments [optional] The GET arguments
     *
     * @return string The fetched content
     *
     * @throws Exception
     * @deprecated Use only in legacy, should use a sub-request for API/GUI.
     *
     * Fetch an HTML content of a module view using a HTTP GET call.
     *
     */
    public static function fetch(string $module, string $file, array $arguments = []): ?string
    {
        $arguments['m']   = $module;
        $arguments['raw'] = $file;

        $base_url = rtrim(CAppUI::conf('base_url'), '/') . '/?' . http_build_query($arguments, '', "&");

        return self::serverCall($base_url)['body'];
    }

    /**
     * Get the base application URL
     *
     * @return string The URL
     */
    static function getBaseUrl()
    {
        // Handle CLI for testing cases
        if (PHP_SAPI === 'cli') {
            return 'http://localhost/mediboard';
        }

        $scheme = "http" . (isset($_SERVER["HTTPS"]) ? "s" : "");
        $host   = $_SERVER["SERVER_NAME"];
        $port   = ($_SERVER["SERVER_PORT"] == 80) ? "" : ":{$_SERVER['SERVER_PORT']}";
        $path   = dirname($_SERVER["SCRIPT_NAME"]);

        return $scheme . "://" . $host . $port . $path;
    }

    static function getRelativeBaseHref(): string
    {
        if (!array_key_exists('REQUEST_URI', $_SERVER)) {
            return '';
        }

        $request_uri = $_SERVER['REQUEST_URI'];
        $pos         = strpos($request_uri, '/gui/');

        // Legacy
        if ($pos === false) {
            $pos = strpos($request_uri, '/token/');
        }

        // Gui token
        if ($pos === false) {
            return '';
        }

        // Must go before index.php.
        $count_remove = str_contains($request_uri, 'index.php') ? 1 : 2;

        // V2
        $request_uri = substr($request_uri, $pos);
        $count_parts = count(explode('/', $request_uri)) - $count_remove;

        return str_repeat('../', $count_parts);
    }

    /**
     * Get root directory
     *
     * @param string $path The directory to get absolute path of
     *
     * @return string
     */
    static function getAbsoluteDirectory($path = null)
    {
        $dir = __DIR__;

        // For Windows
        if (DIRECTORY_SEPARATOR === '\\') {
            $dir = str_replace('\\', '/', __DIR__);
        }

        // Do not use realpath which resolves symbolic links
        return CMbPath::canonicalize("$dir/../../$path");
    }

    /**
     * Return all storable CMbObject classes which module is installed
     *
     * @param array $classes    [optional] Restrain to given classes
     * @param bool  $short_name Return short names or full names
     *
     * @return array Class names
     */
    static function getInstalledClasses($classes = [], $short_name = false)
    {
        $instances = [];

        if (empty($classes)) {
            $classes = self::getMbClasses($instances, $short_name);
        }

        foreach ($classes as $key => $class) {
            if (isset($instances[$class])) {
                $object = $instances[$class];
            } else {
                $object = self::getClassInstance($class);
            }

            // Installed module ? Storable class ?
            if (!$object || $object->_ref_module === null || !$object->_spec->table) {
                unset($classes[$key]);
                continue;
            }
        }

        return $classes;
    }

    /**
     * Return all CStoredObject child classes
     *
     * @param array $instances  If not null, retrieve an array of all object instances
     * @param bool  $short_name Return short names or full names
     *
     * @return array Class names
     */
    static function getMbClasses(&$instances = null, $short_name = false)
    {
        $classes = self::getChildClasses(CStoredObject::class, false, $short_name);

        foreach ($classes as $key => $class) {
            // In case we removed a class and it's still in the cache
            if (!class_exists($class, true)) {
                unset($classes[$key]);
                continue;
            }

            $object = self::getClassInstance($class);

            // Instanciated class?
            if (!$object || !$object->_class) {
                unset($classes[$key]);
                continue;
            }

            $instances[$class] = $object;
        }

        return $classes;
    }

    /**
     * Return all child classes of a given class having given properties
     *
     * @param string $parent        [optional] Parent class
     * @param bool   $active_module [optional] If true, filter on active modules
     * @param bool   $short_names   [optional] If true, return short_names instead of namespaced names
     *
     * @return array Class names
     *
     * @throws Exception
     *
     * @todo Default parent class should probably be CModelObject
     */
    static function getChildClasses(
        $parent = CMbObject::class,
        $active_module = false,
        $short_names = false,
        $only_instantiable = false
    ) {
        $shortname = CClassMap::getSN($parent);
        $cache_key = 'CApp.getChildClasses-' . implode('-', [$shortname, $active_module, $short_names]);

        $cache = Cache::getCache(Cache::INNER_OUTER)->withCompressor();
        if (($value = $cache->get($cache_key)) !== null) {
            return $value;
        }

        $start     = microtime(true);
        $class_map = CClassMap::getInstance();
        $children  = $class_map->getClassChildren($parent, false, $only_instantiable);

        if ($active_module) {
            // Filter on active module
            $cmbo_children = $class_map->getClassChildren(CMbObject::class);
            if ($parent != CMbObject::class && !in_array($parent, $cmbo_children)) {
                throw new Exception("Use active_module only with parent instanceof CMbObject.");
            }

            foreach ($children as $key => &$_child_name) {
                $object = new $_child_name;
                if (!isset($object->_ref_module)) {
                    unset($children[$key]);
                }
            }
        }

        if ($short_names) {
            array_walk(
                $children,
                function (&$_child) use ($class_map) {
                    $_child = $class_map->getShortName($_child);
                }
            );
        }

        $cache->set($cache_key, $children);

        CApp::log(
            sprintf(
                'CACHE : Took %4.3f ms to build %s cache',
                (microtime(true) - $start) * 1000,
                $cache_key
            )
        );

        return $children;
    }

    /**
     * Returns an object instance
     *
     * @param string $class Class
     *
     * @return null|CMbObject
     */
    private static function getClassInstance($class)
    {
        try {
            // TODO: Compatibility PHP 5.x-7.0 & 7.1 (see http://php.net/manual/fr/migration71.incompatible.php)
            @$object = new $class;
        } catch (Throwable $t) {
            // Executed only in PHP 7.1, will not match in PHP 5.x, 7.0
            return null;
        }

        return $object;
    }

    /**
     * Tells whether a given method is overridden (= not the in first declaring class)
     *
     * @param string $class  Class from where to search
     * @param string $method Method to search for
     * @param bool   $strict If strict mode enabled, the method must be declared in given class (not in a parent)
     *
     * @return bool
     * @throws ReflectionException
     */
    static function isMethodOverridden($class, $method, $strict = false)
    {
        // Method does not exist
        if (!method_exists($class, $method)) {
            return false;
        }

        $reflection       = new ReflectionMethod($class, $method);
        $_declaring_class = $reflection->getDeclaringClass();

        // In strict mode, method must be strictly declared in given class
        if ($strict && ($_declaring_class !== $class)) {
            return false;
        }

        $_parent_class = $_declaring_class->getParentClass();

        // Declaring class has no parent
        if (!$_parent_class) {
            return false;
        }

        // Declaring class is different from its own parent class and parent class declares the method too
        if (($_declaring_class->name !== $_parent_class->name) && method_exists($_parent_class->name, $method)) {
            return true;
        }

        // Check if the parent class is the first declaring one
        return static::isMethodOverridden($_parent_class, $method, $strict);
    }

    /**
     * Group installed classes by module names
     *
     * @param array $classes Class names
     *
     * @return array Array with module names as key and class names as values
     */
    static function groupClassesByModule($classes)
    {
        $grouped = [];
        foreach ($classes as $class) {
            $object = self::getClassInstance($class);
            if (!$object) {
                continue;
            }

            if ($module = $object->_ref_module) {
                $grouped[$module->mod_name][] = $class;
            }
        }

        return $grouped;
    }

    /**
     * Try to approximate ouput buffer bandwidth consumption
     * Won't take into account output_compression
     *
     * @return int Number of bytes
     */
    static function getOuputBandwidth()
    {
        // Already flushed
        // @fixme output_compression ignored!!
        $bandwidth = CHTMLResourceLoader::$flushed_output_length;
        // Still something to be flushed ?
        // @fixme output_compression ignored!!
        $bandwidth += ob_get_length();

        return $bandwidth;
    }


    /**
     * Get polyfill request headers
     *
     * @return array
     */
    public static function getRequestHeaders($header = null)
    {
        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
        } else {
            $headers = [];
            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(
                        ' ',
                        '-',
                        ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))
                    )] = $value;
                }
            }
        }

        if ($header) {
            $headers = array_change_key_case($headers, CASE_LOWER);

            return $headers[$header] ?? null;
        }

        return $headers;
    }

    /**
     * @param string      $name
     * @param array       $parameters
     * @param int         $referenceType
     * @param string|null $base_url
     *
     * @return string
     */
    public static function generateUrl(
        string $name,
        array  $parameters = [],
        int    $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH,
        string $base_url = null
    ): string {
        $router = self::getInstance()->router;
        if ($base_url) {
            $base_url = rtrim($base_url, "/");
            $router->setContext(new RequestContext($base_url));
        }

        return $router->generate($name, $parameters, $referenceType);
    }

    public static function getRouteCollection(): RouteCollection
    {
        return self::getInstance()->router->getRouteCollection();
    }

    public function setRouter(RouterInterface $router): void
    {
        $this->router = $router;
    }

    /**
     * @param string $message Message to log
     * @param mixed  $data    Data to add to the log
     * @param int    $level   Use LoggerLevels::const
     *
     * @return bool
     * @throws Exception
     */
    static function log($message, $data = null, int $level = LoggerLevels::LEVEL_INFO): bool
    {
        // force array necessary for monolog's & parse log
        if (!is_null($data)) {
            $data = $data === false ? 0 : $data;
            $data = is_array($data) ? $data : [$data];
        } else {
            $data = [];
        }

        self::getInstance()->logger->log($level, $message, $data);

        return true;
    }

    /**
     * Set logger for application channel
     *
     * @return void
     *
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Get the current request unique ID
     *
     * @return string
     */
    static function getRequestUID()
    {
        if (self::$requestUID === null) {
            self::initRequestUID();
        }

        return self::$requestUID;
    }

    /**
     * Initializes a unique request ID to identify current request
     *
     * @return void
     */
    private static function initRequestUID()
    {
        $user_id = CAppUI::$instance->user_id ?? null;
        $uid     = uniqid("", true);

        $address = CMbServer::getRemoteAddress();
        $ip      = $address["remote"];

        // MD5 is enough as it doesn't have to be crypto proof
        self::$requestUID = md5("$user_id/$uid/$ip");
    }

    /**
     * Execute a script on all servers
     *
     * @param string   $ips_list List of IP adresses
     * @param String[] $get      Parameters GET
     * @param String[] $post     Parameters POST
     *
     * @return array
     */
    static function multipleServerCall($ips_list, $get, $post = null)
    {
        $base = $_SERVER["SCRIPT_NAME"] . "?";
        foreach ($get as $_param => $_value) {
            $base .= "$_param=$_value&";
        }
        $base = substr($base, 0, -1);

        $address = [];
        if ($ips_list) {
            $address = preg_split("/\s*,\s*/", $ips_list, -1, PREG_SPLIT_NO_EMPTY);
            $address = array_flip($address);
        }

        foreach ($address as $_ip => $_value) {
            $address[$_ip] = self::serverCall("http://$_ip$base", $post);
        }

        return $address;
    }

    /**
     * Send the request on the server
     *
     * @param String   $url  URL
     * @param String[] $post Parameters POST
     *
     * @return bool|string|array
     */
    public static function serverCall($url, $post = null)
    {
        self::getSessionHelper()->save();

        $session_name = self::getSessionHelper()->getName();
        $session_id   = self::getSessionHelper()->getId();

        $result = ["code" => "", "body" => ""];
        try {
            $source_http           = new CSourceHTTP();
            $source_http->host     = $url;
            $source_http->loggable = false;

            $http_client          = new CExchangeHTTPClient($url);
            $http_client->_source = $source_http;
            $http_client->setCookie("{$session_name}={$session_id}");
            $http_client->setUserAgent(CAppUI::conf('product_name') . CApp::getVersion()->toArray()["version"]);
            $http_client->setOption(CURLOPT_FOLLOWLOCATION, true);
            if ($post) {
                $request = $http_client->post(http_build_query($post));
            } else {
                $request = $http_client->get();
            }
        } catch (Exception $e) {
            $result["body"] = '<div class="small-error">' . $e->getMessage() . '</div>';

            return $result;
        }

        $result["code"] = $http_client->last_information["http_code"];
        $result["body"] = $request;

        return $result;
    }

    /**
     * Fetch a full page from query parameters
     *
     * @param array $query_params Query parameters
     *
     * @return string
     */
    static function fetchQuery($query_params)
    {
        $url = self::getLocalBaseUrl() . "?" . http_build_query($query_params, null, "&");

        $result = self::serverCall($url);

        return $result["body"];
    }

    /**
     * Get the local base url of the application
     *
     * @return string
     */
    static function getLocalBaseUrl()
    {
        preg_match("/https*:\/\/[^\/]+\/(.+)/u", CAppUI::conf("base_url"), $matches);

        return "http://127.0.0.1/" . $matches[1];
    }

    /**
     * Executes a callback function according in a probabilistic way
     *
     * @param integer        $denominator Denominator's probability (1 / $denominator)
     * @param callable|array $callback    Function to call
     *
     * @return void
     */
    static function doProbably($denominator, $callback)
    {
        if (PHP_SAPI === "cli") {
            return;
        }

        if (!$denominator || mt_rand(1, $denominator) !== 1) {
            return;
        }

        self::registerCallback($callback);
    }

    /**
     * Register a callback called after giving hand back to the user
     *
     * @param callable $callback The callback
     *
     * @return void
     */
    static function registerCallback($callback)
    {
        if (!is_callable($callback)) {
            return;
        }

        self::$callbacks[] = function () use ($callback) {
            call_user_func($callback);
        };
    }

    /**
     * Send email with system message source
     *
     * @param string      $subject        Mail subject
     * @param string      $body           Mail body
     * @param array       $to             Receivers IDs
     * @param array       $re             Replyto addresses
     * @param array       $bcc            Hidden copy addresses
     * @param array       $to_addresses   Specific to-address
     * @param CSourceSMTP $source         Specific SMTPSource
     * @param bool        $display_errors Display the errors, or not
     *
     * @return bool Send status
     * @throws Exception
     *
     */
    static function sendEmail(
        $subject,
        $body,
        $to = [],
        $re = [],
        $bcc = [],
        $to_addresses = [],
        CSourceSMTP $source = null,
        $display_errors = true
    ) {
        if (!$source || !$source->_id) {
            $source       = new CSourceSMTP();
            $source->name = 'system-message';
            $source->loadMatchingObject();
        }

        if (!$source->_id) {
            if ($display_errors) {
                CAppUI::displayAjaxMsg('CExchangeSource.none', CAppUI::UI_MSG_WARNING);
            }

            return false;
        }

        $ds = CSQLDataSource::get('std');

        $user       = new CMediusers();
        $recipients = [];

        if ($to) {
            if (is_array($to)) {
                $to = array_unique($to);

                $users = $user->loadList(
                    ['user_id' => $ds->prepareIn($to)]
                );

                $recipients = CMbArray::pluck($users, '_user_email');
                $recipients = array_filter($recipients);
            } else {
                $user->load($to);

                if ($user && $user->_id && $user->_user_email) {
                    $recipients[] = $user->_user_email;
                }
            }
        }

        if ($to_addresses) {
            if (is_array($to_addresses)) {
                foreach ($to_addresses as $_address) {
                    $recipients[] = $_address;
                }
            } else {
                $recipients[] = $to_addresses;
            }
        }

        $hidden_addresses = [];

        if ($bcc) {
            if (is_array($bcc)) {
                foreach ($bcc as $_bcc) {
                    $hidden_addresses[] = $_bcc;
                }
            } else {
                $hidden_addresses[] = $bcc;
            }
        }

        if (!$recipients && !$hidden_addresses) {
            return false;
        }

        try {
            $source->init();

            foreach ($recipients as $_recipient) {
                $source->addTo($_recipient);
            }

            foreach ($hidden_addresses as $_hidden_address) {
                $source->addBcc($_hidden_address);
            }

            $source->setSubject($subject);
            // HTML purification is done inside.
            $source->setBody($body);

            if ($re) {
                if (is_array($re)) {
                    foreach ($re as $_re) {
                        $source->addRe($_re);
                    }
                } else {
                    $source->addRe($re);
                }
            }

            $source->send();

            if ((!$source->asynchronous || $source->_skip_buffer) && $display_errors) {
                CAppUI::displayAjaxMsg('common-Notification sent', CAppUI::UI_MSG_OK);
            }
        } catch (PHPMailerException $e) {
            if ($display_errors) {
                CAppUI::displayAjaxMsg($e->getMessage(), CAppUI::UI_MSG_ERROR);
            }

            return false;
        } catch (CMbException $e) {
            if ($display_errors) {
                CAppUI::displayAjaxMsg($e->getMessage(), CAppUI::UI_MSG_ERROR);
            }

            return false;
        }

        return true;
    }

    /**
     * Disable all object handlers, and object cache, for heavy data handling (like data imports)
     *
     * @return void
     */
    static function disableCacheAndHandlers()
    {
        // Desactivation des object handlers
        HandlerManager::disableObjectHandlers();

        // Désactivation des traitements sur les fichiers
        CFile::$migration_enabled = false;

        // Desactivation du cache d'objets
        CStoredObject::$useObjectCache = false;
        CSQLDataSource::$log           = false;
    }

    /**
     * Tells if the application is in readonly mode
     *
     * @return bool
     */
    public static function isReadonly(): bool
    {
        if (self::$readonly === null) {
            self::$readonly = CAppUI::conf("readonly");
        }

        return (bool) self::$readonly;
    }

    /**
     * Add a custom 'proxy' HTTP header for LB appliances
     *
     */
    public static function getProxyStatus(): string
    {
        return (file_exists(rtrim(CAppUI::conf('root_dir'), '/') . '/offline')) ? 'offline' : 'online';
    }

    /**
     * Returns the registered shutdown callbacks
     *
     * @return array|SplPriorityQueue
     */
    public static function getShutdownCallbacks()
    {
        return self::$shutdown_callbacks;
    }

    /**
     * Executes the registered shutdown callbacks
     */
    public static function handleShutdownCallbacks(): void
    {
        if (self::$disable_callbacks) {
            return;
        }

        foreach (self::$shutdown_callbacks as $_callback) {
            try {
                call_user_func($_callback);
            } catch (Exception $e) {
                trigger_error($e->getMessage(), E_USER_WARNING);
            }
        }
    }


    /**
     * Get the application master key (for symmetric encryption)
     *
     * Can be generated using base64_encode(random_bytes(32))
     *
     * @return string
     * @throws CMbException
     */
    public static function getAppMasterKey(): string
    {
        $filepath = CAppUI::conf('app_master_key_filepath');

        if (!is_readable($filepath)) {
            throw new CMbException('common-error-Unable to read master key');
        }

        $key = file_get_contents($filepath);

        if ($key !== false) {
            if (strlen($key) > 32) {
                return substr($key, 0, 32);
            }

            return $key;
        }

        throw new CMbException('common-error-Unable to read master key');
    }

    /**
     * Throws an exception is in a public environment.
     *
     * @throws PublicEnvironmentException
     */
    public static function failIfPublic(): void
    {
        if (self::getInstance()->isPublic()) {
            throw new PublicEnvironmentException(
                Response::HTTP_FORBIDDEN,
                'Permission denied'
            );
        }
    }

    /**
     * @throws Exception
     */
    public function start(): void
    {
        BlackfireHelper::addMarker(__FUNCTION__);

        if ($this->is_started) {
            throw new AppException(Response::HTTP_INTERNAL_SERVER_ERROR, 'The app is already started.');
        }

        // Check build
        self::checkMandatoryFiles($this->root_dir);

        // Config
        $this->config = self::includeConfigs($this->root_dir);

        // Timezone
        date_default_timezone_set($this->config["timezone"]);

        // Register shutdown callbacks
        register_shutdown_function([__CLASS__, 'handleShutdownCallbacks']);

        // Init
        Cache::init(static::getAppIdentifier());
        CAppUI::init();
        CClassMap::init();
        CAutoloadAlias::register();

        // If an error occured too early (ParseError during the class autoloading) need to put this
        // @see vendor/symfony/error-handler/README.md
        HtmlErrorRenderer::setTemplate('templates/error.html.php');

        self::registerShutdown([CMbMutex::class, 'releaseMutexes'], self::MUTEX_PRIORITY);

        // Log queries
        if ($this->config['log_all_queries']) {
            $this->enableDataSourceLog();
        }

        // Offline mode
        if ($this->isOffline()) {
            throw UnavailableApplicationException::applicationIsDisabledBecauseOfMaintenance();
        }

        // Offline db
        if (!$this->isDatabaseAccessible()) {
            throw UnavailableApplicationException::databaseIsNotAccessible();
        }

        // Include config in DB
        CMbConfig::loadValuesFromDB();

        // Load modules
        CModule::loadModules();

        // Init shared memory, must be after DB init and Modules loading
        Cache::initDistributed();

        // set default CAppUI (legacy)
        CAppUI::$instance = CAppUI::initInstance();

        // Load locales
        CAppUI::loadCoreLocales();

        // Start chrono
        $this->startChrono();

        // Register configuration
        CConfiguration::registerAllConfiguration();

        // Handlers
        self::notify("BeforeMain");

        // Enable log
        $this->enableDataSourceLog();

        $this->is_started = true;
    }

    /**
     * @return void
     * @throws CouldNotUseCsrf
     */
    public function beforeController(): void
    {
        BlackfireHelper::addMarker('CApp::beforeController');

        // Show errors to admin
        ini_set('display_errors', (bool)CAppUI::pref('INFOSYSTEM'));

        // Load User Perms
        CPermission::loadUserPerms();

        // Load locales
        CAppUI::loadCoreLocales();

        // Init Application User
        CAppUI::initUser();

        if (!AntiCsrf::isInit()) {
            AntiCsrf::init();
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function includeConfigs(string $root_dir): array
    {
        global $dPconfig; // GLOBALS legacy compat

        // include once
        if (!empty($dPconfig)) {
            return $dPconfig;
        }

        require $root_dir . "/includes/config_all.php";

        if (!is_array($dPconfig)
            || !array_key_exists('root_dir', $dPconfig)
            || !is_dir($dPconfig['root_dir'])
        ) {
            throw new AppException(Response::HTTP_INTERNAL_SERVER_ERROR, 'The root directory is misconfigured.');
        }

        return $dPconfig;
    }

    /**
     * @throws Exception
     */
    public static function checkMandatoryFiles(string $root_dir): void
    {
        // Check if config file is present
        if (!is_file($root_dir . "/includes/config.php")) {
            throw new AppException(Response::HTTP_INTERNAL_SERVER_ERROR, 'The config file is not present.');
        }

        // Check if config_dist file is present
        if (!is_file($root_dir . "/includes/config_dist.php")) {
            throw new AppException(Response::HTTP_INTERNAL_SERVER_ERROR, 'The config definition file is not present.');
        }

        // Check if the config_all file is present
        if (!is_file($root_dir . "/includes/config_all.php")) {
            throw new AppException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                'The global config definition file is not present.'
            );
        }

        // Check if the classmap && classref files are presents
        if (
            !is_file($root_dir . "/includes/classmap.php")
            || !is_file($root_dir . "/includes/classref.php")
        ) {
            throw new AppException(Response::HTTP_INTERNAL_SERVER_ERROR, 'The class definition file is not present.');
        }
    }


    /**
     * Must be the same here and SHM::init
     * We don't use CApp because it can be called in /install
     *
     * @param string|null $root_dir
     *
     * @return array|string|string[]|null Application identifier, in a pool of servers
     * @throws Exception
     */
    public static function getAppIdentifier(?string $root_dir = null)
    {
        if ($root_dir === null || $root_dir === '') {
            $root_dir = CAppUI::conf("root_dir");
        }

        return preg_replace("/[^\w]+/", "_", $root_dir);
    }

    /**
     * Registers a callback to execute on process shutdown
     *
     * @param callable $callback The callback to execute
     * @param string   $priority A string representing the priority of the callback execution
     */
    public static function registerShutdown(callable $callback, $priority = CApp::APP_PRIORITY)
    {
        // Initialize the priority queue
        if (!self::$shutdown_callbacks instanceof SplPriorityQueue) {
            self::$shutdown_callbacks = new SplPriorityQueue();
        }

        if ($priority === static::APP_PRIORITY) {
            // Static inner caches in order to ensure that APP callbacks are executed before the framework ones
            static $app_max_priority = 1000;
            static $framework_max_priority = null;

            if (is_null($framework_max_priority)) {
                $framework_max_priority = max(static::FRAMEWORK_PRIORITIES);
            }

            // Decrements THEN returns the value
            $priority = --$app_max_priority;

            if ($priority <= $framework_max_priority) {
                trigger_error(
                    "Register shutdown APP priority must be higher than '{$framework_max_priority}', given: '{$priority}'",
                    E_USER_ERROR
                );
            }
        } elseif (array_key_exists($priority, static::FRAMEWORK_PRIORITIES)) {
            $priority = static::FRAMEWORK_PRIORITIES[$priority];
        } else {
            trigger_error("Invalid register shutdown priority: '{$priority}'", E_USER_ERROR);
        }

        self::$shutdown_callbacks->insert($callback, $priority);
    }

    /**
     * @return void
     */
    private function enableDataSourceLog(): void
    {
        CSQLDataSource::$log = true;
        CRedisClient::$log   = true;
    }

    /**
     * Check if config is in offline mode
     */
    public function isOffline()
    {
        // Offline mode
        if ($this->config["offline"]) {
            return true;
        }

        // If offline period
        if ($this->config["offline_time_start"] && $this->config["offline_time_end"]) {
            $time               = time();
            $offline_time_start = strtotime($this->config["offline_time_start"]);
            $offline_time_end   = strtotime($this->config["offline_time_end"]);

            if (($time >= $offline_time_start) && ($time <= $offline_time_end)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if database is accessible
     * @return bool
     */
    public function isDatabaseAccessible(): bool
    {
        return (bool)@CSQLDataSource::get("std");
    }

    public static function getVersion(): Version
    {
        if (!self::$version) {
            $version_file_path = dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . Builder::VERSION_FILE;
            $version_data      = is_file($version_file_path) ? include $version_file_path : [];

            self::$version = new Version($version_data);
        }

        return self::$version;
    }

    /**
     * @return void
     */
    public function startChrono(): void
    {
        self::$chrono       = new Chronometer();
        self::$chrono->main = true;
        self::$chrono->start();
    }

    /**
     * Todo: Be careful!
     *  First CApp::notify (BeforeMain) is called before $g guessing, and second one (AfterMain) after,
     *  so the BEFORE_MAIN and AFTER_MAIN events may not be called on the same handler!
     *
     * Subject notification mechanism
     *
     * TODO Implement to factorize
     *   on[Before|After][Store|Merge|Delete]()
     *   which have to get back de CPersistantObject layer
     *
     * @param string $message        The notification type
     * @param bool   $break_on_first Don't catch exceptions thrown by the handlers
     *
     * @throws Exception
     */
    public static function notify(string $message, bool $break_on_first = false): void
    {
        // Todo: PROBLEM: We do not know yet if the route is public
        if (self::getInstance()->isPublic()) {
            return;
        }

        $args = func_get_args();
        array_shift($args); // $message

        // Event Handlers
        HandlerManager::makeIndexHandlers();

        foreach (HandlerManager::getIndexHandlers() as $_handler) {
            $_trace = HandlerManager::mustLogHandler($_handler);

            try {
                if ($_trace) {
                    HandlerManager::trace('is called.', $_handler, "on$message", $args);
                }

                call_user_func_array([$_handler, "on$message"], $args);

                if ($_trace) {
                    HandlerManager::trace('has been called.', $_handler, "on$message", $args);
                }
            } catch (Exception $e) {
                if ($break_on_first) {
                    throw $e;
                } else {
                    CAppUI::setMsg($e, CAppUI::UI_MSG_ERROR);
                }
            }
        }
    }

    /**
     * Singelton CApp instance
     * @warning CApp is only instanciate on v2 (sf)
     *
     * @return CApp
     */
    public static function getInstance(): CApp
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Previously CApp::rip
     *
     * @param Request $request
     *
     * @return void
     * @throws Exception
     */
    public function stop(Request $request): void
    {
        BlackfireHelper::addMarker('CApp::stop');

        if (!$this->is_started) {
            throw new AppException(Response::HTTP_INTERNAL_SERVER_ERROR, "The app is not started.");
        }

        // Empty the message stack from remaining messages
        CAppUI::getMsg();

        // Cview checkin control
        if (!CView::$checkedin) {
            $_controller = $request->attributes->get('_controller');
            $msg         = "CView::checkin() has not been called in {$_controller}";

            if ($this->isRequestApi($request) || $this->isRequestWebTestCase($request)) {
                self::log($msg);
            } else {
                trigger_error($msg);
            }
        }
        CView::disableSlave();

        // Handler
        self::notify("AfterMain");

        // Chrono
        if (self::$chrono && self::$chrono->step > 0) {
            self::$chrono->stop();
        }

        $this->disableDataSourceLog();
        $this->is_stoped = true;
    }

    public function getDuration(): float
    {
        return self::$chrono ? round(self::$chrono->total, 3) : 0.000;
    }

    /**
     * @return void
     */
    public function disableDataSourceLog(): void
    {
        CSQLDataSource::$log = false;
        CRedisClient::$log   = false;
    }

    /**
     * Set the self::$is_public boolean to true if the public env has been enabled during the hit. Nether reset this.
     * Put the readonly flag if needed.
     *
     *
     * @param bool $public
     *
     * @return void
     */
    public function setPublic(bool $public): void
    {
        if ($public) {
            $this->is_public = true;
            $this->initPublicEnvironment();
        } else {
            $this->stopPublicEnvironment();
        }
    }

    public function isPublic(): bool
    {
        return $this->is_public;
    }

    private function initPublicEnvironment(): void
    {
        static::$readonly = true;
        CSQLDataSource::setSlaveState(true);
    }

    private function stopPublicEnvironment(): void
    {
        // We reset the readonly flag and recompute it.
        static::$readonly = null;
        static::isReadonly();
        CSQLDataSource::setSlaveState(false);
    }

    /**
     * @return bool
     */
    public function isStarted(): bool
    {
        return $this->is_started;
    }

    public function isInReadOnlyState(): bool
    {
        return static::isReadonly();
    }

    public function getPerformance(): CAppPerformance
    {
        return $this->performance;
    }

    public function getRequestIdentifier(): ?string
    {
        return static::getRequestUID();
    }

    public static function getPathApplicationLog(): string
    {
        return dirname(__DIR__, 2) . self::APPLICATION_LOG_FILE_PATH;
    }

    public function setSessionHelper(SessionHelper $session_helper): void
    {
        $this->session_helper = $session_helper;
    }

    /**
     * @deprecated
     */
    public static function getSessionHelper(): ?SessionHelper
    {
        return self::getInstance()->session_helper;
    }
}
