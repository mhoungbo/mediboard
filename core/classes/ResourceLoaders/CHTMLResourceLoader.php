<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\ResourceLoaders;

use Exception;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbPath;
use Ox\Core\CMbString;
use Ox\Mediboard\Files\CDocumentItem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * HTML resource loader utility class
 */
abstract class CHTMLResourceLoader
{
    public const MODE_SAVE_FILE = 'savefile';

    private const IGNORE_FROM_STYLE_SHEET_URL = ['bg_custom', 'materialdesignicons'];
    private const IGNORE_FROM_STYLE_SHEET     = ['materialdesignicons'];

    private const RE_IMG    = "/<img([^>]*)src\s*=\s*[\"']([^\"']+)[\"']([^>]*)(>|$)/i";
    private const RE_LINK   = "/<link[^>]*rel=\"stylesheet\"[^>]*>/i";
    private const RE_SCRIPT = "/<script[^>]*src\s*=\s*[\"']([^\"']+)[\"'][^>]*>\s*<\/script>/i";
    private const RE_A      = "/<a([^>]*)href\s*=\s*[\"']embed:([^\"']+)[\"']([^>]*)>/i";

    public static int $flushed_output_length = 0;

    private static ?string $build            = null;
    private static ?string $_stylesheet_path = null;
    /** @var bool|string */
    private static $_aio = false;
    /** @var resource */
    private static $_fp_in;
    /** @var resource */
    private static $_fp_out;
    /** @var string|null Path to save the exported files to */
    private static ?string $_path = null;

    /**
     * IE Conditional comments
     *
     * <!--[if IE]>Si IE<![endif]-->
     * <!--[if gte IE 5]> pour rserver le contenu  IE 5.0 et plus rcents (actuellement E5.5, IE6.0 et IE7.0)
     * <![endif]-->
     * <!--[if IE 5.0]> pour IE 5.0 <![endif]-->
     * <!--[if IE 5.5000]> pour IE 5.5 <![endif]-->
     * <!--[if IE 6]> pour IE 6.0 <![endif]-->
     * <!--[if gte IE 5.5000]> pour IE5.5 et suprieur <![endif]-->
     * <!--[if lt IE 6]> pour IE5.0 et IE5.5 <![endif]-->
     * <!--[if lt IE 7]> pour IE infrieur  IE7 <![endif]-->
     * <!--[if lte IE 6]> pour IE5.0, IE5.5 et IE6.0 mais pas IE7.0<![endif]-->
     *
     * @param string $content Content to put between IE conditional comments
     * @param string $cc      The conditional comment
     *
     * @return string The content inside conditional comments
     */
    public static function conditionalComments($content, $cc)
    {
        if ($cc) {
            $content = "\n<!--[if $cc]>$content\n<![endif]-->";
        }

        return $content;
    }

    /**
     * Returns the current app build, or the specified build
     *
     * @param mixed $build [optional]
     *
     * @return mixed The build
     */
    public static function getBuild($build = null)
    {
        if (!$build) {
            if (!self::$build) {
                self::$build = CHTMLResourceLoader::$build = CApp::getVersion()->getKey();
            }

            $build = self::$build;
        }

        return $build;
    }

    /**
     * Returns the hash of a string
     *
     * @param string $string The string to hash
     *
     * @return string The hashed string
     */
    public static function getHash($string)
    {
        return dechex(crc32($string));
    }

    /**
     * Gets the last change of a file
     *
     * @param string $file The file of which we want the last change
     *
     * @return int The timestamp of the last change of the file
     */
    public static function getLastChange($file)
    {
        $stat_cache = stat($file);

        return max($stat_cache[9], $stat_cache[10]);
    }

    /**
     * Returns an HTML tag
     *
     * @param string  $tagName    The tag name
     * @param array   $attributes [optional]
     * @param string  $content    [optional]
     * @param boolean $short      [optional]
     *
     * @return string The HTML source code
     */
    public static function getTag($tagName, $attributes = [], $content = "", $short = true)
    {
        $tag = "<$tagName";
        foreach ($attributes as $key => $value) {
            $tag .= " $key=\"" . CMbString::htmlEntities($value) . '"';
        }
        if ($content != "") {
            $tag .= ">$content</$tagName>";
        } else {
            if ($short) {
                $tag .= " />";
            } else {
                $tag .= "></$tagName>";
            }
        }

        return $tag;
    }

    /**
     * Get real memory peak usage or its placeholder
     *
     * @param bool $real Return the real memory peak usage or the placeholder
     *
     * @return string A formatted memory peak usage view
     */
    public static function getOutputMemory($real = false)
    {
        if ($real || !self::$_aio) {
            return CMbString::toDecaBinary(memory_get_peak_usage(true));
        }

        return "[[AIO-memory]]";
    }

    /**
     * Get the content of a file
     *
     * @param string $filename The name of the file
     *
     * @return string|null The content of the file
     */
    private static function getFileContents($filename)
    {
        if (file_exists($filename)) {
            return file_get_contents($filename);
        }

        return null;
    }

    /**
     * Get embeddable URL
     *
     * @param string $src     Original URL
     * @param string $content File content
     * @param string $subpath Subpath to store the file to
     *
     * @return string
     */
    private static function getEmbedURL($src, $content, $subpath, ?string $ext = null)
    {
        $hash    = md5($src);
        $subpath .= "/";
        CMbPath::forceDir(self::$_path . $subpath);
        file_put_contents(self::$_path . $subpath . $hash . '.' . $ext, $content);

        return "$subpath$hash.$ext";
    }

    /**
     * Replace <script> src attribute with the contents of the script
     *
     * @param array $matches The matches of the regular expression
     *
     * @return string The <script> tag
     */
    private static function replaceScriptSrc($matches)
    {
        $src      = $matches[1];
        $orig_src = $src;
        $src      = preg_replace('/(\?.*)$/', '', $src);
        $script   = self::getFileContents($src);

        if (self::$_path) {
            return '<script type="text/javascript" src="' . self::getEmbedURL(
                    $orig_src,
                    $script,
                    "script"
                ) . '"></script>';
        } else {
            return '<script type="text/javascript">' . $script . '</script>';
        }
    }

    /**
     * Replace <img> src attribute with the contents of the image (base 64 encoded)
     *
     * @param array $matches The matches of the regular expression
     *
     * @return string The <img> tag
     */
    private static function replaceImgSrc(array $matches): string
    {
        $orig_src  = $src = $matches[2];
        $is_base64 = false;
        $src       = preg_replace('/(\?.*)$/', '', $src);

        if (!$src) {
            return '';
        }

        if ($src[0] === "/") {
            $src = $_SERVER['DOCUMENT_ROOT'] . $src;
        }

        $ext = CMbPath::getExtension($src);
        if (preg_match('/data:image\/(?<ext>\w+);base64/', $src, $matches_ext)) {
            $mime      = "image/" . $matches_ext["ext"];
            $is_base64 = true;
        } else {
            $mime = CMbPath::guessMimeType(null, $ext);
        }

        $img = self::getFileContents($src);

        $matches[3] = rtrim($matches[3], " /");
        if ($is_base64) {
            $img = ' src="' . $src . '" ';
        } elseif (self::$_path) {
            $img = " src=\"" . self::getEmbedURL($orig_src, $img, "img", $ext) . "\" ";
        } else {
            $img = " src=\"data:$mime;base64," . base64_encode($img ?? '') . "\" ";
        }

        return '<img ' . $matches[1] . $img . $matches[3] . ($matches[4] ? ' />' : "");
    }

    /**
     * Replace <img> src attribute with the contents of the image (base 64 encoded)
     *
     * @param array $matches The matches of the regular expression
     *
     * @return string|null The <img> tag
     * @throws Exception
     */
    private static function replaceAEmbed($matches)
    {
        $src      = $matches[2];
        $orig_src = $src;
        $src      = preg_replace('/(\?.*)$/', '', $src);

        if (!$src) {
            return null;
        }

        if ($src[0] === "/") {
            $src = $_SERVER['DOCUMENT_ROOT'] . $src;
        }

        $sub_matches = [];
        if (preg_match("/^(\w+),(\d+)$/", $src, $sub_matches)) {
            [$all, $class, $id] = $sub_matches;

            /** @var CDocumentItem $obj */
            $obj = new $class();
            $obj->load($id);
            if (!$obj->canRead()) {
                return null;
            }

            $file = $obj->getBinaryContent();
        } else {
            $file = self::getFileContents($src);
        }

        $href = " href=\"" . self::getEmbedURL($orig_src, $file, "embed") . "\" ";

        return '<a ' . $matches[1] . $href . $matches[3] . '>';
    }

    /**
     * Get the contents of a stylesheet
     *
     * @param array $matches The matches of the regular expression
     *
     * @return string The stylesheet contents
     */
    private static function replaceStylesheetImport($matches)
    {
        return self::getFileContents(self::$_stylesheet_path . "/" . $matches[1]);
    }

    /**
     * Replace the url(...) in a stylsheet with the base 64 encoded content of the stylesheet
     *
     * @param array $matches The regex matches
     *
     * @return string The url(...) string
     */
    private static function replaceStylesheetUrl($matches): string
    {
        $src = $matches[1];

        if (strpos($src, "data:") === 0) {
            return $src;
        }

        if (self::ignoreLine($src, self::IGNORE_FROM_STYLE_SHEET_URL)) {
            return $src;
        }

        $src = preg_replace('/(\?.*)$/', '', $src);
        $ext = CMbPath::getExtension($src);
        $url = self::getFileContents(self::$_stylesheet_path . "/" . $src);

        return "url(data:image/$ext;base64," . base64_encode($url ?? '') . ")";
    }

    /**
     * Embed the stylesheets' content
     *
     * @param array $matches The matches of the regular expression
     *
     * @return string The <style> tag
     */
    private static function replaceStylesheet($matches)
    {
        $line = $matches[0];
        // If the link doesn't contains href attributes, the line is not modify
        if (!preg_match("/href\s*=\s*[\"'](?<href>[^\"']+)[\"']/", $line, $match_src)) {
            return $line;
        }
        $orig_src = $match_src["href"];

        if (self::ignoreLine($line, self::IGNORE_FROM_STYLE_SHEET)) {
            return $line;
        }

        $src        = preg_replace('/(\?.*)$/', '', $orig_src);
        $stylesheet = self::getFileContents($src);

        self::$_stylesheet_path = dirname($src);

        $media = null;
        if (preg_match("/media\s*=\s*[\"'](?<media>[^\"']+)[\"']/", $line, $match_media)) {
            $media = sprintf('media="%s"', $match_media["media"]);
        }

        // @import
        $re         = "/\@import\s+(?:url\()?[\"']?([^\"\'\)]+)[\"']?\)?;/i";
        $stylesheet = preg_replace_callback($re, [CHTMLResourceLoader::class, 'replaceStylesheetImport'], $stylesheet);

        // url(foo)
        $re         = "/url\([\"']?([^\"\'\)]+)[\"']?\)?/i";
        $stylesheet = preg_replace_callback($re, [CHTMLResourceLoader::class, 'replaceStylesheetUrl'], $stylesheet);

        if (self::$_path) {
            return '<link rel="stylesheet" href="' . self::getEmbedURL($orig_src, $stylesheet, "css") . '" '
                . $media . '>';
        } else {
            return '<style type="text/css" ' . $media . '>' . $stylesheet . '</style>';
        }
    }

    public static function setPath(string $path): void
    {
        self::$_path = rtrim($path, "/\\") . "/";
    }

    /**
     * @param string|bool $aio
     */
    public static function setAio($aio): void
    {
        self::$_aio = $aio;
    }

    /**
     * Prepare a response by replacing the assets of the current response or making a zip file in a StreamedResponse.
     *
     * @param Response    $response
     * @param string|bool $mode
     * @param bool        $ignore_scripts
     *
     * @return Response|StreamedResponse
     * @throws Exception
     */
    public static function makeAllInOneResponse(
        Response $response,
                 $mode,
        bool     $ignore_scripts,
        ?string  $memory_limit = '256M'
    ): Response {
        if ($memory_limit) {
            CApp::setMemoryLimit($memory_limit);
        }

        if ($mode === CHTMLResourceLoader::MODE_SAVE_FILE) {
            $path = CAppUI::getTmpPath("embed-" . md5(uniqid("", true)));
            self::setPath($path);
            CMbPath::forceDir($path);

            $zip_path = self::buildZipFile($path, CHTMLResourceLoader::allInOneGui($response->getContent()));

            $headers = [];
            foreach ($response->headers->getIterator() as $key => $value) {
                $headers[$key] = $value;
            }

            // Force headers for file download
            $headers['Content-Type']              = 'application/zip';
            $headers['Content-Disposition']       = 'attachment; filename="' . basename($zip_path) . '";';
            $headers['Content-Transfer-Encoding'] = 'binary';
            $headers['Content-Length']            = filesize($zip_path);

            return new StreamedResponse(
                function () use ($zip_path) {
                    readfile($zip_path);
                    CMbPath::remove($zip_path);
                },
                $response->getStatusCode(),
                $headers
            );
        } else {
            return $response instanceof StreamedResponse
                ? $response
                : $response->setContent(CHTMLResourceLoader::allInOneGui($response->getContent(), $ignore_scripts));
        }
    }

    /**
     * Build a zip file using $content and return the path to the file.
     *
     * @throws Exception
     */
    private static function buildZipFile(string $source_path, string $content): string
    {
        try {
            file_put_contents("$source_path/index.html", $content);

            $zip_path = "$source_path.zip";
            CMbPath::zip($source_path, $zip_path);
        } finally {
            if (is_dir($source_path)) {
                CMbPath::remove($source_path);
            }
        }

        return $zip_path;
    }

    /**
     * Parse $content_in line by line (using strtok) to replace assets.
     *
     * @return string The $content_in with all assets replaced
     */
    private static function allInOneGui(string $content_in, bool $ignore_scripts = false): string
    {
        $content_out = '';

        $line = strtok($content_in, PHP_EOL);
        while ($line !== false) {
            $line        = preg_replace('/<base\s+href=[\"\'][^\"\']+[\"\']>/', '', $line);
            $content_out .= self::replaceLine($line, $ignore_scripts) . PHP_EOL;
            $line        = strtok(PHP_EOL);
        }

        return $content_out;
    }

    /**
     * Replace the assets of a line by gathering them.
     *
     * @param string $line
     * @param bool   $ignore_scripts
     *
     * @return string
     */
    private static function replaceLine(string $line, bool $ignore_scripts): string
    {
        $line = preg_replace_callback(self::RE_IMG, [CHTMLResourceLoader::class, 'replaceImgSrc'], $line);
        $line = preg_replace_callback(self::RE_LINK, [CHTMLResourceLoader::class, 'replaceStylesheet'], $line);

        if (!$ignore_scripts) {
            $line = preg_replace_callback(self::RE_SCRIPT, [CHTMLResourceLoader::class, 'replaceScriptSrc'], $line);
        }

        if (self::$_path) {
            $line = preg_replace_callback(self::RE_A, [CHTMLResourceLoader::class, 'replaceAEmbed'], $line);
        }

        return $line;
    }

    private static function ignoreLine(string $line, array $searches): bool
    {
        foreach ($searches as $search) {
            if (str_contains($line, $search)) {
                return true;
            }
        }

        return false;
    }
}
