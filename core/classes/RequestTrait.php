<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

trait RequestTrait
{
    // Constants are not allowed in Traits (only from PHP8.2)
    private static string $group_by_regexp = '^((`?)((?<=`)[\D\s]+(?=`)|\w+)\2\.)?(`?)\w+\4$';

    private static string $order_by_regexp = '^\-?((`?)((?<=`)[\D\s]+(?=`)|\w+)\2\.)?((`?)\w+\5)(\s+(ASC|DESC))?$';

    private static string $limit_regexp = '^\d+((\s*\,\s*\d+)|\s+OFFSET\s+\d+)?$';

    /**
     * @throws CRequestException
     */
    protected function checkGroupBy(string $group_by): void
    {
        $_group_by_array = explode(',', $group_by);

        foreach ($_group_by_array as $_group) {
            if (!preg_match('/' . self::$group_by_regexp . '/i', trim($_group))) {
                throw CRequestException::nonAllowedGroupBy($group_by);
            }
        }
    }

    /**
     * @throws CRequestException
     */
    protected function checkOrderBy(string $order_by): void
    {
        $_order_by_array = explode(',', $order_by);

        foreach ($_order_by_array as $_order) {
            if (!preg_match('/' . self::$order_by_regexp . '/i', trim($_order))) {
                throw CRequestException::nonAllowedOrderBy($order_by);
            }
        }
    }

    /**
     * @throws CRequestException
     */
    protected function checkLimit(string $limit): void
    {
        if (!preg_match('/' . self::$limit_regexp . '/i', trim($limit))) {
            throw CRequestException::nonAllowedLimit($limit);
        }
    }
}
