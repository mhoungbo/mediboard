<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Twig;

use Ox\Core\CApp;
use Ox\Core\CMbString;
use Ox\Core\EntryPoint;
use SqlFormatter;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * This class contains the needed functions in order to do the query highlighting
 */
class OxExtension extends AbstractExtension
{
    /**
     * Define our functions
     *
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('ox_format_sql', [$this, 'formatQuery']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('ox_entry_point', [$this, 'createEntryPoint'], ["is_safe" => ["html"]]),
            new TwigFunction('ox_relative_base_href', [$this, 'getRelativeBaseHref']),
        ];
    }

    public function getRelativeBaseHref(): string
    {
        return CApp::getRelativeBaseHref();
    }

    public function createEntryPoint(EntryPoint $entry_point, array $tokens)
    {
        $html = '';
        if ($script = $entry_point->getScriptName()) {
            $html = $this->loadVueScript($script);
        }

        $html .= sprintf('<div class="OxEntrypoint" id="%s"', $entry_point->getId());

        if ($data = $entry_point->getData()) {
            foreach ($data as $key => $datum) {
                $html .= sprintf(
                    " %svue-%s='%s'",
                    is_string($datum) || $datum === null ? '' : ':',
                    $key,
                    is_string($datum) || $datum === null ? $datum : json_encode($datum, JSON_HEX_APOS | JSON_HEX_QUOT)
                );
            }
        }

        if ($links = $entry_point->getLinks()) {
            $html .= sprintf(" :vue-links='%s'", json_encode($links, JSON_HEX_APOS | JSON_HEX_QUOT));
        }

        if ($configs = $entry_point->getConfigs()) {
            $html .= sprintf(
                " :vue-configs='%s'",
                json_encode(mb_convert_encoding($configs, 'UTF-8', 'ISO-8859-1'), JSON_HEX_APOS | JSON_HEX_QUOT)
            );
        }

        if ($prefs = $entry_point->getPrefs()) {
            $html .= sprintf(
                " :vue-prefs='%s'",
                json_encode(mb_convert_encoding($prefs, 'UTF-8', 'ISO-8859-1'), JSON_HEX_APOS | JSON_HEX_QUOT)
            );
        }

        if ($meta = $entry_point->getMeta()) {
            $html .= sprintf(
                " :vue-meta='%s'",
                json_encode(mb_convert_encoding($meta, 'UTF-8', 'ISO-8859-1'), JSON_HEX_APOS | JSON_HEX_QUOT)
            );
        }

        if ($locales = $entry_point->getLocales()) {
            $html .= sprintf(
                " :vue-locales='%s'",
                json_encode(mb_convert_encoding($locales, 'UTF-8', 'ISO-8859-1'), JSON_HEX_APOS | JSON_HEX_QUOT)
            );
        }

        if (!empty($tokens)) {
            $html .= sprintf(" :vue-tokens='%s'", json_encode($tokens, JSON_HEX_APOS | JSON_HEX_QUOT));
        }

        $html .= "></div>";

        return $html;
    }

    public function formatQuery($sql, $highlightOnly = false)
    {
        return '<div class="SqlFormater">' . SqlFormatter::format(CMbString::utf8Encode($sql)) . '</div>';
    }

    /**
     * Get the name of the extension
     *
     * @return string
     */
    public function getName()
    {
        return 'twig_extension';
    }

    private function loadVueScript(string $script_name): string
    {
        $path = "javascript/dist/js/$script_name.js";

        return "<script src='$path' type='text/javascript' defer></script>";
    }
}
