<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

/**
 * Math utilities
 */
class CMbMath
{
    /**
     * Round numbers using significant digits
     *
     * @param float $number The number to round
     * @param int   $n      The significant digits to keep
     *
     * @return float
     */
    static function roundSig($number, $n = 4)
    {
        if ($number == 0) {
            return 0;
        }

        $d     = ceil(log10(abs($number)));
        $power = $n - $d;

        return round($number, $power);
    }

    /**
     * Get the min value from arguments or the first argument if it's an array, ignoring null and false
     *
     * @param mixed $values
     *
     * @return mixed
     */
    static function min($values)
    {
        $args = func_get_args();
        if (!is_array($values) || func_num_args() > 1) {
            $values = $args;
        }

        $values = array_filter(
            $values,
            function ($v) {
                return $v !== null && $v !== false;
            }
        );

        if (count($values) === 0) {
            return null;
        }

        return min($values);
    }

    /**
     * Get the max value from arguments or the first argument if it's an array, ignoring null and false
     *
     * @param mixed $values
     *
     * @return mixed
     */
    static function max($values)
    {
        $args = func_get_args();
        if (!is_array($values) || func_num_args() > 1) {
            $values = $args;
        }

        $values = array_filter(
            $values,
            function ($v) {
                return $v !== null && $v !== false;
            }
        );

        if (count($values) === 0) {
            return null;
        }

        return max($values);
    }

    /**
     * Evaluates mathematical expressions, like "a+b", or "round(1 / c)", etc
     *
     * @param string $expression Expression to evaluate
     * @param array  $variables  Expression variables
     *
     * @return number
     */
    static function evaluate($expression, $variables = [])
    {
        $customOpsVars = [
            "Min" => CMbDT::SECS_PER_MINUTE,
            "H"   => CMbDT::SECS_PER_HOUR,
            "J"   => CMbDT::SECS_PER_DAY,
            "Sem" => CMbDT::SECS_PER_WEEK,
            "M"   => CMbDT::SECS_PER_MONTH,
            "A"   => CMbDT::SECS_PER_YEAR,
        ];

        $customOps = CMbArray::get(self::getCustomOps(), 0);

        $executor = new \NXP\MathExecutor();

        // Time ops
        foreach ($customOpsVars as $_key => $_seconds) {
            $divisor = $_seconds;

            $executor->addFunction(
                $_key,
                function ($ms) use ($divisor) {
                    $timestampLimit = 50000;
                    $divisor        *= 1000;

                    // Absolute timestamp
                    if (abs($ms) > $timestampLimit) {
                        return ceil($ms / $divisor);
                    }

                    // Relative timestamp
                    return ceil($ms * $divisor);
                }
            );
        }

        // Math ops
        foreach ($customOps as $_func) {
            $executor->addFunction(
                $_func,
                function ($arg) use ($_func) {
                    return $_func($arg);
                }
            );
        }

        // add Math pow
        $executor->addFunction("pow", "pow", 2);

        $executor->setVars($variables);

        return $executor->execute($expression);
    }

    /**
     * Get name of custom functions managed by executor
     * Index of key array is for number of parameters
     *
     * @return array
     */
    static function getCustomOps()
    {
        return [
            ["sqrt", "log", "exp", "abs", "ceil", "floor", "round"],
            ["pow"],
        ];
    }

    /**
     * @param int $base
     * @param int $nbr
     *
     * @return bool
     */
    public static function isValidExponential(int $base, int $nbr): bool
    {
        $result = $nbr / $base;
        if ($result === 1) {
            return true;
        }

        if (is_int($result)) {
            return static::isValidExponential($base, $result);
        }

        return false;
    }
}
