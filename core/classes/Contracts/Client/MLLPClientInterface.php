<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Contracts\Client;

interface MLLPClientInterface extends ClientInterface
{
    /**
     * Send data
     *
     * @param string|null $data data to send
     *                          the nullable is here to keep backward compatibility with $source->setData()
     *
     * @return string
     */
    public function send(string $data = null): string;

    /**
     * Receive data
     *
     * @return string
     */
    public function receive(): string;
}
