<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Contracts\Client;

use Ox\Core\CMbException;

interface FileClientInterface extends ClientInterface
{
    /**
     * Send content of file to the remote $destination_basename
     * The content should be set in the source through the function setData(content)
     *
     * @param string $destination_basename The full path destination of file
     */
    public function send(string $destination_basename): bool;

    /**
     * Return list of fullpath of files which should be in the directory given
     *
     * @param string $path_directory
     *
     * @return array
     */
    public function receive(string $path_directory): array;

    /**
     * Get the size of file
     *
     * @param string $file_name the full path to the file
     *
     * @return int
     */
    public function getSize(string $file_name): int;

    /**
     * Change the name of a file
     * Be careful, the destination of the new name of file should not changed, only the name should be changed
     * The $new_name could be the full path or just a new name.
     * If the fullpath is given, it will be identic with $file_path
     *
     * @param string $file_path
     * @param string $new_name
     *
     * @return bool
     */
    public function renameFile(string $file_path, string $new_name): bool;

    /**
     * Create the directory.
     * The full path to the directory should be given.
     * Il will create recursively directories
     *
     * @param string $directory_name
     *
     * @return bool
     * @throws CMbException
     */
    public function createDirectory(string $directory_name): bool;

    /**
     * Get the full path to the directory
     * The $directory could be the full path to directory to test if exists
     * Or it could be a local directory to get the full path to this directory if this directory exits
     *
     * @param string $directory
     *
     * @return string
     * @throws CMbException
     */
    public function getDirectory(string $directory): string;

    /**
     * List filenames in the target directory
     * Directories will be ignored
     *
     * @param string $directory
     * @param bool   $information
     *
     * @return array
     * @throws CMbException
     */
    public function getListFiles(string $directory, bool $information = false): array;

    /**
     * List all files in the target directory with informations
     *
     * "type" : type of element (should be always f for file)
     * "user" : the file owner
     * "size" : the size of file
     * "date" : the date of file
     * "name" : The name of file
     * "relativeDate" : the relative date between file and now
     *
     * @param string $current_directory
     *
     * @return array
     * @throws CMbException
     */
    public function getListFilesDetails(string $current_directory): array;

    /**
     * List
     *
     * @param string $current_directory
     *
     * @return array
     * @throws CMbException
     */
    public function getListDirectory(string $current_directory): array;

    /**
     *
     *
     * @param string $source_file
     * @param string $file_name
     *
     * @return bool
     * @throws CMbException
     */
    public function addFile(string $source_file, string $file_name): bool;

    /**
     * Delete the target File
     * The $path parameter should be the fullpath of file
     *
     * @param string $path
     *
     * @return bool
     * @throws CMbException
     */
    public function delFile(string $path): bool;

    /**
     * Remove the target directory.
     * If $purge is false, the target directory should be empty.
     * If $purge is true, all files and directories inside this directory should be deleted
     *
     * @param string $dir_path
     * @param bool   $purge
     *
     * @return bool
     */
    public function removeDirectory(string $dir_path, bool $purge = false): bool;

    /**
     * @return string|null
     */
    public function getError(): ?string;

    /**
     * Get the content of taget file
     * The parameter $dest could be given to put the content in the local destination
     * If $dest is not given, a temporary file will be created
     *
     * @param string      $path
     * @param string|null $dest
     *
     * @return string|null
     */
    public function getData(string $path, ?string $dest = null): ?string;
}
