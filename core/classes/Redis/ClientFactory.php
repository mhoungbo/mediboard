<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Redis;

use Exception;
use Ox\Mediboard\System\CRedisServer;

/**
 * Factory enabling us to autowire client into services (instead of controllers) whereas configurations are not already
 * set.
 */
class ClientFactory
{
    /**
     * @return CRedisClient
     * @throws Exception
     */
    public function getClient(): CRedisClient
    {
        return CRedisServer::getClient();
    }
}
