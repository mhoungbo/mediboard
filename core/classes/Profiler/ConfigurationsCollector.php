<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Profiler;

use Exception;
use Ox\Mediboard\Developpement\Configs\ConfigurationsChecker;
use Symfony\Bundle\FrameworkBundle\DataCollector\AbstractDataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * This class collect requirements of : configurations, preferences...
 */
class ConfigurationsCollector extends AbstractDataCollector
{
    protected ConfigurationsChecker $configurations_checker;

    /**
     * @throws Exception
     */
    public function collect(Request $request, Response $response, Throwable $exception = null): void
    {
        $this->configurations_checker = new ConfigurationsChecker();

        $this->collectConfigurations()
             ->collectPreferences()
             ->collectInfos();
    }

    public function getName(): string
    {
        return 'app.configurations_collector';
    }

    public static function getTemplate(): ?string
    {
        return 'data_collector/configurations.html.twig';
    }

    /**
     * @throws Exception
     */
    private function collectConfigurations(): self
    {
        $this->data['configs'] = $this->configurations_checker->compareAndReturnConfigs();

        return $this;
    }

    /**
     * @throws Exception
     */
    private function collectPreferences(): self
    {
        $this->data['prefs'] = $this->configurations_checker->compareAndReturnPrefs();

        return $this;
    }

    /**
     * @throws Exception
     */
    private function collectInfos(): self
    {
        $this->data['infos'] = [
            'url_wiki'   => "https://gitlab.com/openxtrem/mediboard/-/wikis/home",
            'url_gitlab' => "https://gitlab.com/openxtrem",
        ];

        return $this;
    }

    public function getConfigs(): array
    {
        return $this->data['configs'];
    }

    public function getPrefs(): array
    {
        return $this->data['prefs'];
    }

    public function getInfos(): array
    {
        return $this->data['infos'];
    }
}
