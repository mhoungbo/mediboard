<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Profiler;

use Ox\Core\Chronometer;
use Ox\Core\CSmartyMB;
use Ox\Core\EntryPoint;
use Symfony\Bundle\FrameworkBundle\DataCollector\AbstractDataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * This class collect smarty datas (times, tpl)
 */
class SmartyCollector extends AbstractDataCollector
{

    private ?Chronometer $smarty_chrono = null;

    public function collect(Request $request, Response $response, \Throwable $exception = null)
    {
        $this->collectStats()
            ->collectTemplates()
            ->collectEntryPoints();
    }

    protected function collectTemplates(): SmartyCollector
    {
        $this->data['templates'] = [];
        if ($this->smarty_chrono) {
            foreach ($this->smarty_chrono->report as $template => $chrono) {
                [$fetch, $name] = explode('_', $template, 2);

                $includes = [];
                $level    = 0;
                foreach (CSmartyMB::$includes[$template] as $trace) {
                    if (str_starts_with($trace, '<start>')) {
                        $level++;
                        $margin = $level * 16;
                        str_replace('<start>', '', $trace);
                        $includes[] = '<span style="margin: ' . $margin . 'px">&rdsh;&nbsp;' . $trace . '</span>';
                    } else {
                        $level--;
                    }
                }

                $this->data['templates'][] = [
                    'fetch'    => $fetch,
                    'name'     => $name,
                    'includes' => $includes,
                    'time'     => round($chrono->total * 1000, 2),
                ];
            }
        }

        return $this;
    }

    protected function collectStats(): SmartyCollector
    {
        $this->smarty_chrono = CSmartyMB::$chrono;
        $count_templates     = 0;
        $total_time          = 0;

        if ($this->smarty_chrono) {
            $count_includes = 0;
            foreach (CSmartyMB::$includes as $includes) {
                $count_includes += count($includes) / 2; // start & stop
            }
            $count_templates = $this->smarty_chrono->nbSteps + $count_includes;
            $total_time      = round($this->smarty_chrono->total * 1000, 2);
        }

        $this->data['stats'] = [
            'count_instances'    => CSmartyMB::$count_instances,
            'count_templates'    => $count_templates,
            'count_entry_points' => count(CSmartyMB::$entry_points),
            'total_time'         => $total_time,
        ];


        return $this;
    }

    protected function collectEntryPoints(): SmartyCollector
    {
        $this->data['entry_points'] = CSmartyMB::$entry_points;


        // build all entries dynamicaly
        if (!empty($this->data['entry_points'])) {
            $all_entries = $this->getAllEntries();

            /** @var EntryPoint $entry */
            foreach ($this->data['entry_points'] as &$entry) {
                $script_name = $entry['script_name'];
                if ($entry['script_name'] !== null && isset($all_entries[$script_name])) {
                    $entry['script_name'] = $all_entries[$script_name];
                }
            }
        }

        return $this;
    }

    private function getAllEntries(): array
    {
        $all_entries = [];
        foreach (glob(dirname(__DIR__, 3) . '/modules/*/vue/entry.json') as $entry_json) {
            $_entries = (array)json_decode(file_get_contents($entry_json), true);
            foreach ($_entries as $name => $entry) {
                $all_entries[$name] = $entry['entry'];
            }
        }

        return $all_entries;
    }

    public function getName(): string
    {
        return 'app.smarty_collector';
    }

    public static function getTemplate(): ?string
    {
        return 'data_collector/smarty.html.twig';
    }

    public function getStats(): array
    {
        return $this->data['stats'];
    }

    public function getTemplates(): array
    {
        return $this->data['templates'];
    }

    public function getEntryPoints(): array
    {
        return $this->data['entry_points'];
    }

}
