<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Mutex;

/**
 * Factory enabling us to autowire mutex into services (instead of controllers) whereas configurations are not already
 * set.
 */
class MutexFactory
{
    public function getMutex(string $lock_name): IMbMutex
    {
        return new CMbMutex($lock_name);
    }

    public function getFileMutex(string $lock_name): IMbMutex
    {
        return new CMbFileMutex($lock_name);
    }
}
