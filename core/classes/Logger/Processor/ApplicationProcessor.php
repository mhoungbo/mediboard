<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Logger\Processor;

use Monolog\Processor\ProcessorInterface;
use Ox\Core\CApp;
use Ox\Core\CAppUI;
use Ox\Core\CMbString;
use Ox\Core\Logger\ContextEncoder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Add extra data to log (user, ssid ...) and encode given context
 */
class ApplicationProcessor implements ProcessorInterface
{
    private ?Request $request;

    public function __construct(RequestStack $request_stack = null)
    {
        $this->request = $request_stack?->getCurrentRequest();
    }

    public function __invoke(array $record): array
    {
        // Session may not be started yet
        $session_id = $this->request?->getSession()->getId() ?? '';

        if (isset($record['context'])) {
            $record['context'] = (new ContextEncoder($record['context']))->encode();
        }
        $record['extra']['user_id']     = CAppUI::$instance->_ref_user?->user_id;
        $record['extra']['server_ip']   = $_SERVER["SERVER_ADDR"] ?? null;
        $record['extra']['session_id']  = CMbString::truncate($session_id, 15);
        $record['extra']['request_uid'] = CApp::getRequestUID();
        $record['extra']['request_uri'] = $_SERVER['REQUEST_URI'] ?? 'cli';

        return $record;
    }
}
