<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use Exception;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Core\Exceptions\AppRedirectionException;
use Ox\Core\Kernel\Exception\AccessDeniedException;
use Ox\Core\Kernel\Exception\ControllerStoppedException;
use Ox\Core\Kernel\Exception\ObjectNotFoundException;
use Ox\Core\Logger\LoggerLevels;
use Ox\Core\Module\CModule;
use Ox\Core\Mutex\CMbFileMutex;
use Ox\Mediboard\Admin\CUser;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Medimail\CMedimailAccount;
use Ox\Mediboard\Mediusers\CFunctions;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Messagerie\CUserMessageDest;
use Ox\Mediboard\MondialSante\CMondialSanteAccount;
use Ox\Mediboard\Mssante\CMSSanteUserAccount;
use Ox\Mediboard\System\CConfiguration;
use Ox\Mediboard\System\ConfigurationManager;
use Ox\Mediboard\System\CPreferences;
use Ox\Mediboard\System\CSourcePOP;
use Ox\Mediboard\System\CTranslationOverwrite;
use Ox\Mediboard\System\CUserAgent;
use Ox\Mediboard\System\CUserAuthentication;
use Psr\SimpleCache\InvalidArgumentException;
use TypeError;

/**
 * The Application UI weird Class
 * (Target) Responsibilities:
 *  - messaging
 *  - localization
 *  - user preferences
 *  - system configuration
 */
final class CAppUI
{
    const LOCALES_PREFIX    = ".__prefixes__"; // Prefixed by "." to be first in the list
    const LOCALES_OVERWRITE = "__overwrite__"; // Prefixed by "." to be first in the list

    const MESSAGERIE_ACCOUNTS_CACHE = 'CAppUI.getMessagerieAccounts';

    const MEDIBOARD_EXT_THEME = "mediboard_ext";

    public const UI_MSG_OK      = 1;
    public const UI_MSG_ALERT   = 2;
    public const UI_MSG_WARNING = 3;
    public const UI_MSG_ERROR   = 4;

    private const MULTI_TAB_MSG_READ = 'multi-tab-msg-read';

    /** @var CAppUI */
    static $instance;

    /** @var bool Tells if we must refresh session */
    static $session_no_revive = false;

    /** @var bool Is it a token based session ? */
    static $token_session = false;

    /** @var int Token expiration ISO datetime */
    static $token_expiration;

    /** @var bool Token restricted: session will be closed right after page display */
    static $token_restricted;

    /** @var integer Session CViewAccessToken ID */
    static $token_id;

    /* --- <Localization> --- */
    /** @var bool Localization skipped if false */
    static $localize = true;

    /** @var string Current language */
    static $lang;

    /** @var string Language alert mask */
    static $locale_mask = "";

    /** @var string[] List of unlocalized strings */
    static $unlocalized = [];

    /** @var string Mask of strings to ignore for the localization warning */
    static $localize_ignore = '/(^CExObject|^CMbObject\.dummy)/';

    /** @var array[] Array of locales, groupes by prefix */
    static $locales = [];

    /** @var bool[] List of flags indicating loaded locales groups */
    protected static $locales_loaded = [];

    /** @var bool Echo inline stepMessage & stepAjax */
    private static $echo_step = false;

    /** @var array */
    public static $locale_info = [];

    /* --- </Localization> --- */

    /* -- SESSION VARS BELOW -- */
    public $user_id = 0;
    public $_is_intranet;
    public $ip;
    public $proxy;

    // DEPRECATED Use CAppUI::$instance->_ref_user instead
    // @todo Remove all calls to these variables
    public $user_first_name;
    public $user_last_name;
    public $user_email;

    // Do not remove this, used in order to identify a CPatientUser connection (CUser without a CMediusers)
    public $user_type;

    public $user_group;
    public $user_prev_login;
    public $user_last_login;
    public $user_remote;

    // @todo Remove many calls in templates
    // @todo Handle the CMediusers::get() and CUser::get() cases
    /** @var CMediusers */
    public $_ref_user;
    // END DEPRECATED

    // Global collections
    public $messages   = [];
    public $scripts    = [];
    public $user_prefs = [];

    /** @var string Authentication method used */
    public $auth_method;

    /** @var CUserAgent User agent information */
    public $ua;

    public $_renew_ldap_pwd;

    /** @var bool Is the connected user LDAP related? */
    public $_is_ldap_linked;

    /** @var CUserAuthentication $_ref_last_auth */
    public $_ref_last_auth;

    /**
     * Init by front ctrl
     *
     * @return void
     */
    static function init()
    {
        // Message No Constants
        if (!defined('UI_MSG_OK')) {
            define("UI_MSG_OK", 1);
            define("UI_MSG_ALERT", 2);
            define("UI_MSG_WARNING", 3);
            define("UI_MSG_ERROR", 4);
        }

        // choose to alert for missing translation or not
        $locale_alert        = CAppUI::conf("locale_alert");
        CAppUI::$locale_mask = CAppUI::conf("locale_warn") ? "$locale_alert%s$locale_alert" : null;
    }

    /**
     * Initializes the CAppUI singleton
     *
     * @return CAppUI The singleton
     */
    static function initInstance()
    {
        return self::$instance = new CAppUI();
    }

    /**
     * Executed prior to any serialization of the object
     *
     * @return array Array of field names to be serialized
     */
    function __sleep()
    {
        $vars = get_object_vars($this);
        unset($vars["_ref_user"]);

        return array_keys($vars);
    }

    /**
     * Used to include a php file from the module directory
     *
     * @param string $name [optional] The module name
     * @param string $file [optional] The name of the file to include
     *
     * @return mixed Job-done bool or file return value
     * @todo Migrate to CApp
     *
     */
    static function requireModuleFile($name = null, $file = null)
    {
        if ($name && $root = self::conf("root_dir")) {
            $filename = $file ? $file : $name;

            // dP prefix hack
            if (!is_dir(__DIR__ . "/../../modules/$name") && strpos($name, "dP") !== 0) {
                $name = "dP$name";
            }

            return include_once "$root/modules/$name/$filename.php";
        }

        return false;
    }

    /**
     * Used to store information in tmp directory
     *
     * @param string $subpath in tmp directory
     *
     * @return string The path to the include file
     *
     */
    static function getTmpPath($subpath)
    {
        if ($subpath && $root = self::conf("root_dir")) {
            return "$root/tmp/$subpath";
        }

        return false;
    }

    /**
     * Find directories in a root subpath, excluding source control files
     *
     * @param string $subpath The subpath to read
     *
     * @return array A named array of the directories (the key and value are identical)
     */
    static function readDirs($subpath)
    {
        $root_dir = self::conf("root_dir");
        $dirs     = [];
        $d        = dir("$root_dir/$subpath");

        while (false !== ($name = $d->read())) {
            if (
                $name !== "."
                && $name !== ".."
                && $name !== "CVS"
                && is_dir("$root_dir/$subpath/$name")
            ) {
                $dirs[$name] = $name;
            }
        }

        $d->close();

        return $dirs;
    }

    /**
     * Find files in a roo subpath, excluding a specific filter
     *
     * @param string $subpath The path to read
     * @param string $filter  Filter as a regular expression
     *
     * @return array A named array of the files (the key and value are identical)
     */
    static function readFiles($subpath, $filter = ".")
    {
        $files = [];

        if ($handle = opendir($subpath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file !== "."
                    && $file !== ".."
                    && preg_match("/$filter/", $file)
                ) {
                    $files[$file] = $file;
                }
            }
            closedir($handle);
        }

        return $files;
    }

    /**
     * Utility function to check whether a file name is "safe"
     * Prevents from access to relative directories (eg ../../deadlyfile.php)
     *
     * @param string|null $file The file name
     *
     * @return string Sanitized file name
     */
    static function checkFileName($file)
    {
        // define bad characters and their replacement
        $bad_chars   = ";.\\";
        $bad_replace = "..."; // Needs the same number of chars as $bad_chars

        // check whether the filename contained bad characters
        if (strpos(strtr($file ?? '', $bad_chars, $bad_replace), ".") !== false) {
            self::accessDenied();
        }

        return $file;
    }

    /**
     * Get current user agent
     *
     * @return CUserAgent
     */
    static function getUA()
    {
        if (!CAppUI::$instance->ua) {
            CAppUI::$instance->ua = CUserAgent::create(false);
        }

        return CAppUI::$instance->ua;
    }

    /**
     * Redirects the browser to a new page.
     *
     * @param string $params HTTP GET paramaters to apply
     *
     * @return void
     * @throws AppRedirectionException
     * @deprecated Should only be used by legacy code.
     *
     */
    public static function redirect(string $params = "")
    {
        if (CValue::get("dontRedirect")) {
            return;
        }

        if (CValue::get("dialog")) {
            $params .= "&dialog=1";
        }

        if (CValue::get("ajax")) {
            $params .= "&ajax=1";
        }

        if (CValue::get("suppressHeaders")) {
            $params .= "&suppressHeaders=1";
        }

        $query = ($params && $params[0] !== "#" ? "?$params" : "");

        throw new AppRedirectionException($query);
    }

    /**
     * Returns the CSS class corresponding to a message type
     *
     * @param int $type Message type as a UI constant
     *
     * @return string The CSS class
     */
    static function getErrorClass($type = self::UI_MSG_OK)
    {
        switch ($type) {
            case self::UI_MSG_ERROR:
                return "error";

            case self::UI_MSG_WARNING:
                return "warning";

            default:
            case self::UI_MSG_OK:
            case self::UI_MSG_ALERT:
                return "info";
        }
    }

    /**
     * Add message to the the system UI
     *
     * @param string $msg  The internationalized message
     * @param int    $type [optional] Message type as a UI constant
     * @param mixed  $_    [optional] Any number of printf-like parameters to be applied
     *
     * @return void
     *
     */
    static function setMsg($msg, $type = self::UI_MSG_OK, $_ = null)
    {
        $args = func_get_args();
        $msg  = CAppUI::tr($msg, array_slice($args, 2));

        if (!isset(self::$instance->messages[$type][$msg])) {
            self::$instance->messages[$type][$msg] = 0;
        }

        self::$instance->messages[$type][$msg]++;
    }

    /**
     * Add message to the the system UI from Ajax call
     *
     * @param string $msg  The internationalized message
     * @param int    $type [optional] Message type as a UI constant
     * @param mixed  $_    [optional] Any number of printf-like parameters to be applied
     *
     * @return void
     *
     */
    static function displayAjaxMsg($msg, $type = self::UI_MSG_OK, $_ = null)
    {
        $args  = func_get_args();
        $msg   = CAppUI::tr($msg, array_slice($args, 2));
        $msg   = CMbString::htmlEntities($msg);
        $class = self::getErrorClass($type);
        self::callbackAjax('$("systemMsg").show().insert', "<div class='$class'>$msg</div>");
    }

    /**
     * Check whether UI has any problem message
     *
     * @return bool True if no alert/warning/error message
     */
    static function isMsgOK()
    {
        $messages = self::$instance->messages;
        $errors   =
            (is_countable(@$messages[self::UI_MSG_ALERT]) ? count(@$messages[self::UI_MSG_ALERT]) : 0) +
            (is_countable(@$messages[self::UI_MSG_ALERT]) ? count(@$messages[self::UI_MSG_WARNING]) : 0) +
            (is_countable(@$messages[self::UI_MSG_ERROR]) ? count(@$messages[self::UI_MSG_ERROR]) : 0);

        return $errors === 0;
    }

    /**
     * Add a action pair message
     * Make an error is message is not null, ok otherwise
     *
     * @param string $msg    The internationalized message
     * @param string $action The internationalized action
     * @param mixed  $_      [optional] Any number of printf-like parameters to be applied to action
     *
     * @return void
     *
     */
    static function displayMsg($msg, $action, $_ = null)
    {
        $args   = func_get_args();
        $action = self::tr($action, array_slice($args, 2));
        if ($msg) {
            $msg = self::tr($msg);
            self::setMsg("$action: $msg", self::UI_MSG_ERROR);

            return;
        }

        self::setMsg($action, self::UI_MSG_OK);
    }

    /**
     * Render HTML system message bloc corresponding to current messages
     * Possibly clear messages, thus being shown only once
     *
     * @param bool $reset [optional] Clear messages if true
     *
     * @return string HTML divs
     */
    static function getMsg($reset = true)
    {
        $return = "";

        if (self::$instance && self::$instance->messages) {
            ksort(self::$instance->messages);

            foreach (self::$instance->messages as $type => $messages) {
                $class = self::getErrorClass($type);

                foreach ($messages as $message => $count) {
                    $render = $count > 1 ? "$message x $count" : $message;
                    $return .= "<div class='$class'>$render</div>";
                }
            }

            if ($reset) {
                self::$instance->messages = [];
            }
        }

        return CMbString::purifyHTML($return);
    }

    /**
     * Display an AJAX message step after translation
     *
     * @param int    $type Message type as a UI constant
     * @param string $msg  The internationalized message
     * @param mixed  $_    [optional] Any number of printf-like parameters to be applied
     *
     * @return void
     *
     */
    static function stepMessage($type, $msg, $_ = null)
    {
        $args = func_get_args();

        if (!static::$echo_step) {
            CAppUI::setMsg($msg, $type, array_slice($args, 2));

            return;
        }

        $msg = CAppUI::tr($msg, array_slice($args, 2));
        $msg = CMbString::purifyHTML($msg);

        $class = self::getErrorClass($type);
        echo "\n<div class='small-$class'>$msg</div>";
    }

    /**
     * Display an AJAX step, and exit on error messages
     *
     * @param string $msg  The internationalized message
     * @param int    $type [optional] Message type as a UI constant
     * @param mixed  $_    [optional] Any number of printf-like parameters to be
     *
     * @return void
     */
    static function stepAjax($msg, $type = self::UI_MSG_OK, $_ = null)
    {
        $args = func_get_args();

        if (self::$echo_step) {
            $msg = self::tr($msg, array_slice($args, 2));
            $msg = CMbString::purifyHTML($msg);

            $class = self::getErrorClass($type);
            echo "\n<div class='$class'>$msg</div>";
        } else {
            self::setMsg($msg, $type, array_slice($args, 2));
        }

        if ($type == self::UI_MSG_ERROR) {
            throw new ControllerStoppedException(500, $msg);
        }
    }

    public static function turnOnEchoStep()
    {
        self::$echo_step = true;
    }

    /**
     * Display a common error, without details
     *
     * @param string $msg The message
     *
     * @return void
     */
    static function commonError($msg = 'common-error-An error occurred')
    {
        CAppUI::stepAjax($msg, self::UI_MSG_ERROR);
    }

    /**
     * Echo an ajax callback with given value
     *
     * @param string $callback Name of the javascript function
     * @param string $args     Value parameter(s) for javascript function
     *
     * @return void
     */
    static function callbackAjax($callback, $args = '')
    {
        $args = func_get_args();
        $args = array_slice($args, 1);

        $args = array_map([CMbArray::class, "toJSON"], $args);
        $args = implode(",", $args);

        self::js("try { $callback($args); } catch(e) { console.error(e) }");
    }

    /**
     * Echo an HTML javascript block
     *
     * @param string $script Javascript code
     *
     * @return void
     */
    public static function js(string $script): void
    {
        if (self::$echo_step) {
            echo "\n<script>$script</script>";
        } else {
            self::addScript("\n<script>$script</script>");
        }
    }

    /**
     * Load the stored user preference from the database
     *
     * @param string  $key     Preference's key
     * @param integer $user_id User ID to get the preference of
     *
     * @return array|false
     */
    static function loadPref($key, $user_id)
    {
        $ds = CSQLDataSource::get("std");

        $query = "SELECT `user_id`, `value`
              FROM user_preferences
              WHERE `key` = '$key'";
        $pref  = $ds->loadHashList($query);

        // User preference
        if (isset($pref[$user_id])) {
            return $pref[$user_id];
        }

        // Profile preference
        $query      = "SELECT `profile_id`
              FROM users
              WHERE `user_id` = '$user_id'";
        $profile_id = $ds->loadResult($query);

        if ($profile_id && isset($pref[$profile_id])) {
            return $pref[$profile_id];
        }

        // Default preference
        if (isset($pref[""])) {
            return $pref[""];
        }

        return false;
    }

    /**
     * Load the stored user preferences from the database into cache
     *
     * @param int $user_id User ID, 0 for default preferences
     *
     * @return void
     */
    static function loadPrefs($user_id = null)
    {
        self::$instance->user_prefs = array_merge(self::$instance->user_prefs, CPreferences::get($user_id));
    }

    /**
     * Build preferences for connected user, with the default/profile/user strategy
     *
     * @throws Exception
     */
    public static function buildPrefs(): void
    {
        // Default
        self::loadPrefs();

        // Profile
        $user = CUser::get();
        if ($user->profile_id) {
            self::loadPrefs($user->profile_id);
        }

        // User
        self::loadPrefs($user->_id);
    }

    /**
     * Get a named user preference value
     *
     * @param string $name    Name of the user preference
     * @param string $default [optional] A default value when preference is not set
     *
     * @return string The value
     */
    static function pref($name, $default = null)
    {
        $prefs = self::$instance->user_prefs;

        return isset($prefs[$name]) ? $prefs[$name] : $default;
    }

    /**
     * Load locales from cache or build the cache
     *
     * @return void
     */
    public static function loadLocales()
    {
        $lang = CAppUI::pref("LOCALE", "fr");

        self::$lang = $lang;

        $shared_name = "locales-$lang";

        $cache = Cache::getCache(Cache::OUTER);

        $locales_prefixes = $cache->get("$shared_name-" . self::LOCALES_PREFIX);

        // Load from shared memory if possible
        if ($locales_prefixes) {
            return;
        }

        $mutex = new CMbFileMutex("locales-build");
        $mutex->acquire(5);

        $locales_prefixes = $cache->get("$shared_name-" . self::LOCALES_PREFIX);

        // Load from shared memory if possible
        if ($locales_prefixes) {
            $mutex->release();

            return;
        }

        $start_build = microtime(true);
        $locales     = [];

        // fix opcache bug when include locales files (winnt & php7.1)
        if (PHP_OS == 'WINNT' && function_exists('opcache_reset')) {
            opcache_reset();
        }

        foreach (self::getLocaleFilesPaths($lang) as $_path) {
            include_once $_path;
        }

        $locales = CMbString::filterEmpty($locales);
        foreach ($locales as &$_locale) {
            $_locale = CMbString::unslash($_locale);
        }
        unset($_locale);

        // Load overwritten locales if the table exists
        $overwrite = new CTranslationOverwrite();
        if ($overwrite->isInstalled()) {
            $locales = $overwrite->transformLocales($locales, $lang);
        }

        // Prefix = everything before "." and "-"
        $by_prefix = [];
        $hashes    = [];

        foreach ($locales as $_key => $_value) {
            /** @var string $_prefix */
            /** @var string $_rest */
            /** @var string $_prefix_raw */

            [$_prefix, $_rest, $_prefix_raw] = self::splitLocale($_key);

            if (!isset($by_prefix[$_prefix])) {
                $hashes[$_prefix]    = $_prefix_raw;
                $by_prefix[$_prefix] = [];
            }

            $by_prefix[$_prefix][$_rest] = $_value;
        }

        foreach ($by_prefix as $_prefix => $_locales) {
            self::$locales[$_prefix]        = $_locales;
            self::$locales_loaded[$_prefix] = true;

            $cache->set("$shared_name-$_prefix", $_locales);
        }

        $cache->set("$shared_name-" . self::LOCALES_PREFIX, $hashes);

        CApp::log(
            sprintf(
                'CACHE : Took %4.3f ms to build locales %s cache',
                (microtime(true) - $start_build) * 1000,
                $lang
            )
        );

        $mutex->release();
    }

    /**
     * Returns the list of $locale files
     *
     * @param string $locale The locale name of the paths
     *
     * @return array The paths
     */
    static function getLocaleFilesPaths($locale)
    {
        $root_dir = CAppUI::conf("root_dir");

        $overload = glob("$root_dir/modules/*/locales/$locale.overload.php");
        sort($overload);

        $paths = array_merge(
            glob("$root_dir/locales/$locale/*.php"),
            glob("$root_dir/modules/*/locales/$locale.php"),
            glob("$root_dir/style/*/locales/$locale.php"),
            glob("$root_dir/mobile/modules/*/locales/$locale.php"),
            $overload
        );

        return $paths;
    }

    /**
     * Check translated statement exists
     *
     * @param string $str statement to translate
     *
     * @return boolean if translated statement exists
     */
    static function isTranslated($str)
    {
        if (CAppUI::conf('locale_warn')) {
            $char = CAppUI::conf('locale_alert');

            return CAppUI::tr($str) !== $char . $str . $char;
        } else {
            return CAppUI::tr($str) !== $str;
        }
    }

    /**
     * Get locale prefix CRC32 hash
     *
     * @param string $prefix_raw Prefix, raw value
     *
     * @return string
     */
    static function getLocalePrefixHash($prefix_raw)
    {
        // Cache of [prefix => crc32(prefix)]
        static $prefixes = [];

        if ($prefix_raw === "__common__") {
            return $prefix_raw;
        }

        if (isset($prefixes[$prefix_raw])) {
            return $prefixes[$prefix_raw];
        }

        $prefix                = hash("crc32", $prefix_raw); // Faster than crc32() and md5()
        $prefixes[$prefix_raw] = $prefix;

        return $prefix;
    }

    /**
     * Split a locale key into prefix + rest
     *
     * @param string $str The locale key to split
     *
     * @return array
     */
    static function splitLocale($str)
    {
        $positions = [
            strpos($str, "-"),
            strpos($str, "."),
        ];
        $positions = array_filter($positions);

        // If a separator is present, it's a prefixed locale
        if (count($positions) && ($min_pos = min($positions))) {
            $_prefix_raw = substr($str, 0, $min_pos);
            $_rest       = substr($str, $min_pos);
        } // else a common locales
        else {
            $_prefix_raw = "__common__";
            $_rest       = $str;
        }

        $_prefix = self::getLocalePrefixHash($_prefix_raw);

        return [$_prefix, $_rest, $_prefix_raw];
    }

    /**
     * Localize given statement
     *
     * @param string      $str  Statement to translate
     * @param array|mixed $args Array or any number of sprintf-like arguments
     *
     * @return string translated statement
     */
    static function tr($str, $args = null)
    {
        $initial_args = func_get_args();

        if (empty($str)) {
            return "";
        }

        $str = trim($str);

        // Defined and not empty
        if (self::$localize) {
            [$_prefix, $_rest] = CAppUI::splitLocale($str);

            $_lang_prefix = self::$lang . $_prefix;

            // Not in self::$locales cache
            if (empty(self::$locales_loaded[$_lang_prefix])) {
                $shared_name = "locales-" . self::$lang . "-" . $_prefix;

                $cache = Cache::getCache(Cache::OUTER);

                if ($cache->has($shared_name)) {
                    $_loc = $cache->get($shared_name);

                    if (empty(self::$locales[$_prefix])) {
                        self::$locales[$_prefix] = $_loc;
                    } else {
                        self::$locales[$_prefix] = array_merge(self::$locales[$_prefix], $_loc);
                    }
                }

                self::$locales_loaded[$_lang_prefix] = true;
            }

            // dereferecing makes the systme a lots slower ! :(
            //$by_prefix = array_key_exists($_prefix, self::$locales) ? self::$locales[$_prefix] : null;
            //$by_prefix = self::$locales[$_prefix];

            if (isset(self::$locales[$_prefix][$_rest]) && self::$locales[$_prefix][$_rest] !== "") {
                $str = self::$locales[$_prefix][$_rest];
            } // Other wise keep it in a stack...
            elseif (self::$lang) {
                if (!in_array($str, self::$unlocalized) && !preg_match(self::$localize_ignore, $str)) {
                    self::$unlocalized[] = CMbString::htmlSpecialChars($str);
                }
                // ... and decorate
                if (self::$locale_mask) {
                    $str = sprintf(self::$locale_mask, $str);
                }
            }
        }

        if ($args !== null) {
            if (!is_array($args)) {
                $args = $initial_args;
                unset($args[0]);
            }
            if ($args) {
                $str = vsprintf($str, $args);
            }
        }

        return nl2br($str);
    }

    /**
     * Get a flattened version of all cached locales
     *
     * @param string $lang Language: fr, en
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public static function flattenCachedLocales(string $lang): array
    {
        $shared_name = "locales-{$lang}";

        $cache = Cache::getCache(Cache::OUTER);

        $prefixes = $cache->get("{$shared_name}-" . self::LOCALES_PREFIX);

        if (empty($prefixes)) {
            return [];
        }

        $locales = [];
        foreach ($prefixes as $_hash => $_prefix) {
            $prefixed = $cache->get("{$shared_name}-{$_hash}");

            if (!$prefixed) {
                continue;
            }

            if ($_prefix === "__common__") {
                $_prefix = "";
            }

            foreach ($prefixed as $_key => $_value) {
                $locales["{$_prefix}{$_key}"] = $_value;
            }
        }

        return $locales;
    }

    /**
     * Get the locales from the translations files (do not search for translation overwrite)
     *
     * @return array
     */
    static function getLocalesFromFiles()
    {
        $locales = CAppUI::flattenCachedLocales(CAppUI::$lang);

        //load old locales
        $locale = CAppUI::pref("LOCALE", "fr");
        foreach (CAppUI::getLocaleFilesPaths($locale) as $_path) {
            include $_path; // Do not include once or you can't get the locales from files multiple times in a single query
        }
        $locales = CMbString::filterEmpty($locales);
        foreach ($locales as &$_locale) {
            $_locale = CMbString::unslash($_locale);
        }

        return $locales;
    }

    /**
     * Add a locale at run-time
     *
     * @param string $prefix Prefix
     * @param string $rest   All after the prefix
     * @param string $value  Locale value
     *
     * @return void
     */
    static function addLocale($prefix, $rest, $value)
    {
        $prefix = self::getLocalePrefixHash($prefix);

        if (empty(self::$locales[$prefix])) {
            self::$locales[$prefix] = [
                $rest => $value,
            ];
        } else {
            self::$locales[$prefix][$rest] = $value;
        }
    }

    /**
     * Add a list of locales at run-time
     *
     * @param string $prefix_raw Prefix
     * @param array  $locales    Locales
     *
     * @return void
     */
    static function addLocales($prefix_raw, array $locales)
    {
        $prefix = self::getLocalePrefixHash($prefix_raw);

        if (empty(self::$locales[$prefix])) {
            self::$locales[$prefix] = $locales;
        } else {
            self::$locales[$prefix] = array_merge(self::$locales[$prefix], $locales);
        }
    }

    /**
     * Get all the available languages
     *
     * @return array All the languages (2 letters)
     */
    static function getAvailableLanguages()
    {
        $languages = [];
        $root_dir  = dirname(__DIR__, 2);
        foreach (glob("{$root_dir}/locales/*", GLOB_ONLYDIR) as $lng) {
            $languages[] = basename($lng);
        }

        return $languages;
    }

    /**
     * Return the configuration setting for a given path
     *
     * @param string                $path    Tokenized path, eg "module class var", dP proof
     * @param CMbObject|string|null $context The context
     *
     * @return mixed
     * @throws Exception
     */
    static function conf($path = "", $context = null)
    {
        if ($context) {
            if ($context === 'static') {
                try {
                    return (ConfigurationManager::get())->getValue($path);
                } catch (Exception $e) {
                    if (PHP_SAPI === 'cli') {
                        return null;
                    } else {
                        throw $e;
                    }
                }
            }

            if ($context instanceof CMbObject) {
                $context = $context->_guid;
            }

            return CConfiguration::getValue($context, $path);
        }

        global $dPconfig;
        $conf = $dPconfig;
        if (!$path) {
            return $conf;
        }

        if (!$conf) {
            return null;
        }

        $items = explode(' ', $path);

        try {
            foreach ($items as $part) {
                // dP ugly hack
                if (!array_key_exists($part, $conf) && array_key_exists("dP$part", $conf)) {
                    $part = "dP$part";
                }

                if (!array_key_exists($part, $conf)) {
                    throw new TypeError('Undefined array key "' . $part . '"');
                }

                $conf = $conf[$part];
            }
        } catch (TypeError $exception) {
            // Do not log errors when the call is with an @ or error_reporting(0)
            if (static::isReportingActive(error_reporting())) {
                CApp::log($exception->getMessage(), null, LoggerLevels::LEVEL_WARNING);
            }


            return null;
        }

        return $conf;
    }

    public static function isReportingActive(int $error_reporting): bool
    {
        // Before PHP 8.0 @ totaly disable the error_reporting
        if (PHP_VERSION_ID < 80000) {
            return $error_reporting;
        }

        // After php 8.0 the @ no longer silence fatal errors
        // (E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR, E_RECOVERABLE_ERROR)
        return $error_reporting && $error_reporting !==
            (E_ERROR + E_PARSE + E_CORE_ERROR + E_COMPILE_ERROR + E_USER_ERROR + E_RECOVERABLE_ERROR);
    }

    /**
     * Get the CGroups configuration settings for a given path
     *
     * @param string       $path     Configuration path
     * @param integer|null $group_id CGroups IND
     *
     * @return mixed
     */
    static function gconf($path = '', $group_id = null)
    {
        $group_id = ($group_id) ?: CGroups::get()->_id;

        return static::conf($path, "CGroups-{$group_id}");
    }

    /**
     * Change a configuration value
     *
     * @param string $path           Configuration path
     * @param null   $value          New value
     * @param bool   $allow_all_user Allow all users to store the conf. Be carefull with this argument.
     *
     * @return mixed|null Old value
     * @throws Exception
     */
    static function setConf($path = "", $value = null, bool $allow_all_user = false)
    {
        if (!$allow_all_user && !CAppUI::$instance->_ref_user->isAdmin()) {
            return null;
        }

        $config    = new CMbConfig();
        $old_value = $config->get($path);

        $config->set($path, $value);
        $config->update($config->values);

        $forbidden = false;
        foreach (CMbConfig::$forbidden_values as $_value) {
            if (strpos($path, $_value) !== false) {
                $forbidden = true;
                break;
            }
        }

        if (!$forbidden) {
            CMbConfig::setConfInDB($path, $value);
        }

        return $old_value;
    }

    /**
     * Go to the "offline" page, specifying a reason
     *
     * @param string|object $context The context of the denial
     *
     * @return never
     */
    public static function accessDenied(mixed $context = null): never
    {
        throw new AccessDeniedException(
            CAppUI::tr('common-msg-You are not allowed to access this information (%s)', $context)
        );
    }

    /**
     * Go to the "offline" page, specifying a a reason
     *
     * @param string $object_guid The context of the denial
     *
     * @return never
     */
    public static function notFound(string $object_guid): never
    {
        throw new ObjectNotFoundException($object_guid);
    }

    /**
     * Get authentification method
     *
     * @return string
     */
    static function getAuthMethod()
    {
        return self::$instance->auth_method;
    }

    /**
     * Checks if connected user is a CPatient
     *
     * @return bool
     */
    static function isPatient()
    {
        return CUser::isPatientUser(self::$instance->user_type);
    }

    /**
     * Check if the connection is remote or local
     *
     * @return bool
     * @throws Exception
     */
    static function isIntranet()
    {
        $adress                       = CMbServer::getRemoteAddress();
        self::$instance->ip           = $adress["client"];
        self::$instance->proxy        = $adress["proxy"];
        self::$instance->_is_intranet =
            CMbString::isIntranetIP(self::$instance->ip) &&
            (self::$instance->ip != self::conf("system reverse_proxy"));

        return self::$instance->_is_intranet;
    }

    /**
     * Tells if we have to restrict user creation (for LDAP-only)
     *
     * @return bool
     */
    static function restrictUserCreation()
    {
        $group = CGroups::get();
        $user  = CMediusers::get();

        return (
            static::conf('admin LDAP ldap_connection')
            && static::conf('admin CLDAP restrict_new_users_to_LDAP', $group->_guid)
            && !$user->isAdmin()
        );
    }

    /**
     * Check whether or not patient are partitioned by function
     *
     * @return bool
     */
    static function isCabinet()
    {
        return CAppUI::conf('dPpatients CPatient function_distinct') == 1;
    }

    /**
     * Check whether or not patient are partitioned by group
     *
     * @return bool
     */
    static function isGroup()
    {
        return CAppUI::conf('dPpatients CPatient function_distinct') == 2;
    }

    /**
     * Return the accounts and number of unread messages for the connected user
     *
     * @return array
     */
    public static function getMessagerieInfo()
    {
        if (!CModule::getActive('messagerie') || !CModule::getCanDo('messagerie')->read) {
            return null;
        }

        return ['accounts' => self::getMessagerieAccounts(), 'counters' => self::getMessagerieCounters()];
    }

    /**
     * Return the number of unread messages for each messagerie account of the connected user
     *
     * @return array|null
     */
    static function getMessagerieCounters()
    {
        if (!CModule::getActive('messagerie') || !CModule::getCanDo('messagerie')->read) {
            return null;
        }

        $data = [];

        $categories = self::getMessagerieAccounts();
        foreach ($categories as $category => $accounts) {
            if ($category == 'internal') {
                $count           = CUserMessageDest::getUnreadMessages();
                $data[$category] = ['total' => $count, $count];
            } elseif (is_array($accounts)) {
                $data[$category] = ['total' => 0];
                foreach ($accounts as $guid => $object) {
                    if (is_object($object)) {
                        $data[$category][$guid]   = $object->getUnreadMessages();
                        $data[$category]['total'] += $data[$category][$guid];
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Return the messagerie accounts (internal, external, apicrypt, mssante, medimail, mondialSante) of the connected
     * users
     *
     * @return array
     */
    public static function getMessagerieAccounts()
    {
        if (!CModule::getActive('messagerie') || !CModule::getCanDo('messagerie')->read) {
            return [];
        }
        $user = CMediusers::get();

        $cache = new Cache(
            self::MESSAGERIE_ACCOUNTS_CACHE,
            "accounts-{$user->_guid}",
            Cache::OUTER,
            CUserMessageDest::getCacheLifetime()
        );

        $accounts = ['internal' => [], 'external' => [], 'laboratories' => []];

        if ($cache->exists()) {
            $accounts = $cache->get();
        } else {
            if (CAppUI::gconf('messagerie access allow_internal_mail')) {
                $accounts['internal'][] = true;
            } else {
                unset($accounts['internal']);
            }

            if (CAppUI::gconf('messagerie access allow_external_mail')) {
                $pop_accounts = CSourcePOP::getAccountsFor($user);
                foreach ($pop_accounts as $account) {
                    $accounts['external'][$account->_guid] = $account;
                }
            }

            if (CModule::getActive('apicrypt') && CModule::getCanDo('apicrypt')->read) {
                $apicrypt_account = CSourcePOP::getApicryptAccountFor($user);

                if ($apicrypt_account->_id) {
                    $accounts['external'][$apicrypt_account->_guid] = $apicrypt_account;
                }
            }

            if (CModule::getActive('mssante') && CModule::getCanDo('mssante')->read) {
                $mssante_account = CMSSanteUserAccount::getAccountFor($user);
                if ($mssante_account->_id) {
                    $accounts['external'][$mssante_account->_guid] = $mssante_account;
                }
            }

            if (CModule::getActive('medimail') && CModule::getCanDo('medimail')->read) {
                $medimail_account = CMedimailAccount::getAccountFor($user);
                if ($medimail_account->_id) {
                    $accounts['external'][$medimail_account->_guid] = $medimail_account;
                }

                if (self::pref("allowed_access_application_mailbox") === "1") {
                    $medimail_account_application = CGroups::get()->loadRefMedimailAccount();
                    if ($medimail_account_application->_id) {
                        $accounts['external'][$medimail_account_application->_guid] = $medimail_account_application;
                    }
                }

                if (self::pref("allowed_access_organisational_mailbox") === "1") {
                    $medimail_account_organisational = CFunctions::getCurrent()->loadRefMedimailAccount();
                    if ($medimail_account_organisational->_id) {
                        $accounts['external'][$medimail_account_organisational->_guid]
                            = $medimail_account_organisational;
                    }
                }
            }

            if (CModule::getActive('mondialSante') && CModule::getCanDo('mondialSante')->read) {
                $mondial_sante_account = CMondialSanteAccount::getAccountFor($user);
                if ($mondial_sante_account->_id) {
                    $accounts['laboratories'][$mondial_sante_account->_guid] = $mondial_sante_account;
                }
            }

            if (empty($accounts['laboratories'])) {
                unset($accounts['laboratories']);
            }

            $cache->put($accounts);
        }

        return $accounts;
    }

    /**
     * Reset the messagerie accounts cache
     *
     * @return void
     */
    public static function resetMessagerieAccountsCache()
    {
        $user  = CMediusers::get();
        $cache = new Cache(
            self::MESSAGERIE_ACCOUNTS_CACHE,
            "accounts-{$user->_guid}",
            Cache::OUTER,
            CUserMessageDest::getCacheLifetime()
        );
        $cache->rem();
    }

    /**
     * Tells if we must update session lifetime
     *
     * @return bool
     */
    static function reviveSession()
    {
        return !CAppUI::$session_no_revive;
    }

    public static function create(
        CUser                $user,
        string               $method,
        ?CUserAuthentication $auth
    ): CAppUI {
        $user_group  = null;
        $user_remote = false;

        // Convert CUser to CMediusers for mb compat
        $mediuser = new CMediusers();
        if ($mediuser->isInstalled()) {
            $mediuser->load($user->_id);
            self::$instance->_ref_user = $mediuser;

            if ($mediuser->_id) {
                $user_group  = $mediuser->loadRefFunction()->group_id;
                $user_remote = (bool)$mediuser->remote;
            }
        }

        self::$instance->auth_method = $method;
        self::$instance->user_remote = $user_remote;
        self::$instance->user_group  = $user_group;
        self::$instance->user_id     = $user->_id;

        self::$instance->ua             = new CUserAgent();
        self::$instance->_ref_last_auth = $auth;

        self::$instance->user_first_name = $user->user_first_name;
        self::$instance->user_last_name  = $user->user_last_name;
        self::$instance->user_email      = $user->user_email;
        self::$instance->user_type       = $user->user_type;
        self::$instance->user_last_login = $user->getLastLogin();

        return self::$instance;
    }

    public static function initUser(): void
    {
        if (CModule::getInstalled('mediusers')) {
            $mediuser = new CMediusers();
            if ($mediuser->isInstalled()) {
                $mediuser->load(self::$instance->user_id);
                $mediuser->getBasicInfo();
                self::$instance->_ref_user = $mediuser;

                CApp::$is_robot = $mediuser->isRobot();

                // In case of CUser without a CMediuser, we create a fake one with some properties fallback.
                if (!$mediuser->_id) {
                    // Getting the CUser currently authenticated
                    $user = CUser::get();

                    // Common error is connecting with "super admin" but not valuing the fake mediuser's type.
                    // By applying this, calls to CAppUI::$instance->_ref_user->isAdmin() will not fail.
                    if ($user->isSuperAdmin() || $user->isAdmin()) {
                        $mediuser->_user_type = 1;
                    }
                }
            }
        }
    }

    /**
     * Retourne l'�tat d'activation du mode dark et du theme Mediboard Etendu
     *
     * @return bool
     */
    static function isMediboardExtDark()
    {
        return self::pref("mediboard_ext_dark") === '1';
    }

    /**
     * @param string $locale
     * @param string $value
     * @param bool   $add_slashes
     *
     * @return bool
     */
    public static function localExists(string $locale, string $value = null, bool $add_slashes = false): bool
    {
        $char = CAppUI::conf('locale_warn') ? CAppUI::conf('locale_alert') : '';

        $trad = CAppUI::tr($locale);

        $local_exists = (bool)($locale !== $char . $trad . $char);

        if (!$value) {
            return $local_exists;
        }

        $trad = $add_slashes ? addslashes($trad) : $trad;

        return (bool)($trad == $value);
    }

    public static function loadCoreLocales(): array
    {
        // Init locale info
        $locale_info = [];

        // set locales depend if user is connected
        require CAppUI::conf('root_dir') . "/locales/core.php";

        if (empty($locale_info["names"])) {
            $locale_info["names"] = [];
        }
        setlocale(LC_TIME, $locale_info["names"]);

        if (empty($locale_info["charset"])) {
            $locale_info["charset"] = "UTF-8";
        }

        // We don't use mb_internal_encoding as it may be redefined by libs
        CApp::$encoding = $locale_info["charset"];

        ini_set('default_charset', CApp::$encoding);

        return CAppUI::$locale_info = $locale_info;
    }

    public static function isMultiTabMessageRead(): int
    {
        // For JS usage
        return CValue::sessionAbs(self::MULTI_TAB_MSG_READ, 0);
    }

    public static function markMultiTabMessageAsRead(): void
    {
        CValue::setSessionAbs(self::MULTI_TAB_MSG_READ, 1);
    }

    public static function addScript(string $script): void
    {
        self::$instance->scripts[] = $script;
    }

    public static function getScripts(): array
    {
        $scripts = self::$instance->scripts;

        self::$instance->scripts = [];

        return $scripts;
    }
}
