<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Database;

use Ox\Core\CSQLDataSource;

/**
 * Datasource factory, enabling us to autowire datasource into services (instead of controllers) whereas configurations
 * are not already set.
 */
class DsFactory
{
    public function getDs(): CSQLDataSource
    {
        return CSQLDataSource::get('std');
    }
}
