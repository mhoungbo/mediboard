<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;
use LogicException;
use Ox\Core\Module\CModule;
use Traversable;

/**
 * Simple proxy class enabling us to dynamically load an active CModule.
 * Used in Smarty templates as an alternative to loading all the modules (and perms) at each request.
 *
 * /!\ This class should not be serialized.
 */
class ModuleProxy implements ArrayAccess, Countable, IteratorAggregate
{
    /** @var CModule[] */
    private array $container = [];

    /**
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * @param mixed $offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        $module = $this->container[$offset] ?? null;

        if ($module !== null) {
            return $module;
        }

        $module = CModule::getActive($offset);

        if ($module instanceof CModule) {
            $module->canDo();
            $this->offsetSet($offset, $module);

            return $module;
        }

        return null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        $this->container[$offset] = $value;
    }

    /**
     * @param mixed $offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    public function count(): int
    {
        return count(CModule::getActive());
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->loadAllModules());
    }

    private function loadAllModules(): array
    {
        $modules = CModule::getActive();
        foreach ($modules as $module) {
            $module->canDo();
        }

        return $modules;
    }

    public function __sleep()
    {
        throw new LogicException('This class cannot be serialized');
    }
}
