<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\Model;

use Exception;
use Ox\Core\CMbFieldSpec;
use Ox\Core\CMbModelNotFoundException;
use Ox\Core\CStoredObject;
use Ox\Core\FieldSpecs\CPasswordSpec;
use Ox\Mediboard\System\CEncryptedProperty;
use Ox\Mediboard\System\Keys\KeyStore;
use Throwable;

trait EncryptedPropertyAccessorTrait
{
    public static function getEncryptedPropertiesSpecs(CStoredObject $object): array
    {
        static $encrypted_fields = null;

        if ($encrypted_fields === null) {
            $encrypted_fields = array_filter(
                $object->getSpecs(),
                fn(CMbFieldSpec $spec): bool => (($spec instanceof CPasswordSpec) && $spec->isEncrypted())
            );
        }

        return $encrypted_fields;
    }

    /**
     * @throws CMbModelNotFoundException
     * @throws Exception
     */
    public static function getEncryptedProperty(CStoredObject $object, string $property_name): ?string
    {
        self::checkPropertyIsValid($object, $property_name);

        $encrypted_ref = ($object->{$property_name}) ?? null;

        // NULL, 0, '', undefined
        if (!$encrypted_ref) {
            return null;
        }

        if (!$object->_class || !$object->_id) {
            return null;
        }

        return CEncryptedProperty::findOrFail($encrypted_ref)->decrypt();
    }

    /**
     * @param CStoredObject $object
     * @param string        $property_name
     *
     * @throws Exception
     */
    private static function checkPropertyIsValid(CStoredObject $object, string $property_name): void
    {
        try {
            $spec = ($object->getSpecs()[$property_name]) ?? null;

            if (!($spec instanceof CPasswordSpec)) {
                throw new Exception('Invalid spec provided');
            }

            if (!$spec->isEncrypted()) {
                throw new Exception('Invalid Password spec');
            }

            // Just trying to load it
            KeyStore::loadKey($spec->key);
        } catch (Throwable $t) {
            throw new Exception($t->getMessage());
        }
    }
}
