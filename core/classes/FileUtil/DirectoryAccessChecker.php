<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\FileUtil;

use Ox\Core\Cache;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Service used to check if a directory is accessible.
 * The timeout used avoid being stuck on non available NFS mount-points.
 */
class DirectoryAccessChecker
{
    // Command "type" return 0 in case of success and 127 in case of command not found
    private const RETURN_CODE_OK_TIMEOUT = 0;

    // The timeout command return 124 if the timeout is reached. Other return codes are the ones from the executed
    // command (for ls 0, 1 or 2).
    private const RETURN_CODE_TIMEOUT_REACHED = 124;
    private const TIMEOUT_CHECK_CACHE_TTL     = 60 * 60 * 24;
    private const DIRECTORY_CACHE_TTL         = 60;

    private CacheInterface $cache;

    public function __construct(CacheInterface $inner_outer_cache = null)
    {
        $this->cache = $inner_outer_cache ?? Cache::getCache(Cache::INNER_OUTER);
    }

    /**
     * Tell if a directory is accessible in the timeout given.
     * For the command execution use 2>/dev/null to avoid echoing errors.
     *
     * @param string $directory_path
     * @param int    $timeout Time to wait for directory to respond in seconds.
     *
     * @return bool Return true if the directory is accessible (or if the timeout command is not present on the system).
     *              Return false if the directory is not accessible in the timeout.
     *
     * @throws InvalidArgumentException
     */
    public function isDirectoryAccessible(string $directory_path, int $timeout): bool
    {
        if (!$this->isTimeoutAvailable()) {
            return true;
        }

        $cache_key = 'DirectoryAccessChecker-Directory-' . md5($directory_path);

        if (null === ($code = $this->cache->get($cache_key))) {
            $code = $this->executeCommand(
                sprintf('timeout %ds ls %s 2>/dev/null', $timeout, escapeshellarg($directory_path))
            );

            $this->cache->set($cache_key, $code, self::DIRECTORY_CACHE_TTL);
        }

        return self::RETURN_CODE_TIMEOUT_REACHED !== $code;
    }

    public function isTimeoutAvailable(): bool
    {
        if (null === ($can_timeout = $this->cache->get('DirectoryAccessChecker-can-timeout'))) {
            $can_timeout = $this->executeCommand('type timeout 2>/dev/null');

            $this->cache->set('DirectoryAccessChecker-can-timeout', $can_timeout, self::TIMEOUT_CHECK_CACHE_TTL);
        }

        return self::RETURN_CODE_OK_TIMEOUT === $can_timeout;
    }

    /**
     * @param string $command The command must be escaped !
     *
     * @return int
     */
    protected function executeCommand(string $command): int
    {
        exec($command, $out, $code);

        return $code;
    }
}
