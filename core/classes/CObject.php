<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core;

use AllowDynamicProperties;

/**
 * Object class
 * Useful to declare anonymous object
 * Todo: � supprimer
 * @deprecated
 */
#[AllowDynamicProperties]
class CObject
{
    /**
     * Empty constructor
     */
    function __construct()
    {
    }
}
