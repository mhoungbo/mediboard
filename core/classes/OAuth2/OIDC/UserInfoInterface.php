<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\OAuth2\OIDC;

interface UserInfoInterface
{
    /**
     * Return the Provider internal identifier (sub).
     *
     * @return string|null
     */
    public function getSub(): ?string;

    /**
     * Returns the "identifier" of the user (rpps, email, etc.)
     *
     * @return string|null
     */
    public function getId(): ?string;
}
