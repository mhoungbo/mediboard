<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\OAuth2\OIDC\FC;

use Exception;
use Ox\Core\Config\Conf;

class FcOptions
{
    private readonly Conf $conf;

    public function __construct(Conf $conf = null)
    {
        $this->conf = $conf ?? new Conf();
    }

    /**
     * Check configuration to enable Pro Sante Connect login button
     *
     * @return bool
     * @throws Exception
     */
    public function isLoginEnabled(): bool
    {
        return $this->conf->get('admin FranceConnect enable_fc_authentication')
            && $this->conf->get('admin FranceConnect enable_login_button');
    }

    /**
     * Return Pro Sante Connect options
     *
     * @return array
     * @throws Exception
     */
    public function getOptions(): array
    {
        return [
            'issuer'            => $this->conf->get('admin FranceConnect issuer'),
            'clientId'          => $this->conf->get('admin FranceConnect client_id'),
            'clientSecret'      => $this->conf->get('admin FranceConnect client_secret'),
            'redirectUri'       => $this->conf->get('admin FranceConnect redirect_uri'),
            'logoutRedirectUri' => $this->conf->get('admin FranceConnect logout_redirect_uri'),
        ];
    }
}
