<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\OAuth2\OIDC\PSC;

use Exception;
use Jumbojett\Client as OidcClient;
use Jumbojett\Config;
use Jumbojett\Jwt;
use Jumbojett\OpenIDConnectClientException;
use Ox\Core\CApp;
use Ox\Core\OAuth2\OIDC\TokenSet;
use Ox\Mediboard\Admin\Controllers\LoginController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * OIDC client wrapped around PSC.
 */
class Client
{
    // Connection timeout of 2 seconds seems a little short for PSC service.
    private const CONNECTION_TIMEOUT = 3;
    private const TIMEOUT            = 5;

    private const LEEWAY = 300;

    private OidcClient $client;

    // Must be protected to allow override in mocks
    protected array $options;

    /**
     * @throws Exception
     */
    public function __construct(PscOptions $psc)
    {
        $this->options = $psc->getOptions();

        $issuer = $this->options['issuer'];

        $config = new Config($issuer ?? '', $this->options['clientId'] ?? '', $this->options['clientSecret'] ?? '');
        $config->setIssuer($issuer ?? '');

        $config->setConnectionTimeout(self::CONNECTION_TIMEOUT);
        $config->setTimeout(self::TIMEOUT);

        $config->setWellKnownConfigUrl($issuer . '/.well-known/wallet-openid-configuration');
        $config->addAuthParams(['acr_values' => 'eidas1']);
        $config->addScopes(['openid', 'scope_all']);
        $config->setLeeway(self::LEEWAY);

        // Does not seem to still be necessary
//        $config->addClaimsValidator(
//            function (OidcClient $client, array $claims): bool {
//                $acr = ($claims['acr']) ?? null;
//
//                return $acr === 'eidas1';
//            }
//        );

        $http_client = HttpClient::create();

        $this->client = new OidcClient($http_client, $config);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function requestAuthorization(): string
    {
        return $this->client->requestAuthorization($this->options['redirectUri']);
    }

    /**
     * @param string $code
     * @param string $state
     *
     * @return void
     * @throws Exception
     */
    public function requestTokens(string $code, string $state): void
    {
        $this->client->requestTokens($this->options['redirectUri'], $code, $state, null);
    }

    public function hasRefreshTokenExpired(): bool
    {
        $refresh_token = $this->client->getRefreshToken();

        if ($refresh_token === null) {
            return false;
        }

        $token = new Jwt($refresh_token);

        return $token->hasExpired(self::LEEWAY);
    }

    public function refreshToken(string $refresh_token, TokenSet $token_set = null): void
    {
        $this->client->refreshToken($refresh_token);

        if ($token_set !== null) {
            $token_set->setAccessToken($this->client->getAccessToken());
            $token_set->setRefreshToken($this->client->getRefreshToken());
        }
    }

    public function getTokenSet(): TokenSet
    {
        return new TokenSet(
            TokenSet::PSC_TYPE,
            $this->client->getAccessToken(),
            $this->client->getRefreshToken(),
            $this->client->getIdToken()
        );
    }

    /**
     * @param string $id_token
     * @param bool   $redirect
     *
     * @throws OpenIDConnectClientException
     * @throws Exception
     */
    public function signOut(string $id_token, bool $redirect): string
    {
        $redirect = ($redirect) ? $this->options['redirectUri'] : null;

        return $this->client->signOut($id_token, $redirect);
    }

    /**
     * @return array|mixed|null
     * @throws OpenIDConnectClientException
     */
    private function requestUserInfo()
    {
        return $this->client->requestUserInfo($this->client->getAccessToken());
    }

    /**
     * @return UserInfo
     * @throws OpenIDConnectClientException
     */
    public function getUserInfo(): UserInfo
    {
        $user_info = $this->requestUserInfo();

        return new UserInfo($user_info);
    }

    public static function checkSession(?TokenSet $oidc_tokens): void
    {
        $psc = new PscOptions();

        if (!$psc->isLoginEnabled()) {
            return;
        }

        if (!$psc->getSessionMode()) {
            return;
        }

        if (($oidc_tokens === null) || !$oidc_tokens->isPSC()) {
            return;
        }

        $client = new self($psc);

        if ($client->hasRefreshTokenExpired()) {
            $res = new RedirectResponse(CApp::generateUrl(LoginController::LOGOUT_ROUTE_NAME));
            $res->send();

            return;
        }

        try {
            $client->refreshToken($oidc_tokens->getRefreshToken(), $oidc_tokens);
        } catch (Exception $e) {
            $res = new RedirectResponse(CApp::generateUrl(LoginController::LOGOUT_ROUTE_NAME));
            $res->send();

            return;
        }
    }
}
