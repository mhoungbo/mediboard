<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Core\OAuth2\OIDC\PSC;

use Ox\Core\OAuth2\OIDC\UserInfoInterface;

/**
 * PSC UserInfo container.
 *
 * Contains:
 * - Sub
 * - Id (RPPS or Adeli)
 * - Family name
 * - Given name
 * - Other ids (custom)
 */
class UserInfo implements UserInfoInterface
{
    /** @var string|null */
    private $sub;

    /** @var string|null */
    private $id;

    /** @var string|null */
    private $family_name;

    /** @var string|null */
    private $given_name;

    /** @var array */
    private $other_ids;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->sub         = $data['sub'] ?? null;
        $this->id          = $this->sanitizeRpps($data['SubjectNameID']);
        $this->family_name = $data['family_name'] ?? null;
        $this->given_name  = $data['given_name'] ?? null;
        $this->other_ids   = $data['otherIds'] ?? [];
    }

    private function sanitizeRpps(?string $rpps): ?string
    {
        if ($rpps === null) {
            return null;
        }

        if ((strlen($rpps) === 12) && str_starts_with($rpps, '8')) {
            return substr($rpps, 1);
        }

        return $rpps;
    }

    public function getSub(): ?string
    {
        return $this->sub;
    }

    public function getId(): ?string
    {
        return $this->id;
    }
}
