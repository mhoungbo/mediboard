{{*
 * @package Mediboard\Style\Mediboard
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}


</div> <!-- close div main -->

{{mb_default var=multi_tab_msg_read value=0}}
<script>
  Main.add(function() {
    MultiTabChecker.check('{{$conf.debug}}', '{{$multi_tab_msg_read}}');
  });
</script>

</body>
</html>
