{{*
 * @package Mediboard\Style\Mediboard
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=long_request value=0}}


<ul id="performance" {{if $long_request}} style="position: initial;" {{/if}}>
    {{if $performance.enslaved}}
      <li class="performance-enslaved">
        <strong>ENSLAVED</strong>
      </li>
    {{/if}}

  <li class="performance-time">
    <strong class="title">Temps de g�n�ration</strong>
    <span class="performance-time">{{$performance.genere}} s</span>

      {{assign var=sql_time value=0}}
      {{foreach from=$performance.dataSources key=dsn item=dataSource}}
          {{assign var=sql_time value=$sql_time+$dataSource.time}}
      {{/foreach}}

      {{assign var=nosql_time value=$performance.nosqlTime}}

      {{if !$performance.genere}}
          {{assign var=total value=1}}
      {{else}}
          {{assign var=total value=$performance.genere}}
      {{/if}}

      {{math equation='(x/y)*100' assign=sql_ratio x=$sql_time y=$total}}
      {{math equation='(x/y)*100' assign=nosql_ratio x=$nosql_time y=$total}}

      {{assign var=sql_ratio value=$sql_ratio|round:2}}
      {{assign var=nosql_ratio value=$nosql_ratio|round:2}}

      {{assign var=transport_ratio value=0}}
      {{assign var=transport_time value=0}}
      {{if 'transportTiers'|array_key_exists:$performance && $performance.transportTiers}}
          {{assign var=transport_time value=$performance.transportTiers.total.time}}
          {{math equation='(x/y)*100' assign=transport_ratio x=$transport_time y=$total}}
          {{assign var=transport_ratio value=$transport_ratio|round:2}}
      {{/if}}

      {{math equation='n-(x+y+z)' assign=php_time n=$total x=$sql_time y=$nosql_time z=$transport_time}}
      {{math equation='(x/y)*100' assign=php_ratio x=$php_time y=$total}}
      {{assign var=php_ratio value=$php_ratio|round:2}}

      {{assign var=php_time value=$php_time|round:3}}
      {{assign var=sql_time value=$sql_time|round:3}}
      {{assign var=nosql_time value=$nosql_time|round:3}}
      {{assign var=transport_time value=$transport_time|round:3}}

      {{mb_include style=$style template=performance_bar}}

    <ul>
        {{foreach from=$performance.dataSources key=dsn item=dataSource}}
          <li>
            <strong>{{$dsn}}</strong>
            <span class="performance-count" title="Nombre de requ�tes">{{$dataSource.count}}</span>
            -
            <span class="performance-time" title="Temps de requ�tes">{{$dataSource.time*1000|string_format:'%.3f'}} ms</span>
            -
            <span class="performance-time" title="Nombre de fetch">{{$dataSource.timeFetch*1000|string_format:'%.3f'}} ms</span>
          </li>
        {{/foreach}}
        {{if $performance.nosqlCount}}
          <li>
            <strong>NoSQL</strong>
            <span class="performance-count" title="Nombre de requ�tes">{{$performance.nosqlCount}}</span>
            -
            <span class="performance-time" title="Temps de requ�tes">{{$performance.nosqlTime*1000|string_format:'%.3f'}} ms</span>
          </li>
        {{/if}}
        {{if 'transportTiers'|array_key_exists:$performance && $performance.transportTiers}}
            {{foreach from=$performance.transportTiers.sources key=source item=transport}}
              <li>
                <strong>{{$source}}</strong>
                <span class="performance-count" title="Nombre de transport">{{$transport.count}}</span>
                -
                <span class="performance-time" title="Temps des transports">{{$transport.time*1000|string_format:'%.3f'}} ms</span>
              </li>
            {{/foreach}}
        {{/if}}
    </ul>
  </li>
  
  <li class="performance-memory">
    <strong class="title">M�moire PHP</strong>
      {{$performance.memoire}}
  </li>
  
  <li class="performance-objects" title="Objets charg�s / cachables">
    <strong class="title">Objets charg�s / cachables</strong>
    <span class="performance-count">{{$performance.objets}}</span> /
    <span class="performance-count">{{$performance.cachableCount}}</span>
    <ul>
        {{foreach from=$performance.objectCounts key=objectClass item=objectCount}}
          <li>
            <strong>{{$objectClass}}</strong>
            <span class="performance-count">{{$objectCount}}</span>
          </li>
        {{/foreach}}
      <li class="separator"> ---</li>
        {{foreach from=$performance.cachableCounts key=objectClass item=cachableCount}}
          <li>
            <strong>{{$objectClass}}</strong>
            <span class="performance-count">{{$cachableCount}}</span>
          </li>
        {{/foreach}}
    </ul>
  </li>

  <li class="performance-cache">
    <span class="performance-count">{{$performance.cache.total}}</span>
    <table style="max-height: 600px; overflow: auto;">
      {{foreach from=$performance.cache.totals key=_prefix item=_layers}}
        <tr>
          <td style="text-align: left;">
            <strong>{{$_prefix}}</strong>
          </td>

          {{foreach from=$_layers key=_layer item=_count name=layers}}
            {{if $_count}}
              <td>{{$_count}}</td>
              <td><tt>{{$_layer}}</tt></td>
            {{else}}
              <td colspan="2"></td>
            {{/if}}
          {{/foreach}}
        </tr>
        {{foreachelse}}
        <tr>
          <td class="empty">Aucun cache utilis�</td>
        </tr>
      {{/foreach}}


    </table>
  </li>

  <li class="performance-pagesize">
    <strong class="title">Taille de la page</strong>
      {{$performance.size}}
  </li>

  
  <li class="performance-network">
    <strong class="title">Adresse IP</strong>
      {{$performance.ip}}
  </li>

  <li class="export" onclick="window.open('data:text/html;charset=utf-8,'+encodeURIComponent(this.up('ul').innerHTML))"
      title="{{tr}}Export{{/tr}}"></li>

</ul>
