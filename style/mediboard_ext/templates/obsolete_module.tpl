{{*
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{if $obsolete_module}}
  <div class="small-error">
    <strong>Ce module n'est pas � jour.</strong>
    Veuillez vous rendre sur <a href="{{url name=system_gui_modules_index}}">la page d'administration des modules</a>.
  </div>
{{/if}}
