{{*
 * @package Mediboard\Style\Mediboard
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

{{mb_default var=class value="performance-bar"}}

{{mb_default var=php_time value=0}}
{{mb_default var=sql_time value=0}}
{{mb_default var=nosql_time value=0}}
{{mb_default var=transport_time value=0}}
{{mb_default var=php_ratio value=0}}
{{mb_default var=sql_ratio value=0}}
{{mb_default var=nosql_ratio value=0}}
{{mb_default var=transport_ratio value=0}}

<div class="{{$class}}" style="display: inline-flex; flex-direction: row; flex-wrap: nowrap; gap: 0; width: 100px; border-radius: 4px; overflow: hidden;"
     title="&bull; PHP : {{$php_ratio}} % ({{$php_time}} s)&#10;&bull; SQL : {{$sql_ratio}} % ({{$sql_time}} s)&#10;&bull; NoSQL : {{$nosql_ratio}} % ({{$nosql_time}} s)&#10;&bull; Transport : {{$transport_ratio}} % ({{$transport_time}} s)">
  <div style="background-color: #CB4B4B; width: {{$php_ratio}}%;">&nbsp;</div>
  <div style="background-color: #4DA74D; width: {{$sql_ratio}}%;">&nbsp;</div>
  <div style="background-color: #9440ED; width: {{$nosql_ratio}}%;">&nbsp;</div>
  <div style="background-color: #0086C0; width: {{$transport_ratio}}%;">&nbsp;</div>
</div>
