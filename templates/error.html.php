<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset=iso-8859-1"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no"/>
  <meta name="format-detection" content="telephone=no"/>
  <meta name="Description" content="OX Platform"/>
  <title>Internal Error</title>
  <style>
    body {
      --primary: #3F51B5;
      --primary-surface-800: #283593;
      --background-pattern: url("data:image/svg+xml, %3Csvg%20viewBox%3D%220%200%201366%20700%22%20fill%3D%22none%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%0A%3Cg%20clip-path%3D%22url(%23clip0_4278_136697)%22%3E%0A%3Crect%20width%3D%221366%22%20height%3D%22700%22%20fill%3D%22%23283593%22%2F%3E%0A%3Cg%20filter%3D%22url(%23filter0_ddd_4278_136697)%22%3E%0A%3Cpath%20d%3D%22M276.771%20483.344C225.097%20507.959%20189.149%20489.735%20135.227%20501.288C81.306%20512.841%2024.3888%20551.158%2014.653%20610.281C4.91716%20669.404%20-52%20700%20-52%20700H1408V211.426C1378.54%20264.72%201440.84%20273.49%201276.97%20169.507C1113.11%2065.525%20970.207%20173.375%20923.775%20229.507C877.342%20285.639%20803.273%20311.294%20740.664%20310.006C662.403%20308.395%20537.791%20276.77%20445.246%20319.005C340.399%20366.856%20328.446%20458.729%20276.771%20483.344Z%22%20fill%3D%22%233F51B5%22%2F%3E%0A%3C%2Fg%3E%0A%3C%2Fg%3E%0A%3Cdefs%3E%0A%3Cfilter%20id%3D%22filter0_ddd_4278_136697%22%20x%3D%22-70%22%20y%3D%22109%22%20width%3D%221496%22%20height%3D%22610%22%20filterUnits%3D%22userSpaceOnUse%22%20color-interpolation-filters%3D%22sRGB%22%3E%0A%3CfeFlood%20flood-opacity%3D%220%22%20result%3D%22BackgroundImageFix%22%2F%3E%0A%3CfeColorMatrix%20in%3D%22SourceAlpha%22%20type%3D%22matrix%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%20127%200%22%20result%3D%22hardAlpha%22%2F%3E%0A%3CfeOffset%20dy%3D%223%22%2F%3E%0A%3CfeGaussianBlur%20stdDeviation%3D%222.5%22%2F%3E%0A%3CfeColorMatrix%20type%3D%22matrix%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200.2%200%22%2F%3E%0A%3CfeBlend%20mode%3D%22normal%22%20in2%3D%22BackgroundImageFix%22%20result%3D%22effect1_dropShadow_4278_136697%22%2F%3E%0A%3CfeColorMatrix%20in%3D%22SourceAlpha%22%20type%3D%22matrix%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%20127%200%22%20result%3D%22hardAlpha%22%2F%3E%0A%3CfeOffset%20dy%3D%221%22%2F%3E%0A%3CfeGaussianBlur%20stdDeviation%3D%229%22%2F%3E%0A%3CfeColorMatrix%20type%3D%22matrix%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200.12%200%22%2F%3E%0A%3CfeBlend%20mode%3D%22normal%22%20in2%3D%22effect1_dropShadow_4278_136697%22%20result%3D%22effect2_dropShadow_4278_136697%22%2F%3E%0A%3CfeColorMatrix%20in%3D%22SourceAlpha%22%20type%3D%22matrix%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%20127%200%22%20result%3D%22hardAlpha%22%2F%3E%0A%3CfeOffset%20dy%3D%226%22%2F%3E%0A%3CfeGaussianBlur%20stdDeviation%3D%225%22%2F%3E%0A%3CfeColorMatrix%20type%3D%22matrix%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200.14%200%22%2F%3E%0A%3CfeBlend%20mode%3D%22normal%22%20in2%3D%22effect2_dropShadow_4278_136697%22%20result%3D%22effect3_dropShadow_4278_136697%22%2F%3E%0A%3CfeBlend%20mode%3D%22normal%22%20in%3D%22SourceGraphic%22%20in2%3D%22effect3_dropShadow_4278_136697%22%20result%3D%22shape%22%2F%3E%0A%3C%2Ffilter%3E%0A%3CclipPath%20id%3D%22clip0_4278_136697%22%3E%0A%3Crect%20width%3D%221366%22%20height%3D%22700%22%20fill%3D%22white%22%2F%3E%0A%3C%2FclipPath%3E%0A%3C%2Fdefs%3E%0A%3C%2Fsvg%3E%0A");

      background-color: var(--primary-surface-800);
      margin: 0;
      padding: 0;
    }

    .ErrorPage {
      align-items: center;
      background-color: var(--primary-surface-800);
      background-image: var(--background-pattern);
      background-size: cover;
      display: flex;
      height: 100vh;
      justify-content: center;
      overflow: hidden;
      width: 100vw;
    }

    .ErrorPage-card {
      align-items: center;
      background-color: white;
      border-radius: 8px;
      box-sizing: border-box;
      display: flex;
      filter: drop-shadow(0px 24px 38px rgba(0, 0, 0, 0.14)) drop-shadow(0px 9px 46px rgba(0, 0, 0, 0.12)) drop-shadow(0px 11px 15px rgba(0, 0, 0, 0.2));
      flex-direction: column;
      gap: 24px;
      padding: 28px;
      width: 888px;
      max-width: 80vw;
    }

    .ErrorPage-plateform {
      align-self: flex-start;
    }

    /* DEMO-SPECIFIC STYLES */
    .ErrorPage-plateform div {
      animation: type-effect 2.5s steps(11, end), cursor 1s step-end infinite 2.5s;
      border-right: 2px solid #29b6f6;
      box-sizing: border-box;
      color: var(--primary);
      font-family: monospace;
      letter-spacing: 2.5px;
      overflow: hidden;
      white-space: nowrap;
    }

    @keyframes type-effect {
      from {
        width: 0
      }
      to {
        width: 100%
      }
    }
    @keyframes cursor {
      0% {
        border-color: transparent;
      }
      50% {
        border-color: #29b6f6;
      }
      100% {
        border-color: transparent;
      }
    }

    .ErrorPage-illustration {
      width: 358px;
      max-width: 80vw;
    }

    .ErrorPage-title {
      font-family: 'Arial', 'sans-serif';
      font-style: normal;
      font-weight: 500;
      font-size: 42px;
      line-height: 56px;
      text-align: center;
      color: rgba(0, 0, 0, 0.87);
    }

    .ErrorPage-message {
      font-family: 'Arial', 'sans-serif';
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 24px;
      text-align: center;
      letter-spacing: 0.5px;
      color: rgba(0, 0, 0, 0.6);
    }

    .ErrorPage-cta {
      font-family: 'Arial', 'sans-serif';
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 16px;
      text-align: center;
      letter-spacing: 0.75px;
      color: #FFFFFF;
      padding: 10px 20px;
      background-color: var(--primary);
      border-radius: 99px;
      border: none;
      transition: filter 0.2s linear;
      filter: drop-shadow(0px 0px 2px rgba(0, 0, 0, 0.14)) drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.12)) drop-shadow(0px 1px 3px rgba(0, 0, 0, 0.2));
      margin-bottom: 24px;
    }

    .ErrorPage-cta:hover {
      cursor: pointer;
      filter: drop-shadow(0px 2px 4px rgba(0, 0, 0, 0.14)) drop-shadow(0px 4px 5px rgba(0, 0, 0, 0.12)) drop-shadow(0px 1px 10px rgba(0, 0, 0, 0.2)) brightness(105%);
    }

    @media only screen and (max-width: 600px) {
      .ErrorPage {
        background-image: none;
      }

      .ErrorPage-card {
        border-radius: 0;
        filter: none;
        width: 100vw;
        height: 100vh;
        max-width: unset;
      }

      .ErrorPage-title {
        font-style: normal;
        font-weight: 700;
        font-size: 24px;
        line-height: 32px;
      }
    }
  </style>
</head>
<body>
<div class="ErrorPage">
  <div class="ErrorPage-card">
    <div class="ErrorPage-plateform">
      <div>OX Platform</div>
    </div>
    <svg class="ErrorPage-illustration" viewBox="0 0 725 418" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M717.124 359.9C715.924 368.74 709.914 376.72 702.184 381.93C700.343 383.175 698.399 384.262 696.374 385.18C694.148 386.185 691.852 387.027 689.504 387.7C679.204 390.71 668.074 390.84 657.244 390.9C646.294 390.97 635.348 391.04 624.404 391.11C615.774 391.16 606.864 391.16 598.894 387.85C598.775 387.806 598.658 387.756 598.544 387.7C590.714 384.32 584.044 376.89 584.234 368.39C584.311 365.396 585.158 362.471 586.694 359.9C587.711 358.172 588.934 356.574 590.334 355.14C594.674 350.64 600.694 347.11 606.334 344.27C615.634 339.6 626.144 333.54 628.864 323.94C629.152 322.93 629.346 321.896 629.444 320.85C630.084 314.25 626.874 308.18 622.784 302.75C622.384 302.21 621.964 301.67 621.534 301.13C620.614 299.98 619.664 298.85 618.724 297.77C618.324 297.31 617.924 296.84 617.514 296.37C613.734 291.99 609.924 287.31 607.934 282.08C607.078 279.89 606.601 277.57 606.524 275.22C606.304 266.73 615.794 258.18 622.974 262.19C626.494 261.85 630.014 261.5 633.534 261.14C637.854 260.71 642.154 260.28 646.464 259.84H646.474C647.444 259.76 648.394 259.66 649.344 259.56C658.314 258.67 667.278 257.773 676.234 256.87C680.214 256.48 684.894 256.33 687.484 259.39C690.304 262.73 688.844 268.01 686.054 271.38C683.264 274.74 679.394 277.12 676.774 280.6C673.384 285.1 672.594 290.79 673.564 296.37C673.845 297.99 674.267 299.583 674.824 301.13C675.804 303.893 677.16 306.508 678.854 308.9C684.734 317.14 693.444 322.82 701.174 329.35C708.914 335.89 716.154 344.19 717.214 354.25C717.244 354.54 717.274 354.84 717.284 355.14C717.397 356.728 717.343 358.323 717.124 359.9Z" fill="#CFD8DC"/>
      <path d="M169 246C236.931 246 292 190.931 292 123C292 55.069 236.931 0 169 0C101.069 0 46 55.069 46 123C46 190.931 101.069 246 169 246Z" fill="#F2F2F2"/>
      <path d="M0 29C0 73.7715 28.1777 110 63 110L0 29Z" fill="#2F2E41"/>
      <path d="M63 110C63 65.2285 94.3086 29 133 29L63 110Z" fill="#3F51B5"/>
      <path d="M23 34C23 76.0078 40.8906 110 63 110L23 34Z" fill="#3F51B5"/>
      <path d="M63 110C63 52.5156 99.2285 6 144 6L63 110Z" fill="#2F2E41"/>
      <path d="M50 110.862C50 110.862 58.8686 110.584 61.5414 108.654C64.2141 106.723 75.1835 104.417 75.8465 107.514C76.5096 110.611 89.1744 122.915 79.1618 122.997C69.1492 123.079 55.897 121.415 53.2293 119.766C50.5617 118.118 50 110.862 50 110.862Z" fill="#A8A8A8"/>
      <path opacity="0.2" d="M79.3425 121.933C69.3292 122.013 56.0761 120.37 53.4083 118.742C51.3766 117.502 50.567 113.053 50.2961 111C50.1085 111.008 50 111.012 50 111.012C50 111.012 50.5618 118.178 53.2296 119.806C55.8974 121.434 69.1505 123.078 79.1638 122.997C82.0543 122.974 83.0527 121.943 82.9979 120.417C82.5963 121.339 81.4939 121.915 79.3425 121.933Z" fill="black"/>
      <path d="M115 259C174.647 259 223 252.508 223 244.5C223 236.492 174.647 230 115 230C55.3532 230 7 236.492 7 244.5C7 252.508 55.3532 259 115 259Z" fill="#3F3D56"/>
      <path opacity="0.1" d="M115 257C165.258 257 206 251.404 206 244.5C206 237.596 165.258 232 115 232C64.7421 232 24 237.596 24 244.5C24 251.404 64.7421 257 115 257Z" fill="black"/>
      <path d="M483.5 403C573.799 403 647 393.15 647 381C647 368.85 573.799 359 483.5 359C393.201 359 320 368.85 320 381C320 393.15 393.201 403 483.5 403Z" fill="#3F3D56"/>
      <path d="M267 360.839C319.021 374.087 419.018 343.559 430 309" stroke="#2F2E41" stroke-width="2" stroke-miterlimit="10"/>
      <path d="M179 149.058C179 149.058 254.143 128.209 258.767 195.389C263.392 262.568 196.919 281.68 248.941 295" stroke="#2F2E41" stroke-width="2" stroke-miterlimit="10"/>
      <path d="M229.444 311.769C229.444 311.769 252.398 316.47 244.512 335.01C236.626 353.55 216.229 349.57 228.351 360.223" stroke="#2F2E41" stroke-width="2" stroke-miterlimit="10"/>
      <path d="M449.808 324.742L445.259 353.653C445.259 353.653 422.865 365.497 438.611 365.845C454.357 366.194 528.536 365.845 528.536 365.845C528.536 365.845 542.882 365.845 520.138 353.305L515.59 323L449.808 324.742Z" fill="#2F2E41"/>
      <path opacity="0.1" d="M435.439 365.93C439.015 362.733 445.255 359.401 445.255 359.401L449.804 330.219L515.589 330.281L520.138 359.05C525.442 362.002 528.727 364.264 530.649 366C533.571 365.33 536.763 362.841 520.138 353.589L515.589 323L449.804 324.758L445.255 353.941C445.255 353.941 426.437 363.987 435.439 365.93Z" fill="black"/>
      <path d="M620.558 123H344.442C338.675 123 334 127.655 334 133.398V318.602C334 324.345 338.675 329 344.442 329H620.558C626.325 329 631 324.345 631 318.602V133.398C631 127.655 626.325 123 620.558 123Z" fill="#2F2E41"/>
      <path d="M621 134H344V288H621V134Z" fill="#3F3D56"/>
      <path d="M482.5 130C483.328 130 484 129.328 484 128.5C484 127.672 483.328 127 482.5 127C481.672 127 481 127.672 481 128.5C481 129.328 481.672 130 482.5 130Z" fill="#F2F2F2"/>
      <path d="M631 300V318.403C631 319.795 630.73 321.173 630.205 322.459C629.681 323.744 628.912 324.913 627.942 325.897C626.972 326.881 625.821 327.661 624.554 328.194C623.287 328.726 621.929 329 620.558 329H344.442C343.071 329 341.713 328.726 340.446 328.194C339.179 327.661 338.028 326.881 337.058 325.897C336.088 324.913 335.319 323.744 334.795 322.459C334.27 321.173 334 319.795 334 318.403V300H631Z" fill="#2F2E41"/>
      <path d="M560 384.522V388H372V385.217L372.259 384.522L376.892 372H556.156L560 384.522Z" fill="#2F2E41"/>
      <path d="M632.901 381.346C632.557 382.848 631.261 384.433 628.333 385.932C617.826 391.311 596.461 384.498 596.461 384.498C596.461 384.498 580 381.629 580 374.1C580.462 373.776 580.944 373.482 581.443 373.221C585.861 370.829 600.508 364.925 626.479 373.471C628.392 374.086 630.101 375.233 631.415 376.783C632.468 378.044 633.293 379.644 632.901 381.346Z" fill="#2F2E41"/>
      <path opacity="0.1" d="M632.9 381.385C619.928 386.446 608.366 386.824 596.501 378.432C590.517 374.202 585.08 373.155 581 373.235C585.456 370.834 600.228 364.912 626.423 373.485C628.352 374.102 630.076 375.252 631.402 376.808C632.463 378.073 633.295 379.678 632.9 381.385Z" fill="black"/>
      <path d="M616.5 379C618.985 379 621 378.328 621 377.5C621 376.672 618.985 376 616.5 376C614.015 376 612 376.672 612 377.5C612 378.328 614.015 379 616.5 379Z" fill="#F2F2F2"/>
      <path d="M482.5 321C486.09 321 489 318.09 489 314.5C489 310.91 486.09 308 482.5 308C478.91 308 476 310.91 476 314.5C476 318.09 478.91 321 482.5 321Z" fill="#F2F2F2"/>
      <path opacity="0.1" d="M560 385V388H372V385.6L372.259 385H560Z" fill="black"/>
      <path d="M183 92H63V231H183V92Z" fill="#2F2E41"/>
      <path d="M195 78H51V127H195V78Z" fill="#3F3D56"/>
      <path d="M195 137H51V186H195V137Z" fill="#3F3D56"/>
      <g filter="url(#filter0_ddd_696_700)">
        <path d="M634.527 242.216C639.811 251.829 645.281 259.984 645.488 259.965C645.695 259.945 648.73 259.645 649.059 259.602C649.387 259.559 634.827 240.413 629.179 224.547C623.532 208.681 621.53 191.747 623.323 175.001C625.801 153.087 634.651 132.381 648.775 115.445C649.756 114.277 648.078 112.585 647.091 113.76C646.205 114.811 645.339 115.879 644.494 116.963C630.653 134.787 622.339 156.277 620.582 178.776C619.734 189.823 620.54 200.934 622.971 211.744C625.37 222.395 629.26 232.654 634.527 242.216Z" fill="#3F51B5"/>
        <path d="M643.51 118.986C644.475 118.626 645.419 118.213 646.34 117.752C646.386 117.824 646.432 117.897 646.478 117.968L643.51 118.986Z" fill="#3F51B5"/>
        <path d="M661.905 97.97C661.515 100.153 660.811 102.268 659.815 104.25C657.064 109.773 652.683 114.318 647.265 117.27C646.965 117.43 646.655 117.6 646.345 117.75C645.975 117.17 645.595 116.59 645.235 116.01C642.682 112.21 640.795 108.003 639.655 103.57C638.495 98.36 639.125 92.51 642.465 88.34C645.795 84.17 652.155 82.3 656.765 85C658.096 85.7949 659.225 86.8867 660.065 88.19C659.605 88.76 659.144 89.3199 658.685 89.89C658.644 89.9424 658.601 89.9925 658.555 90.04C658.155 90.54 657.745 91.03 657.345 91.53C655.675 93.58 654.015 95.64 652.345 97.68C654.698 98.1129 657.094 98.2773 659.485 98.17C659.675 98.16 659.875 98.15 660.065 98.14C660.675 98.1 661.295 98.05 661.905 97.97Z" fill="#3F51B5"/>
        <path d="M628.175 138.98L627.535 147.32L627.365 149.58C627.185 149.49 626.995 149.4 626.815 149.31C620.595 146.222 615.105 141.843 610.713 136.464C606.321 131.086 603.127 124.832 601.345 118.12C600.749 115.792 600.331 113.422 600.095 111.03C603.335 110.53 606.585 110.02 609.825 109.52C607.202 106.108 603.916 103.261 600.165 101.15C601.02 92.8041 604.036 84.8252 608.915 78C609.755 91.98 624.395 100.87 629.755 113.8C631.399 117.846 632.111 122.211 631.837 126.57C631.564 130.929 630.312 135.171 628.175 138.98Z" fill="#3F51B5"/>
        <path d="M625.394 212.23C624.614 212.27 623.834 212.29 623.065 212.28C616.41 212.238 609.834 210.837 603.74 208.163C597.645 205.489 592.162 201.598 587.625 196.73C583.952 192.708 580.999 188.084 578.895 183.06C578.965 183.05 579.025 183.04 579.095 183.03C582.335 182.53 585.585 182.02 588.825 181.52C585.383 177.036 580.81 173.55 575.575 171.42C574.866 166.901 574.813 162.304 575.415 157.77C582.735 169.7 599.845 170.66 610.655 179.54C615.81 183.837 619.324 189.782 620.605 196.37C620.924 197.998 621.094 199.651 621.115 201.31C621.125 201.71 621.125 202.1 621.115 202.49L621.355 203.03L625.394 212.23Z" fill="#3F51B5"/>
        <path d="M657.555 226.04C655.485 228.58 653.415 231.14 651.345 233.68C653.889 234.148 656.482 234.302 659.065 234.14C656.771 242.354 652.369 249.824 646.295 255.81C645.725 256.37 645.135 256.92 644.535 257.44C644.415 257.55 644.285 257.66 644.165 257.77L643.625 256.24L643.615 256.23L640.555 247.76C636.672 243.857 633.984 238.927 632.805 233.55V233.53C632.087 230.382 631.922 227.134 632.315 223.93C634.025 210.04 645.765 197.55 642.845 183.86C649.278 189.071 654.27 195.843 657.345 203.53C655.675 205.58 654.015 207.64 652.345 209.68C654.698 210.113 657.094 210.277 659.485 210.17C660.322 213.555 660.778 217.023 660.845 220.51C660.855 221.01 660.855 221.5 660.845 222C659.795 223.3 658.735 224.59 657.685 225.89C657.644 225.942 657.601 225.992 657.555 226.04Z" fill="#3F51B5"/>
      </g>
      <path d="M195 195H51V245H195V195Z" fill="#3F3D56"/>
      <path opacity="0.4" d="M166 86H157V96H166V86Z" fill="#0288D1"/>
      <path opacity="0.8" d="M180 86H170V96H180V86Z" fill="#29B6F6"/>
      <path d="M193 86H184V96H193V86Z" fill="#81D4FA"/>
      <path opacity="0.4" d="M166 145H157V154H166V145Z" fill="#0288D1"/>
      <path opacity="0.8" d="M180 145H170V154H180V145Z" fill="#29B6F6"/>
      <path d="M193 145H184V154H193V145Z" fill="#81D4FA"/>
      <path opacity="0.4" d="M166 203H157V212H166V203Z" fill="#0288D1"/>
      <path opacity="0.8" d="M180 203H170V212H180V203Z" fill="#29B6F6"/>
      <path d="M193 203H184V212H193V203Z" fill="#81D4FA"/>
      <rect width="2.33269" height="6.51298" rx="1.16634" transform="matrix(-0.323165 -0.946343 0.946284 -0.323338 258.618 366.313)" fill="#CFD8DC"/>
      <rect width="2.46368" height="10.2772" rx="1.23184" transform="matrix(0.157985 -0.987442 0.987427 0.158076 252.649 359.177)" fill="#CFD8DC"/>
      <rect width="2.68155" height="8.38066" rx="1.34078" transform="matrix(0.864379 -0.502841 0.502618 0.864509 259.867 350.56)" fill="#CFD8DC"/>
      <rect width="2.33269" height="6.51298" rx="1.16634" transform="matrix(0.896743 -0.442551 -0.442388 -0.896824 249.881 305.636)" fill="#CFD8DC"/>
      <rect width="2.46368" height="10.2772" rx="1.23184" transform="matrix(0.584799 -0.811178 -0.811232 -0.584724 259.146 304.79)" fill="#CFD8DC"/>
      <rect width="2.68155" height="8.38066" rx="1.34078" transform="matrix(-0.257691 -0.966228 -0.966161 0.25794 260.112 293.591)" fill="#CFD8DC"/>
      <defs>
        <filter id="filter0_ddd_696_700" x="561" y="67" width="114.904" height="210.965" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
          <feFlood flood-opacity="0" result="BackgroundImageFix"/>
          <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
          <feOffset dy="4"/>
          <feGaussianBlur stdDeviation="2.5"/>
          <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"/>
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_696_700"/>
          <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
          <feOffset dy="3"/>
          <feGaussianBlur stdDeviation="7"/>
          <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/>
          <feBlend mode="normal" in2="effect1_dropShadow_696_700" result="effect2_dropShadow_696_700"/>
          <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
          <feOffset dy="8"/>
          <feGaussianBlur stdDeviation="5"/>
          <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.14 0"/>
          <feBlend mode="normal" in2="effect2_dropShadow_696_700" result="effect3_dropShadow_696_700"/>
          <feBlend mode="normal" in="SourceGraphic" in2="effect3_dropShadow_696_700" result="shape"/>
        </filter>
      </defs>
    </svg>


    <div class="ErrorPage-title">Something went wrong</div>
    <div class="ErrorPage-message">
      We're sorry, an internal error has occurred. <br>
      Please try again later. If the error persists, please notify your administrator.
    </div>
    <button class="ErrorPage-cta" onclick="window.location.reload()">Try again</button>
  </div>
</div>
</body>
</html>
